package admin.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.HubCiti;
import common.util.Utility;

public class RegionAdminValidator implements Validator

{

	public boolean supports(Class<?> clazz) {
		return HubCiti.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		HubCiti hubCiti = (HubCiti) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hubCitiName", "regionName.required");

		if (!Utility.isEmptyOrNullString(hubCiti.getZipCode())) {
			if ("false".equals(hubCiti.getZipCode())) {
				errors.rejectValue("postalCodes", "postalCodes.select");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "citiExperience", "citiExperience.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "userName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailID", "emailID.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salesPersonEmail", "salesContact.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emails", "emails.required");
		if (hubCiti.getDefaultZipcode() == 0) {
			errors.rejectValue("defaultZipcode", "defaultZipcode.required");
		}
	}

	public void validate(Object target, Errors errors, String status) {

		if (status.equals(ApplicationConstants.INVALIDEMAIL)) {
			errors.rejectValue("emailID", "invalid.email");
		}
		
		if (status.equals(ApplicationConstants.INVALIDSALESCONTACT)) {
			errors.rejectValue("salesPersonEmail", "invalid.salesContact");
		}

		if (status.equals(ApplicationConstants.DUPLICATEUSERNAME)) {

			errors.rejectValue("userName", "userName.exist");
		}
		if (status.equals(ApplicationConstants.DUPLICATEHUBCITI)) {

			errors.rejectValue("hubCitiName", "region.exist");
		}
		if (status.equals(ApplicationConstants.DUPLICATEEMAIL)) {

			errors.rejectValue("emailID", "email.exist");
		}
		if (status.equals(ApplicationConstants.DUPLICATESALESCONTACT)) {

			errors.rejectValue("salesPersonEmail", "salesContact.exist");
		}
		
		if (status.equals(ApplicationConstants.INVALID_EMAILS)) {
			errors.rejectValue("emails", "invalid.emails");
		}
		
		if (status.equals(ApplicationConstants.NUMBER_OF_EMAILS)) {
			errors.rejectValue("emails", "no_of_emails.greaterfive");
		}
		
		if (status.equals(ApplicationConstants.DUPLICATE_EMAILS)) {
			errors.rejectValue("emails", "duplicate.emails");
		}
		
		if (status.equals(ApplicationConstants.INVALID_EMAIL))
		{
			errors.rejectValue("emails", "invalid.email");
		}
	}

	public final void editValidate(Object target, Errors errors) {
		HubCiti citi = (HubCiti) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hubCitiName", "hubCitiName.required");
		if ("false".equals(citi.getZipCode())) {
			errors.rejectValue("postalCodes", "postalCodes.select");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "citiExperience", "citiExperience.required");
		if (citi.getDefaultZipcode() == 0) {
			errors.rejectValue("defaultZipcode", "defaultZipcode.required");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emails", "emails.required");
	}
}

/**
 * 
 */
package admin.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.HotDeal;

/**
 * @author sangeetha.ts
 */
public class HotDealValidator implements Validator
{
	/**
	 * Getting the Login Instance.
	 * 
	 * @param arg0
	 *            as request parameter.
	 * @return Login instance.
	 */
	public final boolean supports(Class<?> arg0)
	{
		return HotDeal.class.isAssignableFrom(arg0);
	}

	/**
	 * This method validate user HotDeals Display screen.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors..
	 */
	public final void validate(Object target, Errors errors)
	{
		HotDeal deal = (HotDeal) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hotDealName", "hotDealName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "price");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salePrice", "salePrice");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hotDealShortDescription", "hotDealShortDescription");

		if (null != deal.getPrice() && !"".equals(deal.getPrice()))
		{
			if (deal.getPrice() <= 0)
			{
				errors.rejectValue("price", "pricenotzero");
			}
		}
		if (null != deal.getSalePrice() && !"".equals(deal.getSalePrice()))
		{
			if (deal.getSalePrice() <= 0)
			{
				errors.rejectValue("salePrice", "salepricenotzero");
			}
		}
	}

	/**
	 * This method validate admin hotdeals screen.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors.
	 * @param status
	 *            as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{

		if (status.equals(ApplicationConstants.DATEAFTER))
		{
			errors.rejectValue("hotDealEndDate", "dateafter");
		}
		if (status.equals(ApplicationConstants.SALEPRICEGREATER))
		{
			errors.rejectValue("salePrice", "salepricegreater");
		}
		if (status.equals(ApplicationConstants.INVALIDURL))
		{
			errors.rejectValue("hotDealURL", "invalid.url");
		}
	}

}

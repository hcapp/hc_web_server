package admin.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.Retailer;
import common.util.Utility;

public class RetailerValidator implements Validator {

	/**
	 * Getting the Retailer Instance.
	 * 
	 * @param arg0
	 *            as request parameter.
	 * @return {@link Retailer} instance.
	 */
	public boolean supports(Class<?> arg0) {
		return Retailer.class.isAssignableFrom(arg0);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "userName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "emailID.required");
		
		Retailer retailer = (Retailer) target;
		
		if(retailer.getEmail() != null && !"".equals(retailer.getEmail().trim()) && !Utility.validateEmailId(retailer.getEmail()))	{
			errors.rejectValue("email", "invalid.email");
		}
	}
	
	public final void validate(Object target, Errors errors, String status)	{
		
		if(ApplicationConstants.DUPLICATEUSERNAME.equals(status))	{
			errors.rejectValue("userName", "userName.exist");
		}
		
		if(ApplicationConstants.DUPLICATEEMAIL.equals(status))	{
			errors.rejectValue("email", "email.exist");
		}
	}

}

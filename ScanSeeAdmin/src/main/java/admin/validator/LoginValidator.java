package admin.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import common.pojo.Login;

public class LoginValidator implements Validator
{
	/**
	 * Getting the Login Instance.
	 * 
	 * @param arg0
	 *            as request parameter.
	 * @return Login instance.
	 */
	public final boolean supports(Class<?> arg0)
	{
		return Login.class.isAssignableFrom(arg0);
	}

	/**
	 * This method validate user login screen.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors..
	 */
	public final void validate(Object arg0, Errors errors)
	{
		ValidationUtils.rejectIfEmpty(errors, "userName", "userName.required");
		ValidationUtils.rejectIfEmpty(errors, "password", "password.rquired");
	}

}

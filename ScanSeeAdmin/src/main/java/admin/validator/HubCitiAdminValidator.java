/**
 * 
 */
package admin.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.HubCiti;

/**
 * @author sangeetha.ts
 */
public class HubCitiAdminValidator implements Validator
{
	/**
	 * Getting the Login Instance.
	 * 
	 * @param arg0
	 *            as request parameter.
	 * @return Login instance.
	 */
	public final boolean supports(Class<?> arg0)
	{
		return HubCiti.class.isAssignableFrom(arg0);
	}

	/**
	 * This method validate user login screen.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors..
	 */
	public final void validate(Object target, Errors errors)
	{
		HubCiti citi = (HubCiti) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hubCitiName", "hubCitiName.required");
		if ("false".equals(citi.getZipCode()))
		{
			errors.rejectValue("postalCodes", "postalCodes.select");
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "citiExperience", "citiExperience.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "userName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailID", "emailID.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salesPersonEmail", "salesContact.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emails", "emails.required");
		
		if(citi.getDefaultZipcode() == 0) {
			errors.rejectValue("defaultZipcode", "defaultZipcode.required");
		}
	}

	public void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.INVALIDEMAIL))
		{
			errors.rejectValue("emailID", "invalid.email");
		}
		if (status.equals(ApplicationConstants.INVALIDSALESCONTACT))
		{
			errors.rejectValue("salesPersonEmail", "invalid.salesContact");
		}
		if (status.equals(ApplicationConstants.DUPLICATEUSERNAME))
		{
			errors.rejectValue("userName", "userName.exist");
		}
		if (status.equals(ApplicationConstants.DUPLICATEHUBCITI))
		{
			errors.rejectValue("hubCitiName", "citi.exist");
		}
		if (status.equals(ApplicationConstants.DUPLICATEEMAIL))
		{
			errors.rejectValue("emailID", "email.exist");
		}
		
		if (status.equals(ApplicationConstants.DUPLICATESALESCONTACT))
		{
			errors.rejectValue("salesPersonEmail", "salesContact.exist");
		}
		/*if (status.equals(ApplicationConstants.INVALIDZIPCODE))
		{
			errors.rejectValue("defaultZipcode", "invalid.zipcode");
		}*/
		
		if (status.equals(ApplicationConstants.INVALID_EMAILS)) {
			errors.rejectValue("emails", "invalid.emails");
		}
		
		if (status.equals(ApplicationConstants.NUMBER_OF_EMAILS)) {
			errors.rejectValue("emails", "no_of_emails.greaterfive");
		}
		
		if (status.equals(ApplicationConstants.DUPLICATE_EMAILS)) {
			errors.rejectValue("emails", "duplicate.emails");
		}
		
		if (status.equals(ApplicationConstants.INVALID_EMAIL))
		{
			errors.rejectValue("emails", "invalid.email");
		}
	}

	/**
	 * This method validate user login screen.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors..
	 */
	public final void editValidate(Object target, Errors errors)
	{
		HubCiti citi = (HubCiti) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hubCitiName", "hubCitiName.required");
		if ("false".equals(citi.getZipCode()))
		{
			errors.rejectValue("postalCodes", "postalCodes.select");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "citiExperience", "citiExperience.required");
		
		if(citi.getDefaultZipcode() == 0) {
			errors.rejectValue("defaultZipcode", "defaultZipcode.required");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailID", "emailID.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salesPersonEmail", "salesContact.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emails", "emails.required");
	}
}

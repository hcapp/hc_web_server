/**
* @author Kumar D
* @version 0.1
*
* Class to validate details of a Filter Screen.
*/
package admin.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.pojo.Filter;
import common.util.Utility;

public class FilterValidator implements Validator
{
	/**
	 * Getting the Filter Instance.
	 * 
	 * @param arg0 as request parameter.
	 * @return Filter instance.
	 */
	public final boolean supports(Class<?> arg0)
	{
		return Filter.class.isAssignableFrom(arg0);
	}

	/**
	 * This method validate user Filter Display screen.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors..
	 */
	public final void validate(Object target, Errors errors)
	{
		Filter filter = (Filter) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "filterName", "filterName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fCategory", "select.category");
	
		if (null != filter.getFilterOption() && "yes".equals(filter.getFilterOption())) {
			if ("".equals(Utility.checkNull(filter.getHiddenFValue()))) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fValue", "add.filterValues");
			}                                                   
		}
	}
}

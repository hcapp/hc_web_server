package admin.service;

import java.text.ParseException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;

import common.exception.ScanSeeServiceException;
import common.pojo.RetailerDetails;
import common.pojo.Version;

/**
 * @author Kumar
 */
public interface VersionService
{

	String addVersionDetails(Version objVersion) throws ScanSeeServiceException, MessagingException;

	/**
	 * This service method is used to display list of all version.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @return RetailerDetails object.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	RetailerDetails getAllVersions(Version objVersion) throws ScanSeeServiceException;

	/**
	 * This service method is used to edit the version details based on versioID
	 * as input parameter.
	 * 
	 * @param objVersionId
	 *            as input parameter.
	 * @return Version object.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	Version getVersionDetailsByID(Integer iVersionId, HttpSession session) throws ScanSeeServiceException;

	/**
	 * This service method is used to Update the version details.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String updateVersion(Version objVersion, HttpSession session) throws ScanSeeServiceException, MessagingException, ParseException;
}

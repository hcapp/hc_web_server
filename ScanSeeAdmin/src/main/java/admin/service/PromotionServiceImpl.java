/**
 * 
 */
package admin.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import admin.dao.PromotionDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Promotion;
import common.pojo.PromotionDetails;
import common.util.Utility;

/**
 * @author sangeetha.ts
 *
 */
public class PromotionServiceImpl implements PromotionService
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(PromotionServiceImpl.class);

	/**
	 * Variable promotionDAO declared as instance of promotionDAO.
	 */
	private PromotionDao promotionDAO;

	/**
	 * To set setPromotionDAO.
	 * 
	 * @param promotionDAO
	 *            to set.
	 */
	public final void setPromotionDAO(PromotionDao promotionDAO)
	{
		this.promotionDAO = promotionDAO;
	}

	/**
	 * This method is used to search the Promotion details based on the search key.
	 * This method is also used to sort the columns. This method is also used to
	 * display the expired Promotions based on flag.
	 * @param searchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @param showExpire
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public PromotionDetails getPromotionList(String searchKey, String sortOrder, String columnName, Integer lowerLimit, String showExpire)
			throws ScanSeeServiceException
	{
		final String methodName = "getPromotionList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		PromotionDetails promotionDetails = null;
		Integer expire;
		
		try
		{
			
			if ("true".equals(showExpire))
			{
				expire = 0;
			}
			else
			{
				expire = 1;
			}
			
			promotionDetails = new PromotionDetails();
			promotionDetails = promotionDAO.getPromotionList(searchKey, sortOrder, columnName, lowerLimit, expire);
			
			if (null != promotionDetails)
			{

				for (Promotion promotion : promotionDetails.getPromotions())
				{
					if (null != promotion.getStartDate())
					{
						promotion.setStartDate(Utility.convertDBdate(promotion.getStartDate()));
					}
					if (null != promotion.getEndDate())
					{
						promotion.setEndDate(Utility.convertDBdate(promotion.getEndDate()));
					}
				}

			}
			
		}
		catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return promotionDetails;
	}

	/**
	 * This method is used to display promotion details.
	 * @param promotionId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Promotion displayPromotionDetails(int promotionId) throws ScanSeeServiceException
	{
		final String methodName = "displayPromotionDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		Promotion promotionDetails = null;
		
		try
		{
			
			promotionDetails = new Promotion();
			promotionDetails = promotionDAO.displayPromotionDetails(promotionId);
			
			if(null != promotionDetails.getStartDate())		
			{
				promotionDetails.setStartDate(Utility.convertDBdate(promotionDetails.getStartDate()));
			}
			if(null != promotionDetails.getEndDate())
			{
				promotionDetails.setEndDate(Utility.convertDBdate(promotionDetails.getEndDate()));
			}
			
		}
		catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return promotionDetails;
	}

	/**
	 * This method is used to display users associated with the promotion. 
	 * @param promotionId
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public PromotionDetails displayPromotionUsers(int promotionId, int lowerLimit) throws ScanSeeServiceException
	{
		final String methodName = "displayPromotionUsers";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		PromotionDetails promotionDetails = null;
		
		try
		{
			
			promotionDetails = new PromotionDetails();
			promotionDetails = promotionDAO.displayPromotionUsers(promotionId, lowerLimit);
			
		}
		catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return promotionDetails;
	}

	/**
	 * This method is used to filter the promotions from the list of promotions.
	 * 
	 * @param promotionIds
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String filterPromotions(String promotionIds) throws ScanSeeServiceException
	{
		final String methodName = "filterPromotions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		String status = null;
		
		try
		{
			
			status = promotionDAO.filterPromotions(promotionIds);
			
		}
		catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

}

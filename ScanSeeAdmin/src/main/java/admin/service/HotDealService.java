/**
 * 
 */
package admin.service;

import java.util.ArrayList;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Category;
import common.pojo.HotDeal;
import common.pojo.HotDealDetails;
import common.pojo.RetailerLocation;

/**
 * @author sangeetha.ts
 */
public interface HotDealService
{
	/**
	 * This method returns list of hot deals for the given search key.
	 * 
	 * @param deal
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public HotDealDetails displayHotDeals(HotDeal deal) throws ScanSeeServiceException;

	/**
	 * This method is used to delete the hotDeals.
	 * 
	 * @param hotDealIds
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deleteHotDeal(String hotDealIds) throws ScanSeeServiceException;

	public HotDeal displayHotDealDetails(int hotdealId) throws ScanSeeServiceException;

	/**
	 * This method is used to display hot deal Categories.
	 * 
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<Category> displayHotDealCategories() throws ScanSeeServiceException;

	/**
	 * This method is used to display Retailer Locations.
	 * 
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<RetailerLocation> displayRetailerLocations(Integer retailId, String city, String state) throws ScanSeeServiceException;

	/**
	 * This method is used to update hot deal details.
	 * 
	 * @param hotDeal
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	public String updateHotDeal(HotDeal hotDeal) throws ScanSeeServiceException;

}

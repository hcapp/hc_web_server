/**
* @author Kumar D
* @version 0.1
*
* Class to store Business service details of a FilterServiceImpl.
*/
package admin.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import admin.dao.FilterDao;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Category;
import common.pojo.Filter;
import common.pojo.FilterInfo;
import common.pojo.FilterValues;

public class FilterServiceImpl implements FilterService
{
	
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(FilterServiceImpl.class);

	/**
	 * Variable filterDAO declared as instance of FilterDao.
	 */
	private FilterDao filterDAO;

	/**
	 * @param filterDAO the filterDAO to set
	 */
	public void setFilterDAO(FilterDao filterDAO) {
		this.filterDAO = filterDAO;
	}

	
	/**
	 * The serviceImpl method for displaying all the filter categories and it
	 * will call DAO.
	 * 
	 * @param iUserId as input parameter.
	 * @throws ScanSeeServiceException as service exception.
	 * @return arCategoryList,List of categories.
	 */
	public  ArrayList<Category> getAllFilterCategory(Integer iUserId) throws ScanSeeServiceException {
		LOG.info("Inside FilterServiceImpl :  getAllFilterCategory ");
		
		ArrayList<Category> arCategoriesList = null;
		try {
			
			arCategoriesList = filterDAO.getAllFilterCategory(iUserId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside FilterServiceImpl :  getAllFilterCategory : " + e);
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info("Exit FilterServiceImpl :  getAllFilterCategory ");
		return arCategoriesList;
	}
	

	/**
	 * The serviceImpl method for displaying all the filters and it will
	 * calling DAO.
	 * 
	 * @param filter instance of Filter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return Filters,List of filters.
	 */
	public FilterInfo getAllFilters(Filter filter) throws ScanSeeServiceException {
		LOG.info("Inside FilterServiceImpl :  getAllFilters");
		
		FilterInfo objFilterInfo = null;
		try {

			objFilterInfo = filterDAO.getAllFilters(filter);
		} catch(ScanSeeWebSqlException e) {
			LOG.error("Inside FilterServiceImpl :  getAllFilterCategory : " + e);
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info("Exit FilterServiceImpl :  getAllFilters");
		return objFilterInfo;
	}
	
	/**
	 * This serviceImpl method is used to Delete the Filter.
	 * 
	 * @param iFilter as input parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteFilter(Integer iFilter) throws ScanSeeServiceException {
		LOG.info("Inside FilterServiceImpl : deleteFilter");

		String strResponse = null;;
		try
		{
			strResponse = filterDAO.deleteFilter(iFilter);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside FilterServiceImpl :  deleteFilter : " + e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit FilterServiceImpl : deleteFilter");
		return strResponse;
	}
	/**
	 * This serviceImpl method is used to Add/Update the Filter details.
	 * 
	 * @param filter instance of Filter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String addUpdateFilter(Filter filter) throws ScanSeeServiceException {
		LOG.info("Inside FilterServiceImpl : addUpdateFilter");
		
		String strResponse = null;;
		try
		{
			strResponse = filterDAO.addUpdateFilter(filter);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside FilterServiceImpl :  addUpdateFilter : " + e);
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info("Exit FilterServiceImpl : addUpdateFilter");
		return strResponse;
	}
	
	
	/**
	 * This serviceImpl method is used to edit the filter details based on filterId
	 * as input parameter.
	 * 
	 * @param iFilterId as input parameter.
	 * @param iUserId as input parameter.
	 * @return Filter object.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public Filter getFilterDetailsByID(Integer iFilterId, Integer iUserId)throws ScanSeeServiceException {
		LOG.info("Inside FilterServiceImpl : getFilterDetailsByID");
		
		Filter objFilter = null;
		try {
			
			objFilter = filterDAO.getFilterDetailsByID(iFilterId, iUserId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside FilterServiceImpl :  getFilterDetailsByID : " + e);
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info("Exit FilterServiceImpl : getFilterDetailsByID");
		return objFilter;
	}
	
	
	/**
	 * This serviceImpl method is used to Add the Filter value.
	 * 
	 * @param filterVal instance of FilterValues.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String addUpdateFilterValue(FilterValues filterVal) throws ScanSeeServiceException {
		LOG.info("Inside FilterServiceImpl : addUpdateFilterValue");

		String strResponse = null;;
		try
		{
			strResponse = filterDAO.addUpdateFilterValue(filterVal);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside FilterServiceImpl :  addUpdateFilterValue : " + e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit FilterServiceImpl : addUpdateFilterValue");
		return strResponse;
	}

	/**
	 * This serviceImpl method is used to Delete the Filter value.
	 * 
	 * @param iFilterVal as input parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteFilterValue(Integer iFilterVal) throws ScanSeeServiceException {
		LOG.info("Inside FilterServiceImpl : deleteFilterValue");

		String strResponse = null;;
		try
		{
			strResponse = filterDAO.deleteFilterValue(iFilterVal);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside FilterServiceImpl :  deleteFilterValue : " + e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit FilterServiceImpl : deleteFilterValue");
		return strResponse;
	}
	
	/**
	 * The serviceImpl method for displaying all the filter values and it will
	 * calling DAO.
	 * 
	 * @param iUserId as input parameter.
	 * @param iFilterId as input parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return filterValues, List of filterValues.
	 */
	public ArrayList<FilterValues> getAllFilterValues(Integer iUserId, Integer iFilterId) throws ScanSeeServiceException {
		LOG.info("Inside FilterServiceImpl :  getAllFilterValues ");

		ArrayList<FilterValues> arFilterValList = null;
		try {

			arFilterValList = filterDAO.getAllFilterValues(iUserId, iFilterId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside FilterServiceImpl :  getAllFilterValues : " + e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit FilterServiceImpl :  getAllFilterValues ");
		return arFilterValList;
	}
	
 }


package admin.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import admin.dao.LoginDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Login;
import common.util.EncryptDecryptPwd;

public class LoginServiceImpl implements LoginService {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(LoginServiceImpl.class);

	/**
	 * Variable loginDAO declared as instance of loginDAO.
	 */
	private LoginDao loginDAO;

	/**
	 * To set setLoginDAO.
	 * 
	 * @param setLoginDAO
	 *            to set.
	 */
	public final void setLoginDAO(LoginDao loginDAO) {
		this.loginDAO = loginDAO;
	}

	/**
	 * This method is used for user login authentication
	 * 
	 * @param userName
	 * @param password
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Login loginAuthentication(String userName, String password) throws ScanSeeServiceException {
		final String methodName = "loginAuthentication";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		EncryptDecryptPwd enryptDecryptpwd;
		String encryptedpwd = null;
		Login login = null;
		try {
			/*
			 * enryptDecryptpwd = new EncryptDecryptPwd(); encryptedpwd =
			 * enryptDecryptpwd.encrypt(password);
			 */

			login = new Login();
			login = loginDAO.loginAuthentication(userName, password);

		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}/*
		 * catch (NoSuchAlgorithmException e) {
		 * LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
		 * e.getStackTrace()); } catch (NoSuchPaddingException e) {
		 * LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
		 * e.getStackTrace()); } catch (InvalidKeyException e) {
		 * LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
		 * e.getStackTrace()); } catch (InvalidAlgorithmParameterException e) {
		 * LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
		 * e.getStackTrace()); } catch (InvalidKeySpecException e) {
		 * LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
		 * e.getStackTrace()); } catch (IllegalBlockSizeException e) {
		 * LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
		 * e.getStackTrace()); } catch (BadPaddingException e) {
		 * LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
		 * e.getStackTrace()); }
		 */

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return login;
	}

}

/**
 * 
 */
package admin.service;

import java.util.List;

import common.exception.ScanSeeServiceException;
import common.pojo.HubCiti;
import common.pojo.Modules;

/**
 * @author sangeetha.ts
 *
 */
public interface ExportService {
	
	public List<HubCiti> getExportHubCities() throws ScanSeeServiceException;
	
	public List<Modules> getExportType() throws ScanSeeServiceException;
	
	public List<Modules> getExportOptions(int exportType) throws ScanSeeServiceException;
	
	public String exportData(Modules module) throws ScanSeeServiceException;
	
	public String exportRetailerData(Modules module) throws ScanSeeServiceException;
	
	public String saveExportHubCities(HubCiti hubCiti) throws ScanSeeServiceException;

}

package admin.service;

import common.exception.ScanSeeServiceException;
import common.pojo.Login;

public interface LoginService
{
	/**
	 * This method is used for user login authentication
	 * @param userName
	 * @param password
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Login loginAuthentication(String userName, String password) throws ScanSeeServiceException;

}

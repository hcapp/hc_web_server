/**
 * 
 */
package admin.service;

import java.util.ArrayList;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AlertCategory;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.Retailer;
import common.pojo.RetailerDetails;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationDetails;
import common.pojo.State;

/**
 * @author sangeetha.ts
 */
public interface RetailerService
{
	/**
	 * This method is used to display list of Retailers.
	 * 
	 * @param SearchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public RetailerDetails displayRetailerList(Retailer retailerInfo) throws ScanSeeServiceException;

	/**
	 * The ServiceImpl method for displaying all the states
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL exception.
	 * @return states,List of states.
	 */
	public ArrayList<State> getAllStates() throws ScanSeeServiceException;

	/**
	 * The service method for all the cities for the given statename and search
	 * keyword.
	 * 
	 * @param stateName
	 *            -for which cities to be fetched
	 * @return the XML containing cities information in the response.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public ArrayList<City> fetchAllCities(String stateName) throws ScanSeeServiceException;

	/**
	 * This method is used to delete the Retailers from the list of Retailers.
	 * 
	 * @param retailerIds
	 * @param userId
	 * @return success or failure
	 * @throws ScanSeeServiceException
	 */
	public String deleteRetailer(String retailerIds, Integer userId,Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This method is used to display list of Retailer locations.
	 * 
	 * @param SearchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeException
	 */
	public RetailerLocationDetails displayRetailerLocation(RetailerLocation retailerInfo) throws ScanSeeServiceException;

	/**
	 * This method is used to delete the Retailer Locations from the list of
	 * Retailer Locations.
	 * 
	 * @param retailerIds
	 * @param userId
	 * @return success or failure
	 * @throws ScanSeeServiceException
	 */
	public String deleteRetailerLocation(String retailerLocationIds, Integer userId) throws ScanSeeServiceException;

	public String saveLocationList(RetailerLocation retailerLocation, long userId) throws ScanSeeServiceException;

	public String saveRetailerInfo(Retailer retailer) throws ScanSeeServiceException;
	
	/**
	 * This service method used get All Business Category/Sub Category associated to Retailer/Location.
	 * 
	 * @param Retailer objRetailer
	 * @return List of Business Category.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be  thrown.
	 */
	public ArrayList<Category> fetchBusCategory(Retailer objRetailer) throws ScanSeeServiceException;
	
	/**
	 * This service method used to update Business Category/Sub Category associated to Retailer/Location.
	 * 
	 * @param objRetailer
	 * @return success or failure
	 * @throws ScanSeeServiceException
	 * 	           If any exception occurs ScanSeeServiceException will be  thrown.
	 */
	public String updateBusCategory(Retailer objRetailer) throws ScanSeeServiceException;
	
	/**
	 * Service Method to get retailer login user name and password.
	 * 
	 * @param objRetailer
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Retailer retailerLoginDetails(Retailer objRetailer) throws ScanSeeServiceException;
	
	/**
	 * Service method to save/update retailer login details i.e. User name, email
	 * and auto generated password.
	 * Updated/Created values will be sent to mail id given by retailer admin.
	 * 
	 * @param objRetailer
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String saveRetailerLoginDetails(Retailer objRetailer) throws ScanSeeServiceException;
	
	/**
	 * Service method to paid/unpaid Band Retailer
	 * 
	 * @param retailedId ,userId,isPaid,isBand
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String saveRetailerPaymentInfo(Integer retailId, Integer userID, Boolean isPaid,Boolean isBand) throws ScanSeeServiceException;
	
	/**
	 * Fetch Band Retailer Business Category and subCategory
	 * 
	 * @param retailerId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Category getBandcat(String retailId)throws ScanSeeServiceException;

	public String savebandcat(String retailId,String buscatIds,String subcatIds,String userID)throws ScanSeeServiceException;
	/**
	 * Below method is used to display retailer location subcategoires.
	 * @param retailer
	 * @return
	 * @throws ScanSeeServiceException
	 */
	AlertCategory getRetLocFilters(Retailer retailer)throws ScanSeeServiceException;
	
	public String associateFilters(RetailerLocation objRetailerLocation) throws ScanSeeServiceException;
	
	
}

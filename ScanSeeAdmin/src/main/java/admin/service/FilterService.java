/**
* @author Kumar D
* @version 0.1
*
* FilterService Interface class that has the service methods.
*/
package admin.service;

import java.util.ArrayList;

import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.Filter;
import common.pojo.FilterInfo;
import common.pojo.FilterValues;

public interface FilterService
{

	/**
	 * The service method for displaying all the filter categories and it will
	 * calling DAO.
	 * 
	 * @param iUserId as input parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return categories,List of categories.
	 */
	ArrayList<Category> getAllFilterCategory(Integer iUserId) throws ScanSeeServiceException;

	/**
	 * The service method for displaying all the filters and it will
	 * calling DAO.
	 * 
	 * @param filter instance of Filter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return Filters,List of filters.
	 */
	FilterInfo getAllFilters(Filter filter) throws ScanSeeServiceException;
			
	/**
	 * This service method is used to Delete the Filter.
	 * 
	 * @param iFilter as input parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String deleteFilter(Integer iFilter) throws ScanSeeServiceException;
			
	/**
	 * This service method is used to Add/Update the Filter details.
	 * 
	 * @param filter instance of Filter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String addUpdateFilter(Filter filter) throws ScanSeeServiceException;
	
	/**
	 * This service method is used to edit the filter details based on filterId
	 * as input parameter.
	 * 
	 * @param iFilterId as input parameter.
	 * @param iUserId as input parameter.
	 * @return Filter object.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	Filter getFilterDetailsByID(Integer iFilterId, Integer iUserId) throws ScanSeeServiceException;
	
	/**
	 * This service method is used to Add the Filter value.
	 * 
	 * @param filterVal instance of FilterValues.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String addUpdateFilterValue(FilterValues filterVal) throws ScanSeeServiceException;

	/**
	 * This service method is used to Delete the Filter value.
	 * 
	 * @param iFilterVal as input parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String deleteFilterValue(Integer iFilterVal) throws ScanSeeServiceException;
	
	/**
	 * The service method for displaying all the filter values and it will
	 * calling DAO.
	 * 
	 * @param iUserId as input parameter.
	 * @param iFilterId as input parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return filterValues, List of filterValues.
	 */
	ArrayList<FilterValues> getAllFilterValues(Integer iUserId, Integer iFilterId) throws ScanSeeServiceException;

}

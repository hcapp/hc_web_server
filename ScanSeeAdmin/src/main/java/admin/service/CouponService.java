package admin.service;

import java.util.List;

import common.exception.ScanSeeServiceException;
import common.pojo.Coupon;
import common.pojo.CouponDetails;
import common.pojo.Product;

public interface CouponService
{
	/**
	 * This method is used to search the coupon details based on the search key.
	 * This method is also used to sort the columns. This method is also used to
	 * display the expired coupons based on flag.
	 * @param searchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @param showExpire
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public CouponDetails getSearchCouponDetail(String searchKey, String sortOrder, String coloumnName, Integer lowerLimit, String showExpire)
			throws ScanSeeServiceException;
	/**
	 * This method is used for product search based on the product name.
	 * @param searchKey
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public List<Product> getSearchProduct(String searchKey)	throws ScanSeeServiceException;
	/**
	 * This method is used to fetch the list of products associated to the
	 * coupon form database.
	 * @param productIds
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public List<Product> getAssociatedProducts(String productIds) throws ScanSeeServiceException;
	/**
	 * This method is used to save the coupon details.
	 * @param coupon
	 * @param couponAmount
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String saveCoupon(Coupon coupon, Double couponAmount) throws ScanSeeServiceException;
	/**
	 * This method is used to get the coupon details.
	 * @param couponId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Coupon getCouponDetails(Integer couponId) throws ScanSeeServiceException;
	
	/**
	 * This method is used to save the edit coupon.
	 * @param coupon
	 * @param couponAmount
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String updateCoupon(Coupon coupon, Double couponAmount) throws ScanSeeServiceException;
	/**
	 * This method is used delete the coupon.
	 * @param couponId
	 * @param type
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deleteCoupon(Integer couponId, String type) throws ScanSeeServiceException;
}

/**
 * 
 */
package admin.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import admin.dao.ExportDao;
import admin.dao.VersionDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.DashboardExportData;
import common.pojo.EmailComponent;
import common.pojo.ExportDetails;
import common.pojo.HubCiti;
import common.pojo.Modules;
import common.pojo.ReportColumn;
import common.pojo.RetailerExportData;
import common.util.Utility;
import common.util.Utility.FormatType;

/**
 * @author sangeetha.ts
 *
 */
public class ExportServiceImpl implements ExportService {
	
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ExportServiceImpl.class);
	
	private static final Integer MAX_SHEETSIZE = 65536;

	/**
	 * Variable exportDAO declared as instance of ExportDao.
	 */
	private ExportDao exportDAO;
	private VersionDao versionDao;
	/**
	 * @param versionDao
	 *            the versionDao to set.
	 */
	public void setVersionDao(VersionDao versionDao)
	{
		this.versionDao = versionDao;
	}

	/**
	 * To set setExportDAO.
	 * 
	 * @param setExportDAO
	 *            to set.
	 */
	public final void setExportDAO(ExportDao exportDAO) {
		this.exportDAO = exportDAO;
	}
	
	public List<HubCiti> getExportHubCities() throws ScanSeeServiceException {
		
		final String methodName = "getExportHubCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<HubCiti> hubCities = null;
		
		try {			
			hubCities = exportDAO.getExportHubCities();			
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hubCities;
	}
	
	public List<Modules> getExportType() throws ScanSeeServiceException {
		
		final String methodName = "getExportType";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Modules> modules = null;
		
		try {			
			modules = exportDAO.getExportType();			
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return modules;
	}
	
	public List<Modules> getExportOptions(int exportType) throws ScanSeeServiceException {
		
		final String methodName = "getExportOptions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Modules> modules = null;
		
		try {			
			modules = exportDAO.getExportOptions(exportType);			
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return modules;
	}
	
	public String exportData(Modules module) throws ScanSeeServiceException {
		
		final String methodName = "exportData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ExportDetails details = null;
		List<DashboardExportData> exportData = null;
		String status = null;
		ArrayList<AppConfiguration> appConfigList = null;
		String[] strToMail = null;
		String strFromMail = null;
		String smtpHost = null;
		String smtpPort = null;
		String strFilePathForBody = null;
		String strSubject = "Data Export";
		File file = null;
		Boolean isFileCreated = null;
		final String fileSeparator = System.getProperty("file.separator");

		ArrayList<AppConfiguration> emailConf = null;
		Integer numberOfSheet = 0;			
		List<DashboardExportData> exportDataSubList = null;	
		
		try {	
			
			details = exportDAO.exportData(module);		
			if(details != null) {
			
				exportData = details.getDashboardExportData();
				
				if(null != exportData && !exportData.isEmpty()) {
					
					ReportColumn[] reportColumns = new ReportColumn[] {
		                    new ReportColumn("retailID", "Retail ID", FormatType.GENERAL),
		                    new ReportColumn("retailName", "Retail Name", FormatType.TEXT),
		                    new ReportColumn("retailLocationID", "Retail LocationID", FormatType.GENERAL),
		                    new ReportColumn("hubCitiName", "HubCiti Name", FormatType.TEXT),
		                    new ReportColumn("categoryName", "Category Name", FormatType.TEXT),
		                    new ReportColumn("subCategoryName", "Sub-Category Name", FormatType.TEXT),
		                    new ReportColumn("retailLocationImpression", "Retail Location Impression", FormatType.INTEGER),
		                    new ReportColumn("retailLocationClicks", "Retail Location Clicks", FormatType.INTEGER),
		                    new ReportColumn("appsiteImpression", "Appsite Impression", FormatType.INTEGER),
		                    new ReportColumn("appsiteClicks", "Appsite Clicks", FormatType.INTEGER),
		                    new ReportColumn("anythingPageImpression", "AnythingPage Impression", FormatType.INTEGER),
		                    new ReportColumn("anythingPageClicks", "AnythingPage Clicks", FormatType.INTEGER),
		                    new ReportColumn("retailerSpecialsImpression", "Retailer Specials Impression", FormatType.INTEGER),
		                    new ReportColumn("retailerSpecialsClicks", "Retailer Specials Clicks", FormatType.INTEGER),
		                    new ReportColumn("hotdealImpression", "Hotdeal Impression", FormatType.INTEGER),
		                    new ReportColumn("hotdealClicks", "Hotdeal Clicks", FormatType.INTEGER),
		                    new ReportColumn("couponImpression", "Coupon Impression", FormatType.INTEGER),
		                    new ReportColumn("couponClicks", "Coupon Clicks", FormatType.INTEGER),
		                    new ReportColumn("dealImpression", "Deal Impression", FormatType.INTEGER),
		                    new ReportColumn("dealClicks", "Deal Clicks", FormatType.INTEGER)};
					
						Utility util = new Utility();
						Integer size = exportData.size();
						Integer listSize = size;
						while(size >= MAX_SHEETSIZE) {
							numberOfSheet++;
							size = size - MAX_SHEETSIZE;
						}						
						if(size > 0) {
							numberOfSheet++;
						}
						
						for(int i = 1; i <= numberOfSheet; i++) {
							
							Integer fromIndex = 0;
							Integer toIndex = listSize;
							
							if(i == 1 && numberOfSheet > 1) {
								fromIndex = 0;
								toIndex = MAX_SHEETSIZE - 2;
							} else if (i > 1) {								
								fromIndex = (MAX_SHEETSIZE * (i - 1)) - 1;
								toIndex = (MAX_SHEETSIZE * i) - 2;
							}
							
							if(toIndex > listSize) {
								toIndex = listSize;
							}
							
							exportDataSubList = exportData.subList(fromIndex, toIndex);
							util.addSheet(exportDataSubList, reportColumns, "Dashboard_Data"+i);							
						}
						
						final StringBuilder mediaTempPathBuilder = Utility.getExportFilePath(ApplicationConstants.DATAEXPORT);
						final String finalPath = mediaTempPathBuilder.toString() + fileSeparator + details.getFilePath();
						
						file = new File(finalPath);
						
						isFileCreated = file.createNewFile();
						
						if(isFileCreated) {
						
							OutputStream output = new FileOutputStream(file);
							
							util.write(output);
							output.close();			
							
							strFilePathForBody = Utility.emailExportBodyContent(module);
							
							appConfigList = versionDao.getAppConfig(ApplicationConstants.EXPORTEMAILCONFIG);
		
							emailConf = versionDao.getAppConfig(ApplicationConstants.EMAILCONFIG);
		
							for (int j = 0; j < emailConf.size(); j++)
							{
								if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
								{
									smtpHost = emailConf.get(j).getScreenContent();
								}
								if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
								{
									smtpPort = emailConf.get(j).getScreenContent();
								}
							}
		
							if (appConfigList != null && !appConfigList.isEmpty())
							{
								for (int j = 0; j < appConfigList.size(); j++)
								{
									if (appConfigList.get(j).getScreenName().equals(ApplicationConstants.EXPORT_FROM))
									{
										 strFromMail = appConfigList.get(j).getScreenContent();
									}
									if (appConfigList.get(j).getScreenName().equals(ApplicationConstants.EXPORT_RECEPIENTS))
									{
										strToMail = appConfigList.get(j).getScreenContent().split(",");
									}
								}
								EmailComponent.sendMailWithAttachment(strToMail, strFromMail, strSubject, strFilePathForBody,
										finalPath, smtpHost, smtpPort);
								status = ApplicationConstants.SUCCESSTEXT;
						}
					}
				}
			}
			
			
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
			
		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e.getMessage());
			throw new ScanSeeServiceException(e);
			
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}
	
	public String exportRetailerData(Modules module) throws ScanSeeServiceException {
		
		final String methodName = "exportRetailerData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ExportDetails details = null;
		List<RetailerExportData> exportData = null;
		String status = null;
		ArrayList<AppConfiguration> appConfigList = null;
		String[] strToMail = null;
		String strFromMail = null;
		String smtpHost = null;
		String smtpPort = null;
		String strFilePathForBody = null;
		String strSubject = "Data Export";
		File file = null;
		Boolean isFileCreated = null;
		final String fileSeparator = System.getProperty("file.separator");

		ArrayList<AppConfiguration> emailConf = null;
		Integer numberOfSheet = 0;		
		List<RetailerExportData> exportDataSubList = null;
		
		try {	
			
			details = exportDAO.exportRetailerData(module);
			
			if(null != details) {
				
				exportData = details.getRetailerExportData();
			
				if(null != exportData && !exportData.isEmpty()) {
					
					ReportColumn[] reportColumns = new ReportColumn[] {
		                    new ReportColumn("hubCitiName", "HubCiti Name", FormatType.TEXT),
		                    new ReportColumn("retailID", "Retail ID", FormatType.GENERAL),
		                    new ReportColumn("retailName", "Retail Name", FormatType.TEXT),
		                    new ReportColumn("retailerLogo", "Retailer Logo", FormatType.TEXT),
		                    new ReportColumn("retailLocationID", "Retail LocationID", FormatType.GENERAL),
		                    new ReportColumn("address", "Address", FormatType.TEXT),
		                    new ReportColumn("city", "City", FormatType.TEXT),
		                    new ReportColumn("state", "State", FormatType.TEXT),
		                    new ReportColumn("postalCode", "Postal Code", FormatType.GENERAL),
		                    new ReportColumn("retailLocationLogo", "Retail Location Logo", FormatType.TEXT),
		                    new ReportColumn("website", "Website", FormatType.TEXT),
		                    new ReportColumn("contactFirstName", "Contact First Name", FormatType.TEXT),
		                    new ReportColumn("contactLastName", "Contact Last Name", FormatType.TEXT),
		                    new ReportColumn("contactPhone", "Contact Phone", FormatType.TEXT),
		                    new ReportColumn("retailerAssociatedDate", "Retailer Associated Date", FormatType.TEXT),
		                    new ReportColumn("categoryName", "Category Name", FormatType.TEXT),
		                    new ReportColumn("subCategoryName", "Sub-Category Name", FormatType.TEXT),
		                    new ReportColumn("filterID", "Filter ID", FormatType.GENERAL),
		                    new ReportColumn("filterName", "Filter Name", FormatType.TEXT)};
					
						Utility util = new Utility();
						Integer size = exportData.size();
						Integer listSize = size;
						while(size >= MAX_SHEETSIZE) {
							numberOfSheet++;
							size = size - MAX_SHEETSIZE;
						}						
						if(size > 0) {
							numberOfSheet++;
						}
						
						for(int i = 1; i <= numberOfSheet; i++) {
							
							Integer fromIndex = 0;
							Integer toIndex = listSize;
							
							if(i == 1 && numberOfSheet > 1) {
								fromIndex = 0;
								toIndex = MAX_SHEETSIZE - 1;
							} else if (i > 1) {								
								fromIndex = MAX_SHEETSIZE * (i - 1);
								toIndex = (MAX_SHEETSIZE * i) - 1;
							}
							
							if(toIndex > listSize) {
								toIndex = listSize;
							}
							
							exportDataSubList = exportData.subList(fromIndex, toIndex);
							util.addSheet(exportDataSubList, reportColumns, "Dashboard_Data"+i);							
						}
						
						final StringBuilder mediaTempPathBuilder = Utility.getExportFilePath(ApplicationConstants.DATAEXPORT);
						final String finalPath = mediaTempPathBuilder.toString() + fileSeparator + details.getFilePath();
						
						file = new File(finalPath);
						
						isFileCreated = file.createNewFile();
						
						if(isFileCreated) {
						
							OutputStream output = new FileOutputStream(file);
							
							util.write(output);
							output.close();			
							
							strFilePathForBody = Utility.emailExportBodyContent(module);
							
							appConfigList = versionDao.getAppConfig(ApplicationConstants.EXPORTEMAILCONFIG);
		
							emailConf = versionDao.getAppConfig(ApplicationConstants.EMAILCONFIG);
		
							for (int j = 0; j < emailConf.size(); j++)
							{
								if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
								{
									smtpHost = emailConf.get(j).getScreenContent();
								}
								if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
								{
									smtpPort = emailConf.get(j).getScreenContent();
								}
							}
		
							if (appConfigList != null && !appConfigList.isEmpty())
							{
								for (int j = 0; j < appConfigList.size(); j++)
								{
									if (appConfigList.get(j).getScreenName().equals(ApplicationConstants.EXPORT_FROM))
									{
										 strFromMail = appConfigList.get(j).getScreenContent();
									}
									if (appConfigList.get(j).getScreenName().equals(ApplicationConstants.EXPORT_RECEPIENTS))
									{
										strToMail = appConfigList.get(j).getScreenContent().split(",");
									}
								}
								EmailComponent.sendMailWithAttachment(strToMail, strFromMail, strSubject, strFilePathForBody,
										finalPath, smtpHost, smtpPort);
								status = ApplicationConstants.SUCCESSTEXT;
						}
					}
				}
			}
			
			
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
			
		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e.getMessage());
			throw new ScanSeeServiceException(e);
			
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}
	
	public String saveExportHubCities(HubCiti hubCiti) throws ScanSeeServiceException {
		
		final String methodName = "exportRetailerData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		String response = null;
		
		try {			
			response = exportDAO.saveExportHubCities(hubCiti);			
		} catch(ScanSeeWebSqlException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}

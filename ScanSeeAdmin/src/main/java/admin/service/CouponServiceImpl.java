/**
 * 
 */
package admin.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import admin.dao.CouponDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Coupon;
import common.pojo.CouponDetails;
import common.pojo.Product;
import common.util.Utility;

/**
 * @author sangeetha.ts
 */
public class CouponServiceImpl implements CouponService
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(CouponServiceImpl.class);

	/**
	 * Variable couponDAO declared as instance of SupplierDAO.
	 */
	private CouponDao couponDAO;

	/**
	 * To set setCouponDAO.
	 * 
	 * @param setCouponDAO
	 *            to set.
	 */
	public final void setCouponDAO(CouponDao couponDAO)
	{
		this.couponDAO = couponDAO;
	}
	
	/**
	 * This method is used to search the coupon details based on the search key.
	 * This method is also used to sort the columns. This method is also used to
	 * display the expired coupons based on flag.
	 * @param searchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @param showExpire
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public CouponDetails getSearchCouponDetail(String searchKey, String sortOrder, String coloumnName, Integer lowerLimit, String showExpire)
			throws ScanSeeServiceException
	{
		final String methodName = "getSearchCouponDetail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		CouponDetails couponDetails = null;
		Integer expire = null;

		try
		{
			if ("true".equals(showExpire))
			{
				expire = 0;
			}
			else
			{
				expire = 1;
			}

			couponDetails = new CouponDetails();
			couponDetails = couponDAO.getSearchCouponDetail(searchKey, sortOrder, coloumnName, lowerLimit, expire);
			if (null != couponDetails)
			{

				for (Coupon coupon : couponDetails.getCouponDetailList())
				{
					if (null != coupon.getCouponStartDate())
					{
						coupon.setCouponStartDate(Utility.convertDBdate(coupon.getCouponStartDate()));
					}
					if (null != coupon.getCouponExpireDate())
					{
						coupon.setCouponExpireDate(Utility.convertDBdate(coupon.getCouponExpireDate()));
					}
				}

			}
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return couponDetails;
	}

	/**
	 * This method is used for product search based on the product name.
	 * @param searchKey
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public List<Product> getSearchProduct(String searchKey) throws ScanSeeServiceException
	{
		final String methodName = "getSearchProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Product> products = null;
		try
		{
			products = new ArrayList<Product>();
			products = couponDAO.getSearchProduct(searchKey);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return products;
	}

	/**
	 * This method is used to fetch the list of products associated to the
	 * coupon form database.
	 * @param productIds
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public List<Product> getAssociatedProducts(String productIds) throws ScanSeeServiceException
	{
		final String methodName = "getAssociatedProducts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Product> products = null;
		try
		{
			products = new ArrayList<Product>();
			products = couponDAO.getAssociatedProducts(productIds);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return products;
	}

	/**
	 * This method is used to save the coupon details.
	 * @param coupon
	 * @param couponAmount
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String saveCoupon(Coupon coupon, Double couponAmount) throws ScanSeeServiceException
	{
		final String methodName = "saveCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;
		try
		{
			status = couponDAO.saveCoupon(coupon, couponAmount);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * This method is used to get the coupon details.
	 * @param couponId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Coupon getCouponDetails(Integer couponId) throws ScanSeeServiceException
	{
		final String methodName = "getCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Coupon coupon = null;
		try
		{
			coupon = new Coupon();
			coupon = couponDAO.getCouponDetails(couponId);
			
			if(null != coupon)
			{
				if(null != coupon.getCouponStartDate())
				{
					coupon.setCouponStartDate(Utility.convertDBdate(coupon.getCouponStartDate()));
				}
				if(null != coupon.getCouponExpireDate())
				{
					coupon.setCouponExpireDate(Utility.convertDBdate(coupon.getCouponExpireDate()));
				}
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return coupon;
	}

	/**
	 * This method is used to save the edit coupon.
	 * @param coupon
	 * @param couponAmount
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String updateCoupon(Coupon coupon, Double couponAmount) throws ScanSeeServiceException
	{
		final String methodName = "updateCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;
		try
		{
			status = couponDAO.updateCoupon(coupon, couponAmount);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * This method is used delete the coupon.
	 * @param couponId
	 * @param type
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deleteCoupon(Integer couponId, String type) throws ScanSeeServiceException
	{
		final String methodName = "deleteCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;
		try
		{
			status = couponDAO.deleteCoupon(couponId, type);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

}

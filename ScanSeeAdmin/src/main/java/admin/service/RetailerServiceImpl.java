/**
 * 
 */
package admin.service;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import admin.dao.HubCitiDao;
import admin.dao.RetailerDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AlertCategory;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.Retailer;
import common.pojo.RetailerDetails;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationDetails;
import common.pojo.State;
import common.util.EncryptDecryptPwd;
import common.util.Utility;

/**
 * @author sangeetha.ts
 */
public class RetailerServiceImpl implements RetailerService {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RetailerServiceImpl.class);

	/**
	 * Variable retailerDAO declared as instance of RetailerDao.
	 */
	private RetailerDao retailerDAO;

	/**
	 * Variable hubCitiDAO declared as instance of HubCitiDao
	 */
	private HubCitiDao hubCitiDAO;

	/**
	 * To set setRetailerDAO.
	 * 
	 * @param retailerDAO
	 *            to set.
	 */
	public final void setRetailerDAO(RetailerDao retailerDAO) {
		this.retailerDAO = retailerDAO;
	}

	/**
	 * To set hubCitiDAO.
	 * 
	 * @param hubCitiDAO
	 */
	public final void setHubCitiDAO(HubCitiDao hubCitiDAO) {
		this.hubCitiDAO = hubCitiDAO;
	}

	/**
	 * This method is used to display list of Retailers.
	 * 
	 * @param SearchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public RetailerDetails displayRetailerList(Retailer retailerInfo) throws ScanSeeServiceException {
		final String methodName = "displayRetailerList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		RetailerDetails retailerDetails = null;

		try {
			retailerDetails = new RetailerDetails();

			retailerDetails = retailerDAO.displayRetailerList(retailerInfo);
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerDetails;
	}

	/**
	 * The ServiceImpl method for displaying all the states
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL exception.
	 * @return states,List of states.
	 */
	public ArrayList<State> getAllStates() throws ScanSeeServiceException {
		final String methodName = "getAllStates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<State> states = null;

		try {
			states = new ArrayList<State>();
			states = retailerDAO.getAllStates();
		} catch (ScanSeeWebSqlException exception) {

			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return states;
	}

	/**
	 * The service method for all the cities for the given statename and search
	 * keyword.
	 * 
	 * @param stateName
	 *            -for which cities to be fetched
	 * @return the XML containing cities information in the response.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public final ArrayList<City> fetchAllCities(String stateName) throws ScanSeeServiceException {
		final String methodName = "fetchAllCities";
		LOG.info("In Service layer:" + ApplicationConstants.METHODSTART + methodName);
		ArrayList<City> cities = null;
		try {
			cities = retailerDAO.getAllCities(stateName);
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}
		return cities;
	}

	/**
	 * This method is used to delete the Retailers from the list of Retailers.
	 * 
	 * @param retailerIds
	 * @param userId
	 * @return success or failure
	 * @throws ScanSeeServiceException
	 */
	public String deleteRetailer(String retailerIds, Integer userId, Boolean isBand) throws ScanSeeServiceException {
		final String methodName = "deleteRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {

			status = retailerDAO.deleteRetailer(retailerIds, userId, isBand);
			if (null != status && !status.equals(ApplicationConstants.FAILURETEXT)) {
				status = Utility.clearRetailerCache(status);

			}
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * This method is used to display list of Retailer locations.
	 * 
	 * @param SearchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public RetailerLocationDetails displayRetailerLocation(RetailerLocation retailerInfo) throws ScanSeeServiceException {
		final String methodName = "displayRetailerLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		RetailerLocationDetails retailerLocationDetails = null;

		try {
			retailerLocationDetails = new RetailerLocationDetails();
			retailerLocationDetails = retailerDAO.displayRetailerLocation(retailerInfo);

			if (null != retailerLocationDetails) {

				if (null != retailerLocationDetails.getRetailerLocations() && !retailerLocationDetails.getRetailerLocations().isEmpty()) {
					String retLocImg = null;
					for (int i = 0; i < retailerLocationDetails.getRetailerLocations().size(); i++) {
						retLocImg = retailerLocationDetails.getRetailerLocations().get(i).getGridImgLocationPath();

						if (!Utility.isEmptyOrNullString(retLocImg)) {
							final int dotIndex = retLocImg.lastIndexOf('.');
							final int slashIndex = retLocImg.lastIndexOf("/");

							if (dotIndex == -1) {
								retailerLocationDetails.getRetailerLocations().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
								retailerLocationDetails.getRetailerLocations().get(i).setGridImgLocationPath(null);

							} else if (retLocImg.contains(ApplicationConstants.IMAGES_SLASH_RETAILER)
									&& !retLocImg.contains(ApplicationConstants.SLASH_LOCATIONLOGO_SLASH)) {
								retailerLocationDetails.getRetailerLocations().get(i).setImgLocationPath(retLocImg);
								retailerLocationDetails.getRetailerLocations().get(i).setGridImgLocationPath(null);

							} else

							{
								if (!Utility.isEmptyOrNullString(retLocImg.substring(slashIndex + 1, retLocImg.length()))) {
									retailerLocationDetails.getRetailerLocations().get(i).setImgLocationPath(retLocImg);
									retailerLocationDetails.getRetailerLocations().get(i)
											.setGridImgLocationPath(retLocImg.substring(slashIndex + 1, retLocImg.length()));
								} else {

									retailerLocationDetails.getRetailerLocations().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
									retailerLocationDetails.getRetailerLocations().get(i).setGridImgLocationPath(null);
								}
							}

						} else {
							retailerLocationDetails.getRetailerLocations().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
						}

					}

				}
				if (null != retailerLocationDetails.getResponse()) {
					Utility.clearRetailerCache(retailerLocationDetails.getResponse());
				}

			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerLocationDetails;
	}

	/**
	 * This method is used to delete the Retailer Locations from the list of
	 * Retailer Locations.
	 * 
	 * @param retailerIds
	 * @param userId
	 * @return success or failure
	 * @throws ScanSeeServiceException
	 */
	public String deleteRetailerLocation(String retailerLocationIds, Integer userId) throws ScanSeeServiceException {
		final String methodName = "deleteRetailerLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {

			status = retailerDAO.deleteRetailerLocation(retailerLocationIds, userId);
			if (null != status && !status.equals(ApplicationConstants.FAILURETEXT)) {
				status = Utility.clearRetailerCache(status);
			}
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	public final String saveLocationList(RetailerLocation retailerLocation, long userId) throws ScanSeeServiceException {
		final String methodName = "saveLocationList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		String strCacheURI = null;

		try {
			final String prodJson = retailerLocation.getLocationJson().replace("\t", "&#09;");
			final List<RetailerLocation> locationList = Utility.jsonToObjectList(prodJson);

			// for saving retailer location list images to server.

			if (null != locationList && !locationList.isEmpty()) {
				for (int i = 0; i < locationList.size(); i++) {
					if (locationList.get(i).isUploadImage()) {
						if (!Utility.isEmptyOrNullString(locationList.get(i).getGridImgLocationPath())) {
							final String fileSeparator = System.getProperty("file.separator");
							StringBuilder mediaPathBuilder = Utility.getMediaLocationPath(ApplicationConstants.RETAILER, locationList.get(i)
									.getRetailId());
							StringBuilder mediaTempPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
							String strMediaPathBuilder = mediaPathBuilder.toString();
							String strMediaTempPathBuilder = mediaTempPathBuilder.toString();
							InputStream inputStream = new BufferedInputStream(new FileInputStream(strMediaTempPathBuilder + fileSeparator
									+ locationList.get(i).getGridImgLocationPath()));
							if (null != inputStream) {
								Utility.writeFileData(inputStream, strMediaPathBuilder + fileSeparator + locationList.get(i).getGridImgLocationPath());
							}
						}
					}
				}
			}

			response = retailerDAO.saveLocationList(locationList, userId);

			if (null != response && !response.equals(ApplicationConstants.FAILURETEXT)) {
				strCacheURI = retailerDAO.getClearCacheForFind();
			}

			if (null != strCacheURI && !strCacheURI.equals(ApplicationConstants.FAILURETEXT)) {
				response = Utility.clearRetailerCache(strCacheURI);
			}

		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String saveRetailerInfo(Retailer retailer) throws ScanSeeServiceException {
		final String methodName = "saveRetailerInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {

			if (null != retailer.getLatitude() && retailer.getLatitude().equalsIgnoreCase("")) {
				retailer.setLatitude(null);
			}
			if (null != retailer.getLongitude() && retailer.getLongitude().equalsIgnoreCase("")) {
				retailer.setLongitude(null);
			}
			status = retailerDAO.saveRetailerInfo(retailer);

			if (null != status && !status.equals(ApplicationConstants.FAILURETEXT)) {
				status = Utility.clearRetailerCache(status);

			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * This RetailerServiceImpl method used get All Business Category associated
	 * to Retailer/Location.
	 * 
	 * @param Retailer
	 *            objRetailer
	 * @return List of Business Category.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public ArrayList<Category> fetchBusCategory(Retailer objRetailer) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : fetchBusCategory");
		ArrayList<Category> arBusCategoryList = null;
		try {
			arBusCategoryList = retailerDAO.fetchBusCategory(objRetailer);
		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}
		return arBusCategoryList;
	}

	/**
	 * This serviceImpl method used to update Business Category/Sub Category
	 * associated to Retailer/Location.
	 * 
	 * @param objRetailer
	 * @return success or failure
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public String updateBusCategory(Retailer objRetailer) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : updateBusCategory");
		String status = null;
		String categoryType = null;

		try {
			status = retailerDAO.updateBusCategory(objRetailer);
			// Check the status if it is null or Failure or which contains
			if (ApplicationConstants.BUSINESS_CATEGORIES.equals(objRetailer.getCategoryType())) {
				categoryType = objRetailer.getCategoryType();
			} else {
				categoryType = "subCategory";
			}
			if (null != status && !status.equals(ApplicationConstants.FAILURETEXT)) {
				status = Utility.clearCache(status, categoryType);
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + " updateBusCategory :" + e.getMessage());

		}
		return status;
	}

	/**
	 * Service Method to get retailer login user name and password.
	 * 
	 * @param objRetailer
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Retailer retailerLoginDetails(Retailer objRetailer) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : retailerLoginDetails");
		Retailer retailer = null;
		try {
			retailer = new Retailer();
			retailer = retailerDAO.retailerLoginDetails(objRetailer);
		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}
		return retailer;
	}

	/**
	 * Service method to save/update retailer login details i.e. User name,
	 * email and auto generated password. Updated/Created values will be sent to
	 * mail id given by retailer admin.
	 * 
	 * @param objRetailer
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String saveRetailerLoginDetails(Retailer objRetailer) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : saveRetailerLoginDetails");
		String autogenPassword = null;
		String encryptPassword = null;
		String status = null;
		String smtpHost = null;
		String smtpPort = null;
		String strAdminEmailId = null;
		String scanSeeUrl = null;
		EncryptDecryptPwd objEncryptDecrypt = null;
		Retailer objRetailerResponse = null;

		try {
			autogenPassword = Utility.randomString(5);
			objEncryptDecrypt = new EncryptDecryptPwd();
			encryptPassword = objEncryptDecrypt.encrypt(autogenPassword);
			objRetailer.setPassword(autogenPassword);
			objRetailerResponse = retailerDAO.saveRetailerLoginDetails(objRetailer, encryptPassword);
			if (ApplicationConstants.SUCCESSTEXT.equals(objRetailerResponse.getStatus())) {
				objRetailer.setRetailName(objRetailerResponse.getRetailName());
				final ArrayList<AppConfiguration> emailConf = hubCitiDAO.getAppConfig(ApplicationConstants.EMAILCONFIG);
				for (int j = 0; j < emailConf.size(); j++) {
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST)) {
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT)) {
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}
				final ArrayList<AppConfiguration> adminEmailList = hubCitiDAO.getAppConfig(ApplicationConstants.WEBREGISTRATION);
				for (int j = 0; j < adminEmailList.size(); j++) {
					if (adminEmailList.get(j).getScreenName().equals(ApplicationConstants.ADMINEMAILID)) {
						strAdminEmailId = adminEmailList.get(j).getScreenContent();
						break;
					}
				}
				final ArrayList<AppConfiguration> list = hubCitiDAO.getAppConfig(ApplicationConstants.FACEBOOKCONFG);
				for (int j = 0; j < list.size(); j++) {
					if (list.get(j).getScreenName().equals(ApplicationConstants.SCANSEEBASEURL)) {
						scanSeeUrl = list.get(j).getScreenContent();
						scanSeeUrl = scanSeeUrl + "login.htm";
						break;
					}
				}
				String emailResponse = Utility.sendMailRetailerLoginSuccess(objRetailer, smtpHost, smtpPort, strAdminEmailId, scanSeeUrl);
				if (emailResponse == null || !ApplicationConstants.SUCCESS.equals(emailResponse)) {
					LOG.error("Email sending failed to retailer :" + objRetailer.getRetailName() + ", emailId :" + objRetailer.getEmail());
				}
			}
			status = objRetailerResponse.getStatus();
		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return status;
	}

	/**
	 * Service Method to get retailer login user name and password.
	 * 
	 * @param objRetailer
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String saveRetailerPaymentInfo(Integer retailId, Integer userID, Boolean isPaid, Boolean isBand) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : saveRetailerPaymentInfo");
		String status = null;

		try {
			status = retailerDAO.saveRetailerPaymentInfo(retailId, userID, isPaid, isBand);
		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}

		return status;
	}

	public Category getBandcat(String retailId) throws ScanSeeServiceException {
		final String methodName = "getBandcat";
		LOG.info("In Service layer:" + ApplicationConstants.METHODSTART + methodName);
		Category category = null;
		try {
			category = retailerDAO.getBandcat(retailId);
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}
		return category;
	}

	public String savebandcat(String retailId, String buscatIds, String subcatIds, String userID) throws ScanSeeServiceException {
		final String methodName = "getBandcat";
		LOG.info("In Service layer:" + ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try {
			response = retailerDAO.savebandcat(retailId, buscatIds, subcatIds, userID);
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}
		return response;
	}

	/**
	 * This method is used to display filter list based on retailer location.
	 */
	public AlertCategory getRetLocFilters(Retailer retailer) throws ScanSeeServiceException {

		final String strMethodName = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		AlertCategory filterlst = null;
		try {

			filterlst = retailerDAO.getRetLocFilters(retailer);

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return filterlst;
	}

	
	public String associateFilters(RetailerLocation objRetailerLocation) throws ScanSeeServiceException {

		final String strMethodName = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String status = null;
		try {

			status = retailerDAO.associateFilters(objRetailerLocation);
			if (null != status && !status.equals(ApplicationConstants.FAILURETEXT)) {
				status = Utility.clearRetailerCache(status);
			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return status;
	}
}

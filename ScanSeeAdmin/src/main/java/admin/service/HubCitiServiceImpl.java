/**
 * 
 */
package admin.service;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;

import admin.dao.HubCitiDao;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.APIPartner;
import common.pojo.AlertCategory;
import common.pojo.AppConfiguration;
import common.pojo.EventCategory;
import common.pojo.HubCiti;
import common.pojo.HubCitiDetails;
import common.pojo.PostalCode;
import common.pojo.RegionInfo;
import common.util.Utility;

/**
 * @author sangeetha.ts
 */
public class HubCitiServiceImpl implements HubCitiService {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(HubCitiServiceImpl.class);

	/**
	 * Variable hubCitiDAO declared as instance of hubCitiDAO.
	 */
	private HubCitiDao hubCitiDAO;

	/**
	 * To set setHubCitiDAO.
	 * 
	 * @param setHubCitiDAO
	 *            to set.
	 */
	public final void setHubCitiDAO(HubCitiDao hubCitiDAO) {
		this.hubCitiDAO = hubCitiDAO;
	}

	/**
	 * This method returns list of hubciti admins for the given search key.
	 * 
	 * @param userId
	 * @param searchKey
	 * @param lowerLimit
	 * @return HubCitiDetails
	 * @throws ScanSeeServiceException
	 */
	public HubCitiDetails displayHubCiti(Integer userId, String searchKey, Integer lowerLimit, Boolean showDeactivated)
			throws ScanSeeServiceException {
		final String methodName = "displayHubCiti";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HubCitiDetails citiDetails = null;

		try {
			citiDetails = hubCitiDAO.displayHubCiti(userId, searchKey, lowerLimit, showDeactivated);
			
			if(null != citiDetails && null != citiDetails.getResponse())
			{
				Utility.clearRetailerCache(citiDetails.getResponse());
			}
			
			
		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return citiDetails;
	}

	/**
	 * This method is used to deactivate the hudciti admin based on admin id
	 * given.
	 * 
	 * @param adminId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deactivateAdmin(Integer userId, Integer adminId, Integer activeFlag) throws ScanSeeServiceException {
		final String methodName = "deactivateAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {
			status = hubCitiDAO.deactivateAdmin(userId, adminId, activeFlag);
		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * this method used get All PostalCodes.
	 * 
	 * @param stateName
	 *            -For which PostalCodes to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public ArrayList<PostalCode> getAllPostalCode(String city, String state) throws ScanSeeServiceException {
		final String methodName = "getAllPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<PostalCode> postalCodes = null;

		try {
			postalCodes = new ArrayList<PostalCode>();
			postalCodes = hubCitiDAO.getAllPostalCode(city, state);
		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return postalCodes;
	}

	public String createAdmin(HubCiti citi, String strLogoImage, ArrayList<HubCiti> postalCodes) throws ScanSeeServiceException {
		final String methodName = "createAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String enryptPassword = null;
		String response = null;
		String smtpHost = null;
		String smtpPort = null;
		String strAdminEmailId = null;
		String strResponse = null;
		String autogenPassword = null;
		Integer userId = citi.getUserID();

		try {
			autogenPassword = Utility.randomString(5);
			citi.setPassword(autogenPassword);
			PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
			enryptPassword = passwordEncoder.encodePassword(autogenPassword, citi.getUserName());
			citi.setEncrptedPassword(enryptPassword);

			response = hubCitiDAO.createAdmin(citi);

			if (response != null && !response.equals(ApplicationConstants.DUPLICATEHUBCITI)
					&& !response.equals(ApplicationConstants.DUPLICATEUSERNAME) && !response.equals(ApplicationConstants.DUPLICATEEMAIL)) {

				Integer hubCitiId = Integer.parseInt(response);
				response = hubCitiDAO.savePostalCodeList(postalCodes, hubCitiId, userId);

				if (null != response && response.equals(ApplicationConstants.SUCCESS)) {

					response = hubCitiDAO.AssociateBottomButtons(userId, hubCitiId);
					response = hubCitiDAO.AssociateRetailLoctions(userId, hubCitiId);

				}

				if (null != response && response.equals(ApplicationConstants.SUCCESS)) {
					final ArrayList<AppConfiguration> emailConf = hubCitiDAO.getAppConfig(ApplicationConstants.EMAILCONFIG);
					for (int j = 0; j < emailConf.size(); j++) {
						if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST)) {
							smtpHost = emailConf.get(j).getScreenContent();
						}
						if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT)) {
							smtpPort = emailConf.get(j).getScreenContent();
						}
					}
					final ArrayList<AppConfiguration> adminEmailList = hubCitiDAO.getAppConfig(ApplicationConstants.WEBREGISTRATION);
					for (int j = 0; j < adminEmailList.size(); j++) {
						if (adminEmailList.get(j).getScreenName().equals(ApplicationConstants.ADMINEMAILID)) {
							strAdminEmailId = adminEmailList.get(j).getScreenContent();
						}
					}
					final ArrayList<AppConfiguration> list = hubCitiDAO.getAppConfig(ApplicationConstants.HUBCITICONFG);
					for (int j = 0; j < list.size(); j++) {
						if (list.get(j).getScreenName().equals(ApplicationConstants.SCANSEEBASEURL)) {

							citi.setHubCitiUrl(list.get(j).getScreenContent());

						}
					}
					strResponse = Utility.sendMailHubCitiLoginSuccess(citi, smtpHost, smtpPort, strAdminEmailId, strLogoImage);
					if (strResponse != null && strResponse.equals(ApplicationConstants.SUCCESS)) {
						response = ApplicationConstants.SUCCESS;
					}
				}
			}

		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e.getStackTrace());
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String getDomainName() throws ScanSeeServiceException {
		final String methodName = "getDomainName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String strDomainName = null;
		try {
			ArrayList<AppConfiguration> domainNameList = hubCitiDAO.getAppConfigForGeneralImages(ApplicationConstants.SERVER_CONFIGURATION);
			for (int j = 0; j < domainNameList.size(); j++) {
				strDomainName = domainNameList.get(j).getScreenContent();
			}
		} catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strDomainName;
	}

	public HubCiti editAdmin(Integer userID, Integer hubCitiID) throws ScanSeeServiceException {
		final String methodName = "editAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HubCiti citi = null;

		try {
			citi = new HubCiti();
			citi = hubCitiDAO.editAdmin(userID, hubCitiID);

		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return citi;
	}

	public String saveAdmin(HubCiti citi, ArrayList<HubCiti> postalCodes, HubCiti removedPostalCodes) throws ScanSeeServiceException {
		final String methodName = "saveAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;

		try {
			response = hubCitiDAO.createAdmin(citi);

			if (response != null && response.equals(ApplicationConstants.SUCCESS) && null != citi.getActive() && true == citi.getActive()) {
				hubCitiDAO.deletePostalCodeAssociation(citi.getHubCitiID(), removedPostalCodes);
				response = hubCitiDAO.savePostalCodeList(postalCodes, citi.getHubCitiID(), citi.getUserID());
				response = hubCitiDAO.AssociateRetailLoctions(citi.getUserID(), citi.getHubCitiID());
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e.getStackTrace());
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * this method used get All PostalCodes associated to hubciti.
	 * 
	 * @param sAdminID
	 *            -For which PostalCodes to be fetched.
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public ArrayList<PostalCode> getPostalCode(Integer sAdminID, Integer hubCitiId) throws ScanSeeServiceException {
		final String methodName = "getPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<PostalCode> postalCodes = null;

		try {
			postalCodes = new ArrayList<PostalCode>();
			postalCodes = hubCitiDAO.getPostalCode(sAdminID, hubCitiId);
		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return postalCodes;
	}

	/**
	 * this method used get All PostalCodes associated to hubciti.
	 * 
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public ArrayList<HubCiti> fetchAdminAssociatedPostalCode(Integer hubCitiId) throws ScanSeeServiceException {
		final String methodName = "getPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<HubCiti> postalCodes = null;

		try {
			postalCodes = hubCitiDAO.fetchAdminAssociatedPostalCode(hubCitiId);

		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return postalCodes;
	}

	/**
	 * This service method is used to fetch event categories and also used to
	 * search event categories.
	 * 
	 * @param category
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public AlertCategory fetchEventCategories(EventCategory category, String userId) throws ScanSeeServiceException {
		final String methodName = "fetchEventCategories";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		AlertCategory objCategory = null;

		try {
			objCategory = hubCitiDAO.fetchEventCategories(category, userId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objCategory;
	}

	/**
	 * This method is used to delete event categories.
	 * 
	 * @param cateId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deleteEventCategory(int cateId) throws ScanSeeServiceException {
		final String strMethodName = "deleteEventCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		String strResponse = null;

		try {
			strResponse = hubCitiDAO.deleteEventCategory(cateId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strResponse;
	}

	/**
	 * This service method is used to add event categories
	 * 
	 * @param catName
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String addEventCategory(EventCategory eventCatObj) throws ScanSeeServiceException {
		final String strMethodName = "addEventCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String response = null;
		final String fileSeparator = System.getProperty("file.separator");
		String strResponse = null;

		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.HUBCITI);
			StringBuilder mediaTempPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			if (!"".equals(Utility.checkNull(eventCatObj.getCateImgName()))) {
				if (!ApplicationConstants.BLANKIMAGE.equals(eventCatObj.getCateImgName())) {
					InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator
							+ eventCatObj.getCateImgName()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream, retMediaPath + fileSeparator + eventCatObj.getCateImgName());
					}
				}
			}

			strResponse = hubCitiDAO.addEventCategory(eventCatObj);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strResponse;
	}

	/**
	 * This service method is used to update event categories.
	 * 
	 * @param category
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String updateEventCategory(EventCategory category, String userId) throws ScanSeeServiceException {
		final String strMethodName = "updateEventCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		String strResponse = null;

		final String fileSeparator = System.getProperty("file.separator");

		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.HUBCITI);
			StringBuilder mediaTempPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			if (null != category.getCatId() && !"".equalsIgnoreCase(category.getCatId().toString())) {
				if (null != category.getOldCateImgName() && !"".equalsIgnoreCase(category.getOldCateImgName())) {
					if (!category.getOldCateImgName().equals(category.getCateImgName())) {
						if (!"".equals(Utility.checkNull(category.getCateImgName()))) {
							if (!ApplicationConstants.BLANKIMAGE.equals(category.getCateImgName())) {
								InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator
										+ category.getCateImgName()));
								if (null != inputStream) {
									Utility.writeFileData(inputStream, retMediaPath + fileSeparator + category.getCateImgName());
								}
							}
						}
					}
				} else {
					if (!"".equals(Utility.checkNull(category.getCateImgName()))) {
						if (!ApplicationConstants.BLANKIMAGE.equals(category.getCateImgName())) {
							InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator
									+ category.getCateImgName()));
							if (null != inputStream) {
								Utility.writeFileData(inputStream, retMediaPath + fileSeparator + category.getCateImgName());
							}
						}
					}
				}
			} else {
				if (!"".equals(Utility.checkNull(category.getCateImgName()))) {
					if (!ApplicationConstants.BLANKIMAGE.equals(category.getCateImgName())) {
						InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator
								+ category.getCateImgName()));
						if (null != inputStream) {
							Utility.writeFileData(inputStream, retMediaPath + fileSeparator + category.getCateImgName());
						}
					}
				}
			}

			strResponse = hubCitiDAO.updateEventCategory(category, userId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strResponse;
	}

	/**
	 * Service method is used to fetch fundraiser categories and also used to
	 * search event categories.
	 * 
	 * @param category
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public AlertCategory fetchFundraiserCategories(EventCategory category, String userId) throws ScanSeeServiceException {
		final String methodName = "fetchFundraiserCategories";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		AlertCategory objCategory = null;

		try {
			objCategory = hubCitiDAO.fetchFundraiserCategories(category, userId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objCategory;
	}

	/**
	 * This service method is used to delete fundraiser categories.
	 * 
	 * @param cateId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deleteFundraiserCategory(int cateId) throws ScanSeeServiceException {
		final String strMethodName = "deleteEventCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		String strResponse = null;

		try {
			strResponse = hubCitiDAO.deleteFundraiserCategory(cateId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strResponse;
	}

	/**
	 * This service method is used to add/update fundraiser categories
	 * 
	 * @param catName
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String addFundraiserCategory(EventCategory eventCatObj) throws ScanSeeServiceException {
		final String strMethodName = "addFundraiserCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String response = null;
		final String fileSeparator = System.getProperty("file.separator");
		String strResponse = null;

		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.HUBCITI);
			StringBuilder mediaTempPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			if (null != eventCatObj.getCatId() && !"".equalsIgnoreCase(eventCatObj.getCatId().toString())) {
				if (null != eventCatObj.getOldCateImgName() && !"".equalsIgnoreCase(eventCatObj.getOldCateImgName())) {
					if (!eventCatObj.getOldCateImgName().equals(eventCatObj.getCateImgName())) {
						if (!"".equals(Utility.checkNull(eventCatObj.getCateImgName()))) {
							if (!ApplicationConstants.BLANKIMAGE.equals(eventCatObj.getCateImgName())) {
								InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator
										+ eventCatObj.getCateImgName()));
								if (null != inputStream) {
									Utility.writeFileData(inputStream, retMediaPath + fileSeparator + eventCatObj.getCateImgName());
								}
							}
						}
					}
				} else {
					if (!"".equals(Utility.checkNull(eventCatObj.getCateImgName()))) {
						if (!ApplicationConstants.BLANKIMAGE.equals(eventCatObj.getCateImgName())) {
							InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator
									+ eventCatObj.getCateImgName()));
							if (null != inputStream) {
								Utility.writeFileData(inputStream, retMediaPath + fileSeparator + eventCatObj.getCateImgName());
							}
						}
					}
				}

			} else {
				if (!"".equals(Utility.checkNull(eventCatObj.getCateImgName()))) {
					if (!ApplicationConstants.BLANKIMAGE.equals(eventCatObj.getCateImgName())) {
						InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator
								+ eventCatObj.getCateImgName()));
						if (null != inputStream) {
							Utility.writeFileData(inputStream, retMediaPath + fileSeparator + eventCatObj.getCateImgName());
						}
					}
				}
			}

			strResponse = hubCitiDAO.addFundraiserCategory(eventCatObj);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strResponse;
	}

	/**
	 * This method is used to display regions.
	 */
	public RegionInfo displayRegions(HubCiti hubCiti) throws ScanSeeServiceException {

		final String strMethodName = "displayRegions";
		RegionInfo regionInfo = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		try {

			regionInfo = hubCitiDAO.displayRegions(hubCiti);
			if(null != regionInfo && null != regionInfo.getResponse())
			{
				Utility.clearRetailerCache(regionInfo.getResponse());
			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName);
			throw new ScanSeeServiceException(exception);

		}
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return regionInfo;
	}

	/**
	 * This method is used to deactivate regions.
	 */
	public String deactivateRegion(HubCiti hubCiti) throws ScanSeeServiceException {
		final String strMethodName = "deactivateRegion";
		String strResponse = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		try {

			strResponse = hubCitiDAO.deactivateRegion(hubCiti);

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return strResponse;
	}

	/**
	 * This method is used to fetch hubCities.
	 */
	public ArrayList<HubCiti> fetchHubCitis() throws ScanSeeServiceException {

		final String strMethodName = "";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ArrayList<HubCiti> hubCiList = null;
		try {

			hubCiList = hubCitiDAO.fetchHubCities();

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return hubCiList;
	}

	public RegionInfo getHubCitiRegions(HubCiti hubCiti) throws ScanSeeServiceException {
		
		final String strMethodName = "getHubCitiRegions";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		
		RegionInfo hubCitiRegLst = null;

		try {
			hubCitiRegLst = hubCitiDAO.getHubCitiRegions(hubCiti);
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return hubCitiRegLst;
	}

	public String saveNationWideDeals(HubCiti hubCiti) throws ScanSeeServiceException {
		
		final String strMethodName = "saveNationWideDeals";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);		

		String response = null;

		try {
			response = hubCitiDAO.saveNationWideDeals(hubCiti);
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return response;
	}

	public ArrayList<HubCiti> getDefaultPostalCodes() throws ScanSeeServiceException {
		final String strMethodName = "getDefaultPostalCodes";
		ArrayList<HubCiti> defaultZipCodelst = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		try {

			defaultZipCodelst = (ArrayList<HubCiti>) hubCitiDAO.getDefaultPostalCodes();

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return defaultZipCodelst;
	}
	
	public ArrayList<APIPartner> fetchNWAPIPartners() throws ScanSeeServiceException {
		
		final String strMethodName = "fetchNWAPIPartners";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		
		ArrayList<APIPartner> apiPartners = null;

		try {
			apiPartners = hubCitiDAO.fetchNWAPIPartners();
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return apiPartners;
	}
}

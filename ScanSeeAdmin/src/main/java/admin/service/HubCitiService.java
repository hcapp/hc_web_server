/**
 * 
 */
package admin.service;

import java.util.ArrayList;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.APIPartner;
import common.pojo.AlertCategory;
import common.pojo.EventCategory;
import common.pojo.HubCiti;
import common.pojo.HubCitiDetails;
import common.pojo.PostalCode;
import common.pojo.RegionInfo;

/**
 * @author sangeetha.ts
 */
public interface HubCitiService
{
	/**
	 * This method returns list of hubciti admins for the given search key.
	 * 
	 * @param userId
	 * @param searchKey
	 * @param lowerLimit
	 * @return HubCitiDetails
	 * @throws ScanSeeServiceException
	 */
	public HubCitiDetails displayHubCiti(Integer userId, String searchKey, Integer lowerLimit, Boolean showDeactivated)
			throws ScanSeeServiceException;

	/**
	 * This method is used to deactivate the hudciti admin based on admin id
	 * given.
	 * 
	 * @param adminId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deactivateAdmin(Integer userId, Integer adminId, Integer activeFlag) throws ScanSeeServiceException;

	/**
	 * this method used get All PostalCodes.
	 * 
	 * @param stateName
	 *            -For which PostalCodes to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public ArrayList<PostalCode> getAllPostalCode(String city, String state) throws ScanSeeServiceException;

	/**
	 * This method is used to create the hudciti admin.
	 * 
	 * @param citi
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String createAdmin(HubCiti citi, String strLogoImage, ArrayList<HubCiti> postalCodes) throws ScanSeeServiceException;

	/**
	 * This serviceImpl method is used to domain Name for Application
	 * configuration.
	 * 
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String getDomainName() throws ScanSeeServiceException;

	/**
	 * This method is used to get the details of hudciti admin.
	 * 
	 * @param userID
	 * @param hubCitiID
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public HubCiti editAdmin(Integer userID, Integer hubCitiID) throws ScanSeeServiceException;

	/**
	 * This method is used to save edited hudciti admin details.
	 * 
	 * @param citi
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String saveAdmin(HubCiti citi, ArrayList<HubCiti> postalCodes, HubCiti removedPostalCodes) throws ScanSeeServiceException;

	/**
	 * this method used get All PostalCodes associated to hubciti.
	 * 
	 * @param sAdminID
	 *            -For which PostalCodes to be fetched.
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public ArrayList<PostalCode> getPostalCode(Integer sAdminID, Integer hubCitiId) throws ScanSeeServiceException;

	/**
	 * this method used get All PostalCodes associated to hubciti.
	 * 
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public ArrayList<HubCiti> fetchAdminAssociatedPostalCode(Integer hubCitiId) throws ScanSeeServiceException;

	/**
	 * This method is used to fetch event categories and also used to search
	 * event categories.
	 * 
	 * @param category
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public AlertCategory fetchEventCategories(EventCategory category,String userId) throws ScanSeeServiceException;

	/**
	 * This method is used to delete event categories.
	 * 
	 * @param cateId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	String deleteEventCategory(int cateId) throws ScanSeeServiceException;

	/**
	 * This method is used to add event categories
	 * 
	 * @param catName
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	String addEventCategory(EventCategory eventCatObj) throws ScanSeeServiceException;

	/**
	 * This method is used to update event categories.
	 * 
	 * @param category
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	String updateEventCategory(EventCategory category, String userId) throws ScanSeeServiceException;
	
	/**
	 * Service method is used to fetch fundraiser categories and also used to search
	 * event categories.
	 * 
	 * @param category
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	AlertCategory fetchFundraiserCategories(EventCategory category, String userId) throws ScanSeeServiceException;
	
	/**
	 * This method is used to delete fundraiser categories.
	 * 
	 * @param cateId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	String deleteFundraiserCategory(int cateId) throws ScanSeeServiceException;
	
	/**
	 * This method is used to add/update fundraiser categories
	 * 
	 * @param catName
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	String addFundraiserCategory(EventCategory eventCatObj) throws ScanSeeServiceException;
	/**
	 * This method is used to display regions
	 * @param hubCiti
	 * @return
	 * @throws ScanSeeServiceException
	 */
	RegionInfo displayRegions(HubCiti hubCiti) throws ScanSeeServiceException;

	/**
	 * This method is used to deactivate region.
	 * @param hubCiti
	 * @return
	 * @throws ScanSeeServiceException
	 */
	String deactivateRegion(HubCiti hubCiti) throws ScanSeeServiceException;
	
	/**
	 * This method is used to fetch HubCities exist in system.
	 * @return
	 * @throws ScanSeeServiceException
	 */
	ArrayList<HubCiti> fetchHubCitis() throws ScanSeeServiceException;
	
	/**
	 * Below method is used to fetch list of hubcities and regions for nationwide deals
	 */

	RegionInfo getHubCitiRegions(HubCiti hubCiti)throws ScanSeeServiceException;
	
	String saveNationWideDeals(HubCiti hubCiti)throws ScanSeeServiceException;
	
	/**
	 * Below method is used to fetch default postal codes.
	 * @return
	 * @throws ScanSeeServiceException
	 */
	ArrayList<HubCiti> getDefaultPostalCodes()throws ScanSeeServiceException;
	
	ArrayList<APIPartner> fetchNWAPIPartners() throws ScanSeeServiceException;
}

/**
 * 
 */
package admin.service;

import common.exception.ScanSeeServiceException;
import common.pojo.Promotion;
import common.pojo.PromotionDetails;

/**
 * @author sangeetha.ts
 *
 */
public interface PromotionService
{

	/**
	 * This method is used to search the Promotion details based on the search key.
	 * This method is also used to sort the columns. This method is also used to
	 * display the expired Promotions based on flag.
	 * @param searchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @param showExpire
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public PromotionDetails getPromotionList(String searchKey, String sortOrder, String columnName, Integer lowerLimit, String showExpire)
	throws ScanSeeServiceException;
	
	/**
	 * This method is used to display promotion details.
	 * @param promotionId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Promotion displayPromotionDetails(int promotionId) throws ScanSeeServiceException;
	
	/**
	 * This method is used to display users associated with the promotion. 
	 * @param promotionId
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public PromotionDetails displayPromotionUsers(int promotionId, int lowerLimit) throws ScanSeeServiceException;
	/**
	 * This method is used to filter the promotions from the list of promotions.
	 * 
	 * @param promotionIds
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String filterPromotions(String promotionIds) throws ScanSeeServiceException;

}

/**
 * 
 */
package admin.service;

import java.text.ParseException;
import java.util.ArrayList;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import admin.dao.VersionDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.EmailComponent;
import common.pojo.RetailerDetails;
import common.pojo.Version;
import common.util.Utility;

/**
 * @author Kumar
 */
public class VersionServiceImpl implements VersionService
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(VersionServiceImpl.class);

	/**
	 * Variable versionDao declared as instance of VersionDao.
	 */
	private VersionDao versionDao;

	/**
	 * @param versionDao
	 *            the versionDao to set.
	 */
	public void setVersionDao(VersionDao versionDao)
	{
		this.versionDao = versionDao;
	}

	public String addVersionDetails(Version objVersion) throws ScanSeeServiceException, MessagingException
	{
		String strMethodName = "addVersionDetails sdf";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strResponse = ApplicationConstants.FAILURETEXT;
		Boolean versionExist = null;
		String strWrittenPath = null;
		String strFilePathForDB = null;
		String strFilePathForMail = null;

		// String strEmailContent = null;
		// String strFtpFilePath = null;
		String strMailStatus = null;

		try
		{
			versionExist = versionDao.checkVersion(objVersion);
			if (null != versionExist)
			{
				if (versionExist)
				{
					strResponse = ApplicationConstants.VERSIONEXIST;
				}
				else
				{

					CommonsMultipartFile file = objVersion.getReleaseNotePath();
					if (objVersion.getReleaseNotePath() != null && file.getSize() > 0)
					{
						strWrittenPath = Utility.writeFileToFTP(objVersion);
						if (null != strWrittenPath && !"".equals(strWrittenPath))
						{
							final String[] strAppTypeArray = strWrittenPath.split(",");
							strFilePathForDB = strAppTypeArray[0];
							strFilePathForMail = strAppTypeArray[1];
						}

						strResponse = versionDao.addVersionDetails(objVersion, strFilePathForDB);
						if (!"".equals(Utility.checkNull(strResponse)) && ApplicationConstants.SUCCESSTEXT.equals(Utility.checkNull(strResponse)))
						{

							strMailStatus = sendReleaseNoteToBatch(strWrittenPath, objVersion);
							strResponse = strMailStatus;
						}
					}
					else
					{

						strResponse = ApplicationConstants.EMPTYFILETEXT;

					}

				}

			}
		}

		catch (ScanSeeServiceException e)
		{
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return strResponse;
	}

	/**
	 * This serviceImpl method is used to display list of all version.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @return RetailerDetails object.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public RetailerDetails getAllVersions(Version objVersion) throws ScanSeeServiceException
	{
		LOG.info("Inside VersionServiceImpl : getAllVersions");
		RetailerDetails retailerDetails = null;
		try
		{
			retailerDetails = versionDao.getAllVersions(objVersion);
			if (null != retailerDetails)
			{
				for (Version version : retailerDetails.getVersionList())
				{
					if (!"".equals(Utility.checkNull(version.getFirstQARelDate())))
					{
						version.setFirstQARelDate(Utility.convertDBdate(version.getFirstQARelDate()));
					}
					if (!"".equals(Utility.checkNull(version.getLastQARelDate())))
					{
						version.setLastQARelDate(Utility.convertDBdate(version.getLastQARelDate()));
					}
					if (!"".equals(Utility.checkNull(version.getFirstProdtnRelDate())))
					{
						version.setFirstProdtnRelDate(Utility.convertDBdate(version.getFirstProdtnRelDate()));
					}
					if (!"".equals(Utility.checkNull(version.getLastProdtnRelDate())))
					{
						version.setLastProdtnRelDate(Utility.convertDBdate(version.getLastProdtnRelDate()));
					}
					if (!"".equals(Utility.checkNull(version.getiTunesUploadDate())))
					{
						version.setiTunesUploadDate(Utility.convertDBdate(version.getiTunesUploadDate()));
					}
					if (!"".equals(Utility.checkNull(version.getBuildApprovalDate())))
					{
						version.setBuildApprovalDate(Utility.convertDBdate(version.getBuildApprovalDate()));
					}
				}
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}
		return retailerDetails;
	}

	/**
	 * This serviceImpl method is used to edit the version details based on
	 * versioID as input parameter.
	 * 
	 * @param iVersionId
	 *            as input parameter.
	 * @return Version object.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public Version getVersionDetailsByID(Integer iVersionId, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside VersionServiceImpl : getVersionDetailsByID");
		Version objVersion = null;
		try
		{
			objVersion = versionDao.getVersionDetailsByID(iVersionId);
			if (null != objVersion)
			{
				if (!"".equals(Utility.checkNull(objVersion.getFirstQARelDate())))
				{
					session.setAttribute("FirstQARelDate", objVersion.getFirstQARelDate());
					objVersion.setFirstQARelDate(Utility.convertDBdateHMS(objVersion.getFirstQARelDate()));
					objVersion.setReleaseDate(objVersion.getFirstQARelDate());
				}
				if (!"".equals(Utility.checkNull(objVersion.getLastQARelDate())))
				{
					session.setAttribute("LastQARelDate", objVersion.getLastQARelDate());
					objVersion.setLastQARelDate(Utility.convertDBdateHMS(objVersion.getLastQARelDate()));
					objVersion.setReleaseDate(objVersion.getLastQARelDate());
				}
				if (!"".equals(Utility.checkNull(objVersion.getFirstProdtnRelDate())))
				{
					session.setAttribute("FirstProdtnRelDate", objVersion.getFirstProdtnRelDate());
					objVersion.setFirstProdtnRelDate(Utility.convertDBdateHMS(objVersion.getFirstProdtnRelDate()));
					objVersion.setReleaseDate(objVersion.getFirstProdtnRelDate());
				}
				if (!"".equals(Utility.checkNull(objVersion.getLastProdtnRelDate())))
				{
					session.setAttribute("LastProdtnRelDate", objVersion.getLastProdtnRelDate());
					objVersion.setLastProdtnRelDate(Utility.convertDBdateHMS(objVersion.getLastProdtnRelDate()));
					objVersion.setReleaseDate(objVersion.getLastProdtnRelDate());
				}
				if (!"".equals(Utility.checkNull(objVersion.getiTunesUploadDate())))
				{
					objVersion.setiTunesUploadDate(Utility.convertDBdateHMS(objVersion.getiTunesUploadDate()));
				}
				if (!"".equals(Utility.checkNull(objVersion.getBuildApprovalDate())))
				{
					objVersion.setBuildApprovalDate(Utility.convertDBdateHMS(objVersion.getBuildApprovalDate()));
					objVersion.setReleaseDate(objVersion.getBuildApprovalDate());
				}
				objVersion.setHiddenRelDate(objVersion.getReleaseDate());
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}
		return objVersion;
	}

	/**
	 * This serviceImpl method is used to Update the version details.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @throws MessagingException
	 */

	public String updateVersion(Version objVersion, HttpSession session) throws ScanSeeServiceException, MessagingException, ParseException
	{
		LOG.info("Inside VersionServiceImpl :updateVersion");
		String strResponse = null;
		String strWrittenPath = null;
		String strMailStatus = null;
		String strFilePathForDB = null;
		// String strFilePathForMail = null;
		// HttpSession session;
		try
		{
			if (!objVersion.getReleaseType().equals(ApplicationConstants.VERSIONITUNESRELEASETEXT))
			{
				CommonsMultipartFile file = objVersion.getReleaseNotePath();
				if (objVersion.getReleaseNotePath() != null && file.getSize() > 0)
				{
					strWrittenPath = Utility.writeFileToFTP(objVersion);
				}
			}

			if (null != strWrittenPath && !"".equals(strWrittenPath))
			{
				final String[] strAppTypeArray = strWrittenPath.split(",");
				strFilePathForDB = strAppTypeArray[0];
				// strFilePathForMail = strAppTypeArray[1];
			}
			strResponse = versionDao.updateVersion(objVersion, strFilePathForDB, session);

			if (!"".equals(Utility.checkNull(strResponse)) && ApplicationConstants.SUCCESSTEXT.equals(Utility.checkNull(strResponse)))
			{
				strMailStatus = sendReleaseNoteToBatch(strWrittenPath, objVersion);
				strResponse = strMailStatus;
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}
		catch (ArrayIndexOutOfBoundsException exception)
		{
			throw new ScanSeeServiceException(exception);
		}
		return strResponse;
	}

	@SuppressWarnings("static-access")
	public String sendReleaseNoteToBatch(String relNotes, Version objVersion) throws ScanSeeServiceException, MessagingException
	{
		LOG.info("Inside VersionServiceImpl : sendReleaseNoteToBatch");
		ArrayList<AppConfiguration> appConfigList = null;
		EmailComponent objEmailComponent = new EmailComponent();
		String[] strToMail = null;
		// String strFromMail = null;
		String response = null;
		String strEmailContent = null;
		// String fileAttachment = null;
		// fileAttachment = relNotes;
		String smtpHost = null;
		String smtpPort = null;
		String strFilePathForBody = null;
		String strFilePathForAttachment = null;

		ArrayList<AppConfiguration> emailConf = null;
		if (null != relNotes && !"".equals(relNotes))
		{
			final String[] strAppTypeArray = relNotes.split(",");
			strFilePathForBody = strAppTypeArray[0];
			strFilePathForAttachment = strAppTypeArray[1];
		}

		strEmailContent = Utility.emailBodyContent(strFilePathForBody, objVersion);
		String strSubject = null;
		if (objVersion.getAppType().equals(ApplicationConstants.VERSIONAPPAPPLICATIONTYPE))
		{
			strSubject = "ScanSee App Release to " + objVersion.getReleaseType();

		}
		else if (objVersion.getAppType().equals(ApplicationConstants.VERSIONWEBAPPLICATIONTYPE))
		{
			strSubject = "ScanSee Web Release to " + objVersion.getReleaseType();
		}
		else
		{
			strSubject = "App on iTunes";
		}

		try
		{
			appConfigList = versionDao.getAppConfig(ApplicationConstants.EMAIL);

			emailConf = versionDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

			for (int j = 0; j < emailConf.size(); j++)
			{
				if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
				{
					smtpHost = emailConf.get(j).getScreenContent();
				}
				if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
				{
					smtpPort = emailConf.get(j).getScreenContent();
				}
			}

			if (appConfigList != null && !appConfigList.isEmpty())
			{
				for (int j = 0; j < appConfigList.size(); j++)
				{
					if (appConfigList.get(j).getScreenName().equals(ApplicationConstants.RELEASE_NOTE_FROM))
					{
						// strFromMail =
						// appConfigList.get(j).getScreenContent();
					}
					if (appConfigList.get(j).getScreenName().equals(ApplicationConstants.RELEASE_NOTE_RECEPIENTS))
					{
						strToMail = appConfigList.get(j).getScreenContent().split(",");
					}
				}
				objEmailComponent.sendMailWithAttachment(strToMail, ApplicationConstants.FROMMAILID, strSubject, strEmailContent,
						strFilePathForAttachment, smtpHost, smtpPort);
				response = ApplicationConstants.SUCCESSTEXT;
			}
			else
			{
				response = "Email recipients are not available. Please Configure";
			}
		}
		catch (MailException e)
		{
			LOG.error("Inside VersionServiceImpl : sendReleaseNoteToBatch : Exception : " + e);
			throw new ScanSeeServiceException(e);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}
		catch (ArrayIndexOutOfBoundsException exception)
		{
			throw new ScanSeeServiceException(exception);
		}
		return response;
	}
}

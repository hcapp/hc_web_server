/**
 * 
 */
package admin.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import admin.dao.HotDealDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Category;
import common.pojo.HotDeal;
import common.pojo.HotDealDetails;
import common.pojo.RetailerLocation;
import common.util.Utility;

/**
 * @author sangeetha.ts
 */
public class HotDealServiceImpl implements HotDealService
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(HotDealServiceImpl.class);

	/**
	 * Variable hotDealDAO declared as instance of hotDealDAO.
	 */
	private HotDealDao hotDealDAO;

	/**
	 * To set setHotDealDAO.
	 * 
	 * @param setHotDealDAO
	 *            to set.
	 */
	public final void setHotDealDAO(HotDealDao hotDealDAO)
	{
		this.hotDealDAO = hotDealDAO;
	}

	/**
	 * This method returns list of hot deals for the given search key.
	 * 
	 * @param deal
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public HotDealDetails displayHotDeals(HotDeal deal) throws ScanSeeServiceException
	{
		final String methodName = "displayHotDeals";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HotDealDetails hotDealDetails = null;
		try
		{
			hotDealDetails = new HotDealDetails();
			hotDealDetails = hotDealDAO.displayHotDeals(deal);

			if (null != hotDealDetails)
			{
				for (HotDeal hotDeal : hotDealDetails.getHotDeals())
				{
					if (null != hotDeal.getHotDealStartDate())
					{
						hotDeal.setHotDealStartDate(Utility.convertDBdate(hotDeal.getHotDealStartDate()));
					}
					if (null != hotDeal.getHotDealEndDate())
					{
						hotDeal.setHotDealEndDate(Utility.convertDBdate(hotDeal.getHotDealEndDate()));
					}
					if (null != hotDeal.getHotDealExpirationDate())
					{
						hotDeal.setHotDealExpirationDate(Utility.convertDBdate(hotDeal.getHotDealExpirationDate()));
					}
				}
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDealDetails;
	}

	/**
	 * This method is used to delete the hotDeals.
	 * 
	 * @param hotDealIds
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deleteHotDeal(String hotDealIds) throws ScanSeeServiceException
	{
		final String methodName = "deleteHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try
		{
			status = hotDealDAO.deleteHotDeal(hotDealIds);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	public HotDeal displayHotDealDetails(int hotdealId) throws ScanSeeServiceException
	{
		final String methodName = "displayHotDealDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HotDeal hotDeal = null;

		try
		{

			hotDeal = new HotDeal();
			hotDeal = hotDealDAO.displayHotDealDetails(hotdealId);

			if (null != hotDeal.getHotDealStartDate())
			{
				hotDeal.setHotDealStartDate(Utility.convertDBdate(hotDeal.getHotDealStartDate()));
			}
			if (null != hotDeal.getHotDealEndDate())
			{
				hotDeal.setHotDealEndDate(Utility.convertDBdate(hotDeal.getHotDealEndDate()));
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDeal;
	}

	public ArrayList<Category> displayHotDealCategories() throws ScanSeeServiceException
	{
		final String methodName = "displayHotDealCategories";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<Category> categories = null;

		try
		{
			categories = new ArrayList<Category>();
			categories = hotDealDAO.displayHotDealCategories();
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return categories;
	}

	public ArrayList<RetailerLocation> displayRetailerLocations(Integer retailId, String city, String state) throws ScanSeeServiceException
	{
		final String methodName = "displayRetailerLocations";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<RetailerLocation> locations = null;

		try
		{
			locations = new ArrayList<RetailerLocation>();
			locations = hotDealDAO.displayRetailerLocations(retailId, city, state);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return locations;
	}

	public String updateHotDeal(HotDeal hotDeal) throws ScanSeeServiceException
	{
		final String methodName = "updateHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try
		{
			status = hotDealDAO.updateHotDeal(hotDeal);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}
}

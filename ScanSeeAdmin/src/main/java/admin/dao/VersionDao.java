/**
 * 
 */
package admin.dao;

import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.RetailerDetails;
import common.pojo.Version;

/**
 * @author Kumar
 */
public interface VersionDao
{

	Boolean checkVersion(Version objVersion) throws ScanSeeServiceException;

	String addVersionDetails(Version objVersion, String strWrittenFilePath) throws ScanSeeServiceException;

	/**
	 * This DAO method is used to display list of all version.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @return RetailerDetails object.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	RetailerDetails getAllVersions(Version objVersion) throws ScanSeeWebSqlException;

	/**
	 * This serviceImpl method is used to edit the version details based on
	 * versioID as input parameter.
	 * 
	 * @param iVersionId
	 *            as input parameter.
	 * @return Version object.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	Version getVersionDetailsByID(Integer iVersionId) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to Update the version details.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception
	 */
	String updateVersion(Version objVersion, String strWrittenPath, HttpSession session) throws ScanSeeWebSqlException, ParseException;

	/**
	 * This DAO method is used to get configuration details details.
	 * 
	 * @param strConfigType
	 *            as input parameter.
	 * @return configuration details list.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception
	 */
	ArrayList<AppConfiguration> getAppConfig(String strConfigType) throws ScanSeeWebSqlException;

}

/**
 * 
 */
package admin.dao;

import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Promotion;
import common.pojo.PromotionDetails;

/**
 * @author sangeetha.ts
 */
public class PromotionDaoImpl implements PromotionDao {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(PromotionDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This method is used to search the Promotion details based on the search
	 * key. This method is also used to sort the columns. This method is also
	 * used to display the expired Promotions based on flag.
	 * 
	 * @param searchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @param showExpire
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public PromotionDetails getPromotionList(String searchKey, String sortOrder, String columnName, Integer lowerLimit, Integer showExpire)
			throws ScanSeeWebSqlException {
		final String methodName = "getPromotionList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		PromotionDetails promotionDetails = null;
		ArrayList<Promotion> promotionDetailList = null;
		Integer rowcount = null;
		Boolean nextpage = null;
		Integer page = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminGiveawaypageDisplay");
			simpleJdbcCall.returningResultSet("promotionList", new BeanPropertyRowMapper<Promotion>(Promotion.class));
			final MapSqlParameterSource promotionParams = new MapSqlParameterSource();
			promotionParams.addValue("SearchParameter", searchKey);
			promotionParams.addValue("ColumnName", columnName);
			promotionParams.addValue("SortOrder", sortOrder);
			promotionParams.addValue("LowerLimit", lowerLimit);
			promotionParams.addValue("ShowExpired", showExpire);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(promotionParams);

			if (null != resultFromProcedure) {
				promotionDetailList = new ArrayList<Promotion>();
				promotionDetailList = (ArrayList<Promotion>) resultFromProcedure.get("promotionList");
				promotionDetails = new PromotionDetails();
				nextpage = (Boolean) resultFromProcedure.get("NextPageFlag");

				if (nextpage == true) {
					page = 1;
				} else {
					page = 0;
				}

				promotionDetails.setNextPage(page);
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				promotionDetails.setTotalSize(rowcount);
				promotionDetails.setPromotions(promotionDetailList);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return promotionDetails;
	}

	/**
	 * This method is used to display promotion details.
	 * 
	 * @param promotionId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Promotion displayPromotionDetails(int promotionId) throws ScanSeeWebSqlException {
		final String methodName = "displayPromotionDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<Promotion> promotionDetailList = null;
		Promotion promotion = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminGiveawaypageDetails");
			simpleJdbcCall.returningResultSet("promotionDetails", new BeanPropertyRowMapper<Promotion>(Promotion.class));
			final MapSqlParameterSource promotionParams = new MapSqlParameterSource();
			promotionParams.addValue("QRRetailerCustomPageID", promotionId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(promotionParams);

			if (null != resultFromProcedure) {
				promotionDetailList = new ArrayList<Promotion>();
				promotionDetailList = (ArrayList<Promotion>) resultFromProcedure.get("promotionDetails");
				promotion = new Promotion();
				promotion = promotionDetailList.get(0);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return promotion;
	}

	/**
	 * This method is used to display users associated with the promotion.
	 * 
	 * @param promotionId
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public PromotionDetails displayPromotionUsers(int promotionId, int lowerLimit) throws ScanSeeWebSqlException {
		final String methodName = "displayPromotionUsers";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		PromotionDetails promotionDetails = null;
		ArrayList<Promotion> promotionDetailList = null;
		Integer rowcount = null;
		Boolean nextpage = null;
		Integer page = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminGiveawayUserDetails");
			simpleJdbcCall.returningResultSet("promotionUsers", new BeanPropertyRowMapper<Promotion>(Promotion.class));
			final MapSqlParameterSource promotionParams = new MapSqlParameterSource();
			promotionParams.addValue("QRRetailerCustomPageID", promotionId);
			promotionParams.addValue("LowerLimit", lowerLimit);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(promotionParams);

			if (null != resultFromProcedure) {
				promotionDetailList = new ArrayList<Promotion>();
				promotionDetailList = (ArrayList<Promotion>) resultFromProcedure.get("promotionUsers");
				promotionDetails = new PromotionDetails();
				nextpage = (Boolean) resultFromProcedure.get("NextPageFlag");

				if (nextpage == true) {
					page = 1;
				} else {
					page = 0;
				}

				promotionDetails.setNextPage(page);
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				promotionDetails.setTotalSize(rowcount);
				promotionDetails.setPromotions(promotionDetailList);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return promotionDetails;
	}

	/**
	 * This method is used to filter the promotions from the list of promotions.
	 * 
	 * @param promotionIds
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String filterPromotions(String promotionIds) throws ScanSeeWebSqlException {
		final String methodName = "filterPromotions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Integer status = null;
		String response = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminGiveawayWinnerUpdation");
			final MapSqlParameterSource promotionParams = new MapSqlParameterSource();
			promotionParams.addValue("QRRetailerCustomPageID", promotionIds);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(promotionParams);

			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				response = "Success";
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}

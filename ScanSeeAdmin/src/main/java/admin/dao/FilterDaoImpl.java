/**
* @author Kumar D
* @version 0.1
*
* Class to store DAO details of a FilterDaoImpl.
*/

package admin.dao;

import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.validation.ValidationUtils;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Category;
import common.pojo.Filter;
import common.pojo.FilterInfo;
import common.pojo.FilterValues;
import common.util.Utility;

@SuppressWarnings({ "rawtypes", "unchecked"})
public class FilterDaoImpl implements FilterDao {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(FilterDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate.
	 * 
	 * @param dataSource from DataSource
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


	
	/**
	 * The DAOImpl method for displaying all the filter categories and it
	 * return to service method.
	 * 
	 * @param iUserId as input parameter.
	 * @return arCategoryList,List of categories.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	public final ArrayList<Category> getAllFilterCategory(Integer iUserId) throws ScanSeeWebSqlException {
		LOG.info("Inside FilterDaoImpl : getAllFilterCategory ");
		
		Map<String, Object> resultFromProcedure = null;
		ArrayList<Category> arCategoryList = null;
		
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			
			simpleJdbcCall.withProcedureName("usp_WebAdminFilterBusinessCategoryDisplay");
			simpleJdbcCall.returningResultSet("filterCategoryList", new BeanPropertyRowMapper<Category>(Category.class));
			
			final MapSqlParameterSource objCategoryParam = new MapSqlParameterSource();
			objCategoryParam.addValue("UserID", iUserId);
				resultFromProcedure = simpleJdbcCall.execute(objCategoryParam);
			arCategoryList = (ArrayList) resultFromProcedure.get("filterCategoryList");


			
		} catch (DataAccessException e) {
			LOG.error("Inside FilterDaoImpl : getAllFilterCategory ", e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		
		LOG.info("Exit FilterDaoImpl : getAllFilterCategory ");
		return arCategoryList;
	}
	
	/**
	 * The DAOImpl method for displaying all the filters.
	 * 
	 * @param filter instance of Filter.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 * @return Filters,List of filters.
	 */
	public FilterInfo getAllFilters(Filter filter) throws ScanSeeWebSqlException {
		LOG.info("Inside FilterDaoImpl : getAllFilters ");
		
		ArrayList<Filter> arFilterList = null;
		FilterInfo filterInfo = null;
		Boolean bNextPageFlag = false;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			
			simpleJdbcCall.withProcedureName("usp_WebAdminFilterList");
			simpleJdbcCall.returningResultSet("filterList",new BeanPropertyRowMapper<Filter>(Filter.class));
			
			final MapSqlParameterSource filterDisplayParams = new MapSqlParameterSource();
				filterDisplayParams.addValue("UserID",filter.getUserId());
				filterDisplayParams.addValue("FilterName",filter.getSearchKey());
				filterDisplayParams.addValue("LowerLimit", filter.getLowerLimit());
				filterDisplayParams.addValue("RecordCount", filter.getRecordCount());
			
			final Map<String, Object> result = simpleJdbcCall.execute(filterDisplayParams);
			
			if (null != result) {
				String errorMsg = (String) result.get("ErrorMessage");
				final Integer iMaxCount = (Integer) result.get("MaxCnt");
				if (null == errorMsg) {
					filterInfo = new FilterInfo();
					
					filterInfo.setTotalSize(iMaxCount);
					filterInfo.setNextPage(Boolean.valueOf(bNextPageFlag).compareTo(false));
					
					arFilterList = (ArrayList<Filter>) result.get("filterList");
					
					if (null != arFilterList && !arFilterList.isEmpty()) {
						filterInfo.setFilterList(arFilterList);
					}
					
				} else {
					final Integer errorNum = (Integer) result.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) result.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside FilterDaoImpl : getAllFilters : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside FilterDaoImpl : getAllFilters : " + e);
			throw new ScanSeeWebSqlException(e);

		}

		LOG.info("Exit FilterDaoImpl : getAllFilters ");
		return filterInfo;
	}
	
	
	/**
	 * This DAO method is used to Delete the Filter.
	 * 
	 * @param iFilter as input parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteFilter(Integer iFilter) throws ScanSeeWebSqlException {
	LOG.info("Inside FilterDaoImpl : deleteFilter");

	Map<String, Object> resFromProc = null;
	String strResponse = null;
	String strErrorMsg = null;
	try {
		simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
		
		simpleJdbcCall.withProcedureName("usp_WebAdminFilterDeletion");
		
		final MapSqlParameterSource delFilterParams = new MapSqlParameterSource();
			delFilterParams.addValue("FilterID", iFilter);

		resFromProc = simpleJdbcCall.execute(delFilterParams);
		strErrorMsg = (String) resFromProc.get("ErrorMessage");
		final Boolean bDelFilter = (Boolean) resFromProc.get("RetailerAssociatedFlag");
		
		if (null == strErrorMsg) { 
			if (bDelFilter) {
				strResponse = ApplicationConstants.FILTERASSCOIATED;
			} else {
				strResponse = ApplicationConstants.SUCCESS;
			}
			
		} else {
			strResponse = ApplicationConstants.FAILURETEXT;
		}
	} catch (DataAccessException e) {
		LOG.error("Inside FilterDaoImpl : deleteFilter : " + e);
		throw new ScanSeeWebSqlException(e);
	}

	LOG.info("Exit FilterDaoImpl : deleteFilter ");
	return strResponse;
}
	/**
	 * This DAOImpl method is used to Add/Update the Filter details.
	 * 
	 * @param filter instance of Filter.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	public String addUpdateFilter(Filter filter) throws ScanSeeWebSqlException {
		LOG.info("Inside FilterDaoImpl : addUpdateFilter ");
		
		String strResponse = null;
		Integer responseFromProc = null;
		
		if ("".equals(Utility.checkNull(filter.getHiddenFValue()))) {
			filter.setHiddenFValue(null);
		}
		
		if ("yes".equals(filter.getFilterOption())) {
			filter.setfValueFlag(1);                                            
		} else {
			filter.setfValueFlag(0); 
		}
		
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			
			simpleJdbcCall.withProcedureName("usp_WebAdminFilterCreationAndUpdation");
			final MapSqlParameterSource addFilterParams = new MapSqlParameterSource();
				addFilterParams.addValue("UserID", filter.getUserId());
				addFilterParams.addValue("FilterID", filter.getFilterId());
				addFilterParams.addValue("FilterName", filter.getFilterName());
				addFilterParams.addValue("BusinessCategoryIDs", filter.getfCategory());
				addFilterParams.addValue("FilterValuesIDS", filter.getHiddenFValue());
				addFilterParams.addValue("FilterValueFlag", filter.getfValueFlag());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addFilterParams);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				final Boolean bFilterExists = (Boolean) resultFromProcedure.get("DuplicateFilterName");
				
				if (bFilterExists) {
					strResponse = ApplicationConstants.FILTEREXISTS;
				} else {
					strResponse = ApplicationConstants.SUCCESS;
				}
			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
			}

		} catch (DataAccessException exception) {

			LOG.error("Inside FilterDaoImpl : addUpdateFilter : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		
		LOG.info("Exit FilterDaoImpl : addUpdateFilter ");
		return strResponse;
	}


	/**
	 * This DaoImpl method is used to edit the filter details based on iFilterId
	 * as input parameter.
	 * 
	 * @param iFilterId as input parameter.
	 * @param iUserId as input parameter.
	 * @return Filter object.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	public Filter getFilterDetailsByID(Integer iFilterId, Integer iUserId) throws ScanSeeWebSqlException {
		LOG.info("Inside FilterDaoImpl : getFilterDetailsByID ");
		
		Filter objFilter = null;
		ArrayList<Filter> arFilterList = null;
		
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			
			simpleJdbcCall.withProcedureName("usp_WebAdminFilterDetails");
			simpleJdbcCall.returningResultSet("filterDetail", new BeanPropertyRowMapper<Filter>(Filter.class));
			
			final MapSqlParameterSource editFilterParam = new MapSqlParameterSource();
				editFilterParam.addValue("FilterID", iFilterId);
				editFilterParam.addValue("UserID", iUserId);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(editFilterParam);
			
			if (null != resultFromProcedure) {
				arFilterList = (ArrayList<Filter>) resultFromProcedure.get("filterDetail");
				objFilter = new Filter();
				objFilter = arFilterList.get(0);
				
				if (objFilter.getfValueFlag() == 1) {
					objFilter.setFilterOption("yes");
				} else {
					objFilter.setFilterOption("no");
				}
				
			}
			
		} catch (DataAccessException e) {
			LOG.error("Inside FilterDaoImpl : getFilterDetailsByID : " + e.getMessage());
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		
		LOG.info("Exit FilterDaoImpl : getFilterDetailsByID ");
		return objFilter;
	}	
	
	/**
	 * This DAOImpl method is used to Add/Update the Filter value.
	 * 
	 * @param filterVal instance of FilterValues.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String addUpdateFilterValue(FilterValues filterVal) throws ScanSeeWebSqlException {
		LOG.info("Inside FilterDaoImpl : addUpdateFilterValue");

		Map<String, Object> resFromProc = null;
		String strResponse = null;
		String strErrorMsg = null;
		
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			
			simpleJdbcCall.withProcedureName("usp_WebAdminFilterValueCreationAndUpdation");
			
			final MapSqlParameterSource filterValParams = new MapSqlParameterSource();
				filterValParams.addValue("UserID", filterVal.getUserId());
				filterValParams.addValue("FilterValueID", filterVal.getfValueId());
				filterValParams.addValue("FilterValue", filterVal.getfValueName());

			resFromProc = simpleJdbcCall.execute(filterValParams);
			strErrorMsg = (String) resFromProc.get("ErrorMessage");

			if (null == strErrorMsg) { 
				final Boolean bFilterValExists = (Boolean) resFromProc.get("DuplicateValue");
				
				if (bFilterValExists) {
					strResponse = ApplicationConstants.FILTERVALEXISTS;
				} else {
					
					if (filterVal.getScreenName().equals("addFValue")) {
						final Integer iFValueId = (Integer) resFromProc.get("AdminFilterID");
						strResponse = iFValueId.toString();
					} else {
						strResponse = ApplicationConstants.SUCCESS;
					}
				}
			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
			}
		} catch (DataAccessException e) {
			LOG.error("Inside FilterDaoImpl : addUpdateFilterValue : " + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info("Exit FilterDaoImpl : addUpdateFilterValue ");
		return strResponse;
	}

	/**
	 * This serviceImpl method is used to Delete the Filter value.
	 * 
	 * @param iFilterVal as input parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteFilterValue(Integer iFilterVal) throws ScanSeeWebSqlException {
		LOG.info("Inside FilterDaoImpl : deleteFilterValue");

		Map<String, Object> resultFromProcedure = null;
		String strResponse = null;
		String strErrorMsg = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			
			simpleJdbcCall.withProcedureName("usp_WebAdminFilterValueDeletion");
			
			final MapSqlParameterSource delFilterValParams = new MapSqlParameterSource();
				delFilterValParams.addValue("FilterValueID", iFilterVal);

			resultFromProcedure = simpleJdbcCall.execute(delFilterValParams);
			strErrorMsg = (String) resultFromProcedure.get("ErrorMessage");
			
			if (null == strErrorMsg) {
				strResponse = ApplicationConstants.SUCCESS;
			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
			}
			
		} catch (DataAccessException e) {
			LOG.error("Inside FilterDaoImpl : deleteFilterValue : " + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info("Exit FilterDaoImpl : deleteFilterValue ");
		return strResponse;
	}
	
	/**
	 * The serviceImpl method for displaying all the filter values and it will
	 * calling DAO.
	 * 
	 * @param iUserId as input parameter.
	 * @param iFilterId as input parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return filterValues, List of filterValues.
	 */
	public ArrayList<FilterValues> getAllFilterValues(Integer iUserId, Integer iFilterId) throws ScanSeeWebSqlException {
		LOG.info("Inside FilterDaoImpl : getAllFilterValues ");

		Map<String, Object> resultFromProcedure = null;
		ArrayList<FilterValues> arFilterValList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebAdminFilterValueList");

			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("filterValuesList", new BeanPropertyRowMapper<FilterValues>(FilterValues.class));

			final MapSqlParameterSource allFilterValParam = new MapSqlParameterSource();
				allFilterValParam.addValue("UserID", iUserId);
				allFilterValParam.addValue("FilterID", iFilterId);
			
			resultFromProcedure = simpleJdbcCall.execute(allFilterValParam);
			arFilterValList = (ArrayList) resultFromProcedure.get("filterValuesList");

		} catch (DataAccessException e) {
			LOG.error("Inside FilterDaoImpl : getAllFilterValues ", e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info("Exit FilterDaoImpl : getAllFilterValues ");
		return arFilterValList;
	}
	
}

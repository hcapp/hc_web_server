/**
 * 
 */
package admin.dao;

import java.util.ArrayList;
import java.util.List;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.APIPartner;
import common.pojo.AlertCategory;
import common.pojo.AppConfiguration;
import common.pojo.EventCategory;
import common.pojo.HubCiti;
import common.pojo.HubCitiDetails;
import common.pojo.PostalCode;
import common.pojo.RegionInfo;

/**
 * @author sangeetha.ts
 */
public interface HubCitiDao {
	

	/**
	 * This method returns list of hubciti admins for the given search key.
	 * 
	 * @param userId
	 * @param searchKey
	 * @param lowerLimit
	 * @return HubCitiDetails
	 * @throws ScanSeeWebSqlException
	 */
	public HubCitiDetails displayHubCiti(Integer userId, String searchKey,
			Integer lowerLimit, Boolean showDeactivated)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to deactivate the hudciti admin based on admin id
	 * given.
	 * 
	 * @param adminId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deactivateAdmin(Integer userId, Integer adminId,
			Integer activeFlag) throws ScanSeeWebSqlException;

	/**
	 * this method used get All PostalCodes.
	 * 
	 * @param stateName
	 *            -For which PostalCodes to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeWebSqlException will be
	 *             thrown.
	 */
	public ArrayList<PostalCode> getAllPostalCode(String city, String state)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to create the hudciti admin.
	 * 
	 * @param citi
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String createAdmin(HubCiti citi) throws ScanSeeWebSqlException;

	/**
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to domain Name for Application configuration
	 * 
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<AppConfiguration> getAppConfigForGeneralImages(
			String configType) throws ScanSeeWebSqlException;

	/**
	 * This method is used to get the details of hudciti admin.
	 * 
	 * @param userID
	 * @param hubCitiID
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public HubCiti editAdmin(Integer userID, Integer hubCitiID)
			throws ScanSeeWebSqlException;

	/**
	 * this method used get All PostalCodes associated to hubciti.
	 * 
	 * @param sAdminID
	 *            -For which PostalCodes to be fetched.
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeWebSqlException will be
	 *             thrown.
	 */
	public ArrayList<PostalCode> getPostalCode(Integer sAdminID,
			Integer hubCitiId) throws ScanSeeWebSqlException;

	public String savePostalCodeList(final ArrayList<HubCiti> postalCodeList,
			final Integer HcHubcitiID, final Integer ScanSeeAdminUserID)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to get the postal code associated to the given
	 * hudciti.
	 * 
	 * @param hubCitiID
	 * @return List of associated postal codes
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<HubCiti> fetchAdminAssociatedPostalCode(Integer hubCitiID)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to delete the postal code association for a given
	 * hubciti.
	 * 
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deletePostalCodeAssociation(Integer hubCitiId,
			HubCiti removedPostalCodes) throws ScanSeeWebSqlException;

	/**
	 * This method is used to associate retailer locations to the given hubciti.
	 * 
	 * @param userId
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String AssociateRetailLoctions(Integer userId, Integer hubCitiId)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to display and search event categories.
	 * 
	 * @param category
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public AlertCategory fetchEventCategories(EventCategory category,
			String userId) throws ScanSeeWebSqlException;

	/**
	 * This method is used delete event categories.
	 * 
	 * @param cateId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	String deleteEventCategory(int cateId) throws ScanSeeWebSqlException;

	/**
	 * This method is used to add event categories.
	 * 
	 * @param catName
	 * @param userId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	String addEventCategory(EventCategory eventCatObj)
			throws ScanSeeWebSqlException;

	/**
	 * DAO method is used to update event category name to database.
	 * 
	 * @param category
	 * @param user
	 * @return
	 * @throws HubCitiWebSqlException
	 */

	/*String updateEventCategory(EventCategory category, String userId)
			throws ScanSeeWebSqlException;

	public AlertCategory fetchFundraiserCategories(EventCategory category,
			String userId) throws ScanSeeWebSqlException;*/


	String updateEventCategory(EventCategory category, String userId) throws ScanSeeWebSqlException;
	
	/**
	 * DAO method is used to fetch fundraiser categories and also used to search
	 * event categories.
	 * 
	 * @param category
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public AlertCategory fetchFundraiserCategories(EventCategory category,String userId) throws ScanSeeWebSqlException;
	

	/**
	 * DAO method is used to delete fundraiser categories.
	 * 
	 * @param cateId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	String deleteFundraiserCategory(int cateId) throws ScanSeeWebSqlException;

	/**
	 * DAO method is used to add/update fundraiser categories.
	 * 
	 * @param catName
	 * @param userId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	String addFundraiserCategory(EventCategory eventCatObj)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to display regions.
	 * 
	 * @param hubCiti
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	RegionInfo displayRegions(HubCiti hubCiti) throws ScanSeeWebSqlException;
	
	/**
	 * This method is used to deactivate regions.
	 * @param hubCiti
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	String deactivateRegion(HubCiti hubCiti)throws ScanSeeWebSqlException;
	
	/**
	 * This method is used to fetch hubcities list.
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	ArrayList<HubCiti> fetchHubCities() throws ScanSeeWebSqlException;
	
	RegionInfo getHubCitiRegions(HubCiti hubCiti)throws ScanSeeWebSqlException;
	
	String saveNationWideDeals(HubCiti hubCiti)throws ScanSeeWebSqlException;
	
	String AssociateBottomButtons(Integer userId, Integer hubCitiId) throws ScanSeeWebSqlException;
	
	List<HubCiti> getDefaultPostalCodes() throws ScanSeeWebSqlException;
	
	ArrayList<APIPartner> fetchNWAPIPartners() throws ScanSeeWebSqlException;

}

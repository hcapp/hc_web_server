/**
 * 
 */
package admin.dao;

import java.util.List;

import common.exception.ScanSeeWebSqlException;
import common.pojo.DashboardExportData;
import common.pojo.ExportDetails;
import common.pojo.HubCiti;
import common.pojo.Modules;

/**
 * @author sangeetha.ts
 *
 */
public interface ExportDao {
	
	public List<HubCiti> getExportHubCities() throws ScanSeeWebSqlException;
	
	public List<Modules> getExportType() throws ScanSeeWebSqlException;
	
	public List<Modules> getExportOptions(int exportType) throws ScanSeeWebSqlException;
	
	public ExportDetails exportData(Modules module) throws ScanSeeWebSqlException;
	
	public ExportDetails exportRetailerData(Modules module) throws ScanSeeWebSqlException;
	
	public String saveExportHubCities(HubCiti hubCiti) throws ScanSeeWebSqlException;

}

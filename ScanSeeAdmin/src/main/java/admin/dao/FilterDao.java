/**
* @author Kumar D
* @version 0.1
*
* FilterDao Interface class that has the DAO methods.
*/
package admin.dao;

import java.util.ArrayList;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Category;
import common.pojo.Filter;
import common.pojo.FilterInfo;
import common.pojo.FilterValues;


public interface FilterDao
{
	/**
	 * The DAO method for displaying all the filter categories and it return
	 * to service method.
	 * 
	 * @param iUserId as input parameter.
	 * @return Categories,List of categories.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	ArrayList<Category> getAllFilterCategory(Integer iUserId) throws ScanSeeWebSqlException;


	/**
	 * The DAO method for displaying all the filters.
	 * 
	 * @param filter instance of Filter.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 * @return Filters,List of filters.
	 */
	FilterInfo getAllFilters(Filter filter) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to Delete the Filter.
	 * 
	 * @param iFilter as input parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String deleteFilter(Integer iFilter) throws ScanSeeWebSqlException; 
	
	/**
	 * This DAO method is used to Add/Update the Filter details.
	 * 
	 * @param filter instance of Filter.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	String addUpdateFilter(Filter filter) throws ScanSeeWebSqlException;

	/**
	 * This Dao method is used to edit the filter details based on iFilterId
	 * as input parameter.
	 * 
	 * @param iFilterId as input parameter.
	 * @param iUserId as input parameter.
	 * @return Filter object.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	Filter getFilterDetailsByID(Integer iFilterId, Integer iUserId) throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method is used to Add the Filter value.
	 * 
	 * @param filterVal instance of FilterValues.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String addUpdateFilterValue(FilterValues filterVal) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to Delete the Filter value.
	 * 
	 * @param iFilterVal as input parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String deleteFilterValue(Integer iFilterVal) throws ScanSeeWebSqlException;
	
	/**
	 * The DAO method for displaying all the filter values.
	 * 
	 * @param iUserId as input parameter.
	 * @param iFilterId as input parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return filterValues, List of filterValues.
	 */
	ArrayList<FilterValues> getAllFilterValues(Integer iUserId, Integer iFilterId) throws ScanSeeWebSqlException;

}

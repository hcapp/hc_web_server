package admin.dao;

import common.exception.ScanSeeWebSqlException;
import common.pojo.Login;

public interface LoginDao
{
	/**
	 * This method is used for user login authentication
	 * @param userName
	 * @param password
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Login loginAuthentication(String userName, String password) throws ScanSeeWebSqlException;

}

/**
 * 
 */
package admin.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.RetailerDetails;
import common.pojo.Version;
import common.util.Utility;

/**
 * @author Kumar
 */
public class VersionDaoImpl implements VersionDao {
	/**
	 * Getting the LOGger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(VersionDaoImpl.class);

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate.
	 * 
	 * @param dataSource
	 *            from DataSource.
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public Boolean checkVersion(Version objVersion) throws ScanSeeServiceException {
		final String methodName = "checkVersion ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Boolean versionExist = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminAPPVersionDuplicateCheck");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("VersionNumber", objVersion.getAppVersionNo());
			scanQueryParams.addValue("AppType", objVersion.getAppType());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {

					versionExist = (Boolean) resultFromProcedure.get("VersionExist");

				}

			} else {
				@SuppressWarnings("null")
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_WebAdminAPPVersionDuplicateCheck Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeServiceException(errorMsg);
			}

		} catch (DataAccessException exception) {
			LOG.error("Exception occurred in checkVersion", exception);
			throw new ScanSeeServiceException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return versionExist;
	}

	public String addVersionDetails(Version objVersion, String strWrittenFilePath) throws ScanSeeServiceException {
		final String methodName = "addVersionDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer intResponse = null;
		String strResponse = ApplicationConstants.FAILURETEXT;
		try {

			if (objVersion.getReleaseType().equals(ApplicationConstants.VERSIONQA)) {
				objVersion.setRelQANotePath(strWrittenFilePath);
			} else {
				objVersion.setiTunesUploadDate(objVersion.getReleaseDate());
			}

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminAPPVersionCreation");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("VersionNumber", objVersion.getAppVersionNo());
			scanQueryParams.addValue("DatabaseSchemaName", objVersion.getSchemaName());
			scanQueryParams.addValue("ServerBuild", objVersion.getServerBuildNo());
			scanQueryParams.addValue("QAFirstReleaseDate", Utility.getFormattedDate());
			scanQueryParams.addValue("QALastReleaseDate", null);
			scanQueryParams.addValue("QAReleaseNotePath", objVersion.getRelQANotePath());
			scanQueryParams.addValue("ProductionFirstReleaseDate", null);
			scanQueryParams.addValue("ProductionLastReleaseDate", null);
			scanQueryParams.addValue("ProductionReleaseNotePath", null);
			scanQueryParams.addValue("ITunesUploadDate", null);
			scanQueryParams.addValue("BuildApprovalDate", null);
			scanQueryParams.addValue("AppType", objVersion.getAppType());
			scanQueryParams.addValue("QAReleaseNote", objVersion.getReleaseNotePath().getOriginalFilename());
			scanQueryParams.addValue("ProductionReleaseNote", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					intResponse = (Integer) resultFromProcedure.get("Status");
					if (null != intResponse) {
						if (intResponse == 0) {
							strResponse = ApplicationConstants.SUCCESSTEXT;
						} else {
							final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
							final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
							LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
							throw new ScanSeeServiceException(errorMsg);
						}
					}
				}
			} else {
				@SuppressWarnings("null")
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_WebAdminAPPVersionCreation Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeServiceException(errorMsg);
			}

		} catch (DataAccessException exception) {
			LOG.error("Exception occurred in addVersionDetails", exception);
			throw new ScanSeeServiceException(exception);
		} catch (ParseException exception) {
			LOG.error("Exception occurred in addVersionDetails", exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This DAO method is used to display list of all version.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @return RetailerDetails object.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	public RetailerDetails getAllVersions(Version objVersion) throws ScanSeeWebSqlException {
		LOG.info("Inside VersionDaoImpl : getAllVersions");
		RetailerDetails objRetailerDetails = null;
		ArrayList<Version> arVersionList = null;
		Integer rowcount = null;
		boolean bNextPageFlag = false;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminAPPVersionDisplay");
			simpleJdbcCall.returningResultSet("versionList", new BeanPropertyRowMapper<Version>(Version.class));
			final MapSqlParameterSource objVersionParam = new MapSqlParameterSource();
			if ("".equals(Utility.checkNull(objVersion.getSearchKey()))) {
				objVersion.setSearchKey(null);
			}
			objVersionParam.addValue("VersionNumber", objVersion.getSearchKey());
			objVersionParam.addValue("LowerLimit", objVersion.getLowerLimit());
			objVersionParam.addValue("RecordCount", objVersion.getRecordCount());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objVersionParam);
			if (null != resultFromProcedure) {
				arVersionList = (ArrayList<Version>) resultFromProcedure.get("versionList");
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				bNextPageFlag = (Boolean) resultFromProcedure.get("NxtPageFlag");
				objRetailerDetails = new RetailerDetails();
				if (null != rowcount) {
					objRetailerDetails.setTotalSize(rowcount);
					objRetailerDetails.setNextPage(Boolean.valueOf(bNextPageFlag).compareTo(false));
				}
				objRetailerDetails.setVersionList(arVersionList);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside VersionDaoImpl : getAllVersions : " + e.getMessage());
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		return objRetailerDetails;
	}

	/**
	 * This DaoImpl method is used to edit the version details based on versioID
	 * as input parameter.
	 * 
	 * @param iVersionId
	 *            as input parameter.
	 * @return Version object.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception.
	 */
	public Version getVersionDetailsByID(Integer iVersionId) throws ScanSeeWebSqlException {
		LOG.info("Inside VersionDaoImpl : getVersionDetailsByID ");
		Version objVersion = null;
		ArrayList<Version> arVersionList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminAPPVersionDetails");
			simpleJdbcCall.returningResultSet("versionDetails", new BeanPropertyRowMapper<Version>(Version.class));
			final MapSqlParameterSource versionParam = new MapSqlParameterSource();
			versionParam.addValue("VersionID", iVersionId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(versionParam);
			if (null != resultFromProcedure) {
				arVersionList = (ArrayList<Version>) resultFromProcedure.get("versionDetails");
				objVersion = new Version();
				objVersion = arVersionList.get(0);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside VersionDaoImpl : getVersionDetailsByID : " + e.getMessage());
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		return objVersion;
	}

	/**
	 * This DAO method is used to Update the version details.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @throws ParseException
	 */

	public final String updateVersion(Version objVersion, String strWrittenPath, HttpSession session) throws ScanSeeWebSqlException, ParseException {
		LOG.info("Inside VersionDaoImpl : updateVersion ");
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer status;
		String strDateTimeStamp = null;
		strDateTimeStamp = Utility.getFormattedDate();

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebAdminAPPVersionUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objUpVersionParam = new MapSqlParameterSource();

			objUpVersionParam.addValue("VersionID", objVersion.getVersionId());
			objUpVersionParam.addValue("VersionNumber", objVersion.getAppVersionNo());
			objUpVersionParam.addValue("AppType", objVersion.getAppType());
			objUpVersionParam.addValue("DatabaseSchemaName", objVersion.getSchemaName());
			objUpVersionParam.addValue("ServerBuild", objVersion.getServerBuildNo());
			if (!"".equals(Utility.checkNull(objVersion.getQAFileName()))) {
				objVersion.setQAFileName(objVersion.getQAFileName());
			} else {
				objVersion.setQAFileName(null);
			}
			if (!"".equals(Utility.checkNull(objVersion.getProdFileName()))) {
				objVersion.setProdFileName(objVersion.getProdFileName());
			} else {
				objVersion.setProdFileName(null);
			}

			objVersion.setFirstQARelDate(Utility.getUsDateFormat((String) session.getAttribute("FirstQARelDate")));
			if (!"".equals(Utility.checkNull((String) session.getAttribute("LastQARelDate")))) {
				objVersion.setLastQARelDate(Utility.getUsDateFormat((String) session.getAttribute("LastQARelDate")));
			} else {
				objVersion.setLastQARelDate(null);
			}
			if (!"".equals(Utility.checkNull((String) session.getAttribute("FirstProdtnRelDate")))) {
				objVersion.setFirstProdtnRelDate(Utility.getUsDateFormat((String) session.getAttribute("FirstProdtnRelDate")));
			} else {
				objVersion.setFirstProdtnRelDate(null);
			}
			if (!"".equals(Utility.checkNull((String) session.getAttribute("LastProdtnRelDate")))) {
				objVersion.setLastProdtnRelDate(Utility.getUsDateFormat((String) session.getAttribute("LastProdtnRelDate")));
			} else {
				objVersion.setLastProdtnRelDate(null);
			}
			objVersion.setiTunesUploadDate(null);
			objVersion.setBuildApprovalDate(null);
			if ("QA".equals(Utility.checkNull(objVersion.getReleaseType()))) {
				objVersion.setLastQARelDate(strDateTimeStamp);
				objVersion.setRelQANotePath(strWrittenPath);
				objUpVersionParam.addValue("QAReleaseNote", objVersion.getReleaseNotePath().getOriginalFilename());
				objUpVersionParam.addValue("ProductionReleaseNote", objVersion.getProdFileName());
			}
			if ("Production".equals(Utility.checkNull(objVersion.getReleaseType()))) {
				objUpVersionParam.addValue("ProductionReleaseNote", objVersion.getReleaseNotePath().getOriginalFilename());
				objUpVersionParam.addValue("QAReleaseNote", objVersion.getQAFileName());
				if ((("QA".equals(Utility.checkNull(objVersion.getHiddenRelType()))) || ("iTunes".equals(Utility.checkNull(objVersion
						.getHiddenRelType())))) && "".equals(Utility.checkNull(objVersion.getFirstProdtnRelDate()))) {
					objVersion.setFirstProdtnRelDate(strDateTimeStamp);
				} else {
					objVersion.setLastProdtnRelDate(strDateTimeStamp);
				}
				objVersion.setRelProdtnNotePath(strWrittenPath);
			}
			if ("iTunes".equals(Utility.checkNull(objVersion.getReleaseType()))) {
				objVersion.setBuildApprovalDate(strDateTimeStamp);
				objUpVersionParam.addValue("ReleaseNoteName", null);
				objUpVersionParam.addValue("ProductionReleaseNote", null);
				objUpVersionParam.addValue("QAReleaseNote", null);
				if ("approvedNo".equals(Utility.checkNull(objVersion.getApprovedYesNo()))) {
					objVersion.setiTunesUploadDate(null);
				} else {
					objVersion.setiTunesUploadDate(strDateTimeStamp);
				}
			}
			objUpVersionParam.addValue("QAFirstReleaseDate", objVersion.getFirstQARelDate());
			objUpVersionParam.addValue("QALastReleaseDate", objVersion.getLastQARelDate());
			objUpVersionParam.addValue("QAReleaseNotePath", objVersion.getRelQANotePath());
			objUpVersionParam.addValue("ProductionFirstReleaseDate", objVersion.getFirstProdtnRelDate());
			objUpVersionParam.addValue("ProductionLastReleaseDate", objVersion.getLastProdtnRelDate());
			objUpVersionParam.addValue("ProductionReleaseNotePath", objVersion.getRelProdtnNotePath());
			objUpVersionParam.addValue("ITunesUploadDate", objVersion.getiTunesUploadDate());
			objUpVersionParam.addValue("BuildApprovalDate", objVersion.getBuildApprovalDate());

			resultFromProcedure = simpleJdbcCall.execute(objUpVersionParam);
			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				response = ApplicationConstants.SUCCESSTEXT;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside VersionDaoImpl : updateVersion : " + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		return response;
	}

	/**
	 * This DAOImpl method is used to get configuration details details.
	 * 
	 * @param strConfigType
	 *            as input parameter.
	 * @return configuration details list.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as SQL exception
	 */
	public ArrayList<AppConfiguration> getAppConfig(String strConfigType) throws ScanSeeWebSqlException {
		LOG.info("Inside VersionDaoImpl : getAppConfig ");
		ArrayList<AppConfiguration> appConfigurationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource objConfigParam = new MapSqlParameterSource();
			objConfigParam.addValue("ConfigurationType", strConfigType);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objConfigParam);
			appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
		} catch (DataAccessException e) {
			LOG.error("Inside VersionDaoImpl : getAppConfig : " + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		return appConfigurationList;
	}
}

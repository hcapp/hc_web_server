/**
 * 
 */
package admin.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.DashboardExportData;
import common.pojo.ExportDetails;
import common.pojo.HubCiti;
import common.pojo.Modules;
import common.pojo.RetailerExportData;
import common.util.Utility;

/**
 * @author sangeetha.ts
 *
 */
public class ExportDaoImpl implements ExportDao {
	
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ExportDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@SuppressWarnings("unchecked")
	public List<HubCiti> getExportHubCities() throws ScanSeeWebSqlException {
		
		final String methodName = "getExportHubCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<HubCiti> hubcitiList = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcDataExportHubCitiList");
			simpleJdbcCall.returningResultSet("hubcitiList", new BeanPropertyRowMapper<HubCiti>(HubCiti.class));
			final MapSqlParameterSource param = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(param);
			status = (Integer) resultFromProcedure.get("Status");

			if (null != status && status == 0) {
				hubcitiList = (ArrayList<HubCiti>) resultFromProcedure.get("hubcitiList");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum	+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hubcitiList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Modules> getExportType() throws ScanSeeWebSqlException {
		
		final String methodName = "getExportType";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Modules> moduleList = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcDataExportTypes");
			simpleJdbcCall.returningResultSet("moduleList", new BeanPropertyRowMapper<Modules>(Modules.class));
			final MapSqlParameterSource param = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(param);
			status = (Integer) resultFromProcedure.get("Status");
			if (null != status && status == 0) {
				moduleList = (ArrayList<Modules>) resultFromProcedure.get("moduleList");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum	+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return moduleList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Modules> getExportOptions(int exportType) throws ScanSeeWebSqlException {
		
		final String methodName = "getExportOptions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Modules> moduleList = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcDataExportModules");
			simpleJdbcCall.returningResultSet("moduleList", new BeanPropertyRowMapper<Modules>(Modules.class));
			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("HcDataExportTypeID", exportType);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(params);
			status = (Integer) resultFromProcedure.get("Status");
			if (null != status && status == 0) {
				moduleList = (ArrayList<Modules>) resultFromProcedure.get("moduleList");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum	+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return moduleList;
	}
	
	@SuppressWarnings("unchecked")
	public ExportDetails exportData(Modules module) throws ScanSeeWebSqlException {
		
		final String methodName = "exportData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<DashboardExportData> exportList = null;
		ExportDetails details = null;
		String exportFilePath = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcDataExportDashboardDataDisplay");
			simpleJdbcCall.returningResultSet("exportList", new BeanPropertyRowMapper<DashboardExportData>(DashboardExportData.class));
			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("HcDataExportTypeID", module.getModuleTypeId());
			params.addValue("HcHubCitiID", module.getHubCitiId());
			params.addValue("HcDataExportModuleID", module.getModuleIds());
			params.addValue("StartDate", Utility.getFormattedDateTime(module.getFromDate()));
			params.addValue("EndDate", Utility.getFormattedDateTime(module.getToDate()));

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(params);
			status = (Integer) resultFromProcedure.get("Status");
			if (null != status && status == 0) {
				exportList = (ArrayList<DashboardExportData>) resultFromProcedure.get("exportList");
				exportFilePath = (String) resultFromProcedure.get("ExportFilePath");
				if(null != exportList && !exportList.isEmpty()) {
					details = new ExportDetails();
					details.setDashboardExportData(exportList);
					details.setFilePath(exportFilePath);
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum	+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		} catch (ParseException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return details;
	}
	
	@SuppressWarnings("unchecked")
	public ExportDetails exportRetailerData(Modules module) throws ScanSeeWebSqlException {
		
		final String methodName = "exportData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<RetailerExportData> exportList = null;
		ExportDetails details = null;
		String exportFilePath = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcDataExportRetailerDataDisplay");
			simpleJdbcCall.returningResultSet("exportList", new BeanPropertyRowMapper<RetailerExportData>(RetailerExportData.class));
			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("HcDataExportTypeID", module.getModuleTypeId());
			params.addValue("HcHubCitiID", module.getHubCitiId());
			params.addValue("HcDataExportModuleID", module.getModuleIds());
			params.addValue("StartDate", Utility.getFormattedDateTime(module.getFromDate()));
			params.addValue("EndDate", Utility.getFormattedDateTime(module.getToDate()));

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(params);
			status = (Integer) resultFromProcedure.get("Status");
			if (null != status && status == 0) {
				exportList = (ArrayList<RetailerExportData>) resultFromProcedure.get("exportList");
				exportFilePath = (String) resultFromProcedure.get("ExportFilePath");
				if(null != exportList && !exportList.isEmpty()) {
					details = new ExportDetails();
					details.setRetailerExportData(exportList);
					details.setFilePath(exportFilePath);
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum	+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new ScanSeeWebSqlException(e.getMessage());
		} catch (ParseException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e.getMessage());
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return details;
	}
	
	public String saveExportHubCities(HubCiti hubCiti) throws ScanSeeWebSqlException {
		
		final String methodName = "saveExportHubCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcDataExportHubCitiListUpdation");
			final MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue("CheckedHcHubCitiID", hubCiti.getSelRegHubCitiIds());
			params.addValue("UncheckedHcHubCitiID", hubCiti.getUnChkdNatId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(params);
			status = (Integer) resultFromProcedure.get("Status");
			if (null != status && status == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum	+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}
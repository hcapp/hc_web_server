/**
 * 
 */
package admin.dao;

import java.util.ArrayList;

import common.exception.ScanSeeWebSqlException;
import common.pojo.Category;
import common.pojo.HotDeal;
import common.pojo.HotDealDetails;
import common.pojo.RetailerLocation;

/**
 * @author sangeetha.ts
 */
public interface HotDealDao
{
	/**
	 * This method returns list of hot deals for the given search key.
	 * 
	 * @param deal
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public HotDealDetails displayHotDeals(HotDeal deal) throws ScanSeeWebSqlException;

	/**
	 * This method is used to filter the promotions from the list of promotions.
	 * 
	 * @param promotionIds
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteHotDeal(String hotDealIds) throws ScanSeeWebSqlException;

	/**
	 * This method is used to display hot deal details.
	 * 
	 * @param promotionId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public HotDeal displayHotDealDetails(int hotdealId) throws ScanSeeWebSqlException;

	/**
	 * This method is used to display hot deal Categories.
	 * 
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<Category> displayHotDealCategories() throws ScanSeeWebSqlException;

	/**
	 * This method is used to display Retailer Locations.
	 * 
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<RetailerLocation> displayRetailerLocations(Integer retailId, String city, String state) throws ScanSeeWebSqlException;

	/**
	 * This method is used to update hot deal details.
	 * 
	 * @param hotDeal
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	public String updateHotDeal(HotDeal hotDeal) throws ScanSeeWebSqlException;
}

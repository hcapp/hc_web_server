/**
 * 
 */
package admin.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AlertCategory;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.Retailer;
import common.pojo.RetailerDetails;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationDetails;
import common.pojo.State;

/**
 * @author sangeetha.ts
 */
public class RetailerDaoImpl implements RetailerDao {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RetailerDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * Variable platformTxManager declared as instance of
	 * PlatformTransactionManager.
	 */
	private PlatformTransactionManager platformTxManager;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * To set platformTransactionManager.
	 * 
	 * @param platformTxManager
	 *            to set.
	 */
	public void setPlatformTransactionManager(PlatformTransactionManager platformTxManager) {
		this.platformTxManager = platformTxManager;
	}

	/**
	 * This method is used to display list of Retailers.
	 * 
	 * @param SearchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public RetailerDetails displayRetailerList(Retailer retailerInfo) throws ScanSeeWebSqlException {
		final String methodName = "displayRetailerList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		RetailerDetails retailerDetails = null;
		ArrayList<Retailer> retailers = null;
		Integer rowcount = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);

			if (null != retailerInfo.getIsBand() && retailerInfo.getIsBand() == true) {
				// Band List
				simpleJdbcCall.withProcedureName("usp_WebBandAdminRetrieveband");
			} else {
				// Retailer List
				simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetrieveRetailer");
			}

			simpleJdbcCall.returningResultSet("retailerList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();

			retailerParams.addValue("RetailName", retailerInfo.getSearchKey());
			retailerParams.addValue("City", retailerInfo.getCity());
			retailerParams.addValue("State", retailerInfo.getState());
			retailerParams.addValue("LowerLimit", retailerInfo.getLowerLimit());
			retailerParams.addValue("ScreenName", ApplicationConstants.RETAILER_SCREENNAME);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);

			if (null != resultFromProcedure) {
				retailers = (ArrayList<Retailer>) resultFromProcedure.get("retailerList");
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");

				retailerDetails = new RetailerDetails();
				if (null != rowcount) {
					retailerDetails.setTotalSize(rowcount);
				}
				retailerDetails.setRetailerDetailList(retailers);
			} else {

				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + "Error number: {}" + errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerDetails;
	}

	/**
	 * The DAOImpl method for displaying all the states
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL exception.
	 * @return states,List of states.
	 */
	public ArrayList<State> getAllStates() throws ScanSeeWebSqlException {
		final String methodName = "getAllStates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<State> states = null;

		try {
			states = (ArrayList<State>) this.jdbcTemplate.query("SELECT StateName,Stateabbrevation FROM [State] order by StateName",
					new RowMapper<State>() {
						public State mapRow(ResultSet rs, int rowNum) throws SQLException {
							final State state = new State();
							state.setStateName(rs.getString("StateName"));
							state.setStateabbr(rs.getString("Stateabbrevation"));

							return state;
						}

					});
		} catch (EmptyResultDataAccessException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeWebSqlException(exception);
		} catch (DataAccessException e) {
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return states;
	}

	/**
	 * this method used get All cities.
	 * 
	 * @param stateName
	 *            -For which cities to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public final ArrayList<City> getAllCities(String stateName) throws ScanSeeWebSqlException {
		final String methodName = "getAllCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<City> cityList = null;
		try {
			cityList = this.jdbcTemplate.query("SELECT Distinct City FROM GeoPosition WHERE [State] =? order by City", new Object[] { stateName },
					new RowMapper<City>() {
						public City mapRow(ResultSet rs, int rowNum) throws SQLException {
							final City city = new City();

							city.setCityName(rs.getString("City"));

							return city;
						}

					});
		} catch (EmptyResultDataAccessException exception) {

			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeWebSqlException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return (ArrayList<City>) cityList;
	}

	/**
	 * This method is used to delete the Retailers from the list of Retailers.
	 * 
	 * @param retailerIds
	 * @param userId
	 * @return success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteRetailer(String retailerIds, Integer userId, Boolean isBand) throws ScanSeeWebSqlException {
		final String methodName = "deleteRetailer";
		LOG.info("Entering DAO " + methodName + " method");

		String statusMsg = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);

			if (null != isBand && isBand == true) {
				simpleJdbcCall.withProcedureName("usp_WebBandAdminDeleteBand");
			} else {
				simpleJdbcCall.withProcedureName("usp_WebRetailerAdminDeleteRetailer");
			}

			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();

			retailerParams.addValue("RetailID", retailerIds);
			retailerParams.addValue("UserID", userId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);

			status = (Integer) resultFromProcedure.get("Status");

			if (null != status && status == 0) {
				statusMsg = (String) resultFromProcedure.get("FindClearCacheURL");
				// statusMsg = ApplicationConstants.SUCCESSTEXT;
			}
			/*
			 * if (status == 0) {
			 * 
			 * statusMsg = "Success"; }
			 */else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return statusMsg;
	}

	/**
	 * This method is used to display list of Retailer Locations.
	 * 
	 * @param SearchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeException
	 */
	public RetailerLocationDetails displayRetailerLocation(RetailerLocation retailerInfo) throws ScanSeeWebSqlException {
		final String methodName = "displayRetailerLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		RetailerLocationDetails retailerLocationDetails = null;
		ArrayList<RetailerLocation> retailerLocations = null;
		Integer rowcount = null;
		String strClearCache = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetrieveRetailerLocation");
			simpleJdbcCall.returningResultSet("retailerLocList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();

			retailerParams.addValue("RetailID", retailerInfo.getRetailId());
			retailerParams.addValue("LowerLimit", retailerInfo.getLowerLimit());
			retailerParams.addValue("StoreIdentification", retailerInfo.getStoreIdentification());
			retailerParams.addValue("Address1", retailerInfo.getAddress1());
			retailerParams.addValue("ScreenName", ApplicationConstants.RETAILER_SCREENNAME);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);

			if (null != resultFromProcedure) {

				retailerLocations = new ArrayList<RetailerLocation>();
				retailerLocations = (ArrayList<RetailerLocation>) resultFromProcedure.get("retailerLocList");
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				strClearCache = (String) resultFromProcedure.get("FindClearCacheURL");

				retailerLocationDetails = new RetailerLocationDetails();
				if (null != rowcount) {
					retailerLocationDetails.setTotalSize(rowcount);
				}
				if (null != strClearCache) {
					retailerLocationDetails.setResponse(strClearCache);
				}
				retailerLocationDetails.setRetailerLocations(retailerLocations);
			}

		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerLocationDetails;
	}

	/**
	 * This method is used to delete the Retailer Locations from the list of
	 * Retailer Locations.
	 * 
	 * @param retailerIds
	 * @param userId
	 * @return success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteRetailerLocation(String retailerLocationIds, Integer userId) throws ScanSeeWebSqlException {
		final String methodName = "deleteRetailerLocation";
		LOG.info("Entering DAO " + methodName + " method");

		String strClearCache = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerAdminDeleteRetailerLocation");
			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();

			retailerParams.addValue("RetailLocationID", retailerLocationIds);
			retailerParams.addValue("UserID", userId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);

			status = (Integer) resultFromProcedure.get("Status");

			if (null != status && status == 0) {
				strClearCache = (String) resultFromProcedure.get("FindClearCacheURL");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strClearCache;
	}

	public final String saveLocationList(final List<RetailerLocation> locationList, final long userId) throws ScanSeeWebSqlException {
		final String methodName = "saveLocationList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		TransactionStatus status = null;

		try {

			DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();
			status = platformTxManager.getTransaction(paramTransactionDefinition);
			final int[] arry = jdbcTemplate.batchUpdate("{call usp_WebRetailerAdminRetailLocationUpdation(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement ps, int i) throws SQLException {
							final RetailerLocation objlocation = locationList.get(i);
							ps.setInt(1, objlocation.getRetailId());
							ps.setInt(2, Integer.valueOf(objlocation.getRetailLocationId()));
							if (null != objlocation.getStoreIdentification() && !"".equals(objlocation.getStoreIdentification())) {
								ps.setString(3, objlocation.getStoreIdentification());
							} else {
								ps.setString(3, null);
							}
							ps.setString(4, objlocation.getAddress1());
							ps.setString(5, objlocation.getCity());
							ps.setString(6, objlocation.getState());
							ps.setString(7, objlocation.getPostalCode());
							ps.setString(8, objlocation.getWebsiteURL());
							if (null == objlocation.getLatitude()) {
								ps.setDouble(9, 0.0);
							} else {
								ps.setDouble(9, objlocation.getLatitude());
							}
							if (null == objlocation.getLongitude()) {
								ps.setDouble(10, 0.0);
							} else {
								ps.setDouble(10, objlocation.getLongitude());
							}
							ps.setString(11, objlocation.getPhone());
							ps.setInt(12, (int) userId);
							ps.setString(13, objlocation.getGridImgLocationPath());
							ps.setNull(14, Types.INTEGER);
							ps.setNull(15, Types.INTEGER);
							ps.setNull(16, Types.VARCHAR);

						}

						public int getBatchSize() {
							return locationList.size();
						}
					});

			response = ApplicationConstants.SUCCESS;
			platformTxManager.commit(status);
		} catch (DataAccessException e) {
			platformTxManager.rollback(status);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (Exception e) {
			platformTxManager.rollback(status);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String saveRetailerInfo(Retailer retailer) throws ScanSeeWebSqlException {
		final String methodName = "saveRetailerInfo";
		LOG.info("Entering DAO " + methodName + " method");

		String statusMsg = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);

			if (null != retailer.getIsBand() && retailer.getIsBand() == true) {
				simpleJdbcCall.withProcedureName("usp_WebBandAdminBandUpdation");
			} else {
				simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetailerUpdation");
			}

			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();

			retailerParams.addValue("UserID", retailer.getUserId());
			retailerParams.addValue("RetailID", retailer.getRetailId());
			retailerParams.addValue("RetailName", retailer.getRetailName());
			retailerParams.addValue("RetailURL", retailer.getRetUrl());
			retailerParams.addValue("Address", retailer.getAddress1());
			retailerParams.addValue("City", retailer.getCity());
			retailerParams.addValue("State", retailer.getState());
			retailerParams.addValue("PosatalCode", retailer.getPostalCode());
			retailerParams.addValue("Latitude", retailer.getLatitude());
			retailerParams.addValue("Longitude", retailer.getLongitude());
			retailerParams.addValue("CorporatePhoneNo", retailer.getPhoneNo());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);

			if (null != resultFromProcedure) {
				status = (Integer) resultFromProcedure.get("Status");

				if (null != status && status == 0) {
					statusMsg = (String) resultFromProcedure.get(ApplicationConstants.FIND_CLEAR_CACHE_URL);
					// statusMsg = ApplicationConstants.SUCCESSTEXT;
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			} else {
				statusMsg = ApplicationConstants.FAILURETEXT;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + "Error number: {}" + errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return statusMsg;
	}

	/**
	 * This RetailerDaoImpl method used get All Business Category/Sub Category
	 * associated to Retailer/Location.
	 * 
	 * @param Retailer
	 *            objRetailer
	 * @return List of Business Category.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public ArrayList<Category> fetchBusCategory(Retailer objRetailer) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDaoImpl : fetchBusCategory");
		ArrayList<Category> arBusCategoryList = null;
		Integer status = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource busCategoryParams = new MapSqlParameterSource();
			if (ApplicationConstants.BUSINESS_CATEGORIES.equals(objRetailer.getCategoryType())) {
				/*
				 * To list out the Business categories and identify if it is
				 * associated to the given Retailer.
				 */
				simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetrieveRetailerBusinessCategory");
			} else {
				/*
				 * To list out the subcategories and identify if it is
				 * associated to the given location.
				 */
				simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetrieveRetailLocationSubCategory");
				busCategoryParams.addValue("RetailLocationID", objRetailer.getRetailLocationId());
			}
			busCategoryParams.addValue("RetailID", objRetailer.getRetailId());
			simpleJdbcCall.returningResultSet("hubCitiBusCategory", new BeanPropertyRowMapper<Category>(Category.class));

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(busCategoryParams);
			status = (Integer) resultFromProcedure.get("Status");
			if (null != status && status == 0) {
				arBusCategoryList = (ArrayList<Category>) resultFromProcedure.get("hubCitiBusCategory");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException dataAccessException) {
			LOG.error("Inside RetailerDaoImpl : fetchBusCategory : dataAccessException  : " + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}
		return arBusCategoryList;
	}

	/**
	 * This daoImpl method used to update Business Category/Sub Category
	 * associated to Retailer/Location.
	 * 
	 * @param objRetailerId
	 *            ,strBCategory
	 * @return success or failure
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeWebSqlException will be
	 *             thrown.
	 */
	public String updateBusCategory(Retailer objRetailer) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDaoImpl : updateBusCategory");
		String statusMsg = null;
		Integer status = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();
			if (ApplicationConstants.BUSINESS_CATEGORIES.equals(objRetailer.getCategoryType())) {
				/* To update the Retailer Business Categories. */
				simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetailerBusinessCategoryUpdation");
			} else {
				/* To associate the subcategories to the locations. */
				simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetailLocationSubCategoryAssociation");
				retailerParams.addValue("RetailLocationID", objRetailer.getRetailLocationId());
				retailerParams.addValue("UserID", objRetailer.getUserId());
			}
			retailerParams.addValue("RetailID", objRetailer.getRetailId());
			retailerParams.addValue("BusinessCategoryIDs", objRetailer.getbCategoryIds());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);
			status = (Integer) resultFromProcedure.get("Status");

			/*
			 * if (status == 0) { statusMsg = ApplicationConstants.SUCCESSTEXT;
			 * }
			 */
			if (null != status && status == 0) {
				// Implementation for ClearCache Scenario Handling
				statusMsg = (String) resultFromProcedure.get(ApplicationConstants.FIND_CLEAR_CACHE_URL);
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				statusMsg = ApplicationConstants.FAILURETEXT;
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDaoImpl : updateBusCategory : dataAccessException  : " + e.getStackTrace());
			throw new ScanSeeWebSqlException(e);
		}
		return statusMsg;
	}

	/**
	 * DAO method to get retailer login user name and password.
	 * 
	 * @param objRetailer
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Retailer retailerLoginDetails(Retailer objRetailer) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDaoImpl : retailerLoginDetails");
		Integer status = null;
		ArrayList<Retailer> retailerList = null;
		Retailer retailerDetails = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();

			if (null != objRetailer.getIsBand() && objRetailer.getIsBand() == true) {
				simpleJdbcCall.withProcedureName("usp_WebUserLoginDetails_Band");
			} else {
				simpleJdbcCall.withProcedureName("usp_WebUserLoginDetails");
			}
			retailerParams.addValue("RetailID", objRetailer.getRetailId());
			simpleJdbcCall.returningResultSet("retailerDetails", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);
			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				retailerList = (ArrayList<Retailer>) resultFromProcedure.get("retailerDetails");
				if (retailerList != null && !retailerList.isEmpty()) {
					retailerDetails = new Retailer();
					retailerDetails.setUserName(retailerList.get(0).getUserName());
					retailerDetails.setEmail(retailerList.get(0).getEmail());
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDaoImpl : retailerLoginDetails : dataAccessException  : " + e.getStackTrace());
			throw new ScanSeeWebSqlException(e);
		}
		return retailerDetails;
	}

	/**
	 * DAO method to save/update retailer login details i.e. User name, email
	 * and auto generated password. Validation will be done for duplicate email
	 * and user name.
	 * 
	 * @param objRetailer
	 * @param enryptPassword
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Retailer saveRetailerLoginDetails(Retailer objRetailer, String enryptPassword) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDaoImpl : saveRetailerLoginDetails.");
		String statusMsg = null;
		Integer status = null;
		Boolean duplicateEmail = null;
		Boolean duplicateUserName = null;
		String retailName = null;
		Retailer retailer = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();
			if (null != objRetailer.getIsBand() && objRetailer.getIsBand() == true) {
				simpleJdbcCall.withProcedureName("usp_WebAdminUserLoginCreationandUpdation_Band");
			} else {
				simpleJdbcCall.withProcedureName("usp_WebAdminUserLoginCreationandUpdation");
			}

			retailerParams.addValue("RetailID", objRetailer.getRetailId());
			retailerParams.addValue("Username", objRetailer.getUserName());
			retailerParams.addValue("Password", enryptPassword);
			retailerParams.addValue("EmailID", objRetailer.getEmail());
			retailerParams.addValue("LoginExist", objRetailer.getIsLoginExist());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);
			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				duplicateUserName = (Boolean) resultFromProcedure.get("DuplicateUserName");
				duplicateEmail = (Boolean) resultFromProcedure.get("DuplicateEmail");
				retailName = (String) resultFromProcedure.get("RetailName");
				retailer = new Retailer();
				retailer.setRetailName(retailName);
				if (duplicateUserName != null && duplicateUserName == true) {
					retailer.setStatus(ApplicationConstants.DUPLICATEUSERNAME);
				} else if (duplicateEmail != null && duplicateEmail == true) {
					retailer.setStatus(ApplicationConstants.DUPLICATEEMAIL);
				} else {
					retailer.setStatus(ApplicationConstants.SUCCESSTEXT);
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDaoImpl : retailerLoginDetails : dataAccessException  : " + e.getStackTrace());
			throw new ScanSeeWebSqlException(e);
		}
		return retailer;
	}

	/**
	 * DAO method to save/update retailer login details i.e. User name, email
	 * and auto generated password. Validation will be done for duplicate email
	 * and user name.
	 * 
	 * @param objRetailer
	 * @param enryptPassword
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String saveRetailerPaymentInfo(Integer retailId, Integer userID, Boolean isPaid, Boolean isBand) throws ScanSeeWebSqlException {

		LOG.info("Inside RetailerDaoImpl : saveRetailerPaymentInfo.");
		String statusMsg = null;
		Integer status = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource retailerParams = new MapSqlParameterSource();

			if (null != isBand && isBand == true) {
				simpleJdbcCall.withProcedureName("usp_WebBandPayUnpay");
			} else {
				simpleJdbcCall.withProcedureName("usp_WebRetailerPayUnpay");
			}

			retailerParams.addValue("RetailerID", retailId);
			retailerParams.addValue("UserID", userID);
			retailerParams.addValue("isPaid", isPaid);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(retailerParams);
			status = (Integer) resultFromProcedure.get("Status");

			if (status == 0) {
				statusMsg = ApplicationConstants.SUCCESSTEXT;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDaoImpl : saveRetailerPaymentInfo : dataAccessException  : " + e.getStackTrace());
			throw new ScanSeeWebSqlException(e);
		}
		return statusMsg;
	}

	/**
	 * The DAOImpl method to get Cached API from the database.
	 * 
	 * @throws ScanSeeWebSqlException
	 * 
	 * @return String object.
	 */
	public String getClearCacheForFind() throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDaoImpl : getClearCacheForFind");

		String statusMsg = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ClearCacheURL");

			final MapSqlParameterSource clearCacheParam = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(clearCacheParam);
			status = (Integer) resultFromProcedure.get("Status");

			if (status == 0) {
				statusMsg = (String) resultFromProcedure.get("FindClearCacheURL");
			} else {
				statusMsg = ApplicationConstants.FAILURETEXT;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDaoImpl : getClearCacheForFind : dataAccessException  : " + e.getStackTrace());
			throw new ScanSeeWebSqlException(e);
		}
		return statusMsg;

	}

	
	public Category getBandcat(String retailId) throws ScanSeeWebSqlException {

		LOG.info("Inside RetailerDaoImpl : fetchBusCategory");
		Category category = null;
		ArrayList<Category> buscategoryList = null;
		ArrayList<Category> subcategoryList = null;

		Integer status = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource busCategoryParams = new MapSqlParameterSource();

			simpleJdbcCall.withProcedureName("usp_WebBandAdminRetrieveBandBusinessCategory");
			busCategoryParams.addValue("RetailID", retailId);

			simpleJdbcCall.returningResultSet("BandBusCategory", new BeanPropertyRowMapper<Category>(Category.class));
			simpleJdbcCall.returningResultSet("subcategoryList", new BeanPropertyRowMapper<Category>(Category.class));

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(busCategoryParams);
			status = (Integer) resultFromProcedure.get("Status");
			if (null != status && status == 0) {
				buscategoryList = (ArrayList<Category>) resultFromProcedure.get("BandBusCategory");
				subcategoryList = (ArrayList<Category>) resultFromProcedure.get("subcategoryList");
				category = new Category();
				category.setBuscategoryList(buscategoryList);
				category.setSubcategoryList(subcategoryList);
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException dataAccessException) {
			LOG.error("Inside RetailerDaoImpl : fetchBusCategory : dataAccessException  : " + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}
		return category;
	}

	
	public String savebandcat(String retailId, String buscatIds, String subcatIds, String userID) throws ScanSeeWebSqlException {

		LOG.info("Inside RetailerDaoImpl : fetchBusCategory");

		String response = null;

		Integer status = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource busCategoryParams = new MapSqlParameterSource();

			simpleJdbcCall.withProcedureName("usp_WebRetailerAdminBandCategoryAssociationUpdation");
			busCategoryParams.addValue("RetailID", retailId);
			busCategoryParams.addValue("BusinessCategoryIDs", buscatIds);
			busCategoryParams.addValue("BusinessSubCategoryIDs", subcatIds);
			busCategoryParams.addValue("UserID", userID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(busCategoryParams);
			status = (Integer) resultFromProcedure.get("Status");
			if (null != status && status == 0) {
				response = "Band Categories are updated successfully.";
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException dataAccessException) {
			LOG.error("Inside RetailerDaoImpl : fetchBusCategory : dataAccessException  : " + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}
		return response;
	}

	/**
	 * Below method is used to display retailer location level filters.
	 */
	@SuppressWarnings("unchecked")
	public AlertCategory getRetLocFilters(Retailer objRetailer) throws ScanSeeWebSqlException {

		final String strMethodName = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		Integer iStatus = null;
		
		ArrayList<Category> categorylst = null;
		AlertCategory filtercategory = null;
		String filterCategory = null;
		String filters = null;
		String filterValues = null;
		try{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource busCategoryParams = new MapSqlParameterSource();
			simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetrieveRetailLocationFilters");
			busCategoryParams.addValue("RetailID",objRetailer.getRetailId());
			busCategoryParams.addValue("RetailLocationID", objRetailer.getRetailLocationId());
	

			simpleJdbcCall.returningResultSet("categorylst", new BeanPropertyRowMapper<Category>(Category.class));

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(busCategoryParams);
			iStatus = (Integer) resultFromProcedure.get("Status");
			if (null != iStatus && iStatus == 0) {				
				
				categorylst = (ArrayList<Category>) resultFromProcedure.get("categorylst");
				
				filterCategory = (String) resultFromProcedure.get("FilterCategory");
				filters= (String) resultFromProcedure.get("Filters");
				filterValues= (String) resultFromProcedure.get("FilterValues");
				
				if(null != categorylst && !categorylst.isEmpty())
				{
					filtercategory= new AlertCategory();
					filtercategory.setCategorieslst(categorylst);
					filtercategory.setFilterCategory(filterCategory);
					filtercategory.setFilters(filters);
					filtercategory.setFilterValues(filterValues);
					
				}
				 
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException);
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return filtercategory;
	}
	
	public static void main(String a[])
	{
		
		
		
	}

	
	public String associateFilters(RetailerLocation objRetailerLocation) throws ScanSeeWebSqlException {
		final String strMethodName = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		Integer iStatus = null;
		String response = null;
		try{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource busCategoryParams = new MapSqlParameterSource();
			simpleJdbcCall.withProcedureName("usp_WebRetailerAdminRetailLocationFilterAssociation");
			busCategoryParams.addValue("RetailID", objRetailerLocation.getRetailId());
			busCategoryParams.addValue("RetailLocationID",objRetailerLocation.getRetailLocationId());
			busCategoryParams.addValue("UserID", objRetailerLocation.getUserId());
			busCategoryParams.addValue("BusinessCategoryIDs",objRetailerLocation.getFilterCategory());
			busCategoryParams.addValue("AdminFilterID", objRetailerLocation.getFilters());
			busCategoryParams.addValue("AdminFilterValueID",objRetailerLocation.getFilterValues());
	
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(busCategoryParams);
			iStatus = (Integer) resultFromProcedure.get("Status");
			if (null != iStatus && iStatus == 0) {
				
				
				response = ApplicationConstants.SUCCESSTEXT;
				response = (String) resultFromProcedure.get("FindClearCacheURL");
				
			} else {
				response = ApplicationConstants.FAILURETEXT;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException);
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return response;
	}
	
	
	
}

/**
 * 
 */
package admin.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.APIPartner;
import common.pojo.AlertCategory;
import common.pojo.AppConfiguration;
import common.pojo.EventCategory;
import common.pojo.HubCiti;
import common.pojo.HubCitiDetails;
import common.pojo.PostalCode;
import common.pojo.RegionInfo;
import common.util.Utility;
/**
 * @author sangeetha.ts
 */
public class HubCitiDaoImpl implements HubCitiDao {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(HubCitiDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;
	/**
	 * Variable platformTxManager declared as instance of
	 * PlatformTransactionManager.
	 */
	private PlatformTransactionManager platformTxManager;

	/**
	 * To set platformTransactionManager.
	 * 
	 * @param platformTxManager
	 *            to set.
	 */
	public void setPlatformTransactionManager(PlatformTransactionManager platformTxManager) {
		this.platformTxManager = platformTxManager;
	}

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This method returns list of hubciti admins for the given search key.
	 * 
	 * @param userId
	 * @param searchKey
	 * @param lowerLimit
	 * @return HubCitiDetails
	 * @throws ScanSeeWebSqlException
	 */

	@SuppressWarnings("unchecked")
	public HubCitiDetails displayHubCiti(Integer userId, String searchKey, Integer lowerLimit, Boolean showDeactivated) throws ScanSeeWebSqlException {
		final String methodName = "displayHubCiti";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HubCitiDetails citiDetails = null;
		ArrayList<HubCiti> citis = null;
		Integer totalSize = null;
		Integer status = null;
		String response = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminHubCitiDisplay");
			simpleJdbcCall.returningResultSet("admins", new BeanPropertyRowMapper<HubCiti>(HubCiti.class));
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("ScanSeeAdminUserID", userId);
			adminParams.addValue("SearchKey", searchKey);
			adminParams.addValue("ShowDeactivated", showDeactivated);
			adminParams.addValue("LowerLimit", lowerLimit);
			adminParams.addValue("AppRole", "HubCitiApp");

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			status = (Integer) resultFromProcedure.get("Status");

			if (null != status && status == 0) {
				citis = (ArrayList<HubCiti>) resultFromProcedure.get("admins");
				totalSize = (Integer) resultFromProcedure.get("MaxCnt");
				response = (String) resultFromProcedure.get("FindClearCacheURL");
				citiDetails = new HubCitiDetails();
				citiDetails.setHubCitis(citis);
				citiDetails.setTotalSize(totalSize);
				citiDetails.setResponse(response);
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return citiDetails;
	}

	/**
	 * This method is used to deactivate the hudciti admin based on admin id
	 * given.
	 * 
	 * @param adminId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deactivateAdmin(Integer userId, Integer adminId, Integer activeFlag) throws ScanSeeWebSqlException {
		final String methodName = "deactivateAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminChangeHubCitiStatus");
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("ScanSeeAdminUserID", userId);
			adminParams.addValue("HubCitiID", adminId);
			adminParams.addValue("Deactivate", activeFlag);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			Integer response = (Integer) resultFromProcedure.get("Status");

			if (null != response && response == 0) {
				status = "Success";
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * this method used get All PostalCodes.
	 * 
	 * @param stateName
	 *            -For which PostalCodes to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeWebSqlException will be
	 *             thrown.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<PostalCode> getAllPostalCode(String city, String state) throws ScanSeeWebSqlException {
		final String methodName = "getAllPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<PostalCode> postalCodes = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminPostalCodeDisplay");
			simpleJdbcCall.returningResultSet("PostalCodes", new BeanPropertyRowMapper<PostalCode>(PostalCode.class));
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("State", state);
			adminParams.addValue("City", city);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			Integer response = (Integer) resultFromProcedure.get("Status");

			if (null != response && response == 0) {
				postalCodes = (ArrayList<PostalCode>) resultFromProcedure.get("PostalCodes");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return postalCodes;
	}

	/**
	 * This method is used to create the hudciti admin.
	 * 
	 * @param citi
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String createAdmin(HubCiti citi) throws ScanSeeWebSqlException {
		final String methodName = "createAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminHubCitiCreation");
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("HcHubCitiID", citi.getHubCitiID());
			adminParams.addValue("ScanSeeAdminUserID", citi.getUserID());
			adminParams.addValue("HubCitiName", citi.getHubCitiName());
			adminParams.addValue("CityExperience", citi.getCitiExperience());
			adminParams.addValue("UserName", citi.getUserName());
			adminParams.addValue("Password", citi.getEncrptedPassword());
			adminParams.addValue("EmailID", citi.getEmailID());
			adminParams.addValue("HcHubCitiIDs", citi.getSelRegHubCitiIds());
			adminParams.addValue("AppRole", citi.getAppRole());
			adminParams.addValue("DefaultPostalCode", citi.getDefaultZipcode());
			adminParams.addValue("SalesPersonEmail", citi.getSalesPersonEmail());
			adminParams.addValue("EmailIDs", citi.getEmails());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);

			if (null != resultFromProcedure) {
				Integer response = (Integer) resultFromProcedure.get("Status");
				if (response != null && response == 0) {
					Boolean duplicateAdmin = (Boolean) resultFromProcedure.get("DuplicateHubCitiName");
					Boolean duplicateUser = (Boolean) resultFromProcedure.get("DuplicateUserName");
					Boolean duplicateEmail = (Boolean) resultFromProcedure.get("DuplicateEmail");
					Integer hubCitiID = (Integer) resultFromProcedure.get("HubCitiID");

					if (duplicateAdmin == true) {
						status = ApplicationConstants.DUPLICATEHUBCITI;
					} else if (duplicateUser == true) {
						status = ApplicationConstants.DUPLICATEUSERNAME;
					} else if (duplicateEmail == true) {
						status = ApplicationConstants.DUPLICATEEMAIL;
					} else if (null != hubCitiID) {
						status = hubCitiID.toString();
					} else if (response == 1) {
						final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
						final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
						LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
						throw new ScanSeeWebSqlException(errorMsg);
					} else {
						status = "Success";
						final String URLs = (String) resultFromProcedure.get("FindClearCacheURL");
						try {
							Utility.clearCache(URLs,"update HubCiti/Region");
						} catch (ScanSeeServiceException e) {
							LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + " : " + e.getMessage());
							e.printStackTrace();
						}
						
					}
				} else {
					status = null;
				}

			}

		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException {
		final String methodName = "getAppConfig";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);

			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return appConfigurationList;
	}

	/**
	 * This method is used to domain Name for Application configuration
	 * 
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<AppConfiguration> getAppConfigForGeneralImages(String configType) throws ScanSeeWebSqlException {
		final String methodName = "getAppConfigForGeneralImages";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_GeneralImagesGetScreenContent");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return appConfigurationList;
	}

	/**
	 * This method is used to get the details of hudciti admin.
	 * 
	 * @param userID
	 * @param hubCitiID
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public HubCiti editAdmin(Integer userID, Integer hubCitiID) throws ScanSeeWebSqlException {
		final String methodName = "editAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<HubCiti> cities = null;
		HubCiti citi = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminHubCitiDetails");
			simpleJdbcCall.returningResultSet("hubCitiDetails", new BeanPropertyRowMapper<HubCiti>(HubCiti.class));
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("ScanSeeAdminUserID", userID);
			adminParams.addValue("HubCitiID", hubCitiID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			Integer response = (Integer) resultFromProcedure.get("Status");

			if (null != response && response == 0) {
				cities = (ArrayList<HubCiti>) resultFromProcedure.get("hubCitiDetails");
				if (null != cities && !cities.isEmpty()) {
					citi = new HubCiti();
					citi = cities.get(0);
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return citi;
	}

	/**
	 * this method used get All PostalCodes associated to hubciti.
	 * 
	 * @param sAdminID
	 *            -For which PostalCodes to be fetched.
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeWebSqlException will be
	 *             thrown.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<PostalCode> getPostalCode(Integer sAdminID, Integer hubCitiId) throws ScanSeeWebSqlException {
		final String methodName = "getPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<PostalCode> postalCodes = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminHubCitiLocationsDisplay");
			simpleJdbcCall.returningResultSet("PostalCodes", new BeanPropertyRowMapper<PostalCode>(PostalCode.class));
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("ScanSeeAdminUserID", sAdminID);
			adminParams.addValue("HubCitiID", hubCitiId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			Integer response = (Integer) resultFromProcedure.get("Status");

			if (null != response && response == 0) {
				postalCodes = (ArrayList<PostalCode>) resultFromProcedure.get("PostalCodes");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return postalCodes;
	}

	/**
	 * The DAOImpl method will update retailer multiple locations and its status
	 * return to service method.
	 * 
	 * @param postalCodeList
	 *            instance of ArrayList<HubCiti>.
	 * @param HcHubcitiID
	 *            as request parameter.
	 * @param ScanSeeAdminUserID
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String savePostalCodeList(final ArrayList<HubCiti> postalCodeList, final Integer HcHubcitiID, final Integer ScanSeeAdminUserID)
			throws ScanSeeWebSqlException {
		final String methodName = "savePostalCodeList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		TransactionStatus status = null;

		try {
			DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();
			status = platformTxManager.getTransaction(paramTransactionDefinition);
			@SuppressWarnings("unused")
			final int[] arry = jdbcTemplate.batchUpdate("{call HubCitiWeb.usp_WebHcHubCitiPostalCodeAssociation(?,?,?,?,?,?,?,?)}",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement ps, int i) throws SQLException {
							final HubCiti postalCode = postalCodeList.get(i);
							ps.setInt(1, HcHubcitiID);
							ps.setString(2, postalCode.getState());
							ps.setString(3, postalCode.getCity());
							ps.setString(4, postalCode.getPostalCodes());
							ps.setInt(5, ScanSeeAdminUserID);
							ps.setNull(6, Types.INTEGER);
							ps.setNull(7, Types.INTEGER);
							ps.setNull(8, Types.VARCHAR);
						}

						public int getBatchSize() {
							return postalCodeList.size();
						}
					});

			response = ApplicationConstants.SUCCESS;
			platformTxManager.commit(status);
		} catch (DataAccessException e) {
			platformTxManager.rollback(status);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (Exception e) {
			platformTxManager.rollback(status);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to get the postal code associated to the given
	 * hudciti.
	 * 
	 * @param hubCitiID
	 * @return List of associated postal codes
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<HubCiti> fetchAdminAssociatedPostalCode(Integer hubCitiID) throws ScanSeeWebSqlException {
		final String methodName = "fetchAdminAssociatedPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<HubCiti> cities = null;
		Integer status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminAssociatedPostalCodeDisplay");
			simpleJdbcCall.returningResultSet("hubCitiPostalCodeDetails", new BeanPropertyRowMapper<HubCiti>(HubCiti.class));
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("HubCitiID", hubCitiID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			status = (Integer) resultFromProcedure.get("Status");

			if (null != status && status == 0) {
				cities = (ArrayList<HubCiti>) resultFromProcedure.get("hubCitiPostalCodeDetails");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return cities;
	}

	/**
	 * This method is used to delete the postal code association for a given
	 * hubciti.
	 * 
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deletePostalCodeAssociation(Integer hubCitiId, HubCiti removedPostalCodes) throws ScanSeeWebSqlException {
		final String methodName = "deletePostalCodeAssociation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiPostalCodeDeletion");
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("HcHubcitiID", hubCitiId);
			adminParams.addValue("PostalCode", removedPostalCodes.getPostalCodes());
			adminParams.addValue("City", removedPostalCodes.getCity());
			adminParams.addValue("State", removedPostalCodes.getState());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			Integer response = (Integer) resultFromProcedure.get("Status");

			if (null != response && response == 0) {
				status = "Success";
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * This method is used to associate retailer locations to the given hubciti.
	 * 
	 * @param userId
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String AssociateRetailLoctions(Integer userId, Integer hubCitiId) throws ScanSeeWebSqlException {
		final String methodName = "AssociateRetailLoctions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiRetailLocationAssociation");
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("UserID", userId);
			adminParams.addValue("HcHubcitiID", hubCitiId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			Integer response = (Integer) resultFromProcedure.get("Status");

			if (null != response && response == 0) {
				status = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * This method is used to display and search event categories.
	 * 
	 * @param category
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public AlertCategory fetchEventCategories(EventCategory category, String userId) throws ScanSeeWebSqlException {
		final String methodName = "fetchEventCategories";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer iRowcount = null;
		ArrayList<EventCategory> categoryList = null;
		AlertCategory objAlertCategory = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiEventsCategoryDisplay");
			simpleJdbcCall.returningResultSet("eventcategoryList", new BeanPropertyRowMapper<EventCategory>(EventCategory.class));
			final MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("UserID", userId);
			param.addValue("CategoryName", category.getCatName());
			param.addValue("EventCategoryID", category.getCatId());
			param.addValue("LowerLimit", category.getLowerLimit());
			param.addValue("ScreenName", ApplicationConstants.SETUPRETAILERLOCATIONSCREENNAME);
			param.addValue("RoleBasedUserID", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(param);

			Integer response = (Integer) resultFromProcedure.get("Status");
			if (null != response) {

				iRowcount = (Integer) resultFromProcedure.get("MaxCnt");
				categoryList = (ArrayList<EventCategory>) resultFromProcedure.get("eventcategoryList");
				if (null != categoryList && !categoryList.isEmpty()) {
					objAlertCategory = new AlertCategory();
					objAlertCategory.setAlertCatLst(categoryList);
					objAlertCategory.setTotalSize(iRowcount);

				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objAlertCategory;
	}

	/**
	 * This method is used delete event categories.
	 * 
	 * @param cateId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteEventCategory(int cateId) throws ScanSeeWebSqlException {
		String methodName = "deleteEventCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String strResponse = null;
		Integer iDeletedStatus = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiEventsCategoryDeletion");
			final MapSqlParameterSource addAlertCategoryParams = new MapSqlParameterSource();

			addAlertCategoryParams.addValue("HcEventCategoryID", cateId);

			resultFromProcedure = simpleJdbcCall.execute(addAlertCategoryParams);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc) {
				iDeletedStatus = responseFromProc;
				if (iDeletedStatus == 0) {
					strResponse = ApplicationConstants.SUCCESS;
				} else {
					strResponse = ApplicationConstants.ALERTCATEGORYASSCOIATED;
				}

			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This method is used to add event categories.
	 * 
	 * @param catName
	 * @param userId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String addEventCategory(EventCategory eventCategoryObj) throws ScanSeeWebSqlException {
		String methodName = "addEventCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		boolean bCatExists;
		String strResponse = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiEventsCategoryCreation");
			final MapSqlParameterSource addAlertCategoryParams = new MapSqlParameterSource();

			addAlertCategoryParams.addValue("ScanSeeAdminUserID", eventCategoryObj.getAdminId());
			addAlertCategoryParams.addValue("HcEventCategoryName", eventCategoryObj.getCatName());
			addAlertCategoryParams.addValue("CategoryImagePath", eventCategoryObj.getCateImgName());

			resultFromProcedure = simpleJdbcCall.execute(addAlertCategoryParams);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				bCatExists = (Boolean) resultFromProcedure.get("DuplicateFlag");
				if (bCatExists == true) {
					strResponse = ApplicationConstants.ALERTCATEXISTS;
				} else {
					strResponse = ApplicationConstants.SUCCESSTEXT;

				}

			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This method is used to update event category name to database.
	 * 
	 * @param category
	 * @param user
	 * @return
	 * @throws HubCitiWebSqlException
	 */
	public String updateEventCategory(EventCategory objCategory, String userId) throws ScanSeeWebSqlException {
		String methodName = "updateEventCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		boolean bCatExists;
		String strResponse = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiEventsCategoryUpdation");
			final MapSqlParameterSource addAlertCategoryParams = new MapSqlParameterSource();

			addAlertCategoryParams.addValue("ScanSeeAdminUserID", userId);
			addAlertCategoryParams.addValue("HcEventCategoryID", objCategory.getCatId());
			addAlertCategoryParams.addValue("HcEventCategoryName", objCategory.getCatName());
			addAlertCategoryParams.addValue("CategoryImagePath", objCategory.getCateImgName());

			resultFromProcedure = simpleJdbcCall.execute(addAlertCategoryParams);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				bCatExists = (Boolean) resultFromProcedure.get("DuplicateCategory");
				if (bCatExists == true) {
					strResponse = ApplicationConstants.ALERTCATEXISTS;
				} else {
					strResponse = ApplicationConstants.SUCCESS;
				}

			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * DAO method is used to display and search fundraiser categories.
	 * 
	 * @param category
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public AlertCategory fetchFundraiserCategories(EventCategory category, String userId) throws ScanSeeWebSqlException {
		final String methodName = "fetchFundraiserCategories";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer iRowcount = null;
		ArrayList<EventCategory> categoryList = null;
		AlertCategory objAlertCategory = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiFundraisingCategoryDisplay");
			simpleJdbcCall.returningResultSet("fundraiserCategoryList", new BeanPropertyRowMapper<EventCategory>(EventCategory.class));
			final MapSqlParameterSource param = new MapSqlParameterSource();
			param.addValue("UserID", userId);
			param.addValue("CategoryName", category.getCatName());
			param.addValue("FundraisingCategoryID", category.getCatId());
			param.addValue("LowerLimit", category.getLowerLimit());
			param.addValue("ScreenName", ApplicationConstants.SETUPRETAILERLOCATIONSCREENNAME);
			param.addValue("RoleBasedUserID", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(param);

			Integer response = (Integer) resultFromProcedure.get("Status");
			if (null != response) {

				iRowcount = (Integer) resultFromProcedure.get("MaxCnt");
				categoryList = (ArrayList<EventCategory>) resultFromProcedure.get("fundraiserCategoryList");
				if (null != categoryList && !categoryList.isEmpty()) {
					objAlertCategory = new AlertCategory();
					objAlertCategory.setAlertCatLst(categoryList);
					objAlertCategory.setTotalSize(iRowcount);
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objAlertCategory;
	}

	/**
	 * DAO method is used to delete fundraiser categories.
	 * 
	 * @param cateId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deleteFundraiserCategory(int cateId) throws ScanSeeWebSqlException {
		String methodName = "deleteFundraiserCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String strResponse = null;
		Integer iDeletedStatus = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiFundraisingCategoryDeletion");
			final MapSqlParameterSource addAlertCategoryParams = new MapSqlParameterSource();

			addAlertCategoryParams.addValue("HcFundraisingCategoryID", cateId);

			resultFromProcedure = simpleJdbcCall.execute(addAlertCategoryParams);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc) {
				iDeletedStatus = responseFromProc;
				if (iDeletedStatus == 0) {
					strResponse = ApplicationConstants.SUCCESS;
				} else {
					strResponse = ApplicationConstants.ALERTCATEGORYASSCOIATED;
				}

			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * DAO method is used to add/update fundraiser categories.
	 * 
	 * @param catName
	 * @param userId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String addFundraiserCategory(EventCategory eventCategoryObj) throws ScanSeeWebSqlException {
		String methodName = "addFundraiserCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		boolean bCatExists;
		String strResponse = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			if (eventCategoryObj.getCatId() != null && !"".equals(eventCategoryObj.getCatId())) {
				simpleJdbcCall.withProcedureName("usp_WebHcHubCitiFundraisingCategoryUpdation");
			} else {
				simpleJdbcCall.withProcedureName("usp_WebHcHubCitiFundraisingCategoryCreation");
			}
			final MapSqlParameterSource addAlertCategoryParams = new MapSqlParameterSource();

			addAlertCategoryParams.addValue("ScanSeeAdminUserID", eventCategoryObj.getAdminId());
			addAlertCategoryParams.addValue("HcFundraisingCategoryName", eventCategoryObj.getCatName());
			addAlertCategoryParams.addValue("CategoryImagePath", eventCategoryObj.getCateImgName());
			if (eventCategoryObj.getCatId() != null && !"".equals(eventCategoryObj.getCatId())) {
				addAlertCategoryParams.addValue("HcFundraisingCategoryID", eventCategoryObj.getCatId());
			}

			resultFromProcedure = simpleJdbcCall.execute(addAlertCategoryParams);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				bCatExists = (Boolean) resultFromProcedure.get("DuplicateFlag");
				if (bCatExists == true) {
					strResponse = ApplicationConstants.ALERTCATEXISTS;
				} else {
					strResponse = ApplicationConstants.SUCCESSTEXT;
				}
			} else {
				strResponse = ApplicationConstants.FAILURETEXT;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	@SuppressWarnings("unchecked")
	public RegionInfo displayRegions(HubCiti hubCiti) throws ScanSeeWebSqlException {
		final String strMethodName = "displayRegions";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		Map<String, Object> strResultFromProc = null;
		Integer strStatusCode = null;
		ArrayList<HubCiti> hubCitisLst = null;
		RegionInfo regionInfo = null;
		Integer iMaxCount = null;
		String response = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminHubCitiDisplay");
			simpleJdbcCall.returningResultSet("regionlst", new BeanPropertyRowMapper<HubCiti>(HubCiti.class));
			final MapSqlParameterSource regionDisplayParams = new MapSqlParameterSource();
			regionDisplayParams.addValue("ScanSeeAdminUserID", hubCiti.getUserID());
			regionDisplayParams.addValue("ShowDeactivated", hubCiti.getShowDeactivated());
			regionDisplayParams.addValue("SearchKey", hubCiti.getSearchKey());
			regionDisplayParams.addValue("LowerLimit", hubCiti.getLowerLimit());
			regionDisplayParams.addValue("AppRole", hubCiti.getAppRole());

			strResultFromProc = simpleJdbcCall.execute(regionDisplayParams);
			strStatusCode = (Integer) strResultFromProc.get(ApplicationConstants.STATUS);

			if (strStatusCode != null && strStatusCode == 0) {

				hubCitisLst = (ArrayList<HubCiti>) strResultFromProc.get("regionlst");

				iMaxCount = (Integer) strResultFromProc.get("MaxCnt");
				response = (String) strResultFromProc.get("FindClearCacheURL");
				if (null != hubCitisLst && !hubCitisLst.isEmpty()) {
					regionInfo = new RegionInfo();
					regionInfo.setRegionLst(hubCitisLst);
					regionInfo.setTotalSize(iMaxCount);
					regionInfo.setResponse(response);
				}

			} else {

				final Integer errorNumber = (Integer) strResultFromProc.get(ApplicationConstants.ERRORNUMBER);
				final String errorMessage = (String) strResultFromProc.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNumber + errorMessage);
				throw new ScanSeeWebSqlException(errorMessage);
			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeWebSqlException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return regionInfo;
	}

	public String deactivateRegion(HubCiti hubCiti) throws ScanSeeWebSqlException {
		final String strMethodName = "deactivateRegion";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strResponse = null;
		Map<String, Object> resultFrmProcedure = null;
		Integer iStatusCode = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSSAdminChangeHubCitiStatus");
			final MapSqlParameterSource deactivateRegionParams = new MapSqlParameterSource();
			deactivateRegionParams.addValue("ScanSeeAdminUserID", hubCiti.getUserID());
			deactivateRegionParams.addValue("HubCitiID", hubCiti.getHubCitiID());
			deactivateRegionParams.addValue("Deactivate", hubCiti.getActive());

			resultFrmProcedure = simpleJdbcCall.execute(deactivateRegionParams);

			if (null != resultFrmProcedure) {
				iStatusCode = (Integer) resultFrmProcedure.get(ApplicationConstants.STATUS);
				if (null != iStatusCode && iStatusCode == 0) {
					strResponse = ApplicationConstants.SUCCESSTEXT;
				} else {
					strResponse = ApplicationConstants.FAILURETEXT;
				}

			} else {

				Integer errorNum = (Integer) resultFrmProcedure.get(ApplicationConstants.ERRORNUMBER);
				String errorMessage = (String) resultFrmProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName + errorMessage);
				throw new ScanSeeWebSqlException(errorMessage);
			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strResponse;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<HubCiti> fetchHubCities() throws ScanSeeWebSqlException {
		final String strMethodName = "fetchHubCities";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ArrayList<HubCiti> hubList = null;
		Map<String, Object> resultFromProcedure = null;
		Integer iStatus = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiList");
			simpleJdbcCall.returningResultSet("hubCitilst", new BeanPropertyRowMapper<HubCiti>(HubCiti.class));
			MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(mapSqlParameterSource);
			iStatus = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != iStatus && iStatus == 0) {
				hubList = (ArrayList<HubCiti>) resultFromProcedure.get("hubCitilst");

			} else {

				Integer iErrorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				String strErrorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + iErrorNum + strErrorMsg);
				throw new ScanSeeWebSqlException(strErrorMsg);
			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return hubList;
	}

	/*
	 * Below method is used to fetch hubciti/regions for nation wide
	 * (non-Javadoc)
	 * 
	 * @see admin.dao.HubCitiDao#getHubCitiRegions()
	 */
	@SuppressWarnings("unchecked")
	public RegionInfo getHubCitiRegions(HubCiti hubCiti) throws ScanSeeWebSqlException {

		final String strMethodName = "getHubCitiRegions";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		ArrayList<HubCiti> hubCitiRegLst = null;
		RegionInfo regionInfo = null;
		Integer iStatus = null;
		Integer iMaxCount = null;
		Map<String, Object> resultFromProcedure = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcNationWideHubcitiListDisplay");
			simpleJdbcCall.returningResultSet("hubCitiRegLst", new BeanPropertyRowMapper<HubCiti>(HubCiti.class));

			MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
			mapSqlParameterSource.addValue("UserID", hubCiti.getUserID());

			if (null != hubCiti.getSearchKey() && !"".equals(hubCiti.getSearchKey())) {
				mapSqlParameterSource.addValue("SearchKey", hubCiti.getSearchKey());
			} else {
				mapSqlParameterSource.addValue("SearchKey", null);
			}

			mapSqlParameterSource.addValue("LowerLimit", hubCiti.getLowerLimit());
			mapSqlParameterSource.addValue("ScreenName", ApplicationConstants.SETUPRETAILERLOCATIONSCREENNAME);

			resultFromProcedure = simpleJdbcCall.execute(mapSqlParameterSource);
			if (null != resultFromProcedure) {
				iStatus = (Integer) resultFromProcedure.get("Status");
				if (null != iStatus && iStatus == 0) {

					hubCitiRegLst = (ArrayList<HubCiti>) resultFromProcedure.get("hubCitiRegLst");
					if (null != hubCitiRegLst && !hubCitiRegLst.isEmpty()) {
						regionInfo = new RegionInfo();
						iMaxCount = (Integer) resultFromProcedure.get("MaxCnt");
						regionInfo.setRegionLst(hubCitiRegLst);
						regionInfo.setTotalSize(iMaxCount);
					}

				} else {
					Integer iErrorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					String strErrorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + iErrorNum + strErrorMsg);
					throw new ScanSeeWebSqlException(strErrorMsg);

				}

			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return regionInfo;
	}

	public String saveNationWideDeals(HubCiti hubCiti) throws ScanSeeWebSqlException {

		final String strMethodName = "saveNationWideDeals";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		Map<String, Object> resultFromProcedure = null;
		Integer iStatus = null;
		String response = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcNationWideHubcitiListUpdation");
			simpleJdbcCall.returningResultSet("hubCitiRegLst", new BeanPropertyRowMapper<HubCiti>(HubCiti.class));

			MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
			mapSqlParameterSource.addValue("UserID", hubCiti.getUserID());
			mapSqlParameterSource.addValue("HubcitiID", hubCiti.getHubCitiID());
			mapSqlParameterSource.addValue("NationWideFlag", hubCiti.getIsNationwide());

			if (hubCiti.getIsNationwide()) {
				mapSqlParameterSource.addValue("APIPartnerIDs", hubCiti.getApiPartners());
			} else {
				mapSqlParameterSource.addValue("APIPartnerIDs", null);
			}

			/*
			 * mapSqlParameterSource.addValue("CheckedHubcitiIDs",
			 * hubCiti.getChkdNatId());
			 * mapSqlParameterSource.addValue("UnCheckedHubcitiIDs",
			 * hubCiti.getUnChkdNatId());
			 */

			resultFromProcedure = simpleJdbcCall.execute(mapSqlParameterSource);
			iStatus = (Integer) resultFromProcedure.get("Status");

			if (null != iStatus && iStatus == 0) {
				response = ApplicationConstants.SUCCESSTEXT;
			} else {
				response = ApplicationConstants.FAILURETEXT;
				Integer iErrorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				String strErrorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + iErrorNum + strErrorMsg);
				throw new ScanSeeWebSqlException(strErrorMsg);
			}

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return response;
	}

	/**
	 * This method is used to associate retailer locations to the given hubciti.
	 * 
	 * @param userId
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String AssociateBottomButtons(Integer userId, Integer hubCitiId) throws ScanSeeWebSqlException {
		final String methodName = "AssociateBottomButtons";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcCreateBottomButtons");
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			adminParams.addValue("UserID", userId);
			adminParams.addValue("HubCitiID", hubCitiId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			Integer response = (Integer) resultFromProcedure.get("Status");

			if (null != response && response == 0) {
				status = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getStackTrace());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * Below method is used to fetch default zip codes from Geo position..
	 */
	public List<HubCiti> getDefaultPostalCodes() throws ScanSeeWebSqlException {
		final String strMethodName = "";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		List<HubCiti> defaultZipcodeLst = null;
		try {

			defaultZipcodeLst = this.jdbcTemplate.query("select distinct PostalCode from GeoPosition", new RowMapper<HubCiti>() {

				public HubCiti mapRow(ResultSet set, int count) throws SQLException {
					HubCiti hubCiti = new HubCiti();
					hubCiti.setPostalCodes(set.getString(1));
					return hubCiti;
				}
			});

		} catch (Exception exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return defaultZipcodeLst;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<APIPartner> fetchNWAPIPartners() throws ScanSeeWebSqlException {
		final String methodName = "fetchNWAPIPartners";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<APIPartner> apiPartners = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HBCITIWEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcNationWideAPIPartnerDisplay");
			simpleJdbcCall.returningResultSet("apiPartners", new BeanPropertyRowMapper<APIPartner>(APIPartner.class));
			final MapSqlParameterSource adminParams = new MapSqlParameterSource();

			// adminParams.addValue("HubCitiID", hubCitiId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(adminParams);
			Integer response = (Integer) resultFromProcedure.get("Status");

			if (null != response && response == 0) {
				apiPartners = (ArrayList<APIPartner>) resultFromProcedure.get("apiPartners");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException dataAccessException) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + dataAccessException.getMessage());
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return apiPartners;
	}
}

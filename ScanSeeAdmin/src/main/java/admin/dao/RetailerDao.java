/**
 * 
 */
package admin.dao;

import java.util.ArrayList;
import java.util.List;

import common.exception.ScanSeeWebSqlException;
import common.pojo.AlertCategory;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.Retailer;
import common.pojo.RetailerDetails;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationDetails;
import common.pojo.State;

/**
 * @author sangeetha.ts
 */
public interface RetailerDao
{

	/**
	 * This method is used to display list of Retailers.
	 * 
	 * @param SearchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public RetailerDetails displayRetailerList(Retailer retailerInfo) throws ScanSeeWebSqlException;

	/**
	 * The DAOImpl method for displaying all the states
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL exception.
	 * @return states,List of states.
	 */
	public ArrayList<State> getAllStates() throws ScanSeeWebSqlException;

	/**
	 * this method used get All cities.
	 * 
	 * @param stateName
	 *            -For which cities to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public ArrayList<City> getAllCities(String stateName) throws ScanSeeWebSqlException;

	/**
	 * This method is used to delete the Retailers from the list of Retailers.
	 * 
	 * @param retailerIds
	 * @param userId
	 * @return success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteRetailer(String retailerIds, Integer userId,Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This method is used to display list of Retailer Locations.
	 * 
	 * @param SearchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeException
	 */
	public RetailerLocationDetails displayRetailerLocation(RetailerLocation retailerInfo) throws ScanSeeWebSqlException;

	/**
	 * This method is used to delete the Retailer Locations from the list of
	 * Retailer Locations.
	 * 
	 * @param retailerIds
	 * @param userId
	 * @return success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteRetailerLocation(String retailerLocationIds, Integer userId) throws ScanSeeWebSqlException;

	public String saveLocationList(List<RetailerLocation> locationList, long userId) throws ScanSeeWebSqlException;

	public String saveRetailerInfo(Retailer retailer) throws ScanSeeWebSqlException;
	

	/**
	 * This RetailerDaoImpl method used get All Business Category/Sub Category associated to Retailer/Location.
	 * 
	 * @param Retailer objRetailer
	 * @return List of Business Category.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeWebSqlException will be  thrown.
	 */
	public ArrayList<Category> fetchBusCategory(Retailer objRetailer) throws ScanSeeWebSqlException;
	
	/**
	 * This dao method used to update Business Category/Sub Category associated to Retailer/Location.
	 * 
	 * @param objRetailer
	 * @return success or failure
	 * @throws ScanSeeWebSqlException
	 * 	           If any exception occurs ScanSeeWebSqlException will be  thrown.
	 */
	public String updateBusCategory(Retailer objRetailer) throws ScanSeeWebSqlException;

	/**
	 * DAO method to get retailer login user name and password.
	 * @param objRetailer
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Retailer retailerLoginDetails(Retailer objRetailer) throws ScanSeeWebSqlException;
	
	/**
	 * DAO method to save/update retailer login details i.e. User name, email
	 * and auto generated password.
	 * @param objRetailer
	 * @param enryptPassword
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Retailer saveRetailerLoginDetails(Retailer objRetailer, String enryptPassword) throws ScanSeeWebSqlException;
	
	public String saveRetailerPaymentInfo(Integer retailId, Integer userID, Boolean isPaid,Boolean isBand) throws ScanSeeWebSqlException;
	
	/**
	 * The DAO method to get Cached API from the database.
	 * 
	 * @throws ScanSeeWebSqlException

	 * @return String object.
	 */
	String getClearCacheForFind() throws ScanSeeWebSqlException;

	public Category getBandcat(String retailId)throws ScanSeeWebSqlException;

	public String savebandcat(String retailId,String buscatIds,String subcatIds,String userID)throws ScanSeeWebSqlException;
	
	/**
	 * Below method is used to display retailer location level filters.
	 * @param retailer
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	AlertCategory getRetLocFilters(Retailer retailer)throws ScanSeeWebSqlException;
	
	public String associateFilters(RetailerLocation objRetailerLocation) throws ScanSeeWebSqlException;
}

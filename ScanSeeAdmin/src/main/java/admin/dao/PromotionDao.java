/**
 * 
 */
package admin.dao;

import common.pojo.Promotion;

import common.exception.ScanSeeWebSqlException;
import common.pojo.PromotionDetails;

/**
 * @author sangeetha.ts
 *
 */
public interface PromotionDao
{

	/**
	 * This method is used to search the Promotion details based on the search key.
	 * This method is also used to sort the columns. This method is also used to
	 * display the expired Promotions based on flag.
	 * @param searchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @param showExpire
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public PromotionDetails getPromotionList(String searchKey, String sortOrder, String columnName, Integer lowerLimit, Integer showExpire)
	throws ScanSeeWebSqlException;
	
	/**
	 * This method is used to display promotion details.
	 * @param promotionId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Promotion displayPromotionDetails(int promotionId) throws ScanSeeWebSqlException;
	
	/**
	 * This method is used to display users associated with the promotion. 
	 * @param promotionId
	 * @param lowerLimit
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public PromotionDetails displayPromotionUsers(int promotionId, int lowerLimit) throws ScanSeeWebSqlException;
	
	/**
	 * This method is used to filter the promotions from the list of promotions.
	 * 
	 * @param promotionIds
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String filterPromotions(String promotionIds) throws ScanSeeWebSqlException;
}

package admin.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Login;

public class LoginDaoImpl implements LoginDao
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(LoginDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This method is used for user login authentication
	 * @param userName
	 * @param password
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Login loginAuthentication(String userName, String password) throws ScanSeeWebSqlException
	{
		final String methodName = "loginAuthentication";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		Login loginInfo = null;
		List<Login> loginInfoLst = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);			
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebLogin");
			simpleJdbcCall.returningResultSet("userInfo", new BeanPropertyRowMapper<Login>(Login.class));
			final MapSqlParameterSource loginCredParams = new MapSqlParameterSource();
			loginCredParams.addValue("UserName", userName);
			loginCredParams.addValue("Password", password);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(loginCredParams);
			
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("Status"))
				{

					LOG.info("Login UnSuccessfull");
					loginInfo = new Login();
					loginInfo.setLoginSuccess(false);

				}
				else
				{
					LOG.info("Login Successfull");
					loginInfo = new Login();
					loginInfoLst = (ArrayList<Login>) resultFromProcedure.get("userInfo");
					if (loginInfoLst != null && !loginInfoLst.isEmpty())
					{
						loginInfo = loginInfoLst.get(0);
						loginInfo.setLoginSuccess(true);
					}

				}
			}
			LOG.info("usp_WebLogin executed Successfully.");
		}
		catch(DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return loginInfo;
	}

}

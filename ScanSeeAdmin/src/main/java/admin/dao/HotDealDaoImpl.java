/**
 * 
 */
package admin.dao;

import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Category;
import common.pojo.HotDeal;
import common.pojo.HotDealDetails;
import common.pojo.RetailerLocation;

/**
 * @author sangeetha.ts
 */
public class HotDealDaoImpl implements HotDealDao {

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(HotDealDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This method returns list of hot deals for the given search key.
	 * 
	 * @param deal
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public HotDealDetails displayHotDeals(HotDeal deal) throws ScanSeeWebSqlException {
		final String methodName = "displayHotDeals";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HotDealDetails hotDealDetails = null;
		ArrayList<HotDeal> hotDeals = null;
		Integer rowcount = null;
		Boolean nextpage = null;
		Integer page = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminHotDealList");
			simpleJdbcCall.returningResultSet("hotdeals", new BeanPropertyRowMapper<HotDeal>(HotDeal.class));
			final MapSqlParameterSource dealParams = new MapSqlParameterSource();

			dealParams.addValue("SearchKey", deal.getSearchKey());
			dealParams.addValue("LowerLimit", deal.getLowerLimit());

			if ("true".equals(deal.getShowExpired())) {
				dealParams.addValue("ShowExpired", 0);
			} else {
				dealParams.addValue("ShowExpired", 1);
			}
			dealParams.addValue("ColumnName", deal.getColumnName());
			dealParams.addValue("SortOrder", deal.getSortOrder());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(dealParams);

			if (null != resultFromProcedure) {
				hotDeals = new ArrayList<HotDeal>();
				hotDealDetails = new HotDealDetails();

				hotDeals = (ArrayList<HotDeal>) resultFromProcedure.get("hotdeals");

				nextpage = (Boolean) resultFromProcedure.get("NextPageFlag");

				if (nextpage == true) {
					page = 1;
				} else {
					page = 0;
				}

				rowcount = (Integer) resultFromProcedure.get("RowCount");

				hotDealDetails.setHotDeals(hotDeals);
				hotDealDetails.setNextpage(page);
				hotDealDetails.setTotalSize(rowcount);

			}

		} catch (DataAccessException dataAccessException) {
			throw new ScanSeeWebSqlException(dataAccessException);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDealDetails;
	}

	public String deleteHotDeal(String hotDealIds) throws ScanSeeWebSqlException {
		final String methodName = "deleteHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Integer status = null;
		String response = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminHotDealDeletion");
			final MapSqlParameterSource promotionParams = new MapSqlParameterSource();
			promotionParams.addValue("ProductHotDealID", hotDealIds);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(promotionParams);

			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				response = "Success";
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public HotDeal displayHotDealDetails(int hotdealId) throws ScanSeeWebSqlException {
		final String methodName = "displayHotDealDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<HotDeal> hotDealDetailList = null;
		HotDeal hotDeal = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminHotDealDetails");
			simpleJdbcCall.returningResultSet("hotDealDetails", new BeanPropertyRowMapper<HotDeal>(HotDeal.class));
			final MapSqlParameterSource hotDealParams = new MapSqlParameterSource();
			hotDealParams.addValue("ProductHotDealID", hotdealId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(hotDealParams);

			if (null != resultFromProcedure) {
				hotDealDetailList = new ArrayList<HotDeal>();
				hotDealDetailList = (ArrayList<HotDeal>) resultFromProcedure.get("hotDealDetails");
				hotDeal = new HotDeal();
				hotDeal = hotDealDetailList.get(0);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDeal;
	}

	public ArrayList<Category> displayHotDealCategories() throws ScanSeeWebSqlException {
		final String methodName = "displayHotDealCategories";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<Category> hotDealCategoryList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminHotDealCategoryDisplay");
			simpleJdbcCall.returningResultSet("hotDealCategories", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource hotDealParams = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(hotDealParams);

			if (null != resultFromProcedure) {
				hotDealCategoryList = new ArrayList<Category>();
				hotDealCategoryList = (ArrayList<Category>) resultFromProcedure.get("hotDealCategories");
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDealCategoryList;
	}

	/**
	 * This method is used to display Retailer Locations.
	 * 
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<RetailerLocation> displayRetailerLocations(Integer retailId, String city, String state) throws ScanSeeWebSqlException {
		final String methodName = "displayRetailerLocations";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<RetailerLocation> retailerLocations = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminHotDealRetailLocationDisplay");
			simpleJdbcCall.returningResultSet("retailerLocations", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			final MapSqlParameterSource hotDealParams = new MapSqlParameterSource();
			hotDealParams.addValue("RetailID", retailId);
			hotDealParams.addValue("City", city);
			hotDealParams.addValue("State", state);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(hotDealParams);

			if (null != resultFromProcedure) {
				retailerLocations = new ArrayList<RetailerLocation>();
				retailerLocations = (ArrayList<RetailerLocation>) resultFromProcedure.get("retailerLocations");
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerLocations;
	}

	public String updateHotDeal(HotDeal hotDeal) throws ScanSeeWebSqlException {
		final String methodName = "saveCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAdminHotDealModification");

			final MapSqlParameterSource dealParams = new MapSqlParameterSource();

			dealParams.addValue("ProductHotDealID", hotDeal.getProductHotDealID());
			dealParams.addValue("HotDealName", hotDeal.getHotDealName());
			dealParams.addValue("RegularPrice", hotDeal.getPrice());
			dealParams.addValue("SalePrice", hotDeal.getSalePrice());
			dealParams.addValue("HotDealShortDescription", hotDeal.getHotDealShortDescription());
			dealParams.addValue("HotDealLongDescription", hotDeal.getHotDeaLonglDescription());
			dealParams.addValue("DealStartDate", hotDeal.getHotDealStartDate());
			dealParams.addValue("DealStartTime", hotDeal.getDealStartTime());
			dealParams.addValue("DealEndDate", hotDeal.getHotDealEndDate());
			dealParams.addValue("DealEndTime", hotDeal.getDealEndTime());
			dealParams.addValue("HotDealURL", hotDeal.getHotDealURL());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(dealParams);

			Integer status = (Integer) resultFromProcedure.get("Status");

			if (null != status) {
				if (status == 0) {
					response = "Success";
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		return response;
	}

}

package admin.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Coupon;
import common.pojo.CouponDetails;
import common.pojo.Product;
import common.util.Utility;

public class CouponDaoImpl implements CouponDao {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(CouponDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This method is used to search the coupon details based on the search key.
	 * This method is also used to sort the columns. This method is also used to
	 * display the expired coupons based on flag.
	 * 
	 * @param searchKey
	 * @param sortOrder
	 * @param coloumnName
	 * @param lowerLimit
	 * @param showExpire
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public CouponDetails getSearchCouponDetail(String searchKey, String sortOrder, String coloumnName, Integer lowerLimit, Integer showExpire)
			throws ScanSeeWebSqlException {
		final String methodName = "getSearchCouponDetail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		CouponDetails couponDetails = null;
		ArrayList<Coupon> couponDetailList = null;
		Integer rowcount = null;
		Boolean nextpage = null;
		Integer page = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebCouponSearch");
			simpleJdbcCall.returningResultSet("couponList", new BeanPropertyRowMapper<Coupon>(Coupon.class));
			final MapSqlParameterSource couponParams = new MapSqlParameterSource();
			couponParams.addValue("SearchParameter", searchKey);
			couponParams.addValue("ColumnName", coloumnName);
			couponParams.addValue("SortOrder", sortOrder);
			couponParams.addValue("LowerLimit", lowerLimit);
			couponParams.addValue("ShowExpired", showExpire);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(couponParams);

			if (null != resultFromProcedure) {
				couponDetailList = new ArrayList<Coupon>();
				couponDetailList = (ArrayList<Coupon>) resultFromProcedure.get("couponList");
				couponDetails = new CouponDetails();
				nextpage = (Boolean) resultFromProcedure.get("NextPageFlag");

				if (nextpage == true) {
					page = 1;
				} else {
					page = 0;
				}

				couponDetails.setNextPage(page);
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				couponDetails.setTotalSize(rowcount);
				couponDetails.setCouponDetailList(couponDetailList);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return couponDetails;
	}

	/**
	 * This method is used for product search based on the product name.
	 * 
	 * @param searchKey
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public List<Product> getSearchProduct(String searchKey) throws ScanSeeWebSqlException {
		final String methodName = "getSearchProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Product> productList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebProductSearch");
			simpleJdbcCall.returningResultSet("productList", new BeanPropertyRowMapper<Product>(Product.class));
			final MapSqlParameterSource productParams = new MapSqlParameterSource();
			productParams.addValue("SearchKey", searchKey);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productParams);

			if (null != resultFromProcedure) {
				productList = new ArrayList<Product>();
				productList = (ArrayList<Product>) resultFromProcedure.get("productList");
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productList;
	}

	/**
	 * This method is used to fetch the list of products associated to the
	 * coupon form database.
	 * 
	 * @param productIds
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public List<Product> getAssociatedProducts(String productIds) throws ScanSeeWebSqlException {
		final String methodName = "getAssociatedProducts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Product> productList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierReRunHotDealDisplayProductInfo");
			simpleJdbcCall.returningResultSet("productList", new BeanPropertyRowMapper<Product>(Product.class));
			final MapSqlParameterSource productParams = new MapSqlParameterSource();
			productParams.addValue("ProductID", productIds);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productParams);

			if (null != resultFromProcedure) {
				productList = new ArrayList<Product>();
				productList = (ArrayList<Product>) resultFromProcedure.get("productList");
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productList;
	}

	/**
	 * This method is used to save the coupon details.
	 * 
	 * @param coupon
	 * @param couponAmount
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String saveCoupon(Coupon coupon, Double couponAmount) throws ScanSeeWebSqlException {
		final String methodName = "saveCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		boolean isExternalCoupon = false;
		boolean isViewableOnWeb = false;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebCouponInsertion");

			if (coupon.getExternalCoupon() != null && "1".equals(coupon.getHiddenExternalCoupon())) {
				isExternalCoupon = true;
			}
			if (coupon.getHiddenViewable() != null && "1".equals(coupon.getHiddenViewable())) {
				isViewableOnWeb = true;
			}

			final MapSqlParameterSource couponParams = new MapSqlParameterSource();
			couponParams.addValue("UserID", coupon.getUserId());
			couponParams.addValue("CouponName", coupon.getCouponName());
			couponParams.addValue("CouponDiscountType", coupon.getCouponDiscountType());
			couponParams.addValue("CouponDiscountAmount", couponAmount);
			couponParams.addValue("CouponDiscountPct", coupon.getCouponDiscountPct());
			couponParams.addValue("CouponShortDescription", coupon.getCouponShortDescription());
			couponParams.addValue("CouponLongDescription", coupon.getCouponShortDescription());
			couponParams.addValue("CouponStartDate", Utility.getFormattedDateTime(coupon.getCouponStartDate()));
			couponParams.addValue("CouponExpireDate", Utility.getFormattedDateTime(coupon.getCouponExpireDate()));
			couponParams.addValue("CouponURL", coupon.getCouponUrl());
			couponParams.addValue("ExternalCoupon", isExternalCoupon);
			couponParams.addValue("ViewableOnWeb", isViewableOnWeb);
			if (!"".equals(coupon.getProductIDHidden())) {
				couponParams.addValue("ProductIds", coupon.getProductIDHidden());
			} else {
				couponParams.addValue("ProductIds", null);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(couponParams);

			Integer status = (Integer) resultFromProcedure.get("Status");

			if (null != status) {
				if (status == 0) {
					response = "Success";
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to get the coupon details.
	 * 
	 * @param couponId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public Coupon getCouponDetails(Integer couponId) throws ScanSeeWebSqlException {
		final String methodName = "getCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Coupon coupon = null;

		ArrayList<Coupon> coupons = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebFetchCouponAssociatedProducts");
			simpleJdbcCall.returningResultSet("couponDetails", new BeanPropertyRowMapper<Coupon>(Coupon.class));
			final MapSqlParameterSource couponParams = new MapSqlParameterSource();
			couponParams.addValue("CouponID", couponId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(couponParams);

			if (null != resultFromProcedure) {
				coupons = new ArrayList<Coupon>();
				coupons = (ArrayList<Coupon>) resultFromProcedure.get("couponDetails");
				coupon = new Coupon();
				coupon = coupons.get(0);
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return coupon;
	}

	/**
	 * This method is used to save the edit coupon.
	 * 
	 * @param coupon
	 * @param couponAmount
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String updateCoupon(Coupon coupon, Double couponAmount) throws ScanSeeWebSqlException {
		final String methodName = "updateCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		boolean isExternalCoupon = false;
		boolean isViewableOnWeb = false;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebCouponUpdation");

			if (coupon.getExternalCoupon() != null && "1".equals(coupon.getHiddenExternalCoupon())) {
				isExternalCoupon = true;
			}
			if (coupon.getHiddenViewable() != null && "1".equals(coupon.getHiddenViewable())) {
				isViewableOnWeb = true;
			}

			final MapSqlParameterSource couponParams = new MapSqlParameterSource();
			couponParams.addValue("CouponID", coupon.getCouponId());
			couponParams.addValue("UserID", coupon.getUserId());
			couponParams.addValue("CouponName", coupon.getCouponName());
			couponParams.addValue("CouponDiscountType", coupon.getCouponDiscountType());
			couponParams.addValue("CouponDiscountAmount", couponAmount);
			couponParams.addValue("CouponDiscountPct", coupon.getCouponDiscountPct());
			couponParams.addValue("CouponShortDescription", coupon.getCouponShortDescription());
			couponParams.addValue("CouponLongDescription", coupon.getCouponShortDescription());
			couponParams.addValue("CouponStartDate", Utility.getFormattedDateTime(coupon.getCouponStartDate()));
			couponParams.addValue("CouponExpireDate", Utility.getFormattedDateTime(coupon.getCouponExpireDate()));
			couponParams.addValue("CouponURL", coupon.getCouponUrl());
			couponParams.addValue("ExternalCoupon", isExternalCoupon);
			couponParams.addValue("ViewableOnWeb", isViewableOnWeb);
			if (!"".equals(coupon.getProductIDHidden())) {
				couponParams.addValue("ProductIds", coupon.getProductIDHidden());
			} else {
				couponParams.addValue("ProductIds", null);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(couponParams);

			Integer status = (Integer) resultFromProcedure.get("Status");

			if (null != status) {
				if (status == 0) {
					response = "Success";
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used delete the coupon.
	 * 
	 * @param couponId
	 * @param type
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteCoupon(Integer couponId, String type) throws ScanSeeWebSqlException {

		final String methodName = "deleteCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebCLRDeletion");

			final MapSqlParameterSource couponParams = new MapSqlParameterSource();

			if ("Coupon".equalsIgnoreCase(type)) {
				couponParams.addValue("CouponID", couponId);
				couponParams.addValue("LoyaltyID", 0);
				couponParams.addValue("RebateID", 0);
			}
			if ("Rebate".equalsIgnoreCase(type)) {
				couponParams.addValue("CouponID", 0);
				couponParams.addValue("LoyaltyID", couponId);
				couponParams.addValue("RebateID", 0);
			}
			if ("Loyalty".equalsIgnoreCase(type)) {
				couponParams.addValue("CouponID", 0);
				couponParams.addValue("LoyaltyID", 0);
				couponParams.addValue("RebateID", couponId);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(couponParams);

			Integer status = (Integer) resultFromProcedure.get("Status");

			if (null != status) {
				if (status == 0) {
					response = "Success";
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + errorNum + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}

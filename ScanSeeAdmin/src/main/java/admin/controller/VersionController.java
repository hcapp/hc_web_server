/**
 * 
 */
package admin.controller;

import java.text.ParseException;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import admin.service.VersionService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerDetails;
import common.pojo.Version;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is used as a controller class for Version Management.
 * 
 * @author Kumar
 */
@Controller
public class VersionController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(VersionController.class);

	@RequestMapping(value = "/addversion.htm", method = RequestMethod.GET)
	public ModelAndView showAddVersionPage(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws ScanSeeServiceException
	{
		String strViewName = "addversion";
		String strMethodName = "showAddVersionPage";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		final Version objVersion = new Version();
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		model.put("Version", objVersion);
		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "/addversion.htm", method = RequestMethod.POST)
	public ModelAndView addVersionDetails(@ModelAttribute("Version") Version version, BindingResult results, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, MessagingException
	{
		String strViewName = "addversion";
		String strMethodName = "addVersionDetails";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final VersionService objVersionService = (VersionService) appContext.getBean(ApplicationConstants.VERSIONSERVICES);
		RetailerDetails retailerDetails = null;
		String strResponse = null;
		Pagination objPage = null;
		final int recordCount = 20;
		Integer lowerLimit = 0;
		int currentPage = 1;
		// final MultipartFile fileBannerAd = version.getReleaseNotePath();
		try
		{
			strResponse = objVersionService.addVersionDetails(version);
			if (strResponse.equals(ApplicationConstants.SUCCESSTEXT))
			{
				version.setLowerLimit(lowerLimit);
				version.setRecordCount(recordCount);
				retailerDetails = objVersionService.getAllVersions(version);
				if (retailerDetails != null && retailerDetails.getVersionList().size() <= 0)
				{
					request.setAttribute("errorMessage", "No version to display.");
				}
				else
				{
					session.setAttribute("versionList", retailerDetails.getVersionList());
				}
				objPage = Utility.getPagination(retailerDetails.getTotalSize(), currentPage, "displayversion.htm", recordCount);
				session.setAttribute("pagination", objPage);
				request.setAttribute("errorMessage", ApplicationConstants.ADDEDVESIONTEXT);
				final Version objVersion = new Version();
				model.put("manageversionform", objVersion);
				strViewName = "manageVersions";
			}
			else if (strResponse.equals(ApplicationConstants.VERSIONEXIST))
			{
				request.setAttribute("errorMessage", ApplicationConstants.VERSIONEXISTSTEXT);
			}
			else if (strResponse.equals(ApplicationConstants.EMPTYFILETEXT))
			{
				request.setAttribute("errorMessage", ApplicationConstants.EMPYFILEVALIDATIONTEXT);
			}
			else
			{
				request.setAttribute("errorMessage", ApplicationConstants.ADDEDVESIONERRORTEXT);
			}
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error("Inside VersionController : displayAllVersion : ", exception.getMessage());
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	/**
	 * This controller method is used to display all the version with
	 * pagination.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @param result
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/displayversion.htm", method = RequestMethod.POST)
	public ModelAndView displayAllVersion(@ModelAttribute("manageversionform") Version objVersion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside VersionController : displayAllVersion");
		final String pageFlag = request.getParameter("pageFlag");
		RetailerDetails retailerDetails = null;
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		session.removeAttribute("versionList");

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final VersionService versionService = (VersionService) appContext.getBean(ApplicationConstants.VERSIONSERVICES);
			session.setAttribute("moduleName", ApplicationConstants.VERSIONMANAGEMENT);
			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}
			objVersion.setLowerLimit(lowerLimit);
			objVersion.setRecordCount(recordCount);
			retailerDetails = versionService.getAllVersions(objVersion);
			if (retailerDetails != null && retailerDetails.getVersionList().size() <= 0)
			{
				request.setAttribute("errorMessage", "No version to display.");
			}
			else
			{
				session.setAttribute("versionList", retailerDetails.getVersionList());
			}
			objPage = Utility.getPagination(retailerDetails.getTotalSize(), currentPage, "displayversion.htm", recordCount);
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside VersionController : displayAllVersion : ", e.getMessage());
			throw e;
		}
		model.put("versionmanagementform", objVersion);
		return new ModelAndView("manageVersions");
	}

	/**
	 * This controller method is used to edit the version details based on
	 * versioID as input parameter.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @param result
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/editversion", method = RequestMethod.POST)
	public final ModelAndView editVersion(@ModelAttribute("editVersion") Version objVersion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside VersionController : editVersion");
		Version version = null;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final VersionService versionService = (VersionService) appContext.getBean(ApplicationConstants.VERSIONSERVICES);
			version = new Version();
			version = versionService.getVersionDetailsByID(objVersion.getVersionId(), session);
			session.setAttribute("AppType", version.getAppType());
			session.setAttribute("ReleaseDate", version.getReleaseDate());
			if (version.getRelTypeId() == 1)
			{
				objVersion.setReleaseType("QA");
				request.setAttribute("ReleaseFileName", version.getQAFileName());
			}
			else if (version.getRelTypeId() == 2)
			{
				request.setAttribute("ReleaseFileName", version.getProdFileName());
				objVersion.setReleaseType("Production");
			}
			else
			{
				objVersion.setReleaseType("iTunes");
			}

			session.setAttribute("ReleaseType", objVersion.getReleaseType());
			if (null != version)
			{
				model.put("editVersion", version);
				request.setAttribute("vRelTypeID", version.getRelTypeId());
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside VersionController : editVersion : ", e.getMessage());
			throw e;
		}
		return new ModelAndView("editversion");
	}

	/**
	 * This controller method is used to edit the version details based on
	 * versioID as input parameter.
	 * 
	 * @param objVersion
	 *            instance of Version.
	 * @param result
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/updateversion", method = RequestMethod.POST)
	public final ModelAndView updateVersion(@ModelAttribute("editVersion") Version objVersion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException, MessagingException, ParseException

	{
		LOG.info("Inside VersionController : updateVersion");
		String strStatus = null;
		String strAppTyp = null;
		String strReleaseDate = null;
		String strReleaseType = null;
		RetailerDetails retailerDetails = null;
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		Integer lowerLimit = 0;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final VersionService versionService = (VersionService) appContext.getBean(ApplicationConstants.VERSIONSERVICES);
			strAppTyp = (String) session.getAttribute("AppType");
			strReleaseDate = (String) session.getAttribute("ReleaseDate");
			strReleaseType = (String) session.getAttribute("ReleaseType");
			final MultipartFile fileBannerAd = objVersion.getReleaseNotePath();
			objVersion.setAppType(null);
			objVersion.setReleaseDate(null);
			objVersion.setAppType(strAppTyp);
			if (strReleaseType.equals(objVersion.getReleaseType()))
			{
				objVersion.setReleaseDate(strReleaseDate);
			}
			else
			{
				objVersion.setReleaseDate(Utility.getTodayDate());
			}
			if (!objVersion.getAppType().equals("App"))
			{
				objVersion.setReleaseNotePath(objVersion.getReleaseNotePathW());
			}
			strStatus = versionService.updateVersion(objVersion, session);
			if ("Success".equalsIgnoreCase(strStatus))
			{
				objVersion.setLowerLimit(lowerLimit);
				objVersion.setRecordCount(recordCount);
				retailerDetails = versionService.getAllVersions(objVersion);
				if (retailerDetails != null && retailerDetails.getVersionList().size() <= 0)
				{
					request.setAttribute("errorMessage", "No version to display.");
				}
				else
				{
					session.setAttribute("versionList", retailerDetails.getVersionList());
				}
				objPage = Utility.getPagination(retailerDetails.getTotalSize(), currentPage, "displayversion.htm", recordCount);
				session.setAttribute("pagination", objPage);
				model.put("manageversionform", objVersion);
				request.setAttribute("errorMessage", "Version Updated Successfully.");
			}
			else
			{
				request.setAttribute("errorMessage", "Error Updating Version.");
				return new ModelAndView("editversion");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside VersionController : updateVersion : ", e.getMessage());
			throw e;
		}
		return new ModelAndView("manageVersions");
	}
}

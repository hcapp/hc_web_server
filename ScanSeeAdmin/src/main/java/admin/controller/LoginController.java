package admin.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import admin.service.CouponService;
import admin.service.LoginService;
import admin.validator.LoginValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Coupon;
import common.pojo.CouponDetails;
import common.pojo.Login;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is used as controller class for user login.
 * 
 * @author sangeetha.ts
 */
@Controller
public class LoginController {

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);
	/**
	 * Variable loginValidator declared as instance of LoginValidator.
	 */
	private LoginValidator loginValidator;

	/**
	 * To loginValidator to set.
	 * 
	 * @param loginvalidator
	 *            to set.
	 */
	@Autowired
	public final void setLoginvalidator(LoginValidator loginvalidator) {
		this.loginValidator = loginvalidator;
	}

	/**
	 * This controller method will display the Login users screen.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/login.htm", method = RequestMethod.GET)
	public final ModelAndView showLoginPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "showLoginPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Login login = new Login();
		model.put("loginform", login);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("Login");
	}

	/**
	 * This Method will validate the login user if the user is a valid user then
	 * it will display coupon screen.
	 * 
	 * @param login
	 *            login instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */

	@RequestMapping(value = "/loginsuccess.htm", method = RequestMethod.GET)
	public final ModelAndView login(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws ScanSeeServiceException {

		final String methodName = "login";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		// final String userName = login.getUserName();
		String view = "Login";
		Login loginResponse = null;
		CouponDetails couponDetails = null;
		final String searchKey = null;
		final String sortOrder = "ASC";
		final String columnName = "CouponName";
		final Integer lowerLimit = 0;
		final String showExpire = "true";
		final int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		Coupon coupon = null;
		try {
			session.removeAttribute("RoleAdmin");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final LoginService loginService = (LoginService) appContext.getBean(ApplicationConstants.LOGINSERVICE);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);
			coupon = new Coupon();
			String authority = null;
			couponDetails = new CouponDetails();
			loginResponse = new Login();
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String userName = user.getUsername();
			String password = user.getPassword();
			Set<GrantedAuthority> grantedAuthorities = (Set<GrantedAuthority>) user.getAuthorities();

			if (null != grantedAuthorities) {

				for (GrantedAuthority grantedAuthority : grantedAuthorities) {
					authority = grantedAuthority.getAuthority();

					if (null != authority && !"".equals(authority)) {
						if (authority.equals(ApplicationConstants.ROLE_RETAIL_ADMIN)) {
							session.setAttribute("RoleAdmin", authority);
							break;
						} else {
							session.setAttribute("RoleAdmin", authority);
						}
					}

				}

				loginResponse = loginService.loginAuthentication(userName, password);
				if (null != loginResponse) {
					session.setAttribute("UserId", String.valueOf(loginResponse.getUserId()));
				}
				session.setAttribute("userName", userName);

				if (authority.equals(ApplicationConstants.ROLE_RETAIL_ADMIN)) {
					LOG.info("ADMIN ROLE IS" + authority);
					return new ModelAndView(new RedirectView("/ScanSeeAdmin/displayretailer.htm"));

				} else {
					LOG.info("ADMIN ROLE IS" + authority);
					view = "adddisplaycoupon";
					session.setAttribute("moduleName", ApplicationConstants.COUPONADMIN);
					couponDetails = couponService.getSearchCouponDetail(searchKey, sortOrder, columnName, lowerLimit, showExpire);
					session.removeAttribute("searchKey");
					session.setAttribute("columnName", "CouponName");
					session.setAttribute("COUPONLIST", couponDetails.getCouponDetailList());
					session.setAttribute("showExpire", "true");
					session.setAttribute("CouponNameImg", "images/sorticonUp.png");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("CouponExpireDateImg", "");
					session.setAttribute("sortOrderCouponName", "DESC");
					session.setAttribute("sortOrderDiscAmnt", "ASC");
					session.setAttribute("sortOrderSTDT", "ASC");
					session.setAttribute("sortOrderENDT", "ASC");
					objPage = Utility.getPagination(couponDetails.getTotalSize(), currentPage, "displaycoupon.htm", recordCount);
					session.setAttribute("pagination", objPage);
					model.put("coupon", coupon);
				}
			}
		} catch (ScanSeeServiceException exception) {
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(view);
	}

	/**
	 * This method is for user logout.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/logout.htm", method = RequestMethod.GET)
	public final ModelAndView logout(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "logout";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Login login = new Login();
		model.put("loginform", login);
		session.invalidate();

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("Login");
	}

	/**
	 * This method will invoke when the session is expired.
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 */
	@RequestMapping(value = "/sessionTimeout.htm", method = RequestMethod.GET)
	public final String SessionTimeOut(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) {
		final String methodName = "SessionTimeOut";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "sessionTimeout";
	}

	/**
	 * This method will invoke when the login error occur.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/loginfailed.htm", method = RequestMethod.GET)
	public String loginerror(ModelMap model) {

		try {
			model.addAttribute("error", "true");
			return "Login";
		} catch (IllegalArgumentException e) {
			LOG.info("Exception occured" + e.getMessage());
			return "Login";
		}

	}

	@RequestMapping(value = "/loginsudccess.htm", method = RequestMethod.GET)
	public ModelAndView loginSuccess(BindingResult result, HttpServletRequest request, ModelMap model, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "loginSuccess";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Login login = new Login();
		final String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

		/*
		 * loginValidator.validate(login, result); if (result.hasErrors()) {
		 * return new ModelAndView(view); }
		 */

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();
		String strAuthorities = user.getAuthorities().toString();
		User user2 = (User) SecurityContextHolder.getContext().getAuthentication().getAuthorities();

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(new RedirectView("welcome.htm"));
	}

}

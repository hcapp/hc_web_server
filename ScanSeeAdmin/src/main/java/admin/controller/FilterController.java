/**
 * @author Kumar D
 * @version 0.1
 *
 * Class to map the filter details of a FilterController.
 */
package admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import admin.service.FilterService;
import admin.validator.FilterValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.Filter;
import common.pojo.FilterInfo;
import common.pojo.FilterValues;
import common.tags.Pagination;
import common.util.Utility;

@Controller
public class FilterController {

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(FilterController.class);

	/**
	 * Variable filterValidator declared as instance of
	 * FilterValidator.
	 */
	private FilterValidator filterValidator;

	/**
	 * @param filterValidator the filterValidator to set
	 */
	@Autowired
	public void setFilterValidator(FilterValidator filterValidator) {
		this.filterValidator = filterValidator;
	}

	
	/**
	 * This controller method will display  all filter by call service
	 * layer.
	 * 
	 * @param filterReq instance of Filter.
	 * @param request as request parameter.
	 * @param session  as request parameter.
	 * @param response as request parameter.
	 * @param model as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/managefilters.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView displayFilter(@ModelAttribute("filterform") Filter filterReq, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside FilterController : displayFilter");

		final String pageFlag = request.getParameter("pageFlag");
		FilterInfo filterInfo = null;
		Filter filterResp = null;

		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		session.removeAttribute("filterList");
		
		if (!"".equals(Utility.checkNull(filterReq.getRemoveMsg()))) {
			session.removeAttribute("filterFont");
			session.removeAttribute("responseFilterMsg");
		}

		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final FilterService filterService = (FilterService) appContext.getBean(ApplicationConstants.FILTERSERVICE);

			final String userID = (String) session.getAttribute("UserId");

			session.setAttribute("moduleName", ApplicationConstants.FILTERADMIN);
			final Integer currentPageNum = (Integer) session.getAttribute("currentPageNum");
			session.removeAttribute("currentPageNum");

			if (null != currentPageNum) {
				currentPage = currentPageNum;
			}

			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");

				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			filterReq.setRecordCount(String.valueOf(recordCount));
			filterReq.setLowerLimit(lowerLimit);
			filterReq.setUserId(Integer.parseInt(userID));
			filterInfo = filterService.getAllFilters(filterReq);

			if (null != filterInfo  &&  filterInfo.getFilterList().size() <= 0  &&  "".equals(Utility.checkNull(filterReq.getRemoveMsg()))) {
				/*  Back button to hide in add Filter screen if manage filter list is empty. */
				session.setAttribute("backButton", "no");
				return new ModelAndView(new RedirectView("/ScanSeeAdmin/addfilter.htm"));

			} else if (null != filterInfo.getFilterList() && !filterInfo.getFilterList().isEmpty()) {
				session.setAttribute("filterList", filterInfo.getFilterList());
				objPage = Utility.getPagination(filterInfo.getTotalSize(), currentPage, "managefilters.htm", recordCount);
				session.setAttribute("pagination", objPage);

				// for Removing "no filter found" message.
				final String strNoRecords = (String)session.getAttribute("responseFilterMsg");

				if (!"".equals(Utility.checkNull(strNoRecords)) && strNoRecords.equals("No Filter found!")){
					session.removeAttribute("filterFont");
					session.removeAttribute("responseFilterMsg");
				}

			}

			if (filterInfo.getTotalSize() > 0 || !"".equals(Utility.checkNull(filterReq.getRemoveMsg()))) {
				if (filterInfo.getTotalSize() == 0) {
					session.setAttribute("filterFont", "font-weight:bold;color:red;");
					session.setAttribute("responseFilterMsg", "No Filter found!");
				}
			}

			filterResp = new Filter();
			filterResp.setLowerLimit(lowerLimit);

			session.removeAttribute("backButton");
			model.put("filterForm", filterResp);

		} catch (ScanSeeServiceException exception) {
			LOG.error("Inside FilterController : displayFilter : " + exception);
			throw new ScanSeeServiceException();
		}

		LOG.info("Exit FilterController : displayFilter");
		return new ModelAndView("managefilters");
	}


	/**
	 * This controller method is used to Add Filter value.
	 * 
	 * @param filterValue as input parameter.
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */

	@RequestMapping(value = "/addfvalue.htm", method = RequestMethod.GET)
	public @ResponseBody
	String addFilterValue(@RequestParam(value = "filterValue", required = true) String filterValue,
			 HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside FilterController : addFilterValue");

		String strResponse = "";

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FilterService filterService = (FilterService) appContext.getBean(ApplicationConstants.FILTERSERVICE);
		try {
			
			@SuppressWarnings("unchecked")
			List<FilterValues> arFValuesList = (ArrayList<FilterValues>) session.getAttribute("filterValuesList");
			
			final String userID = (String) session.getAttribute("UserId");
			
			final FilterValues fValueReq = new FilterValues();
				fValueReq.setfValueName(filterValue);
				fValueReq.setUserId(Integer.parseInt(userID));
				fValueReq.setScreenName("addFValue");
			
			strResponse = filterService.addUpdateFilterValue(fValueReq);


			if (null != strResponse && !strResponse.equals(ApplicationConstants.FILTERVALEXISTS)) {

				FilterValues objFValues = new FilterValues();
				objFValues.setfValueId(Integer.parseInt(strResponse));
				objFValues.setfValueName(filterValue);

				if (null != arFValuesList) {
					arFValuesList.add(objFValues);
				} else {
					arFValuesList = new ArrayList<FilterValues>();
					arFValuesList.add(objFValues);
				}
				session.setAttribute("FValuesList", arFValuesList);
			}
			
		} catch (ScanSeeServiceException exception) {
			LOG.error("Inside FilterController : addFilterValue : " + exception);
			throw new ScanSeeServiceException();
		}

		LOG.info("Inside FilterController : addFilterValue");
		return strResponse;
	}
	
	
	
	/**
	 * This controller method is used to Update Filter value.
	 * 
	 * @param filterValue as input parameter.
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */

	@RequestMapping(value = "/updatefvalue.htm", method = RequestMethod.GET)
	public @ResponseBody
	String updateFilterValue(@RequestParam(value = "filterValue", required = true) String filterValue,
			@RequestParam(value = "fValueId" , required = true) Integer filterValueId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside FilterController : updateFilterValue");

		String strResponse = "";

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FilterService filterService = (FilterService) appContext.getBean(ApplicationConstants.FILTERSERVICE);
		try {			
			
			final String userID = (String) session.getAttribute("UserId");
			
			final FilterValues fValueReq = new FilterValues();
				fValueReq.setfValueName(filterValue);
				fValueReq.setUserId(Integer.parseInt(userID));
				fValueReq.setfValueId(filterValueId);
				fValueReq.setScreenName("updateFValue");
			
			strResponse = filterService.addUpdateFilterValue(fValueReq);

			
		} catch (ScanSeeServiceException exception) {
			LOG.error("Inside FilterController : addFilterValue : " + exception);
			throw new ScanSeeServiceException();
		}
		LOG.info("Inside FilterController : addFilterValue");
		return strResponse;
	}
	




	/**
	 * This controller method is used to delete Filter value.
	 * 
	 * @param iFValue as input parameter.
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	@RequestMapping(value = "/deletefvalue.htm", method = RequestMethod.GET)
	public @ResponseBody
	String deleteFilterValue(@RequestParam(value = "fValueId", required = true) Integer iFValue, 
			
			HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside FilterController : deleteFilterValue");

		String strResponse = "";

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FilterService filterService = (FilterService) appContext.getBean(ApplicationConstants.FILTERSERVICE);

		try {
			strResponse = filterService.deleteFilterValue(iFValue);

		} catch (ScanSeeServiceException exception) {
			LOG.error("Inside FilterController : deleteFilterValue : " + exception);
			throw new ScanSeeServiceException();
		}

		LOG.info("Inside FilterController : deleteFilterValue");
		return strResponse;
	}

	/**
	 * This controller method will display add filter screen.
	 * 
	 * @param filterReq instance of Filter.
	 * @param request HttpServletRequest instance.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return RETURN_VIEW add filter screen.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/addfilter.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public final ModelAndView addFilter(@ModelAttribute("filterform") Filter filterReq, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap map) throws ScanSeeServiceException {
		LOG.info("Inside FilterController : addFilter ");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FilterService filterService = (FilterService) appContext.getBean(ApplicationConstants.FILTERSERVICE);

		final String userID = (String) session.getAttribute("UserId");

		session.removeAttribute("filterFont");
		session.removeAttribute("responseFilterMsg");
		filterReq.setFilterId(null);

		ArrayList<Category> arFCategoryList = null;
		ArrayList<FilterValues> arFilterValuesList = null;
		String strBckButton = null;

		try {

			arFCategoryList = filterService.getAllFilterCategory(Integer.parseInt(userID));
			session.setAttribute("fCategoryList", arFCategoryList);
			
			arFilterValuesList = filterService.getAllFilterValues(Integer.parseInt(userID), filterReq.getFilterId());
			session.setAttribute("filterValuesList", arFilterValuesList);
			
			strBckButton = (String)session.getAttribute("backButton");
			filterReq.setFilterOption("no");
			filterReq.setBackButton(strBckButton);

			map.put("filterform", filterReq);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside FilterController : addFilter : " + e.getMessage());
			throw e;
		}
		return new ModelAndView("addfilter");
	}


	/**
	 * This controller method will display edit filter screen.
	 * 
	 * @param filterReq instance of Filter.
	 * @param request HttpServletRequest instance.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return RETURN_VIEW add filter screen.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/editfilter.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public final ModelAndView editFilter(@ModelAttribute("filterform") Filter filterReq, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap map) throws ScanSeeServiceException {
		LOG.info("Inside FilterController : editFilter ");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FilterService filterService = (FilterService) appContext.getBean(ApplicationConstants.FILTERSERVICE);

		final String userID = (String) session.getAttribute("UserId");
		
		session.setAttribute("filterId", filterReq.getFilterId());

		session.removeAttribute("filterFont");
		session.removeAttribute("responseFilterMsg");

		ArrayList<Category> arFCategoryList = null;
		ArrayList<FilterValues> arFilterValuesList = null;
		Filter editFilter = null;

		try {

			editFilter = filterService.getFilterDetailsByID(filterReq.getFilterId(), Integer.parseInt(userID));
			editFilter.setfCategoryHidden(editFilter.getfCategory());

			arFCategoryList = filterService.getAllFilterCategory(Integer.parseInt(userID));
			session.setAttribute("fCategoryList", arFCategoryList);

			arFilterValuesList = filterService.getAllFilterValues(Integer.parseInt(userID), filterReq.getFilterId());
			session.setAttribute("filterValuesList", arFilterValuesList);
			
			editFilter.setFilterId(filterReq.getFilterId());
			map.put("filterform", editFilter);
			
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside FilterController : editFilter : " + e.getMessage());
			throw e;
		}
		return new ModelAndView("editfilter");
	}


	/**
	 * This controller method will delete filter by call service layer.
	 * 
	 * @param filterReq instance of Filter.
	 * @param request request parameter.
	 * @param session as request parameter.
	 * @param response as request parameter.
	 * @param model as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/deletefilter", method = RequestMethod.POST)
	public ModelAndView deleteFilter(@ModelAttribute("filterform") Filter filterReq, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside FilterController : deleteFilter");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FilterService filterService = (FilterService) appContext.getBean(ApplicationConstants.FILTERSERVICE);

		session.removeAttribute("filterFont");
		session.removeAttribute("responseFilterMsg");
		
		final String userID = (String) session.getAttribute("UserId");
		final int recordCount = 20;
		final int lowerLimit = 0;
		final int currentPage = 1;

		FilterInfo filterInfo = null;
		Pagination objPage = null;

		try {

			final String status = filterService.deleteFilter(filterReq.getFilterId());

			if (ApplicationConstants.SUCCESS.equals(status)) {
				session.setAttribute("filterFont", "font-weight:bold;color:#00559c;");
				session.setAttribute("responseFilterMsg", "Filter Deleted Successfully.");
			} else if (ApplicationConstants.FILTERASSCOIATED.equals(status)) {
				session.setAttribute("filterFont", "font-weight:bold;color:red;");
				session.setAttribute("responseFilterMsg", ApplicationConstants.FILTERASSCOIATED);
			} else {
				session.setAttribute("filterFont", "font-weight:bold;color:red;");
				session.setAttribute("responseFilterMsg", "Error While Deleting Filter");	
			}

			filterReq.setLowerLimit(lowerLimit);
			filterReq.setUserId(Integer.parseInt(userID));
			filterReq.setSearchKey(null);
			filterReq.setFilterId(null);
			filterInfo = filterService.getAllFilters(filterReq);

			if (null != filterInfo  &&  filterInfo.getFilterList().size() <= 0 ) {
				/*  Back button to hide in add Filter screen if manage filter list is empty. */
				session.setAttribute("backButton", "no");
				return new ModelAndView(new RedirectView("/ScanSeeAdmin/addfilter.htm"));

			} else if (null != filterInfo) {
				session.setAttribute("filterList", filterInfo.getFilterList());
				objPage = Utility.getPagination(filterInfo.getTotalSize(), currentPage, "managefilters.htm", recordCount);
				session.setAttribute("pagination", objPage);
			}


			final Filter filterResp = new Filter();
			model.put("filterForm", filterResp);
		} catch (ScanSeeServiceException exception) {
			LOG.error("Inside FilterController : deleteEvent : " + exception.getMessage());
			throw exception;
		}
		return new ModelAndView("managefilters");
	}



	/**
	 * This controller method will Add/Update Filter info by calling
	 * service layer.
	 * 
	 * @param filterReq instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/savefilter.htm", method = RequestMethod.POST)
	ModelAndView saveUpdateFilter(@ModelAttribute("filterform") Filter filterReq, BindingResult result, HttpServletRequest request,
			HttpServletResponse httpresponse, HttpSession session, ModelMap map) throws ScanSeeServiceException {
		LOG.info("Inside FilterController : saveUpdateFilter ");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FilterService filterService = (FilterService) appContext.getBean(ApplicationConstants.FILTERSERVICE);
		
		session.removeAttribute("filterFont");
		session.removeAttribute("responseFilterMsg");
		
		final String userID = (String) session.getAttribute("UserId");
		
		String response = null;
		ArrayList<FilterValues> arFilterValuesList = null;

		session.removeAttribute("responseFilterMsg");
		session.removeAttribute("filterFont");

		filterReq.setfCategoryHidden(filterReq.getfCategory());
		filterReq.setUserId(Integer.parseInt(userID));
		
		try {

			filterValidator.validate(filterReq, result);
			
			arFilterValuesList = filterService.getAllFilterValues(Integer.parseInt(userID), filterReq.getFilterId());
			session.setAttribute("filterValuesList", arFilterValuesList);
			
			if (result.hasErrors()) {
				
				map.put("filterForm", filterReq);
				return new ModelAndView(filterReq.getViewName());
			} else {
				response = filterService.addUpdateFilter(filterReq);

				if (response.equals(ApplicationConstants.SUCCESS)) {
					
					if (null == filterReq.getFilterId() || "".equals(filterReq.getFilterId())) {
						session.setAttribute("filterFont", "font-weight:bold;color:#00559c;");
						session.setAttribute("responseFilterMsg", "Filter Created Successfully");
					} else {
						session.setAttribute("filterFont", "font-weight:bold;color:#00559c;");
						session.setAttribute("responseFilterMsg", "Filter Updated Successfully");
					}
					
					return new ModelAndView(new RedirectView("/ScanSeeAdmin/managefilters.htm"));
					
				} else if (response.equals(ApplicationConstants.FILTEREXISTS)) {
					request.setAttribute("errorMessage", ApplicationConstants.DUPLICATEFILTERNAME);
				} else {
					if (null == filterReq.getFilterId() || "".equals(filterReq.getFilterId())) {
						request.setAttribute("errorMessage", ApplicationConstants.ADD_FILTER_ERROR_MSG);
					} else {
						request.setAttribute("errorMessage", ApplicationConstants.UPDATE_FILTER_ERROR_MSG);
					}
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside FilterController : saveUpdateFilter : " + e.getMessage());
			throw e;
		}
		return new ModelAndView(filterReq.getViewName());
	}

}

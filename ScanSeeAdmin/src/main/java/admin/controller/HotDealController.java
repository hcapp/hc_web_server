/**
 * 
 */
package admin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import admin.service.HotDealService;
import admin.validator.HotDealValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.HotDeal;
import common.pojo.HotDealDetails;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This Class is a Controller class for hot deal admin.
 * 
 * @author sangeetha.ts
 */
@Controller
public class HotDealController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(HotDealController.class);

	/**
	 * Variable hubCitiAdminValidator declared as instance of
	 * HubCitiAdminValidator.
	 */
	private HotDealValidator hotDealValidator;

	/**
	 * To set hubCitiAdminValidator.
	 * 
	 * @param hubCitiAdminValidator
	 *            to set.
	 */
	@Autowired
	public final void setHotDealValidator(HotDealValidator hotDealValidator)
	{
		this.hotDealValidator = hotDealValidator;
	}

	/**
	 * This method is used to display all the hot deals with pagination.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/displayhotdeal.htm", method = RequestMethod.POST)
	public final String displayHotDeals(@ModelAttribute("admin") HotDeal hotDeal, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "displayHotDeals";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.removeAttribute("searchKey");
		final String pageFlag = request.getParameter("pageFlag");
		HotDealDetails hotDealDetails = null;
		HotDeal requestHotDeal = null;
		Integer lowerLimit = 0;
		final String showExpire = "true";
		final String sortOrder = "ASC";
		final String columnName = "Name";
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HotDealService hotDealService = (HotDealService) appContext.getBean(ApplicationConstants.HOTDEALSERVICE);

			requestHotDeal = new HotDeal();
			hotDealDetails = new HotDealDetails();

			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			requestHotDeal.setLowerLimit(lowerLimit);
			requestHotDeal.setShowExpired(showExpire);
			requestHotDeal.setSortOrder(sortOrder);
			requestHotDeal.setColumnName(columnName);
			hotDealDetails = hotDealService.displayHotDeals(requestHotDeal);

			if (null != hotDealDetails)
			{
				for (HotDeal deal : hotDealDetails.getHotDeals())
				{
					deal.setHotDealName(deal.getHotDealName().replace("�", "&apos;"));
				}
			}

			session.setAttribute("moduleName", ApplicationConstants.HOTDEALADMIN);
			session.setAttribute("hotdeallist", hotDealDetails.getHotDeals());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("columnName", columnName);
			session.setAttribute("HotDealNameImg", "images/sorticonUp.png");
			session.setAttribute("HotDealPartnerImg", "");
			session.setAttribute("HotDealPriceImg", "");
			session.setAttribute("HotDealStartDateImg", "");
			session.setAttribute("HotDealEndDateImg", "");
			session.setAttribute("sortOrderName", "DESC");
			session.setAttribute("sortOrderPartner", "ASC");
			session.setAttribute("sortOrderPrice", "ASC");
			session.setAttribute("sortOrderStartDate", "ASC");
			session.setAttribute("sortOrderEndDate", "ASC");
			objPage = Utility.getPagination(hotDealDetails.getTotalSize(), currentPage, "displayhotdeal.htm", recordCount);
			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		hotDeal = new HotDeal();
		model.put("hotdeal", hotDeal);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "displayhotdeal";
	}

	/**
	 * This method is used for hot deal search and for displaying expired hot
	 * deals with pagination.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/searchhotdeal.htm", method = RequestMethod.POST)
	public final String searchHotDeal(@ModelAttribute("hotdeal") HotDeal hotDeal, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "searchHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		HotDealDetails hotDealDetails = null;
		HotDeal requestHotDeal = null;
		String searchKey = hotDeal.getSearchKey();
		Integer lowerLimit = 0;
		final String showExpire = hotDeal.getShowExpired();
		final String sortOrder = "ASC";
		final String columnName = "Name";
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HotDealService hotDealService = (HotDealService) appContext.getBean(ApplicationConstants.HOTDEALSERVICE);

			requestHotDeal = new HotDeal();
			hotDealDetails = new HotDealDetails();

			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			requestHotDeal.setSearchKey(searchKey);
			requestHotDeal.setLowerLimit(lowerLimit);
			requestHotDeal.setShowExpired(showExpire);
			requestHotDeal.setSortOrder(sortOrder);
			requestHotDeal.setColumnName(columnName);
			hotDealDetails = hotDealService.displayHotDeals(requestHotDeal);

			if (null == hotDealDetails || hotDealDetails.getTotalSize() == 0)
			{
				request.setAttribute("errorMessage", "No HotDeals Found.");
			}

			if (null != hotDealDetails)
			{
				for (HotDeal deal : hotDealDetails.getHotDeals())
				{
					deal.setHotDealName(deal.getHotDealName().replace("�", "&apos;"));
				}
			}

			session.setAttribute("moduleName", ApplicationConstants.HOTDEALADMIN);
			session.setAttribute("hotdeallist", hotDealDetails.getHotDeals());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("columnName", columnName);
			session.setAttribute("HotDealNameImg", "images/sorticonUp.png");
			session.setAttribute("HotDealPartnerImg", "");
			session.setAttribute("HotDealPriceImg", "");
			session.setAttribute("HotDealStartDateImg", "");
			session.setAttribute("HotDealEndDateImg", "");
			session.setAttribute("sortOrderName", "DESC");
			session.setAttribute("sortOrderPartner", "ASC");
			session.setAttribute("sortOrderPrice", "ASC");
			session.setAttribute("sortOrderStartDate", "ASC");
			session.setAttribute("sortOrderEndDate", "ASC");
			objPage = Utility.getPagination(hotDealDetails.getTotalSize(), currentPage, "searchhotdeal.htm", recordCount);
			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "displayhotdeal";
	}

	/**
	 * This method is used for hot deal sort with pagination.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/sorthotdeal.htm", method = RequestMethod.POST)
	public final String sortHotDeal(@ModelAttribute("hotdeal") HotDeal hotDeal, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "sortHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		HotDealDetails hotDealDetails = null;
		HotDeal requestHotDeal = null;
		final String searchKey = hotDeal.getSearchKey();
		Integer lowerLimit = 0;
		final String showExpire = hotDeal.getShowExpired();
		String sortOrder = "";
		String columnName = hotDeal.getColumnName();
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HotDealService hotDealService = (HotDealService) appContext.getBean(ApplicationConstants.HOTDEALSERVICE);

			if ("".equals(columnName))
			{
				columnName = (String) session.getAttribute("Name");
			}

			if ("Name".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderName");
			}
			else if ("Partner".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderPartner");
			}
			else if ("Price".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderPrice");
			}
			else if ("StartDate".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderStartDate");
			}
			else if ("EndDate".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderEndDate");
			}

			requestHotDeal = new HotDeal();
			hotDealDetails = new HotDealDetails();

			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}

				if ("ASC".equalsIgnoreCase(sortOrder))
				{
					sortOrder = "DESC";
				}
				else
				{
					sortOrder = "ASC";
				}
			}

			requestHotDeal.setSearchKey(searchKey);
			requestHotDeal.setLowerLimit(lowerLimit);
			requestHotDeal.setShowExpired(showExpire);
			requestHotDeal.setSortOrder(sortOrder);
			requestHotDeal.setColumnName(columnName);
			hotDealDetails = hotDealService.displayHotDeals(requestHotDeal);

			if (null != hotDealDetails)
			{
				for (HotDeal deal : hotDealDetails.getHotDeals())
				{
					deal.setHotDealName(deal.getHotDealName().replace("�", "&apos;"));
				}
			}

			session.setAttribute("moduleName", ApplicationConstants.HOTDEALADMIN);
			session.setAttribute("hotdeallist", hotDealDetails.getHotDeals());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("columnName", columnName);

			if ("Name".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("HotDealNameImg", "images/sorticonUp.png");
					session.setAttribute("HotDealPartnerImg", "");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "DESC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
				else
				{
					session.setAttribute("HotDealNameImg", "images/sorticon.png");
					session.setAttribute("HotDealPartnerImg", "");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
			}
			else if ("Partner".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "images/sorticonUp.png");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "DESC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
				else
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "images/sorticon.png");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
			}
			else if ("Partner".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "images/sorticonUp.png");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "DESC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
				else
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "images/sorticon.png");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
			}
			else if ("Price".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "");
					session.setAttribute("HotDealPriceImg", "images/sorticonUp.png");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "DESC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
				else
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "");
					session.setAttribute("HotDealPriceImg", "images/sorticon.png");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
			}
			else if ("StartDate".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "images/sorticonUp.png");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "DESC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
				else
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "images/sorticon.png");
					session.setAttribute("HotDealEndDateImg", "");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
			}
			else if ("EndDate".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "images/sorticonUp.png");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "DESC");
				}
				else
				{
					session.setAttribute("HotDealNameImg", "");
					session.setAttribute("HotDealPartnerImg", "");
					session.setAttribute("HotDealPriceImg", "");
					session.setAttribute("HotDealStartDateImg", "");
					session.setAttribute("HotDealEndDateImg", "images/sorticon.png");
					session.setAttribute("sortOrderName", "ASC");
					session.setAttribute("sortOrderPartner", "ASC");
					session.setAttribute("sortOrderPrice", "ASC");
					session.setAttribute("sortOrderStartDate", "ASC");
					session.setAttribute("sortOrderEndDate", "ASC");
				}
			}
			objPage = Utility.getPagination(hotDealDetails.getTotalSize(), currentPage, "sorthotdeal.htm", recordCount);
			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "displayhotdeal";
	}

	/**
	 * This method is used for hot deal Deletion.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/deletehotdeal.htm", method = RequestMethod.POST)
	public final String deleteHotDeal(@ModelAttribute("hotdeal") HotDeal hotDeal, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "deleteHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HotDealDetails hotDealDetails = null;
		HotDeal requestHotDeal = null;
		final String searchKey = hotDeal.getSearchKey();
		Integer lowerLimit = 0;
		final String showExpire = "true";
		final String sortOrder = "ASC";
		final String columnName = "Name";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		String status = null;
		String hotDealIds = hotDeal.getHotDealIDs();

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HotDealService hotDealService = (HotDealService) appContext.getBean(ApplicationConstants.HOTDEALSERVICE);

			status = hotDealService.deleteHotDeal(hotDealIds);

			if ("Success".equalsIgnoreCase(status))
			{
				request.setAttribute("message", "Hot Deal deleted successfully.");
			}
			else
			{
				request.setAttribute("message", "Failed to delete Hot Deal.");
			}

			requestHotDeal = new HotDeal();
			hotDealDetails = new HotDealDetails();
			requestHotDeal.setSearchKey(searchKey);
			requestHotDeal.setLowerLimit(lowerLimit);
			requestHotDeal.setShowExpired(showExpire);
			requestHotDeal.setSortOrder(sortOrder);
			requestHotDeal.setColumnName(columnName);
			hotDealDetails = hotDealService.displayHotDeals(requestHotDeal);

			if (null != hotDealDetails)
			{
				for (HotDeal deal : hotDealDetails.getHotDeals())
				{
					deal.setHotDealName(deal.getHotDealName().replace("�", "&apos;"));
				}
			}

			session.setAttribute("moduleName", ApplicationConstants.HOTDEALADMIN);
			session.setAttribute("hotdeallist", hotDealDetails.getHotDeals());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("columnName", columnName);
			session.setAttribute("HotDealNameImg", "images/sorticonUp.png");
			session.setAttribute("HotDealPartnerImg", "");
			session.setAttribute("HotDealPriceImg", "");
			session.setAttribute("HotDealStartDateImg", "");
			session.setAttribute("HotDealEndDateImg", "");
			session.setAttribute("sortOrderName", "DESC");
			session.setAttribute("sortOrderPartner", "ASC");
			session.setAttribute("sortOrderPrice", "ASC");
			session.setAttribute("sortOrderStartDate", "ASC");
			session.setAttribute("sortOrderEndDate", "ASC");
			objPage = Utility.getPagination(hotDealDetails.getTotalSize(), currentPage, "searchhotdeal.htm", recordCount);
			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "displayhotdeal";
	}

	/**
	 * This method is used for hot deal Deletion.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/edithotdeal.htm", method = RequestMethod.POST)
	public final String editHotDeal(@ModelAttribute("hotdeal") HotDeal hotDeal, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "editHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		int hotDealId = hotDeal.getProductHotDealID();
		HotDeal responseHotDeal = null;
		ArrayList<Category> categories = null;
		String dealStartTime = null;
		String dealEndTime = null;

		String[] tempStartTimeHrsMin = null;
		String[] tempEndTimeHrsMin = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HotDealService hotDealService = (HotDealService) appContext.getBean(ApplicationConstants.HOTDEALSERVICE);

			responseHotDeal = new HotDeal();
			responseHotDeal = hotDealService.displayHotDealDetails(hotDealId);

			if (null != responseHotDeal.getState())
			{
				responseHotDeal.setState(responseHotDeal.getState().replace("|", "\n"));
			}
			if (null != responseHotDeal.getCity())
			{
				responseHotDeal.setCity(responseHotDeal.getCity().replace("|", "\n"));
			}
			if (null != responseHotDeal.getRetailLocationAddress())
			{
				responseHotDeal.setRetailLocationAddress(responseHotDeal.getRetailLocationAddress().replace("|", "\n"));
			}

			if (null != responseHotDeal.getDealStartTime())
			{
				dealStartTime = responseHotDeal.getDealStartTime();
				tempStartTimeHrsMin = dealStartTime.split(":");
				responseHotDeal.setDealStartHrs(tempStartTimeHrsMin[0]);
				responseHotDeal.setDealStartMins(tempStartTimeHrsMin[1]);
			}

			if (null != responseHotDeal.getDealEndTime())
			{
				dealEndTime = responseHotDeal.getDealEndTime();
				tempEndTimeHrsMin = dealEndTime.split(":");
				responseHotDeal.setDealEndhrs(tempEndTimeHrsMin[0]);
				responseHotDeal.setDealEndMins(tempEndTimeHrsMin[1]);
			}

			categories = hotDealService.displayHotDealCategories();

			session.setAttribute("moduleName", ApplicationConstants.HOTDEALADMIN);
			session.setAttribute("hotDeal", responseHotDeal);
			model.put("hotdeal", responseHotDeal);
			session.setAttribute("categories", categories);
		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "edithotdeal";
	}

	/**
	 * This method is used for hot deal updation.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/savehotdeal.htm", method = RequestMethod.POST)
	public final ModelAndView saveHotDeal(@ModelAttribute("hotdeal") HotDeal hotDeal, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "saveHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HotDealDetails hotDealDetails = null;
		HotDeal requestHotDeal = null;
		final String searchKey = hotDeal.getSearchKey();
		Integer lowerLimit = 0;
		final String showExpire = "true";
		final String sortOrder = "ASC";
		final String columnName = "Name";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		String status = null;
		String compDate = null;
		boolean isValidURL = true;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HotDealService hotDealService = (HotDealService) appContext.getBean(ApplicationConstants.HOTDEALSERVICE);

			String sTime = String.valueOf(hotDeal.getDealStartHrs()) + ":" + String.valueOf(hotDeal.getDealStartMins());
			String endTime = String.valueOf(hotDeal.getDealEndhrs()) + ":" + String.valueOf(hotDeal.getDealEndMins());

			hotDeal.setDealStartTime(sTime);
			hotDeal.setDealEndTime(endTime);

			hotDealValidator.validate(hotDeal, result);

			if (result.hasErrors())
			{
				session.setAttribute("hotDeal", hotDeal);
				return new ModelAndView("edithotdeal");
			}

			if (hotDeal.getSalePrice() >= hotDeal.getPrice())
			{
				hotDealValidator.validate(hotDeal, result, ApplicationConstants.SALEPRICEGREATER);
			}

			if (result.hasErrors())
			{
				session.setAttribute("hotDeal", hotDeal);
				return new ModelAndView("edithotdeal");
			}

			if (null != hotDeal.getHotDealURL() && !hotDeal.getHotDealURL().isEmpty())
			{

				isValidURL = Utility.validateURL(hotDeal.getHotDealURL());
			}

			if (!isValidURL)
			{
				hotDealValidator.validate(hotDeal, result, ApplicationConstants.INVALIDURL);
			}

			if (result.hasErrors())
			{
				session.setAttribute("hotDeal", hotDeal);
				return new ModelAndView("edithotdeal");
			}

			compDate = Utility.compareDate(hotDeal.getHotDealStartDate(), hotDeal.getHotDealEndDate());
			if (null != compDate)
			{
				hotDealValidator.validate(hotDeal, result, ApplicationConstants.DATEAFTER);
			}
			if (result.hasErrors())
			{
				session.setAttribute("hotDeal", hotDeal);
				return new ModelAndView("edithotdeal");
			}

			status = hotDealService.updateHotDeal(hotDeal);

			if ("Success".equalsIgnoreCase(status))
			{
				request.setAttribute("message", "Hot Deal updated successfully.");
			}
			else
			{
				request.setAttribute("message", "Failed to update Hot Deal.");
			}

			requestHotDeal = new HotDeal();
			hotDealDetails = new HotDealDetails();
			requestHotDeal.setSearchKey(searchKey);
			requestHotDeal.setLowerLimit(lowerLimit);
			requestHotDeal.setShowExpired(showExpire);
			requestHotDeal.setSortOrder(sortOrder);
			requestHotDeal.setColumnName(columnName);
			hotDealDetails = hotDealService.displayHotDeals(requestHotDeal);

			if (null != hotDealDetails)
			{
				for (HotDeal deal : hotDealDetails.getHotDeals())
				{
					deal.setHotDealName(deal.getHotDealName().replace("�", "&apos;"));
				}
			}

			session.setAttribute("moduleName", ApplicationConstants.HOTDEALADMIN);
			session.setAttribute("hotdeallist", hotDealDetails.getHotDeals());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("columnName", columnName);
			session.setAttribute("HotDealNameImg", "images/sorticonUp.png");
			session.setAttribute("HotDealPartnerImg", "");
			session.setAttribute("HotDealPriceImg", "");
			session.setAttribute("HotDealStartDateImg", "");
			session.setAttribute("HotDealEndDateImg", "");
			session.setAttribute("sortOrderName", "DESC");
			session.setAttribute("sortOrderPartner", "ASC");
			session.setAttribute("sortOrderPrice", "ASC");
			session.setAttribute("sortOrderStartDate", "ASC");
			session.setAttribute("sortOrderEndDate", "ASC");
			objPage = Utility.getPagination(hotDealDetails.getTotalSize(), currentPage, "searchhotdeal.htm", recordCount);
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		hotDeal = new HotDeal();
		model.put("hotdeal", hotDeal);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayhotdeal");
	}

	/**
	 * This ModelAttribute sort Deal start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("DealStartHours")
	public Map<String, String> populateDealStartHrs() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			}
			else
			{
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort Deal start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("DealStartMinutes")
	public Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			}
			else
			{
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}
}

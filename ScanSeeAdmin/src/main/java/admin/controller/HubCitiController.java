/**
 * 
 */
package admin.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import admin.service.HubCitiService;
import admin.service.RetailerService;
import admin.validator.HubCitiAdminValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AlertCategory;
import common.pojo.City;
import common.pojo.EventCategory;
import common.pojo.HubCiti;
import common.pojo.HubCitiDetails;
import common.pojo.PostalCode;
import common.pojo.State;
import common.tags.Pagination;
import common.util.Utility;

/**
 * Controller for HunCiti.
 * 
 * @author sangeetha.ts
 */
@Controller
public class HubCitiController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(HubCitiController.class);
	/**
	 * Variable hubCitiAdminValidator declared as instance of
	 * HubCitiAdminValidator.
	 */
	private HubCitiAdminValidator hubCitiAdminValidator;

	/**
	 * constant as SHOWDEACTIVATE.
	 */
	private static final String SHOWDEACTIVATE = "showDeactivated";

	/**
	 * To set hubCitiAdminValidator.
	 * 
	 * @param hubCitiAdminValidator
	 *            to set.
	 */
	@Autowired
	public final void setHubCitiAdminValidator(HubCitiAdminValidator hubCitiAdminValidator) {
		this.hubCitiAdminValidator = hubCitiAdminValidator;
	}

	/**
	 * This method is used to display all the hubciti's with pagination.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/displayhubcitiadmin.htm", method = RequestMethod.POST)
	public final String displayHubCitis(@ModelAttribute("admin") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "displayHubCitis";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		session.removeAttribute("selectedPostalCodeList");

		final String pageFlag = request.getParameter("pageFlag");
		HubCitiDetails citiDetails = null;
		final String searchKey = hubCiti.getSearchKey();
		Integer user = null;
		final String userId = (String) session.getAttribute("UserId");
		if (!Utility.isEmptyOrNullString(userId)) {
			user = Integer.parseInt(userId);
		}
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		Boolean showDeactivated = hubCiti.getShowDeactivated();
		ArrayList<State> states = null;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			hubCiti.setAppRole(ApplicationConstants.HUBCITIAPP);
			if (null == showDeactivated) {
				showDeactivated = false;
			}
			if (null != hubCiti.getLowerLimit()) {
				lowerLimit = hubCiti.getLowerLimit();
			}
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {
				currentPage = (lowerLimit + recordCount) / recordCount;
			}

			states = retailerService.getAllStates();
			session.setAttribute("statesListCreat", states);

			citiDetails = hubCitiService.displayHubCiti(user, searchKey, lowerLimit, showDeactivated);

			if (null != citiDetails && null != citiDetails.getHubCitis() && !citiDetails.getHubCitis().isEmpty()) {
				session.setAttribute("adminlist", citiDetails.getHubCitis());
				objPage = Utility.getPagination(citiDetails.getTotalSize(), currentPage, "displayhubcitiadmin.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				if (null != searchKey && !"".equals(searchKey)) {
					request.setAttribute("errorMessage", "No HubCitis Found.");
				}
				session.setAttribute("adminlist", null);
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}

			request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
			session.setAttribute(SHOWDEACTIVATE, showDeactivated);

		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException();
		}

		HubCiti citi = new HubCiti();
		citi.setSearchKey(searchKey);
		model.put("hubciti", citi);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "displayhubcitiadmin";
	}

	/**
	 * This method is used to show de activated hubciti's.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/deactivateadmin.htm", method = RequestMethod.POST)
	public final String deactivateAdmin(@ModelAttribute("hubciti") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "deactivateAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HubCitiDetails citiDetails = null;
		final Integer hcAdminId = hubCiti.getHubCitiID();
		final String searchKey = hubCiti.getSearchKey();
		final Boolean showDeactivated = hubCiti.getShowDeactivated();
		final String userId = (String) session.getAttribute("UserId");
		final Integer user = new Integer(userId);
		Integer lowerLimit = 0;
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		String status = null;
		final Boolean active = hubCiti.getActive();
		Integer activeFlag = 0;
		if (active == true) {
			activeFlag = 1;
		}

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

			status = hubCitiService.deactivateAdmin(user, hcAdminId, activeFlag);

			if ("Success".equalsIgnoreCase(status)) {
				if (active == true) {
					request.setAttribute("message", "HubCiti Admin Deactivated successfully.");
				} else {
					request.setAttribute("message", "HubCiti Admin Activated successfully.");
				}
			} else {
				if (active == true) {
					request.setAttribute("message", "Failed to Deactivate HubCiti Admin.");
				} else {
					request.setAttribute("message", "Failed to Activate HubCiti Admin.");
				}

			}

			if (null != hubCiti.getLowerLimit()) {
				lowerLimit = hubCiti.getLowerLimit();
				currentPage = (lowerLimit + recordCount) / recordCount;
			}

			citiDetails = hubCitiService.displayHubCiti(user, searchKey, lowerLimit, showDeactivated);

			if (null != citiDetails && null != citiDetails.getHubCitis() && citiDetails.getHubCitis().isEmpty() && lowerLimit > 0) {
				lowerLimit = lowerLimit - recordCount;
				currentPage = (lowerLimit + recordCount) / recordCount;
				citiDetails = hubCitiService.displayHubCiti(user, searchKey, lowerLimit, showDeactivated);
			}

			if (null != citiDetails && null != citiDetails.getHubCitis() && !citiDetails.getHubCitis().isEmpty()) {
				session.setAttribute("adminlist", citiDetails.getHubCitis());
				objPage = Utility.getPagination(citiDetails.getTotalSize(), currentPage, "displayhubcitiadmin.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				session.setAttribute("adminlist", null);
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}

			request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
			session.setAttribute(SHOWDEACTIVATE, showDeactivated);

		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException();
		}

		HubCiti citi = new HubCiti();
		citi.setSearchKey(searchKey);
		model.put("hubciti", citi);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "displayhubcitiadmin";
	}

	/**
	 * This method will return add admin screen.
	 * 
	 * @param hubCiti
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/addadmin.htm", method = RequestMethod.POST)
	public final ModelAndView addAdmin(@ModelAttribute("hubciti") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "addAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		session.removeAttribute("defltPstlCdeLst");
		session.removeAttribute("selectedPostalCodeList");
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		request.setAttribute(ApplicationConstants.LOWERLIMIT, hubCiti.getLowerLimit());
		session.setAttribute("postalCodeMap", new HashMap<String, String>());

		HubCiti citi = new HubCiti();
		citi.setSearchKey(hubCiti.getSearchKey());
		model.put("hubciti", citi);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("addadmin");
	}

	/**
	 * This method populate the city drop down based on the given state code.
	 * 
	 * @param stateCode
	 * @param city
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/fetchcities.htm", method = RequestMethod.GET)
	public @ResponseBody
	final String getCities(@RequestParam(value = "statecode", required = true) String stateCode,
			@RequestParam(value = "city", required = true) String city, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<City> cities = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='city' id='city' class='textboxBig' onchange='loadPostalCode();'><option value=''>--Select--</option>";

		innerHtml.append(finalHtml);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		cities = retailerService.fetchAllCities(stateCode);
		response.setContentType("text/xml");

		final String selCity = city;

		for (int i = 0; i < cities.size(); i++) {
			final String cityName = cities.get(i).getCityName();
			if (null != selCity && cityName.equalsIgnoreCase(selCity)) {
				innerHtml.append("<option selected=true >" + cities.get(i).getCityName() + "</option>");
			} else {
				innerHtml.append("<option>" + cities.get(i).getCityName() + "</option>");
			}
		}
		innerHtml.append("</select>");

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	/**
	 * This method populate the city drop down based on the given state code.
	 * 
	 * @param stateCode
	 * @param city
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/fetchzip.htm", method = RequestMethod.GET)
	public @ResponseBody
	final String getPostalCodes(@RequestParam(value = "state", required = true) String state,
			@RequestParam(value = "city", required = true) String city, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getPostalCodes";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<PostalCode> postalCodes = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='postalCodes' multiple='multiple' id='postalCodes' class='textboxBig multiSelDropDown' style='font-size: 11px;'>";

		innerHtml.append(finalHtml);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

		postalCodes = hubCitiService.getAllPostalCode(city, state);
		response.setContentType("text/xml");
		final String key = state + "/" + city;
		String value = null;
		@SuppressWarnings("unchecked")
		final HashMap<String, String> postalCodeMap = (HashMap<String, String>) session.getAttribute("postalCodeMap");

		if (null != postalCodeMap && !postalCodeMap.isEmpty()) {
			for (Entry<String, String> entry : postalCodeMap.entrySet()) {
				if (entry.getKey().equalsIgnoreCase(key)) {
					value = entry.getValue();
				}
			}
		}

		String[] selPostalCodes = null;

		if (null != value && !value.isEmpty()) {
			selPostalCodes = value.split(",");
		}

		for (PostalCode code : postalCodes) {
			Integer selectFlag = 0;
			if (null != selPostalCodes) {
				for (String postalCode : selPostalCodes) {
					if (code.getPostalCode().equals(postalCode)) {
						selectFlag = 1;
					}
				}

				if (selectFlag != 1) {
					innerHtml.append("<option value=" + code.getPostalCode() + " title=" + code.getLocation() + ">" + code.getLocation()
							+ "</option>");
				}
			} else {
				innerHtml.append("<option value=" + code.getPostalCode() + " title=" + code.getLocation() + ">" + code.getLocation() + "</option>");
			}
		}

		innerHtml.append("</select>");

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	/**
	 * This method is used to create hubciti.
	 * 
	 * @param hubCiti
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return view
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/createhubcitiadmin.htm", method = RequestMethod.POST)
	public final ModelAndView createHubCitiAdmin(@ModelAttribute("hubciti") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "createHubCitiAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String userId = (String) session.getAttribute("UserId");
		String status = null;
		final String searchKey = null;
		final Boolean showDeactivated = false;
		final Integer lowerLimit = 0;
		final int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		HubCitiDetails citiDetails = null;
		boolean isValidEmail = true;
		boolean isValidSalesContact = true;
		boolean isValidEmails = true;
		ArrayList<HubCiti> postalCodes = null;
//		String strPostalcode = null;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

			request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
			request.setAttribute("state", hubCiti.getState());
			request.setAttribute("city", hubCiti.getCity());

			postalCodes = iterateSelectedPostalCode(session);

			if (null != postalCodes && !postalCodes.isEmpty()) {
				hubCiti.setZipCode("true");
			} else {
				hubCiti.setZipCode("false");
			}

			hubCitiAdminValidator.validate(hubCiti, result);

			if (result.hasErrors()) {
				return new ModelAndView("addadmin");
			}

			isValidEmail = Utility.validateEmailId(hubCiti.getEmailID());
			isValidSalesContact = Utility.validateEmailId(hubCiti.getSalesPersonEmail());
			
			final String[] strEmails = hubCiti.getEmails().split(";");
			
			Set<String> list = new LinkedHashSet<String>(Arrays.asList(strEmails));
			list.removeAll(Arrays.asList("", null));
			int icount = 0;
			
			if (list.size() > 5) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.NUMBER_OF_EMAILS);
				if (result.hasErrors()) {
					return new ModelAndView("addadmin");
				}
			}
			
			if (strEmails.length != list.size()) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATE_EMAILS);

				if (result.hasErrors()) {
					return new ModelAndView("addadmin");
				}
			}
			
			if (list.size() != 1) {
				for (int i = 0; i < list.size(); i++) {
					if (!Utility.validateEmailId(strEmails[i])) {
						++icount;
					} 
				}
			} else {
					isValidEmails = Utility.validateEmailId(hubCiti.getEmails());
					
					if (!isValidEmails) {
						hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALID_EMAIL);

						if (result.hasErrors()) {
							return new ModelAndView("addadmin");
						}
					}
			}

			/*strPostalcode = String.valueOf(hubCiti.getDefaultZipcode());

			if (null != strPostalcode && strPostalcode.length() < 5) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDZIPCODE);

			}*/
				
			if (result.hasErrors()) {
				return new ModelAndView("addadmin");
			}

			if (!isValidEmail) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDEMAIL);

				if (result.hasErrors()) {
					return new ModelAndView("addadmin");
				}
			}
			
			
			if (!isValidSalesContact) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDSALESCONTACT);
				if (result.hasErrors()) {
					return new ModelAndView("addadmin");
				}
			}
			
			if (!isValidEmails || (icount > 0)) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALID_EMAILS);
			}
			

			if (result.hasErrors()) {
				return new ModelAndView("addadmin");
			}
			
			StringBuilder sb = new StringBuilder();
				for (String emails : list) {
				    sb.append(emails).append(";");
				}
			
			hubCiti.setEmails(sb.toString());
			
			final String strLogoImage = hubCitiService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
			hubCiti.setUserID(new Integer(userId));

			status = hubCitiService.createAdmin(hubCiti, strLogoImage, postalCodes);
			if (!Utility.isEmptyOrNullString(status)) {
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATEHUBCITI)) {
					hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATEHUBCITI);

					if (result.hasErrors()) {
						return new ModelAndView("addadmin");
					}
				}
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATEUSERNAME)) {
					hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATEUSERNAME);

					if (result.hasErrors()) {
						return new ModelAndView("addadmin");
					}
				}
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATEEMAIL)) {
					hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATEEMAIL);

					if (result.hasErrors()) {
						return new ModelAndView("addadmin");
					}
				}
				
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATESALESCONTACT)) {
					hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATESALESCONTACT);

					if (result.hasErrors()) {
						return new ModelAndView("addadmin");
					}
				}

				if ("Success".equalsIgnoreCase(status)) {
					request.removeAttribute("state");
					request.removeAttribute("city");
					session.removeAttribute("selectedPostalCodeList");
					request.setAttribute("message", "HubCiti Admin Created successfully.");

					citiDetails = hubCitiService.displayHubCiti(new Integer(userId), searchKey, lowerLimit, showDeactivated);

					session.setAttribute("adminlist", citiDetails.getHubCitis());
					objPage = Utility.getPagination(citiDetails.getTotalSize(), currentPage, "displayhubcitiadmin.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				} else {
					request.setAttribute("message", "Failed to Create HubCiti Admin.");
				}
			} else {
				request.setAttribute("message", "Failed to Create HubCiti Admin.");
			}

		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException();
		}

		session.setAttribute(SHOWDEACTIVATE, showDeactivated);
		HubCiti citi = new HubCiti();
		citi.setSearchKey(searchKey);
		model.put("hubciti", citi);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayhubcitiadmin");
	}

	/**
	 * This method will return edit hubciti screen.
	 * 
	 * @param userID
	 * @param hubCitiID
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/edithubcitiadmin.htm", method = RequestMethod.POST)
	public final ModelAndView editHubCitiAdmin(@ModelAttribute("hubciti") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "editHubCitiAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.removeAttribute("defltPstlCdeLst");
		session.removeAttribute("selectedPostalCodeList");
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		final String userId = (String) session.getAttribute("UserId");
		final Integer hubCitiId = hubCiti.getHubCitiID();
		HubCiti citi = null;
		ArrayList<HubCiti> postalCodes = null;
		HashMap<String, String> postalCodeMap = null;
		ArrayList<PostalCode> selectedPostalCodeList = null;
		ArrayList<String> defltPstlCdeLst = null;
		Boolean containsKey = false;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

			citi = hubCitiService.editAdmin(new Integer(userId), hubCitiId);

			if (null != citi) {
				session.setAttribute("hubCitiUserName", citi.getUserName());
				session.setAttribute("hubCitiEmail", citi.getEmailID());
				session.setAttribute("salesContact", citi.getSalesPersonEmail());
			}

			postalCodes = hubCitiService.fetchAdminAssociatedPostalCode(hubCitiId);

			if (null != postalCodes && !postalCodes.isEmpty()) {
				postalCodeMap = new HashMap<String, String>();
				selectedPostalCodeList = new ArrayList<PostalCode>();
				defltPstlCdeLst = new ArrayList<String>();
				for (HubCiti hubCiti2 : postalCodes) {
					containsKey = false;
					String key = hubCiti2.getState() + "/" + hubCiti2.getCity();
					String pCode = hubCiti2.getPostalCodes() + ",";

					PostalCode code = new PostalCode();
					code.setPostalCode(hubCiti2.getPostalCodes());
					code.setLocation(hubCiti2.getPostalCodes() + "," + hubCiti2.getCity() + "," + hubCiti2.getState());
					code.setIsAssocaited(hubCiti2.getIsAssocaited());

					selectedPostalCodeList.add(code);

					if (!postalCodeMap.isEmpty()) {
						for (Entry<String, String> entry : postalCodeMap.entrySet()) {
							if (entry.getKey().equalsIgnoreCase(key)) {
								containsKey = true;
								String value = entry.getValue();
								value += pCode;
								entry.setValue(value);
							}
						}
						if (!containsKey) {
							postalCodeMap.put(key, pCode);
						}
					} else {
						postalCodeMap.put(key, pCode);
					}
					
					defltPstlCdeLst.add(hubCiti2.getPostalCodes());
				}

			}
			hubCitiAdminValidator.validate(citi, result);
			
			session.setAttribute("postalCodeMap", postalCodeMap);
			session.setAttribute("defltPstlCdeLst", defltPstlCdeLst);
			session.setAttribute("selectedPostalCodeList", selectedPostalCodeList);
			request.setAttribute(ApplicationConstants.LOWERLIMIT, hubCiti.getLowerLimit());
			citi.setSearchKey(hubCiti.getSearchKey());
			model.put("hubciti", citi);

		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("editadmin");
	}

	/**
	 * This method will update hubciti details.
	 * 
	 * @param
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/savehubcitiadmin.htm", method = RequestMethod.POST)
	public final ModelAndView saveHubCitiAdmin(@ModelAttribute("hubciti") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "saveHubCitiAdmin";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String userId = (String) session.getAttribute("UserId");
		String status = null;
		final String searchKey = hubCiti.getSearchKey();
		final Boolean showDeactivated = hubCiti.getShowDeactivated();
		Integer lowerLimit = 0;
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		HubCitiDetails citiDetails = null;
		ArrayList<HubCiti> postalCodes = null;
		boolean isValidEmail = true;
		boolean isValidSalesContact = true;
		boolean isValidEmails = true;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

			if (null != hubCiti.getLowerLimit()) {
				lowerLimit = hubCiti.getLowerLimit();
				currentPage = (lowerLimit + recordCount) / recordCount;
			}

			request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
			request.setAttribute("state", hubCiti.getState());
			request.setAttribute("city", hubCiti.getCity());

			postalCodes = iterateSelectedPostalCode(session);

			if (null != postalCodes && !postalCodes.isEmpty()) {
				hubCiti.setZipCode("true");
			} else {
				hubCiti.setZipCode("false");
			}

			/*strPostalcode = String.valueOf(hubCiti.getDefaultZipcode());

			if (null != strPostalcode && strPostalcode.length() < 5) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDZIPCODE);

			}*/
			if (result.hasErrors()) {
				return new ModelAndView("editadmin");
			}
			
			
			hubCitiAdminValidator.editValidate(hubCiti, result);

			if (result.hasErrors()) {
				return new ModelAndView("editadmin");
			}

			hubCiti.setUserID(new Integer(userId));

			/*
			 * To get un-associated postal code, City, State from HubCitiAdmin
			 * which is in Session. If more than one values, the postal codes,
			 * cities and states will be separated by comma (","). After getting
			 * values, it is removed from session.
			 */
			HubCiti removedPostalCodes = new HubCiti();
			if (session.getAttribute("removedPostalCodes") != null) {
				removedPostalCodes = (HubCiti) session.getAttribute("removedPostalCodes");
			}
			
			isValidEmail = Utility.validateEmailId(hubCiti.getEmailID());
			isValidSalesContact = Utility.validateEmailId(hubCiti.getSalesPersonEmail());
			

			final String[] strEmails = hubCiti.getEmails().split(";");
			Set<String> list = new LinkedHashSet<String>(Arrays.asList(strEmails));
			list.removeAll(Arrays.asList("", null));
			int icount = 0;
			
			if (list.size() > 5) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.NUMBER_OF_EMAILS);
				if (result.hasErrors()) {
					return new ModelAndView("editadmin");
				}
			}
			
			if (strEmails.length != list.size()) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATE_EMAILS);

				if (result.hasErrors()) {
					return new ModelAndView("editadmin");
				}
			}
			
			if (list.size() != 1) {
				for (int i = 0; i < list.size(); i++) {
					if (!Utility.validateEmailId(strEmails[i])) {
						++icount;
					} 
				}
			} else {
				if (strEmails.length > 1) {
					hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATE_EMAILS);

					if (result.hasErrors()) {
						return new ModelAndView("editadmin");
					}
				} else {
					isValidEmails = Utility.validateEmailId(hubCiti.getEmails());
					
					if (!isValidEmails) {
						hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALID_EMAIL);

						if (result.hasErrors()) {
							return new ModelAndView("editadmin");
						}
					}
				}
			}
			
			
			if (!isValidEmail) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDEMAIL);
			}
			
			if (!isValidSalesContact) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDSALESCONTACT);
			}
			
			if (!isValidEmails || (icount > 0)) {
				hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALID_EMAILS);
			}
			
			if (result.hasErrors()) {
				return new ModelAndView("editadmin");
			}

			StringBuilder sb = new StringBuilder();
			for (String emails : list) {
			    sb.append(emails).append(";");
			}
		
			hubCiti.setEmails(sb.toString());
		
			status = hubCitiService.saveAdmin(hubCiti, postalCodes, removedPostalCodes);
			
			if (result.hasErrors()) {
				return new ModelAndView("editadmin");
			}
			
			if (!Utility.isEmptyOrNullString(status)) {
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATEHUBCITI)) {
					hubCitiAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATEHUBCITI);

					if (result.hasErrors()) {
						return new ModelAndView("editadmin");
					}
				}

				if ("Success".equalsIgnoreCase(status)) {
					session.removeAttribute("selectedPostalCodeList");
					session.removeAttribute("removedPostalCodes");
					request.removeAttribute("state");
					request.removeAttribute("city");
					request.setAttribute("message", "HubCiti Admin Updated successfully.");
				} else {
					request.setAttribute("message", "Failed to Update HubCiti Admin.");
				}
			} else {
				request.setAttribute("message", "Failed to Update HubCiti Admin.");
			}
			citiDetails = hubCitiService.displayHubCiti(new Integer(userId), searchKey, lowerLimit, showDeactivated);

			session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
			session.setAttribute("adminlist", citiDetails.getHubCitis());
			objPage = Utility.getPagination(citiDetails.getTotalSize(), currentPage, "displayhubcitiadmin.htm", recordCount);
			session.setAttribute(ApplicationConstants.PAGINATION, objPage);

		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException();
		}

		session.setAttribute(SHOWDEACTIVATE, showDeactivated);
		HubCiti citi = new HubCiti();
		citi.setSearchKey(searchKey);
		model.put("hubciti", citi);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayhubcitiadmin");
	}

	/**
	 * This method will return hubciti associated postal codes along with city
	 * and state.
	 * 
	 * @param hubCiti
	 * @param request
	 * @param response
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/fetchpostalcode.htm", method = RequestMethod.GET)
	final ModelAndView getZipCodes(@ModelAttribute("hubciti") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getZipCodes";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<HubCiti> postalCodes = null;
		String city = null;
		String state = null;
		String postalCode = null;
		Boolean containsState = false;
		Boolean containsCity = false;
		final ArrayList<State> states = new ArrayList<State>();
		ArrayList<City> cities = null;

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

		postalCodes = hubCitiService.fetchAdminAssociatedPostalCode(hubCiti.getHubCitiID());

		if (null != postalCodes) {
			for (HubCiti citi : postalCodes) {
				containsState = false;
				containsCity = false;
				state = citi.getStateName();
				city = citi.getCity();
				postalCode = citi.getPostalCodes();

				for (State state2 : states) {
					if (state.equalsIgnoreCase(state2.getStateName())) {
						containsState = true;
						for (City city2 : state2.getCities()) {
							if (city.equalsIgnoreCase(city2.getCityName())) {
								containsCity = true;
								String tempPostalCode = city2.getPostalCode();
								tempPostalCode = tempPostalCode + "," + postalCode;
								city2.setPostalCode(tempPostalCode);
								break;
							}
						}
						if (!containsCity) {
							cities = state2.getCities();
							final City city2 = new City();
							city2.setCityName(city);
							city2.setPostalCode(postalCode);
							cities.add(city2);
							state2.setCities(cities);
						}
						break;
					}
				}
				if (!containsState) {
					cities = new ArrayList<City>();
					final City city2 = new City();
					city2.setCityName(city);
					city2.setPostalCode(postalCode);
					cities.add(city2);
					final State state2 = new State();
					state2.setStateName(state);
					state2.setCities(cities);
					states.add(state2);
				}
			}
		}

		session.setAttribute("postalCodes", states);
		model.put("postalCodes", postalCodes);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayzip");
	}

	/**
	 * This method will maintain the user selected zipcodes.
	 * 
	 * @param stateCode
	 * @param city
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/movezip.htm", method = RequestMethod.GET)
	public @ResponseBody
	final String addSelectedPostalCode(@RequestParam(value = "state", required = true) String state,
			@RequestParam(value = "city", required = true) String city, @RequestParam(value = "zip", required = true) String zip,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "addSelectedPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String key = state + "/" + city;
		final String[] tempPostalCodes = zip.split(",");
		String value = null;
		Boolean containsKey = false;
		@SuppressWarnings("unchecked")
		HashMap<String, String> postalCodeMap = (HashMap<String, String>) session.getAttribute("postalCodeMap");
		@SuppressWarnings("unchecked")
		ArrayList<String> defltPstlCdeLst = (ArrayList<String>) session.getAttribute("defltPstlCdeLst");

		if (null != postalCodeMap && !postalCodeMap.isEmpty()) {
			for (Entry<String, String> entry : postalCodeMap.entrySet()) {
				if (entry.getKey().equalsIgnoreCase(key)) {
					containsKey = true;
					value = entry.getValue();
					value += zip;
					entry.setValue(value);
				}
			}
			if (!containsKey) {
				postalCodeMap.put(key, zip);
			}
		} else {
			postalCodeMap = new HashMap<String, String>();
			postalCodeMap.put(key, zip);
		}
		
		for(String postalCode : tempPostalCodes) {
			if(null != defltPstlCdeLst) {
				defltPstlCdeLst.add(postalCode);
			} else {
				defltPstlCdeLst = new ArrayList<String>();
				defltPstlCdeLst.add(postalCode);
			}
		}

		session.setAttribute("postalCodeMap", postalCodeMap);
		session.setAttribute("defltPstlCdeLst", defltPstlCdeLst);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return ApplicationConstants.SUCCESS;
	}

	/**
	 * This method is used to de-associate the zipcode.
	 * 
	 * @param stateCode
	 * @param city
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/removezip.htm", method = RequestMethod.GET)
	public @ResponseBody
	final String removeSelectedPostalCode(@RequestParam(value = "zip", required = true) String zip, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "removeSelectedPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String[] tempPostalCodes = zip.split(",");
		final String zipCode = tempPostalCodes[0];
		final String key = tempPostalCodes[2] + "/" + tempPostalCodes[1];
		String value = null;
		boolean removeKey = false;
		@SuppressWarnings("unchecked")
		final HashMap<String, String> postalCodeMap = (HashMap<String, String>) session.getAttribute("postalCodeMap");
		@SuppressWarnings("unchecked")
		ArrayList<String> defltPstlCdeLst = (ArrayList<String>) session.getAttribute("defltPstlCdeLst");
		
		defltPstlCdeLst.remove(zipCode);

		/*
		 * Getting unassociated postal code, city, state from HubCiti Admin.
		 */
		HubCiti removePostalCode = new HubCiti();
		removePostalCode.setPostalCodes(tempPostalCodes[0]);
		removePostalCode.setCity(tempPostalCodes[1]);
		removePostalCode.setState(tempPostalCodes[2]);

		/*
		 * Adding unassociated postal code, city, state from HubCiti Admin to
		 * Session. If it exist in Session, values are inserted separated by
		 * comma (","). If dose not exist, new session parameter is created and
		 * added.
		 */
		if (session.getAttribute("removedPostalCodes") == null) {
			session.setAttribute("removedPostalCodes", removePostalCode);
		} else {
			HubCiti postalCodes = new HubCiti();
			postalCodes = (HubCiti) session.getAttribute("removedPostalCodes");
			postalCodes.setPostalCodes(postalCodes.getPostalCodes() + "," + removePostalCode.getPostalCodes());
			postalCodes.setCity(postalCodes.getCity() + "," + removePostalCode.getCity());
			postalCodes.setState(postalCodes.getState() + "," + removePostalCode.getState());
		}

		if (null != postalCodeMap && !postalCodeMap.isEmpty()) {
			for (Entry<String, String> entry : postalCodeMap.entrySet()) {
				if (entry.getKey().equalsIgnoreCase(key)) {
					value = entry.getValue().replaceAll(zipCode, "");
					value = value.replace(",,", ",");
					if (value.startsWith(",")) {
						value = value.substring(1, value.length());
					}
					if (!"".equals(value)) {
						entry.setValue(value);
					} else {
						removeKey = true;
					}
				}

			}
			if (removeKey) {
				postalCodeMap.remove(key);
			}
		}

		session.setAttribute("postalCodeMap", postalCodeMap);
		session.setAttribute("defltPstlCdeLst", defltPstlCdeLst);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return ApplicationConstants.SUCCESS;
	}

	public ArrayList<HubCiti> iterateSelectedPostalCode(HttpSession session) {
		final String methodName = "iterateSelectedPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		@SuppressWarnings("unchecked")
		final HashMap<String, String> postalCodeMap = (HashMap<String, String>) session.getAttribute("postalCodeMap");
		ArrayList<PostalCode> selectedPostalCodeList = null;
		ArrayList<HubCiti> postalCodes = null;

		if (null != postalCodeMap && !postalCodeMap.isEmpty()) {
			selectedPostalCodeList = new ArrayList<PostalCode>();
			postalCodes = new ArrayList<HubCiti>();
			for (Entry<String, String> entry : postalCodeMap.entrySet()) {
				String key = entry.getKey();
				final String[] tempkey = key.split("/");
				String state = tempkey[0];
				String city = tempkey[1];
				String value = entry.getValue();

				HubCiti citi = new HubCiti();
				citi.setState(state);
				citi.setCity(city);
				citi.setPostalCodes(value);

				postalCodes.add(citi);

				List<String> items = Arrays.asList(value.split(","));
				for (String postalCode : items) {
					PostalCode code = new PostalCode();
					code.setPostalCode(postalCode);
					code.setLocation(postalCode + "," + city + "," + state);
					selectedPostalCodeList.add(code);
				}

			}
			session.setAttribute("selectedPostalCodeList", selectedPostalCodeList);
		} else {
			session.setAttribute("selectedPostalCodeList", null);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return postalCodes;
	}

	/**
	 * This method is used to display and search event categories
	 * 
	 * @param category
	 * @param result
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */

	@RequestMapping(value = "/eventcategory.htm", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView displayEventCategory(@ModelAttribute("EventCategoryHome") EventCategory category, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException {

		String strMethodName = "displayEventCategory";
		String strViewName = "eventcategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		EventCategory objCategory = new EventCategory();
		AlertCategory objAlertCategory = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.CATEGORYADMIN);
//		String strActionType = null;
		if (null != request && request.getMethod().equals("GET")) {
			category.setCatName(null);
		}
		// for pagination.
		final String pageFlag = request.getParameter("pageFlag");
		final String userId = (String) session.getAttribute("UserId");
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		String strType = null;
		try {
			session.removeAttribute("eventcatlst");

			strType = request.getParameter("type");
			if (!Utility.isEmptyOrNullString(strType)) {
				if (strType.equals("add")) {
					final String successMsg = "Event category created successfully";
					request.setAttribute("successMsg", successMsg);
				} else {
					final String successMsg = "Event category updated successfully";
					request.setAttribute("successMsg", successMsg);
				}

			}

			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {
				currentPage = (lowerLimit + recordCount) / recordCount;
			}

			objCategory.setLowerLimit(lowerLimit);
			if (null != category) {
				if (null != category.getCatName() && !"".equals(category.getCatName())) {
					objCategory.setCatName(category.getCatName());
					request.setAttribute("searchEvntCat", "searchEvntCat");
				}
			}

			objAlertCategory = hubCitiService.fetchEventCategories(objCategory, userId);

			if (null != objAlertCategory) {

				if (null != objAlertCategory.getTotalSize()) {
					objPage = Utility.getPagination(objAlertCategory.getTotalSize(), currentPage, "eventcategory.htm", recordCount);
					session.setAttribute("pagination", objPage);
				} else {
					session.removeAttribute("pagination");
				}
			}

			if (null != category) {
				request.setAttribute("catdelete", category.getActionType());
			}

			session.setAttribute("searckey", category.getCatName());
			model.put("EventCategoryHome", category);
			session.setAttribute("eventcatlst", objAlertCategory);

		} catch (ScanSeeServiceException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	/**
	 * This method is used to open event category popup.
	 * 
	 * @param category
	 * @param result
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/openevtcategory.htm", method = RequestMethod.GET)
	public ModelAndView openEvtCategory(@ModelAttribute("EventCategoryHome") EventCategory category, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException {
		String strMethodName = "displayEventCategory";
		String strViewName = "addevtcategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		int cateId = 0;
		AlertCategory objAlertCategory = null;
		List<EventCategory> categoryLst = null;
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		if (null != category.getCatId() && !"".equals(category.getCatId())) {

			cateId = category.getCatId();
			request.setAttribute("cateId", cateId);
			objAlertCategory = (AlertCategory) session.getAttribute("eventcatlst");
			if (objAlertCategory != null) {
				categoryLst = objAlertCategory.getAlertCatLst();
				if (null != categoryLst && !"".equals(categoryLst)) {
					for (EventCategory objCate : categoryLst) {
						int catId = objCate.getCatId();
						if (catId == cateId) {
							request.setAttribute("catename", objCate.getCatName());
							break;
						}

					}
				}
			}

		} else {
			request.setAttribute("cateId", null);
			request.setAttribute("catename", null);
		}

		model.put("EventCategoryForm", category);
		return new ModelAndView(strViewName);
	}

	/**
	 * This method is used to delete event category
	 * 
	 * @param cateId
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/eventcatedelete.htm", method = RequestMethod.GET)
	public @ResponseBody
	String deleteEventCategory(@RequestParam(value = "cateId", required = true) int cateId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException {
		String strResponse = null;
		String strMethodName = "deleteEventCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
//		AlertCategory objAlertCategory = null;
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
//		List<EventCategory> categoryLst = null;

		try {

			strResponse = hubCitiService.deleteEventCategory(cateId);

		} catch (ScanSeeServiceException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strResponse;
	}

	/**
	 * This method is used to add event categories
	 * 
	 * @param catName
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/saveeventcate.htm", method = RequestMethod.POST)
	public ModelAndView saveEvtCateInfo(@ModelAttribute("EventCategoryForm") EventCategory eventCategory, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		String strResponse = null;
		String strViewName = "eventaddcategory";
		String strMethodName = "saveEvtCateInfo";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		// session.setAttribute(ApplicationConstants.MODULENAME,
		// ApplicationConstants.HUBCITIADMIN);
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.CATEGORYADMIN);
//		AlertCategory objAlertCategory = null;
		try {
			EventCategory eventCategoryObj = new EventCategory();
//			objAlertCategory = (AlertCategory) session.getAttribute("eventcatlst");
			final String userId = (String) session.getAttribute("UserId");

			if (!Utility.isEmptyOrNullString(userId)) {
				eventCategoryObj.setAdminId(userId);
			}
			if (null != eventCategory) {
				if (!Utility.isEmptyOrNullString(eventCategory.getCatName())) {
					eventCategoryObj.setCatName(eventCategory.getCatName());
				}
				if (!Utility.isEmptyOrNullString(eventCategory.getCatName())) {

					eventCategoryObj.setCateImgName(eventCategory.getCateImgName());
				}
			}
			strResponse = hubCitiService.addEventCategory(eventCategoryObj);

			if (!Utility.isEmptyOrNullString(strResponse)) {
				if (strResponse.equals(ApplicationConstants.ALERTCATEXISTS)) {
					final String existsMsg = "Event category already exists";
					request.setAttribute("existsMsg", existsMsg);

				} else {
					// final String successMsg =
					// "Event category created successfully";
					// request.setAttribute("successMsg", successMsg);
					return new ModelAndView(new RedirectView("eventcategory.htm?type=add"));
				}

			} else {
				final String errorMsg = "Error occurred while creating event category";
				request.setAttribute("errorMsg", errorMsg);
			}

		} catch (ScanSeeServiceException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	/**
	 * This method is used to navigate to add event category screen.
	 */
	@RequestMapping(value = "addeventcate.htm", method = RequestMethod.GET)
	public ModelAndView addEventCategory(@ModelAttribute("EventCategoryForm") EventCategory eventCategory, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {

		String strMethodName = "addEventCategory";
		String strViewName = "eventaddcategory";
		session.setAttribute("imageCropPage", "addEvent");
		session.setAttribute("minCropWd", 46);
		session.setAttribute("minCropHt", 46);
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		session.removeAttribute("cropImageSource");
		session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
		// session.setAttribute(ApplicationConstants.MODULENAME,
		// ApplicationConstants.HUBCITIADMIN);
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.CATEGORYADMIN);

		EventCategory categoryObj = new EventCategory();
		model.put("EventCategoryForm", categoryObj);

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	/**
	 * This method is used to update event categories
	 * 
	 * @param catName
	 * @param cateId
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */

	@RequestMapping(value = "/updateeventcat.htm", method = RequestMethod.POST)
	public ModelAndView updateEventCategory(@ModelAttribute("EventCategoryForm") EventCategory eventCategory, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException

	{
		String strMethodName = "updateEventCategory";
		String strViewName = "eventupdatecategory";
		String strResponse = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String evtCatImgName = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		session.removeAttribute("eventcatlst");
		try {

			final String userId = (String) session.getAttribute("UserId");
			/*
			 * if (null != eventCategory.getCateImgPath() &&
			 * !eventCategory.getCateImgPath().equals("")) { if
			 * (eventCategory.getCateImgPath().getOriginalFilename() == null ||
			 * eventCategory.getCateImgPath().getOriginalFilename().equals(""))
			 * eventCategory.setCateImgName(eventCategory.getCateImg()); }
			 */
			if (!Utility.isEmptyOrNullString(eventCategory.getCateImgName())) {
				evtCatImgName = FilenameUtils.getName(eventCategory.getCateImgName());
				eventCategory.setCateImgName(evtCatImgName);

				evtCatImgName = FilenameUtils.getName(eventCategory.getOldCateImgName());
				eventCategory.setCateImgName(evtCatImgName);
			}

			strResponse = hubCitiService.updateEventCategory(eventCategory, userId);

			if (!Utility.isEmptyOrNullString(strResponse)) {
				if (strResponse.equals(ApplicationConstants.ALERTCATEXISTS)) {
					final String existsMsg = "Event category already exists";
					request.setAttribute("existsMsg", existsMsg);

				} else {
					// final String successMsg =
					// "Event category updated successfully";
					// request.setAttribute("successMsg", successMsg);
					// strViewName = "editeventcate.htm?catId=" +
					// eventCategory.getCatId();
					return new ModelAndView(new RedirectView("eventcategory.htm?type=update"));
				}

			} else {
				final String errorMsg = "Error occurred while updating event category";
				request.setAttribute("errorMsg", errorMsg);
			}

			model.put("EventCategoryForm", eventCategory);
		} catch (ScanSeeServiceException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "cropImageGeneral.htm", method = RequestMethod.GET)
	public String cropImage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException {

		LOG.info("cropImage:: Inside Get Method");
		LOG.info("cropImage: Inside Exit Get Method");
		return "cropImage";

	}

	/**
	 * This method is used to navigate to add event category screen.
	 */
	@RequestMapping(value = "editeventcate.htm", method = RequestMethod.GET)
	public ModelAndView editEventCategory(@ModelAttribute("EventCategoryForm") EventCategory eventCategory, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {

		String strMethodName = "addEventCategory";
		String strViewName = "eventupdatecategory";
		session.setAttribute("minCropWd", 46);
		session.setAttribute("minCropHt", 46);
		session.setAttribute("imageCropPage", "editEvent");
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		session.removeAttribute("cropImageSource");
		session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		AlertCategory objAlertCategory = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		// session.setAttribute(ApplicationConstants.MODULENAME,
		// ApplicationConstants.HUBCITIADMIN);
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.CATEGORYADMIN);
		EventCategory categoryObj = new EventCategory();
		ArrayList<EventCategory> alertCatLst;
		session.removeAttribute("eventcatlst");
		session.removeAttribute("catId");
		String iCatId = request.getParameter("catId");
		final String userId = (String) session.getAttribute("UserId");
		if (!Utility.isEmptyOrNullString(iCatId)) {
			categoryObj.setCatId(Integer.parseInt(iCatId));
			session.setAttribute("catId", categoryObj.getCatId());

		}
		objAlertCategory = hubCitiService.fetchEventCategories(categoryObj, userId);
		if (null != objAlertCategory) {
			alertCatLst = objAlertCategory.getAlertCatLst();
			session.setAttribute("evtcatinfo", objAlertCategory);
			for (EventCategory eventCategory2 : alertCatLst) {
				categoryObj.setCatName(eventCategory2.getCatName());
				categoryObj.setCateImgName(eventCategory2.getCateImgName());
				session.setAttribute("cateImgPath", eventCategory2.getCateImgName());
				session.setAttribute("cateImg", eventCategory2.getCateImg());
			}
		}

		model.put("EventCategoryForm", categoryObj);

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	/**
	 * This method is used to display and search fundraiser categories.
	 * 
	 * @param category
	 * @param result
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "fundraiserCategory.htm", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView displayFundraiserCategory(@ModelAttribute("FundraiserCategoryHome") EventCategory category, BindingResult result,
			ModelMap model, HttpServletRequest request, HttpSession session) throws ScanSeeServiceException {
		String strMethodName = "displayFundraiserCategory";
		String strViewName = "fundraiserCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		if (null != request && request.getMethod().equals("GET")) {
			category.setCatName(null);
		}
		EventCategory objCategory = new EventCategory();
		AlertCategory objAlertCategory = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.CATEGORYADMIN);
		// for pagination.
		final String pageFlag = request.getParameter("pageFlag");
		final String userId = (String) session.getAttribute("UserId");
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		String strType = null;

		try {
			session.removeAttribute("fundraisercatlst");

			strType = request.getParameter("type");
			if (!Utility.isEmptyOrNullString(strType)) {
				if (strType.equals("add")) {
					final String successMsg = "Fundraiser category created successfully";
					request.setAttribute("successMsg", successMsg);
				} else if (strType.equals("update")) {
					final String successMsg = "Fundraiser category updated successfully";
					request.setAttribute("successMsg", successMsg);
				}

			}

			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {
				currentPage = (lowerLimit + recordCount) / recordCount;
			}

			objCategory.setLowerLimit(lowerLimit);
			if (null != category) {
				if (null != category.getCatName() && !"".equals(category.getCatName())) {
					objCategory.setCatName(category.getCatName());
					request.setAttribute("searchFundraiserCat", "searchFundraiserCat");
				}
			}

			objAlertCategory = hubCitiService.fetchFundraiserCategories(objCategory, userId);

			if (null != objAlertCategory) {
				if (null != objAlertCategory.getTotalSize()) {
					objPage = Utility.getPagination(objAlertCategory.getTotalSize(), currentPage, "fundraiserCategory.htm", recordCount);
					session.setAttribute("pagination", objPage);
				} else {
					session.removeAttribute("pagination");
				}
			}

			if (null != category) {
				request.setAttribute("catdelete", category.getActionType());
			}

			session.setAttribute("searckey", category.getCatName());
			model.put("FundraiserCategoryHome", category);
			session.setAttribute("fundraisercatlst", objAlertCategory);

		} catch (ScanSeeServiceException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		return new ModelAndView(strViewName);
	}

	/**
	 * This method is used to delete fundraiser category.
	 * 
	 * @param cateId
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/deletefundraisercat.htm", method = RequestMethod.GET)
	public @ResponseBody
	String deleteFundraiserCategory(@RequestParam(value = "cateId", required = true) int cateId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		String strResponse = null;
		String strMethodName = "deleteFundraiserCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

		try {
			strResponse = hubCitiService.deleteFundraiserCategory(cateId);
		} catch (ScanSeeServiceException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strResponse;
	}

	/**
	 * This method is used to go to add/update fundraiser category screen.
	 * 
	 * @param eventCategory
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "addFundraiserCategory.htm", method = RequestMethod.GET)
	public ModelAndView addFundraiserCategory(@ModelAttribute("FundraiserCategoryForm") EventCategory eventCategory, BindingResult result,
			ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {

		String strMethodName = "addFundraiserCategory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strViewName = "addFundriserCategory";
		session.setAttribute("imageCropPage", "addFundraiser");
		session.setAttribute("minCropWd", 46);
		session.setAttribute("minCropHt", 46);
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		session.removeAttribute("cropImageSource");
		session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.CATEGORYADMIN);

		ArrayList<EventCategory> alertCatLst;
		EventCategory objCategory = new EventCategory();
		EventCategory categoryObj = new EventCategory();
		if (eventCategory.getCatId() != null) {
			final String userId = (String) session.getAttribute("UserId");
			AlertCategory objAlertCategory = null;
			objCategory.setCatId(eventCategory.getCatId());
			objAlertCategory = hubCitiService.fetchFundraiserCategories(objCategory, userId);

			if (null != objAlertCategory) {
				alertCatLst = objAlertCategory.getAlertCatLst();
				// session.setAttribute("evtcatinfo", objAlertCategory);
				for (EventCategory eventCategory2 : alertCatLst) {
					categoryObj.setCatName(eventCategory2.getCatName());
					categoryObj.setCateImgName(eventCategory2.getCateImgName());
					categoryObj.setOldCateImgName(eventCategory2.getCateImgName());
					categoryObj.setCatId(eventCategory.getCatId());
					session.setAttribute("cateImgPath", eventCategory2.getCateImgName());
					session.setAttribute("cateImg", eventCategory2.getCateImg());
				}
			}
		} else {
			categoryObj = new EventCategory();
		}
		model.put("FundraiserCategoryForm", categoryObj);

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	/**
	 * This method is used to save new fundraiser category.
	 * 
	 * @param eventCategory
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/saveFundraiserCat.htm", method = RequestMethod.POST)
	public ModelAndView saveFundraiserCategoryInfo(@ModelAttribute("FundraiserCategoryForm") EventCategory eventCategory, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		String strResponse = null;
		String strViewName = "addFundriserCategory";
		String strMethodName = "saveFundraiserCategoryInfo";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.CATEGORYADMIN);
		// AlertCategory objAlertCategory = null;
		String catImgName = null;
		Integer catId = null;
		try {
			EventCategory eventCategoryObj = new EventCategory();
			// objAlertCategory = (AlertCategory)
			// session.getAttribute("fundraisercatlst");
			final String userId = (String) session.getAttribute("UserId");

			if (!Utility.isEmptyOrNullString(userId)) {
				eventCategoryObj.setAdminId(userId);
			}
			if (null != eventCategory) {
				if (!Utility.isEmptyOrNullString(eventCategory.getCatName())) {
					eventCategoryObj.setCatName(eventCategory.getCatName());
				}

				if (!Utility.isEmptyOrNullString(eventCategory.getCateImgName())) {
					catImgName = FilenameUtils.getName(eventCategory.getCateImgName());
					eventCategoryObj.setCateImgName(catImgName);
				}

				if (!Utility.isEmptyOrNullString(eventCategory.getOldCateImgName())) {
					catImgName = FilenameUtils.getName(eventCategory.getOldCateImgName());
					eventCategoryObj.setOldCateImgName(catImgName);
				}

				if (!Utility.isEmptyOrNullString(eventCategory.getCatName())) {
					catId = eventCategory.getCatId();
					eventCategoryObj.setCatId(eventCategory.getCatId());
				}
			}

			strResponse = hubCitiService.addFundraiserCategory(eventCategoryObj);

			if (!Utility.isEmptyOrNullString(strResponse)) {
				if (strResponse.equals(ApplicationConstants.ALERTCATEXISTS)) {
					final String existsMsg = "Fundraiser category already exists";
					request.setAttribute("existsMsg", existsMsg);
				} else {
					if (catId != null && !"".equals(catId)) {
						return new ModelAndView(new RedirectView("fundraiserCategory.htm?type=update"));
					} else {
						return new ModelAndView(new RedirectView("fundraiserCategory.htm?type=add"));
					}
				}
			} else {
				final String errorMsg = "Error occurred while creating fundraiser category";
				request.setAttribute("errorMsg", errorMsg);
			}
		} catch (ScanSeeServiceException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	/**
	 * Below method is used to fetch default postal codes list.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/getdefaultzipcodes.htm", method = RequestMethod.GET)
	@ResponseBody
	public String getDefaultPostalCodelst(@ModelAttribute() HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String strMethodName = "getDefaultPostalCodelst";
		ArrayList<HubCiti> defaultPostalCodeslst = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HubCitiService hubCitiService = (HubCitiService) webApplicationContext.getBean("hubCitiService");

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		JSONObject jsonObject = new JSONObject();
		JSONObject valueObject = null;
		JSONArray jsonArray = new JSONArray();
		try {

			defaultPostalCodeslst = hubCitiService.getDefaultPostalCodes();
			if (null != defaultPostalCodeslst && !defaultPostalCodeslst.isEmpty()) {
				valueObject = new JSONObject();
				response.setContentType("application/json");
				for (HubCiti hubCiti : defaultPostalCodeslst) {

					String value = hubCiti.getPostalCodes();
					valueObject.put("label", hubCiti.getPostalCodes());
					valueObject.put("value", value);
					jsonArray.put(valueObject);
				}

			} else {

				valueObject = new JSONObject();
				valueObject.put("label", "No Record Found");
				valueObject.put("value", "No Record Found");
			}

			jsonObject.put("postalCodes", jsonArray);
		} catch (ScanSeeServiceException exception) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName);
			throw new ScanSeeServiceException(exception);

		}
		return jsonObject.get("postalCodes").toString();
	}

}

/**
 * 
 */
package admin.controller;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import admin.service.ExportService;
import admin.service.HubCitiService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.HubCiti;
import common.pojo.Modules;
import common.pojo.RegionInfo;
import common.tags.Pagination;
import common.util.Utility;

/**
 * @author sangeetha.ts
 *
 */
@Controller
public class ExportController {
	
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ExportController.class);
	
	@RequestMapping(value = "/displayexport.htm", method = RequestMethod.GET)
	public ModelAndView displayExportData(@ModelAttribute("module") Modules module, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "displayExportData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<HubCiti> hubcities = null;
		ArrayList<Modules> modules = null;
		ArrayList<Modules> exportOptions = null;
		int moduleId = 0;
		String moduleName = null;
		
		try {
			
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final ExportService exportService = (ExportService) appContext.getBean(ApplicationConstants.EXPORTSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.EXPORTADMIN);

			hubcities = (ArrayList<HubCiti>) exportService.getExportHubCities();
			modules = (ArrayList<Modules>) exportService.getExportType();
			
			if(module.getModuleTypeId() != 0) {
				moduleId = module.getModuleTypeId();
				moduleName = module.getModuleTypeName();
			} else {
				moduleId = modules.get(0).getModuleTypeId();
				moduleName = modules.get(0).getModuleTypeName();
			}
			
			exportOptions = (ArrayList<Modules>) exportService.getExportOptions(moduleId);

			session.setAttribute("hubcities", hubcities);
			session.setAttribute("modules", modules);
			session.setAttribute("exportOptions", exportOptions);
			session.setAttribute("selectedModule", moduleId);
			session.setAttribute("selectedModuleName", moduleName);
			
			Calendar now = Calendar.getInstance();
			String toDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);
			now.add(Calendar.MONTH, -1);
			String fromDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);
			
			module = new Modules();
			module.setFromDate(fromDate);
			module.setToDate(toDate);
			model.put("module", module);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayexport");
	}
	
	@RequestMapping(value = "/exportdata.htm", method = RequestMethod.POST)
	public ModelAndView exportData(@ModelAttribute("module") Modules module, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		final String methodName = "displayExportData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String msg = null;
		
		try {
			
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final ExportService exportService = (ExportService) appContext.getBean(ApplicationConstants.EXPORTSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.EXPORTADMIN);
			
			if("Dashboard Data".equals(module.getModuleTypeName())) {
				exportService.exportData(module);
				msg = "Dashboard data exported successfully.";
			} else if("Retailer Data".equals(module.getModuleTypeName())) {
				exportService.exportRetailerData(module);
				msg = "Retailer data exported successfully.";
			}			
			
			request.setAttribute("successmsg", msg);
			Calendar now = Calendar.getInstance();
			String toDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);
			now.add(Calendar.MONTH, -1);
			String fromDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);
			
			module = new Modules();
			module.setFromDate(fromDate);
			module.setToDate(toDate);
			model.put("module", module);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayexport");
	}
	
	@RequestMapping(value = "/displayexporthubcities.htm", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView displayExportHubcities(@ModelAttribute("exporthubcities") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		final String methodName = "displayExportHubcities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		RegionInfo hubcities = null;
		final String searchKey = hubCiti.getSearchKey();
		final String pageFlag = request.getParameter("pageFlag");
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		
		try {
			
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final ExportService exportService = (ExportService) appContext.getBean(ApplicationConstants.EXPORTSERVICE);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			session.setAttribute("moduleName", ApplicationConstants.EXPORTADMIN);
			
			final String userId = (String) session.getAttribute("UserId");
			if (!Utility.isEmptyOrNullString(userId)) {
				hubCiti.setUserID(Integer.parseInt(userId));
			}
			
			if (null != hubCiti.getLowerLimit()) {
				lowerLimit = hubCiti.getLowerLimit();				
			}
			
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {
				currentPage = (lowerLimit + recordCount) / recordCount;
			}
			
			hubCiti.setLowerLimit(lowerLimit);
			
			if(null == searchKey || "".equals(searchKey)) {
				hubCiti.setSearchKey(null);
			}
			
			if(null != hubCiti.getIsDataExport() && hubCiti.getIsDataExport()) {
				exportService.saveExportHubCities(hubCiti);
			}
			
			hubcities = hubCitiService.getHubCitiRegions(hubCiti);
			
			if (null != hubcities && null != hubcities.getRegionLst() && !hubcities.getRegionLst().isEmpty()) {
				session.setAttribute("hubcities", hubcities.getRegionLst());
				objPage = Utility.getPagination(hubcities.getTotalSize(), currentPage, "displayexporthubcities.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				request.setAttribute("errorMessage", "No HubCiti / Region Found.");
				session.setAttribute("hubcities", null);
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}

			request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
			HubCiti citi = new HubCiti();
			citi.setSearchKey(searchKey);
			citi.setUnChkdNatId(hubCiti.getUnChkdNatId());
			model.put("exporthubcities", citi);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("exporthubcities");
	}
	
	@RequestMapping(value = "/saveexporthubcities.htm", method = RequestMethod.POST)
	public ModelAndView saveExportHubcities(@ModelAttribute("exporthubcities") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		final String methodName = "saveExportHubcities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		RegionInfo hubcities = null;
		final String searchKey = hubCiti.getSearchKey();
		Integer lowerLimit = 0;
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		
		try {
			
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final ExportService exportService = (ExportService) appContext.getBean(ApplicationConstants.EXPORTSERVICE);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			session.setAttribute("moduleName", ApplicationConstants.EXPORTADMIN);
			
			exportService.saveExportHubCities(hubCiti);
			
			final String userId = (String) session.getAttribute("UserId");
			if (!Utility.isEmptyOrNullString(userId)) {
				hubCiti.setUserID(Integer.parseInt(userId));
			}
			
			if (null != hubCiti.getLowerLimit()) {
				lowerLimit = hubCiti.getLowerLimit();
			}
			
			currentPage = (lowerLimit + recordCount) / recordCount;
			
			hubCiti.setLowerLimit(lowerLimit);
			
			if(null == searchKey || "".equals(searchKey)) {
				hubCiti.setSearchKey(null);
			}
			
			hubcities = hubCitiService.getHubCitiRegions(hubCiti);
			
			if (null != hubcities && null != hubcities.getRegionLst() && !hubcities.getRegionLst().isEmpty()) {
				session.setAttribute("hubcities", hubcities.getRegionLst());
				objPage = Utility.getPagination(hubcities.getTotalSize(), currentPage, "displayexporthubcities.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				request.setAttribute("errorMessage", "No HubCities Found.");
				session.setAttribute("hubcities", null);
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}

			request.setAttribute("successMsg", "Export Hubciti / Region saved successfully");
			request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
			HubCiti citi = new HubCiti();
			citi.setSearchKey(searchKey);
			citi.setUnChkdNatId(hubCiti.getUnChkdNatId());
			model.put("exporthubcities", citi);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("exporthubcities");
	}

}

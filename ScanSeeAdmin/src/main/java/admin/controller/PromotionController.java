package admin.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import admin.service.PromotionService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Promotion;
import common.pojo.PromotionDetails;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is used as a controller class for promotion admin.
 * 
 * @author sangeetha.ts
 */
@Controller
public class PromotionController {

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PromotionController.class);

	/**
	 * This method is used to display all the promotions with pagination.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/displaypromotion.htm", method = RequestMethod.POST)
	public ModelAndView displayPromotions(@ModelAttribute("admin") Promotion promotion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "displayPromotions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		PromotionDetails promotionDetails = null;
		final String searchKey = null;
		final String sortOrder = "ASC";
		final String columnName = "PageTitle";
		Integer lowerLimit = 0;
		final String showExpire = "true";
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final PromotionService promotionService = (PromotionService) appContext.getBean(ApplicationConstants.PROMOTIONSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.PROMOTIONADMIN);

			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			promotionDetails = new PromotionDetails();
			promotionDetails = promotionService.getPromotionList(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			for (Promotion ipromotion : promotionDetails.getPromotions()) {
				ipromotion.setShortDescription(ipromotion.getShortDescription().replaceAll("<[^>]+>", ""));
			}

			session.removeAttribute("searchKey");
			session.setAttribute("PromotionList", promotionDetails.getPromotions());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("PromotionNameImg", "images/sorticonUp.png");
			session.setAttribute("QuantityImg", "");
			session.setAttribute("RetailNameImg", "");
			session.setAttribute("StartDateImg", "");
			session.setAttribute("EndDateImg", "");
			session.setAttribute("sortOrderPageTitle", "DESC");
			session.setAttribute("sortOrderQuantity", "ASC");
			session.setAttribute("sortOrderRetailName", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");

			objPage = Utility.getPagination(promotionDetails.getTotalSize(), currentPage, "displaypromotion.htm", recordCount);
			session.setAttribute("pagination", objPage);
			session.setAttribute("columnName", "PageTitle");

		} catch (ScanSeeServiceException e) {
			throw new ScanSeeServiceException(e);
		}

		model.put("promotion", promotion);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("promotionList");
	}

	/**
	 * This method is to display expired Promotions.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/showexpiredpromotion.htm", method = RequestMethod.POST)
	public ModelAndView showExpiredPromotions(@ModelAttribute("promotion") Promotion promotion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "showExpiredPromotions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		PromotionDetails promotionDetails = null;
		final String searchKey = (String) session.getAttribute("searchKey");
		final String sortOrder = "ASC";
		final String columnName = "PageTitle";
		Integer lowerLimit = 0;
		final String showExpire = promotion.getShowExpire();
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final PromotionService promotionService = (PromotionService) appContext.getBean(ApplicationConstants.PROMOTIONSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.PROMOTIONADMIN);
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			promotionDetails = new PromotionDetails();
			promotionDetails = promotionService.getPromotionList(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			for (Promotion ipromotion : promotionDetails.getPromotions()) {
				ipromotion.setShortDescription(ipromotion.getShortDescription().replaceAll("<[^>]+>", ""));
			}

			session.setAttribute("PromotionList", promotionDetails.getPromotions());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("PromotionNameImg", "images/sorticonUp.png");
			session.setAttribute("QuantityImg", "");
			session.setAttribute("RetailNameImg", "");
			session.setAttribute("StartDateImg", "");
			session.setAttribute("EndDateImg", "");
			session.setAttribute("sortOrderPageTitle", "DESC");
			session.setAttribute("sortOrderQuantity", "ASC");
			session.setAttribute("sortOrderRetailName", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");

			objPage = Utility.getPagination(promotionDetails.getTotalSize(), currentPage, "showexpiredpromotion.htm", recordCount);
			session.setAttribute("pagination", objPage);
			session.setAttribute("columnName", "PageTitle");

		} catch (ScanSeeServiceException e) {
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("promotionList");
	}

	/**
	 * This method is used for promtion search based on the promotion name as a
	 * search key
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/searchpromotion.htm", method = RequestMethod.POST)
	public ModelAndView searchPromotions(@ModelAttribute("promotion") Promotion promotion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "searchPromotions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		PromotionDetails promotionDetails = null;
		final String searchKey = promotion.getSearchKey();
		final String sortOrder = "ASC";
		final String columnName = "PageTitle";
		Integer lowerLimit = 0;
		final String showExpire = promotion.getShowExpire();
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final PromotionService promotionService = (PromotionService) appContext.getBean(ApplicationConstants.PROMOTIONSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.PROMOTIONADMIN);
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			promotionDetails = new PromotionDetails();
			promotionDetails = promotionService.getPromotionList(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			for (Promotion ipromotion : promotionDetails.getPromotions()) {
				ipromotion.setShortDescription(ipromotion.getShortDescription().replaceAll("<[^>]+>", ""));
			}

			if (promotionDetails.getPromotions().isEmpty()) {
				final String errorMsg = "No Promotions Found";
				request.setAttribute("errorMessage", errorMsg);
			}

			session.setAttribute("PromotionList", promotionDetails.getPromotions());
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("PromotionNameImg", "images/sorticonUp.png");
			session.setAttribute("QuantityImg", "");
			session.setAttribute("RetailNameImg", "");
			session.setAttribute("StartDateImg", "");
			session.setAttribute("EndDateImg", "");
			session.setAttribute("sortOrderPageTitle", "DESC");
			session.setAttribute("sortOrderQuantity", "ASC");
			session.setAttribute("sortOrderRetailName", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");

			objPage = Utility.getPagination(promotionDetails.getTotalSize(), currentPage, "searchpromotion.htm", recordCount);
			session.setAttribute("pagination", objPage);
			session.setAttribute("columnName", "PageTitle");

		} catch (ScanSeeServiceException e) {
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("promotionList");
	}

	/**
	 * This method will display the details of the promotion .
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/displaypromotiondetails.htm", method = RequestMethod.POST)
	public ModelAndView displayPromotionDetails(@ModelAttribute("promotion") Promotion promotion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "displayPromotionDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		PromotionDetails promotionDetails = null;
		Promotion promotionDetail = null;
		final Integer promotionId = promotion.getPromotionId();
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final PromotionService promotionService = (PromotionService) appContext.getBean(ApplicationConstants.PROMOTIONSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.PROMOTIONADMIN);
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}
			promotionDetail = new Promotion();
			promotionDetail = promotionService.displayPromotionDetails(promotionId);
			promotionDetail.setShortDescription(promotionDetail.getShortDescription().replaceAll("<[^>]+>", ""));
			promotionDetail.setLongDescription(promotionDetail.getLongDescription().replaceAll("<[^>]+>", ""));
			session.setAttribute("promotionDetails", promotionDetail);

			promotionDetails = new PromotionDetails();
			promotionDetails = promotionService.displayPromotionUsers(promotionId, lowerLimit);

			if (promotionDetails.getTotalSize() == 0) {
				final String errorMsg = "No Users Found";
				request.setAttribute("errorMessage", errorMsg);
			}
			session.setAttribute("promotionUsers", promotionDetails.getPromotions());
			objPage = Utility.getPagination(promotionDetails.getTotalSize(), currentPage, "displaypromotiondetails.htm", recordCount);
			session.setAttribute("pagination", objPage);
			request.setAttribute("promotionId", promotionId);

		} catch (ScanSeeServiceException e) {
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("promotionDetails");
	}

	/**
	 * This method will filter the promotions.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/filterpromotion.htm", method = RequestMethod.POST)
	public ModelAndView filterPromotions(@ModelAttribute("promotion") Promotion promotion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "filterPromotions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		PromotionDetails promotionDetails = null;
		final String searchKey = (String) session.getAttribute("searchKey");
		final String sortOrder = "ASC";
		final String columnName = "PageTitle";
		Integer lowerLimit = 0;
		final String showExpire = "true";
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		final String promotionIds = promotion.getPromotionIds();
		String status = null;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final PromotionService promotionService = (PromotionService) appContext.getBean(ApplicationConstants.PROMOTIONSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.PROMOTIONADMIN);
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			status = promotionService.filterPromotions(promotionIds);

			if ("Success".equalsIgnoreCase(status)) {
				request.setAttribute("message", "Promotion updated successfully");
			} else {
				request.setAttribute("message", "Failed to update Promotion");
			}

			promotionDetails = new PromotionDetails();
			promotionDetails = promotionService.getPromotionList(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			for (Promotion ipromotion : promotionDetails.getPromotions()) {
				ipromotion.setShortDescription(ipromotion.getShortDescription().replaceAll("<[^>]+>", ""));
			}

			session.setAttribute("PromotionList", promotionDetails.getPromotions());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("columnName", columnName);
			session.setAttribute("PromotionNameImg", "images/sorticonUp.png");
			session.setAttribute("QuantityImg", "");
			session.setAttribute("RetailNameImg", "");
			session.setAttribute("StartDateImg", "");
			session.setAttribute("EndDateImg", "");
			session.setAttribute("sortOrderPageTitle", "DESC");
			session.setAttribute("sortOrderQuantity", "ASC");
			session.setAttribute("sortOrderRetailName", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");

			objPage = Utility.getPagination(promotionDetails.getTotalSize(), currentPage, "displaypromotion.htm", recordCount);
			session.setAttribute("pagination", objPage);

		} catch (ScanSeeServiceException e) {
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("promotionList");
	}

	/**
	 * This method is used to sort the Promotions based on the given column
	 * name.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/sortpromotion.htm", method = RequestMethod.POST)
	public ModelAndView sortPromotions(@ModelAttribute("promotion") Promotion promotion, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "sortPromotions";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		PromotionDetails promotionDetails = null;
		final String searchKey = promotion.getSearchKey();
		String sortOrder = null;
		final String columnName = promotion.getColumnName();
		Integer lowerLimit = 0;
		final String showExpire = promotion.getShowExpire();
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final PromotionService promotionService = (PromotionService) appContext.getBean(ApplicationConstants.PROMOTIONSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.PROMOTIONADMIN);
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				if ("PageTitle".equals(columnName)) {
					sortOrder = (String) session.getAttribute("sortOrderPageTitle");

					if ("ASC".equalsIgnoreCase(sortOrder)) {
						sortOrder = "DESC";
					} else {
						sortOrder = "ASC";
					}
				}
			} else {

				if ("PageTitle".equals(columnName)) {
					sortOrder = (String) session.getAttribute("sortOrderPageTitle");
				}
			}

			promotionDetails = new PromotionDetails();
			promotionDetails = promotionService.getPromotionList(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			for (Promotion ipromotion : promotionDetails.getPromotions()) {
				ipromotion.setShortDescription(ipromotion.getShortDescription().replaceAll("<[^>]+>", ""));
			}
			session.setAttribute("PromotionList", promotionDetails.getPromotions());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("columnName", columnName);
			if ("PageTitle".equals(columnName)) {
				if ("ASC".equals(sortOrder)) {
					session.setAttribute("PromotionNameImg", "images/sorticonUp.png");
					session.setAttribute("QuantityImg", "");
					session.setAttribute("RetailNameImg", "");
					session.setAttribute("StartDateImg", "");
					session.setAttribute("EndDateImg", "");
					session.setAttribute("sortOrderPageTitle", "DESC");
				} else {
					session.setAttribute("PromotionNameImg", "images/sorticon.png");
					session.setAttribute("QuantityImg", "");
					session.setAttribute("RetailNameImg", "");
					session.setAttribute("StartDateImg", "");
					session.setAttribute("EndDateImg", "");
					session.setAttribute("sortOrderPageTitle", "ASC");
				}
			}

			objPage = Utility.getPagination(promotionDetails.getTotalSize(), currentPage, "sortpromotion.htm", recordCount);
			session.setAttribute("pagination", objPage);

		} catch (ScanSeeServiceException e) {
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("promotionList");
	}
}

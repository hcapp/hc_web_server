package admin.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.EventCategory;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationDetails;
import common.util.Utility;

/**
 * This controller class is used to crop the image.
 * 
 * @author shyamsundara_hm
 */
@Controller
public class ImageCropController {

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ImageCropController.class);

	@RequestMapping(value = "/uploadImage.htm", method = RequestMethod.POST)
	public ModelAndView onSubmitImage(@ModelAttribute("EventCategoryForm") EventCategory eventCategory, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException

	{
		String strMethodName = "";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		final boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");
		String imageFileName = null;
		InputStream inputStream = null;

		try {
			inputStream = eventCategory.getCateImgPath().getInputStream();
			imageFileName = eventCategory.getCateImgPath().getOriginalFilename();
			session.setAttribute("minCropHt", 46);
			session.setAttribute("minCropWd", 46);
			imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, inputStream);

			if (imageSizeValFlg) {

				response.getWriter().write("<imageScr>" + "maxSizeImageError" + "</imageScr>");
				return null;
			} else {

				inputStream = eventCategory.getCateImgPath().getInputStream();
				@SuppressWarnings("deprecation")
				final BufferedImage img = Utility.getBufferedImageForMinDimension(inputStream, request.getRealPath("images"), imageFileName);
				w = img.getWidth(null);
				h = img.getHeight(null);
				session.setAttribute("imageHt", h);
				session.setAttribute("imageWd", w);

				final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
				// session.removeAttribute("welcomePageBtnVal");
				imageSource = imageFileName;
				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png" + "?" + date.getTime();
				final String filePath = tempImgPath + fileSeparator + imageFileName;
				// Utility.writeFileData(objRetLocAds.getBannerAdImagePath(),
				// filePath);
				Utility.writeImage(img, filePath);
				if (imageValidSizeValFlg) {
					strResponse.append("ValidImageDimention");
					strResponse.append("|" + imageFileName);

					session.setAttribute("welcomePageCreateImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
							+ imageSource);
					session.setAttribute("cateImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);

				}
				session.setAttribute("cateImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);

				strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");

				// Used for image cropping popup
				session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);

				// }
				if (null != inputStream) {

					inputStream.close();
				}

				LOG.info(ApplicationConstants.METHODEND + strMethodName);
				return null;
			}

		} catch (IOException exception) {
			LOG.error(exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}

	}

	@RequestMapping(value = "/uploadaddcroppedimage.htm", method = RequestMethod.POST)
	public String uploadCropAddCategoryImage(@ModelAttribute("EventCategoryForm") EventCategory objCouponAdd, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside uploadCropAddCategoryImage : uploadCropAddCategoryImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		String strViewName = "eventaddcategory";

		objCouponAdd.setCateImgName(objCouponAdd.getCateImgPath().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objCouponAdd.getCateImgPath(), null, null);
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objCouponAdd.setCateImgName(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("cateImgPath", strImageName);
				}
			}
		} else {
			objCouponAdd.setCateImgName(null);
			session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
		}
		model.put("EventCategoryForm", objCouponAdd);
		return strViewName;
	}

	@RequestMapping(value = "/uploadeditcroppedimage.htm", method = RequestMethod.POST)
	public String uploadCropEditCategoryImage(@ModelAttribute("EventCategoryForm") EventCategory objCouponAdd, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside uploadCropAddCategoryImage : uploadCropAddCategoryImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		String strViewName = "eventupdatecategory";
		
		objCouponAdd.setCateImgName(objCouponAdd.getCateImgPath().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objCouponAdd.getCateImgPath(), null, null);
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objCouponAdd.setCateImgName(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("cateImgPath", strImageName);
				}
			}
		} else {
			objCouponAdd.setCateImgName(null);
			session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
		}
		model.put("EventCategoryForm", objCouponAdd);
		return strViewName;
	}

	/**
	 * This method saves the cropped image.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @param imageFile
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String saveCroppedImage(HttpServletRequest request, HttpServletResponse response, HttpSession session, CommonsMultipartFile imageFile,
			String screen, Integer retailId) throws ScanSeeServiceException {
		LOG.info("Inside saveCroppedImage");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		String outputFileName;
		final String status = "FALSE";
		final double parseX = Double.parseDouble(request.getParameter("x"));
		final double parseY = Double.parseDouble(request.getParameter("y"));
		final double parseW = Double.parseDouble(request.getParameter("w"));
		final double parseH = Double.parseDouble(request.getParameter("h"));
		final int xPixel = (int) parseX;
		final int yPixel = (int) parseY;
		final int wPixel = (int) parseW;
		final int hPixel = (int) parseH;

		try {

			@SuppressWarnings("deprecation")
			final String filePathImages = request.getRealPath("images");
			outputFileName = processRetailerCroppedImage(imageFile, filePathImages, xPixel, yPixel, wPixel, hPixel, screen, retailId);

		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in  UploadCroppedLogoController:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return outputFileName;
	}

	/**
	 * This will check the height and width of cropped image.
	 * 
	 * @param imageInfo
	 * @param realPath
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String processRetailerCroppedImage(CommonsMultipartFile imageInfo, String realPath, int x, int y, int w, int h, String screen,
			Integer retailId) throws ScanSeeServiceException {

		final String response = ApplicationConstants.SUCCESS;
		final String fileSeparator = System.getProperty("file.separator");
		final Random rand = new Random();
		int numNoRange = rand.nextInt(100000);
		String outputFileName;
		String sourceFileName;
		final StringBuilder mediaPathBuilder;
		try {

		/*	if (!Utility.isEmptyOrNullString(screen)) {
				mediaPathBuilder = Utility.getMediaLocationPath(ApplicationConstants.RETAILER, retailId);
			} else {*/
				mediaPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
			//}
			final String imgMediaPath = mediaPathBuilder.toString();
			// String imgMediaPath = realPath + fileSeparator + "flow_old";
			outputFileName = imageInfo.getOriginalFilename();
			sourceFileName = imageInfo.getOriginalFilename();
			if (!Utility.isEmptyOrNullString(outputFileName)) {
				outputFileName = FilenameUtils.removeExtension(imageInfo.getOriginalFilename());
				sourceFileName = FilenameUtils.removeExtension(imageInfo.getOriginalFilename());
				outputFileName = outputFileName + "_" + String.valueOf(numNoRange) + ".png";
				sourceFileName = sourceFileName + ".png";
			}
			LOG.info(" Images path ********************" + imgMediaPath + fileSeparator + outputFileName);
			// Update local Utiliy Code and uncomment below line for cropping
			// feature
			Utility.writeCroppedFileData(imageInfo, imgMediaPath + fileSeparator + sourceFileName, imgMediaPath + fileSeparator + outputFileName, x,
					y, w, h);

		} catch (Exception exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return outputFileName;
	}

	@RequestMapping(value = "/uploadretlocimg.htm", method = RequestMethod.POST)
	public ModelAndView onSubmitImage(@ModelAttribute("retailer") RetailerLocation objCouponAdd, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException

	{
		String strMethodName = "";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		final boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");
		String imageFileName = null;
		InputStream inputStream = null;
		int rowIndex = 0;

		try {

			objCouponAdd.getImageFile()[objCouponAdd.getRowIndex()].getInputStream();
			rowIndex = objCouponAdd.getRowIndex();
		} catch (Exception e) {
			rowIndex = 0;
		}

		try {

			inputStream = objCouponAdd.getImageFile()[rowIndex].getInputStream();
			imageFileName = objCouponAdd.getImageFile()[rowIndex].getOriginalFilename();
			imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, inputStream);

			if (imageSizeValFlg) {

				response.getWriter().write("<imageScr>" + "maxSizeImageError" + "</imageScr>");
				return null;
			} else {

				@SuppressWarnings("deprecation")
				final BufferedImage img = Utility.getBufferedImageForMinDimension(objCouponAdd.getImageFile()[rowIndex].getInputStream(),
						request.getRealPath("images"), objCouponAdd.getImageFile()[rowIndex].getOriginalFilename());
				w = img.getWidth(null);
				h = img.getHeight(null);
				session.setAttribute("imageHt", h);
				session.setAttribute("imageWd", w);

				final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
				// session.removeAttribute("welcomePageBtnVal");
				imageSource = imageFileName;
				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png" + "?" + date.getTime();
				final String filePath = tempImgPath + fileSeparator + objCouponAdd.getImageFile()[rowIndex].getOriginalFilename();
				// Utility.writeFileData(objRetLocAds.getBannerAdImagePath(),
				// filePath);
				Utility.writeImage(img, filePath);
				if (imageValidSizeValFlg) {
					strResponse.append("ValidImageDimention");
					strResponse.append("|" + objCouponAdd.getImageFile()[rowIndex].getOriginalFilename());
					session.setAttribute("locationImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);

				}
				strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
				session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);

				// }
				if (null != inputStream) {

					inputStream.close();
				}

				LOG.info(ApplicationConstants.METHODEND + strMethodName);
				return null;
			}

		} catch (IOException exception) {
			LOG.error(exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}

	}

	@RequestMapping(value = "/uploadretloccroppedimage.htm", method = RequestMethod.POST)
	public String uploadRetailLocCroppedImage(@ModelAttribute("retailer") RetailerLocation objCouponAdd, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside uploadRetailLocCroppedImage : uploadRetailLocCroppedImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		String strViewName = "displayretailerlocations";
		String screenName = "retloc";
		int rowIndex = 0;
		RetailerLocation retailerLocation = null;
		

		rowIndex = objCouponAdd.getRowIndex();
		//final String locationJson = objCouponAdd.getLocationJson();
		final String prodJson = objCouponAdd.getLocationJson().replace("\t", "&#09;");
		ArrayList<RetailerLocation> arLocationList1 = (ArrayList<RetailerLocation>) Utility.jsonToObjectList(prodJson);
		retailerLocation = (RetailerLocation) arLocationList1.get(rowIndex);

		status = saveCroppedImage(request, response, session, objCouponAdd.getImageFile()[rowIndex], screenName, objCouponAdd.getRetailId());
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				retailerLocation.setGridImgLocationPath(strImageSource);

			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {

					retailerLocation.setGridImgLocationPath(null);
					retailerLocation.setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					retailerLocation.setImgLocationPath(strImageName);
					retailerLocation.setUploadImage(true);
				}
			}
		} else {

			retailerLocation.setGridImgLocationPath(null);
			retailerLocation.setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
		}

		arLocationList1.remove(rowIndex);
		arLocationList1.add(rowIndex, retailerLocation);

		RetailerLocationDetails locResult = new RetailerLocationDetails();
		locResult.setRetailerLocations(arLocationList1);
		session.setAttribute("RetailerLocationList", locResult.getRetailerLocations());
	
		final RetailerLocation retailLocation = new RetailerLocation();

		model.put("retailer", retailLocation);
		return strViewName;
	}
	
	@RequestMapping(value = "/uploadaddfundraisercroppedimage.htm", method = RequestMethod.POST)
	public String uploadCropAddFundraiserCategoryImage(@ModelAttribute("FundraiserCategoryForm") EventCategory objCouponAdd, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside ImageCropController : uploadCropAddFundraiserCategoryImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		String strViewName = "addFundriserCategory";
		
		objCouponAdd.setCateImgName(objCouponAdd.getCateImgPath().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objCouponAdd.getCateImgPath(), null, null);
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objCouponAdd.setCateImgName(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("cateImgPath", strImageName);
				}
			}
		} else {
			objCouponAdd.setCateImgName(null);
			session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);
		}
		model.put("FundraiserCategoryForm", objCouponAdd);
		return strViewName;
	}

}

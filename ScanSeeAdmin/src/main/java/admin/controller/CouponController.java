/**
 * 
 */
package admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import admin.service.CouponService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Coupon;
import common.pojo.CouponDetails;
import common.pojo.Product;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is used as a controller class for coupon admin.
 * 
 * @author sangeetha.ts
 */
@Controller
public class CouponController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CouponController.class);

	/**
	 * This method is used to display all the coupons with pagination.
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/displaycoupon.htm", method = RequestMethod.POST)
	public final ModelAndView displayCouponList(@ModelAttribute("admin") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "displayCouponList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		CouponDetails couponDetails = null;
		final String searchKey = null;
		final String sortOrder = "ASC";
		final String columnName = "CouponName";
		Integer lowerLimit = 0;
		final String showExpire = "true";
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);
			coupon = new Coupon();
			couponDetails = new CouponDetails();
			session.setAttribute("moduleName", ApplicationConstants.COUPONADMIN);
			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			couponDetails = couponService.getSearchCouponDetail(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			session.removeAttribute("searchKey");
			session.setAttribute("COUPONLIST", couponDetails.getCouponDetailList());
			session.setAttribute("showExpire", "true");
			session.setAttribute("CouponNameImg", "images/sorticonUp.png");
			session.setAttribute("CouponDiscountAmountImg", "");
			session.setAttribute("CouponStartDateImg", "");
			session.setAttribute("CouponExpireDateImg", "");
			session.setAttribute("sortOrderCouponName", "DESC");
			session.setAttribute("sortOrderDiscAmnt", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");
			objPage = Utility.getPagination(couponDetails.getTotalSize(), currentPage, "displaycoupon.htm", recordCount);
			session.setAttribute("pagination", objPage);
			session.setAttribute("columnName", "CouponName");

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		model.put("coupon", coupon);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("adddisplaycoupon");
	}

	/**
	 * This method is used for coupon search based on the coupon name as a
	 * search key
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/searchcoupon.htm", method = RequestMethod.POST)
	public final ModelAndView couponSearch(@ModelAttribute("coupon") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "couponSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		CouponDetails couponDetails = null;
		final String searchKey = coupon.getSearchKey();
		final String sortOrder = coupon.getSortOrder();
		final String columnName = coupon.getColumnName();
		Integer lowerLimit = 0;
		final String showExpire = (String) session.getAttribute("showExpire");
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);
			coupon = new Coupon();
			couponDetails = new CouponDetails();
			session.setAttribute("moduleName", ApplicationConstants.COUPONADMIN);
			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			couponDetails = couponService.getSearchCouponDetail(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			if (null == couponDetails || couponDetails.getTotalSize() == 0)
			{
				request.setAttribute("errorMessage", "No Coupons Found.");
			}
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("COUPONLIST", couponDetails.getCouponDetailList());
			session.setAttribute("CouponNameImg", "images/sorticonUp.png");
			session.setAttribute("CouponDiscountAmountImg", "");
			session.setAttribute("CouponStartDateImg", "");
			session.setAttribute("CouponExpireDateImg", "");
			session.setAttribute("sortOrderCouponName", "DESC");
			session.setAttribute("sortOrderDiscAmnt", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");
			objPage = Utility.getPagination(couponDetails.getTotalSize(), currentPage, "searchcoupon.htm", recordCount);
			session.setAttribute("pagination", objPage);
			session.setAttribute("columnName", "CouponName");

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		model.put("coupon", coupon);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("adddisplaycoupon");
	}

	/**
	 * This method is to display expired coupons.
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/showexpiredcoupon.htm", method = RequestMethod.POST)
	public final ModelAndView showExpired(@ModelAttribute("coupon") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "showExpired";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		CouponDetails couponDetails = null;
		final String searchKey = coupon.getSearchKey();
		final String sortOrder = "ASC";
		final String columnName = "CouponName";
		Integer lowerLimit = 0;
		final String showExpire = coupon.getShowExpire();
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);
			coupon = new Coupon();
			couponDetails = new CouponDetails();
			session.setAttribute("moduleName", ApplicationConstants.COUPONADMIN);
			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			couponDetails = couponService.getSearchCouponDetail(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			session.setAttribute("showExpire", showExpire);
			session.setAttribute("columnName", columnName);
			session.setAttribute("COUPONLIST", couponDetails.getCouponDetailList());
			session.setAttribute("CouponNameImg", "images/sorticonUp.png");
			session.setAttribute("CouponDiscountAmountImg", "");
			session.setAttribute("CouponStartDateImg", "");
			session.setAttribute("CouponExpireDateImg", "");
			session.setAttribute("sortOrderCouponName", "DESC");
			session.setAttribute("sortOrderDiscAmnt", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");
			objPage = Utility.getPagination(couponDetails.getTotalSize(), currentPage, "showexpiredcoupon.htm", recordCount);
			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		model.put("coupon", coupon);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("adddisplaycoupon");
	}

	/**
	 * This method is used to sort the coupon based on the given column name.
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/sortcolumn.htm", method = RequestMethod.POST)
	public final ModelAndView sortColumn(@ModelAttribute("coupon") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "sortColumn";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		CouponDetails couponDetails = null;
		final String searchKey = coupon.getSearchKey();
		String sortOrder = "";
		String columnName = coupon.getColumnName();
		Integer lowerLimit = 0;
		final String showExpire = (String) session.getAttribute("showExpire");
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);
			coupon = new Coupon();
			couponDetails = new CouponDetails();
			session.setAttribute("moduleName", ApplicationConstants.COUPONADMIN);
			if ("".equals(columnName))
			{
				columnName = (String) session.getAttribute("columnName");
			}
			if ("CouponName".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderCouponName");
			}
			else if ("CouponDiscountAmount".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderDiscAmnt");
			}
			else if ("CouponStartDate".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderSTDT");
			}
			else if ("CouponExpireDate".equals(columnName))
			{
				sortOrder = (String) session.getAttribute("sortOrderENDT");
			}

			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				if ("ASC".equalsIgnoreCase(sortOrder))
				{
					sortOrder = "DESC";
				}
				else
				{
					sortOrder = "ASC";
				}
			}

			couponDetails = couponService.getSearchCouponDetail(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			session.setAttribute("COUPONLIST", couponDetails.getCouponDetailList());
			session.setAttribute("columnName", columnName);
			if ("CouponName".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("CouponNameImg", "images/sorticonUp.png");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("CouponExpireDateImg", "");
					session.setAttribute("sortOrderCouponName", "DESC");
				}
				else
				{
					session.setAttribute("CouponNameImg", "images/sorticon.png");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("CouponExpireDateImg", "");
					session.setAttribute("sortOrderCouponName", "ASC");
				}
			}
			else if ("CouponDiscountAmount".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("CouponDiscountAmountImg", "images/sorticonUp.png");
					session.setAttribute("CouponNameImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("CouponExpireDateImg", "");
					session.setAttribute("sortOrderDiscAmnt", "DESC");
				}
				else
				{
					session.setAttribute("CouponDiscountAmountImg", "images/sorticon.png");
					session.setAttribute("CouponNameImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("CouponExpireDateImg", "");
					session.setAttribute("sortOrderDiscAmnt", "ASC");
				}
			}
			else if ("CouponStartDate".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("CouponStartDateImg", "images/sorticonUp.png");
					session.setAttribute("CouponNameImg", "");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponExpireDateImg", "");
					session.setAttribute("sortOrderSTDT", "DESC");
				}
				else
				{
					session.setAttribute("CouponStartDateImg", "images/sorticon.png");
					session.setAttribute("CouponNameImg", "");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponExpireDateImg", "");
					session.setAttribute("sortOrderSTDT", "ASC");
				}
			}
			else if ("CouponExpireDate".equals(columnName))
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("CouponExpireDateImg", "images/sorticonUp.png");
					session.setAttribute("CouponNameImg", "");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("sortOrderENDT", "DESC");
				}
				else
				{
					session.setAttribute("CouponExpireDateImg", "images/sorticon.png");
					session.setAttribute("CouponNameImg", "");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("sortOrderENDT", "ASC");
				}
			}
			else
			{
				if ("ASC".equals(sortOrder))
				{
					session.setAttribute("CouponNameImg", "images/sorticonUp.png");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("CouponExpireDateImg", "");
				}
				else
				{
					session.setAttribute("CouponNameImg", "images/sorticon.png");
					session.setAttribute("CouponDiscountAmountImg", "");
					session.setAttribute("CouponStartDateImg", "");
					session.setAttribute("CouponExpireDateImg", "");
				}
			}
			objPage = Utility.getPagination(couponDetails.getTotalSize(), currentPage, "sortcolumn.htm", recordCount);
			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException();
		}

		model.put("coupon", coupon);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("adddisplaycoupon");
	}

	/**
	 * This method is to add coupons.
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/addcoupon.htm", method = RequestMethod.POST)
	public final ModelAndView addCoupon(@ModelAttribute("coupon") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "addCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		coupon = new Coupon();
		model.put("coupon", coupon);
		session.setAttribute("moduleName", ApplicationConstants.COUPONADMIN);
		request.setAttribute("COUPON", coupon);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("editcoupon");
	}

	/**
	 * This method is to get the products which are associated to the particular
	 * coupon.
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/getassociatedproducts.htm", method = RequestMethod.GET)
	public final ModelAndView getAssociatedProducts(@ModelAttribute("coupon") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getAssociatedProducts";
		LOG.info("Entering " + methodName + " method");

		ArrayList<Product> pdtInfoList = new ArrayList<Product>();
		String prodID = coupon.getProductIDHidden();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);

			if (!"".equals(prodID))
			{
				final int temp = prodID.indexOf(",0,");
				if (temp > 0)
				{
					String t1 = prodID.substring(0, temp);
					prodID = prodID.substring(temp + 2, prodID.length());
					prodID = prodID.concat(t1);
				}
				final int temp1 = prodID.indexOf(",,");
				if (temp1 > 0)
				{
					final String t1 = prodID.substring(0, temp1);
					prodID = prodID.substring(temp1 + 1, prodID.length());
					prodID = prodID.concat(t1);
				}
				pdtInfoList = (ArrayList<Product>) couponService.getAssociatedProducts(prodID);
			}
			session.setAttribute("pdtInfoList", pdtInfoList);
		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		final Product product = new Product();
		model.put("product", product);
		LOG.info("Exiting " + methodName + " method");
		return new ModelAndView("productsearch");
	}

	/**
	 * This method is for searching products based on the product name.
	 * 
	 * @param product
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/searchproduct", method = RequestMethod.POST)
	public final ModelAndView getSearchProduct(@ModelAttribute("product") Product product, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getSearchProduct";
		LOG.info("Entering " + methodName + " method");

		ArrayList<Product> pdtInfoList = null;
		List<Product> prodList = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);

			prodList = couponService.getSearchProduct(product.getSearchKey());

			pdtInfoList = (ArrayList<Product>) session.getAttribute("pdtInfoList");

			if (pdtInfoList != null && !pdtInfoList.isEmpty())
			{
				for (int i = 0; i < pdtInfoList.size(); i++)
				{
					final String prodID = pdtInfoList.get(i).getProductId();
					for (int j = 0; j < prodList.size(); j++)
					{
						if (prodID.equals(prodList.get(j).getProductId()))
						{
							prodList.remove(j);
						}
					}
				}
			}
			request.setAttribute("ProdList", prodList);
			if (prodList == null || prodList.isEmpty())
			{
				final String errorMsg = "No Products available for the give string " + "'" + product.getSearchKey() + "'";
				request.setAttribute("errorMsg", errorMsg);
			}
		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info("Exiting " + methodName + " method");
		return new ModelAndView("productsearch");

	}

	/**
	 * This method is used to save the coupon.
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/savecoupon", method = RequestMethod.POST)
	public final ModelAndView saveCoupon(@ModelAttribute("coupon") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "saveCoupon";
		LOG.info("Entering " + methodName + " method");

		final String pageFlag = request.getParameter("pageFlag");
		CouponDetails couponDetails = null;
		final String searchKey = (String) session.getAttribute("searchKey");
		final String sortOrder = "ASC";
		final String columnName = "CouponName";
		Integer lowerLimit = 0;
		final String showExpire = (String) session.getAttribute("showExpire");
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		String status = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);

			final String userId = (String) session.getAttribute("UserId");
			final Double couponAmount = Utility.formatDecimalValue(coupon.getCouponValue());
			coupon.setUserId(new Integer(userId));
			if (coupon.getCouponId() == null || coupon.getCouponId() == 0)
			{
				status = couponService.saveCoupon(coupon, couponAmount);

				if ("Success".equalsIgnoreCase(status))
				{
					request.setAttribute("message", "Coupon Added Successfully.");
				}
				else
				{
					request.setAttribute("message", "Error Adding Coupon.");
				}
			}
			else
			{
				status = couponService.updateCoupon(coupon, couponAmount);

				if ("Success".equalsIgnoreCase(status))
				{
					request.setAttribute("message", "Coupon Updated Successfully.");
				}
				else
				{
					request.setAttribute("message", "Error Updating Coupon.");
				}
			}

			coupon = new Coupon();
			couponDetails = new CouponDetails();

			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			couponDetails = couponService.getSearchCouponDetail(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			session.setAttribute("COUPONLIST", couponDetails.getCouponDetailList());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("columnName", columnName);
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("CouponNameImg", "images/sorticonUp.png");
			session.setAttribute("CouponDiscountAmountImg", "");
			session.setAttribute("CouponStartDateImg", "");
			session.setAttribute("CouponExpireDateImg", "");
			session.setAttribute("sortOrderCouponName", "DESC");
			session.setAttribute("sortOrderDiscAmnt", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");
			objPage = Utility.getPagination(couponDetails.getTotalSize(), currentPage, "searchcoupon.htm", recordCount);
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info("Exiting " + methodName + " method");
		return new ModelAndView("adddisplaycoupon");

	}

	/**
	 * This method will display the coupon details and allow the user to edit
	 * it.
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/editcoupon", method = RequestMethod.POST)
	public final ModelAndView editCoupon(@ModelAttribute("coupon") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "editCoupon";
		LOG.info("Entering " + methodName + " method");

		final Integer couponId = coupon.getCouponId();
		Coupon couponDetails = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);

			couponDetails = new Coupon();
			couponDetails = couponService.getCouponDetails(couponId);
			request.setAttribute("EDIT", "editcoupon");
			request.setAttribute("COUPON", couponDetails);
			String temp = "";
			if (couponDetails.getProductNames() != null)
			{
				temp = couponDetails.getProductNames();
				temp = temp.replace(",", ",\n");
			}
			final ArrayList<Product> pdtInfoList = (ArrayList<Product>) couponService.getAssociatedProducts(couponDetails.getProductIDs());

			if (pdtInfoList != null && pdtInfoList.size() > 0)
			{
				session.setAttribute("pdtInfoList", pdtInfoList);
			}
			request.setAttribute("productNameHidden", temp);
			request.setAttribute("productIDHidden", couponDetails.getProductIDs());

		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info("Exiting " + methodName + " method");
		return new ModelAndView("editcoupon");

	}

	/**
	 * This method is used to delete the coupon.
	 * 
	 * @param coupon
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/deletecoupon", method = RequestMethod.POST)
	public final ModelAndView deleteCoupon(@ModelAttribute("coupon") Coupon coupon, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "deleteCoupon";
		LOG.info("Entering " + methodName + " method");

		final String pageFlag = request.getParameter("pageFlag");
		CouponDetails couponDetails = null;
		final String searchKey = coupon.getSearchKey();
		final String sortOrder = "ASC";
		final String columnName = "CouponName";
		Integer lowerLimit = 0;
		final String showExpire = coupon.getShowExpire();
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		String status = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CouponService couponService = (CouponService) appContext.getBean(ApplicationConstants.COUPONSERVICE);

			final Integer couponId = coupon.getCouponId();
			final String type = coupon.getCouponDiscountType();

			status = couponService.deleteCoupon(couponId, type);

			if ("Success".equalsIgnoreCase(status))
			{
				request.setAttribute("message", "Coupon Expired Successfully.");
			}
			else
			{
				request.setAttribute("message", "Error Expiring Coupon.");
			}

			coupon = new Coupon();
			couponDetails = new CouponDetails();

			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}

			couponDetails = couponService.getSearchCouponDetail(searchKey, sortOrder, columnName, lowerLimit, showExpire);

			session.setAttribute("COUPONLIST", couponDetails.getCouponDetailList());
			session.setAttribute("showExpire", showExpire);
			session.setAttribute("columnName", columnName);
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("CouponNameImg", "images/sorticonUp.png");
			session.setAttribute("CouponDiscountAmountImg", "");
			session.setAttribute("CouponStartDateImg", "");
			session.setAttribute("CouponExpireDateImg", "");
			session.setAttribute("sortOrderCouponName", "DESC");
			session.setAttribute("sortOrderDiscAmnt", "ASC");
			session.setAttribute("sortOrderSTDT", "ASC");
			session.setAttribute("sortOrderENDT", "ASC");
			objPage = Utility.getPagination(couponDetails.getTotalSize(), currentPage, "searchcoupon.htm", recordCount);
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException exception)
		{
			throw new ScanSeeServiceException(exception);
		}

		LOG.info("Exiting " + methodName + " method");
		return new ModelAndView("adddisplaycoupon");

	}

}

package admin.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import admin.service.HubCitiService;
import admin.service.RetailerService;
import admin.validator.RegionAdminValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.City;
import common.pojo.HubCiti;
import common.pojo.PostalCode;
import common.pojo.RegionInfo;
import common.pojo.State;
import common.tags.Pagination;
import common.util.Utility;

@Controller
public class RegionAdminController {

	private static final Logger LOG = LoggerFactory.getLogger("RegionAdminController");

	private RegionAdminValidator regionAdminValidator;

	@Autowired
	public void setRegionAdminValidator(RegionAdminValidator regionAdminValidator) {
		this.regionAdminValidator = regionAdminValidator;
	}

	/**
	 * This method is used to display the regions.
	 * 
	 * @param hubCiti
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/displayregions.htm", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView displayRegions(@ModelAttribute("RegionForm") HubCiti hubCiti, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException

	{
		final String strMethodName = "displayRegions";
		final String strViewName = "regionAdmin";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		RegionInfo regionInfo = null;
		Integer iUserId = null;
		// For pagination
		Pagination objPage = null;
		Integer iLowerLimit = 0;
		Integer iCurrentPage = 1;
		String strPageNum = "0";
		String strPageFlag = null;
		Integer iRecordCount = 10;
		Boolean strShowActivateDeactive = false;
		ArrayList<State> regStateLst = null;
		ArrayList<HubCiti> hubList = null;
		try {

			// Removing session objects.

			session.removeAttribute("regionlst");
			session.removeAttribute("showDeactivated");
			session.removeAttribute("hubCitilst");
			session.removeAttribute("regStatelst");
			// for pagination
			strPageFlag = (String) request.getParameter("pageFlag");

			String strRes = request.getParameter("res");
			if (!Utility.isEmptyOrNullString(strRes)) {
				if (strRes.equals(ApplicationConstants.SUCCESSTEXT)) {
					request.setAttribute("message", "Region Admin Created successfully.");
				} else {
					request.setAttribute("message", "Failed to Create Region Admin.");
				}

			}

			if (!Utility.isEmptyOrNullString(strPageFlag) && strPageFlag.equals("true")) {
				strPageNum = request.getParameter("pageNumber");
				objPage = (Pagination) session.getAttribute("pagination");

				if (null != strPageNum && strPageNum != "0") {

					if (Integer.valueOf(strPageNum) != 0) {
						iCurrentPage = Integer.valueOf(strPageNum);
						final int num = iCurrentPage - 1;
						final int pageSize = objPage.getPageRange();
						iLowerLimit = (num * pageSize);

					}

				}

			} else {
				iCurrentPage = (iLowerLimit + iRecordCount) / iRecordCount;
			}

			hubCiti.setLowerLimit(iLowerLimit);

			final String strUserId = (String) session.getAttribute("UserId");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			HubCitiService hubCitiService = (HubCitiService) webApplicationContext.getBean("hubCitiService");
			RetailerService retailerService = (RetailerService) webApplicationContext.getBean("retailerService");

			hubCiti.setAppRole(ApplicationConstants.REGIONAPP);

			if (null == hubCiti.getShowDeactivated()) {
				strShowActivateDeactive = false;
			} else {

				strShowActivateDeactive = hubCiti.getShowDeactivated();
			}

			hubCiti.setShowDeactivated(strShowActivateDeactive);

			if (null != strUserId) {
				iUserId = Integer.parseInt(strUserId);

			}
			if (hubCiti.getLowerLimit() == null) {
				hubCiti.setLowerLimit(0);
			}

			if (Utility.isEmptyOrNullString(hubCiti.getSearchKey())) {
				hubCiti.setSearchKey(null);
			}
			hubCiti.setUserID(iUserId);
			regionInfo = hubCitiService.displayRegions(hubCiti);

			// to fetch states list for region creation.
			regStateLst = retailerService.getAllStates();
			session.setAttribute("regStatelst", regStateLst);

			// To fetch HubCiti list exists in the system for region creation.

			hubList = hubCitiService.fetchHubCitis();
			session.setAttribute("hubCitilst", hubList);

			if (null != regionInfo && regionInfo.getRegionLst() != null && !regionInfo.getRegionLst().isEmpty()) {
				objPage = Utility.getPagination(regionInfo.getTotalSize(), iCurrentPage, "displayregions.htm", iRecordCount);
				session.setAttribute("pagination", objPage);
				session.setAttribute("regionlst", regionInfo.getRegionLst());
			} else {
				session.removeAttribute("pagination");
				if (Utility.isEmptyOrNullString(hubCiti.getSearchKey())) {
					return new ModelAndView("createregion");
				}
			}

			if (null == regionInfo && null != hubCiti.getSearchKey()) {
				request.setAttribute("noregfound", ApplicationConstants.REGIONNOTFOUNDTEXT);

			}

			session.setAttribute("showDeactivated", strShowActivateDeactive);
			request.setAttribute("lowerLimit", iLowerLimit);
			model.put("RegionForm", hubCiti);
		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);

	}

	@RequestMapping(value = "/deactivateRegion.htm", method = RequestMethod.POST)
	public ModelAndView deactivateRegions(@ModelAttribute("RegionForm") HubCiti hubCiti, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException

	{
		final String strMethodName = "deactivateRegions";
		final String strViewName = "regionAdmin";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strResponse = null;
		Integer iUserId = null;
		RegionInfo regionInfo = null;
		Integer iLowerLimit = 0;
		Integer iCurrentPage = 1;
		Integer iRecordCount = 10;

		try {
			final Boolean active = hubCiti.getActive();
			Integer activeFlag = 0;
			if (active == true) {
				activeFlag = 1;
			}

			Boolean isShowDeactivate = (Boolean) session.getAttribute("showDeactivated");
			// removing session variables.
			session.removeAttribute("showDeactivated");

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			HubCitiService hubCitiService = (HubCitiService) webApplicationContext.getBean("hubCitiService");

			final String strUserId = (String) session.getAttribute("UserId");
			if (null != strUserId) {
				iUserId = Integer.parseInt(strUserId);

			}
			hubCiti.setUserID(iUserId);
			hubCiti.setActive(active);
			strResponse = hubCitiService.deactivateRegion(hubCiti);

			if (null != isShowDeactivate && isShowDeactivate == true) {
				hubCiti.setShowDeactivated(isShowDeactivate);
			} else {
				hubCiti.setShowDeactivated(false);
			}
			// displaying regions
			if (null != hubCiti.getLowerLimit()) {
				iLowerLimit = hubCiti.getLowerLimit();
				iCurrentPage = (iLowerLimit + iRecordCount) / iRecordCount;

			}

			hubCiti.setAppRole(ApplicationConstants.REGIONAPP);
			if (!Utility.isEmptyOrNullString(hubCiti.getSearchKey())) {
				hubCiti.setSearchKey(null);
			}

			regionInfo = hubCitiService.displayRegions(hubCiti);

			if (null != regionInfo) {
				if (null != regionInfo.getRegionLst() && !regionInfo.getRegionLst().isEmpty()) {
					Pagination pagination = Utility.getPagination(regionInfo.getTotalSize(), iCurrentPage, "displayRegions.htm", iRecordCount);
					session.setAttribute("pagination", pagination);
					session.setAttribute("regionlst", regionInfo.getRegionLst());

				} else {
					session.removeAttribute("pagination");
				}

			} else {
				session.setAttribute("regionlst", null);
			}

			if (!Utility.isEmptyOrNullString(strResponse)) {
				if (strResponse.equals(ApplicationConstants.SUCCESSTEXT)) {
					if (hubCiti.getActive() == true) {
						request.setAttribute("deactivateMsg", ApplicationConstants.DEACTIVATEREGSUCCESSTEXT);
					} else {
						request.setAttribute("deactivateMsg", ApplicationConstants.ACTIVATEREGSUCESSTEXT);
					}
				} else {

					if (hubCiti.getActive() == true) {
						request.setAttribute("deactivateMsg", ApplicationConstants.DEACTIVATEFAILURETEXT);
					} else {
						request.setAttribute("deactivateMsg", ApplicationConstants.ACTIVATEREGFAILURETEXT);
					}
				}

			} else {
				if (hubCiti.getActive() == true) {
					request.setAttribute("deactivateMsg", ApplicationConstants.DEACTIVATEFAILURETEXT);
				} else {
					request.setAttribute("deactivateMsg", ApplicationConstants.ACTIVATEREGFAILURETEXT);
				}
			}

			session.setAttribute("showDeactivated", hubCiti.getShowDeactivated());
			request.setAttribute("lowerLimit", hubCiti.getLowerLimit());
			model.put("RegionForm", hubCiti);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "/createregion.htm", method = RequestMethod.GET)
	public ModelAndView createRegion(@ModelAttribute("RegionForm") HubCiti hubCiti, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		final String strViewName = "createregion";
		final String strMethodName = "createRegion";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		session.removeAttribute("defltPstlCdeLst");
		session.removeAttribute("selectedRegPostalCodes");
		session.setAttribute("regpostalcodesmap", new HashMap<String, String>());

		HubCiti hubCit = new HubCiti();
		model.put("RegionForm", hubCit);

		/*
		 * try{
		 * 
		 * ServletContext servletContext =
		 * request.getSession().getServletContext(); WebApplicationContext
		 * webApplicationContext =
		 * WebApplicationContextUtils.getWebApplicationContext(servletContext);
		 * HubCitiService hubCitiService = (HubCitiService)
		 * webApplicationContext.getBean("hubCitiService"); RetailerService
		 * retailerService = (RetailerService)
		 * webApplicationContext.getBean("retailerService");
		 * 
		 * }catch(ScanSeeServiceException exception) {
		 * LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName);
		 * throw new ScanSeeServiceException(exception.getMessage());
		 * 
		 * }
		 */
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "/loadregciti.htm", method = RequestMethod.GET)
	public @ResponseBody
	final String loadregciti(@RequestParam(value = "state", required = true) String state,
			@RequestParam(value = "city", required = true) String city, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException

	{
		final String strMethodName = "loadregciti";
		LOG.info(ApplicationConstants.METHODSTART);
		ArrayList<City> citiesLst = null;
		StringBuffer strInnerHTML = new StringBuffer();

		try {

			strInnerHTML
					.append("<select  class='textboxBig bigDropdownBox' name='city' id='city' onchange='loadPostalcodes()'><option value=''>--Select--</option>");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) webApplicationContext.getBean("retailerService");

			citiesLst = retailerService.fetchAllCities(state);

			if (null != citiesLst && !citiesLst.isEmpty()) {
				for (City city2 : citiesLst) {

					if (!Utility.isEmptyOrNullString(city2.getCityName())) {
						if (!Utility.isEmptyOrNullString(city)) {
							if (city.equalsIgnoreCase(city2.getCityName())) {
								strInnerHTML.append("<option selected=true>" + city2.getCityName() + "</option>");
							} else {
								strInnerHTML.append("<option>" + city2.getCityName() + "</option>");
							}

						} else {
							strInnerHTML.append("<option>" + city2.getCityName() + "</option>");
						}
					}
				}
			}

			strInnerHTML.append("</select>");

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strInnerHTML.toString();

	}

	@RequestMapping(value = "/getregpostalcode.htm", method = RequestMethod.GET)
	public @ResponseBody
	String fetchRegionPostalCodes(@RequestParam(value = "state", required = true) String state,
			@RequestParam(value = "city", required = true) String city, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String strMethodName = "";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		StringBuffer innerHtml = new StringBuffer();
		ArrayList<PostalCode> postalCodesLst = null;
		try {

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			HubCitiService hubCitiService = (HubCitiService) webApplicationContext.getBean("hubCitiService");

			postalCodesLst = hubCitiService.getAllPostalCode(city, state);

			final String finalHtml = "<select name='postalCodes' multiple='multiple' id='postalCodes' class='textboxBig multiSelDropDown' style='font-size: 11px;'>";

			innerHtml.append(finalHtml);

			response.setContentType("text/xml");
			final String key = state + "/" + city;
			String value = null;
			@SuppressWarnings("unchecked")
			final HashMap<String, String> postalCodeMap = (HashMap<String, String>) session.getAttribute("regpostalcodesmap");

			if (null != postalCodeMap && !postalCodeMap.isEmpty()) {
				for (Entry<String, String> entry : postalCodeMap.entrySet()) {
					if (entry.getKey().equalsIgnoreCase(key)) {
						value = entry.getValue();
					}
				}
			}

			String[] selPostalCodes = null;

			if (null != value && !value.isEmpty()) {
				selPostalCodes = value.split(",");
			}

			if (null != postalCodesLst && !postalCodesLst.isEmpty()) {
				for (PostalCode code : postalCodesLst) {
					Integer selectFlag = 0;
					if (null != selPostalCodes) {
						for (String postalCode : selPostalCodes) {
							if (code.getPostalCode().equals(postalCode)) {
								selectFlag = 1;
							}
						}

						if (selectFlag != 1) {
							innerHtml.append("<option value=" + code.getPostalCode() + " title=" + code.getLocation() + ">" + code.getLocation()
									+ "</option>");
						}
					} else {
						innerHtml.append("<option value=" + code.getPostalCode() + " title=" + code.getLocation() + ">" + code.getLocation()
								+ "</option>");
					}
				}
			}

			innerHtml.append("</select>");

			/*
			 * if (null != postalCodesLst && !postalCodesLst.isEmpty()) {
			 * innerHtml .append(
			 * "<select size='1' multiple='multiple' id='postalCodes' name='postalCodes' class='textboxBig multiSelDropDown'>"
			 * );
			 * 
			 * for (PostalCode postalCode : postalCodesLst) {
			 * 
			 * innerHtml.append("<option value=" + postalCode.getPostalCode() +
			 * "title=" + postalCode.getLocation() + ">" +
			 * postalCode.getLocation() + "</option>");
			 * 
			 * }
			 * 
			 * innerHtml.append("</select>"); }
			 */

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return innerHtml.toString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/movepostalcode.htm", method = RequestMethod.GET)
	public @ResponseBody
	String moveRegZipcodes(@RequestParam(value = "state", required = true) String state, @RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "zipcode", required = true) String zipcode, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException {
		final String strMethodName = "";
//		StringBuffer postalCodes = new StringBuffer();
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strKey = state + "/" + city;
		final String[] tempPostalCodes = zipcode.split(",");
		HashMap<String, String> postalCodesMap = null;
		Boolean contains = false;
		String strValue = null;

		postalCodesMap = (HashMap<String, String>) session.getAttribute("regpostalcodesmap");
	
		ArrayList<String> defltPstlCdeLst = (ArrayList<String>) session.getAttribute("defltPstlCdeLst");

		if (null != postalCodesMap && !postalCodesMap.isEmpty()) {

			for (Entry<String, String> entry : postalCodesMap.entrySet()) {

				if (entry.getKey().equalsIgnoreCase(strKey)) {
					contains = true;
					strValue = entry.getValue();
					strValue += zipcode;
					entry.setValue(strValue);

				}

			}
			if (!contains) {
				postalCodesMap.put(strKey, zipcode);
			}

		} else {

			postalCodesMap = new HashMap<String, String>();
			postalCodesMap.put(strKey, zipcode);
		}

		for (String postalCode : tempPostalCodes) {
			if (null != defltPstlCdeLst) {
				defltPstlCdeLst.add(postalCode);
			} else {
				defltPstlCdeLst = new ArrayList<String>();
				defltPstlCdeLst.add(postalCode);
			}
		}

		session.setAttribute("regpostalcodesmap", postalCodesMap);
		session.setAttribute("defltPstlCdeLst", defltPstlCdeLst);

		LOG.info(ApplicationConstants.METHODEND);
		return ApplicationConstants.SUCCESSTEXT;

	}

	@RequestMapping(value = "/saveregion.htm", method = {RequestMethod.POST})
	public ModelAndView saveRegionDetails(@ModelAttribute("RegionForm") HubCiti hubciti, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		final String strMethodName = "saveRegionDetails";
		final String strViewName = "createregion";
		Boolean isValidEmailId = true;
		Boolean isValidSalesContact = true;
		String status = null;
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strRes = null;
//		String strPostalcode = null;
		boolean isValidEmails = true;
		try {

			if (null == hubciti.getActive()) {

				hubciti.setActive(false);
			}

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			HubCitiService hubCitiService = (HubCitiService) webApplicationContext.getBean("hubCitiService");

			hubciti.setAppRole(ApplicationConstants.REGIONAPP);

			request.setAttribute(ApplicationConstants.LOWERLIMIT, hubciti.getLowerLimit());
			request.setAttribute("state", hubciti.getState());
			request.setAttribute("city", hubciti.getCity());

			request.setAttribute("reghubids", hubciti.getSelRegHubCitiIds());

			ArrayList<HubCiti> regPostalCodelst = selectedRegPostalCodes(session);

			if (null != regPostalCodelst && !regPostalCodelst.isEmpty()) {
				hubciti.setZipCode("true");

			} else {
				hubciti.setZipCode("false");
			}

			regionAdminValidator.validate(hubciti, result);

			if (result.hasErrors()) {
				return new ModelAndView(strViewName);
			}

			isValidEmailId = Utility.validateEmailId(hubciti.getEmailID());
			isValidSalesContact = Utility.validateEmailId(hubciti.getSalesPersonEmail());
			
			final String[] strEmails = hubciti.getEmails().split(";");
			Set<String> list = new LinkedHashSet<String>(Arrays.asList(strEmails));
			list.removeAll(Arrays.asList("", null));
			int icount = 0;
			
			if (list.size() > 5) {
				regionAdminValidator.validate(hubciti, result, ApplicationConstants.NUMBER_OF_EMAILS);
				if (result.hasErrors()) {
					return new ModelAndView(strViewName);
				}
			}
			
			
			if (strEmails.length != list.size()) {
				regionAdminValidator.validate(hubciti, result, ApplicationConstants.DUPLICATE_EMAILS);

				if (result.hasErrors()) {
					return new ModelAndView(strViewName);
				}
			}
			if (list.size() != 1) {
				for (int i = 0; i < list.size(); i++) {
					if (!Utility.validateEmailId(strEmails[i])) {
						++icount;
					} 
				}
			} else {
				isValidEmails = Utility.validateEmailId(hubciti.getEmails());
				
				if (!isValidEmails) {
					regionAdminValidator.validate(hubciti, result, ApplicationConstants.INVALID_EMAIL);

					if (result.hasErrors()) {
						return new ModelAndView(strViewName);
					}
				}
			}

			if (isValidEmailId == false)
			{
				regionAdminValidator.validate(hubciti, result, ApplicationConstants.INVALIDEMAIL);
			}
			if (isValidSalesContact == false)
			{
				regionAdminValidator.validate(hubciti, result, ApplicationConstants.INVALIDSALESCONTACT);
			}
			
			if (!isValidEmails || (icount > 0)) {
				regionAdminValidator.validate(hubciti, result, ApplicationConstants.INVALID_EMAILS);
			}
			
			if (result.hasErrors()) {
				return new ModelAndView(strViewName);
			}

			final String strLogoImage = hubCitiService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
			final String strUserId = (String) session.getAttribute("UserId");
			hubciti.setUserID(new Integer(strUserId));

			if (Utility.isEmptyOrNullString(hubciti.getSelRegHubCitiIds())) {
				hubciti.setSelRegHubCitiIds(null);
			} else {

				String ids = hubciti.getSelRegHubCitiIds().trim();
				hubciti.setSelRegHubCitiIds(ids);
			}

			/*
			 * strPostalcode = String.valueOf(hubciti.getDefaultZipcode());
			 * 
			 * if (null != strPostalcode && strPostalcode.length() < 5) {
			 * regionAdminValidator.validate(hubciti, result,
			 * ApplicationConstants.INVALIDZIPCODE);
			 * 
			 * }
			 */
			if (result.hasErrors()) {
				return new ModelAndView(strViewName);
			}
			
			StringBuilder sb = new StringBuilder();
			for (String emails : list) {
			    sb.append(emails).append(";");
			}
		
			hubciti.setEmails(sb.toString());
			status = hubCitiService.createAdmin(hubciti, strLogoImage, regPostalCodelst);
			if (!Utility.isEmptyOrNullString(status)) {
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATEHUBCITI)) {
					regionAdminValidator.validate(hubciti, result, ApplicationConstants.DUPLICATEHUBCITI);

					if (result.hasErrors()) {
						return new ModelAndView(strViewName);
					}
				}
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATEUSERNAME)) {
					regionAdminValidator.validate(hubciti, result, ApplicationConstants.DUPLICATEUSERNAME);

					if (result.hasErrors()) {
						return new ModelAndView(strViewName);
					}
				}
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATEEMAIL)) {
					regionAdminValidator.validate(hubciti, result, ApplicationConstants.DUPLICATEEMAIL);

					if (result.hasErrors()) {
						return new ModelAndView(strViewName);
					}
				}
				
				if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATESALESCONTACT)) {
					regionAdminValidator.validate(hubciti, result, ApplicationConstants.DUPLICATESALESCONTACT);

					if (result.hasErrors()) {
						return new ModelAndView(strViewName);
					}
				}

				if ("Success".equalsIgnoreCase(status)) {
					request.removeAttribute("state");
					request.removeAttribute("city");
					session.removeAttribute("selectedRegPostalCodes");
					request.removeAttribute("reghubids");
					request.setAttribute("message", "Region Admin Created successfully.");
					strRes = ApplicationConstants.SUCCESSTEXT;

					model.put("RegionForm", hubciti);

				} else {
					request.setAttribute("message", "Failed to Create Region Admin.");
					strRes = ApplicationConstants.FAILURETEXT;
				}
			} else {
				request.setAttribute("message", "Failed to Create Region Admin.");
				strRes = ApplicationConstants.FAILURETEXT;

			}
			HubCiti citi = new HubCiti();
			citi.setSearchKey(null);
			model.put("RegionForm", hubciti);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		// return new ModelAndView("regionAdmin");
		return new ModelAndView(new RedirectView("/ScanSeeAdmin/displayregions.htm?res=" + strRes));

	}

	public ArrayList<HubCiti> selectedRegPostalCodes(HttpSession session) {

		final String strMethodName = "selectedRegPostalCodes";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ArrayList<HubCiti> regionPostalCodelst = null;
		ArrayList<PostalCode> selRegPostalcodeLst = null;

		@SuppressWarnings("unchecked")
		HashMap<String, String> regPostalCodeMap = (HashMap<String, String>) session.getAttribute("regpostalcodesmap");

		if (null != regPostalCodeMap && !regPostalCodeMap.isEmpty()) {
			regionPostalCodelst = new ArrayList<HubCiti>();
			selRegPostalcodeLst = new ArrayList<PostalCode>();
			for (Entry<String, String> entry : regPostalCodeMap.entrySet()) {
				String key = entry.getKey();
				final String[] postalCode = key.split("/");
				String strState = postalCode[0];
				String strCity = postalCode[1];
				String strValue = entry.getValue();

				HubCiti region = new HubCiti();
				region.setState(strState);
				region.setCity(strCity);
				region.setPostalCodes(strValue);

				regionPostalCodelst.add(region);

				if (!Utility.isEmptyOrNullString(strValue)) {
					List<String> items = Arrays.asList(strValue.split(","));

					for (String string : items) {
						PostalCode pCode = new PostalCode();
						pCode.setPostalCode(string);
						pCode.setLocation(string + "," + strCity + "," + strState);
						selRegPostalcodeLst.add(pCode);
					}
				}
			}
			session.setAttribute("selectedRegPostalCodes", selRegPostalcodeLst);
		} else {

			session.setAttribute("selectedRegPostalCodes", null);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return regionPostalCodelst;

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/editregadmin.htm", method = RequestMethod.POST)
	public ModelAndView editRegionAdmin(@ModelAttribute("RegionForm") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			ModelMap model, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		final String strMethodName = "updateRegionAdmin";
		final String strViewName = "editregionadmin";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		session.removeAttribute("selectedRegPostalCodes");
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
		session.removeAttribute("hubCitiUserName");
		session.removeAttribute("hubCitiEmail");
		session.removeAttribute("salesContact");

		final String userId = (String) session.getAttribute("UserId");
		final Integer hubCitiId = hubCiti.getHubCitiID();
		HubCiti citi = null;
		ArrayList<HubCiti> postalCodes = null;
		HashMap<String, String> postalCodeMap = null;
		ArrayList<PostalCode> selectedPostalCodeList = null;
		ArrayList<String> defltPstlCdeLst = null;
		Boolean containsKey = false;
		ArrayList<HubCiti> regHubcitiIdlst = new ArrayList<HubCiti>();
		HubCiti regHub = null;
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

			citi = hubCitiService.editAdmin(new Integer(userId), hubCitiId);

			if (null != citi) {
				session.setAttribute("hubCitiUserName", citi.getUserName());
				session.setAttribute("hubCitiEmail", citi.getEmailID());
				session.setAttribute("salesContact", citi.getSalesPersonEmail());

				if (!Utility.isEmptyOrNullString(citi.getRegHubcitiIds())) {
					String ids = citi.getRegHubcitiIds().trim();
					request.setAttribute("reghubids", ids);

				}

			}
			postalCodes = hubCitiService.fetchAdminAssociatedPostalCode(hubCitiId);

			if (null != postalCodes && !postalCodes.isEmpty()) {
				postalCodeMap = new HashMap<String, String>();
				selectedPostalCodeList = new ArrayList<PostalCode>();
				defltPstlCdeLst = new ArrayList<String>();
				for (HubCiti hubCiti2 : postalCodes) {
					containsKey = false;
					String key = hubCiti2.getState() + "/" + hubCiti2.getCity();
					String pCode = hubCiti2.getPostalCodes() + ",";

					PostalCode code = new PostalCode();
					code.setPostalCode(hubCiti2.getPostalCodes());
					code.setLocation(hubCiti2.getPostalCodes() + "," + hubCiti2.getCity() + "," + hubCiti2.getState());
					code.setIsAssocaited(hubCiti2.getIsAssocaited());

					selectedPostalCodeList.add(code);

					if (!postalCodeMap.isEmpty()) {
						for (Entry<String, String> entry : postalCodeMap.entrySet()) {
							if (entry.getKey().equalsIgnoreCase(key)) {
								containsKey = true;
								String value = entry.getValue();
								value += pCode;
								entry.setValue(value);
							}
						}
						if (!containsKey) {
							postalCodeMap.put(key, pCode);
						}
					} else {
						postalCodeMap.put(key, pCode);
					}

					defltPstlCdeLst.add(hubCiti2.getPostalCodes());
				}

			}

			session.setAttribute("regpostalcodesmap", postalCodeMap);
			session.setAttribute("defltPstlCdeLst", defltPstlCdeLst);
			session.setAttribute("selectedRegPostalCodes", selectedPostalCodeList);
			request.setAttribute(ApplicationConstants.LOWERLIMIT, hubCiti.getLowerLimit());
			citi.setSearchKey(hubCiti.getSearchKey());
			model.put("RegionForm", citi);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView("editregionadmin");
	}

	@RequestMapping(value = "/updateregion.htm", method = RequestMethod.POST)
	public ModelAndView updateRegionDetails(@ModelAttribute("RegionForm") HubCiti hubCiti, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		final String strMethodName = "updateRegionDetails";
		final String strViewName = "regionAdmin";

		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		try {

			final String userId = (String) session.getAttribute("UserId");
			String status = null;
			final String searchKey = hubCiti.getSearchKey();
			final Boolean showDeactivated = hubCiti.getShowDeactivated();
			Pagination objPage = null;
			Integer iLowerLimit = 0;
			Integer iCurrentPage = 1;
			Integer iRecordCount = 10;
			ArrayList<HubCiti> postalCodes = null;
			RegionInfo regionInfo = null;
			Integer iUserId = null;
//			String strPostalcode = null;
			boolean isValidEmailId = true;
			boolean isValidSalesContact = true;
			boolean isValidEmails = true;
			try {
				final ServletContext servletContext = request.getSession().getServletContext();
				final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
				final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

				hubCiti.setAppRole(ApplicationConstants.REGIONAPP);
				if (null != hubCiti.getLowerLimit()) {
					iLowerLimit = hubCiti.getLowerLimit();
					iCurrentPage = (iLowerLimit + iRecordCount) / iRecordCount;
				}

				if (null != userId) {
					iUserId = Integer.parseInt(userId);

				}
				request.setAttribute(ApplicationConstants.LOWERLIMIT, iLowerLimit);
				request.setAttribute("state", hubCiti.getState());
				request.setAttribute("city", hubCiti.getCity());
				request.setAttribute("reghubids", hubCiti.getSelRegHubCitiIds());

				postalCodes = selectedRegPostalCodes(session);

				if (null != postalCodes && !postalCodes.isEmpty()) {
					hubCiti.setZipCode("true");
				} else {
					hubCiti.setZipCode("false");
				}

				regionAdminValidator.editValidate(hubCiti, result);
				if (result.hasErrors()) {
					return new ModelAndView("editregionadmin");
				}
				
				isValidEmailId = Utility.validateEmailId(hubCiti.getEmailID());
				isValidSalesContact = Utility.validateEmailId(hubCiti.getSalesPersonEmail());

				if (isValidEmailId == false)
				{
					regionAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDEMAIL);
				}
				if (isValidSalesContact == false)
				{
					regionAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDSALESCONTACT);
				}

				final String[] strEmails = hubCiti.getEmails().split(";");
				Set<String> list = new LinkedHashSet<String>(Arrays.asList(strEmails));
				list.removeAll(Arrays.asList("", null));
				int icount = 0;
				
				if (list.size() > 5) {
					regionAdminValidator.validate(hubCiti, result, ApplicationConstants.NUMBER_OF_EMAILS);
					if (result.hasErrors()) {
						return new ModelAndView("editregionadmin");
					}
				}
				
				if (strEmails.length != list.size()) {
					regionAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATE_EMAILS);

					if (result.hasErrors()) {
						return new ModelAndView("editregionadmin");
					}
				}
				
				
				if (list.size() != 1) {
					for (int i = 0; i < list.size(); i++) {
						if (!Utility.validateEmailId(strEmails[i])) {
							++icount;
						} 
					}
				} else {
					isValidEmails = Utility.validateEmailId(hubCiti.getEmails());
					
					if (!isValidEmails) {
						regionAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALID_EMAIL);

						if (result.hasErrors()) {
							return new ModelAndView("editregionadmin");
						}
					}
				}

				
				if (!isValidEmails || (icount > 0)) {
					regionAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALID_EMAILS);
				}

				if (result.hasErrors()) {
					return new ModelAndView("editregionadmin");
				}

				/*strPostalcode = String.valueOf(hubCiti.getDefaultZipcode());

				if (null != strPostalcode && strPostalcode.length() < 5) {
					regionAdminValidator.validate(hubCiti, result, ApplicationConstants.INVALIDZIPCODE);

				}
				if (result.hasErrors()) {
					return new ModelAndView("editregionadmin");
				}*/

				hubCiti.setUserID(new Integer(userId));

				/*
				 * To get un-associated postal code, City, State from
				 * HubCitiAdmin which is in Session. If more than one values,
				 * the postal codes, cities and states will be separated by
				 * comma (","). After getting values, it is removed from
				 * session.
				 */
				HubCiti removedPostalCodes = new HubCiti();
				if (session.getAttribute("removedPostalCodes") != null) {
					removedPostalCodes = (HubCiti) session.getAttribute("removedPostalCodes");
					session.removeAttribute("removedPostalCodes");
				}

				if (Utility.isEmptyOrNullString(hubCiti.getSelRegHubCitiIds())) {
					hubCiti.setSelRegHubCitiIds(null);
				} else {

					String ids = hubCiti.getSelRegHubCitiIds().trim();
					hubCiti.setSelRegHubCitiIds(ids);
				}
				
				StringBuilder sb = new StringBuilder();
				for (String emails : list) {
				    sb.append(emails).append(";");
				}
			
				hubCiti.setEmails(sb.toString());
				status = hubCitiService.saveAdmin(hubCiti, postalCodes, removedPostalCodes);

				if (!Utility.isEmptyOrNullString(status)) {
					if (status.equalsIgnoreCase(ApplicationConstants.DUPLICATEHUBCITI)) {
						regionAdminValidator.validate(hubCiti, result, ApplicationConstants.DUPLICATEHUBCITI);

						if (result.hasErrors()) {
							return new ModelAndView("editregionadmin");
						}
					}

					if ("Success".equalsIgnoreCase(status)) {
						session.removeAttribute("selectedRegPostalCodes");
						request.removeAttribute("state");
						request.removeAttribute("city");
						request.removeAttribute("reghubids");
						request.setAttribute("message", "Region Admin Updated successfully.");
					} else {
						request.setAttribute("message", "Failed to Update Region Admin.");
					}
				} else {
					request.setAttribute("message", "Failed to Update Region Admin.");
				}

				session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.HUBCITIADMIN);
				hubCiti.setUserID(iUserId);
				hubCiti.setSearchKey(null);
				regionInfo = hubCitiService.displayRegions(hubCiti);

				if (null != regionInfo && regionInfo.getRegionLst() != null && !regionInfo.getRegionLst().isEmpty()) {
					objPage = Utility.getPagination(regionInfo.getTotalSize(), iCurrentPage, "displayregions.htm", iRecordCount);
					session.setAttribute("pagination", objPage);
					session.setAttribute("regionlst", regionInfo.getRegionLst());
				} else {
					session.removeAttribute("pagination");

				}
				hubCiti.setSearchKey(null);

				model.put("RegionForm", hubCiti);

			} catch (ScanSeeServiceException exception) {
				throw new ScanSeeServiceException();
			}

			session.setAttribute("showDeactivated", showDeactivated);
			HubCiti citi = new HubCiti();
			citi.setSearchKey(searchKey);
			model.put("RegionForm", citi);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "/removeRegionzip.htm", method = RequestMethod.GET)
	public @ResponseBody
	final String removeSelectedPostalCode(@RequestParam(value = "zip", required = true) String zip, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "removeSelectedPostalCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String[] tempPostalCodes = zip.split(",");
		final String zipCode = tempPostalCodes[0];
		final String key = tempPostalCodes[2] + "/" + tempPostalCodes[1];
		String value = null;
		boolean removeKey = false;
		@SuppressWarnings("unchecked")
		final HashMap<String, String> postalCodeMap = (HashMap<String, String>) session.getAttribute("regpostalcodesmap");
		@SuppressWarnings("unchecked")
		ArrayList<String> defltPstlCdeLst = (ArrayList<String>) session.getAttribute("defltPstlCdeLst");

		defltPstlCdeLst.remove(zipCode);

		/*
		 * Getting unassociated postal code, city, state from HubCiti Admin.
		 */
		HubCiti removePostalCode = new HubCiti();
		removePostalCode.setPostalCodes(tempPostalCodes[0]);
		removePostalCode.setCity(tempPostalCodes[1]);
		removePostalCode.setState(tempPostalCodes[2]);

		/*
		 * Adding unassociated postal code, city, state from HubCiti Admin to
		 * Session. If it exist in Session, values are inserted separated by
		 * comma (","). If dose not exist, new session parameter is created and
		 * added.
		 */
		if (session.getAttribute("removedPostalCodes") == null) {
			session.setAttribute("removedPostalCodes", removePostalCode);
		} else {
			HubCiti postalCodes = new HubCiti();
			postalCodes = (HubCiti) session.getAttribute("removedPostalCodes");
			postalCodes.setPostalCodes(postalCodes.getPostalCodes() + "," + removePostalCode.getPostalCodes());
			postalCodes.setCity(postalCodes.getCity() + "," + removePostalCode.getCity());
			postalCodes.setState(postalCodes.getState() + "," + removePostalCode.getState());
		}

		if (null != postalCodeMap && !postalCodeMap.isEmpty()) {
			for (Entry<String, String> entry : postalCodeMap.entrySet()) {
				if (entry.getKey().equalsIgnoreCase(key)) {
					value = entry.getValue().replaceAll(zipCode, "");
					value = value.replace(",,", ",");
					if (value.startsWith(",")) {
						value = value.substring(1, value.length());
					}
					if (!"".equals(value)) {
						entry.setValue(value);
					} else {
						removeKey = true;
					}
				}

			}
			if (removeKey) {
				postalCodeMap.remove(key);
			}
		}

		session.setAttribute("regpostalcodesmap", postalCodeMap);
		session.setAttribute("defltPstlCdeLst", defltPstlCdeLst);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return ApplicationConstants.SUCCESS;
	}

}

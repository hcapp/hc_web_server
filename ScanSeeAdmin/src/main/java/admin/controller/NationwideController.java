package admin.controller;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import admin.service.HubCitiService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.APIPartner;
import common.pojo.HubCiti;
import common.pojo.RegionInfo;
import common.tags.Pagination;
import common.util.Utility;

@Controller
public class NationwideController {

	private static final Logger LOG = LoggerFactory.getLogger(NationwideController.class);

	@RequestMapping(value = "/nationwide.htm", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView getNationwideDeals(@ModelAttribute("nationwideform") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		final String strMethodName = "getNationwideDeals";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		
		final String strViewName = "nationwideadmin";
		session.setAttribute(ApplicationConstants.MODULENAME, ApplicationConstants.NATIONADMIN);

		// for pagination.
		Pagination objPage = null;
		Integer iLowerLimit = hubCiti.getLowerLimit();
		Integer iCurrentPage = 1;
		Integer iRecordCount = 10;
		String strPageNum = "0";
		String strPageFlag = null;
		RegionInfo regionInfo = null;
		ArrayList<APIPartner> apiPartners = null; 
		
		try {
			
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			HubCitiService hubCitiService = (HubCitiService) webApplicationContext.getBean("hubCitiService");
			final String strUserId = (String) session.getAttribute("UserId");
			
			if (null != strUserId) {
				hubCiti.setUserID(Integer.valueOf(strUserId));
			}
			
			if(null == iLowerLimit || "".equals(iLowerLimit)) {
				iLowerLimit = 0;
			}
			
			// Removing session attributes.
			session.removeAttribute("hubcitireglst");

			// for pagination.
			strPageFlag = request.getParameter("pageFlag");
			if (null != strPageFlag && "true".equals(strPageFlag)) {
				strPageNum = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(strPageNum) != 0) {
					iCurrentPage = Integer.valueOf(strPageNum);
					final int number = Integer.valueOf(iCurrentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					iLowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {				
				iCurrentPage = (iLowerLimit + iRecordCount) / iRecordCount;
			}
			// For updating the hubciti/region ids depending upon nationwide checked or not
			/*if (!Utility.isEmptyOrNullString(hubCiti.getChkdNatId()) || !Utility.isEmptyOrNullString(hubCiti.getUnChkdNatId())) {
				strResponse = hubCitiService.saveNationWideDeals(hubCiti);
				request.setAttribute("successMsg", ApplicationConstants.NATIONWIDETEXT);
			}*/

			hubCiti.setLowerLimit(iLowerLimit);			
			if (Utility.isEmptyOrNullString(hubCiti.getSearchKey())) {
				hubCiti.setSearchKey(null);
			}
			
			regionInfo = hubCitiService.getHubCitiRegions(hubCiti);
			apiPartners = hubCitiService.fetchNWAPIPartners();

			session.setAttribute("apiPartners", apiPartners);
			
			if (null != regionInfo && !regionInfo.getRegionLst().isEmpty()) {
				objPage = Utility.getPagination(regionInfo.getTotalSize(), iCurrentPage, "nationwide.htm", iRecordCount);
				session.setAttribute("pagination", objPage);
				session.setAttribute("hubcitireglst", regionInfo.getRegionLst());

			} else {
				session.removeAttribute("pagination");				
				if(null != hubCiti.getSearchKey()) {
					request.setAttribute("noregfound", ApplicationConstants.NATIONHUBCITIREGNOTFOUNDTEXT);
				}
			}

			/*if (null == regionInfo && null != hubCiti.getSearchKey()) {				
				request.setAttribute("noregfound", ApplicationConstants.NATIONHUBCITIREGNOTFOUNDTEXT);
			}*/
			
			
			request.setAttribute("lowerLimit", iLowerLimit);
			//hubCiti.setIsNationwide(false);
			model.put("nationwideform", hubCiti);
			
		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "/saveNationwide.htm", method = RequestMethod.POST)
	public ModelAndView saveNationWideDeals(@ModelAttribute("nationwideform") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		
		final String strMethodName = "saveNationWideDeals";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		
		final String strViewName = "nationwideadmin";
		String strResponse = null;
		
		try {
			
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			HubCitiService hubCitiService = (HubCitiService) webApplicationContext.getBean("hubCitiService");
			final String strUserId = (String) session.getAttribute("UserId");
			
			RegionInfo regionInfo = null;
			Pagination objPage = null;
			Integer iLowerLimit = hubCiti.getLowerLimit();	
			Integer iRecordCount = 10;
			Integer iCurrentPage = (iLowerLimit + iRecordCount) / iRecordCount;
			
			if (null != strUserId) {
				hubCiti.setUserID(Integer.valueOf(strUserId));
			}
			
			if(hubCiti.getIsNationwide()) {
				if(hubCiti.getApiPartners().startsWith(",")) {
					hubCiti.setApiPartners(hubCiti.getApiPartners().substring(1));
				}
			}

			strResponse = hubCitiService.saveNationWideDeals(hubCiti);
			
			if(ApplicationConstants.SUCCESSTEXT.equals(strResponse)) {				

				regionInfo = hubCitiService.getHubCitiRegions(hubCiti);

				if (null != regionInfo && regionInfo.getRegionLst() != null && !regionInfo.getRegionLst().isEmpty()) {
					objPage = Utility.getPagination(regionInfo.getTotalSize(), iCurrentPage, "nationwide.htm", iRecordCount);
					session.setAttribute("pagination", objPage);
					session.setAttribute("hubcitireglst", regionInfo.getRegionLst());
				} else {
					session.removeAttribute("pagination");
					session.removeAttribute("hubcitireglst");
				}
				
				request.setAttribute("lowerLimit", iLowerLimit);
				request.setAttribute("successMsg", ApplicationConstants.NATIONWIDETEXT);
			}	

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);

	}
	
	@RequestMapping(value = "/fetchapipartner.htm", method = RequestMethod.GET)
	final ModelAndView displayAPIPartners(@ModelAttribute("nationwideform") HubCiti hubCiti, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException, ScanSeeWebSqlException {
		
		final String methodName = "displayAPIPartners";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		model.put("nationwideform", hubCiti);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("nationwidepopup");
	}

}

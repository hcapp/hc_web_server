/**
 * 
 */
package admin.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import admin.service.RetailerService;
import admin.validator.RetailerValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AlertCategory;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.Retailer;
import common.pojo.RetailerDetails;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationDetails;
import common.pojo.State;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is used as a controller class for retailer admin.
 * 
 * @author sangeetha.ts
 */
@Controller
public class RetailerController {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RetailerController.class);

	/**
	 * Variable for retailer validator
	 */
	private RetailerValidator retailerValidator;

	/**
	 * To set retailerValidator
	 * 
	 * @param retailerValidator
	 */
	@Autowired
	public final void setRetailerValidator(RetailerValidator retailerValidator) {
		this.retailerValidator = retailerValidator;
	}

	/**
	 * This method is used to display all the retailer with pagination.
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */

	/*
	 * @RequestMapping(value = "/displayretailer.htm", method =
	 * RequestMethod.GET) public ModelAndView
	 * displayRetailerGet(@ModelAttribute("admin") Retailer retailer,
	 * BindingResult result, HttpServletRequest request, HttpSession session,
	 * ModelMap model) throws ScanSeeServiceException { final String methodName
	 * = "displayRetailer"; LOG.info(ApplicationConstants.METHODSTART +
	 * methodName); LOG.info(ApplicationConstants.METHODEND + methodName);
	 * return new ModelAndView("displayretailers"); }
	 */
	@RequestMapping(value = "/displayretailer.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView displayRetailer(@ModelAttribute("admin") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "displayRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Boolean isBand = retailer.getIsBand();
		final String pageFlag = request.getParameter("pageFlag");
		RetailerDetails retailerDetails = null;
		Retailer requestretailer = null;
		ArrayList<State> states = new ArrayList<State>();
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		String iUserId = null;

		try {
			session.removeAttribute("searchKey");
			session.removeAttribute("city");
			session.removeAttribute("state");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.RETAILERADMIN);
			final Integer currentPageNum = (Integer) session.getAttribute("currentPageNum");

			iUserId = (String) session.getAttribute("UserId");

			if (null == iUserId || iUserId.equals("")) {
				return new ModelAndView("Login");
			}

			session.removeAttribute("currentPageNum");
			if (currentPageNum != null) {
				currentPage = currentPageNum;
			}
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}
			requestretailer = new Retailer();
			requestretailer.setLowerLimit(lowerLimit);
			requestretailer.setIsBand(isBand);
			retailerDetails = retailerService.displayRetailerList(requestretailer);

			states = retailerService.getAllStates();

			session.setAttribute("RetailerList", retailerDetails.getRetailerDetailList());
			session.setAttribute("statesListCreat", states);
			session.setAttribute("RetailerNameImg", "images/sorticonUp.png");

			objPage = Utility.getPagination(retailerDetails.getTotalSize(), currentPage, "displayretailer.htm", recordCount);
			session.setAttribute("pagination", objPage);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		retailer = new Retailer();
		retailer.setRetailLowerLimit(lowerLimit);
		retailer.setIsBand(isBand);
		model.put("retailer", retailer);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (null != retailer.getIsBand() && retailer.getIsBand() == true) {
			return new ModelAndView("displaybands");
		} else {
			return new ModelAndView("displayretailers");
		}

	}

	/**
	 * This method populate the city drop down based on the given state code.
	 * 
	 * @param stateCode
	 * @param city
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/fetchcity", method = RequestMethod.GET)
	public @ResponseBody
	final String getCities(@RequestParam(value = "statecode", required = true) String stateCode,
			@RequestParam(value = "city", required = true) String city, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<City> cities = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='city' id='city' class='textboxBig'><option value=''>--Select--</option>";

		innerHtml.append(finalHtml);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		cities = retailerService.fetchAllCities(stateCode);
		response.setContentType("text/xml");

		String selCity = (String) session.getAttribute("city");
		if (null == selCity || "".equals(selCity)) {
			selCity = city;
		}
		for (int i = 0; i < cities.size(); i++) {
			final String cityName = cities.get(i).getCityName();
			if (null != selCity && cityName.equals(selCity)) {
				innerHtml.append("<option selected=true >" + cities.get(i).getCityName() + "</option>");
			} else {
				innerHtml.append("<option>" + cities.get(i).getCityName() + "</option>");
			}
		}
		innerHtml.append("</select>");

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	/**
	 * This method is used for retailer search based on the retailer, city and
	 * state name as a search key
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/searchretailer.htm", method = RequestMethod.POST)
	public ModelAndView searchRetailer(@ModelAttribute("retailer") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "searchRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		RetailerDetails retailerDetails = null;
		Retailer requestretailer = null;
		ArrayList<State> states = new ArrayList<State>();
		Integer lowerLimit = retailer.getRetailLowerLimit();
		Boolean isBand = retailer.getIsBand();
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		String searchKey = null;
		String city = null;
		String state = null;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.RETAILERADMIN);
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {
				currentPage = (lowerLimit + recordCount) / recordCount;
			}
			requestretailer = new Retailer();
			requestretailer.setLowerLimit(lowerLimit);
			requestretailer.setIsBand(isBand);
			if (null != retailer.getSearchKey()) {
				searchKey = retailer.getSearchKey();
				requestretailer.setSearchKey(searchKey);
			}
			if (null != retailer.getState()) {
				state = retailer.getState();
				requestretailer.setState(state);
			}
			if (null != retailer.getCity()) {
				city = retailer.getCity();
				requestretailer.setCity(city);
			}

			retailerDetails = retailerService.displayRetailerList(requestretailer);

			if (null == retailerDetails || retailerDetails.getTotalSize() == 0) {
				if (null != isBand && isBand) {
					request.setAttribute("errorMessage", "No Bands Found.");
				} else {
					request.setAttribute("errorMessage", "No Retailers Found.");
				}
			}

			states = retailerService.getAllStates();

			session.setAttribute("RetailerList", retailerDetails.getRetailerDetailList());
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("city", city);
			session.setAttribute("state", state);
			session.setAttribute("statesListCreat", states);
			session.setAttribute("RetailerNameImg", "images/sorticonUp.png");

			objPage = Utility.getPagination(retailerDetails.getTotalSize(), currentPage, "searchretailer.htm", recordCount);
			session.setAttribute("pagination", objPage);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		retailer.setRetailLowerLimit(lowerLimit);
		model.put("retailer", retailer);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (null != isBand && isBand) {
			return new ModelAndView("displaybands");
		} else {
			return new ModelAndView("displayretailers");
		}

	}

	/**
	 * This method is used to delete retailers.
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/deleteretailer.htm", method = RequestMethod.POST)
	public ModelAndView deleteRetailer(@ModelAttribute("retailer") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "deleteRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Boolean isBand = retailer.getIsBand();

		RetailerDetails retailerDetails = null;
		Retailer requestretailer = null;
		ArrayList<State> states = new ArrayList<State>();
		final Integer lowerLimit = retailer.getRetailLowerLimit();
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		final String searchKey = (String) session.getAttribute("searchKey");
		final String city = (String) session.getAttribute("city");
		final String state = (String) session.getAttribute("state");
		String status = null;
		final String userID = (String) session.getAttribute("UserId");
		final Integer userId = Integer.parseInt(userID);
		final String retailerIds = retailer.getRetailerIds();

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.RETAILERADMIN);

			status = retailerService.deleteRetailer(retailerIds, userId, isBand);

			if ("Failure".equalsIgnoreCase(status)) {
				final String errorMsg = "Error occurred while deleting Retailer";
				request.setAttribute("errorMessage", errorMsg);
			} else {
				if (null != isBand && isBand == true) {
					final String msg = "Band deleted successfully";
					request.setAttribute("errorMessage", msg);
				} else {
					final String msg = "Retailer deleted successfully";
					request.setAttribute("errorMessage", msg);
				}

			}

			requestretailer = new Retailer();
			requestretailer.setLowerLimit(lowerLimit);
			requestretailer.setIsBand(isBand);

			if (null != searchKey) {
				requestretailer.setSearchKey(searchKey);
			}
			if (null != city) {
				requestretailer.setCity(city);
			}
			if (null != state) {
				requestretailer.setState(state);
			}

			retailerDetails = retailerService.displayRetailerList(requestretailer);

			states = retailerService.getAllStates();

			session.setAttribute("RetailerList", retailerDetails.getRetailerDetailList());
			session.setAttribute("statesListCreat", states);
			session.setAttribute("RetailerNameImg", "images/sorticonUp.png");

			currentPage = (lowerLimit + recordCount) / recordCount;
			objPage = Utility.getPagination(retailerDetails.getTotalSize(), currentPage, "searchretailer.htm", recordCount);
			session.setAttribute("pagination", objPage);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		retailer.setRetailLowerLimit(lowerLimit);
		model.put("retailer", retailer);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (null != isBand && isBand == true) {
			return new ModelAndView("displaybands");
		} else {
			return new ModelAndView("displayretailers");
		}
	}

	/**
	 * This method is used to display retailer locations.
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/displayretailerLocation.htm", method ={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView displayRetailerLocation(@ModelAttribute("retailer") RetailerLocation retailer, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "displayRetailerLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.removeAttribute("locSearchkey");
		session.removeAttribute("searchBy");

		final String pageFlag = request.getParameter("pageFlag");
		RetailerLocationDetails retailerLocationDetails = null;
		RetailerLocation retailerLocation = null;
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		session.setAttribute("minCropWd", 28);
		session.setAttribute("minCropHt", 28);
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.RETAILERADMIN);
			session.setAttribute("imageCropPage", "retlocpage");
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}
			retailerLocation = new RetailerLocation();
			retailerLocation.setLowerLimit(lowerLimit);
			retailerLocation.setRetailId(retailer.getRetailId());

			retailerLocationDetails = new RetailerLocationDetails();
			retailerLocationDetails = retailerService.displayRetailerLocation(retailerLocation);

			session.setAttribute("RetailerLocationList", retailerLocationDetails.getRetailerLocations());
			session.setAttribute("RetailerName", retailer.getRetailName());
			session.setAttribute("RetailerId", retailer.getRetailId());
			// session.setAttribute("cateImgPath",
			// retailerLocationDetails.getCateImgName());
			objPage = Utility.getPagination(retailerLocationDetails.getTotalSize(), currentPage, "displayretailerLocation.htm", recordCount);
			session.setAttribute("pagination", objPage);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}
		session.setAttribute("cateImgPath", ApplicationConstants.BLANKIMAGE);

		retailer.setRetailLocLowerLimit(lowerLimit);
		model.put("retailer", retailer);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayretailerlocations");
	}

	/**
	 * This method is used search retailer location based on store id and
	 * address.
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/searchretailerLocation.htm", method = RequestMethod.POST)
	public ModelAndView searchRetailerLocation(@ModelAttribute("retailer") RetailerLocation retailer, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "searchRetailerLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String pageFlag = request.getParameter("pageFlag");
		RetailerLocationDetails retailerLocationDetails = null;
		RetailerLocation retailerLocation = null;
		Integer lowerLimit = retailer.getRetailLocLowerLimit();
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		final String locSearchKey = retailer.getLocSearchkey();
		final String searchBy = retailer.getSearchBy();
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.RETAILERADMIN);
			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {
				currentPage = (lowerLimit + recordCount) / recordCount;
			}
			retailerLocation = new RetailerLocation();
			retailerLocation.setLowerLimit(lowerLimit);
			retailerLocation.setRetailId(retailer.getRetailId());

			if ("SI".equalsIgnoreCase(searchBy)) {
				retailerLocation.setStoreIdentification(locSearchKey);
			}
			if ("Address".equalsIgnoreCase(searchBy)) {
				retailerLocation.setAddress1(locSearchKey);
			}

			retailerLocationDetails = retailerService.displayRetailerLocation(retailerLocation);

			session.setAttribute("locSearchkey", locSearchKey);
			session.setAttribute("searchBy", searchBy);

			if (null == retailerLocationDetails || retailerLocationDetails.getTotalSize() <= 0) {
				final String errorMsg = "No Retailer Locations Found";
				request.setAttribute("errorMessage", errorMsg);
			}

			session.setAttribute("RetailerLocationList", retailerLocationDetails.getRetailerLocations());
			session.setAttribute("RetailerName", retailer.getRetailName());
			session.setAttribute("RetailerId", retailer.getRetailId());

			objPage = Utility.getPagination(retailerLocationDetails.getTotalSize(), currentPage, "searchretailerLocation.htm", recordCount);
			session.setAttribute("pagination", objPage);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		retailer.setRetailLocLowerLimit(lowerLimit);
		model.put("retailer", retailer);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayretailerlocations");
	}

	/**
	 * This method is used to delete retailer locations.
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/deleteretailerlocation.htm", method = RequestMethod.POST)
	public ModelAndView deleteRetailerLocation(@ModelAttribute("retailer") RetailerLocation retailer, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "deleteRetailerLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Integer lowerLimit = retailer.getRetailLocLowerLimit();
		final int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 10;
		final String locSearchKey = (String) session.getAttribute("locSearchkey");
		final String searchBy = (String) session.getAttribute("searchBy");
		RetailerLocationDetails retailerLocationDetails = null;
		RetailerLocation retailerLocation = null;
		String status = null;
		final String userID = (String) session.getAttribute("UserId");
		final Integer userId = Integer.parseInt(userID);
		final String retailerLocIds = retailer.getRetailerLocIds();

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			session.setAttribute("moduleName", ApplicationConstants.RETAILERADMIN);
			status = retailerService.deleteRetailerLocation(retailerLocIds, userId);

			if ("Failure".equalsIgnoreCase(status)) {
				final String errorMsg = "Error occurred while deleting Retailer Location";
				request.setAttribute("errorMessage", errorMsg);
			} else {
				final String msg = "Retailer Location deleted successfully";
				request.setAttribute("errorMessage", msg);
			}

			retailerLocation = new RetailerLocation();
			if (null == lowerLimit || lowerLimit.equals("")) {
				retailerLocation.setLowerLimit(0);
			} else {
				retailerLocation.setLowerLimit(lowerLimit);
			}
			retailerLocation.setRetailId(retailer.getRetailId());

			if ("SI".equalsIgnoreCase(searchBy)) {
				retailerLocation.setStoreIdentification(locSearchKey);
			}
			if ("Address".equalsIgnoreCase(searchBy)) {
				retailerLocation.setAddress1(locSearchKey);
			}

			retailerLocationDetails = new RetailerLocationDetails();
			retailerLocationDetails = retailerService.displayRetailerLocation(retailerLocation);

			session.setAttribute("RetailerLocationList", retailerLocationDetails.getRetailerLocations());

			objPage = Utility.getPagination(retailerLocationDetails.getTotalSize(), currentPage, "searchretailer.htm", recordCount);
			session.setAttribute("pagination", objPage);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		retailer.setRetailLocLowerLimit(lowerLimit);
		model.put("retailer", retailer);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("displayretailerlocations");
	}

	@RequestMapping(value = "/savelocationlist.htm", method = RequestMethod.POST)
	public final ModelAndView saveLocationList(@ModelAttribute("retailer") RetailerLocation retailerLocation, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "saveLocationList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		int currentPage = 1;
		String pageNumber = "0";
		Integer lowerLimit = retailerLocation.getRetailLocLowerLimit();
		final String pageFlag = retailerLocation.getPageFlag();
		final String userID = (String) session.getAttribute("UserId");
		final Integer userId = Integer.parseInt(userID);
		final String locSearchKey = retailerLocation.getLocSearchkey();
		final String searchBy = retailerLocation.getSearchBy();
		pageNumber = retailerLocation.getPageNumber();
		Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
		Pagination objPage = null;
		RetailerLocationDetails retailerLocationDetails = null;
		RetailerLocation retailerLocation1 = null;
		final Integer recordCount = 10;

		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			retailerLocation1 = new RetailerLocation();
			retailerLocation1.setRetailId(retailerLocation.getRetailId());
			if ("SI".equalsIgnoreCase(searchBy)) {
				retailerLocation1.setStoreIdentification(locSearchKey);
			}
			if ("Address".equalsIgnoreCase(searchBy)) {
				retailerLocation1.setAddress1(locSearchKey);
			}

			if (pageFlag != null && pageFlag.equals("false")) {
				retailerService.saveLocationList(retailerLocation, userId);
				request.setAttribute("message", "Retailer Location Information saved successfully.");

				retailerLocation1.setLowerLimit(0);
				retailerLocationDetails = retailerService.displayRetailerLocation(retailerLocation1);
			} else {
				pageNumber = request.getParameter("pageNumber");

				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					currentPage = (lowerLimit + recordCount) / recordCount;
				}
				retailerService.saveLocationList(retailerLocation, userId);
				request.setAttribute("message", "Retailer Location Information saved successfully.");

				retailerLocation1.setLowerLimit(lowerLimit);
				retailerLocationDetails = retailerService.displayRetailerLocation(retailerLocation1);
			}

			session.setAttribute("RetailerLocationList", retailerLocationDetails.getRetailerLocations());
			session.setAttribute("RetailerName", retailerLocation.getRetailName());
			session.setAttribute("RetailerId", retailerLocation.getRetailId());

			objPage = Utility.getPagination(retailerLocationDetails.getTotalSize(), currentPage, "searchretailerLocation.htm", recordCount);
			session.setAttribute("pagination", objPage);
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED, e.getMessage());
			throw e;
		}

		retailerLocation.setPageFlag("false");
		retailerLocation.setRetailLocLowerLimit(lowerLimit);
		model.put("retailer", retailerLocation);
		return new ModelAndView("displayretailerlocations");
	}

	/**
	 * This method is used to update retailer information.
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/saveRetailerInfo.htm", method = RequestMethod.POST)
	ModelAndView saveRetailerInfo(@ModelAttribute("retailer") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String methodName = "saveRetailerInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Boolean isBand = retailer.getIsBand();

		String strViewName = "displayretailers";
		String strResponse = null;
		Retailer retailerObj = new Retailer();
		Integer userId = null;
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			final String userID = (String) session.getAttribute("UserId");
			if (null != userID && !"".equals(userID)) {
				userId = Integer.parseInt(userID);
			}

			// converting JSON to retailer object.
			if (null != retailer.getRetailInfoJson() && !"".equals(retailer.getRetailInfoJson())) {
				retailerObj = Utility.jsonToObjectRetailerList(retailer.getRetailInfoJson());

			}
			retailerObj.setUserId(userId);
			retailerObj.setIsBand(isBand);
			strResponse = retailerService.saveRetailerInfo(retailerObj);

			if (ApplicationConstants.SUCCESSTEXT.equalsIgnoreCase(strResponse)) {

				if (null != isBand && isBand == true) {
					final String msg = "Band details updated successfully";
					request.setAttribute("retUpdateMessage", msg);
				} else {
					final String msg = "Retailer details updated successfully";
					request.setAttribute("retUpdateMessage", msg);
				}

				ArrayList<Retailer> RetailerList = (ArrayList<Retailer>) session.getAttribute("RetailerList");

				for (Retailer retailer2 : RetailerList) {
					Integer retailId = retailer2.getRetailId();
					if (retailId.equals(retailerObj.getRetailId())) {
						retailer2.setRetailName(retailerObj.getRetailName());
						retailer2.setAddress1(retailerObj.getAddress1());
						retailer2.setCity(retailerObj.getCity());
						retailer2.setState(retailerObj.getState());
						retailer2.setPostalCode(retailerObj.getPostalCode());
						retailer2.setRetUrl(retailerObj.getRetUrl());
						retailer2.setLatitude(retailerObj.getLatitude());
						retailer2.setLongitude(retailerObj.getLongitude());
						retailer2.setPhoneNo(retailerObj.getPhoneNo());
						retailer2.setIsPaid(retailerObj.getIsPaid());
					}
				}
				session.setAttribute("RetailerList", RetailerList);
			} else {
				final String errorMsg = "Error occurred while updating Retailer details";
				request.setAttribute("retUpdateMessage", errorMsg);

			}
		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		if (null != isBand && isBand == true) {
			return new ModelAndView("displaybands");
		} else {
			return new ModelAndView(strViewName);
		}
	}

	/**
	 * This method is used fetch retailer cities.
	 * 
	 * @param stateCode
	 * @param city
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 * @throws ScanSeeWebSqlException
	 */

	@RequestMapping(value = "/getRetCity", method = RequestMethod.GET)
	public @ResponseBody
	final String getRetailerCities(@RequestParam(value = "statecode", required = true) String stateCode,
			@RequestParam(value = "city", required = true) String city, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getRetailerCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<City> cities = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='retCity'  class='textboxBig'><option value=''>--Select--</option>";

		innerHtml.append(finalHtml);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		try {
			cities = retailerService.fetchAllCities(stateCode);
			response.setContentType("text/xml");

			for (int i = 0; i < cities.size(); i++) {
				final String cityName = cities.get(i).getCityName();
				if (null != city && cityName.equals(city)) {
					innerHtml.append("<option selected=true >" + cities.get(i).getCityName() + "</option>");
				} else {
					innerHtml.append("<option>" + cities.get(i).getCityName() + "</option>");
				}
			}
			innerHtml.append("</select>");
		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	/**
	 * This method is used to fetch retailer states.
	 * 
	 * @param state
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/getRetStates", method = RequestMethod.GET)
	public @ResponseBody
	final String getRetailerStates(@RequestParam(value = "state", required = true) String state, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getRetailerCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String vCity = null;
		ArrayList<State> retStatelst = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='retState'  class='textboxBig' onchange=loadCities('')><option value=''>--Select--</option>";

		innerHtml.append(finalHtml);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		try {
			retStatelst = retailerService.getAllStates();
			response.setContentType("text/xml");

			for (int i = 0; i < retStatelst.size(); i++) {
				final String stateAbv = retStatelst.get(i).getStateabbr();
				if (null != state && stateAbv.equals(state)) {
					innerHtml.append("<option value=" + retStatelst.get(i).getStateabbr() + "  selected=true >" + retStatelst.get(i).getStateName()
							+ "</option>");
				} else {
					innerHtml.append("<option value=" + retStatelst.get(i).getStateabbr() + ">" + retStatelst.get(i).getStateName() + "</option>");
				}
			}
			innerHtml.append("</select>");
		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	/**
	 * This Controller method used get All Business Category associated to
	 * Retailer.
	 * 
	 * @param hubCiti
	 * @param request
	 * @param response
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/fetchbuscategory.htm", method = RequestMethod.GET)
	final ModelAndView getBusCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws ScanSeeServiceException, ScanSeeWebSqlException {
		LOG.info("Inside RetailerController : getBusCategory");
		ArrayList<Category> arBusCategoryList = null;
		Retailer retailer = new Retailer();

		String retailId = request.getParameter("retailId");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		retailer.setRetailId(Integer.valueOf(retailId));
		retailer.setCategoryType(ApplicationConstants.BUSINESS_CATEGORIES);
		arBusCategoryList = retailerService.fetchBusCategory(retailer);

		session.setAttribute("busCategory", arBusCategoryList);
		model.put("busCategory", retailer);
		return new ModelAndView("displaybuscategory");
	}

	/**
	 * This Controller method used get All Business Category associated to
	 * Retailer.
	 * 
	 * @param hubCiti
	 * @param request
	 * @param response
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/editbuscategory.htm", method = RequestMethod.POST)
	final ModelAndView editBusCategory(@ModelAttribute("retailer") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException, ScanSeeWebSqlException {
		LOG.info("Inside RetailerController : editBusCategory");
		ArrayList<Category> arBusCategoryList = null;
		String status = null;
		String strMsg = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		retailer.setCategoryType(ApplicationConstants.BUSINESS_CATEGORIES);
		status = retailerService.updateBusCategory(retailer);

		if (ApplicationConstants.FAILURETEXT.equalsIgnoreCase(status)) {
			strMsg = ApplicationConstants.UPDATE_BUSINESS_CATEGORIES_ERROR_MSG;
			request.setAttribute("errorMessage", strMsg);
		} else if (ApplicationConstants.SUCCESSTEXT.equalsIgnoreCase(status)) {
			strMsg = ApplicationConstants.UPDATE_BUSINESS_CATEGORIES_SUCCESS_MSG;
			request.setAttribute("successMessage", strMsg);
		}
		arBusCategoryList = retailerService.fetchBusCategory(retailer);
		session.setAttribute("busCategory", arBusCategoryList);
		model.put("busCategory", retailer);
		return new ModelAndView("displaybuscategory");
	}

	/**
	 * This Controller method used get All Sub Category associated to Location.
	 * 
	 * @param hubCiti
	 * @param request
	 * @param response
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/fetchsubcategory.htm", method = RequestMethod.GET)
	final ModelAndView getSubCategory(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws ScanSeeServiceException, ScanSeeWebSqlException {
		LOG.info("Inside RetailerController : getSubCategory");
		ArrayList<Category> arSubCategoryList = null;
		Retailer retailer = new Retailer();
		String retailId = request.getParameter("retailId");
		String retailLocationId = request.getParameter("retailLocationId");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		retailer.setRetailLocationId(Integer.valueOf(retailLocationId));
		retailer.setRetailId(Integer.valueOf(retailId));
		arSubCategoryList = retailerService.fetchBusCategory(retailer);
		session.removeAttribute("subCategory");
		if (arSubCategoryList != null && !arSubCategoryList.isEmpty()) {
			session.setAttribute("subCategory", arSubCategoryList);
		} else {
			request.setAttribute("errorMessage", ApplicationConstants.ASSOCIATE_BUSINESS_CATEGORY);
		}

		model.put("subCategory", retailer);
		return new ModelAndView("displaysubcategory");
	}

	/**
	 * This Controller method to sub category associated to Location.
	 * 
	 * @param hubCiti
	 * @param request
	 * @param response
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/editsubcategory.htm", method = RequestMethod.POST)
	final ModelAndView editSubCategory(@ModelAttribute("retailer") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException, ScanSeeWebSqlException {
		LOG.info("Inside RetailerController : editSubCategory");
		ArrayList<Category> arSubCategoryList = null;
		String status = null;
		String strMsg = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final String userID = (String) session.getAttribute("UserId");
		retailer.setUserId(Integer.parseInt(userID));
		status = retailerService.updateBusCategory(retailer);

		if (ApplicationConstants.FAILURETEXT.equalsIgnoreCase(status)) {
			strMsg = ApplicationConstants.UPDATE_SUB_CATEGORIES_ERROR_MSG;
			request.setAttribute("errorMessage", strMsg);
		} else if (ApplicationConstants.SUCCESSTEXT.equalsIgnoreCase(status)) {
			strMsg = ApplicationConstants.UPDATE_SUB_CATEGORIES_SUCCESS_MSG;
			request.setAttribute("successMessage", strMsg);
		}

		arSubCategoryList = retailerService.fetchBusCategory(retailer);
		session.setAttribute("subCategory", arSubCategoryList);
		model.put("subCategory", retailer);
		return new ModelAndView("displaysubcategory");
	}

	/**
	 * This method to get retailer login details (user name, email, password) to
	 * create/update.
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/retailerCreateLogin.htm", method = RequestMethod.POST)
	final ModelAndView retailerCreateLogin(@ModelAttribute("retailer") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException, ScanSeeWebSqlException {
		LOG.info("Inside RetailerController : retailerCreateLogin");
		Retailer retailerDetails = null;
		Pagination objPage = null;
		Boolean isBand = retailer.getIsBand();
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			retailerDetails = new Retailer();
			if (retailer.getIsLoginExist() != null && retailer.getIsLoginExist() == 1) {
				retailerDetails = (Retailer) retailerService.retailerLoginDetails(retailer);
			}
		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException();
		}
		objPage = new Pagination();
		objPage = (Pagination) session.getAttribute("pagination");
		retailerDetails.setCurrentPageNum(objPage.getCurrentPage());
		retailerDetails.setIsLoginExist(retailer.getIsLoginExist());
		retailerDetails.setRetailName(retailer.getRetailName());
		retailerDetails.setRetailId(retailer.getRetailId());
		retailerDetails.setIsBand(isBand);
		model.put("retailer", retailerDetails);

		ModelAndView view = new ModelAndView("retailerCreateLogin");
		return view;
	}

	/**
	 * This method to save/update retailer login details i.e. User name, email
	 * and auto generated password.
	 * 
	 * @param retailer
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @param map
	 * @return
	 * @throws ScanSeeServiceException
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/saveRetailerLogin.htm", method = RequestMethod.POST)
	final ModelAndView saveRetailerLoginDetails(@ModelAttribute("retailer") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) throws ScanSeeServiceException, ScanSeeWebSqlException {
		LOG.info(ApplicationConstants.METHODSTART + "Inside RetailerController : saveRetailerLogin");
		String status;
		Pagination objPage = null;
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			retailerValidator.validate(retailer, result);
			if (result.hasErrors()) {
				return new ModelAndView("retailerCreateLogin");
			}

			status = retailerService.saveRetailerLoginDetails(retailer);

			if (!ApplicationConstants.SUCCESSTEXT.equals(status)) {
				retailerValidator.validate(retailer, result, status);
				if (result.hasErrors()) {
					return new ModelAndView("retailerCreateLogin");
				}
			}

			session.setAttribute("currentPageNum", retailer.getCurrentPageNum());

		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + "Inside RetailerController : saveRetailerLogin");

		ModelAndView view = null;

		if (null != retailer.getIsBand() && retailer.getIsBand() == true) {
			view = new ModelAndView(new RedirectView("displayretailer.htm?isBand=true"));
		} else {
			view = new ModelAndView(new RedirectView("displayretailer.htm"));
		}
		return view;
	}

	@RequestMapping(value = "/saveRetPayInfo", method = RequestMethod.GET)
	public @ResponseBody
	final String saveRetPayInfo(@RequestParam(value = "retailID", required = true) Integer retailID,
			@RequestParam(value = "isPaid", required = true) Boolean isPaid, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException, ScanSeeWebSqlException {

		final String methodName = "saveRetPayInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;
		final String userID = (String) session.getAttribute("UserId");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		try {

			status = retailerService.saveRetailerPaymentInfo(retailID, Integer.parseInt(userID), isPaid, false);

		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	@RequestMapping(value = "/saveRetPayInfoBand", method = RequestMethod.GET)
	public @ResponseBody
	final String saveRetPayInfoBand(@RequestParam(value = "retailID", required = true) Integer retailID,
			@RequestParam(value = "isPaid", required = true) Boolean isPaid, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException, ScanSeeWebSqlException {

		final String methodName = "saveRetPayInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;
		final String userID = (String) session.getAttribute("UserId");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		try {
			status = retailerService.saveRetailerPaymentInfo(retailID, Integer.parseInt(userID), isPaid, true);
		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeServiceException();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * This method populate the city drop down based on the given state code.
	 * 
	 * @param stateCode
	 * @param city
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	@RequestMapping(value = "/fetchbandcat", method = RequestMethod.GET)
	public @ResponseBody
	final String getBandcat(@RequestParam(value = "retailId") String retailId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getBandcat";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null == retailId) {

			retailId = (String) request.getAttribute("retailId");
		}
		Category category = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "";

		innerHtml.append(finalHtml);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		category = retailerService.getBandcat(retailId);
		response.setContentType("text/xml");
		innerHtml.append("<div class=\"input-actn\">");
		innerHtml.append("<input id=\"findchkAll\" type=\"checkbox\"> <label for=\"findchkAll\">Select All</label>");
		innerHtml.append("</div>");
		innerHtml.append("<ul class=\"infoList cmnList menuLst\" id=\"find\">");
		if (null != category) {

			if (null != category.getBuscategoryList()) {

				for (Category busCat : category.getBuscategoryList()) {

					innerHtml.append("<li>");
					innerHtml.append("<span class=\"cell zeroMrgn\">");
					if (null != busCat.getChecked() && busCat.getChecked() == 1) {
						innerHtml.append("<input name=\"findChk\" class=\"main-ctrgy\" type=\"checkbox\"" + "id=\" " + busCat.getCategoryId()
								+ "\" \"" + " checked>");
					} else {
						innerHtml.append("<input name=\"findChk\" class=\"main-ctrgy\" type=\"checkbox\"" + "id=\" " + busCat.getCategoryId()
								+ "\" \"" + ">");
					}
					innerHtml.append("<label for=\"find6\">" + busCat.getCategoryName() + "</label>");
					innerHtml.append("</span>");

					if (null != category.getSubcategoryList()) {
						innerHtml.append("<ul class=\"sub-ctgry\">");
						for (Category subCat : category.getSubcategoryList()) {

							if (null != subCat.getAssociated() && null != busCat.getCategoryId() && subCat.getAssociated() == busCat.getCategoryId()) {

								if (null != subCat.getChecked() && subCat.getChecked() == 1) {
									innerHtml.append("<li><input name=\"findChk\"  type=\"checkbox\"" + "id=\" " + subCat.getCategoryId() + "\" \""
											+ " checked>");
								} else {
									innerHtml.append("<li><input name=\"findChk\"  type=\"checkbox\"" + "id=\" " + subCat.getCategoryId() + "\" \""
											+ ">");
								}
								innerHtml.append("<label>" + subCat.getCategoryName() + "</label></li>");
							}
						}
						innerHtml.append("</ul>");

					}
					innerHtml.append("</li>");
				}
			}

		}
		innerHtml.append("</ul>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	@RequestMapping(value = "/savebandcat.htm", method = RequestMethod.GET)
	public @ResponseBody
	final String savebandcat(@RequestParam(value = "retailId") String retailId, @RequestParam(value = "buscatIds") String buscatIds,
			@RequestParam(value = "subcatIds") String subcatIds, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getBandcat";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final String userID = (String) session.getAttribute("UserId");
		String message = retailerService.savebandcat(retailId, buscatIds, subcatIds, userID);

		if (null == retailId) {

			retailId = (String) request.getAttribute("retailId");
			message = (String) request.getAttribute("message");
		}
		Category category = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "";

		innerHtml.append(finalHtml);
		category = retailerService.getBandcat(retailId);
		response.setContentType("text/xml");
		innerHtml.append("<span class=\"highLightMsg\">");
		innerHtml.append(message);
		innerHtml.append("</span>");
		innerHtml.append("<div class=\"input-actn\">");
		innerHtml.append("<input id=\"findchkAll\" type=\"checkbox\"> <label for=\"findchkAll\">Select All</label>");
		innerHtml.append("</div>");
		innerHtml.append("<ul class=\"infoList cmnList menuLst\" id=\"find\">");
		if (null != category) {

			if (null != category.getBuscategoryList()) {

				for (Category busCat : category.getBuscategoryList()) {

					innerHtml.append("<li>");
					innerHtml.append("<span class=\"cell zeroMrgn\">");
					if (null != busCat.getChecked() && busCat.getChecked() == 1) {
						innerHtml.append("<input name=\"findChk\" class=\"main-ctrgy\" type=\"checkbox\"" + "id=\" " + busCat.getCategoryId()
								+ "\" \"" + " checked>");
					} else {
						innerHtml.append("<input name=\"findChk\" class=\"main-ctrgy\" type=\"checkbox\"" + "id=\" " + busCat.getCategoryId()
								+ "\" \"" + ">");
					}
					innerHtml.append("<label for=\"find6\">" + busCat.getCategoryName() + "</label>");
					innerHtml.append("</span>");

					if (null != category.getSubcategoryList()) {
						innerHtml.append("<ul class=\"sub-ctgry\">");
						for (Category subCat : category.getSubcategoryList()) {

							if (null != subCat.getAssociated() && null != busCat.getCategoryId() && subCat.getAssociated() == busCat.getCategoryId()) {

								if (null != subCat.getChecked() && subCat.getChecked() == 1) {
									innerHtml.append("<li><input name=\"findChk\"  type=\"checkbox\"" + "id=\" " + subCat.getCategoryId() + "\" \""
											+ " checked>");
								} else {
									innerHtml.append("<li><input name=\"findChk\"  type=\"checkbox\"" + "id=\" " + subCat.getCategoryId() + "\" \""
											+ ">");
								}

								innerHtml.append("<label>" + subCat.getCategoryName() + "</label></li>");
							}
						}
						innerHtml.append("</ul>");
					}
					innerHtml.append("</li>");
				}
			}

		}

		innerHtml.append("</ul>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	@RequestMapping(value = "/adminFilters.htm", method = RequestMethod.GET)
	public final ModelAndView associateFilter(@ModelAttribute("associateFilters") Category objCategory, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		LOG.info("Inside EditRetailerProfileController : associateFilter() ");

		return new ModelAndView("adminFilters");
	}

	@RequestMapping(value = "/getretlocfilter.htm", method = RequestMethod.GET)
	@ResponseBody
	public final String getRetLocFilters(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {

		final String strMethodName = "getRetLocFilters";

		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		StringBuffer filterStr = null;
		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) webApplicationContext.getBean(ApplicationConstants.RETAILERSERVICE);
			String retailerId = null;
			String retailerLocId = null;
			ArrayList<Category> filters = null;
			AlertCategory filterCategorylst = null;
			retailerId = request.getParameter("retId");
			retailerLocId = request.getParameter("retLocId");

			session.removeAttribute("retId");
			session.removeAttribute("retlocId");
			Retailer retailer = new Retailer();
			if (null != retailerId) {
				retailer.setRetailId(Integer.valueOf(retailerId));
				session.setAttribute("retId", retailerId);
			}
			if (null != retailerLocId) {
				retailer.setRetailLocationId(Integer.valueOf(retailerLocId));
				session.setAttribute("retlocId", retailerLocId);
			}
			filterCategorylst = retailerService.getRetLocFilters(retailer);

			if (null != filterCategorylst) {

				filters = filterCategorylst.getCategorieslst();
				if (null != filters && !filters.isEmpty()) {
					int i = 0;
					Boolean subFilterFlag = false;
					Long categoryId = filters.get(0).getBusinessCategoryID();
					String categoryName = filters.get(0).getBusinessCategoryName();
					Integer filterId = filters.get(0).getFilterID();
					String filterName = filters.get(0).getFilterName();
					filterStr = new StringBuffer();
					filterStr.append("{\"categories\": [{\"id\":" + categoryId + ",\"Name\":\"" + categoryName + "\",\"filters\": [{\"id\":"
							+ filterId + ",\"Name\":\"" + filterName + "\"");

					for (Category category : filters) {
						if (categoryName.equals(category.getBusinessCategoryName())) {
							if (filterName.equals(category.getFilterName())) {
								if (null != category.getFilterValueID() && !"".equals(category.getFilterValueID())) {
									if (i == 0) {
										filterStr.append(",\"subfilters\":[{\"id\":" + category.getFilterValueID() + ",\"Name\":\""
												+ category.getFilterValueName() + "\"}");
										subFilterFlag = true;
										i++;
									} else {
										filterStr.append(",{\"id\":" + category.getFilterValueID() + ",\"Name\":\"" + category.getFilterValueName()
												+ "\"}");
									}
								}
							} else {
								i = 0;
								filterId = category.getFilterID();
								filterName = category.getFilterName();
								if (subFilterFlag != false) {
									filterStr.append("]");
								}
								subFilterFlag = false;
								filterStr.append("},{\"id\":" + filterId + ",\"Name\":\"" + filterName + "\"");
								if (null != category.getFilterValueID() && !"".equals(category.getFilterValueID())) {
									filterStr.append(",\"subfilters\":[{\"id\":" + category.getFilterValueID() + ",\"Name\":\""
											+ category.getFilterValueName() + "\"}");
									subFilterFlag = true;
									i++;
								}
							}
						} else {
							i = 0;
							categoryId = category.getBusinessCategoryID();
							categoryName = category.getBusinessCategoryName();
							filterId = category.getFilterID();
							filterName = category.getFilterName();
							if (subFilterFlag != false) {
								filterStr.append("]");
							}
							subFilterFlag = false;
							filterStr.append("}]},{\"id\":" + categoryId + ",\"Name\":\"" + categoryName + "\",\"filters\": [{\"id\":" + filterId
									+ ",\"Name\":\"" + filterName + "\"");
							if (null != category.getFilterValueID() && !"".equals(category.getFilterValueID())) {
								filterStr.append(",\"subfilters\":[{\"id\":" + category.getFilterValueID() + ",\"Name\":\""
										+ category.getFilterValueName() + "\"}");
								subFilterFlag = true;
								i++;
							}
						}
					}

					if (subFilterFlag != false) {
						filterStr.append("]");
					}
					filterStr.append("}]}]}");

					filterStr.append("&");
					filterStr.append(filterCategorylst.getFilterCategory());
					filterStr.append("&");
					filterStr.append(filterCategorylst.getFilters());
					filterStr.append("&");
					filterStr.append(filterCategorylst.getFilterValues());

				}
			}
		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getStackTrace());
			throw new ScanSeeServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		if (null != filterStr) {
			return filterStr.toString();
		} else {
			return "";
		}
		// return new ModelAndView(strViewName);

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/associatefilter.htm", method = RequestMethod.GET)
	public @ResponseBody
	final String associateFilters(@RequestParam(value = "filter") String filter, @RequestParam(value = "subFilters") String subFilters,
			@RequestParam(value = "cat") String cat, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException, ScanSeeWebSqlException {

		final String strMethodName = "associateFilters";
		final String strViewName = "adminFilters";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		ArrayList<Category> arSubCategoryList = null;
		String status = null;
		String strMsg = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final String userID = (String) session.getAttribute("UserId");
		RetailerLocation retailerLocation = new RetailerLocation();
		StringBuffer sb = null;
		String catids = null;
		List<Integer> categoiries = new ArrayList<Integer>();
		if (null != userID) {
			retailerLocation.setUserId(Integer.parseInt(userID));
		}
		
	//	if("NULL" != filter && !"NULL".equals(subFilters))
	//	{

		String retId = (String) session.getAttribute("retId");
		String retLocId = (String) session.getAttribute("retlocId");

		/*if (null != cat && !cat.equals("")) {

			StringTokenizer st = new StringTokenizer(cat, ",");

			while (st.hasMoreElements()) {
				String s = (String) st.nextElement();
				categoiries.add(Integer.valueOf(s));

			}
			Collections.reverse(categoiries);
			 sb = new StringBuffer();
			
			for(int i=0;i<categoiries.size();i++)
			{
				Integer s=categoiries.get(i);
				sb.append(String.valueOf(s));
				if(categoiries.size()-1>i)
				{
			sb.append(",");
				}
				
			}

			retailerLocation.setFilterCategory(sb.toString());

		} else {
			retailerLocation.setFilterCategory(null);
		}*/
		if("".equals(cat))
		{
			cat = null;
		}
			
		retailerLocation.setFilterCategory(cat);
		retailerLocation.setFilters(filter);
		retailerLocation.setFilterValues(subFilters);
		if (null != retId) {
			retailerLocation.setRetailId(Integer.valueOf(retId));
		}
		if (null != retLocId) { 
			retailerLocation.setRetailLocationId(Integer.valueOf(retLocId));
		}
		status = retailerService.associateFilters(retailerLocation);

		if (ApplicationConstants.FAILURETEXT.equalsIgnoreCase(status)) {
			strMsg = ApplicationConstants.FAILURETEXT;

		} else if (ApplicationConstants.SUCCESSTEXT.equalsIgnoreCase(status)) {
			strMsg = ApplicationConstants.SUCCESS;

		}

		//}else{
		//	strMsg="";
	//	}
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return strMsg;
	}
	public static void main(String a[])
	{
		String cat="23";
		List<Integer> categoiries = new ArrayList<Integer>();
		StringTokenizer st = new StringTokenizer(cat, ",");

		while (st.hasMoreElements()) {
			String s = (String) st.nextElement();
			categoiries.add(Integer.valueOf(s));

		}
		Collections.reverse(categoiries);
		StringBuffer sb = new StringBuffer();

		for(int i=0;i<categoiries.size();i++)
		{
			Integer s=categoiries.get(i);
			sb.append(String.valueOf(s));
			if(categoiries.size()-1>i)
			{
		sb.append(",");
			}
		}
		System.out.println(sb);
		
	}

}

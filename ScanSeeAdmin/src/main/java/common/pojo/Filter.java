/**
* @author Kumar D
* @version 0.1
*
* Class to store the details of a Filter.
*/
package common.pojo;

import java.util.ArrayList;

/**
 * Class to Filter Administartion.
 * @author kumar_dodda
 *
 */
public class Filter {
	/**
	 * variable for user Id
	 */
	private Integer userId;
	/**
	 * 
	 */
	private Integer filterId;
	/**
	 * 
	 */
	private String filterName;
	/**
	 * 
	 */
	private String fCategory;
	
	/**
	 * 
	 */
	private String fCategoryName;
	/**
	 * 
	 */
	private String fCategoryHidden;
	/**
	 * 
	 */
	private String filterOption;
	/**
	 * 
	 */
	private String fValue;
	/**
	 * 
	 */
	private String hiddenFValue;
	/**
	 * 
	 */
	private String fValueHidden;
	
	/**
	 * 
	 */
	private Integer fValueFlag;
	
	/**
	 * variable for hubCiti Name
	 */
	private String searchKey;
	/**
	 * 
	 */
	private Integer lowerLimit;
	/**
	 * 
	 */
	private String viewName;
	/**
	 * 
	 */
	private String recordCount;
	/**
	 * 
	 */
	private String removeMsg;
	/**
	 * 
	 */
	private String backButton;
	
	private ArrayList<SubFilter> subFilterlst;
	
	private Integer optionId;
	private String optionName;
	
	/**
	 * @return the removeMsg
	 */
	public String getRemoveMsg() {
		return removeMsg;
	}
	/**
	 * @param removeMsg the removeMsg to set
	 */
	public void setRemoveMsg(String removeMsg) {
		this.removeMsg = removeMsg;
	}
	/**
	 * @return the recordCount
	 */
	public String getRecordCount() {
		return recordCount;
	}
	/**
	 * @param recordCount the recordCount to set
	 */
	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}
	/**
	 * @return the filterId
	 */
	public Integer getFilterId() {
		return filterId;
	}
	/**
	 * @param filterId the filterId to set
	 */
	public void setFilterId(Integer filterId) {
		this.filterId = filterId;
	}
	/**
	 * @return the filterName
	 */
	public String getFilterName() {
		return filterName;
	}
	/**
	 * @param filterName the filterName to set
	 */
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	/**
	 * @return the fCategory
	 */
	public String getfCategory() {
		return fCategory;
	}
	/**
	 * @param fCategory the fCategory to set
	 */
	public void setfCategory(String fCategory) {
		this.fCategory = fCategory;
	}
	/**
	 * @return the fCategoryHidden
	 */
	public String getfCategoryHidden() {
		return fCategoryHidden;
	}
	/**
	 * @param fCategoryHidden the fCategoryHidden to set
	 */
	public void setfCategoryHidden(String fCategoryHidden) {
		this.fCategoryHidden = fCategoryHidden;
	}
	/**
	 * @return the filterOption
	 */
	public String getFilterOption() {
		return filterOption;
	}
	/**
	 * @param filterOption the filterOption to set
	 */
	public void setFilterOption(String filterOption) {
		this.filterOption = filterOption;
	}
	/**
	 * @return the fValue
	 */
	public String getfValue() {
		return fValue;
	}
	/**
	 * @param fValue the fValue to set
	 */
	public void setfValue(String fValue) {
		this.fValue = fValue;
	}
	/**
	 * @return the fValueHidden
	 */
	public String getfValueHidden() {
		return fValueHidden;
	}
	/**
	 * @param fValueHidden the fValueHidden to set
	 */
	public void setfValueHidden(String fValueHidden) {
		this.fValueHidden = fValueHidden;
	}
	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * @return the searchKey
	 */
	public String getSearchKey() {
		return searchKey;
	}
	/**
	 * @param searchKey the searchKey to set
	 */
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit() {
		return lowerLimit;
	}
	/**
	 * @param lowerLimit the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	/**
	 * @return the viewName
	 */
	public String getViewName() {
		return viewName;
	}
	/**
	 * @param viewName the viewName to set
	 */
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	/**
	 * @return the fCategoryName
	 */
	public String getfCategoryName() {
		return fCategoryName;
	}
	/**
	 * @param fCategoryName the fCategoryName to set
	 */
	public void setfCategoryName(String fCategoryName) {
		this.fCategoryName = fCategoryName;
	}
	/**
	 * @return the fValueFlag
	 */
	public Integer getfValueFlag() {
		return fValueFlag;
	}
	/**
	 * @param fValueFlag the fValueFlag to set
	 */
	public void setfValueFlag(Integer fValueFlag) {
		this.fValueFlag = fValueFlag;
	}
	/**
	 * @return the hiddenFValue
	 */
	public String getHiddenFValue() {
		return hiddenFValue;
	}
	/**
	 * @param hiddenFValue the hiddenFValue to set
	 */
	public void setHiddenFValue(String hiddenFValue) {
		this.hiddenFValue = hiddenFValue;
	}
	/**
	 * @return the backButton
	 */
	public String getBackButton() {
		return backButton;
	}
	/**
	 * @param backButton the backButton to set
	 */
	public void setBackButton(String backButton) {
		this.backButton = backButton;
	}
	public ArrayList<SubFilter> getSubFilterlst() {
		return subFilterlst;
	}
	public void setSubFilterlst(ArrayList<SubFilter> subFilterlst) {
		this.subFilterlst = subFilterlst;
	}
	public Integer getOptionId() {
		return optionId;
	}
	public void setOptionId(Integer optionId) {
		this.optionId = optionId;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

}

package common.pojo;

import java.util.ArrayList;

public class RegionInfo {

	private ArrayList<HubCiti> regionLst;
	
	private Integer totalSize;
	
	private String response;

	public Integer getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

	public ArrayList<HubCiti> getRegionLst() {
		return regionLst;
	}

	public void setRegionLst(ArrayList<HubCiti> regionLst) {
		this.regionLst = regionLst;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}

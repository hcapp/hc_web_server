package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * Pojo class for RetailerLocation
 * 
 * @author sangeetha.ts
 */
public class RetailerLocation {
	/**
	 * Variable declared for user Id
	 */
	private int userId;
	/**
	 * Variable declared for retailer Id
	 */
	private Integer retailId;
	/**
	 * Variable declared for retailer Id
	 */
	private String retailerIds;
	/**
	 * Variable declared for retailer Name
	 */
	private String retailName;

	/**
	 * Variable declared for retailer Id
	 */
	private Integer retailLocationId;
	/**
	 * Variable declared for retailer Id
	 */
	private String retailerLocIds;
	/**
	 * Variable declared for retailer address
	 */
	private String address1;
	/**
	 * Variable declared for retailer address
	 */
	private String address2;
	/**
	 * Variable declared for retailer state
	 */
	private String state;
	/**
	 * Variable declared for retailer city
	 */
	private String city;
	/**
	 * Variable declared for sortOrder
	 */
	private String sortOrder;

	/**
	 * Variable declared for columnName
	 */
	private String columnName;
	/**
	 * Variable declared for lowerLimit
	 */
	private Integer lowerLimit;
	/**
	 * Variable declared for retailer Name
	 */
	private String retailerName;
	/**
	 * Variable declared for retailer Name
	 */
	private String storeIdentification;

	/**
	 * Variable declared for postalCode
	 */
	private String postalCode;
	/**
	 * Variable declared for searchBy
	 */
	private String searchBy;
	/**
	 * Variable declared for searchBy
	 */
	private String locSearchkey;

	/**
	 * Variable declared for searchKey
	 */
	private String searchKey;

	/**
	 * Variable declared for postalCode
	 */
	private String phone;
	/**
	 * Variable declared for searchBy
	 */
	private Double latitude;
	/**
	 * Variable declared for searchBy
	 */
	private Double longitude;

	/**
	 * Variable declared for searchKey
	 */
	private String websiteURL;
	/**
	 * Variable declared for searchKey
	 */
	private String locationJson;

	private String pageNumber;
	private String pageFlag;
	/**
	 * 
	 */
	private Integer retailLowerLimit;
	/**
	 * 
	 */
	private Integer retailLocLowerLimit;

	private CommonsMultipartFile cateImgPath;

	private String cateImgName;
	
	private String imgLocationPath;
	
	private CommonsMultipartFile[] imageFile;
	
	private String gridImgLocationPath;
	
	private int rowIndex;
	
	private boolean uploadImage;
	
	private String filterCategory;
	
	private String filters;
	
	private String filterValues;
	
	
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit() {
		return lowerLimit;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @param retailerName
	 *            the retailerName to set
	 */
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	/**
	 * @return the retailerName
	 */
	public String getRetailerName() {
		return retailerName;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param retailLocationId
	 *            the retailLocationId to set
	 */
	public void setRetailLocationId(Integer retailLocationId) {
		this.retailLocationId = retailLocationId;
	}

	/**
	 * @return the retailLocationId
	 */
	public Integer getRetailLocationId() {
		return retailLocationId;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param storeIdentification
	 *            the storeIdentification to set
	 */
	public void setStoreIdentification(String storeIdentification) {
		this.storeIdentification = storeIdentification;
	}

	/**
	 * @return the storeIdentification
	 */
	public String getStoreIdentification() {
		return storeIdentification;
	}

	/**
	 * @param retailId
	 *            the retailId to set
	 */
	public void setRetailId(Integer retailId) {
		this.retailId = retailId;
	}

	/**
	 * @return the retailId
	 */
	public Integer getRetailId() {
		return retailId;
	}

	/**
	 * @param retailerIds
	 *            the retailerIds to set
	 */
	public void setRetailerIds(String retailerIds) {
		this.retailerIds = retailerIds;
	}

	/**
	 * @return the retailerIds
	 */
	public String getRetailerIds() {
		return retailerIds;
	}

	/**
	 * @param retailName
	 *            the retailName to set
	 */
	public void setRetailName(String retailName) {
		this.retailName = retailName;
	}

	/**
	 * @return the retailName
	 */
	public String getRetailName() {
		return retailName;
	}

	/**
	 * @param retailerLocIds
	 *            the retailerLocIds to set
	 */
	public void setRetailerLocIds(String retailerLocIds) {
		this.retailerLocIds = retailerLocIds;
	}

	/**
	 * @return the retailerLocIds
	 */
	public String getRetailerLocIds() {
		return retailerLocIds;
	}

	/**
	 * @param searchBy
	 *            the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}

	/**
	 * @param locSearchkey
	 *            the locSearchkey to set
	 */
	public void setLocSearchkey(String locSearchkey) {
		this.locSearchkey = locSearchkey;
	}

	/**
	 * @return the locSearchkey
	 */
	public String getLocSearchkey() {
		return locSearchkey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey() {
		return searchKey;
	}

	/**
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the websiteURL
	 */
	public String getWebsiteURL() {
		return websiteURL;
	}

	/**
	 * @param websiteURL
	 *            the websiteURL to set
	 */
	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	/**
	 * @param locationJson
	 *            the locationJson to set
	 */
	public void setLocationJson(String locationJson) {
		this.locationJson = locationJson;
	}

	/**
	 * @return the locationJson
	 */
	public String getLocationJson() {
		return locationJson;
	}

	/**
	 * @return the pageNumber
	 */
	public String getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber
	 *            the pageNumber to set
	 */
	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the pageFlag
	 */
	public String getPageFlag() {
		return pageFlag;
	}

	/**
	 * @param pageFlag
	 *            the pageFlag to set
	 */
	public void setPageFlag(String pageFlag) {
		this.pageFlag = pageFlag;
	}

	/**
	 * @return the retailLowerLimit
	 */
	public Integer getRetailLowerLimit() {
		return retailLowerLimit;
	}

	/**
	 * @param retailLowerLimit
	 *            the retailLowerLimit to set
	 */
	public void setRetailLowerLimit(Integer retailLowerLimit) {
		this.retailLowerLimit = retailLowerLimit;
	}

	/**
	 * @return the retailLocLowerLimit
	 */
	public Integer getRetailLocLowerLimit() {
		return retailLocLowerLimit;
	}

	/**
	 * @param retailLocLowerLimit
	 *            the retailLocLowerLimit to set
	 */
	public void setRetailLocLowerLimit(Integer retailLocLowerLimit) {
		this.retailLocLowerLimit = retailLocLowerLimit;
	}

	public CommonsMultipartFile getCateImgPath() {
		return cateImgPath;
	}

	public void setCateImgPath(CommonsMultipartFile cateImgPath) {
		this.cateImgPath = cateImgPath;
	}

	public String getCateImgName() {
		return cateImgName;
	}

	public void setCateImgName(String cateImgName) {
		this.cateImgName = cateImgName;
	}

	public String getImgLocationPath() {
		return imgLocationPath;
	}

	public void setImgLocationPath(String imgLocationPath) {
		this.imgLocationPath = imgLocationPath;
	}

	public CommonsMultipartFile[] getImageFile() {
		return imageFile;
	}

	public void setImageFile(CommonsMultipartFile[] imageFile) {
		this.imageFile = imageFile;
	}

	public String getGridImgLocationPath() {
		return gridImgLocationPath;
	}

	public void setGridImgLocationPath(String gridImgLocationPath) {
		this.gridImgLocationPath = gridImgLocationPath;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public boolean isUploadImage() {
		return uploadImage;
	}

	public void setUploadImage(boolean uploadImage) {
		this.uploadImage = uploadImage;
	}

	public String getFilterCategory() {
		return filterCategory;
	}

	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public String getFilterValues() {
		return filterValues;
	}

	public void setFilterValues(String filterValues) {
		this.filterValues = filterValues;
	}
}

/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 *
 */
public class RetailerExportData {
	
	private String hubCitiName;
	
	private String retailName;
	
	private Integer retailID;
	
	private String retailerLogo;
	
	private Integer retailLocationID;
	
	private String address;
	
	private String city;
	
	private String state;
	
	private Integer postalCode;
	
	private String retailLocationLogo;
	
	private String website;
	
	private String contactFirstName;
	
	private String contactLastName;
	
	private String contactPhone;
	
	private String retailerAssociatedDate;
	
	private String categoryName;
	
	private String subCategoryName;
	
	private Integer filterID;
	
	private String filterName;

	/**
	 * @return the hubCitiName
	 */
	public String getHubCitiName() {
		return hubCitiName;
	}

	/**
	 * @param hubCitiName the hubCitiName to set
	 */
	public void setHubCitiName(String hubCitiName) {
		this.hubCitiName = hubCitiName;
	}

	/**
	 * @return the retailName
	 */
	public String getRetailName() {
		return retailName;
	}

	/**
	 * @param retailName the retailName to set
	 */
	public void setRetailName(String retailName) {
		this.retailName = retailName;
	}

	/**
	 * @return the retailID
	 */
	public Integer getRetailID() {
		return retailID;
	}

	/**
	 * @param retailID the retailID to set
	 */
	public void setRetailID(Integer retailID) {
		this.retailID = retailID;
	}
	
	/**
	 * @return the retailerLogo
	 */
	public String getRetailerLogo() {
		return retailerLogo;
	}

	/**
	 * @param retailerLogo the retailerLogo to set
	 */
	public void setRetailerLogo(String retailerLogo) {
		this.retailerLogo = retailerLogo;
	}

	/**
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID() {
		return retailLocationID;
	}

	/**
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID) {
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the address1
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the postalCode
	 */
	public Integer getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(Integer postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the retailLocationLogo
	 */
	public String getRetailLocationLogo() {
		return retailLocationLogo;
	}

	/**
	 * @param retailLocationLogo the retailLocationLogo to set
	 */
	public void setRetailLocationLogo(String retailLocationLogo) {
		this.retailLocationLogo = retailLocationLogo;
	}
	
	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * @return the contactFirstName
	 */
	public String getContactFirstName() {
		return contactFirstName;
	}

	/**
	 * @param contactFirstName the contactFirstName to set
	 */
	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}
	

	/**
	 * @return the contactLastName
	 */
	public String getContactLastName() {
		return contactLastName;
	}

	/**
	 * @param contactLastName the contactLastName to set
	 */
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	/**
	 * @return the contactPhone
	 */
	public String getContactPhone() {
		return contactPhone;
	}

	/**
	 * @param contactPhone the contactPhone to set
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	/**
	 * @return the retailerAssociatedDate
	 */
	public String getRetailerAssociatedDate() {
		return retailerAssociatedDate;
	}

	/**
	 * @param retailerAssociatedDate the retailerAssociatedDate to set
	 */
	public void setRetailerAssociatedDate(String retailerAssociatedDate) {
		this.retailerAssociatedDate = retailerAssociatedDate;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the subCategoryName
	 */
	public String getSubCategoryName() {
		return subCategoryName;
	}

	/**
	 * @param subCategoryName the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	/**
	 * @return the filterID
	 */
	public Integer getFilterID() {
		return filterID;
	}

	/**
	 * @param filterID the filterID to set
	 */
	public void setFilterID(Integer filterID) {
		this.filterID = filterID;
	}

	/**
	 * @return the filterName
	 */
	public String getFilterName() {
		return filterName;
	}

	/**
	 * @param filterName the filterName to set
	 */
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	
	
}

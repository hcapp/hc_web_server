/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 */
public class HubCiti
{
	/**
	 * variable for user ID
	 */
	private Integer userID;
	/**
	 * variable for password
	 */
	private String password;
	/**
	 * variable for password
	 */
	private String encrptedPassword;
	/**
	 * variable for hubCiti ID
	 */
	private Integer hubCitiID;
	/**
	 * variable for hubCiti Name
	 */
	private String hubCitiName;
	/**
	 * variable for hubCiti Name
	 */
	private String citiExperience;
	/**
	 * variable for state
	 */
	private String state;
	/**
	 * variable for postalCodes
	 */
	private String postalCodes;
	/**
	 * variable for state Name
	 */
	private String stateName;
	/**
	 * variable for PostalCode or city Flag
	 */
	private Integer PCFlag;
	/**
	 * variable for user Name
	 */
	private String userName;
	/**
	 * variable for email ID
	 */
	private String emailID;
	/**
	 * variable for city
	 */
	private String city;
	/**
	 * variable for zip Code
	 */
	private String zipCode;
	/**
	 * variable for hubCiti Name
	 */
	private String searchKey;
	/**
	 * variable for scanSeeUrl
	 */
	private String hubCitiUrl;
	/**
	 * variable for locations
	 */
	private String locations;
	/**
	 * variable for active
	 */
	private Boolean active;
	/**
	 * variable for show Deactivated
	 */
	private Boolean showDeactivated;
	/**
	 * 
	 */
	private Integer lowerLimit;
	
	private String appRole;
	
	private String regionName;
	
	private String regHubcitiIds;
	
	private String selRegHubCitiIds;
	
	/**
	 * Variable isAssocaited
	 */
	private Boolean isAssocaited;
	
	private Boolean isNationwide;
	
	private String chkdNatId;
	
	private String unChkdNatId;
	
	private Integer defaultZipcode;
	
	private Boolean isDataExport;
	
	private int moduleTypeId;
	
	private String moduleTypeName;
	
	private String apiPartners;
	
	private String hidAPIPartners;
	
	/**
	 * variable for email ID
	 */
	private String emails;
	/**
	 * variable for sales Contact
	 */
	private String salesPersonEmail;
	
	public String getChkdNatId() {
		return chkdNatId;
	}

	public void setChkdNatId(String chkdNatId) {
		this.chkdNatId = chkdNatId;
	}

	public String getUnChkdNatId() {
		return unChkdNatId;
	}

	public void setUnChkdNatId(String unChkdNatId) {
		this.unChkdNatId = unChkdNatId;
	}

	
	
	public Boolean getIsNationwide() {
		return isNationwide;
	}

	public void setIsNationwide(Boolean isNationwide) {
		this.isNationwide = isNationwide;
	}

	public String getReghubcitiList() {
		return reghubcitiList;
	}

	public void setReghubcitiList(String reghubcitiList) {
		this.reghubcitiList = reghubcitiList;
	}

	private String reghubcitiList;

	public String getSelRegHubCitiIds() {
		return selRegHubCitiIds;
	}

	public void setSelRegHubCitiIds(String selRegHubCitiIds) {
		this.selRegHubCitiIds = selRegHubCitiIds;
	}

	public String getRegHubcitiIds() {
		return regHubcitiIds;
	}

	public void setRegHubcitiIds(String regHubcitiIds) {
		this.regHubcitiIds = regHubcitiIds;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return the hubCitiID
	 */
	public Integer getHubCitiID()
	{
		return hubCitiID;
	}

	/**
	 * @param hubCitiID
	 *            the hubCitiID to set
	 */
	public void setHubCitiID(Integer hubCitiID)
	{
		this.hubCitiID = hubCitiID;
	}

	/**
	 * @return the hubCitiName
	 */
	public String getHubCitiName()
	{
		return hubCitiName;
	}

	/**
	 * @param hubCitiName
	 *            the hubCitiName to set
	 */
	public void setHubCitiName(String hubCitiName)
	{
		this.hubCitiName = hubCitiName;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName()
	{
		return stateName;
	}

	/**
	 * @param stateName
	 *            the stateName to set
	 */
	public void setStateName(String stateName)
	{
		this.stateName = stateName;
	}

	/**
	 * @return the pCFlag
	 */
	public Integer getPCFlag()
	{
		return PCFlag;
	}

	/**
	 * @param pCFlag
	 *            the pCFlag to set
	 */
	public void setPCFlag(Integer pCFlag)
	{
		PCFlag = pCFlag;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the emailID
	 */
	public String getEmailID()
	{
		return emailID;
	}

	/**
	 * @param emailID
	 *            the emailID to set
	 */
	public void setEmailID(String emailID)
	{
		this.emailID = emailID;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode()
	{
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param postalCodes
	 *            the postalCodes to set
	 */
	public void setPostalCodes(String postalCodes)
	{
		this.postalCodes = postalCodes;
	}

	/**
	 * @return the postalCodes
	 */
	public String getPostalCodes()
	{
		return postalCodes;
	}

	/**
	 * @param citiExperience
	 *            the citiExperience to set
	 */
	public void setCitiExperience(String citiExperience)
	{
		this.citiExperience = citiExperience;
	}

	/**
	 * @return the citiExperience
	 */
	public String getCitiExperience()
	{
		return citiExperience;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(Integer userID)
	{
		this.userID = userID;
	}

	/**
	 * @return the userID
	 */
	public Integer getUserID()
	{
		return userID;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param scanSeeUrl
	 *            the scanSeeUrl to set
	 */
	public void setHubCitiUrl(String hubCitiUrl)
	{
		this.hubCitiUrl = hubCitiUrl;
	}

	/**
	 * @return the scanSeeUrl
	 */
	public String getHubCitiUrl()
	{
		return hubCitiUrl;
	}

	/**
	 * @param locations
	 *            the locations to set
	 */
	public void setLocations(String locations)
	{
		this.locations = locations;
	}

	/**
	 * @return the locations
	 */
	public String getLocations()
	{
		return locations;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active)
	{
		this.active = active;
	}

	/**
	 * @return the active
	 */
	public Boolean getActive()
	{
		return active;
	}

	/**
	 * @param showDeactivated
	 *            the showDeactivated to set
	 */
	public void setShowDeactivated(Boolean showDeactivated)
	{
		this.showDeactivated = showDeactivated;
	}

	/**
	 * @return the showDeactivated
	 */
	public Boolean getShowDeactivated()
	{
		return showDeactivated;
	}

	/**
	 * @param encrptedPassword
	 *            the encrptedPassword to set
	 */
	public void setEncrptedPassword(String encrptedPassword)
	{
		this.encrptedPassword = encrptedPassword;
	}

	/**
	 * @return the encrptedPassword
	 */
	public String getEncrptedPassword()
	{
		return encrptedPassword;
	}

	/**
	 * @param lowerLimit the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit()
	{
		return lowerLimit;
	}

	public String getAppRole() {
		return appRole;
	}

	public void setAppRole(String appRole) {
		this.appRole = appRole;
	}

	/**
	 * @return the isAssocaited
	 */
	public Boolean getIsAssocaited() {
		return isAssocaited;
	}

	/**
	 * @param isAssocaited the isAssocaited to set
	 */
	public void setIsAssocaited(Boolean isAssocaited) {
		this.isAssocaited = isAssocaited;
	}

	public Integer getDefaultZipcode() {
		return defaultZipcode;
	}

	public void setDefaultZipcode(Integer defaultZipcode) {
		this.defaultZipcode = defaultZipcode;
	}

	/**
	 * @return the isDataExport
	 */
	public Boolean getIsDataExport() {
		return isDataExport;
	}

	/**
	 * @param isDataExport the isDataExport to set
	 */
	public void setIsDataExport(Boolean isDataExport) {
		this.isDataExport = isDataExport;
	}

	/**
	 * @return the moduleTypeId
	 */
	public int getModuleTypeId() {
		return moduleTypeId;
	}

	/**
	 * @param moduleTypeId the moduleTypeId to set
	 */
	public void setModuleTypeId(int moduleTypeId) {
		this.moduleTypeId = moduleTypeId;
	}

	/**
	 * @return the moduleTypeName
	 */
	public String getModuleTypeName() {
		return moduleTypeName;
	}

	/**
	 * @param moduleTypeName the moduleTypeName to set
	 */
	public void setModuleTypeName(String moduleTypeName) {
		this.moduleTypeName = moduleTypeName;
	}

	/**
	 * @return the apiPartners
	 */
	public String getApiPartners() {
		return apiPartners;
	}

	/**
	 * @param apiPartners the apiPartners to set
	 */
	public void setApiPartners(String apiPartners) {
		this.apiPartners = apiPartners;
	}

	/**
	 * @return the hidAPIPartners
	 */
	public String getHidAPIPartners() {
		return hidAPIPartners;
	}

	/**
	 * @param hidAPIPartners the hidAPIPartners to set
	 */
	public void setHidAPIPartners(String hidAPIPartners) {
		this.hidAPIPartners = hidAPIPartners;
	}
	/**
	 * @return the SalesPersonEmail
	 */

	public String getSalesPersonEmail() {
		return salesPersonEmail;
	}
	/**
	 * @param SalesPersonEmail the SalesPersonEmail to set
	 */
	public void setSalesPersonEmail(String salesPersonEmail) {
		this.salesPersonEmail = salesPersonEmail;
	}


	/**
	 * @return the emails
	 */
	public String getEmails() {
		return emails;
	}

	/**
	 * @param emails the emails to set
	 */
	public void setEmails(String emails) {
		this.emails = emails;
	}

}

/**
 * 
 */
package common.pojo;

import java.util.ArrayList;

/**
 * @author sangeetha.ts
 */
public class HubCitiDetails
{
	/**
	 * variable for hubcitis
	 */
	private ArrayList<HubCiti> hubCitis;
	/**
	 * variable for totalSize
	 */
	private Integer totalSize;
	
	private String response;

	/**
	 * @return the hubCitis
	 */
	public ArrayList<HubCiti> getHubCitis()
	{
		return hubCitis;
	}

	/**
	 * @param hubCitis
	 *            the hubCitis to set
	 */
	public void setHubCitis(ArrayList<HubCiti> hubCitis)
	{
		this.hubCitis = hubCitis;
	}

	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize()
	{
		return totalSize;
	}

	/**
	 * @param totalSize
	 *            the totalSize to set
	 */
	public void setTotalSize(Integer totalSize)
	{
		this.totalSize = totalSize;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}

package common.pojo;

import java.util.ArrayList;

/**
 * Pojo class for RetailerLocationDetails
 * @author sangeetha.ts
 *
 */
public class RetailerLocationDetails
{

	/**
	 * Variable declared for retailerLocations
	 */
	public ArrayList<RetailerLocation> retailerLocations = null;	
	/**
	 * variable declared for totalSize
	 */
	public int totalSize;
	
	/**
	 * Variable declared for nextPage
	 */
	public int nextPage;

	private String response;
	/**
	 * @return the retailerDetailList
	 */
	public ArrayList<RetailerLocation> getRetailerLocations() {
		return retailerLocations;
	}

	/**
	 * @param retailerLocations the retailerLocations to set
	 */
	public void setRetailerLocations(ArrayList<RetailerLocation> retailerLocations) {
		this.retailerLocations = retailerLocations;
	}

	/**
	 * @return the totalSize
	 */
	public int getTotalSize() {
		return totalSize;
	}
	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	/**
	 * @return the nextPage
	 */
	public int getNextPage() {
		return nextPage;
	}

	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}

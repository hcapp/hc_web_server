package common.pojo;

/**
 * @author sangeetha.ts
 */
public class HotDeal
{
	/**
	 * Variable for hot deal ID.
	 */
	private Integer productHotDealID;
	/**
	 * Variable for hot deal IDs.
	 */
	private String hotDealIDs;
	/**
	 * variable for hot deal name.
	 */
	private String hotDealName;

	/**
	 * Variable for retailer ID.
	 */
	private Integer retailID;
	/**
	 * Variable for retailer Name.
	 */
	private String retailName;
	/**
	 * Variable for retailer Location Address.
	 */
	private String retailLocationAddress;
	/**
	 * Variable for retailer Location Id.
	 */
	private Integer retailLocationId;
	/**
	 * Variable for APIPartner ID.
	 */
	private Integer APIPartnerID;
	/**
	 * variable for API Partner Name.
	 */
	private String APIPartnerName;
	/**
	 * variable for API Partner Name.
	 */
	private String APIPartnerImagePath;
	/**
	 * variable for Hot Deal Short Description.
	 */
	private String hotDealShortDescription;
	/**
	 * variable for Hot Deal Long Description.
	 */
	private String hotDeaLonglDescription;
	/**
	 * variable for Hot Deal Terms and Conditions.
	 */
	private String hotDealTermsConditions;
	/**
	 * variable for Hot Deal sale Price.
	 */
	private Double salePrice;
	/**
	 * variable for Hot Deal Price.
	 */
	private Double price;
	/**
	 * variable for Hot Deal Discount Amount.
	 */
	private Double hotDealDiscountAmount;
	/**
	 * variable for Hot Deal Discount Percentage.
	 */
	private Double hotDealDiscountPct;
	/**
	 * variable for Hot Deal Image Path.
	 */
	private String hotDealImagePath;
	/**
	 * variable for Hot Deal Start Date.
	 */
	private String hotDealStartDate;
	/**
	 * variable for Hot Deal End Date.
	 */
	private String hotDealEndDate;
	/**
	 * variable for Hot Deal Expiration Date.
	 */
	private String hotDealExpirationDate;
	/**
	 * variable for Hot Deal URL.
	 */
	private String hotDealURL;
	/**
	 * variable for Hot Deal category ID.
	 */
	private Integer categoryID;
	/**
	 * variable for Hot Deal category.
	 */
	private String category;
	/**
	 * variable for number of Hot Deal To issues.
	 */
	private Integer noOfHotDealsToIssue;
	/**
	 * variable for Hot Deal code.
	 */
	private String hotDealCode;
	/**
	 * variable for search Key.
	 */
	private String searchKey;
	/**
	 * variable for lower Limit.
	 */
	private Integer lowerLimit;
	/**
	 * variable for show Expired.
	 */
	private String showExpired;
	/**
	 * Variable used for sortOrder
	 */
	private String sortOrder;
	/**
	 * Variable used for columnName
	 */
	private String columnName;
	/**
	 * Variable used for city
	 */
	private String city;
	/**
	 * Variable used for state
	 */
	private String state;

	/**
	 * Variable dealStartTime declared as String.
	 */
	private String dealStartTime;
	/**
	 * Variable dealEndTime declared as String.
	 */
	private String dealEndTime;

	private String dealStartHrs;

	private String dealStartMins;

	private String dealEndhrs;

	private String dealEndMins;

	/**
	 * @return the productHotDealID
	 */
	public Integer getProductHotDealID()
	{
		return productHotDealID;
	}

	/**
	 * @param productHotDealID
	 *            the productHotDealID to set
	 */
	public void setProductHotDealID(Integer productHotDealID)
	{
		this.productHotDealID = productHotDealID;
	}

	/**
	 * @return the hotDealName
	 */
	public String getHotDealName()
	{
		return hotDealName;
	}

	/**
	 * @param hotDealName
	 *            the hotDealName to set
	 */
	public void setHotDealName(String hotDealName)
	{
		this.hotDealName = hotDealName;
	}

	/**
	 * @return the retailID
	 */
	public Integer getRetailID()
	{
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Integer retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * @return the aPIPartnerID
	 */
	public Integer getAPIPartnerID()
	{
		return APIPartnerID;
	}

	/**
	 * @param aPIPartnerID
	 *            the aPIPartnerID to set
	 */
	public void setAPIPartnerID(Integer aPIPartnerID)
	{
		APIPartnerID = aPIPartnerID;
	}

	/**
	 * @return the aPIPartnerName
	 */
	public String getAPIPartnerName()
	{
		return APIPartnerName;
	}

	/**
	 * @param aPIPartnerName
	 *            the aPIPartnerName to set
	 */
	public void setAPIPartnerName(String aPIPartnerName)
	{
		APIPartnerName = aPIPartnerName;
	}

	/**
	 * @return the hotDealShortDescription
	 */
	public String getHotDealShortDescription()
	{
		return hotDealShortDescription;
	}

	/**
	 * @param hotDealShortDescription
	 *            the hotDealShortDescription to set
	 */
	public void setHotDealShortDescription(String hotDealShortDescription)
	{
		this.hotDealShortDescription = hotDealShortDescription;
	}

	/**
	 * @return the hotDeaLonglDescription
	 */
	public String getHotDeaLonglDescription()
	{
		return hotDeaLonglDescription;
	}

	/**
	 * @param hotDeaLonglDescription
	 *            the hotDeaLonglDescription to set
	 */
	public void setHotDeaLonglDescription(String hotDeaLonglDescription)
	{
		this.hotDeaLonglDescription = hotDeaLonglDescription;
	}

	/**
	 * @return the hotDealTermsConditions
	 */
	public String getHotDealTermsConditions()
	{
		return hotDealTermsConditions;
	}

	/**
	 * @param hotDealTermsConditions
	 *            the hotDealTermsConditions to set
	 */
	public void setHotDealTermsConditions(String hotDealTermsConditions)
	{
		this.hotDealTermsConditions = hotDealTermsConditions;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice()
	{
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(Double salePrice)
	{
		this.salePrice = salePrice;
	}

	/**
	 * @return the price
	 */
	public Double getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Double price)
	{
		this.price = price;
	}

	/**
	 * @return the hotDealDiscountAmount
	 */
	public Double getHotDealDiscountAmount()
	{
		return hotDealDiscountAmount;
	}

	/**
	 * @param hotDealDiscountAmount
	 *            the hotDealDiscountAmount to set
	 */
	public void setHotDealDiscountAmount(Double hotDealDiscountAmount)
	{
		this.hotDealDiscountAmount = hotDealDiscountAmount;
	}

	/**
	 * @return the hotDealDiscountPct
	 */
	public Double getHotDealDiscountPct()
	{
		return hotDealDiscountPct;
	}

	/**
	 * @param hotDealDiscountPct
	 *            the hotDealDiscountPct to set
	 */
	public void setHotDealDiscountPct(Double hotDealDiscountPct)
	{
		this.hotDealDiscountPct = hotDealDiscountPct;
	}

	/**
	 * @return the hotDealImagePath
	 */
	public String getHotDealImagePath()
	{
		return hotDealImagePath;
	}

	/**
	 * @param hotDealImagePath
	 *            the hotDealImagePath to set
	 */
	public void setHotDealImagePath(String hotDealImagePath)
	{
		this.hotDealImagePath = hotDealImagePath;
	}

	/**
	 * @return the hotDealStartDate
	 */
	public String getHotDealStartDate()
	{
		return hotDealStartDate;
	}

	/**
	 * @param hotDealStartDate
	 *            the hotDealStartDate to set
	 */
	public void setHotDealStartDate(String hotDealStartDate)
	{
		this.hotDealStartDate = hotDealStartDate;
	}

	/**
	 * @return the hotDealEndDate
	 */
	public String getHotDealEndDate()
	{
		return hotDealEndDate;
	}

	/**
	 * @param hotDealEndDate
	 *            the hotDealEndDate to set
	 */
	public void setHotDealEndDate(String hotDealEndDate)
	{
		this.hotDealEndDate = hotDealEndDate;
	}

	/**
	 * @return the hotDealExpirationDate
	 */
	public String getHotDealExpirationDate()
	{
		return hotDealExpirationDate;
	}

	/**
	 * @param hotDealExpirationDate
	 *            the hotDealExpirationDate to set
	 */
	public void setHotDealExpirationDate(String hotDealExpirationDate)
	{
		this.hotDealExpirationDate = hotDealExpirationDate;
	}

	/**
	 * @return the hotDealURL
	 */
	public String getHotDealURL()
	{
		return hotDealURL;
	}

	/**
	 * @param hotDealURL
	 *            the hotDealURL to set
	 */
	public void setHotDealURL(String hotDealURL)
	{
		this.hotDealURL = hotDealURL;
	}

	/**
	 * @return the categoryID
	 */
	public Integer getCategoryID()
	{
		return categoryID;
	}

	/**
	 * @param categoryID
	 *            the categoryID to set
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * @return the category
	 */
	public String getCategory()
	{
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category)
	{
		this.category = category;
	}

	/**
	 * @return the noOfHotDealsToIssue
	 */
	public Integer getNoOfHotDealsToIssue()
	{
		return noOfHotDealsToIssue;
	}

	/**
	 * @param noOfHotDealsToIssue
	 *            the noOfHotDealsToIssue to set
	 */
	public void setNoOfHotDealsToIssue(Integer noOfHotDealsToIssue)
	{
		this.noOfHotDealsToIssue = noOfHotDealsToIssue;
	}

	/**
	 * @return the hotDealCode
	 */
	public String getHotDealCode()
	{
		return hotDealCode;
	}

	/**
	 * @param hotDealCode
	 *            the hotDealCode to set
	 */
	public void setHotDealCode(String hotDealCode)
	{
		this.hotDealCode = hotDealCode;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit()
	{
		return lowerLimit;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @return the showExpired
	 */
	public String getShowExpired()
	{
		return showExpired;
	}

	/**
	 * @param showExpired
	 *            the showExpired to set
	 */
	public void setShowExpired(String showExpired)
	{
		this.showExpired = showExpired;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder()
	{
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName()
	{
		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	/**
	 * @param hotDealIDs
	 *            the hotDealIDs to set
	 */
	public void setHotDealIDs(String hotDealIDs)
	{
		this.hotDealIDs = hotDealIDs;
	}

	/**
	 * @return the hotDealIDs
	 */
	public String getHotDealIDs()
	{
		return hotDealIDs;
	}

	/**
	 * @param retailName
	 *            the retailName to set
	 */
	public void setRetailName(String retailName)
	{
		this.retailName = retailName;
	}

	/**
	 * @return the retailName
	 */
	public String getRetailName()
	{
		return retailName;
	}

	/**
	 * @param retailLocationAddress
	 *            the retailLocationAddress to set
	 */
	public void setRetailLocationAddress(String retailLocationAddress)
	{
		this.retailLocationAddress = retailLocationAddress;
	}

	/**
	 * @return the retailLocationAddress
	 */
	public String getRetailLocationAddress()
	{
		return retailLocationAddress;
	}

	/**
	 * @param retailLocationId
	 *            the retailLocationId to set
	 */
	public void setRetailLocationId(Integer retailLocationId)
	{
		this.retailLocationId = retailLocationId;
	}

	/**
	 * @return the retailLocationId
	 */
	public Integer getRetailLocationId()
	{
		return retailLocationId;
	}

	/**
	 * @param aPIPartnerImagePath
	 *            the aPIPartnerImagePath to set
	 */
	public void setAPIPartnerImagePath(String aPIPartnerImagePath)
	{
		APIPartnerImagePath = aPIPartnerImagePath;
	}

	/**
	 * @return the aPIPartnerImagePath
	 */
	public String getAPIPartnerImagePath()
	{
		return APIPartnerImagePath;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param dealStartTime
	 *            the dealStartTime to set
	 */
	public void setDealStartTime(String dealStartTime)
	{
		this.dealStartTime = dealStartTime;
	}

	/**
	 * @return the dealStartTime
	 */
	public String getDealStartTime()
	{
		return dealStartTime;
	}

	/**
	 * @param dealEndTime
	 *            the dealEndTime to set
	 */
	public void setDealEndTime(String dealEndTime)
	{
		this.dealEndTime = dealEndTime;
	}

	/**
	 * @return the dealEndTime
	 */
	public String getDealEndTime()
	{
		return dealEndTime;
	}

	/**
	 * @return the dealStartHrs
	 */
	public String getDealStartHrs()
	{
		return dealStartHrs;
	}

	/**
	 * @param dealStartHrs
	 *            the dealStartHrs to set
	 */
	public void setDealStartHrs(String dealStartHrs)
	{
		this.dealStartHrs = dealStartHrs;
	}

	/**
	 * @return the dealStartMins
	 */
	public String getDealStartMins()
	{
		return dealStartMins;
	}

	/**
	 * @param dealStartMins
	 *            the dealStartMins to set
	 */
	public void setDealStartMins(String dealStartMins)
	{
		this.dealStartMins = dealStartMins;
	}

	/**
	 * @return the dealEndhrs
	 */
	public String getDealEndhrs()
	{
		return dealEndhrs;
	}

	/**
	 * @param dealEndhrs
	 *            the dealEndhrs to set
	 */
	public void setDealEndhrs(String dealEndhrs)
	{
		this.dealEndhrs = dealEndhrs;
	}

	/**
	 * @return the dealEndMins
	 */
	public String getDealEndMins()
	{
		return dealEndMins;
	}

	/**
	 * @param dealEndMins
	 *            the dealEndMins to set
	 */
	public void setDealEndMins(String dealEndMins)
	{
		this.dealEndMins = dealEndMins;
	}

}

/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 */
public class PostalCode
{
	/**
	 * Variable PostalCode
	 */
	private String postalCode;
	/**
	 * Variable location
	 */
	private String location;
	/**
	 * Variable isAssocaited
	 */
	private Boolean isAssocaited;

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location)
	{
		this.location = location;
	}

	/**
	 * @return the location
	 */
	public String getLocation()
	{
		return location;
	}

	/**
	 * @return the isAssocaited
	 */
	public Boolean getIsAssocaited() {
		return isAssocaited;
	}

	/**
	 * @param isAssocaited the isAssocaited to set
	 */
	public void setIsAssocaited(Boolean isAssocaited) {
		this.isAssocaited = isAssocaited;
	}
}

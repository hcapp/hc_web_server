package common.pojo;

public class Product
{
	private String productName;
	private String productUpc;
	private String scanCode;
	private String productShortDescription;
	private String productId;
	private String category;
	private String searchKey;
	
	
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @param productShortDescription the productShortDescription to set
	 */
	public void setProductShortDescription(String productShortDescription)
	{
		this.productShortDescription = productShortDescription;
	}
	/**
	 * @return the productShortDescription
	 */
	public String getProductShortDescription()
	{
		return productShortDescription;
	}
	/**
	 * @param scanCode the scanCode to set
	 */
	public void setScanCode(String scanCode)
	{
		this.scanCode = scanCode;
	}
	/**
	 * @return the scanCode
	 */
	public String getScanCode()
	{
		return scanCode;
	}
	/**
	 * @param searchKey the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}
	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}
	/**
	 * @param productUpc the productUpc to set
	 */
	public void setProductUpc(String productUpc)
	{
		this.productUpc = productUpc;
	}
	/**
	 * @return the productUpc
	 */
	public String getProductUpc()
	{
		return productUpc;
	}
}

/**
* @author Kumar D
* @version 0.1
*
* Class to store the details of a FilterValues.
*/
package common.pojo;

public class FilterValues
{
	/**
	 * variable for user Id
	 */
	private Integer userId;
	/**
	 * 
	 */
	private Integer fValueId;
	/**
	 * 
	 */
	private String fValueName;
	/**
	 * 
	 */
	private String screenName;
	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}
	/**
	 * @param screenName the screenName to set
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	/**
	 * @return the fValueId
	 */
	public Integer getfValueId() {
		return fValueId;
	}
	/**
	 * @param fValueId the fValueId to set
	 */
	public void setfValueId(Integer fValueId) {
		this.fValueId = fValueId;
	}
	/**
	 * @return the fValueName
	 */
	public String getfValueName() {
		return fValueName;
	}
	/**
	 * @param fValueName the fValueName to set
	 */
	public void setfValueName(String fValueName) {
		this.fValueName = fValueName;
	}
	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}

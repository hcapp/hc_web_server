package common.pojo;

import java.util.ArrayList;

/**
 * Pojo class for RetailerDetails
 * 
 * @author sangeetha.ts
 */
public class RetailerDetails
{
	/**
	 * Variable declared for retailerDetailList
	 */
	public ArrayList<Retailer> retailerDetailList = null;
	/**
	 * Variable declared for retailerDetailList
	 */
	public ArrayList<Version> versionList = null;
	/**
	 * variable declared for totalSize
	 */
	public int totalSize;

	/**
	 * Variable declared for nextPage
	 */
	public int nextPage;

	/**
	 * @return the retailerDetailList
	 */
	public ArrayList<Retailer> getRetailerDetailList()
	{
		return retailerDetailList;
	}

	/**
	 * @param retailerDetailList
	 *            the retailerDetailList to set
	 */
	public void setRetailerDetailList(ArrayList<Retailer> retailerDetailList)
	{
		this.retailerDetailList = retailerDetailList;
	}

	/**
	 * @return the totalSize
	 */
	public int getTotalSize()
	{
		return totalSize;
	}

	/**
	 * @param totalSize
	 *            the totalSize to set
	 */
	public void setTotalSize(int totalSize)
	{
		this.totalSize = totalSize;
	}

	/**
	 * @return the nextPage
	 */
	public int getNextPage()
	{
		return nextPage;
	}

	/**
	 * @param nextPage
	 *            the nextPage to set
	 */
	public void setNextPage(int nextPage)
	{
		this.nextPage = nextPage;
	}

	/**
	 * @return the versionList
	 */
	public ArrayList<Version> getVersionList()
	{
		return versionList;
	}

	/**
	 * @param versionList
	 *            the versionList to set
	 */
	public void setVersionList(ArrayList<Version> versionList)
	{
		this.versionList = versionList;
	}
}

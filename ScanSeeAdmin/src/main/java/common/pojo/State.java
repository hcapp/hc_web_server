package common.pojo;

import java.util.ArrayList;

public class State
{
	/**
	 * Variable declared for stateName
	 */
	private String stateName;

	/**
	 * Variable declared for stateabbr
	 */
	private String stateabbr;

	/**
	 * 
	 */
	private ArrayList<City> cities;

	/**
	 * @return the stateName
	 */
	public String getStateName()
	{
		return stateName;
	}

	/**
	 * @param stateName
	 *            the stateName to set
	 */
	public void setStateName(String stateName)
	{
		this.stateName = stateName;
	}

	/**
	 * @return the stateabbr
	 */
	public String getStateabbr()
	{
		return stateabbr;
	}

	/**
	 * @param stateabbr
	 *            the stateabbr to set
	 */
	public void setStateabbr(String stateabbr)
	{
		this.stateabbr = stateabbr;
	}

	/**
	 * @param cities the cities to set
	 */
	public void setCities(ArrayList<City> cities)
	{
		this.cities = cities;
	}

	/**
	 * @return the cities
	 */
	public ArrayList<City> getCities()
	{
		return cities;
	}

}

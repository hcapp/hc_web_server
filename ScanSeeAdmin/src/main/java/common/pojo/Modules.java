/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 *
 */
public class Modules {
	
	private int moduleId;
	
	private String moduleName;
	
	private int moduleTypeId;
	
	private String moduleTypeName;
	
	private int hubCitiId;
	
	private String moduleIds;
	
	private String moduleNames;
	
	private String hubCitiNames;
	
	private Boolean isMandatory;
	
	private String fromDate;
	
	private String toDate;
	
	/**
	 * @return the moduleId
	 */
	public int getModuleId() {
		return moduleId;
	}
	/**
	 * @param moduleId the moduleId to set
	 */
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	/**
	 * @return the moduleName
	 */
	public String getModuleName() {
		return moduleName;
	}
	/**
	 * @param moduleName the moduleName to set
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	/**
	 * @return the moduleTypeId
	 */
	public int getModuleTypeId() {
		return moduleTypeId;
	}
	/**
	 * @param moduleTypeId the moduleTypeId to set
	 */
	public void setModuleTypeId(int moduleTypeId) {
		this.moduleTypeId = moduleTypeId;
	}
	/**
	 * @return the moduleTypeName
	 */
	public String getModuleTypeName() {
		return moduleTypeName;
	}
	/**
	 * @param moduleTypeName the moduleTypeName to set
	 */
	public void setModuleTypeName(String moduleTypeName) {
		this.moduleTypeName = moduleTypeName;
	}
	/**
	 * @return the hubCitiId
	 */
	public int getHubCitiId() {
		return hubCitiId;
	}
	/**
	 * @param hubCitiId the hubCitiId to set
	 */
	public void setHubCitiId(int hubCitiId) {
		this.hubCitiId = hubCitiId;
	}
	/**
	 * @return the moduleIds
	 */
	public String getModuleIds() {
		return moduleIds;
	}
	/**
	 * @param moduleIds the moduleIds to set
	 */
	public void setModuleIds(String moduleIds) {
		this.moduleIds = moduleIds;
	}
	/**
	 * @return the moduleNames
	 */
	public String getModuleNames() {
		return moduleNames;
	}
	/**
	 * @param moduleNames the moduleNames to set
	 */
	public void setModuleNames(String moduleNames) {
		this.moduleNames = moduleNames;
	}
	/**
	 * @return the hubCitiNames
	 */
	public String getHubCitiNames() {
		return hubCitiNames;
	}
	/**
	 * @param hubCitiNames the hubCitiNames to set
	 */
	public void setHubCitiNames(String hubCitiNames) {
		this.hubCitiNames = hubCitiNames;
	}
	/**
	 * @return the isMandatory
	 */
	public Boolean getIsMandatory() {
		return isMandatory;
	}
	/**
	 * @param isMandatory the isMandatory to set
	 */
	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
}

package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * Pojo for Version Management.
 * 
 * @author Kumar
 */
public class Version
{

	/**
	 * declared versionId as Integer.
	 */
	private Integer versionId;
	/**
	 * declared versionNumber as String.
	 */
	private String appVersionNo;
	/**
	 * declared SchemaName as String.
	 */
	private String SchemaName;
	/**
	 * declared firstQARelDate as String.
	 */
	private String firstQARelDate;
	/**
	 * declared lastQARelDate as String.
	 */
	private String lastQARelDate;
	/**
	 * declared relQANotePath as String.
	 */
	private String relQANotePath;
	/**
	 * declared firstProdtnRelDate as String.
	 */
	private String firstProdtnRelDate;
	/**
	 * declared lastProdtnRelDate as String.
	 */
	private String lastProdtnRelDate;
	/**
	 * declared relProdtnNotePath as String.
	 */
	private String relProdtnNotePath;
	/**
	 * declared iTunesUploadDate as String.
	 */
	private String iTunesUploadDate;
	/**
	 * declared buildApprovalDate as String.
	 */
	private String buildApprovalDate;
	/**
	 * Variable declared for searchKey
	 */
	private String searchKey;
	/**
	 * Variable declared for lowerLimit
	 */
	private Integer lowerLimit;
	/**
	 * Variable declared for sortOrder
	 */
	private String sortOrder;
	/**
	 * Variable used for columnName
	 */
	private String columnName;

	private String serverBuildNo;

	private String releaseType;
	/**
	 * Variable declared for lowerLimit
	 */
	private Integer recordCount;

	/**
	 * declared releaseDate as Integer.
	 */
	private String releaseDate;
	/**
	 * declared releaseDate as Integer.
	 */
	private String hiddenRelDate;
	/**
	 * declared relTypeId as Integer.
	 */
	private Integer relTypeId;
	/**
	 * declared approvedYesNo as Integer.
	 */
	private String approvedYesNo;
	/**
	 * declared approvedDate as Integer.
	 */
	private String approvedDate;
	/**
	 * declared approvedDate as Integer.
	 */
	private CommonsMultipartFile releaseNotePath;
	/**
	 * declared releaseNotePathW as CommonsMultipartFile.
	 */
	private CommonsMultipartFile releaseNotePathW;

	private String appType;
	/**
	 * Variable used for hiddenRelType
	 */
	private String hiddenRelType;

	/**
	 * Variable used for itunesFlag
	 */
	private Boolean itunesFlag;

	/**
	 * Variable used for QAFileName
	 */
	private String QAFileName;

	/**
	 * Variable used for QAFileName
	 */
	private String ProdFileName;

	/**
	 * @return the versionId
	 */
	public Integer getVersionId()
	{
		return versionId;
	}

	/**
	 * @return the schemaName
	 */
	public String getSchemaName()
	{
		return SchemaName;
	}

	/**
	 * @return the firstQARelDate
	 */
	public String getFirstQARelDate()
	{
		return firstQARelDate;
	}

	/**
	 * @return the lastQARelDate
	 */
	public String getLastQARelDate()
	{
		return lastQARelDate;
	}

	/**
	 * @return the relQANotePath
	 */
	public String getRelQANotePath()
	{
		return relQANotePath;
	}

	/**
	 * @return the firstProdtnRelDate
	 */
	public String getFirstProdtnRelDate()
	{
		return firstProdtnRelDate;
	}

	/**
	 * @return the lastProdtnRelDate
	 */
	public String getLastProdtnRelDate()
	{
		return lastProdtnRelDate;
	}

	/**
	 * @return the relProdtnNotePath
	 */
	public String getRelProdtnNotePath()
	{
		return relProdtnNotePath;
	}

	/**
	 * @return the iTunesUploadDate
	 */
	public String getiTunesUploadDate()
	{
		return iTunesUploadDate;
	}

	/**
	 * @return the buildApprovalDate
	 */
	public String getBuildApprovalDate()
	{
		return buildApprovalDate;
	}

	/**
	 * @param versionId
	 *            the versionId to set
	 */
	public void setVersionId(Integer versionId)
	{
		this.versionId = versionId;
	}

	/**
	 * @param schemaName
	 *            the schemaName to set
	 */
	public void setSchemaName(String schemaName)
	{
		SchemaName = schemaName;
	}

	/**
	 * @param firstQARelDate
	 *            the firstQARelDate to set
	 */
	public void setFirstQARelDate(String firstQARelDate)
	{
		this.firstQARelDate = firstQARelDate;
	}

	/**
	 * @param lastQARelDate
	 *            the lastQARelDate to set
	 */
	public void setLastQARelDate(String lastQARelDate)
	{
		this.lastQARelDate = lastQARelDate;
	}

	/**
	 * @param relQANotePath
	 *            the relQANotePath to set
	 */
	public void setRelQANotePath(String relQANotePath)
	{
		this.relQANotePath = relQANotePath;
	}

	/**
	 * @param firstProdtnRelDate
	 *            the firstProdtnRelDate to set
	 */
	public void setFirstProdtnRelDate(String firstProdtnRelDate)
	{
		this.firstProdtnRelDate = firstProdtnRelDate;
	}

	/**
	 * @param lastProdtnRelDate
	 *            the lastProdtnRelDate to set
	 */
	public void setLastProdtnRelDate(String lastProdtnRelDate)
	{
		this.lastProdtnRelDate = lastProdtnRelDate;
	}

	/**
	 * @param relProdtnNotePath
	 *            the relProdtnNotePath to set
	 */
	public void setRelProdtnNotePath(String relProdtnNotePath)
	{
		this.relProdtnNotePath = relProdtnNotePath;
	}

	/**
	 * @param iTunesUploadDate
	 *            the iTunesUploadDate to set
	 */
	public void setiTunesUploadDate(String iTunesUploadDate)
	{
		this.iTunesUploadDate = iTunesUploadDate;
	}

	/**
	 * @param buildApprovalDate
	 *            the buildApprovalDate to set
	 */
	public void setBuildApprovalDate(String buildApprovalDate)
	{
		this.buildApprovalDate = buildApprovalDate;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit()
	{
		return lowerLimit;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder()
	{
		return sortOrder;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName()
	{
		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	public String getAppVersionNo()
	{
		return appVersionNo;
	}

	public void setAppVersionNo(String appVersionNo)
	{
		this.appVersionNo = appVersionNo;
	}

	public String getServerBuildNo()
	{
		return serverBuildNo;
	}

	public void setServerBuildNo(String serverBuildNo)
	{
		this.serverBuildNo = serverBuildNo;
	}

	public String getReleaseType()
	{
		return releaseType;
	}

	public void setReleaseType(String releaseType)
	{
		this.releaseType = releaseType;
	}

	public String getReleaseDate()
	{
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate)
	{
		this.releaseDate = releaseDate;
	}

	/**
	 * @return the recordCount
	 */
	public Integer getRecordCount()
	{
		return recordCount;
	}

	/**
	 * @param recordCount
	 *            the recordCount to set
	 */
	public void setRecordCount(Integer recordCount)
	{
		this.recordCount = recordCount;
	}

	/**
	 * @return the relTypeId
	 */
	public Integer getRelTypeId()
	{
		return relTypeId;
	}

	/**
	 * @param relTypeId
	 *            the relTypeId to set
	 */
	public void setRelTypeId(Integer relTypeId)
	{
		this.relTypeId = relTypeId;
	}

	/**
	 * @return the approvedYesNo
	 */
	public String getApprovedYesNo()
	{
		return approvedYesNo;
	}

	/**
	 * @return the approvedDate
	 */
	public String getApprovedDate()
	{
		return approvedDate;
	}

	/**
	 * @param approvedYesNo
	 *            the approvedYesNo to set
	 */
	public void setApprovedYesNo(String approvedYesNo)
	{
		this.approvedYesNo = approvedYesNo;
	}

	/**
	 * @param approvedDate
	 *            the approvedDate to set
	 */
	public void setApprovedDate(String approvedDate)
	{
		this.approvedDate = approvedDate;
	}

	public CommonsMultipartFile getReleaseNotePath()
	{
		return releaseNotePath;
	}

	public void setReleaseNotePath(CommonsMultipartFile releaseNotePath)
	{
		this.releaseNotePath = releaseNotePath;
	}

	public String getAppType()
	{
		return appType;
	}

	public void setAppType(String appType)
	{
		this.appType = appType;
	}

	/**
	 * @return the hiddenRelType
	 */
	public String getHiddenRelType()
	{
		return hiddenRelType;
	}

	/**
	 * @param hiddenRelType
	 *            the hiddenRelType to set
	 */
	public void setHiddenRelType(String hiddenRelType)
	{
		this.hiddenRelType = hiddenRelType;
	}

	/**
	 * @return the hiddenRelDate
	 */
	public String getHiddenRelDate()
	{
		return hiddenRelDate;
	}

	/**
	 * @param hiddenRelDate
	 *            the hiddenRelDate to set
	 */
	public void setHiddenRelDate(String hiddenRelDate)
	{
		this.hiddenRelDate = hiddenRelDate;
	}

	/**
	 * @return the itunesFlag
	 */
	public Boolean getItunesFlag()
	{
		return itunesFlag;
	}

	/**
	 * @param itunesFlag
	 *            the itunesFlag to set
	 */
	public void setItunesFlag(Boolean itunesFlag)
	{
		this.itunesFlag = itunesFlag;
	}

	/**
	 * @return the releaseNotePathW
	 */
	public CommonsMultipartFile getReleaseNotePathW()
	{
		return releaseNotePathW;
	}

	/**
	 * @param releaseNotePathW
	 *            the releaseNotePathW to set
	 */
	public void setReleaseNotePathW(CommonsMultipartFile releaseNotePathW)
	{
		this.releaseNotePathW = releaseNotePathW;
	}

	/**
	 * @return the qAFileName
	 */
	public String getQAFileName()
	{
		return QAFileName;
	}

	/**
	 * @param qAFileName
	 *            the qAFileName to set
	 */
	public void setQAFileName(String qAFileName)
	{
		QAFileName = qAFileName;
	}

	/**
	 * @return the prodFileName
	 */
	public String getProdFileName()
	{
		return ProdFileName;
	}

	/**
	 * @param prodFileName
	 *            the prodFileName to set
	 */
	public void setProdFileName(String prodFileName)
	{
		ProdFileName = prodFileName;
	}
}

package common.pojo;

/**
 * This pojo class is used to store the details of coupon
 * 
 * @author pradip_k
 */

public class Coupon
{

	/**
	 * Variable used for userId
	 */
	private Integer userId;

	/**
	 * Variable used for userName
	 */
	private String userName;

	/**
	 * Variable used for couponId
	 */
	private Integer couponId;

	/**
	 * Variable used for couponName
	 */
	private String couponName;

	/**
	 * Variable used for couponValue
	 */
	private String couponValue;

	/**
	 * Variable used for noOfCoupon
	 */
	private String noOfCoupon;

	/**
	 * Variable used for couponDescritption
	 */
	private String couponDescritption;

	/**
	 * Variable used for couponStartDate
	 */
	private String couponStartDate;

	/**
	 * Variable used for couponEndDate
	 */
	private String couponExpireDate;

	/**
	 * Variable used for couponImage
	 */
	private String couponImage;

	/**
	 * Variable used for productUpc
	 */
	private String productUpc;

	/**
	 * Variable used for productIDHidden
	 */
	private String productIDHidden;

	/**
	 * Variable used for productNameHidden
	 */
	private String productNameHidden;

	/**
	 * Variable used for couponDiscountType
	 */
	private String couponDiscountType;

	/**
	 * Variable used for couponDiscountAmount
	 */
	private String couponDiscountAmount;

	/**
	 * Variable used for couponShortDescription
	 */
	private String couponShortDescription;

	/**
	 * Variable used for imageUrl
	 */
	private String imageUrl;

	/**
	 * Variable used for retailerId
	 */
	private Integer retailerId;

	/**
	 * Variable used for externalCoupon
	 */
	private String externalCoupon;

	/**
	 * Variable used for couponUrl
	 */
	private String couponUrl;

	/**
	 * Variable used for viewableOnWeb
	 */
	private String viewableOnWeb;

	/**
	 * Variable used for discountPercentage
	 */
	private Double couponDiscountPct;

	/**
	 * Variable used for hiddenViewable
	 */
	private String hiddenViewable;

	/**
	 * Variable used for hiddenExternalCoupon
	 */
	private String hiddenExternalCoupon;
	/**
	 * Variable used for hiddenExternalCoupon
	 */
	private String producthidden;

	/**
	 * Variable used for searchKey
	 */
	private String searchKey;

	/**
	 * Variable used for sortOrder
	 */
	private String sortOrder;

	/**
	 * Variable used for columnName
	 */
	private String columnName;

	/**
	 * Variable declared for expireFlag
	 */
	private Integer expireFlag;

	/**
	 * Variable declared for showExpire
	 */
	private String showExpire;

	/**
	  * 
	  */
	private String searchFlag;

	/**
	 * Variable declared for mode
	 */
	private String mode;

	/**
	 * Variable used for productUpc
	 */
	private String productNames;
	/**
	 * Variable used for productUpc
	 */
	private String productIDs;

	/**
	 * @return the productIDHidden
	 */
	public String getProductIDHidden()
	{
		return productIDHidden;
	}

	/**
	 * @param productIDHidden
	 *            the productIDHidden to set
	 */
	public void setProductIDHidden(String productIDHidden)
	{
		this.productIDHidden = productIDHidden;
	}

	/**
	 * @return the showExpire
	 */
	public String getShowExpire()
	{
		return showExpire;
	}

	/**
	 * @param showExpire
	 *            the showExpire to set
	 */
	public void setShowExpire(String showExpire)
	{
		this.showExpire = showExpire;
	}

	/**
	 * @return the expireFlag
	 */
	public Integer getExpireFlag()
	{
		return expireFlag;
	}

	/**
	 * @param expireFlag
	 *            the expireFlag to set
	 */
	public void setExpireFlag(Integer expireFlag)
	{
		this.expireFlag = expireFlag;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder()
	{
		return sortOrder;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName()
	{
		return columnName;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	public String getHiddenExternalCoupon()
	{
		return hiddenExternalCoupon;
	}

	public void setHiddenExternalCoupon(String hiddenExternalCoupon)
	{
		this.hiddenExternalCoupon = hiddenExternalCoupon;
	}

	/**
	 * @return the retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * @param retailerId
	 *            the retailerId to set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl()
	{
		return imageUrl;
	}

	/**
	 * @param imageUrl
	 *            the imageUrl to set
	 */
	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the userId
	 */
	public int getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the couponId
	 */
	public Integer getCouponId()
	{
		return couponId;
	}

	/**
	 * @param couponId
	 *            the couponId to set
	 */
	public void setCouponId(Integer couponId)
	{
		this.couponId = couponId;
	}

	/**
	 * @return the couponName
	 */
	public String getCouponName()
	{
		return couponName;
	}

	/**
	 * @param couponName
	 *            the couponName to set
	 */
	public void setCouponName(String couponName)
	{
		if (null != couponName)
		{
			couponName = couponName.trim();
			this.couponName = couponName;
		}
		else
		{
			this.couponName = couponName;
		}
	}

	/**
	 * @return the couponValue
	 */
	public String getCouponValue()
	{
		return couponValue;
	}

	/**
	 * @param couponValue
	 *            the couponValue to set
	 */
	public void setCouponValue(String couponValue)
	{
		this.couponValue = couponValue;
	}

	/**
	 * @return the noOfCoupon
	 */
	public String getNoOfCoupon()
	{
		return noOfCoupon;
	}

	/**
	 * @param noOfCoupon
	 *            the noOfCoupon to set
	 */
	public void setNoOfCoupon(String noOfCoupon)
	{
		this.noOfCoupon = noOfCoupon;
	}

	/**
	 * @return the couponDescritption
	 */
	public String getCouponDescritption()
	{
		return couponDescritption;
	}

	/**
	 * @param couponDescritption
	 *            the couponDescritption to set
	 */
	public void setCouponDescritption(String couponDescritption)
	{
		if (couponDescritption == null)
		{
			this.couponDescritption = "N/A";
		}
		else
		{
			couponDescritption = couponDescritption.trim();
			this.couponDescritption = couponDescritption;
		}
	}

	/**
	 * @return the couponStartDate
	 */
	public String getCouponStartDate()
	{
		return couponStartDate;
	}

	/**
	 * @param couponStartDate
	 *            the couponStartDate to set
	 */
	public void setCouponStartDate(String couponStartDate)
	{
		if (couponStartDate == null)
		{
			this.couponStartDate = "";
		}
		else
		{
			this.couponStartDate = couponStartDate;
		}

	}

	/**
	 * @return the couponImage
	 */
	public String getCouponImage()
	{
		return couponImage;
	}

	/**
	 * @param couponImage
	 *            the couponImage to set
	 */
	public void setCouponImage(String couponImage)
	{
		if (couponImage == null)
		{
			this.couponImage = "";
		}
		else
		{
			this.couponImage = couponImage;
		}

	}

	/**
	 * @return the productUpc
	 */
	public String getProductUpc()
	{
		return productUpc;
	}

	/**
	 * @param productUpc
	 *            the productUpc to set
	 */
	public void setProductUpc(String productUpc)
	{
		if (productUpc == null)
		{
			this.productUpc = "";
		}
		else
		{
			if (productUpc.length() > 100)
			{
				productUpc = productUpc.substring(0, 97);
				productUpc = productUpc + "...";
			}
			this.productUpc = productUpc;
		}
	}

	/**
	 * @return the couponDiscountType
	 */
	public String getCouponDiscountType()
	{
		return couponDiscountType;
	}

	/**
	 * @param couponDiscountType
	 *            the couponDiscountType to set
	 */
	public void setCouponDiscountType(String couponDiscountType)
	{
		if (couponDiscountType == null)
		{
			this.couponDiscountType = "";
		}
		else
		{
			this.couponDiscountType = couponDiscountType;
		}
		this.couponDiscountType = couponDiscountType;
	}

	/**
	 * @return the couponDiscountAmount
	 */
	public String getCouponDiscountAmount()
	{
		return couponDiscountAmount;
	}

	/**
	 * @param couponDiscountAmount
	 *            the couponDiscountAmount to set
	 */
	public void setCouponDiscountAmount(String couponDiscountAmount)
	{
		this.couponDiscountAmount = couponDiscountAmount;
	}

	/**
	 * @return the couponShortDescription
	 */
	public String getCouponShortDescription()
	{
		return couponShortDescription;
	}

	/**
	 * @param couponShortDescription
	 *            the couponShortDescription to set
	 */
	public void setCouponShortDescription(String couponShortDescription)
	{
		if (couponShortDescription == null)
		{
			this.couponShortDescription = "N/A";
		}
		else
		{
			this.couponShortDescription = couponShortDescription;
		}
	}

	/**
	 * @return the externalCoupon
	 */
	public String getExternalCoupon()
	{
		return externalCoupon;
	}

	/**
	 * @param externalCoupon
	 *            the externalCoupon to set
	 */
	public void setExternalCoupon(String externalCoupon)
	{
		this.externalCoupon = externalCoupon;
	}

	/**
	 * @return the couponUrl
	 */
	public String getCouponUrl()
	{
		return couponUrl;
	}

	/**
	 * @param couponUrl
	 *            the couponUrl to set
	 */
	public void setCouponUrl(String couponUrl)
	{
		if (null != couponUrl)
		{
			couponUrl = couponUrl.trim();
			this.couponUrl = couponUrl;
		}
		else
		{
			this.couponUrl = couponUrl;
		}
	}

	/**
	 * @return the viewableOnWeb
	 */
	public String getViewableOnWeb()
	{
		return viewableOnWeb;
	}

	/**
	 * @return the hiddenViewable
	 */
	public String getHiddenViewable()
	{
		return hiddenViewable;
	}

	/**
	 * @param viewableOnWeb
	 *            the viewableOnWeb to set
	 */
	public void setViewableOnWeb(String viewableOnWeb)
	{
		this.viewableOnWeb = viewableOnWeb;
	}

	/**
	 * @param hiddenViewable
	 *            the hiddenViewable to set
	 */
	public void setHiddenViewable(String hiddenViewable)
	{
		this.hiddenViewable = hiddenViewable;
	}

	/**
	 * @return the producthidden
	 */
	public String getProducthidden()
	{
		return producthidden;
	}

	/**
	 * @param producthidden
	 *            the producthidden to set
	 */
	public void setProducthidden(String producthidden)
	{
		this.producthidden = producthidden;
	}

	/**
	 * @return the productNameHidden
	 */
	public String getProductNameHidden()
	{
		return productNameHidden;
	}

	/**
	 * @param productNameHidden
	 *            the productNameHidden to set
	 */
	public void setProductNameHidden(String productNameHidden)
	{
		this.productNameHidden = productNameHidden;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(String mode)
	{
		this.mode = mode;
	}

	/**
	 * @return the mode
	 */
	public String getMode()
	{
		return mode;
	}

	/**
	 * @param searchFlag
	 *            the searchFlag to set
	 */
	public void setSearchFlag(String searchFlag)
	{
		this.searchFlag = searchFlag;
	}

	/**
	 * @return the searchFlag
	 */
	public String getSearchFlag()
	{
		return searchFlag;
	}

	/**
	 * @param couponExpireDate
	 *            the couponExpireDate to set
	 */
	public void setCouponExpireDate(String couponExpireDate)
	{
		this.couponExpireDate = couponExpireDate;
	}

	/**
	 * @return the couponExpireDate
	 */
	public String getCouponExpireDate()
	{
		return couponExpireDate;
	}

	/**
	 * @param couponDiscountPct
	 *            the couponDiscountPct to set
	 */
	public void setCouponDiscountPct(Double couponDiscountPct)
	{
		this.couponDiscountPct = couponDiscountPct;
	}

	/**
	 * @return the couponDiscountPct
	 */
	public Double getCouponDiscountPct()
	{
		return couponDiscountPct;
	}

	/**
	 * @param productNames the productNames to set
	 */
	public void setProductNames(String productNames)
	{
		this.productNames = productNames;
	}

	/**
	 * @return the productNames
	 */
	public String getProductNames()
	{
		return productNames;
	}

	/**
	 * @param productIDs the productIDs to set
	 */
	public void setProductIDs(String productIDs)
	{
		this.productIDs = productIDs;
	}

	/**
	 * @return the productIDs
	 */
	public String getProductIDs()
	{
		return productIDs;
	}

}

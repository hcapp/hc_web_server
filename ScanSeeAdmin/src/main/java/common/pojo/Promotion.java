package common.pojo;

public class Promotion
{
	/**
	 * declared promotionId as Integer.
	 */
	private Integer promotionId;
	/**
	 * declared promotionIds as String.
	 */
	private String promotionIds;
	/**
	 * declared promotionName as String.
	 */
	private String promotionName;
	/**
	 * declared quantity as Integer.
	 */
	private Integer quantity;
	/**
	 * declared shortDescription as String.
	 */
	private String shortDescription;
	/**
	 * declared longDescription as String.
	 */
	private String longDescription;
	/**
	 * declared retailerName as String.
	 */
	private String retailName;
	/**
	 * declared startDate as String.
	 */
	private String startDate;
	/**
	 * declared endDate as String.
	 */
	private String endDate;
	/**
	 * declared imagePath as String.
	 */
	private String image;
	/**
	 * declared termsAndCondition as String.
	 */
	private String termsAndCondition;
	/**
	 * declared rules as String.
	 */
	private String rules;
	/**
	 * declared title as String.
	 */
	private String title;
	/**
	 * declared name as String.
	 */
	private String userName;
	/**
	 * declared email as String.
	 */
	private String email;
	/**
	 * declared searchKey as String.
	 */
	private String searchKey;
	/**
	 * declared sortOrder as String.
	 */
	private String sortOrder;
	/**
	 * declared columnName as String.
	 */
	private String columnName;
	/**
	 * declared searchFlag as String.
	 */
	private String searchFlag;
	/**
	 * declared showExpire as String.
	 */
	private String showExpire;
	/**
	 * 
	 */
	private Integer QRRetailerCustomPageID;

	/**
	 * 
	 */
	private String Pagetitle;
	/**
	 * @return the promotionName
	 */
	public String getPromotionName()
	{
		return promotionName;
	}

	/**
	 * @param promotionName
	 *            the promotionName to set
	 */
	public void setPromotionName(String promotionName)
	{
		this.promotionName = promotionName;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the shortDescription
	 */
	public String getShortDescription()
	{
		return shortDescription;
	}

	/**
	 * @param shortDescription
	 *            the shortDescription to set
	 */
	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	/**
	 * @return the longDescription
	 */
	public String getLongDescription()
	{
		return longDescription;
	}

	/**
	 * @param longDescription
	 *            the longDescription to set
	 */
	public void setLongDescription(String longDescription)
	{
		this.longDescription = longDescription;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	/**
	 * @return the termsAndCondition
	 */
	public String getTermsAndCondition()
	{
		return termsAndCondition;
	}

	/**
	 * @param termsAndCondition
	 *            the termsAndCondition to set
	 */
	public void setTermsAndCondition(String termsAndCondition)
	{
		this.termsAndCondition = termsAndCondition;
	}

	/**
	 * @return the rules
	 */
	public String getRules()
	{
		return rules;
	}

	/**
	 * @param rules
	 *            the rules to set
	 */
	public void setRules(String rules)
	{
		this.rules = rules;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return the name
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @param promotionId
	 *            the promotionId to set
	 */
	public void setPromotionId(Integer promotionId)
	{
		this.promotionId = promotionId;
	}

	/**
	 * @return the promotionId
	 */
	public Integer getPromotionId()
	{
		return promotionId;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder()
	{
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName()
	{
		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	/**
	 * @return the searchFlag
	 */
	public String getSearchFlag()
	{
		return searchFlag;
	}

	/**
	 * @param searchFlag
	 *            the searchFlag to set
	 */
	public void setSearchFlag(String searchFlag)
	{
		this.searchFlag = searchFlag;
	}

	/**
	 * @return the showExpire
	 */
	public String getShowExpire()
	{
		return showExpire;
	}

	/**
	 * @param showExpire
	 *            the showExpire to set
	 */
	public void setShowExpire(String showExpire)
	{
		this.showExpire = showExpire;
	}

	/**
	 * @param promotionIds
	 *            the promotionIds to set
	 */
	public void setPromotionIds(String promotionIds)
	{
		this.promotionIds = promotionIds;
	}

	/**
	 * @return the promotionIds
	 */
	public String getPromotionIds()
	{
		return promotionIds;
	}

	/**
	 * @param qRRetailerCustomPageID the qRRetailerCustomPageID to set
	 */
	public void setQRRetailerCustomPageID(Integer qRRetailerCustomPageID)
	{
		QRRetailerCustomPageID = qRRetailerCustomPageID;
	}

	/**
	 * @return the qRRetailerCustomPageID
	 */
	public Integer getQRRetailerCustomPageID()
	{
		return QRRetailerCustomPageID;
	}

	/**
	 * @param pagetitle the pagetitle to set
	 */
	public void setPagetitle(String pagetitle)
	{
		Pagetitle = pagetitle;
	}

	/**
	 * @return the pagetitle
	 */
	public String getPagetitle()
	{
		return Pagetitle;
	}

	/**
	 * @param retailName the retailName to set
	 */
	public void setRetailName(String retailName)
	{
		this.retailName = retailName;
	}

	/**
	 * @return the retailName
	 */
	public String getRetailName()
	{
		return retailName;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image)
	{
		this.image = image;
	}

	/**
	 * @return the image
	 */
	public String getImage()
	{
		return image;
	}
}

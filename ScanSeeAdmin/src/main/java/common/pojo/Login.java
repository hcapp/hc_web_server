package common.pojo;

/**
 * @author sangeetha.ts
 * 
 */

public class Login {
	
	private String userName;
	private String password;
	private int userId;
	/**
	 * Login Status.
	 */	
	private  String loginStatus;
	
	/**
	 * Login loginSuccess.
	 */	
	protected  boolean loginSuccess;
	
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}	

	/**
	 * @return the loginStatus
	 */
	public String getLoginStatus() {
		return loginStatus;
	}

	/**
	 * @param loginStatus the loginStatus to set
	 */
	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	/**
	 * @return the loginSuccess
	 */
	public boolean isLoginSuccess() {
		return loginSuccess;
	}

	/**
	 * @param loginSuccess the loginSuccess to set
	 */
	public void setLoginSuccess(boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}

	/**
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}

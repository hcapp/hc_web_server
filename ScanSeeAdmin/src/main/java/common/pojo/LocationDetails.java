/**
 * 
 */
package common.pojo;

import java.util.ArrayList;

/**
 * @author sangeetha.ts
 */
public class LocationDetails
{
	private ArrayList<RetailerLocation> locationData;

	/**
	 * @return the locationData
	 */
	public ArrayList<RetailerLocation> getLocationData()
	{
		return locationData;
	}

	/**
	 * @param locationData
	 *            the locationData to set
	 */
	public void setLocationData(ArrayList<RetailerLocation> locationData)
	{
		this.locationData = locationData;
	}
}

/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 *
 */
public class DashboardExportData {
	
	private int retailID;
	
	private String retailName;
	
	private int retailLocationID;

	private String hubCitiName;
	
	private String categoryName;
	
	private String subCategoryName;
	
	private Integer retailLocationImpression;
	
	private Integer retailLocationClicks;
	
	private Integer appsiteImpression;
	
	private Integer appsiteClicks;
	
	private Integer anythingPageImpression;
	
	private Integer anythingPageClicks;
	
	private Integer retailerSpecialsImpression;
	
	private Integer retailerSpecialsClicks;
	
	private Integer hotdealImpression;
	
	private Integer hotdealClicks;
	
	private Integer couponImpression;
	
	private Integer couponClicks;
	
	private Integer dealImpression;
	
	private Integer dealClicks;
	
	public DashboardExportData(){
		
	}

	/**
	 * @param retailID
	 * @param retailName
	 * @param retailLocationID
	 * @param hubCitiName
	 * @param retailLocationImpression
	 * @param retailLocationClicks
	 * @param appsiteImpression
	 * @param appsiteClicks
	 * @param anythingPageImpression
	 * @param anythingPageClicks
	 * @param retailerSpecialsImpression
	 * @param retailerSpecialsClicks
	 * @param hotdealImpression
	 * @param hotdealClicks
	 * @param couponImpression
	 * @param couponClicks
	 * @param dealImpression
	 * @param dealClicks
	 */
	public DashboardExportData(int retailID, String retailName,
			int retailLocationID, String hubCitiName, String categoryName,
			String subCategoryName,	Integer retailLocationImpression, 
			Integer retailLocationClicks, Integer appsiteImpression, 
			Integer appsiteClicks, Integer anythingPageImpression, 
			Integer anythingPageClicks, Integer retailerSpecialsImpression, 
			Integer retailerSpecialsClicks, Integer hotdealImpression, 
			Integer hotdealClicks, Integer couponImpression, Integer couponClicks,
			Integer dealImpression, Integer dealClicks) {
		super();
		this.retailID = retailID;
		this.retailName = retailName;
		this.retailLocationID = retailLocationID;
		this.hubCitiName = hubCitiName;
		this.categoryName = categoryName;
		this.subCategoryName = subCategoryName;
		this.retailLocationImpression = retailLocationImpression;
		this.retailLocationClicks = retailLocationClicks;
		this.appsiteImpression = appsiteImpression;
		this.appsiteClicks = appsiteClicks;
		this.anythingPageImpression = anythingPageImpression;
		this.anythingPageClicks = anythingPageClicks;
		this.retailerSpecialsImpression = retailerSpecialsImpression;
		this.retailerSpecialsClicks = retailerSpecialsClicks;
		this.hotdealImpression = hotdealImpression;
		this.hotdealClicks = hotdealClicks;
		this.couponImpression = couponImpression;
		this.couponClicks = couponClicks;
		this.dealImpression = dealImpression;
		this.dealClicks = dealClicks;
	}

	/**
	 * @return the retailID
	 */
	public int getRetailID() {
		return retailID;
	}

	/**
	 * @param retailID the retailID to set
	 */
	public void setRetailID(int retailID) {
		this.retailID = retailID;
	}

	/**
	 * @return the retailName
	 */
	public String getRetailName() {
		return retailName;
	}

	/**
	 * @param retailName the retailName to set
	 */
	public void setRetailName(String retailName) {
		this.retailName = retailName;
	}

	/**
	 * @return the retailLocationID
	 */
	public int getRetailLocationID() {
		return retailLocationID;
	}

	/**
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(int retailLocationID) {
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the hubCitiName
	 */
	public String getHubCitiName() {
		return hubCitiName;
	}

	/**
	 * @param hubCitiName the hubCitiName to set
	 */
	public void setHubCitiName(String hubCitiName) {
		this.hubCitiName = hubCitiName;
	}
	
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the subCategoryName
	 */
	public String getSubCategoryName() {
		return subCategoryName;
	}

	/**
	 * @param subCategoryName the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	/**
	 * @return the retailLocationImpression
	 */
	public Integer getRetailLocationImpression() {
		return retailLocationImpression;
	}

	/**
	 * @param retailLocationImpression the retailLocationImpression to set
	 */
	public void setRetailLocationImpression(Integer retailLocationImpression) {
		this.retailLocationImpression = retailLocationImpression;
	}

	/**
	 * @return the retailLocationClicks
	 */
	public Integer getRetailLocationClicks() {
		return retailLocationClicks;
	}

	/**
	 * @param retailLocationClicks the retailLocationClicks to set
	 */
	public void setRetailLocationClicks(Integer retailLocationClicks) {
		this.retailLocationClicks = retailLocationClicks;
	}

	/**
	 * @return the appsiteImpression
	 */
	public Integer getAppsiteImpression() {
		return appsiteImpression;
	}

	/**
	 * @param appsiteImpression the appsiteImpression to set
	 */
	public void setAppsiteImpression(Integer appsiteImpression) {
		this.appsiteImpression = appsiteImpression;
	}

	/**
	 * @return the appsiteClicks
	 */
	public Integer getAppsiteClicks() {
		return appsiteClicks;
	}

	/**
	 * @param appsiteClicks the appsiteClicks to set
	 */
	public void setAppsiteClicks(Integer appsiteClicks) {
		this.appsiteClicks = appsiteClicks;
	}

	/**
	 * @return the anythingPageImpression
	 */
	public Integer getAnythingPageImpression() {
		return anythingPageImpression;
	}

	/**
	 * @param anythingPageImpression the anythingPageImpression to set
	 */
	public void setAnythingPageImpression(Integer anythingPageImpression) {
		this.anythingPageImpression = anythingPageImpression;
	}

	/**
	 * @return the anythingPageClicks
	 */
	public Integer getAnythingPageClicks() {
		return anythingPageClicks;
	}

	/**
	 * @param anythingPageClicks the anythingPageClicks to set
	 */
	public void setAnythingPageClicks(Integer anythingPageClicks) {
		this.anythingPageClicks = anythingPageClicks;
	}

	/**
	 * @return the retailerSpecialsImpression
	 */
	public Integer getRetailerSpecialsImpression() {
		return retailerSpecialsImpression;
	}

	/**
	 * @param retailerSpecialsImpression the retailerSpecialsImpression to set
	 */
	public void setRetailerSpecialsImpression(Integer retailerSpecialsImpression) {
		this.retailerSpecialsImpression = retailerSpecialsImpression;
	}

	/**
	 * @return the retailerSpecialsClicks
	 */
	public Integer getRetailerSpecialsClicks() {
		return retailerSpecialsClicks;
	}

	/**
	 * @param retailerSpecialsClicks the retailerSpecialsClicks to set
	 */
	public void setRetailerSpecialsClicks(Integer retailerSpecialsClicks) {
		this.retailerSpecialsClicks = retailerSpecialsClicks;
	}

	/**
	 * @return the hotdealImpression
	 */
	public Integer getHotdealImpression() {
		return hotdealImpression;
	}

	/**
	 * @param hotdealImpression the hotdealImpression to set
	 */
	public void setHotdealImpression(Integer hotdealImpression) {
		this.hotdealImpression = hotdealImpression;
	}

	/**
	 * @return the hotdealClicks
	 */
	public Integer getHotdealClicks() {
		return hotdealClicks;
	}

	/**
	 * @param hotdealClicks the hotdealClicks to set
	 */
	public void setHotdealClicks(Integer hotdealClicks) {
		this.hotdealClicks = hotdealClicks;
	}

	/**
	 * @return the couponImpression
	 */
	public Integer getCouponImpression() {
		return couponImpression;
	}

	/**
	 * @param couponImpression the couponImpression to set
	 */
	public void setCouponImpression(Integer couponImpression) {
		this.couponImpression = couponImpression;
	}

	/**
	 * @return the couponClicks
	 */
	public Integer getCouponClicks() {
		return couponClicks;
	}

	/**
	 * @param couponClicks the couponClicks to set
	 */
	public void setCouponClicks(Integer couponClicks) {
		this.couponClicks = couponClicks;
	}

	/**
	 * @return the dealImpression
	 */
	public Integer getDealImpression() {
		return dealImpression;
	}

	/**
	 * @param dealImpression the dealImpression to set
	 */
	public void setDealImpression(Integer dealImpression) {
		this.dealImpression = dealImpression;
	}

	/**
	 * @return the dealClicks
	 */
	public Integer getDealClicks() {
		return dealClicks;
	}

	/**
	 * @param dealClicks the dealClicks to set
	 */
	public void setDealClicks(Integer dealClicks) {
		this.dealClicks = dealClicks;
	}
	
	
}

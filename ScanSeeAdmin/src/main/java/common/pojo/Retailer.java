package common.pojo;

/**
 * Pojo for Retailer
 * 
 * @author sangeetha.ts
 */
public class Retailer {
	/**
	 * Variable declared for user Id
	 */
	private int userId;
	/**
	 * Variable declared for retailer Id
	 */
	private Integer retailId;
	/**
	 * Variable declared for retailer Id
	 */
	private String retailerIds;
	/**
	 * Variable declared for retailer Name
	 */
	private String retailName;
	/**
	 * Variable declared for number of locations
	 */
	private Integer NumberOfLocations;
	/**
	 * Variable declared for retailer address
	 */
	private String address1;
	/**
	 * Variable declared for retailer state
	 */
	private String state;
	/**
	 * Variable declared for retailer city
	 */
	private String city;
	/**
	 * Variable declared for searchKey
	 */
	private String searchKey;
	/**
	 * Variable declared for sortOrder
	 */
	private String sortOrder;

	/**
	 * Variable declared for columnName
	 */
	private String columnName;
	/**
	 * Variable declared for lowerLimit
	 */
	private Integer lowerLimit;
	/**
	 * Variable declared for postalCode
	 */
	private String postalCode;

	private String storeID;

	private String latitude;
	private String longitude;

	private String retUrl;
	private String phoneNo;
	private String retailInfoJson;

	/**
	 * Variable declared for bCategoryIds.
	 */
	private String bCategoryIds;
	/**
	 * Variable declared for retailer Id
	 */
	private Integer retailLocationId;
	/**
	 * Variable declared for retailer Id
	 */
	private String categoryType;
	/**
	 * 
	 */
	private Integer retailLowerLimit;

	private Integer isLoginExist;

	private String email;

	private String userName;

	private String password;

	private Integer currentPageNum;

	private String status;

	private Boolean isPaid;

	private Boolean isBand;

	private String buscatIds;

	private String subcatIds;

	public String getBuscatIds() {
		return buscatIds;
	}

	public void setBuscatIds(String buscatIds) {
		this.buscatIds = buscatIds;
	}

	public String getSubcatIds() {
		return subcatIds;
	}

	public void setSubcatIds(String subcatIds) {
		this.subcatIds = subcatIds;
	}

	public Boolean getIsBand() {
		return isBand;
	}

	public void setIsBand(Boolean isBand) {
		this.isBand = isBand;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey() {
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	/**
	 * @return the sortOrder
	 */
	public String getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * @param columnName
	 *            the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit() {
		return lowerLimit;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param storeID
	 *            the storeID to set
	 */
	public void setStoreID(String storeID) {
		this.storeID = storeID;
	}

	/**
	 * @return the storeID
	 */
	public String getStoreID() {
		return storeID;
	}

	/**
	 * @param retailId
	 *            the retailId to set
	 */
	public void setRetailId(Integer retailId) {
		this.retailId = retailId;
	}

	/**
	 * @return the retailId
	 */
	public Integer getRetailId() {
		return retailId;
	}

	/**
	 * @param retailName
	 *            the retailName to set
	 */
	public void setRetailName(String retailName) {
		this.retailName = retailName;
	}

	/**
	 * @return the retailName
	 */
	public String getRetailName() {
		return retailName;
	}

	/**
	 * @param numberOfLocations
	 *            the numberOfLocations to set
	 */
	public void setNumberOfLocations(Integer numberOfLocations) {
		NumberOfLocations = numberOfLocations;
	}

	/**
	 * @return the numberOfLocations
	 */
	public Integer getNumberOfLocations() {
		return NumberOfLocations;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param retailerIds
	 *            the retailerIds to set
	 */
	public void setRetailerIds(String retailerIds) {
		this.retailerIds = retailerIds;
	}

	/**
	 * @return the retailerIds
	 */
	public String getRetailerIds() {
		return retailerIds;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getRetUrl() {
		return retUrl;
	}

	public void setRetUrl(String retUrl) {
		this.retUrl = retUrl;
	}

	public String getRetailInfoJson() {
		return retailInfoJson;
	}

	public void setRetailInfoJson(String retailInfoJson) {
		this.retailInfoJson = retailInfoJson;
	}

	/**
	 * @return the bCategoryIds
	 */
	public String getbCategoryIds() {
		return bCategoryIds;
	}

	/**
	 * @param bCategoryIds
	 *            the bCategoryIds to set
	 */
	public void setbCategoryIds(String bCategoryIds) {
		this.bCategoryIds = bCategoryIds;
	}

	/**
	 * @return the retailLocationId
	 */
	public Integer getRetailLocationId() {
		return retailLocationId;
	}

	/**
	 * @param retailLocationId
	 *            the retailLocationId to set
	 */
	public void setRetailLocationId(Integer retailLocationId) {
		this.retailLocationId = retailLocationId;
	}

	/**
	 * @return the categoryType
	 */
	public String getCategoryType() {
		return categoryType;
	}

	/**
	 * @param categoryType
	 *            the categoryType to set
	 */
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	/**
	 * @param retailLowerLimit
	 *            the retailLowerLimit to set
	 */
	public void setRetailLowerLimit(Integer retailLowerLimit) {
		this.retailLowerLimit = retailLowerLimit;
	}

	/**
	 * @return the retailLowerLimit
	 */
	public Integer getRetailLowerLimit() {
		return retailLowerLimit;
	}

	public Integer getIsLoginExist() {
		return isLoginExist;
	}

	public void setIsLoginExist(Integer isLoginExist) {
		this.isLoginExist = isLoginExist;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getCurrentPageNum() {
		return currentPageNum;
	}

	public void setCurrentPageNum(Integer currentPageNum) {
		this.currentPageNum = currentPageNum;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the isPaid
	 */
	public Boolean getIsPaid() {
		return isPaid;
	}

	/**
	 * @param isPaid
	 *            the isPaid to set
	 */
	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}

}

/**
 * 
 */
package common.pojo;

import java.util.ArrayList;

/**
 * @author sangeetha.ts
 *
 */
public class HotDealDetails
{
	/**
	 * Variable which holds list of hot deal details.
	 */
	private ArrayList<HotDeal> hotDeals;
	/**
	 * Variable which holds total number of hot deals.
	 */
	private Integer totalSize;
	/**
	 * Variable which act as a next page flag.
	 */
	private Integer nextpage;
	/**
	 * @return the hotDeals
	 */
	public ArrayList<HotDeal> getHotDeals()
	{
		return hotDeals;
	}
	/**
	 * @param hotDeals the hotDeals to set
	 */
	public void setHotDeals(ArrayList<HotDeal> hotDeals)
	{
		this.hotDeals = hotDeals;
	}
	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize()
	{
		return totalSize;
	}
	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(Integer totalSize)
	{
		this.totalSize = totalSize;
	}
	/**
	 * @return the nextpage
	 */
	public Integer getNextpage()
	{
		return nextpage;
	}
	/**
	 * @param nextpage the nextpage to set
	 */
	public void setNextpage(Integer nextpage)
	{
		this.nextpage = nextpage;
	}
	
	
}

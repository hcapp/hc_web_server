package common.pojo;

import java.util.ArrayList;

public class AlertCategory
{

	private Integer totalSize;
	private ArrayList<EventCategory> alertCatLst;
	
	private ArrayList<Category> categorieslst;
	
	private String filterCategory;
	
	private String filters;
	
	private String filterValues;

	public Integer getTotalSize()
	{
		return totalSize;
	}

	public void setTotalSize(Integer totalSize)
	{
		this.totalSize = totalSize;
	}

	public ArrayList<EventCategory> getAlertCatLst()
	{
		return alertCatLst;
	}

	public void setAlertCatLst(ArrayList<EventCategory> alertCatLst)
	{
		this.alertCatLst = alertCatLst;
	}

	public ArrayList<Category> getCategorieslst() {
		return categorieslst;
	}

	public void setCategorieslst(ArrayList<Category> categorieslst) {
		this.categorieslst = categorieslst;
	}

	public String getFilterCategory() {
		return filterCategory;
	}

	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public String getFilterValues() {
		return filterValues;
	}

	public void setFilterValues(String filterValues) {
		this.filterValues = filterValues;
	}

}

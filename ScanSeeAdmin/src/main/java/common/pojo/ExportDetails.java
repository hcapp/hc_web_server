/**
 * 
 */
package common.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sangeetha.ts
 *
 */
public class ExportDetails {
	
	private String filePath;
	
	private ArrayList<DashboardExportData> dashboardExportData;
	
	private ArrayList<RetailerExportData> retailerExportData;

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return the dashboardExportData
	 */
	public ArrayList<DashboardExportData> getDashboardExportData() {
		return dashboardExportData;
	}

	/**
	 * @param dashboardExportData the dashboardExportData to set
	 */
	public void setDashboardExportData(
			ArrayList<DashboardExportData> dashboardExportData) {
		this.dashboardExportData = dashboardExportData;
	}

	/**
	 * @return the retailerExportData
	 */
	public ArrayList<RetailerExportData> getRetailerExportData() {
		return retailerExportData;
	}

	/**
	 * @param exportList the retailerExportData to set
	 */
	public void setRetailerExportData(
			ArrayList<RetailerExportData> exportList) {
		this.retailerExportData =  exportList;
	}
}

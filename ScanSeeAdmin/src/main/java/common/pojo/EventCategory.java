package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class EventCategory
{

	private Integer lowerLimit;
	private Integer rowNum;
	private Integer catId;
	private String catName;
	private boolean associateCate;
	private String actionType;
	private String cateImgName;
	private String oldCateImgName;
	private String adminId;
	private String cateImg;
	private CommonsMultipartFile cateImgPath;
	private String viewName;

	public Integer getLowerLimit()
	{
		return lowerLimit;
	}

	public void setLowerLimit(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	public Integer getRowNum()
	{
		return rowNum;
	}

	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}

	public Integer getCatId()
	{
		return catId;
	}

	public void setCatId(Integer catId)
	{
		this.catId = catId;
	}

	public String getCatName()
	{
		return catName;
	}

	public void setCatName(String catName)
	{
		this.catName = catName;
	}

	public boolean isAssociateCate()
	{
		return associateCate;
	}

	public void setAssociateCate(boolean associateCate)
	{
		this.associateCate = associateCate;
	}

	public String getActionType()
	{
		return actionType;
	}

	public void setActionType(String actionType)
	{
		this.actionType = actionType;
	}

	
	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getCateImgName() {
		return cateImgName;
	}

	public void setCateImgName(String cateImgName) {
		this.cateImgName = cateImgName;
	}

	public CommonsMultipartFile getCateImgPath() {
		return cateImgPath;
	}

	public void setCateImgPath(CommonsMultipartFile cateImgPath) {
		this.cateImgPath = cateImgPath;
	}

	public String getCateImg() {
		return cateImg;
	}

	public void setCateImg(String cateImg) {
		this.cateImg = cateImg;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getOldCateImgName() {
		return oldCateImgName;
	}

	public void setOldCateImgName(String oldCateImgName) {
		this.oldCateImgName = oldCateImgName;
	}

	
}



package common.pojo;

import java.util.ArrayList;

/**
 * This pojo class is used to store the coupon details
 * @author malathi_lr
 *
 */
public class CouponDetails {

	/**
	 * Variable declared for couponDetailList
	 */
	public ArrayList<Coupon> couponDetailList = null;
	
	/**
	 * variable declared for totalSize
	 */
	public int totalSize;
	
	/**
	 * Variable declared for nextPage
	 */
	public int nextPage;

	/**
	 * @return the nextPage
	 */
	public int getNextPage() {
		return nextPage;
	}

	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}

	/**
	 * @return the couponDetailList
	 */
	public ArrayList<Coupon> getCouponDetailList() {
		return couponDetailList;
	}

	/**
	 * @return the totalSize
	 */
	public int getTotalSize() {
		return totalSize;
	}

	/**
	 * @param couponDetailList the couponDetailList to set
	 */
	public void setCouponDetailList(ArrayList<Coupon> couponDetailList) {
		this.couponDetailList = couponDetailList;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}
}

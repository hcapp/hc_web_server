/**
 * 
 */
package common.pojo;

import java.util.List;

/**
 * @author sangeetha.ts
 */
public class Category {
	/**
	 * variable for category id.
	 */
	private Integer categoryId;
	/**
	 * variable for category Name.
	 */
	private String categoryName;

	/**
	 * variable for bCategory.
	 */
	private String bCategory;

	/**
	 * variable for associated .
	 */
	private Integer associated;
	/**
	 * variable for associated .
	 */
	private Boolean filterAssociated;
	/**
	 * variable for associated .
	 */

	private Integer checked;

	private Boolean subCatAssociated;

	private List<Category> buscategoryList;

	private List<Category> subcategoryList;
	
	private List<Filter> filterlst;
	private Integer filterID;
	private String filterName;	
	private Integer filterValueID;
	private String FilterValueName;
	
	private String filterCategory;
	
	private String filters;
	
	private String filterValues;
    private Long businessCategoryID;
	
	private String businessCategoryName;
	
	private Integer retailId;
	private Integer retailLocationId;
	

	public Integer getChecked() {
		return checked;
	}

	public void setChecked(Integer checked) {
		this.checked = checked;
	}

	public List<Category> getBuscategoryList() {
		return buscategoryList;
	}

	public void setBuscategoryList(List<Category> buscategoryList) {
		this.buscategoryList = buscategoryList;
	}

	public List<Category> getSubcategoryList() {
		return subcategoryList;
	}

	public void setSubcategoryList(List<Category> subcategoryList) {
		this.subcategoryList = subcategoryList;
	}

	/**
	 * @return the categoryId
	 */
	public Integer getCategoryId() {
		return categoryId;
	}

	/**
	 * @param categoryId
	 *            the categoryId to set
	 */
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the bCategory
	 */
	public String getbCategory() {
		return bCategory;
	}

	/**
	 * @param bCategory
	 *            the bCategory to set
	 */
	public void setbCategory(String bCategory) {
		this.bCategory = bCategory;
	}

	/**
	 * @return the associated
	 */
	public Integer getAssociated() {
		return associated;
	}

	/**
	 * @param associated
	 *            the associated to set
	 */
	public void setAssociated(Integer associated) {
		this.associated = associated;
	}

	/**
	 * @return the filterAssociated
	 */
	public Boolean getFilterAssociated() {
		return filterAssociated;
	}

	/**
	 * @param filterAssociated
	 *            the filterAssociated to set
	 */
	public void setFilterAssociated(Boolean filterAssociated) {
		this.filterAssociated = filterAssociated;
	}

	/**
	 * @return the subCatAssociated
	 */
	public Boolean getSubCatAssociated() {
		return subCatAssociated;
	}

	/**
	 * @param subCatAssociated
	 *            the subCatAssociated to set
	 */
	public void setSubCatAssociated(Boolean subCatAssociated) {
		this.subCatAssociated = subCatAssociated;
	}

	public List<Filter> getFilterlst() {
		return filterlst;
	}

	public void setFilterlst(List<Filter> filterlst) {
		this.filterlst = filterlst;
	}

	

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}



	public String getFilterValueName() {
		return FilterValueName;
	}

	public void setFilterValueName(String filterValueName) {
		FilterValueName = filterValueName;
	}

	public Long getBusinessCategoryID() {
		return businessCategoryID;
	}

	public void setBusinessCategoryID(Long businessCategoryID) {
		this.businessCategoryID = businessCategoryID;
	}

	public String getBusinessCategoryName() {
		return businessCategoryName;
	}

	public void setBusinessCategoryName(String businessCategoryName) {
		this.businessCategoryName = businessCategoryName;
	}

	public Integer getFilterID() {
		return filterID;
	}

	public void setFilterID(Integer filterID) {
		this.filterID = filterID;
	}

	public Integer getFilterValueID() {
		return filterValueID;
	}

	public void setFilterValueID(Integer filterValueID) {
		this.filterValueID = filterValueID;
	}

	public String getFilterCategory() {
		return filterCategory;
	}

	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public String getFilterValues() {
		return filterValues;
	}

	public void setFilterValues(String filterValues) {
		this.filterValues = filterValues;
	}

	public Integer getRetailId() {
		return retailId;
	}

	public void setRetailId(Integer retailId) {
		this.retailId = retailId;
	}

	public Integer getRetailLocationId() {
		return retailLocationId;
	}

	public void setRetailLocationId(Integer retailLocationId) {
		this.retailLocationId = retailLocationId;
	}

	

}

package common.pojo;

/**
 * Pojo for City
 * 
 * @author sangeetha.ts
 */
public class City
{
	/**
	 * variable for city name
	 */
	private String cityName;

	/**
	 * 
	 */
	private String postalCode;

	/**
	 * @return cityName as string
	 */
	public String getCityName()
	{
		return cityName;
	}

	/**
	 * @param cityName
	 */
	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

}

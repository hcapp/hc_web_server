package common.pojo;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * This class for sending mails to single and multiple users.
 * @author shyamsundara_hm
 *
 */
/**
 * The class for sending emails.
 * 
 * @author Kumar D
 */

public class EmailComponent
{
	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory.getLogger(EmailComponent.class.getName());

	/**
	 * default constructor for EmailComponent.
	 */
	public EmailComponent()
	{
	}

	/**
	 * The method for sending emails.
	 * 
	 * @param fromAddress
	 *            of the user.
	 * @param toAddrArray
	 *            as multiple recepients.
	 * @param subject
	 *            of the mail.
	 * @param msgBody
	 *            as email content are the request parameters.
	 * @throws MessagingException
	 *             for any exceptions occured while sending email.
	 */
	public static void mailingComponent(String fromAddress, String toAddrArray, String subject, String msgBody, String smtpHost, String smtpPort)
			throws MessagingException
	{
		LOG.info("Inside EmailComponent: mailingComponent");
		Session session;
		MimeMessage mimeMessage = null;
		final Properties props = System.getProperties();
		props.put("mail.smtp.host", smtpHost);
		props.setProperty("mail.smtp.port", smtpPort);
		session = Session.getDefaultInstance(props, null);
		final InternetAddress[] toAddress = { new InternetAddress(toAddrArray) };
		// create a message
		mimeMessage = new MimeMessage(session);
		mimeMessage.setFrom(new InternetAddress(fromAddress));
		if (toAddrArray != null)
		{
			mimeMessage.setRecipients(Message.RecipientType.TO, toAddress);
		}
		mimeMessage.setSubject(subject);
		// create and fill the first message part
		final MimeBodyPart bodyText = new MimeBodyPart();
		// bodyText.setText(msgBody);
		bodyText.setContent(msgBody, "text/html");
		// create the Multipart and add its parts to it
		final Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(bodyText);
		// add the Multipart to the message
		mimeMessage.setContent(multipart);
		// set the Date: header
		mimeMessage.setSentDate(new Date());
		// Jboss & Fix
		Thread.currentThread().setContextClassLoader(mimeMessage.getClass().getClassLoader());

		// send the message
		Transport.send(mimeMessage);
		LOG.info("Inside EmailComponent : mailingComponent : Mail Sent successfully");
	}

	/**
	 * The method for sending emails to multiple users.
	 * 
	 * @param fromAddress
	 *            of the user.
	 * @param toAddrArray
	 *            as multiple recepients.
	 * @param strSubject
	 *            of the mail.
	 * @param strMsgBody
	 *            as email content are the request parameters.
	 * @throws MessagingException
	 *             for any exceptions occured while sending email.
	 */
	public static boolean sendMailWithAttachment(String[] strTo, String strFrom, String strSubject, String strBody, String fileAttachment,
			String smtpHost, String smtpPort)
	{
		final String methodName = "sendMailWithAttachment";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		final Properties props = System.getProperties();
		try
		{
			props.put("mail.smtp.host", smtpHost);
			props.setProperty("mail.smtp.port", smtpPort);
			final Session session = Session.getDefaultInstance(props, null);
			final Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(strFrom));
			for (int i = 0; i < strTo.length; i++)
			{
				InternetAddress[] address = InternetAddress.parse(strTo[i]);
				msg.addRecipients(Message.RecipientType.TO, address);
			}

			msg.setSubject(strSubject);
			MimeBodyPart textPart = new MimeBodyPart();
			textPart.setContent(strBody, "text/html");
			MimeBodyPart attachFilePart = new MimeBodyPart();
			Multipart mp = new MimeMultipart();
			if (!"".equals(Utility.checkNull(fileAttachment)))
			{
				FileDataSource fds = new FileDataSource(fileAttachment);
				attachFilePart.setDataHandler(new DataHandler(fds));
				attachFilePart.setFileName(fds.getName());
				mp.addBodyPart(attachFilePart);
			}

			mp.addBodyPart(textPart);

			msg.setContent(mp);
			Transport.send(msg);
		}
		catch (MessagingException mex)
		{
			LOG.error("Exception occured in sending mail with attachment" + mex);
			return false;
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return true;
	}

	public static void multipleUsersmailingComponent(String fromAddress, String[] toAddrArray, String subject, String msgBody, String smtpHost,
			String smtpPort) throws MessagingException
	{

		Session session;
		MimeMessage mimeMessage = null;

		final Properties props = System.getProperties();

		/*
		 * ClassLoader loader = ClassLoader.getSystemClassLoader(); URL url =
		 * loader.getResource("scansee.properties"); Properties properties = new
		 * Properties(); properties.load( url.openStream()); String
		 * host=properties.getProperty("mail.smtp.host"); String
		 * post=properties.getProperty("mail.smtp.port");
		 */

		props.put("mail.smtp.host", smtpHost);
		props.setProperty("mail.smtp.port", smtpPort);

		session = Session.getDefaultInstance(props, null);

		// InternetAddress[] toAddress2 = { new InternetAddress(toAddrArray) };

		mimeMessage = new MimeMessage(session);
		mimeMessage.setFrom(new InternetAddress(fromAddress));
		final InternetAddress[] toAddress2 = new InternetAddress[toAddrArray.length];

		for (int i = 0; i < toAddrArray.length; i++)
		{
			toAddress2[i] = new InternetAddress(toAddrArray[i]);
		}

		mimeMessage.setRecipients(Message.RecipientType.TO, toAddress2);
		mimeMessage.addRecipients(Message.RecipientType.BCC, toAddress2);

		// create a message

		/*
		 * if (toAddrArray != null) {
		 * mimeMessage.addRecipient(Message.RecipientType.TO, new
		 * InternetAddress(toAddrArray) );
		 * mimeMessage.setRecipients(Message.RecipientType.TO, toAddress); }
		 */

		mimeMessage.setSubject(subject);

		// create and fill the first message part
		final MimeBodyPart bodyText = new MimeBodyPart();
		// bodyText.setText(msgBody);
		bodyText.setContent(msgBody, "text/html");
		// create the Multipart and add its parts to it
		final Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(bodyText);
		// add the Multipart to the message
		mimeMessage.setContent(multipart);

		// set the Date: header
		mimeMessage.setSentDate(new Date());

		// Jboss & Fix
		Thread.currentThread().setContextClassLoader(mimeMessage.getClass().getClassLoader());

		// send the message
		Transport.send(mimeMessage);

	}

}

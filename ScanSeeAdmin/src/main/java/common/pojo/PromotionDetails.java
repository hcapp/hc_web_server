package common.pojo;

import java.util.ArrayList;

public class PromotionDetails
{
	/**
	 * promotions as ArrayList
	 */
	private ArrayList<Promotion> promotions;
	/**
	 * totalSize as int
	 */
	private int totalSize;
	/**
	 * nextPage as int
	 */
	private int nextPage;

	/**
	 * @return the promotions
	 */
	public ArrayList<Promotion> getPromotions()
	{
		return promotions;
	}

	/**
	 * @param promotions the promotions to set
	 */
	public void setPromotions(ArrayList<Promotion> promotions)
	{
		this.promotions = promotions;
	}

	/**
	 * @return the totalSize
	 */
	public int getTotalSize()
	{
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(int totalSize)
	{
		this.totalSize = totalSize;
	}

	/**
	 * @return the nextPage
	 */
	public int getNextPage()
	{
		return nextPage;
	}

	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(int nextPage)
	{
		this.nextPage = nextPage;
	}

}

/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 *
 */
public class APIPartner {
	
	private Integer apiPartnerID;
	
	private String apiPartnerName;

	/**
	 * @return the apiPartnerID
	 */
	public Integer getApiPartnerID() {
		return apiPartnerID;
	}

	/**
	 * @param apiPartnerID the apiPartnerID to set
	 */
	public void setApiPartnerID(Integer apiPartnerID) {
		this.apiPartnerID = apiPartnerID;
	}

	/**
	 * @return the apiPartnerName
	 */
	public String getApiPartnerName() {
		return apiPartnerName;
	}

	/**
	 * @param apiPartnerName the apiPartnerName to set
	 */
	public void setApiPartnerName(String apiPartnerName) {
		this.apiPartnerName = apiPartnerName;
	}
}

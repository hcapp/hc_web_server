package common.pojo;

import java.util.ArrayList;

public class FilterInfo {

	private ArrayList<Filter> filterList = new ArrayList<Filter>();
	
	private Integer totalSize;
	
	private Integer nextPage;

	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize() {
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

	/**
	 * @return the filterList
	 */
	public ArrayList<Filter> getFilterList() {
		return filterList;
	}

	/**
	 * @param filterList the filterList to set
	 */
	public void setFilterList(ArrayList<Filter> filterList) {
		this.filterList = filterList;
	}

	/**
	 * @return the nextPage
	 */
	public Integer getNextPage() {
		return nextPage;
	}

	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}
}

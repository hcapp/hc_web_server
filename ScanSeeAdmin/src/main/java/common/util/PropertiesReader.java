package common.util;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesReader
{
	/**
	 * Variable prop declared as Properties.
	 */
	private static Properties prop; // Properties
	/**
	 * Get logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(PropertiesReader.class);

	/**
	 * Private constructor to avoid instantiation of this object.
	 */

	/**
	 * This method takes the property name and returns the value of it in the
	 * properties file.
	 * 
	 * @param strPropertyName
	 *            The property name
	 * @return strPropertyValue The property value
	 */

	public void readProperties()
	{
		prop = null;
		if (prop == null)
		{
			InputStream stream = this.getClass().getClassLoader().getResourceAsStream("scanseeadmin.properties");
			try
			{
				prop = new Properties();
				prop.load(stream);
			}
			catch (Exception ex)
			{
				prop = null;
				ex.printStackTrace();
			}
		}
	}

	public static String getPropertyValue(String strPropertyName)
	{
		String strPropertyValue = "";
		try
		{
			strPropertyValue = ((String) prop.get(strPropertyName)).trim();
		}// end of try
		catch (Exception e)
		{
		}
		return strPropertyValue;
	}
}

package common.constatns;

/**
 * Class to store all application related constants.
 * 
 * @author manjunatha_gh
 */
public class ApplicationConstants
{

	/**
	 * for database Error Message.
	 */
	public static final String SUCCESS = "Success";

	/**
	 * MethodStart declared as String for logger messages.
	 */
	public static final String METHODSTART = "In side method >>> ";
	/**
	 * MethodEnd declared as String for logger messages.
	 */
	public static final String METHODEND = " Exiting method >>> ";
	/**
	 * for database status.
	 */
	public static final String STATUS = "Status";

	/**
	 * for database error number.
	 */
	public static final String ERRORNUMBER = "ErrorNumber";

	/**
	 * for database Error Message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";
	/**
	 * 
	 */
	public static final String LOGINSERVICE = "loginService";
	/**
	 * 
	 */
	public static final String COUPONSERVICE = "couponService";
	/**
	 * 
	 */
	public static final String PROMOTIONSERVICE = "promotionService";
	/**
	 * 
	 */
	public static final String RETAILERSERVICE = "retailerService";
	/**
	 * 
	 */
	public static final String HOTDEALSERVICE = "hotDealService";
	/**
	 * 
	 */
	public static final String HUBCITISERVICE = "hubCitiService";
	
	/**
	 * 
	 */
	public static final String EXPORTSERVICE = "exportService";

	/**
	 * Declared USERNAME for first use.
	 */
	public static final String USERNAME = "UserName";

	/**
	 * userID declared as String for UserID text.
	 */
	public static final String USERID = "UserID";

	/**
	 * ExceptionOccurred declared as String for logger messages.
	 */
	public static final String EXCEPTIONOCCURRED = "Exception Occurred in  >>> ";

	/**
	 * for empty string.
	 */
	public static final String EMPTYSTRING = " ";

	/**
	 * Declared ScreenName for first use.
	 */
	public static final String SCREENNAME = "ScreenName";

	/**
	 * Declared CLRSCREENNAME for my gallery.
	 */
	public static final String CLRSCREENNAME = "CLR Screen";

	/**
	 * Declared LOWERLIMIT for pagination.
	 */
	public static final String LOWERLIMIT = "lowerLimit";

	/**
	 * Declared LOGIN as String for Login Screen.
	 */
	public static final String LOGIN = "Login";

	/**
	 * Schema name for Store procedures!
	 */
	public static final String WEBSCHEMA = "dbo";
	/**
	 * Schema name for Store procedures!
	 */
	public static final String HBCITIWEBSCHEMA = "HubCitiWeb";

	/**
	 * PAGINATION declared as String constant variable.
	 */
	public static final String PAGINATION = "pagination";
	/**
	 * ZERO declared as String constant variable.
	 */
	public static final String ZERO = "0";

	/**
	 * LOGINUSER declared as String constant variable.
	 */
	public static final String LOGINUSER = "loginuser";

	public static final String COUPONADMIN = "Coupon Administration";
	public static final String PROMOTIONADMIN = "Promotion Administration";
	public static final String RETAILERADMIN = "Retailer Administration";
	public static final String HOTDEALADMIN = "HotDeal Administration";
	public static final String HUBCITIADMIN = "HubCiti Administration";
	public static final String NATIONADMIN = "Nationwide Administration";
	public static final String EXPORTADMIN = "Export Administration";
	/**
	 * VERSIONMANAGEMENT declared as String constant variable.
	 */
	public static final String VERSIONMANAGEMENT = "Version Administration";
	/**
	 * VERSIONSERVICES declared as String constant variable.
	 */
	public static final String VERSIONSERVICES = "versionService";

	public static final String VERSIONEXIST = "Exists";

	public static final String SUCCESSTEXT = "Success";
	public static final String FAILURETEXT = "Failure";
	/**
	 * Constant for NOT APPLICABLE.
	 */
	public static final String NOTAPPLICABLE = "N/A";
	/**
	 * Declared EMAIL as String.
	 */
	public static final String EMAIL = "Release Note Email Configuaration";
	/**
	 * Declared SMTPHOST as String.
	 */
	public static final String SMTPHOST = "SMTP_Host";
	/**
	 * Declared SMTPPORT as String.
	 */
	public static final String SMTPPORT = "SMTP_Port";
	/**
	 * Declared RELEASE_NOTE_FROM as String.
	 */
	public static final String RELEASE_NOTE_FROM = "Release Note From";
	/**
	 * Declared RELEASE_NOTE_RECEPIENTS as String.
	 */
	public static final String RELEASE_NOTE_RECEPIENTS = "Release Note Recepients";
	/**
	 * Declared RELEASE_NOTE_SUBJECT as String.
	 */
	public static final String RELEASE_NOTE_SUBJECT = "Release Note Recepients";

	public static final String VERSIONQA = "QA";
	public static final String VERSIONPRODUCTION = "Production";
	public static final String JBOSS_HOME = "JBOSS_HOME";
	public static final String DEFAULT = "default";
	public static final String SERVER = "server";
	public static final String DEPLOY = "deploy";
	public static final String ROOTWAR = "ROOT.war";
	public static final String STANDALONE = "standalone";

	public static final String DEPLOYMENT = "deployments";

	public static final String ADDEDVESIONTEXT = "Created Version Successfully.";
	public static final String ADDEDVESIONERRORTEXT = "Error Adding Version.";
	public static final String VERSIONAPPAPPLICATIONTYPE = "App";
	public static final String VERSIONWEBAPPLICATIONTYPE = "Web";
	public static final String VERSIONITUNESRELEASETEXT = "iTunes";
	public static final String VERSIONEXISTSTEXT = "Version Exists.";
	public static final String EMAILCONFIG = "Email";
	public static final String FTPPATHTEXT = "ftp://ftp.scansee.net";
	public static final String FROMMAILID = "support@hubciti.com";
	public static final String VERSIONRELEASEPATH = "ReleaseNote";

	/**
	 * Declared WEBREGISTRATION as String for Web Configuration.
	 */
	public static final String WEBREGISTRATION = "Website Registration";
	/**
	 * Declared ADMINEMAILID as String for Web Configuration.
	 */
	public static final String ADMINEMAILID = "AdminMailID";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String FACEBOOKCONFG = "FaceBook";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String HUBCITICONFG = "HubCiti";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SCANSEEBASEURL = "Base_URL";

	public static final String SCANSEE_LOGO_FOR_MAILSENDING = "ScanSeeWeb/images/ScanSeeMailLogo.png";
	
	public static final String SUBJECT_MSG_FOR_SUCCESS_REGISTRATION = "Your HubCiti Has Been Created";

	public static final String SERVER_CONFIGURATION = "Configuration of server";

	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String INVALIDEMAIL = "invalidemail";
	public static final String URL_PATTERN = "^(https://|http://|ftp://|file://|www.)[-a-zA-Z0-9+&@#/%?=~_|!:.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	public static final String WEBURL = "invalidurl";
	public static final String DUPLICATEEMAIL = "DuplicateEmail";
	public static final String DUPLICATEHUBCITI = "DuplicateHubCiti";
	public static final String DUPLICATEUSERNAME = "DuplicateUser";
	public static final String DATEAFTER = "after";
	public static final String SALEPRICEGREATER = "salepricegreater";
	public static final String INVALIDURL = "invalidurl";
	public static final String PRICENOTZERO = "pricenotzero";
	public static final String EMPTYFILETEXT = "empty";
	public static final String EMPYFILEVALIDATIONTEXT = "Release note is empty,please select valid release note.";

	public static final String EMAILCONTENT_QA = "version has been released to ScanSee QA.";
	public static final String EMAILCONTENT_PRODUCTION = "version has been released to Production.";
	public static final String SEARCHKEY = "searchKey";
	public static final String MODULENAME = "moduleName";

	public static final String HUBCITIWEBPAGINATION = "HubCitiWeb";

	public static final String ALERTCATEGORYASSCOIATED = "This Category is already associated";

	public static final String SETUPRETAILERLOCATIONSCREENNAME = "HubCitiRetaillocationSetup";

	public static final String ALERTCATEXISTS = "CategoryExists";
	/**
	 * 
	 */
	public static final String BUSINESS_CATEGORIES  = "Business categories";
	/**
	 * 
	 */
	public static final String UPDATE_BUSINESS_CATEGORIES_SUCCESS_MSG  = "Retailer Business Categories are updated successfully.";
	/**
	 * 
	 */
	public static final String UPDATE_BUSINESS_CATEGORIES_ERROR_MSG  = "Error occurred while updating Business Categories.";
	/**
	 * 
	 */
	public static final String UPDATE_SUB_CATEGORIES_SUCCESS_MSG  = "Location Sub Categories are updated successfully.";
	/**
	 * 
	 */
	public static final String UPDATE_SUB_CATEGORIES_ERROR_MSG  = "Error occurred while updating Sub Categories.";
	/**
	 * 
	 */
	public static final String ASSOCIATE_BUSINESS_CATEGORY  = "Please associate Business categories for the retailer and then select sub-categories and continue to save.";


	public static final String ROLE_RETAIL_ADMIN  ="Retailer Admin";
	
	public static final Integer CROPIMAGEHEIGHT =600;
	
	public static final Integer CROPIMAGEWIDTH =800;
	
	public static final String IMAGES = "Images";
	
	public static final String TEMP = "temp";
	
	public static final String TEMPFOLDER = "temp";
	
	public static final String UPLOADIMAGEPATH = "../images/imgIcon.png";
	
	public static final String 	EVENTVIEWNAME = "addEvent";
	
	public static final String 	HUBCITI = "hubciti";
	
	public static final String 	BLANKIMAGE ="../images/uploadIcon.png";
	
	public static final String SUBJECT_MSG_FOR_RETAILER_LOGIN = "Your Login Details For HubCiti";
	
	public static final String SUBJECT_MSG_FOR_BAND_LOGIN = "Your Login Details For Band Portal";
	
	
	/**
	 * LOCATIONLOGO declared as String constant variable .
	 */
	public static final String LOCATIONLOGO = "locationlogo";
	
	public static final String RETAILER = "retailer";
	
	public static final String UPLOADGRIDIMAGE="/ScanSeeAdmin/images/dfltImg.png";
	
	/**
	 * IMAGES_SLASH_RETAILER declared as String constant variable .
	 */
	public static final String IMAGES_SLASH_RETAILER = "/Images/retailer/";
	
	/**
	 * SLASH_LOCATIONLOGO_SLASH declared as String constant variable .
	 * 
	 */
	public static final String SLASH_LOCATIONLOGO_SLASH = "/locationlogo/";
	
	public static final String RETAILER_SCREENNAME = "RetailAdmin";
	
	public static final String CATEGORYADMIN = "Category Administration";
	
	public static final String REGIONAPP = "RegionApp";
	
	public static final String HUBCITIAPP = "HubCitiApp";
	
	
	
	public static final String REGIONNOTFOUNDTEXT = "No Region Found.";
	
	public static final String DEACTIVATEREGSUCCESSTEXT = "Region Admin deactivated successfully";
	
	public static final String DEACTIVATEFAILURETEXT = "Failed to deactivate Region Admin";
	
	public static final String ACTIVATEREGSUCESSTEXT = "Region Admin activated successfully";
	
	public static final String ACTIVATEREGFAILURETEXT = "Failed to activate Region Admin";
	
	public static final String SUBJECT_MSG_FOR_SUCCESS_REGION_REGISTRATION = "Your Region Has Been Created";

	
	
	
	/*
	 	Filter Administartion constant.
	 */
	public static final String FILTEREXISTS = "FilterExists";
	public static final String FILTERVALEXISTS = "FilterValueExists";
	public static final String FILTERADMIN = "Filter Administration";
	public static final String FILTERSERVICE = "filterService";
	public static final String DUPLICATEFILTERNAME = "Filter Name already exist";
	public static final String ADD_FILTER_ERROR_MSG  = "Error occurred while adding Filter.";
	public static final String UPDATE_FILTER_ERROR_MSG  = "Error occurred while updating Filter.";
	public static final String FILTERASSCOIATED = "This Filter is already associated to retailer. Please De-Associate and continue";
	

	//Nationwide deals constants
	public static final String NATIONWIDETEXT = "Nationwide Deals changes saved successfully";
	
	public static final String NATIONHUBCITIREGNOTFOUNDTEXT = "No HubCiti / Region Found.";

	public static final String EXPORTEMAILCONFIG = "Export Email Configuration";
	/**
	 * Declared RELEASE_NOTE_FROM as String.
	 */
	public static final String EXPORT_FROM = "Export From";
	/**
	 * Declared RELEASE_NOTE_RECEPIENTS as String.
	 */
	public static final String EXPORT_RECEPIENTS = "Export Recepients";

	public static final String DATAEXPORT = "DataExport";
	
	public static final String INVALIDZIPCODE = "Invalidzipcode";
	
	
	/*
	 * Caching Related Constants
	 */
	public static final String FIND_CLEAR_CACHE_URL = "FindClearCacheURL";
	

	public static final String FILTERSTEXT = "There is no filter associated to business category selected at retailer.";
	
	public static final String UPDATE_FILTERS_SUCCESS_MSG  = "Location Filters are saved successfully.";
	public static final String UPDATE_FILTERS_FAILURE_MSG  = "Error occurred while updating filters.";
	

	public static final String INVALIDSALESCONTACT = "invalidsalescontact";
	
	public static final String DUPLICATESALESCONTACT = "DuplicateSalesContact";
	
	
	public static final String INVALID_EMAILS = "invalid_emails";
	
	public static final String NUMBER_OF_EMAILS = "num_of_emails";
	public static final String DUPLICATE_EMAILS = "duplicate_emails";
	public static final String INVALID_EMAIL = "invalidemails";

	private ApplicationConstants()
	{

	}
}

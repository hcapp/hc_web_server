package common.tags;

import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.constatns.ApplicationConstants;
import common.util.Utility;

public class MenuTag extends SimpleTagSupport {

	private static final Logger LOG = LoggerFactory
			.getLogger(PaginationTagHandler.class);

	public static final String COUPONADMIN = "Coupon Administration";
	public static final String PROMOTIONADMIN = "Promotion Administration";
	public static final String RETAILERADMIN = "Retailer Administration";
	public static final String HOTDEALADMIN = "HotDeal Administration";
	public static final String HUBCITIADMIN = "HubCiti Administration";
	public static final String VERSIONADMIN = "Version Administration";
	public static final String CATEGORYADMIN = "Category Administration";
	public static final String FILTERADMIN = "Filter Administration";
	public static final String NATIONWIDEADMIN = "Nationwide Administration";
	public static final String EXPORTADMIN = "Export Administration";


	String[][] modulesDetails = new String[][] {
			{ COUPONADMIN, "couponAdmin();" },
			{ PROMOTIONADMIN, "promotionAdmin();" },
			{ RETAILERADMIN, "retailerAdmin();" },
			{ HOTDEALADMIN, "hotdealAdmin();" },
			{ VERSIONADMIN, "versionAdmin();" },
			{ HUBCITIADMIN, "hubcitiAdmin();" },
			{ CATEGORYADMIN, "categoryAdmin();" },
			{ FILTERADMIN, "filterAdmin();" },
			{ NATIONWIDEADMIN, "nationwideAdmin();" },
			{ EXPORTADMIN, "exportAdmin();" }				
	};
	

	/**
	 * This variable holds selects menu name.
	 */

	public String menuTitle = null;

	/**
	 * @return the menuTitle
	 */
	public String getMenuTitle() {
		return menuTitle;
	}

	/**
	 * @param menuTitle
	 *            the menuTitle to set
	 */
	public void setMenuTitle(String menuTitle) {
		this.menuTitle = menuTitle;
	}

	@Override
	public void doTag() throws JspException, IOException {
		LOG.info("Inside ConsumerTab tag handler : doTag ");
		PageContext pageContext = (PageContext) getJspContext();
		HttpSession session = pageContext.getSession();
		JspWriter out = pageContext.getOut();
		StringBuffer menuStrBuffer = new StringBuffer("<ul>");
		if (Utility.checkNull(this.menuTitle) != "") {
			/*
			 * if (HOME.equals(this.menuTitle)) {
			 */
			menuStrBuffer.append(getMenuItem(session));
			menuStrBuffer.append("</ul>");
			LOG.info("Pagination tag body :: " + menuStrBuffer.toString());
			out.write(menuStrBuffer.toString());

			// }
		} else {
			LOG.error("No Menu items Provided");

		}

	}

	public String getMenuItem(HttpSession session) {
		StringBuffer itemStrBuffer = new StringBuffer();
		String isRoleAdmin = (String) session.getAttribute("RoleAdmin");

		Boolean isCategorize = false;
		Boolean isCategorize2 = false;
		
		if (null != isRoleAdmin && !"".equals(isRoleAdmin)
				&& isRoleAdmin.equals(ApplicationConstants.ROLE_RETAIL_ADMIN)) {

			for (int i = 0; i < modulesDetails.length; i++) {

				if (menuTitle.equals(modulesDetails[i][0])
						&& isRoleAdmin
								.equals(ApplicationConstants.ROLE_RETAIL_ADMIN)) {

					if (i == modulesDetails.length - 1) {

						itemStrBuffer
								.append("<li class=\"subTab subTabLast active\">");
					} else {

						itemStrBuffer.append("<li class=\"subTab active\">");
					}

					itemStrBuffer.append("<a onclick=\"" + modulesDetails[i][1]
							+ "\" href=\"#\" >");
					itemStrBuffer.append(modulesDetails[i][0]);
					itemStrBuffer.append("</a></li>");
				} else {
					continue;
				}
			}
		} else {

			for (int i = 0; i < modulesDetails.length; i++) {

				if (menuTitle.equals(modulesDetails[i][0])) {
					if (modulesDetails[i][0].equals(RETAILERADMIN)) {
						continue;
					}


					if (modulesDetails[i][0].equals(HUBCITIADMIN) || modulesDetails[i][0].equals(CATEGORYADMIN) || modulesDetails[i][0].equals(FILTERADMIN)) {

						if (isCategorize == false) {
							itemStrBuffer.append("<li class=\"grpTab\">HubCiti</li>");
							isCategorize = true;
						}
					} else if (isCategorize2 == false) {
						itemStrBuffer.append("<li class=\"grpTab\">General</li>");
						isCategorize2 = true;

					}

					if (i == modulesDetails.length - 1) {

						itemStrBuffer
						.append("<li class=\"subTab subTabLast active\">");
					} else {

						itemStrBuffer.append("<li class=\"subTab active\">");
					}

				} else {
					if (modulesDetails[i][0].equals(RETAILERADMIN)) {
						continue;
					}


					if (modulesDetails[i][0].equals(HUBCITIADMIN) || modulesDetails[i][0].equals(CATEGORYADMIN) || modulesDetails[i][0].equals(FILTERADMIN)) {

						if (isCategorize == false) {
							itemStrBuffer.append("<li class=\"grpTab\">HubCiti</li>");
							isCategorize = true;
						}
					}else if (isCategorize2 == false) {
						itemStrBuffer.append("<li class=\"grpTab\">General</li>");
						isCategorize2 = true;

					}

					if (i == modulesDetails.length - 1) {

						itemStrBuffer
						.append("<li class=\"subTab subTabLast\">");
					} else {

						itemStrBuffer.append("<li class=\"subTab\">");
					}

				}

				itemStrBuffer.append("<a onclick=\"" + modulesDetails[i][1]
						+ "\" href=\"#\" >");
				itemStrBuffer.append(modulesDetails[i][0]);
				itemStrBuffer.append("</a></li>");
			}
		}
		return itemStrBuffer.toString();
	}
}

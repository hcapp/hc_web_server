<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Region</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Region Administration</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>



<script type="text/javascript">
$(document).ready(function() {
	
	var vRegHubId = '${requestScope.reghubids}';
	
		var arr = vRegHubId.split(',');	
	if(arr != '' && arr != null){
	
	jQuery.each(arr, function(i, val) { 

		$("#reghubcitiList option[value='" + val + "']").attr('selected', 'selected');
	});	
	}


var vState = '${requestScope.state}';
if(vState != '' && vState != null)
{
loadRegCities();
}else{
//$('input[name$="chkAllZip"]').attr('disabled', 'disabled');		
$('input[name$="chkAllZip"]').attr('disabled', 'disabled');	

}

var vHubCitiOptLen = $('#reghubcitiList').find("option").length;

	var vHubCitiSelLen = $('#reghubcitiList').find("option:selected").length;
	
	if(vHubCitiOptLen > 0)
		{
		
		if(vHubCitiOptLen == vHubCitiSelLen )
			{
				$('#chkAllHubCiti').attr('checked', 'checked');
				
			}else
				{
				$('#chkAllHubCiti').removeAttr('checked');
				}
		}else{
			
			$('#chkAllHubCiti').removeAttr("checked");
			
		}

$('#reghubcitiList').change(function (){

	var vHubCitiOptLen = $(this).find("option").length;
	var vHubCitiSelLen = $(this).find("option:selected").length;
	
	if(vHubCitiOptLen > 0)
		{
		
		if(vHubCitiOptLen == vHubCitiSelLen )
			{
			
			$('#chkAllHubCiti').attr('checked', 'checked');
			
			}else
				{
				$('#chkAllHubCiti').removeAttr('checked');
				}
		}else{
			
			$('#chkAllHubCiti').removeAttr("checked");
			
		}
	
	});
	
$("#postalCodes").live("change", function() {
	var totOpt = $('#postalCodes option').length;
	var totOptSlctd = $('#postalCodes option:selected').length;
	if(totOpt > 0)
	{
		if (totOpt == totOptSlctd) {
			$('#chkAllZip').attr('checked', 'checked');
		} else {
			$('#chkAllZip').removeAttr('checked');
		}
	}
	else
	{
		$('#chkAllZip').removeAttr('checked');
	}
});		

});




/* check and uncheck checkall button for postalcodelist if option item changes 
$('#postalCodes').change(function  (){


var vPostcodeOptlen = $('#postalCodes option').length;
var vSelPostcodelen = $('#postalCodes option:selected').length;
if(vPostcodeOptlen > 0)
{
if(vPostcodeOptlen == vSelPostcodelen)
	{
	$('#chkAllZip').add('checked', 'checked');
	
	}else{
		
		$('#chkAllZip').removeAttr("checked");
	}


}else{
$('#chkAllZip').removeAttr("checked");
}

});

 */
 
 



function selectAllZip(checked) {
		var sel = document.getElementById("postalCodes");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
	
	function selectAllHubCiti(checked)
	{
	var selHubCiti = document.getElementById("reghubcitiList");
	if(checked == true)
	{
	
		for( var j=0;j<selHubCiti.options.length;j++)
		{
		selHubCiti.options[j].selected = true;
		}
	}else{
	for(var j=0;j<selHubCiti.options.length;j++)
	{
	selHubCiti.options[j].selected =  false;
	}
	
	
	}
	
	
	}
	
	



/*Below method is used to load cities based on state selected.*/

 function loadRegCities()
 {
	var vState =  $('#state').val();
	var vCity = '${requestScope.regCity}';
	
	$.ajaxSetup ({cache : false});
	$.ajax({
		
		type :"GET" ,
		url : "loadregciti.htm",
		data : {
			'state' : vState,
			'city'  : vCity
			
		},
	success :function (response)
	{
		$('#regCtyDiv').html(response);
		
		//load postalcodes.
		loadPostalcodes();
	},	
	error : function()
	{
		alert("Error occured while loading cities");
	}	
	
	});
	
 }

/* Below method is used to fetch postalc codes based on city and state */
 function loadPostalcodes()
 {
	
	var vState = $('#state').val();
	var vCity = $('#city').val();
	if(vCity != null && vCity != "")
		{
			$('input[name$="chkAllZip"]').removeAttr('disabled');
		}
		else
		{
			$('input[name$="chkAllZip"]').attr('disabled', 'disabled');
		}
	
	
	$.ajaxSetup({cache:false});
	$.ajax({
	
		type : "GET",
		url  :  "getregpostalcode.htm",
		data :{
			'state' :vState,
			'city'  :vCity 
		},
		
		success :function(response)
		{
			$('#regZipcodeDiv').html(response);
			$("#postalCodes").trigger("change");

				var srcLstCnt = $("#postalCodes option").length;
				if(srcLstCnt==0){
					$('input[name$="chkAllZip"]').attr('disabled', 'disabled');	
				}
			
		},error : function()
		{
			alert("Error occured while loading zipcodes");
		}
		
	});
 }
	
	//This function is used to move code from from postalcode to postalcode. 
	function movePostalcodes(sourceid, destinationid){
		
		var vState = $('#state').val();
		var vCity = $('#city').val();
		
		
		
		var vPostalcode = '';
		$('#postalCodes option:selected').each(function (){		
			vPostalcode += $(this).val() + ",";			
		});
			
		if("" == vPostalcode)
			{
			
			alert("Please select  postalcode to move right");
			
			}else{
				
				$.ajaxSetup({cache:false});
				$.ajax({
					
					type : "GET",
					url: "movepostalcode.htm",
					data : {
						 "state" :  vState,
						 "city"   : vCity,
						 "zipcode" :vPostalcode 
					},
					
					success : function(response)
					{		

						$('#active').val(true);
						$("#" + sourceid + " option:selected").each(function(){
							var val = $(this).val();
							$("#defaultZipcode").append(new Option(val, val));
						});
						$("#" + sourceid + " option:selected").prependTo(
								"#" + destinationid).removeAttr("selected");
						var srcLstCnt = $("#" + sourceid + " option").length;
						$("#chkAllZip").removeAttr("checked");
						if (srcLstCnt == 0) {
							$('input[name$="chkAllZip"]').attr('disabled', 'disabled');
						}
						
					},error : function()
					{
						alert("Error occured while moving postalcodes");
					}
					
				
				
				
				});
				
				
				
			}
		
		}
	
	function backToRegionAdmin()
	{
	var msg =	confirm("Are you sure you want to leave this page without saving  Region details");
	if(msg == true)
		{
		
		document.RegionForm.lowerLimit = '${requestScopt.lowerLimit}';
		document.RegionForm.method = "POST";
		document.RegionForm.action = "displayregions.htm";
		document.RegionForm.submit();
		
		}
		
	}
	
	function updateRegionAdmin()
	{
	var vRegHubcitiIds='';
		$('#reghubcitiList option:selected').each(function (){	
			if(vRegHubcitiIds == '')
			{
			vRegHubcitiIds = $(this).val() + ",";	
			}else{
			vRegHubcitiIds += $(this).val() + ",";	
			}
			
		});
	vRegHubcitiIds=$.trim(vRegHubcitiIds);
		document.RegionForm.searchKey.value = null;
		document.RegionForm.selRegHubCitiIds.value =vRegHubcitiIds;
		document.RegionForm.method ="POST";
		document.RegionForm.action = "updateregion.htm";
		document.RegionForm.submit();
		
		
	}
	
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hubciti.png"
					alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
		<form:form name="RegionForm" commandName="RegionForm" acceptCharset="ISO-8859-1">

			<input type="hidden" name="searchKey" />
	
		<input type="hidden" name="selRegHubCitiIds"/>
		<form:hidden path="active" id="active"/>
		<form:hidden path="hubCitiID"/>
			<div class="floatL" id="vNav">
					<div class="vNavtopBg"></div>
					<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
					<div class="vNavbtmBg"></div>
				</div>
		
		 <div class="rtContPnl floatR">
      <div class="contBlock">
		
				
		
		 <fieldset id="hubcitiAdmin" class="dscntType mrgnTop">
          <legend>Region Details</legend>
          <div class="contWrap">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl">
              <tbody>
                <tr>
                  <td colspan="5" class="header">Edit Region</td>
                </tr>
                <tr>
                  <td class="Label"  width="18%" ><label for="regionName"  class="mandTxt">Region  Name</label></td>
                  <td colspan="5">
                   <form:errors cssStyle="color:red" path="hubCitiName"></form:errors>
                  <form:input type="text" name="hubCitiName" path="hubCitiName"  id="hubCitiName"   tabindex="1"/>
                 
                  </td>
                </tr>
                <tr>
                  <td class="Label"><label>State</label></td>
                  <td ><select class="textboxBig bigDropdownBox"  name="state" id="state" onchange="loadRegCities()"  tabindex="2">
                      <option value="">--Select--</option>
					  <c:forEach items="${sessionScope.regStatelst}" var="rstate" > 
					  <c:choose>
					  <c:when test="${requestScope.state ne rstate.stateabbr}">
																	<option value="${rstate.stateabbr}">
																		<c:out value="${rstate.stateName}"></c:out>
																	</option>
																</c:when>
																<c:otherwise>
																	<option value="${rstate.stateabbr}" selected="selected">
																		<c:out value="${rstate.stateName}"></c:out>
																	</option>
																</c:otherwise>
															</c:choose>					  
					  
					  			  
					  </c:forEach>
                    
                     
                    </select></td>
                  <td class="Label" width="9%"><label>City</label></td>
                  <td width="50%" colspan="2" >
                  <div id="regCtyDiv">
                    <select class="textboxBig bigDropdownBox" name="city" id="city"  tabindex="3">
                    <option value="">--Select--</option>
                    </select>
                  </div>           
                    
               </td>
                </tr>
                <tr>
                  <td class="Label" valign="top"><label  class="mandTxt"> Zip Code</label>
                    <p class="infoTxt">(Hold Ctrl to select more than one Zip Code)</p></td>
                  <td width="33%" class="remove-rt-brdr">
                  <div id="regZipcodeDiv">
                   <select name="postalCodes" size="1" multiple="multiple" id="postalCodes" 
                   class="textboxBig multiSelDropDown"  tabindex="4">
                     
                    </select>                
                  
                  </div>
				 
                    <p class="infoTxt">
                      <input type="checkbox" name="chkAllZip" id="chkAllZip" onclick="selectAllZip(this.checked);"  disabled="disabled"  tabindex="5"/>
                      <label for="checkAll">Select All</label>
                    </p></td>
                  <td valign="top" class="remove-rt-brdr" align="center"><p class="btnCntrl mrgnTop"><a href="javascript:void(0);"  

				  id="moveright" onclick="movePostalcodes('postalCodes','toPostalCode');">
				  <img src="images/MoveRt.png" alt="Move Right" width="24" height="16" title="Move Right"  tabindex="6" /></a></p>
                    <!--<p class="mrgnTop"><a href="javascript:void(0);" id="moveleft" alt="Move Left" width="24" height="16" title="Move Left" onclick="move_list_items('to-zipcode','from-zipcode');"><img src="images/MoveLt.png"/></a></p>--></td>
                  <td valign="top" class="remove-rt-brdr">
                  <form:errors cssStyle="color:red" path="postalCodes"></form:errors>
												<select name="toPostalCode" size="1" multiple="multiple" id="toPostalCode" class="textboxBig multiSelDropDown"
												 style="font-size: 11px;"  tabindex="7">
											  <c:if test="${sessionScope.selectedRegPostalCodes ne null || !empty sessionScope.selectedRegPostalCodes}">
													<c:forEach items="${sessionScope.selectedRegPostalCodes}" var="postalCode">
														<option value="${postalCode.postalCode}" title="${postalCode.location}" isAssocaited="${postalCode.isAssocaited}">${postalCode.location}</option>
													</c:forEach>
												</c:if>
                      
				 
                      
                    </select>
                    <p class="infoTxt">Selected Zip Codes
                  <td valign="top"><a href="#" title="Delete" id="remRegionZipcode">
				  <img src="images/deleteRec.png" width="24" height="16" alt="delete" class="mrgnTop"  tabindex="8"/></a>
                </tr>
                <tr>
                  <td class="Label">               
                  
                  <label  class="mandTxt"> Citi Experience</label></td>
                  
                  <td>
                   <form:errors cssStyle="color:red" path="citiExperience"></form:errors>
                  <form:input type="text" name="citiExperience" path="citiExperience" id="ciyExp" /></td>
                  
                  <td class="Label"><label for="userName"  class="mandTxt">Username</label></td>
                  <td colspan="2">
                  <form:errors cssStyle="color:red" path="userName"></form:errors>
                  <form:input type="text" name="userName" id="userName" path="userName"  value="${sessionScope.hubCitiUserName}" disabled="true" /></td>
                  
                  
                </tr>
                <tr>
                  
                  <td><label  class="mandTxt">Email ID</label></td>
                  <td>
                  <form:errors cssStyle="color:red" path="emailID"></form:errors>
                  <form:input type="text" name="emailID" path="emailID" id="emailID"/></td>
                  
                    <td class="Label">
												<label class="mandTxt" for="cityExp">Default Postal Code</label>
											</td>
											<td colspan="2">
												<form:errors cssStyle="color:red" path="defaultZipcode"></form:errors>
												<form:select path="defaultZipcode">
													<form:option value="0">----Select----</form:option>
													
													<c:forEach items="${sessionScope.defltPstlCdeLst}" var="postalCode">
														<form:option value="${postalCode}">${postalCode}</form:option>
													</c:forEach>
													
												</form:select>
											</td>
                </tr>
                
                <tr class="">
                  <td class="Label">HubCiti List</td>
                  <td colspan="1"><span class="remove-rt-brdr">
				  
				  
				  
				<select name="reghubcitiList" size="1" multiple="multiple" id="reghubcitiList" class="textboxBig multiSelDropDown"
												 style="font-size: 11px;"  tabindex="7">
												 
					 <c:forEach items="${sessionScope.hubCitilst}" var="hubciti">
				  
				    <option value="${hubciti.regHubcitiIds}" >${hubciti.hubCitiName}</option>
                 
                  </c:forEach> 							 
				
				</select>
				  
                   <!-- <form:select  path="reghubcitiList" size="1" multiple="multiple"  class="textboxBig multiSelDropDown">
                    
                  <c:forEach items="${sessionScope.hubCitilst}" var="hubciti">
				  
				    <form:option value="${hubciti.regHubcitiIds}" selected="">${hubciti.hubCitiName}</form:option>
                 
                  </c:forEach></form:select> -->
				  
				 
                    
		  				<p class="infoTxt">(Hold Ctrl to select more than one HubCiti)</p>
                    </span>
                    <p class="infoTxt">
                      <input type="checkbox" name="chkAllHubCiti" id="chkAllHubCiti" onclick="selectAllHubCiti(this.checked);" />
                      <label for="checkAll">Select All</label>
                    </p></td>
                    <td><label  class="mandTxt">Region Sales Contact</label></td>
                  <td colspan="2">
                  <form:errors cssStyle="color:red" path="salesPersonEmail"></form:errors>
                  <form:input type="text" name="salesPersonEmail" path="salesPersonEmail" id="salesPersonEmail"/></td>
                  
                </tr>
                 <tr>
                  <td class="Label"  width="18%" ><label for="Emails"  class="mandTxt">Claim Business Email ID's</label></td>
                  <td colspan="4">
                   <form:errors cssStyle="color:red" path="emails"></form:errors>
                  <form:input path="emails"  maxlength="300"/>
                 <br><p class="infoTxt">[Allows 5 emails separated by semicolon(;)]</p>
                  </td>
                </tr>
              </tbody>
            </table>
            <div class="navTabSec mrgnRt">
              <div align="right">
                <input name="Save" value="Update" type="button" title="Update Region Details" class="btn hltBtn" id="save" onclick="updateRegionAdmin()"/>
                <input type="button" onclick="backToRegionAdmin()" id="Back" class="btn" value="Back" name="back" value="Back">
              </div>
            </div>
          </div>
        </fieldset>
		
		</div>
		</div>
		</form:form>

</div>

<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>

</div>
</body>
</html>
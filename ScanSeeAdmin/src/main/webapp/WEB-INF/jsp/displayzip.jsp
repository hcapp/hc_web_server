<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="java.util.ArrayList,common.pojo.Product"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript">
$(window).scroll(function() {
	var scrlPos = $(this).scrollTop();
	if(scrlPos>=30){
	$("#clone-th").slideDown('fast');
	}
	else{
	$("#clone-th").hide();
	}
});
</script>

</head>
<body class="whiteBG">
	<div class="contBlock searchGrd relative">
		<form:form commandName="postalCodes" name="postalCodes"></form:form>
		<div id="clone-th">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
				<tr class="header">
					<td width="23%">State</td>
					<td width="25%">City</td>
					<td width="44%">Zipcode</td>
				</tr>
			</table>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
			<tr class="header">
				<td width="23%">State</td>
				<td width="25%">City</td>
				<td width="44%">Zipcode</td>
			</tr>			
			<c:forEach items="${sessionScope.postalCodes}" var="postalCodes" varStatus="status">
				<tr>
					<td valign="top">${postalCodes.stateName}</td>
					<td colspan="2" class="zeroPdng">
						<ul class="list-view">
							<c:forEach items="${postalCodes.cities}" var="cities">							
								<li>
									<span class="col1">${cities.cityName}</span> 
									<span class="col2">${cities.postalCode}</span>
								</li>					
							</c:forEach>
						</ul>
					</td>
				</tr>
			</c:forEach>			
		</table>
	</div>
</body>
</html>
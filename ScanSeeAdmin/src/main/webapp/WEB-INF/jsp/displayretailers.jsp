<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Retailers</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>

<script type="text/javascript">
			$(document).ready(function(){
			    
			    
				$("input[name='phoneNo']").live('keypress',function(evt) {
					
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode < 31 || charCode == 46)
						return true;
				
					return false;
			   });
				
			   $("input[name='postalCode']").live('keypress',function(evt) {
					
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode < 31 || charCode == 46)
						return true;
				
					return false;
			   });
			   
				$("input[name='latitude']").live('keypress',function(evt) {
					
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
							|| charCode == 45 || charCode == 43)
						return true;
					return false;
				});
				
				$("input[name='longitude']").live('keypress',function(evt) {
					
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
							|| charCode == 45 || charCode == 43)
						return true;
					return false;
				});
		
				$("#scrlGrd select").attr("disabled","disabled");
							$('input[name$="deleteAll"]').click(function() {
								var status = $(this).attr("checked");
								$('input[name$="checkboxDel"]').attr('checked',status);
								$del = $('#delete');
								$del.removeAttr('disabled').addClass('hltBtn');
							
								if(!status) {
								$('input[name$="checkboxDel"]').removeAttr('checked');
								$del.attr('disabled','disabled').removeClass('hltBtn');
								}
							});
							$('input[name$="checkboxDel"]').click(function() {											   
								var tolCnt = $('input[name$="checkboxDel"]').length;	
								var chkCnt = $('input[name$="checkboxDel"]:checked').length;
								if(tolCnt == chkCnt)
									$('input[name$="deleteAll"]').attr('checked','checked');
								else
									$('input[name$="deleteAll"]').removeAttr('checked');
								
								$del = $('#delete');
								if(chkCnt > 0)
								{
									
									$del.removeAttr('disabled').addClass('hltBtn');
								}
								else{
									$del.attr('disabled','disabled').removeClass('hltBtn');
								}
							});	
			
							if(null != "${sessionScope.state}" && null != "${sessionScope.city}")
							{
								loadCity();
							}
							
						
										
				});
			
			function isLatLong(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
						|| charCode == 45 || charCode == 43)
					return true;
				return false;
			}
</script>
<script>
		function loadCity() {
			var stateCode = $('#Country').val();
			
			$.ajaxSetup({cache:false})
			$.ajax({
				type : "GET",
				url : "fetchcity.htm",
				data : {
					'statecode' : stateCode,
					'city' : null
				},

				success : function(response) {
					$('#cityDiv').html(response);
				},
				error : function(e) {
					
				}
			});
		}

			function searchRetailer() 
			{
				document.retailer.retailLowerLimit.value = 0;
				document.retailer.action = "searchretailer.htm"; 
				document.retailer.method = "post";
				document.retailer.submit();
			}
			
			function displayband() 
			{
				document.retailer.isBand.value = true;
				document.retailer.action = "displayretailer.htm"; 
				document.retailer.method = "post";
				document.retailer.submit();
			}			
			function searchRetailerOnKeyPress(event) 
			{
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode == '13') 
				{
					document.retailer.retailLowerLimit.value = 0;
					document.retailer.action = "searchretailer.htm"; 
					document.retailer.method = "post";
					document.retailer.submit();
					
				}
			}

			function deleteRetailers(){				
				var checkedValue = [];
				var i = 0;
				var r;
				chkdBx = $('input[name="checkboxDel"]:checkbox:checked').length;
				if(chkdBx > 0) {
					$('input[name="checkboxDel"]:checked').each(function(){
					 	checkedValue[i] = $(this).val();
						 i++;
			    	});
				}
				if(checkedValue.length == 1)
				{
					r = confirm("Are you sure you want to delete this Retailer ?");
						
				}else
				{
					r = confirm("Are you sure you want to delete these Retailers ?");
				}
				if(r == true){
					document.retailer.retailerIds.value = checkedValue;
					document.retailer.action = "deleteretailer.htm";
					document.retailer.method = "post";
					document.retailer.submit();					
				}
			}
			
			

			function deleteRetailer(retailerId)
			{			
				var r = confirm("Are you sure you want to delete this Retailer ?");
				if(r == true){
					document.retailer.retailerIds.value = retailerId;
					document.retailer.action = "deleteretailer.htm";
					document.retailer.method = "post";
					document.retailer.submit();	
				}			
			}

			function displayLocation(retailerId, retailerName)
			{				
				document.retailer.retailId.value = retailerId;
				document.retailer.retailName.value = retailerName;
				document.retailer.action = "displayretailerLocation.htm";
				document.retailer.method = "post";
				document.retailer.submit();
				
			}
			
			function showRetailBusCategory(retailerId) {
				openIframePopup('ifrmPopup', 'ifrm',
						'fetchbuscategory.htm?retailId='+ retailerId, 226,
						400, 'Business Category');
			}
			
			function retailerCreateLogin(isLoginExist, retailerId)	{
				document.retailer.isLoginExist.value = isLoginExist;	
				document.retailer.retailId.value = retailerId;
				document.retailer.action = "retailerCreateLogin.htm";
				document.retailer.method = "post";
				document.retailer.submit();
			}
			
			function makeItPaidOrUnpaid(retailID) {
				var isPaid = $("#paid-" + retailID).attr("isPaid");
				var pay ;
				var r;
				if(isPaid == "true") {
					r = confirm("Are you sure you want to make this retailer as 'Unpaid Retailer' !");
					pay = "false";
				} else {
					r = confirm("Are you sure you want to make this retailer as 'Paid Retailer' !");
					pay = "true";
				}
				
				if(r == true) {
					$.ajaxSetup({cache:false})
					$.ajax({
						type : "GET",
						url : "saveRetPayInfo.htm",
						data : {
							'retailID' : retailID,
							'isPaid' : pay
						},

						success : function(response) {
							if(isPaid == "true") {
								$("#paid-" + retailID).attr("isPaid", pay);
								$("#paid-" + retailID).val("Unpaid");
							} else {
								$("#paid-" + retailID).attr("isPaid", pay);
								$("#paid-" + retailID).val("Paid");
							}							
						},
						error : function(e) {
							alert("error updating");
						}
					});
				}
			}
				

		</script>
</head>
<body onload="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-rtlr.png" alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left" title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a></li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<!--
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
		-->
			<div id="subnav" class="tabdSec">
				<ul>
					<li><a class="active"><span>Retailer Administration</span></a></li>
					<li><a onclick="displayband()" href ="#"><span>Band Administration</span></a></li>
				</ul>
			</div>
			<div class="roleRtContPnl">
				<div class="contBlock">
					<form:form name="retailer" commandName="retailer" acceptCharset="ISO-8859-1">
						<form:hidden path="retailerIds" />

						<form:hidden path="retailId" />
						<form:hidden path="retailName" />
						<form:hidden path="retailInfoJson" />
						<form:hidden path="retailLowerLimit" />
						<form:hidden path="isLoginExist" />
						<form:hidden path="isBand"/>
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />

						<fieldset>

							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
									<tr>
										<td class="Label" width="20%"><label for="Supr_srchRbts">Retailer Name</label></td>
										<td colspan="3"><c:if test="${sessionScope.searchKey ne null && !empty sessionScope.searchKey}">
												<form:input path="searchKey" value="${sessionScope.searchKey}" onkeypress="searchRetailerOnKeyPress(event)" />
											</c:if> <c:if test="${sessionScope.searchKey eq null || empty sessionScope.searchKey}">
												<form:input path="searchKey" onkeypress="searchRetailerOnKeyPress(event)" />
											</c:if> <label></label> <a href="#"> <img src="images/searchIcon.png" alt="Search" width="20" height="17" title="Search"
												onclick="searchRetailer()" />
										</a></td>
									</tr>
									<tr>
										<td class="Label">State</td>
										<td width="29%"><select name="state" id="Country" class="textboxBig" onchange="loadCity();">
												<option value="">--Select--</option>
												<c:forEach items="${sessionScope.statesListCreat}" var="s">
													<c:if test="${sessionScope.state ne s.stateabbr}">
														<option value="${s.stateabbr}">
															<c:out value="${s.stateName}"></c:out>
														</option>
													</c:if>
													<c:if test="${sessionScope.state eq s.stateabbr}">
														<option value="${s.stateabbr}" selected="selected">
															<c:out value="${s.stateName}"></c:out>
														</option>
													</c:if>
												</c:forEach>
										</select></td>
										<td width="10%">City</td>
										<td width="41%">
											<div id="cityDiv">
												<select name="city" id="city" class="textboxBig">
													<option value="">--Select--</option>
												</select>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</fieldset>
					</form:form>
				</div>
				<center>
					<span class="highLightErr" style="font: red;"> <c:out value="${requestScope.message}" />
					</span> <span class="highLightErr" style="font: red;"> <c:out value="${requestScope.errorMessage}" />
					</span> <span class="highLightMsg"> <c:out value="${requestScope.retUpdateMessage}" />
					</span>

				</center>

				<fieldset class="relative">
					<div class="overlay"></div>
					<legend>Retailers</legend>
					<div id="roleScrlGrd" class="grdCont searchGrd roleHrzScrl">
						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0" cellpadding="0" width="296%">
							<tbody>
								<tr class="header">
									<td width="" align="center"><c:if test="${!empty sessionScope.RetailerList}">
											<label><input type="checkbox" name="deleteAll" /> </label>
										</c:if></td>
									<td width="">Retailer ID</td>
									<td width="" onclick="sortColumn('RetailerName')">
										<div class="txtwrap">
											<a href="#" title="Retailer Name"> <label class="mandTxt">Retailer Name</label> <c:if test="${!empty sessionScope.RetailerNameImg}">
													<img src="${sessionScope.RetailerNameImg}" width="11" height="6" />
												</c:if>
											</a>
										</div>
									</td>
									<td width="">No of Locations</td>
									<td width=""><label class="mandTxt">Address</label></td>
									<td width=""><label class="mandTxt">State</label></td>
									<td width=""><label class="mandTxt">City</label></td>
									<td width=""><label class="mandTxt">Postal Code</label></td>
									<td width="">Website URL</td>
									<td width=""><label class="mandTxt">Latitude</label></td>
									<td width=""><label class="mandTxt">Longitude</label></td>
									<td width=""><label class="mandTxt">Phone No</label></td>
									<td width="5%">Paid / Unpaid</td>
									<td width="6%">Business Category</td>
									<td width="270" align="center">Action</td>

								</tr>
								<c:forEach items="${sessionScope.RetailerList}" var="item">

									<tr id="retInfo">
										<td align="center" class="mand"><input type="checkbox" name="checkboxDel" value="${item.retailId}" /></td>

										<td align="left"><c:out value="${item.retailId}" /></td>

										<td align="left" class="editCol"><a href="#" title="View Locations"
											onclick='displayLocation("${item.retailId}","<c:out value="${item.retailName}"/>")'> <c:out value="${item.retailName}"
													escapeXml="false" />
										</a></td>
										<td align="left"><c:out value="${item.numberOfLocations}" /></td>
										<td class="editCol"><c:out value="${item.address1}" /></td>

										<td class="editSlct"><c:out value="${item.state}" /></td>

										<td class="editSlct"><c:out value="${item.city}" /></td>
										<td class="editCol"><c:out value="${item.postalCode}" /></td>
										<td class="editCol"><c:out value="${item.retUrl}" /></td>
										<td class="editCol latChk"><c:out value="${item.latitude}" /></td>

										<td class="editCol lonChk"><c:out value="${item.longitude}" /></td>

										<td class="editCol"><c:out value="${item.phoneNo}" /></td>
										<td align="center"><c:choose>
												<c:when test="${item.isPaid ne null && item.isPaid ne false}">
													<input type="button" class="cstmbtn" id="paid-${item.retailId}" value="Paid" onclick="makeItPaidOrUnpaid(${item.retailId});"
														isPaid="${item.isPaid}" name="isPaid" />
												</c:when>
												<c:otherwise>
													<input type="button" class="cstmbtn" id="paid-${item.retailId}" value="Unpaid" onclick="makeItPaidOrUnpaid(${item.retailId});"
														isPaid="${item.isPaid}" name="isPaid" />
												</c:otherwise>
											</c:choose></td>
										<td align="center"><input type="button" value="Edit Category" class="btn tgl-overlay"
											onclick="showRetailBusCategory('<c:out value="${item.retailId}"/>')" title="Edit Business Category" /></td>
										<td align="center"><input type="button" class="btn row-edit" value="Edit" /> <input type="button" class="btn row-save" value="Save" />
											<input type="button" class="btn row-del" value="Delete" onclick="deleteRetailer('${item.retailId}')" /> <c:choose>
												<c:when test="${item.isLoginExist ne null && item.isLoginExist != 0}">
													<input type="button" class="cstmbtn" value="Edit Login" onclick="retailerCreateLogin('${item.isLoginExist}', '${item.retailId}')" />
												</c:when>
												<c:otherwise>
													<input type="button" class="cstmbtn" value="Create Login" onclick="retailerCreateLogin('${item.isLoginExist}', '${item.retailId}')" />
												</c:otherwise>
											</c:choose></td>
									</tr>

								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="pagination">
						<p>
							<page:pageTag currentPage="${sessionScope.pagination.currentPage}" nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
				<div class="mrgnTop" align="right">
					<input type="button" class="btn" id="delete" value="Delete" title="Delete" onclick="deleteRetailers();" disabled="disabled" />
				</div>
			</div>

			<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color: White">
				</iframe>
			</div>

		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti. All rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
	</div>
	<script>
	
		$(".latChk").focusout(function() {
			/* Matches	 90.0,-90.9,1.0,-23.343342
			Non-Matches	 90, 91.0, -945.0,-90.3422309*/
				var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
						 // var vLat = this.value;
						 var vLat = $(this).parents('tr.curRow').find('input[name="latitude"]').val();
						  //Validate for Latitude.
						  if (0 === vLat.length || !vLat || vLat == "") {
							return false;
						 } else {
							if(!vLatLngVal.test(vLat)) {
						      alert("Latitude are not correctly typed");
						      $(this).parents('tr.curRow').find('input[name="latitude"]').val("");
						      return false;
						  }
						 }
			});

		$(".lonChk").focusout(function() {
			/* Matches	180.0, -180.0, 98.092391
			Non-Matches	181, 180, -98.0923913*/
			var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
				//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
						  var vLong = $(this).parents('tr.curRow').find('input[name="longitude"]').val();
						  //Validate for Longitude.
						  if (0 === vLong.length || !vLong || vLong == "") {
							return false;
						 } else {
							if(!vLatLngVal.test(vLong)) {
						      alert("Longitude are not correctly typed");
						      $(this).parents('tr.curRow').find('input[name="longitude"]').val("");
						      return false;
						  }
						 }
			});
		</script>

</body>



</html>
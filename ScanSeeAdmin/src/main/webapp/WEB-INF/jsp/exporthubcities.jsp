<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Export HubCities</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">

	function displayExport(moduleId, moduleName) {
		document.exporthubcities.moduleTypeId.value = moduleId;
		document.exporthubcities.moduleTypeName.value = moduleName;
		document.exporthubcities.action = "displayexport.htm";
		document.exporthubcities.method = "GET";
		document.exporthubcities.submit();
	}
	
	function searchHubCitiRegEvt(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {			
			document.exporthubcities.lowerLimit.value = 0;
			document.exporthubcities.action = "displayexporthubcities.htm";
			document.exporthubcities.method = "post";
			document.exporthubcities.submit();

		}
	}
	 
	function searchHubCitiReg() {		
		document.exporthubcities.lowerLimit.value = 0;
		document.exporthubcities.action = "displayexporthubcities.htm";
		document.exporthubcities.method = "post";
		document.exporthubcities.submit();
	}
	
	function SaveExportHubCities() {
		var unchkd = $.makeArray($('input[name="selRegHubCitiIds"]:checkbox:not(:checked)')
		             	.map(function() { return $(this).val(); 
		             }));
		
		document.exporthubcities.unChkdNatId.value = unchkd;
		document.exporthubcities.lowerLimit.value = ${requestScope.lowerLimit};
		document.exporthubcities.action = "saveexporthubcities.htm";
		document.exporthubcities.method = "POST";
		document.exporthubcities.submit();
	}
	
	function callNextPage(pagenumber, url) {
		
		var res = confirm("Do you want to save this page changes ?");
		if(res) {
			var unchkd = $.makeArray($('input[name="selRegHubCitiIds"]:checkbox:not(:checked)')
			             	.map(function() { return $(this).val(); 
			             }));
			
			document.exporthubcities.unChkdNatId.value = unchkd;
			document.exporthubcities.isDataExport.value = true;
		} else {
			document.exporthubcities.isDataExport.value = false;
		}
		document.exporthubcities.lowerLimit.value = 0;
		document.exporthubcities.pageNumber.value = pagenumber;
		document.exporthubcities.pageFlag.value = "true";
		document.exporthubcities.action = url;
		document.exporthubcities.method = "post";
		document.exporthubcities.submit();
	}
	
</script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hubciti.png" alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left" title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a></li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<form:form name="exporthubcities" commandName="exporthubcities">
				<input type="hidden" name="pageNumber" />
				<input type="hidden" name="pageFlag" />
				<form:hidden path="lowerLimit" />
				<form:hidden path="moduleTypeId" />
				<form:hidden path="moduleTypeName" />
				<form:hidden path="unChkdNatId" />
				<form:hidden path="isDataExport"/>
				
				<div class="floatL" id="vNav">
					<div class="vNavtopBg"></div>
					<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
					<div class="vNavbtmBg"></div>
				</div>
				<div class="rtContPnl floatR">
					<div id="subnav" class="tabdSec">
						<ul>
							<c:forEach items="${sessionScope.modules}" var="module">
								<li><a href="#" id="mod-${module.moduleTypeId}" onclick="displayExport('${module.moduleTypeId}', '${module.moduleTypeName}')"><span>${module.moduleTypeName}</span></a></li>
							</c:forEach>
							<li><a href="#" class="active"><span>Export HubCities</span></a></li>
						</ul>
					</div>
					<div class="contBlock mrgnTop">
						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
									<tr>
										<td colspan="3" class="header">Search</td>
									</tr>
									<tr>
										<td class="Label" width="30%"><label for="hubCitiName">HubCiti / Region Name</label></td>
										<td width="70%"><form:input type="text" path="searchKey" id="searchKey" onkeypress="searchHubCitiRegEvt(event)" maxlength="30" /> &nbsp;
											<label></label> <a href="#"><img src="images/searchIcon.png" alt="Search" width="20" height="17" onclick="searchHubCitiReg()"
												title="Search HubCiti / Region"
											/></a></td>
									</tr>
								</table>
							</div>
						</fieldset>
					</div>
					<center>
						<c:if test="${requestScope.errorMessage ne null && !empty requestScope.errorMessage}">
							<span class="highLightErr"><c:out value="${requestScope.errorMessage}"></c:out></span>
						</c:if>
						<c:if test="${requestScope.successMsg ne null && !empty requestScope.successMsg}">
							<span class="highLightTxt"><c:out value="${requestScope.successMsg}" /> </span>
						</c:if>
					</center>
					<fieldset>
						<legend>Export HubCiti / Region</legend>
						<div id="scrlGrd" class="grdCont searchGrd">
							<table id="cpnLst" class="stripeMe" border="0" cellspacing="0" cellpadding="0" width="100%">
								<tbody>
									<tr class="header">
										<td width="88%">HubCiti / Region Name</td>
										<td align="center">Export HubCiti</td>
										<c:if test="${sessionScope.hubcities ne null && !empty sessionScope.hubcities}">
											<c:forEach items="${sessionScope.hubcities}" var="citi">
												<tr>
													<td align="left">${citi.hubCitiName}</td>
													<c:choose>
														<c:when test="${citi.isDataExport eq true}">
															<td width="12%" align="center"><form:checkbox path="selRegHubCitiIds" checked="checked" value="${citi.hubCitiID}" /></td>
														</c:when>
														<c:otherwise>
															<td width="12%" align="center"><form:checkbox path="selRegHubCitiIds" value="${citi.hubCitiID}" /></td>
														</c:otherwise>
													</c:choose>
												</tr>
											</c:forEach>
										</c:if>
								</tbody>
							</table>
						</div>
						<c:if test="${sessionScope.hubcities ne null && !empty sessionScope.hubcities}">
							<div class="pagination">
								<p>
									<page:pageTag totalSize="${sessionScope.pagination.totalSize}" nextPage="4" url="${sessionScope.pagination.url}"
										currentPage="${sessionScope.pagination.currentPage}" pageRange="${sessionScope.pagination.pageRange}"
									/>
								</p>
							</div>
						</c:if>
					</fieldset>
					<div align="right" class="mrgnTop">
						<input type="button" value="Save" class="btn" title="Save Export HubCities" onclick="SaveExportHubCities()" />
					</div>
				</div>
			</form:form>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>
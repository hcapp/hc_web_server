<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="java.util.ArrayList,common.pojo.Product"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	$('#bCategoryIds option').click(function() {		
		var alertFlag = false;
		var subCatFlag = false;
		$("#bCategoryIds option").each(function() {
			var filterAssociation = $(this).attr("filterAssocaited");
			var subCatAssociated = $(this).attr("subCatAssociated");
			if(filterAssociation == "true" && subCatAssociated == "true" && !($(this).is(":selected"))) {
				alertFlag = true;
				subCatFlag = true;
				$(this).attr("selected", "selected");
			} else if(filterAssociation == "true" && !($(this).is(":selected"))) {
				alertFlag = true;
				$(this).attr("selected", "selected");
			} else if(subCatAssociated == "true" && !($(this).is(":selected"))) {
				subCatFlag = true;
				$(this).attr("selected", "selected");
			}			
			
		});
		
		if(alertFlag == true && subCatFlag == true) {
			alert("Filters and SubCategories has been associated to the categories. Please de-associate both and continue.");
		} else if(alertFlag == true) {
			alert("Filters has been associated to the categories. Please de-associate Filters and continue.");
		} else if(subCatFlag == true) {
			alert("SubCategories has been associated to the categories. Please de-associate SubCategories and continue.");
		}
		
		var totOpt = $('#bCategoryIds option').length;
		var totOptSlctd = $('#bCategoryIds option:selected').length;
		if (totOpt == totOptSlctd) {
			$('input[name$="chkAllCatgy"]').attr('checked', 'true');
		} else {
			$('input[name$="chkAllCatgy"]').removeAttr('checked');
		}
	});
});



	function editBusCategory() {
		/*var actCnt = $("#bCategoryIds option:selected").length;
		if(actCnt==0){
			$("#alertMsg").text("Please Select Business categories and then continue to save");
		}else{
			$("#alertMsg").text("");*/
			document.busCategory.action = "editbuscategory.htm";
			document.busCategory.method = "post";
			document.busCategory.submit();
		/*}*/
	
	}
	
	function SelectAllBusCategory(checked) {
		if (checked == true) {
			$("#bCategoryIds option").each(function() {
				$(this).attr("selected", "selected");
			});
		} else {			
			var alertFlag = false;
			var subCatFlag = false;
			$("#bCategoryIds option").each(function() {
				var filterAssociation = $(this).attr("filterAssocaited");
				var subCatAssociated = $(this).attr("subCatAssociated");
				if(filterAssociation == "true") {
					alertFlag = true;
					$(this).attr("selected", "selected");
				} else {
					$(this).attr("selected", false);
				}	
				if(subCatAssociated == "true") {
					subCatFlag = true;
					$(this).attr("selected", "selected");
				} else {
					$(this).attr("selected", false);
				}	
			});
			
			if(alertFlag == true && subCatFlag == true) {
				alert("Filters and SubCategories has been associated to the categories. Please de-associate both and continue.");
			} else if(alertFlag == true) {
				alert("Filters has been associated to the categories. Please de-associate Filters and continue.");
			} else if(subCatFlag == true) {
				alert("SubCategories has been associated to the categories. Please de-associate SubCategories and continue.");
			}
		}
	}
	
	
	function onLoadCategoryIDs() {
		var actCnt = $("#bCategoryIds option").length;
		var slcCnt = $("#bCategoryIds option:selected").length;
		if (actCnt == slcCnt) {
			$('input[name$="chkAllCatgy"]').attr('checked', 'true');
		} else {
			$('input[name$="chkAllCatgy"]').removeAttr('checked');
		}
	}
</script>

</head>
<body class="whiteBG">
	<div class="contBlock searchGrd relative">
		<form:form commandName="busCategory" name="busCategory">
			<form:hidden path="retailId"/>
			<center>
					<span class="highLightMsg" > <c:out
							value="${requestScope.successMessage}" /> </span> 
							<!-- <span class="highLightErr"
						style="font: red;" id="alertMsg">  --><span class="highLightErr"
						style="font: red;"> <c:out
							value="${requestScope.errorMessage}" /> </span>
			</center>
		<fieldset>
			<div class="contWrap">
				<table width="100%" cellspacing="0" cellpadding="0" border="0"
					class="grdTbl">
					<tbody>
						<tr>
							<td width="35%" class="Label"><label for="ctgry">Category</label><p class="labelInfoTxt">(Hold CTRL to select
									multiple categories)</p></td>
							<td width="29%"><form:select path="bCategoryIds" id="bCategoryIds" class="txtAreaBox" size="1" multiple="true">
									<c:forEach items="${sessionScope.busCategory}" var="c">
										<c:choose>
											<c:when test="${c.associated == 0}">
												<form:option value="${c.categoryId}" label="${c.categoryName}" filterAssocaited="${c.filterAssociated}" subCatAssociated="${c.subCatAssociated}"/>
											</c:when>
										<c:otherwise>
											<form:option value="${c.categoryId}" label="${c.categoryName}"  selected="selected" filterAssocaited="${c.filterAssociated}" subCatAssociated="${c.subCatAssociated}"/>
										</c:otherwise>
										</c:choose>
									</c:forEach>
								</form:select></td>
								<td colspan="2" align="left" valign="top" class="Label"><label>
									<input type="checkbox" name="chkAllCatgy" id="chkAllCatgy"
									onclick="SelectAllBusCategory(this.checked);"/></label>
									<p class="labelInfoTxt">Select All Category  </p><br> <br></td>
						</tr>
					</tbody>
				</table>
				<div class="navTabSec mrgnRt" align="right">
					<input name="Save" title="Save" value="Save" type="button" class="btn" id="save"  onclick="editBusCategory();" />
					<input name="Cancel" title="Click here to Close" value="Cancel" type="button" class="btn" id="Cancel"  onclick="javascript:closeIframePopup('ifrmPopup','ifrm')" />
				</div>
			</div>
		</fieldset>
		</form:form>
	</div>
	<script>
	onLoadCategoryIDs();
</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Session Expire</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript">
	function submitForm() {
		document.login.action = "login.htm";
		document.login.method = "get";
		document.login.submit();
	}
</script>

</head>
<body>

<div id="wrapper" style="min-height:576px!important;">
  <div id="header">
   
   <div id="header_logo" class="floatL"> <a href="#"> <img src="images/scansee-logo.png" alt="ScanSee" width="400" height="54"/></a> </div>
  </div>
  <div class="clear"></div>
  
  
 <form:form commandName="login" name="login"> 
  <div id="content" class="PageNf" style="height : 234px;">
		<h1 align="center">Your session has been expired.</h1>
		<br />
		<br />
	
		<h4 align="center"><u><a href="#" onclick="submitForm();">Please Login Again</a></u></h4>
		
  </div>
</form:form>   
</div>
</body>
</html>
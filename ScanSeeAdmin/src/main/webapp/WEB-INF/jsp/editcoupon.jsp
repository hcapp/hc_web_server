<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="java.util.ArrayList"%>
<%@page session="true"%>
<%@ page import="java.util.Date"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Coupon</title>
<link rel="stylesheet" type="text/css"	href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet"	type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('input#dtp').datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonImage : 'images/cal.gif'
		});
		$('.ui-datepicker-trigger').css("padding-left", "5px");
	
		var tit = document.coupon.productUpcImg;
		var fit = document.coupon.productNameHidden.value;
		$('#productUpcImg').attr('title', fit);
	
		$("input#endDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonImage : 'images/cal.gif'
		});
		$('.ui-datepicker-trigger').css("padding-left", "5px");

		
		$('.txtAreaSmall').val(function(i,v){     
			return v.replace(/^\s+|\s+$/g,""); 
		});
		
		});	
	</script>
	<script type="text/javascript">
	function submitFormEdit(couponId) {
		var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
		var upc = document.coupon.productUpc.value;
		var name = document.coupon.couponName.value;
		var value = document.coupon.couponValue.value;
		var startDate = document.coupon.couponStartDate.value;
		var endDate = document.coupon.couponExpireDate.value;
		var couponDiscountPct = document.coupon.couponDiscountPct.value;
		var url = document.coupon.couponUrl.value;
		if (name == null || name == "" || name == "null") {
			alert('Please enter valid Coupon Name');
		} else if (isNaN(value)) {
			alert('Please enter valid Coupon Face Value');
		} else if (isNaN(couponDiscountPct)) {
			alert('Please enter valid discount Value');
		} else if (url != "" && url != null) {
			if (!regexp.test(url)) {
				alert("Please enter a valid URL.");
			} else {
				if (startDate == null || startDate == "" || startDate == "null") {
					alert('Please enter valid Start Date');
				} else if (endDate == null || endDate == ""
						|| endDate == "null") {
					alert('Please enter valid End Date');
				} else if (compareTodayDate(endDate) == false) {
					alert('End Date should be greater than or equal to Current date');
				} else if (!checkDate(endDate)) {
					alert('Please enter valid End Date');
				} else {
					if (document.coupon.externalCoupon.checked) {
						document.coupon.hiddenExternalCoupon.value = '1';
					} else {
						document.coupon.hiddenExternalCoupon.value = '0';
					}
					if (document.coupon.viewableOnWeb.checked) {
						document.coupon.hiddenViewable.value = '1';
					} else {
						document.coupon.hiddenViewable.value = '0';
					}
					if (!checkDate(startDate)) {
						alert('Please enter valid Start Date');
					}
					if (compareTodayDate(endDate) == false) {
						alert('End Date should be greater than or equal to Current date');
					} else if (!compareCurrentDate(startDate, endDate)) {
						alert('End Date should be greater than or equal to Start Date');
					}
					document.coupon.couponId.value = document.coupon.couponId.value;
					document.coupon.action = "savecoupon.htm";
					document.coupon.method = "post";
					document.coupon.submit();
				}
			}
		} else if (startDate == null || startDate == "" || startDate == "null") {
			alert('Please enter valid Start Date');
		} else if (endDate == null || endDate == "" || endDate == "null") {
			alert('Please enter valid End Date');
		} else if (!checkDate(startDate)) {
			alert('Please enter valid Start Date');
		} else if (compareTodayDate(endDate) == false) {
			alert('End Date should be greater than or equal to Current date');
		} else if (!compareCurrentDate(startDate, endDate)) {
			alert('End Date should be greater than or equal to Start Date');
		} else {
			if (document.coupon.externalCoupon.checked) {
				document.coupon.hiddenExternalCoupon.value = '1';
			} else {
				document.coupon.hiddenExternalCoupon.value = '0';
			}
			if (document.coupon.viewableOnWeb.checked) {
				document.coupon.hiddenViewable.value = '1';
			} else {
				document.coupon.hiddenViewable.value = '0';
			}
			document.coupon.couponId.value = document.coupon.couponId.value;
			document.coupon.action = "savecoupon.htm";
			document.coupon.method = "post";
			document.coupon.submit();
		}
	}
	
	function submitForm(couponId) {
		var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
		var upc = document.coupon.productUpc.value;
		var name = document.coupon.couponName.value;
		var value = document.coupon.couponValue.value;
		var startDate = document.coupon.couponStartDate.value;
		var endDate = document.coupon.couponExpireDate.value;
		var couponDiscountPct = document.coupon.couponDiscountPct.value;
		var url = document.coupon.couponUrl.value;
		if (name == null || name == "" || name == "null") {
			alert('Please enter valid Coupon Name');
		} else if (isNaN(value)) {
			alert('Please enter valid Coupon Face Value');
		} else if (isNaN(couponDiscountPct)) {
			alert('Please enter valid discount Value');
		} else if (url != "" && url != null) {
			if (!regexp.test(url)) {
				alert("Please enter a valid URL.");
			} else {
				if (startDate == null || startDate == "" || startDate == "null") {
					alert('Please enter valid Start Date');
				} else if (endDate == null || endDate == ""
						|| endDate == "null") {
					alert('Please enter valid End Date');
				} else if (compareTodayDate(startDate) == false) {
					alert('Start Date should be greater than or equal to Current date');
				} else if (!checkDate(endDate)) {
					alert('Please enter valid End Date');
				} else if (!compareCurrentDate(startDate, endDate)) {
					alert('End Date should be greater than or equal to Start Date');
				} else {
					if (document.coupon.externalCoupon.checked) {
						document.coupon.hiddenExternalCoupon.value = '1';
					} else {
						document.coupon.hiddenExternalCoupon.value = '0';
					}
					if (document.coupon.viewableOnWeb.checked) {
						document.coupon.hiddenViewable.value = '1';
					} else {
						document.coupon.hiddenViewable.value = '0';
					}
					document.coupon.couponId.value = document.coupon.couponId.value;
					document.coupon.action = "savecoupon.htm";
					document.coupon.method = "post";
					document.coupon.submit();
				}
			}
		} else if (startDate == null || startDate == "" || startDate == "null") {
			alert('Please enter valid Start Date');
		} else if (endDate == null || endDate == "" || endDate == "null") {
			alert('Please enter valid End Date');
		} else if (compareTodayDate(startDate) == false) {
			alert('Start Date should be greater than or equal to Current date');
		} else if (!checkDate(endDate)) {
			alert('Please enter valid End Date');
		} else if (!compareCurrentDate(startDate, endDate)) {
			alert('End Date should be greater than or equal to Start Date');
		} else {
			if (document.coupon.externalCoupon.checked) {
				document.coupon.hiddenExternalCoupon.value = '1';
			} else {
				document.coupon.hiddenExternalCoupon.value = '0';
			}
			if (document.coupon.viewableOnWeb.checked) {
				document.coupon.hiddenViewable.value = '1';
			} else {
				document.coupon.hiddenViewable.value = '0';
			}
			document.coupon.couponId.value = document.coupon.couponId.value;
			document.coupon.action = "savecoupon.htm";
			document.coupon.method = "post";
			document.coupon.submit();
		}
	}

	function clearForm() {
		var r = confirm("Do you want to clear this form?");
		if (r == true) {
			document.coupon.action = "addcoupon.htm";
			document.coupon.method = "post";
			document.coupon.submit();
		}
	}

	function checkDate(value) {
		if (isDate(value)) {
			return true;
		} else {
			return false;
		}
	}
	
	function isDate(txtDate) {
		var objDate, // date object initialized from the txtDate string
		mSeconds, // txtDate in milliseconds
		day, // day
		month, // month
		year; // year
		// date length should be 10 characters (no more no less)
		if (txtDate.length !== 10) {
			return false;
		}
		// third and sixth character should be '/'
		if (txtDate.substring(2, 3) !== '/' || txtDate.substring(5, 6) !== '/') {
			return false;
		}
		// extract month, day and year from the txtDate (expected format is mm/dd/yyyy)
		// subtraction will cast variables to integer implicitly (needed
		// for !== comparing)
		month = txtDate.substring(0, 2) - 1; // because months in JS start from 0
		day = txtDate.substring(3, 5) - 0;
		year = txtDate.substring(6, 10) - 0;
		// convert txtDate to milliseconds
		mSeconds = (new Date(year, month, day)).getTime();
		// initialize Date() object from calculated milliseconds
		objDate = new Date();
		objDate.setTime(mSeconds);
		// compare input date and parts from Date() object
		// if difference exists then date isn't valid
		if (objDate.getFullYear() !== year || objDate.getMonth() !== month
				|| objDate.getDate() !== day) {
			return false;
		}
		// otherwise return true
		return true;
	}

	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31)
			return true;
		return false;
	}

	function compareCurrentDate(startDate, endDate) {
		var startmiliSeconds, // startDate in milliseconds
		endmiliSeconds, // endDate in milliseconds
		sday, // Startday
		smonth, // startmonth
		syear, // startyear
		eday, // endday  
		emonth, // endmonth
		eyear; // endyear
		smonth = startDate.substring(0, 2) - 0;
		sday = startDate.substring(3, 5) - 0;
		syear = startDate.substring(6, 10) - 0;
		emonth = endDate.substring(0, 2) - 0;
		eday = endDate.substring(3, 5) - 0;
		eyear = endDate.substring(6, 10) - 0;
		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth() + 1; //months are zero based
		var curr_year = d.getFullYear();
		var cdate = new Date(curr_year, curr_month, curr_date);
		var sdate = new Date(syear, smonth, sday);
		var edate = new Date(eyear, emonth, eday);
		if (edate >= curr_date && edate >= sdate) {
			return true;
		} else {
			return false;
		}
	}

	function compareTodayDate(startDate) {
		var startmiliSeconds, // startDate in milliseconds
		sday, // Startday
		smonth, // startmonth
		syear, // startyear
		smonth = startDate.substring(0, 2) - 0;
		sday = startDate.substring(3, 5) - 0;
		syear = startDate.substring(6, 10) - 0;
		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth() + 1; //months are zero based
		var curr_year = d.getFullYear();
		var sdate = new Date(syear, smonth, sday);
		var cdate = new Date(curr_year, curr_month, curr_date);
		if (sdate >= cdate) {
			return true;
		}
		return false;
	}

	function backToMain() {
		var r = confirm("Do you want to leave this page without saving the coupon?");
		if (r == true) {
			document.coupon.action = "showexpiredcoupon.htm";
			document.coupon.method = "post";
			document.coupon.submit();
		}
	}

	function openAddEditProd() {
		var hiddnId = document.coupon.productIDHidden.value;
		openIframePopup('ifrmPopup', 'ifrm',
				'getassociatedproducts.htm?productIDHidden=' + hiddnId, 500,
				800, 'View Product/UPC');
	}

	function clearProducts() {
		var prod = document.coupon.productUpc.value;
		if (prod != '') {
			var r = confirm("Do you want to Deassociate all products?");
			if (r == true) {
				document.coupon.productUpc.value = "";
				document.coupon.productIDHidden.value = null;
				document.coupon.productNameHidden.value = null;
				var tit = document.coupon.productUpcImg;
				$('#productUpcImg').attr('title', '');
			}
		}
	}
	
</script>
</head>
<body onload="resizeDoc();">
	<div id="wrapper">
		<form:form commandName="coupon" name="coupon">
			<div id="header">
				<div id="header_logo" class="floatL">
					<a href="#"> <img src="images/scansee-logo.png" alt="ScanSee"
						width="400" height="54" /> </a>
				</div>
				<div id="header_link" class="floatR">
					<ul>
						<li>Welcome <span class="highLightTxt"><c:out
									value="${sessionScope.userName}" />, </span>
						</li>
						<li><img src="images/logoutIcon.gif" alt="Logout"
							title="Logout" align="left" /> <a href="logout.htm"
							title="Logut"> &nbsp;Logout</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="content">
				<div class="contBlock">
					<fieldset>
						<c:choose>
							<c:when test="${requestScope.EDIT eq 'editcoupon'}">
								<legend>Edit Coupon Details</legend>
							</c:when>
							<c:otherwise>
								<legend>Add Coupon Details</legend>
							</c:otherwise>
						</c:choose>
						<div class="contWrap">
							<form:hidden path="mode" value="add"/>
							<form:hidden path="showExpire" value="${sessionScope.showExpire}"/>
							<form:hidden path="searchKey" value="${sessionScope.searchKey}"/>
							<form:hidden path="hiddenViewable" />
							<form:hidden path="hiddenExternalCoupon" />
							<form:hidden path="producthidden" />
							<form:hidden path="productIDHidden"
								value="${requestScope.productIDHidden}" />
							<form:hidden path="productNameHidden"
								value="${requestScope.productNameHidden}" />
								
							<c:url value="prodData.do" var="displayURL">
								<c:param name="method" value="prodData" />
								<c:param name="productIDHidden" value="${param.age}" />
							</c:url>
							<c:choose>
								<c:when test="${requestScope.EDIT eq 'editcoupon'}">
									<form:hidden path="couponId" value="${requestScope.COUPON.couponId}"/>
								</c:when>
								<c:otherwise>
									<form:hidden path="couponId" />
								</c:otherwise>
							</c:choose>
							<table width="100%" cellspacing="0" cellpadding="0" border="0"	class="grdTbl">
								<tbody>
									<tr>
										<c:choose>
											<c:when test="${requestScope.EDIT eq 'editcoupon'}">
												<td colspan="4" class="header">Edit Coupon</td>
											</c:when>
											<c:otherwise>
												<td colspan="4" class="header">Create New Coupon</td>
											</c:otherwise>
										</c:choose>
									</tr>
									<%
										String dateError = (String) request.getAttribute("dateMsg");
											if (dateError != null) {
									%>
									<h4>Please Enter valid date</h4>
									<%
										}
									%>
									<tr>
										<td width="18%" class="Label"><label for="couponName">Product
												Name </label>
										</td>
										<td>
											<form:input path="productUpc" value="${requestScope.COUPON.productNames}" readonly="true"/>&nbsp;
											<a href="#">
												<img src="images/searchIcon.png" class="cursorPointer" onclick="openAddEditProd()" title="Click here to add products" /> 
											</a> 
											<img src="images/infoIcon.png" class="cursorPointer" id="productUpcImg" name="productUpcImg" />
											<img src="images/delicon.png" class="cursorPointer" title="DeAssociate All Products" id="productClear" 
												name="productClear"	onclick="javascript:clearProducts()" />
										</td>
										<td class="Label">
											<label for="couponAmt" class="mandTxt">
												Coupon Name 
											</label>
										</td>
										<td>
											<form:input path="couponName" cssClass="couponAmt" value="${requestScope.COUPON.couponName}"/>
										</td>
									</tr>
									<tr>
										<td class="Label">
											<label for="numofCpn">Coupon Face Value $</label>
										</td>
										<td>
											<form:input path="couponValue" value="${requestScope.COUPON.couponDiscountAmount}" onkeypress="return isNumberKey(event)"/>
										</td>
										<td>
											Coupon Description
										</td>
										<td>
										
										<spring:bind path="coupon.couponShortDescription">
											<TEXTAREA name="${status.expression }" class="txtAreaSmall" >
												<c:out value="${requestScope.COUPON.couponShortDescription}"/>	
											</TEXTAREA>
										</spring:bind>
											
										</td>
									</tr>
									<tr>
										<td class="Label" width="17%">
											<label for="numofCpn">
												Discount %
											</label>
										</td>
										<td colspan="3" width="32%" align="left">
											<form:input path="couponDiscountPct" value="${requestScope.COUPON.couponDiscountPct}" onkeypress="return isNumberKey(event)"/>
										</td>
									</tr>
									<tr>
										<td class="Label"><label for="csd">External
												Coupon</label>
										</td>
										<c:if test="${requestScope.COUPON.externalCoupon ne null && !empty requestScope.COUPON.externalCoupon}">
											<td width="23%" align="left" id="externalCoupon">
												<form:checkbox path="externalCoupon" value="${requestScope.COUPON.externalCoupon}"/>&nbsp;
											</td>
										</c:if>
										<c:if test="${requestScope.COUPON.externalCoupon eq null || empty requestScope.COUPON.externalCoupon}">
											<td width="23%" align="left" id="externalCoupon">
												<form:checkbox path="externalCoupon" value="" checked="checked"/>&nbsp;
											</td>
										</c:if>
										<td class="Label"><label for="csd">ViewableOnWeb</label>
										</td>
										<c:if test="${requestScope.COUPON.viewableOnWeb ne null && !empty requestScope.COUPON.viewableOnWeb}">
											<td width="23%" align="left">
												<form:checkbox path="viewableOnWeb" value="${requestScope.COUPON.viewableOnWeb}"/>&nbsp;
											</td>
										</c:if>
										<c:if test="${requestScope.COUPON.viewableOnWeb eq null || empty requestScope.COUPON.viewableOnWeb}">
											<td width="23%" align="left">
												<form:checkbox path="viewableOnWeb" value="" checked="checked"/>&nbsp;
											</td>
										</c:if>
									</tr>
									<tr>
										<td width="17%" class="Label"><label for="ced">URL</label>
										</td>
										<td width="32%" align="left" colspan="3">
											<form:input path="couponUrl" value="${requestScope.COUPON.couponUrl}"/>
										</td>
									</tr>
									<tr>
										<td class="Label">
											<label for="csd" class="mandTxt">
												Coupon Start Date
											</label>
										</td>
										<td width="23%" align="left">
											<form:input path="couponStartDate" size="10" id="dtp" cssClass="textboxDate" 
													value="${requestScope.COUPON.couponStartDate}" name="datepicker1" 
													readonly="true" />&nbsp; (MM/DD/yyyy)
										</td>
										<td width="17%" class="Label">
											<label for="ced" class="mandTxt">
												Coupon End Date
											</label>
										</td>
										<td width="32%" align="left">
											<form:input path="couponExpireDate" size="10" id="endDT" cssClass="textboxDate" 
													value="${requestScope.COUPON.couponExpireDate}" name="endDT" 
													readonly="true" />&nbsp; (MM/DD/yyyy)
										</td>
									</tr>
								</tbody>
							</table>
							<div class="navTabSec mrgnRt">
								<div align="right">
									<c:choose>
										<c:when test="${requestScope.EDIT eq 'editcoupon'}">
											<input type="button" value="save" name="save" title="save" onclick="submitFormEdit()" class="btn hltBtn" /> 
										</c:when>
										<c:otherwise>
											<input type="button" value="save" name="save" onclick="submitForm()" class="btn hltBtn" title="save" />
										</c:otherwise>
									</c:choose>
									<input type="button" value="clear" onclick="clearForm()" class="btn" title="Clear" />
									<input type="button" value="Back" onclick="backToMain()" class="btn" title="Back" />
								</div>
							</div>
						</div>
					</fieldset>
				</div>
				<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
					<div class="headerIframe">
						<img src="images/popupClose.png" class="closeIframe" alt="close"
							onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
							title="Click here to Close" align="middle" /> <span
							id="popupHeader"></span>
					</div>
					<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
						height="100%" allowtransparency="yes" width="100%"
						style="background-color: White"> </iframe>
				</div>
			</div>
			<div id="footer">
				<div id="ourpartners_info">
					<div class="floatR" id="followus_section">
						<p>&nbsp;</p>
					</div>
					<div class="clear"></div>
				</div>
				<div id="footer_nav">
					<div class="clear"></div>
					<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
						rights reserved</div>
					<p align="center">&nbsp;</p>
				</div>
			</div>
		</form:form>
	</div>
</body>
<script>
var viewable = '${requestScope.COUPON.viewableOnWeb}';
		var externalcpn = '${requestScope.COUPON.externalCoupon}';
		if (viewable != '' && viewable != 'null' && viewable != 'undefined') {
			if (viewable == '1') {
				document.coupon.viewableOnWeb.checked = true;
			} else {
				document.coupon.viewableOnWeb.checked = false;
			}
		}
		if (externalcpn != '' && externalcpn != 'null'
				&& externalcpn != 'undefined') {
			if (externalcpn == '1') {
				document.coupon.externalCoupon.checked = true;
			} else {
				document.coupon.externalCoupon.checked = false;
			}
		}
</script>
</html>
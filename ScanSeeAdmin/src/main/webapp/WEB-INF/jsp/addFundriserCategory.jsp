<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Fundraiser Category</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript"
	src="/ScanSeeAdmin/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/ScanSeeAdmin/scripts/colorPickDynamic.js"></script>
<script type="text/javascript"
	src="/ScanSeeAdmin/scripts/colorPicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeAdmin/styles/colorPicker.css" />
<script type="text/javascript">
	$(document).ready(
			function() {
				if (document.FundraiserCategoryForm.catId.value != null
						&& "" != document.FundraiserCategoryForm.catId.value) {
					$("#save").val("Update Category");
					$("#legendValue").text("Edit Category");
					$("title").text("Update Fundraiser Category");
				}

				$(".alrtClose").click(function() {
					$('p.alertErr').hide();
					$('p.alertCatErr').css("display", "none");
				});
				
			});
</script>
<script type="text/javascript">
	
	function checkCateImgValidate(input) {
		var vCateImg = document.getElementById("trgrUpld").value;
		if (vCateImg != '') {
			var checkCateImg = vCateImg.toLowerCase();
			if (!checkCateImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload category image with following extensions :  .png, .gif, .bmp, .jpg, .jpeg");
				document.FundraiserCategoryForm.cateImgPath.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}

	function clearFundraiserCateInfo() {
		document.FundraiserCategoryForm.cateImgName.value = "";
		document.getElementById("ctgryNm").value = "";
		$('#couponImg').attr("src", "/ScanSeeAdmin/images/uploadIcon.png");
		$('#cateImgName').attr("src", "/ScanSeeAdmin/images/uploadIcon.png");
		document.getElementById("ctgryNm").value = "";
		$('p.dupctgry').hide();
		$('p.alertCatErr').hide();
		$('p.alertInfo').hide();
		$('i.emptyctgry').hide();
		$('i.emptycatimg').hide();
		$('#cateErr').hide();
		$('#cateImagePathErr').hide();
	}

	function saveFundraiserCatDetails() {
		var vCatName = document.getElementById("ctgryNm").value;
		var vCatImg = document.FundraiserCategoryForm.cateImgName.value;
		vCatName = $.trim(vCatName);
		
		$('p.dupctgry').hide();
		$('i.emptyctgry').hide();
		$('#cateErr').hide();
		$('p.dupctgry').hide();
		$('i.emptyctgry').hide();
		$('i.emptycatimg').hide();

		if ("" == vCatName && "" == vCatImg || vCatImg == 'undefined'
				|| vCatImg == 'null'
				|| vCatImg == '/ScanSeeAdmin/images/uploadIcon.png') {
			$('i.emptyctgry').css("display", "block");
			$('i.emptycatimg').css("display", "block");
		} else if ("" == vCatName) {
			$('i.emptyctgry').css("display", "block");
		} else if ("" == vCatImg || vCatImg == 'undefined' || vCatImg == 'null'
				|| vCatImg == '/ScanSeeAdmin/images/uploadIcon.png') {
			$('i.emptycatimg').css("display", "block");
		} else {
			document.FundraiserCategoryForm.action = "saveFundraiserCat.htm";
			document.FundraiserCategoryForm.method = "post";
			document.FundraiserCategoryForm.submit();
		}
	}

	function fundraiserCategoryHome() {
		document.FundraiserCategoryForm.action = "fundraiserCategory.htm";
		document.FundraiserCategoryForm.method = "get";
		document.FundraiserCategoryForm.submit();
	}

	function eventCategoryHome() {
		document.FundraiserCategoryForm.action = "eventcategory.htm";
		document.FundraiserCategoryForm.method = "get";
		document.FundraiserCategoryForm.submit();
	}

	function hubcitiAdminHome() {
		document.FundraiserCategoryForm.action = "displayhubcitiadmin.htm";
		document.FundraiserCategoryForm.method = "post";
		document.FundraiserCategoryForm.submit();
	}
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="index.html"> <img src="images/scansee-logo-hubciti.png"
					alt="ScanSee" /></a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<!--Left Navigation Starts here-->
			<div id="vNav" class="floatL">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<!--Left Navigation ends here-->
			<!--Right container starts here-->
			<div class="rtContPnl floatR">
				<div id="subnav" class="tabdSec">
					<ul>
						<!-- <li><a href="#" onclick="hubcitiAdminHome()" class=""
							id="prodDet"><span>HubCiti Administration</span> </a></li> -->
						<li><a class="" href="#" onclick="eventCategoryHome()"
							id="reviewDet" title="Manage Event Category"><span>Manage
									Event Category</span> </a></li>
						<li><a href="#" onclick="fundraiserCategoryHome()"
							title="Manage Fundraiser Category" class="active"><span>Manage
									Fundraiser Category</span></a></li>
					</ul>
				</div>
				<div class="contBlock mrgnTop">
					<fieldset>
						<form:form name="FundraiserCategoryForm"
							commandName="FundraiserCategoryForm"
							enctype="multipart/form-data" action="uploadImage.htm">
							<form:hidden path="cateImgName" id="cateImgName" />
							<form:hidden path="oldCateImgName"/>
							<form:hidden path="catId" />
							<form:hidden path="cateImg" id="cateImg" />
							<%-- <form:hidden path="viewName" value="addFundriserCategory" /> --%>
							<input type="hidden" name="searchKey" />
							<legend id="legendValue">Add Category</legend>
							<c:if
								test="${requestScope.existsMsg ne null && !empty requestScope.existsMsg}">
								<p class="alertCatErr">
									${requestScope.existsMsg} <a class="alrtClose"
										href="javascript:void(0);" title="Close">x</a>
								</p>
							</c:if>
							<div class="contWrap">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<tbody>
										<tr>
											<td width="20%" class="Label"
												style="border-right: 1px solid rgb(218, 218, 218);"><label
												class="mandTxt">Category Name</label></td>
											<td colspan="2"><form:input type="text" id="ctgryNm"
													name="catName" path="catName" class="textboxBig noEnterBtnSubmit"
													maxlength="40" tabindex="1" /> <i class="emptyctgry">Please
													Enter Category Name</i>
												<p class="dupctgry">Category Name already exists</p></td>
										</tr>
										<tr>
											<td class="Label"
												style="border-right: 1px solid rgb(218, 218, 218);">Category
												Image</td>
											<!-- <td width="6%" class="remove-rt-brdr"><img
												src="images/dfltImg.png" width="30" height="30" id="preview" />
											</td> -->
											<td width="74%">
												<ul class="imgInfoSplit">
													<li>
														<label> 														
															<c:choose>
																<c:when test="${null ne sessionScope.cateImgPath && '' ne sessionScope.cateImgPath}">
																	<img id="couponImg" alt="upload" src="${sessionScope.cateImgPath}" height="46" width="46"
																	onerror="this.src = '/ScanSeeAdmin/images/uploadIcon.png';">
																</c:when>
																<c:otherwise>
																	<img id="couponImg" src="/ScanSeeAdmin/images/uploadIcon.png" width="46" height="46"
																	alt="image" onerror="this.src = '/ScanSeeAdmin/images/uploadIcon.png';" />
																</c:otherwise>
															</c:choose>
														</label> 
														<span class="topPadding forceBlock"> 
															<label for="trgrUpld"> <input type="button" value="Upload" id="trgrUpldBtn" width="350" height="460"
																class="btn trgrUpld" title="Upload Image File" tabindex="2"> <form:input path="cateImgPath"
																type="file" class="textboxBig" id="trgrUpld" onchange="checkCateImgValidate(this);" />
															</label>
														</span>
													</li>
													<li><b class="txt-nowrp">Suggested Size:46px/46px</b></li>
													<br>
													<li><i class="emptycatimg">Please Upload Category
															Image</i></li>
													<li><br> <label id="cateImagePathErr"
														class="cateImageErr "></label></li>
												</ul> 
											</td>
										</tr>
									</tbody>
								</table>

								<c:if
									test="${requestScope.errorMsg ne null && !empty requestScope.errorMsg}">
									<div class="alertBx success mrgnTop cntrAlgn" id="retSucces">
										<span title="close" class="actn-close"></span>
										<p class="msgBx">${requestScope.errorMsg}</p>
									</div>
								</c:if>

								<c:if
									test="${requestScope.successMsg ne null && !empty requestScope.successMsg}">
									<p class="alertInfo">
										${requestScope.successMsg} <a class="alrtClose"
											href="javascript:void(0);" title="Close">x</a>
									</p>
								</c:if>

								<div align="right" class="navTabSec mrgnRt">
									<input type="button" id="save" class="btn"
										value="Save Category" name="save" title="Save Category"
										onclick="saveFundraiserCatDetails()" tabindex="3"> <input
										type="button" id="clr" class="btn" value="Clear" title="Clear"
										onclick="clearFundraiserCateInfo()" tabindex="4"> <input
										type="button" onclick="fundraiserCategoryHome()" id="Back"
										class="btn" value="Back" name="back">
								</div>

							</div>
						</form:form>
					</fieldset>
				</div>
			</div>

			<!--Right container ends here-->
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>

		<div class="ifrmPopupPannelImage" id="ifrmPopup"
			style="display: none;">
			<div class="headerIframe">
				<img src="/HubCiti/images/popupClose.png" class="closeIframe"
					alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
				allowtransparency="yes" width="100%" style="background-color: White">
			</iframe>
		</div>
	</div>
</body>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {
						$("#FundraiserCategoryForm").attr('action',
								'/ScanSeeAdmin/uploadImage.htm');
						/*show progress bar : ETA for Web 1.3*/
						//showProgressBar();/* Commented to fix body scroll disable issue*/
						/*End*/
						$("#uploadBtn").val("trgrUpldBtn")
						$.ajaxSetup({
							cache : false
						});
						$("#FundraiserCategoryForm")
								.ajaxForm(
										{
											success : function(response) {
												$('#loading-image').css(
														"visibility", "hidden");
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue
												if (imgRes == 'maxSizeImageError') {
													$('#cateImagePathErr')
															.css("display",
																	"block")
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#cateImagePathErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {
													$('#cateImagePathErr')
															.text("");
													var substr = imgRes
															.split('|');
													if (substr[0] == 'ValidImageDimention') {
														$('#couponImg').width(
																'70px');
														$('#couponImg').height(
																'70px');
														var imgName = substr[1];
														$('#cateImgName').val(
																imgName);
														$('#couponImg').attr(
																"src",
																substr[2]);
													} else {
														/*commented to fix iframe popup scroll issue
														/$('body').css("overflow-y","hidden");*/
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeAdmin/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}
											}
										}).submit();
					});
</script>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Event Category</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>

<script type="text/javascript">
$(document).ready( function() {

$(".alrtClose").click(function() {
						$('p.alertErr').hide();
						$('p.alertInfo').css("display", "none");
					});
$(".actn-close").click(function() {
						
		var elemnt = $(this).parents('div'); 
		var alrtId = elemnt.attr("id");
		if(alrtId == 'evntCatClose'){
			$("#"+alrtId).hide();
			}
			else if(elemnt){
			$(this).parent('div.alertBx').hide();
			}
		 });	
						
					});

	function hubcitiAdminHome() {
		
		document.EventCategoryHome.action = "displayhubcitiadmin.htm";
		document.EventCategoryHome.method = "post";
		document.EventCategoryHome.submit();
	}
	function searchEvtCategory(event)
	{
	$('p.alertErr').hide();
	$('p.alertInfo').css("display", "none");
	var keyCode = (event.keyCode ? event.keyCode : event.which);
	if(keyCode == 13)
	{
		document.EventCategoryHome.action = "eventcategory.htm";
		document.EventCategoryHome.method = "post";
		document.EventCategoryHome.submit();
		}else if(event == ''){
		document.EventCategoryHome.action = "eventcategory.htm";
		document.EventCategoryHome.method = "post";
		document.EventCategoryHome.submit();
		}else{
		return true;
		}
		
	}
	
function displayEvtCategories()
	{
	$('p.alertErr').hide();
	$('p.alertInfo').css("display", "none");
	
		document.EventCategoryHome.action = "eventcategory.htm";
		document.EventCategoryHome.method = "post";
		document.EventCategoryHome.submit();
		
		
	}

/** Event category : delete category * */
function deleteEvtCategory(cateId) {
$('p.alertErr').hide();
$('p.alertInfo').css("display", "none");
	$('p.cathighLightErr').hide();
	var msg = confirm(" You want  to Delete Category?\n ");
	if (msg) {
		$.ajaxSetup({
			cache : false
		})
		$.ajax({
			type : "GET",
			url : "eventcatedelete.htm",
			data : {
				"cateId" : cateId,
			},

			success : function(response) {
					
				if (response == 'Success') {
				
					document.EventCategoryHome.actionType.value='delete';
					document.EventCategoryHome.action = "eventcategory.htm";
					document.EventCategoryHome.method = "post";
					document.EventCategoryHome.submit();
					
					
				} else {

					$('p.alertErr').show();
				}
			},
			error : function(e) {
				alert("Error occured while deleting event category");

			}

		});

	}
}

/** this function is used to open add event category popup. * */
function openEvtCategory()
{
$('p.alertErr').hide();
$('p.alertInfo').css("display", "none");
openIframePopup('ifrmPopup','ifrm','openevtcategory.htm',166,400,'Add Category');
}

/** this function is used to open edit event category popup. * */
function EditEventCategory(){
	/*$('p.alertErr').hide();
	$('p.alertInfo').css("display", "none");
	openIframePopup('ifrmPopup','ifrm','openevtcategory.htm?catId='+catId,166,400,'Edit Category');*/
	document.EventCategoryHome.action = "editeventcate.htm";
	document.EventCategoryHome.method = "get";
	document.EventCategoryHome.submit();

}
function addEvtCategory()
{
	//document.EventCategoryHome.actionType.value='delete';
	document.EventCategoryHome.action = "addeventcate.htm";
	document.EventCategoryHome.method = "get";
	document.EventCategoryHome.submit();
}

function fundraiserCategoryHome() {
	document.EventCategoryHome.action = "fundraiserCategory.htm";
	document.EventCategoryHome.method = "get";
	document.EventCategoryHome.submit();
}

</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hubciti.png"
					alt="ScanSee" width="400" height="54" /> </a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />, </span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">
				<div id="subnav" class="tabdSec">
					<ul>
					
						<li><a href="#" class="active" title="Manage Event Category"><span>Manage Event
									Category</span> </a></li>
					  <li><a href="#" title="Manage Fundraiser Category" onclick="fundraiserCategoryHome()"><span>Manage Fundraiser Category</span></a></li> 
					</ul>
				</div>
				<div class="contBlock mrgnTop">
					<form:form name="EventCategoryHome" commandName="EventCategoryHome"
						acceptCharset="ISO-8859-1">

						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<input type="hidden" name="searchKey" />
						<input type="hidden" name="actionType"
							onclick="displayEvtCategories();" id="hidbtn" />

						<fieldset>


							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<c:if
										test="${sessionScope.eventcatlst ne null && !empty sessionScope.eventcatlst || sessionScope.searckey ne null && !empty sessionScope.searckey}">
										<tr>
											<td class="header" colspan="3">Search</td>
										</tr>
										<tr>

											<td class="Label" width="18%"><label for="Supr_srchRbts">
													Category </label></td>
											<td width="60%" colspan="2"><form:input type="text"
													name="catName" path="catName"
													onkeypress="searchEvtCategory(event)" /> <a href="#"
												title="Search Event Category"
												onclick="searchEvtCategory('')" id="catsearch"> <img
													src="images/searchIcon.png" alt="Search" width="20"
													height="17" /> </a>
											</td>


										</tr>
									</c:if>
									<tr>
										<td width="22%" colspan="3" align="right"><input
											type="button" class="btn" value="Add Category"
											onclick="addEvtCategory()" title="Add Category" />
										</td>
									</tr>
								</table>
							</div>
						</fieldset>
				</div>
				
						<c:if
											test="${requestScope.successMsg ne null && !empty requestScope.successMsg}">
											<p class="alertInfo">
												${requestScope.successMsg} <a class="alrtClose"
													href="javascript:void(0);" title="Close">x</a>
											</p>
										</c:if>
				
				<c:if
					test="${requestScope.errorMessage ne null && !empty requestScope.errorMessage}">
					<div class="alertBx success mrgnTop cntrAlgn" id="retSucces">
						<span title="close" class="actn-close"></span>
						<p class="msgBx">${requestScope.errorMessage}</p>
					</div>
				</c:if>

				<p class="alertErr">
					Category has been associated to Event. Please deassociate and
					delete <a class="alrtClose" href="javascript:void(0);"
						title="close">x</a>
				</p>
				<c:if
					test="${requestScope.catdelete ne null && !empty requestScope.catdelete}">
					<p class="alertInfo">
						Event Category Deleted Successfully. <a class="alrtClose"
							href="javascript:void(0);" title="close">x</a>
					</p>
				</c:if>
				<c:choose>
					<c:when
						test="${sessionScope.eventcatlst ne null && !empty sessionScope.eventcatlst}">

						<fieldset>
							<legend>Event Category Details</legend>
							<div id="scrlGrd" class="grdTbl searchGrd">
								<table id="cpnLst" border="0" cellspacing="0" cellpadding="0"
									width="100%">
									<tbody>
										<tr class="header">
											<td width="68%">Category</td>
											<td align="center">Category Image</td>
											<td colspan="3" align="center">Action</td>
										</tr>


										<c:forEach items="${sessionScope.eventcatlst.alertCatLst}"
											var="cate">


											<tr>
												<td align="left"><a href="javascript:void(0);"
													id="${cate.catId}">${cate.catName}</a></td>
												<td width="16%" align="center"><img id="couponImg" 
													src="${cate.cateImgName}" width="46" height="46"
													alt="image"
													onerror="this.src = '/ScanSeeAdmin/images/uploadIcon.png';" />
												</td>

												<td width="8%" align="center"><a href="editeventcate.htm?catId=${cate.catId}"><img
														src="images/edit_icon.png" alt="edit"
														title="Edit Category" width="25" height="19"
														 />
												</a></td>
												<td width="8%" align="center"><a href="#"><img
														src="images/delicon.png" title="Delete Category"
														width="18" height="18"
														onclick="deleteEvtCategory(${cate.catId})" /> </a></td>
											</tr>
										</c:forEach>

									</tbody>
								</table>

							</div>

						</fieldset>
					</c:when>
					<c:otherwise>
						<c:if
							test="${requestScope.catdelete eq null || empty requestScope.catdelete}">
							<!--<div class="alertBx warning mrgnTop cntrAlgn" id="evntCatClose">
								<span class="actn-close" title="close"></span>
								<p class="msgBx"></p>
							</div>-->
							<center>
								<span class="highLightErr"><c:out
										value="No Category found" /> </span>
							</center>
						</c:if>
					</c:otherwise>
				</c:choose>
				<c:if
					test="${sessionScope.eventcatlst.alertCatLst ne null && !empty sessionScope.eventcatlst.alertCatLst}">
					<div class="pagination">
						<p>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</c:if>

				</form:form>
			</div>

			<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="images/popupClose.png" class="closeIframe" alt="close"
						onclick="javascript:	closeEvtIframePopup('ifrmPopup','ifrm');"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="no" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>

		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>


</body>
</html>

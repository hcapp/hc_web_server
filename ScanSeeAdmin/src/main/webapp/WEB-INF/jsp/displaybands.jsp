<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bands</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>

<script type="text/javascript">
			
			$(document).ready(function(){
			   $("input[name='phoneNo']").live('keypress',function(evt) {
					
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode < 31 || charCode == 46)
						return true;
				
					return false;
				});
				$("input[name='postalCode']").live('keypress',function(evt) {
					
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode < 31 || charCode == 46)
						return true;
				
					return false;
				});
				$("input[name='latitude']").live('keypress',function(evt) {
					
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
							|| charCode == 45 || charCode == 43)
						return true;
					return false;
				});
				$("input[name='longitude']").live('keypress',function(evt) {
					
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
							|| charCode == 45 || charCode == 43)
						return true;
					return false;
				});
					
					$("#scrlGrd select").attr("disabled","disabled");
								$('input[name$="deleteAll"]').click(function() {
									var status = $(this).attr("checked");
									$('input[name$="checkboxDel"]').attr('checked',status);
									$del = $('#delete');
									$del.removeAttr('disabled').addClass('hltBtn');
								
									if(!status) {
									$('input[name$="checkboxDel"]').removeAttr('checked');
									$del.attr('disabled','disabled').removeClass('hltBtn');
									}
								});
								$('input[name$="checkboxDel"]').click(function() {											   
									var tolCnt = $('input[name$="checkboxDel"]').length;	
									var chkCnt = $('input[name$="checkboxDel"]:checked').length;
									if(tolCnt == chkCnt)
										$('input[name$="deleteAll"]').attr('checked','checked');
									else
										$('input[name$="deleteAll"]').removeAttr('checked');
									
									$del = $('#delete');
									if(chkCnt > 0)
									{
										
										$del.removeAttr('disabled').addClass('hltBtn');
									}
									else{
										$del.attr('disabled','disabled').removeClass('hltBtn');
									}
								});	
				
								if(null != "${sessionScope.state}" && null != "${sessionScope.city}")
								{
									loadCity();
								}
								
							
											
							});
							
							function isLatLong(evt) {
					var charCode = (evt.which) ? evt.which : event.keyCode
					if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
							|| charCode == 45 || charCode == 43)
						return true;
					return false;
}
</script>
<script>
		/*
				Modal starts
		*/
		$(document).ready(function(){		
			
		    var  mainChkd = $('.main-ctrgy').attr('checked');
			if(mainChkd){
				$(mainChkd).each(function(index, element) {
				   $('.sub-ctgry').show();
				});
			}
		    
		    $('#findchkAll').live('click.chkAll',function() {
					var status = $(this).attr('checked');
					$('input[name$="findChk"]').attr('checked',status);
					$(".sub-ctgry").show();
					getMainSubCtryIds();
					if(!status) {
						$('input[name$="findChk"]').removeAttr('checked');
						$(".sub-ctgry").hide();
					}
				});
				
				/*Any one of the child{chkbx) uncheck ,uncheck parent chkbx*/	
				$('input[name$="findChk"]').live('click',function() {	
					var tolCnt = $('input[name$="findChk"]').length;
					var chkCnt = $('input[name$="findChk"]:checked').length;
					var prntCtgry = $(this).hasClass('main-ctrgy');
					var prntstatus = $(this).attr("checked");
					if(tolCnt == chkCnt) {
					$('#findchkAll').attr('checked','checked');
					}
					else {
					$('#findchkAll').removeAttr('checked');
					}
					
					if(prntCtgry){
						$(this).parents('li').find('.sub-ctgry li').each(function(index, element) {
							$(this).find('input[name$="findChk"]').attr('checked',prntstatus);
						});
						$(this).parents('li').find('.sub-ctgry').show();
					}
					if(prntstatus){
						$(this).parents('li').find('.sub-ctgry').show();
					} else {
						childCheck(this);
					}
					prntCheckAll();
				}); 
			
				
				$(".sub-ctgry li").live('click', 'input[name$="findChk"]', function(){
				var childChkbx =  $(this).parents('li').find('input[name$="findChk"]:not(".main-ctrgy")').length;
				var childChkbxChkd = $(this).parents('li').find('input[name$="findChk"]:not(".main-ctrgy"):checked').length;
				if(childChkbxChkd < 1){
					$(this).parents('li').find(".main-ctrgy").removeAttr('checked');
					$(this).parents('.sub-ctgry').hide();
				}else{
					$(this).parents('li').find(".main-ctrgy").attr('checked','checked');
					$(this).parents('.sub-ctgry').show();
					
				}
				prntCheckAll();
				});
			
			
			});
		
		
			function prntCheckAll(){
				var tolCnt = $('input[name$="findChk"]').length;
				var chkCnt = $('input[name$="findChk"]:checked').length;
				if(tolCnt == chkCnt)
				$('#findchkAll').attr('checked','checked');
				else
				$('#findchkAll').removeAttr('checked');
			}
			
			function childCheck(obj){
				var childChkbx =  $(obj).parents('li').find('input[name$="findChk"]:not(".main-ctrgy")').length;
				var childChkbxChkd = $(obj).parents('li').find('input[name$="findChk"]:not(".main-ctrgy"):checked').length;
				if(childChkbx === childChkbxChkd){
					$(obj).parents('li').find('.sub-ctgry').show();
					
				}
				if(childChkbxChkd < 1){
					$(obj).parents('li').find('.sub-ctgry').hide();
					$(this).parents('li').find(".main-ctrgy").removeAttr('checked');
				}
			}
			function getMainSubCtryIds(){
			  var arr = [];
			  var mainarr = [];
			  var subLen = $('.sub-ctgry').length;
			  var mainChk = $(".main-ctrgy").map(function(index, element) {
			      return($(this).attr("id"));
			  }).toArray();
			
			  for(i=0;i<subLen;i++){
			  $('.sub-ctgry').find('li').each(function(){
			    arr[i++] = $(this).find("input:checkbox").attr('id');
			  if(($(this).is(":last-child"))){
			     arr[i++] = "|";
			    $(this).addClass("last");
			  }
			  });
			 }
			}
		
			function setCheckBox() {

			    var  mainChkd = $('.main-ctrgy');

			    if(mainChkd){
			      for(i=0; i<= $('.main-ctrgy:checked').length; i++ ) {

			        $('.main-ctrgy:checked').parent(".cell").next(".sub-ctgry").show();
			      }
			    }
				var tolCnt = $('input[name$="findChk"]').length;
				var chkCnt = $('input[name$="findChk"]:checked').length;
				if(tolCnt == chkCnt)
				$('#findchkAll').attr('checked','checked');
				else
				$('#findchkAll').removeAttr('checked');
							
			}	
		    

		/*
				modal end
		*/
			function saveCat(retailId){
			    var childChk = [];
				var mainarr = [];
				var retId = $('#retailId').attr('id');
				var mainChk = $(".main-ctrgy:checked").map(function(index, element) {
					return($(this).attr("id"));
				}).toArray();
		  		
				var childChk = $(".sub-ctgry input:checkbox:checked").map(function(index, element) {
					return($(this).attr("id"));
				}).toArray();
				
				mainChk   = mainChk.toString();
				childChk  = childChk.toString();
				
				$.ajaxSetup({cache:false})
				$.ajax({
					type : "GET",
					url : "savebandcat.htm",
					data : {
						'retailId' : document.retailer.retailId.value,
						'buscatIds':mainChk,
						'subcatIds':childChk
					},

					success : function(response) {
						$('.modal-cont').html(response);
						var bodyHt = $('body').height();
						var setHt = parseInt(bodyHt);
						$(".modal-popupWrp").show();
						$(".modal-popupWrp").height(setHt);
						$(".modal-popup").slideDown('fast');
						
						setCheckBox();
					},
					error : function(e) {
						
					}
				});
				
			}
		function showModal(retailId) {
		    
		    $.ajaxSetup({cache:false})
			$.ajax({
				type : "GET",
				url : "fetchbandcat.htm",
				data : {
					'retailId' : retailId,
				},

				success : function(response) {
					$('.modal-cont').html(response);
					document.retailer.retailId.value = retailId;
					var bodyHt = $('body').height();
					var setHt = parseInt(bodyHt);
					$(".modal-popupWrp").show();
					$(".modal-popupWrp").height(setHt);
					$(".modal-popup").slideDown('fast');
					setCheckBox();
					
				},
				error : function(e) {
					
				}
			});		    
		}
		
		/*close modal popup on click of x*/
		function closeModal() {
			$(".modal-popupWrp").hide();
			$(".modal-popup").slideUp();
			$(".modal-popup").find(".modal-bdy input").val("");
			$(".modal-popup i").removeClass("errDsply");
			$(".overlay").hide();
		}
		function loadCity() {
			var stateCode = $('#Country').val();
			
			$.ajaxSetup({cache:false})
			$.ajax({
				type : "GET",
				url : "fetchcity.htm",
				data : {
					'statecode' : stateCode,
					'city' : null
				},

				success : function(response) {
					$('#cityDiv').html(response);
				},
				error : function(e) {
					
				}
			});
		}

			function searchRetailer() 
			{
				document.retailer.retailLowerLimit.value = 0;
				document.retailer.action = "searchretailer.htm"; 
				document.retailer.method = "post";
				document.retailer.submit();
			}
			
			function 	displayretailer() 
			{
				document.retailer.isBand.value = false;
				document.retailer.action = "displayretailer.htm"; 
				document.retailer.method = "post";
				document.retailer.submit();
			}
		
			function searchRetailerOnKeyPress(event) 
			{
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode == '13') 
				{
					document.retailer.retailLowerLimit.value = 0;
					document.retailer.action = "searchretailer.htm"; 
					document.retailer.method = "post";
					document.retailer.submit();
					
				}
			}

			function deleteRetailers(){				
				var checkedValue = [];
				var i = 0;
				var r;
				chkdBx = $('input[name="checkboxDel"]:checkbox:checked').length;
				if(chkdBx > 0) {
					$('input[name="checkboxDel"]:checked').each(function(){
					 	checkedValue[i] = $(this).val();
						 i++;
			    	});
				}
				if(checkedValue.length == 1)
				{
					r = confirm("Are you sure you want to delete this Band ?");
						
				}else
				{
					r = confirm("Are you sure you want to delete these Bands ?");
				}
				if(r == true){
				    document.retailer.isBand.value = true;
					document.retailer.retailerIds.value = checkedValue;
					document.retailer.action = "deleteretailer.htm";
					document.retailer.method = "post";
					document.retailer.submit();					
				}
			}
			
			

			function deleteRetailer(retailerId)
			{			
				var r = confirm("Are you sure you want to delete this Band ?");
				if(r == true){
					document.retailer.retailerIds.value = retailerId;
					document.retailer.action = "deleteretailer.htm";
					document.retailer.method = "post";
					document.retailer.submit();	
				}			
			}

			function displayLocation(retailerId, retailerName)
			{				
				document.retailer.retailId.value = retailerId;
				document.retailer.retailName.value = retailerName;
				document.retailer.action = "displayretailerLocation.htm";
				document.retailer.method = "post";
				document.retailer.submit();
				
			}
			
			function showRetailBusCategory(retailerId) {
				openIframePopup('ifrmPopup', 'ifrm',
						'fetchbuscategory.htm?retailId='+ retailerId, 226,
						400, 'Business Category');
			}
			
			function retailerCreateLogin(isLoginExist, retailerId)	{
			    document.retailer.isBand.value = true;
				document.retailer.isLoginExist.value = isLoginExist;	
				document.retailer.retailId.value = retailerId;
				document.retailer.action = "retailerCreateLogin.htm";
				document.retailer.method = "post";
				document.retailer.submit();
			}
			
			function makeItPaidOrUnpaid(retailID) {
				var isPaid = $("#paid-" + retailID).attr("isPaid");
				var pay ;
				var r;
				if(isPaid == "true") {
					r = confirm("Are you sure you want to make this Band as 'Unpaid Band' !");
					pay = "false";
				} else {
					r = confirm("Are you sure you want to make this Band as 'Paid Band' !");
					pay = "true";
				}
				
				if(r == true) {
					$.ajaxSetup({cache:false})
					$.ajax({
						type : "GET",
						url : "saveRetPayInfoBand.htm",
						data : {
							'retailID' : retailID,
							'isPaid' : pay
						},

						success : function(response) {
							if(isPaid == "true") {
								$("#paid-" + retailID).attr("isPaid", pay);
								$("#paid-" + retailID).val("Unpaid");
							} else {
								$("#paid-" + retailID).attr("isPaid", pay);
								$("#paid-" + retailID).val("Paid");
							}							
						},
						error : function(e) {
							alert("error updating");
						}
					});
				}
			}				
		</script>
</head>
<body onload="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-band.png" alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left" title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a></li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<!--
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
		-->
			<div id="subnav" class="tabdSec">
				<ul>
					<li><a onclick="displayretailer()" href="#"><span>Retailer Administration</span></a></li>
					<li><a class="active"><span>Band Administration</span></a></li>
				</ul>
			</div>
			<div class="roleRtContPnl">
				<div class="contBlock">
					<form:form name="retailer" commandName="retailer" acceptCharset="ISO-8859-1">
						<form:hidden path="retailerIds" />
						<form:hidden path="retailId" />
						<form:hidden path="retailName" />
						<form:hidden path="retailInfoJson" />
						<form:hidden path="retailLowerLimit" />
						<form:hidden path="isLoginExist" />
						<form:hidden path="isBand" />
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />

						<fieldset>

							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
									<tr>
										<td class="Label" width="20%"><label for="Supr_srchRbts">Band Name</label></td>
										<td colspan="3"><c:if test="${sessionScope.searchKey ne null && !empty sessionScope.searchKey}">
												<form:input path="searchKey" value="${sessionScope.searchKey}" onkeypress="searchRetailerOnKeyPress(event)" />
											</c:if> <c:if test="${sessionScope.searchKey eq null || empty sessionScope.searchKey}">
												<form:input path="searchKey" onkeypress="searchRetailerOnKeyPress(event)" />
											</c:if> <label></label> <a href="#"> <img src="images/searchIcon.png" alt="Search" width="20" height="17" title="Search"
												onclick="searchRetailer()" />
										</a></td>
									</tr>
									<tr>
										<td class="Label">State</td>
										<td width="29%"><select name="state" id="Country" class="textboxBig" onchange="loadCity();">
												<option value="">--Select--</option>
												<c:forEach items="${sessionScope.statesListCreat}" var="s">
													<c:if test="${sessionScope.state ne s.stateabbr}">
														<option value="${s.stateabbr}">
															<c:out value="${s.stateName}"></c:out>
														</option>
													</c:if>
													<c:if test="${sessionScope.state eq s.stateabbr}">
														<option value="${s.stateabbr}" selected="selected">
															<c:out value="${s.stateName}"></c:out>
														</option>
													</c:if>
												</c:forEach>
										</select></td>
										<td width="10%">City</td>
										<td width="41%">
											<div id="cityDiv">
												<select name="city" id="city" class="textboxBig">
													<option value="">--Select--</option>
												</select>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</fieldset>
					</form:form>
				</div>
				<center>
					<span class="highLightErr" style="font: red;"> <c:out value="${requestScope.message}" />
					</span> <span class="highLightErr" style="font: red;"> <c:out value="${requestScope.errorMessage}" />
					</span> <span class="highLightMsg"> <c:out value="${requestScope.retUpdateMessage}" />
					</span>

				</center>

				<fieldset class="relative">
					<div class="overlay"></div>
					<legend>Bands</legend>
					<div id="roleScrlGrd" class="grdCont searchGrd roleHrzScrl">
						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0" cellpadding="0" width="296%">
							<tbody>
								<tr class="header">
									<td width="" align="center"><c:if test="${!empty sessionScope.RetailerList}">
											<label><input type="checkbox" name="deleteAll" /> </label>
										</c:if></td>
									<td width="">Band ID</td>
									<td width="" onclick="sortColumn('RetailerName')">
										<div class="txtwrap">
											<a href="#" title="Retailer Name"> <label class="mandTxt">Band Name</label> <c:if test="${!empty sessionScope.RetailerNameImg}">
													<img src="${sessionScope.RetailerNameImg}" width="11" height="6" />
												</c:if>
											</a>
										</div>
									</td>
									<td width=""><label>Address</label></td>
									<td width=""><label>State</label></td>
									<td width=""><label>City</label></td>
									<td width=""><label>Postal Code</label></td>
									<td width="">Website URL</td>
									<td width=""><label>Latitude</label></td>
									<td width=""><label>Longitude</label></td>
									<td width=""><label>Phone No</label></td>
									<td width="5%">Paid / Unpaid</td>
									<td width="6%">Band Category</td>
									<td width="270" align="center">Action</td>

								</tr>
								<c:forEach items="${sessionScope.RetailerList}" var="item">

									<tr id="retInfo">
										<td align="center" class="mand"><input type="checkbox" name="checkboxDel" value="${item.retailId}" /></td>

										<td align="left"><c:out value="${item.retailId}" /></td>

										<td align="left" class="editCol"><c:out value="${item.retailName}" escapeXml="false" /></td>

										<td class="editCol"><c:out value="${item.address1}" /></td>

										<td class="editSlct"><c:out value="${item.state}" /></td>

										<td class="editSlct"><c:out value="${item.city}" /></td>
										<td class="editCol"><c:out value="${item.postalCode}" /></td>
										<td class="editCol"><c:out value="${item.retUrl}" /></td>
										<td class="editCol latChk"><c:out value="${item.latitude}" /></td>

										<td class="editCol lonChk"><c:out value="${item.longitude}" /></td>

										<td class="editCol"><c:out value="${item.phoneNo}" /></td>
										<td align="center"><c:choose>
												<c:when test="${item.isPaid ne null && item.isPaid ne false}">
													<input type="button" class="cstmbtn" id="paid-${item.retailId}" value="Paid" onclick="makeItPaidOrUnpaid(${item.retailId});"
														isPaid="${item.isPaid}" name="isPaid" />
												</c:when>
												<c:otherwise>
													<input type="button" class="cstmbtn" id="paid-${item.retailId}" value="Unpaid" onclick="makeItPaidOrUnpaid(${item.retailId});"
														isPaid="${item.isPaid}" name="isPaid" />
												</c:otherwise>
											</c:choose></td>

										<td align="center"><input type="button" value="Edit Category" class="btn tgl-overlay"
											onclick="showModal('<c:out value="${item.retailId}"/>')"></input></td>

										<%-- <td align="center"><input type="button" value="Edit Category" class="btn tgl-overlay"
											onclick="showRetailBusCategory('<c:out value="${item.retailId}"/>')" title="Edit Business Category" /></td> --%>

										<td align="center"><input type="button" class="btn row-edit-band" value="Edit" /> <input type="button" class="btn row-save-band"
											value="Save" /> <input type="button" class="btn row-del" value="Delete" onclick="deleteRetailer('${item.retailId}')" /> <c:choose>
												<c:when test="${item.isLoginExist ne null && item.isLoginExist != 0}">
													<input type="button" class="cstmbtn" value="Edit Login" onclick="retailerCreateLogin('${item.isLoginExist}', '${item.retailId}')" />
												</c:when>
												<c:otherwise>
													<input type="button" class="cstmbtn" value="Create Login" onclick="retailerCreateLogin('${item.isLoginExist}', '${item.retailId}')" />
												</c:otherwise>
											</c:choose></td>
									</tr>

								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="pagination">
						<p>
							<page:pageTag currentPage="${sessionScope.pagination.currentPage}" nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
				<div class="mrgnTop" align="right">
					<input type="button" class="btn" id="delete" value="Delete" title="Delete" onclick="deleteRetailers();" disabled="disabled" />
				</div>
			</div>


			<div class="modal-popupWrp">
				<div class="modal-popup">
					<div class="modal-hdr">
						Setup Category and SubCategory  <a class="modal-close" title="close" onclick="closeModal()"><img src="images/popupClose.png" /></a>
					</div>
					<div class="modal-bdy">
						<div class="modal-cont">
							<!-- <div class="input-actn">
								<input id="findchkAll" type="checkbox"> <label for="findchkAll">Select All</label>
							</div>
							<ul style="display: block;" class="infoList cmnList menuLst" id="find"> -->
							<!-- <li><span class="cell zeroMrgn"> <input name="findChk" class="main-ctrgy" id="f6" type="checkbox"> <label for="find6">Find
												6</label></span>
									<ul class="sub-ctgry">
										<li><input name="findChk"  type="checkbox"> <label>Find 6.1</label></li>
									</ul>
								</li> -->
							</ul>
						</div>
					</div>
					<div class="modal-ftr">
						<p align="right">

							<!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
							<input name="Save" value="Save" type="button" class="btn" id="save" onclick="saveCat('<c:out value="${item.retailId}"/>')"/> 
							<input type="button" id="Cancel" class="btn" value="Cancel" name="Save" onclick="closeModal()">
						</p>
					</div>
				</div>
			</div>

		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti. All rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
	</div>
	<script>
	
		$(".latChk").focusout(function() {
			/* Matches	 90.0,-90.9,1.0,-23.343342
			Non-Matches	 90, 91.0, -945.0,-90.3422309*/
				var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
						 // var vLat = this.value;
						 var vLat = $(this).parents('tr.curRow').find('input[name="latitude"]').val();
						  //Validate for Latitude.
						  if (0 === vLat.length || !vLat || vLat == "") {
							return false;
						 } else {
							if(!vLatLngVal.test(vLat)) {
						      alert("Latitude are not correctly typed");
						      $(this).parents('tr.curRow').find('input[name="latitude"]').val("");
						      return false;
						  }
						 }
			});

		$(".lonChk").focusout(function() {
			/* Matches	180.0, -180.0, 98.092391
			Non-Matches	181, 180, -98.0923913*/
			var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
				//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
						  var vLong = $(this).parents('tr.curRow').find('input[name="longitude"]').val();
						  //Validate for Longitude.
						  if (0 === vLong.length || !vLong || vLong == "") {
							return false;
						 } else {
							if(!vLatLngVal.test(vLong)) {
						      alert("Longitude are not correctly typed");
						      $(this).parents('tr.curRow').find('input[name="longitude"]').val("");
						      return false;
						  }
						 }
			});
		</script>

</body>
</html>
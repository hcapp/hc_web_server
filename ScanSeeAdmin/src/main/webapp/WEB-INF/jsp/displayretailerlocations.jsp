<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Retailer Locations</title>
<script type="text/javascript" src="/ScanSeeAdmin/scripts/jquery.js"></script>
<script type="text/javascript" src="/ScanSeeAdmin/scripts/jquery-1.6.2.min.js"></script>
<script type="text/javascript"
	src="/ScanSeeAdmin/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/ScanSeeAdmin/scripts/colorPickDynamic.js"></script>
<script type="text/javascript"
	src="/ScanSeeAdmin/scripts/colorPicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeAdmin/styles/colorPicker.css" />
<link rel="stylesheet" type="text/css" href="/ScanSeeAdmin/styles/styles.css" />
<link href="/ScanSeeAdmin/styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/ScanSeeAdmin/scripts/global.js"></script>
<script type="text/javascript" src="/ScanSeeAdmin/scripts/jquery-ui.min.js"></script>

<script>
			saveRetalLocation = true;
		</script>
<script type="text/javascript">

			$(document).ready(function(){
								
			
			$(".selectFilters").click(function() {	
		
				var vRetlocId =$(this).parents("tr").attr("id");
				
				var vRetailId = document.retailer.retailId.value;
						
		var finalresponse="";
		var filCat ;
			var filter_split;
			var subfilter_split;
		  $('body,html').animate({scrollTop :0}, 'slow');
		/*var ifrmcont = $( "#ifrm3" ).contents();	 


		var slctdOptns =$(ifrmcont).find(".main:checked").parents("ul").map(function() {
			return this.id;
		  }).get().join();*/
		 


		  $.ajaxSetup({
				cache : false
		  });
		  
		  $.ajax({
				type : "GET",
				url : "getretlocfilter.htm",
				data : {
					'retId' : vRetailId,
					'retLocId' :vRetlocId,
				},
				success : function(response) {    
				
				if("" !== response) {
				 finalresponse =response.split('&');
				 filCat = finalresponse[1].split(",");
				 filter_split =finalresponse[2].split("!~~!");
				 subfilter_split =finalresponse[3].split("!~~!");
				$("#ifrm3").contents().find('p.highLightMsg').hide();
			$("#ifrm3").contents().find('p.highLightErr').hide();
					$("#ifrm3").contents().find("body").html("");
					
					//$("#hiddenFilterCategory").val(slctdOptns);
					openIframePopup('ifrmPopup3','ifrm3','/ScanSeeAdmin/adminFilters.htm',590,960,'Select Filter(s)');
					//$("#ifrm3").contents().find("body").html("");
			$("#ifrm3").contents().find('p.highLightMsg').hide();
		
					if("" !== finalresponse[0]) {
						var resJSON = JSON.parse(finalresponse[0]);
						var JSONLength = resJSON['categories'].length;
						if(JSONLength > 0) {
							var displayContent;
							if(JSONLength === 1) {
								displayContent = "<div class='row single'>";
							} else {
								displayContent = "<div class='row'>";
							}
							
							for(var i = 0; i < JSONLength; i++) {
								if(i > 0) {
									displayContent += "</ul></div>";
								}
								var category = resJSON['categories'][i];
								var catid = category.id;
								var catname = category.Name;
								displayContent += "<div class='col " + catid +"'><p class='pnlHdr'><input type='checkbox'  class='trgrChlAll bCategory' value='" + catid + "'/><label>"; 
								displayContent += catname + "</label></p><ul class='treeview' id='"+ catid +"'>";
								var filterJSON = category["filters"];
								
								for(var j = 0; j < filterJSON.length ;j++) {
									var filter = filterJSON[j];
									var filterName = filter.Name;
									var filterId = filter.id;
									displayContent += "<li class='mainCtgry' name='"+ filterName + "'>";
									displayContent += "<input class='main' type='checkbox' value='"+ filterId + "'/><label>" + filterName + "</label></li>";
									var subfilterJSON = filter["subfilters"];
									if(subfilterJSON != 'undefined' && "undefined" !== typeof(subfilterJSON)) {
										for(var k = 0; k < subfilterJSON.length; k++) {
											displayContent += "<li class='subCtgry' name='"+ filterName +"'>";
											displayContent += "<input class='sub' type='checkbox' value='"+ subfilterJSON[k].id + "'/><label>" + subfilterJSON[k].Name + "</label></li>";
										}
									}
								}
							}
							
							
							displayContent += "</ul></div></div>";
						
						
							$("#ifrm3").load(function(){	
						
							$("#ifrm3").contents().find('p.highLightMsg').hide();
							$("#ifrm3").contents().find('p.highLightErr').hide();
					
							//	$("#ifrm3").contents().html("");
								var iframe = $("#ifrm3").contents();
								iframe.find("#message").css("display","none");
								iframe.find("#associate").removeAttr("disabled");
								iframe.find("#associate").removeClass("disabled");								
								iframe.find("div.row").remove();
								iframe.find("#tabularPnl").html(displayContent);
								
							
							//	var filCat = $("#filterCategory").val().split(",");
							//	var filter_split = $("#filters").val().split("!~~!");
							//	var subfilter_split = $("#filterValues").val().split("!~~!");
								var k=0;
								var n=0;							
								var filter_split_length = filter_split.length;
								if(filter_split_length == 1 && filter_split[0] === "") {
									filter_split_length = 0;							
								}
								for(var i =0 ; i < filter_split_length; i++) {
									var $this = $("#ifrm3").contents().find("div." + filCat[i]);
									var filStr = filter_split[i].split("|");
									if(filStr.length == 1 && filStr === "NULL") {
										k++;
									} else {
										for(var j =0 ; j < filStr.length; j++) {
											$($this).find("input:checkbox[value="+ filStr[j] +"].main").attr("checked", true);
											var valFil = subfilter_split[k].split("|");
											for(var m = 0; m < valFil.length; m++) {
												$($this).find("input:checkbox[value="+ valFil[m] +"].sub").attr("checked", true);
												var li_name = $($this).find("input:checkbox[value="+ valFil[m] +"].sub").parent('li').attr("name");
												$($this).find('li[name="'+li_name+'"]').show();
												//$($this).find("input:checkbox[value="+ valFil[m] +"].sub").parent('li').show();
											}
											k++;
										}
									}

									var getId = $("#ifrm3").contents().find("ul#" + filCat[i]);
									var chkbx = $(getId).find('li :checkbox').length;
									var chkbxChkd = $(getId).find('li :checkbox:checked').length;
									if(chkbx === chkbxChkd){
										$(getId).prev('p').find(':checkbox').attr('checked', true);
									}
									else {
										$(getId).prev('p').find(':checkbox').removeAttr('checked');
									}
								}														
							});
							
						
						} 
					} else {
						$("#ifrm3").load(function(){
							var iframe = $("#ifrm3").contents();
							var iframe = $("#ifrm3").contents();
						//	$("#ifrm3").contents().find('p.highLightMsg').hide();
							iframe.find('p.highLightMsg').hide();
							iframe.find("div.row").remove();
							iframe.find("#message").css({"display":"block","margin-top":"10px"});
							iframe.find("#associate").attr("disabled","disabled");
							iframe.find("#associate").addClass("disabled");
						});
					}
					}else{
				
				
					openIframePopup('ifrmPopup3','ifrm3','/ScanSeeAdmin/adminFilters.htm',590,960,'Select Filter(s)');
					$("#ifrm3").load(function(){
					var iframe = $("#ifrm3").contents();
					iframe.find("#message").css({"display":"block","margin-top":"10px"});
					});
					
					}
					
					},
				error : function(e) {
					alert(e);
				}
			});		  	
		});
			
			for(i = 1 ; i <= 20; i++)
				{
					var city = $('#state' + i).attr('city');
					loadCity(i, city);
				}			
				
				$('input[name$="deleteAll"]').click(function() {
					var status = $(this).attr("checked");
					$('input[name$="checkboxDel"]').attr('checked',status);
					$del = $('#delete');
					$del.removeAttr('disabled').addClass('hltBtn');
				
					if(!status) {
					$('input[name$="checkboxDel"]').removeAttr('checked');
					$del.attr('disabled','disabled').removeClass('hltBtn');
					}
				});
				$('input[name$="checkboxDel"]').click(function() {											   
					var tolCnt = $('input[name$="checkboxDel"]').length;	
					var chkCnt = $('input[name$="checkboxDel"]:checked').length;
					if(tolCnt == chkCnt)
						$('input[name$="deleteAll"]').attr('checked','checked');
					else
						$('input[name$="deleteAll"]').removeAttr('checked');
					
					$del = $('#delete');
					if(chkCnt > 0)
					{
						
						$del.removeAttr('disabled').addClass('hltBtn');
					}
					else{
						$del.attr('disabled','disabled').removeClass('hltBtn');
					}
				});				
			});
		</script>
<script>

			function numRows() {
				var pagenumber;
				var rowCount = $('#cpnLst tr').length;
				if (rowCount <= 1) {
					alert("No records to save");
					return false;
				}
				var result = confirm("Do you want to save the changes");
				if (result) {
					saveRetailerLocs(pagenumber);
				}
			}

			function loadCity(count, city) {
			$('#state' + i).removeAttr('city');
				var stateCode = $('#state' + count).val();
				$.ajaxSetup({cache:false})
				$.ajax({
					type : "GET",
					url : "fetchcities.htm",
					data : {
						'statecode' : stateCode,
						'city' : city
					},
			
					success : function(response) {
						$('#cityDiv' + count).html(response);
						
						//$('#yourElement option[value="'+yourValue+'"]').attr('selected', 'selected');
						
						$("#city"+count).val(city);
						//$('#cityDiv' + count).find("select").attr("id", "city"+count);
					},
					error : function(e) {
						
					}
				});
			}

			function back() {
				document.retailer.action = "displayretailer.htm";
				document.retailer.method = "post";
				document.retailer.submit();		
			}
			
			function deleteRetailerLocs(){					
				var checkedValue = [];
				var i = 0;
				var r;
				chkdBx = $('input[name="checkboxDel"]:checkbox:checked').length;
				if(chkdBx > 0) {
					$('input[name="checkboxDel"]:checked').each(function(){
					 	checkedValue[i] = $(this).val();
						 i++;
			    	});
				}
				if(checkedValue.length == 1)
				{
					r = confirm("Are you sure you want to delete this Location ?");
						
				}else
				{
					r = confirm("Are you sure you want to delete these Locations ?");
				}
				if(r == true){
					document.retailer.retailerLocIds.value = checkedValue;
					document.retailer.action = "deleteretailerlocation.htm";
					document.retailer.method = "post";
					document.retailer.submit();					
				}					
			}

			function deleteRetailerLoc(retailerLocId)
			{	
				var r = confirm("Are you sure you want to delete this Location ?");
				if(r == true){		
					document.retailer.retailerLocIds.value = retailerLocId;
					document.retailer.action = "deleteretailerlocation.htm";
					document.retailer.method = "post";
					document.retailer.submit();		
				}				
			}

			function searchRetailerLoc() { 
				var searchBy = $('#searchBy').val();
				var searchKey = $('input:text').val();
				
				if(searchBy == "" && searchKey != "")
				{
					alert('Please select search by option');
				}
				else
				{
					document.retailer.retailLocLowerLimit.value = 0;
					document.retailer.action = "searchretailerLocation.htm";
					document.retailer.method = "post";
					document.retailer.submit();
				}
			}
			
			function searchRetailerLocOnKeyPress(event) {				
				var keycode = (event.keyCode ? event.keyCode : event.which);
				var searchBy = $('#searchBy').val();
				var searchKey = $('input:text').val();
				if (keycode == '13') 
				{						
					if(searchBy == "" && searchKey != "")
					{
						alert('Please select search by option');
						event.preventDefault();
						return false;
					}
					else
					{
						document.retailer.retailLocLowerLimit.value = 0;
						document.retailer.action = "searchretailerLocation.htm";
						document.retailer.method = "post";
						document.retailer.submit();
					}						
				}			
			}

			function isNumeric(vNum, count) {
				var ValidChars = "0123456789";
				var IsNumber = true;
				var Char;
				var vPostal = document.getElementById("postalCode" +count);
				for (i = 0; i < vNum.length && IsNumber == true; i++) {
					Char = vNum.charAt(i);
					if (ValidChars.indexOf(Char) == -1) {
						vPostal.value = "";
						vPostal.focus();
						IsNumber = false;
					}
				}
				return IsNumber;
			}
			
			function isLatLong(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
						|| charCode == 45 || charCode == 43)
					return true;
				return false;
			}

			function isNumberKeyPhone(evt) {
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31)
					return true;

				return false;
			}
			
			function showRetailSubCategory(retailLocationId) {
				var vRetailId = document.retailer.retailId.value;
				openIframePopup('ifrmPopup2', 'ifrm2',
						'fetchsubcategory.htm?retailLocationId='+ retailLocationId +'&retailId='+ vRetailId, 226,
						400, 'Sub Category');
			}
			
function showRetailFilters(retailLocationId) {
			
				
		}
	
			function closeFilterIframePopup(popupId, IframeID, funcUrl) {

//	setTimeout(function() {
		try {

			document.body.removeChild(frameDiv);
			document.getElementById(popupId).style.display = "none";
			document.getElementById(popupId).style.height = "0px";
			document.getElementById(popupId).style.width = "0px";
			document.getElementById(popupId).style.marginLeft = "0px";
			document.getElementById(popupId).style.marginTop = "0px";
			document.getElementById(IframeID).removeAttribute('src');
			$("#ifrm3").contents().find("body").html("");
			$("#ifrm3").contents().find('p.highLightMsg').hide();
			$("#ifrm3").contents().find('p.highLightErr').hide();
		
			if (funcUrl) {
				funcUrl;
			}
		} catch (e) {
			top.$('.framehide').remove();
			top.document.getElementById(popupId).style.display = "none";
			top.document.getElementById(popupId).style.height = "0px";
			top.document.getElementById(popupId).style.width = "0px";
			top.document.getElementById(popupId).style.marginLeft = "0px";
			top.document.getElementById(popupId).style.marginTop = "0px";
			top.document.getElementById(IframeID).removeAttribute('src');
			$("#ifrm3").contents().find("body").html("");
			$("#ifrm3").contents().find('p.highLightMsg').hide();
			$("#ifrm3").contents().find('p.highLightErr').hide();
			if (funcUrl) {
				funcUrl;
			}
		}

	//}, 200);
}
				function getRetailerLocs() {
				var vRetailId = document.retailer.retailId.value;
				document.retailer.action = "deleteretailerlocation.htm";
				document.retailer.method = "get";
				document.retailer.submit();		
			}
			
		</script>
</head>
<body onload="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-rtlr.png"
					alt="ScanSee" width="400" height="54" /> </a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />, </span></li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<!--
				<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div> -->
			<div class="roleRtContPnl">
				<div class="contBlock">
					<form:form commandName="retailer" name="retailer"
						enctype="multipart/form-data" action="uploadretlocimg.htm">


						
						<form:hidden path="retailId" value="${sessionScope.RetailerId}" />
						<form:hidden path="retailName"
							value="${sessionScope.RetailerName}" />
						<form:hidden path="retailLocationId" />
						<form:hidden path="retailerLocIds" />
						<form:hidden path="searchKey" value="${sessionScope.searchakey}" />
						<form:hidden path="city" value="${sessionScope.city}" />
						<form:hidden path="state" value="${sessionScope.state}" />
						<form:hidden path="pageNumber" />
						<form:hidden path="pageFlag" />
						<form:hidden path="locationJson" />
						<form:hidden path="filterCategory" id ="filterCategory"/>
						<form:hidden path="retailLowerLimit" />
						<form:hidden path="retailLocLowerLimit" />
						<form:hidden path="rowIndex" />
						<form:hidden path="filters" id ="filters"/>
						<form:hidden path="filterValues" id ="filterValues"/>
						
						
						<fieldset class="mrgnBtm">
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" cellspacing="0" cellpadding="0" border="0"
									class="grdTbl">
									<tbody>
										<tr>
											<td width="16%" class="Label"
												style="border-right: 1px solid rgb(218, 218, 218);">Search
												By</td>
											<td width="24%"
												style="border-right: 1px solid rgb(218, 218, 218);"><form:select
													path="searchBy" cssClass="textboxBig">
													<form:option value="">--Select--</form:option>
													<c:if test="${sessionScope.searchBy ne 'SI'}">
														<form:option value="SI">Store Identification</form:option>
													</c:if>
													<c:if test="${sessionScope.searchBy eq 'SI'}">
														<form:option value="SI" selected="selected">Store Identification</form:option>
													</c:if>
													<c:if test="${sessionScope.searchBy eq 'address'}">
														<form:option value="address" selected="selected">Store Address</form:option>
													</c:if>
													<c:if test="${sessionScope.searchBy ne 'address'}">
														<form:option value="address">Store Address</form:option>
													</c:if>
												</form:select></td>
											<td width="60%" colspan="2"><c:if
													test="${sessionScope.locSearchkey ne null && !empty sessionScope.locSearchkey}">
													<form:input path="locSearchkey"
														value="${sessionScope.locSearchkey}"
														onkeypress="searchRetailerLocOnKeyPress(event)" />
												</c:if> <c:if
													test="${sessionScope.locSearchkey eq null || empty sessionScope.locSearchkey}">
													<form:input path="locSearchkey"
														onkeypress="searchRetailerLocOnKeyPress(event)" />
												</c:if> <a href="javascript:void(0)"> <img
													src="images/searchIcon.png" alt="Search" width="20"
													height="17" title="Search" onclick="searchRetailerLoc()" />
											</a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</fieldset>
					
				</div>
				<center>
					<span class="highLightTxt" style="font-weight:bold;color:#00559c;"> <c:out
							value="${requestScope.message}" /> </span>
							 <span class="highLightErr"
						style="font: red;"> <c:out
							value="${requestScope.errorMessage}" /> </span>
				</center>
				<fieldset class="relative">
					<div class="overlay"></div>
					<legend>${sessionScope.RetailerName} - Locations</legend>
					<div id="roleScrlGrd" class="grdCont searchGrd roleHrzScrl">
						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0"
							cellpadding="0" width="240%">
							<tbody>
								<tr class="header">
									<td width="4%" align="center"><c:if
											test="${!empty sessionScope.RetailerLocationList}">
											<label><input type="checkbox" name="deleteAll" />
											</label>
										</c:if></td>
									<td width="6%">Location ID</td>
									<td width="10%" onclick="sortColumn('RetailerName')">
										<div class="txtwrap">
											<a href="#" title="Store Identification"> Store
												Identification <c:if
													test="${!empty sessionScope.RetailerNameImg}">
													<img src="${sessionScope.RetailerNameImg}" width="11"
														height="6" />
												</c:if> </a>
										</div></td>
									<td width="10%"><label class="mandTxt">Address</label></td>
									<td width="9%"><label class="mandTxt">State</label></td>
									<td width="9%"><label class="mandTxt">City</label></td>
									<td width="5%"><label class="mandTxt">Postal Code</label>
									</td>
									<td width="15%">Website URL</td>
									<td width="8%"><label class="mandTxt">Latitude</label></td>
									<td width="8%"><label class="mandTxt">Longitude</label></td>
									<td width="10%"><label class="mandTxt">Phone</label></td>
									<td width="10%">Sub Category</td>
										<td width="10%">Select Filter</td>
									<td width="10%" align="center">Upload Logo</td>
									<td align="center">Delete</td>
								</tr>
								<c:set var="count" value="0"></c:set>
								<c:set var="rowIndex" value="0"/>
								<c:forEach items="${sessionScope.RetailerLocationList}"
									var="item">
									<c:set var="count" value="${count + 1}"></c:set>
									<tr id="${item.retailLocationId}">
										<td align="center"><input type="checkbox"
											name="checkboxDel" value="${item.retailLocationId}" /></td>
										<td align="left"><input type="text"
											value="${item.retailLocationId}" name="retailLocationId"
											disabled="disabled" /></td>
										<td align="left"><c:choose>
												<c:when
													test="${item.storeIdentification ne null && !empty item.storeIdentification}">
													<input type="text" value="${item.storeIdentification}"
														name="storeIdentification" disabled="disabled" />
												</c:when>
												<c:otherwise>
													<input type="text" value="N/A" name="storeIdentification"
														disabled="disabled" />
												</c:otherwise>
											</c:choose></td>
										<td><input type="text" value="${item.address1}"
											name="address" /></td>
										<td><select name="state" id="state${count}"
											class="textboxBig"
											onchange="loadCity(${count},'${item.city}');"
											city="${item.city}">
												<option value="">--Select--</option>
												<c:forEach items="${sessionScope.statesListCreat}" var="s">
													<c:choose>
														<c:when test="${item.state ne s.stateabbr}">
															<option value="${s.stateabbr}">
																<c:out value="${s.stateName}"></c:out>
															</option>
														</c:when>
														<c:otherwise>
															<option value="${s.stateabbr}" selected="selected">
																<c:out value="${s.stateName}"></c:out>
															</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
										</select></td>
										<td>
											<div id="cityDiv${count}">
												<select name="city" id="city" class="textboxBig">
													<option value="">--Select--</option>
												</select>
											</div></td>
										<td><input type="text" value="${item.postalCode}"
											name="postalCode" onchange="isNumeric(this.value,${count});"
											maxlength="5" /></td>
										<td><input type="text" value="${item.websiteURL}"
											name="websiteURL" maxlength="75" /></td>
										<td><input type="text" value="${item.latitude}"
											name="latitude" onkeypress="return isLatLong(event);" /></td>
										<td><input type="text" value="${item.longitude}"
											name="longitude" onkeypress="return isLatLong(event);" /></td>
										<td><input type="text" value="${item.phone}" name="phone"
											maxlength="10" onkeypress="return isNumberKey(event)" /></td>
									 <td align="center">
											<input type="button" value="Edit Category" class="btn tgl-overlay" 
											onclick="showRetailSubCategory('<c:out value="${item.retailLocationId}"/>')" title="Edit Sub Category"/>
										</td> 
										 <td align="center">
											<input type="button" value="Select Filter" class="btn tgl-overlay selectFilters" 
											 title="Edit Filter""></input>
										</td> 
										<form:hidden
													path="retailLocationId" value="${item.retailLocationId}" />
										<td>
										<div class="img-row">
										<span class="col">
										<form:hidden path="gridImgLocationPath" 
														value="${item.gridImgLocationPath}" />
														<form:hidden path ="uploadImage" value="${item.uploadImage}"/>
															<form:hidden path="imgLocationPath"
														value="${item.imgLocationPath}" />
										<img src="${item.imgLocationPath}" width="30" height="30"
										name="${item.retailLocationId}" alt="image" onerror="this.src = '/ScanSeeAdmin/images/dfltImg.png';"
										class="img-preview" />
										</span>
										<input type="file" name="imageFile" class="textboxBig mrgnTopSmall" id="img${item.retailLocationId}"
										onchange="uploadRetailLocationLogo(this, ${rowIndex })" />
										</div>																			
										</td>
										<td width="5%" align="center"><a href="#"
											onclick="deleteRetailerLoc('${item.retailLocationId}')">
												<img src="images/delicon.png" alt="edit" title="Delete"
												width="18" height="18" /> </a></td>
									</tr>
									<c:set var="rowIndex" value="${rowIndex+1}"/>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="pagination">
						<p>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
				
				<div class="mrgnTop" align="right">
					<input type="button" class="btn" id="save" value="Save"
						title="Save" onclick="numRows();" /> <input type="button"
						class="btn" id="delete" value="Delete" title="Delete"
						onclick="deleteRetailerLocs();" disabled="disabled" /> <input
						type="button" class="btn" id="Back" value="Back" title="Back"
						onclick="back();" />
				</div>
			</div>
</form:form>

		<div class="ifrmPopupPannelImage" id="ifrmPopup"
			style="display: none;">
			<div class="headerIframe">
				<img src="images/popupClose.png" class="closeIframe"
					alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
				allowtransparency="yes" width="100%" style="background-color: White">
			</iframe>
		</div>

			<div class="ifrmPopupPannel" id="ifrmPopup2" style="display: none;">
				<div class="headerIframe">
					<img src="images/popupClose.png" class="closeIframe" alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup2','ifrm2')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="auto" id="ifrm2" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
			
				<div class="ifrmPopupPannel" id="ifrmPopup3" style="display: none;">
				<div class="headerIframe">
					<img src="images/popupClose.png" class="closeIframe" alt="close"
						onclick="javascript:closeFilterIframePopup('ifrmPopup3','ifrm3')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader">Select Filters</span>
				</div>
				<iframe frameborder="0" scrolling="auto" id="ifrm3" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
			

		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
	</div>
	



	<script>

			$("input[name='latitude']").focusout(function() {
			/* Matches	 90.0,-90.9,1.0,-23.343342
			Non-Matches	 90, 91.0, -945.0,-90.3422309*/
				var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
						  var vLat = $(this).parents('tr').find('input[name="latitude"]').val();
						  //Validate for Latitude.
						  if (0 === vLat.length || !vLat || vLat == "") {
							return false;
						 } else {
							if(!vLatLngVal.test(vLat)) {
						      alert("Latitude are not correctly typed");
						      $(this).parents('tr').find('input[name="latitude"]').val("");
						      return false;
						  }
						 }
			});

			$("input[name='longitude']").focusout(function() {
			/* Matches	180.0, -180.0, 98.092391
			Non-Matches	181, 180, -98.0923913*/
			var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
				//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
						  var vLong =  $(this).parents('tr').find('input[name="longitude"]').val();
						  //Validate for Longitude.
						  if (0 === vLong.length || !vLong || vLong == "") {
							return false;
						 } else {
							if(!vLatLngVal.test(vLong)) {
						      alert("Longitude are not correctly typed");
						      $(this).parents('tr').find('input[name="longitude"]').val("");
						      return false;
						  }
						 }
			});

			</script>
</body>
</html>
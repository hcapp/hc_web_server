<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Coupon Administration</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">

	function searchCoupon(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			document.coupon.sortOrder.value = "ASC";
			document.coupon.columnName.value = "CouponName";
			document.coupon.action = "searchcoupon.htm";
			document.coupon.method = "post";
			document.coupon.submit();

		}
	}
	function searchCouponDet() {
		document.coupon.sortOrder.value = "ASC";
		document.coupon.columnName.value = "CouponName";
		document.coupon.action = "searchcoupon.htm";
		document.coupon.method = "post";
		document.coupon.submit();
	}
	function showExpireCoupons() {
		var showExpire = $('input[name$="showExpirebtn"]').val();
		if (showExpire == "Show Expired") {
			document.coupon.showExpire.value = "false";
		} else {
			document.coupon.showExpire.value = "true";
		}
		document.coupon.action = "showexpiredcoupon.htm";
		document.coupon.method = "post";
		document.coupon.submit();
	}

	function sortColumn(columnName) {
		document.coupon.columnName.value = columnName;
		document.coupon.action = "sortcolumn.htm";
		document.coupon.method = "post";
		document.coupon.submit();
	}
	function addCoupon() {
		document.coupon.action = "addcoupon.htm";
		document.coupon.method = "post";
		document.coupon.submit();
	}
	function editCoupon(couponId) {
		document.coupon.couponId.value = couponId;
		document.coupon.action = "editcoupon.htm";
		document.coupon.method = "post";
		document.coupon.submit();
	}

	function deleteCoupon(couponId) {
		var r = confirm("Do you want to really expire the Coupon?");
		if (r == true) {
			document.coupon.couponId.value = couponId;
			document.coupon.couponDiscountType.value = "Coupon";
			document.coupon.action = "deletecoupon.htm";
			document.coupon.method = "post";
			document.coupon.submit();
		}
	}
</script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo.png" alt="ScanSee"
					width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">
				<div class="contBlock">
					<form:form name="coupon" commandName="coupon"
						acceptCharset="ISO-8859-1">
						<form:hidden path="couponId"
							value="${requestScope.COUPON.couponId}" />
						<form:hidden path="mode" value="add" />
						<form:hidden path="hiddenViewable" />
						<form:hidden path="hiddenExternalCoupon" />
						<form:hidden path="sortOrder" />
						<form:hidden path="columnName" value="${sessionScope.columnName}" />
						<form:hidden path="searchFlag" />
						<form:hidden path="showExpire" value="${sessionScope.showExpire}" />
						<form:hidden path="couponDiscountType" />
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<tr>
										<td class="Label" width="18%"><label for="Supr_srchRbts">
												Coupon Name </label></td>
										<td width="60%"><c:if
												test="${sessionScope.searchKey ne null && !empty sessionScope.searchKey}">
												<form:input path="searchKey"
													onkeypress="searchCoupon(event)"
													value="${sessionScope.searchKey}" />
											</c:if> <c:if
												test="${sessionScope.searchKey eq null || empty sessionScope.searchKey}">
												<form:input path="searchKey"
													onkeypress="searchCoupon(event)" />
											</c:if> <a href="#" title="Search Coupon"> <img
												src="images/searchIcon.png" onclick="searchCouponDet()"
												alt="Search" width="20" height="17" />
										</a></td>
										<td width="22%"><input type="button" class="btn"
											title="Add Coupon" value="Add" onclick="addCoupon()" /></td>
									</tr>
								</table>
							</div>
						</fieldset>
						<c:if test="${sessionScope.showExpire eq 'true'}">
							<div class="mrgnBtm" align="center">
								<input type="button" class="btn" id="showExpirebtn"
									name="showExpirebtn" value="Show Expired" title="Show Expired"
									onclick="javascript:showExpireCoupons()" />
							</div>
						</c:if>
						<c:if test="${sessionScope.showExpire eq 'false'}">
							<div class="mrgnBtm" align="center">
								<input type="button" class="btn" id="showExpirebtn"
									name="showExpirebtn" value="Do Not Show Expired"
									title="Do Not Show Expired"
									onclick="javascript:showExpireCoupons()" />
							</div>
						</c:if>
					</form:form>
				</div>
				<center>
					<span class="highLightTxt"><c:out
							value="${requestScope.message}" /> </span> <span class="highLightErr"><c:out
							value="${requestScope.errorMessage}" /> </span>
				</center>
				<fieldset>
					<legend>Coupon Details</legend>
					<div id="scrlGrd" class="grdTbl searchGrd">
						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0"
							cellpadding="0" width="100%">
							<tbody>
								<tr class="header">
									<td width="20%" onclick="sortColumn('CouponName')">
										<div class="txtwrap">
											<a href="#" title="Coupon Name"> Coupon Name <c:if
													test="${!empty sessionScope.CouponNameImg}">
													<img src="${sessionScope.CouponNameImg}" width="11"
														height="6" />
												</c:if>
											</a>
										</div>
									</td>
									<td width="12%">Discount Type</td>
									<td width="12%" onclick="sortColumn('CouponDiscountAmount')"><a
										href="#" title="Discount Amount">Discount Amount <c:if
												test="${!empty sessionScope.CouponDiscountAmountImg}">
												<img src="${sessionScope.CouponDiscountAmountImg}"
													width="11" height="6" />
											</c:if>
									</a></td>
									<td width="27%">Coupon Description</td>
									<td width="7%" onclick="sortColumn('CouponStartDate')"><a
										href="#" title="Start Date">Start Date <c:if
												test="${!empty sessionScope.CouponStartDateImg}">
												<img src="${sessionScope.CouponStartDateImg}" width="11"
													height="6" />
											</c:if>
									</a></td>
									<td width="11%" onclick="sortColumn('CouponExpireDate')"><a
										href="#" title="Expiry Date">Expiry Date <c:if
												test="${!empty sessionScope.CouponExpireDateImg}">
												<img src="${sessionScope.CouponExpireDateImg}" width="11"
													height="6" />
											</c:if>
									</a></td>
									<td colspan="2" align="center">Action</td>
								</tr>
								<c:forEach items="${sessionScope.COUPONLIST}" var="item">
									<tr>
										<td align="left"><div class="txtwrap ">
												<c:choose>
													<c:when test="${item.couponUrl eq ''}">
														<c:out value="${item.couponName}"></c:out>
													</c:when>
													<c:otherwise>
														<a href="#" title="Click here to open URL"
															onclick="openIframePopup('ifrmPopup','ifrm','${item.couponUrl}',500,800,'URL')">
															<c:out value="${item.couponName}"></c:out>
														</a>
													</c:otherwise>
												</c:choose>
											</div></td>
										<td align="left"><c:out
												value="${item.couponDiscountType}" /></td>
										<td><c:out value="${item.couponDiscountAmount}" /></td>
										<td><div class="txtwrap">
												<c:out value="${item.couponShortDescription}" />
											</div></td>
										<td><c:out value="${item.couponStartDate}" /></td>
										<td><c:out value="${item.couponExpireDate}" /></td>
										<td width="6%" align="center"><a href="#"
											onclick="editCoupon('<c:out value="${item.couponId}"/>')"><img
												src="images/edit_icon.png" alt="edit" title="Edit"
												width="25" height="19" /> </a></td>
										<td width="5%" align="center"><c:choose>
												<c:when test="${item.expireFlag eq 1}">
												</c:when>
												<c:otherwise>
													<a href="#"
														onclick="deleteCoupon('<c:out value="${item.couponId}"/>');">
														<img src="images/delicon.png" alt="delete" title="Expire"
														width="18" height="18" />
													</a>
												</c:otherwise>
											</c:choose></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="pagination">
						<p>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
			</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
			<div class="headerIframe">
				<img src="images/popupClose.png" class="closeIframe" alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
				height="100%" allowtransparency="yes" width="100%"
				style="background-color: White"> </iframe>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>

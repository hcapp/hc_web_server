<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Add Event Category</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
</head>
<body class="whiteBG">

	<div class="contBlock">
		<form name="AddCategoryForm" commandName="AddCategoryForm"
			acceptCharset="ISO-8859-1">
			<input type="hidden" name="catId" value="${requestScope.cateId}"
				id="evtcatid" /> <input type="hidden" name="actionType" />
			<fieldset>
				<legend></legend>
				<div class="contWrap">
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="grdTbl">
						<tbody>


							<tr>
								<div></div>
								<td width="30%" class="Label">Category Name</td>
								<td><c:choose>
										<c:when
											test="${requestScope.catename ne null && !empty requestScope.catename}">
											<input type="text" name="catName" id="ctgryNm"
												value="${requestScope.catename}" maxlength="30" />
										</c:when>
										<c:otherwise>
											<input type="text" name="catName" id="ctgryNm" maxlength="30" />
										</c:otherwise>
									</c:choose> <i class="emptyctgry">Please Enter Category Name</i>
									<p class="dupctgry">Category Name already exists</p></td>
							</tr>
						</tbody>
					</table>
					<div class="navTabSec evtMrgnRt" align="right">
						<c:choose>
							<c:when
								test="${requestScope.catename ne null && !empty requestScope.catename}">
								<input name="save" value="Update Category" type="button"
									class="btn" id="update" onclick="updateCategory();" />
							</c:when>
							<c:otherwise>
								<input name="save" value="Save Category" type="button"
									class="btn" id="save" onclick="addEvtCategory();" />
							</c:otherwise>
						</c:choose>


						<input value="Clear" type="button" class="btn" id="clr"
							onclick="clearCategory()" />
					</div>
				</div>
			</fieldset>
		</form>
	</div>

</body>
</html>
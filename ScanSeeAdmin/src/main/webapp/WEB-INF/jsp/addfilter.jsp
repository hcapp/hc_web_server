<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Filter</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>

<script type="text/javascript">

	$(document).ready(function() {
		
		
		var vCatID = $("#fCategoryHidden").val();
		var vCatList = vCatID.split(",");

		for(var i = 0; i < vCatList.length; i++) {
			$("#fCategory option[value='" + vCatList[i] +"']").attr("selected", true);
		}
		
		var totLen = $("#fCategory option").length;
		var totSelLen = $("#fCategory option:selected").length;
		
		if(totLen == totSelLen) {
			$('#chkAllLoc').attr("checked", true);
		} else {
			$('#chkAllLoc').removeAttr("checked");
		}

		$('#fCategory').change(function() {
			document.filterform.fCategoryHidden.value = $('#fCategory').val();
			var totOpt = $(this).find("option").length;
			var totOptSlctd = $(this).find("option:selected").length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$("#chkAllLoc").removeAttr('checked');
			}
		});
		

		var vBackBtn = $("#backButton").val();
		if (vBackBtn === 'no') {
			document.getElementById('back').style.visibility = 'hidden';
		}
		
	});

	function searchFilter() {
		document.filterform.action = "managefilters.htm";
		document.filterform.method = "post";
		document.filterform.submit();
	}

	function callNextPage(pagenumber, url) {
		document.filterform.pageNumber.value = pagenumber;
		document.filterform.pageFlag.value = "true";
		document.filterform.action = url;
		document.filterform.method = "post";
		document.filterform.submit();
	}

	function saveFilter() {
		var vFValue = "";
		var vFOption = $('input:radio[name=filterOption]:checked').val();

		if (vFOption === 'yes') {
			var vFValue = $("#slctFilter option").map(function() {
				return $(this).attr("value");
			}).get().join(',');
		}
		document.filterform.hiddenFValue.value = vFValue;
		document.filterform.action = "savefilter.htm";
		document.filterform.method = "POST";
		document.filterform.submit();
	}

	function clearFilterForm() {
		var r = confirm("Do you want to clear this form?");
		if (r == true) {
			$(".error").remove();
			document.filterform.filterName.value = "";
			document.filterform.fCategory.value = "";
			document.filterform.fCategoryHidden.value = "";
			document.filterform.fValue.value = "";
			document.filterform.hiddenFValue.value = "";
			$("input[name='chkAllLoc']").attr("checked", false);
			$("#fltrNo").attr('checked', true).trigger('change');
			$("#filterAlert").remove();
			$("#fltrTxtValue").val("");
			$("#fValueMsg").val("");
		}
	}

	function backManageFilters() {
		var r = confirm("Do you want to leave this page without saving the filter?");
		if (r == true) {
			document.filterform.action = "managefilters.htm";
			document.filterform.searchKey.value ="";
			document.filterform.method = "post";
			document.filterform.submit();
		}
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("fCategory");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}

</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="javascript:void(0);"> <img
					src="images/scansee-logo-filter.png" alt="ScanSee" width="400"
					height="54" />
				</a>
			</div>

			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span></li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<!--Header ends-->
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">
				<div class="contBlock">


					<fieldset>
						<legend>Add Filter</legend>
						<form:form name="filterform" commandName="filterform"
							acceptCharset="ISO-8859-1">
							<form:hidden path="viewName" value="addfilter" />
							<form:hidden path="fCategoryHidden" />
							<!--<form:hidden path="filterId"  id="filterId"/>-->
							<form:hidden path="hiddenFValue"  id="hiddenFValue"/>
							<form:hidden path="backButton" id="backButton" />
							<form:hidden path="searchKey"/>
							<c:if test="${requestScope.errorMessage ne null }">
							<div id="message">
								<center><label style="font-weight:bold;color:red;" ><c:out value="${requestScope.errorMessage}" /></label></center>
							</div>
								
							</c:if>

							<div class="contWrap">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<tbody>
										
															
										<tr>
													
         									 <td width="20%" class="Label" style="border-right: 1px solid rgb(218, 218, 218);"><label class="mandTxt">Filter Name</label></td>
         									  <td><form:errors cssClass="error" path="filterName" cssStyle="color:red"></form:errors>
											
											  <form:input type="text" path="filterName"
													name="filterName" id="filterName" class="textboxBig"
													maxlength="40" tabindex="1" /></td>
       
										</tr>


										<tr>
											<td><label class="mandTxt">Category</label> &nbsp; (Hold CTRL to select multiple categories)</td>
											<td class="remove-rt-brdr"><form:errors cssClass="error"
													path="fCategory" cssStyle="color:red" /> <form:select
													path="fCategory" name="fCategory" id="fCategory"
													
													class="textboxBig multiSelDropDown" multiple="true" tabindex="2" >
													<!--  <option value="">---Select---</option>-->
													<c:forEach items="${sessionScope.fCategoryList}" var="item">
														<option value="${item.categoryId }">${item.categoryName}</option>
													</c:forEach>
												</form:select>
												
												<p class="infoTxt">
												       <input type="checkbox" name="chkAllLoc" id="chkAllLoc" onclick="SelectAllLocation(this.checked);" tabindex="3" /> <label for="checkAll">Select All Categories</label>
									              </label> <br> <br>
												</p>
												
												</td>
										</tr>


										<tr>
											<td class="Label">Filter Options</td>
											<td class="remove-rt-brdr multipleInputs"><form:errors
											cssClass="error" path="filterOption" cssStyle="color:red"></form:errors>
											<form:radiobutton
													path="filterOption" name="filterOption" value="no"
													id="fltrNo" tabindex="3" /> <label for="fltrNo">No</label>
												<form:radiobutton path="filterOption" name="filterOption"
													value="yes" id="fltrYes" tabindex="4" /> <label
												for="fltrYes"> Yes</label>
										</tr>


										<tr class="fltrValue">
											<td class="Label"><label class="mandTxt">Values</label></td>
											<td class="remove-rt-brdr dynamicList">
											<form:errors cssClass="error" path="fValue" cssStyle="color:red"/>
												<div class="cntrls">
													<form:select path="fValue" name="fValue" size="1"
														multiple="multiple" id="slctFilter"
														class="textboxBig multiSelDropDown" tabindex="5">
															<c:forEach items="${sessionScope.filterValuesList}" var="fvalues">
															
																<option value="${fvalues.fValueId}">${fvalues.fValueName}</option>
																                         
												             </c:forEach>

													</form:select>
												</div>
												<div class="slct-cntrls cntrls">
													<a href="javascript:void(0);" class="slct-cntrl-add jsactn"
														id="addOption" title="add"></a> <a
														href="javascript:void(0);" class="slct-cntrl-edit jsactn"
														id="editOption" title="edit"></a> <a
														href="javascript:void(0);" class="slct-cntrl-del jsactn"
														id="delOption" title="delete"></a>
												</div>
												
												<div id="fValueMsg" class="msg" > </div>
												 <p class="info">instruction</p>
											</td>
										</tr>


										<tr class="fltrValueRow">
											<td class="Label">Filter value</td>

											<td class=""><input type="text" id="fltrTxtValue"
												name="fltrTxtValue" class="textboxBig" maxlength="30" tabindex="6" />
												<span
												class="mrgnTop">
												<input type="button" value="Save"
													class="btn" id="addNewOptn" tabindex="7" /> 
													
													<input type="button" value="Save"
													class="btn" id="editNewOptn" tabindex="7" /> 
												<input type="button" value="Cancel"
													class="btn jsactn" id="cnclOption" tabindex="8" /></span></td>
										</tr>
									</tbody>
								</table>


								<div align="right" class="navTabSec mrgnRt">
								<input type="button" id="saveFilterBtn" value="Save" onclick="saveFilter();" class="btn" title="save" tabindex="10" />
							    <input type="button" value="Clear" onclick="clearFilterForm();" class="btn" title="clear" tabindex="11" /> 
								<input type="button" id="back" value="Back" onclick="backManageFilters();" class="btn"  title="back" tabindex="12" />
								</div>
							</div>
						</form:form>
					</fieldset>
				</div>
			</div>
		</div>

		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>

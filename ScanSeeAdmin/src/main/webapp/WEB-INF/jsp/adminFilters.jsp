<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Image Crop</title>
<link href="/ScanSeeAdmin/styles/styles.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/ScanSeeAdmin/styles/jquery.Jcrop.css" />
<script src="/ScanSeeAdmin/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeAdmin/scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="/ScanSeeAdmin/scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="/ScanSeeAdmin/scripts/jquery.tablescroll.js" type="text/javascript"></script>
<script src="/ScanSeeAdmin/scripts/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="/ScanSeeAdmin/scripts/global.js"></script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
$(document).ready(function (){

$('p.highLightMsg').hide();

$('p.highLightErr').hide();
});

</script>

</head>
<body class="whiteBG">
	<form:form name="associateFilters" commandName="associateFilters">
		<form:hidden path="filterCategory" id ="filterCategory"/>
		<form:hidden path="filters" id ="filters"/>
		<form:hidden path="filterValues" id ="filterValues"/> 
		<form:hidden path="retailId" value="${requestScope.retId}"/> 
		<form:hidden path="retailLocationId" value="${requestScope.retailLocationId}"/> 
		
	
			
		<div class="cntrlPnl">
		
	<center>
	<input type="button" id="associate" value="Associate" title="Associate" class="btn"/>
	<p class="highLightMsg">
	
					<span> Location Filters are saved successfully. </span> </p>
			<!--<p class="highLightErr">				<span style="font: red;">Error occurred while updating filters. </span></p>-->
			
			</center>
		</div>
		<div id="message" class="highLightErr"	style="font: red;">
			<center>
				Please associate Business categories for the retailer and then select filters and continue to save.
			</center>
		</div>
		<div class="contBlock">
			<fieldset class="">
				<div class="tabularPnl" id="tabularPnl">
					
				</div>
			</fieldset>
		</div>
	</form:form>
	<script>

$(window).load(function() {
	    	$("#associate").click(function() {
			
			$('p.highLightMsg').hide();

			$('p.highLightErr').hide();
			var mainFilter = "";
			var subFilter = "";
			var i = 0;
			var curMainFilter = "";
			var curSubFilter = "";
			var previouClass = "";
			var selectedCat = "";
			/*var curCtgrySlct = $("div.col").find(".treeview .main:checked").parents(".col");
			var curCtgrySlct = $("div.col").find(".treeview .main:checked").each(function() {

			$(this).parents("div.col").attr("class");


			});

			alert(curCtgrySlct);*/
			$("div.col").find(".treeview .main:checked").parents(".col").each(function(){
				if(i > 0) {
					if(curMainFilter !== "") {
						if(mainFilter !== "") {
							mainFilter += "!~~!" + curMainFilter;
						} else {
							mainFilter = curMainFilter;
						}
					} else {
						if(mainFilter !== "") {
							mainFilter += "!~~!" + 'NULL';
						} else {
							mainFilter = 'NULL';
						}
					}
					if(curSubFilter != "") {
						if(previouClass === "main") {
							curSubFilter += "!~~!" + 'NULL';
						}
						if(subFilter !== "") {
							subFilter += "!~~!" + curSubFilter;
						} else {
							subFilter = curSubFilter;
						}
					} else {
						if(subFilter !== "") {
							subFilter += "!~~!" + 'NULL';
						} else {
							subFilter = 'NULL';
						}
					}

					curMainFilter = "";
					curSubFilter = "";
					previouClass = "";
					i = 0;
				}
				var ulId = $(this).attr("class").split(" ").slice(-1);
				if(selectedCat === "") {
					selectedCat = ulId;
				} else {
					selectedCat += "," + ulId;
				}
				$("#"+ulId+" input:checkbox:checked").each(function(){					
					var curCls = $(this).attr("class");
					if(curCls === "main") {						
						if(previouClass === "") {
						  curMainFilter = $(this).val();
						} else if(previouClass === "main") {
						  curMainFilter += "|" + $(this).val();
						  if(curSubFilter != "") {
							curSubFilter += "!~~!" + 'NULL';
						  } else {
							curSubFilter = 'NULL';
						  }
						} else if(previouClass === "sub") {
						  curMainFilter += "|" + $(this).val();
						}							
					} else if(curCls === "sub") {
						if(curSubFilter != "") {
							if(previouClass === "main") {
								curSubFilter += "!~~!" + $(this).val();
							} else {
								curSubFilter += "|" + $(this).val();
							}
						} else {
							curSubFilter = $(this).val();
						}
					}
					previouClass = curCls;
				});
				i++;
			});
				
			if(curMainFilter !== "") {
				if(mainFilter !== "") {
					mainFilter += "!~~!" + curMainFilter;
				} else {
					mainFilter = curMainFilter;
				}
			} else {
				if(mainFilter !== "") {
					mainFilter += "!~~!" + 'NULL';
				} else {
					mainFilter = 'NULL';
				}
			}
			if(curSubFilter != "") {
				if(previouClass === "main") {
					curSubFilter += "!~~!" + 'NULL';
				} 
				if(subFilter !== "") {
					subFilter += "!~~!" + curSubFilter;
				} else {
					subFilter = curSubFilter;
				}
			} else {
				if(subFilter !== "") {
					subFilter += "!~~!" + 'NULL';
				} else {
					subFilter = 'NULL';
				}
			}
			
			if(mainFilter === "") {
				mainFilter = 'NULL';
				subFilter = 'NULL';
			}
			
								
		var ifrmcont = $( "#ifrm3" ).contents();	 
			

			/*	var selBusCat = $(".treeview").find(".main:checked").parents("ul").map(function() {
					return this.id;
				  }).get().join();*/
				  
			var selBusCat = $(".treeview").find(".main:checked").parents("ul").map(function() {
				return this.id;
			  }).get().reverse().join();
			var cat=selBusCat;
				
					 
			var hiddenCat = cat.split(",");
			var hiddenCatLength = hiddenCat.length;
			var hiddenMainFilter = mainFilter.split("!~~!");
			var mainLength = hiddenMainFilter.length;
			var hiddenSubFilter = subFilter.split("!~~!");
			var filter = "";
			var subFilters = "";
			var startLength = 0;
			if(hiddenCatLength > mainLength) {
				for(var m = 0; m < hiddenCatLength; m++) {
					if($("div."+hiddenCat[m]).length <= 0) {
						var assocaited = false;
						for(i = 0; i < hiddenCatLength; i++) {	
							if(i > 0) {
								if(i == m) {
									filter += "!~~!" + "NULL";
									assocaited = true;									
								} else {									
									if(assocaited == false) {
										filter += "!~~!" + hiddenMainFilter[i];
										var split = hiddenMainFilter[i].split("|");
										startLength = startLength + split.length;
									} else {
										filter += "!~~!" + hiddenMainFilter[i-1];
									}
								}
							} else {
								if(i == m) {
									filter = "NULL";
									assocaited = true;	
								} else {
									filter = hiddenMainFilter[i];
									var split = hiddenMainFilter[i].split("|");
									startLength = startLength + split.length;
								}
							}
						}
						assocaited = false;
						var hidsubFil = filter.split("!~~!");
						var hidsubFilLeng = hidsubFil.length;
						var leng = 0;
						for(var k = 0; k < hidsubFilLeng; k++) {
							var split = hidsubFil[k].split("|");
							leng = leng + split.length;
						}
						for(var j = 0; j < leng; j++) {
							if(j > 0) {
								if(j == startLength) {
									subFilters += "!~~!" + "NULL";
									assocaited = true;	
								} else {
									if(assocaited == false) {
										subFilters += "!~~!" + hiddenSubFilter[j];
									} else {
										subFilters += "!~~!" + hiddenSubFilter[j-1];
									}
								}
							} else {
								if(j == startLength) {
									subFilters = "NULL";
									assocaited = true;									
								} else {
									subFilters = hiddenSubFilter[j];
								}
							}
						}
					} 
				}
			} else {
				filter = mainFilter;
				subFilters = subFilter;
			}
			
		//	closeIframePopup('ifrmPopup3','ifrm3');
			
			window.top.document.getElementById("filters").value=filter;
			window.top.document.getElementById("filterValues").value=subFilters;
			window.top.document.getElementById("filterCategory").value=cat;
			document.associateFilters.filterValues.value=subFilters;
			document.associateFilters.filters.value=filter;			
			document.associateFilters.filterCategory.value=cat;
			
			 $.ajaxSetup({
				cache : false
		  });
		  
		  $.ajax({
				type : "GET",
				url : "associatefilter.htm",
				data : {
					'filter' : filter,
					'subFilters' :subFilters,
					'cat':cat
				},
				success : function(response) {    
			
			if(response === "Success")
			{
			$('p.highLightMsg').show();
$('p.highLightErr').hide();
			}else{
			$("p.highLightMsg").hide();
//$("p.highLightErr").show();
			}
			
			
			}
			
		});
		
		});
		
		});
	</script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Promotion Details</title>
		<link rel="stylesheet" type="text/css" href="styles/styles.css" />
		<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="scripts/jquery.js"></script>
		<script type="text/javascript" src="scripts/global.js"></script>
		<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
		<script>
		$(document).ready(function(){
			$('.txtAreaSmall').val(function(i,v){     
				return v.replace(/^\s+|\s+$/g,""); 
			});
		});
		
		</script>
		<script type="text/javascript">			
			function backToMain() {
				document.promotion.showExpire.value = '${sessionScope.showExpire}';
				document.promotion.action = "showexpiredpromotion.htm";
				document.promotion.method = "post";
				document.promotion.submit();
			}
		</script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="header_logo" class="floatL">
					<a href="#"> 
						<img src="images/scansee-logo-prmtn.png" alt="ScanSee" width="400" height="54" /> 
					</a>
				</div>
				<div id="header_link" class="floatR">
					<ul>
						<li>
							Welcome 
							<span class="highLightTxt">
								<c:out value="${sessionScope.userName}" />, 
							</span>
						</li>
						<li>
							<img src="images/logoutIcon.gif" alt="Logut" align="left" title="Logut" /> 
							<a href="logout.htm" title="Logut">&nbsp;Logout</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="content">
				
			
	    		<div class="contBlock">
	    			<form:form name="promotion" commandName="promotion" acceptCharset="ISO-8859-1">
						<form:hidden path="promotionId" value="${requestScope.promotionId}"/>
						<form:hidden path="showExpire" value="${sessionScope.showExpire}"/>
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
	      				<fieldset id="Promotions" class="dscntType">
	      					<legend>Promotion Details</legend>
	      					<div class="contWrap">
	        					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl">
	         						<tbody>
	            						<tr>
	             							<td width="18%" class="Label">
									              <label for="prTitle">
									              	<b>Title</b>
									              </label>
											</td>
											<td>
											<c:if test="${!empty sessionScope.promotionDetails.pagetitle || null ne sessionScope.promotionDetails.pagetitle}">
									        		<c:out value="${sessionScope.promotionDetails.pagetitle}"></c:out>									        		
									        	</c:if>
									        	<c:if test="${empty sessionScope.promotionDetails.pagetitle || null eq sessionScope.promotionDetails.pagetitle}">
									        		<c:out value="N/A"></c:out>
									        	</c:if>
											</td>
									        <td class="Label">
									        	<label for="Name">
									            	<b>Image</b>
									         	</label>
									        </td>
									        <td>
									        	<c:if test="${!empty sessionScope.promotionDetails.image || null ne sessionScope.promotionDetails.image}">
									        		<img src="${sessionScope.promotionDetails.image}" alt="image" height="70px" width="70px" onerror="this.src = 'images/imgIcon.png';"/>									        		
									        	</c:if>
									        	<c:if test="${empty sessionScope.promotionDetails.image || null eq sessionScope.promotionDetails.image}">
									        		<img src='images/imgIcon.png' alt="image"/>
									        	</c:if>									          	
									        </td>
									 	</tr>
									    <tr>
									    	<td class="Label">
									         	<label for="cpnFcval">
									             	<b>Terms &amp; Condition</b>
									             </label>
									      	</td>
									       	<td colspan="3">
									        	<label for="cpnDesc">
									        	<c:if test="${!empty sessionScope.promotionDetails.termsAndCondition || null ne sessionScope.promotionDetails.termsAndCondition}">
									        		<c:out value="${sessionScope.promotionDetails.termsAndCondition}"></c:out>									        		
									        	</c:if>
									            </label>
									            <p>ScanSee is not the sponsor of the deal</p>
									        </td>
									   	</tr>
										<tr>
									    	<td class="Label">
									        	<label for="disctype">
									            	<b>Rules</b>
									            </label>
									        </td>
									        <td width="23%">
									        	<textarea readonly="readonly" class="txtAreaSmall">
									        	<c:if test="${!empty sessionScope.promotionDetails.rules || null ne sessionScope.promotionDetails.rules}">
									        		<c:out value="${sessionScope.promotionDetails.rules}"></c:out>									        		
									        	</c:if>
									        	<c:if test="${empty sessionScope.promotionDetails.rules || null eq sessionScope.promotionDetails.rules}">
									        		<c:out value="N/A"></c:out>
									        	</c:if>
									        	</textarea>
									        </td>
									        <td class="Label" width="17%">
									          	<label for="discPercnt"><b>Short Description</b></label>
									        </td>
									        <td width="32%">
									        	<textarea readonly="readonly" class=txtAreaSmall >
										        	
										        	<c:if test="${!empty sessionScope.promotionDetails.shortDescription || null ne sessionScope.promotionDetails.shortDescription}">
										        		<c:out value="${sessionScope.promotionDetails.shortDescription}" escapeXml="false"></c:out>									        		
										        	</c:if>
										        	<c:if test="${empty sessionScope.promotionDetails.shortDescription || null eq sessionScope.promotionDetails.shortDescription}">
										        		<c:out value="N/A"></c:out>
										        	</c:if>
										        	
									         	</textarea>
									        </td>
									  	</tr>
									    <tr>
									       	<td class="Label">
									       		<label for="csd">
									       			<b>Long Description</b>
									       		</label>
									       	</td>
									       	<td width="23%" align="left">
									       		<textarea readonly="readonly" class=txtAreaSmall>
									       			<c:if test="${!empty sessionScope.promotionDetails.longDescription || null ne sessionScope.promotionDetails.longDescription}">
									       				<c:out value="${sessionScope.promotionDetails.longDescription}" escapeXml="false"></c:out>								        		
									        		</c:if>
									        		<c:if test="${empty sessionScope.promotionDetails.longDescription || null eq sessionScope.promotionDetails.longDescription}">
									        			<c:out value="N/A"></c:out>
									        		</c:if>
									       		</textarea>
									       	</td>
											<td width="17%" class="Label">
									        	<label for="ced"><b> Start Date</b></label>
									        </td>
									        <td width="32%" align="left">
									       		<c:if test="${!empty sessionScope.promotionDetails.startDate || null ne sessionScope.promotionDetails.startDate}">
									        		<c:out value="${sessionScope.promotionDetails.startDate}"></c:out>									        		
									        	</c:if>
									        	<c:if test="${empty sessionScope.promotionDetails.startDate || null eq sessionScope.promotionDetails.startDate}">
									        		<c:out value="N/A"></c:out>
									        	</c:if>
									        </td>
										</tr>
										<tr>
											<td class="Label">
												<b>End Date</b>
											</td>
											<td align="left">
												<c:if test="${!empty sessionScope.promotionDetails.endDate || null ne sessionScope.promotionDetails.endDate}">
									        		<c:out value="${sessionScope.promotionDetails.endDate}"></c:out>									        		
									        	</c:if>
									        	<c:if test="${empty sessionScope.promotionDetails.endDate || null eq sessionScope.promotionDetails.endDate}">
									        		<c:out value="N/A"></c:out>
									        	</c:if>
											</td>
									        <td class="Label">
									        	<b>Quantity</b>
									        </td>
									        <td align="left">
									        <c:if test="${!empty sessionScope.promotionDetails.quantity || null ne sessionScope.promotionDetails.quantity}">
									        		<c:out value="${sessionScope.promotionDetails.quantity}"></c:out>									        		
									        	</c:if>
									        	<c:if test="${empty sessionScope.promotionDetails.quantity || null eq sessionScope.promotionDetails.quantity}">
									        		<c:out value="N/A"></c:out>
									        	</c:if>
									        </td>
										</tr>
									</tbody>
								</table>
	        					<div class="navTabSec mrgnRt">
	          						<div align="right">
	            						<input name="save2" value="Back" type="button" class="btn" id="Back" title="Back" onclick="backToMain();" />
									</div>
								</div>
							</div>
						</fieldset>
					</form:form>
					<fieldset id="Promotions" class="dscntType">
						<legend>Registered User</legend>
						<center>
							<span class="highLightErr" style="font: red;">
								<c:out value="${requestScope.errorMessage}"/> 
							</span>
						</center>
						<div class="grdCont searchGrd" id="scrlGrd">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" class="stripeMe" id="cpnLst">
								<tbody>
									<tr class="header hvrEfct">
								    	<td width="20%" style="border-right: 1px solid rgb(193, 228, 240);">
								         	Name
								         	<a class="sortOprtn" href="#">
								         		<img width="11" height="6" title="Sort by name" alt="sort" src="images/sorticon.png">
								            </a>
										</td>
										<td width="13%" style="border-right: 1px solid rgb(193, 228, 240);">
											Email Address
											<a class="sortOprtn" href="#">
												<img width="11" height="6" title="Sort by discount type" alt="sort" src="images/sorticon.png">
								            </a>
								         </td>
									</tr>
								    <c:forEach items="${sessionScope.promotionUsers}" var="item">
								    	<tr class="">
								        	<td align="left" style="border-right: 1px solid rgb(193, 228, 240);">
								        		<c:out value="${item.userName }"></c:out>
								        	</td>
								        	<td align="left" style="border-right: 1px solid rgb(193, 228, 240);">
								        		<c:out value="${item.email }"></c:out>
								        	</td>
								        </tr>
								    </c:forEach>
								</tbody>
							</table>
						</div>
	      				<div class="pagination">
	        				<p>
	        					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" /> 
							</p>
						</div>
	      			</fieldset>
	    		</div>
	  		</div>
			</div>
	  		<div id="footer">
				<div id="ourpartners_info">
					<div class="floatR" id="followus_section">
						<p>&nbsp;</p>
					</div>
					<div class="clear"></div>
				</div>
				<div id="footer_nav">
					<div class="clear"></div>
					<div id="footer_info">
						Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All	rights reserved
					</div>
					<p align="center">&nbsp;</p>
				</div>
			</div>
		</div>
	</body>
</html>
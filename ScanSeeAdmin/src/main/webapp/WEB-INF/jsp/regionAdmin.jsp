<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Region Administration</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>

<script type="text/javascript">

$(document).ready(function ()
		{
	
	$("alrtClose").click(function (){		
		$("alertInfo").hide();
			
	});
	
		});


function openZipCode(adminID) {
	openIframePopup('ifrmPopup', 'ifrm',
			'fetchpostalcode.htm?hubCitiID=' + adminID, 400,
			800, 'View PostalCode');
}
function hubcitiHome()
{
	document.RegionForm.searchKey.value = '';
	document.RegionForm.action = "displayhubcitiadmin.htm";
	document.RegionForm.method = "POST";
	document.RegionForm.submit();
	
	}
function displaySearchRegions(event)
{
var keyCode = (event.keyCode ? event.keyCode : event.which);
	if(keyCode == 13)
	{
		document.RegionForm.action = "displayregions.htm";
	document.RegionForm.method = "POST";
	document.RegionForm.submit();
		}else if(event == ''){
	document.RegionForm.action = "displayregions.htm";
	document.RegionForm.method = "POST";
	document.RegionForm.submit();
		}else{
		return true;
		}
		
	
}

function showDeactiveRegion(showRegion)
{
	
	if(showRegion == "showDective")
		{
		
		document.RegionForm.showDeactivated.value = true;
		}else{
		document.RegionForm.showDeactivated.value =  false;
			
		}
	document.RegionForm.action = "displayregions.htm";
	document.RegionForm.method = "POST";
	document.RegionForm.submit();
		
	}
	
	function deactivateRegion(regId,active)
	{
		if(active == true )
			{
			var msg = confirm("Are you sure you want deactivate this Region Admin");
			
			}else{
				var msg = confirm("Are you sure you want activate this Region Admin");
				
			}
				
		if(msg == true)
			{
			document.RegionForm.lowerLimit.value = ${requestScope.lowerLimit};
			document.RegionForm.active.value=active;
			document.RegionForm.hubCitiID.value=regId;
			document.RegionForm.action = "deactivateRegion.htm";
			document.RegionForm.method = "POST";
			document.RegionForm.submit();
			
	}
		}
	function createRegion()
	{

		document.RegionForm.action = "createregion.htm";
		document.RegionForm.method = "GET";
		document.RegionForm.submit();
		
	}
	
	function editRegionAdmin(regionId)
	{
		document.RegionForm.hubCitiID.value=regionId;
		document.RegionForm.action = "editregadmin.htm";
		document.RegionForm.method = "POST";
		document.RegionForm.submit();
	
	}

</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hubciti.png"
					alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<form:form name="RegionForm" commandName="RegionForm"
				acceptCharset="ISO-8859-1">


				<input type="hidden" name="pageNumber" />
				<input type="hidden" name="pageFlag" />
				<input type ="hidden" name="hubCitiID"/>
				<input type="hidden" name="active"/>
				<input type="hidden" name="lowerLimit"/>
				<form:input type="hidden" name="showDeactivated" path="showDeactivated" value="${sessionScope.showDeactivated}" />
				<!--Left Navigation Starts here-->
				<div class="floatL" id="vNav">
					<div class="vNavtopBg"></div>
					<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
					<div class="vNavbtmBg"></div>
				</div>


				<!--Left Navigation ends here-->
				<!--Right container starts here-->
				<div class="rtContPnl floatR">
					<div id="subnav" class="tabdSec">
						<ul>
							<li><a href="#" class="" id="prodDet"
								onclick="hubcitiHome()"><span>HubCiti Administration</span></a></li>
							<li><a class="active" href="#" id="reviewDet"><span>Region
										Administration</span></a></li>
						</ul>
					</div>



					<div class="contBlock mrgnTop">
						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<tr>
										<td colspan="3" class="header">Search</td>
									</tr>
									<tr>
										<td class="Label" width="20%"><label for="hubCitiName">Region
												Name</label></td>
										<td width="58%"><form:input type="text" path="searchKey"
												id="searchKey" onkeypress="displaySearchRegions(event)"
												maxlength="30" /> &nbsp; <label></label> <a href="#"><img
												src="images/searchIcon.png" alt="Search" width="20"
												height="17" onclick="displaySearchRegions('')"
												title="Search Region Admin" /></a></td>
												
										<td width="22%"><input type="button" class="btn"
											value="Add"
											onclick="createRegion()" title="Add Region Admin"/></td>
									</tr>
								</table>
							</div>
						</fieldset>
					</div>
						<center>
					<c:if
						test="${requestScope.noregfound ne null && !empty requestScope.noregfound}">
					<!-- 	<p class="alertInfo">
							${requestScope.noregfound} <a class="alrtClose"
								href="javascript:void(0);" title="Close">x</a>
						</p> -->
						<span class="highLightErr"><c:out value="${requestScope.noregfound}"></c:out></span>
					</c:if>
					
					<c:if test="${requestScope.deactivateMsg ne null && !empty requestScope.deactivateMsg}">
					
					<span class="highLightTxt"><c:out value="${requestScope.deactivateMsg}" /> </span>
					
					</c:if>
					
					<c:if test="${requestScope.message ne null && !empty requestScope.message}">
					
					<span class="highLightTxt"><c:out value="${requestScope.message}" /> </span>
					
					</c:if>
				
					 
					
				</center>

					<c:if test="${sessionScope.showDeactivated eq false}">
						<div class="mrgnBtm" align="center">
							<input type="button" class="btn" id="showExpirebtn"
								name="showExpirebtn" value="Show Deactivated"
								title="Show Deactivated"
								onclick="javascript:showDeactiveRegion('showDective')" />
						</div>
					</c:if>
					<c:if test="${sessionScope.showDeactivated eq true}">
						<div class="mrgnBtm" align="center">
							<input type="button" class="btn" id="showExpirebtn"
								name="showExpirebtn" value="Do Not Show Deactivated"
								title="Do Not Show Deactivated"
								onclick="javascript:showDeactiveRegion('donotshowDective')" />
						</div>
					</c:if>
					<c:if
						test="${sessionScope.regionlst ne null && !empty sessionScope.regionlst}">
						<fieldset>
							<legend>Region Details</legend>
							<div id="scrlGrd" class="grdTbl searchGrd">
								<table id="cpnLst" class="stripeMe" border="0" cellspacing="0"
									cellpadding="0" width="100%">
									<tbody>
																
										<tr class="header">
											<td width="30%">Region Name</td>
											<td width="27%">State | City | Postal Code</td>
											<td width="40%">Username</td>
											<td colspan="3" align="center">Action</td>
										</tr>

										<!-- Displaying Region Details  -->
										<c:forEach items="${sessionScope.regionlst}" var="region">

											<tr>
												<td align="left">${region.hubCitiName}</td>
												<td align="center"><input type="button" value="View"
													class="btn"
													onclick="openZipCode(${region.hubCitiID})" title="Show Postal Code" /></td>
												<td>${region.userName}</td>
												<td width="5%" align="center"><a href="#" onclick="editRegionAdmin(${region.hubCitiID})"><img
														src="images/edit_icon.png" alt="edit" title="Edit Region Admin"
														width="25" height="19" /></a></td>
														
														<c:choose>
														<c:when test="${region.active eq true}">
														<td width="9%" align="center"><a href="#"><img
														src="images/deactivate_icon.png" alt="Deactivate" 
														onclick="deactivateRegion(${region.hubCitiID},true)"
														title="Deactivate Region" width="18" height="18"  /></a></td>
														
														</c:when>
														
														<c:otherwise>
														<td width="9%" align="center"><a href="#"><img
														src="images/activate_icon.png" alt="Deactivate" onclick="deactivateRegion(${region.hubCitiID},false)"
														title="Activate Region" width="18" height="18"  /></a></td>
														
														</c:otherwise>
														
														</c:choose>
																																
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>



							<div class="pagination">
								<p>
									<page:pageTag totalSize="${sessionScope.pagination.totalSize}"
										nextPage="4" url="${sessionScope.pagination.url}"
										currentPage="${sessionScope.pagination.currentPage}"
										pageRange="${sessionScope.pagination.pageRange}" />

								</p>

							</div>





						</fieldset>



					</c:if>

				</div>
			</form:form>
			<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="images/popupClose.png" class="closeIframe" alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
			<!--Right container ends here-->
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>



	</div>
</body>
</html>

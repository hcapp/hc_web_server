<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add hubcitiAdmin</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	var state = '${requestScope.state}';
	if(state != "" && state != null)
	{
		loadCity();		
	}
	else
	{
		$('input[name$="chkAllZip"]').attr('disabled', 'disabled');		
	}
	
	$("#postalCodes").live("change", function() {
		var totOpt = $('#postalCodes option').length;
		var totOptSlctd = $('#postalCodes option:selected').length;
		if(totOpt > 0)
		{
			if (totOpt == totOptSlctd) {
				$('#chkAllZip').attr('checked', 'checked');
			} else {
				$('#chkAllZip').removeAttr('checked');
			}
		}
		else
		{
			$('#chkAllZip').removeAttr('checked');
		}
	});		
});

</script>
<script type="text/javascript">	

	function backToMain() {
		var r = confirm("Are you sure you want to leave this page without saving the hubciti details?");
		if (r == true) {
			document.hubciti.lowerLimit.value = ${requestScope.lowerLimit};
			document.hubciti.action = "displayhubcitiadmin.htm";
			document.hubciti.method = "post";
			document.hubciti.submit();
		}
	}
	
	function loadCity() {
		var stateCode = $('#state').val();
		var city = '${requestScope.city}';
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "fetchcities.htm",
			data : {
				'statecode' : stateCode,
				'city' : city
			},
	
			success : function(response) {
				$('#cityDiv').html(response);
				
				loadPostalCode();
			},
			error : function(e) {
				
			}
		});
	}
	
	function loadPostalCode() {
		var stateCode = $('#state').val();
		var city = $('#city').val();
		if(city != null && city != "")
		{
			$('input[name$="chkAllZip"]').removeAttr('disabled');
		}
		else
		{
			$('input[name$="chkAllZip"]').attr('disabled', 'disabled');
		}
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "fetchzip.htm",
			data : {
				'state' : stateCode,
				'city' : city
			},
	
			success : function(response) {
				$('#PCDiv').html(response);
				$("#postalCodes").trigger("change");

				var srcLstCnt = $("#postalCodes option").length;
				if(srcLstCnt==0){
					$('input[name$="chkAllZip"]').attr('disabled', 'disabled');	
				}
			},
			error : function(e) {
				
			}
		});
	}
	
	function save() 
	{
	
	
		document.hubciti.lowerLimit.value = 0;
		document.hubciti.action = "createhubcitiadmin.htm";
		document.hubciti.method = "post";
		document.hubciti.submit();
	}
	
	function SelectAllZip(checked) {
		var sel = document.getElementById("postalCodes");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
</script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hubciti.png" alt="ScanSee" width="400" height="54" /> </a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out value="${sessionScope.userName}" />, </span></li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left" title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a></li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix" style="min-height:450px;">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">
				<fieldset id="Coupons" class="dscntType">
					<legend>HubCiti Details</legend>
					<div class="contWrap">
							<form:form name="hubciti" commandName="hubciti" acceptCharset="ISO-8859-1">
								<form:hidden path="showDeactivated" value="${sessionScope.showDeactivated}"/>
								<form:hidden path="searchKey"/>
								<form:hidden path="lowerLimit"/>
								<form:hidden path="active" id="active"/>
								<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl">
									<tbody>
										<tr>
											<td colspan="5" class="header">Create New hubciti Admin</td>
										</tr>
										<tr>
											<td class="Label" width="19%"><label class="mandTxt" for="hubCitiName">HubCiti Name</label>
											</td>
											<td colspan="5">
												<form:errors cssStyle="color:red" path="hubCitiName"></form:errors>
												<form:input path="hubCitiName"/>
											</td>
										</tr>
										<tr>
											<td class="Label"><label>State</label></td>
											<td>
												<select name="state" id="state" class="textboxBig" onchange="loadCity();">
													<option value="">--Select--</option>
													<c:forEach items="${sessionScope.statesListCreat}" var="s">
														<c:choose>
															<c:when test="${requestScope.state ne s.stateabbr}">
																<option value="${s.stateabbr}">
																	<c:out value="${s.stateName}"></c:out>
																</option>
															</c:when>
															<c:otherwise>
																<option value="${s.stateabbr}" selected="selected">
																	<c:out value="${s.stateName}"></c:out>
																</option>
															</c:otherwise>
														</c:choose>																												
													</c:forEach>
												</select>
											</td> 
											<td class="Label" width="12%">
												<label>City</label>
											</td>
											<td colspan="2">										
												<div id="cityDiv">
													<select name="city" id="city" class="textboxBig">
														<option value="">--Select--</option>
													</select>
												</div>
											</td>
										</tr>
										<tr>
											<td class="Label" valign="top"><label class="mandTxt">Postal Code</label>
                  								<p class="infoTxt">(Hold ctrl or alt to select multiple Postal Codes)</p>
											</td>
											<td width="30%" class="remove-rt-brdr">	
												<div id="PCDiv">
													<select name="postalCodes" size="1" multiple="multiple" id="postalCodes" class="textboxBig multiSelDropDown" style="font-size: 11px;">
													</select>
												</div>
												<p class="infoTxt">
													<input type="checkbox" name="chkAllZip" id="chkAllZip" onclick="SelectAllZip(this.checked);" disabled="disabled"/> 
													<label for="checkAll">Select All</label>
												</p>
											</td>	
											<td valign="top" class="remove-rt-brdr" align="center">
												<p class="btnCntrl mrgnTop">
													<a href="javascript:void(0);"  id="moveright" onclick="move_list_items('postalCodes','toPostalCode','defaultZipcode');">
														<img src="images/MoveRt.png" alt="Move Right" width="24" height="16" title="Move Right" />
													</a>
												</p>
											</td>
											<td valign="top" class="remove-rt-brdr">
												<form:errors cssStyle="color:red" path="postalCodes"></form:errors>
												<select name="toPostalCode" size="1" multiple="multiple" id="toPostalCode" class="textboxBig multiSelDropDown" style="font-size: 11px;">
												<c:if test="${sessionScope.selectedPostalCodeList ne null || !empty sessionScope.selectedPostalCodeList}">
													<c:forEach items="${sessionScope.selectedPostalCodeList}" var="postalCode">
														<option value="${postalCode.postalCode}" title="${postalCode.location}" isAssocaited="${postalCode.isAssocaited}">${postalCode.location}</option>
													</c:forEach>
												</c:if>
												</select>  
												<p class="infoTxt">Selected Postal Codes</p> 
											</td>               
											<td valign="top">
												<a href="javascript:void(0);" title="Delete" id="delZipcode">
													<img src="images/deleteRec.png" width="24" height="16" alt="delete" class="mrgnTop" />
												</a>
											</td>                                 
										</tr>
										<tr>
											<td class="Label">
												<label class="mandTxt" for="cityExp">City Experience</label>
											</td>
											<td>
												<form:errors cssStyle="color:red" path="citiExperience"></form:errors>
												<form:input path="citiExperience"/>
											</td>
												<td class="Label"><label class="mandTxt" for="userName">Username</label>
											</td>
											<td colspan="2">
												<form:errors cssStyle="color:red" path="userName"></form:errors>
												<form:input path="userName"/>
											</td>
										</tr>
									
										<tr>
											<td class="Label"><label class="mandTxt" for="emailId">Email ID</label>
											</td>
											<td>
												<form:errors cssStyle="color:red" path="emailID"></form:errors>
												<form:input path="emailID"/>
											</td>
											<td class="Label">
												<label class="mandTxt" for="cityExp">Default Postal Code</label>
											</td>
											<td colspan="2">
												<form:errors cssStyle="color:red" path="defaultZipcode"></form:errors>
												<form:select path="defaultZipcode">
													<form:option value="0">----Select----</form:option>
													
													<c:forEach items="${sessionScope.defltPstlCdeLst}" var="postalCode">
														<form:option value="${postalCode}">${postalCode}</form:option>
													</c:forEach>
													
												</form:select>
											</td>
											</tr>
											<tr>
												<td class="Label"><label class="mandTxt" for="salesPersonEmail">HubCiti Sales Contact</label>
												</td>
											
												<td>
													<form:errors cssStyle="color:red" path="salesPersonEmail"></form:errors>
													<form:input path="salesPersonEmail"/>
												</td>
												
												
												<td class="Label"><label class="mandTxt" for="emails">Claim Business Email ID's</label>
												</td>
											
												<td colspan="2">
													<form:errors cssStyle="color:red" path="emails"></form:errors>
													<form:input path="emails" maxlength="300"/>
													<br><p class="infoTxt">[Allows 5 emails separated by semicolon(;)]</p>
													</td>
												</td>
											</tr>
										
									</tbody>
								</table>
							</form:form>
							<div class="navTabSec mrgnRt">
								<div align="right">
									<input name="Save" value="Save" type="button" class="btn hltBtn" id="save" onclick="save();" title="Save"/> 
									<input type="button" onclick="backToMain();" id="Back" class="btn" value="Back" name="back" title="Back">
								</div>
							</div>
					</div>
				</fieldset>
			</div>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div> 
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nationwide Admin</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		var apiPartners = $("#hidAPIPartners").val();		

		$("input[name='apiPartners']").each(function () {
            $(this).attr("checked", false);
        });

		if(typeof apiPartners != 'undefined' && "" !== apiPartners) {
			var arr = apiPartners.split(',');			
			for (var i = 0; i < arr.length; i++) {
		          $("#"+arr[i]).attr('checked', 'checked');
		    }			
		}
		
		var isNationwide = $("input[name='isNationwide']:checked").val();
		
		if(isNationwide == "true") {
			$(".nationwide").show();
		} else {
			$(".nationwide").hide();
		}
		
		$(".error").hide();
		
		$("input[name='isNationwide']").change(function() {
			var isNationwide = $("input[name='isNationwide']:checked").val();			
			if(isNationwide == "true") {
				$(".nationwide").show();
			} else {
				$(".nationwide").hide();
			}
			$(".error").hide();
		});
		
	});
	
	function SaveNationwideInfo() {		
		var isNationwide = $("input[name='isNationwide']:checked").val();
		var apiPartner = $("input[name='apiPartners']:checked").length;
		
		if(isNationwide == "true" && apiPartner == 0) {
			$(".error").show();
		} else {		
			closeIframePopup('ifrmPopup','ifrm');
			parent.location.reload(true);
			window.parent.location.reload(true);
			document.nationwideform.action = "saveNationwide.htm";
			document.nationwideform.method = "POST";
			document.nationwideform.submit();
		}
			
	}
</script>
</head>
<body class="whiteBG">
	<div class="contBlock">
		<fieldset>
			<div class="contWrap">
				<form:form name="nationwideform" commandName="nationwideform" action="#">
					<form:hidden path="hidAPIPartners" id="hidAPIPartners"/>
					<form:hidden path="hubCitiID" id="hubCitiID"/>
					<form:hidden path="lowerLimit" />
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl">
						<tbody>
							<tr>
								<td width="100%" class="multipleInputs"><label for="nationwide_yes"> <form:radiobutton path="isNationwide" value="true" />Yes
								</label> <label for="nationwide_no"> <form:radiobutton path="isNationwide" value="false" />No
								</label></td>
							</tr>
							<tr class="nationwide">
								<td class="cstm-hdr">Nationwide</td>
							</tr>
							<tr class="error">
								<td class="highLightErr">Please select atleast one API partner</td>
							</tr>
							<tr class="nationwide">
								<td class="Label"><ul class="nw-list">
										<c:forEach items="${sessionScope.apiPartners}" var="apiPartner">
											<li><label for="chkbx1"> <form:checkbox path="apiPartners" value="${apiPartner.apiPartnerID}" id="${apiPartner.apiPartnerID}"/>
													${apiPartner.apiPartnerName}
											</label></li>
										</c:forEach>
									</ul></td>
							</tr>
						</tbody>
					</table>
				</form:form>
				<div class="navTabSec mrgnRt" align="right">
					<input name="Save" value="Save" type="button" class="btn" id="save" onclick="SaveNationwideInfo();"/> 
					<input type="button" id="Cancel" class="btn" value="Cancel" name="Save" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')" />
				</div>
			</div>
		</fieldset>
	</div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>ScanSeeAdmin</title>
		<link rel="stylesheet" type="text/css" href="styles/styles.css" />
		<script type="text/javascript" src="scripts/jquery.js"></script>
		<script type="text/javascript" src="scripts/global.js"></script>
	</head>
	<body onload="resizeDoc();">
		<div id="loginWrpr">
			<div id="header">
				<div id="header_logo" class="floatL"> 
					<a href="#"> 
					<img src="images/scansee-logo-Admin.png" alt="ScanSee" width="400" height="54"/>
					</a> 
				</div>
			</div>
			
					<form name='f' action="<c:url value='j_spring_security_check' />" method='POST' acceptCharset="ISO-8859-1">
						<div id="content">
				<div id="loginSec">
				<h1>&nbsp;</h1>
					<c:if test="${not empty error}">
						<div class="errorblock">
							Your login attempt was not successful, try again.<br /> Caused :
							${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
						</div>
					</c:if>
		
						
						<ul>
							<li>
							
							 	<input  name="j_username" name="userName" type="text" class="floatR" id="userName" size="30"/>
							 	<span class="floatL">
							 		User Name
							 	</span>
							 	<span class="clear"></span>
								<i class="errDisp">Please enter User Name</i>
							</li>							
							<li class="clear"></li>
							<li>
							
								<input  name='j_password' name="password" type="password" class="floatR" id="password" size="30"/>
							 	<span class="floatL">
							 		Password
							 	</span> 
							 	<span class="clear"></span>
								<i class="errDisp">Please enter Password </i>
							</li>
							<li class="clear"></li>
							<li>
								<img src="images/loginBtn.png"
									alt="login" onclick="validateUserForm(['#password','#userName'],'li',chkUser);" align="right"/>
							</li>
						</ul>
						<div class="loginBtm">&nbsp;</div>
					</div>
			</div>
				<div id="footer">
					<div id="ourpartners_info">
						<div class="floatR" id="followus_section">
							<p>&nbsp;</p>
						</div>
						<div class="clear"></div>
					</div>
					<div id="footer_nav">
						<div class="clear"></div>
						<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All rights reserved</div>
						<p align="center">&nbsp;</p>
					</div>
			  </div>
			</form>
		</div>
	</body>
</html>
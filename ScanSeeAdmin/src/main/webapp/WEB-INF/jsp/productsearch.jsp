<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="java.util.ArrayList,common.pojo.Product"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript">

	function submitSearch() {
		var val = document.product.searchKey.value;
		if (val == null || val == "" || val == "null") {
			alert("Please Enter valid Search key");
		} else {
			document.product.action = "searchproduct.htm";
			document.product.method = "post";
			document.product.submit();
		}
	}

	function searchOnKeyPress(event)
	{
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') 
		{
			var val = document.product.searchKey.value;
			if (val == null || val == "" || val == "null") {
				alert("Please Enter valid Search key");
				event.preventDefault();
				return false;
			} else {
				document.product.action = "searchproduct.htm";
				document.product.method = "post";
				document.product.submit();
			}
			
		}
	}

	function clearSearch() {
		document.product.searchKey.value = "";
	}

	function validateSelection() {
		var productName = []
		$('input[name="pUpcChk"]:checked').each(function() {
			productName.push(this.value);
		});
		var productID = []
		$('input[name="pUpcChk"]:checked').each(function() {
			productID.push(this.id);
		});
		
		if (productName.length > 0) {
			closeIframePopup('ifrmPopup', 'ifrm', callBackFunc());
		} else {
			alert("Please select Products");
		}
	}
</script>
</head>
<body class="whiteBG">
	<div class="contBlock">
		<form:form commandName="product" name="product">
			<fieldset>
				<legend>
					Product Search
				</legend>
				<div class="contWrap">
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="grdTbl">
						<tbody>
							<tr>
								<td width="18%" class="Label">
									<label for="couponName">
										Product Name/UPC
									</label>
								</td>
								<td width="29%">
									<form:input path="searchKey" maxlength="30" onkeypress="searchOnKeyPress(event);"/>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="navTabSec mrgnRt" align="right">
						<input type="button" value="Search"	onclick="submitSearch();" class="btn" title="Search"/>
						<input type="button" value="Clear"	onclick="clearSearch() ;" class="btn" title="clear"/>
					</div>
				</div>
			</fieldset>
			</form:form>
		<fieldset class="popUpSrch">
			<legend>Product UPC Details</legend>
			<div id="" class="grdCont searchGrd">
				<div align="right" class="btmPadding">
				<center>
				<span style="font: red;"><c:out
						value="${requestScope.errorMsg}" /> </span>
			</center>
					<c:if
						test="${requestScope.ProdList !=null  && !empty requestScope.ProdList}">
						<span class="floatL">Select checkboxes and click on
							Associate button</span>
					</c:if>
					<input name="associate" value="Associate"
						onclick="javascript:validateSelection()" type="button" class="btn"
						id="associate" title="Associate"/>
				</div>
				<div class="scrollTbl">
					<table id="cpnLst1" class="stripeMe" border="0" cellspacing="0"
						cellpadding="0" width="100%">
						<tbody>
							<tr class="header">
								<td width="18%">Product Name</td>
								<td width="15%">UPC</td>
								<td width="30%">Short Description</td>
								<td width="32%">Category</td>
								<td align="center">Action</td>
							</tr>
							<c:if test="${pdtInfoList ne null && ! empty pdtInfoList}">
							<c:forEach items="${sessionScope.pdtInfoList}"
								var="earlierAddedPdts">
								<tr>
									<td><c:out value="${earlierAddedPdts.productName}" /></td>
									<td><c:out value="${earlierAddedPdts.scanCode}" /></td>
									<td><c:out value="${earlierAddedPdts.productShortDescription}" />
									</td>
									<td><c:out value="${earlierAddedPdts.category}" /></td>
									<td width="12%" align="center"><input type="checkbox"
										checked="checked" 
										id="${earlierAddedPdts.productId}" name="pUpcChk" value="${earlierAddedPdts.productName}" 
										
										 />
									</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:forEach items="${requestScope.ProdList}" var="item">
							<tr>
								<td><c:out value="${item.productName}" /></td>
								<td><c:out value="${item.scanCode}" /></td>
								<td><c:out value="${item.productShortDescription}" /></td>
								<td><c:out value="${item.category}" /></td>
								<td width="6%" align="center">
									<input type="checkbox" id="${item.productId}" name="pUpcChk" value="${item.productName}"/>
								</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</fieldset>
	</div>
</body>
</html>
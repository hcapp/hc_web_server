<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ScanSee</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link rel="stylesheet" type="text/css" href="styles/colorPicker.css" />
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="scripts/colorPicker.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="index.html"> <img src="images/scansee-logo-Admin.png"
					alt="ScanSee" width="400" height="54" /></a>
			</div>
		</div>
		<div id="content">
			<a href="../ScanSeeAdmin/login.htm">
			<img src="images/ErrorOccured.png" alt="error" width="968" height="450" usemap="#Map" border="0"/></a>
			<map name="Map" id="Map">
				<area shape="rect" coords="745,37,887,59" href="../ScanSeeAdmin/login.htm" alt="Home" />
				<area shape="rect" coords="900,15,942,65" href="../ScanSeeAdmin/login.htm" alt="Home" />
			</map>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>

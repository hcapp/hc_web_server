<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="java.util.ArrayList,common.pojo.Product"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	$('#bCategoryIds option').click(function() {
		var totOpt = $('#bCategoryIds option').length;
		var totOptSlctd = $('#bCategoryIds option:selected').length;
		if (totOpt == totOptSlctd) {
			$('input[name$="chkAllCatgy"]').attr('checked', 'true');
		} else {
			$('input[name$="chkAllCatgy"]').removeAttr('checked');
		}
	});
	$("#bCategoryIds").change(function() {
		var totOpt = $('#bCategoryIds option').length;
		var totOptSlctd = $('#bCategoryIds option:selected').length;
		if (totOpt == totOptSlctd) {
			$('input[name$="chkAllCatgy"]').attr('checked', 'true');
		} else {
			$('input[name$="chkAllCatgy"]').removeAttr('checked');
		}
	});
});


	function editSubCategory() {
		document.subCategory.action = "editsubcategory.htm";
		document.subCategory.method = "post";
		document.subCategory.submit();
	}
	
	function SelectAllSubCategory(checked) {
		var sel = document.getElementById("bCategoryIds");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
	
	function onLoadCategoryIDs() {
	var actCnt = $("#bCategoryIds option").length;
	
		if (actCnt > 0) {
			var slcCnt = $("#bCategoryIds option:selected").length;
			if (actCnt == slcCnt) {
				$('input[name$="chkAllCatgy"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllCatgy"]').removeAttr('checked');
			}
		} else {
				$('#save').attr('disabled','disabled');
		}
	}
</script>

</head>
<body class="whiteBG">
	<div class="contBlock searchGrd relative">
		<form:form commandName="subCategory" name="subCategory">
			<form:hidden path="retailId"/>
			<form:hidden path="retailLocationId"/>
			<center>
					<span class="highLightMsg"> <c:out
							value="${requestScope.successMessage}" /> </span> <span class="highLightErr"
						style="font: red;"> <c:out
							value="${requestScope.errorMessage}" /> </span>
			</center>
		<fieldset>
			<div class="contWrap">
				<table width="100%" cellspacing="0" cellpadding="0" border="0"
					class="grdTbl">
					<tbody>
						<tr>
							<td width="35%" class="Label"><label for="ctgry">Category</label><p class="labelInfoTxt">(Hold CTRL to select
									multiple categories)</p></td>
							<td width="29%"><form:select path="bCategoryIds" id="bCategoryIds" class="txtAreaBox" size="1" multiple="true">
									<c:forEach items="${sessionScope.subCategory}" var="c">
										<c:choose>
											<c:when test="${c.associated == 0}">
												<form:option value="${c.categoryId}" label="${c.categoryName}"/>
											</c:when>
										<c:otherwise>
											<form:option value="${c.categoryId}" label="${c.categoryName}"  selected="selected"/>
										</c:otherwise>
										</c:choose>
									</c:forEach>
								</form:select></td>
								
								<td colspan="2" align="left" valign="top" class="Label"><label>
									<input type="checkbox" name="chkAllCatgy" id="chkAllCatgy"
									onclick="SelectAllSubCategory(this.checked);" /></label> 
									<p class="labelInfoTxt">Select All Category  </p><br> <br></td>
						</tr>
					</tbody>
				</table>
				<div class="navTabSec mrgnRt" align="right">
					<input name="Save" title="Save" value="Save" type="button" class="btn" id="save"  onclick="editSubCategory();" />
					<input name="Save" title="Click here to Close" value="Cancel" type="button" class="btn" id="Cancel"  onclick="javascript:closeIframePopup('ifrmPopup2','ifrm2')"/>
				</div>
			</div>
		</fieldset>
		</form:form>
	</div>
	
<script>
	onLoadCategoryIDs();
</script>
</body>
</html>
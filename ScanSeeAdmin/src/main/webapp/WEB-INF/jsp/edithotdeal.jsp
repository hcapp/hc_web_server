<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hot Deals</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	$('.txtAreaBigFit').val(function(i, v) {
		return v.replace(/^\s+|\s+$/g, ""); 
	});
	$('.txtAreaFit').val(function(i, v) {
		return v.replace(/^\s+|\s+$/g, ""); 
	});
	//alert("${sessionScope.hotDeal.dealStartHrs}")
	$('input#dtp').datepicker({
	showOn : 'both',
	buttonImageOnly : true,
	buttonImage : 'images/cal.gif'
	});
	$('.ui-datepicker-trigger').css("padding-left", "5px");

	$("input#endDT").datepicker({
		showOn : 'both',
		buttonImageOnly : true,
		buttonImage : 'images/cal.gif'
	}); 
	$('.ui-datepicker-trigger').css("padding-left", "5px");
	
	$("#price").keypress(function(e) {
		var charCode = (e.which) ? e.which : event.keyCode
		if ((charCode > 47 && charCode < 58) || charCode < 31 || charCode == 46)
		{
			return true;
		}
		return false;
	});
	$("#salePrice").keypress(function(e) {
		var charCode = (e.which) ? e.which : event.keyCode
		if ((charCode > 47 && charCode < 58) || charCode < 31 || charCode == 46)
		{
			return true;
		}
		return false;
	});
});

</script>
<script type="text/javascript">	

	function backToMain() {		
			document.hotdeal.action = "searchhotdeal.htm";
			document.hotdeal.method = "post";
			document.hotdeal.submit();
	}
	
	function loadCity() 
	{
		var stateCode = $('#Country').val();
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "fetchcity.htm",
			data : {
				'statecode' : stateCode,
				'city' : null
			},

			success : function(response) {
				$('#cityDiv').html(response);
			},
			error : function(e) {
				
			}
		});
	}
	
	function loadLocation() 
	{
		var state = $('#Country').val();
		var city = $('#city').val();
		var retailId = document.hotdeal.productHotDealID.value;
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "fetchretaillocations.htm",
			data : {
				'retailId' : retailId,
				'state' : state,
				'city' : null
			},

			success : function(response) {
				$('#categoryDiv').html(response);
			},
			error : function(e) {
				
			}
		});
	}

	function saveEdit() 
	{
		document.hotdeal.action = "savehotdeal.htm";
		document.hotdeal.method = "post";
		document.hotdeal.submit();
	}
</script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hotdealAdmin.png" alt="ScanSee" width="400" height="54" /> </a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out value="${sessionScope.userName}" />, </span></li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left" title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a></li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">
				<fieldset id="Coupons" class="dscntType">
					<legend>Hot Deal Details</legend>
					<div class="contWrap">
						<form:form name="hotdeal" commandName="hotdeal" acceptCharset="ISO-8859-1">
						<form:hidden path="productHotDealID" value="${sessionScope.hotDeal.productHotDealID}"/>
						<form:hidden path="showExpired" value="${sessionScope.showExpire}" />
						<form:hidden path="columnName" value="${sessionScope.columnName}" />
						<form:hidden path="searchKey" value="${sessionScope.searchKey}" /> 
						<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl">
							<tbody>
								<tr>
									<td colspan="4" class="header">Update Hot Deal Details</td>
								</tr>
								<tr>
									<td width="18%" class="Label"><label class="mandTxt" for="dealName">Hot Deal Name</label>
									</td>
									<td colspan="3">
										<form:errors cssStyle="color:red" path="hotDealName"></form:errors>
										<form:input path="hotDealName" value="${sessionScope.hotDeal.hotDealName}"/>
									</td>
								</tr>
								<tr>
									<td class="Label"><label class="mandTxt" for="price">Price</label>
									</td>
									<td>
										<form:errors cssStyle="color:red" path="price"></form:errors>
										<form:input path="price" value="${sessionScope.hotDeal.price}"/>
									</td>
									<td class="Label"><label class="mandTxt" for="salePrice">Sale Price</label>
									</td>
									<td>
										<form:errors cssStyle="color:red" path="salePrice"></form:errors>
										<form:input path="salePrice" value="${sessionScope.hotDeal.salePrice}"/>
									</td>
								</tr>
								<tr>
									<td class="Label"><label class="mandTxt" for="shortDesc">Short Description</label>
									</td>
									<td>
										<form:errors cssStyle="color:red" path="hotDealShortDescription"></form:errors>
										<spring:bind path="hotdeal.hotDealShortDescription">
											<TEXTAREA name="${status.expression }" class="txtAreaBigFit" >
												<c:out value="${sessionScope.hotDeal.hotDealShortDescription}"/>
											</TEXTAREA>
										</spring:bind>
									</td>
									<td><label for="longDesc">Long Description</label>
									</td>
									<td>
										<spring:bind path="hotdeal.hotDeaLonglDescription">
											<TEXTAREA name="${status.expression }" class="txtAreaBigFit" >
												<c:out value="${sessionScope.hotDeal.hotDeaLonglDescription}"/>
											</TEXTAREA>
										</spring:bind>
									</td>
								</tr>
								<tr>
									<td class="Label" valign="top"><label for="retailer">Retailer</label>
									</td>
									<td>
										<form:input path="retailName" value="${sessionScope.hotDeal.retailName}" disabled="true"/>
									</td>
									<td class="Label" valign="top"><label for="categorySel">Category</label>
									</td>
									<td>
										<div id="categoryDiv">
											<select name="Category" id="Category" class="textboxBigFit">
													<option>--Select--</option>
													<c:forEach items="${sessionScope.categories}" var="s">
														<c:if test="${sessionScope.hotDeal.categoryID ne s.categoryId}">
															<option value="${s.categoryId}">
																<c:out value="${s.categoryName}"></c:out>
															</option>
														</c:if>
														<c:if test="${sessionScope.hotDeal.categoryID eq s.categoryId}">
															<option value="${s.categoryId}" selected="selected">
																<c:out value="${s.categoryName}"></c:out>
															</option>
														</c:if>
													</c:forEach>
											</select>
										</div>
									</td>
								</tr>
								<tr>
									<td class="Label"><label>State</label>
									</td>
									<td>
										<spring:bind path="hotdeal.state">
											<TEXTAREA name="${status.expression }" class="txtAreaFit" disabled="true">
												<c:out value="${sessionScope.hotDeal.state}"/>
											</TEXTAREA>
										</spring:bind>
									</td>
									<td class="Label" width="12%"><label>City</label>
									</td>
									<td>	
										<spring:bind path="hotdeal.city">
											<TEXTAREA name="${status.expression }" class="txtAreaFit" disabled="true">
												<c:out value="${sessionScope.hotDeal.city}"/>
											</TEXTAREA>
										</spring:bind>
									</td>
								</tr>
								<tr>
									<td class="Label" valign="top"><label for="retailerLocations">Retail Location</label>
									</td>
									<td>
									<spring:bind path="hotdeal.retailLocationAddress">
											<TEXTAREA name="${status.expression }" class="txtAreaFit" disabled="true">
												<c:out value="${sessionScope.hotDeal.retailLocationAddress}"/>
											</TEXTAREA>
										</spring:bind>
									</td>
									<td colspan="2" valign="top">
									<c:if test="${sessionScope.locations ne null && !empty sessionScope.locations}">
									<input type="checkbox" name="chkAllRetailerLoc" id="chkAllRetailerLoc" /> <label for="chkAllRetailerLoc">Select All Retail Locations</label>
									</c:if>
									</td>
								</tr>
								<tr>
									<td class="Label"><label>Image</label>
									</td>
									<td><img src="${sessionScope.hotDeal.hotDealImagePath}" width="125" height="125" alt="" />
									</td>
									<td class="Label"><label for="url">URL</label>
									</td>
									<td>
										<form:errors cssStyle="color:red" path="hotDealURL"></form:errors>
										<form:input path="hotDealURL" value="${sessionScope.hotDeal.hotDealURL}"/>
									</td>
								</tr>
								<tr>
									<td class="Label"><label>Provider</label>
									</td>
									<td colspan="3"><img src="${sessionScope.hotDeal.APIPartnerImagePath}" alt="" />
									</td>
								</tr>
								<tr>
									<td class="Label"><label for="hdsd">Start Date</label>
									</td>
									<td width="23%" align="left">
										<form:input path="hotDealStartDate" size="10" id="dtp" cssClass="textboxDate" 
													value="${sessionScope.hotDeal.hotDealStartDate}" name="datepicker1" 
													readonly="true" />&nbsp; (MM/DD/YYYY)</td>
									<td width="17%" class="Label"><label for="hded">End Date</label>
									</td>
									<td width="32%" align="left">
										<form:errors cssStyle="color:red" path="hotDealEndDate"></form:errors>
										<form:input path="hotDealEndDate" size="10" id="endDT" cssClass="textboxDate" 
													value="${sessionScope.hotDeal.hotDealEndDate}" name="endDT" 
													readonly="true" />&nbsp; (MM/DD/YYYY)
									</td>
								</tr>
								<tr>
									<td class="Label"><label for="cst">Deal Start Time</label>
									</td>
									<td><form:select path="dealStartHrs" class="slctSmall">
											<form:options items="${DealStartHours}" />
										</form:select> Hrs <form:select path="dealStartMins" class="slctSmall">
											<form:options items="${DealStartMinutes}" />
										</form:select> Mins</td>
									<td class="Label"><label for="cet">Deal End Time</label></td>
									<td><form:select path="dealEndhrs" class="slctSmall">
											<form:options items="${DealStartHours}" />
										</form:select> Hrs <form:select path="dealEndMins" class="slctSmall">
											<form:options items="${DealStartMinutes}" />
										</form:select> Mins</td>
								</tr>
							</tbody>
						</table>
						</form:form>
						<div class="navTabSec mrgnRt">
							<div align="right">
								<input name="Save" value="Save" type="button" class="btn hltBtn" id="save" onclick="saveEdit();" />
								<input name="Back" value="Back" type="button" class="btn" id="Back" onclick="javascript:backToMain();" />
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div> 
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>
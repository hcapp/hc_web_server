<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HubCitiAdmins</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>

<script type="text/javascript">
	
	function addAdmin() {
		document.hubciti.lowerLimit.value = ${requestScope.lowerLimit};
		document.hubciti.action = "addadmin.htm";
		document.hubciti.method = "post";
		document.hubciti.submit();
	}
	
	function searchHubCiti(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			document.hubciti.lowerLimit.value = 0;
			document.hubciti.action = "displayhubcitiadmin.htm";
			document.hubciti.method = "post";
			document.hubciti.submit();

		}
	}
	 
	function searchHubCitiDet() {
		document.hubciti.lowerLimit.value = 0;
		document.hubciti.action = "displayhubcitiadmin.htm";
		document.hubciti.method = "post";
		document.hubciti.submit();
	}

	function deactivateHCAdmin(adminId,active)
	{			
		var r = confirm("Are you sure you want to deactivate this HubCiti Admin ?");
		if(r == true){
			document.hubciti.lowerLimit.value = ${requestScope.lowerLimit};
			document.hubciti.hubCitiID.value = adminId;
			document.hubciti.active.value = active;
			document.hubciti.action = "deactivateadmin.htm";
			document.hubciti.method = "post";
			document.hubciti.submit();
		}			
	}
	
	function activateHCAdmin(adminId,active)
	{			
		var r = confirm("Are you sure you want to activate this HubCiti Admin ?");
		if(r == true){
			document.hubciti.lowerLimit.value = ${requestScope.lowerLimit};
			document.hubciti.hubCitiID.value = adminId;
			document.hubciti.active.value = active;
			document.hubciti.action = "deactivateadmin.htm";
			document.hubciti.method = "post";
			document.hubciti.submit();
		}			
	}

	function editHCAdmin(adminId)
	{
		document.hubciti.lowerLimit.value = ${requestScope.lowerLimit};
		document.hubciti.hubCitiID.value = adminId;
		document.hubciti.action = "edithubcitiadmin.htm";
		document.hubciti.method = "post";
		document.hubciti.submit();
	}

	function openZipCode(adminID) {
		openIframePopup('ifrmPopup', 'ifrm',
				'fetchpostalcode.htm?hubCitiID=' + adminID, 400,
				800, 'View PostalCode');
	}
	
	function showDeactiveHC()
	{
		var showDeactivated = $('input[name$="showExpirebtn"]').val();
		if (showDeactivated == "Show Deactivated") {
			document.hubciti.showDeactivated.value = true;
		} else {
			document.hubciti.showDeactivated.value = false;
		}
		document.hubciti.lowerLimit.value = 0;
		document.hubciti.action = "displayhubcitiadmin.htm";
		document.hubciti.method = "post";
		document.hubciti.submit();
	}
	
	function eventCategoryHome() {
		
		document.hubciti.action = "eventcategory.htm";
		document.hubciti.method = "post";
		document.hubciti.submit();
	}
	
	function fundraiserCategoryHome() {
		document.hubciti.action = "fundraiserCategory.htm";
		document.hubciti.method = "get";
		document.hubciti.submit();
	}
	
	function regionAdmin()
	{
		document.hubciti.searchKey.value = '';
		document.hubciti.action = "displayregions.htm";
		document.hubciti.method = "POST";
		document.hubciti.submit();
		
	}
</script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hubciti.png"
					alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">
			 <div id="subnav" class="tabdSec">
      <ul>
        <li><a href="#" class="active" id="prodDet"><span>HubCiti Administration</span></a></li>
       <li><a class="#" href="#" id="reviewDet" onclick="regionAdmin()"><span>Region Administration</span></a></li>
      </ul>
    </div>  
			<div class="contBlock"> 
					<form:form name="hubciti" commandName="hubciti" acceptCharset="ISO-8859-1">
						<form:hidden path="hubCitiID"/>
						<form:hidden path="showDeactivated" value="${sessionScope.showDeactivated}"/>
						<form:hidden path="active"/>
						<form:hidden path="lowerLimit"/>
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
									<tr>
										<td class="header" colspan="3">Search</td>
									</tr>
									<tr>
										<td class="Label" width="18%"><label for="Supr_srchRbts"> HubCiti Name </label></td>
										<td width="60%">
											<form:input path="searchKey" onkeypress="searchHubCiti(event)" />
											<a href="#" title="Search HubCitiAdmin"> 
												<img src="images/searchIcon.png" onclick="searchHubCitiDet()" alt="Search" width="20" height="17" /> 
											</a>
										</td>
										<td width="22%">
											<input type="button" onclick="addAdmin();" value="Add" class="btn" title="Add Admin">
										</td>
									</tr>
								</table>
							</div>
						</fieldset>
						<c:if test="${sessionScope.showDeactivated eq false}">
							<div class="mrgnBtm" align="center">
								<input type="button" class="btn" id="showExpirebtn" name="showExpirebtn" value="Show Deactivated" title="Show Deactivated" onclick="javascript:showDeactiveHC()" />
							</div>
						</c:if>
						<c:if test="${sessionScope.showDeactivated eq true}">
							<div class="mrgnBtm" align="center">
								<input type="button" class="btn" id="showExpirebtn" name="showExpirebtn" value="Do Not Show Deactivated" title="Do Not Show Deactivated" onclick="javascript:showDeactiveHC()" />
							</div>
						</c:if>
					</form:form>
				</div>
				<center>
					<span class="highLightTxt"><c:out value="${requestScope.message}" /> </span> <span class="highLightErr"><c:out value="${requestScope.errorMessage}" /> </span>
				</center>
				<fieldset>
					<legend>HubCiti Details</legend>
					<div id="scrlGrd" class="grdTbl searchGrd">
						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0" cellpadding="0" width="100%">
							<tbody>
								<tr class="header">
									<!-- <td width="4%" align="center">
										<c:if test="${!empty sessionScope.adminlist}">
											<label>
												<input type="checkbox" name="deleteAll" />
											</label>
										</c:if>
									</td> -->
									<td width="30%">HubCiti Name</td>
					                <td width="27%">State | City | Postal Code</td>
					                <td width="40%">Username</td>
					                <td colspan="3" align="center">Action</td>
								</tr>
								<c:forEach items="${sessionScope.adminlist}" var="item">
									<tr>
										<td align="left"><c:out value="${item.hubCitiName}"/></td>										
										<td align="center"><input type="button" value="View" class="btn" onclick="openZipCode('<c:out value="${item.hubCitiID}"/>')" title="Show Postal Code"></td>
										<td><c:out value="${item.userName}" /></td>
										<td width="6%" align="center"><a href="#" onclick="editHCAdmin('<c:out value="${item.hubCitiID}"/>')"> <img src="images/edit_icon.png" alt="edit" title="Edit" width="25" height="19" /> </a></td>
										<c:choose>
											<c:when test="${item.active eq true}">
												<td width="5%" align="center"><a href="#" onclick="deactivateHCAdmin(${item.hubCitiID},true);"> <img src="images/deactivate_icon.png" alt="delete" title="Deactivate" width="18" height="18" /> </a></td>
											</c:when>
											<c:otherwise>
												<td width="5%" align="center"><a href="#" onclick="activateHCAdmin(${item.hubCitiID},false);"> <img src="images/activate_icon.png" alt="delete" title="Activate" width="18" height="18" /> </a></td>
											</c:otherwise>
										</c:choose>							
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="pagination">
						<p>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
				<!-- <div class="mrgnTop" align="right">
					<input type="button" class="btn" id="delete" value="Delete"
						title="Delete" onclick="deleteHotDeals();" disabled="disabled" />
				</div> -->
			</div>
			<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
					<div class="headerIframe">
						<img src="images/popupClose.png" class="closeIframe" alt="close"
							onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
							title="Click here to Close" align="middle" /> <span
							id="popupHeader"></span>
					</div>
					<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
						height="100%" allowtransparency="yes" width="100%"
						style="background-color: White"> </iframe>
			</div>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>
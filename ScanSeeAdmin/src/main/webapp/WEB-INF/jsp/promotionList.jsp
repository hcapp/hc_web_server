<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Promotions</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('input[name$="deleteAll"]').click(function() {
			var status = $(this).attr("checked");
			$('input[name$="checkboxDel"]').attr('checked', status);
			if (!status) {
				$('input[name$="checkboxDel"]').removeAttr('checked');
			}
		});
		$('input[name$="checkboxDel"]').click(function() {
			var tolCnt = $('input[name$="checkboxDel"]').length;
			var chkCnt = $('input[name$="checkboxDel"]:checked').length;
			if (tolCnt == chkCnt)
				$('input[name$="deleteAll"]').attr('checked', 'checked');
			else
				$('input[name$="deleteAll"]').removeAttr('checked');
		});
		$('.txtAreaSmall').val(function(i, v) {
			return v.replace(/^\s+|\s+$/g, "");
		});

	});
</script>
<script type="text/javascript">

	function showExpirePromotions() {
		var showExpire = $('input[name$="showExpirebtn"]').val();
		if (showExpire == "Show Expired") {
			document.promotion.showExpire.value = "false";
		} else {
			document.promotion.showExpire.value = "true";
		}
		document.promotion.action = "showexpiredpromotion.htm";
		document.promotion.method = "post";
		document.promotion.submit();
	}

	function searchPromotion() {
		document.promotion.action = "searchpromotion.htm";
		document.promotion.method = "post";
		document.promotion.submit();
	}

	function searchPromotionOnKeyPress(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			document.promotion.action = "searchpromotion.htm";
			document.promotion.method = "post";
			document.promotion.submit();

		}
	}

	function promotionDetails(promotionId) {
		document.promotion.promotionId.value = promotionId;
		document.promotion.action = "displaypromotiondetails.htm";
		document.promotion.method = "post";
		document.promotion.submit();
	}

	function filterPromotions() {
		var checkedValue = [];
		var i = 0;
		chkdBx = $('input[name="checkboxDel"]:checkbox:checked').length;
		if (chkdBx > 0) {
			$('input[name="checkboxDel"]:checked').each(function() {
				checkedValue[i] = $(this).val();
				i++;
			});
			document.promotion.promotionIds.value = checkedValue;
			document.promotion.action = "filterpromotion.htm";
			document.promotion.method = "post";
			document.promotion.submit();
		} else {
			alert("Please select any of the Promotions");
		}
	}

	function sortColumn(columnName) {
		document.promotion.columnName.value = columnName;
		document.promotion.action = "sortpromotion.htm";
		document.promotion.method = "post";
		document.promotion.submit();
	}
</script>
<!-- 
		</script> -->
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-prmtn.png"
					alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>

			<div class="rtContPnl floatR">
				<div class="contBlock">
					<form:form name="promotion" commandName="promotion"
						acceptCharset="ISO-8859-1">
						<form:hidden path="promotionId" />
						<form:hidden path="promotionIds" />
						<form:hidden path="sortOrder" />
						<form:hidden path="columnName" />
						<form:hidden path="searchFlag" />
						<form:hidden path="showExpire" value="${sessionScope.showExpire}" />
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<tr>
										<td class="Label" width="20%"><label for="Supr_srchRbts">Promotion
												Name</label></td>
										<td><c:if
												test="${sessionScope.searchKey ne null && !empty sessionScope.searchKey}">
												<input type="text" value="${sessionScope.searchKey}"
													name="searchKey" id="searchKey"
													onkeypress="searchPromotionOnKeyPress(event)" />
											</c:if> <c:if
												test="${sessionScope.searchKey eq null || empty sessionScope.searchKey}">
												<input type="text" name="searchKey" id="searchKey"
													onkeypress="searchPromotionOnKeyPress(event)" />
											</c:if> <label></label> <a href="#"> <img
												src="images/searchIcon.png" alt="Search" width="20"
												height="17" title="Search" onclick="searchPromotion()" />
										</a></td>
									</tr>
								</table>
							</div>
						</fieldset>					
						<c:if test="${sessionScope.showExpire eq 'true'}">
							<div class="mrgnBtm" align="center">
								<input type="button" class="btn" id="showExpirebtn"
									name="showExpirebtn" value="Show Expired" title="Show Expired"
									onclick="javascript:showExpirePromotions()" />
							</div>
						</c:if>
						<c:if test="${sessionScope.showExpire eq 'false'}">
							<div class="mrgnBtm" align="center">
								<input type="button" class="btn" id="showExpirebtn"
									name="showExpirebtn" value="Do Not Show Expired"
									title="Do Not Show Expired"
									onclick="javascript:showExpirePromotions()" />
							</div>
						</c:if>
					</form:form>
				</div>
				<center>
					<span class="highLightErr" style="font: red;"> <c:out
							value="${requestScope.message}" />
					</span> <span class="highLightErr" style="font: red;"> <c:out
							value="${requestScope.errorMessage}" />
					</span>
				</center>
				<fieldset>
					<legend>Promotion Details</legend>
					<div id="scrlGrd" class="grdCont searchGrd">
						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0"
							cellpadding="0" width="100%">
							<tbody>
								<tr class="header">
									<td width="4%" align="center"><label><input
											type="checkbox" name="deleteAll" /></label></td>
									<td width="24%" onclick="sortColumn('PageTitle')">
										<div class="txtwrap">
											<a href="#" title="Promotion Name"> Promotion Name <c:if
													test="${!empty sessionScope.PromotionNameImg}">
													<img src="${sessionScope.PromotionNameImg}" width="11"
														height="6" />
												</c:if>
											</a>
										</div>
									</td>
									<td width="9%"><a
										href="#" title="Quantity"> Quantity <c:if
												test="${!empty sessionScope.QuantityImg}">
												<img src="${sessionScope.QuantityImg}" width="11" height="6" />
											</c:if>
									</a></td>
									<td width="20%">Short Description</td>
									<td width="7%"><a
										href="#" title="RetailName"> Retailer Name <c:if
												test="${!empty sessionScope.RetailNameImg}">
												<img src="${sessionScope.RetailNameImg}" width="11"
													height="6" />
											</c:if>
									</a></td>
									<td width="15%"><a
										href="#" title="StartDate"> StartDate (mm/dd/yyyy)<c:if
												test="${!empty sessionScope.StartDateImg}">
												<img src="${sessionScope.StartDateImg}" width="11"
													height="6" />
											</c:if>
									</a></td>
									<td width="15%"><a
										href="#" title="EndDate"> EndDate (mm/dd/yyyy)<c:if
												test="${!empty sessionScope.EndDateImg}">
												<img src="${sessionScope.EndDateImg}" width="11" height="6" />
											</c:if>
									</a></td>
									<td width="3%" align="center" width="8%">Action</td>
								</tr>
								<c:forEach items="${sessionScope.PromotionList}" var="item">
									<tr>
										<td align="center"><input type="checkbox"
											name="checkboxDel" value="${item.QRRetailerCustomPageID}" /></td>
										<td align="left"><c:out value="${item.pagetitle}" /></td>
										<td align="left"><c:out value="${item.quantity}" /></td>
										<td><textarea readonly="readonly" class="txtAreaSmall">
												<c:out value="${item.shortDescription}" />
											</textarea></td>
										<td><c:out value="${item.retailName}" /></td>
										<td><c:out value="${item.startDate}" /></td>
										<td><c:if
												test="${item.endDate ne null || !empty item.endDate}">
												<c:out value="${item.endDate}" />
											</c:if> <c:if test="${item.endDate eq null || empty item.endDate}">
												<c:out value="No EndDate" />
											</c:if></td>
										<td width="5%" align="center"><a href="#"
											onclick="promotionDetails('${item.QRRetailerCustomPageID}')">
												<img src="images/view_icon.png" alt="edit" title="View"
												width="25" height="19" />
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="pagination">
						<p>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
				<div class="mrgnTop" align="left">
					<input type="button" class="btn" value="Filter" title="Filter"
						onclick="filterPromotions();" />
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div id="ourpartners_info">
			<div class="floatR" id="followus_section">
				<p>&nbsp;</p>
			</div>
			<div class="clear"></div>
		</div>
		<div id="footer_nav">
			<div class="clear"></div>
			<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All rights
				reserved</div>
			<p align="center">&nbsp;</p>
		</div>
	</div>
	</div>
	</div>
</body>
</html>
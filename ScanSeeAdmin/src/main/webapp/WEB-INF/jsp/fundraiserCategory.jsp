<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Fundraiser Category</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript">

	$(document).ready( function() {

		$(".alrtClose").click(function() {
			$('p.alertErr').hide();
			$('p.alertInfo').css("display", "none");
		});
		
		$(".actn-close").click(function() {
			var elemnt = $(this).parents('div'); 
			var alrtId = elemnt.attr("id");
			if(alrtId == 'evntCatClose') {
				$("#"+alrtId).hide();
			} else if(elemnt) {
				$(this).parent('div.alertBx').hide();
			}
		 });	
	});

	function eventCategoryHome() {
		document.FundraiserCategoryHome.action = "eventcategory.htm";
		document.FundraiserCategoryHome.method = "get";
		document.FundraiserCategoryHome.submit();
	}

	function hubcitiAdminHome() {
		document.FundraiserCategoryHome.action = "displayhubcitiadmin.htm";
		document.FundraiserCategoryHome.method = "post";
		document.FundraiserCategoryHome.submit();
	}
	
	function searchFundraiserCategory(event)	{
		$('p.alertErr').hide();
		$('p.alertInfo').css("display", "none");
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		if(keyCode == 13 || event == '') {
			document.FundraiserCategoryHome.action = "fundraiserCategory.htm";
			document.FundraiserCategoryHome.method = "post";
			document.FundraiserCategoryHome.submit();
		} else	{
			return true;
		}
	}
	
	function deleteFundraiserCategory(cateId) {
		$('p.alertErr').hide();
		$('p.alertInfo').css("display", "none");
		$('p.cathighLightErr').hide();
		var msg = confirm("You want to Delete Category?\n ");
		if (msg) {
			$.ajaxSetup({
				cache : false
			})
			$.ajax({
				type : "GET",
				url : "deletefundraisercat.htm",
				data : {
					"cateId" : cateId,
				},
				success : function(response) {
					if (response == 'Success') {
						document.FundraiserCategoryHome.actionType.value='delete';
						document.FundraiserCategoryHome.action = "fundraiserCategory.htm";
						document.FundraiserCategoryHome.method = "post";
						document.FundraiserCategoryHome.submit();
					} else {
						$('p.alertErr').show();
					}
				},
				error : function(e) {
					alert("Error occured while deleting event category");
				}
			});
		}
	}
	
	function addFundraiserCategory()	{
		document.FundraiserCategoryHome.catId.value = null;
		document.FundraiserCategoryHome.action = "addFundraiserCategory.htm";
		document.FundraiserCategoryHome.method = "get";
		document.FundraiserCategoryHome.submit();
	}
	
	function editFundraierCategory(catId)	{
		document.FundraiserCategoryHome.catId.value = catId;
		document.FundraiserCategoryHome.action = "addFundraiserCategory.htm";
		document.FundraiserCategoryHome.method = "get";
		document.FundraiserCategoryHome.submit();
	}
	
 	function displayFundCategories() {
		$('p.alertErr').hide();
		$('p.alertInfo').css("display", "none");
		document.FundraiserCategoryHome.action = "fundraiserCategory.htm";
		document.FundraiserCategoryHome.method = "post";
		document.FundraiserCategoryHome.submit();
	} 
	
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="index.html"> <img src="images/scansee-logo-hubciti.png"
					alt="ScanSee" /></a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<!--Left Navigation Starts here-->
			<div id="vNav" class="floatL">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<!--Left Navigation ends here-->
			<!--Right container starts here-->
			<div class="rtContPnl floatR">
				<div id="subnav" class="tabdSec">
					<ul>
						<!-- <li><a href="#" onclick="hubcitiAdminHome()" id="prodDet"><span>HubCiti
									Administration</span> </a></li> -->
						<li><a href="#" onclick="eventCategoryHome()"><span>Manage
									Event Category</span> </a></li>
						<li><a href="#" class="active"><span>Manage
									Fundraiser Category</span></a></li>
					</ul>
				</div>
				<form:form name="FundraiserCategoryHome"
					commandName="FundraiserCategoryHome" acceptCharset="ISO-8859-1">
					<div class="contBlock mrgnTop">
						<form:hidden path="catId" />
						<input type="hidden" name="actionType" onclick="displayFundCategories();" id="hidbtn" />
						<input type="hidden" name="pageNumber" /> <input type="hidden" name="pageFlag" />
						<input type="hidden" name="searchKey" />
						<fieldset>
							<!-- <legend>Search</legend> -->
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<c:if
										test="${sessionScope.fundraisercatlst ne null && !empty sessionScope.fundraisercatlst || sessionScope.searckey ne null && !empty sessionScope.searckey}">
										<tr>
											<td class="header" colspan="3">Search</td>
										</tr>
										<tr>
											<td class="Label" width="18%"><label for="Supr_srchRbts">Category
											</label></td>
											<td width="60%" colspan="2"><form:input type="text"
													name="catName" path="catName"
													onkeypress="searchFundraiserCategory(event)" /> <a
												href="#" title="Search Fundraiser Category"
												onclick="searchFundraiserCategory('')" id="catsearch"> <img
													src="images/searchIcon.png" alt="Search" width="20"
													height="17" /></a></td>
										</tr>
									</c:if>
									<tr>
										<td width="22%" colspan="3" align="right"><input
											type="button" class="btn" value="Add Category"
											onclick="addFundraiserCategory()" title="Add Category" /></td>
									</tr>
								</table>
							</div>
						</fieldset>
					</div>
					<c:if
						test="${requestScope.successMsg ne null && !empty requestScope.successMsg}">
						<p class="alertInfo">
							${requestScope.successMsg} <a class="alrtClose"
								href="javascript:void(0);" title="Close">x</a>
						</p>
					</c:if>

					<c:if
						test="${requestScope.errorMessage ne null && !empty requestScope.errorMessage}">
						<div class="alertBx success mrgnTop cntrAlgn" id="retSucces">
							<span title="close" class="actn-close"></span>
							<p class="msgBx">${requestScope.errorMessage}</p>
						</div>
					</c:if>

					<p class="alertErr">
						Category has been associated to Fundraiser. Please deassociate and
						delete <a class="alrtClose" href="javascript:void(0);"
							title="close">x</a>
					</p>

					<c:if
						test="${requestScope.catdelete ne null && !empty requestScope.catdelete}">
						<p class="alertInfo">
							Fundraiser Category Deleted Successfully. <a class="alrtClose"
								href="javascript:void(0);" title="close">x</a>
						</p>
					</c:if>

					<c:choose>
						<c:when
							test="${sessionScope.fundraisercatlst ne null && !empty sessionScope.fundraisercatlst}">
							<fieldset>
								<legend>Category Details</legend>
								<div id="scrlGrd" class="grdCont searchGrd">
									<table class="stripeMe" border="0" cellspacing="0"
										cellpadding="0" width="100%">
										<tbody>
											<tr class="header">
												<td width="68%">Category</td>
												<td align="center">Category Image</td>
												<td colspan="2" align="center">Action</td>
											</tr>

											<c:forEach
												items="${sessionScope.fundraisercatlst.alertCatLst}"
												var="cat">
												<tr>
													<td align="left"><a href="javascript:void(0);"
														id="${cat.catId}">${cat.catName}</a></td>
													<td width="16%" align="center">
														<c:choose>
															<c:when test="${null ne cat.cateImgName && !empty cat.cateImgName}">
																<img id="couponImg"
																src="${cat.cateImgName}" width="46" height="46"
																alt="image" onerror="this.src = '/ScanSeeAdmin/images/uploadIcon.png';" />
															</c:when>
															<c:otherwise>
																<img id="couponImg"
																src="/ScanSeeAdmin/images/uploadIcon.png" width="46" height="46"
																alt="image" onerror="this.src = '/ScanSeeAdmin/images/uploadIcon.png';" />
															</c:otherwise>
														</c:choose>
													</td>
													<%-- <td width="8%" align="center"><a
													href="editfundraisercate.htm?catId=${cat.catId}"><img
														src="images/edit_icon.png" alt="edit"
														title="Edit Category" width="25" height="19" /></a></td> --%>
													<td width="8%" align="center"><a href="#"><img
														src="images/edit_icon.png" alt="edit"
														title="Edit Category" width="25" height="19"
														onclick="editFundraierCategory(${cat.catId})" /></a></td>
													<td width="8%" align="center"><a href="#"><img
															src="images/delicon.png" title="Delete Category"
															width="18" height="18"
															onclick="deleteFundraiserCategory(${cat.catId})" /> </a></td>
												</tr>
											</c:forEach>
											<!-- <tr>
											<td align="left">Coupon_edit_Stp</td>
											<td width="6%" align="center"><img
												src="images/dfltImg.png" width="30" height="30" alt="image" /></td>
											<td width="6%" align="center"><a href="#"><img
													src="images/edit_icon.png" alt="edit" title="Edit"
													width="25" height="19" /></a></td>
											<td align="center"><a href="#"><img
													src="images/delicon.png" alt="delete" title="Delete"
													width="18" height="18" /></a></td>
										</tr> -->

										</tbody>
									</table>
								</div>
							</fieldset>
						</c:when>
						<c:otherwise>
							<c:if
								test="${requestScope.catdelete eq null || empty requestScope.catdelete}">
								<center>
									<span class="highLightErr"><c:out
											value="No Category found" /> </span>
								</center>
							</c:if>
						</c:otherwise>
					</c:choose>
					<c:if
						test="${sessionScope.fundraisercatlst.alertCatLst ne null && !empty sessionScope.fundraisercatlst.alertCatLst}">
						<div class="pagination">
							<p>
								<page:pageTag
									currentPage="${sessionScope.pagination.currentPage}"
									nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
									pageRange="${sessionScope.pagination.pageRange}"
									url="${sessionScope.pagination.url}" />
							</p>
						</div>
					</c:if>
				</form:form>
			</div>

			<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="images/popupClose.png" class="closeIframe" alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="no" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
			<!--Right container ends here-->
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>
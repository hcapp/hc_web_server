<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
      pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Version Administration</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">
		var vDay;
		var vMonth;
		var vYear;
			$(document).ready(function() {	
				var vCurrentDate = new Date();
				vDay = vCurrentDate.getDate();
				vMonth = vCurrentDate.getMonth() + 1;
				vYear = vCurrentDate.getFullYear();
				document.getElementById("approvedDate").value = vMonth + "/" + vDay + "/" + vYear;
		});	
      </script>
<script type="text/javascript">
      /*function compareTodayDate(startDate) {
            var startmiliSeconds, // startDate in milliseconds
            sday, // Startday
            smonth, // startmonth
            syear, // startyear
            smonth = startDate.substring(0, 2) - 0;
            sday = startDate.substring(3, 5) - 0;
            syear = startDate.substring(6, 10) - 0;
            var d = new Date();
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //months are zero based
            var curr_year = d.getFullYear();
            var sdate = new Date(syear, smonth, sday);
            var cdate = new Date(curr_year, curr_month, curr_date);
            
            if (sdate >= cdate) {
                  return true;
            }
            return false;
      }*/


      function backToMain() {
            var result = confirm("Do you want to leave this page without saving the Version details?");
            if (result == true) {
                  document.editVersion.action = "displayversion.htm";
                  document.editVersion.method = "post";
                  document.editVersion.submit();
            }
      }

      window.onload = function() {
          // var vAppType = $('input[name=appType]:checked').attr("id");
		  var vAppType = $("#appType2").val();
          var vRelTypeID = $('#relTypeId').val();
		  var vItunesFlg = $('#itunesFlag').val();
          var vHidRelType ;
			
      if (vAppType != "null" && vAppType != "") {
                  if (vAppType === "App" || vAppType == "App") {
					if (vItunesFlg == 'true' || vItunesFlg === 'true') {
                    	   document.getElementById("seliTunes").disabled=true;
						   document.getElementById('approvedNoAD').checked = 'checked';
						   $('#save').hide();
                         }
                        document.getElementById('App').checked = 'checked';
                        document.getElementById("Web").disabled=true;
                        document.getElementById("appID").style.display="";
						
                 if (vRelTypeID === 2 || vRelTypeID == 2) {
                       document.getElementById('selectProduction').checked = 'checked';
                       vHidRelType = document.getElementById('selectProduction').value;
                        //document.getElementById("selectQA").disabled=true;
						document.getElementById("selectQAProd").style.display="";
						
                 } else if (vRelTypeID === 3 || vRelTypeID == 3) {
                       $('#releaseITunes').show();
                       $('#selectQAProd').hide();
                        document.getElementById('seliTunes').checked = 'checked';
                        document.getElementById("selectItunes").style.display="none";
						document.getElementById("approveDte").style.display="";
                        document.getElementById("selectQAProd").style.display="none";           
                        vHidRelType = document.getElementById('seliTunes').value;
                       // document.getElementById("selectProduction").disabled=true;
                        //document.getElementById("selectQA").disabled=true;
						var vItunesDte = document.editVersion.iTunesUploadDate.value;
						if ( vItunesDte != null && vItunesDte != '') {
							document.getElementById("selectItunes").style.display="";
							document.getElementById("approveDte").style.display="none";
							document.getElementById('approvedYesSI').checked = 'checked';
							$('#hideApproveDate').show();
							$('#approvedDate').show();
							document.getElementById('approvedDate').value = vItunesDte;
							document.getElementById("approvedYesSI").disabled=true;
							document.getElementById("approvedNoSI").disabled=true;
						} else {
							document.getElementById('approvedNoAD').checked = 'checked';
						}
                 } else {
                      document.getElementById("selectQAProd").style.display="";
                        document.getElementById("releaseITunes").style.display="none";
                        document.getElementById('selectQA').checked = 'checked';
                        vHidRelType = document.getElementById('selectQA').value;
                        document.getElementById("seliTunes").disabled=true;
                  }
      } else {
                  document.getElementById('Web').checked = 'checked';
                  document.getElementById("App").disabled=true;
                  //document.getElementById("selectProductionW").disabled=true;
                  //document.getElementById("selectQAW").disabled=true;
                  document.getElementById("webID").style.display="";
                 if (vRelTypeID === 2 || vRelTypeID == 2) {
                       document.getElementById('selectProductionW').checked = 'checked';
                       vHidRelType = document.getElementById('selectProductionW').value;
                        //document.getElementById("selectQA").disabled=true;
                 } else {
                        document.getElementById('selectQAW').checked = 'checked';
                        vHidRelType = document.getElementById('selectQAW').value;
                  }     
      }
      document.getElementById('hiddenRelType').value = vHidRelType ; 
}
}


      function checkReleaseNoteValidate(input) {
		var vRelNote = $('#releaseNotePath').val();
            if (vRelNote != '') {
                  var checkRelNote = vRelNote.toLowerCase();
                 if (!checkRelNote.match(/(\.doc|\.docx)$/)) {
                        alert("You must upload Release Note with following extensions : .doc, .docx");
                        document.getElementById("releaseNotePath").value = "";
                        document.getElementById("releaseNotePath").focus();
                        return false;
                 }   
			}
     }
   
	  
	   function checkReleaseNoteValidateW(input) {
		var vRelNote = $('#releaseNotePathW').val();
            if (vRelNote != '') {
                  var checkRelNote = vRelNote.toLowerCase();
                  if (!checkRelNote.match(/(\.doc|\.docx)$/)) {
                        alert("You must upload Release Note with following extensions : .doc, .docx");
                        document.getElementById("releaseNotePathW").value = "";
                        document.getElementById("releaseNotePathW").focus();
                        return false;
                  } 
			}
     }
  

      function updateEditVersion() {
			//var vAppType = $('input[name=appType]:checked').attr("id");
			var vAppType = $("#appType2").val();
            var vNewVersn = $('#newVersion').val();
            var vSchema = $('#databaseSch').val();
            var vSerBuild = $('#serverBuildNo').val();
            var vRelType = $('input[name=releaseType]:checked').val();
            var vRelNote = $('#releaseNotePath').val();
			var vRelNoteW = $('#releaseNotePathW').val();
            var vRelDate = $('#releaseDate').val();
            var vApprDate = $('#approvedDate').val();
            var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            var vApprYesNo = $('input:radio[name=approvedYesNo]:checked').val();
            /*if(null== vSchema || "" == vSchema|| "null" == vSchema) {
                  alert('Please Enter Valid Database Schema');
                  return false;
            } else if(null== vSerBuild || "" == vSerBuild|| "null" == vSerBuild) {
                  alert('Please Enter  Server Build Number');
                  return false;
            } else if(isNaN(vSerBuild)) {
                  alert('Please Enter Valid Server Build Number');
                  return false;
            } else if(null== vRelDate || "" == vRelDate|| "null" == vRelDate) {
                  alert('Please Enter Valid Release Date');
                  return false;
            }else if(compareTodayDate(vRelDate) == true) {
                  alert('Release date less or equal to current date');
                  return false;
            }*/ if (vRelType != "iTunes" && vAppType == "App") {
                  if(null== vRelNote || "" == vRelNote|| "null" == vRelNote) {
                        alert('Release Note is empty.');
                        return false;
                  } 
				} else if (vAppType == "Web") {
                    if(null== vRelNoteW || "" == vRelNoteW|| "null" == vRelNoteW) {
				
                        alert('Release Note is empty.');
                        return false;
                  } 
				}
            document.editVersion.action = "updateversion.htm";
            document.editVersion.method = "post";
            document.editVersion.submit();
         }


      $(document).ready(function(){
	  var vRelTypeID = $('#relTypeId').val();
	  var vHiddenDate = $('#hiddenRelDate').val();
	  var vCurrDate =  vMonth + "/" + vDay + "/" + vYear;
	  
            $('#selectQA').click(function() {
            	if (vRelTypeID == 1 || vRelTypeID === 1) {
            			document.getElementById("releaseDate").value = vHiddenDate;
                  	} else {
                  		document.getElementById("releaseDate").value = vCurrDate;
                    }
                  document.getElementById("releaseNotePath").value = "";
                        $('#selectItunes').hide();
                        $('#approveDte').hide();
                        $('#releaseITunes').hide();
                        $('#selectQAProd').show();
                        $('#hideReleaseNote').show();
						$('#save').show();
                  });
                  $('#selectProduction').click(function() {
                	if (vRelTypeID == 2 || vRelTypeID === 2) {
						document.getElementById("releaseDate").value = vHiddenDate;
                  	} else {
                  		document.getElementById("releaseDate").value = vCurrDate;
                    }
                        document.getElementById("releaseNotePath").value = "";
                        $('#selectItunes').hide();
                        $('#approveDte').hide();
                        $('#releaseITunes').hide();
                        $('#selectQAProd').show();
                        $('#hideReleaseNote').show();
						$('#save').show();
                  });
                  $('#seliTunes').click(function() {
                	 if (vRelTypeID == 3 || vRelTypeID === 3) {
						document.getElementById("releaseDate").value = vHiddenDate;
                  	} else {
                  		document.getElementById("releaseDate").value = vCurrDate;
                    }
                        $('#selectItunes').show();
                        $('#releaseITunes').show();
                        $('#hideReleaseNote').hide();
                        $('#selectQAProd').hide();
						$("#selectItunes td:gt(0)").css("border-right","none");
                        document.getElementById('approvedNoSI').checked = 'checked';
                        $('#hideApproveDate').hide();
                        $('#approvedDate').hide();
						if (vItunesFlg == 'true' || vItunesFlg === 'true') {
                    	   $('#save').hide();
                         }
                  });

                  $('#approvedYesSI').click(function() {
                        $('#hideApproveDate').show();
                        $('#approvedDate').show();
                  });

                  $('#approvedNoSI').click(function() {
				  $("#selectItunes td:gt(0)").css("border-right","none");
                        $('#hideApproveDate').hide();
                        $('#approvedDate').hide();
                  });
				  $('#approvedYesAD').click(function() {
						document.getElementById("selectItunes").style.display="";
						document.getElementById("approveDte").style.display="none";
						document.getElementById('approvedYesSI').checked = 'checked';
                       $('#hideApproveDate').show();
                       $('#approvedDate').show();
                  });

                  $('#approvedNoAD').click(function() {
					document.getElementById('approvedNoAD').checked = 'checked';
                        $('#hideApproveDate').hide();
                        $('#approvedDate').hide();
                  });
				  
				 
				 $('#selectQAW').click(function() {
            	if (vRelTypeID == 1 || vRelTypeID === 1) {
            		document.getElementById("releaseDateW").value = vHiddenDate;
                  	} else {
                  		document.getElementById("releaseDateW").value = vCurrDate;
                    }
                  document.getElementById("releaseNotePathW").value = "";
                        $('#selectQAProdW').show();
                        $('#hideReleaseNote').show();
                  });
				  
                  $('#selectProductionW').click(function() {
                	if (vRelTypeID == 2 || vRelTypeID === 2) {
						document.getElementById("releaseDateW").value = vHiddenDate;
                  	} else {
                  		document.getElementById("releaseDateW").value = vCurrDate;
                    }
                        document.getElementById("releaseNotePathW").value = "";
                        $('#selectQAProdW').show();
                        $('#hideReleaseNote').show();
                  });
            });
            
</script>
</head>
<body>
      <div id="wrapper">
            <div id="header">
                  <div id="header_logo" class="floatL">
                        <a href="#"> <img src="images/scansee-logo.png" alt="ScanSee"
                              width="400" height="54" />
                        </a>
                  </div>
                  <div id="header_link" class="floatR">
                        <ul>
                              <li>Welcome <span class="highLightTxt"> <c:out
                                                value="${sessionScope.userName}" />,
                              </span>
                              </li>
                              <li><img src="images/logoutIcon.gif" alt="Logut" align="left"
                                    title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
                              </li>
                        </ul>
                  </div>
            </div>
            <div id="content" class="clear-fix minHt">
                  <div class="floatL" id="vNav">
                        <div class="vNavtopBg"></div>
                        <menu:MenuTag menuTitle="${sessionScope.moduleName}" />
                        <div class="vNavbtmBg"></div>
                  </div>
                  <div class="rtContPnl floatR">
                        <div class="contBlock">
                              <form:form name="editVersion" commandName="editVersion" acceptCharset=""
                                    enctype="multipart/form-data">
                                    <form:hidden path="relTypeId" id="relTypeId" />
                                    <form:hidden path="versionId" id="versionId" />
                                    <form:hidden path="hiddenRelType" id="hiddenRelType" />
                                    <form:hidden path="appType" id="appType2" />
									<form:hidden path="hiddenRelDate" id="hiddenRelDate" />
									<form:hidden path="itunesFlag" id="itunesFlag" />
									
									<form:hidden path="firstQARelDate" />
									<form:hidden path="lastQARelDate"  />
									<form:hidden path="firstProdtnRelDate"/>
									<form:hidden path="lastProdtnRelDate"/>
									<form:hidden path="iTunesUploadDate"/>
									<form:hidden path="buildApprovalDate"/>
									<form:hidden path="QAFileName"  />
									<form:hidden path="ProdFileName" />
                                    <center>
                                          <span class="highLightErr">
                                                      <c:out value="${requestScope.message}" /> 
                                          </span>
                                    </center>
		<fieldset id="Coupons" class="dscntType">							
        <legend>Edit Version</legend>
        <div class="contWrap">
          <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl">
            <tbody>
              <tr>
                <td colspan="4" class="header">Edit Version</td>
              </tr>
              <tr>
                <td width="18%" class="Label"><label for="newVersion" class="mandTxt">Version #</label></td>
                <td>
                        <form:input type="text" path="appVersionNo" name="appVersionNo" id="newVersion" class="txtAreaSmall" readonly="true" tabindex="1"/></td>
                        
                        <td><label class="mandTxt">Application Type</label></td>
                        <td>
                          <form:input type="radio" path="appType" name="appType" value="App" checked="checked" id="App" readonly="true" tabindex="2"/>
                          <label for="approvedYes">App</label>
                          
                          <form:input type="radio" path="appType" name="appType" value="Web"  id="Web" readonly="true" tabindex="3"/>
                          <label for="approvedNo">Web</label>
                          </td>
              </tr>
              <tr>
                <td class="Label" class="mandTxt"><label for="databaseSch" class="mandTxt">Database Schema</label></td>
                <td><form:input type="text" path="SchemaName" name="SchemaName" id="databaseSch" tabindex="4" maxlength="20" readonly="true"/></td>
                <td><label for="serverBuildNo" class="mandTxt">Server Build No.</label></td>
                <td><form:input  type="text" path="serverBuildNo" name="serverBuildNo" id="serverBuildNo" tabindex="5" onkeypress="return isAlphaNumeric(event)" maxlength="10" readonly="true"/></td>
              </tr>
              <tr id="appID" style="display:none">
                <td class="Label" valign="top" class="mandTxt"><label class="mandTxt">Release Type</label></td>
                <td colspan="3" class="zeroPdng">
				<div class="tdPdng">
				<form:input type="radio" path="releaseType" name="releaseType" value="QA"   checked="checked" id="selectQA" tabindex="6"/>
                  <label for="selectQA">QA</label>
                  <form:input type="radio"  path="releaseType" name="releaseType" value="Production"  class="min-margin-left" id="selectProduction" tabindex="7"/>
                  <label for="selectProduction">Production</label>
                  <form:input type="radio"  path="releaseType" name="releaseType" value="iTunes"  class="min-margin-left" id="seliTunes" tabindex="8"/>
                  <label for="seliTunes">iTunes</label></div>
                  <div class="min-margin-top">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl brdrTop">
									<tr id="fileName">
                <td  style="border-right: 1px solid rgb(218, 218, 218)8);" class="Label"><label for="releaseNote">Release File Name</label></td>
                <td  colspan="3"><c:out value="${requestScope.ReleaseFileName}" /></td>

              </tr>
                      <tr id="selectQAProd" style="display:none">
                        <td width="20%">
                           <label for="releaseDate" class="mandTxt">Release Date</label></td>
                        <td><form:input path="releaseDate" size="10" id="releaseDate" cssClass="textboxDate"  name="releaseDate" readonly="true" tabindex="9"/> </td>
                        <td><label for="releaseNote" class="mandTxt" id="hideReleaseNote">Upload Release Note</label></td>
                        <td><form:input type="file" path="releaseNotePath" id="releaseNotePath" class="textboxBig"   onchange="checkReleaseNoteValidate(this);" tabindex="10"/></td>
                      </tr>
                      
                      <tr id="releaseITunes" style="display:none">
                        <td width="20%">
                                    <label for="releaseDate" class="mandTxt">Release Date</label></td>
                        <td colspan="3"><form:input path="releaseDate" size="10" id="releaseDate" cssClass="textboxDate"  name="releaseDate" readonly="true" tabindex="11"/> </td>
                      </tr>
                      
                        <tr id="selectItunes" style="display:none">
                        <td><label class="mandTxt">Approved</label></td>
                        <td>
                          <form:input type="radio" path="approvedYesNo" name="approvedYesNo" value="approvedYes"  id="approvedYesSI" tabindex="12"/>
                          <label for="approvedYes">Yes</label>
                          
                          <form:input type="radio" path="approvedYesNo" name="approvedYesNo" value="approvedNo"   checked="checked" id="approvedNoSI" tabindex="13"/>
                          <label for="approvedNo">No</label>
                          </td>
                        <td><label for="approcedDate" class="mandTxt" id="hideApproveDate">Approved Date</label></td>
                        <td><form:input path="buildApprovalDate" size="10" id="approvedDate" cssClass="textboxDate"  name="buildApprovalDate" readonly="true" tabindex="14"/></td>
                      </tr>
                      
                      <tr id="approveDte" style="display:none">
                        <td><label class="mandTxt">Approved</label></td>
                        <td colspan="3">
                          <form:input type="radio" path="approvedYesNo" name="approvedYesNo" value="approvedYes"  id="approvedYesAD" tabindex="12"/>
                          <label for="approvedYes">Yes</label>
                          
                          <form:input type="radio" path="approvedYesNo" name="approvedYesNo" value="approvedNo"   id="approvedNoAD" tabindex="13"/>
                          <label for="approvedNo">No</label>
                          </td>
                      </tr>
                      
                    </table>
                  </div></td>
              </tr>
                    
               <tr id="webID" style="display:none">
                <td class="Label" valign="top" class="mandTxt"><label class="mandTxt">Release Type</label></td>
                <td colspan="3" class="zeroPdng">
				<div class="tdPdng">
				<form:input type="radio" path="releaseType" name="releaseType" value="QA"   checked="checked" id="selectQAW" tabindex="7"/>
                  <label for="selectQA">QA</label>
                  <form:input type="radio"  path="releaseType" name="releaseType" value="Production"  class="min-margin-left" id="selectProductionW" tabindex="8"/>
                  <label for="selectProduction">Production</label>
				  </div>
                  <div class="min-margin-top brdrTop">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl">
					<tr>
                <td style="border-right: 1px solid rgb(218, 218, 218)8);" class="Label"><label for="releaseNote">Release File Name</label></td>
                <td colspan="3"><c:out value="${requestScope.ReleaseFileName}" /></td>

              </tr>
                      <tr id="selectQAProdW">
                        <td>
                                    <label for="releaseDate" class="mandTxt">Release Date</label></td>
                        <td><form:input path="releaseDate" size="10" id="releaseDateW" cssClass="textboxDate"  name="releaseDate" readonly="true" tabindex="9"/> </td>
                        <td><label for="releaseNote" class="mandTxt" id="hideReleaseNote">Upload Release Note</label></td>
                        <td><form:input type="file" path="releaseNotePathW" id="releaseNotePathW" class="textboxBig"   onchange="checkReleaseNoteValidateW(this);" tabindex="10"/></td>
                      </tr>
                    </table>
                  </div></td>
              </tr>
                    
            </tbody>
          </table>
          <div class="navTabSec mrgnRt">
            <div align="right"> 
              <!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
              <input name="Save" value="Save" type="button" class="btn hltBtn" id="save" onclick="updateEditVersion()" tabindex="14"/>
             <!--   <input name="save" value="Clear" type="button" class="btn" id="Clear" onclick="clearVersionForm()"/>-->
              <input name="save2" value="Back" type="button" class="btn" id="Back" onclick="backToMain()" tabindex="15"/>
            </div>
          </div>
         </form:form>
           </div>
            </div>
          </div>
            </div>
       </fieldset>
            <div id="footer">
                  <div id="ourpartners_info">
                        <div class="floatR" id="followus_section">
                              <p>&nbsp;</p>
                        </div>
                        <div class="clear"></div>
                  </div>
                  <div id="footer_nav">
                        <div class="clear"></div>
                        <div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
                              rights reserved</div>
                        <p align="center">&nbsp;</p>
                  </div>
            </div>
</body>
</html>

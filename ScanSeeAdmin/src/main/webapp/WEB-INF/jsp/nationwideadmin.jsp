<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Nationwide Admin</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/* var hidCnt = $('input[name$="nationId"]:checkbox:checked').length;
		document.nationwideform.hidchkcnt.value = hidCnt;
		//alert(hidCnt);
		//for checked check box.
		var chkd = $.makeArray($('input[name="nationId"]:checkbox:checked')
		                  .map(function() { return $(this).val() }));

			//for unchecked check box.
		var unchkd = $.makeArray($('input[name="nationId"]:checkbox:not(:checked)')
		                  .map(function() { return $(this).val() }));
		
		alert(chkd);
		alert(unchkd);*/
		
		$("input[name='isNationwide']").change(function() {
			var isNationwide = $("input[name='isNationwide']:checked").val();			
			if(isNationwide == "true") {
				$(".nationwide").show();
			} else {
				$(".nationwide").hide();
			}
			$(".error").hide();
		});
	});

	/* function SaveNationwideInfo() {

		//for checked check box.
		var chkd = $.makeArray($('input[name="nationId"]:checkbox:checked').map(function() {
			return $(this).val();
		}));

		//for unchecked check box.
		var unchkd = $.makeArray($('input[name="nationId"]:checkbox:not(:checked)').map(function() {
			return $(this).val();
		}));

		document.nationwideform.chkdNatId.value = chkd;
		document.nationwideform.unChkdNatId.value = unchkd;

		document.nationwideform.pageNumber.value = $('#regHubcitiIds').val();
		document.nationwideform.pageFlag.value = "true";
		document.nationwideform.action = "saveNationwide.htm";
		document.nationwideform.method = "POST";
		document.nationwideform.submit();
	} */

	function callNextPage(pagenumber, url) {
		/* 		var hidChdCnt = document.nationwideform.hidchkcnt.value;
		 var tolChkCnt = $('input[name$="nationId"]:checkbox:checked').length;
		 ;
		 if (hidChdCnt != tolChkCnt) {

		 var msg = confirm("Do you want to save the changes?\n ");

		 if (msg) {
		 //for checked check box.
		 var chkd = $.makeArray($('input[name="nationId"]:checkbox:checked').map(function() {
		 return $(this).val()
		 }));

		 //for unchecked check box.
		 var unchkd = $.makeArray($('input[name="nationId"]:checkbox:not(:checked)').map(function() {
		 return $(this).val()
		 }));

		 document.nationwideform.chkdNatId.value = chkd;
		 document.nationwideform.unChkdNatId.value = unchkd;

		 document.nationwideform.pageNumber.value = pagenumber;
		 document.nationwideform.pageFlag.value = "true";
		 document.nationwideform.action = "nationwide.htm";
		 document.nationwideform.method = "POST";
		 document.nationwideform.submit();

		 } else {

		 }

		 } */

		document.nationwideform.pageNumber.value = pagenumber;
		document.nationwideform.pageFlag.value = "true";
		document.nationwideform.action = "nationwide.htm";
		document.nationwideform.method = "POST";
		document.nationwideform.submit();

	}

	function searchHubCitiReg(event) {
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		if (keyCode == 13) {
			document.nationwideform.action = "nationwide.htm";
			document.nationwideform.method = "POST";
			document.nationwideform.submit();
		} else if (event == '') {
			document.nationwideform.action = "nationwide.htm";
			document.nationwideform.method = "POST";
			document.nationwideform.submit();
		} else {
			return true;
		}

	}

	/* function openAPIPartners(adminID, apiPartners, isNationwide) {

		var lowerLimit = $
		{
			requestScope.lowerLimit
		}
		;
		openIframePopup('ifrmPopup', 'ifrm', 'fetchapipartner.htm?hubCitiID=' + adminID + '&hidAPIPartners=' + apiPartners + '&isNationwide='
				+ isNationwide + '&lowerLimit=' + lowerLimit, 300, 400, 'Setup Nationwide');
	} */

	function showModal(hubcitiId, apiPartners, isNationwide) {
		
		document.nationwideform.hubCitiID.value = hubcitiId;
		
		$(".error").hide();
		
		if(isNationwide == "true") {
			$("input[name='isNationwide'][value='true']").attr("checked", "checked");
			$(".nationwide").show();
		} else {
			$("input[name='isNationwide'][value='false']").attr("checked", "checked");
			$(".nationwide").hide();
		}
		
		$("input[name='apiPartners']").each(function () {
            $(this).attr("checked", false);
        });

		if(typeof apiPartners != 'undefined' && "" !== apiPartners) {
			var arr = apiPartners.split(',');			
			for (var i = 0; i < arr.length; i++) {
		          $("#"+arr[i]).attr('checked', 'checked');
		    }			
		}
		
		var bodyHt = $('body').height();
		var setHt = parseInt(bodyHt);
		$(".modal-popupWrp").show();
		$(".modal-popupWrp").height(setHt);
		$(".modal-popup").slideDown('fast');
	}

	/*close modal popup on click of x*/
	function closeModal() {
		$(".modal-popupWrp").hide();
		$(".modal-popup").slideUp();
		//$(".modal-popup").find(".modal-bdy input").val("");
		//$(".modal-popup i").removeClass("errDsply");
	}
	
	function SaveNationwideInfo() {
		
		var isNationwide = $("input[name='isNationwide']:checked").val();
		var apiPartner = $("input[name='apiPartners']:checked").length;		
		
		if(isNationwide == "true" && apiPartner == 0) {
			$(".error").show();
		} else {	
			document.nationwideform.lowerLimit.value = ${requestScope.lowerLimit};
			document.nationwideform.action = "saveNationwide.htm";
			document.nationwideform.method = "POST";
			document.nationwideform.submit();
		}			
	}
	
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hubciti.png" alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left" title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a></li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<form:form name="nationwideform" commandName="nationwideform">
				<input type="hidden" name="pageNumber" />
				<input type="hidden" name="pageFlag" />
				<input type="hidden" name="hubCitiID" />
				<input type="hidden" name="lowerLimit" />
				<!-- <input type="hidden" name="hidchkcnt" />
				<input type="hidden" name="chkdNatId" />
				<input type="hidden" name="unChkdNatId" /> 
				<input type="hidden" id="regHubcitiIds" name="regHubcitiIds" value="${requestScope.hidPageNum}" />-->
				<div class="floatL" id="vNav">
					<div class="vNavtopBg"></div>
					<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
					<div class="vNavbtmBg"></div>
				</div>
				<div class="rtContPnl floatR">
					<div class="contBlock mrgnTop">
						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
									<tr>
										<td colspan="3" class="header">Search</td>
									</tr>
									<tr>
										<td class="Label" width="30%"><label for="hubCitiName">HubCiti / Region Name</label></td>
										<td width="70%"><form:input type="text" path="searchKey" id="searchKey" onkeypress="searchHubCitiReg(event)" maxlength="30" /> &nbsp;
											<label></label> <a href="#"><img src="images/searchIcon.png" alt="Search" width="20" height="17" onclick="searchHubCitiReg('')"
												title="Search HubCiti / Region"
											/></a></td>
									</tr>
								</table>
							</div>
						</fieldset>
					</div>
					<center>
						<c:if test="${requestScope.noregfound ne null && !empty requestScope.noregfound}">
							<span class="highLightErr"><c:out value="${requestScope.noregfound}"></c:out></span>
						</c:if>
						<c:if test="${requestScope.successMsg ne null && !empty requestScope.successMsg}">
							<span class="highLightTxt"><c:out value="${requestScope.successMsg}" /> </span>
						</c:if>
					</center>
					<fieldset>
						<legend>Nationwide Details</legend>
						<div id="scrlGrd" class="grdCont searchGrd">
							<table id="cpnLst" class="stripeMe" border="0" cellspacing="0" cellpadding="0" width="100%">
								<tbody>
									<tr class="header">
										<td width="88%">HubCiti / Region Name <!-- <a href="#"
											class="sortOprtn"><img src="images/sorticon.png"
												alt="sort" width="11" height="6" title="Sort by name" /></a> -->
										</td>
										<td align="center">Nationwide</td>
									</tr>
									<c:if test="${sessionScope.hubcitireglst ne null && !empty sessionScope.hubcitireglst}">
										<c:forEach items="${sessionScope.hubcitireglst}" var="citi">
											<tr>
												<td align="left">${citi.hubCitiName}</td>
												<td align="center"><input type="button" value="Setup Nationwide" class="btn"
													onclick="showModal('<c:out value="${citi.hubCitiID}"/>', '<c:out value="${citi.apiPartners}"/>', '<c:out value="${citi.isNationwide}"/>')"
													title="Setup Nationwide"
												/></td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</div>
						<c:if test="${sessionScope.hubcitireglst ne null && !empty sessionScope.hubcitireglst}">
							<div class="pagination">
								<p>
									<page:pageTag totalSize="${sessionScope.pagination.totalSize}" nextPage="4" url="${sessionScope.pagination.url}"
										currentPage="${sessionScope.pagination.currentPage}" pageRange="${sessionScope.pagination.pageRange}"
									/>
								</p>
							</div>
						</c:if>
					</fieldset>
					<!-- <div align="right" class="mrgnTop">
						<input type="button" value="Save" class="btn" title="Save Nationwide Changes" onclick="SaveNationwideInfo()" />
					</div> -->
				</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
			<div class="headerIframe">
				<img src="images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle"
				/> <span id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color: White">
			</iframe>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">
					Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All rights reserved
				</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
	<div class="modal-popupWrp">
		<div class="modal-popup">
			<div class="modal-hdr">
				Setup Nationwide <a class="modal-close" title="close" onclick="closeModal()"><img src="images/popupClose.png" /></a>
			</div>
			<div class="modal-bdy">
				<div class="modal-cont">
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl">
						<tbody>
							<tr>
								<td width="100%" class="multipleInputs"><label for="nationwide_yes"> <form:radiobutton path="isNationwide" value="true" />Yes
								</label> <label for="nationwide_no"> <form:radiobutton path="isNationwide" value="false" />No
								</label></td>
							</tr>
							<tr class="nationwide">
								<td class="cstm-hdr">Nationwide</td>
							</tr>
							<tr class="error">
								<td class="highLightErr">Please select atleast one API partner</td>
							</tr>
							<tr class="nationwide">
								<td class="Label"><ul class="nw-list">
										<c:forEach items="${sessionScope.apiPartners}" var="apiPartner">
											<li><label for="chkbx1"> <form:checkbox path="apiPartners" value="${apiPartner.apiPartnerID}" id="${apiPartner.apiPartnerID}" />
													${apiPartner.apiPartnerName}
											</label></li>
										</c:forEach>
									</ul></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-ftr">
				<p align="right">
					<!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
					<input name="Save" value="Save" type="button" class="btn" id="save" onclick="SaveNationwideInfo();" /> <input type="button" id="Cancel"
						class="btn" value="Cancel" name="Save" onclick="closeModal();"
					/>
				</p>
			</div>
		</div>
	</div>
	</form:form>
</body>
</html>
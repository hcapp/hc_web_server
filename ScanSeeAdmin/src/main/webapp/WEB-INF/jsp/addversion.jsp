<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Version Administration</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var currentDate = new Date();
		var day = currentDate.getDate();
		var month = currentDate.getMonth() + 1;
		var year = currentDate.getFullYear();
		document.Version.releaseDate.value = month + "/" + day + "/" + year;


		$('#newVersion').focusout(function(){
			var regexp = /^\d+(\.\d{1,2})(\.\d{1,2})?$/;
			var vVerNum =$("#newVersion").val();
			if (vVerNum != undefined || vVerNum != null || vVerNum != '' ) {
		    if(regexp.test(vVerNum)){
		        return true;
		      }else{
		        alert("Please enter valid version Number.")
		        $("#newVersion").val("").focus();
		        return false;
		     }
		   }
		});

		$('#serverBuildNo').focusout(function(){
			var regexp = /^\d+(\.\d{1,2})(\.\d{1,2})?$/;
			var vVerNum =$("#serverBuildNo").val();
			if (vVerNum != undefined || vVerNum != null || vVerNum != '') {
		   	 if(regexp.test(vVerNum)){
		        return true;
		     }else{
		      	alert("Please enter valid Server Build No.")
		      	$("#serverBuildNo").val("").focus();
		      	return false;
		     }
		   }
		});
	});

	
</script>
<script type="text/javascript">
	function couponAdmin() {
		document.Version.action = "displaycoupon.htm";
		document.Version.method = "post";
		document.Version.submit();
	}

	function promotionAdmin() {
		document.Version.action = "displaypromotion.htm";
		document.Version.method = "post";
		document.Version.submit();
	}

	function retailerAdmin() {
		document.Version.action = "displayretailer.htm";
		document.Version.method = "post";
		document.Version.submit();
	}

	function hotdealAdmin() {
		document.Version.action = "displayhotdeal.htm";
		document.Version.method = "post";
		document.Version.submit();
	}

	function callNextPage(pagenumber, url) {
		document.Version.pageNumber.value = pagenumber;
		document.Version.pageFlag.value = "true";
		document.Version.action = url;
		document.Version.method = "post";
		document.Version.submit();

	}
	function searchCoupon(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			document.Version.sortOrder.value = "ASC";
			document.Version.columnName.value = "CouponName";
			document.Version.action = "searchcoupon.htm";
			document.Version.method = "post";
			document.Version.submit();

		}
	}
	function searchCouponDet() {
		document.Version.sortOrder.value = "ASC";
		document.Version.columnName.value = "CouponName";
		document.Version.action = "searchcoupon.htm";
		document.Version.method = "post";
		document.Version.submit();
	}
	function showExpireCoupons() {
		var showExpire = $('input[name$="showExpirebtn"]').val();
		if (showExpire == "Show Expired") {
			document.Version.showExpire.value = "false";
		} else {
			document.Version.showExpire.value = "true";
		}
		document.Version.action = "showexpiredcoupon.htm";
		document.Version.method = "post";
		document.Version.submit();
	}

	function sortColumn(columnName) {
		document.Version.columnName.value = columnName;
		document.Version.action = "sortcolumn.htm";
		document.Version.method = "post";
		document.Version.submit();
	}

	function versionAdmin() {
		document.Version.action = "displayversion.htm";
		document.Version.method = "POST";
		document.Version.submit();
	}

	function submitVersion() {

		var getUsrOptn = $("input[name='releaseType']:checked").attr("value");

		var curDate = new Date();
		var curr_date = curDate.getDate();

		var appVersion = document.Version.appVersionNo.value;
		var dbSchema = document.Version.SchemaName.value;
		var serBuildNo = document.Version.serverBuildNo.value;
		var relType = document.Version.releaseType.value;
		var relDate = document.Version.releaseDate.value;
		var relNotePath = document.Version.releaseNotePath.value;

		if (null == appVersion || "" == appVersion || "null" == appVersion) {
			alert('Please Enter  App Version');
		} else if (null == dbSchema || "" == dbSchema || "null" == dbSchema) {
			alert('Please Enter Valid Database Schema');
		} else if (null == serBuildNo || "" == serBuildNo
				|| "null" == serBuildNo) {
			alert('Please Enter  Server Build Number');
		} else if (null == relDate || "" == relDate || "null" == relDate) {
			alert('Please Enter Valid Release Date');
		} else if (relNotePath == "") {
			alert('Please Select Release Note');
		} else {

			document.Version.action = "addversion.htm";
			document.Version.method = "POST";
			document.Version.submit();
		}
	}

	function compareTodayDate(startDate) {

		var startmiliSeconds, // startDate in milliseconds
		sday, // Startday
		smonth, // startmonth
		syear, // startyear
		smonth = startDate.substring(0, 2) - 0;
		sday = startDate.substring(3, 5) - 0;
		syear = startDate.substring(6, 10) - 0;
		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth() + 1; //months are zero based
		var curr_year = d.getFullYear();
		var sdate = new Date(syear, smonth, sday);
		var cdate = new Date(curr_year, curr_month, curr_date);

		if (sdate >= cdate) {
			return true;
		}
		return false;
	}

	function clearVersionForm() {
		var r = confirm("Do you want to clear this form?");
		if (r == true) {
			document.Version.appVersionNo.value = "";
			document.Version.SchemaName.value = "";
			document.Version.serverBuildNo.value = "";
			document.Version.releaseType.value = "";
			document.Version.releaseNotePath.value = "";
			$("#vrsnAlrt").remove();
		}
	}

	function checkReleaseNoteValidate(input) {

		var vRelNote = document.getElementById("releaseNotePath").value;
		if (vRelNote != '') {
			var checkRelNote = vRelNote.toLowerCase();
			if (!checkRelNote.match(/(\.doc|\.docx)$/)) {

				alert("You must upload Release Note with following extensions : .doc, .docx");
				document.Version.releaseNotePath.value = "";
				document.getElementById("releaseNotePath").focus();
				return false;
			} else {
				var fileSize = 0;
				fileSize = ($("#releaseNotePath")[0].files[0].size / 1024);

				if (fileSize > 0) {
					fileSize = (Math.round((fileSize / 1024) * 100) / 100)
					if (fileSize > 10) {
						alert("Release note size greater than maximum limit.");
						document.Version.releaseNotePath.value = "";
					} else {
						document.Version.releaseNotePath.value = checkRelNote;
					}
				} else {
					alert("Release Note is Empty!, Please select valid release note ");
					document.Version.releaseNotePath.value = "";
				}
			}
		}
	}

	function backToDisplay() {
		var r = confirm("Do you want to leave this page without saving the version?");
		if (r == true) {
			document.Version.action = "displayversion.htm";
			document.Version.method = "post";
			document.Version.submit();
		}
	}

	function checkAlphaNumeric(event) {
		var keycode;
		if (window.event)
			keycode = window.event.keyCode;
		else if (event)
			keycode = event.keyCode;
		else if (e)
			keycode = e.which;
		else
			return true;
		if ((keycode >= 47 && keycode <= 57)
				|| (keycode >= 65 && keycode <= 90)
				|| (keycode >= 95 && keycode <= 122)) {
			return true;
		} else {
			alert("Please do not use special characters")
			return false;
		}
		return true;
	}


	
	</script>
	</head>
	<body onload="resizeDoc();" onresize="resizeDoc();">
		<div id="wrapper">
			<div id="header">
				<div id="header_logo" class="floatL">
					<a href="javascript:void(0);"> <img
						src="images/scansee-logo-version_mgt.png" alt="ScanSee" width="400"
						height="54" /> </a>
				</div>

				<div id="header_link" class="floatR">
					<ul>
						<li>Welcome <span class="highLightTxt"> <c:out
									value="${sessionScope.userName}" />, </span></li>
						<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
							title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
						</li>
					</ul>
				</div>
			</div>
			<!--Header ends-->
			<div id="content" class="clear-fix">
				<div class="floatL" id="vNav">
					<div class="vNavtopBg"></div>
					<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
					<div class="vNavbtmBg"></div>
				</div>
				<div class="rtContPnl floatR">
					<div class="contBlock">
						<fieldset id="Coupons" class="dscntType">
							<legend>Add Version</legend>
							<form:form name="Version" commandName="Version"
								acceptCharset="ISO-8859-1" method="POST"
								enctype="multipart/form-data">

								<p align="center" id="vrsnAlrt">
									<span class="highLightErr"><c:out
											value="${requestScope.errorMessage}" /> </span>
								</p>
								<div class="contWrap">
									<table width="100%" cellspacing="0" cellpadding="0" border="0"
										class="grdTbl">
										<tbody>
											<tr>
												<td colspan="4" class="header">New Version Creation</td>
											</tr>
											<tr>
												<td width="18%" class="Label"><label for="newVersion"
													class="mandTxt">Version #</label>
												</td>
												<td><form:input type="text" path="appVersionNo"
														maxlength="10" onkeypress="return isNumberKey(event)"
														name="appVersionNo" id="newVersion" class="txtAreaSmall" />
												</td>

												<td><label class="mandTxt">Application Type</label>
												</td>
												<td><form:input type="radio" path="appType"
														name="appType" value="App" checked="checked" id="App"
														readonly="true" /> <label for="approvedYes">App</label> <form:input
														type="radio" path="appType" name="appType" value="Web"
														id="Web" readonly="true" /> <label
													for="approvedNo">Web</label></td>
											</tr>

											<tr>
												<td class="Label" class="mandTxt"><label
													for="databaseSch" class="mandTxt">Database Schema</label>
												</td>
												<td><form:input type="text" path="SchemaName"
														name="SchemaName" id="databaseSch" maxlength="20"
														onkeypress="return isAlphaNumeric(event)" />
												</td>
												<td><label for="serverBuildNo" class="mandTxt">Server
														Build No.</label>
												</td>
												<td><form:input type="text" path="serverBuildNo"
														name="serverBuildNo" id="serverBuildNo" maxlength="10"
														onkeypress="return isNumberKey(event)" />
												</td>
											</tr>
											<tr>
												<td class="Label" valign="top" class="mandTxt"><label
													class="mandTxt">Release Environment</label>
												</td>
												<td colspan="3" class="zeroPdng">
													<div class="tdPdng">
														<form:input type="radio" path="releaseType"
															name="releaseType" value="QA" checked="checked" />
														<label for="selectQA">QA</label>
														<form:input type="radio" path="releaseType"
															name="releaseType" value="Production"
															class="min-margin-left" id="selectProduction"
															disabled="true" />
														<label for="selectProduction">Production</label>
														<form:input type="radio" path="releaseType"
															name="releaseType" value="iTunes" class="min-margin-left"
															id="seliTunes" disabled="true" />
														<label for="seliTunes">iTunes</label>
													</div>

													<div class="selReleaseSec">
														<table width="100%" cellspacing="0" cellpadding="0"
															border="0" class="grdTbl">
															<tr id="selectQAProd">


																<td width="15%"><label for="releaseDate">Date</label>
																</td>

																<td width="35%"><form:input type="text" size="16"
																		readonly="true" path="releaseDate" name="releaseDate"
																		id="releaseDate" class="textboxDate" /></td>


																<td colspan="2" width="50%">
																	<table width="100%" class="table-border-none"
																		id="hideReleaseNote">
																		<tr>
																			<td><label for="releaseNote" class="mandTxt">Upload Release
																					Note</label>
																			</td>
																			<td><input type="file" name="releaseNotePath"
																				id="releaseNotePath" class="textboxBig"
																				onchange="checkReleaseNoteValidate(this)"
																				tabindex="10" />
																			</td>
																		</tr>
																	</table></td>
															</tr>
														</table>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
									<div class="navTabSec mrgnRt">
										<div align="right">
											<!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
											<input name="Save" value="Save" type="button"
												class="btn hltBtn" id="save" onclick="submitVersion()" />
											<input name="save" value="Clear" type="button" class="btn"
												id="Clear" onclick="clearVersionForm()" /> <input
												name="save2" value="Back" type="button" class="btn" id="Back"
												onclick="backToDisplay()" />
										</div>
									</div>
								</div>
							</form:form>
						</fieldset>
					</div>
				</div>
			</div>

			<div id="footer">
				<div id="ourpartners_info">
					<div class="floatR" id="followus_section">
						<p>&nbsp;</p>
					</div>
					<div class="clear"></div>
				</div>
				<div id="footer_nav">
					<div class="clear"></div>
					<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
						rights reserved</div>
					<p align="center">&nbsp;</p>
				</div>
			</div>
		</div>
	</body>
	</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Version Management</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">

	function couponAdmin() {
	  document.manageversionform.action = "displaycoupon.htm";
	  document.manageversionform.method = "post";
	  document.manageversionform.submit();
	}

	function promotionAdmin() {
	  document.manageversionform.action = "displaypromotion.htm";
	  document.manageversionform.method = "post";
	  document.manageversionform.submit();
	}

	function retailerAdmin() {
	  document.manageversionform.action = "displayretailer.htm";
	  document.manageversionform.method = "post";
	  document.manageversionform.submit();
	}

	function hotdealAdmin() {
	  document.manageversionform.action = "displayhotdeal.htm";
	  document.manageversionform.method = "post";
	  document.manageversionform.submit();
    }

	function versionAdmin() {
		document.manageversionform.searchKey.value = "";
		document.manageversionform.action = "displayversion.htm";
		document.manageversionform.method = "post";
		document.manageversionform.submit();
	}

	function searchVersion() {
		document.manageversionform.action = "displayversion.htm";
		document.manageversionform.method = "post";
		document.manageversionform.submit();
	}

	function callNextPage(pagenumber, url) {
		document.manageversionform.pageNumber.value = pagenumber;
		document.manageversionform.pageFlag.value = "true";
		document.manageversionform.action = url;
		document.manageversionform.method = "post";
		document.manageversionform.submit();
	}
	function addVersion()
	{
		document.manageversionform.action = "addversion.htm";
		document.manageversionform.method = "GET";
		document.manageversionform.submit();
	}
	function editVersion(versionId) {
		document.manageversionform.versionId.value = versionId;
		document.manageversionform.action = "editversion.htm";
		document.manageversionform.method = "post";
		document.manageversionform.submit();
	}

</script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-version_mgt.png" alt="ScanSee"
					width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">
				<div class="contBlock">
					<form:form name="manageversionform" commandName="manageversionform" acceptCharset="ISO-8859-1">
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<form:hidden path="versionId" value="${requestScope.versionList.versionId}" />
						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<tr>
										<td class="Label" width="18%"><label for="Supr_srchRbts">
												Version No.# </label></td>
										<td width="60%"><form:input path="searchKey" type="text" name="textfield" />
											 <a href="#" title="Search Version"> <img
												src="images/searchIcon.png" onclick="searchVersion()"
												alt="Search" title="Search" width="20" height="17" />
										</a></td>
										<td width="22%"><input type="button" class="btn"
											title="Add Version" value="Add" onclick="addVersion()" /></td>
									</tr>
								</table>
							</div>
						</fieldset>
					</form:form>
				</div>
				<center>
					<span class="highLightErr"><c:out value="${requestScope.errorMessage}" /> </span>
				</center>
				<fieldset>
					<legend>Manage Version Details</legend>
					<div id="scrlGrd" class="grdCont searchGrd">
					
						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0"
							cellpadding="0" width="100%">
							<tbody>
								<tr class="header">
									<td width="8%">Application Type</td>
									<td width="10%">Version No.#</td>
									<td width="10%">QA Release Date </td>
									<td width="10%">Production Release Date</td>
									<td width="10%">Apple Approval Date</td>
									<td width="10%">Release Environment</td>
									<td width="4%" align="center">Action</td>
								</tr>
							<c:if test="${!empty sessionScope.versionList}"> 
								<c:forEach items="${sessionScope.versionList}" var="item">
									<tr>
										<td align="left"><c:out value="${item.appType}" /></td>
										<td align="left"><c:out value="${item.appVersionNo}"/></td>
										<td align="left">
										<c:choose><c:when test="${item.lastQARelDate ne null && item.lastQARelDate ne ''}">
										      			<c:out value="${item.lastQARelDate}"/>
										    </c:when>
											<c:otherwise>
													<c:out value="${item.firstQARelDate}"/>
											</c:otherwise>
										</c:choose>
										</td>
										<td>
										<c:choose><c:when test="${item.lastProdtnRelDate ne null && item.lastProdtnRelDate ne ''}">
										      			<c:out value="${item.lastProdtnRelDate}"/>
										    </c:when>
											<c:otherwise>
													<c:out value="${item.firstProdtnRelDate}"/>
											</c:otherwise>
										</c:choose>
										</td>
										<td><c:out value="${item.iTunesUploadDate}" /></td>
										<td>
										<c:choose><c:when test="${item.relProdtnNotePath ne null && item.relProdtnNotePath ne '' && item.relTypeId == 2}">
										<a href="${item.relProdtnNotePath}" target="_blank" >
										      		<p title='<c:out value="Production Release Note"/>'><c:out value="Production"/></p></a>
										    </c:when>
										    <c:when test="${item.relQANotePath ne null && item.relQANotePath ne '' && item.relTypeId == 1}">
												<a href='<c:out value="${item.relQANotePath}" />' target="_blank" >
										      			<p title='<c:out value="QA Release Note"/>'><c:out value="QA"/></p></a>
										    </c:when>
										     <c:when test="${item.buildApprovalDate ne null && item.buildApprovalDate ne '' && item.relTypeId == 3}">
												<p title='<c:out value="iTunes Release Note"/>'><c:out value="iTunes"/></p>
										    </c:when>
											<c:otherwise>
										      			<p title='<c:out value="No Release Note"/>'><c:out value="No Release Note"/></p>
											</c:otherwise>
										</c:choose>
										</td>
										<td width="5%" align="center"><a href="#"
											onclick="editVersion('${item.versionId}')"> 
											<img src="images/edit_icon.png" alt="edit" title="edit" width="18" height="18" />
										</a></td>
									</tr>
								</c:forEach>
								</c:if>
							</tbody>
						</table>
						
					</div>
					<div class="pagination">
						<p>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
			</div>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Filter Administration</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">

	function searchFilter(search) {
		document.filterform.removeMsg.value = search;
		document.filterform.action = "managefilters.htm";
		document.filterform.method = "post";
		document.filterform.submit();
	}

	function callNextPage(pagenumber, url) {
		document.filterform.pageNumber.value = pagenumber;
		document.filterform.pageFlag.value = "true";
		document.filterform.action = url;
		document.filterform.method = "post";
		document.filterform.submit();
	}
	
	function addFilter() {
		document.filterform.action = "addfilter.htm";
		document.filterform.method = "post";
		document.filterform.submit();
	}
	
	function editFilter(filterId) {
		document.filterform.filterId.value = filterId;
		document.filterform.action = "editfilter.htm";
		document.filterform.method = "post";
		document.filterform.submit();
	}
	
	function deleteFilter(filterId) {
	var r = confirm("Are you sure you want to delete this filter?");
		if (r == true) {
			document.filterform.filterId.value = filterId;
			document.filterform.action = "deletefilter.htm";
			document.filterform.method = "post";
			document.filterform.submit();
		}
	}
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-filter.png" alt="ScanSee"
					width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">

				<div id="subnav" class="tabdSec">
					<ul>
						<li><a class="active" href="#" id="reviewDet"><span>Manage
									Filters</span></a></li>
					</ul>
				</div>
				<div class="contBlock">
					<form:form name="filterform" commandName="filterform"
						acceptCharset="ISO-8859-1">
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<form:hidden path="filterId" />
						<form:hidden path="removeMsg" value ="managefilters"/>

						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="grdTbl">
									<tr>
										<td class="Label" width="18%"><label for="Supr_srchRbts">
												Filter </label></td>
										<td width="60%"><form:input path="searchKey" type="text"
												name="searchKey" /> <a href="#" title="Search Filter">
												<img src="images/searchIcon.png" onclick="searchFilter('managefilters');"
												alt="Search" title="Search" width="20" height="17" />
										</a></td>
										<td width="22%"><input type="button" class="btn"
											title="Add Filter" value="Add" onclick="addFilter();" /></td>
									</tr>
								</table>
							</div>
						</fieldset>
					</form:form>
				</div>

				<c:if test="${sessionScope.responseFilterMsg ne null }">
					<div id="message">
						<center>
							<label style="${sessionScope.filterFont}"><c:out
									value="${sessionScope.responseFilterMsg}" /></label>
						</center>
					</div>
					<script>var PAGE_MESSAGE = true;</script>
				</c:if>

				<fieldset>
					<legend>Filter Details</legend>
					<div id="scrlGrd" class="grdCont searchGrd">

						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0"
							cellpadding="0" width="100%">
							<tbody>
								<tr class="header">
									<td width="24%">Filter Name</td>
									<td align="center">Category</td>
									<td colspan="2" align="center">Action</td>
								</tr>


								<c:if test="${!empty sessionScope.filterList}">
									<c:forEach items="${sessionScope.filterList}" var="item">
										<tr>

											<td align="left"><c:out value="${item.filterName}" /></td>
											<td width="64%" align="left"><c:out
													value="${item.fCategoryName}" /></td>


											<td width="5%" align="center"><a href="#"
												onclick="editFilter('<c:out value="${item.filterId}"/>');">
													<img src="images/edit_icon.png" alt="edit" title="edit"
													width="25" height="19" />
											</a></td>

											<td width="7%" align="center"><a href="#"
												onclick="deleteFilter('<c:out value="${item.filterId}"/>');">
													<img src="images/delicon.png" alt="delete" title="delete"
													width="18" height="18" />
											</a></td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>

					</div>
					<div class="pagination">
						<p>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
			</div>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Export</title>
		<link rel="stylesheet" type="text/css" href="styles/styles.css" />
		<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="scripts/jquery.js"></script>
		<script type="text/javascript" src="scripts/global.js"></script>
		<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>		
		<script type="text/javascript" src="scripts/datetimepicker.js"></script>
		
		<script type="text/javascript">
		
			$(document).ready(function() {	
				
				$("#fromDate").datepicker({
					showOn : 'both',
					buttonImageOnly : true,
					buttonText : 'Click to show the calendar',
					buttonImage : '/ScanSeeAdmin/images/cal.gif'
				});

				$("#toDate").datepicker({
					showOn : 'both',
					buttonImageOnly : true,
					buttonText : 'Click to show the calendar',
					buttonImage : '/ScanSeeAdmin/images/cal.gif'
				});
				$('.ui-datepicker-trigger').css("padding-left", "5px");
				
				var msg = '${requestScope.successmsg}';
				if(null != msg && "" != msg) {
					alert(msg);
				}
				
				$("#export").attr('disabled','disabled');
				
				$("input[type='radio'][name='hubCitiId']").click(function() {
					var hubCitiId = $("input[type='radio'][name='hubCitiId']:checked").val();
					var moduleIds = $("input[type='checkbox'][name='moduleIds']:checked").val();

					if("" != hubCitiId && typeof(hubCitiId) != "undefined" && "" != moduleIds && typeof(moduleIds) != "undefined") {
						$("#export").removeAttr('disabled').addClass("hltBtn");
					} else {
						$("#export").attr('disabled','disabled').removeClass("hltBtn");
					}					
					  
				});
				
				$("input[type='checkbox'][name='moduleIds']").click(function() {
					
					var isChecked = $(this).is(":checked");
					//alert($(this).attr("ismadatory"))
					if(!isChecked) {
						var mad = $(this).attr("ismadatory");
						if(mad == "true") {
							alert("Retailer Information is mandatory for other options. So you can't de-select it.");
							$(this).attr("checked", "checked");
						}
					}
					
					var hubCitiId = $("input[type='radio'][name='hubCitiId']:checked").val();
					var moduleIds = $("input[type='checkbox'][name='moduleIds']:checked").val();
					  
					if("" != hubCitiId && typeof(hubCitiId) != "undefined" && "" != moduleIds && typeof(moduleIds) != "undefined") {
						$("#export").removeAttr('disabled').addClass("hltBtn");
					} else {
						$("#export").attr('disabled','disabled').removeClass("hltBtn");
					}					
					
				});
				
			});
		
		</script>
		
		<script type="text/javascript">
		
			function displayExport(moduleId, moduleName) {
				document.module.moduleTypeId.value = moduleId;
				document.module.moduleTypeName.value = moduleName;
				document.module.action = "displayexport.htm";
				document.module.method = "GET";
				document.module.submit();				
			}
			
			function exportData() {
				
				var validationMsg = "";
				var isEmpty = false;
				var inpfromDate = $("#fromDate").val();
				var inptoDate = $("#toDate").val();
				
				var today = new Date();
				var fromDate = new Date(inpfromDate);
				var toDate = new Date(inptoDate);
				
				if(inpfromDate == "" && inptoDate == "") {
					validationMsg = "Please select From and To date";
					isEmpty = true;
				} else if(inpfromDate == "") {
					validationMsg = "Please select From date";
					isEmpty = true;
				} else if(inptoDate == "") {
					validationMsg = "Please select To date";
					isEmpty = true;
				} else if (toDate > today) {
					validationMsg = "To date should be less than or equal to Current Date";
					isEmpty = true;
				} else if (fromDate > toDate) {
					validationMsg = "From Date should be less than or equal to To Date";
					isEmpty = true;
				}
				
				

				if(!isEmpty) {
					var moduleNames = "";
					var length = $("input[type='checkbox'][name='moduleIds']:checked").length;
					var i = 1;
					$("input[type='checkbox'][name='moduleIds']:checked").each(function () {
						if(moduleNames === "") {
							moduleNames = $(this).attr("moduleNames");
						} else if(length == i){
							moduleNames += " and " + $(this).attr("moduleNames");
						} else {
							moduleNames += ", " + $(this).attr("moduleNames");
						}	 
						i++;
					});
					document.module.moduleNames.value = moduleNames;
					document.module.hubCitiNames.value = $("input[type='radio'][name='hubCitiId']:checked").attr("hubCitiNames");
					document.module.action = "exportdata.htm";
					document.module.method = "POST";
					document.module.submit();
				} else {
					alert(validationMsg);
				}
			}
		
		</script>
		
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="header_logo" class="floatL">
					<a href="#"> <img src="images/scansee-logo-hubciti.png" alt="ScanSee" width="400" height="54" /> </a>
				</div>
				<div id="header_link" class="floatR">
					<ul>
						<li>Welcome <span class="highLightTxt"> <c:out value="${sessionScope.userName}" />, </span></li>
						<li><img src="images/logoutIcon.gif" alt="Logut" align="left" title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a></li>
					</ul>
				</div>
			</div>
			<div id="content" class="clear-fix" style="min-height:450px;">
				<div class="floatL" id="vNav">
					<div class="vNavtopBg"></div>
					<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
					<div class="vNavbtmBg"></div>
				</div> 
		    	<div class="rtContPnl floatR">
		    		<form:form name="module" commandName="module" acceptCharset="ISO-8859-1">
		    		<form:hidden path="moduleTypeName" value="${sessionScope.selectedModuleName}"/>
		    		<form:hidden path="moduleNames" />
		    		<form:hidden path="hubCitiNames" />
		    		<input type="hidden" name="searchKey">
						<div id="subnav" class="tabdSec">
							<ul>				    	
						    	<c:forEach items="${sessionScope.modules}" var="module">
						    		<c:choose>
						    			<c:when test="${module.moduleTypeId eq sessionScope.selectedModule}">
						    				<form:hidden path="moduleTypeId" value="${module.moduleTypeId}"/>
						    				<li><a class="active" href="#" id="mod-${module.moduleTypeId}"><span>${module.moduleTypeName}</span></a></li>	
						    			</c:when>
						    			<c:otherwise>
						    				<li><a href="#" id="mod-${module.moduleTypeId}" onclick="displayExport('${module.moduleTypeId}', '${module.moduleTypeName}')"><span>${module.moduleTypeName}</span></a></li>	
						    			</c:otherwise>
						    		</c:choose>						    						    	
						    	</c:forEach>
						    	<li><a href="displayexporthubcities.htm"><span>Export HubCities</span></a></li>					    	
						  	</ul>
						</div>
			      		<div class="contBlock mrgnTop">
				        	<fieldset>
				          		<legend>Export ${sessionScope.selectedModuleName}</legend>
				          		<div class="contWrap removeBtmBrdr">
					            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">					            
					              	<tr>
					                	<td width="20%" align="left" valign="top" class="Label"><label for="hubCitiName">HubCities</label></td>
					                	<td colspan="3">
					                		<ul class="form-list">
						                      		<c:forEach items="${sessionScope.hubcities}" var="hubciti">
											    		<li>
											    			<form:radiobutton path="hubCitiId" value="${hubciti.hubCitiID}" hubCitiNames="${hubciti.hubCitiName}"/>
						                      				<label for="${hubciti.hubCitiName}">${hubciti.hubCitiName}</label>
						                      			</li>					    	
											    	</c:forEach>
					                  		</ul>          
					                  	</td>
					              	</tr>					              
					              	<tr>
										<td align="left" valign="top" class="Label">Options</td>
					                	<td colspan="3">
					                		<ul class="form-list">
					                      		<c:forEach items="${sessionScope.exportOptions}" var="option">
										    		<li>
										    			<c:choose>
										    				<c:when test="${option.isMandatory eq true}">
										    					<form:checkbox path="moduleIds" value="${option.moduleId}" moduleNames="${option.moduleName}" isMadatory="${option.isMandatory}" checked="checked"/>
										    				</c:when>
										    				<c:otherwise>
										    					<form:checkbox path="moduleIds" value="${option.moduleId}" moduleNames="${option.moduleName}" isMadatory="${option.isMandatory}"/>
										    				</c:otherwise>
										    			</c:choose>
										    			
						                      			<label for="${option.moduleName}">${option.moduleName}</label>
					                      			</li>					    	
										    	</c:forEach>					                    	
						                  	</ul>
						                  </td>
					              	</tr>
					              	<tr>
					                	<td class="Label">From</td>
					                	<td><form:input path="fromDate" maxlength="10" cssClass="textboxDate" readonly="true"/>&nbsp; (MM/DD/YYYY)</td>
					                	<td class="Label">To</td>
					                	<td><form:input path="toDate" maxlength="10" cssClass="textboxDate" readonly="true"/>&nbsp; (MM/DD/YYYY)</td>
					              	</tr>
					              	<tr>
					                	<td class="Label">&nbsp;</td>
					                	<td colspan="3"><input type="button" id="export" class="btn" value="Export" onclick="exportData();"/></td>
					              	</tr>
								</table>
							</div>
						</fieldset>
					</div>
				</form:form>
			</div>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div> 
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>
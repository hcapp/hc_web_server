<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Retailer/Band</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>
<script>
    function saveRetailerLoginDetails() {
        var name = document.retailer.userName.value;
        var emailId = document.retailer.email.value;
        name = name.trim;
        emailId = emailId.trim;
        if (name == "" || emailId == "") {
            document.retailer.action = "saveRetailerLogin.htm";
            document.retailer.method = "post";
            document.retailer.submit();
        } else {
            //alert("Login information will be saved and password will be sent to specified Email ID");
            var isUpdateLogin = confirm("Login information will be saved and password will be sent to specified Email ID");
            if (isUpdateLogin) {
                document.retailer.action = "saveRetailerLogin.htm";
                document.retailer.method = "post";
                document.retailer.submit();
            }
        }
    }

    function backDisplayRetailer(pageNum) {
        var vRetailerCredentials = confirm("Are you sure you want to leave this page without saving the changes!");
        if (vRetailerCredentials) {
            callNextPage(pageNum, "displayretailer.htm");
        }
    }
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<c:choose>
				<c:when test="${retailer.isBand ne null && retailer.isBand == true}">
					<a href="index.html"> <img src="images/scansee-logo-band.png" alt="ScanSee" width="411" height="54" /></a>
				</c:when>
				<c:otherwise>
					<a href="index.html"> <img src="images/scansee-logo-rtlr.png" alt="ScanSee" width="411" height="54" /></a>
				</c:otherwise>
				</c:choose>



			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>User Name: <span class="highLightTxt"><c:out value="${sessionScope.userName}" />, </span></li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left" /> <a href="logout.htm" title="Logut"> &nbsp;Logout</a></li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">

			<!--Right container starts here-->
			<div class="mrgnTop">

				<div class="contBlock">
					<fieldset>
						<c:choose>
							<c:when test="${retailer.isBand ne null && retailer.isBand == true}">
								<legend>Band</legend>
							</c:when>
							<c:otherwise>
								<legend>Retailer</legend>
							</c:otherwise>
						</c:choose>
						<form:form name="retailer" commandName="retailer">
							<!--<form:hidden path="userName" />
							<form:hidden path="email" /> -->
							<form:hidden path="retailName" />
							<form:hidden path="retailId" />
							<form:hidden path="isLoginExist" />
							<form:hidden path="isBand" />
							<input type="hidden" name="pageNumber" />
							<input type="hidden" name="pageFlag" />
							<form:hidden path="currentPageNum" />
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
									<tr>
										<c:choose>
											<c:when test="${retailer.isLoginExist ne null && retailer.isLoginExist != 0}">
												<td colspan="2" class="header">Edit Login</td>
											</c:when>
											<c:otherwise>
												<td colspan="2" class="header">Create Login</td>
											</c:otherwise>
										</c:choose>
									</tr>
									<tr>
										<td class="Label" width="17%"><label for="usrName" class="mandTxt">User Name</label></td>
										<!-- <td><input type="text" name="usrName" id="usrName" />&nbsp; <label></label></td> -->
										<td><form:input type="text" path="userName" value="${retailer.userName}" />&nbsp; <form:errors path="userName" cssStyle="color:red"></form:errors><label></label></td>
									</tr>
									<tr>
										<td class="Label"><label for="" class="mandTxt">Email</label></td>
										<!-- <td><input type="text" name="usrEmail" id="usrEmail" /></td> -->
										<td><form:input type="text" path="email" size="" value="${retailer.email}" /> <form:errors path="email" cssStyle="color:red"></form:errors></td>
									</tr>
								</table>

							</div>
							<div class="navTabSec mrgnRt">
								<div align="right">
									<c:choose>
										<c:when test="${retailer.isLoginExist ne null && retailer.isLoginExist != 0}">
											<input type="button" value="Update" class="btn" name="saveRtrlr" onclick="saveRetailerLoginDetails()" />
										</c:when>
										<c:otherwise>
											<input type="button" value="Save" class="btn" name="saveRtrlr" onclick="saveRetailerLoginDetails()" />
										</c:otherwise>
									</c:choose>
									<!-- <input type="button" value="Save" class="btn" name="saveRtrlr" /> -->
									<input name="Cancel2" value="Back" type="button" class="btn" onclick="backDisplayRetailer('${sessionScope.pagination.currentPage}')" />
								</div>
							</div>
						</form:form>
					</fieldset>
				</div>
				<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
					<div class="headerIframe">
						<img src="images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
							title="Click here to Close" align="middle" /> <span id="popupHeader"></span>
					</div>
					<iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color: White">
					</iframe>
				</div>
			</div>
			<!--Right container ends here-->
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">
					Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All rights reserved
				</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="menu" uri="/WEB-INF/menu.tld"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hot Deals</title>
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/global.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
				$('input[name$="deleteAll"]').click(function() {
					var status = $(this).attr("checked");
					$('input[name$="checkboxDel"]').attr('checked',status);
					$del = $('#delete');
					$del.removeAttr('disabled').addClass('hltBtn');
				
					if(!status) {
					$('input[name$="checkboxDel"]').removeAttr('checked');
					$del.attr('disabled','disabled').removeClass('hltBtn');
					}
				});
				$('input[name$="checkboxDel"]').click(function() {											   
					var tolCnt = $('input[name$="checkboxDel"]').length;	
					var chkCnt = $('input[name$="checkboxDel"]:checked').length;
					if(tolCnt == chkCnt)
						$('input[name$="deleteAll"]').attr('checked','checked');
					else
						$('input[name$="deleteAll"]').removeAttr('checked');
					
					$del = $('#delete');
					if(chkCnt > 0)
					{
						
						$del.removeAttr('disabled').addClass('hltBtn');
					}
					else{
						$del.attr('disabled','disabled').removeClass('hltBtn');
					}
				});	

	});
</script>
<script type="text/javascript">
	function searchHotDeal(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') {
			document.hotdeal.action = "searchhotdeal.htm";
			document.hotdeal.method = "post";
			document.hotdeal.submit();

		}
	}
	 
	function searchHotDealDet() {
			document.hotdeal.action = "searchhotdeal.htm";
			document.hotdeal.method = "post";
			document.hotdeal.submit();
	}
	
	function showExpireHotDeal()
	{
		var showExpire = $('input[name$="showExpirebtn"]').val();
		if (showExpire == "Show Expired") {
			document.hotdeal.showExpired.value = "false";
		} else {
			document.hotdeal.showExpired.value = "true";
		}
		document.hotdeal.action = "searchhotdeal.htm";
		document.hotdeal.method = "post";
		document.hotdeal.submit();
	}

	function sortColumn(columnName) {
		document.hotdeal.columnName.value = columnName;
		document.hotdeal.action = "sorthotdeal.htm";
		document.hotdeal.method = "post";
		document.hotdeal.submit();
	}

	function deleteHotDeals(){				
		var checkedValue = [];
		var i = 0;
		var r;
		chkdBx = $('input[name="checkboxDel"]:checkbox:checked').length;
		if(chkdBx > 0) {
			$('input[name="checkboxDel"]:checked').each(function(){
			 	checkedValue[i] = $(this).val();
				 i++;
	    	});
		}
		if(checkedValue.length == 1)
		{
			r = confirm("Are you sure you want to delete this HotDeal ?");
				
		}else
		{
			r = confirm("Are you sure you want to delete these HotDeals ?");
		}
		if(r == true){
			document.hotdeal.hotDealIDs.value = checkedValue;
			document.hotdeal.action = "deletehotdeal.htm";
			document.hotdeal.method = "post";
			document.hotdeal.submit();					
		}
	}

	function deleteHotDeal(DealId)
	{			
		var r = confirm("Are you sure you want to delete this HotDeal ?");
		if(r == true){
			document.hotdeal.hotDealIDs.value = DealId;
			document.hotdeal.action = "deletehotdeal.htm";
			document.hotdeal.method = "post";
			document.hotdeal.submit();
		}			
	}

	function editHotDeal(DealId)
	{			
		document.hotdeal.productHotDealID.value = DealId;
		document.hotdeal.action = "edithotdeal.htm";
		document.hotdeal.method = "post";
		document.hotdeal.submit();
	}
</script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="#"> <img src="images/scansee-logo-hotdealAdmin.png"
					alt="ScanSee" width="400" height="54" />
				</a>
			</div>
			<div id="header_link" class="floatR">
				<ul>
					<li>Welcome <span class="highLightTxt"> <c:out
								value="${sessionScope.userName}" />,
					</span>
					</li>
					<li><img src="images/logoutIcon.gif" alt="Logut" align="left"
						title="Logut" /> <a href="logout.htm" title="Logut">&nbsp;Logout</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="content" class="clear-fix">
			<div class="floatL" id="vNav">
				<div class="vNavtopBg"></div>
				<menu:MenuTag menuTitle="${sessionScope.moduleName}" />
				<div class="vNavbtmBg"></div>
			</div>
			<div class="rtContPnl floatR">
			<div class="contBlock">
					<form:form name="hotdeal" commandName="hotdeal" acceptCharset="ISO-8859-1">
						<form:hidden path="showExpired" value="${sessionScope.showExpire}" />
						<form:hidden path="columnName" value="${sessionScope.columnName}" /> 
						<form:hidden path="hotDealIDs"/>
						<form:hidden path="productHotDealID"/>
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<fieldset>
							<legend>Search</legend>
							<div class="contWrap removeBtmBrdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
									<tr>
										<td class="Label" width="18%"><label for="Supr_srchRbts"> Hot Deal Name </label></td>
										<td width="60%"><c:if test="${sessionScope.searchKey ne null && !empty sessionScope.searchKey}">
												<form:input path="searchKey" onkeypress="searchHotDeal(event)" value="${sessionScope.searchKey}" />
											</c:if> <c:if test="${sessionScope.searchKey eq null || empty sessionScope.searchKey}">
												<form:input path="searchKey" onkeypress="searchHotDeal(event)" />
											</c:if> <a href="#" title="Search HotDeal"> <img src="images/searchIcon.png" onclick="searchHotDealDet()" alt="Search" width="20" height="17" /> </a></td>
									</tr>
								</table>
							</div>
						</fieldset>
						<c:if test="${sessionScope.showExpire eq 'true'}">
							<div class="mrgnBtm" align="center">
								<input type="button" class="btn" id="showExpirebtn" name="showExpirebtn" value="Show Expired" title="Show Expired" onclick="javascript:showExpireHotDeal()" />
							</div>
						</c:if>
						<c:if test="${sessionScope.showExpire eq 'false'}">
							<div class="mrgnBtm" align="center">
								<input type="button" class="btn" id="showExpirebtn" name="showExpirebtn" value="Do Not Show Expired" title="Do Not Show Expired" onclick="javascript:showExpireHotDeal()" />
							</div>
						</c:if>
					</form:form>
				</div>
				<center>
					<span class="highLightTxt"><c:out value="${requestScope.message}" /> </span> <span class="highLightErr"><c:out value="${requestScope.errorMessage}" /> </span>
				</center>
				<fieldset>
					<legend>Hot Deal Details</legend>
					<div id="scrlGrd" class="grdTbl searchGrd">
						<table id="cpnLst" class="stripeMe" border="0" cellspacing="0" cellpadding="0" width="100%">
							<tbody>
								<tr class="header">
									<td width="4%" align="center">
										<c:if test="${!empty sessionScope.hotdeallist}">
											<label>
												<input type="checkbox" name="deleteAll" />
											</label>
										</c:if>
									</td>
									<td width="20%" onclick="sortColumn('Name')">
										<a href="#" title="HotDeal Name">
											Name 
											<c:if test="${!empty sessionScope.HotDealNameImg}">
												<img src="${sessionScope.HotDealNameImg}" width="11" height="6" />
											</c:if>
										</a>
									</td>
									<td width="25%" onclick="sortColumn('Partner')">
										<a href="#" title="API Partner">
											Partner 
											<c:if test="${!empty sessionScope.HotDealPartnerImg}">
												<img src="${sessionScope.HotDealPartnerImg}" width="11" height="6" />
											</c:if>
										</a>
									</td>
									<td width="12%" onclick="sortColumn('Price')"> 
										<a href="#" title="Price">
											Price
											<c:if test="${!empty sessionScope.HotDealPriceImg}">
												<img src="${sessionScope.HotDealPriceImg}" width="11" height="6" />
											</c:if>
										</a>
									</td>
									<td width="15%">Sale Price</td>
									<td width="10%" onclick="sortColumn('StartDate')"> 
										<a href="#" title="Start Date">
											Start Date
											<c:if test="${!empty sessionScope.HotDealStartDateImg}">
												<img src="${sessionScope.HotDealStartDateImg}" width="11" height="6" />
											</c:if>
										</a>
									</td>
									<td width="9%" onclick="sortColumn('EndDate')"> 
										<a href="#" title="End Date">
											End Date
											<c:if test="${!empty sessionScope.HotDealEndDateImg}">
												<img src="${sessionScope.HotDealEndDateImg}" width="11" height="6" />
											</c:if>
										</a>
									</td>
									<td width="9%" colspan="2" align="center">Action</td>
								</tr>
								<c:forEach items="${sessionScope.hotdeallist}" var="item">
									<tr>
										<td align="center"><input type="checkbox" name="checkboxDel" value="${item.productHotDealID}" /></td>
										<td align="left"><c:out value="${item.hotDealName}" escapeXml="false"/></td>
										<td align="left"><c:out value="${item.APIPartnerName}" /></td>
										<td><c:out value="${item.price}" /></td>
										<td><c:out value="${item.salePrice}" /></td>
										<td><c:out value="${item.hotDealStartDate}" /></td>
										<td><c:out value="${item.hotDealEndDate}" /></td>
										<td width="6%" align="center"><a href="#" onclick="editHotDeal('<c:out value="${item.productHotDealID}"/>')"> <img src="images/edit_icon.png" alt="edit" title="Edit" width="25" height="19" /> </a></td>
										<td width="5%" align="center"><a href="#" onclick="deleteHotDeal('<c:out value="${item.productHotDealID}"/>');"> <img src="images/delicon.png" alt="delete" title="Expire" width="18" height="18" /> </a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="pagination">
						<p>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" />
						</p>
					</div>
				</fieldset>
				<div class="mrgnTop" align="right">
					<input type="button" class="btn" id="delete" value="Delete"
						title="Delete" onclick="deleteHotDeals();" disabled="disabled" />
				</div>
			</div>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div class="floatR" id="followus_section">
					<p>&nbsp;</p>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer_nav">
				<div class="clear"></div>
				<div id="footer_info">Copyright &copy; 2011 HubCiti<sup>&#174;</sup>. All
					rights reserved</div>
				<p align="center">&nbsp;</p>
			</div>
		</div>
	</div>
</body>
</html>
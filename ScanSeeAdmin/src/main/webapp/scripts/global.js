var cont = 1;
var actIndex;
var saveRetalLocation = false;
var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;
var isIE = $.browser.msie;
var bVer = $.browser.version;
var isTE8 = isIE && bVer < 9 || document.documentMode == 8;
var isChrome = /Chrome/.test(navigator.userAgent)
&& /Google Inc/.test(navigator.vendor);
$(document).ready(function() {
	

	/*manage locations grid row upload image function*/
   
	if (isTE8) {
		$('html').addClass('ie8');
	}
	
	if (isChrome) {
		$('html').addClass('chrome');
	}
	
				
					  
		$(document ).delegate( ".trgrChlAll", "change", function() {		
			var chkdstatus = $(this).attr("checked");
			var self = this;
			if(self.checked) {
				$(this).parents('div.col').find('.treeview input:checkbox').each(function(index, element) {
				$(this).attr("checked",chkdstatus);
				$(this).parents('div.col').find('.treeview li.subCtgry').show();
			});
			}else {
				$(this).parents('div.col').find('.treeview input:checkbox').each(function(index, element) {
				$(this).removeAttr("checked");
				$(this).parents('div.col').find('.treeview li.subCtgry').hide();
			});
			}
		});
				
			$( document ).delegate( ".treeview li.mainCtgry input:checkbox", "click", function() {			
		
			var getId = $(this).parents('ul').attr("id");
			var maincatNm =  $(this).parents('li').attr('name');	
			var self = this;
			if(self.checked) {
				$('#'+getId).find('li[name="'+maincatNm+'"]').show();
				$('#'+getId).find('li[name="'+maincatNm+'"]').each(function(i) {
					if(i>0 && self.checked) {
						$(this).find('input:checkbox').attr('checked', 'checked');
					}	
				}); 
			}else {
				$('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"]').hide();
				$('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"]').each(function() {
					$(this).find('input:checkbox').removeAttr('checked');
				});
			}
			var chkbx = $('#'+getId).find(':checkbox').length;
			var chkbxChkd = $('#'+getId).find(':checkbox:checked').length;
			if(chkbx === chkbxChkd){
				$('#'+getId).prev('p').find(':checkbox').attr('checked', true);
			}
			else {
				$('#'+getId).prev('p').find(':checkbox').removeAttr('checked');
			}
		
		});
						
		$( document ).delegate( ".treeview li:not(.mainCtgry) input:checkbox", "click", function() {	
			var getId = $(this).parents('ul').attr("id");
			var maincatNm =  $(this).parents('li').attr('name');	
			if($('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length < 1) {
				$('#'+getId).find('li[name="'+maincatNm+'"]').each(function(i) {
					if(i==0) {
						$(this).find('input:checkbox').removeAttr('checked');
					}else {
						$(this).hide();	
					}
				});
			}
			/*var totChk = $('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox').length;
			var chkLen = $('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length;
			if(chkLen === totChk) {
				$('#'+getId).find('li.mainCtgry[name="'+maincatNm+'"] :checkbox').attr('checked','checked');
			} 
			else {	
				$('#'+getId).find('li.mainCtgry[name="'+maincatNm+'"] :checkbox').removeAttr('checked');
			}*/		
			
			totChk = $('#'+getId).find('li :checkbox').length;
			chkLen = $('#'+getId).find('li :checkbox:checked').length;
			if(chkLen === totChk) {
				$('#'+getId).prev('p').find(':checkbox').attr('checked', true);	
			} 
			else {
				$('#'+getId).prev('p').find(':checkbox').removeAttr('checked');
			}
		});
						
						
		
	$("input[name='img-upld']").change(function(){
		fileUploadGrid(this);
	});
	
	$('.trgrUpld').click(function() {

		if (!isIE && !isTE8)
			$('#trgrUpld').click(function(event) {
				event.stopPropagation();
			});

	});
	
	$(".row-edit").click(function() {
		rowEdit(this);
	});
	
	$(".row-edit-band").click(function() {
        rowEditBand(this);
    });

	$(".row-save").click(function() {
		validateRetailer(this);
	});
	
	$(".row-save-band").click(function() {
        validateBand(this);
    });
	$(".tgl-overlay").click(function(){
		$(".overlay").show();
	});
	$(".closeIframe").click(function(){
		$(".overlay").hide();
	});
	$("#Cancel").click(function(){
		$('.overlay', window.parent.document).hide();
	});

	/* Manage Zipcode changes Start */
	$("#delZipcode").click(function() {
		var selectedDefaultCode = $("#defaultZipcode option:selected").val();
		var length = $("#toPostalCode option:selected").length;
		if (length > 0) {
			var codes = "";
			$("#toPostalCode option:selected").each(function() {				
				var isAssocaited = $(this).attr("isAssocaited");				
				if("true" === isAssocaited) {		
					if("" === codes) {
						codes = $(this).val();
					} else {
						codes = codes + "," + $(this).val();
					}
				} else {		
					var postalCode = $(this).text();
					var zipCode = $(this).val();
					$.ajaxSetup({
						cache : false
					})
					$.ajax({
						type : "GET",
						url : "removezip.htm",
						data : {
							'zip' : postalCode
						},
	
						success : function(response) {
							$('#active').val(true);
							if(selectedDefaultCode == zipCode) {
								$("#defaultZipcode option[value='0']").attr('selected', 'selected');
							}
							$("#defaultZipcode option[value='" + zipCode + "']").remove();
						},
						error : function(e) {
							alert('error occured');
						}
					});				
				}

			});
			
			if("" != codes) {
				alert("Retailer Location(s) has been associated to Zip Code(s). Please delete the association and continue.");
			}
			
			var arr = codes.split(',');
			jQuery.each(arr, function(i, val) { 
				$("#toPostalCode option[value='" + val + "']").attr('selected', false);
			});

			$("#toPostalCode option:selected").remove();			
			
			var stateCode = $('#state').val();
			var city = $('#city').val();

			if ("" != stateCode && "" != city) {
				$.ajaxSetup({
					cache : false
				})
				$.ajax({
					type : "GET",
					url : "fetchzip.htm",
					data : {
						'state' : stateCode,
						'city' : city
					},

					success : function(response) {
						$('#PCDiv').html(response);
						$("#postalCodes").trigger("change");

						var totOpt = $('#postalCodes option').length;
						if (totOpt > 0) {
							$('#chkAllZip').removeAttr('disabled');
						} else {
							$('#chkAllZip').attr('disabled', 'disabled');
						}
					},
					error : function(e) {

					}
				});
			}
		} else {
			alert("Please select postal code to remove");
		}
	});
	/* Manage Zipcode changes End */

	$(".stripeMe tr").mouseover(function() {
		$(this).addClass("over");
	}).mouseout(function() {
		$(this).removeClass("over");
	});
	// $(".stripeMe tr:even").addClass("hvrEfct");
	$(".stripeMe tr:even,.list-view li:even").addClass("hvrEfct");

	// To avoid border for last td in each row
	$("table tr td:not(:last-child)").css({
		"border-right" : "1px solid #dadada"
	});
	$(".searchGrd table tr td:not(:last-child)").css({
		"border-right" : "1px solid #c1e4f0"
	});
	// $('#nav li').last().css('background', 'none');
	$('#clearFrm').click(function() {
		$('form').clearForm();// to clear form data
	});

	$(document).keypress(function(e) {
		// for enter key & space bar.
		if (e.which == 13 || e.which == 32) {
			// alert('You pressed enter!');
			var bool = $('#loginSec').find('ul.relogin').length > 0;
			if (bool) {
				validateUserForm([ '#pswdNew', '#pswdCfrm' ], 'li', chkUser);
			} else {
				validateUserForm([ '#password', '#userName' ], 'li', chkUser);
			}
		}
	});

//for region admin zipcode add/remove functionality.	
	$("#remRegionZipcode").click(function() {
		var selectedDefaultCode = $("#defaultZipcode option:selected").val();
		var length = $("#toPostalCode option:selected").length;
		if (length > 0) {
			var codes = "";
			$("#toPostalCode option:selected").each(function() {				
				var isAssocaited = $(this).attr("isAssocaited");				
				if("true" === isAssocaited) {	
					if("" === codes) {
						codes = $(this).val();
					} else {
						codes = codes + "," + $(this).val();
					}					
					
				} else {	
					var postalCode = $(this).text();
					var zipCode = $(this).val();
					$.ajaxSetup({
						cache : false
					})
					$.ajax({
						type : "GET",
						url : "removeRegionzip.htm",
						data : {
							'zip' : postalCode
						},
	
						success : function(response) {
							$('#active').val(true);
							if(selectedDefaultCode == zipCode) {
								$("#defaultZipcode option[value='0']").attr('selected', 'selected');
							}
							$("#defaultZipcode option[value='" + zipCode + "']").remove();
						},
						error : function(e) {
							alert('error occured');
						}
					});					
				}

			});
			
			if("" != codes) {
				alert("Retailer Location(s) has been associated to Zip Code(s). Please delete the association and continue.");
			}
			
			var arr = codes.split(',');
			jQuery.each(arr, function(i, val) { 
				$("#toPostalCode option[value='" + val + "']").attr('selected', false);
			});

			$("#toPostalCode option:selected").remove();			
			
			var stateCode = $('#state').val();
			var city = $('#city').val();

			if ("" != stateCode && "" != city) {
				$.ajaxSetup({
					cache : false
				})
				$.ajax({
					type : "GET",
					url : "getregpostalcode.htm",
					data : {
						'state' : stateCode,
						'city' : city
					},

					success : function(response) {
						$('#regZipcodeDiv').html(response);
						$("#postalCodes").trigger("change");

						var totOpt = $('#postalCodes option').length;
						if (totOpt > 0) {
							$('#chkAllZip').removeAttr('disabled');
						} else {
							$('#chkAllZip').attr('disabled', 'disabled');
						}
					},
					error : function(e) {

					}
				});
			}
		} else {
			alert("Please select postal code to remove");
		}
	});
	

		
    /* Filter Administration: ADD,EDIT,DELETE Filters*/
	
	
    $("#addNewOptn").click(function(){
    
                    addOption();
    });
    
     $("#editNewOptn").click(function(){          
                    var getEditTxt = $("#fltrTxtValue").val();
                    var vFValueId = $("#slctFilter option:selected").length;
                    
                    if(getEditTxt === ""){
                                    alert("Please Enter Filter Value");
                                    return false;
                    } else if (vFValueId === 1) {
                    	editOption();
                    } else {
                    	  alert("Please Select one Filter Value in the list to Edit.");
                          return false;
                    }
					
                    $("#slctFilter option:selected").text(getEditTxt);
                    $("#fltrTxtValue").val("");
                    $(".fltrValueRow").hide();
    });
     
     $("#delOption").click(function(){

    		 $("#fltrTxtValue").val("");
    		 $(".fltrValueRow").hide();

    		 var vFValueId = $("#slctFilter option:selected").length;

    		 if (vFValueId === 1) {
    			 deleteOption();
    		 } 
     });

     
    $(".jsactn").click(function(){
         controllerActn($(this).attr("id"));          
    });
	
	
    $("input[name='filterOption']").change(function(){
                    var getFltrOptn = $(this).attr("id");
                    if(getFltrOptn == "fltrYes"){
                         $(".fltrValue").show();
                    }else {
                    	$("#slctFilter").val("");
                        $(".fltrValue,.fltrValueRow").hide();    
                    }
    });
    $("input[name='filterOption']:checked").trigger('change');
    /* Filter Administration: ADD,EDIT,DELETE Filters ends*/

	$('.noEnterBtnSubmit').keypress(function(e){
	    if ( e.which == 13 ) return false;
	});
	
});

/*
 * This function for Opening the iframe popup by using the any events & we
 * cann't able to access any thing in the screen except popup parameters:
 * popupId : Popup Container ID IframeID : Iframe ID url : page path i.e src of
 * a page height : Height of a popup width : Width of a popup name : here name
 * means Header text of a popup btnBool : if you have any buttons means we can
 * use it i.e true/false
 */
function openIframePopup(popupId, IframeID, url, height, width, name, btnBool) {

	frameDiv = document.createElement('div');

	frameDiv.className = 'framehide';
	document.body.appendChild(frameDiv);
	document.getElementById(IframeID).setAttribute('src', url);

	document.getElementById(popupId).style.display = "block";
	height = (height == "100%") ? frameDiv.offsetHeight - 20 : height;
	width = (width == "100%") ? frameDiv.offsetWidth - 16 : width;
	document.getElementById(popupId).style.height = height + "px";
	document.getElementById(popupId).style.width = width + "px";
	var marLeft = -1 * parseInt(width / 2);
	var marTop = -1 * parseInt(height / 2);
	document.getElementById('popupHeader').innerHTML = name;
	document.getElementById(popupId).style.marginLeft = marLeft + "px";
	document.getElementById(popupId).style.marginTop = marTop + "px";
	var iframeHt = height - 27;
	document.getElementById(IframeID).height = iframeHt + "px";
	if (btnBool) {
		var btnHt = height - 50;
		document.getElementById(IframeID).style.height = btnHt + "px";
	}

}

/*
 * This function for Closing the iframe popup parameters: popupId : Popup
 * Container ID IframeID : Iframe ID
 */
function closeIframePopup(popupId, IframeID, funcUrl) {

	setTimeout(function() {
		try {

			document.body.removeChild(frameDiv);
			document.getElementById(popupId).style.display = "none";
			document.getElementById(popupId).style.height = "0px";
			document.getElementById(popupId).style.width = "0px";
			document.getElementById(popupId).style.marginLeft = "0px";
			document.getElementById(popupId).style.marginTop = "0px";
			document.getElementById(IframeID).removeAttribute('src');
			if (funcUrl) {
				funcUrl;
			}
		} catch (e) {
			top.$('.framehide').remove();
			top.document.getElementById(popupId).style.display = "none";
			top.document.getElementById(popupId).style.height = "0px";
			top.document.getElementById(popupId).style.width = "0px";
			top.document.getElementById(popupId).style.marginLeft = "0px";
			top.document.getElementById(popupId).style.marginTop = "0px";
			top.document.getElementById(IframeID).removeAttribute('src');
			if (funcUrl) {
				funcUrl;
			}
		}

	}, 200);
}

function callBackFunc() {

	var productName = []
	$('input[name="pUpcChk"]:checked').each(function() {
		productName.push(this.value);
	});
	var productID = []
	$('input[name="pUpcChk"]:checked').each(function() {
		productID.push(this.id);
	});

	var temp = productName.toString();
	var tep1 = temp.replace(new RegExp(",", "gm"), ",\n")
	var tit = top.document.forms[0].productUpcImg;
	tit.title = tep1;
	if (tep1.length > 100) {
		top.document.forms[0].productNameHidden.value = productName;
		tep1 = tep1.substring(0, 97);
		tep1 = tep1 + "...";
		productName = tep1;
	} else {
		top.document.forms[0].productNameHidden.value = productName;
	}
	top.document.forms[0].productUpc.value = productName;
	top.document.forms[0].productIDHidden.value = productID;

}

function resizeDoc() {
	var headerPanel = document.getElementById('header');
	var footerPanel = document.getElementById('footer');
	var resizePanel = document.getElementById('content');

	if (document.getElementById('wrapper')) {
		if (getWinDimension()[1]
				- (headerPanel.offsetHeight + footerPanel.offsetHeight + 20) > 0) {
			resizePanel.style.minHeight = getWinDimension()[1]
					- (headerPanel.offsetHeight + footerPanel.offsetHeight + 20)
					+ "px";
		}
	} else {
		resizePanel.style.minHeight = getWinDimension()[1]
				- (headerPanel.offsetHeight + footerPanel.offsetHeight + 120)
				+ "px";
	}
}

// To get client height
function getWinDimension() {
	var windowHeight = "";
	var windowWidth = "";

	if (!document.all || isOpera()) {
		windowHeight = window.innerHeight;
		windowWidth = window.innerWidth;
	} else {
		windowHeight = document.documentElement.clientHeight;
		windowWidth = document.documentElement.clientWidth;
	}
	return [ windowWidth, windowHeight ]

}

// check if the client is opera
function isOpera() {
	return (navigator.appName.indexOf('Opera') != -1);
}

function validateUserForm(arryNm, parent, funcCall) {
	var errBool = true;

	$('.errIcon').removeClass('errIcon');
	for ( var i in arryNm) {
		var $frmE = $(arryNm[i]);
		var val = $.trim($frmE.val());
		val = (val == "(   )      -") ? "" : val;
		if (val == '') {
			$frmE.parents(parent).find('.errDisp').addClass('errIcon');
			errBool = false;
		}
	}

	if (errBool) {
		if (typeof funcCall == "function")
			funcCall();

		else
			window.location.href = funcCall;

	}

}
function chkUser() {
	/*
	 * document.loginform.action = "login.htm"; document.loginform.method =
	 * "post"; document.loginform.submit();
	 */
	document.forms[0].submit();
}

$(document).ready(function() {
	$('#selectQA').click(function() {
		$('#selectItunes').hide();
		$('#selectQAProd').show();
		$('#hideReleaseNote').show();
	});
	$('#selectProduction').click(function() {
		$('#selectItunes').hide();
		$('#selectQAProd').show();
		$('#hideReleaseNote').show();
	});
	$('#seliTunes').click(function() {
		$('#selectItunes').show();
		$('#selectQAProd').show();
		$('#hideReleaseNote').hide();
		$('#hideApproveDate').hide();
		$('#approvedDate').hide();

	});

	$('#approvedYes').click(function() {

		$('#hideApproveDate').show();
		$('#approvedDate').show()

	});

	$('#approvedNo').click(function() {

		$('#hideApproveDate').hide();
		$('#approvedDate').hide()

	});
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 47 && charCode < 58) || charCode < 31 || charCode == 46)
		return true;

	return false;
}
function IsNumeric(e) {
	var key = window.event ? e.keyCode : e.which;
	var keychar = String.fromCharCode(key);
	reg = /[0-9]+(\.[0-9])?/;
	return reg.test(keychar);
}

function isAlphaNumeric(e) {
	var k = e.keyCode || e.charCode; // cross browser check
	// Mozilla and Safari define e.charCode, while IE defines e.keyCode which
	// returns the ASCII value

	// document.all ? k=e.keycode : k=e.which;
	return ((k > 47 && k < 58) || (k > 64 && k < 91) || (k > 96 && k < 123)
			|| k == 0 || k == 8 || k == 9);

}

function couponAdmin() {
	document.forms[0].searchKey.value = "";
	document.forms[0].action = "displaycoupon.htm";
	document.forms[0].method = "post";
	document.forms[0].submit();
}

function promotionAdmin() {
	document.forms[0].searchKey.value = "";
	document.forms[0].action = "displaypromotion.htm";
	document.forms[0].method = "post";
	document.forms[0].submit();
}

function retailerAdmin() {
	document.forms[0].searchKey.value = "";
	document.forms[0].action = "displayretailer.htm";
	document.forms[0].method = "post";
	document.forms[0].submit();
}

function hotdealAdmin() {
	document.forms[0].searchKey.value = "";
	document.forms[0].action = "displayhotdeal.htm";
	document.forms[0].method = "post";
	document.forms[0].submit();
}
function hubcitiAdmin() {
	document.forms[0].searchKey.value = "";
	document.forms[0].action = "displayhubcitiadmin.htm";
	document.forms[0].method = "post";
	document.forms[0].submit();
}
function versionAdmin() {
	document.forms[0].searchKey.value = "";
	document.forms[0].action = "displayversion.htm";
	document.forms[0].method = "POST";
	document.forms[0].submit();
}

function categoryAdmin(){
	document.forms[0].action = "eventcategory.htm";
	document.forms[0].method = "GET";
	document.forms[0].submit();
}

function nationwideAdmin(){
	document.forms[0].searchKey.value = "";
	document.forms[0].action = "nationwide.htm";
	document.forms[0].method = "GET";
	document.forms[0].submit();
}

function exportAdmin(){
	document.forms[0].action = "displayexport.htm";
	document.forms[0].method = "GET";
	document.forms[0].submit();
}

// move selected option form source to destination list.
function move_list_items(sourceid, destinationid, defaultId) {
	var stateCode = $('#state').val();
	var city = $('#city').val();
	var postalCode = '';
	$("#" + sourceid + " option:selected").each(function() {
		postalCode += $(this).val() + ',';
	});
	if ('' == postalCode) {
		alert("Please select postal code to move right");
	} else {
		$.ajaxSetup({
			cache : false
		})
		$.ajax({
			type : "GET",
			url : "movezip.htm",
			data : {
				'state' : stateCode,
				'city' : city,
				'zip' : postalCode
			},

			success : function(response) {
				$('#active').val(true);
				$("#" + sourceid + " option:selected").each(function(){
					var val = $(this).val();
					$("#" + defaultId).append(new Option(val, val));
				});
				$("#" + sourceid + " option:selected").prependTo("#" + destinationid).removeAttr("selected");
				var srcLstCnt = $("#" + sourceid + " option").length;
				$("#chkAllZip").removeAttr("checked");
				if (srcLstCnt == 0) {
					$('input[name$="chkAllZip"]').attr('disabled', 'disabled');
				}
			},
			error : function(e) {
				alert('error occured');
			}
		});
	}
}

/** Manage Event Category functions starts * */

/** This function is used to close event category popup * */
function closeEvtIframePopup(popupId, IframeID, funcUrl) {

	try {
		document.body.removeChild(frameDiv);
		document.getElementById(popupId).style.display = "none";
		document.getElementById(popupId).style.height = "0px";
		document.getElementById(popupId).style.width = "0px";
		document.getElementById(popupId).style.marginLeft = "0px";
		document.getElementById(popupId).style.marginTop = "0px";
		document.getElementById(IframeID).removeAttribute('src');
		document.getElementById(IframeID).src = "about:blank";

	} catch (e) {
		top.$('.framehide').remove();
		top.document.getElementById(popupId).style.display = "none";
		top.document.getElementById(popupId).style.height = "0px";
		top.document.getElementById(popupId).style.width = "0px";
		top.document.getElementById(popupId).style.marginLeft = "0px";
		top.document.getElementById(popupId).style.marginTop = "0px";
		top.document.getElementById(IframeID).removeAttribute('src');
		top.document.getElementById(IframeID).src = "about:blank";
	}
}

/** Event category : Clear * */
function clearCategory() {
	document.getElementById("ctgryNm").value = "";
	$('p.dupctgry').hide();
	$('i.emptyctgry').hide();
}

/** Event category : Add category * */
function addEvtCategory() {

	var vCatName = document.getElementById("ctgryNm").value;
	$('p.alertErr').hide();
	$('p.alertInfo').css("display", "none");

	$('p.dupctgry').hide();
	$('i.emptyctgry').hide();
	vCatName = $.trim(vCatName);
	if ("" == vCatName) {
		$('i.emptyctgry').css("display", "block");

	} else {

		$.ajaxSetup({
			cache : false
		})
		$.ajax({
			type : "GET",
			url : "addeventcat.htm",
			data : {
				"catName" : vCatName
			},

			success : function(response) {

				if (response != null && response != ""
						&& response != "CategoryExists") {

					alert("Event Category is Created Successfully.");
					closeEvtIframePopup('ifrmPopup', 'ifrm');
					top.$("#hidbtn").click();

				} else {
					$('p.dupctgry').css("display", "block");

				}

			},
			error : function(e) {
				alert("Error occured while creating category");

			}

		});

	}

}

/** Event category : update category * */
function updateCategory() {

	$('p.alertErr').hide();
	$('p.alertInfo').css("display", "none");
	var vCatName = document.getElementById("ctgryNm").value;
	var vCatId = document.getElementById("evtcatid").value;

	vCatName = $.trim(vCatName);
	$('p.dupctgry').hide();
	$('i.emptyctgry').hide();

	if ("" == vCatName) {
		$('i.emptyctgry').css("display", "block");

	} else {
		$.ajaxSetup({
			cache : false
		})
		$.ajax({
			type : "GET",
			url : "updateeventcat.htm",
			data : {
				"catName" : vCatName,
				"cateId" : vCatId
			},

			success : function(response) {

				if (response != null && response == "Success") {
					alert("Event Category is Updated Successfully.");
					closeEvtIframePopup('ifrmPopup', 'ifrm');
					// top.$("#catsearch").click();
					top.$("#hidbtn").click();
				} else {
					$('p.dupctgry').css("display", "block");
				}
			},
			error : function(e) {
				alert("Error occured while updating category");

			}
		});

	}
}

function callHomePage(vtext) {
	document.EventCategoryForm.action = "eventcategory.htm";
	document.EventCategoryForm.method = "post";
	document.EventCategoryForm.submit();
}

/* This method is used to edit retailer details */
function rowEdit(obj) {

	$("#scrlGrd select").removeAttr("disabled", "disabled");
	var tblId = $(obj).parents('table').attr("id");
	var curIndex = $(obj).parents('tr').index();
	$(obj).parents('tr').find(".row-edit").hide().end().find(".row-save")
			.show();
	$("#" + tblId).find('tr').removeClass("curRow");
	var $curRow = $(obj).parents('tr').addClass("curRow");

	$('tr:not(".curRow")').find('input, select').attr("disabled", "disabled");
	$('#' + tblId + ' tr:not(".curRow")').find('.row-edit').css("opacity", 0.4);
	var $curItem = $curRow.find(".editCol");
	var names = [ "retailName", "address1", "postalCode", "retUrl", "latitude",
			"longitude", "phoneNo" ];
	$curItem.each(function(index, td) {
		var colVal = $.trim($(this).text());
		$(this).html(
				"<input type='text' name='" + names[index] + "' value='"
						+ colVal + "'/>");
	});

	$("input[name='retailName']").attr("maxlength", 50);
	$("input[name='address1']").attr("maxlength", 50);
	$("input[name='postalCode']").attr("maxlength", 5);
	$("input[name='retUrl']").attr("maxlength", 50);
	$("input[name='latitude']").attr("maxlength", 25);
	$("input[name='longitude']").attr("maxlength", 25);
	$("input[name='phoneNo']").attr("maxlength", 10);	
	
	
	var vState = $.trim($(".curRow").find('td.editSlct:eq(0)').text());
	var vCity = $.trim($(".curRow").find('td.editSlct:eq(1)').text());

	loadStates(vState, vCity);
	// loadCities();

}
/* This method is used to edit retailer details */
function rowEditBand(obj) {

    $("#scrlGrd select").removeAttr("disabled", "disabled");
    var tblId = $(obj).parents('table').attr("id");
    var curIndex = $(obj).parents('tr').index();
    $(obj).parents('tr').find(".row-edit-band").hide().end().find(".row-save-band")
            .show();
    $("#" + tblId).find('tr').removeClass("curRow");
    var $curRow = $(obj).parents('tr').addClass("curRow");

    $('tr:not(".curRow")').find('input, select').attr("disabled", "disabled");
    $('#' + tblId + ' tr:not(".curRow")').find('.row-edit-band').css("opacity", 0.4);
    var $curItem = $curRow.find(".editCol");
    var names = [ "retailName", "address1", "postalCode", "retUrl", "latitude",
            "longitude", "phoneNo" ];
    $curItem.each(function(index, td) {
        var colVal = $.trim($(this).text());
        $(this).html(
                "<input type='text' name='" + names[index] + "' value='"
                        + colVal + "'/>");
    });

    $("input[name='retailName']").attr("maxlength", 50);
    $("input[name='address1']").attr("maxlength", 50);
    $("input[name='postalCode']").attr("maxlength", 5);
    $("input[name='retUrl']").attr("maxlength", 50);
    $("input[name='latitude']").attr("maxlength", 25);
    $("input[name='longitude']").attr("maxlength", 25);
    $("input[name='phoneNo']").attr("maxlength", 10);   
    
    
    var vState = $.trim($(".curRow").find('td.editSlct:eq(0)').text());
    var vCity = $.trim($(".curRow").find('td.editSlct:eq(1)').text());

    loadStates(vState, vCity);
    // loadCities();

}
/* This method is used to load states */
function loadStates(vState, vCity) {
	
	$.ajaxSetup({
		cache : false
	})
	$.ajax({
		type : "GET",
		url : "getRetStates.htm",
		data : {
			'state' : vState
		},

		success : function(response) {

			$(".curRow").find('td.editSlct:eq(0)').html(response);
			loadCities(vCity);
			},
		error : function(e) {

		}
	});
}
/* This method is used to load cities */
function loadCities(vCity) {
	var vState = $.trim($(".curRow").find('select:eq(0) option:selected').val());
	
	//var vCity = $.trim($(".curRow").find('td.editSlct:eq(1)').text());
	$.ajaxSetup({
		cache : false
	})
	$.ajax({
		type : "GET",
		url : "getRetCity.htm",
		data : {
			'statecode' : vState,
			'city' : vCity
		},

		success : function(response) {

			$('.curRow').find(".editSlct:eq(1)").html(response);
		},
		error : function(e) {

		}
	});
}

/* This method is used to validate retailer details */
function validateRetailer(obj) {

	var tblId = $(obj).parents('table').attr("id");
	$(obj).parents('tr').find(".row-save").hide().end().find(".row-edit")
			.show();
	var $curRow = $(obj).parents('tr').addClass("curRow");
	var $curItem = $curRow.find(".editCol");
	var $usrNm = $curRow.find("td:eq(2) input:text").val();
	var object = {};
	var stringObj;
	var retId;

	retId = $.trim($('#cpnLst tr.curRow td:eq(1)').text());
	var isPaid = $curRow.find("input[type='button'][name='isPaid']").attr("isPaid");

	$curItem.each(function(index, td) {

		var colVal = $(this).find('input:text').val();
		var name = $(this).find('input:text').attr("name");
			var inputTagVal = colVal;

		if (name == 'retailName') {
			object.retailName = inputTagVal;

		} else if (name == 'address1') {
			object.address1 = inputTagVal;

		} else if (name == 'postalCode') {
			object.postalCode = inputTagVal;

		} else if (name == 'retUrl') {
			object.retUrl = inputTagVal

		} else if (name == 'latitude') {
			object.latitude = inputTagVal

		} else if (name == 'longitude') {
			object.longitude = inputTagVal

		} else if (name == 'phoneNo') {
			object.phoneNo = inputTagVal

		}

	});

	if (retId != null && retId != "") {
	
		object.retailId =$.trim(retId);
	}

	var vState = $(".curRow").find('select:eq(0) option:selected').val();
	object.state = vState;
	var vCity = $(".curRow").find('select:eq(1) option:selected').val();
	object.city = vCity;
	var vRetValidation = true;

	if (object != null) {

		var vRetName = $.trim(object.retailName);
		var vAddress = $.trim(object.address1);
		var vState = $.trim(object.state);
		var vCity = $.trim(object.city);
		var vPostalCode = $.trim(object.postalCode);
		var vRetUrl = $.trim(object.retUrl);
		var vLatitude = $.trim(object.latitude);
		var vLongitude = $.trim(object.longitude);
		var vPhoneNo = $.trim(object.phoneNo);
		var vRetId = $.trim(object.retailId);
		var phoneNum = validatePhoneForLoc(vPhoneNo);
		

		// Validation
		if (vRetName == '' || vRetName == 'undefined' || vRetName == 'null'
				|| vRetName == 'NULL') {
			alert("Please Enter Retailer Name");
			vRetValidation = false;
		} else if (vAddress == '' || vAddress == 'undefined'
				|| vAddress == 'null' || vAddress == 'NULL') {
			alert("Please Enter Address");
			vRetValidation = false;
		} else if (vState == '' || vState == 'undefined' || vState == 'null'
				|| vState == 'select' || vState == 'NULL') {
			alert("Please Select State");
			vRetValidation = false;
		} else if (vCity == '' || vCity == 'undefined' || vCity == 'null'
				|| vCity == 'select') {
			alert("Please Select City");
			vRetValidation = false;
		} else if (vPostalCode == '' || vPostalCode == 'undefined'
				|| vPostalCode == 'null' || vPostalCode == 'NULL') {
			alert("Please Enter PostalCode");
			vRetValidation = false;
		} else if (vLatitude == '' || vLatitude == 'undefined'
				|| vLatitude == 'null' || vLatitude == 'NULL') {
			alert("Please Enter Latitude");
			vRetValidation = false;
		} else if (vLongitude == '' || vLongitude == 'undefined'
				|| vLongitude == 'null' || vLongitude == 'NULL') {
			alert("Please Enter Longitude");
			vRetValidation = false;
		} else if (vPhoneNo == '' || vPhoneNo == 'undefined'
				|| vPhoneNo == 'null' || vPhoneNo == 'NULL') {
			alert("Please Enter Phone No");
			vRetValidation = false;
		} else if(!phoneNum){
			alert("Please Enter 10 Digit Phone No");
			vRetValidation = false;
		} else if (vRetUrl != '' && vRetUrl != 'undefined' && vRetUrl != 'null') {
			var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

			if (!regexp.test(vRetUrl)) {
				alert("Please Enter Valid Website URL");
				vRetValidation = false;
			}
		} else if (vLatitude != '' || vLatitude != 'undefined'
				|| vLatitude != 'null' || vLatitude != 'NULL') {

			var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;

			if (0 === vLongitude.length || !vLongitude || vLongitude == "") {
				return false;
			} else {
				if (!vLatLngVal.test(vLatitude)) {
					alert("Latitude are not correctly typed");
					vRetValidation = false;
				}
			}

		} else if (vLongitude != '' || vLongitude != 'undefined'
				|| vLongitude != 'null' || vLongitude != 'NULL') {

			var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;

			if (0 === vLongitude.length || !vLongitude || vLongitude == "") {
				return false;
			} else {
				if (!vLatLngVal.test(vLongitude)) {
					alert("Longitude are not correctly typed");
					vRetValidation = false;
				}
			}
		}

		if (vRetValidation) {

			$('tr:not(".curRow")').find('input, select').removeAttr("disabled");
			$('#' + tblId + ' tr').find('.row-edit').removeAttr("style");
			$('#' + tblId + ' tr.curRow').removeClass("curRow");
			$('tr.curRow').removeClass("curRow");

			var stringObj;
			var vRetValidation = true;
			stringObj = "{\"retailId\":\"" +vRetId + "\","
					+ "\"retailName\":\"" + vRetName + "\","
					+ "\"address1\":\"" + vAddress + "\"," + "\"state\":\""
					+ vState + "\"," + "\"city\":\"" + vCity + "\","
					+ "\"postalCode\":\"" + vPostalCode + "\","
					+ "\"retUrl\":\"" + vRetUrl + "\"," + "\"latitude\":\""
					+ vLatitude + "\"," + "\"longitude\":\"" + vLongitude
					+ "\"," + "\"phoneNo\":\"" + vPhoneNo 
					+ "\"," + "\"isPaid\":\"" + isPaid

					+ "\"}";

			SaveRetailerDetails(stringObj.toString());

		} else {

			/*
			 * $('tr:not(".curRow")').find('input,
			 * select').addAttribute("disabled"); $('#'+tblId+'
			 * tr').find('.row-edit').addAttribute("style"); $('#'+tblId+'
			 * tr.curRow').addAttribute("curRow");
			 * $('tr.curRow').addAttribute("curRow");
			 */
			$(obj).parents('tr').find(".row-save").show().end().find(
					".row-edit").hide();
			return false;
		}

	}
}
/* This method is used to validate retailer details */
function validateBand(obj) {

    var tblId = $(obj).parents('table').attr("id");
    $(obj).parents('tr').find(".row-save-band").hide().end().find(".row-edit-band")
            .show();
    var $curRow = $(obj).parents('tr').addClass("curRow");
    var $curItem = $curRow.find(".editCol");
    var $usrNm = $curRow.find("td:eq(2) input:text").val();
    var object = {};
    var stringObj;
    var retId;

    retId = $.trim($('#cpnLst tr.curRow td:eq(1)').text());
    var isPaid = $curRow.find("input[type='button'][name='isPaid']").attr("isPaid");

    $curItem.each(function(index, td) {

        var colVal = $(this).find('input:text').val();
        var name = $(this).find('input:text').attr("name");
            var inputTagVal = colVal;

        if (name == 'retailName') {
		
        	inputTagVal=inputTagVal.replace(/"/g,'&quot;');
			 object.retailName = inputTagVal;
          } else if (name == 'address1') {
            object.address1 = inputTagVal;

        } else if (name == 'postalCode') {
            object.postalCode = inputTagVal;

        } else if (name == 'retUrl') {
            object.retUrl = inputTagVal

        } else if (name == 'latitude') {
            object.latitude = inputTagVal

        } else if (name == 'longitude') {
            object.longitude = inputTagVal

        } else if (name == 'phoneNo') {
            object.phoneNo = inputTagVal

        }

    });

    if (retId != null && retId != "") {
    
        object.retailId =$.trim(retId);
    }

    var vState = $(".curRow").find('select:eq(0) option:selected').val();
    object.state = vState;
    var vCity = $(".curRow").find('select:eq(1) option:selected').val();
    object.city = vCity;
    var vRetValidation = true;

    if (object != null) {

        var vRetName = $.trim(object.retailName);
        var vAddress = $.trim(object.address1);
        var vState = $.trim(object.state);
        var vCity = $.trim(object.city);
        var vPostalCode = $.trim(object.postalCode);
        var vRetUrl = $.trim(object.retUrl);
        var vLatitude = $.trim(object.latitude);
        var vLongitude = $.trim(object.longitude);
        var vPhoneNo = $.trim(object.phoneNo);
        var vRetId = $.trim(object.retailId);
        var phoneNum = validatePhoneForLoc(vPhoneNo);
        

        // Validation
        if (vRetName == '' || vRetName == 'undefined' || vRetName == 'null'
                || vRetName == 'NULL') {
            alert("Please Enter Retailer Name");
            vRetValidation = false;
        }else if (vRetUrl != '' && vRetUrl != 'undefined' && vRetUrl != 'null') {
            var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

            if (!regexp.test(vRetUrl)) {
                alert("Please Enter Valid Website URL");
                vRetValidation = false;
            }
        } else if (vLatitude != '' && vLatitude != 'undefined'
                && vLatitude != 'null' && vLatitude != 'NULL') {

            var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;

            if (0 === vLongitude.length || !vLongitude || vLongitude == "") {
                return false;
            } else {
                if (!vLatLngVal.test(vLatitude)) {
                    alert("Latitude are not correctly typed");
                    vRetValidation = false;
                }
            }

        } else if (vLongitude != '' && vLongitude != 'undefined'
                && vLongitude != 'null' && vLongitude != 'NULL') {

            var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;

            if (0 === vLongitude.length || !vLongitude || vLongitude == "") {
                return false;
            } else {
                if (!vLatLngVal.test(vLongitude)) {
                    alert("Longitude are not correctly typed");
                    vRetValidation = false;
                }
            }
        }

        if (vRetValidation) {

            $('tr:not(".curRow")').find('input, select').removeAttr("disabled");
            $('#' + tblId + ' tr').find('.row-edit-band').removeAttr("style");
            $('#' + tblId + ' tr.curRow').removeClass("curRow");
            $('tr.curRow').removeClass("curRow");

            var stringObj;
            var vRetValidation = true;
            stringObj = "{\"retailId\":\"" +vRetId + "\","
                    + "\"retailName\":\"" + vRetName + "\","
                    + "\"address1\":\"" + vAddress + "\"," + "\"state\":\""
                    + vState + "\"," + "\"city\":\"" + vCity + "\","
                    + "\"postalCode\":\"" + vPostalCode + "\","
                    + "\"retUrl\":\"" + vRetUrl + "\"," + "\"latitude\":\""
                    + vLatitude + "\"," + "\"longitude\":\"" + vLongitude
                    + "\"," + "\"phoneNo\":\"" + vPhoneNo 
                    + "\"," + "\"isPaid\":\"" + isPaid

                    + "\"}";

            SaveRetailerDetails(stringObj.toString());

        } else {

            /*
             * $('tr:not(".curRow")').find('input,
             * select').addAttribute("disabled"); $('#'+tblId+'
             * tr').find('.row-edit').addAttribute("style"); $('#'+tblId+'
             * tr.curRow').addAttribute("curRow");
             * $('tr.curRow').addAttribute("curRow");
             */
            $(obj).parents('tr').find(".row-save-band").show().end().find(
                    ".row-edit-band").hide();
            return false;
        }

    }
}
/* This method is used to save retailer detaisl */
function SaveRetailerDetails(retInfo) {

	

	if (null != retInfo) {

		document.retailer.retailInfoJson.value = retInfo;

	} else {

		document.retailer.retailInfoJson.value = "";
	}

	document.retailer.action = "saveRetailerInfo.htm";
	document.retailer.method = "POST";
	document.retailer.submit();
}
// ***************** Manage Event Category functions Ends **************

function validatePhoneForLoc(num) {
	if (num.length != 10) {
		return false;
	} else {
		return true;
	}
}

function saveRetailerLocs(pagenumber) {
	var productJson = [];
	var productObj = {};
	var validationFlag = true;
	var validurlFlag = true;				
	var objArr = [];
	var retailerID = document.retailer.retailId.value;
	$("#cpnLst").find("tr").each(function(index) {
		if (index > 0) {
			var highlightObj = $($("#cpnLst").find("tr")[index]).children();
			var object = {};
			var stringObj;
			object.retailId = retailerID;	
			highlightObj.find("input").each(function(rVal) {
				
				var name = $(this).attr("name");
				var inputTagVal = $(this)[0].value;				

				if (name == 'checkboxDel') {
					object.retailLocationId = inputTagVal;

				} else if (name == 'storeIdentification') {
					if(inputTagVal == "N/A")
					{
						object.storeIdentification = "";
					}
					else
					{
						object.storeIdentification = inputTagVal;
					}

				} else if (name == 'address') {
					object.address1 = inputTagVal;

				} else if (name == 'latitude') {
					object.latitude = inputTagVal;

				} else if (name == 'longitude') {
					object.longitude = inputTagVal;

				} else if (name == 'postalCode') {
					object.postalCode = inputTagVal

				} else if (name == 'phone') {
					object.phone = inputTagVal	
				} else if (name == 'websiteURL') {
					object.websiteURL = inputTagVal

				}
				else if (name == 'gridImgLocationPath') {
					object.gridImgLocationPath = inputTagVal
				
			} else if (name == 'imgLocationPath') {
				object.imgLocationPath = inputTagVal
				
			} else if (name == 'uploadImage') {
				object.uploadImage = inputTagVal
			}
				
				
				
				
			});
			
			highlightObj.find('select').each(function(rVal) {
			
				var name = $(this).attr("name");
				var inputTagVal = $(this).find('option:selected').val();
				
				if (name == 'city') {
					object.city = inputTagVal

				} else if (name == 'state') {
					object.state = inputTagVal
				}
				
			});
			
			objArr[index - 1] = object;
		}
	});

	if (objArr.length > 0) {
		for ( var k = 0; k < objArr.length; k++) {
			var object = objArr[k];
			var vRetailerLocID = object.retailLocationId;
			var storeIdentification = object.storeIdentification;
			var city = object.city;
			var state = object.state;
			var postalCode = object.postalCode;
			var address1 = object.address1;
			var phonenumber = object.phone;
			var retailLocationUrl = object.websiteURL;
			var vLatitude = object.latitude;
			var vLongitude = object.longitude;
			var phoneNum = validatePhoneForLoc(phonenumber);
			
			if (city == '' || typeof(city) == 'undefined' || city == 'null' || city == 0) {		
				$("tr#" + vRetailerLocID).addClass("requiredVal");					
				validationFlag = false;
			} else if (state == '' || typeof(state) == 'undefined'
					|| state == 'null' || state == 0) {
				$("tr#" + vRetailerLocID).addClass("requiredVal");	
				validationFlag = false;
			} else if (postalCode == '' || postalCode == 'undefined'
					|| postalCode == 'null') {
				$("tr#" + vRetailerLocID).addClass("requiredVal");	
				validationFlag = false;
			} else if (address1 == '' || address1 == 'undefined'
					|| address1 == 'null') {
				$("tr#" + vRetailerLocID).addClass("requiredVal");	
				validationFlag = false;
			}  else if (vLatitude == '' || vLatitude == 'undefined'
					|| vLatitude == 'null') {
				$("tr#" + vRetailerLocID).addClass("requiredVal");	
				validationFlag = false;
			} else if (vLongitude == '' || vLongitude == 'undefined'
					|| vLongitude == 'null') {
				$("tr#" + vRetailerLocID).addClass("requiredVal");	
				validationFlag = false;
			} else if (phonenumber == '' || phonenumber == 'undefined'
				|| phonenumber == 'null' || !phoneNum){
				$("tr#" + vRetailerLocID).addClass("requiredVal");
					validationFlag = false;
			} else {
				$("tr#" + vRetailerLocID).removeClass("requiredVal");
			}
			
			

			var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (retailLocationUrl != '') {
				if (!regexp.test(retailLocationUrl)) {	
					$("tr#" + vRetailerLocID).addClass("requiredVal");
					validurlFlag = false;					
				}
			}
		}

		if (validationFlag) {
			if (!validurlFlag) {
				alert("Please enter a valid URL for the highlighted rows in the screen.");
				return false;
			}

			var strinObjArr = []
			for ( var k = 0; k < objArr.length; k++) {
				var object = objArr[k];

				var stringObj;

				stringObj = "{\"retailId\":\""
						+ object.retailId + "\","
						+ "\"retailLocationId\":\""
						+ object.retailLocationId + "\","
						+ "\"storeIdentification\":\""
						+ object.storeIdentification + "\","						
						+ "\"address1\":\""
						+ object.address1 + "\","
						+ "\"city\":\"" + object.city + "\","
						+ "\"state\":\"" + object.state + "\","
						+ "\"postalCode\":\"" + object.postalCode + "\","
						+ "\"phone\":\"" + object.phone + "\","
						+ "\"websiteURL\":\""
						+ object.websiteURL + "\","
						+ "\"latitude\":\""
						+ object.latitude + "\","
						+ "\"longitude\":\""
						+ object.longitude 	+"\","					
						+ "\"imgLocationPath\":\""
						+ object.imgLocationPath + "\","
						+ "\"gridImgLocationPath\":\""
						+ object.gridImgLocationPath + "\","
						+ "\"uploadImage\":\""
						+ object.uploadImage + "\"}";
				
				strinObjArr[k] = stringObj;
			}
			saveLocationList(strinObjArr.toString(), pagenumber)
		} else {
			alert("Please Enter values for the mandatory fields for the highlighted rows in the screen");
			return false;
		}
	} else {
		alert("Please Enter values for the mandatory fields and Click on Save to Save the Changes")
	}
}

function saveLocationList(objArr, pagenumber) {
	if (pagenumber == null || typeof(pagenumber) == undefined || pagenumber == "") {
		document.retailer.pageFlag.value = "false";
	} else {
		document.retailer.pageNumber.value = pagenumber;
		document.retailer.pageFlag.value = "true";
	}				
	document.retailer.locationJson.value = "{\"locationData\":[" + objArr + "]}";
	document.retailer.action = "savelocationlist.htm";
	document.retailer.method = "POST";
	document.retailer.submit();	
}

function callNextPage(pagenumber, url) {
	if (saveRetalLocation) {
		saveRetailerLocs(pagenumber);
	} else {
		document.forms[0].pageNumber.value = pagenumber;
		document.forms[0].pageFlag.value = "true";
		document.forms[0].action = url;
		document.forms[0].method = "post";
		document.forms[0].submit();
	}
}

function backspacerUP(object, e) {
	if (e) {
		e = e
	} else {
		e = window.event
	}
	if (e.which) {
		var keycode = e.which
	} else {
		var keycode = e.keyCode
	}

	ParseForNumber1(object)

	if (keycode >= 48) {
		ValidatePhone(object)
	}
}

function backspacerDOWN(object, e) {
	if (e) {
		e = e
	} else {
		e = window.event
	}
	if (e.which) {
		var keycode = e.which
	} else {
		var keycode = e.keyCode
	}
	ParseForNumber2(object)
}

function ValidatePhone(object) {

	var p = phonevalue1

	p = p.replace(/[^\d]*/gi, "")

	if (p.length < 3) {
		object.value = p
	} else if (p.length == 3) {
		pp = p;
		d4 = p.indexOf('(')
		d5 = p.indexOf(')')
		if (d4 == -1) {
			pp = "(" + pp;
		}
		if (d5 == -1) {
			pp = pp + ")";
		}
		object.value = pp;
	} else if (p.length > 3 && p.length < 7) {
		p = "(" + p;
		l30 = p.length;
		p30 = p.substring(0, 4);
		p30 = p30 + ")"

		p31 = p.substring(4, l30);
		pp = p30 + p31;

		object.value = pp;

	} else if (p.length >= 7) {
		p = "(" + p;
		l30 = p.length;
		p30 = p.substring(0, 4);
		p30 = p30 + ")"

		p31 = p.substring(4, l30);
		pp = p30 + p31;

		l40 = pp.length;
		p40 = pp.substring(0, 8);
		p40 = p40 + "-"

		p41 = pp.substring(8, l40);
		ppp = p40 + p41;

		object.value = ppp.substring(0, maxphonelength);
	}

	GetCursorPosition()

	if (cursorposition >= 0) {
		if (cursorposition == 0) {
			cursorposition = 2
		} else if (cursorposition <= 2) {
			cursorposition = cursorposition + 1
		} else if (cursorposition <= 5) {
			cursorposition = cursorposition + 2
		} else if (cursorposition == 6) {
			cursorposition = cursorposition + 2
		} else if (cursorposition == 7) {
			cursorposition = cursorposition + 4
			e1 = object.value.indexOf(')')
			e2 = object.value.indexOf('-')
			if (e1 > -1 && e2 > -1) {
				if (e2 - e1 == 4) {
					cursorposition = cursorposition - 1
				}
			}
		} else if (cursorposition < 11) {
			cursorposition = cursorposition + 3
		} else if (cursorposition == 11) {
			cursorposition = cursorposition + 1
		} else if (cursorposition >= 12) {
			cursorposition = cursorposition
		}

		var txtRange = object.createTextRange();
		txtRange.moveStart("character", cursorposition);
		txtRange.moveEnd("character", cursorposition - object.value.length);
		txtRange.select();
	}

}
function GetCursorPosition() {

	var t1 = phonevalue1;
	var t2 = phonevalue2;
	var bool = false
	for (i = 0; i < t1.length; i++) {
		if (t1.substring(i, 1) != t2.substring(i, 1)) {
			if (!bool) {
				cursorposition = i
				bool = true
			}
		}
	}
}
function ParseForNumber1(object) {
	phonevalue1 = ParseChar(object.value, zChar);
}
function ParseForNumber2(object) {
	phonevalue2 = ParseChar(object.value, zChar);
}
function ParseChar(sStr, sChar) {
	if (sChar.length == null) {
		zChar = new Array(sChar);
	} else
		zChar = sChar;

	for (i = 0; i < zChar.length; i++) {
		sNewStr = "";

		var iStart = 0;
		var iEnd = sStr.indexOf(sChar[i]);

		while (iEnd != -1) {
			sNewStr += sStr.substring(iStart, iEnd);
			iStart = iEnd + 1;
			iEnd = sStr.indexOf(sChar[i], iStart);
		}
		sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

		sStr = sNewStr;
	}

	return sNewStr;
}
function openIframePopupForImage(popupId, IframeID, url, height, width, name, btnBool) {
	
	frameDiv = document.createElement('div');
	frameDiv.className = 'framehide';
	document.body.appendChild(frameDiv);
	document.getElementById(IframeID).setAttribute('src', url);
	// frameDiv.setAttribute('onclick', closePopup(obj,popupId));
	document.getElementById(popupId).style.display = "block"
	height = (height == "100%") ? frameDiv.offsetHeight - 20 : height;
	width = (width == "100%") ? frameDiv.offsetWidth - 16 : width;
	document.getElementById(popupId).style.height = height + "%"
	document.getElementById(popupId).style.width = width + "%"
// var marLeft = -1 * parseInt(width / 2);
// var marTop = -1 * parseInt(height / 2);
	document.getElementById('popupHeader').innerHTML = name;
	// document.getElementById(popupId).style.marginLeft = marLeft + "px"
	// document.getElementById(popupId).style.marginTop = marTop + "px"
	// var iframeHt = height - 27;
	var setHt = getDocHeight();
	
    var iframeHt = setHt - 27;
	document.getElementById(IframeID).height = iframeHt + "px";
	if (btnBool) {
		var btnHt = height - 50;
		document.getElementById(IframeID).style.height = btnHt + "px";
	}

}
function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}

function uploadAddCategoryCroppedImage(x, y, w, h) {
	
	
	top.document.EventCategoryForm.action = "/ScanSeeAdmin/uploadaddcroppedimage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h ;// +"&imgValue="+img;
	top.document.EventCategoryForm.method = "POST";
	top.document.EventCategoryForm.submit();
}
function uploadEditCategoryCroppedImage(x, y, w, h) {	
	
	top.document.EventCategoryForm.action = "/ScanSeeAdmin/uploadeditcroppedimage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h ;// +"&imgValue="+img;
	top.document.EventCategoryForm.method = "POST";
	top.document.EventCategoryForm.submit();
}

/*grid row upload image*/
function fileUploadGrid(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			var curRowImg = $(input).parents('tr').find('img.img-preview');
			$(curRowImg).attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	}
}
function uploadRetailLocCroppedImage(x, y, w, h) {
	
	
	top.document.retailer.action = "/ScanSeeAdmin/uploadretloccroppedimage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h ;// +"&imgValue="+img;
	top.document.retailer.method = "POST";
	top.document.retailer.submit();
}
// this method is used to upload logo for each retailer location.

function uploadRetailLocationLogo(input,rowIndex)
{
	var vRetLocImg =  $(input).parents('tr').find("input[name='imageFile']").val();
	$("#rowIndex").val(rowIndex);
	if(vRetLocImg != '')
		{
		var vRetLocationID = $(input).parents('tr').find(
		"input[name='retailLocationID']").val();
		var vChkRetLocImg = vRetLocImg.toLowerCase();
		if (!vChkRetLocImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
		
			alert("You must upload location image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
			$(input).parents('tr').find("input[name='imageFile']").val("");
			$(input).parents('tr').find("input[name='imageFile']").focus();
			return false;
		}else{
			createJsonRetLocs(vRetLocationID);
			$.ajaxSetup({
				cache : false
			});
			
			$("#retailer").ajaxForm({
			
				success: function(response){
					$("#loading-image").css("visibility","hidden");
					var imgResponse = response.getElementsByTagName('imageScr')[0].firstChild.nodeValue;
				
					if(imgResponse == 'maxSizeImageError')
						{
						alert("Image dimension should not exceed Width:800px and Heigth:600px");
						$(input).parents('tr').find("input[name='imageFile'] ").val("");
						$(input).parents('tr').find("input[name='imageFile'] ").focus();
									
						}else if(imgResponse == 'UploadLogoMinSize')
							{
							alert("Image dimension should be Minimum Width: 70px Height: 70px");
							$(input).parents('tr').find("input[name='imageFile'] ").val("");
							$(input).parents('tr').find("input[name='imageFile'] ").focus();
							
							}else{
									var imgSplit = imgResponse.split("|");
								
								if(imgSplit[0] == 'ValidImageDimention')
									{
								
									$('.img-preview').width('28');
									$('.img-preview').height('28');
									var imgName =  imgSplit[1];
									$(this).parents('tr').find('img.img-preview').attr("src",imgSplit[2]);
									$(input).parents('tr').find("input[name='gridImgLocationPath']").val(imgName);
									
									}else{
										openIframePopupForImage(
												'ifrmPopup',
												'ifrm',
												'/ScanSeeAdmin/cropImageGeneral.htm',
												100, 99.5,
												'Crop Image');
										
									}
								
								
							}
					
					
					
				}
			
			
			}).submit();
					
		}
		
		}
	
	

}
function createJsonRetLocs(vRetLocationID) {
	
	var productJson = [];
	var productObj = {};
	var validationFlag = true;
	var validurlFlag = true;				
	var objArr = [];
	var retailerID = document.retailer.retailId.value;
	$("#cpnLst").find("tr").each(function(index) {
		if (index > 0) {
			var highlightObj = $($("#cpnLst").find("tr")[index]).children();
			var object = {};
			var stringObj;
			object.retailId = retailerID;	
			highlightObj.find("input").each(function(rVal) {
				
				var name = $(this).attr("name");
				var inputTagVal = $(this)[0].value;				

				if (name == 'checkboxDel') {
					object.retailLocationId = inputTagVal;

				} else if (name == 'storeIdentification') {
					if(inputTagVal == "N/A")
					{
						object.storeIdentification = "";
					}
					else
					{
						object.storeIdentification = inputTagVal;
					}

				} else if (name == 'address') {
					object.address1 = inputTagVal;

				} else if (name == 'latitude') {
					object.latitude = inputTagVal;

				} else if (name == 'longitude') {
					object.longitude = inputTagVal;

				} else if (name == 'postalCode') {
					object.postalCode = inputTagVal

				} else if (name == 'phone') {
					object.phone = inputTagVal	
				} else if (name == 'websiteURL') {
					object.websiteURL = inputTagVal

				}else if (name == 'gridImgLocationPath') {
						object.gridImgLocationPath = inputTagVal
					
				} else if (name == 'imgLocationPath') {
					object.imgLocationPath = inputTagVal
					
				} else if (name == 'uploadImage') {
					object.uploadImage = inputTagVal
				}
			});
			
			highlightObj.find('select').each(function(rVal) {
			
				var name = $(this).attr("name");
				var inputTagVal = $(this).find('option:selected').val();
				
				if (name == 'city') {
					object.city = inputTagVal

				} else if (name == 'state') {
					object.state = inputTagVal
				}
				
			});
			
			objArr[index - 1] = object;
		}
	});


	if (objArr.length > 0) {
			var strinObjArr = [];
			var strinObjArr1
			for ( var k = 0; k < objArr.length; k++) {
				var object = objArr[k];

				var stringObj;

				stringObj = "{\"retailId\":\""
						+ object.retailId + "\","
						+ "\"retailLocationId\":\""
						+ object.retailLocationId + "\","
						+ "\"storeIdentification\":\""
						+ object.storeIdentification + "\","						
						+ "\"address1\":\""
						+ object.address1 + "\","
						+ "\"city\":\"" + object.city + "\","
						+ "\"state\":\"" + object.state + "\","
						+ "\"postalCode\":\"" + object.postalCode + "\","
						+ "\"phone\":\"" + object.phone + "\","
						+ "\"websiteURL\":\""
						+ object.websiteURL + "\","
						+ "\"latitude\":\""
						+ object.latitude + "\","
						+ "\"longitude\":\""
						+ object.longitude 	+"\","		
						+ "\"imgLocationPath\":\""
						+ object.imgLocationPath + "\","
						+ "\"gridImgLocationPath\":\""
						+ object.gridImgLocationPath + "\","
						+ "\"uploadImage\":\""
						+ object.uploadImage  				
						+ "\"}";
				if (object.retailLocationID === vRetLocationID) {
					strinObjArr1 = stringObj;
				}
				strinObjArr[k] = stringObj;
			}
			document.retailer.locationJson.value ="{\"locationData\":[" + strinObjArr.toString()+ "]}";
		}
}

function uploadAddFundraiserCategoryCroppedImage(x, y, w, h) {
	top.document.FundraiserCategoryForm.action = "/ScanSeeAdmin/uploadaddfundraisercroppedimage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;
	top.document.FundraiserCategoryForm.method = "POST";
	top.document.FundraiserCategoryForm.submit();
}



function filterAdmin() {
	document.forms[0].searchKey.value = "";
	document.forms[0].action = "managefilters.htm";
	document.forms[0].method = "POST";
	document.forms[0].submit();
}



/* Add options functionality for filter value. */
function addOption() {
	
	var vFltrVal = $("#fltrTxtValue").val();

	if(vFltrVal.length) {
		$.ajaxSetup({
			cache : false
		})


		$.ajax({
			type : "GET",
			url : "addfvalue.htm",
			
			data : {
				"filterValue" : vFltrVal,
			},

			success : function(response) {
				if (response != null  &&  response != "" && response != "FilterValueExists" && response != "Failure") {
				   $('#slctFilter').find('option').removeAttr("selected");
					$("#slctFilter").append('<option value=' + response + ' selected="selected">' + vFltrVal+ '</option>');
					$('#fValueMsg').text("Filter Option Value Added Successfully.");
					$('#fValueMsg').removeClass("error").addClass("success");
					$("#fltrTxtValue").val("");
					$(".fltrValueRow").hide();
				} else if (response != null  &&  response != "" && response == "FilterValueExists") {
					$('#slctFilter').find('option').removeAttr("selected");
					$('#fValueMsg').text("Filter Option Value already Exists.");
					$('#fValueMsg').removeClass("success").addClass("error");
				} else {
					$('#slctFilter').find('option').removeAttr("selected");
				    $('#fValueMsg').text("Error occured while creating Filter Value");
					$('#fValueMsg').removeClass("success").addClass("error");
				}
			},
			
			error : function(e) {
				alert("Error occured while creating Filter Value");
			}
		});

	} else {
		alert("Please Enter Filter Value");
	} 
}
	
	
/* Edit options functionality for filter value. */
function editOption() {
	
    var vEditFValueId = $("#slctFilter option:selected").attr("value");
	var vFltrVal = $("#fltrTxtValue").val();
	
	if(vFltrVal.length) {
		$.ajaxSetup({
			cache : false
		})
		
		
		$.ajax({
			type : "GET",
			url : "updatefvalue.htm",
			
			data : {
				"filterValue" : vFltrVal,
				"fValueId" : vEditFValueId,
			},

			success : function(response) {
				if (response != null && response != "" && response != "FilterValueExists" && response != "Failure") {
					$('#fValueMsg').text("Filter Option Value Updated Successfully.");
					$('#fValueMsg').removeClass("error").addClass("success");
					$("#fltrTxtValue").val("");
					$(".fltrValueRow").hide();
				} else if (response != null  &&  response != "" && response == "FilterValueExists") {
					$('#slctFilter').find('option').removeAttr("selected");
					$('#fValueMsg').text("Filter Option Value already Exists.");
					$('#fValueMsg').removeClass("success").addClass("error");
				} else {
					$('#slctFilter').find('option').removeAttr("selected");
					$('#fValueMsg').text("Error occured while creating Filter Value");
					$('#fValueMsg').removeClass("success").addClass("error");
				}
			},
			
			error : function(e) {
				alert("Error occured while creating Filter Value");
			}
			
		});
		
	} else {
		alert("Please Enter Filter Value");
	} 
}
	

/* Delete options functionality for filter value. */
function deleteOption(){
	
	var vFValueId = $("#slctFilter option:selected").attr("value");
	var vLength = $("#slctFilter").find('option:selected').length

		$.ajaxSetup({
			cache : false
		})
		
		$.ajax({
			type : "GET",
			url : "deletefvalue.htm",
			
			data : {
				"fValueId" : vFValueId,
			},

			success : function(response) {
				if (response != null && response != "" && response != "Failure") {
					$('#fValueMsg').text("Filter Value Deleted Successfully.");
					$('#fValueMsg').removeClass("error").addClass("success");
				} else {
					$('#slctFilter').find('option').removeAttr("selected");
					$('#fValueMsg').text("Error occured while creating Filter Value");
					$('#fValueMsg').removeClass("success").addClass("error");
				}
			},
			
			error : function(e) {
				alert("Error occured while deleting Filter Value");
			}
			
		});
}
	

function controllerActn(actn){
	var getActn = actn;
	$(".msg").text("");
	switch (getActn) {
	case "addOption":
		$("#fltrTxtValue").val("");
		$(".fltrValueRow").show();
		$("#editNewOptn").hide();
		$("#addNewOptn").show();
		break;
	case "editOption":
			$("#fltrTxtValue").val("");
			$(".fltrValueRow").hide();
			
		$("#addNewOptn").hide();
		$("#editNewOptn").show();
		if($("#slctFilter").find('option:selected').length === 0){
			$(".fltrValueRow").hide();
			alert("Please Select one Filter Value in the list to Edit.");
			return false;
		}  else
			if($("#slctFilter").find('option:selected').length !== 1){
				alert("Please Select one Filter Value in the list to Edit.");
                return false;
			} else
			if($("#slctFilter").find('option').length){
				var slctele = $("#slctFilter").find('option:selected').text();
				$("#fltrTxtValue").val(slctele);
				$(".fltrValueRow").show();
			}
		break;
	case "delOption":
			$("#fltrTxtValue").val("");
			$(".fltrValueRow").hide();
		if($("#slctFilter").find('option:selected').length === 1){
			$("#slctFilter").find('option:selected').remove();
		} else {
			alert("Please Select one Filter Value in the list to delete.");
		}
		break;
	case "cnclOption":
		$("#fltrTxtValue").val("");
		$(".fltrValueRow").hide();
		break;
	}

}


/* generate unique ID */
var dynId = 0;
function genDynId(prefix){
	var id = ++dynId + '';
	return prefix ? prefix + id : id;
}
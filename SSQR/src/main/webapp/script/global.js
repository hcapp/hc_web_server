
var cont = 1;
var actIndex;
var extrnTbl = $("#cpnGallery tr:first");
$(document).ready(function(){	
	
	/* 
	 * page title changes 
	 * 
	*/
	
	var titleName =  $("#titleName").val();
	if(null != titleName && "" != titleName) {
		document.title = titleName;
	}	
	
	$('.trgrUpld').click(function() {
		$('#trgrUpld').click();		  
	});
	/*Prevent bottom border for last li*/
	$("#VertNav ul li:last-child").css("background-image","none");
	//Retailer Special Offer display rows based on selected option
	$('tr.AttachLink').hide();	
	$('#slctSpclOffr,#lndgPgSlct').change(function() {										 
		$this = $(this);
		var curVal = $this.find('option:selected').val();
		var curTxt = $this.find('option:selected').text();
		var curTbl = $(this).parents('table').attr('id');
		if(curVal == "AttachLink") {
			$('#'+curTbl+' tr:gt(3)').hide(); 
			$('.AttachLink,.cmnActn').show()
			$('#'+curTbl+' td.header').text(curTxt+" Page:");
			$this.parents('table').find('input[value="Preview"]').hide();
		}
		else {
			$('#'+curTbl+' tr.AttachLink').hide();	
			$('#'+curTbl+' tr:gt(3)').show(); 
			$('#'+curTbl+' td.header').text(curTxt+" Page:");
			$this.parents('table').find('input[value="Preview"]').show();
		}
	});
	
	$('#slctSpclOffr').val("Special Offer").trigger('change');
	$('#lndgPgSlct').val("Landing").trigger('change');
	//To avoid border for last td in each row
	$("table tr td:not(:last-child)").css({ "border-right":"1px solid #dadada" });
	$(".searchGrd table tr td:not(:last-child)").css({ "border-right":"1px solid #c1e4f0" });
	//$('#nav li').last().css('background', 'none');
		if($('td.rowone div').length >= 2) {
			$('.imgarea,.txtarea').css('width','40%');
		}
		else {
			$('.imgarea,.txtarea').css('width','100%');
			$('.txtarea').css('margin-top','0px')
		}
		
		$('.trgrClick tr:eq(1) td').click(function(event) {
		if (event.target.type !== 'radio') {
			$(':radio', this).trigger('click');
		}	
		$('.trgrClick').find(".active").removeClass("active");
		$(this).addClass("active");
		});
		$('.trgrClick tr:eq(1) td').click(function() {
			var gtTblCls = $(this).find('input[type="radio"]').attr('value');
			$('.cstmpg').hide();
			$('#'+gtTblCls).show();			
		});
		$('.addBG :radio:checked').parent('td').trigger('click');
		
		$('.frgtLnk').click(function() { 
		$(this).parents('#loginSec').slideUp();
		$('.forgotPswd').show();
		});
		$('#send').click(function() {
		var self = $(this);		
		$('.mainLogin').slideDown();
		self.parents('.forgotPswd').fadeOut('slow'); 
		});
	    $('#storeNum').hide();
		$('input[name="isLoc"]').click(function() {
		var curChkBx = $(this); 
		if ($(curChkBx).attr('checked')) {
			$('#storeNum').show();		    
		}
		else {
			$('#storeNum').hide();
		}
		});
	$('#srchMsg').click(function() {
+				$('.infoSlide').html('').append("<div class='overlaySec'>"+"<div class='overlayMsg'>"+"Din't find what you were looking for? <b><a href='#' id='glink'>Google</a></b>"+" <img src='images/searchIcon.png' style='vertical-align:middle;'/>"+"</div>"+"</div>");	
$('#glink').click(function() {
						   $('.overlaySec').remove();
						   //$('#srchMsg').show();
						 	$('.infoSlide').append("<input type='submit' class='mobBtn' value='Search' onclick='navToPage();' id='srchMsg' />");
							$('img.chngImg').attr('src','images/googleIcon.png');
							$('form').attr('action','http://www.google.com/search');
							$('input.textboxSmall').attr('name','q');
						   	});
								 });
	
	//To toggle display of credit card Form & Bank information form.
	$('.achBank').show();
	$('input[name$="paymenttype"]').change(function() {
		var curVal = $(this).attr('value');
		$('.cstmTbl').hide();
		$("."+curVal).show();
	  });
	//In upload Location & Manage Location: Check & Uncheck checkboxes 
	$('input[name$="deleteAll"]').click(function() {
		var status = $(this).attr("checked");
		$('input[name$="checkboxDel"]').attr('checked',status);
		if(!status) {
		$('input[name$="checkboxDel"]').removeAttr('checked');
		}	
	});
	/*In upload Location & Manage Location: if all child checkbox is unchecked then uncheck parent check box.
	If all child checkbox is checked check parent checkbox*/
	$('input[name$="checkboxDel"]').click(function() {											   
	var tolCnt = $('input[name$="checkboxDel"]').length;	
	var chkCnt = $('input[name$="checkboxDel"]:checked').length;
	if(tolCnt == chkCnt)
		$('input[name$="deleteAll"]').attr('checked','checked');
	else
		$('input[name$="deleteAll"]').removeAttr('checked');
	});
	
	$('.mobGrd td img[name$="DeleteRow"]').hide();
	// For Shopper Module to get the view height
	var mnHt = $(".mobLink").height();
	//alert(mnHt);
	$(".dataView .fluidView div,.fluidViewHD div.cmnPnl").height(mnHt);
	if($(".topSubBar")) {
		var subBarHt = $(".topSubBar").height();
		$(".dataView,.fluidView div,.fluidViewHD div.cmnPnl").height(mnHt - subBarHt - 2);
	}
	$(".dataView").css("overflow-x","hidden");
	$(".fluidView div,.fluidViewHD div.cmnPnl").css("overflow-x","hidden");
	$(".dataView #rtlrLst").hide();
	$('li #actSht').click(function() { $('#actionSheet').slideToggle('medium'); });
	 $('img[name$="actnCncl"],.actnShtLst li a').click(function() {$('#actionSheet').slideUp('medium'); });
	
	//shopper module left navigation links
	$("#mainMenu li").click(function() {
				$("#mainMenu li:eq(0) a").attr('href','thisLocation.html'); 
				$("#mainMenu li:eq(1) a").attr('href','shopperFind.html'); 
				$("#mainMenu li:eq(2) a").attr('href','scanNow.html'); 
				$("#mainMenu li:eq(3) a").attr('href','shoppingList.html');
				$("#mainMenu li:eq(4) a").attr('href','hotDeals_new.html');
				$("#mainMenu li:eq(5) a").attr('href','wishList.html');
				$("#mainMenu li:eq(6) a").attr('href','gallery_All.html');
				$("#mainMenu li:eq(7) a").attr('href','settings_pref.html');
				$("#mainMenu li:eq(8) a").attr('href','myAccount.html');
									 });
				$("#mainMenu li:eq(6) a").text("My Coupons");
				$("#mainMenu li:eq(7) a").attr('class','Pref');
				$("#mainMenu li:eq(7) a").text("Preferences");
	          /*$("#mainMenu li:eq(4) a").click(function() {
											 
				var msg = confirm("GPS Disabled, Enable GPS");											 
											 
											 });*/
			  
	/*Grids alternate color & hover effect*/
	$(".stripeMe").find('tr').mouseover(function(){$(this).addClass("over");}).mouseout(function(){$(this).removeClass("over");});
	$(".stripeMe tr:even").addClass("hvrEfct");
	/*$(".prodSummary tr:even").addClass("evenRow");*/
	$(".prodSummary td:first-child").addClass("oddRow");	
	//To Show n hide the progress indicator and next button
	if(localStorage.getItem("hideSts") == "close") {
		$('.tglSec').hide();
	}
	else if(localStorage.getItem("hideSts") == "open"){
	var $this = $('#togglePnl a');
		//var $this = $(this);
		if($this.get(0)) {
		var $img = $this.find('img');
		var imgSrc = $img.attr('src');
		var imgPath = imgSrc.substr(0,imgSrc.lastIndexOf('/'));

		//alert(imgPath);
		var htmlTest =  "<img src='"+imgPath+"/upBtn.png'/> Hide Panel";
		//alert(htmlTest);
		$this.html(htmlTest);	}
	}
	
	$('#togglePnl a').click(function(){		 
		var $this = $(this);	
		var $img = $this.find('img');
		var imgSrc = $img.attr('src');
		var imgPath = imgSrc.substr(0,imgSrc.lastIndexOf('/'));
		$('.tglSec').slideToggle('fast',function() { 
			var htmlTest = ($(this).css('display') == "block")? "<img src='"+imgPath+"/upBtn.png'/> Hide Panel":  "<img src='"+imgPath+"/downBtn.png'/> Show Panel"
			localStorage.setItem("hideSts",($(this).css('display') == "block")?"open":"close");
			$this.html(htmlTest);
		});
		
	});		
	
		
	//To show rollover effect on mouse over
	$('img.NextNav').hover(function () {
	this.src = 'images/nextBtnHover.png';
	}, function () {
	this.src = 'images/nextBtn.png';
	});
	
	//To show rollover effect on mouse over
	$('img.NextNav_R').hover(function () {
	this.src = '../images/nextBtnHover.png';
	}, function () {
	this.src = '../images/nextBtn.png';
	});
	

	
		
	
	$('#srchByCity_view thead tr,.mobSubTitle,.mobApi').mouseover(function() {$(this).css("cursor","default")});
	$('#HotDeal_listView').hide();
	$('#hotDealDetails').hide();
	$('#subnavTab').hide();
	$('#srchByCity_view tr,#srchByCtgry_view tr').not('thead tr').click(function() {
												$('#HotDeal_listView').show();  
												$('#srchByCity_view,#srchByCtgry_view,#preferedCatgry_view').hide();
												
													  });
	$('#HotDeal_listView tr,.mobSubTitle,.mobApi,#HotDeal_listView_2 tr').not('thead tr').click(function() {
												$('#hotDealDetails').show();  
												$('#subnavTab').slideDown();
													  });
	
	$('#prefCtgry').click(function() {
	$('#preferedCatgry_view').show();	
	//$('#hotDealDetails').show();  
				   });
	
	$('#savePrefCtgry').click(function() {
	
	$('#HotDeal_listView').show();  
	$('#preferedCatgry_view').hide();
	});
	
	//To display map onclick of map icon in hotdeals.
	$('#mapviews').hide();
	$('img[name$="ByMap"]').click(function() {
										   //alert("map");
										   
										  // $('div.cmnPnl').hide();
										   $('#mapviews').show();
										   $('#hotDealDetails,#subnavTab').hide();
										   
										   $('#HotDeal_listView').click(function() { 
																 $('#mapviews').hide();				 
																				 });
										   }); 
	$('img[name$="ByCity"],img[name$="SearchByText"]').click(function() { 
																	  
															 $('div.cmnPnl').show();	
															 $('#mapviews').hide();
																	  });
	
	if($('.preferencesLst tbody tr').hasClass('mainCtgry')) {
	$('.preferencesLst tbody tr:not(.mainCtgry)').hide();	
	}
	$(".preferencesLst tr.mainCtgry input:checkbox").click(function() {								
		var maincatNm =  $(this).parents('tr').attr('name');	
		var self = this;
		if(self.checked) {
			$('.preferencesLst tr[name="'+maincatNm+'"]').show();
			var cnt = $('.preferencesLst tr[name="'+maincatNm+'"]').length;
			$('tr[name="'+maincatNm+'"]').each(function(i) {
				if(i>0 && self.checked) {
					$(this).find('input:checkbox').attr('checked', 'checked')
				}	
			}); 
		}else {
			$('.preferencesLst tr:not(.mainCtgry)[name="'+maincatNm+'"]').hide();
		/*	$('tr[name="'+maincatNm+'"]').each(function(i) {
						$(this).find('input:checkbox').attr('checked', self.checked)								
														});*/
		}
	});
	
	$(".preferencesLst tr:not(.mainCtgry) input:checkbox").click(function() {
		var maincatNm =  $(this).parents('tr').attr('name');	
		if($('tr:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length < 1) {
			$('tr[name="'+maincatNm+'"]').each(function(i) {
				if(i==0) {
					$(this).find('input:checkbox').removeAttr('checked');
				}else {
					$(this).hide();	
				}
			});
		}
		var totChk = $('.preferencesLst tr:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox').length;
		var chkLen = $('.preferencesLst tr:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length;
		if(chkLen == totChk)
			$('.preferencesLst tr.mainCtgry[name="'+maincatNm+'"] :checkbox').attr('checked','checked');
	});
	
	
	$(".preferencesLst input:checkbox:checked").each(function() {
		var maincatNm =  $(this).parents('tr').attr('name');
		
		$('.preferencesLst tr[name="'+maincatNm+'"]').show();
		var totChk = $('.preferencesLst tr:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox').length;
		var chkLen = $('.preferencesLst tr:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length;
		if(chkLen == totChk)
			$('.preferencesLst tr.mainCtgry[name="'+maincatNm+'"] :checkbox').attr('checked','checked');
	});	

});

	$(window).load(function(){						
	$('img[name$="DeleteRow"]').click(function() { 
	var catname = $(this).parents('tr').attr('name');
	//alert(catname);
	var catCnt = $('#cpnUsed tr[name="'+catname+'"]').length;
	//alert(cpnCnt);
	if(catCnt <= 2) {
		alert("Last Row: Deleting "+catname +" Section");
		$('#cpnUsed tr[name="'+catname+'"]:first').remove();
	}
	});
	  
	//For displaying banner ad & ribbon ad
	$("#ribbonAd").hide();
	$("#onlyAds").hide();
	$("#bannerAd").delay(2000).fadeOut('slow', function() { $("#ribbonAd,#onlyAds").show()});
	$("#loading-image").hide();	
	$("#ribbonAd").css('border-bottom','1px solid #e3e3e3');
	//Table scroll with fixed header. Check if that element exists than set the properties.
	if($.fn.tableScroll)		{	
		if(document.getElementById('thetable')){
			$('#thetable').tableScroll({height:140});//Height for customization
			$('.shprView').tableScroll({height:180});
		}

	}
	// Vertical Navigation scroll along page scroll In Shopper Module
	var $scrollingDiv = $("#verticalNav");
	$(window).scroll(function(){
	console.log($(window).scrollTop())	
		if($(window).scrollTop() > 95) {
			$scrollingDiv
			.stop()
			.animate({"top":$(document).scrollTop()},"slow")
			.css('position','absolute');
			}else {
			$scrollingDiv.css('position','static');
		}
	});
	
	//Trigger enter key for the image button in log in screen
	$(document).keypress(function(e) {
    if(e.which == 13) {
       // call the validation function to check user name & password 
		validateUserForm(['#pswd','#userName'],'li',chkUser);
    }
	});
	
	$('img[name$="shprDshbrd"]').hide();
	$('img[name$="shprDshbrd"]').fadeIn('slow');
	$('select[name$="srchOpt"]').change(function() {
	var slctOpt = $('select[name$="srchOpt"] option:selected').val();
	$(this).attr("checked","true");
	});
	
/*	$('.mobGrd td img[name$="DeleteRow"]').hide();*/
	$('.mobGrd td').live('mouseover', function() {
    $(this).closest('tr').find('img[name$="DeleteRow"]').show();
	}).live('mouseout',function() { 
	 $(this).closest('tr').find('img[name$="DeleteRow"]').hide();
	});
	$('.mobGrd td img[name$="DeleteRow"]').click(function() {
	  $(this).closest('tr').remove();
	  });
	
	/*Coupon add icon chng*/
	$('img[name$="cpnTgl"]').css("cursor","pointer");
	$('img[name$="cpnTgl"]').click(function() {	
		var curRowData = $(this).closest('tr').html();
		var image = $(this);
		if ($(image).attr("src") == "images/cpnAddicon.png")
		{
		$(image).attr("src", "images/cpnIcon.png");
		alert("Coupon Added to Gallery");
		}
		else
		{
		$(image).attr("src", "images/cpnAddicon.png");
		alert("Coupon Removed from Gallery");
		}
	});
	
	/*Search Page Toggle display of 2 divs*/
    $("#HdealLst").hide();
    $("#subnav a").click(function() {
			$("#subnav a").removeClass('active');
			$(this).addClass('active')
		if($(this).attr('id') =="productList" ) { 
			$("#prodLst").show();
			$("#HdealLst").hide();
		}
		else {
			$("#HdealLst").show();
			$("#prodLst").hide();
			
		}
    });
	$("#subnav.tabdSec a").click(function() {
				$(this).removeClass('active');
				$(this).addClass('active')
				if($(this).attr('id') == "prodDet") {
								$('table.prodDet').show();
								$('table.reviewDet').hide();
								}
								else {
								$('table.reviewDet').show();
								$('table.prodDet').hide();
									
								}
	  });
	/*$('.tabdView table').hide();
	var intAct = $("#subnav.HdPtabdSec a").attr('id');
	$('.'+intAct).show();
	$("#subnav.HdPtabdSec a").click(function() {							
				$(this).removeClass('active');
				$(this).addClass('active')
				var curTab = $(this).attr('id');
				$('.'+curTab).not("'.'+curTab").hide();
				$('.'+curTab).show();
											 });*/
	//To navigate for search results page based on dropdwon values selection
	$("#HdealLst").hide(); 
	$('.leftPdng select option').each(function(i) {
		var $this = $(this);
		if(i < 1) {									   	
			$this.attr('value','searchResults.html') 
		}else {
			$this.attr('value','search_result_sharp2.html') 	
		}
	});
	
	 $(".srchBtn").click(function(){
		var $sel = $('.leftPdng select'); 
    	if ($sel.val()!='') {
			if(window.location.pathname.search("Manufacturer") > 1 || window.location.pathname.search("Retailer") > 1 ) {
			    window.location.href = "../"+$sel.val();
			}else {
		  		window.location.href=$sel.val();
			}
    	}
  	});
	 
	 /*hightlight table row & add/remove read only attr for inputs*/
	
	 $("#highLight tr input").attr("readonly", "readonly");
	 $("#highLight tr img").css('cursor','pointer');	  
	 /*$("#highLight tr:gt(0)").click(function() {
											 alert("Inside1");
		var $tr = $(this).closest('tr');
		var myRow = $tr.index();
		var inputText = new Array();
		$("input").each(function(index){
			var $indexVal = $(this).get();
			if($indexVal[0].className == ""){
				var val = $indexVal[0].value;
				inputText.push(val);
			}
		});
		  });*/
		//document.getElementsByTagName("input")[2].classList[0];
		$("#highLight tr:gt(0),#highLight tr input").click(function(){
																	//alert("Inside2");
										$("#highLight").find('tr.hilite')
													.removeClass('hilite')
													.find("input").attr("readonly","true");
										$(this).addClass('hilite')
												.find("input").removeAttr("readonly").focus();
									 
										  });
	
	 	$("#locSetUpGrd input").attr("readonly", "readonly");
		$("#locSetUpGrd tr:gt(0)").click(function(e){
			$("#locSetUpGrd").find('tr.hilite')
			.removeClass('hilite')
			.find("input").attr("readonly","true");
			$(this).addClass('hilite')
			.find("input").removeAttr("readonly");
			if(e.target.nodeName == "INPUT") {
				e.target.select();	
			}
		});
		
		/* Click table row to trigger a checkbox*/
		$('.stripeMe tr').click(function(event) {
		if (event.target.type !== 'checkbox') {
		  $(':checkbox', this).trigger('click');
		}
  		});


	
	$(".btnNav").click(function() {
		window.location=$(this).find("a").attr("href");
		return false;
	});

	//Home Page Slide Show
	theRotator();
	$('div.rotator').fadeIn(1000);
	$('div.rotator ul li').fadeIn(1000); // tweek for IE

	$('#clearFrm').click(function() {
	$('form').clearForm();//to clear form data		  
	});
	
	$('#iphoneMenu').slideDown();
	
	//Toggle Section code for dashboard grids
	var collaspeCls = '.searchGrd h1[class="searchHeaderCollaspe"]';
	var expandCls = '.searchGrd h1[class="searchHeaderExpand"]';
	var secContentPanel = '.grdCont';
	$('.searchGrd h1').attr("title", "Collapse");

	$(expandCls).click(function(){
	if($(this).hasClass('searchHeaderExpand')){
	$('.searchGrd h1').attr("title", "Expand");
	$(this).removeClass('searchHeaderExpand').addClass('searchHeaderCollaspe');
	$(this).next(secContentPanel).slideUp('jswing');
	
	}
	else{
	$('.searchGrd h1').attr("title", "Collapse");
	$(this).removeClass('searchHeaderCollaspe').addClass('searchHeaderExpand');
	$(this).next(secContentPanel).slideDown('jswing');
	} 
	});

	
	
/*	$('.tabs a').click(function(e) {
	if($(this).is('.tabActive'))
		return false;
	var index = $(this).parent('li').index();
	highLightImg(index,e.type)
	});
	actIndex = $('.tabs a.tabActive').parent('li').index()
	if(document.getElementById('menu')){
	var t = setTimeout(function() {
	highLightImg(actIndex,'click')
	},100);
	}*/
					
					 
	if($.fn.ticker)		{	
		if(document.getElementById('js-news')){
			$('#js-news').ticker();
		}

	}

/*----------------------------------------------------Shopping Cart Code in shopper module Starts-----------------------------------------------------*/
	var itemCount = $("#slctItems tr td:first-child img").length;
	var cartCount = $("#cartItems tr td:first-child img").length;
	$(".topSubBar tr td span.cntItems").html(itemCount);
	$(".topSubBar tr td span.cntCart").html(cartCount);
	$("#slctItems tr td img[name$='tglImgs']").css("cursor","pointer"); 
	$("#slctItems tr td img[name$='tglImgs']").attr("title","Add to Cart"); 
	$("#slctItems tr td img[name$='tglImgs']").live('click',function() {
																	
												var catNm =  $(this).parents('tr').attr('name');
												if($('#cartItems tr[name="'+catNm+'"]').length == 0) {
													var rowData = $('<tr name="'+catNm+'" class="noHvr"><td colspan="5" class="mobSubTitle">'+catNm+'</td>')
												    
													if($('#cartItems tr').length == 0){
														$(rowData).appendTo($('#cartItems tbody'));
													}else {
												    	$(rowData).insertAfter('#cartItems tr:last')
													}
												}
												var crr = $(this).parents('tr').remove('tr').clone(true).insertAfter('#cartItems tr[name="'+catNm+'"]:last');
												//alert($('#slctItems tr[name="'+catNm+'"]').length)
												if($('#slctItems tr[name="'+catNm+'"]').length == 1) {
													$('#slctItems tr[name="'+catNm+'"]').remove()				 
												}
												$("#cartItems tr td img[name$='tglImgs']").attr("src", "images/cartDelete.png");
												$("#cartItems tr td img[name$='tglImgs']").attr("title", "Move to List");
												var itemCount = $("#slctItems tr td:first-child img").length;
												var cartCount = $("#cartItems tr td:first-child img").length;
												//alert(itemCount);
												$(".topSubBar tr td span.cntItems").html(itemCount);
												$(".topSubBar tr td span.cntCart").html(cartCount);
												$('.mobGrd td img[name$="DeleteRow"]').hide();
												
	$('.mobGrd td').live('mouseover', function() {
    $(this).closest('tr').find('img[name$="DeleteRow"]').show();
	}).live('mouseout',function() { 
	 $(this).closest('tr').find('img[name$="DeleteRow"]').hide();
	});
	$(".htlt tr").mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct")});
	
	/*$('.mobGrd td img[name$="DeleteRow"]').hide();*/
	$('.mobGrd td').live('mouseover', function() {
    $(this).closest('tr').find('img[name$="DeleteRow"]').show();
	}).live('mouseout',function() { 
	 $(this).closest('tr').find('img[name$="DeleteRow"]').hide();
	});
	$('.mobGrd td img[name$="DeleteRow"]').click(function() {
	 
	 $(this).closest('tr').remove();
	/* var grpdRow = $('#alertedItmLst tr td.grpdtxt').length;
	
	 alert(grpdRow);*/
	  });
	
												   });
	$("#cartItems tr td img[name$='tglImgs']").css("cursor","pointer"); 
	$("#cartItems tr td img[name$='tglImgs']").attr("title","Move to List"); 
	$("#cartItems tr td img[name$='tglImgs']").live('click',function() {
												var catNm =  $(this).parents('tr').attr('name');
												if($('#slctItems tr[name="'+catNm+'"]').length == 0) {
													var rowData = $('<tr name="'+catNm+'" class="noHvr"><td colspan="5" class="mobSubTitle">'+catNm+'</td>')
													if($('#slctItems tr').length == 0){
														$(rowData).appendTo($('#slctItems tbody'));
													}else {
												    	$(rowData).insertAfter('#slctItems tr:last')
													}
												}
												var crr = $(this).parents('tr').remove('tr').clone(true).insertAfter('#slctItems tr[name="'+catNm+'"]:last');
													if($('#cartItems tr[name="'+catNm+'"]').length == 1) {
													$('#cartItems tr[name="'+catNm+'"]').remove()				 
												}
												$("#slctItems tr td img[name$='tglImgs']").attr("src", "images/cartAdd.png");
												$("#slctItems tr td img[name$='tglImgs']").attr("title", "Add to Cart");
												var itemCount = $("#slctItems tr td:first-child img").length;
												var cartCount = $("#cartItems tr td:first-child img").length;
												//alert(itemCount);
												$(".topSubBar tr td span.cntItems").html(itemCount);
												$(".topSubBar tr td span.cntCart").html(cartCount);
												$('.mobGrd td img[name$="DeleteRow"]').hide();
	$('.mobGrd td').live('mouseover', function() {
    $(this).closest('tr').find('img[name$="DeleteRow"]').show();
	}).live('mouseout',function() { 
	 $(this).closest('tr').find('img[name$="DeleteRow"]').hide();
	});
	$(".htlt tr").mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct")});
	
	$('.mobGrd td img[name$="DeleteRow"]').hide();
	$('.mobGrd td').live('mouseover', function() {
    $(this).closest('tr').find('img[name$="DeleteRow"]').show();
	}).live('mouseout',function() { 
	 $(this).closest('tr').find('img[name$="DeleteRow"]').hide();
	});
	$('.mobGrd td img[name$="DeleteRow"]').click(function() {
	 
	 $(this).closest('tr').remove();
	/* var grpdRow = $('#alertedItmLst tr td.grpdtxt').length;
	
	 alert(grpdRow);*/
	  });
	
												   });
	
/*----------------------------------------------------Shopping Cart Code in shopper module Ends-----------------------------------------------------*/	

	$('.rtlrPnl tr').mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct");})
	.bind('click',function() {
										
								if($("table").attr('id') == 'rtlrLst'){
									window.location.href="thisLocation_prod.html";	
								 }
								 if($("table").attr('id') == 'prodView'){
								    window.location.href="thisLocation_prodSum.html";	
								 }
								 var curLink = $(this).find('a').attr('href');
								 //alert(curLink);
								
								 if(curLink !== undefined){
									 window.location.href=curLink;
									
								 }else {
									window.location.href="thisLocation_prodSum.html";	 
								 }
								
								 
							});	
	$("#rtlrLst tr").click(function() {
							window.location.href="thisLocation_prod.html";			
									});
		$("#prodView tr").mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct");})
	.bind('click',function() {
							window.location.href="thisLocation_prodSum.html";			
									});
	$("#findPnl tr").click(function() {
							window.location.href="thisLocation_prodSum.html";			
									});
	$("#slctItems tr td:nth-child(2)").css("cursor","pointer"); 
	$("#slctItems tr td:nth-child(2)").click(function() {
													
													 window.location.href="thisLocation_prodSum.html";	
													  });
	$("#cartItems tr td:nth-child(2)").css("cursor","pointer"); 
	$("#cartItems tr td:nth-child(2)").click(function() {
													
													 window.location.href="thisLocation_prodSum.html";	
													  });
	$(".htlt tr").mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct")});
	
	$("#htdealLst").hide();																																				
	$('#srchByCity tr').mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct");})
	.bind('click',function() {
						var curLink = $(this).find('a').attr('href');
						if(curLink !== undefined){
									 window.location.href=curLink;
									
								 }else {
									window.location.href="curLink";	 
								 }
								$("#htdealLst").fadeOut().fadeIn();
								$("#Note").hide();
						 });
	$('#srchByCtgry tr').mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct");})
	.bind('click',function() {
						var curLink = $(this).find('a').attr('href');
						if(curLink !== undefined){
									 window.location.href=curLink;
									
								 }else {
									window.location.href="curLink";	 
								 }
								$("#htdealLst").fadeOut().fadeIn();
								$("#Note").hide();
						 });
	$('tr.noHvr').mouseover(function() {
									
									 $(this).removeClass("hvrEfct").css("cursor","default"); 
									 
									 });
	$("#Note tr.noHvr").click(function() {
							  window.location.href="thisLocation.html";
							  $("#Note tr.noHvr").unbind('click');
							  });
	$("#cnfrm").click(function() {
	$('.cnfrmImg').attr('src',"../images/confirmicon.png");
	$('.cnfrmImg').attr('title',"Confirmed");
	var Imgtitle = $('.cnfrmImg').attr("title");
	$('em').text(Imgtitle).fadeOut('slow');
	
	$('#cnfrm').attr('disabled', 'disabled');
	 
				   });
	$('#cnfrm').removeAttr('disabled', 'disabled');
	$("tr td ul li a.delRow").click(function() {
	//alert($(this).parent().parent().parent().parent().parent().html());
	//$(this).parent().parent().parent().parent().remove();
	alert("Row Deletion");
	$(this).parents('tr:eq(0)').remove();
	});	
	
	$(".focustxt").click(function() {
			$(this).val("");						  
									  });
/*------------------------------------------Shopper Module tab bar icons rollover effect Starts-----------------------------------*/
	$(".tabBar li img,#subnavTab li img").live('mouseover',function()
    {
		/*var t= $(this);
       	$(this).attr('src',t.attr('src').replace('_up','_down'));*/
		this.src = this.src.replace("_up","_down");
		
     	// this.src = this.src.replace("tab_btn_up_cpnGlry.png","_ov.jpg");
    }).live('mouseout',
    function()
    { this.src = this.src.replace("_down","_up"); });
	
/*------------------------------------------Shopper Module tab bar icons rollover effect Ends-----------------------------------*/	
	$('#srchByCtgry').hide();
  	$(".tabBar li").click(function() {
					$('#srchByCity').hide();
					$('#srchByCtgry').hide();
					var clsNm = $(this).attr("class");
						
						$('#'+clsNm).fadeIn();	
						
								   });
	
	radioSel('slctOpt')		
	$('input[name="slctOpt"]').click(function() {
		radioSel('slctOpt')			   
					   });
	$('.ctgry').hide();
	$(".srchCtgry img").click(function() {
							//alert("hi");
							$('.ctgry').slideDown('fast');
									   });
	
	// Click of row trigger checkbox
	/*$(".preferencesLst tbody tr").click(function(event) {
	if(event.target.type !== 'checkbox') 
	{
		$(':checkbox', this).trigger('click');
	}
	});*/

	// Pass Selected drop down value to the title attribute of Add Image
	$("#addItemsTo option").click(function() {
									var curVal = $("#addItemsTo option:selected").text();
									//alert(curVal);
									$('img.addItemsImg').attr('title',curVal);
									
									});

	// Check if first option is selected & alert user
	$('img.addItemsImg').click(function() {
										var curVal = $("#addItemsTo option:selected").val();
									//alert(curVal);
									$('img.addItemsImg').attr('title',curVal);
								if($("#addItemsTo option:first:selected").val() == "Please Select")
								{
								alert("Note: Please Select Your Option");	
								}else
								alert("Selected Items "+curVal);
										});
	
	$('#alertedItmLst tr').mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct");})
	.bind('click',function() {
	
	if($("table").attr('id') == 'alertedItmLst'){
		var navurl = $(this).find('a').text();
		//alert(navurl);
	//window.location.href=navurl;	
	}
	
	});
	$('#findPnl tr').mouseover(function(){$(this).addClass("hvrEfct");$(this).css("cursor","pointer");}).mouseout(function(){$(this).removeClass("hvrEfct");})
	
/*	if($('#srchByCity').html()) {
	
	var $fixedTable = $('<table class="gnrlGrd fixedTableHeader" cellspacing="0" cellpadding="0" width="100%"></table>');  
	$fixedTable.append($("<thead></thead>"));
	$fixedTable.css({'top':$('#srchByCity').offset().top,'left':$('#srchByCity').offset().left,"width":$('#srchByCity').width()})
	$('#srchByCity').parent().append($fixedTable);
	$('#srchByCity').each(function() {
		var $this = $(this);
		$(this).find('tr:first')
		.clone()
		.appendTo($('.fixedTableHeader thead'));
	});
	
	}*/
	//toggle input field in FIND module.
//	$('input[name$="search_find"]').hide();
	if($('select[name$="shprfindOpt"] option:selected').text() == "Product")
		{
			$('input[name$="search_find"]').show();
		}	
	$('select[name$="shprfindOpt"]').change(function() {
		if($('select[name$="shprfindOpt"] option:selected').text() == "Product")
		{
			//$('input[name$="search_find"]').slideDown();
			$('.chngImg').attr('src','images/scanseeSrchIcon.png').show();
			$('input.textboxSmall').attr('name','search_find');
			$('form').attr('action','shopperFindprod.html');
			$('.stretch').hide();
		}	
		else {
			//$('input[name$="search_find"]').slideUp();
			$('.chngImg').attr('src','images/googleIcon.png').show();
			$('.stretch').show();
		}
	});
	
						   });

/*--------------------------------------------------------------Functions Section-----------------------------------------------------*/

//To resize the html page
function resizeDoc(){
	var headerPanel = document.getElementById('header');
	var footerPanel = document.getElementById('footer');	
	var resizePanel = document.getElementById('content');
	/*var dockPanel = document.getElementById('dockPanel');*/
	
	if (document.getElementById('wrapper')) {
		if(getWinDimension()[1] - (headerPanel.offsetHeight + footerPanel.offsetHeight + 20) > 0) {
		resizePanel.style.minHeight = getWinDimension()[1]
		- (headerPanel.offsetHeight + footerPanel.offsetHeight + 20)
		+ "px";
		}
	}
	}
	
	//To get client height
	function getWinDimension(){
	var windowHeight = "";
	var windowWidth = "";
	
	if(!document.all||isOpera()){
		windowHeight = window.innerHeight;
		windowWidth = window.innerWidth;
	}
	else {
		windowHeight = document.documentElement.clientHeight;
		windowWidth = document.documentElement.clientWidth;
	}
	return [windowWidth, windowHeight]
	
	}
	//check if the client is opera
	function isOpera(){
		return (navigator.appName.indexOf('Opera') != -1);
}

function radioSel(radioName) {
	$('tr.slctCty').hide();
	$('tr.slctLoc').hide();
	var cls = $('input[name ="'+radioName+'"]:checked').val();
	$('tr.'+cls).show();
}
//move selected option form source to destination list.
function move_list_items(sourceid, destinationid)
{	
	$("#"+sourceid+"  option:selected").appendTo("#"+destinationid);
}

function theRotator() {
	//Set the opacity of all images to 0
	$('div.rotator ul li').css({opacity: 0.0});
	
	//Get the first image and display it (gets set to full opacity)
	$('div.rotator ul li:first').css({opacity: 1.0});
	
	//Call the rotator function to run the slideshow, 6000 = change to next image after 6 seconds
	
	setInterval('rotate()',6000);

}

function rotate() {	
	//Get the first image
	var current = ($('div.rotator ul li.show')?  $('div.rotator ul li.show') : $('div.rotator ul li:first'));
	
	if ( current.length == 0 ) current = $('div.rotator ul li:first');
	
	//Get next image, when it reaches the end, rotate it back to the first image
	var next = ((current.next().length) ? ((current.next().hasClass('show')) ? $('div.rotator ul li:first') :current.next()) : $('div.rotator ul li:first'));
	
	//Un-comment the 3 lines below to get the images in random order
	
	//var sibs = current.siblings();
	//var rndNum = Math.floor(Math.random() * sibs.length );
	//var next = $( sibs[ rndNum ] );
	
	
	//Set the fade in effect for the next image, the show class has higher z-index
	next.css({opacity: 0.0})
	.addClass('show')
	.animate({opacity: 1.0}, 1000);
	
	//Hide the current image
	current.animate({opacity: 0.0}, 1000)
	.removeClass('show');

};

//To delete row 
function deleteRowThis(obj) {
	alert("Row Deletion");
	$(obj).parents('tr').remove();
}
//table row delete #MngLoc 

function chkUser(){
	var loginTxtBox = document.getElementById('userName');
	var str = 'Please Enter User Name as:  \n \nSupplier / Retailer'
	
	if(loginTxtBox.value==''){
		alert(str)
	}

	else {

	if(loginTxtBox.value.toUpperCase() == 'SUPPLIER')
		document.location.href = 'adminpage_mfg.html';

	else if(loginTxtBox.value.toUpperCase()=='RETAILER')
		document.location.href = 'adminpage_ret.html';

	else{
		alert('Invalid User');
		alert(str)
		}
	}
	document.getElementById('userName').value = '';
	document.getElementById('pswd').value = '';
}


/* This function for Opening the iframe popup by using the any events &  we cann't able to access any thing in the screen except popup   
	parameters:
				popupId		: Popup Container ID
				IframeID	: Iframe ID
				url			: page path i.e src of a page
				height		: Height of a popup
				width		: Width of a popup 	
				name		: here name means Header text of a popup 
				btnBool		: if you have any buttons means we can use it i.e true/false
*/
function openIframePopup(popupId, IframeID, url, height, width, name,btnBool) {
	frameDiv = document.createElement('div');
	frameDiv.className = 'framehide';
	document.body.appendChild(frameDiv);
	document.getElementById(IframeID).setAttribute('src', url);
	//frameDiv.setAttribute('onclick', closePopup(obj,popupId));
	document.getElementById(popupId).style.display = "block"
	height = (height == "100%")?frameDiv.offsetHeight-20:height;
	width = (width == "100%")?frameDiv.offsetWidth-16:width;
	document.getElementById(popupId).style.height = height + "px"
	document.getElementById(popupId).style.width = width + "px"
	var marLeft = -1 * parseInt(width / 2);
	var marTop = -1 * parseInt(height / 2);
	document.getElementById('popupHeader').innerHTML = name;
	document.getElementById(popupId).style.marginLeft = marLeft + "px"
	document.getElementById(popupId).style.marginTop = marTop + "px"
	var iframeHt = height - 27;
	document.getElementById(IframeID).height = iframeHt+"px";
	if(btnBool) {
		var btnHt = height - 50;
		document.getElementById(IframeID).style.height = btnHt+"px";
	}

}
/* This function for Closing the iframe popup 
	parameters:
				popupId	: Popup Container ID
				IframeID	: Iframe ID
*/
function closeIframePopup(popupId,IframeID,funcUrl) {
    try {
		document.body.removeChild(frameDiv);
		document.getElementById(popupId).style.display = "none";
		document.getElementById(popupId).style.height = "0px";
		document.getElementById(popupId).style.width = "0px";
		document.getElementById(popupId).style.marginLeft = "0px";
		document.getElementById(popupId).style.marginTop = "0px";
		document.getElementById(IframeID).removeAttribute('src');
		if(funcUrl){
			funcUrl	
		}
	}catch (e) {
		top.$('.framehide').remove();	
		top.document.getElementById(popupId).style.display = "none";
		top.document.getElementById(popupId).style.height = "0px";
		top.document.getElementById(popupId).style.width = "0px";
		top.document.getElementById(popupId).style.marginLeft = "0px";
		top.document.getElementById(popupId).style.marginTop = "0px";
		top.document.getElementById(IframeID).removeAttribute('src');
		if(funcUrl){
			funcUrl	
		}
	}	
}

function callBackFunc() {
	var $cpNm = top.$('#couponName');
	var chkArry = []
		$('input[name="pUpcChk"]:checked').each(function() {
					chkArry.push(this.value)									 
		});
	$cpNm.val(chkArry)
}



function triggerFile(obj) {
	$("#imgBrwse")
		.click()
		.select(function() {
		   $(obj).parents('td').find('input').val(this.value)})
		.change(function() {
			 $(obj).parents('td').find('input').val(this.value)
		});
		
}

function validateUserForm(arryNm,parent,funcCall) {
	var errBool = true;
	$('.errIcon').removeClass('errIcon');
	for(var i in arryNm) {
		var $frmE = $(arryNm[i]);
		var val = $.trim($frmE.val());
		val = (val =="(   )      -")?"":val;
		if(val =='' ) {
			$frmE.parents(parent).find('.errDisp').addClass('errIcon')
			errBool = false;
		}
	}
	
	if(errBool) {  
	 if(typeof funcCall == "function")
		funcCall();
	 else 
	 	window.location.href =funcCall;
		 
	}
}

//Append form data to the table 
function appendData()
{
	
	var rowCount = $('#attrGrd >tbody >tr').length;

	
	// alert(rowCount); To limit attribute to 5
	 if(rowCount > 4){
		
		alert("Note: You can add only 5 Attributes");
		return false;
	 }
	 /*To clone entire row*/
	  $('#attrGrd tbody>tr:last').clone(true).insertAfter('#attrGrd tbody>tr:last');
	  //clear the cloned row input values
	  $("#attrGrd tr:last input:text").val("");

	  $('#attrGrd tr').each(function(i) {
    		$(this).find('td').each(function(j) {
						if(j%2 == 0 ) {
							var txt = $(this).find('label').text().split(' ');
							var incr = i+1;
							var labelTxt = txt[0]+" "+txt[1]+" "+incr;
							$(this).find('label').text(labelTxt)
						}
											  });
		});

}
	
//To delete row 
function deleteRowThis(obj) {
	//alert("Delete Attribute");
	if($(obj).parents('table').find('tr').length>1){
		var msg = confirm("Do u want to delete this row")
		if(msg){
			$(obj).parents('tr').remove();
		}
	}
}
//delete all rows 
function deleteCurRowThis(obj) {
	//alert("Delete Attribute");
	if($(obj).parents('table').find('tr').length>0){
		var msg = confirm("Do u want to delete this row")
		if(msg){
			$(obj).parents('tr').remove();
		}
	}
}
//create Attribute dynamically
function crtAtrr() {
	
	var slctVals = $('select.attrList option:selected').val();
	if(slctVals == ''){
		alert("Note: Please select attribute");
		return false;
	}
	var attrContVal = $("#attrCont").val();
	var rowCount2 = $('#attrDpsly >tbody >tr').length;

	$('.slctdValues').text(slctVals);
	var tmpId = "ContVal_"+rowCount2;
	if(attrContVal == ''){
		alert("Note: Please enter attribute value");
		return false;
	}
	
	$("#attrDpsly tr td:not(:last-child)").css({ "border-right":"1px solid #dadada" });
	$("#attrDpsly tbody").append("<tr>" +
	  "<td width='24%'  class='Label Rtbrdr'>" + "<label>" + slctVals + "</label>" + "</td>" + 
	  "<td width='76%'>" + "<input id="+tmpId+" type='text'/ >" +
	  " <a href='#'><img src='images/delete.png' alt='delete' title='delete' width='16' height='16' onclick='deleteCurRowThis(this)'/></a>" + "</td>" + "</tr>" );	
	$("#"+tmpId).val(attrContVal);
	$('.tglDsply').show();
	$('form').get(0).reset();	
}
//To add new record at the end of the last row in table
function addNewRow() {
	var curRow = $("#locSetUpGrd tr:last").clone(true).insertAfter('#locSetUpGrd tr:last');
	//clear the last row input field values before cloning.
	$("#locSetUpGrd tr:last input:text").val("");
	
}

function moveToHstry() {
	var msg = confirm("        Ready to check-out?\n If you do, all your items will be\n          moved to history.");
	window.location.href="shoppingListHstry.html";
	
}
//To check whether zipcode is entered & Drop down values selected
function Validate() {
	var zipcodeChk = $('input[name$="zipcodeTL"]').val();
	
	if(zipcodeChk == ''){
		alert("Note: Please enter zip code");
		return false;
	}
	if($('select[name$="raiudTL"] option:first:selected').val() == "Select Radius")
	{
		alert("Note: Please Select Radius");
		return false;
	}
	$(".dataView table").show(); 
	$("#Note").fadeOut(); 
	$('.pagination').slideDown('fast');
}
//Navigate page based on drop down value selection
function navToPage() {
	var optSlctd = $('select[name$="shprfindOpt"] option:selected').val();
	var srchtxt = $('input[name$="search_find"]').val();
	if(srchtxt == ''){
		if($('input[name$="search_find"]').is(":visible")) {
		alert("Please enter product name");
		return false;
		}
	}
	window.location.href=optSlctd;
	$('input[name$="search_find"]').val("");	
}
function chkfields() {
	//alert("checked");
	var valDesc = $("#lngDsc").val();
	if(valDesc == ''){
	var palert = $("p").attr("class");
	$("p#alertMsg").html(palert);
	}
	else {
		window.location.href="Manufacturer/Mfg_choosePlan.html";		
	}
	//alert("Enter desc");
	return false;
	
}
//Before clearing all entered values alert user
function clrValues() {
	var alertMsg = confirm("All Values will be Cleared");
	if(alertMsg){
		$('form').get(0).reset();
	}
	else {
		return false;	
	}
}
//To Show progress bar on page load and navigate to another page
function showLoader() {
		 $('html,body').animate({scrollTop:0}, 'slow');//IE, FF
 		// $('body').animate({scrollTop:0}, 'slow');
		$('body').append("<div id='loading-image'>" + "<img src='images/ajaxblue.gif' alt='Loading...' title='Loading' width='128' height='15' />" + "<span id='loading-txt'>" + "Loading" + "</span>" + "</div>"); 
		//To get the original text and append ... to the text get animated effect Loading...
		var originalText = $("#loading-txt").text();
		i  = 0;
		$("#loading-txt").css('color','#ffffff');
		setInterval(function() {
		$("#loading-txt").append(".");
		i++;
		
		if(i == 4)
		{
		$("#loading-txt").html(originalText);
		i = 0;
		}
		
		}, 500); 	
		$(window).unbind('scroll');
		//Navigate to other page automatically once loading screen is displayed
		$("#loading-image").fadeIn('slow').data("active", false).delay(10000).fadeOut('slow',function() {
																						//function call to navigate page
																						toPge();
																						//todetach();//to remove the loading animation by calling this function.
																						});
	
			$(window).scroll(function(e){
			 $('html, body').animate({scrollTop:0}, 'slow');
			 return false;
      		});

}
function toPge() {
	window.location.href='Retailer/Retailer_AddLocSetup.html';
}
function todetach() {
	$("#loading-image").remove();
}
function sendMail() {
		 if($(".mailpopPnl").length==0){
		$('body .relatvDiv').append("<div class='mailpopPnl'>" + "<div class='mailInfo'>" + "<div class='actTitleBar'>" + "Send Mail:" + "<img src='images/actnClose.png' alt='close' title='close' name='actClose' align='right'/>" + "</div>" + "<ul>" + "<li>" + "<span>" + "To:" + "</span>" + "<input type='text'/> " + "</li>" + "<li>" + "<input type='button' value='Send' class='btn' id='emailId'/> <input type='reset' value='Clear' class='btn'/>" + "</li>" + "</ul>"+ "</div>" + "</div>"); 
		 }
		 else 
		 {
			 //alert user if popup already opened
			alert("Send Mail is active: Please fill in"); 
		 }
$('img[name$="actClose"]').click(function() { 
									$('.mailpopPnl').remove();			  
												  });
}

function editPswd() {

	$('.stretch').append("<div class='mailpopPnl'>" + "<div class='mailInfo'>" + "<div class='actTitleBar'>" + "Change Password:" + "<img src='images/actnClose.png' alt='close' title='close' name='actClose' align='right'/>" + "</div>" + "<ul>" + "<li>" + "<span>" + "Enter Password:" + "</span>" + "<input type='text' class='textboxSmall'/> " + "</li>" + "<li>" + "<span>" + "Confirm Password:" + "</span>" + "<input type='text' class='textboxSmall'/> " + "</li>" + "<li>" + "<input type='button' value='update' class='btn' id='emailId'/> <input type='reset' value='Clear' class='btn'/>" + "</li>" + "</ul>"+ "</div>" + "</div>"); 
	$('.mailInfo li:eq(0) span').width(109);
	$('img[name$="actClose"]').click(function() { 
									$('.mailpopPnl').remove();			  
												  });
			
}	



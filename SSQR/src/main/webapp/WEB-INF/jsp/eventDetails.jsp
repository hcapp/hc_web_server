<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>

<div class="viewAreaiPhn">
	<div class="iPhnCont">
		<h2 class="iPhnTitle">${sessionScope.events.hcEventName}</h2>
		<input type="hidden" id="titleName" value="${sessionScope.events.hcEventName}" />
		<div class="cont">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="zeroBrdrTbl">
				<tr>
					<td align="center" colspan="4"><c:choose>
							<c:when
								test="${sessionScope.events.eventImagePath ne null && !empty sessionScope.events.eventImagePath}">
								<div class="image">
									<img src="${sessionScope.events.eventImagePath}" width="70"
										height="70" />
								</div>
							</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose></td>
				</tr>
				<tr>
					<c:if
						test="${sessionScope.events.eventStartDate ne null && !empty sessionScope.events.eventStartDate}">
						<td width="25%" colspan="4" align="center"><b>Start Date : </b> ${sessionScope.events.eventStartDate}</td>
					</c:if>
				</tr>
				<tr>
					<c:if
						test="${sessionScope.events.eventEndDate ne null && !empty sessionScope.events.eventEndDate}">
						<td width="25%" colspan="4" align="center"><b>End Date : </b> ${sessionScope.events.eventEndDate}</td>
					</c:if>
				</tr>
				<tr>
					<td colspan="4"><div class="shrtDesc" align="center">${sessionScope.events.shortDescription}</div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><div class="lngDesc" align="center">${sessionScope.events.longDescription}
						</div></td>
				</tr>
				<c:if test="${sessionScope.events.moreInfoURL ne null && !empty sessionScope.events.moreInfoURL}">
					<tr>
						<td colspan="4" align="center" class="loclbl"><a href="${sessionScope.events.moreInfoURL}" title="${sessionScope.events.moreInfoURL}">More Information</a></td>
					</tr>
				</c:if>
				<tr>
					<td colspan="4" class="loclbl">Location Information</td>
				</tr>
				<tr>
					<td colspan="4">
						<ul class="loclist">
						<c:choose>
							<c:when test="${sessionScope.appsites ne null && !empty sessionScope.appsites}">
								<c:forEach items="${sessionScope.appsites}" var="appsite">
									<li>
										<c:choose>
											<c:when test="${appsite.appSiteImg ne null && !empty appsite.appSiteImg}">
												<img width="30" height="30" src="${appsite.appSiteImg}"/>												
											</c:when>
											<c:otherwise>												
												<img width="30" height="30"/>
											</c:otherwise>
										</c:choose>
										<span>${appsite.appSiteName}</span> ${appsite.address} , ${appsite.city} , ${appsite.state} - ${appsite.postalCode}
									</li>
								</c:forEach>	
							</c:when>
							<c:otherwise>
								<li>
								 	${sessionScope.events.address} , ${sessionScope.events.city} , ${sessionScope.events.state} - ${sessionScope.events.postalCode}
								 </li>
							</c:otherwise>
						</c:choose>
														
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<head>
<title>ScanSee</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
<script type="text/javascript" src="../scripts/jquery-1.6.2.min.js"></script>
<script src="../scripts/global.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
		//Get actual content and display as title on mouse hover on it.
	    var actText = $("td.genTitle").text();
		$("td.genTitle").attr('title',actText);
        var limit = 20;// character limit restricted to 20
        var chars = $("td.genTitle").text(); 
        if (chars.length > limit) {
            var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
            var dots = $("<span class='dots'>...</span>");
            $("td.genTitle").empty()
                .append(visibleArea)
                .append(dots);//append trailing dots to the visibile Area 
        }
		
    });
</script>
</head>
<body style="background:#FFFFFF">
	<div class="iPhoneBrdr">
		<!-- <div class="cstmBtn"><a href="#" title="Download App">Download App</a></div>-->
		<div class="viewAreaHlf viewAreaHlfExtnd">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zeroBrdrTbl">
				<!-- <tr class="whiteBG noBg">
					<td colspan="3" class="dwnldApp hgltRow" align="center">
						<c:if test="${sessionScope.ProductInfo.appDownloadLink ne null && !empty sessionScope.ProductInfo.appDownloadLink}">
							<div class="downloadHdr">	
								<img src="/SSQR/images/dwnldHubcitiApp.jpg"
								alt="download-app" target="_blank" width="138" height="28"
								onerror="this.src = '/SSQR/images/blankImage.gif';" />
								<a href="https://itunes.apple.com/us" class="cstmCursr" title="Download IOS HubCiti App">iOS</a>  
            					<a href="#" class="cstmCursr" title="Download Android HubCiti App">Android</a>
							</div>
						</c:if>
					</td>
				</tr> -->
				
				<tr class="noBg">
					<td align="center" class="rowone">
						<c:choose>
        					<c:when test="${sessionScope.ProductInfo.productImagePath ne null && !empty sessionScope.ProductInfo.productImagePath}">
								<div><img src="${sessionScope.ProductInfo.productImagePath}" width="160" height="160"/></div>
          						<div class="txtarea">${sessionScope.ProductInfo.productName}</div>
        					</c:when>
        					<c:otherwise>
        						<div class="floatL txtareaRemoveMrgn">${sessionScope.ProductInfo.productName}</div>
        					</c:otherwise>
        				</c:choose>
        				<input type="hidden" id="titleName" value="${sessionScope.ProductInfo.productName}" />
					</td>
				</tr>
				<c:if test="${sessionScope.ProductInfo.modelNumber ne null && !empty sessionScope.ProductInfo.modelNumber}">
					<tr>
						<td align="center"><strong>Model Number:</strong>${sessionScope.ProductInfo.modelNumber}</td>
	      			</tr>
      			</c:if>
      			<tr>
        			<td align="center"><span>${sessionScope.ProductInfo.productLongDescription}</span></td>
      			</tr>
				<tr>
        			<td align="center"><span>${sessionScope.ProductInfo.productShortDescription}</span></td>
      			</tr>
      			
      			<c:if test="${sessionScope.ProductInfo.warrantyServiceInformation ne null && !empty sessionScope.ProductInfo.warrantyServiceInformation}">
	      			<tr>
	        			<td align="center"><strong>Warranty:</strong>${sessionScope.ProductInfo.warrantyServiceInformation}</td>
	      			</tr>
      			</c:if>
      			<c:if test="${sessionScope.ProductAttributesList ne null && !empty sessionScope.ProductAttributesList}">
      				<tr>
      					<td align="center" valign="top">
      						<ol>
      							<c:forEach items="${sessionScope.ProductAttributesList}" var="item">
      								<li>${item.prodAttributeName}: ${item.prodDisplayValue} </li>
      							</c:forEach>
      						</ol>
      					</td>
      				</tr>
      			</c:if>
      			<c:if test="${sessionScope.ProductAudioList ne null && !empty sessionScope.ProductAudioList}">
      				<tr>
        				<td align="center"><span class="iphoneTitle">Audio:</span></td>
      				</tr>
      				<tr>
      					<td align="center" valign="top">
      						<ol>
      							<c:forEach items="${sessionScope.ProductAudioList}" var="item1">
      								<li><a href="${item1.fullMediaPath}">${item1.mediaPath}</a></li>
      							</c:forEach>
      						</ol>
      					</td>
      				</tr>
      			</c:if>
      			<c:if test="${sessionScope.ProductVideoList ne null && !empty sessionScope.ProductVideoList}">
      				<tr>
        				<td align="center"><span class="iphoneTitle">Video:</span></td>
      				</tr>
      				<tr>
      					<td align="center" valign="top">
      						<ol>
      							<c:forEach items="${sessionScope.ProductVideoList}" var="item2">	
      								<li><a href="${item2.fullMediaPath}">${item2.mediaPath}</a></li>
      							</c:forEach>
      						</ol>
      					</td>
      				</tr>
      			</c:if>
			</table>
		</div>
	</div>
</body>
</html>
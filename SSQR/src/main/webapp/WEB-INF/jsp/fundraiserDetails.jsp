<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>

<div class="viewAreaiPhn">
	<div class="iPhnCont">
	<c:choose>
		<c:when test="${sessionScope.events.organizationHosting ne null && !empty sessionScope.events.organizationHosting}">
			<h2 class="iPhnTitle">${sessionScope.events.organizationHosting} ${sessionScope.events.hcEventName}</h2>
		</c:when>
		<c:otherwise>
			<h2 class="iPhnTitle">${sessionScope.events.hcEventName}</h2>
		</c:otherwise>
	</c:choose>
	<input type="hidden" id="titleName" value="${sessionScope.events.hcEventName}" />	
		<div class="cont">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="zeroBrdrTbl">
				<tr>
					<td align="center" colspan="4"><c:choose>
							<c:when
								test="${sessionScope.events.eventImagePath ne null && !empty sessionScope.events.eventImagePath}">
								<div class="image">
									<img src="${sessionScope.events.eventImagePath}" width="70"
										height="70" />
								</div>
							</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose></td>
				</tr>
				<tr>
					<c:if
						test="${sessionScope.events.eventDate ne null && !empty sessionScope.events.eventDate}">
						<td width="25%" class="" colspan="4" align="center"><b>Start Date :</b> ${sessionScope.events.eventDate}</td>
						<!-- <td width="34%" colspan="2">${sessionScope.events.eventDate}</td> -->
					</c:if>
				</tr>
				<tr>
					<c:if
						test="${sessionScope.events.eventEDate ne null && !empty sessionScope.events.eventEDate}">
						<td width="25%" class="" colspan="4" align="center"><b>End Date :</b> ${sessionScope.events.eventEDate}</td>
						<!--<td width="34%" colspan="2">${sessionScope.events.eventEDate}</td>-->
					</c:if>
				</tr>
				<tr>
					<td colspan="4"><div class="shrtDesc" align="center">${sessionScope.events.shortDescription}</div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><div class="lngDesc" align="center">${sessionScope.events.longDescription}
						</div></td>
				</tr>
				<c:if test="${sessionScope.events.fundraisingGoal ne null && !empty sessionScope.events.fundraisingGoal}">
					<tr>
						<td width="25%" class="" colspan="4" align="center"><b>Fundraising Goal :</b> ${sessionScope.events.fundraisingGoal}</td>
					</tr>
				</c:if>
				<c:if test="${sessionScope.events.currentLevel ne null && !empty sessionScope.events.currentLevel}">
					<tr>
						<td width="25%" class="" colspan="4" align="center"><b>Current Level :</b> ${sessionScope.events.currentLevel}</td>
					</tr>
				</c:if>
				<c:if test="${sessionScope.events.moreInfoURL ne null && !empty sessionScope.events.moreInfoURL}">
					<tr>
						<td colspan="4" align="center" class="loclbl"><a href="${sessionScope.events.moreInfoURL}" title="${sessionScope.events.moreInfoURL}">More Information</a></td>
					</tr>
				</c:if>
				<c:if test="${sessionScope.events.purchaseProducts ne null && !empty sessionScope.events.purchaseProducts}">
					<tr>
						<td colspan="4" align="center" class="loclbl"><a href="${sessionScope.events.purchaseProducts}" title="${sessionScope.events.purchaseProducts}">Purchase Product</a></td>
					</tr>
				</c:if>					
				<c:if test="${sessionScope.fundraiserEvents ne null && !empty sessionScope.fundraiserEvents}">
					<tr>
						<td colspan="4" class="loclbl">Event Tied Information</td>
					</tr>
					<tr>
						<td colspan="4">
							<ul class="loclist">
								<c:forEach items="${sessionScope.fundraiserEvents}" var="fundraiserEvent">
									<li>
										<c:choose>
											<c:when test="${fundraiserEvent.eventImagePath ne null && !empty fundraiserEvent.eventImagePath}">
												<img width="30" height="30" src="${fundraiserEvent.eventImagePath}"/>												
											</c:when>
											<c:otherwise>												
												<img width="30" height="30"/>
											</c:otherwise>
										</c:choose>
										<span>${fundraiserEvent.hcEventName}</span> ${fundraiserEvent.eventStartDate} , ${fundraiserEvent.eventStartTime}
									</li>
								</c:forEach>								
							</ul>
						</td>
					</tr>
				</c:if>
			</table>
		</div>
	</div>
</div>
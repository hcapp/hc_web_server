<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<head>
<title>${sessionScope.bandevtinfo.bandEvtTitle}</title>

<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit" />
 <script type="text/javascript" src="/SSQR/script/jquery-1.6.2.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		document.title="${sessionScope.bandevtinfo.bandEvtTitle}";	
		//iphone preview qrcode check for link and make entire row clickable;                                                                     
		$('.iphoneGrd tr').click(function(e) {
			var href = $(this).find("a").attr("href");
			if (href) {
				window.open(href);
			}
			e.preventDefault();

		});
		$('.iphoneGrd tr').mouseover(function() {
			var href = $(this).find("a").attr("href");
			if (href) {
				$(this).addClass("cstmCursr");
			}
		});
		
		
		$("#dsplyVenue2").click(function(e) {
	showModal();
	});
	 /*close modal popup on click of x*/
	$(".modal-close").click(function(e) {
		$(".modal-popupWrp").hide();
		$(".modal-popup").slideUp();
		$(".modal-popup").find(".modal-bdy input").val("");
		$(".modal-popup i").removeClass("errDsply");
	});
	
	});
	
	function displayMap()
	{
	
	document.bandform.bandAddress.value="${sessionScope.bandevtinfo.bandAddress}";
	document.bandform.action="showbandmap.htm";
	document.bandform.method="post";
	document.bandform.submit();
	}
	
		function showModal() {
		var bodyHt = $('body').height();
		var setHt = parseInt(bodyHt);
		$(".modal-popupWrp").show();
		$(".modal-popupWrp").height(setHt);
		$(".modal-popup").slideDown('fast');
	}
	
</script>
</head>

<div class="viewAreaiPhn">
	<div class="iPhnCont">
		<h2 class="iPhnTitle">${sessionScope.bandevtinfo.bandEvtTitle}</h2>
<div class="app-view">
  <div class="app-content">
  <form name="bandform" commandName="bandform">
  <input type="hidden" name="bandAddress"/>
    <div class="app-img-view"> 
	
	<!--<h2>${sessionScope.bandevtinfo.bandEvtTitle}</h2>-->
	<c:choose>
				<c:when test="${sessionScope.bandevtinfo.bndEvntImg ne null && !empty sessionScope.bandevtinfo.bndEvntImg}">
					<tr> 
						<td colspan="3" align="center" class="hgltRow">
						
								<img src="${sessionScope.bandevtinfo.bndEvntImg}" onerror="this.src = '/SSQR/images/blankImage.gif';" width="303"  />
							
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="4" align="center">${sessionScope.bandevtinfo.title}</td>						
					</tr>
				</c:otherwise>
			</c:choose>
		
	
	 </div>
    <h2>${sessionScope.bandevtinfo.title}</h2>
    <div class="app-split-view mrgn-btm row app-info">
      <div class="col col-6"><span class="icon"><img src="/SSQR/images/calendar1.png" alt="icon" /></span>${sessionScope.bandevtinfo.startDate}</div>
      <div class="col col-6 rt-algn"><span class="icon"><img src="/SSQR/images/clock.png" alt="icon" /></span>${sessionScope.bandevtinfo.startTime}</div>
    </div>
    <p class="app-short-desc mrgn-btm">${sessionScope.bandevtinfo.shortDesc}</p>
    <div class="app-split-view mrgn-btm row">
      <div class="col col-6">
      <a href="${sessionScope.bandevtinfo.bandSumURL}" target="_blank" class="splitBtn" > ${sessionScope.bandevtinfo.bandName}</a>
      </div>
      <div class="col col-6 rt-algn">
	  <c:choose>
	  <c:when test="${sessionScope.bandevtinfo.bandVenueFlag eq 'true'}">
	  <a href="${sessionScope.bandevtinfo.retSumURL}" target="_blank" class="splitBtn">Venue</a>
	  </c:when>
	  <c:otherwise>
	    <a href="#" id="dsplyVenue2" class="splitBtn">Venue</a>
	  </c:otherwise>
	  </c:choose>
	  
       
      </div>
    </div>
    <p class="app-long-desc">
	
	${sessionScope.bandevtinfo.longDesc}
	</p>
	  </form>
  </div>

</div>
  </div>
</div>
<div class="modal-popupWrp">
    <div class="modal-popup" >
    <div class="modal-hdr"> <a title="close" class="modal-close">×</a>
        <h3>Venue</h3>
      </div>
    <div class="modal-bdy">
        <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
            <td width="25%">Address</td>
            <td width="75%">
                ${sessionScope.bandevtinfo.bandAddress}</td>
          </tr>
          </tbody>
      </table>
      </div>
    <div class="modal-ftr"> </div>
  </div>
  </div>


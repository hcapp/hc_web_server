<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<head>
<title>ScanSee</title>
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
<link rel="stylesheet" type="text/css" href="../styles/style.css"/>
<script type="text/javascript" src="../script/jquery-1.6.2.min.js"></script>
<script src="../script/global.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
		//Get actual content and display as title on mouse hover on it.
	    var actText = $("td.genTitle").text();
		$("td.genTitle").attr('title',actText);
        var limit = 20;// character limit restricted to 20
        var chars = $("td.genTitle").text(); 
        if (chars.length > limit) {
            var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
            var dots = $("<span class='dots'>...</span>");
            $("td.genTitle").empty()
                .append(visibleArea)
                .append(dots);//append trailing dots to the visibile Area 
        }
		
    });
</script>
</head>
<body style="background:#FFFFFF">
	<div class="iPhoneBrdr">
		<div class="infoStrip">
			<ul>
				<li>${sessionScope.Message}</li>
			</ul>
		</div>
	</div>
</body>
</html>		
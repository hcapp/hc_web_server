<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<head>
<title>ScanSee</title>
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit" />
<script type="text/javascript" src="/SSQR/script/jquery-1.6.2.min.js"></script>
<script src="/SSQR/script/global.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//iphone preview qrcode check for link and make entire row clickable;                                                                     
		$('.iphoneGrd tr').click(function(e) {
			var href = $(this).find("a").attr("href");
			if (href) {
				window.open(href);
			}
			e.preventDefault();

		});
		$('.iphoneGrd tr').mouseover(function() {
			var href = $(this).find("a").attr("href");
			if (href) {
				$(this).addClass("cstmCursr");
			}
		});
	});
</script>
</head>
<body class="whiteBG">
	<div class="viewAreaNew iPhoneBrdr">
		<input type="hidden" id="titleName" value="${sessionScope.retailerPageInfoList.retailerName}" />
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="iphoneGrd">
			<!-- Displaying retailer app down link.  
			<tr class="whiteBG noBg cstmCursr">
				<td colspan="3" class="dwnldApp hgltRow" align="center"><c:if
						test="${sessionScope.retailerPageInfoList.appDownloadLink ne null && !empty sessionScope.retailerPageInfoList.appDownloadLink}">
	
						<a href="${sessionScope.retailerPageInfoList.appDownloadLink}"
							title="Download App"> <img src="/SSQR/images/dwnldHubcitiApp.jpg"
							alt="download-app" target="_blank" width="268" height="28"
							onerror="this.src = '/SSQR/images/blankImage.gif';" />
						</a>
	
					</c:if></td>
			</tr>-->
			<!-- Displaying if retailer has ribbenAdImage and ribbenAdurl otherwise retailer name.  -->
			<c:choose>
				<c:when test="${sessionScope.retailerPageInfoList.ribbonAdURL ne null && !empty sessionScope.retailerPageInfoList.ribbonAdURL}">
					<tr> 
						<td colspan="3" align="center" class="hgltRow">
							<a href="${sessionScope.retailerPageInfoList.ribbonAdURL}">
								<img src="${sessionScope.retailerPageInfoList.ribbonAdImagePath}" onerror="this.src = '/SSQR/images/blankImage.gif';" width="303" height="50" />
							 </a>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="4" align="center">${sessionScope.retailerPageInfoList.retailerName}</td>						
					</tr>
				</c:otherwise>
			</c:choose>
	
			<!-- Displaying retailer current special -->
			<c:if test="${sessionScope.retailerPageInfoList.saleFlag ne null}">
				<c:choose>
					<c:when test="${sessionScope.retailerPageInfoList.saleFlag > 0}">
						<tr class="relatv">
							<td width="40">
								<img src="/SSQR/images/curntSpcls.png" width="36" height="36" />
							</td>
							<td class="greyedOut" width="96%">See Current Specials
								<div class="dwnldAppStrip"></div>
							</td>
							<td align="right" valign="top" class="greyedOut">
								<!--<div class="dwnldAppStrip">
									<c:if test="${sessionScope.retailerPageInfoList.appDownloadLink ne null && !empty sessionScope.retailerPageInfoList.appDownloadLink}">
										<a href="${sessionScope.retailerPageInfoList.appDownloadLink}" title="Download App"> 
											<img src="/SSQR/images/downloadAppicon.png" alt="download" />
										</a>
									</c:if>
								</div>-->
							</td>
						</tr>
					</c:when>
				</c:choose>
			</c:if>
	
			<!-- Displaying retailer get directions.  -->
			<c:if test="${sessionScope.retailerPageInfoList.retaileraddress1 ne null && !empty sessionScope.retailerPageInfoList.retaileraddress1}">
				<tr>
					<td width="40">
						<img src="/SSQR/images/FNB_getdirnivn.png"	width="36" height="36" />
					</td>
					<td class="greyedOut" width="96%">Get Directions<span>${sessionScope.retailerPageInfoList.retaileraddress1}</span>
					</td>
					<td align="right" valign="top" class="greyedOut">
						<!--<div class="dwnldAppStrip">
							<c:if test="${sessionScope.retailerPageInfoList.appDownloadLink ne null && !empty sessionScope.retailerPageInfoList.appDownloadLink}">
								<a href="${sessionScope.retailerPageInfoList.appDownloadLink}" title="Download App"> 
									<img src="/SSQR/images/downloadAppicon.png" alt="download" />
								</a>
							</c:if>
						</div>-->
					</td>
				</tr>
			</c:if>
	
			<!-- Displaying retailer contact phone.  -->
			<c:if test="${sessionScope.retailerPageInfoList.contactPhone ne null && !empty sessionScope.retailerPageInfoList.contactPhone}">
				<tr>
					<td width="40">
						<img src="/SSQR/images/phone_icon.png"	alt="callstore" width="34" height="34" />
					</td>
					<td width="96%">Call Store <span>${sessionScope.retailerPageInfoList.contactPhone}</span>
					</td>
					<td><img src="/SSQR/images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
				</tr>
			</c:if>
			
			<!-- Displaying retailer web url.  -->
			<c:if test="${sessionScope.retailerPageInfoList.retailerURL ne null && !empty sessionScope.retailerPageInfoList.retailerURL}">
				<tr>
					<td width="40">
						<img src="/SSQR/images/FNB_browseIcon.png"	alt="browseweb" width="34" height="34" />
					</td>
					<td width="96%">Browse Website<span>${sessionScope.retailerPageInfoList.retailerURL}</span>
					</td>
					<td><a href="${sessionScope.retailerPageInfoList.retailerURL}" title="${sessionScope.retailerPageInfoList.retailerURL}">
						<img src="/SSQR/images/rightArrow.png" alt="Arrow" width="11" height="15" /></a></td>
				</tr>
			</c:if>
			
			<!-- Displaying retailer created page.  -->
			<c:if test="${sessionScope.retCreatedPagelst ne null && !empty sessionScope.retCreatedPagelst}">
				<c:forEach items="${sessionScope.retCreatedPagelst}" var="createdPages">
					<tr>
						<td>
							<img src="${createdPages.pageImage}" width="36" height="36" onerror="this.src = '/SSQR/images/blankImage.gif';" />
						</td>
						<td width="96%"><a href="${createdPages.pageLink}"> ${createdPages.pageTitle} </a></td>
						<td><img src="/SSQR/images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
					</tr>
				</c:forEach>
			</c:if>
		</table>
	</div>
</body>
</html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<head>
<title>ScanSee</title>
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit" />

<script type="text/javascript"
	src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>

<!--<script type="text/javascript">
    $(function() {
		//Get actual content and display as title on mouse hover on it.
	    var actText = $("td.genTitle").text();
		$("td.genTitle").attr('title',actText);
        var limit = 20;// character limit restricted to 20
        var chars = $("td.genTitle").text(); 
        if (chars.length > limit) {
            var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
            var dots = $("<span class='dots'>...</span>");
            $("td.genTitle").empty()
                .append(visibleArea)
                .append(dots);//append trailing dots to the visibile Area 
        }
		
    });
</script>
-->

<script type="text/javascript">
	$(function() {
		//Get actual content and display as title on mouse hover on it.
		var actText = $(".iPhnTitle").text();
		$(".iPhnTitle").attr('title', actText);
		var limit = 35;// character limit restricted to 35
		var chars = $(".iPhnTitle").text();
		if (chars.length > limit) {
			var visibleArea = $("<span> " + chars.substr(0, limit - 1)
					+ "</span>");
			var dots = $("<span class='dots'>...</span>");
			$(".iPhnTitle").empty().append(visibleArea).append(dots);//append trailing dots to the visibile Area 
		}

	});
	
	$( document ).ready(function() {
	    var pageTitle = $('#pageTitle').val();
	    if(null != pageTitle) {
	    	document.title = pageTitle;
	    }	    
	});
</script>

</head>

<!--
<c:if test="${sessionScope.scanSeeFlag ne null && sessionScope.scanSeeFlag == 'N'}">
			<c:if test="${sessionScope.retailerCreatedPageInfo.appDownloadLink ne null && !empty sessionScope.retailerCreatedPageInfo.appDownloadLink}">
				<div class="cstmBtn"><a href="${sessionScope.retailerCreatedPageInfo.appDownloadLink}" title="Download App"><img src="../images/downloadAppicon.png" alt="download" width="15" height="17" />Download App</a></div>
			</c:if>
		</c:if>
-->
<!-- 
<div class="infoStrip">
	<ul>
		<li><img src="${sessionScope.retailerCreatedPageInfo.logo}" /></li>
		<li>${sessionScope.retailerCreatedPageInfo.retailName}</li>
	</ul>
</div>
-->

<body>

	<div class="viewAreaiPhn">
		<div class="iPhnCont">
			<h2 class="iPhnTitle">${sessionScope.retailerCreatedPageInfo.pageTitle}</h2>
			<input type="hidden" value="${sessionScope.retailerCreatedPageInfo.pageTitle}" id="pageTitle"/>
			<div class="cont">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="zeroBrdrTbl">
				<tr>
					<td align="center" colspan="4"><c:choose>
							<c:when test="${sessionScope.retailerCreatedPageInfo.imagePath ne null && !empty sessionScope.retailerCreatedPageInfo.imagePath}">
								<div class="image">
									<img src="${sessionScope.retailerCreatedPageInfo.imagePath}" width="125" height="130" />
								</div>
							</c:when>
						</c:choose>
					</td>
				</tr>
				<tr>
					<td colspan="4" align="center"><div class="shrtDesc">${sessionScope.retailerCreatedPageInfo.shortDescription}</div>
					</td>
				</tr>
				<tr>
					<td colspan="4" align="center"><div class="lngDesc">${sessionScope.retailerCreatedPageInfo.longDescription}
						</div></td>
				</tr>
				<tr>
					<c:if test="${sessionScope.retailerCreatedPageInfo.startDate ne null && !empty sessionScope.retailerCreatedPageInfo.startDate}">
						<td colspan="4" align="center"><b>Start Date : </b>${sessionScope.retailerCreatedPageInfo.startDate}</td>
					</c:if>
				</tr>
				<tr align="center">
					<c:if test="${sessionScope.retailerCreatedPageInfo.endDate ne null && !empty sessionScope.retailerCreatedPageInfo.endDate}">
						<td colspan="4" align="center"><b>End Date : </b>${sessionScope.retailerCreatedPageInfo.endDate}</td>
					</c:if>
				</tr>
				<c:if test="${sessionScope.retailerCreatedPageInfo.rules ne null && !empty sessionScope.retailerCreatedPageInfo.rules}">
					<tr>
						<td class="iphoneLabels"  align="center">Rules:</td>
						<td align="center"><div>${sessionScope.retailerCreatedPageInfo.rules}</div>
						</td>
					</tr>
				</c:if>

				<c:if test="${sessionScope.retailerCreatedPageInfo.termsandConditions ne null && !empty sessionScope.retailerCreatedPageInfo.termsandConditions}">
					<tr>
						<td class="iphoneLabels" align="center">Terms and Conditions:</td>
						<td align="center">
							<div>${sessionScope.retailerCreatedPageInfo.termsandConditions}</div>
						</td>
					</tr>
				</c:if>
			</table>
			</div>
		</div>
	</div>

</body>
</html>
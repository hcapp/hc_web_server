<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<head>
<title>${requestScope.title}</title>

<script type="text/javascript" src="/SSQR/script/jquery-1.6.2.min.js"></script>
<script src="/SSQR/script/global.js" type="text/javascript"></script>

<style type="text/css">
.iPhnNewsTitle {
    background: linear-gradient(to bottom, #ffffff 1%, #eeeeee 38%, #cccccc 100%)
 repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 1px solid #dedede;
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
    font-size: 16px;
    /* height: 35px; */
    /* line-height: 35px; */
    margin: 0;
    text-align: center;
    padding: 10px 0!important;
    line-height: 23px;
}

p {
    margin-bottom: 12px;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		document.title="${requestScope.title}";	
	});
</script>
</head>
<div class="viewAreaiPhn">
	<div class="iPhnCont">
		<h2 class="iPhnNewsTitle">${requestScope.title}</h2>
		<input type="hidden" id="titleName" value="${requestScope.title}" />
		<div class="cont" style="text-align:left">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="zeroBrdrTbl">
				<tr>
				</tr>
				<tr></tr>
				<c:if test="${requestScope.image ne null && !empty requestScope.image }">
				<tr>
					<td align="center" colspan="4">
							<div style="margin-bottom: 10px;">
								<img src="${requestScope.image}" width="240" height="240" />
							</div>
						</td>
				</tr>
				</c:if>
				
							
				<c:if test="${requestScope.videolink ne null && !empty requestScope.videolink}">
				<tr>
					
						<td colspan="4" align="center"><b><a
								style="cursor: pointer; color: blue; font-weight: normal; letter-spacing: normal;"
								href="${requestScope.videolink}">View Video Link</a></b></td>
				</tr>
				</c:if>
				<tr></tr>
				

				<tr>
					<td colspan="4"><div class="shrtDesc">${requestScope.description}</div>
					</td>
				</tr>

			</table>
		</div>
	</div>
</div>
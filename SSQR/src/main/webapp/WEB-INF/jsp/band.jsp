<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<head>
<title>${sessionScope.bandinfo.bandName}</title>

<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit" />
<script type="text/javascript" src="/SSQR/script/jquery-1.6.2.min.js"></script>
<script src="/SSQR/script/global.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		document.title="${sessionScope.bandinfo.bandName}";	
		//iphone preview qrcode check for link and make entire row clickable;                                                                     
		$('.iphoneGrd tr').click(function(e) {
			var href = $(this).find("a").attr("href");
			if (href) {
				window.open(href);
			}
			e.preventDefault();

		});
		$('.iphoneGrd tr').mouseover(function() {
			var href = $(this).find("a").attr("href");
			if (href) {
				$(this).addClass("cstmCursr");
			}
		});
	});
</script>
</head>

<div class="viewAreaiPhn">
	<div class="iPhnCont">
		<h2 class="iPhnTitle">${sessionScope.bandinfo.bandName}</h2>
<div class="app-view">
  <div class="app-content zero-pdng">
    <div class="app-img-view"> <a href="${sessionScope.bandinfo.bannerImgURL}" target="_blank"><img src="${sessionScope.bandinfo.bandBannerImg}" /> </a></div>
    <ul class="app-list-view">
	<c:if test="${sessionScope.bandinfo.phoneNo ne null && !empty sessionScope.bandinfo.phoneNo}">
      <li><a href="tel:+${sessionScope.bandinfo.phoneNo}"><span class="icon"><img src="${sessionScope.bandinfo.phoneImg}" alt="icon" /></span>${sessionScope.bandinfo.phoneNo}</a></li>
	  </c:if>
	  
	  <c:if test="${sessionScope.bandinfo.bandURL ne null && !empty sessionScope.bandinfo.bandURL}">
      <li><a href="${sessionScope.bandinfo.bandURL}" target="_blank"><span class="icon"><img src="${sessionScope.bandinfo.bandURLImgPath}" alt="icon" /></span> ${sessionScope.bandinfo.bandURL}</a></li>
	  </c:if>
	  
	  <c:if test="${sessionScope.bandinfo.bandEmail ne null && !empty sessionScope.bandinfo.bandEmail}">
      <li><a href="mailto:${sessionScope.bandinfo.bandEmail}"><span class="icon"><img src="${sessionScope.bandinfo.mailImg}" alt="icon" /></span> ${sessionScope.bandinfo.bandEmail}</a></li>
	  </c:if>
	  <!-- Displaying band created page.  -->
			<c:if test="${sessionScope.bandpglst ne null && !empty sessionScope.bandpglst}">
				<c:forEach items="${sessionScope.bandpglst}" var="createdPages">
				
				 <li><a href="${createdPages.pageLink}" target="_blank"><span class="icon"><img src="${createdPages.pageImg}" alt="icon" /></span> ${createdPages.pageTitle}</a></li>
					
				</c:forEach>
			</c:if>
	  
	  
	  
      
    </ul>
  </div>
</div>
</div>
</div>


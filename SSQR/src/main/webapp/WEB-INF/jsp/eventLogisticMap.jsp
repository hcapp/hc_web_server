<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<html>
	<head>	
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">		
		<title>Complex icons</title>		
		<style type="text/css">
			html { overflow: hidden; }
			body { overflow: hidden; padding: 0; margin: 0;
			width: 100%; height: 100%; font-family: Trebuchet MS, Trebuchet, Arial, sans-serif; }
			#map { position: absolute; top: 10px; left: 10px; right: 10px; bottom: 15px; overflow: auto; }
			#footer { position: absolute; bottom: 0px; left: 0px; width:100%; height: 12px; overflow: hidden; }
			@media screen and (max-width: 600px) {
			  #map { top:0px; left:0px; width:100%; height:100%;}
			}
			body { background: #f4f4f4;}
			#header { background: #fff; box-shadow: 0 1px 3px #CCC; border: 1px solid #ccc; }
			#header h1 { padding:7px 10px; margin:0; font-size: 28px; }
			#map { border: 1px solid #ccc; box-shadow: 0 1px 3px #CCC; background-color: #DEDCD7;}
			#footer { text-align:center; font-size:9px; color:#606060; }
		</style>

		<script type="text/javascript" src="https://maps.google.com/maps/api/js?v=3"></script>
		<script type="text/javascript" src="../script/jquery-1.6.2.min.js"></script>		
		<script type="text/javascript">
		
			$(document).ready(function() {
	            init();
	        });

			var marker, latlong, map, currentMarker;
			var latitude = ${sessionScope.eventLogisticsLatLongs.eventLogisticLatitude};
			var longitude = ${sessionScope.eventLogisticsLatLongs.eventLogisticLongitude};
			var leftLatitude = ${sessionScope.eventLogisticsLatLongs.eventLogisticTopLeftLat};
			var leftLongitude = ${sessionScope.eventLogisticsLatLongs.eventLogisticTopLeftLong};
			var rightLatitude = ${sessionScope.eventLogisticsLatLongs.eventLogisticBottomRigthLat};
			var rightLongitude = ${sessionScope.eventLogisticsLatLongs.eventLogisticBottomRightLong};
			var myPins = ${sessionScope.Markers};
			var mapTylerImagePath = ${sessionScope.mapTylerImagePath};
			
			var mapBounds = new google.maps.LatLngBounds(
			    new google.maps.LatLng(leftLatitude, leftLongitude),
			    new google.maps.LatLng(rightLatitude, rightLongitude));
			 
			var mapMinZoom = 13;
			var mapMaxZoom = 20;
			
			var maptiler = new google.maps.ImageMapType({
			    getTileUrl: function(coord, zoom) { 
			        var proj = map.getProjection();
			        var z2 = Math.pow(2, zoom);
			        var tileXSize = 256 / z2;
			        var tileYSize = 256 / z2;
			        var tileBounds = new google.maps.LatLngBounds(
			            proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
			            proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
			        );
			        var y = coord.y;
			        var x = coord.x >= 0 ? coord.x : z2 + coord.x
			        if (mapBounds.intersects(tileBounds) && (mapMinZoom <= zoom) && (zoom <= mapMaxZoom)) {
			            return mapTylerImagePath + zoom + "/" + x + "/" + y + ".png";
			        }  else {
			            return "http://www.maptiler.org/img/none.png";
			        }
			    },
			    tileSize: new google.maps.Size(256, 256),
			    isPng: true,
			
			    opacity: 1.0
			});

			function init() {
				
			    var opts = {
			        streetViewControl: false,
			        center: new google.maps.LatLng(latitude, longitude),
			        zoom: 17
			    };
			    
				map = new google.maps.Map(document.getElementById("map"), opts);
				map.setMapTypeId('roadmap');
				map.overlayMapTypes.insertAt(0, maptiler);
				
				setMarkers(map, myPins);
				
				if (navigator.geolocation) {
			        navigator.geolocation.getCurrentPosition(displayAndWatch, locError);
			    } else {
			        alert("Your browser does not support the Geolocation API!");
			    }
				
			}
			
			function displayAndWatch(position) {
		        // set current position
		        setCurrentPosition(position);	             
		        // watch position
		        watchCurrentPosition();
		    }			
			
			function locError(error) {
				alert("To Allow Location Services, go to Settings, General, Reset, Reset Location and Privacy");
			}			
			
			function setMarkers(map, locations) {
				
			  var image = {
			    url: 'images/beachflag.png',
			    size: new google.maps.Size(40, 40),
			    origin: new google.maps.Point(0,0),
			    anchor: new google.maps.Point(0, 40)
			  };
			  
			  var shape = {
			      coords: [1, 1, 1, 40, 40, 40, 40 , 1],
			      type: 'poly',
			  };
			  
			  for (var i = 0; i < locations.length; i++) {
			    var myPins = locations[i];
			    var myLatLng = new google.maps.LatLng(myPins[1], myPins[2]);
				var infowindow = new google.maps.InfoWindow();
			    var marker = new google.maps.Marker({
			        animation: google.maps.Animation.DROP,
					position: myLatLng,
			        map: map,
			        icon: locations[i][3],
			        shape: shape,
			        title: myPins[0],
					zIndex: myPins[4],
			    });
		       google.maps.event.addListener(marker, 'click',  (function(marker, i) {
			        return function() {
			          infowindow.setContent(locations[i][0]);
			          infowindow.open(map, marker);
			        }
			      })(marker, i));
			  }
			}
			
			function setCurrentPosition(pos) {
				var shape = {
			      coords: [1, 1, 1, 40, 40, 40, 40 , 1],
			      type: 'poly',
			    };
				var infowindow = new google.maps.InfoWindow();
			    currentMarker = new google.maps.Marker({
			        animation: google.maps.Animation.DROP,
					position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
			        map: map,
			        icon: '../images/pin.png',
			        shape: shape,
			        title: 'You Are Here',
					zIndex: 1,
			    });
			    map.panTo(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
				google.maps.event.addListener(currentMarker, 'click',  (function(currentMarker) {
			        return function() {
			          infowindow.setContent('You Are Here');
			          infowindow.open(map, currentMarker);
			        }
			      })(currentMarker));
				
			}
			
		    function watchCurrentPosition() {
		        var positionTimer = navigator.geolocation.watchPosition(function (position) {
		            setMarkerPosition(currentMarker, position);
		        });
		    }   
		    
		    function setMarkerPosition(currentMarker, position) {
		    	currentMarker.setPosition(new google.maps.LatLng( position.coords.latitude, position.coords.longitude));
		    }
		    
	</script>
</head>
<body>
	<div id="map"></div>
</body>
</html>
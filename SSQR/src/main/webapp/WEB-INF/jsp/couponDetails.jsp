<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>

<div class="viewAreaiPhn">
	<div class="iPhnCont">
		<h2 class="iPhnTitle">${sessionScope.coupon.couponName}</h2>
		<input type="hidden" id="titleName" value="${sessionScope.coupon.couponName}" />
		<div class="cont">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zeroBrdrTbl">
				<tr>
				</tr>
				<tr>
				<td class="hilite">${sessionScope.coupon.bannerTitle}</td>
				</tr>
				<tr>
					<td align="center" colspan="4"><c:choose>
							<c:when test="${sessionScope.coupon.couponImagePath ne null && !empty sessionScope.coupon.couponImagePath}">
								<div class="image">
									<img src="${sessionScope.coupon.couponImagePath}" width="70" height="70" />
								</div>
							</c:when>
							<c:otherwise>

							</c:otherwise>
						</c:choose></td>
				</tr>
				<tr class="highlite">
					<c:if
						test="${sessionScope.coupon.couponStartDate ne null && !empty sessionScope.coupon.couponStartDate && sessionScope.coupon.couponExpireDate ne null && !empty sessionScope.coupon.couponExpireDate}">
						<td colspan="4" align="center"><b>Valid From </b> ${sessionScope.coupon.couponStartDate} <b>to</b> ${sessionScope.coupon.couponExpireDate}</td>
					</c:if>
				</tr>
				<%-- <tr>
					<c:if
						test="${sessionScope.coupon.couponExpireDate ne null && !empty sessionScope.coupon.couponExpireDate}">
						<td width="25%" colspan="4" align="center"><b>End Date : </b> ${sessionScope.coupon.couponExpireDate}</td>
					</c:if>
				</tr> --%>
				<tr>
					<td colspan="4"><div class="shrtDesc" align="center">${sessionScope.coupon.couponShortDescription}</div>
					</td>
				</tr>
				<tr>
					<td colspan="4"><div class="shrtDesc" align="center">${sessionScope.coupon.couponLongDescription}</div>
					</td>
				</tr>
				<c:if test="${sessionScope.coupon.externalCoupon ne null && sessionScope.coupon.externalCoupon eq true}">
					<c:if test="${sessionScope.coupon.couponURL ne null && !empty sessionScope.coupon.couponURL}">
						<tr>
							<td colspan="4" align="center" class="loclbl"><a href="${sessionScope.coupon.couponURL}" title="${sessionScope.coupon.couponURL}">This is a Web Coupon. Click here to view</a></td>
						</tr>
					</c:if>
				</c:if>
				<!-- <tr>
					<td align="center" colspan="6">
						<input class="locbtn" type="button" value="Location" />
					</td>
				</tr> -->
				
				<tr>
					<td colspan="4" class="loclbl">Location Information</td>
				</tr>
				<tr>
					<td colspan="4">
						<ul class="loclist">
						<c:choose>
							<c:when test="${sessionScope.coupon.locationDetails ne null && !empty sessionScope.coupon.locationDetails}">
								<c:forEach items="${sessionScope.coupon.locationDetails}" var="location">
									<li>
										<c:choose>
											<c:when test="${location.imagepath ne null && !empty location.imagepath}">
												<img width="30" height="30" src="${location.imagepath}"/>												
											</c:when>
											<c:otherwise>												
												<img width="30" height="30"/>
											</c:otherwise>
										</c:choose>
										 ${location.address} , ${location.city} , ${location.state} - ${location.postalCode}
									</li>
								</c:forEach>	
							</c:when>
						</c:choose>
														
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
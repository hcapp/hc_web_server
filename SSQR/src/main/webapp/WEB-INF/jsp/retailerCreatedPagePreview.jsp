<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<head>
<title>ScanSee</title>
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter" />
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit" />

<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>

<!--<script type="text/javascript">
    $(function() {
		//Get actual content and display as title on mouse hover on it.
	    var actText = $("td.genTitle").text();
		$("td.genTitle").attr('title',actText);
        var limit = 20;// character limit restricted to 20
        var chars = $("td.genTitle").text(); 
        if (chars.length > limit) {
            var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
            var dots = $("<span class='dots'>...</span>");
            $("td.genTitle").empty()
                .append(visibleArea)
                .append(dots);//append trailing dots to the visibile Area 
        }
		
    });
</script>
-->

<script type="text/javascript">
	$(function() {
		//Get actual content and display as title on mouse hover on it.
		var actText = $(".iPhnTitle").text();
		$(".iPhnTitle").attr('title', actText);
		var limit = 35;// character limit restricted to 35
		var chars = $(".iPhnTitle").text();
		if (chars.length > limit) {
			var visibleArea = $("<span> " + chars.substr(0, limit - 1)
					+ "</span>");
			var dots = $("<span class='dots'>...</span>");
			$(".iPhnTitle").empty().append(visibleArea).append(dots);//append trailing dots to the visibile Area 
		}

	});
	
	
	$( document ).ready(function() {
	    var pageTitle = $('#pageTitle').val();
	    if(null != pageTitle) {
	    	document.title = pageTitle;
	    }	    
	});
</script>

</head>

<!--
<c:if test="${sessionScope.scanSeeFlag ne null && sessionScope.scanSeeFlag == 'N'}">
			<c:if test="${sessionScope.retailerCreatedPageInfo.appDownloadLink ne null && !empty sessionScope.retailerCreatedPageInfo.appDownloadLink}">
				<div class="cstmBtn"><a href="${sessionScope.retailerCreatedPageInfo.appDownloadLink}" title="Download App"><img src="../images/downloadAppicon.png" alt="download" width="15" height="17" />Download App</a></div>
			</c:if>
		</c:if>
-->
<!-- 
<div class="infoStrip">
	<ul>
		<li><img src="${sessionScope.retailerCreatedPageInfo.logo}" /></li>
		<li>${sessionScope.retailerCreatedPageInfo.retailName}</li>
	</ul>
</div>
-->

<body>
	<div class="viewAreaiPhn">
			<c:choose>
				<c:when test="${sessionScope.retailerCreatedPageInfo.qrType == 'Special Offer Page'}">
					<div class="">
						<h2 class="splIPhnTitle">${sessionScope.retailerCreatedPageInfo.pageTitle}</h2>
						<div class="splCont">
				</c:when>
				<c:otherwise>
					<div class="iPhnCont">
						<h2 class="iPhnTitle">${sessionScope.retailerCreatedPageInfo.pageTitle}</h2>
						<div class="cont">
				</c:otherwise>
			</c:choose>	
			<input type="hidden" value="${sessionScope.retailerCreatedPageInfo.pageTitle}" id="pageTitle"/> 		
			<table width="100%" border="0" cellspacing="0" cellpadding="0"	class="zeroBrdrTbl">
				<tr>
					<td align="center" colspan="4">
						<c:choose>
							<c:when test="${sessionScope.retailerCreatedPageInfo.imagePath ne null && !empty sessionScope.retailerCreatedPageInfo.imagePath}">
								<div class="image">
									<img src="${sessionScope.retailerCreatedPageInfo.imagePath}" width="70" height="70" />
								</div>
							</c:when>
						</c:choose>
					</td>
				</tr>
				<c:if test="${sessionScope.retailerCreatedPageInfo.qrType == 'Special Offer Page'}">
					<tr>
						<c:if test="${sessionScope.retailerCreatedPageInfo.startDate ne null && !empty sessionScope.retailerCreatedPageInfo.startDate}">
							<td colspan="4" align="center" class="splIPhnTitle splIPhnTitleHgt">Starts: ${sessionScope.retailerCreatedPageInfo.startDate}</td>
						</c:if>						
					</tr>
					<tr>
						<td colspan="4">
							<c:if test="${sessionScope.specialRetailLocs ne null && !empty sessionScope.specialRetailLocs}">
								<ul class="loclist">
									<c:forEach items="${sessionScope.specialRetailLocs}" var="specialRetailLoc">
										<li>
											<c:choose>
												<c:when test="${specialRetailLoc.retailerImagePath ne null && !empty specialRetailLoc.retailerImagePath}">
													<img width="30" height="30" src="${specialRetailLoc.retailerImagePath}"/>												
												</c:when>
												<c:otherwise>												
													<img width="30" height="30"/>
												</c:otherwise>
											</c:choose>
											<span>${specialRetailLoc.retailName}</span> ${specialRetailLoc.address1} , ${specialRetailLoc.city} , ${specialRetailLoc.state} - ${specialRetailLoc.postalCode}
										</li>
									</c:forEach>
								</ul>
							</c:if>		
						</td>
					</tr>		
				</c:if>
				<c:if test="${sessionScope.retailerCreatedPageInfo.qrType == 'Anything Page'}">
					<tr>
						<c:if test="${sessionScope.retailerCreatedPageInfo.startDate ne null && !empty sessionScope.retailerCreatedPageInfo.startDate}">
							<td colspan="4" align="center"><b>Start Date : </b>${sessionScope.retailerCreatedPageInfo.startDate}</td>
						</c:if>
					</tr>
					<tr>
						<c:if test="${sessionScope.retailerCreatedPageInfo.endDate ne null && !empty sessionScope.retailerCreatedPageInfo.endDate}">
							<td colspan="4" align="center"><b>End Date : </b>${sessionScope.retailerCreatedPageInfo.endDate}</td>
						</c:if>
					</tr>
				</c:if>
				<tr>
					<td colspan="4" align="center"><div class="shrtDesc">${sessionScope.retailerCreatedPageInfo.shortDescription}</div>
					</td>
				</tr>
				<tr>
					<td colspan="4" align="center"><div class="lngDesc">${sessionScope.retailerCreatedPageInfo.longDescription}
						</div></td>
				</tr>
				<c:if test="${sessionScope.retailerCreatedPageInfo.qrType == 'Special Offer Page'}">
					<tr>
						<c:if test="${sessionScope.retailerCreatedPageInfo.endDate ne null && !empty sessionScope.retailerCreatedPageInfo.endDate}">
							<td colspan="4" align="center">Promotion Ends: ${sessionScope.retailerCreatedPageInfo.endDate}</td>
						</c:if>						
					</tr>
				</c:if>
			</table>
			</div>
		</div>
	</div>
</body>
</html>

<!--
<td align="left" colspan="2"><span>${sessionScope.retailerCreatedPageInfo.shortDescription}</span>
</td>
</tr>
<tr>
	<td align="left" colspan="2"><span>${sessionScope.retailerCreatedPageInfo.longDescription}</span>
	</td>
</tr>
<c:if
	test="${sessionScope.retailerCreatedPageInfo.qrType == 'Special Offer Page'}">

	<c:if
		test="${sessionScope.retailerCreatedPageInfo.startDate ne null && !empty sessionScope.retailerCreatedPageInfo.startDate}">
		<tr>
			<td width="50%" align="left"><strong>Start Date:</strong> <span
				class="strtDt">${sessionScope.retailerCreatedPageInfo.startDate}</span>
			</td>
		</tr>
	</c:if>
	<c:if
		test="${sessionScope.retailerCreatedPageInfo.startTime ne '00:00:00'}">
		<tr>
			<td width="50%" align="left"><strong>Start Time:</strong> <span
				class="strtDt">${sessionScope.retailerCreatedPageInfo.startTime}</span>
			</td>
		</tr>
	</c:if>
	<c:if
		test="${sessionScope.retailerCreatedPageInfo.endDate ne null && !empty sessionScope.retailerCreatedPageInfo.endDate}">
		<tr>
			<td width="50%" align="left"><strong>End Date:</strong> <span
				class="endDt">${sessionScope.retailerCreatedPageInfo.endDate}</span>
			</td>
		</tr>
	</c:if>
	<c:if
		test="${sessionScope.retailerCreatedPageInfo.endTime ne '00:00:00'}">
		<tr>
			<td width="50%" align="left"><strong>End Time:</strong> <span
				class="endDt">${sessionScope.retailerCreatedPageInfo.endTime}</span>
			</td>
		</tr>
	</c:if>
</c:if>
<c:if
	test="${sessionScope.retailerCreatedPageInfo.mediaList ne null && !empty sessionScope.retailerCreatedPageInfo.mediaList}">

	<tr>
		<td colspan="2" align="left"><span class="iphoneTitle">Files:</span>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left">
			<ol>
				<c:forEach items="${sessionScope.retailerCreatedPageInfo.mediaList}"
					var="item">
					<li><a href="${item.filePath}">${item.fileName}</a>
					</li>
				</c:forEach>
			</ol></td>
	</tr>
</c:if>
</table>
</div>
</div>
-->

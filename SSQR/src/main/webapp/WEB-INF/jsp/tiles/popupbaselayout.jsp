<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta name="author" content="Google, Inc.">
<meta content="app-id=${requestScope.IOSAppID}, app-argument=${requestScope.URL}" name="apple-itunes-app"/>
<meta name="google-play-app" content="app-id=${requestScope.AndroidAppID}">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
<link rel="stylesheet" type="text/css" href="../styles/style.css"/>
<link rel="stylesheet" href="../styles/jquery.smartbanner.css" type="text/css" media="screen">
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<script type="text/javascript" src="../script/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../script/jquery.smartbanner.js"></script>
<script src="../script/global.js" type="text/javascript"></script>
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>
<body>
    <script type="text/javascript">
      $.smartbanner();
    </script>
<div><tiles:insertAttribute name="body" /></div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
<title>5</title>

   <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
<style type="text/css">
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
</style>
<!--[if lte IE 6]>
<style type="text/css">
#map {
    height:expression(document.body.clientHeight-35); /* 10+10+15=35 */
    width:expression(document.body.clientWidth-20); /* 10+10=20 */
}
</style>
<![endif]-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var myAddress = [
  ['604 Avenue G, MARBLE FALLS, TX, 78654'],
  ['fdg, Ulysses, KS, 67880'],
  ['604 Avenue G, Marble Falls, TX, 78654'],
  ['canadian, Marble Falls, TX, 78654'],
  ['er, Marble Falls, TX, 75701'],
  ['Corporate address, Marble Falls, TX, 75701'],
  ['1100 Congress Ave, Austin, TX, 78701'],
  ['2266-2598 US-69, Tyler, TX 75701, USA, Tyler, TX, 75701'],
  ['1901 E 51st St, Austin, TX, 78723'],
  ['783 Davis Lane, Rockwall, TX 75032, US, Rockwall, TX, 75032'],
  ['659 Boylston St, Boston, MA, 02116'],
  ['118 King St # 2, Alexandria, VA, 22314'],
  ['1700 W Internatl Speedway #252, Daytona Beach, FL, 32114'],
  ['10 Columbus Cir # 118, New York, NY, 10019'],
  ['66 E Walton St, Chicago, IL, 60611'],
  ['170 Maiden Ln, San Francisco, CA, 94108']
];
var geocoder = new google.maps.Geocoder();
var address = "604 Avenue G, Marble Falls, TX, 78654";

geocoder.geocode({
    'address': address
}, function(results, status) {

    if (status == google.maps.GeocoderStatus.OK) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
        var myLatLng = {
            lat: latitude,
            lng: longitude
        };
        var infowindow = new google.maps.InfoWindow({
            content: address
        });

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: address
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });

    }
});

</script>
</head>
<body>
<div id="map"></div>
</body>
</html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
		<title>Custom Page</title>
		<style type="text/css">
		
			.wrapper {
				background: #fff;
				width: 100%;
			}
			
			.content {
				padding: 20px;
			}
			
			.btn {
				background: #ffffff; /* Old browsers */
				background: -moz-linear-gradient(top, #ffffff 0%, #e5e5e5 100%); /* FF3.6+ */
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ffffff), color-stop(100%, #e5e5e5)); /* Chrome,Safari4+ */
				background: -webkit-linear-gradient(top, #ffffff 0%, #e5e5e5 100%); /* Chrome10+,Safari5.1+ */
				background: -o-linear-gradient(top, #ffffff 0%, #e5e5e5 100%); /* Opera 11.10+ */
				background: -ms-linear-gradient(top, #ffffff 0%, #e5e5e5 100%); /* IE10+ */
				background: linear-gradient(to bottom, #ffffff 0%, #e5e5e5 100%); /* W3C */
				border: 1px solid #dedede;
				/* Safari 3-4, iOS 1-3.2, Android 1.6- */
				-webkit-border-radius: 4px;
				/* Firefox 1-3.6 */
				-moz-border-radius: 4px;
				/* Opera 10.5, IE 9, Safari 5, Chrome, Firefox 4, iOS 4, Android 2.1+ */
				border-radius: 4px;
				color: #4a4a4a;
				cursor:pointer;
				margin: 10px 0;
				padding: 10px;
				width: 100%;
			 filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5', GradientType=0 ); /* IE6-9 */
			 -moz-box-shadow: 1px 2px 3px #444;
				-webkit-box-shadow: 1px 2px 3px #444;
				box-shadow: 1px 2px 3px #444;
				-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#444444')";
				filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#444444');
			}
			
		</style>
	</head>
	<body>
		<div class="wrapper">
			<div class="image-panel">
				<center>
					<a href="${sessionScope.logisticsDetails.eventFilePath}">
						<img src="${sessionScope.logisticsDetails.eventLogisticImgPath}" alt="Event Logistic Image" title="Event Logistic Image" width="60%"/>
					</a>
				</center>
					<label style="font-family: Arial; font-size: 12px"><center>Click On Image To View The Map</center></label>
				
			</div> 
			<div class="content">
			  	<c:forEach items="${sessionScope.logisticsDetails.buttonInfos}" var="buttonInfo">
			  		<input type="button" class="btn" value="${buttonInfo.buttonName}" onclick="location.href='${buttonInfo.buttonLink}'"/>
			  	</c:forEach>
			</div>
		</div>
	</body>
</html>
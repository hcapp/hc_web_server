package common.constatns;

/**
 * @author chandrasekaran_j
 *
 */
public class ApplicationConstants
{

	/**
	 * MethodStart declared as String for logger messages.
	 */
	public static final String METHODSTART = "In side method >>> ";
	/**
	 * MethodEnd declared as String for logger messages.
	 */
	public static final String METHODEND = " Exiting method >>> ";

	/**
	 * for database status.
	 */
	public static final String STATUS = "Status";

	/**
	 * for database error number.
	 */
	public static final String ERRORNUMBER = "ErrorNumber";

	/**
	 * for database Error Message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";

	/**
	 * for database Error Message.
	 */
	public static final String SUCCESS = "SUCCESS";
	/**
	 * for database Error Message
	 */
	public static final String FLAG = "Success";
	/**
	 * Declared USERNAME for first use.
	 */
	public static final String USERNAME = "UserName";
	/**
	 * Declared password for first use.
	 */
	public static final String FACEBOOK = "FaceBookAuthenticatedUser";

	/**
	 * FAILURE declared as String for displaying message in FAILURE.
	 */
	public static final String FAILURE = "FAILURE";

	/**
	 * userID declared as String for UserID text.
	 */
	public static final String USERID = "UserID";
	/**
	 * for empty string.
	 */
	public static final String EMPTYSTR = " ";
	/**
	 * Declared Comma.
	 */
	public static final String COMMA = ",";
	/**
	 * NoRecordsFound declared as String for getting response code.
	 */
	public static final String NORECORDSFOUND = "10005";
	/**
	 * NoRecordsFoundText declared as String for getting response text.
	 */
	public static final String NORECORDSFOUNDTEXT = "No Records Found.";
	/**
	 * NoRetailerFoundText declared as String for getting response text.
	 */
	public static final String NORETAILERFOUNDTEXT = "No Retailer Found.";
	/**
	 * phone pattern expression.
	 */
	public static final String PHONENUM_PATTERN = "\\(\\d{3}\\)\\d{3}-\\d{4}";
	/**
	 * digit expression.
	 */
	public static final String DIGIT = "^(?=.*\\d).{4,60}$";
	
	/**
	 * Declared NOTAPPLICABLE for use.
	 */
	public static final String NOTAPPLICABLE = "NotApplicable";
	/**
	 * after variable.
	 */
	public static final String DATEAFTER = "after";


	/**
	 * SCHEMANAME declared as String for database schema name.
	 */
	//Needs to be renamed
	//WEBSCHEMANAME not in use
	public static final String WEBSCHEMANAME = "dbo";
	
	/**
	 * SCHEMANAME declared as String for database schema name.
	 */
	public static final String APPSCHEMANAME = "Version7";
	
	/**
	 * SCHEMANAME declared for Older version of app without user tracking
	 */
	public static final String APPOLDSCHEMANAME = "Version6";
	
	public static final String SPECIALOFFERPAGE = "SpecialOfferPage";
	
	public static final String ANYTHINGPAGE = "AnythingPage";
	
	public static final String SPECIALOFFERPAGEURL = "/2200.htm";
	
	public static final String ANYTHINGPAGEURL = "/2100.htm";
	
	public static final String TERMSANDCONDITIONS = "<p>ScanSee is not the sponsor of the deal.</p>";
	
	public static final String HUBCITI = "HubCiti";
	
	public static final String HUBCITI_SCHEMA = "HubCitiWeb";
	
	/**
	 * HTTP declared as String constant variable .
	 */
	public static final String HTTP = "http://";
	
	/**
	 * WWW declared as String constant variable .
	 */
	public static final String WWW = "www.";
	
	/**
	 * EXCEPTIONOCCURED declared as string constant variable.
	 */
	public static final String EXCEPTIONOCCURED ="Exception occured in :";

}

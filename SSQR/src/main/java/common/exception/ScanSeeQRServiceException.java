package common.exception;

/**
 * @author chandrasekaran_j
 *
 */
public class ScanSeeQRServiceException extends Exception {

	/**
		 *. 
		 */
	private static final long serialVersionUID = 1L;

	/**
	 * constructor with two arguments.
	 * 
	 * @param message
	 *            the message to be displayed to the user
	 * @param cause
	 *            the cause of the exception
	 */
	public ScanSeeQRServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * constructor with one argument.
	 * 
	 * @param cause
	 *            the cause of the exception
	 */
	public ScanSeeQRServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * no-arg constructor.
	 * 
	 */
	public ScanSeeQRServiceException() {
	}

	/**
	 * Single argument constructor.
	 * 
	 * @param s
	 *            message to be displayed
	 */
	public ScanSeeQRServiceException(String s) {
		super(s);
	}

}

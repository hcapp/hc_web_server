package common.exception;

/**
 * @author chandrasekaran_j
 *
 */
public class ScanSeeQRWebSqlException extends Exception {
	
	/**
	 * .
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * errorCode declared as int.
	 */
	private int errorCode;
	
	
	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}


	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	
	

	/**constructor with two arguments.
	 * @param message the message to be displayed to the user
	 * @param cause the cause of the exception
	 */ 
	public ScanSeeQRWebSqlException(String message, Throwable cause) {
       super(message , cause);
    }
	
	
	
	
	 /**
     * constructor.
     * @param errorMessage .
     * @param errorCode .
     * @param errorCause .
     */
    public ScanSeeQRWebSqlException(String errorMessage , int errorCode , Throwable errorCause) {
    	super(errorMessage, errorCause);
    	this.errorCode = errorCode;
        }
	
	
	 /**constructor with one argument.
     * @param cause the cause of the exception
     */

    public ScanSeeQRWebSqlException(Throwable cause) {
        super(cause);
    }

    /**
     * constructor.
     */
 public ScanSeeQRWebSqlException() {
    }
 
 /**Single argument constructor
  * @param s message to be displayed
  */

    public ScanSeeQRWebSqlException(String s) {
        super(s);
    }
    
   
    /**
     * constructor.
     * @param errorMessage .
     * @param errorCode .
     */
    public ScanSeeQRWebSqlException(String errorMessage , int errorCode) {
    	super(errorMessage);
    	this.errorCode = errorCode;
        }
	
   

}

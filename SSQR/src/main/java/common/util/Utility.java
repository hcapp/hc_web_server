package common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.constatns.ApplicationConstants;

/**
 * @author chandrasekaran_j
 * 
 */
public class Utility {

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(Utility.class);

	/**
	 * variable for checking Debugging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * getUsDateFormat method will convert date format to MM-dd-yyyy.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */
	public static String getUsDateFormat(String enteredDate)
			throws java.text.ParseException {
		final String methodName = "getUsDateFormat";
		// LOG.info(ApplicationConstants.METHODSTART + methodName);

		final DateFormat oldFormatter = new SimpleDateFormat(
				"dd-MM-yyyy HH:mm:ss");
		final DateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

		final Date convertedDate = (Date) oldFormatter.parse(enteredDate);
		final String cDate = formatter.format(convertedDate);
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return cDate;
	}

	/**
	 * getUsDateFormatwithoutTime method will convert date format to MM/dd/yyyy.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */
	public static String getUsDateFormatwithoutTime(String enteredDate)
			throws java.text.ParseException {
		if (null != enteredDate) {
			final String methodName = "getUsDateFormat";
			// LOG.info(ApplicationConstants.METHODSTART + methodName);

			final DateFormat oldFormatter = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			final DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			final Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			final String cDate = formatter.format(convertedDate);
			// LOG.info(ApplicationConstants.METHODEND + methodName);
			return cDate;
		}
		return enteredDate;
	}

	/**
	 * getUsDateFormatwithoutDate method will convert date format to HH:mm:ss.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */
	public static String getUsDateFormatwithoutDate(String enteredDate)
			throws java.text.ParseException {
		if (null != enteredDate) {
			final String methodName = "getUsDateFormat";
			// LOG.info(ApplicationConstants.METHODSTART + methodName);

			final DateFormat oldFormatter = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			final DateFormat formatter = new SimpleDateFormat("HH:mm:ss");

			final Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			final String cTime = formatter.format(convertedDate);
			// LOG.info(ApplicationConstants.METHODEND + methodName);
			return cTime;
		}
		return enteredDate;
	}

	/**
	 * getUsDateFormatwithoutTime method will convert date format to MM/dd/yyyy.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */
	public static String getDateFormat(String enteredDate)
			throws java.text.ParseException {
		if (null != enteredDate) {
			//final String methodName = "getDateFormat";
			// LOG.info(ApplicationConstants.METHODSTART + methodName);

			final DateFormat oldFormatter = new SimpleDateFormat(
					"yyyy-MM-dd");
			final DateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");

			final Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			final String cDate = formatter.format(convertedDate);
			// LOG.info(ApplicationConstants.METHODEND + methodName);
			return cDate;
		}
		return enteredDate;
	}
	
	/**
	 * check empty.
	 * 
	 * @param s
	 *            s
	 * @return return
	 */
	public static String checkEmpty(String s) {

		if (null != s && "".equals(s.trim())) {
			s = null;
		}
		return s;
	}
	
	/**
	 * This method is used to check Empty strings or "".
	 * 
	 * @param arg
	 *            -As parameter
	 * @return arg
	 */
	public static boolean isEmptyOrNullString(String arg) {

		final String methodName = "isEmptyString";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final boolean emptyString;

		if (null == arg || "".equals(arg.trim())) {
			emptyString = true;
		} else {
			emptyString = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emptyString;
	}

}

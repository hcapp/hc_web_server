package common.pojo;

import java.util.ArrayList;

public class Event {
	
	private String hcEventName;
	
	private String eventDate;

	private String longDescription;
	private String shortDescription;
	private String eventImagePath;
	private String eventStartDate;
	private String eventStartTime;

	private String eventEndDate;
	private String eventEndTime;
	
	private String eventEDate;
	
	private String address;
	private String city;
	private String state;
	private String postalCode;
	private String appSiteIDs;
	private Integer appSiteId;
	private String appSiteName;
	private String appSiteImg;
	private String moreInfoURL;
	private String eventTiedIds;
	private String currentLevel;
	private String purchaseProducts;
	private String fundraisingGoal;
	private String organizationHosting;
	private String QRUrl;
	private String IOSAppID;
	private String AndroidAppID;
	private ArrayList<ButtonInfo> buttonInfos;
	private String eventFilePath;
	private String eventLogisticImgPath;
	
	/**
	 * @return the hcEventName
	 */
	public String getHcEventName() {
		return hcEventName;
	}
	/**
	 * @param hcEventName the hcEventName to set
	 */
	public void setHcEventName(String hcEventName) {
		this.hcEventName = hcEventName;
	}
	/**
	 * @return the eventDate
	 */
	public String getEventDate() {
		return eventDate;
	}
	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	/**
	 * @return the longDescription
	 */
	public String getLongDescription() {
		return longDescription;
	}
	/**
	 * @param longDescription the longDescription to set
	 */
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return shortDescription;
	}
	/**
	 * @param shortDescription the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	/**
	 * @return the eventImagePath
	 */
	public String getEventImagePath() {
		return eventImagePath;
	}
	/**
	 * @param eventImagePath the eventImagePath to set
	 */
	public void setEventImagePath(String eventImagePath) {
		this.eventImagePath = eventImagePath;
	}
	/**
	 * @return the eventStartDate
	 */
	public String getEventStartDate() {
		return eventStartDate;
	}
	/**
	 * @param eventStartDate the eventStartDate to set
	 */
	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	/**
	 * @return the eventStartTime
	 */
	public String getEventStartTime() {
		return eventStartTime;
	}
	/**
	 * @param eventStartTime the eventStartTime to set
	 */
	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}
	
	/**
	 * @return the eventEndDate
	 */
	public String getEventEndDate() {
		return eventEndDate;
	}
	/**
	 * @param eventEndDate the eventEndDate to set
	 */
	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}
	/**
	 * @return the eventEndTime
	 */
	public String getEventEndTime() {
		return eventEndTime;
	}
	/**
	 * @param eventEndTime the eventEndTime to set
	 */
	public void setEventEndTime(String eventEndTime) {
		this.eventEndTime = eventEndTime;
	}
	
	/**
	 * @return the eventEDate
	 */
	public String getEventEDate() {
		return eventEDate;
	}
	/**
	 * @param eventEDate the eventEDate to set
	 */
	public void setEventEDate(String eventEDate) {
		this.eventEDate = eventEDate;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	/**
	 * @return the appSiteIDs
	 */
	public String getAppSiteIDs() {
		return appSiteIDs;
	}
	/**
	 * @param appSiteIDs the appSiteIDs to set
	 */
	public void setAppSiteIDs(String appSiteIDs) {
		this.appSiteIDs = appSiteIDs;
	}
	/**
	 * @return the appSiteId
	 */
	public Integer getAppSiteId() {
		return appSiteId;
	}
	/**
	 * @param appSiteId the appSiteId to set
	 */
	public void setAppSiteId(Integer appSiteId) {
		this.appSiteId = appSiteId;
	}
	/**
	 * @return the appSiteName
	 */
	public String getAppSiteName() {
		return appSiteName;
	}
	/**
	 * @param appSiteName the appSiteName to set
	 */
	public void setAppSiteName(String appSiteName) {
		this.appSiteName = appSiteName;
	}
	/**
	 * @return the appSiteImg
	 */
	public String getAppSiteImg() {
		return appSiteImg;
	}
	/**
	 * @param appSiteImg the appSiteImg to set
	 */
	public void setAppSiteImg(String appSiteImg) {
		this.appSiteImg = appSiteImg;
	}
	/**
	 * @return the moreInfoURL
	 */
	public String getMoreInfoURL() {
		return moreInfoURL;
	}
	/**
	 * @param moreInfoURL the moreInfoURL to set
	 */
	public void setMoreInfoURL(String moreInfoURL) {
		this.moreInfoURL = moreInfoURL;
	}
	/**
	 * @return the eventTiedIds
	 */
	public String getEventTiedIds() {
		return eventTiedIds;
	}
	/**
	 * @param eventTiedIds the eventTiedIds to set
	 */
	public void setEventTiedIds(String eventTiedIds) {
		this.eventTiedIds = eventTiedIds;
	}
	/**
	 * @return the currentLevel
	 */
	public String getCurrentLevel() {
		return currentLevel;
	}
	/**
	 * @param currentLevel the currentLevel to set
	 */
	public void setCurrentLevel(String currentLevel) {
		this.currentLevel = currentLevel;
	}
	/**
	 * @return the purchaseProduct
	 */
	public String getPurchaseProducts() {
		return purchaseProducts;
	}
	/**
	 * @param purchaseProduct the purchaseProduct to set
	 */
	public void setPurchaseProducts(String purchaseProducts) {
		this.purchaseProducts = purchaseProducts;
	}
	/**
	 * @return the fundraisingGoal
	 */
	public String getFundraisingGoal() {
		return fundraisingGoal;
	}
	/**
	 * @param fundraisingGoal the fundraisingGoal to set
	 */
	public void setFundraisingGoal(String fundraisingGoal) {
		this.fundraisingGoal = fundraisingGoal;
	}
	/**
	 * @return the organizationHosting
	 */
	public String getOrganizationHosting() {
		return organizationHosting;
	}
	/**
	 * @param organizationHosting the organizationHosting to set
	 */
	public void setOrganizationHosting(String organizationHosting) {
		this.organizationHosting = organizationHosting;
	}
	/**
	 * @return the qRUrl
	 */
	public String getQRUrl() {
		return QRUrl;
	}
	/**
	 * @param qRUrl the qRUrl to set
	 */
	public void setQRUrl(String qRUrl) {
		QRUrl = qRUrl;
	}
	/**
	 * @return the iOSAppID
	 */
	public String getIOSAppID() {
		return IOSAppID;
	}
	/**
	 * @param iOSAppID the iOSAppID to set
	 */
	public void setIOSAppID(String iOSAppID) {
		IOSAppID = iOSAppID;
	}
	/**
	 * @return the androidAppID
	 */
	public String getAndroidAppID() {
		return AndroidAppID;
	}
	/**
	 * @param androidAppID the androidAppID to set
	 */
	public void setAndroidAppID(String androidAppID) {
		AndroidAppID = androidAppID;
	}
	/**
	 * @return the buttonInfos
	 */
	public ArrayList<ButtonInfo> getButtonInfos() {
		return buttonInfos;
	}
	/**
	 * @param buttonInfos the buttonInfos to set
	 */
	public void setButtonInfos(ArrayList<ButtonInfo> buttonInfos) {
		this.buttonInfos = buttonInfos;
	}
	/**
	 * @return the eventFilePath
	 */
	public String getEventFilePath() {
		return eventFilePath;
	}
	/**
	 * @param eventFilePath the eventFilePath to set
	 */
	public void setEventFilePath(String eventFilePath) {
		this.eventFilePath = eventFilePath;
	}
	/**
	 * @return the eventLogisticImgPath
	 */
	public String getEventLogisticImgPath() {
		return eventLogisticImgPath;
	}
	/**
	 * @param eventLogisticImgPath the eventLogisticImgPath to set
	 */
	public void setEventLogisticImgPath(String eventLogisticImgPath) {
		this.eventLogisticImgPath = eventLogisticImgPath;
	}
	
} 
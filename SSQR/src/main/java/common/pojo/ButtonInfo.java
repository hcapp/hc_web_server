/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 *
 */
public class ButtonInfo {
	
	private String buttonName;
	private String buttonLink;
	/**
	 * @return the buttonName
	 */
	public String getButtonName() {
		return buttonName;
	}
	/**
	 * @param buttonName the buttonName to set
	 */
	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}
	/**
	 * @return the buttonLink
	 */
	public String getButtonLink() {
		return buttonLink;
	}
	/**
	 * @param buttonLink the buttonLink to set
	 */
	public void setButtonLink(String buttonLink) {
		this.buttonLink = buttonLink;
	}
}

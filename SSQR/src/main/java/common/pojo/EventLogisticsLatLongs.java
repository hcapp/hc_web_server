/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 *
 */
public class EventLogisticsLatLongs {
	
	private Double eventLogisticLatitude;
	private Double eventLogisticLongitude;
	private Double eventLogisticTopLeftLat;
	private Double eventLogisticTopLeftLong;
	private Double eventLogisticBottomRigthLat;
	private Double eventLogisticBottomRightLong;
	private Integer initialZoomLevel;
	
	/**
	 * @return the eventLogisticLatitude
	 */
	public Double getEventLogisticLatitude() {
		return eventLogisticLatitude;
	}
	/**
	 * @param eventLogisticLatitude the eventLogisticLatitude to set
	 */
	public void setEventLogisticLatitude(Double eventLogisticLatitude) {
		this.eventLogisticLatitude = eventLogisticLatitude;
	}
	/**
	 * @return the eventLogisticLongitude
	 */
	public Double getEventLogisticLongitude() {
		return eventLogisticLongitude;
	}
	/**
	 * @param eventLogisticLongitude the eventLogisticLongitude to set
	 */
	public void setEventLogisticLongitude(Double eventLogisticLongitude) {
		this.eventLogisticLongitude = eventLogisticLongitude;
	}
	/**
	 * @return the eventLogisticTopLeftLat
	 */
	public Double getEventLogisticTopLeftLat() {
		return eventLogisticTopLeftLat;
	}
	/**
	 * @param eventLogisticTopLeftLat the eventLogisticTopLeftLat to set
	 */
	public void setEventLogisticTopLeftLat(Double eventLogisticTopLeftLat) {
		this.eventLogisticTopLeftLat = eventLogisticTopLeftLat;
	}
	/**
	 * @return the eventLogisticTopLeftLong
	 */
	public Double getEventLogisticTopLeftLong() {
		return eventLogisticTopLeftLong;
	}
	/**
	 * @param eventLogisticTopLeftLong the eventLogisticTopLeftLong to set
	 */
	public void setEventLogisticTopLeftLong(Double eventLogisticTopLeftLong) {
		this.eventLogisticTopLeftLong = eventLogisticTopLeftLong;
	}
	/**
	 * @return the eventLogisticBottomRigthLat
	 */
	public Double getEventLogisticBottomRigthLat() {
		return eventLogisticBottomRigthLat;
	}
	/**
	 * @param eventLogisticBottomRigthLat the eventLogisticBottomRigthLat to set
	 */
	public void setEventLogisticBottomRigthLat(Double eventLogisticBottomRigthLat) {
		this.eventLogisticBottomRigthLat = eventLogisticBottomRigthLat;
	}
	/**
	 * @return the eventLogisticBottomRightLong
	 */
	public Double getEventLogisticBottomRightLong() {
		return eventLogisticBottomRightLong;
	}
	/**
	 * @param eventLogisticBottomRightLong the eventLogisticBottomRightLong to set
	 */
	public void setEventLogisticBottomRightLong(Double eventLogisticBottomRightLong) {
		this.eventLogisticBottomRightLong = eventLogisticBottomRightLong;
	}
	/**
	 * @return the initialZoomLevel
	 */
	public Integer getInitialZoomLevel() {
		return initialZoomLevel;
	}
	/**
	 * @param initialZoomLevel the initialZoomLevel to set
	 */
	public void setInitialZoomLevel(Integer initialZoomLevel) {
		this.initialZoomLevel = initialZoomLevel;
	}

}

package common.pojo;

import java.util.List;

/**
 * @author chandrasekaran_j
 */
public class RetailerCreatedPage
{

	/**
	 * pageID.
	 */
	private String pageID;
	/**
	 * pageTitle.
	 */
	private String pageTitle;
	/**
	 * pageDescription.
	 */
	private String pageDescription;
	/**
	 * shortDescription.
	 */
	private String shortDescription;
	/**
	 * longDescription.
	 */
	private String longDescription;
	/**
	 * imageName.
	 */
	private String imageName;
	/**
	 * imagePath.
	 */
	private String imagePath;
	/**
	 * mediaType.
	 */
	private String mediaType;
	/**
	 * mediaPath.
	 */
	private String mediaPath;
	/**
	 * externalFlag.
	 */
	private String externalFlag;
	/**
	 * retailID.
	 */
	private String retailID;
	/**
	 * startDate.
	 */
	private String startDate;
	/**
	 * endDate.
	 */
	private String endDate;
	/**
	 * expired.
	 */
	private String expired;
	/**
	 * logo.
	 */
	private String logo;
	/**
	 * retailName.
	 */
	private String retailName;
	/**
	 * mediaList.
	 */
	private List<Media> mediaList;

	/**
	 * retailName.
	 */
	private String qrType;
	/**
	 * appDownloadLink.
	 */
	private String appDownloadLink;
	/**
	 * startTime.
	 */
	private String startTime;
	/**
	 * endTime.
	 */
	private String endTime;

	private String rules;

	private String termsandConditions;

	private String externalLinkUrl;

	private boolean externalLinkUrlFlag;

	private String QRUrl;
	private String IOSAppID;
	private String AndroidAppID;
	/**
	 * @return the startTime
	 */
	public String getStartTime()
	{
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime()
	{
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}

	/**
	 * @return the appDownloadLink
	 */
	public String getAppDownloadLink()
	{
		return appDownloadLink;
	}

	/**
	 * @param appDownloadLink
	 *            the appDownloadLink to set
	 */
	public void setAppDownloadLink(String appDownloadLink)
	{
		this.appDownloadLink = appDownloadLink;
	}

	/**
	 * .
	 * 
	 * @return the qrType
	 */
	public String getQrType()
	{
		return qrType;
	}

	/**
	 * .
	 * 
	 * @param qrType
	 *            the qrType to set
	 */
	public void setQrType(String qrType)
	{
		this.qrType = qrType;
	}

	/**
	 * .
	 * 
	 * @return the retailName
	 */
	public String getRetailName()
	{
		return retailName;
	}

	/**
	 * .
	 * 
	 * @param retailName
	 *            the retailName to set
	 */
	public void setRetailName(String retailName)
	{
		this.retailName = retailName;
	}

	/**
	 * .
	 * 
	 * @return the logo
	 */
	public String getLogo()
	{
		return logo;
	}

	/**
	 * .
	 * 
	 * @param logo
	 *            the logo to set
	 */
	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	/**
	 * .
	 * 
	 * @return the pageID
	 */
	public String getPageID()
	{
		return pageID;
	}

	/**
	 * .
	 * 
	 * @param pageID
	 *            the pageID to set
	 */
	public void setPageID(String pageID)
	{
		this.pageID = pageID;
	}

	/**
	 * .
	 * 
	 * @return the pageTitle
	 */
	public String getPageTitle()
	{
		return pageTitle;
	}

	/**
	 * .
	 * 
	 * @param pageTitle
	 *            the pageTitle to set
	 */
	public void setPageTitle(String pageTitle)
	{
		this.pageTitle = pageTitle;
	}

	/**
	 * .
	 * 
	 * @return the pageDescription
	 */
	public String getPageDescription()
	{
		return pageDescription;
	}

	/**
	 * .
	 * 
	 * @param pageDescription
	 *            the pageDescription to set
	 */
	public void setPageDescription(String pageDescription)
	{
		this.pageDescription = pageDescription;
	}

	/**
	 * .
	 * 
	 * @return the shortDescription
	 */
	public String getShortDescription()
	{
		return shortDescription;
	}

	/**
	 * .
	 * 
	 * @param shortDescription
	 *            the shortDescription to set
	 */
	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	/**
	 * .
	 * 
	 * @return the longDescription
	 */
	public String getLongDescription()
	{
		return longDescription;
	}

	/**
	 * .
	 * 
	 * @param longDescription
	 *            the longDescription to set
	 */
	public void setLongDescription(String longDescription)
	{
		this.longDescription = longDescription;
	}

	/**
	 * .
	 * 
	 * @return the imageName
	 */
	public String getImageName()
	{
		return imageName;
	}

	/**
	 * .
	 * 
	 * @param imageName
	 *            the imageName to set
	 */
	public void setImageName(String imageName)
	{
		this.imageName = imageName;
	}

	/**
	 * .
	 * 
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * .
	 * 
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		this.imagePath = imagePath;
	}

	/**
	 * .
	 * 
	 * @return the mediaType
	 */
	public String getMediaType()
	{
		return mediaType;
	}

	/**
	 * .
	 * 
	 * @param mediaType
	 *            the mediaType to set
	 */
	public void setMediaType(String mediaType)
	{
		this.mediaType = mediaType;
	}

	/**
	 * .
	 * 
	 * @return the mediaPath
	 */
	public String getMediaPath()
	{
		return mediaPath;
	}

	/**
	 * .
	 * 
	 * @param mediaPath
	 *            the mediaPath to set
	 */
	public void setMediaPath(String mediaPath)
	{
		this.mediaPath = mediaPath;
	}

	/**
	 * .
	 * 
	 * @return the externalFlag
	 */
	public String getExternalFlag()
	{
		return externalFlag;
	}

	/**
	 * .
	 * 
	 * @param externalFlag
	 *            the externalFlag to set
	 */
	public void setExternalFlag(String externalFlag)
	{
		this.externalFlag = externalFlag;
	}

	/**
	 * .
	 * 
	 * @return the retailID
	 */
	public String getRetailID()
	{
		return retailID;
	}

	/**
	 * .
	 * 
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(String retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * .
	 * 
	 * @return the startDate.
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * .
	 * 
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	/**
	 * .
	 * 
	 * @return the endDate
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * .
	 * 
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	/**
	 * .
	 * 
	 * @return the expired
	 */
	public String getExpired()
	{
		return expired;
	}

	/**
	 * .
	 * 
	 * @param expired
	 *            the expired to set
	 */
	public void setExpired(String expired)
	{
		this.expired = expired;
	}

	/**
	 * .
	 * 
	 * @return the mediaList
	 */
	public List<Media> getMediaList()
	{
		return mediaList;
	}

	/**
	 * .
	 * 
	 * @param mediaList
	 *            the mediaList to set
	 */
	public void setMediaList(List<Media> mediaList)
	{
		this.mediaList = mediaList;
	}

	public String getRules()
	{
		return rules;
	}

	public void setRules(String rules)
	{
		this.rules = rules;
	}

	public String getTermsandConditions()
	{
		return termsandConditions;
	}

	public void setTermsandConditions(String termsandConditions)
	{
		this.termsandConditions = termsandConditions;
	}

	public String getExternalLinkUrl()
	{
		return externalLinkUrl;
	}

	public void setExternalLinkUrl(String externalLinkUrl)
	{
		this.externalLinkUrl = externalLinkUrl;
	}

	public boolean isExternalLinkUrlFlag()
	{
		return externalLinkUrlFlag;
	}

	public void setExternalLinkUrlFlag(boolean externalLinkUrlFlag)
	{
		this.externalLinkUrlFlag = externalLinkUrlFlag;
	}

	/**
	 * @return the qRUrl
	 */
	public String getQRUrl() {
		return QRUrl;
	}

	/**
	 * @param qRUrl the qRUrl to set
	 */
	public void setQRUrl(String qRUrl) {
		QRUrl = qRUrl;
	}

	/**
	 * @return the iOSAppID
	 */
	public String getIOSAppID() {
		return IOSAppID;
	}

	/**
	 * @param iOSAppID the iOSAppID to set
	 */
	public void setIOSAppID(String iOSAppID) {
		IOSAppID = iOSAppID;
	}

	/**
	 * @return the androidAppID
	 */
	public String getAndroidAppID() {
		return AndroidAppID;
	}

	/**
	 * @param androidAppID the androidAppID to set
	 */
	public void setAndroidAppID(String androidAppID) {
		AndroidAppID = androidAppID;
	}

}

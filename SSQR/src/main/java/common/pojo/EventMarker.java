/**
 * 
 */
package common.pojo;

/**
 * @author sangeetha.ts
 *
 */
public class EventMarker {
	
	private String evtMarkerName;
	private Double latitude;
	private Double logitude;
	private String eventImageName;
	/**
	 * @return the evtMarkerName
	 */
	public String getEvtMarkerName() {
		return evtMarkerName;
	}
	/**
	 * @param evtMarkerName the evtMarkerName to set
	 */
	public void setEvtMarkerName(String evtMarkerName) {
		this.evtMarkerName = evtMarkerName;
	}
	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the logitude
	 */
	public Double getLogitude() {
		return logitude;
	}
	/**
	 * @param logitude the logitude to set
	 */
	public void setLogitude(Double logitude) {
		this.logitude = logitude;
	}
	/**
	 * @return the eventImageName
	 */
	public String getEventImageName() {
		return eventImageName;
	}
	/**
	 * @param eventImageName the eventImageName to set
	 */
	public void setEventImageName(String eventImageName) {
		this.eventImageName = eventImageName;
	}
}

package common.pojo;
/**
 * The Item class implements an application Rss News.
 * 
 * @author Kumar D
 */

public class Item {
	private int id;
	private String title;
	private String adCopy;
	private String videoLink;

	private String section;
	private String classification;

	private String image;
	private String imagePath;
	
	private String publishedDate;
	private String pubStrtDate;
	private String pubTime;

	private String link;
	private String feedType;
	private String date;
	private String shortDesc;
	
	private String description;
	private String androidAppID;
	private String iOSAppID;
	private String qrURL;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the adCopy
	 */
	public String getAdCopy() {
		return adCopy;
	}

	/**
	 * @param adCopy the adCopy to set
	 */
	public void setAdCopy(String adCopy) {
		this.adCopy = adCopy;
	}

	/**
	 * @return the videoLink
	 */
	public String getVideoLink() {
		return videoLink;
	}

	/**
	 * @param videoLink the videoLink to set
	 */
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}

	/**
	 * @return the section
	 */
	public String getSection() {
		return section;
	}

	/**
	 * @param section the section to set
	 */
	public void setSection(String section) {
		this.section = section;
	}

	/**
	 * @return the classification
	 */
	public String getClassification() {
		return classification;
	}

	/**
	 * @param classification the classification to set
	 */
	public void setClassification(String classification) {
		this.classification = classification;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}


	/**
	 * @return the publishedDate
	 */
	public String getPublishedDate() {
		return publishedDate;
	}

	/**
	 * @param publishedDate the publishedDate to set
	 */
	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	/**
	 * @return the pubStrtDate
	 */
	public String getPubStrtDate() {
		return pubStrtDate;
	}

	/**
	 * @param pubStrtDate the pubStrtDate to set
	 */
	public void setPubStrtDate(String pubStrtDate) {
		this.pubStrtDate = pubStrtDate;
	}

	/**
	 * @return the pubTime
	 */
	public String getPubTime() {
		return pubTime;
	}

	/**
	 * @param pubTime the pubTime to set
	 */
	public void setPubTime(String pubTime) {
		this.pubTime = pubTime;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the feedType
	 */
	public String getFeedType() {
		return feedType;
	}

	/**
	 * @param feedType the feedType to set
	 */
	public void setFeedType(String feedType) {
		this.feedType = feedType;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the shortDesc
	 */
	public String getShortDesc() {
		return shortDesc;
	}

	/**
	 * @param shortDesc the shortDesc to set
	 */
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the androidAppID
	 */
	public String getAndroidAppID() {
		return androidAppID;
	}

	/**
	 * @param androidAppID the androidAppID to set
	 */
	public void setAndroidAppID(String androidAppID) {
		this.androidAppID = androidAppID;
	}

	/**
	 * @return the iOSAppID
	 */
	public String getiOSAppID() {
		return iOSAppID;
	}

	/**
	 * @param iOSAppID the iOSAppID to set
	 */
	public void setiOSAppID(String iOSAppID) {
		this.iOSAppID = iOSAppID;
	}

	/**
	 * @return the qrURL
	 */
	public String getQrURL() {
		return qrURL;
	}

	/**
	 * @param qrURL the qrURL to set
	 */
	public void setQrURL(String qrURL) {
		this.qrURL = qrURL;
	}



}

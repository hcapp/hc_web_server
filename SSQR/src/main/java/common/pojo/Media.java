package common.pojo;

/**
 * @author chandrasekaran_j
 *
 */
public class Media
{
	/**
	 * filename.
	 */
	private String fileName;
	
	/**
	 * filePath.
	 */
	private String filePath;

	/**.
	 * @return the fileName
	 */
	public String getFileName()
	{
		return fileName;
	}

	/**.
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	/**.
	 * @return the filePath
	 */
	public String getFilePath()
	{
		return filePath;
	}

	/**.
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
	}
}

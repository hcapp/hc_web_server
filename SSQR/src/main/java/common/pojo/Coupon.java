/**
 * 
 */
package common.pojo;

import java.util.ArrayList;

/**
 * @author sangeetha.ts
 *
 */
public class Coupon {
	
	private String couponName;
	private String couponShortDescription;
	private String couponLongDescription;
	private String couponStartDate;
	private String couponExpireDate;
	private String couponImagePath;
	private String couponURL;
	private Boolean externalCoupon;
	private String QRUrl;
	private String iOSAppID;
	private String androidAppID;
	private String bannerTitle;
	
	private String address;
	private String city;
	private String state;
	private String postalCode;
	private String imagepath;
	
	private ArrayList<Coupon> locationDetails;
	/**
	 * @return the couponName
	 */
	public String getCouponName() {
		return couponName;
	}
	/**
	 * @param couponName the couponName to set
	 */
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	/**
	 * @return the couponShortDescription
	 */
	public String getCouponShortDescription() {
		return couponShortDescription;
	}
	/**
	 * @param couponShortDescription the couponShortDescription to set
	 */
	public void setCouponShortDescription(String couponShortDescription) {
		this.couponShortDescription = couponShortDescription;
	}
	/**
	 * @return the couponStartDate
	 */
	public String getCouponStartDate() {
		return couponStartDate;
	}
	/**
	 * @param couponStartDate the couponStartDate to set
	 */
	public void setCouponStartDate(String couponStartDate) {
		this.couponStartDate = couponStartDate;
	}
	/**
	 * @return the couponExpireDate
	 */
	public String getCouponExpireDate() {
		return couponExpireDate;
	}
	/**
	 * @param couponExpireDate the couponExpireDate to set
	 */
	public void setCouponExpireDate(String couponExpireDate) {
		this.couponExpireDate = couponExpireDate;
	}
	/**
	 * @return the couponImagePath
	 */
	public String getCouponImagePath() {
		return couponImagePath;
	}
	/**
	 * @param couponImagePath the couponImagePath to set
	 */
	public void setCouponImagePath(String couponImagePath) {
		this.couponImagePath = couponImagePath;
	}
	/**
	 * @return the qRUrl
	 */
	public String getQRUrl() {
		return QRUrl;
	}
	/**
	 * @param qRUrl the qRUrl to set
	 */
	public void setQRUrl(String qRUrl) {
		QRUrl = qRUrl;
	}
	/**
	 * @return the iOSAppID
	 */
	public String getiOSAppID() {
		return iOSAppID;
	}
	/**
	 * @param iOSAppID the iOSAppID to set
	 */
	public void setiOSAppID(String iOSAppID) {
		this.iOSAppID = iOSAppID;
	}
	/**
	 * @return the androidAppID
	 */
	public String getAndroidAppID() {
		return androidAppID;
	}
	/**
	 * @param androidAppID the androidAppID to set
	 */
	public void setAndroidAppID(String androidAppID) {
		this.androidAppID = androidAppID;
	}
	/**
	 * @return the couponURL
	 */
	public String getCouponURL() {
		return couponURL;
	}
	/**
	 * @param couponURL the couponURL to set
	 */
	public void setCouponURL(String couponURL) {
		this.couponURL = couponURL;
	}
	/**
	 * @return the externalCoupon
	 */
	public Boolean getExternalCoupon() {
		return externalCoupon;
	}
	/**
	 * @param externalCoupon the externalCoupon to set
	 */
	public void setExternalCoupon(Boolean externalCoupon) {
		this.externalCoupon = externalCoupon;
	}
	/**
	 * @return the couponLongDescription
	 */
	public String getCouponLongDescription() {
		return couponLongDescription;
	}
	/**
	 * @param couponLongDescription the couponLongDescription to set
	 */
	public void setCouponLongDescription(String couponLongDescription) {
		this.couponLongDescription = couponLongDescription;
	}
	public String getBannerTitle() {
		return bannerTitle;
	}
	public void setBannerTitle(String bannerTitle) {
		this.bannerTitle = bannerTitle;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getImagepath() {
		return imagepath;
	}
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	public ArrayList<Coupon> getLocationDetails() {
		return locationDetails;
	}
	public void setLocationDetails(ArrayList<Coupon> locationDetails) {
		this.locationDetails = locationDetails;
	}

}

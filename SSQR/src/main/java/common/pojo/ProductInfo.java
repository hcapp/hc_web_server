package common.pojo;

/**
 * @author chandrasekaran_j
 *
 */
public class ProductInfo {
	/**
	 * productID.
	 */
	private String productID;
	/**
	 * productName.
	 */
	private String productName;
	/**
	 * productShortDescription.
	 */
	private String productShortDescription;
	/**
	 * productLongDescription.
	 */
	private String productLongDescription;
	/**
	 * productImagePath.
	 */
	private String productImagePath;
	/**
	 * warrantyServiceInformation.
	 */
	private String warrantyServiceInformation;
	/**
	 * modelNumber.
	 */
	private String modelNumber;
	/**
	 * appDownloadLink.
	 */
    private String appDownloadLink;
    
    private String QRUrl;
	private String IOSAppID;
	private String AndroidAppID;
	
	
	/**
	 * @return the appDownloadLink
	 */
	public String getAppDownloadLink() {
		return appDownloadLink;
	}
	/**
	 * @param appDownloadLink the appDownloadLink to set
	 */
	public void setAppDownloadLink(String appDownloadLink) {
		this.appDownloadLink = appDownloadLink;
	}
	/**
	 * @return the modelNumber
	 */
	public String getModelNumber() {
		return modelNumber;
	}
	/**
	 * @param modelNumber the modelNumber to set
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	/**
	 * @return the productID
	 */
	public String getProductID() {
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(String productID) {
		this.productID = productID;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the productShortDescription
	 */
	public String getProductShortDescription() {
		return productShortDescription;
	}
	/**
	 * @param productShortDescription the productShortDescription to set
	 */
	public void setProductShortDescription(String productShortDescription) {
		this.productShortDescription = productShortDescription;
	}
	/**
	 * @return the productLongDescription
	 */
	public String getProductLongDescription() {
		return productLongDescription;
	}
	/**
	 * @param productLongDescription the productLongDescription to set
	 */
	public void setProductLongDescription(String productLongDescription) {
		this.productLongDescription = productLongDescription;
	}
	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath() {
		return productImagePath;
	}
	/**
	 * @param productImagePath the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath) {
		this.productImagePath = productImagePath;
	}
	/**
	 * @return the warrantyServiceInformation
	 */
	public String getWarrantyServiceInformation() {
		return warrantyServiceInformation;
	}
	/**
	 * @param warrantyServiceInformation the warrantyServiceInformation to set
	 */
	public void setWarrantyServiceInformation(String warrantyServiceInformation) {
		this.warrantyServiceInformation = warrantyServiceInformation;
	}
	/**
	 * @return the qRUrl
	 */
	public String getQRUrl() {
		return QRUrl;
	}
	/**
	 * @param qRUrl the qRUrl to set
	 */
	public void setQRUrl(String qRUrl) {
		QRUrl = qRUrl;
	}
	/**
	 * @return the iOSAppID
	 */
	public String getIOSAppID() {
		return IOSAppID;
	}
	/**
	 * @param iOSAppID the iOSAppID to set
	 */
	public void setIOSAppID(String iOSAppID) {
		IOSAppID = iOSAppID;
	}
	/**
	 * @return the androidAppID
	 */
	public String getAndroidAppID() {
		return AndroidAppID;
	}
	/**
	 * @param androidAppID the androidAppID to set
	 */
	public void setAndroidAppID(String androidAppID) {
		AndroidAppID = androidAppID;
	}
	
}

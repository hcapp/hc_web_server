package common.pojo;
/**
 * This POJO is used to fetch appsite marker details.
 * @author shyamsundara_hm
 *
 */
public class AppsiteMarkers {

	private String markerName;
	private String markerImage;
	private String markerLatitude;
	private String markerLongitude;
	private String anythingPageURL;

	public String getMarkerName() {
		return markerName;
	}

	public void setMarkerName(String markerName) {
		this.markerName = markerName;
	}

	public String getMarkerImage() {
		return markerImage;
	}

	public void setMarkerImage(String markerImage) {
		this.markerImage = markerImage;
	}

	public String getMarkerLatitude() {
		return markerLatitude;
	}

	public void setMarkerLatitude(String markerLatitude) {
		this.markerLatitude = markerLatitude;
	}

	public String getMarkerLongitude() {
		return markerLongitude;
	}

	public void setMarkerLongitude(String markerLongitude) {
		this.markerLongitude = markerLongitude;
	}

	public String getAnythingPageURL() {
		return anythingPageURL;
	}

	public void setAnythingPageURL(String anythingPageURL) {
		this.anythingPageURL = anythingPageURL;
	}

}

package common.pojo;

/**
 * @author chandrasekaran_j
 *
 */
public class ProductAttributes {
	/**
	 * prodAttributesID.
	 */
	private String prodAttributesID;
	/**
	 * prodAttributeName.
	 */
	private String prodAttributeName;
	/**
	 * prodDisplayValue.
	 */
	private String prodDisplayValue;
	
	/**
	 * @return the prodAttributesID
	 */
	public String getProdAttributesID() {
		return prodAttributesID;
	}
	/**
	 * @param prodAttributesID the prodAttributesID to set
	 */
	public void setProdAttributesID(String prodAttributesID) {
		this.prodAttributesID = prodAttributesID;
	}
	/**
	 * @return the prodAttributeName
	 */
	public String getProdAttributeName() {
		return prodAttributeName;
	}
	/**
	 * @param prodAttributeName the prodAttributeName to set
	 */
	public void setProdAttributeName(String prodAttributeName) {
		this.prodAttributeName = prodAttributeName;
	}
	/**
	 * @return the prodDisplayValue
	 */
	public String getProdDisplayValue() {
		return prodDisplayValue;
	}
	/**
	 * @param prodDisplayValue the prodDisplayValue to set
	 */
	public void setProdDisplayValue(String prodDisplayValue) {
		this.prodDisplayValue = prodDisplayValue;
	}
}

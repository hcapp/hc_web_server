package common.pojo;

public class Band {

	
	
	private String bndEvntImg;
	private String startDate;
	private String endDate;
	private String startTime;
	private String shortDesc;
	private String longDesc;
	private String title;
	private String bandName;
	private Boolean bandVenueFlag;
	private Boolean bandFlag;
	private String endTime;
	private String bandBannerImg;
	private String bandURL;
	private String bandURLImgPath;
	private String phoneImg;
	private String phoneNo;
	private String mailImg;
	private String bandEmail;
	private Boolean isShowFlag;
	private String bandSumURL;
	private String retSumURL;
	private String bandEvtTitle;
	private String pageTitle;
	private String pageImg;
	private String pageLink;
	private String androidAppID;
	private String iOSAppID;
	private String qrURL;
	private String bandAddress;
	private String bannerImgURL;
	
	public String getBndEvntImg() {
		return bndEvntImg;
	}
	public void setBndEvntImg(String bndEvntImg) {
		this.bndEvntImg = bndEvntImg;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getShortDesc() {
		return shortDesc;
	}
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	public String getLongDesc() {
		return longDesc;
	}
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBandName() {
		return bandName;
	}
	public void setBandName(String bandName) {
		this.bandName = bandName;
	}
	
	public Boolean getBandVenueFlag() {
		return bandVenueFlag;
	}
	public void setBandVenueFlag(Boolean bandVenueFlag) {
		this.bandVenueFlag = bandVenueFlag;
	}
	public Boolean getBandFlag() {
		return bandFlag;
	}
	public void setBandFlag(Boolean bandFlag) {
		this.bandFlag = bandFlag;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getBandBannerImg() {
		return bandBannerImg;
	}
	public void setBandBannerImg(String bandBannerImg) {
		this.bandBannerImg = bandBannerImg;
	}
	public String getBandURL() {
		return bandURL;
	}
	public void setBandURL(String bandURL) {
		this.bandURL = bandURL;
	}
	public String getBandURLImgPath() {
		return bandURLImgPath;
	}
	public void setBandURLImgPath(String bandURLImgPath) {
		this.bandURLImgPath = bandURLImgPath;
	}
	public String getPhoneImg() {
		return phoneImg;
	}
	public void setPhoneImg(String phoneImg) {
		this.phoneImg = phoneImg;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getMailImg() {
		return mailImg;
	}
	public void setMailImg(String mailImg) {
		this.mailImg = mailImg;
	}
	public String getBandEmail() {
		return bandEmail;
	}
	public void setBandEmail(String bandEmail) {
		this.bandEmail = bandEmail;
	}
	public Boolean getIsShowFlag() {
		return isShowFlag;
	}
	public void setIsShowFlag(Boolean isShowFlag) {
		this.isShowFlag = isShowFlag;
	}
	public String getBandSumURL() {
		return bandSumURL;
	}
	public void setBandSumURL(String bandSumURL) {
		this.bandSumURL = bandSumURL;
	}
	public String getRetSumURL() {
		return retSumURL;
	}
	public void setRetSumURL(String retSumURL) {
		this.retSumURL = retSumURL;
	}
	public String getBandEvtTitle() {
		return bandEvtTitle;
	}
	public void setBandEvtTitle(String bandEvtTitle) {
		this.bandEvtTitle = bandEvtTitle;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	public String getPageImg() {
		return pageImg;
	}
	public void setPageImg(String pageImg) {
		this.pageImg = pageImg;
	}
	public String getPageLink() {
		return pageLink;
	}
	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}
	public String getAndroidAppID() {
		return androidAppID;
	}
	public void setAndroidAppID(String androidAppID) {
		this.androidAppID = androidAppID;
	}
	public String getiOSAppID() {
		return iOSAppID;
	}
	public void setiOSAppID(String iOSAppID) {
		this.iOSAppID = iOSAppID;
	}
	public String getQrURL() {
		return qrURL;
	}
	public void setQrURL(String qrURL) {
		this.qrURL = qrURL;
	}
	public String getBandAddress() {
		return bandAddress;
	}
	public void setBandAddress(String bandAddress) {
		this.bandAddress = bandAddress;
	}
	public String getBannerImgURL() {
		return bannerImgURL;
	}
	public void setBannerImgURL(String bannerImgURL) {
		this.bannerImgURL = bannerImgURL;
	}
	
}

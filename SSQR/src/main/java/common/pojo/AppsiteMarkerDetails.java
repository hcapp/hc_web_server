package common.pojo;

import java.util.List;

/**
 * This POJO is used
 * 
 * @author shyamsundara_hm
 * 
 */
public class AppsiteMarkerDetails {

	List<AppsiteMarkers> appsiteMarkerLst;

	private String appsiteMapImagePath;
	private Double appsiteLogisticLatitude;
	private Double appsiteLogisticLongitude;
	private Double appsiteLogisticTopLeftLat;
	private Double appsiteLogisticTopLeftLong;
	private Double appsiteLogisticBottomRightLat;
	private Double appsiteLogisticBottomRightLong;
	private Integer initialZoomLevel;

	public List<AppsiteMarkers> getAppsiteMarkerLst() {
		return appsiteMarkerLst;
	}

	public void setAppsiteMarkerLst(List<AppsiteMarkers> appsiteMarkerLst) {
		this.appsiteMarkerLst = appsiteMarkerLst;
	}

	public String getAppsiteMapImagePath() {
		return appsiteMapImagePath;
	}

	public void setAppsiteMapImagePath(String appsiteMapImagePath) {
		this.appsiteMapImagePath = appsiteMapImagePath;
	}

	public Double getAppsiteLogisticLatitude() {
		return appsiteLogisticLatitude;
	}

	public void setAppsiteLogisticLatitude(Double appsiteLogisticLatitude) {
		this.appsiteLogisticLatitude = appsiteLogisticLatitude;
	}

	public Double getAppsiteLogisticLongitude() {
		return appsiteLogisticLongitude;
	}

	public void setAppsiteLogisticLongitude(Double appsiteLogisticLongitude) {
		this.appsiteLogisticLongitude = appsiteLogisticLongitude;
	}

	public Double getAppsiteLogisticTopLeftLat() {
		return appsiteLogisticTopLeftLat;
	}

	public void setAppsiteLogisticTopLeftLat(Double appsiteLogisticTopLeftLat) {
		this.appsiteLogisticTopLeftLat = appsiteLogisticTopLeftLat;
	}

	public Double getAppsiteLogisticTopLeftLong() {
		return appsiteLogisticTopLeftLong;
	}

	public void setAppsiteLogisticTopLeftLong(Double appsiteLogisticTopLeftLong) {
		this.appsiteLogisticTopLeftLong = appsiteLogisticTopLeftLong;
	}

	public Double getAppsiteLogisticBottomRightLat() {
		return appsiteLogisticBottomRightLat;
	}

	public void setAppsiteLogisticBottomRightLat(
			Double appsiteLogisticBottomRightLat) {
		this.appsiteLogisticBottomRightLat = appsiteLogisticBottomRightLat;
	}

	public Double getAppsiteLogisticBottomRightLong() {
		return appsiteLogisticBottomRightLong;
	}

	public void setAppsiteLogisticBottomRightLong(
			Double appsiteLogisticBottomRightLong) {
		this.appsiteLogisticBottomRightLong = appsiteLogisticBottomRightLong;
	}

	/**
	 * @return the initialZoomLevel
	 */
	public Integer getInitialZoomLevel() {
		return initialZoomLevel;
	}

	/**
	 * @param initialZoomLevel the initialZoomLevel to set
	 */
	public void setInitialZoomLevel(Integer initialZoomLevel) {
		this.initialZoomLevel = initialZoomLevel;
	}

}

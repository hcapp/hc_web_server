package common.pojo;

/**
 * @author chandrasekaran_j
 *
 */
public class ProductMedia {
	/**
	 * productMediaID.
	 */
	private String productMediaID;
	/**
	 * mediaPath.
	 */
	private String mediaPath;
	/**
	 * productMediaType.
	 */
	private String productMediaType;
	/**
	 * fullMediaPath.
	 */
	private String fullMediaPath;
	
	/**
	 * @return the productMediaID
	 */
	public String getProductMediaID() {
		return productMediaID;
	}
	/**
	 * @param productMediaID the productMediaID to set
	 */
	public void setProductMediaID(String productMediaID) {
		this.productMediaID = productMediaID;
	}
	/**
	 * @return the mediaPath
	 */
	public String getMediaPath() {
		return mediaPath;
	}
	/**
	 * @param mediaPath the mediaPath to set
	 */
	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}
	/**
	 * @return the productMediaType
	 */
	public String getProductMediaType() {
		return productMediaType;
	}
	/**
	 * @param productMediaType the productMediaType to set
	 */
	public void setProductMediaType(String productMediaType) {
		this.productMediaType = productMediaType;
	}
	/**
	 * @return the fullMediaPath
	 */
	public String getFullMediaPath() {
		return fullMediaPath;
	}
	/**
	 * @param fullMediaPath the fullMediaPath to set
	 */
	public void setFullMediaPath(String fullMediaPath) {
		this.fullMediaPath = fullMediaPath;
	}
}

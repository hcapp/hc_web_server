package com.scansee.qr.dao;

import java.util.ArrayList;

import common.exception.ScanSeeQRWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.AppsiteMarkerDetails;
import common.pojo.Band;
import common.pojo.Coupon;
import common.pojo.Event;
import common.pojo.EventMarkerDetails;
import common.pojo.Item;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfo;
import common.pojo.ProductMedia;
import common.pojo.RetailerCreatedPage;
import common.pojo.RetailerPage;

/**
 * @author chandrasekaran_j
 * 
 */
public interface QRDao {

	/**
	 * get retailer web page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param pageId
	 *            pageId.
	 * @return RetailerPage ArrayList
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	RetailerPage getRetailerPageInfoList(Integer hubCitiID, int retailerId, int retLocationId) throws ScanSeeQRWebSqlException;

	/**
	 * get retailer created web page info and offer page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param retailLocationid
	 *            retailLocationid
	 * @param pageId
	 *            pageId
	 * @return RetailerCreatedPage RetailerCreatedPage
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	RetailerCreatedPage getRetailerLandingPage(Integer hubcitiId, Integer retailerId, Integer retailLocationid, Integer pageId, Integer mmID,
			Integer scanTypID, String pageType, String source) throws ScanSeeQRWebSqlException;

	/**
	 * get product details info.
	 * 
	 * @param productId
	 *            productId
	 * @return ProductInfo ProductInfo
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	ProductInfo getProductInfo(Integer hubcitiId, int productId) throws ScanSeeQRWebSqlException;

	/**
	 * get product attributes info.
	 * 
	 * @param productId
	 *            productId
	 * @return ArrayList ProductAttributes
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	ArrayList<ProductAttributes> getProductAttributesList(int productId) throws ScanSeeQRWebSqlException;

	/**
	 * get product media info.
	 * 
	 * @param productId
	 *            productId
	 * @return ArrayList ProductMedia
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	ArrayList<ProductMedia> getProductMediaList(int productId) throws ScanSeeQRWebSqlException;

	/**
	 * get retailer web page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param pageId
	 *            pageId.
	 * @return RetailerPage ArrayList
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	ArrayList<RetailerPage> getRetailerCretedPages(Integer hubcitiId, Integer retailerId, Integer retlocation) throws ScanSeeQRWebSqlException;

	/**
	 * DAO method to get Giveaway page details
	 * 
	 * @param retailerId
	 *            as a parameter.
	 * @param pageId
	 *            as a parameter.
	 * @param mainMenuID
	 *            as a parameter.
	 * @param scanTypeID
	 *            as a parameter.
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException.
	 * @return RetailerCreatedPage retailerCreatedPage.
	 */
	RetailerCreatedPage getGiveawayPageInfo(int retailerId, int pageId, int mainMenuID, int scanTypeID) throws ScanSeeQRWebSqlException;

	/**
	 * DAO method to get Application configuration for App Download link.
	 * 
	 * @param configType
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException.
	 * @return appConfigurationList .
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeQRWebSqlException;

	/**
	 * DAO method to get event details.
	 * 
	 * @param eventId
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException.
	 * @return Event .
	 */
	public Event getEventDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException;

	/**
	 * DAO method to get fundraiser details.
	 * 
	 * @param fundraiserId
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException.
	 * @return Event .
	 */
	public Event getFundraiserDetails(Integer fundraiserId, Integer hubcitiId) throws ScanSeeQRWebSqlException;

	public ArrayList<Event> getEventAppSites(Integer eventId) throws ScanSeeQRWebSqlException;

	public ArrayList<Event> getFundraiserEvents(Integer fundraiserId) throws ScanSeeQRWebSqlException;

	/**
	 * DAO method to get event Logistics details.
	 * 
	 * @param eventId
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException.
	 * @return Event .
	 */
	public Event getEventLogistics(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException;

	public ArrayList<RetailerPage> getSpecialOfferRetLocs(Integer retailId, Integer pageId) throws ScanSeeQRWebSqlException;

	public EventMarkerDetails getEventLogisticsMap(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException;

	public RetailerPage getHotdealInfo(Integer hotdealId, Integer hubcitiId) throws ScanSeeQRWebSqlException;

	public ArrayList<Event> getHotdealLocs(Integer hotdealId) throws ScanSeeQRWebSqlException;

	public Coupon getCouponDetails(Integer couponID, Integer hubcitiId) throws ScanSeeQRWebSqlException;

	/**
	 * Below method is used to fetch marker points for appsite.
	 * 
	 * @param logisticId
	 * @param hubCitiId
	 * @return AppsiteMarkerDetails
	 * @throws ScanSeeQRWebSqlException
	 */
	public AppsiteMarkerDetails showAppsiteLogisticPage(Integer logisticId, Integer hubCitiId) throws ScanSeeQRWebSqlException;

	public Band showBandEventDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException;

	public Band showBandDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException;

	public ArrayList<Band> fetchBandAnythingpglst(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException;
	
	/**
	 * DAO method to get News detail.
	 * 
	 * @param iNewsId
	 * @throws ScanSeeQRWebSqlException
	 *           
	 * @return News Detail .
	 */
	Item getNewsDetail(Integer iNewsId)throws ScanSeeQRWebSqlException;
}

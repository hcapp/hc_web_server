package com.scansee.qr.dao;

import java.util.ArrayList;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeQRWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.AppsiteMarkerDetails;
import common.pojo.AppsiteMarkers;
import common.pojo.Band;
import common.pojo.ButtonInfo;
import common.pojo.Coupon;
import common.pojo.Event;
import common.pojo.EventMarker;
import common.pojo.EventMarkerDetails;
import common.pojo.Item;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfo;
import common.pojo.ProductMedia;
import common.pojo.RetailerCreatedPage;
import common.pojo.RetailerPage;

/**
 * @author chandrasekaran_j
 */
public class QRDaoImpl implements QRDao {

	/**
	 * error message.
	 */
	public static final String ERRORMESSAGEVAR = "ErrorMessage";

	/**
	 * Input value.
	 */
	public static final String PRODUCTIDVAR = "ProductID";

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(QRDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * get retailer web page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param pageId
	 *            pageId.
	 * @return RetailerPage ArrayList
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RetailerPage getRetailerPageInfoList(Integer hubCitiID, int retailerId, int retlocationId) throws ScanSeeQRWebSqlException {

		LOG.info("Inside QRDaoImpl : getRetailerPageInfoList ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<RetailerPage> retailerPageInfoList = null;
		RetailerPage page = null;
		String androidAppID = null;
		String iOSAppID = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcDisplayRetailLocationInfo");
			simpleJdbcCall.returningResultSet("listRetailerPageInfo", new BeanPropertyRowMapper<RetailerPage>(RetailerPage.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("HubCitiID", hubCitiID);
			objSearchParameter.addValue("RetailID", retailerId);
			objSearchParameter.addValue("RetailLocationID", retlocationId);

			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			retailerPageInfoList = (ArrayList) resultFromProcedure.get("listRetailerPageInfo");
			if (null != retailerPageInfoList && !retailerPageInfoList.isEmpty()) {
				page = retailerPageInfoList.get(0);
				iOSAppID = (String) resultFromProcedure.get("IOSAppID");
				androidAppID = (String) resultFromProcedure.get("AndroidAppID");
				page.setAndroidAppID(androidAppID);
				page.setIOSAppID(iOSAppID);
			}

		} catch (Exception exception) {
			LOG.error("Inside QRDaoImpl : getRetailerPageInfoList : " + exception.getMessage());
			throw new ScanSeeQRWebSqlException(exception.getMessage(), exception.getCause());
		}
		return page;
	}

	/**
	 * get retailer created web page info and offer page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param retailLocationid
	 *            retailLocationid
	 * @param pageId
	 *            pageId
	 * @return RetailerCreatedPage RetailerCreatedPage
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public RetailerCreatedPage getRetailerLandingPage(Integer hubcitiId, Integer retailerId, Integer retailLocationid, Integer pageId, Integer mmID,
			Integer scanTypID, String pageType, String source) throws ScanSeeQRWebSqlException {
		final String methodName = "getRetailerLandingPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		final Integer responseFromProc = null;
		ArrayList<RetailerCreatedPage> retailerPageInfoList = null;
		Boolean externalLinkUrlFlag;
		String externalLinkUrl = null;
		String androidAppID = null;
		String iOSAppID = null;

		RetailerCreatedPage pageDetail = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("retailerCreatedPageInfo", new BeanPropertyRowMapper<RetailerCreatedPage>(RetailerCreatedPage.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();

			// New Schema is used if request is from HubCiti, else ScanSee
			// schema
			if (null != source && ApplicationConstants.HUBCITI.equalsIgnoreCase(source)) {
				simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			} else {
				simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMANAME);
			}
			if (null != pageType && pageType.equals(ApplicationConstants.ANYTHINGPAGE)) {
				// New SP is used if request is from HubCiti, else ScanSee SP is
				// used
				if (null != source && ApplicationConstants.HUBCITI.equalsIgnoreCase(source)) {
					simpleJdbcCall.withProcedureName("usp_HcQRRetailerCreatedPagesDetails");
					objSearchParameter.addValue("HubCitiID", hubcitiId);
				} else {
					simpleJdbcCall.withProcedureName("usp_QRRetailerCreatedPagesDetails");
				}
			} else if (null != pageType && pageType.equals(ApplicationConstants.SPECIALOFFERPAGE)) {
				// New SP is used if request is from HubCiti, else ScanSee SP is
				// used
				if (null != source && ApplicationConstants.HUBCITI.equalsIgnoreCase(source)) {
					simpleJdbcCall.withProcedureName("usp_HcRetailerLocationSpecialOffersDetails");
					objSearchParameter.addValue("HubCitiID", hubcitiId);
				} else {
					simpleJdbcCall.withProcedureName("usp_RetailerLocationSpecialOffersDetails");
				}
			}

			objSearchParameter.addValue("RetailID", retailerId);
			objSearchParameter.addValue("RetailLocationID", retailLocationid);
			objSearchParameter.addValue("PageID", pageId);
			objSearchParameter.addValue("MainMenuID", mmID);
			objSearchParameter.addValue("ScanTypeID", scanTypID);
			/*
			 * // For user tracking if (mmID != null && scanTypID != null) {
			 * objSearchParameter.addValue("MainMenuID", mmID);
			 * objSearchParameter.addValue("ScanTypeID", scanTypID); }
			 */
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get(ERRORMESSAGEVAR);
				if (null == errorMsg) {
					externalLinkUrlFlag = (Boolean) resultFromProcedure.get("ExternalLinkFlag");
					if (externalLinkUrlFlag) {
						pageDetail = new RetailerCreatedPage();
						externalLinkUrl = (String) resultFromProcedure.get("ExternalLink");
						pageDetail.setExternalLinkUrlFlag(externalLinkUrlFlag);
						pageDetail.setExternalLinkUrl(externalLinkUrl);
					} else {
						retailerPageInfoList = (ArrayList<RetailerCreatedPage>) resultFromProcedure.get("retailerCreatedPageInfo");
						if (null != retailerPageInfoList && !retailerPageInfoList.isEmpty()) {

							pageDetail = retailerPageInfoList.get(0);
						}

					}

					iOSAppID = (String) resultFromProcedure.get("IOSAppID");
					androidAppID = (String) resultFromProcedure.get("AndroidAppID");
					pageDetail.setAndroidAppID(androidAppID);
					pageDetail.setIOSAppID(iOSAppID);

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : usp_QRRetailerCreatedPagesDetails  : errorNumber  : " + errorNum + "errorMessage is : "
							+ errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
		} catch (Exception exception) {
			LOG.error("Inside QRDaoImpl : getRetailerPageInfoList : " + exception.getMessage());
			throw new ScanSeeQRWebSqlException(exception.getMessage(), exception.getCause());
		}

		return pageDetail;
	}

	/**
	 * get product details info.
	 * 
	 * @param productId
	 *            productId
	 * @return ProductInfo ProductInfo
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	public ProductInfo getProductInfo(Integer hubcitiId, int productId) throws ScanSeeQRWebSqlException {
		final String methodName = "getProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		final Integer responseFromProc = null;
		ArrayList<ProductInfo> productPageInfoList = null;

		ProductInfo productPageInfo = null;
		String androidAppID = null;
		String iOSAppID = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcProductDetails");
			simpleJdbcCall.returningResultSet("productPageInfo", new BeanPropertyRowMapper<ProductInfo>(ProductInfo.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();

			objSearchParameter.addValue("HubCitiID", hubcitiId);
			objSearchParameter.addValue(PRODUCTIDVAR, productId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get(ERRORMESSAGEVAR);
				if (null == errorMsg) {
					productPageInfoList = (ArrayList<ProductInfo>) resultFromProcedure.get("productPageInfo");
					if (null != productPageInfoList && !productPageInfoList.isEmpty()) {
						productPageInfo = productPageInfoList.get(0);
						iOSAppID = (String) resultFromProcedure.get("IOSAppID");
						androidAppID = (String) resultFromProcedure.get("AndroidAppID");
						productPageInfo.setAndroidAppID(androidAppID);
						productPageInfo.setIOSAppID(iOSAppID);
					}

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside QRDaoImpl : usp_WebProductDetails  : errorNumber  : " + errorNum + "errorMessage is : " + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
		} catch (Exception exception) {
			LOG.error("Inside QRDaoImpl : getProductInfo : " + exception.getMessage());
			throw new ScanSeeQRWebSqlException(exception.getMessage(), exception.getCause());
		}

		return productPageInfo;
	}

	/**
	 * get product attributes info.
	 * 
	 * @param productId
	 *            productId
	 * @return ArrayList ProductAttributes
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	public ArrayList<ProductAttributes> getProductAttributesList(int productId) throws ScanSeeQRWebSqlException {
		final String methodName = "getProductAttributesList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		final Integer responseFromProc = null;
		ArrayList<ProductAttributes> productAttributesList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebSupplierAttributeDisplay");
			simpleJdbcCall.returningResultSet("productAttributesInfo", new BeanPropertyRowMapper<ProductAttributes>(ProductAttributes.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();

			objSearchParameter.addValue(PRODUCTIDVAR, productId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get(ERRORMESSAGEVAR);
				if (null == errorMsg) {
					productAttributesList = (ArrayList<ProductAttributes>) resultFromProcedure.get("productAttributesInfo");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside QRDaoImpl : usp_WebSupplierAttributeDisplay  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
		} catch (Exception exception) {
			LOG.error("Inside QRDaoImpl : getProductAttributesList : " + exception.getMessage());
			throw new ScanSeeQRWebSqlException(exception.getMessage(), exception.getCause());
		}

		return productAttributesList;
	}

	/**
	 * get product media info.
	 * 
	 * @param productId
	 *            productId
	 * @return ArrayList ProductMedia
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	public ArrayList<ProductMedia> getProductMediaList(int productId) throws ScanSeeQRWebSqlException {
		final String methodName = "getProductMediaList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		final Integer responseFromProc = null;
		ArrayList<ProductMedia> productMediaList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductMedia");
			simpleJdbcCall.returningResultSet("productMediaInfo", new BeanPropertyRowMapper<ProductMedia>(ProductMedia.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();

			objSearchParameter.addValue(PRODUCTIDVAR, productId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get(ERRORMESSAGEVAR);
				if (null == errorMsg) {
					productMediaList = (ArrayList<ProductMedia>) resultFromProcedure.get("productMediaInfo");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside QRDaoImpl : usp_WebDisplayProductMedia  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
		} catch (Exception exception) {
			LOG.error("Inside QRDaoImpl : getProductMediaList : " + exception.getMessage());
			throw new ScanSeeQRWebSqlException(exception.getMessage(), exception.getCause());
		}

		return productMediaList;
	}

	/**
	 * get retailer web page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param pageId
	 *            pageId.
	 * @return RetailerPage ArrayList
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException
	 */
	public ArrayList<RetailerPage> getRetailerCretedPages(Integer hubcitiId, Integer retailerId, Integer retlocationId)
			throws ScanSeeQRWebSqlException {

		LOG.info("Inside QRDaoImpl : getRetailerCretedPages ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<RetailerPage> retCreatedPageList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcQRDisplayRetailerCreatedPages");
			simpleJdbcCall.returningResultSet("listRetailerPageInfo", new BeanPropertyRowMapper<RetailerPage>(RetailerPage.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("HubCitiID", hubcitiId);
			objSearchParameter.addValue("RetailID", retailerId);
			objSearchParameter.addValue("RetailLocationID", retlocationId);

			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			retCreatedPageList = (ArrayList) resultFromProcedure.get("listRetailerPageInfo");

		} catch (Exception exception) {
			LOG.error("Inside QRDaoImpl : getRetailerCretedPages : " + exception.getMessage());
			throw new ScanSeeQRWebSqlException(exception.getMessage(), exception.getCause());
		}
		return retCreatedPageList;
	}

	/**
	 * DAO method to get Giveaway page details
	 * 
	 * @param retailerId
	 *            as a parameter.
	 * @param pageId
	 *            as a parameter.
	 * @param mainMenuID
	 *            as a parameter.
	 * @param scanTypeID
	 *            as a parameter.
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException.
	 * @return RetailerCreatedPage retailerCreatedPage.
	 */
	public RetailerCreatedPage getGiveawayPageInfo(int retailerId, int pageId, int mainMenuID, int scanTypeID) throws ScanSeeQRWebSqlException {
		final String methodName = "getGiveawayPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		final Integer responseFromProc = null;
		ArrayList<RetailerCreatedPage> retailerPageInfoList = null;

		RetailerCreatedPage pageDetail = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GiveAwaypagedetails");
			simpleJdbcCall.returningResultSet("retailerCreatedPageInfo", new BeanPropertyRowMapper<RetailerCreatedPage>(RetailerCreatedPage.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();

			objSearchParameter.addValue("RetailID", retailerId);
			objSearchParameter.addValue("QRRetailerCustomPageID", pageId);
			objSearchParameter.addValue("MainMenuID", mainMenuID);
			objSearchParameter.addValue("ScanTypeID", scanTypeID);

			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get(ERRORMESSAGEVAR);
				if (null == errorMsg) {
					retailerPageInfoList = (ArrayList<RetailerCreatedPage>) resultFromProcedure.get("retailerCreatedPageInfo");
					if (null != retailerPageInfoList && !retailerPageInfoList.isEmpty()) {

						pageDetail = retailerPageInfoList.get(0);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside QRDaoImpl : usp_GiveAwaypagedetails  : errorNumber  : " + errorNum + "errorMessage is : " + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
		} catch (Exception exception) {
			LOG.error("Inside QRDaoImpl : getGiveawayPageInfo : " + exception.getMessage());
			throw new ScanSeeQRWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageDetail;
	}

	/**
	 * DAO method to get Application configuration for App Download link.
	 * 
	 * @param configType
	 * @throws ScanSeeQRWebSqlException
	 *             ScanSeeQRWebSqlException.
	 * @return appConfigurationList .
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeQRWebSqlException {
		final String methodName = "getAppConfig";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside QRDaoImpl : getAppConfig : Error occurred in usp_GetScreenContent Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeQRWebSqlException(errorMsg);
			}
			LOG.info("usp_GetScreenContent is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getAppConfig : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return appConfigurationList;
	}

	@SuppressWarnings("unchecked")
	public Event getEventDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException {
		final String methodName = "getEventDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> events = null;
		Event event = null;
		String iOSAppID = null;
		String androidAppID = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiRetailerEventDetails");
			simpleJdbcCall.returningResultSet("eventDetails", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("HcEventID", eventId);
			queryParams.addValue("HubCitiId", hubcitiId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					events = (ArrayList<Event>) resultFromProcedure.get("eventDetails");
					if (null != events && !events.isEmpty()) {
						event = events.get(0);
						iOSAppID = (String) resultFromProcedure.get("IOSAppID");
						androidAppID = (String) resultFromProcedure.get("AndroidAppID");
						event.setAndroidAppID(androidAppID);
						event.setIOSAppID(iOSAppID);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getEventDetails : Error occurred in usp_WebHcHubCitiEventDetails Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiEventDetails is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getEventDetails : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	@SuppressWarnings("unchecked")
	public Event getFundraiserDetails(Integer fundraiserId, Integer hubcitiId) throws ScanSeeQRWebSqlException {
		final String methodName = "getFundraiserDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> events = null;
		Event event = null;
		String androidAppID = null;
		String iOSAppID = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiRetailerFundraisingEventDetails");
			simpleJdbcCall.returningResultSet("eventDetails", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("HcFundraisingID", fundraiserId);
			queryParams.addValue("HubCitiId", hubcitiId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					events = (ArrayList<Event>) resultFromProcedure.get("eventDetails");
					if (null != events && !events.isEmpty()) {
						event = events.get(0);
					}

					iOSAppID = (String) resultFromProcedure.get("IOSAppID");
					androidAppID = (String) resultFromProcedure.get("AndroidAppID");
					event.setAndroidAppID(androidAppID);
					event.setIOSAppID(iOSAppID);

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getFundraiserDetails : Error occurred in usp_WebHcHubCitiFundraisingEventDetails Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiFundraisingEventDetails is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getFundraiserDetails : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Event> getEventAppSites(Integer eventId) throws ScanSeeQRWebSqlException {
		final String methodName = "getEventAppSites";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> events = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiEventAppSiteDisplay");
			simpleJdbcCall.returningResultSet("eventDetails", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("HcEventID", eventId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					events = (ArrayList<Event>) resultFromProcedure.get("eventDetails");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getEventAppSites : Error occurred in usp_WebHcHubCitiEventAppSiteDisplay Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiEventAppSiteDisplay is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getEventAppSites : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return events;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Event> getFundraiserEvents(Integer fundraiserId) throws ScanSeeQRWebSqlException {
		final String methodName = "getFundraiserEvents";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> events = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiFundraiserEventDisplay");
			simpleJdbcCall.returningResultSet("eventDetails", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("HcFundraiserID", fundraiserId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					events = (ArrayList<Event>) resultFromProcedure.get("eventDetails");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getFundraiserEvents : Error occurred in usp_WebHcHubCitiFundraiserEventDisplay Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiFundraiserEventDisplay is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getFundraiserEvents : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return events;
	}

	@SuppressWarnings("unchecked")
	public Event getEventLogistics(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException {
		final String methodName = "getEventLogistics";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ButtonInfo> eventButtons = null;
		Event event = null;
		String eventLogisticImgPath = null;
		String eventLogisticMapURL = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiEventLogisticsDetails");
			simpleJdbcCall.returningResultSet("eventButtonDetails", new BeanPropertyRowMapper<ButtonInfo>(ButtonInfo.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("HcEventID", eventId);
			queryParams.addValue("HubCitiId", hubcitiId);
			queryParams.addValue("UserID", null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					eventButtons = (ArrayList<ButtonInfo>) resultFromProcedure.get("eventButtonDetails");
					eventLogisticImgPath = (String) resultFromProcedure.get("EventLogisticImgPath");
					eventLogisticMapURL = (String) resultFromProcedure.get("EventLogisticMapURL");
					if (null != eventButtons && !eventButtons.isEmpty()) {
						event = new Event();
						event.setButtonInfos(eventButtons);
						event.setEventLogisticImgPath(eventLogisticImgPath);
						event.setEventFilePath(eventLogisticMapURL);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getEventLogistics : Error occurred in usp_WebHcHubCitiEventLogisticsDetails Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiEventLogisticsDetails is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getEventLogistics : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<RetailerPage> getSpecialOfferRetLocs(Integer retailId, Integer pageId) throws ScanSeeQRWebSqlException {

		final String methodName = "getSpecialOfferRetLocs";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<RetailerPage> specialOfferLocs = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcSpecialOfferRetailLocationsList");
			simpleJdbcCall.returningResultSet("specialOfferLocs", new BeanPropertyRowMapper<RetailerPage>(RetailerPage.class));

			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("RetailID", retailId);
			queryParams.addValue("PageID", pageId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					specialOfferLocs = (ArrayList<RetailerPage>) resultFromProcedure.get("specialOfferLocs");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getSpecialOfferRetLocs : Error occurred in usp_WebHcSpecialOfferRetailLocationsList Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}

			LOG.info("usp_WebHcSpecialOfferRetailLocationsList is executed Successfully.");

		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getSpecialOfferRetLocs : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return specialOfferLocs;
	}

	@SuppressWarnings("unchecked")
	public EventMarkerDetails getEventLogisticsMap(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException {
		final String methodName = "getEventLogisticsMap";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<EventMarker> eventMarkers = null;
		EventMarkerDetails eventMarkerDetails = null;
		Double latitude;
		Double longitude;
		Double topLeftLat;
		Double topLeftLong;
		Double bottomRigthLat;
		Double bottomRightLong;
		String mapTylerImagePath = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiEventMarkerDetailsDisplay");
			simpleJdbcCall.returningResultSet("eventMarkerDetails", new BeanPropertyRowMapper<EventMarker>(EventMarker.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("HcEventsID", eventId);
			queryParams.addValue("HubCitiID", hubcitiId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					eventMarkers = (ArrayList<EventMarker>) resultFromProcedure.get("eventMarkerDetails");

					latitude = (Double) resultFromProcedure.get("EventLogisticLatitude");
					longitude = (Double) resultFromProcedure.get("EventLogisticLongitude");
					topLeftLat = (Double) resultFromProcedure.get("EventLogisticTopLeftLat");
					topLeftLong = (Double) resultFromProcedure.get("EventLogisticTopLeftLong");
					bottomRigthLat = (Double) resultFromProcedure.get("EventLogisticBottomRightLat");
					bottomRightLong = (Double) resultFromProcedure.get("EventLogisticBottomRightLong");

					mapTylerImagePath = (String) resultFromProcedure.get("MapTylerImagePath");

					eventMarkerDetails = new EventMarkerDetails();
					eventMarkerDetails.setEventMarkers(eventMarkers);
					eventMarkerDetails.setEventLogisticLatitude(latitude);
					eventMarkerDetails.setEventLogisticLongitude(longitude);
					eventMarkerDetails.setEventLogisticTopLeftLat(topLeftLat);
					eventMarkerDetails.setEventLogisticTopLeftLong(topLeftLong);
					eventMarkerDetails.setEventLogisticBottomRigthLat(bottomRigthLat);
					eventMarkerDetails.setEventLogisticBottomRightLong(bottomRightLong);
					eventMarkerDetails.setMapTylerImagePath(mapTylerImagePath);

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getEventLogistics : Error occurred in usp_WebHcHubCitiEventMarkerDetailsDisplay Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiEventMarkerDetailsDisplay is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getEventLogisticsMap : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return eventMarkerDetails;
	}

	@SuppressWarnings("unchecked")
	public RetailerPage getHotdealInfo(Integer hotdealId, Integer hubcitiId) throws ScanSeeQRWebSqlException {

		final String methodName = "getHotdealInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String androidAppID = null;
		String iOSAppID = null;
		ArrayList<RetailerPage> specialOfferLocs = null;
		RetailerPage retailerPage = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHotDealDetails");
			simpleJdbcCall.returningResultSet("hotdeallst", new BeanPropertyRowMapper<RetailerPage>(RetailerPage.class));

			final MapSqlParameterSource queryParams = new MapSqlParameterSource();

			queryParams.addValue("HotDealID", hotdealId);
			queryParams.addValue("HcHubcitiID", hubcitiId);
			// queryParams.addValue("UserID", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					specialOfferLocs = (ArrayList<RetailerPage>) resultFromProcedure.get("hotdeallst");

					if (null != specialOfferLocs && !specialOfferLocs.isEmpty()) {

						retailerPage = specialOfferLocs.get(0);
						iOSAppID = (String) resultFromProcedure.get("IOSAppID");
						androidAppID = (String) resultFromProcedure.get("AndroidAppID");

						retailerPage.setIOSAppID(iOSAppID);
						retailerPage.setAndroidAppID(androidAppID);

					}

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getHotdealInfo : Error occurred in usp_WebHcSpecialOfferRetailLocationsList Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}

			LOG.info("usp_WebHcSpecialOfferRetailLocationsList is executed Successfully.");

		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getHotdealInfo : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerPage;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Event> getHotdealLocs(Integer hotdealId) throws ScanSeeQRWebSqlException {
		final String methodName = "getHotdealLocs";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> events = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiHotdealLocationDisplay");
			simpleJdbcCall.returningResultSet("eventDetails", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("ProductHotdealID", hotdealId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					events = (ArrayList<Event>) resultFromProcedure.get("eventDetails");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getHotdealLocs : Error occurred in usp_WebHcHubCitiHotdealLocationDisplay Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiHotdealLocationDisplay is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getHotdealLocs : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return events;
	}

	@SuppressWarnings("unchecked")
	public Coupon getCouponDetails(Integer couponID, Integer hubcitiId) throws ScanSeeQRWebSqlException {
		final String methodName = "getCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Coupon> coupons = null;
		Coupon coupon = null;
		String androidAppID = null;
		String iOSAppID = null;
		ArrayList<Coupon> locationDetails = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcCouponDetails");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<Coupon>(Coupon.class));
			simpleJdbcCall.returningResultSet("result-set-2", new BeanPropertyRowMapper<Coupon>(Coupon.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("CouponID", couponID);
			queryParams.addValue("HcHubcitiID", hubcitiId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					coupons = (ArrayList<Coupon>) resultFromProcedure.get("CouponDetails");
					locationDetails = (ArrayList<Coupon>) resultFromProcedure.get("result-set-2");
					if (null != coupons && !coupons.isEmpty()) {
						coupon = coupons.get(0);
					}
					if (null != locationDetails && !locationDetails.isEmpty()) {
						coupon.setLocationDetails(locationDetails);
					}

					iOSAppID = (String) resultFromProcedure.get("IOSAppID");
					androidAppID = (String) resultFromProcedure.get("AndroidAppID");
					coupon.setAndroidAppID(androidAppID);
					coupon.setiOSAppID(iOSAppID);

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getCouponDetails : Error occurred in usp_WebHcCouponDetails Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcCouponDetails is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getCouponDetails : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return coupon;
	}

	/**
	 * Below method is used to fetch marker details for appsite.
	 * 
	 * @param logisticId
	 * @param hubCitiId
	 * @return AppsiteMarkerDetails
	 * 
	 */
	public AppsiteMarkerDetails showAppsiteLogisticPage(Integer logisticId, Integer hubCitiId) throws ScanSeeQRWebSqlException {

		final String strMethodName = "showAppsiteLogisticPage";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ArrayList<AppsiteMarkers> appsiteMarkersLst = null;
		AppsiteMarkerDetails appsiteMarkerDetails = null;
		Double latitude = null;
		Double longitude = null;
		String imagePath = null;
		Double topLeftLat = null;
		Double topLeftLong = null;
		Double bottomRightLat = null;
		Double bottomRightLong = null;
		Integer initialZoomLevel = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcAppsiteMarkerDetails");
			simpleJdbcCall.returningResultSet("appsitelogisticinfo", new BeanPropertyRowMapper<AppsiteMarkers>(AppsiteMarkers.class));
			final MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
			mapSqlParameterSource.addValue("HubCitiID", hubCitiId);
			mapSqlParameterSource.addValue("HcAppsiteLogisticID", logisticId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(mapSqlParameterSource);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) {
					appsiteMarkersLst = (ArrayList<AppsiteMarkers>) resultFromProcedure.get("appsitelogisticinfo");

					imagePath = (String) resultFromProcedure.get("AppsiteMapImagePath");
					latitude = (Double) resultFromProcedure.get("AppsiteLogisticLatitude");
					longitude = (Double) resultFromProcedure.get("AppsiteLogisticLongitude");

					topLeftLat = (Double) resultFromProcedure.get("AppsiteLogisticTopLeftLat");
					topLeftLong = (Double) resultFromProcedure.get("AppsiteLogisticTopLeftLong");
					bottomRightLat = (Double) resultFromProcedure.get("AppsiteLogisticBottomRightLat");
					bottomRightLong = (Double) resultFromProcedure.get("AppsiteLogisticBottomRightLong");
					initialZoomLevel = (Integer) resultFromProcedure.get("InitialZoomLevel");

					appsiteMarkerDetails = new AppsiteMarkerDetails();
					if (null != appsiteMarkersLst && !appsiteMarkersLst.isEmpty()) {

						appsiteMarkerDetails.setAppsiteMarkerLst(appsiteMarkersLst);

					}

					appsiteMarkerDetails.setAppsiteMapImagePath(imagePath);
					appsiteMarkerDetails.setAppsiteLogisticLatitude(latitude);
					appsiteMarkerDetails.setAppsiteLogisticLongitude(longitude);
					appsiteMarkerDetails.setAppsiteLogisticTopLeftLat(topLeftLat);
					appsiteMarkerDetails.setAppsiteLogisticTopLeftLong(topLeftLong);
					appsiteMarkerDetails.setAppsiteLogisticBottomRightLat(bottomRightLat);
					appsiteMarkerDetails.setAppsiteLogisticBottomRightLong(bottomRightLong);
					appsiteMarkerDetails.setInitialZoomLevel(initialZoomLevel);

				}

			} else {

				final String strErrorNum = (String) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String strErronMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside QRDaoImpl : showAppsiteLogisticPage : Error occurred in usp_WebHcAppsiteMarkerDetails Store Procedure with error number: {} "
						+ strErrorNum + " and error message: {}" + strErronMsg);
				throw new ScanSeeQRWebSqlException(strErronMsg);

			}

		} catch (ScanSeeQRWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURED + strMethodName + exception.getMessage());
			throw new ScanSeeQRWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return appsiteMarkerDetails;
	}

	@SuppressWarnings("unchecked")
	public Event getBandEventDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException {
		final String methodName = "getEventDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> events = null;
		Event event = null;
		String iOSAppID = null;
		String androidAppID = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiBandAndBandEventDetails");
			simpleJdbcCall.returningResultSet("eventDetails", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("HcEventID", eventId);
			queryParams.addValue("HubCitiId", hubcitiId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					events = (ArrayList<Event>) resultFromProcedure.get("eventDetails");
					if (null != events && !events.isEmpty()) {
						event = events.get(0);
						iOSAppID = (String) resultFromProcedure.get("IOSAppID");
						androidAppID = (String) resultFromProcedure.get("AndroidAppID");
						event.setAndroidAppID(androidAppID);
						event.setIOSAppID(iOSAppID);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getEventDetails : Error occurred in usp_WebHcHubCitiEventDetails Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiEventDetails is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getEventDetails : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	/**
	 * To display band event details.
	 */
	@SuppressWarnings("unchecked")
	public Band showBandEventDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRWebSqlException {
		final String methodName = "showBandEventDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Band bandEvt = null;
		ArrayList<Band> bandEvtlst = null;
		String iOSAppID = null;
		String androidAppID = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHcHubCitiBandAndBandEventDetails");
			simpleJdbcCall.returningResultSet("bndeventDetails", new BeanPropertyRowMapper<Band>(Band.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("HcBandEventID", eventId);
			queryParams.addValue("BandID", null);
			queryParams.addValue("HubCitiID", hubcitiId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					bandEvtlst = (ArrayList<Band>) resultFromProcedure.get("bndeventDetails");
					if (null != bandEvtlst && !bandEvtlst.isEmpty()) {
						bandEvt = bandEvtlst.get(0);
						iOSAppID = (String) resultFromProcedure.get("IOSAppID");
						androidAppID = (String) resultFromProcedure.get("AndroidAppID");
						bandEvt.setAndroidAppID(androidAppID);
						bandEvt.setiOSAppID(iOSAppID);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : showBandEventDetails : Error occurred in usp_WebHcHubCitiEventDetails Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiEventDetails is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getEventDetails : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return bandEvt;
	}

	/**
	 * Below method is used to display band summary details.
	 */
	@SuppressWarnings("unchecked")
	public Band showBandDetails(Integer hubcitiId, Integer bandId) throws ScanSeeQRWebSqlException {
		final String methodName = "showBandDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Band> bandDetail = null;
		Band bndDetail = null;
		String iOSAppID;
		String androidAppID;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_HcBandSummaryDetail");
			simpleJdbcCall.returningResultSet("BndDetails", new BeanPropertyRowMapper<Band>(Band.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();

			queryParams.addValue("BandID", bandId);
			queryParams.addValue("HubCitiID", hubcitiId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					bandDetail = (ArrayList<Band>) resultFromProcedure.get("BndDetails");
					if (null != bandDetail && !bandDetail.isEmpty()) {
						bndDetail = bandDetail.get(0);

						iOSAppID = (String) resultFromProcedure.get("IOSAppID");
						androidAppID = (String) resultFromProcedure.get("AndroidAppID");
						bndDetail.setAndroidAppID(androidAppID);
						bndDetail.setiOSAppID(iOSAppID);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : showBandDetails : Error occurred in usp_WebHcHubCitiBandAndBandEventDetails Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiEventDetails is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getEventDetails : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return bndDetail;
	}

	/**
	 * to display band created anything pages.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Band> fetchBandAnythingpglst(Integer bandId, Integer hubCitiId) throws ScanSeeQRWebSqlException {
		final String methodName = "showBandDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Band> bandDetail = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_HcQRDisplayBandCreatedPages");
			simpleJdbcCall.returningResultSet("BndAnythngpgs", new BeanPropertyRowMapper<Band>(Band.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("BandID", bandId);
			queryParams.addValue("HcHubCitiID", hubCitiId);
			queryParams.addValue("UserID", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					bandDetail = (ArrayList<Band>) resultFromProcedure.get("BndAnythngpgs");

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : showBandDetails : Error occurred in usp_WebHcHubCitiBandAndBandEventDetails Store Procedure with error number: {} "
							+ errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHcHubCitiEventDetails is executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getEventDetails : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return bandDetail;
	}
	
	
	
	/**
	 * DAOImpl method to get News detail.
	 * 
	 * @param iNewsId
	 * @throws ScanSeeQRWebSqlException
	 *           
	 * @return News Detail .
	 */
	public Item getNewsDetail(Integer iNewsId) throws ScanSeeQRWebSqlException {
		
		LOG.info("Inside QRDaoImpl : getNewsDetail");
		
		ArrayList<Item> arItemList = null;
		
		Item objItem = null;
		String iOSAppID = null;
		String androidAppID = null;
		
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.HUBCITI_SCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebNewsFirstDetails");
			simpleJdbcCall.returningResultSet("newsDetails", new BeanPropertyRowMapper<Item>(Item.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("NewsID", iNewsId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					arItemList = (ArrayList<Item>) resultFromProcedure.get("newsDetails");
					if (null != arItemList && !arItemList.isEmpty()) {
						objItem = arItemList.get(0);
						iOSAppID = (String) resultFromProcedure.get("IOSAppID");
						androidAppID = (String) resultFromProcedure.get("AndroidAppID");
						objItem.setAndroidAppID(androidAppID);
						objItem.setiOSAppID(iOSAppID);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside QRDaoImpl : getNewsDetail " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeQRWebSqlException(errorMsg);
				}
			}
		} catch (DataAccessException e) {
			LOG.error("Inside QRDaoImpl : getNewsDetail : " + e);
			throw new ScanSeeQRWebSqlException(e);
		}

		LOG.info("Exit QRDaoImpl : getNewsDetail");
		return objItem;
	}
}

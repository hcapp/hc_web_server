package com.scansee.qr.controller;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.scansee.qr.service.QRService;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeQRServiceException;
import common.pojo.AppConfiguration;
import common.pojo.AppsiteMarkerDetails;
import common.pojo.AppsiteMarkers;
import common.pojo.Band;
import common.pojo.Coupon;
import common.pojo.Event;
import common.pojo.EventLogisticsLatLongs;
import common.pojo.EventMarker;
import common.pojo.EventMarkerDetails;
import common.pojo.Item;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfo;
import common.pojo.ProductMedia;
import common.pojo.RetailerCreatedPage;
import common.pojo.RetailerPage;
import common.util.Utility;

/**
 * @author chandrasekaran_j
 */
@Controller
public class QRCodeController {
	/**
	 * request param value.
	 */
	public static final String RETID = "retId";
	/**
	 * request param value.
	 */
	public static final String RETLOCID = "retlocId";
	/**
	 * request param value.
	 */
	public static final String PAGEID = "pageId";
	/**
	 * request param value.
	 */
	public static final String HUBCITIID = "hubcitiId";
	/**
	 * request param value.
	 */
	public static final String EVENTID = "eventId";
	/**
	 * request param value.
	 */
	public static final String FUNDEVTID = "fundEvtId";

	/**
	 * valid input message.
	 */
	public static final String INPUTMESSAGE = "No Data Found Please Provide Valid Input.";

	/**
	 * expired page.
	 */
	public static final String EXPIREDPAGE = "expiredPage";

	/**
	 * error page.
	 */
	public static final String ERRORPAGE = "errorPage";

	/**
	 * error message .
	 */
	public static final String ERRORINPUTMESSAGE = "Error Occurred Please Contact Administrator.";

	/**
	 * session variable name .
	 */
	public static final String SESSIONVARNAME = "Message";

	/**
	 * qr service variable name .
	 */
	public static final String QRSERVICEVAR = "QRService";

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(QRCodeController.class);

	public static final String HOTDEALID = "hotdealId";

	/**
	 * request param value.
	 */
	public static final String BANDEVENTID = "bndevtId";

	public static final String BANDID = "bandId";

	public static final String NEWSID = "newsId";

	/**
	 * This method is used for displaying retailer web page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/2000.htm", method = RequestMethod.GET)
	public final String showRetailerPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {

		String viewName = "";
		final String retailerId = Utility.checkEmpty(request.getParameter(RETID));
		final String locationId = Utility.checkEmpty(request.getParameter(RETLOCID));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		Integer hubcitiParam = null;
		Integer retailerIdParam = null;
		Integer locationIdParam = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);
		RetailerPage retailerPageInfo = null;
		ArrayList<RetailerPage> retCreatedPagelst = null;
		String strMessage = "";
		String newURL = null;

		try {

			if (null != retailerId && null != locationId) {

				if (null != hubcitiId && !"null".equalsIgnoreCase(hubcitiId)) {
					hubcitiParam = Integer.parseInt(hubcitiId);
				}
				if (null != retailerId && !"null".equalsIgnoreCase(retailerId)) {
					retailerIdParam = Integer.parseInt(retailerId);
				}
				if (null != locationId && !"null".equalsIgnoreCase(locationId)) {
					locationIdParam = Integer.parseInt(locationId);
				}

				retailerPageInfo = qrService.getRetailerPageInfo(hubcitiParam, retailerIdParam, locationIdParam);
				retCreatedPagelst = qrService.getRetailerCreatedPageLst(hubcitiParam, retailerIdParam, locationIdParam);

				if (retCreatedPagelst != null && !retCreatedPagelst.isEmpty()) {
					session.setAttribute("retCreatedPagelst", retCreatedPagelst);
					viewName = "previewRetailerPage";
				}

				if (retailerPageInfo != null) {
					if (null != retailerPageInfo.getRetailerURL() && !"".equals(retailerPageInfo.getRetailerURL())) {
						if (retailerPageInfo.getRetailerURL().startsWith(ApplicationConstants.WWW)) {
							newURL = ApplicationConstants.HTTP + retailerPageInfo.getRetailerURL();
							retailerPageInfo.setRetailerURL(newURL);
						}
					}
					session.setAttribute("retailerPageInfoList", retailerPageInfo);
					request.setAttribute("AndroidAppID", retailerPageInfo.getAndroidAppID());
					request.setAttribute("IOSAppID", retailerPageInfo.getIOSAppID());
					request.setAttribute("URL", retailerPageInfo.getQRUrl());
					viewName = "previewRetailerPage";
				} else {
					strMessage = INPUTMESSAGE;
					viewName = EXPIREDPAGE;
				}

			} else {
				strMessage = ERRORINPUTMESSAGE;
				viewName = ERRORPAGE;
			}
			session.setAttribute(SESSIONVARNAME, strMessage);

		} catch (Exception e) {
			LOG.error("Exception occurred in showRetailerPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);

		}

		LOG.info("showRetailerPage:: Inside Exit Get Method");
		return viewName;
	}

	/**
	 * This method is used for displaying retailer created page and opper page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = { "/2100.htm", "/2200.htm" }, method = RequestMethod.GET)
	public final ModelAndView showRetailerCreatedPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		final String methodName = "showRetailerCreatedPage controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		session.removeAttribute(SESSIONVARNAME);
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);
		final String retailerId = Utility.checkEmpty(request.getParameter(RETID));
		final String retailLocId = Utility.checkEmpty(request.getParameter(RETLOCID));
		final String pageId = Utility.checkEmpty(request.getParameter(PAGEID));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		Integer hubcitiParam = null;
		Integer retailerIdParam = null;
		Integer retailLocIdParam = null;
		String mainMenuID = Utility.checkEmpty(request.getParameter("mainMenuID"));
		String scanTypeID = Utility.checkEmpty(request.getParameter("scanTypeID"));
		String ExternalLInkFlag = Utility.checkEmpty(request.getParameter("EL"));

		// To check request is from HubCiti or ScanSee
		String source = Utility.checkEmpty(request.getParameter("source"));

		Integer mmID = null;
		Integer scanTypID = null;
		String fromSS = "";
		String viewName = "";
		String strMessage = "";
		String pathInfo = request.getPathInfo();
		String pageType = null;
		RetailerCreatedPage retailerpage = null;
		ArrayList<RetailerPage> retailerPages = null;

		if (null != mainMenuID) {
			mmID = Integer.parseInt(mainMenuID);
		}
		if (null != scanTypeID) {
			scanTypID = Integer.parseInt(scanTypeID);
		}

		try {

			if (null != request.getParameter("fromSS")) {
				fromSS = request.getParameter("fromSS");
			} else {
				fromSS = "N";
			}

			LOG.info("FROMSS VALUE " + fromSS);

			if (null != retailerId && null != retailLocId && null != pageId) {

				if (null != pathInfo && pathInfo.equals(ApplicationConstants.ANYTHINGPAGEURL)) {
					pageType = ApplicationConstants.ANYTHINGPAGE;
				} else if (null != pathInfo && pathInfo.equals(ApplicationConstants.SPECIALOFFERPAGEURL)) {
					pageType = ApplicationConstants.SPECIALOFFERPAGE;
				}

				if (null != hubcitiId && !"null".equalsIgnoreCase(hubcitiId)) {
					hubcitiParam = Integer.parseInt(hubcitiId);
				}

				if (null != retailerId && !"null".equalsIgnoreCase(retailerId)) {
					retailerIdParam = Integer.parseInt(retailerId);
				}

				if (null != retailLocId && !"null".equalsIgnoreCase(retailLocId)) {
					retailLocIdParam = Integer.parseInt(retailLocId);
				}

				retailerpage = qrService.getRetailerLandingPage(hubcitiParam, retailerIdParam, retailLocIdParam, Integer.parseInt(pageId), mmID,
						scanTypID, pageType, source);

				if (pageType.equalsIgnoreCase(ApplicationConstants.SPECIALOFFERPAGE)) {

					retailerPages = qrService.getSpecialOfferRetLocs(retailerIdParam, Integer.parseInt(pageId));

					if (null != retailerPages && !retailerPages.isEmpty()) {
						session.setAttribute("specialRetailLocs", retailerPages);
					} else {
						session.setAttribute("specialRetailLocs", null);
					}

				}

				if (null != retailerpage) {

					if (null != ExternalLInkFlag && ExternalLInkFlag.equals("true")) {
						viewName = retailerpage.getExternalLinkUrl();
					} else {
						session.setAttribute("retailerCreatedPageInfo", retailerpage);
						session.setAttribute("scanSeeFlag", fromSS);
						request.setAttribute("AndroidAppID", retailerpage.getAndroidAppID());
						request.setAttribute("IOSAppID", retailerpage.getIOSAppID());
						request.setAttribute("URL", retailerpage.getQRUrl());
						if ("1".equals(retailerpage.getExpired())) {
							strMessage = "Offer is expired";
							viewName = EXPIREDPAGE;
						} else {
							viewName = "createdPage";
						}
					}
				} else {
					strMessage = INPUTMESSAGE;
					viewName = EXPIREDPAGE;
				}
			} else {
				strMessage = ERRORINPUTMESSAGE;
				viewName = ERRORPAGE;
			}
			session.setAttribute(SESSIONVARNAME, strMessage);
		} catch (Exception e) {
			LOG.error("Exception occurred in showRetailerCreatedPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}
		if (null != ExternalLInkFlag && ExternalLInkFlag.equals("true")) {
			if (viewName.startsWith(ApplicationConstants.WWW)) {
				viewName = ApplicationConstants.HTTP + viewName;
			}
			return new ModelAndView(new RedirectView(viewName));
		} else {
			return new ModelAndView(viewName);
		}
	}

	/**
	 * This method is used for displaying product web page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/1000.htm", method = RequestMethod.GET)
	public final String showProductInfoPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		final String methodName = "showProductInfoPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		session.removeAttribute(SESSIONVARNAME);
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);
		final String productId = Utility.checkEmpty(request.getParameter("prodId"));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		Integer hubcitiParam = null;
		String viewName = "";
		String strMessage = "";
		String strMediaType = "";
		ProductInfo productInfo = null;
		ArrayList<ProductAttributes> prodAttributeList = null;
		ArrayList<ProductMedia> prodMediaList = null;
		final ArrayList<ProductMedia> productAudioList = new ArrayList<ProductMedia>();
		final ArrayList<ProductMedia> productVideoList = new ArrayList<ProductMedia>();

		try {
			if (null != productId) {
				if (null != hubcitiId) {
					hubcitiParam = Integer.parseInt(hubcitiId);
				}
				productInfo = qrService.getProductInfo(hubcitiParam, Integer.parseInt(productId));
				if (null != productInfo) {
					prodAttributeList = qrService.getProductAttributes(Integer.parseInt(productId));
					prodMediaList = qrService.getProductMedia(Integer.parseInt(productId));
					if (null != prodAttributeList && !prodAttributeList.isEmpty()) {
						session.setAttribute("ProductAttributesList", prodAttributeList);
					} else {
						session.setAttribute("ProductAttributesList", "");
					}
					if (null != prodMediaList && !prodMediaList.isEmpty()) {
						for (int i = 0; i < prodMediaList.size(); i++) {
							strMediaType = prodMediaList.get(i).getProductMediaType();
							if ("Audio Files".equals(strMediaType)) {
								productAudioList.add(prodMediaList.get(i));
								session.setAttribute("ProductAudioList", productAudioList);
							} else if ("Video Files".equals(strMediaType)) {
								productVideoList.add(prodMediaList.get(i));
								session.setAttribute("ProductVideoList", productVideoList);
							}
						}

					} else {
						session.setAttribute("ProductAudioList", "");
						session.setAttribute("ProductVideoList", "");
					}
					session.setAttribute("ProductInfo", productInfo);
					request.setAttribute("AndroidAppID", productInfo.getAndroidAppID());
					request.setAttribute("IOSAppID", productInfo.getIOSAppID());
					request.setAttribute("URL", productInfo.getQRUrl());

					viewName = "productDetailsPage";
				} else {
					strMessage = INPUTMESSAGE;
					viewName = EXPIREDPAGE;
				}
			} else {
				strMessage = ERRORINPUTMESSAGE;
				viewName = ERRORPAGE;
			}
			session.setAttribute(SESSIONVARNAME, strMessage);
		} catch (Exception e) {
			LOG.error("Exception occurred in showRetailerCreatedPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}
		return viewName;

	}

	/**
	 * This method is used for displaying giveaway page
	 * 
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param response
	 *            response
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/2300.htm", method = RequestMethod.GET)
	public final ModelAndView showGiveawayPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpServletResponse response)
			throws ScanSeeQRServiceException {
		final String methodName = "showGiveawayPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		session.removeAttribute(SESSIONVARNAME);
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);
		final String retailerId = Utility.checkEmpty(request.getParameter(RETID));
		final String pageId = Utility.checkEmpty(request.getParameter(PAGEID));
		final String mainMenuID = Utility.checkEmpty(request.getParameter("mainmenuid"));
		final String scanTypeID = Utility.checkEmpty(request.getParameter("scantypeid"));
		String fromSS = "";
		String viewName = "";
		String strMessage = "";
		RetailerCreatedPage retailerpage = null;
		ArrayList<AppConfiguration> appConfig = null;
		AppConfiguration appConfiguration = null;
		String appDownLoadLink = null;
		try {
			if (null != request.getParameter("fromSS")) {
				fromSS = request.getParameter("fromSS");
			} else {
				fromSS = "No";
			}
			LOG.info("FROMSS VALUE " + fromSS);
			if (null != fromSS && fromSS.equals("Yes")) {

				if (null != retailerId && null != pageId && null != mainMenuID && null != scanTypeID) {
					retailerpage = qrService.getGiveawayPageInfo(Integer.parseInt(retailerId), Integer.parseInt(pageId),
							Integer.parseInt(mainMenuID), Integer.parseInt(scanTypeID));
					if (null != retailerpage) {
						retailerpage.setQrType("Giveaway Page");
						session.setAttribute("retailerCreatedPageInfo", retailerpage);
						session.setAttribute("scanSeeFlag", fromSS);
						if ("1".equals(retailerpage.getExpired())) {
							strMessage = "I am sorry. This promotion is no longer active.";
							viewName = EXPIREDPAGE;
						} else {
							viewName = "giveawaypage";
						}
					} else {
						strMessage = INPUTMESSAGE;
						viewName = EXPIREDPAGE;
					}
				} else {
					strMessage = ERRORINPUTMESSAGE;
					viewName = ERRORPAGE;
				}

			} else {
				appConfig = qrService.getAppConfig("App Download Link");

				if (null != appConfig && !appConfig.isEmpty()) {

					appConfiguration = appConfig.get(0);
					appDownLoadLink = appConfiguration.getScreenContent();

					return new ModelAndView(new RedirectView(appDownLoadLink));

				}
			}

			session.setAttribute(SESSIONVARNAME, strMessage);
		} catch (Exception e) {
			LOG.error("Exception occurred in showGiveawayPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used for displaying event web page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/2400.htm", method = RequestMethod.GET)
	public final String showEventInfoPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		final String methodName = "showEventInfoPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute(SESSIONVARNAME);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);

		final String eventId = Utility.checkEmpty(request.getParameter(EVENTID));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		Integer eventIdParam = Integer.parseInt(eventId);
		Integer hubcitiIdParam = null;
		String viewName = "eventdetails";
		Event event = null;
		String newURL = null;
		ArrayList<Event> eventAppsites = null;
		// String newURL = null;

		try {

			if (null != hubcitiId) {
				hubcitiIdParam = Integer.parseInt(hubcitiId);
			}

			event = qrService.getEventDetails(eventIdParam, hubcitiIdParam);

			if (null != event && null != event.getAppSiteIDs() && !"".equals(event.getAppSiteIDs())) {
				eventAppsites = qrService.getEventAppSites(eventIdParam);
			}

			if (null != event.getMoreInfoURL() && !"".equals(event.getMoreInfoURL())) {
				if (event.getMoreInfoURL().startsWith(ApplicationConstants.WWW)) {
					newURL = ApplicationConstants.HTTP + event.getMoreInfoURL();
					event.setMoreInfoURL(newURL);
				}
			}

			session.setAttribute("appsites", eventAppsites);
			session.setAttribute("events", event);
			request.setAttribute("AndroidAppID", event.getAndroidAppID());
			request.setAttribute("IOSAppID", event.getIOSAppID());
			request.setAttribute("URL", event.getQRUrl());

		} catch (Exception e) {
			LOG.error("Exception occurred in showEventInfoPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;

	}

	/**
	 * This method is used for displaying event web page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/2500.htm", method = RequestMethod.GET)
	public final String showFundraiserInfoPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		final String methodName = "showFundraiserInfoPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute(SESSIONVARNAME);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);

		final String fundraiserId = Utility.checkEmpty(request.getParameter(FUNDEVTID));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		final Integer fundraiserIdParam = Integer.parseInt(fundraiserId);
		Integer hubcitiIdParam = null;
		String viewName = "fundraiserdetails";
		String newURL = null;
		Event event = null;
		ArrayList<Event> fundraiserEvents = null;

		try {

			if (null != hubcitiId) {
				hubcitiIdParam = Integer.parseInt(hubcitiId);
			}

			event = qrService.getFundraiserDetails(fundraiserIdParam, hubcitiIdParam);

			if (null != event.getMoreInfoURL() && !"".equals(event.getMoreInfoURL())) {
				if (event.getMoreInfoURL().startsWith(ApplicationConstants.WWW)) {
					newURL = ApplicationConstants.HTTP + event.getMoreInfoURL();
					event.setMoreInfoURL(newURL);
				}
			}
			if (null != event.getPurchaseProducts() && !"".equals(event.getPurchaseProducts())) {
				if (event.getPurchaseProducts().startsWith(ApplicationConstants.WWW)) {
					newURL = ApplicationConstants.HTTP + event.getPurchaseProducts();
					event.setPurchaseProducts(newURL);
				}
			}

			if (null != event && null != event.getEventTiedIds() && !"".equals(event.getEventTiedIds())) {
				fundraiserEvents = qrService.getFundraiserEvents(fundraiserIdParam);
			}
			session.setAttribute("fundraiserEvents", fundraiserEvents);
			session.setAttribute("events", event);
			request.setAttribute("AndroidAppID", event.getAndroidAppID());
			request.setAttribute("IOSAppID", event.getIOSAppID());
			request.setAttribute("URL", event.getQRUrl());

		} catch (Exception e) {
			LOG.error("Exception occurred in showFundraiserInfoPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;

	}

	/**
	 * This method is used for displaying event web Logistics page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/2410.htm", method = RequestMethod.GET)
	public final String showEventLogisticsPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		final String methodName = "showEventLogisticsPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute(SESSIONVARNAME);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);

		final String eventId = Utility.checkEmpty(request.getParameter(EVENTID));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		final Integer eventIdParam = Integer.parseInt(eventId);
		Integer hubcitiIdParam = null;
		String viewName = "eventLogistics";
		Event event = null;

		try {

			if (null != hubcitiId) {
				hubcitiIdParam = Integer.parseInt(hubcitiId);
			}

			event = qrService.getEventLogistics(eventIdParam, hubcitiIdParam);

			session.setAttribute("logisticsDetails", event);

		} catch (Exception e) {
			LOG.error("Exception occurred in showEventLogisticsPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;

	}

	/**
	 * This method is used for displaying event web Logistics page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/2420.htm", method = RequestMethod.GET)
	public final String showEventLogisticMap(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		final String methodName = "showEventLogisticMap";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute(SESSIONVARNAME);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);

		final String eventId = Utility.checkEmpty(request.getParameter(EVENTID));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		final Integer eventIdParam = Integer.parseInt(eventId);
		Integer hubcitiIdParam = null;
		String viewName = "eventLogisticMap";
		EventMarkerDetails eventMarkerDetails = null;
		EventLogisticsLatLongs eventLogisticsLatLongs = null;
		Integer zindex = 1;
		StringBuffer markerDetails = new StringBuffer();
		markerDetails.append("[");

		try {

			if (null != hubcitiId) {
				hubcitiIdParam = Integer.parseInt(hubcitiId);
			}

			eventMarkerDetails = qrService.getEventLogisticsMap(eventIdParam, hubcitiIdParam);

			if (null != eventMarkerDetails && null != eventMarkerDetails.getEventMarkers() && !eventMarkerDetails.getEventMarkers().isEmpty()) {

				eventLogisticsLatLongs = new EventLogisticsLatLongs();

				eventLogisticsLatLongs.setEventLogisticLatitude(eventMarkerDetails.getEventLogisticLatitude());
				eventLogisticsLatLongs.setEventLogisticLongitude(eventMarkerDetails.getEventLogisticLongitude());
				eventLogisticsLatLongs.setEventLogisticTopLeftLat(eventMarkerDetails.getEventLogisticTopLeftLat());
				eventLogisticsLatLongs.setEventLogisticTopLeftLong(eventMarkerDetails.getEventLogisticTopLeftLong());
				eventLogisticsLatLongs.setEventLogisticBottomRigthLat(eventMarkerDetails.getEventLogisticBottomRigthLat());
				eventLogisticsLatLongs.setEventLogisticBottomRightLong(eventMarkerDetails.getEventLogisticBottomRightLong());

				for (EventMarker marker : eventMarkerDetails.getEventMarkers()) {
					if (zindex == 1) {
						markerDetails.append("[\"" + marker.getEvtMarkerName() + "\"," + marker.getLatitude() + "," + marker.getLogitude() + ",\""
								+ marker.getEventImageName() + "\"," + zindex + "]");
					} else {
						markerDetails.append(",[\"" + marker.getEvtMarkerName() + "\"," + marker.getLatitude() + "," + marker.getLogitude() + ",\""
								+ marker.getEventImageName() + "\"," + zindex + "]");
					}
					zindex++;
				}
			}

			markerDetails.append("]");
			session.setAttribute("Markers", markerDetails);
			session.setAttribute("eventLogisticsLatLongs", eventLogisticsLatLongs);
			session.setAttribute("mapTylerImagePath", "\"" + eventMarkerDetails.getMapTylerImagePath() + "\"");

		} catch (Exception e) {
			LOG.error("Exception occurred in showEventLogisticMap:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;

	}

	/**
	 * This method is used for displaying event web page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/2600.htm", method = RequestMethod.GET)
	public final String showHotdealInfoPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		final String methodName = "showHotdealInfoPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute(SESSIONVARNAME);
		ArrayList<Event> eventAppsites = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);

		final String hotdealid = Utility.checkEmpty(request.getParameter(HOTDEALID));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));

		Integer hubcitiIdParam = null;
		Integer ihotdealId = null;
		String viewName = "hotdeals";
		RetailerPage retailerPage = null;

		try {

			session.removeAttribute("appsites");
			session.removeAttribute("retailerPage");

			if (null != hubcitiId) {
				hubcitiIdParam = Integer.parseInt(hubcitiId);
			}

			if (null != hotdealid) {
				ihotdealId = Integer.parseInt(hotdealid);
			}
			retailerPage = qrService.getHotdealsDetails(ihotdealId, hubcitiIdParam);

			if (null != retailerPage && null != retailerPage.getHdLocFlag() && retailerPage.getHdLocFlag() == true) {

				eventAppsites = qrService.getHotdealLocs(ihotdealId);

			}
			session.setAttribute("appsites", eventAppsites);
			session.setAttribute("retailerPage", retailerPage);
			request.setAttribute("AndroidAppID", retailerPage.getAndroidAppID());
			request.setAttribute("IOSAppID", retailerPage.getIOSAppID());
			request.setAttribute("URL", retailerPage.getQRUrl());

		} catch (Exception e) {
			LOG.error("Exception occurred in showFundraiserInfoPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;

	}

	/**
	 * This method is used for displaying event web page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/2700.htm", method = RequestMethod.GET)
	public final ModelAndView showCouponInfoPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		final String methodName = "showCouponInfoPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute(SESSIONVARNAME);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);

		final String couponId = Utility.checkEmpty(request.getParameter("couponId"));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		final Integer couponIdParam = Integer.parseInt(couponId);
		Integer hubcitiIdParam = null;
		String viewName = "coupondetails";
		Coupon coupon = null;
		String newURL = null;

		try {

			if (null != hubcitiId) {
				hubcitiIdParam = Integer.parseInt(hubcitiId);
			}

			coupon = qrService.getCouponDetails(couponIdParam, hubcitiIdParam);

			if (null != coupon.getCouponURL() && !"".equals(coupon.getCouponURL())) {
				if (coupon.getCouponURL().startsWith(ApplicationConstants.WWW)) {
					newURL = ApplicationConstants.HTTP + coupon.getCouponURL();
					coupon.setCouponURL(newURL);
				}
			}

			session.setAttribute("coupon", coupon);
			request.setAttribute("AndroidAppID", coupon.getAndroidAppID());
			request.setAttribute("IOSAppID", coupon.getiOSAppID());
			request.setAttribute("URL", coupon.getQRUrl());

		} catch (Exception e) {
			LOG.error("Exception occurred in showFundraiserInfoPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(viewName);
	}

	/**
	 * Below method is used to display appsite logistic map.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @param map
	 * @return
	 * @throws ScanSeeQRServiceException
	 */
	@RequestMapping(value = "/3000.htm", method = RequestMethod.GET)
	public final ModelAndView showAppsiteLogisticPage(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map)
			throws ScanSeeQRServiceException {

		final String strMethodName = "showAppsiteLogisticPage";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strViewName = "appsiteLogisticsMap";
		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final QRService qrService = (QRService) webApplicationContext.getBean(QRSERVICEVAR);

			String strHubCitiId = null;
			String strLogisticId = null;
			Integer iHubCitiId = null;
			Integer iLogisticId = null;
			strHubCitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
			strLogisticId = Utility.checkEmpty(request.getParameter("logisticsId"));
			AppsiteMarkerDetails appsiteMarkerDetails = null;
			EventLogisticsLatLongs eventLogisticsLatLongs = null;
			StringBuffer appsiteMarkerInfo = new StringBuffer();
			appsiteMarkerInfo.append("[");
			Integer zindex = 1;

			if (!Utility.isEmptyOrNullString(strHubCitiId)) {
				iHubCitiId = Integer.parseInt(strHubCitiId);

			}

			if (!Utility.isEmptyOrNullString(strLogisticId)) {
				iLogisticId = Integer.parseInt(strLogisticId);
			}

			appsiteMarkerDetails = qrService.showAppsiteLogisticPage(iLogisticId, iHubCitiId);

			if (null != appsiteMarkerDetails && !appsiteMarkerDetails.getAppsiteMarkerLst().isEmpty()
					&& null != appsiteMarkerDetails.getAppsiteMarkerLst()) {
				eventLogisticsLatLongs = new EventLogisticsLatLongs();
				eventLogisticsLatLongs.setEventLogisticLatitude(appsiteMarkerDetails.getAppsiteLogisticLatitude());
				eventLogisticsLatLongs.setEventLogisticLongitude(appsiteMarkerDetails.getAppsiteLogisticLongitude());
				eventLogisticsLatLongs.setEventLogisticTopLeftLat(appsiteMarkerDetails.getAppsiteLogisticTopLeftLat());
				eventLogisticsLatLongs.setEventLogisticTopLeftLong(appsiteMarkerDetails.getAppsiteLogisticTopLeftLong());
				eventLogisticsLatLongs.setEventLogisticBottomRigthLat(appsiteMarkerDetails.getAppsiteLogisticBottomRightLat());
				eventLogisticsLatLongs.setEventLogisticBottomRightLong(appsiteMarkerDetails.getAppsiteLogisticBottomRightLong());
				eventLogisticsLatLongs.setInitialZoomLevel(appsiteMarkerDetails.getInitialZoomLevel());

				for (AppsiteMarkers appsiteMarker : appsiteMarkerDetails.getAppsiteMarkerLst()) {

					if (zindex == 1) {
						appsiteMarkerInfo.append("[\"" + appsiteMarker.getMarkerName() + "\"," + appsiteMarker.getMarkerLatitude() + ","
								+ appsiteMarker.getMarkerLongitude() + ",\"" + appsiteMarker.getMarkerImage() + "\"," + zindex + ",\""
								+ appsiteMarker.getAnythingPageURL() + "\"]");

					} else {

						appsiteMarkerInfo.append(",[\"" + appsiteMarker.getMarkerName() + "\"," + appsiteMarker.getMarkerLatitude() + ","
								+ appsiteMarker.getMarkerLongitude() + ",\"" + appsiteMarker.getMarkerImage() + "\"," + zindex + ",\""
								+ appsiteMarker.getAnythingPageURL() + "\"]");
					}
					zindex++;

				}

				appsiteMarkerInfo.append("]");
				session.setAttribute("Markers", appsiteMarkerInfo);
				session.setAttribute("eventLogisticsLatLongs", eventLogisticsLatLongs);
				session.setAttribute("mapTylerImagePath", "\"" + appsiteMarkerDetails.getAppsiteMapImagePath() + "\"");

			}

		} catch (ScanSeeQRServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURED + exception.getMessage());
			throw new ScanSeeQRServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);

	}

	/**
	 * This method is used for displaying band event web page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/3001.htm", method = RequestMethod.GET)
	public final String showBandEventInfoPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		String viewName = "";
		final String bandeventId = Utility.checkEmpty(request.getParameter(BANDEVENTID));
		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		Integer hubcitiParam = null;
		Integer bandevtIdParam = null;

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);
		Band bandInfo = null;
		String strMessage = "";

		try {

			if (null != bandeventId && null != hubcitiId) {

				if (null != hubcitiId && !"null".equalsIgnoreCase(hubcitiId)) {
					hubcitiParam = Integer.parseInt(hubcitiId);
				}
				if (null != bandeventId && !"null".equalsIgnoreCase(bandeventId)) {
					bandevtIdParam = Integer.parseInt(bandeventId);
				}

				bandInfo = qrService.showBandEventDetails(bandevtIdParam, hubcitiParam);

				if (null != bandInfo) {
					session.setAttribute("bandevtinfo", bandInfo);
					viewName = "bandevent";
				

				request.setAttribute("AndroidAppID", bandInfo.getAndroidAppID());
				request.setAttribute("IOSAppID", bandInfo.getiOSAppID());
				request.setAttribute("URL", bandInfo.getQrURL());
				}

			} else {
				strMessage = ERRORINPUTMESSAGE;
				viewName = ERRORPAGE;
			}
			session.setAttribute(SESSIONVARNAME, strMessage);

		} catch (Exception e) {
			LOG.error("Exception occurred in showRetailerPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);

		}

		LOG.info("showRetailerPage:: Inside Exit Get Method");
		return viewName;

	}

	/**
	 * Below method is used to display band summary page.
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 * @throws ScanSeeQRServiceException
	 */

	@RequestMapping(value = "/3002.htm", method = RequestMethod.GET)
	public final String showBandInfoPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
		String viewName = "";
		final String bandId = Utility.checkEmpty(request.getParameter(BANDID));

		final String hubcitiId = Utility.checkEmpty(request.getParameter(HUBCITIID));
		Integer hubcitiParam = null;
		Integer bandIdParam = null;

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);
		Band bandInfo = null;
		ArrayList<Band> bandpageLst = null;
		String strMessage = "";

		try {

			if (null != bandId && null != hubcitiId) {

				if (null != hubcitiId && !"null".equalsIgnoreCase(hubcitiId)) {
					hubcitiParam = Integer.parseInt(hubcitiId);
				}
				if (null != bandId && !"null".equalsIgnoreCase(bandId)) {
					bandIdParam = Integer.parseInt(bandId);
				}

				bandInfo = qrService.showBandDetails(hubcitiParam, bandIdParam);

				bandpageLst = qrService.fetchBandAnythingpglst(bandIdParam, hubcitiParam);

				if (null != bandInfo) {
					session.setAttribute("bandinfo", bandInfo);
					viewName = "band";
				}

				if (null != bandpageLst && !bandpageLst.isEmpty()) {
					session.setAttribute("bandpglst", bandpageLst);

				}

				if(null != bandInfo)
				{
				request.setAttribute("AndroidAppID", bandInfo.getAndroidAppID());
				request.setAttribute("IOSAppID", bandInfo.getiOSAppID());
				request.setAttribute("URL", bandInfo.getQrURL());
				}

			} else {
				strMessage = ERRORINPUTMESSAGE;
				viewName = ERRORPAGE;
			}
			session.setAttribute(SESSIONVARNAME, strMessage);

		} catch (Exception e) {
			LOG.error("Exception occurred in showRetailerPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);

		}

		LOG.info("showRetailerPage:: Inside Exit Get Method");
		return viewName;

	}

	/**
	 * Below method is used to display band created pages.
	 * 
	 * @param band
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 * @throws ScanSeeQRServiceException
	 */

	@RequestMapping(value = "/showbandmap.htm", method = RequestMethod.POST)
	public final ModelAndView showBandMap(@ModelAttribute("bandform") Band band, HttpServletRequest request, HttpSession session, ModelMap model,
			HttpSession session1) throws ScanSeeQRServiceException {
		String strViewName = "bandmap";
		try {

			request.setAttribute("bandadress", band.getBandAddress());

		} catch (Exception e) {
			LOG.error("Exception occurred in showRetailerPage:::::" + e.getMessage());
			throw new ScanSeeQRServiceException(e);

		}

		LOG.info("showRetailerPage:: Inside Exit Get Method");
		return new ModelAndView(strViewName);

	}
	
	
	
	/**
	 * This method is used for displaying event web page.
	 * 
	 * @RequestMapping
	 * @param request
	 *            request
	 * @param session
	 *            session
	 * @param model
	 *            model
	 * @param session1
	 *            session1
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 * @return viewName viewName
	 */
	@RequestMapping(value = "/4000.htm", method = RequestMethod.GET)
	public final String getNewsDetailPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeQRServiceException {
	
		LOG.info("Inside QRCodeController : getNewsDetailPage");
		session.removeAttribute(SESSIONVARNAME);

		final String viewName = "newsdetail";
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final QRService qrService = (QRService) appContext.getBean(QRSERVICEVAR);

		final String strNewsId = Utility.checkEmpty(request.getParameter(NEWSID));
		Integer iNewsId = Integer.parseInt(strNewsId);
		
		Item objItem = null;

		try {
			
			objItem = qrService.getNewsDetail(iNewsId);

			if (null != objItem.getTitle()) {
				request.setAttribute("title", objItem.getTitle());
			}
			
			if (null != objItem.getImagePath()) {
				request.setAttribute("image", objItem.getImagePath());
			}

			if (null != objItem.getPubStrtDate()) {
				request.setAttribute("pubStrtDate", objItem.getPubStrtDate());
			}
			
			if (null != objItem.getPubTime()) {
				request.setAttribute("pubTime", objItem.getPubTime());
			}

			if (null != objItem.getShortDesc()) {
				request.setAttribute("shortDesc", objItem.getShortDesc());
			}
			
			if (null != objItem.getDescription()) {
				request.setAttribute("description", objItem.getDescription());
			}
			
			if (null != objItem.getVideoLink()) {
				request.setAttribute("videolink", objItem.getVideoLink());
			}
			
			if (null != objItem.getLink()) {
				request.setAttribute("link", objItem.getLink());
			}
			
			if (null != objItem.getFeedType()) {
				request.setAttribute("feedType", objItem.getFeedType());
			}

			request.setAttribute("AndroidAppID", objItem.getAndroidAppID());
			request.setAttribute("IOSAppID", objItem.getiOSAppID());
			request.setAttribute("URL", objItem.getQrURL());

		} catch (Exception e) {
			LOG.error("Exit QRCodeController : getNewsDetailPage : " + e.getMessage());
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info("Exit QRCodeController : getNewsDetailPage");
		return viewName;

	}
}

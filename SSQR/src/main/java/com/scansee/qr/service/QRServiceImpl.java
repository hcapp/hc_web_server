package com.scansee.qr.service;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.pojo.Event;

import com.scansee.qr.controller.QRCodeController;
import com.scansee.qr.dao.QRDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeQRServiceException;
import common.exception.ScanSeeQRWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.AppsiteMarkerDetails;
import common.pojo.Band;
import common.pojo.ButtonInfo;
import common.pojo.Coupon;
import common.pojo.EventMarker;
import common.pojo.EventMarkerDetails;
import common.pojo.Item;
import common.pojo.Media;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfo;
import common.pojo.ProductMedia;
import common.pojo.RetailerCreatedPage;
import common.pojo.RetailerPage;
import common.util.Utility;

/**
 * @author chandrasekaran_j
 */
public class QRServiceImpl implements QRService {

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(QRCodeController.class);

	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();
	/**
	 * QRDao variable.
	 */
	private QRDao qrDao;

	/**
	 * set QRDao.
	 * 
	 * @param qrDao
	 *            qrDao.
	 */
	public final void setQrDao(QRDao qrDao) {
		this.qrDao = qrDao;
	}

	/**
	 * used to get retailer web page info.
	 * 
	 * @param retailerId
	 *            retailerId.
	 * @param pageId
	 *            pageId.
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return retailerpageList retailerpageList.
	 */
	public final RetailerPage getRetailerPageInfo(Integer hubCitiID, Integer retailerId, Integer retlocationId) throws ScanSeeQRServiceException {
		RetailerPage retailerpage = null;
		final String methodName = "getRetailerPageInfo";
		LOG.info("In service.." + ApplicationConstants.METHODSTART + methodName);
		try {
			retailerpage = qrDao.getRetailerPageInfoList(hubCitiID, retailerId, retlocationId);

		} catch (ScanSeeQRWebSqlException exception) {
			throw new ScanSeeQRServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service..." + ApplicationConstants.METHODEND + methodName);
		return retailerpage;
	}

	/**
	 * used to get retailer created info and offer info.
	 * 
	 * @param retailerId
	 *            retailerId.
	 * @param retailLocationid
	 *            retailLocationid.
	 * @param pageId
	 *            pageId.
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return retailerpage retailerpage.
	 */
	public final RetailerCreatedPage getRetailerLandingPage(Integer hubcitiId, Integer retailerId, Integer retailLocationid, Integer pageId,
			Integer mmID, Integer scanTypID, String pageType, String source) throws ScanSeeQRServiceException {
		final String methodName = "getRetailerLandingPage service";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerCreatedPage retailerpage = null;
		String startDate = "";
		String endDate = "";
		String startTime = "";
		String endTime = "";
		String fileStr = "";
		int ind = 0;
		try {
			retailerpage = qrDao.getRetailerLandingPage(hubcitiId, retailerId, retailLocationid, pageId, mmID, scanTypID, pageType, source);
			if (null != retailerpage) {
				if (!retailerpage.isExternalLinkUrlFlag()) {

					if (null != retailerpage.getStartDate() || null != retailerpage.getEndDate()) {
						if (!pageType.equalsIgnoreCase(ApplicationConstants.SPECIALOFFERPAGE)) {
							startDate = Utility.getUsDateFormatwithoutTime(retailerpage.getStartDate());
							String start = "start time:";
							LOG.info(start + startTime);
							endDate = Utility.getUsDateFormatwithoutTime(retailerpage.getEndDate());
							startTime = Utility.getUsDateFormatwithoutDate(retailerpage.getStartDate());
							endTime = Utility.getUsDateFormatwithoutDate(retailerpage.getEndDate());
							retailerpage.setStartTime(startTime);
							retailerpage.setEndTime(endTime);
							retailerpage.setStartDate(startDate);
							retailerpage.setEndDate(endDate);
						}
					}
					// Split up media and its name
					final String media = retailerpage.getMediaPath();
					final String[] pathArray = media.split(",");
					Media mediaObject = null;
					final List<Media> mediaList = new ArrayList<Media>();
					for (int i = 0; i < pathArray.length; i++) {
						mediaObject = new Media();
						fileStr = pathArray[i];
						ind = fileStr.lastIndexOf("/");
						if (ind > 0) {
							fileStr = fileStr.substring(fileStr.lastIndexOf("/") + 1, fileStr.length());
							mediaObject.setFileName(fileStr);
							mediaObject.setFilePath(pathArray[i]);
							if (null != fileStr && !fileStr.isEmpty()) {
								mediaList.add(mediaObject);
							}
						}

					}
					retailerpage.setMediaList(mediaList);
				}
			}
		} catch (ScanSeeQRWebSqlException exception) {
			throw new ScanSeeQRServiceException(exception.getMessage(), exception);
		} catch (ParseException exception) {
			LOG.error("Parse error occured Inside getRetailerLandingPage : showPage : " + exception.getMessage());
		}
		LOG.info("In Service." + ApplicationConstants.METHODEND + methodName);
		return retailerpage;

	}

	/**
	 * used to get product info.
	 * 
	 * @param productId
	 *            productId.
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return productInfo productInfo.
	 */
	public final ProductInfo getProductInfo(Integer hubcitiId, int productId) throws ScanSeeQRServiceException {
		final String methodName = "getProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductInfo productInfo = null;
		try {
			productInfo = qrDao.getProductInfo(hubcitiId, productId);
		} catch (ScanSeeQRWebSqlException exception) {
			throw new ScanSeeQRServiceException(exception.getMessage(), exception);
		}
		return productInfo;

	}

	/**
	 * used to get product attributes list.
	 * 
	 * @param productId
	 *            productId.
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return prodAttributeList prodAttributeList.
	 */
	public final ArrayList<ProductAttributes> getProductAttributes(int productId) throws ScanSeeQRServiceException {
		final String methodName = "getProductAttributes";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductAttributes> prodAttributeList = null;
		try {
			prodAttributeList = qrDao.getProductAttributesList(productId);
		} catch (ScanSeeQRWebSqlException exception) {
			throw new ScanSeeQRServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return prodAttributeList;
	}

	/**
	 * used to get product media list.
	 * 
	 * @param productId
	 *            productId.
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return prodMediaList prodMediaList.
	 */
	public final ArrayList<ProductMedia> getProductMedia(int productId) throws ScanSeeQRServiceException {
		final String methodName = "getProductMedia";
		LOG.info("In service......" + ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductMedia> prodMediaList = null;
		try {
			prodMediaList = qrDao.getProductMediaList(productId);

		} catch (ScanSeeQRWebSqlException exception) {
			throw new ScanSeeQRServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service....." + ApplicationConstants.METHODEND + methodName);
		return prodMediaList;
	}

	/**
	 * used to get retailer web page info.
	 * 
	 * @param retailerId
	 *            retailerId.
	 * @param pageId
	 *            pageId.
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return retailerpageList retailerpageList.
	 */
	public final ArrayList<RetailerPage> getRetailerCreatedPageLst(Integer hubcitiId, Integer retailerId, Integer retlocationId)
			throws ScanSeeQRServiceException {
		ArrayList<RetailerPage> retCreatedPageLst = null;
		final String methodName = "getRetailerPageInfo";
		LOG.info("In service.." + ApplicationConstants.METHODSTART + methodName);
		try {
			retCreatedPageLst = qrDao.getRetailerCretedPages(hubcitiId, retailerId, retlocationId);

		} catch (ScanSeeQRWebSqlException exception) {
			throw new ScanSeeQRServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service..." + ApplicationConstants.METHODEND + methodName);
		return retCreatedPageLst;
	}

	/**
	 * Service method to get Giveaway page details
	 * 
	 * @param retailerId
	 *            as a parameter.
	 * @param pageId
	 *            as a parameter.
	 * @param mainMenuID
	 *            as a parameter.
	 * @param scanTypeID
	 *            as a parameter.
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return RetailerCreatedPage retailerCreatedPage.
	 */
	public RetailerCreatedPage getGiveawayPageInfo(int retailerId, int pageId, int mainMenuID, int scanTypeID) throws ScanSeeQRServiceException {
		final String methodName = "getGiveawayPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerCreatedPage retailerpage = null;
		String startDate = "";
		String endDate = "";
		String startTime = "";
		String endTime = "";
		try {
			retailerpage = qrDao.getGiveawayPageInfo(retailerId, pageId, mainMenuID, scanTypeID);
			if (null != retailerpage) {
				if (null != retailerpage.getStartDate() || null != retailerpage.getEndDate()) {
					startDate = Utility.getUsDateFormatwithoutTime(retailerpage.getStartDate());
					String start = "start time:";
					LOG.info(start + startTime);
					endDate = Utility.getUsDateFormatwithoutTime(retailerpage.getEndDate());
					startTime = Utility.getUsDateFormatwithoutDate(retailerpage.getStartDate());
					endTime = Utility.getUsDateFormatwithoutDate(retailerpage.getEndDate());
					retailerpage.setStartTime(startTime);
					retailerpage.setEndTime(endTime);
					retailerpage.setStartDate(startDate);
					retailerpage.setEndDate(endDate);

				}

				retailerpage.setTermsandConditions(retailerpage.getTermsandConditions() + ApplicationConstants.TERMSANDCONDITIONS);

			}
		} catch (ScanSeeQRWebSqlException exception) {
			LOG.error("Error occurred in getGiveawayPageInfo::" + exception);
			throw new ScanSeeQRServiceException();
		} catch (ParseException exception) {
			LOG.error("Error occurred in getGiveawayPageInfo::" + exception);
			throw new ScanSeeQRServiceException();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerpage;
	}

	/**
	 * Service method to get Application configuration for App Download link.
	 * 
	 * @param configType
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return appConfigurationList .
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeQRServiceException {
		final String methodName = "getAppConfig";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfig = null;
		try {
			appConfig = qrDao.getAppConfig(configType);
		}

		catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getAppConfig : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return appConfig;
	}

	public Event getEventDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRServiceException {

		final String methodName = "getEventDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Event event = null;

		try {

			event = qrDao.getEventDetails(eventId, hubcitiId);

			if (null != event) {
				if (null != event.getEventStartDate() && !"".equals(event.getEventStartDate())) {
					String startDate = Utility.getDateFormat(event.getEventStartDate());
					event.setEventStartDate(startDate);
				}
				if (null != event.getEventEndDate() && !"".equals(event.getEventEndDate())) {
					String endDate = Utility.getDateFormat(event.getEventEndDate());
					event.setEventEndDate(endDate);
				}
			}

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getEventDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		} catch (ParseException e) {
			LOG.error("Exception occured in : getEventDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	public Event getFundraiserDetails(Integer fundraiserId, Integer hubcitiId) throws ScanSeeQRServiceException {

		final String methodName = "getFundraiserDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Event event = null;

		try {

			event = qrDao.getFundraiserDetails(fundraiserId, hubcitiId);
			if (null != event) {
				if (null != event.getEventDate() && !"".equals(event.getEventDate())) {
					String startDate = Utility.getDateFormat(event.getEventDate());
					event.setEventDate(startDate);
				}
				if (null != event.getEventEDate() && !"".equals(event.getEventEDate())) {
					String endDate = Utility.getDateFormat(event.getEventEDate());
					event.setEventEDate(endDate);
				}
			}

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getFundraiserDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		} catch (ParseException e) {
			LOG.error("Exception occured in : getFundraiserDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	public ArrayList<Event> getEventAppSites(Integer eventId) throws ScanSeeQRServiceException {

		final String methodName = "getEventAppSites";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> event = null;

		try {
			event = qrDao.getEventAppSites(eventId);
		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getEventAppSites : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	public ArrayList<Event> getFundraiserEvents(Integer fundraiserId) throws ScanSeeQRServiceException {

		final String methodName = "getFundraiserEvents";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> events = null;

		try {
			events = qrDao.getFundraiserEvents(fundraiserId);
			if (null != events && !events.isEmpty()) {
				for (Event event : events) {
					if (null != event) {
						if (null != event.getEventStartDate() && !"".equals(event.getEventStartDate())) {
							String startDate = Utility.getDateFormat(event.getEventStartDate());
							event.setEventStartDate(startDate);
						}
						if (null != event.getEventEndDate() && !"".equals(event.getEventEndDate())) {
							String endDate = Utility.getDateFormat(event.getEventEndDate());
							event.setEventEndDate(endDate);
						}
					}
				}
			}

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getFundraiserEvents : " + e);
			throw new ScanSeeQRServiceException(e);
		} catch (ParseException e) {
			LOG.error("Exception occured in : getFundraiserEvents : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return events;
	}

	public Event getEventLogistics(Integer eventId, Integer hubcitiId) throws ScanSeeQRServiceException {

		final String methodName = "getEventLogistics";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Event event = null;
		String newURL = null;

		try {

			event = qrDao.getEventLogistics(eventId, hubcitiId);

			if (null != event) {
				for (ButtonInfo info : event.getButtonInfos()) {
					if (null != info.getButtonLink() && !"".equals(info.getButtonLink())) {
						if (info.getButtonLink().startsWith(ApplicationConstants.WWW)) {
							newURL = ApplicationConstants.HTTP + info.getButtonLink();
							info.setButtonLink(newURL);
						}
					}
				}
			}

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getEventLogistics : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	public ArrayList<RetailerPage> getSpecialOfferRetLocs(Integer retailId, Integer pageId) throws ScanSeeQRServiceException {
		final String methodName = "getSpecialOfferRetLocs";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<RetailerPage> retailerPages = null;

		try {
			retailerPages = qrDao.getSpecialOfferRetLocs(retailId, pageId);
		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getSpecialOfferRetLocs : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerPages;
	}

	public EventMarkerDetails getEventLogisticsMap(Integer eventId, Integer hubcitiId) throws ScanSeeQRServiceException {

		final String methodName = "getEventLogisticsMap";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		EventMarkerDetails eventMarkerDetails = null;

		try {

			eventMarkerDetails = qrDao.getEventLogisticsMap(eventId, hubcitiId);

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getEventLogisticsMap : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return eventMarkerDetails;
	}

	public Coupon getCouponDetails(Integer couponId, Integer hubcitiId) throws ScanSeeQRServiceException {

		final String methodName = "getCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Coupon coupon = null;

		try {

			coupon = qrDao.getCouponDetails(couponId, hubcitiId);
			if (null != coupon) {
				if (null != coupon.getCouponStartDate() && !"".equals(coupon.getCouponStartDate())) {
					String startDate = Utility.getDateFormat(coupon.getCouponStartDate());
					coupon.setCouponStartDate(startDate);
				}
				if (null != coupon.getCouponExpireDate() && !"".equals(coupon.getCouponExpireDate())) {
					String endDate = Utility.getDateFormat(coupon.getCouponExpireDate());
					coupon.setCouponExpireDate(endDate);
				}
			}

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getCouponDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		} catch (ParseException e) {
			LOG.error("Exception occured in : getCouponDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return coupon;
	}

	public RetailerPage getHotdealsDetails(Integer hotdealId, Integer hubcitiId) throws ScanSeeQRServiceException {
		final String methodName = "getHotdealsDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerPage retailerpage = null;

		try {

			retailerpage = qrDao.getHotdealInfo(hotdealId, hubcitiId);

			if (null != retailerpage.gethDStartDate() && !"".equals(retailerpage.gethDStartDate())) {
				String startDate = Utility.getDateFormat(retailerpage.gethDStartDate());
				retailerpage.sethDStartDate(startDate);
			}
			if (null != retailerpage.gethDEndDate() && !"".equals(retailerpage.gethDEndDate())) {
				String endDate = Utility.getDateFormat(retailerpage.gethDEndDate());
				retailerpage.sethDEndDate(endDate);
			}

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getHotdealsDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		} catch (ParseException e) {
			LOG.error("Exception occured in : getHotdealsDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerpage;
	}

	public ArrayList<Event> getHotdealLocs(Integer eventId) throws ScanSeeQRServiceException {
		final String methodName = "getHotdealLocs";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Event> event = null;

		try {
			event = qrDao.getHotdealLocs(eventId);
		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getHotdealLocs : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	/**
	 * Below method is used to fetch marker points for appsite
	 * 
	 * @param logisticId
	 *            , hubCitiId.
	 * @return marker points
	 */
	public AppsiteMarkerDetails showAppsiteLogisticPage(Integer logisticId, Integer hubCitiId) throws ScanSeeQRServiceException {

		final String strMethodName = "showAppsiteLogisticPage";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		AppsiteMarkerDetails appsiteMarkerDetails = null;
		try {

			appsiteMarkerDetails = qrDao.showAppsiteLogisticPage(logisticId, hubCitiId);

		} catch (ScanSeeQRWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURED + strMethodName + exception.getMessage());
			throw new ScanSeeQRServiceException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return appsiteMarkerDetails;
	}

	public Event getBandEventDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRServiceException {

		final String methodName = "getEventDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Event event = null;

		try {

			event = qrDao.getEventDetails(eventId, hubcitiId);

			if (null != event) {
				if (null != event.getEventStartDate() && !"".equals(event.getEventStartDate())) {
					String startDate = Utility.getDateFormat(event.getEventStartDate());
					event.setEventStartDate(startDate);
				}
				if (null != event.getEventEndDate() && !"".equals(event.getEventEndDate())) {
					String endDate = Utility.getDateFormat(event.getEventEndDate());
					event.setEventEndDate(endDate);
				}
			}

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : getEventDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		} catch (ParseException e) {
			LOG.error("Exception occured in : getEventDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return event;
	}

	/**
	 * to display band event details.
	 */
	public Band showBandEventDetails(Integer bandEvtId, Integer hubCitiId) throws ScanSeeQRServiceException {

		final String methodName = "showBandEventDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Band bndEvent = null;

		try {

			bndEvent = qrDao.showBandEventDetails(bandEvtId, hubCitiId);

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : showBandEventDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return bndEvent;
	}

	/**
	 * to display band details.
	 */
	public Band showBandDetails(Integer hubCitiId, Integer bandEvtId) throws ScanSeeQRServiceException {

		final String methodName = "showBandDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Band bandInfo = null;

		try {

			bandInfo = qrDao.showBandDetails(hubCitiId, bandEvtId);

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : showBandDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return bandInfo;
	}

	/**
	 * to display band created pages details.
	 */

	public ArrayList<Band> fetchBandAnythingpglst(Integer bandEvtId, Integer hubCitiId) throws ScanSeeQRServiceException {
		final String methodName = "showBandDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Band> bandlst = null;

		try {

			bandlst = qrDao.fetchBandAnythingpglst(bandEvtId, hubCitiId);

		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Exception occured in : showBandDetails : " + e);
			throw new ScanSeeQRServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return bandlst;
	}
	
	
	public Item getNewsDetail(Integer iNewsId) throws ScanSeeQRServiceException {
		
		LOG.info("Inside QRServiceImpl : getNewsDetail");
		Item objItem = null;

		try {
			objItem = qrDao.getNewsDetail(iNewsId);
		} catch (ScanSeeQRWebSqlException e) {
			LOG.error("Inside QRServiceImpl : getNewsDetail : " + e);
			throw new ScanSeeQRServiceException(e);
		}
		
		LOG.info("Exit QRServiceImpl : getNewsDetail");
		return objItem;
	}

}

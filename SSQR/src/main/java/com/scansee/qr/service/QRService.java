package com.scansee.qr.service;

import java.util.ArrayList;

import common.exception.ScanSeeQRServiceException;
import common.pojo.AppConfiguration;
import common.pojo.AppsiteMarkerDetails;
import common.pojo.Band;
import common.pojo.Coupon;
import common.pojo.Event;
import common.pojo.EventMarkerDetails;
import common.pojo.Item;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfo;
import common.pojo.ProductMedia;
import common.pojo.RetailerCreatedPage;
import common.pojo.RetailerPage;

/**
 * @author chandrasekaran_j
 */
public interface QRService {

	/**
	 * get retailer web page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param pageId
	 *            pageId.
	 * @return RetailerPage ArrayList
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 */
	RetailerPage getRetailerPageInfo(Integer hubCitiID, Integer retailerId, Integer retLocationId) throws ScanSeeQRServiceException;

	/**
	 * get retailer created web page info and offer page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param retailLocationid
	 *            retailLocationid
	 * @param pageId
	 *            pageId
	 * @return RetailerCreatedPage RetailerCreatedPage
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 */
	RetailerCreatedPage getRetailerLandingPage(Integer hubcitiId, Integer retailerId, Integer retailLocationid, Integer pageId, Integer mmID,
			Integer scanTypID, String pageType, String source) throws ScanSeeQRServiceException;

	/**
	 * get product details info.
	 * 
	 * @param productId
	 *            productId
	 * @return ProductInfo ProductInfo
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 */
	ProductInfo getProductInfo(Integer hubcitiId, int productId) throws ScanSeeQRServiceException;

	/**
	 * get product attributes info.
	 * 
	 * @param productId
	 *            productId
	 * @return ArrayList ProductAttributes
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 */
	ArrayList<ProductAttributes> getProductAttributes(int productId) throws ScanSeeQRServiceException;

	/**
	 * get product media info.
	 * 
	 * @param productId
	 *            productId
	 * @return ArrayList ProductMedia
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 */
	ArrayList<ProductMedia> getProductMedia(int productId) throws ScanSeeQRServiceException;

	/**
	 * get retailer web page info.
	 * 
	 * @param retailerId
	 *            retailerId
	 * @param pageId
	 *            pageId.
	 * @return RetailerPage ArrayList
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException
	 */
	ArrayList<RetailerPage> getRetailerCreatedPageLst(Integer hubcitiId, Integer retailerId, Integer retLocationId) throws ScanSeeQRServiceException;

	/**
	 * Service method to get Giveaway page details
	 * 
	 * @param retailerId
	 *            as a parameter.
	 * @param pageId
	 *            as a parameter.
	 * @param mainMenuID
	 *            as a parameter.
	 * @param scanTypeID
	 *            as a parameter.
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return RetailerCreatedPage retailerCreatedPage.
	 */
	RetailerCreatedPage getGiveawayPageInfo(int retailerId, int pageId, int mainMenuID, int scanTypeID) throws ScanSeeQRServiceException;

	/**
	 * Service method to get Application configuration for App Download link.
	 * 
	 * @param configType
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return appConfigurationList .
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeQRServiceException;

	/**
	 * Service method to get event details.
	 * 
	 * @param eventId
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return Event .
	 */
	public Event getEventDetails(Integer eventId, Integer hubcitiId) throws ScanSeeQRServiceException;

	/**
	 * Service method to get Fundraiser details.
	 * 
	 * @param fundraiserId
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return Event .
	 */
	public Event getFundraiserDetails(Integer fundraiserId, Integer hubcitiId) throws ScanSeeQRServiceException;

	public ArrayList<Event> getEventAppSites(Integer eventId) throws ScanSeeQRServiceException;

	public ArrayList<Event> getFundraiserEvents(Integer fundraiserId) throws ScanSeeQRServiceException;

	/**
	 * Service method to get event Logistics details.
	 * 
	 * @param eventId
	 * @throws ScanSeeQRServiceException
	 *             ScanSeeQRServiceException.
	 * @return Event .
	 */
	public Event getEventLogistics(Integer eventId, Integer hubcitiId) throws ScanSeeQRServiceException;

	public ArrayList<RetailerPage> getSpecialOfferRetLocs(Integer retailId, Integer pageId) throws ScanSeeQRServiceException;

	public EventMarkerDetails getEventLogisticsMap(Integer eventId, Integer hubcitiId) throws ScanSeeQRServiceException;

	public RetailerPage getHotdealsDetails(Integer hotdealId, Integer hubcitiId) throws ScanSeeQRServiceException;

	public ArrayList<Event> getHotdealLocs(Integer eventId) throws ScanSeeQRServiceException;

	public Coupon getCouponDetails(Integer couponId, Integer hubcitiId) throws ScanSeeQRServiceException;

	/**
	 * Below method is used to fetch marker points for appsite.
	 * 
	 * @param logisticId
	 * @param hubCitiId
	 * @return AppsiteMarkerDetails
	 * @throws ScanSeeQRServiceException
	 */
	public AppsiteMarkerDetails showAppsiteLogisticPage(Integer logisticId, Integer hubCitiId) throws ScanSeeQRServiceException;

	/**
	 * To display band event details.
	 * 
	 * @param bandEvtId
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeQRServiceException
	 */
	public Band showBandEventDetails(Integer bandEvtId, Integer hubCitiId) throws ScanSeeQRServiceException;

	/**
	 * To display band summary page.
	 * 
	 * @param bandEvtId
	 * @param hubCitiId
	 * @return
	 * @throws ScanSeeQRServiceException
	 */
	public Band showBandDetails(Integer bandEvtId, Integer hubCitiId) throws ScanSeeQRServiceException;

	/**
	 * To display band created pages.
	 * 
	 * @param eventId
	 * @param hubcitiId
	 * @return
	 * @throws ScanSeeQRServiceException
	 */
	public ArrayList<Band> fetchBandAnythingpglst(Integer eventId, Integer hubcitiId) throws ScanSeeQRServiceException;
	
	/**
	 * Service method to get News detail.
	 * 
	 * @param iNewsId
	 * @throws ScanSeeQRServiceException
	 *           
	 * @return News Detail .
	 */
	Item getNewsDetail(Integer iNewsId) throws ScanSeeQRServiceException;

}

function isEmpty(txt)
{
	
	//alert("hi");
	if(txt.length == 0 || !txt){
		return true;
		}else
			return false;
		
}

function isNotEmpty(txt, msg) {
	//alert("inside empty");
    if (txt != "") {
        spa = " ";
        for (var i = 0; i < txt.length; i++)
        {
            if (txt.charAt(i) != spa)
                return true;
        }
    }else
        alert(msg + " should not be empty.");
    return false;
}


/*function Space(text){
	 var strField = text;
	 var i;
	 for (i = 0; i < strField.length; i++)
	 {
	     if (strField.charAt(i) != " ")
	     {
	         break;
	     }
	 }

	 if (i == strField.length)
	 {
	     return(true);
	 }
	 else
	 {
	     return(false);
	 }
	}
*/

function trim(str) {
	  var newstr;
	  newstr = str.replace(/^\s*/, "").replace(/\s*$/, "");
	  newstr = newstr.replace(/\s{2,}/, " ");
	  return newstr;
	} 

/*function isNumeric(elem ,msg){
	//alert("inside numeric");
	if(isNotEmpty(elem ,msg)){
	var numericExpression = /^[0-9]+$/;
	if(elem.match(numericExpression)){
		return true;
	}else
		alert(msg + " Should be numeric only");
		elem.focus();
		return false;
	}
}*/

function compareDate(from,to,flag){
	var mon1  = parseInt(from.substring(0,2),10); 
	var dt1  = parseInt(from.substring(3,5),10);
	var yr1   = parseInt(from.substring(6,10),10); 
	var mon2  = parseInt(to.substring(0,2),10); 
	var dt2  = parseInt(to.substring(3,5),10); 
	var yr2   = parseInt(to.substring(6,10),10); 
	var date1 = new Date(yr1, mon1, dt1); 
	var date2 = new Date(yr2, mon2, dt2); 
	var d = new Date();
	var curr_date = d.getDate();
	var curr_month = d.getMonth();
	curr_month = curr_month + 1;
	var curr_year = d.getFullYear();
	var date3 =  new Date(curr_year,curr_month,curr_date);
	if(flag == 'yes'){
	if(date1 < date3){
		alert("Start Date should be greater than or equal to current date");
		return false;
	}
	}
	if(date2 < date1)
	{
	   alert("start date cannot be greater than  end date");
	   return false; 
	} 
	else 
	{ 
	   return true;
	} 
}

function modeSelection(elem,msg){
	if(elem == 0){
		alert("Please choose " +  msg);
		return false;
	}else{
		return true;
	}
}


function isDecimal(str,msg){
	if(isNotEmpty(str ,msg)){
    if(isNaN(str) || str.indexOf(".")<0){
    	 alert(msg +" should be in decimal format");  
          return false;
    }else if(str == 0.0){
    	 alert("Please Enter " + msg);
    	 return false;
    	 }
    else{
         return true;
    }
	}
}

function checkDate(value){

    // define date string to test
   // var txtDate = document.getElementById('txtDate').value;
    // check date and print message
    if (isDate(value)) {
        return true;
    }
    else {
     return false;
    }
}

function isDate(txtDate) {
    var objDate,  // date object initialized from the txtDate string
        mSeconds, // txtDate in milliseconds
        day,      // day
        month,    // month
        year;     // year
    // date length should be 10 characters (no more no less)
    if (txtDate.length !== 10) {
        return false;
    }
      //alert("pp"+txtDate.substring(2, 3));

    if(txtDate.match("/")){
        // third and sixth character should be '/'
        if (txtDate.substring(2, 3) !== '/' || txtDate.substring(5, 6) !== '/') {
            return false;
        }
    }
    if(txtDate.match("-")){
        // third and sixth character should be '/'
        if (txtDate.substring(2, 3) !== '-' || txtDate.substring(5, 6) !== '-') {
            return false;
        }
    }

    // extract month, day and year from the txtDate (expected format is mm/dd/yyyy)
    // subtraction will cast variables to integer implicitly (needed
    // for !== comparing)
    month = txtDate.substring(0, 2) - 1; // because months in JS start from 0
    day = txtDate.substring(3, 5) - 0;
    year = txtDate.substring(6, 10) - 0;

   /* alert("month"+month);
    alert("day"+day);
    alert("year"+year);*/
    // test year range
   // if (year < 1000 || year > 3000) {
    //    return false;
  //  }
    // convert txtDate to milliseconds
    mSeconds = (new Date(year, month, day)).getTime();
    // initialize Date() object from calculated milliseconds
    objDate = new Date();
    objDate.setTime(mSeconds);
    // compare input date and parts from Date() object
    // if difference exists then date isn't valid
    if (objDate.getFullYear() !== year ||
        objDate.getMonth() !== month ||
        objDate.getDate() !== day) {
		alert("The entered date is not in proper format");
        return false;
    }
    // otherwise return true
    return true;
}

function validateAmt(value,string)
{
 var pattern = /^\d+.?\d*$/;
 if(value==""){
  alert("Please enter "+string+".");
  return false;
 }else if ( value.match(pattern)==null ){
  alert(string+' is not valid.');
  return false;
 }else if(value < 0 || value == 0)
	 alert(string+' cannot be zero or less'); 
 else{
  return true;
 }
}

function isValidURL(url){
    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	if (url != "") {
		   if(RegExp.test(url)){
			return true;
			}else{
			alert("Please enter a valid URL");
			return false;
			}
	}else{ return true;}
}


function noCTRL(e){
	var code = (document.all) ? event.keyCode:e.which;
	var ctrl = (document.all) ? event.ctrlKey:e.modifiers & Event.CONTROL_MASK;
	//var msg = "Sorry, this functionality is not allowed.";

	if (document.all)
	{
		if (ctrl && code==86) {
			//CTRL + V (Paste)
			//alert(msg);
			window.event.returnValue = false;
		} else if (ctrl && code==67) {
			//CTRL + C (Copy)
			//alert(msg);
			window.event.returnValue = false;
		}
	} else {
		if (ctrl==2) { //CTRL key
		//alert(msg);
		return false;
		}
	}
} 
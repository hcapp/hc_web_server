/**
 * 
 */
// check for on key press
function validateFind(event) {

	if (event && event.keyCode == 13) {

		var searchKey = document.SearchForm.searchKey.value;
		var zipCode = document.SearchForm.zipCode.value;

		if ("" == searchKey && zipCode == 'Zipcode') {
			alert('Please enter search key or zipcode');

		} else {

			var getUsrOptn = $("input[name='srchOptns']:checked").attr("value");
			if (getUsrOptn == "Products") {

				findprodsearch('');
				// window.location.href=findprodsearch.;
			}
			if (getUsrOptn == "Locations") {

				var categoryName = $('input:radio[name=catName]:checked').val();
				document.SearchForm.categoryName.value = categoryName;
				document.SearchForm.action = "/ScanSeeWeb/shopper/findresultloctnlist.htm";
				document.SearchForm.method = "POST";
				document.SearchForm.submit();

			}
			if (getUsrOptn == "Deals") {

				finddealssearch('');
				// window.location.href="deals.html"
			}
		}
	}
}

// check for valid numeric values
function IsValidNumeric(e) {
	var key = window.event ? e.keyCode : e.which;
	var keychar = String.fromCharCode(key);
	reg = /[0-9]+(\.[0-9])?/;
	return reg.test(keychar);
}

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 47 && charCode < 58) || charCode < 31)
		return true;

	return false;
}

/**
 * This method for calling product manual search function in find module.
 */
function findprodsearch(event) {

	var zipcode = document.SearchForm.zipCode.value;
	var searchKey = document.SearchForm.searchKey.value;

	if (event && event.which == 13) {

		if (searchKey == '') {
			alert("Please Enter keyword to search");
			return true;

		}

		document.SearchForm.action = "consfindprodsearch.htm";
		document.SearchForm.method = "POST";
		document.SearchForm.submit();
	} else if (event == '') {

		if (searchKey == '') {
			alert("Please Enter keyword to search");
			return true;

		}
		// if (zipcode == '' || zipcode == 'Zipcode') {
		// alert("Note: Please Enter Zipcode");
		// return true; }
		document.SearchForm.action = "consfindprodsearch.htm";
		document.SearchForm.method = "POST";
		document.SearchForm.submit();

	} else {
		return true;
	}

}

/**
 * This method for calling product summary method in find module.
 */
function consprodsummary(productId) {

	document.FindProdLstForm.productId.value = productId;
	document.FindProdLstForm.action = "consfindprodinfo.htm";
	document.FindProdLstForm.method = "POST";
	document.FindProdLstForm.submit();

}
/**
 * This method for calling deals search method in find module.
 */
function finddealssearch(event) {

	var zipcode = document.SearchForm.zipCode.value;

	if (event && event.which == 13) {

		document.SearchForm.action = "consdealssearch.htm";
		document.SearchForm.method = "POST";
		document.SearchForm.submit();

	} else if (event == '') {
		document.SearchForm.action = "consdealssearch.htm";
		document.SearchForm.method = "POST";
		document.SearchForm.submit();

	} else {
		return true;
	}
}

/**
 * This method for calling product share method in find module.
 */
function consprodfacebookshare(productId, productListID) {
	var redirectUrl = 'findprodlist';
	document.FindProdLstForm.productListID.value = productListID;
	document.FindProdLstForm.productId.value = productId;
	document.FindProdLstForm.redirectUrl.value = redirectUrl;
	document.FindProdLstForm.action = "fbShareProductInfo.htm";
	document.FindProdLstForm.method = "POST";
	document.FindProdLstForm.submit();

}
function prodSummaryfacebookshare(productId, productListID) {
	var redirectUrl = 'findprodsummary';
	document.prodsummaryform.productId.value = productId;
	document.prodsummaryform.productListID.value = productListID;
	document.prodsummaryform.redirectUrl.value = redirectUrl;
	document.prodsummaryform.action = "fbShareProductInfo.htm";
	document.prodsummaryform.method = "POST";
	document.prodsummaryform.submit();

}

function hotDealfbshare(hotDealId, frompage) {
	var redirectUrl = frompage;
	document.dealsearchform.hotDealId.value = hotDealId;
	document.dealsearchform.redirectUrl.value = redirectUrl;
	document.dealsearchform.action = "fbShareHotDealInfo.htm";
	document.dealsearchform.method = "POST";
	document.dealsearchform.submit();

}

function gethdinfo(hotDealId) {
	var redirectUrl = 'dealssearch';
	document.dealsearchform.hotDealId.value = hotDealId;
	document.dealsearchform.redirectUrl.value = redirectUrl;
	document.dealsearchform.action = "conshddetails.htm";
	document.dealsearchform.method = "POST";
	document.dealsearchform.submit();

}
function shareProdInfoViaEmail(productId, productListId) {

	var toEmailId = $("input[name='toEmail']").val();

	var fromEmailId = $("input[name='fromEmail']").val();

	var regEx = /^.+@.+\..{2,5}$/;

	if (toEmailId == "" && fromEmailId == "") {
		alert("Please Enter Valid EmailIds");
		return true;
	} else if (!regEx.test(fromEmailId)) {
		alert("Please Enter Valid From EmailId");
		return true;
	} else if (!regEx.test(toEmailId)) {
		alert("Please Enter Valid  To EmailId");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "shareprodinfoviaemail.htm",
			data : {
				'toEmailId' : toEmailId,
				'fromEmailId' : fromEmailId,
				'productId' : productId,
				'productListId' : productListId
			},
			success : function(response) {

				if (response == 'Email sent successfully.') {

					alert(response);
					$(".cncl-actn").click();
				} else {

					alert('Error occurred while sending mail');
					$(".cncl-actn").click();
				}

			},

			error : function(xhr, status, error) {
				alert(status);
			}

		});

	}
}
function saveUserProdRating(productId) {

	$.ajaxSetup({
		cache : false
	});
	var userRating = $('input:radio[name=Rating]:checked').val();

	$.ajax({
		type : "GET",
		url : "saveuserprodrating.htm",
		data : {
			'userRating' : userRating,
			'productId' : productId
		},

		success : function(response) {

			var fields = response.split('&');
			var avgRating = fields[1];
			$("span.valText").text(avgRating);
		},
		error : function(e) {
			alert(e);
		}
	});

}

function deleteUserProdRating(productId) {

	$.ajaxSetup({
		cache : false
	});
	$.ajax({
		type : "GET",
		url : "saveuserprodrating.htm",
		data : {
			'userRating' : 0,
			'productId' : productId
		},

		success : function(response) {

			var fields = response.split('&');
			var avgRating = fields[1];
			$("span.valText").text(avgRating);
		},
		error : function(e) {
		}
	});
}

function findprodlstsearch() {

	document.consfindprodfrom.action = "consfindprodsearch.htm";
	document.consfindprodfrom.method = "POST";
	document.consfindprodfrom.submit();

}
function findprodlstdisplay() {

	document.FindProdLstForm.action = "consfindprodsearch.htm";
	document.FindProdLstForm.method = "POST";
	document.FindProdLstForm.submit();

}
function findprodsearchhome() {
	document.FindProdLstForm.action = "consfindhome.htm";
	document.FindProdLstForm.method = "GET";
	document.FindProdLstForm.submit();
}
function finddealsearchhome() {

	document.dealsearchform.action = "consfindhome.htm";
	document.dealsearchform.method = "GET";
	document.dealsearchform.submit();
}

function findpaginationdealssearch() {
	document.dealsearchform.action = "consdealssearch.htm";
	document.dealsearchform.method = "POST";
	document.dealsearchform.submit();
}
function shareHotdealInfoViaEmail(hotdealId) {

	var toEmailId = $("input[name='toEmail']").val();

	var fromEmailId = $("input[name='fromEmail']").val();

	var regEx = /^.+@.+\..{2,5}$/;

	if (toEmailId == "" && fromEmailId == "") {
		alert("Please Enter Valid EmailIds");
		return true;
	} else if (!regEx.test(fromEmailId)) {
		alert("Please Enter Valid From EmailId");
		return true;
	} else if (!regEx.test(toEmailId)) {
		alert("Please Enter Valid  To EmailId");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "sharehotdealinfoviaemail.htm",
			data : {
				'toEmailId' : toEmailId,
				'fromEmailId' : fromEmailId,
				'hotdealId' : hotdealId
			},
			success : function(response) {

				if (response == 'Email sent successfully.') {

					alert(response);
					$(".cncl-actn").click();
				} else {

					alert('Error occurred while sending mail');
					$(".cncl-actn").click();
				}

			},

			error : function(xhr, status, error) {
				alert(status);
			}

		});

	}
}
/** FOR DISPLAYING FIND DEAL DETAILS BASED ON SEARCH KEY AND ZIPCODE **/
function finddeallstsearch() {
	document.dealsearchform.action = "consdealssearch.htm";
	document.dealsearchform.method = "POST";
	document.dealsearchform.submit();
}
/** DISPLAYING SHOPPING LIST PRODUCTS **/
function shoppinglistdisplay(scrName) {
	
	document.ShoppingListHome.screenName.value = scrName;
	document.ShoppingListHome.action = "consdisplayslprod.htm";
	document.ShoppingListHome.method = "POST";
	document.ShoppingListHome.submit();

}
/** FOR ADDING PRODUCT TO TODAY LIST FROM FAVORITES **/


function addfavoriteprodtosl() {
	var userProductIds = '';
var selcunt= $('#selPerPage :selected').val();
	$('#slfavorite .favChkbx:checked').each(function() {
		var values = $(this).val();
		userProductIds += values;
	});
	if (userProductIds == "") {
		alert("Please select the product");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "consfavaddprod.htm",
			data : {
				'userproductId' : userProductIds
			},
			success : function(response) {
				alert(response);
				if(selcunt == undefined)
				{
			
				document.ShoppingListHome.recordCount.value=20;
				}else{
				document.ShoppingListHome.recordCount.value=selcunt;
				}
				
				document.ShoppingListHome.action = "consdisplayslprod.htm";
				document.ShoppingListHome.method = "POST";
				document.ShoppingListHome.submit();

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}
}
/** FOR ADDING PRODUCT TO TODAY LIST FROM SEARCH **/
function addsearchprodtosl(addTo,screen) {
var selcunt= $('#selPerPage :selected').val();

	var productIds = '';
	var scrName = screen;

	$('#slsearch .searchChkbx:checked').each(function() {
		var values = $(this).val();
		productIds += values;
	});

	if (productIds == "") {
		alert("Please select the product");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$
				.ajax({
					type : "GET",
					url : "conssearchaddprod.htm",
					data : {
						'productId' : productIds,
						'addTo' : addTo
					},
					success : function(response) {
						alert(response);

						var prodSearchKey = document.ShoppingListHome.prodSearchKey.value;
						if(selcunt == undefined)
				{
			
				document.ShoppingListHome.recordCount.value=20;
				}else{
				document.ShoppingListHome.recordCount.value=selcunt;
				}
					
					
						document.ShoppingListHome.screenName.value = scrName;
						document.ShoppingListHome.searchkey.value = prodSearchKey;
						document.ShoppingListHome.action = "consdisplayslprod.htm";
						document.ShoppingListHome.method = "POST";
						document.ShoppingListHome.submit();

					},
					error : function(xhr, status, error) {
						alert(status);
					}

				});
	}
}

/** FOR ADDING PRODUCT TO TODAY LIST FROM HISTORY **/
function deletefavprod() {

	var userProductIds = '';

	$('#slfavorite .favChkbx:checked').each(function() {
		var values = $(this).val();
		userProductIds += values;
	});
	if (userProductIds == "") {
		alert("Please select the product");
		return true;
	}else{
	var msg = confirm(" You want  to Delete Product?\n ");
}
if(msg){
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "consfavdeleteprod.htm",
			data : {
				'productId' : userProductIds
			},
			success : function(response) {
				//alert(response);

				document.ShoppingListHome.action = "consdisplayfavprods.htm";
				document.ShoppingListHome.method = "GET";
				document.ShoppingListHome.submit();

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}

}
/** FOR ADDING PRODUCT TO TODAY LIST FROM HISTORY **/
function addhistoryprodtosl(addTo,screen) {

var selcunt= $('#selPerPage :selected').val();
	var usrProdId = '';
var scrName=screen;
	$('#slhistory .hstryChkbx:checked').each(function() {
		var values = $(this).val();
		usrProdId += values;
	});

	if (usrProdId == "") {
		alert("Please select the product");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "conshistoryaddprod.htm",
			data : {
				'usrProdId' : usrProdId,
				'addTo' : addTo
			},
			success : function(response) {
					alert(response);	
					if(selcunt == undefined)
				{
			
				document.ShoppingListHome.recordCount.value=20;
				}else{
				document.ShoppingListHome.recordCount.value=selcunt;
				}
					
				document.ShoppingListHome.screenName.value = scrName;
				document.ShoppingListHome.action = "consdisplayslprod.htm";
				document.ShoppingListHome.method = "POST";
				document.ShoppingListHome.submit();

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}
}

/** FOR DELETING TODAY LIST PRODUCT **/
function deletetodaylstprod(scrName) {

	var userProductIds = '';

	$('#shpngList .chkbxGrp:checked').each(function() {
		var values = $(this).val();
		userProductIds += values;
	});

	if (userProductIds == "") {
		alert("Please select the product");
		return true;
	} else{
	var msg = confirm(" You want  to Delete Product?\n ");
}
if(msg){
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "constodaylstdeleteprod.htm",
			data : {
				'productId' : userProductIds
			},
			success : function(response) {
				//alert(response);

				document.ShoppingListHome.screenName.value = scrName;
				document.ShoppingListHome.action = "consdisplayslprod.htm";
				document.ShoppingListHome.method = "GET";
				document.ShoppingListHome.submit();

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}

}
/** FOR MOVING  TODAY LIST PRODUCTS TO BASKET **/
function moveprodtobasket(usrproductid, scnName) {
var selcunt= $('#selPerPage :selected').val();
var screen = scnName;
	if (usrproductid == "") {
		alert("Please select the product");
		return true;
	} else {

		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "consmovetobasket.htm",
			data : {
				'usrproductid' : usrproductid

			},
			success : function(response) {
				
				if(selcunt == undefined)
				{
			
				document.ShoppingListHome.recordCount.value=20;
				}else{
				document.ShoppingListHome.recordCount.value=selcunt;
				}
				document.ShoppingListHome.screenName.value=screen;
				document.ShoppingListHome.action = "consdisplayslprod.htm";
				document.ShoppingListHome.method = "POST";
				document.ShoppingListHome.submit();

			},
			error : function(xhr, status, error) {
				 
			alert("error info"+error.Message);
			}

		});
	}
}

/** FOR  SHOPPING LIST PRODUCTS CHECKOUT **/
function checkoutslprod(screen) {

	var userProductIds = '';
	var scrName = screen;
	$('#shpngList .chkbxGrp:checked').each(function() {
		var values = $(this).val();
		userProductIds += values;
	});

	if (userProductIds == "") {
		alert("Please select product to check out");
	} else {
		var msg = confirm(" Ready to check-out?\n If you do, all your items will be\n moved to history.");
	}

	if (msg) {

		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "conscheckoutshoplst.htm",
			data : {
				'productId' : userProductIds
			},
			success : function(response) {
			//	alert(response);

				document.ShoppingListHome.screenName.value = scrName;
				document.ShoppingListHome.action = "consdisplayslprod.htm";
				document.ShoppingListHome.method = "GET";
				document.ShoppingListHome.submit();

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}

}
/** FOR  WISH LIST PRODUCT SEARCH**/
function wishlistprodsearch(event) {

	var searchKey = document.WishListHome.searchkey.value;

	if (event && event.which == 13) {

		if (searchKey == '') {
			alert("Note: Please Enter keyword to search!");
			return true;

		}

		document.WishListHome.action = "conswlsearchprods.htm";
		document.WishListHome.method = "POST";
		document.WishListHome.submit();
	} else if (event == '') {

		if (searchKey == '') {
			alert("Note: Please Enter keyword to search!");
			return true;

		}

		document.WishListHome.action = "conswlsearchprods.htm";
		document.WishListHome.method = "POST";
		document.WishListHome.submit();

	} else {
		return true;
	}

}

/** FOR DELETING WISH LIST PRODUCT **/
function deletewishlstprod(screenName) {
var scrName= screenName;
var selcunt= $('#selPerPage :selected').val();
	var userProductIds = '';

	$('#wishLst .chkbxGrp:checked').each(function() {
		var values = $(this).val();
		userProductIds += values;
	});

	if (userProductIds == "") {

		alert("Please select the product");
		return true;
	} else{
	var msg = confirm(" You want  to Delete Product?\n ");
}
if(msg){
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "conswldeleteprod.htm",
			data : {
				'userprodid' : userProductIds
			},
			success : function(response) {
			//	alert(response);
			if(selcunt == undefined)
				{
			
				document.WishListHome.recordCount.value=20;
				}else{
				document.WishListHome.recordCount.value=selcunt;
				}
				document.WishListHome.screenName.value = scrName;
				document.WishListHome.action = "conswishlisthome.htm";
				document.WishListHome.method = "POST";
				document.WishListHome.submit();

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}

}
/** FOR DELETING WISH LIST HISTORY PRODUCT **/
function deletewlhistoryprod() {

	var userProductIds = '';

	$('#wishLsthistory .historychkbxGrp:checked').each(function() {
		var values = $(this).val();
		userProductIds += values;
	});

	if (userProductIds == "") {
		alert("Please select the product");
		return true;
	} else{
	var msg = confirm(" You want  to Delete Product?\n ");
}
if(msg){
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "conswlhistorydelprod.htm",
			data : {
				'userprodid' : userProductIds
			},
			success : function(response) {
				//alert(response);

				document.WishListHome.action = "conswlhistory.htm";
				document.WishListHome.method = "GET";
				document.WishListHome.submit();

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}

}

/** FOR ADDING WISH LIST SEARCH PRODUCT TO WISH LIST **/
function addsearchprodtowl(search) {

var selcunt= $('#selPerPage :selected').val();
	var productId = '';
 var scrName = search;
	$('#wlsearch .searchChkbx:checked').each(function() {
		var values = $(this).val();
		productId += values;
	});

	if (productId == "") {
		alert("Please select the product");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "consaddsearchprodtowl.htm",
			data : {
				'productId' : productId,
			},
			success : function(response) {
				alert(response);
				var prodSearchKey = document.WishListHome.prodSearchKey.value;
				
				if(selcunt == undefined)
				{
			
				document.WishListHome.recordCount.value=20;
				}else{
				document.WishListHome.recordCount.value=selcunt;
				}
				document.WishListHome.screenName.value = scrName;
				document.WishListHome.searchkey.value = prodSearchKey;
				document.WishListHome.action = "conswishlisthome.htm";
				document.WishListHome.method = "POST";
				document.WishListHome.submit();

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}
}

/** WISH LIST ALERTED PRODUCT COUPON OPERATIONS.*/
function conswladdcoupon(couponId, couponlstId) {
	if (couponId == "") {
		alert("Please select the Coupon");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "conswladdcoupon.htm",
			data : {
				'couponId' : couponId,
				'couponlstId' : couponlstId
			},
			success : function(response) {

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}
}

/** WISH LIST ALERTED PRODUCT COUPON OPERATIONS.*/
function conswlremovecoupon(couponId) {

	if (couponId == "") {
		alert("Please select the Coupon");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "conswlremovecoupon.htm",
			data : {
				'couponId' : couponId

			},
			success : function(response) {

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}
}

function printwlProductInfo() {
var selcunt= $('#selPerPage :selected').val();
	if(null !=selcunt && "" != selcunt)
	{
	if(selcunt == 'undefined')
	{
	
	var vOpenTab = window.open(
				'/ScanSeeWeb/shopper/conswlprintprod.htm?recordCount=20'
						, '_blank');
	}else{
	
	var vOpenTab = window.open(
				'/ScanSeeWeb/shopper/conswlprintprod.htm?recordCount=selcunt'
						, '_blank');
	}
		
		vOpenTab.focus();
		}
}

function wishlisthomedisplay(scrName) {
document.WishListHome.screenName.value = scrName;
	document.WishListHome.action = "conswishlisthome.htm";
	document.WishListHome.method = "POST";
	document.WishListHome.submit();

}
function wlhistorydisplay() {
	document.WishListHome.action = "conswlhistory.htm";
	document.WishListHome.method = "GET";
	document.WishListHome.submit();

}

function printshoplistProductInfo() {

	

		var vOpenTab = window.open(
				'/ScanSeeWeb/shopper/consslprintprod.htm'
						, '_blank');
		vOpenTab.focus();
	
}
function addingprodtoSl() {

	
	document.prodsummaryform.action = "consaddprodtoshopp.htm";
	document.prodsummaryform.method = "GET";
	document.prodsummaryform.submit();

}

function addsummaryprodtoshoplist() {

	$.ajaxSetup({
		cache : false
	});
	$.ajax({
		type : "GET",
		url : "consaddprodtoshopp.htm",

		success : function(response) {
			alert(response);

		},
		error : function(xhr, status, error) {
			alert(status);
		}

	});

}
function addsummaryprodtowishlist() {

	$.ajaxSetup({
		cache : false
	});
	$.ajax({
		type : "GET",
		url : "consaddprodtowishlist.htm",

		success : function(response) {
			alert(response);

		},
		error : function(xhr, status, error) {
			alert(status);
		}

	});

}
function shopwlhome()
{
alert("please log in or sign up - it's free ");
}
function getcoupassoiatedprods(coupId)
{
	document.clrForm.action = "conscoupprod.htm";
	document.clrForm.method = "GET";
	document.clrForm.submit();
}
/** FOR ADDING PRODUCT TO TODAY LIST FROM coupon associated products **/
function addcoupprodtosl() {
	var productIds = '';

	$('#conscoupprod .coupChkbx:checked').each(function() {
		var values = $(this).val();
		productIds += values;
	});
	if (productIds == "") {
		alert("Please select the product");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "conssearchaddprod.htm",
			data : {
						'productId' : productIds,
						'addTo' : 'List'
					},
			success : function(response) {
				alert(response);

				

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}
}
function addcoupprodtowl() {
	var userProductIds = '';

	$('#conscoupprod .coupChkbx:checked').each(function() {
		var values = $(this).val();
		userProductIds += values;
	});
	if (userProductIds == "") {
		alert("Please select the product");
		return true;
	} else {
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "consaddsearchprodtowl.htm",
			data : {
				'productId' : userProductIds
			},
			success : function(response) {
				alert(response);
	

			},
			error : function(xhr, status, error) {
				alert(status);
			}

		});
	}
}
function load(lattitude, longitude) {
	lattitude = lattitude.toString();
	longitude = longitude.toString();
	var lat = lattitude.substring(0, 5);
	var longt = longitude.substring(0, 6);
	var myOptions = {
		center : new google.maps.LatLng(lat, longt),
		zoom : 8,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_canvas"),
			myOptions);
	var marker = new google.maps.Marker({
		map : map,
		draggable : true,
		position : map.getCenter()
	});
}

function showAddress(zipcodeChk) {

	if (zipcodeChk == '') {
		zipcodeChk = $('input[name$="cityNameTL"]').val();
		if (zipcodeChk == '') {
			alert('Please Enter City');
			return false;
		}
	}

	// Get the Lat long of city

	var geo = new google.maps.Geocoder;
	var lat;
	geo.geocode({
		'address' : zipcodeChk
	}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			lat = results[0].geometry.location;
			lat = lat.toString();
			lat = lat.substring(1, (lat.length - 1));
			var commIndex = lat.indexOf(',');
			var lattitude = lat.substring(0, commIndex);
			var longitude = lat.substring(commIndex + 1, lat.length);
			document.getElementById("latitude").value = lattitude;
			document.getElementById("longitude").value = longitude;
			var myOptions = {
				center : new google.maps.LatLng(lattitude, longitude),
				zoom : 8,
				mapTypeId : google.maps.MapTypeId.ROADMAP
			};

			var map = new google.maps.Map(
					document.getElementById("map_canvas"), myOptions);

			var marker = new google.maps.Marker({
				map : map,
				draggable : true,
				position : map.getCenter()
			});
		} else {
			alert("Geocode was not successful for the following reason: "
					+ status);
		}

	});

	// End

}

function callThisLocation() {
	document.shopperdashboard.action = "thislocation.htm";
	document.shopperdashboard.method = "GET";
	document.shopperdashboard.submit();
}

function ValidateRetailers(event) {

	var city = $('input[name$="cityNameTL"]').val();
	var zipcode = document.thislocation.zipcodeTL.value;
	if (event && event.which == 13) {

		if (city == '' || city == 'null' || city == 'undefined') {
			if (zipcode == '') {
				alert("Note: Please Enter Zipcode");
				return true;
			}

		}

		document.thislocation.action = "retailerinfo.htm";
		document.thislocation.method = "POST";
		document.thislocation.submit();
	} else if (event == '') {

		if (city == '' || city == 'null' || city == 'undefined') {
			if (zipcode == '') {
				alert("Note: Please Enter Zipcode");
				return true;
			}

		}
		document.thislocation.action = "retailerinfo.htm";
		document.thislocation.method = "POST";
		document.thislocation.submit();
	} else {
		return true;
	}

	/*
	 * if ($('select[name$="raiudTL"] option:first:selected').val() == 0) {
	 * alert("Note: Please Select Radius"); return true; }
	 */

}

function callProdDetails(retailLocationID, retailerId, retailerName) {
	var repRetailerName = retailerName.replace("%27", "'");
	document.thislocation.retailLocationID.value = retailLocationID;
	document.thislocation.retailerId.value = retailerId;
	document.thislocation.retailerName.value = repRetailerName;
	document.thislocation.action = "productinfo.htm"
	document.thislocation.method = "POST";
	document.thislocation.submit();
}

function callProdDetailsBack(retailLocationID, retailerId, retailerName) {
	var repRetailerName = retailerName.replace("%27", "'");
	document.productsummary.retailLocationID.value = retailLocationID;
	document.productsummary.retailerId.value = retailerId;
	document.productsummary.retailerName.value = repRetailerName;
	document.productsummary.action = "productinfo.htm"
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function callprodSummary(productId, retailerId, salePrice, retailLocationID,
		retailerName) {
	document.thislocation.productId.value = productId;
	document.thislocation.retailerId.value = retailerId;
	document.thislocation.salePrice.value = salePrice;
	document.thislocation.retailerName.value = retailerName;
	document.thislocation.retailLocationID.value = retailLocationID;
	document.thislocation.action = "productSummary.htm";
	document.thislocation.method = "POST";
	document.thislocation.submit();
}

function callNearBy(productId) {
	document.productsummary.productId.value = productId;
	document.productsummary.action = "nearBy.htm?";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function callRetailerDetails(retailerId, distance, productPrice, productId) {
	document.productsummary.productId.value = productId;
	document.productsummary.distance.value = distance;
	document.productsummary.retailerId.value = retailerId;
	document.productsummary.productPrice.value = productPrice;
	document.productsummary.action = "nearByReatailer.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();

}

function callProductInfo(productId, retailId) {
	document.productsummary.productId.value = productId;
	document.productsummary.retailerId.value = retailId;
	document.productsummary.action = "productdesc.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();

}

function callFind() {

	document.shopperdashboard.action = "find.htm";
	document.shopperdashboard.method = "GET";
	document.shopperdashboard.submit();

}

function shopperProductSearch() {
	var searchType = document.SearchForm.searchType.value;
	var searchKey = document.SearchForm.searchKey.value;

	if (null == searchType || "" == searchType || searchType == 'Please Select') {
		alert('Please select search type as product')
	} else if (null == searchKey || "" == searchKey) {
		alert('Please Enter search key')
	} else {
		document.SearchForm.action = "productsearchresults.htm";
		document.SearchForm.method = "POST";
		document.SearchForm.submit();
	}

}

function getDiscount(productId, retailID) {

	document.productsummary.productId.value = productId;
	document.productsummary.retailerId.value = retailID;
	document.productsummary.action = 'getdiscount.htm';
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function callReviews(productId) {
	document.productsummary.productId.value = productId;
	document.productsummary.action = "reviews.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

// for displaying delete image in wish list display.
$('.mobGrd td img[name$="DeleteRow"]').hide();
$('.mobGrd td').live('mouseover', function() {
	$(this).closest('tr').find('img[name$="DeleteRow"]').show();
}).bind('mouseout', function() {
	$(this).closest('tr').find('img[name$="DeleteRow"]').hide();
});
$('.mobGrd td img[name$="DeleteRow"]').click(function() {

	function callWishListHistory() {

		document.wishlistdisplay.action = "wishlisthistorydisplay.htm";
		document.wishlistdisplay.method = "GET";
		document.wishlistdisplay.submit();

	}

	$(this).closest('tr').remove();
	/*
	 * var grpdRow = $('#alertedItmLst tr td.grpdtxt').length;
	 * 
	 * alert(grpdRow);
	 */
});

function deleteWishListItem(userProductId, id) {

	$.ajax({
		type : "GET",
		url : "wishlistdelete.htm",
		data : {
			'userProductId' : userProductId
		},

		success : function(response) {
			$('#' + id).hide();
		},
		error : function(e) {

		}
	});
}

function callproductSummary(productId) {
	document.wishlistdisplay.productId.value = productId;
	document.wishlistdisplay.action = "productSummary.htm";
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();
}

function getOnlineStoreInfo(productId, retailID) {

	document.productsummary.productId.value = productId;
	document.productsummary.retailerId.value = retailID;
	document.productsummary.action = 'getonlinestoreinfo.htm';
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function ValidateWishList(event) {
	if (event && event.which == 13) {
		document.wishlistdisplay.action = "wishlistdisplay.htm";
		document.wishlistdisplay.method = "POST";
		document.wishlistdisplay.submit();
	} else if (event == '') {
		document.wishlistdisplay.action = "wishlistdisplay.htm";
		document.wishlistdisplay.method = "POST";
		document.wishlistdisplay.submit();
	} else {
		return true;
	}
}

function callWLLocalStores(productId) {
	document.getElementById("productId").value = productId;
	// document.wishlistdisplay.productId.value = productId;
	document.wishlistdisplay.action = "wllocalstores.htm";
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();
}

function getWLOnlineStoreInfo(productId) {
	document.wishlistdisplay.wishlist.value = "wishlist"
	document.wishlistdisplay.productId.value = productId;
	document.wishlistdisplay.action = 'wlgetonlinestoreinfo.htm';
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();
}

function backToRetailers() {
	document.thislocation.action = "retailerinfo.htm";
	document.thislocation.method = "POST";
	document.thislocation.submit();
}

function thisLocationProductSearch(retailerID) {
	var searchKey = document.thislocation.searchKey.value;
	document.thislocation.tlFind.value = "tlFind";
	document.thislocation.retailerId.value = retailerID;
	if (null == searchKey || "" == searchKey) {
		alert('Please Enter search key');
	} else {
		document.thislocation.action = "productsearchresults.htm";
		document.thislocation.method = "POST";
		document.thislocation.submit();
	}

}

function getWLHotdealInfo(productId) {
	document.wishlistdisplay.productId.value = productId;
	document.wishlistdisplay.action = 'wlhotdealproductinfo.htm';
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();
}

function getWLOnlineStores(productId) {

	document.wishlistdisplay.productId.value = productId;
	document.wishlistdisplay.action = "wlonlinestores.htm";
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();

}

function getWLCouponInfo(productId) {
	document.wishlistdisplay.productId.value = productId;
	document.wishlistdisplay.action = 'wlcouponproductinfo.htm';
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();
}

function searchWishList(IsWishlst) {

	document.wishlistdisplay.IsWishlst.value = 'true';
	document.wishlistdisplay.action = "searchShoppingList.htm";
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();

}

function searchWishListBack(IsWishlst) {

	document.productsummary.action = "searchShoppingList.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();

}
function searchWishListevent(event) {
	if (event && event.which == 13) {
		document.wishlistdisplay.IsWishlst.value = 'true';
		document.wishlistdisplay.action = "searchShoppingList.htm";
		document.wishlistdisplay.method = "POST";
		document.wishlistdisplay.submit();
	} else {
		return true;
	}

}
function addWLProductsBySearch(productId) {
	document.shoppingList.action = "addWLProductBySearch.htm";
	document.shoppingList.selectedProductId.value = productId;
	document.shoppingList.method = "POST";
	document.shoppingList.submit();
}

function saveUserRating(productId) {
	$.ajaxSetup({
		cache : false
	});
	var userRating = $('input:radio[name=Rating]:checked').val();
	$.ajax({
		type : "GET",
		url : "saveuserrating.htm",
		data : {
			'userRating' : userRating,
			'productId' : productId
		},

		success : function(response) {

		},
		error : function(e) {

		}
	});

}

function deleteUserRating(productId) {
	$.ajaxSetup({
		cache : false
	});
	$.ajax({
		type : "GET",
		url : "saveuserrating.htm",
		data : {
			'userRating' : 0,
			'productId' : productId
		},

		success : function(response) {

		},
		error : function(e) {

		}
	});

}

function callproductSummarySC(productId) {

	document.productsummary.productId.value = productId;
	document.productsummary.action = "productSummary.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function callProdSummaryBack(productId) {
	document.productsummary.productId.value = productId;
	document.productsummary.action = "productSummary.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function searchProductList(event) {
	if (event && event.keyCode == 13) {
		document.thislocation.action = "productsearchresults.htm";
		document.thislocation.method = "POST";
		document.thislocation.submit();
	} else if (event && event.which == 13) {
		document.thislocation.action = "productsearchresults.htm";
		document.thislocation.method = "POST";
		document.thislocation.submit();
	} else if (event == '') {
		document.thislocation.action = "productsearchresults.htm";
		document.thislocation.method = "POST";
		document.thislocation.submit();
	} else {
		return true;
	}
}
function callproductSummaryWL(productId) {
	document.wishlisthistorydisplay.productId.value = productId;
	document.wishlisthistorydisplay.action = "productSummary.htm";
	document.wishlisthistorydisplay.method = "POST";
	document.wishlisthistorydisplay.submit();
}

function callMediaFile(productId, mediaType) {
	document.productsummary.productId.value = productId;
	document.productsummary.mediaType.value = mediaType;
	document.productsummary.action = "getMediaInfo.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}
function callWLReviews(productId) {

	document.wishlistdisplay.wlratereview.value = 'true';
	document.getElementById("productId").value = productId;
	// document.wishlistdisplay.productId.value = productId;
	document.wishlistdisplay.action = "reviews.htm";
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();
}
function ValidateTLWishList() {

	document.productsummary.action = "wishlistdisplay.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function addWishList(productId) {

	$.ajax({
		type : "GET",
		url : "addWLProduct.htm",
		data : {
			'productId' : productId
		},

		success : function(response) {
			alert(response);
		},
		error : function(e) {

		}
	});
}

function callTLWishlstLocalStores(productId) {
	document.productsummary.productId.value = productId;
	document.productsummary.action = "wllocalstores.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}
function callTLWishlstOnlineStores(productId) {
	document.productsummary.productId.value = productId;
	document.productsummary.action = "wlonlinestores.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();

}

function callWLOnlineReviews(productId) {

	document.wishlistdisplay.wlonlineratereview.value = 'true';
	document.getElementById("productId").value = productId;
	// document.wishlistdisplay.productId.value = productId;
	document.wishlistdisplay.action = "reviews.htm";
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();
}

function callWLHistoryFrmOnlineStores(productId) {
	document.getElementById("productId").value = productId;
	document.wishlistdisplay.wlonlineratereview.value = 'true';
	document.wishlistdisplay.action = "wishlisthistorydisplay.htm";
	document.wishlistdisplay.method = "GET";
	document.wishlistdisplay.submit();

}
function callWLHistoryFrmLocalStores(productId) {

	// document.wishlistdisplay.productId.value = productId;
	document.getElementById("productId").value = productId;
	document.wishlistdisplay.wlratereview.value = 'true';
	document.wishlistdisplay.action = "wishlisthistorydisplay.htm";
	document.wishlistdisplay.method = "GET";
	document.wishlistdisplay.submit();

}
function callproductSummaryFrmSearch(productId) {

	document.shoppingList.searchProd.value = 'true';
	document.shoppingList.productId.value = productId;
	document.shoppingList.action = "productSummary.htm";
	document.shoppingList.method = "POST";
	document.shoppingList.submit();

}

function getDiscountBack(productId, retailID) {
	document.productsummary.productId.value = productId;
	document.productsummary.retailerId.value = retailID;
	document.productsummary.action = 'getdiscount.htm';
	document.productsummary.method = "POST";
	document.productsummary.submit();

}
function searchFromWishList(IsWishlst) {

	document.shoppingList.IsWishlst.value = 'true';
	document.shoppingList.action = "searchShoppingList.htm";
	document.shoppingList.method = "POST";
	document.shoppingList.submit();

}

function ValidateWLFromHistory(event) {
	if (event && event.which == 13) {
		document.wishlisthistorydisplay.action = "wishlistdisplay.htm";
		document.wishlisthistorydisplay.method = "POST";
		document.wishlisthistorydisplay.submit();
	} else if (event == '') {
		document.wishlisthistorydisplay.action = "wishlistdisplay.htm";
		document.wishlisthistorydisplay.method = "POST";
		document.wishlisthistorydisplay.submit();
	} else {
		return true;
	}
}
function onSubmitSLHistoryBack() {
	document.productsummary.action = "slHistory.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function callFindProductSummary(productId) {
	document.SearchForm.fromFind.value = 'true';
	document.SearchForm.productId.value = productId;
	document.SearchForm.action = "productSummary.htm";
	document.SearchForm.method = "POST";
	document.SearchForm.submit();
}
function callFindSearch() {
	document.productsummary.action = "findproductsearch.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}
function callFindProductInfo(productId, retailId) {
	document.thislocation.productId.value = productId;
	document.thislocation.retailerId.value = retailId;
	document.thislocation.action = "productdesc.htm";
	document.thislocation.method = "POST";
	document.thislocation.submit();
}
function scanNowProductSearch() {

	var searchKey = document.scannowform.searchKey.value;

	if (null == searchKey || "" == searchKey) {
		alert('Please Enter search key');
	} else {
		document.scannowform.action = "scannowprodsearch.htm";
		document.scannowform.method = "POST";
		document.scannowform.submit();
	}
}

function scannowSmartSearch() {

	var searchKey = document.scannowform.searchKey.value;

	if (null == searchKey || "" == searchKey) {
		alert('Please Enter search key');
	} else {

		$.ajax({
			type : "GET",
			url : "scannowsmartsearch.htm",
			data : {
				'searchKey' : searchKey
			},
			success : function(response) {

				response($.map(response.countries, function(item) {
					return {
						label : item.label,
						value : item.label
					}
				}));

			}
		});
	}
}

function callScannowProductSummary(productId) {
	document.scannowform.fromScannow.value = 'true';
	document.scannowform.productId.value = productId;
	document.scannowform.action = "productSummary.htm";
	document.scannowform.method = "POST";
	document.scannowform.submit();
}

function addShopingList(productId) {

	$.ajax({
		type : "GET",
		url : "addSLProduct.htm",
		data : {
			'productId' : productId
		},

		success : function(response) {
			alert(response);
		},
		error : function(e) {

		}
	});
}

function ValidateRetailersList() {
	var zipcode = document.productsummary.zipcodeTL.value;
	var city = $('input[name$="cityNameTL"]').val();
	if (city == '' || city == 'null' || city == 'undefined') {
		if (zipcode == '') {
			alert("Note: Please Enter Zipcode");
			return true;
		}
	}
	if ($('select[name$="raiudTL"] option:first:selected').val() == 0) {
		alert("Note: Please Select Radius");
		return true;
	}
	document.productsummary.action = "retailerinfo.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function saveUserSettings() {
		document.preferencesusersettings.action = "conssaveusersettings.htm";
		document.preferencesusersettings.method = "POST";
		document.preferencesusersettings.submit();
}
function saveUserInfo() {
		document.preferencesuserinfo.action = "consfetchuserinfo.htm";
		document.preferencesuserinfo.method = "POST";
		document.preferencesuserinfo.submit();
}

function saveUserPreferences() {

		document.preferencesusrcategories.action = "consuserPreferences.htm";
		document.preferencesusrcategories.method = "POST";
		document.preferencesusrcategories.submit();
	
}

function callRetSummary(retailLocationID, retailerId, retailerName) {

	var repRetailerName = retailerName.replace("%27", "'");
	document.thislocation.retailLocationID.value = retailLocationID;
	document.thislocation.retailerId.value = retailerId;
	document.thislocation.retailerName.value = repRetailerName;
	document.thislocation.action = "retsummary.htm";
	document.thislocation.method = "POST";
	document.thislocation.submit();
}

function callRetSeeCurrentSpecials(retailerId, retailLocationID) {
	document.thislocation.retailLocationID.value = retailLocationID;
	document.thislocation.retailerId.value = retailerId;
	document.thislocation.action = "getcurrentspecial.htm";
	document.thislocation.method = "POST";
	document.thislocation.submit();
}
function callRetailerSales(retailerId, retailLocationID) {
	document.thislocation.retailLocationID.value = retailLocationID;
	document.thislocation.retailerId.value = retailerId;
	document.thislocation.action = "productinfo.htm"
	document.thislocation.method = "POST";
	document.thislocation.submit();
}
function callRetailerSpecials(retailerId, retailLocationID) {
	document.thislocation.retailLocationID.value = retailLocationID;
	document.thislocation.retailerId.value = retailerId;
	document.thislocation.action = "retspecials.htm"
	document.thislocation.method = "POST";
	document.thislocation.submit();
}
function callRetailerHotdeals(retailerId, retailLocationID) {
	document.thislocation.retailLocationID.value = retailLocationID;
	document.thislocation.retailerId.value = retailerId;
	document.thislocation.action = "rethotdeals.htm"
	document.thislocation.method = "POST";
	document.thislocation.submit();
}
function callRetailerCoupons(retailerId, retailLocationID) {
	document.thislocation.retailLocationID.value = retailLocationID;
	document.thislocation.retailerId.value = retailerId;
	document.thislocation.action = "retcoupons.htm"
	document.thislocation.method = "POST";
	document.thislocation.submit();
}
function getRetailerHotDealInfo(hotDealID) {

	document.thislocation.hotDealID.value = hotDealID;
	document.thislocation.action = "/ScanSeeWeb/shopper/hotdealinfo.htm";
	document.thislocation.method = "POST";
	document.thislocation.submit();
}
function callNearByRetSummary(retailLocationID, retailerId, retailerName) {

	var repRetailerName = retailerName.replace("%27", "'");

	document.productsummary.retailLocationID.value = retailLocationID;
	document.productsummary.retailerId.value = retailerId;
	document.productsummary.retailerName.value = repRetailerName;
	document.productsummary.action = "retsummary.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}
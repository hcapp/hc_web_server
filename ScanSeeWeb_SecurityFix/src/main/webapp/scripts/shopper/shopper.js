/**
 * 
 */

function callWishList() {
	document.shopperdashboard.action = "wishlistdisplay.htm";
	document.shopperdashboard.method = "GET";
	document.shopperdashboard.submit();
}
function Validate() {
	document.wishlistdisplay.action = "wishlistdisplay.htm";
	document.wishlistdisplay.method = "POST";
	document.wishlistdisplay.submit();
}

function callWishListHistory() {
	document.wishlistdisplay.action = "wishlisthistorydisplay.htm";
	document.wishlistdisplay.method = "GET";
	document.wishlistdisplay.submit();

}

function getGalleryCouponDetails(couponType,couponId, added, clrImgPath,viewableOnWeb) {
	document.clrForm.viewableOnWeb.value = viewableOnWeb;
	document.clrForm.clrImagePath.value = clrImgPath;
	document.clrForm.added.value = added;
	document.clrForm.couponId.value = couponId;
	document.clrForm.clrType.value = couponType;
	document.clrForm.action = "consgallerycoupondetails.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}

function getCouponDetails(couponType,couponId, added, clrImgPath,viewableOnWeb) {
	document.clrForm.viewableOnWeb.value = viewableOnWeb;
	document.clrForm.clrImagePath.value = clrImgPath;
	document.clrForm.added.value = added;
	document.clrForm.couponId.value = couponId;
	document.clrForm.clrType.value = couponType;
	document.clrForm.action = "conscoupondetails.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}

function getRebateDetails(rebateId, added, clrImgPath) {
	document.clrForm.clrImagePath.value = clrImgPath;
	document.clrForm.added.value = added;
	document.clrForm.rebateId.value = rebateId;
	document.clrForm.clrType.value = "R";
	document.clrForm.action = "fetchclrdetails.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}

function getLoyaltyDetails(loyaltyId, added, clrImgPath) {
	document.clrForm.clrImagePath.value = clrImgPath;
	document.clrForm.added.value = added;
	document.clrForm.loyaltyDealID.value = loyaltyId;
	document.clrForm.clrType.value = "L";
	document.clrForm.action = "fetchclrdetails.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}

function getSummaryCouponDetails(couponId, usage, clrImgPath) {
	document.productsummary.clrImagePath.value = clrImgPath;

	if (usage == 'Red') {
		document.productsummary.added.value = 0;
	} else {
		document.productsummary.added.value = 1;
	}

	document.productsummary.couponID.value = couponId;
	document.productsummary.clrType.value = "C";
	document.productsummary.action = "fetchclrinfo.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function getSummaryRebateDetails(rebateId, usage, clrImgPath) {
	document.productsummary.clrImagePath.value = clrImgPath;

	if (usage == 'Red') {
		document.productsummary.added.value = 0;
	} else {
		document.productsummary.added.value = 1;
	}

	document.productsummary.rebateId.value = rebateId;
	document.productsummary.clrType.value = "R";
	document.productsummary.action = "fetchclrinfo.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function getSummaryLoyaltyDetails(loyaltyId, usage, clrImgPath) {
	document.productsummary.clrImagePath.value = clrImgPath;
	if (usage == 'Red') {
		document.productsummary.added.value = 0;
	} else {
		document.productsummary.added.value = 1;
	}

	document.productsummary.loyaltyDealID.value = loyaltyId;
	document.productsummary.clrType.value = "L";
	document.productsummary.action = "fetchclrinfo.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

/*
 * function addUsedCLR(clrType,addFlag){
 * 
 * 
 * if(addFlag == '1' && clrType == 'coupon') { alert("Coupon already added");
 * }else if(addFlag == '1' && clrType == 'loyality') { alert("Loyality already
 * added");
 * 
 * }else if(addFlag == '1' && clrType == 'rebate') { alert("Rebate already
 * added"); }
 * 
 * else{
 * 
 * document.clrForm.clrType.value=clrType; document.clrForm.action =
 * "addusedclr.htm"; document.clrForm.method = "POST";
 * document.clrForm.submit(); } }
 */
function userRedeemCLR(clrType) {
	document.clrForm.clrType.value = clrType;
	document.clrForm.action = "userredeemclr.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}

function getNextClrRecords(pageFlow) {
	document.clrForm.pageFlowType.value = pageFlow;
	document.clrForm.action = "getpagedclr.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}

/*
 * function addCLR(clrId,clrType,addFlag){
 * 
 * alert("clrId"+clrId); alert("clrType"+clrType); alert("addFlag"+addFlag);
 * 
 * 
 * if(addFlag == '1' && clrType == 'coupon') { alert("Coupon already added");
 * }else if(addFlag == '1' && clrType == 'loyality') { alert("Loyality already
 * added");
 * 
 * }else if(addFlag == '1' && clrType == 'rebate') { alert("Rebate already
 * added"); }
 * 
 * else{
 * 
 * $.ajax({ type : "GET", url : "addusedclr.htm", data : {
 * 
 * 
 * 'clrId' : clrId, 'clrType' : clrType, 'addFlag' : addFlag },
 * 
 * success : function(response) {
 * 
 * alert("response"+response);
 * 
 * $('#myAjax').replaceWith(response);
 * 
 * 
 * if(clrType == 'coupon') { alert("Coupon successfully"); }else if(clrType ==
 * 'loyality') { alert("Loyality successfully");
 * 
 * }else if(clrType == 'rebate') { alert("Rebate added successfully"); }
 *  }, error : function(e) { } }); } }
 */

function addCLR(clrId, clrType, addFlag) {

	$.ajax({
		type : "GET",
		url : "addusedclr.htm",
		data : {

			'clrId' : clrId,
			'clrType' : clrType,
			'addFlag' : addFlag

		},

		success : function(response) {

			$('#myAjax').replaceWith(response);

			if (clrType == 'coupon') {
				alert("Coupon added");
			} else if (clrType == 'loyality') {
				alert("Loyality added");

			} else if (clrType == 'rebate') {
				alert("Rebate added");

			}

		},
		error : function(e) {

		}
	});

}

function addWishListWithProduct() {
	var productIds = document.clrForm.hdnProdId.value;
	$.ajax({

		type : "GET",
		url : "addWishListwithproduct.htm",
		data : {
			'productIds' : productIds
		},

		success : function(response) {
			alert(response);
		},
		error : function(e) {

		}
	});
}

function findProductSearch(event) {

	var searchType = document.SearchForm.searchType.value;
	var searchKey = document.SearchForm.searchKey.value;

	if (null == searchType || "" == searchType || searchType == 'Please Select') {
		alert('Please select search type as product');
	} else if (event && event.keyCode == 13) {
		document.SearchForm.action = "findproductsearch.htm";
		document.SearchForm.method = "POST";
		document.SearchForm.submit();
	} else if (event && event.which == 13) {
		document.SearchForm.action = "findproductsearch.htm";
		document.SearchForm.method = "POST";
		document.SearchForm.submit();
	} else if (event == '') {
		document.SearchForm.action = "findproductsearch.htm";
		document.SearchForm.method = "POST";
		document.SearchForm.submit();
	} else {
		return true;

	}
}

function getCouponProductDetails(couponId) {
	document.clrForm.couponId.value = couponId;
	document.clrForm.clrType.value = "C";
	document.clrForm.action = "fetchclrproductdetails.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}


function addShoppingListWithProduct() {
		var productIds = document.clrForm.hdnProdId.value;
		$.ajax({

			type : "GET",
			url : "addShoppingListwithproduct.htm",
			data : {
				'productIds' : productIds
			},

			success : function(response) {
				alert(response);
			},
			error : function(e) {

			}
		});
	}
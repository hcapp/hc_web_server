/**
 * this file contains action methods for submitting forms and validations
 */

var saveRetalLocation = 'false';
var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;
var saveManageProductsPage = 'false';
var saveManageLocationPage = 'false';
var submitFlag = null;

function productInfo(retailLocationID, productID) {

	document.productform.retailLocationID.value = retailLocationID;
	document.productform.productID.value = productID;
	document.productform.action = "productInfo.htm";
	document.productform.method = "POST";
	document.productform.submit();
}

function supproductInfo(productID) {

	// document.productform.retailLocationID.value = retailLocationID;
	document.productform.productID.value = productID;
	document.productform.action = "productInfo.htm";
	document.productform.method = "POST";
	document.productform.submit();
}

function shopperproductInfo(productID) {

	// document.productform.retailLocationID.value = retailLocationID;
	document.productform.productID.value = productID;
	document.productform.action = "productDetail.htm";
	document.productform.method = "POST";
	document.productform.submit();
}
function ParseForNumber1(object) {
	phonevalue1 = ParseChar(object.value, zChar);
}
function ParseForNumber2(object) {
	phonevalue2 = ParseChar(object.value, zChar);
}

function backspacerUP(object, e) {
	if (e) {
		e = e
	} else {
		e = window.event
	}
	if (e.which) {
		var keycode = e.which
	} else {
		var keycode = e.keyCode
	}

	ParseForNumber1(object)

	if (keycode >= 48) {
		ValidatePhone(object)
	}
}

function backspacerDOWN(object, e) {
	if (e) {
		e = e
	} else {
		e = window.event
	}
	if (e.which) {
		var keycode = e.which
	} else {
		var keycode = e.keyCode
	}
	ParseForNumber2(object)
}

function GetCursorPosition() {

	var t1 = phonevalue1;
	var t2 = phonevalue2;
	var bool = false
	for (i = 0; i < t1.length; i++) {
		if (t1.substring(i, 1) != t2.substring(i, 1)) {
			if (!bool) {
				cursorposition = i
				bool = true
			}
		}
	}
}

function ValidatePhone(object) {

	var p = phonevalue1

	p = p.replace(/[^\d]*/gi, "")

	if (p.length < 3) {
		object.value = p
	} else if (p.length == 3) {
		pp = p;
		d4 = p.indexOf('(')
		d5 = p.indexOf(')')
		if (d4 == -1) {
			pp = "(" + pp;
		}
		if (d5 == -1) {
			pp = pp + ")";
		}
		object.value = pp;
	} else if (p.length > 3 && p.length < 7) {
		p = "(" + p;
		l30 = p.length;
		p30 = p.substring(0, 4);
		p30 = p30 + ")"

		p31 = p.substring(4, l30);
		pp = p30 + p31;

		object.value = pp;

	} else if (p.length >= 7) {
		p = "(" + p;
		l30 = p.length;
		p30 = p.substring(0, 4);
		p30 = p30 + ")"

		p31 = p.substring(4, l30);
		pp = p30 + p31;

		l40 = pp.length;
		p40 = pp.substring(0, 8);
		p40 = p40 + "-"

		p41 = pp.substring(8, l40);
		ppp = p40 + p41;

		object.value = ppp.substring(0, maxphonelength);
	}

	GetCursorPosition()

	if (cursorposition >= 0) {
		if (cursorposition == 0) {
			cursorposition = 2
		} else if (cursorposition <= 2) {
			cursorposition = cursorposition + 1
		} else if (cursorposition <= 5) {
			cursorposition = cursorposition + 2
		} else if (cursorposition == 6) {
			cursorposition = cursorposition + 2
		} else if (cursorposition == 7) {
			cursorposition = cursorposition + 4
			e1 = object.value.indexOf(')')
			e2 = object.value.indexOf('-')
			if (e1 > -1 && e2 > -1) {
				if (e2 - e1 == 4) {
					cursorposition = cursorposition - 1
				}
			}
		} else if (cursorposition < 11) {
			cursorposition = cursorposition + 3
		} else if (cursorposition == 11) {
			cursorposition = cursorposition + 1
		} else if (cursorposition >= 12) {
			cursorposition = cursorposition
		}

		var txtRange = object.createTextRange();
		txtRange.moveStart("character", cursorposition);
		txtRange.moveEnd("character", cursorposition - object.value.length);
		txtRange.select();
	}

}

function ParseChar(sStr, sChar) {
	if (sChar.length == null) {
		zChar = new Array(sChar);
	} else
		zChar = sChar;

	for (i = 0; i < zChar.length; i++) {
		sNewStr = "";

		var iStart = 0;
		var iEnd = sStr.indexOf(sChar[i]);

		while (iEnd != -1) {
			sNewStr += sStr.substring(iStart, iEnd);
			iStart = iEnd + 1;
			iEnd = sStr.indexOf(sChar[i], iStart);
		}
		sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

		sStr = sNewStr;
	}

	return sNewStr;
}

function getRebateByName() {
	document.displayrebatesform.action = "rebatesMfg.htm";
	document.displayrebatesform.method = "POST";
	document.displayrebatesform.submit();
}

function getDealsByName() {
	document.displayDealsForm.action = "hotDeal.htm";
	document.displayDealsForm.method = "POST";
	document.displayDealsForm.submit();
}

function productSearch() {

	// var optionSlctd = document.headerform.searchType.value;

	var searchKey = document.headerform.searchKey.value;
	var zipCode = document.headerform.zipCode.value;
	if (null == searchKey || "" == searchKey) {
		alert('Please Enter valid Search Key')
	} // else if (null == zipCode || "" == zipCode) {
	// alert('Please Enter valid Zipcode')
	// }
	else {
		document.headerform.method = "POST";
		document.headerform.action = "header.htm";
		document.headerform.pagination.value = "false";
		document.headerform.submit();
	}
}

function productSearchForSupRet() {
	var searchKey = document.headerform.searchKey.value;

	if (null == searchKey || "" == searchKey) {
		alert('Please Enter valid Search Key')
	} else {
		document.headerform.method = "POST";
		document.headerform.action = "header.htm";
		if (window.location.pathname.search("uploadRetailerLogo/") > 0) {
			document.headerform.action = "../header.htm";
		}
		document.headerform.pagination.value = "false";
		document.headerform.submit();
	}
}

function editRebates(rebateId) {
	document.displayrebatesform.rebateID.value = rebateId;
	document.displayrebatesform.action = "rebatesEdit.htm";
	document.displayrebatesform.method = "GET";
	document.displayrebatesform.submit();
}

function rerunRebates(rebateId) {
	// alert(productId)
	document.displayrebatesform.rebateID.value = rebateId;
	// document.displayrebatesform.rebproductId.value = productId;
	document.displayrebatesform.action = "rerunrebate.htm";
	document.displayrebatesform.method = "GET";
	document.displayrebatesform.submit();
}

function hotDealEdit(dealID) {
	// alert("12345");
	document.displayDealsForm.hotDealID.value = dealID;
	document.displayDealsForm.action = "dailyDealsEdit.htm";
	document.displayDealsForm.method = "GET";
	document.displayDealsForm.submit();
}

function hotDealReRun(dealID) {
	document.displayDealsForm.hotDealID.value = dealID;
	document.displayDealsForm.action = "rerunHotDeals.htm";
	document.displayDealsForm.method = "GET";
	document.displayDealsForm.submit();
}

function hotDealRetailerEdit(dealID) {
	document.retailerDealsForm.hotDealID.value = dealID;
	document.retailerDealsForm.action = "retailerdealedit.htm";
	document.retailerDealsForm.method = "GET";
	document.retailerDealsForm.submit();
}

function hotDealRetailerReRun(dealID) {
	document.retailerDealsForm.hotDealID.value = dealID;
	document.retailerDealsForm.action = "retailerdealrerun.htm";
	document.retailerDealsForm.method = "GET";
	document.retailerDealsForm.submit();
}
function hotDealEditSupplier() {

	if (document.getElementById("slctCty").checked == true) {

		document.editdealsform.retailID.value = "0";
		document.editdealsform.retailerLocID.value = "0";
		hotDealEditSaveInfo();
	} else if (document.getElementById("slctLoc").checked == true) {
		document.editdealsform.City.value = "0";
		hotDealEditSaveInfo();
	} else {
		document.editdealsform.retailID.value = "0";
		document.editdealsform.retailerLocID.value = "0";
		document.editdealsform.City.value = "0";
		hotDealEditSaveInfo();
	}

}

function saveRerunRebate() {
	// alert("Saving Rebate");
	document.reRunRebatesForm.action = "rerunrebate.htm";
	document.reRunRebatesForm.method = "POST";
	document.reRunRebatesForm.submit();
}

function saveEditRebate() {
	// alert("Saving Rebates");
	document.editRebatesForm.action = "rebatesEdit.htm";
	document.editRebatesForm.method = "POST";
	document.editRebatesForm.submit();

}

function fbRedirectUser(fbUserId) {
	document.loginform.fbUserId.value = fbUserId;
	document.loginform.action = "shopper/registerfbuser.htm";
	document.loginform.method = "POST";
	document.loginform.submit();
}

function choosePlan() {
	document.myForm.action = "choosePlan.htm";
	document.myForm.method = "GET";
	document.myForm.submit();

}

/*
 * function planInfo() {
 * 
 * if(!document.getElementById("checkbox").checked) {
 * $('#checkBoxError').show(); } else { var productJson = []; var productObj =
 * {};
 * 
 * var objArr = [];
 * 
 * var table = document.getElementById('choosePln'); var rowCount =
 * table.rows.length; //alert("Table row count : "+rowCount); var trs =
 * table.getElementsByTagName("tr"); var rowId; var arrIndex = 0; for(var j=0;j<trs.length;j++) { //
 * alert("ROW ID :" + trs[j].id); rowId = trs[j].id;
 * 
 * if((rowId != "" || rowId != " ") && rowId > 0) { var object = {};
 * 
 * var inputFields = trs[j].getElementsByTagName("input"); var count = 0; //
 * alert("input fields length :"+inputFields.length) for(var i=0;i<inputFields.length;i++) { //
 * alert(inputFields[i].name + " : "+inputFields[i].value);
 * 
 * var name = inputFields[i].name; var inputTagVal = inputFields[i].value; if
 * (name == 'productId') { object.productId = inputTagVal; count++; } else if
 * (name == 'qty') { object.qty = inputTagVal; count++; } else if (name ==
 * 'price') { object.price = inputTagVal; count++; } else if (name == 'total') {
 * object.total = inputTagVal; count++; } }
 * 
 * if (count > 0 && Object.getOwnPropertyNames(object).length > 0) {
 * objArr[arrIndex] = object; arrIndex++; } } }
 * 
 * 
 * //$("#choosePln").find("tr").each(function(index) { $('#choosePln tbody
 * tr').each(function(index) { if (index > 0) { var highlightObj =
 * $($("#choosePln").find("tr")[index]).children(); var object = {}; var
 * stringObj; // alert(index) highlightObj.find("input").each(function(rVal) { //
 * if(rVal > 0){ var inputTagVal = $(this)[0].value; var name =
 * $(this).attr("name") // alert(name) // alert(name+" "+inputTagVal)
 * 
 * if (name == 'productId') { object.productId = inputTagVal; } else if (name ==
 * 'qty') { object.qty = inputTagVal; } else if (name == 'price') {
 * 
 * object.price = inputTagVal; } else if (name == 'total') { object.total =
 * inputTagVal; } // } });
 * 
 * 
 * if (Object.getOwnPropertyNames(object).length > 0) { objArr[index - 1] =
 * object; }
 * 
 * if (typeof Object.getOwnPropertyNames !== "function") {
 * Object.getOwnPropertyNames = function(object) { if (typeof object ===
 * "object" && object !== null) { if (Object.getOwnPropertyNames(object).length >
 * 0) { objArr[index - 1] = object; } } } } } });
 * 
 * //alert("Number of products : "+objArr.length); // Converting Sting JSOn
 * 
 * var strinObjArr = [] for ( var k = 0; k < objArr.length; k++) {
 * 
 * var object = objArr[k]; var stringObj = "{\"productID\":\"" +
 * object.productId + "\"," + "\"quantity\":\"" + object.qty + "\"," +
 * "\"price\":\"" + object.price + "\"," + "\"subTotal\":\"" + object.total +
 * "\"}"; strinObjArr[k] = stringObj; }
 * 
 * document.myForm.planJson.value = "{\"planData\":[" + strinObjArr.toString() +
 * "]}"; document.myForm.grandTotal.value = document
 * .getElementById("discountsubtotal").value; document.myForm.productId.value =
 * ""; document.myForm.action = "planInfo.htm"; document.myForm.method = "POST";
 * document.myForm.submit(); } }
 */

function planInfo() {

	var skipPayment = document.getElementById("skipPayment").value;
	if (skipPayment == 'skippayment') {
		document.myForm.action = "dashboard.htm";
		document.myForm.method = "GET";
		document.myForm.submit();
	} else {

		var productJson = [];
		var productObj = {};

		var objArr = [];
		$("#choosePln").find("tr").each(
				function(index) {
					if (index > 0) {
						var highlightObj = $($("#choosePln").find("tr")[index])
								.children();
						var object = {};
						var stringObj;
						// alert(index)
						highlightObj.find("input").each(function(rVal) {
							// if(rVal > 0){
							var inputTagVal = $(this)[0].value;
							var name = $(this).attr("name")
							// alert(name)
							// alert(name+" "+inputTagVal)

							if (name == 'productId') {
								object.productId = inputTagVal;

							} else if (name == 'qty') {
								object.qty = inputTagVal;

							} else if (name == 'price') {

								object.price = inputTagVal;

							} else if (name == 'total') {
								object.total = inputTagVal;

							}

							// }
						});

						if (Object.getOwnPropertyNames(object).length > 0) {
							objArr[index - 1] = object;
						}

						/*
						 * if (typeof Object.getOwnPropertyNames !== "function") {
						 * Object.getOwnPropertyNames = function(object) { if
						 * (typeof object === "object" && object !== null) { if
						 * (Object.getOwnPropertyNames(object).length > 0) {
						 * objArr[index - 1] = object; } } } }
						 */
					}
				});

		// Converting Sting JSOn

		var strinObjArr = []
		for ( var k = 0; k < objArr.length; k++) {

			var object = objArr[k];
			var stringObj = "{\"productID\":\"" + object.productId + "\","
					+ "\"quantity\":\"" + object.qty + "\"," + "\"price\":\""
					+ object.price + "\"," + "\"subTotal\":\"" + object.total
					+ "\"}";
			strinObjArr[k] = stringObj;
		}

		document.myForm.planJson.value = "{\"planData\":["
				+ strinObjArr.toString() + "]}";
		document.myForm.grandTotal.value = document
				.getElementById("discountsubtotal").value;
		document.myForm.productId.value = "";
		document.myForm.action = "planInfo.htm";
		document.myForm.method = "POST";
		document.myForm.submit();

	}

}

function confirmPlanAndSubmit() {
	document.plandetailform.action = "dashboard.htm";
	document.plandetailform.method = "GET";
	document.plandetailform.submit();

}
/*
 * function total() {
 * 
 * 
 * var qantity = eval(document.plandetailform.elements[0].value);
 * alert("qantity"+qantity); var price =
 * eval(document.plandetailform.elements[1].value); alert("price"+price); var
 * total = qantity*price; alert("total"+total);
 * 
 * eval(document.plandetailform.elements[2].value) = total;
 * alert("subtotal"+eval(document.plandetailform.elements[2].value));
 * 
 * 
 * var allTotal = new array();
 * 
 * allTotal = document.plandetailform.total.value;
 * alert("length"+allTotal.lenght()); subTotal = 0; for (i=0;i<allTotal.lenght();i++){
 * subTotal=+allTotal[i]; } }
 */

function totalPrice(size) {

	var qantity = document.getElementsByName("qty");
	var priceValue = document.getElementsByName("price");
	var total = document.getElementsByName("total");
	var discountCode = document.getElementById("discountCode").value;
	var subtotal = 0;

	for ( var i = 0; i < size; i++) {
		total[i].value = (qantity[i].value) * (priceValue[i].value);
		subtotal += parseFloat(total[i].value)

	}

	document.getElementById("sub").value = subtotal;
	document.getElementById("discountsubtotal").value = subtotal;
	if (!discountCode) {

		// document.getElementById("sub").value=subtotal;
		document.getElementById("discountsubtotal").value = subtotal;
	} else {
		getDiscount();
		document.getElementById("discountsubtotal").value = subtotal;

	}
}
function getDiscount() {

	// get the form values
	var discountCode = $('#discountCode').val();
	var subtotal = document.getElementById("sub").value;

	$.ajax({
		type : "GET",
		url : "fetchdiscount.htm",
		data : {
			'discountcode' : discountCode

		},

		success : function(response) {
			var fields = response.split('&');
			var discount1 = fields[0];
			var discount2 = fields[1];

			$('#myAjax').html(discount1);
			getTotalValue(discount2);
		},
		error : function(e) {
			alert("Please give a correct discountcode");
			$('#discountCode').val("");
			$('input[name$="discount"]').val("");
			// $('#discountsubtotal').val("");
			document.getElementById("discountsubtotal").value = subtotal;
		}
	});

}

function getTotalValue(discount2) {
	var subTotal = document.getElementById("sub").value;
	var discount = discount2;
	var total = subTotal - discount;
	document.getElementById("discountsubtotal").value = total;
}

function totalRetailerPrice(size) {

	var qantity = document.getElementsByName("qty");
	var priceValue = document.getElementsByName("price");
	var total = document.getElementsByName("total");
	var discountCode = document.getElementById("discountCode").value;
	var subtotal = 0;

	for ( var i = 0; i < size; i++) {
		total[i].value = (qantity[i].value) * (priceValue[i].value);
		subtotal += parseFloat(total[i].value)

	}
	document.getElementById("sub").value = subtotal;
	document.getElementById("discountsubtotal").value = subtotal;
	// document.getElementById("discountsubtotal").value = subtotal +
	// document.getElementById("setupFee").value;
	if (!discountCode) {

		// document.getElementById("sub").value=subtotal;
		document.getElementById("discountsubtotal").value = subtotal;
	} else {
		getRetailerDiscount();
		document.getElementById("discountsubtotal").value = subtotal;

	}

}

function searchResult() {
	document.myform.action = "manageProduct.htm";
	document.myform.method = "POST";
	document.myform.submit();
}

function registeredSearchResult() {
	document.myform.action = "registprodmanageprod.htm";
	document.myform.method = "POST";
	document.myform.submit();

}

function addRebates() {
	// alert("Saving Rebate");
	document.addRebatesform.retailerLocationID.value = document.addRebatesform.retailerLocID.value
	document.addRebatesform.action = "rebatesaddmfg.htm";
	document.addRebatesform.method = "POST";
	document.addRebatesform.submit();
}

// check for valid email format
function IsEMail() {

	var keychar = document.createprofileform.contactEmail.value;
	reg = /^.+@.+\..{2,5}$/;
	if (!reg.test(keychar)) {
		document.getElementsByName("contactEmail").focus();
		document.createprofileform.contactEmail.value = "";
		alert("Please Enter Valid EmailId");

	}
	/*
	 * else {
	 * 
	 * if (name.toString() == "contactEmail") {
	 * document.createprofileform.retypeEmail.focus(); } else {
	 * document.createprofileform.password.focus(); } }
	 */

}

// Calls next pagepageFlag
function callNextPage(pagenumber, url) {
	var selValue = $('#selPerPage :selected').val();
	// alert("hi")
	// for Saving Retail Location separate floe
	if (saveRetalLocation == 'true') {
		saveNcallNextRetailLocation(pagenumber);
	} else if (saveManageProductsPage == 'true') {
		saveNcallNextManageProducts(pagenumber);
	} else if (saveManageLocationPage == 'true') {
		saveNcallNextManageLocation(pagenumber);
	} else {
		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}
}

function validateShooperProfile() {

	// var password = document.createshopperprofileform.password.value;
	document.createshopperprofileform.action = "createShopperProfile.htm";
	document.createshopperprofileform.method = "POST";
	document.createshopperprofileform.submit();
	/*
	 * var passFlag = validatePassword(password); if (passFlag) {
	 * document.createshopperprofileform.action = "createShopperProfile.htm";
	 * document.createshopperprofileform.method = "POST";
	 * document.createshopperprofileform.submit(); }
	 */

	/*
	 * var regEx = /^.+@.+\..{2,5}$/; var emailCheck="No"; var email =
	 * document.createshopperprofileform.contEml.value; var repassword =
	 * document.createshopperprofileform.rePassword.value; var password =
	 * document.createshopperprofileform.password.value; var
	 * username=document.createshopperprofileform.userName.value; if(email!=""){
	 * if (!regEx.test(email)) { alert("Please Enter Valid EmailId");
	 * emailCheck="Yes"; } } if(emailCheck=="No"){ if(username==""){
	 * 
	 * alert("Please Enter Username"); } else if (password == "") {
	 * alert("Please Enter Password"); } else if (password.toString() !=
	 * repassword.toString()) { alert("Password fields does't match"); } else if
	 * (document.createshopperprofileform.checkbox.checked == false) {
	 * alert("Please accept the Terms and Conditions"); } else {
	 * 
	 */

}

// }
// }

// -----

/*
 * function isDecimal(str, msg, flag){ if(isNaN(str) || str.indexOf(".")<0){
 * flag = false; } }
 */

// *******************start*********
function saveUploadFile() {
	var prodFile = document.batchform.productuploadFilePath.value;
	var imageFile = document.batchform.imageUploadFilePath.value
	var attFile = document.batchform.attributeuploadFile.value
	var valFlag = true;
	if (prodFile == 'null' || prodFile == '') {
		alert('Please select file to upload the Products')
	} else if (imageFile == 'null' || imageFile == '') {
		alert('Please select file to upload the Images')
	} else if (attFile == 'null' || attFile == '') {
		alert('Please select file to upload the Product attributes');
	} else {

		var isProdFleValid = validateUploadFileExt(prodFile);

		var isAttrFleValid = validateUploadFileExt(attFile);
		if (!isProdFleValid) {
			alert('Product upload file should be of txt or csv format');
			return false;
		}

		if (!isAttrFleValid) {
			alert('Attribute upload file should be of txt or csv format')
			return false;
		}
		document.batchform.action = "savebatchupload.htm";
		document.batchform.method = "POST";
		document.batchform.submit();
	}

}

function saveSupplierUploadFile() {
	var prodFile = document.batchform.productuploadFilePath.value;
	var imageFile = document.batchform.imageUploadFilePath.value
	var attFile = document.batchform.attributeuploadFile.value
	if (prodFile == 'null' || prodFile == '') {
		alert('Please select file to upload the Products')
	} else if (imageFile == 'null' || imageFile == '') {
		alert('Please select file to upload the Images')
	} else if (attFile == 'null' || attFile == '') {
		alert('Please select file to upload the Product attributes');
	} else {

		var isProdFleValid = validateUploadFileExt(prodFile);

		var isAttrFleValid = validateUploadFileExt(attFile);
		if (!isProdFleValid) {
			alert('Product upload file should be of txt or csv format');
			return false;
		}

		if (!isAttrFleValid) {
			alert('Attribute upload file should be of txt or csv format')
			return false;
		}

		document.batchform.action = "savesupplierbatchupload.htm";
		document.batchform.method = "POST";
		document.batchform.submit();
	}

}

function uploadProductFile() {
	var selFile = document.batchform.productuploadFilePath.value
	if (selFile == 'null' || selFile == '') {
		alert('Please select file to upload the Products')
	} else {
		var isProdFleValid = validateUploadFileExt(selFile);
		if (!isProdFleValid) {
			alert('Product Upload file should be .CSV format');
			return false;
		}
		document.batchform.action = "uploadproduct.htm";
		document.batchform.method = "POST";
		document.batchform.submit();
	}

}

function uploadRetProductFile() {
	var selFile = document.batchUploadretprodform.productuploadFilePath.value;
	if (selFile == 'null' || selFile == '') {
		alert('Please select file to upload the Products')
	} else {
		var isProdFleValid = validateUploadFileExt(selFile);
		if (!isProdFleValid) {
			alert('Product upload file should be of ext or csv format');
			return false;
		}
		document.batchUploadretprodform.action = "/ScanSeeWeb/retailer/regbatchuploadretprod.htm";
		document.batchUploadretprodform.method = "POST";
		document.batchUploadretprodform.submit();
	}

}

function uploadDashRetProductFile() {
	var selFile = document.batchUploadretprodform.productuploadFilePath.value;
	if (selFile == 'null' || selFile == '') {
		alert('Please select file to upload the Products');
	} else {
		var isProdFleValid = validateUploadFileExt(selFile);
		if (!isProdFleValid) {
			alert('Product upload file should be of ext or csv format');
			return false;
		}
		/* show progress bar : ETA for Web 1.3 */
		showProgressBar();
		/* End */
		document.batchUploadretprodform.action = "/ScanSeeWeb/retailer/batchuploadretprod.htm";
		document.batchUploadretprodform.method = "POST";
		document.batchUploadretprodform.submit();
	}

}

function uploadImageFile() {
	var selFile = document.batchform.imageUploadFilePath.value
	if (selFile == 'null' || selFile == '') {
		alert('Please select file to upload the Images')
	} else {
		document.batchform.action = "uploadimage.htm";
		document.batchform.method = "POST";
		document.batchform.submit();
	}
}

function uploadAttribute() {
	var selFile = document.batchform.attributeuploadFile.value
	// alert(selFile)
	if (selFile == 'null' || selFile == '') {
		alert('Please select file to upload the Product attributes')
	} else {

		var isAttrFleValid = validateUploadFileExt(selFile);
		// alert(isAttrFleValid)

		if (!isAttrFleValid) {
			alert('Attribute upload file should be of txt or csv format')
			return false;
		}
		document.batchform.action = "uploadattribute.htm";
		document.batchform.method = "POST";
		document.batchform.submit();
	}
}

function uploadFileSupplier() {
	var selFile = document.batchform.productuploadFilePath.value
	if (selFile == 'null' || selFile == '') {
		alert('Please select the file to upload the Product(s) information')
	} else {
		var isProdFleValid = validateUploadFileExt(selFile);
		if (!isProdFleValid) {
			alert('Product upload file should be of txt or csv format');
			return false;
		}
		document.batchform.action = "uploadproductsupplier.htm";
		document.batchform.method = "POST";
		document.batchform.submit();
	}
}
function uploadImageSupplier() {

	var selFile = document.batchform.imageUploadFilePath.value
	if (selFile == 'null' || selFile == '') {
		alert('Please select Images,Audio or Video file');
	} else {
		var file = validateUploadAudioVideoImageFileExt(selFile);
		if (file) {
			document.batchform.action = "uploadaimageesupplier.htm";
			document.batchform.method = "POST";
			document.batchform.submit();
		} else {
			alert('Please select Images,Audio or Video file');
		}

	}

}
function uploadAttributeSupplier() {
	var selFile = document.batchform.attributeuploadFile.value
	if (selFile == 'null' || selFile == '') {
		alert('Please select file to upload the Product(s) attribute(s) information')
	} else {
		var isAttrFleValid = validateUploadFileExt(selFile);

		if (!isAttrFleValid) {
			alert('Attribute upload file should be of txt or csv format')
			return false;
		}
		document.batchform.action = "uploadattributesupplier.htm";
		document.batchform.method = "POST";
		document.batchform.submit();
	}

}

function loadReport() {

	var reportServerURL = document.getElementById("reportServerURL").value;
	var userID = document.getElementById("userID").value;
	var fromDate = document.getElementById("fromDate").value;
	var toDate = document.getElementById("toDate").value;
	if (fromDate == "") {
		alert("Please Select From Date");
	} else if (toDate == "") {
		alert("Please Select To Date");
	} else {

		var baseURLiframe1 = reportServerURL
				+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/AdSpendOverView&rs:Command=Render&rc:Parameters=false&";
		var baseURLiframe2 = reportServerURL
				+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/CouponDetails&rs:Command=Render&rc:Parameters=false&";
		var baseURLiframe3 = reportServerURL
				+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/DemographicBarGraph&rs:Command=Render&rc:Parameters=false&manufacid="
				+ userID;
		var baseURLiframe4 = reportServerURL
				+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/DemographicMap&rs:Command=Render";
		var baseURLiframe5 = reportServerURL
				+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/OverviewProductGraph&rs:Command=Render&rc:Parameters=false&manufacid="
				+ userID;
		var baseURLiframe6 = reportServerURL
				+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/OverViewProductPieChart&rs:Command=Render&rc:Parameters=false&manufacid="
				+ userID;
		var baseURLiframe7 = reportServerURL
				+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/ProductDetails&rs:Command=Render&rc:Parameters=false&";
		var baseURLiframe8 = reportServerURL
				+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/Scansee_WebReports&rs:Command=Render&rc:Parameters=false&";

		var fromDate = document.getElementById("fromDate").value;
		var toDate = document.getElementById("toDate").value;
		var dateRange = "From=" + fromDate + "&To=" + toDate;
		var dateRange2 = "fromdate=" + fromDate + "&todate=" + toDate;

		document.getElementById("iframe1").setAttribute('src',
				baseURLiframe1 + dateRange);
		document.getElementById("iframe2").setAttribute('src',
				baseURLiframe2 + dateRange);
		document.getElementById("iframe3").setAttribute('src', baseURLiframe3);
		document.getElementById("iframe4").setAttribute('src', baseURLiframe4);
		document.getElementById("iframe5").setAttribute('src', baseURLiframe5);
		document.getElementById("iframe6").setAttribute('src', baseURLiframe6);
		document.getElementById("iframe7").setAttribute('src',
				baseURLiframe7 + dateRange);
		document.getElementById("iframe8").setAttribute('src',
				baseURLiframe8 + dateRange2);

	}

}

function onloadReports() {

	var reportServerURL = document.getElementById("reportServerURL").value;
	var userID = document.getElementById("userID").value;

	var baseURLiframe1 = reportServerURL
			+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/AdSpendOverView&rs:Command=Render&rc:Parameters=false&";
	var baseURLiframe2 = reportServerURL
			+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/CouponDetails&rs:Command=Render&rc:Parameters=false&";
	var baseURLiframe3 = reportServerURL
			+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/DemographicBarGraph&rs:Command=Render&rc:Parameters=false&manufacid="
			+ userID;
	var baseURLiframe4 = reportServerURL
			+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/DemographicMap&rs:Command=Render";
	var baseURLiframe5 = reportServerURL
			+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/OverviewProductGraph&rs:Command=Render&rc:Parameters=false&manufacid="
			+ userID;
	var baseURLiframe6 = reportServerURL
			+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/OverViewProductPieChart&rs:Command=Render&rc:Parameters=false&manufacid="
			+ userID;
	var baseURLiframe7 = reportServerURL
			+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/ProductDetails&rs:Command=Render&rc:Parameters=false&";
	var baseURLiframe8 = reportServerURL
			+ "/Pages/ReportViewer.aspx?/ScanSee/Sample+Reports/Reports/Scansee_WebReports&rs:Command=Render&rc:Parameters=false&";

	var currentTime = new Date();

	var month = currentTime.getMonth();

	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	document.getElementById("fromDate").value = month + 1 + "/" + day + "/"
			+ year;
	document.getElementById("toDate").value = month + 2 + "/" + day + "/"
			+ year;
	var fromDate = document.getElementById("fromDate").value;
	var toDate = document.getElementById("toDate").value;
	var dateRange = "From=" + fromDate + "&To=" + toDate;
	var dateRange2 = "fromdate=" + fromDate + "&todate=" + toDate;
	document.getElementById("iframe1").setAttribute('src',
			baseURLiframe1 + dateRange);
	document.getElementById("iframe2").setAttribute('src',
			baseURLiframe2 + dateRange);
	document.getElementById("iframe3").setAttribute('src', baseURLiframe3);
	document.getElementById("iframe4").setAttribute('src', baseURLiframe4);
	document.getElementById("iframe5").setAttribute('src', baseURLiframe5);
	document.getElementById("iframe6").setAttribute('src', baseURLiframe6);
	document.getElementById("iframe7").setAttribute('src',
			baseURLiframe7 + dateRange);
	document.getElementById("iframe8").setAttribute('src',
			baseURLiframe8 + dateRange2);

}

function exportReport() {
	var reportServerURL = document.getElementById("reportServerURL").value;
	var fromDate = document.getElementById("fromDate").value;
	var toDate = document.getElementById("toDate").value;
	if (fromDate == "") {
		alert("Please Select From Date");
	} else if (toDate == "") {
		alert("Please Select To Date");
	} else {
		var fromDate = document.getElementById("fromDate").value;
		var toDate = document.getElementById("toDate").value;
		var dateRange = "fromdate=" + fromDate + "&todate=" + toDate;
		var e = document.getElementById("exportType");
		var exportType = e.options[e.selectedIndex].value;
		var csvURL = reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fScansee_WebReports&rs:Command=Render"
				+ reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fCoupon&rs:Command=Render"
				+ reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fAdOverView&rs:Command=Render"
				+ reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fProductDetails&rs:Command=Render"
				+ reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fHotdeal&rs:Command=Render"
				+ reportServerURL
				+ "/Pages/ReportViewer.aspx?%2fScanSee%2fSample+Reports%2fReports%2fProduct&rs:Command=Render&rc:Parameters=false&"
				+ dateRange + "&rs:Format=excel";
		var pdfURL = reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fScansee_WebReports&rs:Command=Render"
				+ reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fCoupon&rs:Command=Render"
				+ reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fAdOverView&rs:Command=Render"
				+ reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fProductDetails&rs:Command=Render"
				+ reportServerURL
				+ "?%2fScanSee%2fSample+Reports%2fReports%2fHotdeal&rs:Command=Render"
				+ reportServerURL
				+ "/Pages/ReportViewer.aspx?%2fScanSee%2fSample+Reports%2fReports%2fProduct&rs:Command=Render&rc:Parameters=false&"
				+ dateRange + "&rs:Format=pdf";

		if (exportType == "PDF") {
			window.open(pdfURL, '_top', null, null);
		} else if (exportType == "CSV") {
			window.open(csvURL, '_top', null, null);
		}
	}

}

function showImage() {
	document.productInfoVO.action = "uploadtemp.htm";
	document.productInfoVO.method = "POST";
	document.productInfoVO.submit();
	/*
	 * var imgSrcpath = "/ScanSeeWeb/images/"; var imgsrc =
	 * document.productSetupForm.imageFile.value; var image = imgSrcpath +
	 * imgsrc; document.productSetupForm.action = "uploadtempimg.htm";
	 * document.productSetupForm.method = "POST";
	 * document.productSetupForm.submit();
	 * document.getElementById('prodImage').setAttribute('src', image);
	 */
}

function editCoupon(couponID) {

	document.managecouponform.couponID.value = couponID;
	document.managecouponform.action = "editcoupon.htm";
	document.managecouponform.method = "GET";
	document.managecouponform.submit();
}

function searchCoupon() {

	document.managecouponform.action = "managecoupons.htm";
	document.managecouponform.method = "POST";
	document.managecouponform.submit();
}

function rerunCoupon(couponID) {

	document.managecouponform.couponID.value = couponID;
	document.managecouponform.action = "reruncoupon.htm";
	document.managecouponform.method = "GET";
	document.managecouponform.submit();
}

// *******************startLocation*********

function batchUpdateLocationSetup() {
	var selFile = document.locationsetupform.locationSetUpFile.value;
	
	if (selFile == 'null' || selFile == '') {
		alert('Please select a file to upload the Retailer Locations');
	} else {
		var isProdFleValid = validateUploadFileExtension(selFile);
		if (!isProdFleValid) {
			alert('Locations upload file should be of csv format');
			/*if (document.getElementById('imageFilePath.errors') != null) {
				document.getElementById('imageFilePath.errors').style.display = 'none';
			}*/
			var vLocMsg = $("#locMsg").text();
			 if (vLocMsg.length > 0) {
				 $("#locMsg").text("");
			 }
			 var vLocMsg = $("#locSuccessMsg").text();
			 if (vLocMsg.length > 0) {
				 $("#locSuccessMsg").text("");
			 }
			document.locationsetupform.locationSetUpFile.value = "";
			document.getElementById("locationSetUpFile").focus();
			return false;
		}
		/* show progress bar : ETA for Web 1.3 */
		showProgressBar();
		/* End */
		document.locationsetupform.action = "locationbatchupload.htm";
		document.locationsetupform.method = "POST";
		document.locationsetupform.submit();
	}
}

function validateUploadFileExtension(file) {
	if (file != '' && file != 'undefined') {
		var dotIndex = file.lastIndexOf('.');
		var length = file.length;
		var ext = file.substr(dotIndex + 1, length);
		if (ext == 'csv') {
			return true;
		} else {
			return false
		}
	}
}

// *******************EndLocation*********

function submitRegProdct(btn) {

	var attributes = [];
	var values = [];
	var errorFlag = false;
	$("#attrDpsly").find("tr").each(
			function(index) {

				// if(index > 0){
				var highlightObj = $($("#attrDpsly").find("tr")[index])
						.children();
				errorFlag = false;
				highlightObj.find("input").each(
						function(rVal) {

							var inputTagVal = $(this)[0].value;
							var name = $(this).attr("name");
							errorFlag = false;
							if (inputTagVal == '' || inputTagVal == 'undefined'
									|| inputTagVal == 'null') {
								alert('Please Enter values for attribute');
								errorFlag = true;
							} else {
								attributes[index] = name;
								values[index] = inputTagVal;
							}

						});
				if (errorFlag) {
					return false;
				}
				// }
			});

	if (!errorFlag) {
		var attrHtml = $('.attributesGrd').html();
		document.productInfoVO.attributeHtml.value = attrHtml;
		document.productInfoVO.prdAttributes.value = attributes.toString();
		document.productInfoVO.values.value = values.toString();
		document.productInfoVO.btnType.value = btn;
		document.productInfoVO.action = "registprodsetup.htm";
		document.productInfoVO.method = "POST";
		document.productInfoVO.submit();
	}

}

function addDashProduct() {

	var attributes = [];
	var values = [];
	var errorFlag = false;
	$("#attrDpsly").find("tr").each(
			function(index) {
				// if(index > 0){
				var highlightObj = $($("#attrDpsly").find("tr")[index])
						.children();
				errorFlag = false;
				highlightObj.find("input").each(
						function(rVal) {

							var inputTagVal = $(this)[0].value;
							var name = $(this).attr("name");
							errorFlag = false;
							if (inputTagVal == '' || inputTagVal == 'undefined'
									|| inputTagVal == 'null') {
								alert('Please Enter values for attribute');
								errorFlag = true;
							} else {
								attributes[index] = name;
								values[index] = inputTagVal;
							}

						});

				if (errorFlag) {
					return false;
				}
				// }
			});
	if (!errorFlag) {
		var attrHtml = $('.attributesGrd').html();
		document.productInfoVO.attributeHtml.value = attrHtml;
		document.productInfoVO.prdAttributes.value = attributes.toString();
		document.productInfoVO.values.value = values.toString();
		document.productInfoVO.action = "upload.htm";
		document.productInfoVO.method = "POST";
		document.productInfoVO.submit();
	}

}

function searchProdUPC() {

	var $cpNm = top.$('input[name="formName"]');
	var formName = $cpNm.val();
	var locID = null;
	if (formName == 'createCoupon') {

		locID = top.document.createcouponform.selRelLoc.value;

	} else if (formName == 'editCoupon') {
		locID = top.document.editcouponform.selRelLoc.value;

	} else if (formName == 'rerunCoupon') {
		locID = top.document.reruncouponform.selRelLoc.value;

	}

	document.produpclistform.retLocID.value = locID;
	document.produpclistform.action = "produpclist.htm";
	document.produpclistform.method = "POST";
	document.produpclistform.submit();
}

function searchrebateProdUPC() {

	var locID = top.document.addRebatesform.retailerLocID.value;
	document.produpclistForm.retailerLocID.value = locID;
	var retailid = top.document.addRebatesform.retailID.value;
	document.produpclistForm.retailID.value = retailid;

	document.produpclistForm.action = "rebateprodupclist.htm";
	document.produpclistForm.method = "POST";
	document.produpclistForm.submit();

}

function searchHotDealsProdUPC() {
	// var locID = top.document.addRebatesform.retailerLocID.value;
	// document.produpclistForm.retailerLocID.value = locID;
	// var retailid = top.document.addRebatesform.retailID.value;
	// document.produpclistForm.retailID.value = retailid;
	document.hotdealprodupclistForm.action = "prodHotDealList.htm";
	document.hotdealprodupclistForm.method = "POST";
	document.hotdealprodupclistForm.submit();
}

function searchdealsProducts() {
	document.hotdealprodupclistForm.action = "prodHotDealList.htm";
	document.hotdealprodupclistForm.method = "POST";
	document.hotdealprodupclistForm.submit();
}

function searchAds() {

	document.manageadform.action = "manageads.htm";
	document.manageadform.method = "POST";
	document.manageadform.submit();
}

// check for valid numeric values
function IsNumeric(e) {
	var key = window.event ? e.keyCode : e.which;
	var keychar = String.fromCharCode(key);
	reg = /[0-9]+(\.[0-9])?/;
	return reg.test(keychar);
}

/*
 * This function for Opening the iframe popup by using the any events & we
 * cann't able to access any thing in the screen except popup parameters:
 * popupId : Popup Container ID IframeID : Iframe ID url : page path i.e src of
 * a page height : Height of a popup width : Width of a popup name : here name
 * means Header text of a popup btnBool : if you have any buttons means we can
 * use it i.e true/false
 */
function openIframePopupRebate(popupId, IframeID, url, height, width, name,
		btnBool) {
	// alert("111111");
	var e = document.getElementById("retailerLocID");
	var retailerLocID = e.options[e.selectedIndex].value;

	// alert("222222");
	if (retailerLocID == 0) {
		alert("Please Select Location");
	} else {

		frameDiv = document.createElement('div');
		frameDiv.className = 'framehide';
		document.body.appendChild(frameDiv);
		document.getElementById(IframeID).setAttribute('src', url);
		// frameDiv.setAttribute('onclick', closePopup(obj,popupId));
		document.getElementById(popupId).style.display = "block"
		height = (height == "100%") ? frameDiv.offsetHeight - 20 : height;
		width = (width == "100%") ? frameDiv.offsetWidth - 16 : width;
		document.getElementById(popupId).style.height = height + "px"
		document.getElementById(popupId).style.width = width + "px"
		var marLeft = -1 * parseInt(width / 2);
		var marTop = -1 * parseInt(height / 2);
		document.getElementById('popupHeader').innerHTML = name;
		document.getElementById(popupId).style.marginLeft = marLeft + "px"
		document.getElementById(popupId).style.marginTop = marTop + "px"
		var iframeHt = height - 27;
		document.getElementById(IframeID).height = iframeHt + "px";
		if (btnBool) {
			var btnHt = height - 50;
			document.getElementById(IframeID).style.height = btnHt + "px";
		}
	}
}
/*
 * This function for Closing the iframe popup parameters: popupId : Popup
 * Container ID IframeID : Iframe ID
 */
function closeIframePopup(popupId, IframeID, funcUrl) {
	try {

		cument.body.removeChild(frameDiv);
		document.getElementById(popupId).style.display = "none";
		document.getElementById(popupId).style.height = "0px";
		document.getElementById(popupId).style.width = "0px";
		document.getElementById(popupId).style.marginLeft = "0px";
		document.getElementById(popupId).style.marginTop = "0px";
		document.getElementById(IframeID).removeAttribute('src');

	} catch (e) {
		//op.$('.framehide').remove();
		top.document.getElementById(popupId).style.display = "none";
		top.document.getElementById(popupId).style.height = "0px";
		top.document.getElementById(popupId).style.width = "0px";
		top.document.getElementById(popupId).style.marginLeft = "0px";
		top.document.getElementById(popupId).style.marginTop = "0px";
		top.document.getElementById(IframeID).removeAttribute('src');

	}
}

function callBackFunc() {
	/*
	 * var $cpNm = top.$('#couponName'); var chkArry1 = []
	 * $('input[name="pUpcChk"]:checked').each(function() {
	 * chkArry1.push(this.value) }); $cpNm.val(chkArry1)
	 */
}

/*
 * function createProfile() { document.createprofileform.action =
 * "createProfile.htm"; document.createprofileform.method = "POST";
 * document.createprofileform.submit(); }
 */

function createProfile_old() {
	var email = document.createprofileform.contactEmail.value;
	var retypeEmail = document.createprofileform.retypeEmail.value;
	var password = document.createprofileform.password.value;
	var repassword = document.createprofileform.retypePassword.value;
	var regEx = /^.+@.+\..{2,5}$/;
	if (!regEx.test(email)) {

		alert("Please Enter Valid EmailId");
		/*
		 * document.document.getElementById("contEml").value = "";
		 * document.document.getElementById("contREml").value = "";
		 * document.document.getElementById("contEml").focus();
		 */
	} else if (!(email.toString() == retypeEmail.toString())) {
		alert("Password fields does not match");
	} else if (!(password.toString() == repassword.toString())) {

		alert("Password Fileds Does't match");
	} else

	if (document.createprofileform.tandc.checked == false) {
		alert("Please accept the Terms and Conditions");
	} else {
		document.createprofileform.action = "createProfile.htm";
		document.createprofileform.method = "POST";
		document.createprofileform.submit();
	}
}

function retLocProducts() {
	$("#retLocID option:selected").removeAttr("selected");
	var multipleValues = $("#retLocID").val();
	var locIds = "";
	if (multipleValues == null) {
		var retLocID = document.getElementById("retLocID");
		for ( var i = 0; i < retLocID.length; i++) {
			var val = retLocID.options[i].value;
			locIds = locIds + val + ",";
		}
		var arraylocIds = locIds.split(',');
		$('#retLocID').val(arraylocIds);
	}
	document.retprodsetupform.action = "manageRetLocProd.htm";
	document.retprodsetupform.method = "POST";
	document.retprodsetupform.submit();
}

function retLocProductsSearch() {
	var multipleValues = $("#retLocID").val();
	var locIds = "";
	if (multipleValues == null) {
		var retLocID = document.getElementById("retLocID");
		for ( var i = 0; i < retLocID.length; i++) {
			var val = retLocID.options[i].value;
			locIds = locIds + val + ",";
		}
		var arraylocIds = locIds.split(',');
		$('#retLocID').val(arraylocIds);
	}
	document.retprodsetupform.action = "manageRetLocProd.htm";
	document.retprodsetupform.method = "POST";
	document.retprodsetupform.submit();
}

function openManageAttrPopUp(productId) {

	openIframePopup('ifrmPopup', 'ifrm', 'manageattr.htm?productID='
			+ productId, 440, 900, 'Manage Attributes');
}

function previewManageProductPopUp(productId, productName) {
	// document.myform.productName.value = productId;
	// document.getElementById('productNm').value = productName;
	window.open("/ScanSeeWeb/previewregprod.htm?productID=" + productId
			+ "&productName=" + productName);

	// openIframePopup('ifrmPopup', 'ifrm', 'previewregprod.htm?productID='+
	// productId+'&productName='+productName, 440, 900, 'Preview Products');
}

function openUploadPreviewPopUp() {
	var temp = document.retaileruploadlogoinfoform.retailerID.value;
	window.open('uploadRetailerLogo/previewretaler.htm?retailerID=' + temp,
			'RetailerUpload',
			'height=760,width=400,screenX=50,left=400,screenY=50,top=200');
}

function previewCouponPopUp() {
	/*
	 * if (document.getElementById('couponName.errors') != null) {
	 * document.getElementById('couponName.errors').style.display = 'none'; } if
	 * (document.getElementById('couponDiscountAmt.errors') != null) {
	 * document.getElementById('couponDiscountAmt.errors').style.display =
	 * 'none'; } if (document.getElementById('numOfCouponIssue.errors') != null) {
	 * document.getElementById('numOfCouponIssue.errors').style.display =
	 * 'none'; } if (document.getElementById('locationID.errors') != null) {
	 * document.getElementById('locationID.errors').style.display = 'none'; } if
	 * (document.getElementById('couponStartDate.errors') != null) {
	 * document.getElementById('couponStartDate.errors').style.display = 'none'; }
	 * if (document.getElementById('couponExpireDate.errors') != null) {
	 * document.getElementById('couponExpireDate.errors').style.display =
	 * 'none'; }
	 */
	document.createcouponform.action = "previewcouponretaler.htm";
	document.createcouponform.method = "POST";
	document.createcouponform.submit();
}
function previewReRunCouponPopUp() {
	document.reruncouponform.action = "previewreruncouponretailer.htm";
	document.reruncouponform.method = "POST";
	document.reruncouponform.submit();
}
function previewEditCouponPopUp() {
	document.editcouponform.action = "previeweditcoupon.htm";
	document.editcouponform.method = "POST";
	document.editcouponform.submit();
}

function previewEditSupplPopUp() {

	if (document.getElementById('slctLoc').checked == true) {
		document.editdealsform.dealForCityLoc.value = "Location";
	} else if (document.getElementById('slctCty').checked == true) {
		document.editdealsform.dealForCityLoc.value = "City";
	}

	document.editdealsform.action = "previeweditsuppldeal.htm";
	document.editdealsform.method = "POST";
	document.editdealsform.submit();

}
function previewReRunSupplDeals() {

	if (document.getElementById('slctLoc').checked == true) {
		document.rerundealsform.dealForCityLoc.value = "Location";
	} else if (document.getElementById('slctCty').checked == true) {
		document.rerundealsform.dealForCityLoc.value = "City";
	}

	document.rerundealsform.action = "previewrerunsuppldeals.htm";
	document.rerundealsform.method = "POST";
	document.rerundealsform.submit();

}
function previewRetAddRebPopUp() {
	document.addretrebatesForm.action = "previewretailerrebate.htm";
	document.addretrebatesForm.method = "POST";
	document.addretrebatesForm.submit();
}
function previewRetEditRebPopUp() {
	document.editretRebatesForm.action = "previewretailereditrebate.htm";
	document.editretRebatesForm.method = "POST";
	document.editretRebatesForm.submit();
}
function previewRetRerunRebates() {
	document.reRunretRebatesForm.action = "previewretailerrerunrebate.htm";
	document.reRunretRebatesForm.method = "POST";
	document.reRunretRebatesForm.submit();
}

function previewRebAdd() {

	document.addRebatesform.action = "previewsuppladdrebate.htm";
	document.addRebatesform.method = "POST";
	document.addRebatesform.submit();
}
function previewEditRebate() {
	document.editRebatesForm.action = "previewsuppleditrebate.htm";
	document.editRebatesForm.method = "POST";
	document.editRebatesForm.submit();
}
function previewRerunRebate() {
	document.reRunRebatesForm.action = "previewsupplrerunrebate.htm";
	document.reRunRebatesForm.method = "POST";
	document.reRunRebatesForm.submit();
}

function openManageAdioVideoPopUp(productId) {
	openIframePopup('ifrmPopup', 'ifrm', 'managemedia.htm?productID='
			+ productId, 550, 800, 'Manage Audio And Video');
}

function addNewRow() {
	var curRow = $("#locSetUpGrd tr:last").clone(true).insertAfter(
			'#locSetUpGrd tr:last');
	$("#locSetUpGrd tr:last input:text").val("");

}

function retProdSearch() {
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.retprodsetupform.action = "addseachprod.htm";
	document.retprodsetupform.method = "POST";
	document.retprodsetupform.submit();
}

/*
 * function associateRetProd(productID, obj) { var locId =
 * document.retprodsetupform.retLocID.value; if (locId == "") {
 * 
 * alert("Please select Location"); } else {
 * 
 * var retLocID = document.getElementById("retLocID"); var locIds = ""; for (
 * var i = 0; i < retLocID.length; i++) {
 * 
 * if (retLocID.options[i].selected) { var val = retLocID.options[i].value;
 * locIds = locIds + val + ","; } } var par = obj.parentNode; while
 * (par.nodeName.toLowerCase() != 'tr') { par = par.parentNode; } var desc = $(
 * $($("#prodinfotbl").find("tr")[par.rowIndex]).children()[3])
 * .children().val(); var price = $(
 * $($("#prodinfotbl").find("tr")[par.rowIndex]).children()[4])
 * .children().val(); var salePrice = $(
 * $($("#prodinfotbl").find("tr")[par.rowIndex]).children()[5])
 * .children().val(); var saleStartDate = $(
 * $($("#prodinfotbl").find("tr")[par.rowIndex]).children()[6])
 * .children().val(); var saleEndDate = $(
 * $($("#prodinfotbl").find("tr")[par.rowIndex]).children()[7])
 * .children().val();
 * 
 * if ((saleStartDate == "" && saleEndDate == "" && salePrice == "") ||
 * (saleStartDate != "" && saleEndDate != "" && salePrice != "")) { var
 * stringObj = "{\"productID\":\"" + productID + "\"," + "\"description\":\"" +
 * desc + "\"," + "\"price\":\"" + price + "\"," + "\"salePrice\":\"" +
 * salePrice + "\"," + "\"saleStartDate\":\"" + saleStartDate + "\"," +
 * "\"saleEndDate\":\"" + saleEndDate + "\"," + "\"location\":\"" + locIds +
 * "\"}";
 * 
 * $.ajaxSetup({ cache : false }); $.ajax({ type : "POST", url :
 * "/ScanSeeWeb/retailer/associateprod.htm", data : { 'data' : stringObj },
 * 
 * success : function(response) { if (response == 'FAILURE') { alert('Error
 * While Adding Product'); } else { alert(response) } }, error : function(e) {
 * alert('Error While Adding Product'); } }); } else if (salePrice != "") {
 * 
 * if ((saleStartDate == "" && saleEndDate == "")) { alert("Please Enter Start
 * Date and End Date") } else if (saleStartDate == "") { alert("Please Enter
 * Start Date") } else if (saleEndDate == "") { alert("Please Enter End Date") } }
 * else if ((saleStartDate != "" || saleEndDate != "")) { if (salePrice == "") {
 * alert("Please Enter Sale Price"); } } } }
 */

function searchRebate() {
	document.displayretailerrebatesform.action = "rebatesretailMfg.htm";
	document.displayretailerrebatesform.method = "POST";
	document.displayretailerrebatesform.submit();
}

function addRetailerRebate() {
	document.addretrebatesForm.action = "addretrebates.htm";
	document.addretrebatesForm.method = "POST";
	document.addretrebatesForm.submit();
}

function retailerRebateProdUPC() {
	var locID = top.document.addretrebatesForm.retailerLocID.value;
	document.retprodupclistForm.retailerLocID.value = locID;
	document.retprodupclistForm.action = "rebateretprodupclist.htm";
	document.retprodupclistForm.method = "POST";
	document.retprodupclistForm.submit();
}

// *******************startLocation*********

function saveGridResultList(objArr, pagenumber) {
	if (pagenumber == null || pagenumber == undefined) {
		document.locationsetupform.pageFlag.value = "false";
	} else {

		document.locationsetupform.pageNumber.value = pagenumber;
		document.locationsetupform.pageFlag.value = "true";
	}
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.locationsetupform.prodJson.value = "{\"locationData\":[" + objArr
			+ "]}";
	document.locationsetupform.action = "updatelocation.htm";
	document.locationsetupform.method = "POST";
	document.locationsetupform.submit();
}

function editLocationSetUp(pagenumber) {

	var validurlFlag = true;
	var objArr = [];
	var vRetLocList = [];

	var vRetailerLocID = $("#retailerLocID").val();
	if (vRetailerLocID != "null" && vRetailerLocID != "") {
		vRetLocList = vRetailerLocID.split(",");
	}

	$("#locSetUpGrd").find("tr").each(
			function(index) {
				if (index > 0) {
					var highlightObj = $($("#locSetUpGrd").find("tr")[index])
							.children();
					var object = {};

					highlightObj.find("input").each(function(rVal) {

						var inputTagVal = $(this)[0].value;
						var name = $(this).attr("name")

						if (name == 'uploadRetaillocationsID') {
							object.uploadRetaillocationsID = inputTagVal;

						} else if (name == 'retailID') {
							object.retailID = inputTagVal;

						} else if (name == 'storeIdentification') {
							object.storeIdentification = inputTagVal;

						} else if (name == 'address1') {
							object.address1 = inputTagVal;

						} else if (name == 'retailerLocationLatitude') {
							object.retailerLocationLatitude = inputTagVal;

						} else if (name == 'retailerLocationLongitude') {
							object.retailerLocationLongitude = inputTagVal;

						} else if (name == 'city') {
							object.city = inputTagVal

						} else if (name == 'state') {
							object.state = inputTagVal

						} else if (name == 'postalCode') {
							object.postalCode = inputTagVal

						} else if (name == 'phonenumber') {
							object.phonenumber = inputTagVal

						} else if (name == 'retailLocationUrl') {
							object.retailLocationUrl = inputTagVal
							
						} else if (name == 'keyword') {
							object.keyword = inputTagVal
							
						} else if (name == 'gridImgLocationPath') {
						    object.gridImgLocationPath = inputTagVal
						} else if (name == 'uploadImage') {
						   object.uploadImage = inputTagVal
					   }
						// }
					});

					highlightObj.find("select").each(function(rVal) {

						var inputTagVal = $(this)[0].value;
						var name = $(this).attr("name")

						if (name == 'contactTitle') {
							object.contactTitle = inputTagVal;
						}
					});

					objArr[index - 1] = object;
					/*
					 * }else{ // no change }
					 */

				}
			});

	if (objArr.length > 0) {
		var validationFlag = true;
		for ( var k = 0; k < objArr.length; k++) {
			var object = objArr[k];
			var retailID = object.retailID;
			var uploadRetaillocationsID = object.uploadRetaillocationsID;
			$("tr#" + uploadRetaillocationsID).removeClass("hilite");
			var storeIdentification = object.storeIdentification;
			var city = object.city;
			var state = object.state;
			var postalCode = object.postalCode;
			var address1 = object.address1;
			/*
			 * var contactFirstName = object.contactFirstName; var
			 * contactLastName = object.contactLastName; var contactTitle =
			 * object.contactTitle; var contactMobilePhone =
			 * object.contactMobilePhone; var contactEmail =
			 * object.contactEmail;
			 */
			var retailLocationUrl = object.retailLocationUrl;
			var keyword = object.keyword;
			var vLatitude = object.retailerLocationLatitude;
			var vLongitude = object.retailerLocationLongitude;
			var phonenumber = object.phonenumber;

			if (vRetailerLocID != "null" && vRetailerLocID != "") {
				if (storeIdentification == ''
						|| storeIdentification == 'undefined'
						|| storeIdentification == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (city == '' || city == 'undefined' || city == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (state == '' || state == 'undefined'
						|| state == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (postalCode == '' || postalCode == 'undefined'
						|| postalCode == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (address1 == '' || address1 == 'undefined'
						|| address1 == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} /*
				 * else if (contactFirstName == '' || contactFirstName ==
				 * 'undefined' || contactFirstName == 'null') { $("tr#" +
				 * uploadRetaillocationsID).addClass("requiredVal");
				 * validationFlag = false; } else if (contactLastName == '' ||
				 * contactLastName == 'undefined' || contactLastName ==
				 * 'null') { $("tr#" +
				 * uploadRetaillocationsID).addClass("requiredVal");
				 * validationFlag = false; } else if (contactTitle == '' ||
				 * contactTitle == 'undefined' || contactTitle == 'null') {
				 * $("tr#" +
				 * uploadRetaillocationsID).addClass("requiredVal");
				 * validationFlag = false; } else if (contactEmail == '' ||
				 * contactEmail == 'undefined' || contactEmail == 'null') {
				 * $("tr#" +
				 * uploadRetaillocationsID).addClass("requiredVal");
				 * validationFlag = false; }
				 */else if (vLatitude == '' || vLatitude == 'undefined'
						|| vLatitude == 'null') {
					$("tr#" + uploadRetaillocationsID).removeClass("hilite")
							.addClass("requiredVal");
					validationFlag = false;
				} else if (vLongitude == '' || vLongitude == 'undefined'
						|| vLongitude == 'null') {
					$("tr#" + uploadRetaillocationsID).removeClass("hilite")
							.addClass("requiredVal");
					validationFlag = false;
				} else if (phonenumber == '' || phonenumber == 'undefined'
						|| phonenumber == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else {
					$("tr#" + uploadRetaillocationsID).removeClass(
							"requiredVal");
				}
			} else {

				if (storeIdentification == ''
						|| storeIdentification == 'undefined'
						|| storeIdentification == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (city == '' || city == 'undefined' || city == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (state == '' || state == 'undefined'
						|| state == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (postalCode == '' || postalCode == 'undefined'
						|| postalCode == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (address1 == '' || address1 == 'undefined'
						|| address1 == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (phonenumber == '' || phonenumber == 'undefined'
						|| phonenumber == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else {
					$("tr#" + uploadRetaillocationsID).removeClass(
							"requiredVal");
				}
			}

			var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (retailLocationUrl != '') {
				if (!regexp.test(retailLocationUrl)) {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validurlFlag = false;
				}
			}

		}

		if (validationFlag) {
			if (!validurlFlag) {
				alert("Please enter a valid URL.");
				return false;
			}
			var strinObjArr = []
			for ( var k = 0; k < objArr.length; k++) {
				var object = objArr[k];

				var stringObj

				if (vRetailerLocID != "null" && vRetailerLocID != "") {
					stringObj = "{\"retailID\":\"" + object.retailID + "\","
							+ "\"uploadRetaillocationsID\":\""
							+ object.uploadRetaillocationsID + "\","
							+ "\"storeIdentification\":\""
							+ object.storeIdentification + "\","
							+ "\"address1\":\"" + object.address1 + "\","
							+ "\"city\":\"" + object.city + "\","
							+ "\"state\":\"" + object.state + "\","
							+ "\"postalCode\":\"" + object.postalCode + "\","
							+ "\"phonenumber\":\"" + object.phonenumber + "\","
							+ "\"retailLocationUrl\":\""
							+ object.retailLocationUrl + "\","
							+ "\"keyword\":\"" + object.keyword + "\","
							+ "\"retailerLocationLatitude\":\""
							+ object.retailerLocationLatitude + "\","
							+ "\"retailerLocationLongitude\":\""
							+ object.retailerLocationLongitude + "\","
							+ "\"gridImgLocationPath\":\""
							+ object.gridImgLocationPath +  "\","
							+ "\"uploadImage\":\""
							+ object.uploadImage + "\"}";
				} else {
					stringObj = "{\"retailID\":\"" + object.retailID + "\","
							+ "\"uploadRetaillocationsID\":\""
							+ object.uploadRetaillocationsID + "\","
							+ "\"storeIdentification\":\""
							+ object.storeIdentification + "\","
							+ "\"address1\":\"" + object.address1 + "\","
							+ "\"city\":\"" + object.city + "\","
							+ "\"state\":\"" + object.state + "\","
							+ "\"postalCode\":\"" + object.postalCode + "\","
							+ "\"phonenumber\":\"" + object.phonenumber + "\","
							+ "\"retailLocationUrl\":\""
							+ object.retailLocationUrl + "\","
							+ "\"keyword\":\"" + object.keyword + "\","
							+ "\"retailerLocationLatitude\":\""
							+ object.retailerLocationLatitude + "\","
							+ "\"retailerLocationLongitude\":\""
							+ object.retailerLocationLongitude + "\","
							+ "\"gridImgLocationPath\":\""
							+ object.gridImgLocationPath + "\","
							+ "\"uploadImage\":\""
							+ object.uploadImage + "\"}";
				}
				strinObjArr[k] = stringObj;
			}
			// Submtitting to save the changes.
			saveGridResultList(strinObjArr.toString(), pagenumber)
		} else {

			alert("Please Enter values for the mandatory fields for the highlighted rows in the screen");
			return false;
		}
	} else {
		alert("Please upload locations and Click on Save to Save the Changes")
	}

}

function next() {
	document.locationform.action = "productManagement.htm";
	document.locationform.method = "POST";
	document.locationform.submit();
}

function saveNcallNextRetailLocation(pageNumber) {
	updateLocationInfo(pageNumber)
}

// *******************EndLocation*********

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if ((charCode > 47 && charCode < 58) || charCode < 31)
		return true;

	return false;
}

function isNumberKeyPhone(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31)
		return true;

	return false;
}

// *******************start choose plan for retailer***********************

function retailerChoosePlan() {
	document.myForm.action = "retailerchoosePlan.htm";
	document.myForm.method = "GET";
	document.myForm.submit();

}

function retailerPlanInfo(total) {

	var productJson = [];
	var productObj = {};

	var objArr = [];
	$("#choosePln").find("tr").each(function(index) {
		if (index > 0) {
			var highlightObj = $($("#choosePln").find("tr")[index]).children();
			var object = {};
			var stringObj;
			// alert(index)
			highlightObj.find("input").each(function(rVal) {
				// if(rVal > 0){
				var inputTagVal = $(this)[0].value;
				var name = $(this).attr("name")
				// alert(name)
				// alert(name+" "+inputTagVal)

				if (name == 'productId') {
					object.productId = inputTagVal;
				} else if (name == 'qty') {
					object.qty = inputTagVal;
				} else if (name == 'price') {
					object.price = inputTagVal;
				} else if (name == 'total') {
					object.total = inputTagVal;
				}

				// }
			});
			if (Object.getOwnPropertyNames(object).length > 0) {
				objArr[index - 1] = object;
			}

		}
	});

	// Converting Sting JSOn

	var strinObjArr = []
	for ( var k = 0; k < objArr.length; k++) {
		var object = objArr[k];
		var stringObj = "{\"productID\":\"" + object.productId + "\","
				+ "\"quantity\":\"" + object.qty + "\"," + "\"price\":\""
				+ object.price + "\"," + "\"subTotal\":\"" + object.total
				+ "\"}";
		strinObjArr[k] = stringObj;
	}

	document.myForm.planJson.value = "{\"planData\":[" + strinObjArr.toString()
			+ "]}";
	document.myForm.grandTotal.value = document
			.getElementById("discountsubtotal").value;
	document.myForm.productId.value = "";
	document.myForm.action = "retailerplanInfo.htm";
	document.myForm.method = "POST";
	document.myForm.submit();

}

function confirmPayment() {
	document.myForm.action = "paymentconfirm.htm";
	document.myForm.method = "post";
	document.myForm.submit();

}

function retailerConfirmPayment() {
	document.myForm.action = "retailerpaymentconfirm.htm";
	document.myForm.method = "post";
	document.myForm.submit();

}

function savePlanInfo() {
	// Spring won't supports special characters in the form fields. So setting
	// to dummy value before submission
	$('#creditCardNumber').val('');
	$('#cVV').val('');
	$('#month').val('00');
	$('#year').val('0000');

	document.chooseplanform.action = "saveplanInfo.htm";
	document.chooseplanform.method = "POST";
	document.chooseplanform.submit();
}

function saveRetailerPlanInfo() {
	// Spring won't supports special characters in the form fields. So setting
	// to dummy value before submission
	$('#creditCardNumber').val('');
	$('#cVV').val('');
	$('#month').val('00');
	$('#year').val('0000');

	document.retchooseplanform.action = "saveretailerplanInfo.htm";
	document.retchooseplanform.method = "POST";
	document.retchooseplanform.submit();
}

// **************************end choose plan********************

function retailerRerunRebates(rebateId) {
	// alert(productId)
	// alert(rebateId)
	document.displayretailerrebatesform.rebateID.value = rebateId;
	// document.displayretailerrebatesform.rebproductId.value = productId;
	document.displayretailerrebatesform.action = "rerunretrebate.htm";
	document.displayretailerrebatesform.method = "GET";
	document.displayretailerrebatesform.submit();
}
// **************************Start Search****************
function searchDeals() {

	document.productform.action = "searchdeal.htm";
	document.productform.method = "post";
	document.productform.submit();

}

function searchProductWithDeals() {
	document.productform.action = "searchproductwithdeal.htm";
	document.productform.method = "post";
	document.productform.submit();

}

function productInfoWithDeals(productID) {

	document.productform.productID.value = productID;
	document.productform.action = "productInfowithdeal.htm";
	document.productform.method = "POST";
	document.productform.submit();
}

function productDealInfo(hotDealID) {

	document.productform.hotDealID.value = hotDealID;
	document.productform.action = "productDealInfo.htm";
	document.productform.method = "POST";
	document.productform.submit();
}
// **************************End Search****************
function saveRetRerunRebates() {
	document.reRunretRebatesForm.action = "rerunretrebate.htm";
	document.reRunretRebatesForm.method = "POST";
	document.reRunretRebatesForm.submit();
}

function editretRebates(rebateId) {
	document.displayretailerrebatesform.rebateID.value = rebateId;
	document.displayretailerrebatesform.action = "editretrebates.htm";
	document.displayretailerrebatesform.method = "GET";
	document.displayretailerrebatesform.submit();
}

function saveRetEditRebates() {
	document.editretRebatesForm.action = "editretrebates.htm";
	document.editretRebatesForm.method = "POST";
	document.editretRebatesForm.submit();
}

function clearBacthUploadForm() {
	var r = confirm("Do you really want to clear the form")
	if (r == true) {
		document.batchform.reset();

		if (document.getElementById('productuploadFilePath.errors') != null) {
			document.getElementById('productuploadFilePath.errors').style.display = 'none';
		}
		if (document.getElementById('attributeuploadFile.errors') != null) {
			document.getElementById('attributeuploadFile.errors').style.display = 'none';
		}
		if (document.getElementById('imageUploadFilePath.errors') != null) {
			document.getElementById('imageUploadFilePath.errors').style.display = 'none';
		}
	}
}

function uploadImage() {
	var imagePath = document.productInfoVO.imageFile.value;

	if (imagePath == '' || imagePath == 'undefined') {
		alert('Please select image for upload')
		return false;
	} else {
		// var validImage = validateUploadImageFileExt(imagePath);
		document.productInfoVO.action = "uploadtempimg.htm";
		document.productInfoVO.method = "POST";
		document.productInfoVO.submit();

	}
}

function uploadDashProdImage() {
	var imagePath = document.productInfoVO.imageFile.value;
	// commented code fot testing the array of image
	/*
	 * alert(document.productInfoVO.imageFile.length) for(var i=0;i<document.productInfoVO.imageFile.length;i++){
	 * alert(document.productInfoVO.imageFile[i].value) }
	 */
	if (imagePath == '' || imagePath == 'undefined') {
		alert('Please select image for upload')
		return false;
	} else {
		// var validImage = validateUploadImageFileExt(imagePath);
		document.productInfoVO.action = "uploadDashboardtempimg.htm";
		document.productInfoVO.method = "POST";
		document.productInfoVO.submit();
	}
}

function checkSubmit(e) {
	if (e && e.keyCode == 13) {
		document.forms[0].submit();
	}
}

function retailerPlanInfo() {

	var skipPayment = document.getElementById("skipPayment").value;
	if (skipPayment == 'skippayment') {
		document.myForm.action = "retailerhome.htm";
		document.myForm.method = "GET";
		document.myForm.submit();
	} else {
		var productJson = [];
		var productObj = {};

		var objArr = [];
		$("#choosePln").find("tr").each(
				function(index) {
					if (index > 0) {
						var highlightObj = $($("#choosePln").find("tr")[index])
								.children();
						var object = {};
						var stringObj;
						// alert(index)
						highlightObj.find("input").each(function(rVal) {
							// if(rVal > 0){
							var inputTagVal = $(this)[0].value;
							var name = $(this).attr("name")
							// alert(name)
							// alert(name+" "+inputTagVal)

							if (name == 'productId') {
								object.productId = inputTagVal;
							} else if (name == 'qty') {
								object.qty = inputTagVal;
							} else if (name == 'price') {
								object.price = inputTagVal;
							} else if (name == 'total') {
								object.total = inputTagVal;
							}

							// }
						});
						if (Object.getOwnPropertyNames(object).length > 0) {
							objArr[index - 1] = object;
						}

					}
				});

		// Converting Sting JSOn

		var strinObjArr = []
		for ( var k = 0; k < objArr.length; k++) {
			var object = objArr[k];
			var stringObj = "{\"productID\":\"" + object.productId + "\","
					+ "\"quantity\":\"" + object.qty + "\"," + "\"price\":\""
					+ object.price + "\"," + "\"subTotal\":\"" + object.total
					+ "\"}";
			strinObjArr[k] = stringObj;
		}

		document.myForm.planJson.value = "{\"planData\":["
				+ strinObjArr.toString() + "]}";
		document.myForm.grandTotal.value = document
				.getElementById("discountsubtotal").value;
		document.myForm.productId.value = "";
		var paymentType = $('input:radio[name=radio1]:checked').val();
		document.myForm.action = "retailerplanInfo.htm?paymentType="
				+ paymentType;
		document.myForm.action = "retailerplanInfo.htm";
		document.myForm.method = "POST";
		document.myForm.submit();

	}

}

function getRetailersDealsByName() {
	showProgressBar();
	document.retailerDealsForm.action = "hotDealRetailer.htm";
	document.retailerDealsForm.method = "POST";
	document.retailerDealsForm.submit();
}

function clearAddProductForm() {
	var r = confirm("Do you really want to clear the form")
	if (r == true) {
		$('input:file').MultiFile('reset');
		document.getElementById("productInfoVO").reset();
		// document.getElementById('retailID').selectedIndex = -1;

		document.productInfoVO.productUPC.value = "";
		document.productInfoVO.productName.value = "";
		document.productInfoVO.modelNumber.value = "";
		document.productInfoVO.suggestedRetailPrice.value = "";
		document.productInfoVO.productUPC.value = "";
		document.productInfoVO.productName.value = "";
		document.productInfoVO.modelNumber.value = "";
		document.productInfoVO.suggestedRetailPrice.value = "";
		// document["image"].src = "";
		document.productInfoVO.longDescription.value = "";
		document.productInfoVO.warrantyORService.value = "";
		document.productInfoVO.shortDescription.value = "";
		document.productInfoVO.productCategory.value = "0";

		if (document.getElementById('productUPC.errors') != null) {
			document.getElementById('productUPC.errors').style.display = 'none';
		}
		if (document.getElementById('productName.errors') != null) {
			document.getElementById('productName.errors').style.display = 'none';
		}
		if (document.getElementById('suggestedRetailPrice.errors') != null) {
			document.getElementById('suggestedRetailPrice.errors').style.display = 'none';
		}
		if (document.getElementById('longDescription.errors') != null) {
			document.getElementById('longDescription.errors').style.display = 'none';
		}

		$("#attrDpsly > tbody").empty();
		$("#attrDispDiv").css('display', 'none');

	}
}

function uploadProductImage() {
	document.manageform.mediaType.value = "Image";
	var imagePath = document.manageform.imageFile.value;
	var productId = document.manageform.productID.value;
	if (imagePath == '' || imagePath == 'undefined') {
		alert('Please select image for upload')
		return false;
	} else {
		document.manageform.action = "managemedia.htm";
		document.manageform.method = "POST";
		document.manageform.submit();
	}
}

function UploadProdAudioVideo(type) {
	if (type == 'Audio') {
		var audioPath = document.manageform.audioFile.value;
		document.manageform.mediaType.value = type;
		if (audioPath == '' || audioPath == 'undefined') {
			alert('Please select Audio for upload')
			// return false;
		} else {
			// var validAudio = validateUploadAudioFileExt(audioPath);
			// if (validAudio) {
			document.manageform.action = "managemedia.htm";
			document.manageform.method = "POST";
			document.manageform.submit();
		}// else {
		// alert('Audio file format should be mp4 or mp3')
		// }
	}

	if (type == 'Video') {

		var videoPath = document.manageform.videoFile.value;
		document.manageform.mediaType.value = type;
		if (videoPath == '' || videoPath == 'undefined') {
			alert('Please select Video for upload')
			// return false;
		} else {

			// var validVideo = validateUploadAudioVideoFileExt(videoPath);
			// if (validVideo) {
			document.manageform.action = "managemedia.htm";
			document.manageform.method = "POST";
			document.manageform.submit();
		}// else {
		// alert('Video file format should be mp4')
		// }
	}

}

function saveNcallNextManageProducts(pageNumber) {
	saveMangageProducts(submitFlag, pageNumber);
}

function deleteRetProd(productID, obj) {
	var productDel = confirm("Are you sure you want to delete this product associated with Retailer Location ?")
	if (productDel == true) {
		$('#deleteprod').live('click', function() {
			alert("hi")
			var options = {

				url : "/ScanSeeWeb/retailer/deleteprod.htm",
				type : "POST"

			};

			$('#retprodsetupform').submit(function() {
				// inside event callbacks 'this' is the DOM element so we first
				// wrap it in a jQuery object and then invoke ajaxSubmit
				$(this).ajaxSubmit(options);

				// !!! Important !!!
				// always return false to prevent standard browser submit and
				// page navigation
				return false;
			});

		});

	}
}

function createProfile() {

	document.createprofileform.action = "createProfile.htm";
	document.createprofileform.method = "POST";
	document.createprofileform.submit();
}

function retDeleteRetProd(productID, obj) {
	var productDel = confirm("Are you sure you want to delete this product associated with Retailer Location ?")
	if (productDel == true) {
		document.retprodsetupform.productID.value = productID;
		document.retprodsetupform.action = "/ScanSeeWeb/retailer/registProductsetup.htm";
		document.retprodsetupform.method = "POST";
		document.retprodsetupform.submit();
	}
}

function preferencesSubmit() {

	document.editdealsform.action = "preferences.htm";
	document.editdealsform.method = "POST";
	document.editdealsform.submit();
}

function schoolOnsubmit() {

	document.selectschoolform.action = "selectschool.htm";
	document.selectschoolform.method = "POST";
	document.selectschoolform.submit();

}
function doubleCheck() {

	document.editdealsform.action = "selectschool.htm";
	document.editdealsform.method = "POST";
	document.editdealsform.submit();

}

function uploadRetLogo(nav) {
	var imagePath = document.retaileruploadlogoinfoform.retailerLogo.value;
	if (imagePath == '' || imagePath == 'undefined') {
		alert('Please Select Retailer Logo to upload')
		return false;
	} else {
		var validImage = validateUploadImageFileExt(imagePath);
		if (validImage) {

			// var isImgSizeNotProper =
			// imgExceedSize(document.getElementById('retailerLogo'),900,900);
			// if(!isImgSizeNotProper)
			// {
			// document.retaileruploadlogoinfoform.action =
			// "/ScanSeeWeb/retailer/uploadRetailerLogo.htm";
			// document.retaileruploadlogoinfoform.method = "POST";
			// document.retaileruploadlogoinfoform.submit();
			// }

			document.retaileruploadlogoinfoform.navigation.value = nav;
			document.retaileruploadlogoinfoform.action = "/ScanSeeWeb/retailer/uploadRetailerLogo.htm";
			document.retaileruploadlogoinfoform.method = "POST";
			document.retaileruploadlogoinfoform.submit();
		} else {
			alert('Image file format should be png')
		}
	}
}

function uploadRetLogoWithCroppedImage(x, y, w, h, img) {
	// document.retaileruploadlogoinfoform.action =
	// "/ScanSeeWeb/retailer/uploadRetailerLogo.htm?x="+x+"&y="+y+"&w="+w+"&h="+h;//+"&imgValue="+img;
	top.document.retaileruploadlogoinfoform.action = "/ScanSeeWeb/retailer/uploadCroppedLogo.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;

	top.document.retaileruploadlogoinfoform.method = "POST";
	top.document.retaileruploadlogoinfoform.submit();
}

function uploadRetLogoDashboard() {
	var imagePath = document.retaileruploadlogoinfoform.retailerLogo.value;
	if (imagePath == '' || imagePath == 'undefined') {
		alert('Please Select Retailer Logo to upload')
		return false;
	} else {
		var validImage = validateUploadImageFileExt(imagePath);
		if (validImage) {
			document.retaileruploadlogoinfoform.action = "/ScanSeeWeb/retailer/uploadRetailerLogoDashboard.htm";
			document.retaileruploadlogoinfoform.method = "POST";
			document.retaileruploadlogoinfoform.submit();
		} else {
			alert('Image file format should be png')
		}
	}
}

function previewSupplierHotDeals() {

	if (document.getElementById('slctLoc').checked == true) {
		document.addhotdealform.dealForCityLoc.value = "Location";
	} else if (document.getElementById('slctCty').checked == true) {
		document.addhotdealform.dealForCityLoc.value = "City";
	}

	document.addhotdealform.action = "previewHotDealSupplier.htm";
	document.addhotdealform.method = "POST";
	document.addhotdealform.submit();
}

function previewRetailerAds() {
	document.createbanneradform.action = "/ScanSeeWeb/retailer/previewRetailerAds.htm";
	document.createbanneradform.method = "POST";
	document.createbanneradform.submit();
}

function submitRetailerAds() {
	showProgressBar();
	document.createbanneradform.action = "/ScanSeeWeb/retailer/createbannerad.htm";
	document.createbanneradform.method = "POST";
	document.createbanneradform.submit();
}

function submitCouponInfo() {

	/*
	 * var locID = document.getElementById("locationID");
	 * 
	 * var selLoc = ""; for ( var i = 0; i < locID.length; i++) { if
	 * (locID.options[i].selected == true && locID.options[i].value != "") {
	 * selLoc = selLoc + locID.options[i].value + ","; } } if (selLoc != "") {
	 * 
	 * document.createcouponform.locationID.value = selLoc; }
	 */

	document.createcouponform.action = "/ScanSeeWeb/retailer/createcoupon.htm";
	document.createcouponform.method = "POST";
	document.createcouponform.submit();
}
function saveEditCoupon() {
	document.editcouponform.action = "/ScanSeeWeb/retailer/editcoupon.htm";
	document.editcouponform.method = "POST";
	document.editcouponform.submit();

}
function saveRerunCoupon() {
	document.reruncouponform.action = "/ScanSeeWeb/retailer/reruncoupon.htm";
	document.reruncouponform.method = "POST";
	document.reruncouponform.submit();

}

function getDealsForCity(popCentrID, dMAName) {

	document.hotdealmainpage.populationCenterID.value = popCentrID;
	document.hotdealmainpage.popCntCity.value = dMAName;
	document.hotdealmainpage.action = "/ScanSeeWeb/shopper/hotdealforcity.htm";
	document.hotdealmainpage.method = "POST";
	document.hotdealmainpage.submit();

}

function getPopulationCenters() {

	var populationCenterName = document.hotdealmainpage.dMAName.value;
	if (populationCenterName == "" || populationCenterName == null) {
		document.hotdealmainpage.action = "/ScanSeeWeb/shopper/populationcenterslist.htm";
		document.hotdealmainpage.method = "GET";
		document.hotdealmainpage.submit();
	} else {
		document.hotdealmainpage.action = "/ScanSeeWeb/shopper/populationcenterslist.htm";
		document.hotdealmainpage.method = "POST";
		document.hotdealmainpage.submit();
	}
}

function getHotDealInfo(hotDealID) {
	document.hotdealmainpage.hotDealID.value = hotDealID;
	document.hotdealmainpage.action = "/ScanSeeWeb/shopper/hotdealinfo.htm";
	document.hotdealmainpage.method = "POST";
	document.hotdealmainpage.submit();
}

function getHotDealforCat() {

	var hdCat = document.hotdealmainpage.categoryId.value;

	if (hdCat != "") {
		document.hotdealmainpage.action = "/ScanSeeWeb/shopper/hotdealforcat.htm";
		document.hotdealmainpage.method = "POST";
		document.hotdealmainpage.submit();
	}

}

function searchProdUPCHotdeal() {

	document.produpclisthotdealform.action = "produpclisthotdeal.htm";
	document.produpclisthotdealform.method = "POST";
	document.produpclisthotdealform.submit();
}

function saveFavCat() {
	var checkBoxes = $("input[name=favCatId]");
	var favCatID = "";
	$.each(checkBoxes, function() {
		if ($(this).attr('checked')) {

			favCatID = favCatID.concat($(this).val(), ",")
		}
	});

	$.ajax({
		type : "POST",
		url : "/ScanSeeWeb/shopper/hdcategoryPref.htm",

		data : {
			'category' : favCatID
		},
		success : function(response) {
			if (response == "SUCCESS") {
				alert('Preferred category saved!!!');
			} else {
				alert('Error occurred while saving Preferred category!!!');
			}
		},
		error : function(e) {
			alert('Error: ' + e);
		}
	});

}

function searchHotDeal() {
	document.hotdealmainpage.action = "/ScanSeeWeb/shopper/hotdealprodlist.htm";
	document.hotdealmainpage.method = "POST";
	document.hotdealmainpage.submit();

}

function searchHotDealLocation() {
	document.hotdealmainpage.action = "/ScanSeeWeb/shopper/hotdealforloc.htm";
	document.hotdealmainpage.method = "POST";
	document.hotdealmainpage.submit();

}

/**
 * **********************************************************Start of Supplier
 * HotDeal Methods*********************************************
 */
function addHotDeals() {
	document.addhotdealform.action = "dailyDealsmfg.htm";
	document.addhotdealform.method = "POST";
	document.addhotdealform.submit();

}
function hotDealAddSupplier() {

	if (document.getElementById("slctCty").checked == true) {

		document.addhotdealform.dealForCityLoc.value = "City";
		addHotDeals();
	} else if (document.getElementById("slctLoc").checked == true) {

		document.addhotdealform.dealForCityLoc.value = "Location";
		addHotDeals();
	} else {

		document.addhotdealform.dealForCityLoc.value = "NoCityLoc";
		addHotDeals();
	}

}
function hotDealEditSaveInfo() {

	document.editdealsform.action = "dailyDealsEdit.htm";
	document.editdealsform.method = "POST";
	document.editdealsform.submit();
}
function hotDealEditSupplier() {

	if (document.getElementById("slctCty").checked == true) {

		document.editdealsform.dealForCityLoc.value = "City";
		hotDealEditSaveInfo();
	} else if (document.getElementById("slctLoc").checked == true) {

		document.editdealsform.dealForCityLoc.value = "Location";
		hotDealEditSaveInfo();
	} else {

		document.editdealsform.dealForCityLoc.value = "NoCityLoc";
		hotDealEditSaveInfo();
	}

}

function hotDealReRunSaveSupplier() {

	if (document.getElementById("slctCty").checked == true) {

		document.rerundealsform.dealForCityLoc.value = "City";
		hotDealReRunSaveInfo();
	} else if (document.getElementById("slctLoc").checked == true) {

		document.rerundealsform.dealForCityLoc.value = "Location";
		hotDealReRunSaveInfo();
	} else {

		document.rerundealsform.dealForCityLoc.value = "NoCityLoc";
		hotDealReRunSaveInfo();
	}

}

function hotDealReRunSaveInfo() {

	document.rerundealsform.action = "rerunHotDeals.htm";
	document.rerundealsform.method = "POST";
	document.rerundealsform.submit();
}

function openIframePopupCoupon(page) {
	var locID = document.getElementById("locationID");

	var selLoc = "";
	for ( var i = 0; i < locID.length; i++) {
		if (locID.options[i].selected == true && locID.options[i].value != "") {
			selLoc = selLoc + locID.options[i].value + ",";
		}

	}
	if (selLoc != "") {

		if (page == 'add') {
			document.createcouponform.selRelLoc.value = selLoc;
		} else if (page == 'edit') {
			document.editcouponform.selRelLoc.value = selLoc;

		}

	}

	openIframePopup('ifrmPopup', 'ifrm', 'produpclist.htm', 420, 600,
			'View Product/UPC')

}
/**
 * **********************************************************End of Supplier
 * HotDeal Methods*********************************************
 */

/**
 * **********************************************************Start of Retailer
 * HotDeal Methods*********************************************
 */

function addRetailerHotDeals() {
	showProgressBar();
	document.addhotdealretailerform.action = "/ScanSeeWeb/retailer/hotDealRetailoradd.htm";
	document.addhotdealretailerform.method = "POST";
	document.addhotdealretailerform.submit();

}

function hotDealAddRetailer() {

	if (document.getElementById("slctCty").checked == true) {
		document.addhotdealretailerform.dealForCityLoc.value = "City";
	} else if (document.getElementById("slctLoc").checked == true) {
		document.addhotdealretailerform.dealForCityLoc.value = "Location";
	} else {
		document.addhotdealretailerform.dealForCityLoc.value = "NoCityLoc";
	}
	addRetailerHotDeals();
}

function hotDealEditRetailer() {
	if (document.getElementById("slctCty").checked == true) {

		document.edithotdealretailerform.dealForCityLoc.value = "City";
		editRetailerHotDeals();
	} else if (document.getElementById("slctLoc").checked == true) {

		document.edithotdealretailerform.dealForCityLoc.value = "Location";
		editRetailerHotDeals();
	} else {

		document.edithotdealretailerform.dealForCityLoc.value = "NoCityLoc";
		editRetailerHotDeals();
	}
}

function editRetailerHotDeals() {
	document.edithotdealretailerform.action = "/ScanSeeWeb/retailer/retailerdealedit.htm";
	document.edithotdealretailerform.method = "POST";
	document.edithotdealretailerform.submit();

}

function hotDealReRunSaveRetailer() {

	if (document.getElementById("slctCty").checked == true) {

		document.retailerHotDealsReRunForm.dealForCityLoc.value = "City";
		rerunRetailerHotDeals();
	} else if (document.getElementById("slctLoc").checked == true) {

		document.retailerHotDealsReRunForm.dealForCityLoc.value = "Location";
		rerunRetailerHotDeals();
	} else {

		document.retailerHotDealsReRunForm.dealForCityLoc.value = "NoCityLoc";
		rerunRetailerHotDeals();
	}
}

function rerunRetailerHotDeals() {
	document.retailerHotDealsReRunForm.action = "/ScanSeeWeb/retailer/retailerdealrerun.htm";
	document.retailerHotDealsReRunForm.method = "POST";
	document.retailerHotDealsReRunForm.submit();

}

function previewEditdealPopUp() {

	if (document.getElementById('slctLoc').checked == true) {
		document.edithotdealretailerform.dealForCityLoc.value = "Location";
	} else if (document.getElementById('slctCty').checked == true) {
		document.edithotdealretailerform.dealForCityLoc.value = "City";
	}

	document.edithotdealretailerform.action = "previeweditdeals.htm";
	document.edithotdealretailerform.method = "POST";
	document.edithotdealretailerform.submit();
}

function previewRetailerHotDeals() {

	if (document.getElementById('slctLoc').checked == true) {
		document.addhotdealretailerform.dealForCityLoc.value = "Location";
	} else if (document.getElementById('slctCty').checked == true) {
		document.addhotdealretailerform.dealForCityLoc.value = "City";
	}
	document.addhotdealretailerform.action = "/ScanSeeWeb/retailer/previewHotDealRetailor.htm";
	document.addhotdealretailerform.method = "POST";
	document.addhotdealretailerform.submit();
}
function previewReRundealPopUp() {

	if (document.getElementById('slctLoc').checked == true) {
		document.retailerHotDealsReRunForm.dealForCityLoc.value = "Location";
	} else if (document.getElementById('slctCty').checked == true) {
		document.retailerHotDealsReRunForm.dealForCityLoc.value = "City";
	}

	document.retailerHotDealsReRunForm.action = "previewrerundeals.htm";
	document.retailerHotDealsReRunForm.method = "POST";
	document.retailerHotDealsReRunForm.submit();
}
/**
 * **********************************************************End of Retailer
 * HotDeal Methods*********************************************
 */

function clearFormRegisterProductSetUp() {
	var r = confirm("Do you really want to clear the form")
	if (r == true) {

		$('input:file').MultiFile('reset');
		document.getElementById("productInfoVO").reset();
		document.getElementById('retailID').selectedIndex = -1;

		$('#imgSwap img').attr('src', '/ScanSeeWeb/images/alternateImg.png');

		if (document.getElementById('productUPC.errors') != null) {
			document.getElementById('productUPC.errors').style.display = 'none';
		}
		if (document.getElementById('productName.errors') != null) {
			document.getElementById('productName.errors').style.display = 'none';
		}
		if (document.getElementById('suggestedRetailPrice.errors') != null) {
			document.getElementById('suggestedRetailPrice.errors').style.display = 'none';
		}
		if (document.getElementById('longDescription.errors') != null) {
			document.getElementById('longDescription.errors').style.display = 'none';
		}

		$("#attrDpsly > tbody").empty();
		$("#attrDispDiv").css('display', 'none');

	}

}

function checkAudioType(input) {
	var vAudioType = document.getElementById("audioFile").value;
	var ext = vAudioType.substring(vAudioType.lastIndexOf('.') + 1);
	if (ext == "mp3" || ext == "Mp3" || ext == "MP3" || ext == "mP3") {
		return true;
	} else {
		alert("You must upload Audio file with following extensions : .mp3");
		return false;
	}
}

function checkVideoType(input) {
	var vAudioType = document.getElementById("videoFile").value;
	var ext = vAudioType.substring(vAudioType.lastIndexOf('.') + 1);
	if (ext == "mp4" || ext == "Mp4" || ext == "MP4" || ext == "mP4") {
		return true;
	} else {
		alert("You must upload Video file with following extensions : .mp4 ");
		return false;
	}
}

function addLocation() {
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.addlocationform.action = "saveLocation.htm";
	document.addlocationform.method = "POST";
	document.addlocationform.submit();
}

/*
 * function addLocationWithCoordintes() { var locCordinates = ""; if
 * (document.getElementById("locCoordinates")) { locCordinates =
 * document.getElementById("locCoordinates").value; }
 * //document.addlocationform.action = "saveLocation.htm?locationCoordinates="+
 * locCordinates; document.addlocationform.action = "saveLocation.htm";
 * document.addlocationform.method = "POST"; document.addlocationform.submit(); }
 */

/*
 * function getAddLocationCoordinates() { // alert("Inside Location
 * Co-ordinates!!!"); var streetAddress = $('#storeAddress').val(); var state =
 * $('#Country').val(); var city = $('#City').val(); var zipCode =
 * $('#pCode').val();
 * 
 * var address = streetAddress + " " + city + " " + state + " " + zipCode; //
 * alert("Address "+address); var geocoder = new google.maps.Geocoder();
 * geocoder .geocode( { 'address' : address }, function(results, status) { if
 * (status == google.maps.GeocoderStatus.OK) { // alert("Location
 * :"+results[0].geometry.location);
 * document.getElementById("locCoordinates").value =
 * results[0].geometry.location; addLocationWithCoordintes(); } else {
 * alert("Unable to find address: " + status); addLocationWithCoordintes(); }
 * }); }
 */

function updateManageProd(productID, obj) {
	var locId = document.retprodsetupform.retLocID.value;
	if (locId == "") {
		alert("Please select Location");
	} else {
		var retLocID = document.getElementById("retLocID");
		var locIds = "";
		for ( var i = 0; i < retLocID.length; i++) {

			if (retLocID.options[i].selected) {
				var val = retLocID.options[i].value;
				locIds = locIds + val + ",";

			}

		}

		var par = obj.parentNode;
		while (par.nodeName.toLowerCase() != 'tr') {
			par = par.parentNode;
		}
		var desc = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[3])
				.children().val();
		var price = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[4])
				.children().val();
		var salePrice = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[5])
				.children().val();
		var saleStartDate = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[6])
				.children().val();
		var saleEndDate = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[7])
				.children().val();

		if ((saleStartDate == "" && saleEndDate == "" && salePrice == "")
				|| (saleStartDate != "" && saleEndDate != "" && salePrice != "")) {

			var stringObj = "{\"productID\":\"" + productID + "\","
					+ "\"description\":\"" + desc + "\"," + "\"price\":\""
					+ price + "\"," + "\"salePrice\":\"" + salePrice + "\","
					+ "\"saleStartDate\":\"" + saleStartDate + "\","
					+ "\"saleEndDate\":\"" + saleEndDate + "\","
					+ "\"location\":\"" + locIds + "\"}";

			$.ajaxSetup({
				cache : false
			});
			$
					.ajax({
						type : "POST",
						url : "/ScanSeeWeb/retailer/updateRetLocProd.htm",
						data : {
							'data' : stringObj
						},

						success : function(response) {
							if (response == 'FAILURE') {
								alert('Error occurred while updating product details!');
							} else {
								alert(response)
							}
						},
						error : function(e) {
							alert('Error occurred while updating product details!');
						}
					});

		} else if (salePrice != "") {

			if ((saleStartDate == "" && saleEndDate == "")) {
				alert("Please Enter Start Date and End Date")
			} else if (saleStartDate == "") {
				alert("Please Enter Start Date")
			} else if (saleEndDate == "") {
				alert("Please Enter End Date")
			}

		} else if ((saleStartDate != "" || saleEndDate != "")) {
			if (salePrice == "") {
				alert("Please Enter Sale Price");
			}
		}

	}
}

function productSetup() {
	document.addlocationform.action = "retailerchoosePlan.htm";
	document.addlocationform.method = "GET";
	document.addlocationform.submit();
}

function regRetProdSearch() {
	document.retprodsetupform.action = "regaddseachprod.htm";
	document.retprodsetupform.method = "POST";
	document.retprodsetupform.submit();
}

function planSetup() {
	document.addlocationform.action = "retailerchoosePlan.htm";
	document.addlocationform.method = "GET";
	document.addlocationform.submit();
}

function regRetLocProducts() {
	$("#retLocID option:selected").removeAttr("selected");
	var multipleValues = $("#retLocID").val()
	var locIds = "";
	if (multipleValues == null) {
		var retLocID = document.getElementById("retLocID");
		for ( var i = 0; i < retLocID.length; i++) {
			var val = retLocID.options[i].value;
			locIds = locIds + val + ",";
		}
		var arraylocIds = locIds.split(',');
		$('#retLocID').val(arraylocIds);
	}
	document.retprodsetupform.action = "regRetLocProd.htm";
	document.retprodsetupform.method = "POST";
	document.retprodsetupform.submit();
}

function regRetLocProductsSearch() {
	var multipleValues = $("#retLocID").val()
	var locIds = "";
	if (multipleValues == null) {
		var retLocID = document.getElementById("retLocID");
		for ( var i = 0; i < retLocID.length; i++) {
			var val = retLocID.options[i].value;
			locIds = locIds + val + ",";
		}
		var arraylocIds = locIds.split(',');
		$('#retLocID').val(arraylocIds);
	}
	document.retprodsetupform.action = "regRetLocProd.htm";
	document.retprodsetupform.method = "POST";
	document.retprodsetupform.submit();
}

function regUpdateManageProd(productID, obj) {
	var locId = document.retprodsetupform.LocID.value;
	if (locId == "") {
		alert("Please select Location");
	} else {

		var retLocID = document.getElementById("retLocID");
		var locIds = "";
		for ( var i = 0; i < retLocID.length; i++) {

			if (retLocID.options[i].selected) {
				var val = retLocID.options[i].value;
				locIds = locIds + val + ",";

			}
		}
		var par = obj.parentNode;
		while (par.nodeName.toLowerCase() != 'tr') {
			par = par.parentNode;
		}
		var desc = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[3])
				.children().val();
		var price = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[4])
				.children().val();
		var salePrice = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[5])
				.children().val();
		var saleStartDate = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[6])
				.children().val();
		var saleEndDate = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[7])
				.children().val();
		/*
		 * document.retprodsetupform.price.value = price;
		 * document.retprodsetupform.retailLocationProductDesc.value = desc;
		 * document.retprodsetupform.productID.value = productID;
		 * document.retprodsetupform.salePrice.value = salePrice;
		 * document.retprodsetupform.saleStartDate.value = startDate;
		 * document.retprodsetupform.saleEndDate.value = endDate;
		 */

		if ((saleStartDate == "" && saleEndDate == "" && salePrice == "")
				|| (saleStartDate != "" && saleEndDate != "" && salePrice != "")) {

			var stringObj = "{\"productID\":\"" + productID + "\","
					+ "\"description\":\"" + desc + "\"," + "\"price\":\""
					+ price + "\"," + "\"salePrice\":\"" + salePrice + "\","
					+ "\"saleStartDate\":\"" + saleStartDate + "\","
					+ "\"saleEndDate\":\"" + saleEndDate + "\","
					+ "\"location\":\"" + locIds + "\"}";

			$.ajaxSetup({
				cache : false
			});
			$
					.ajax({
						type : "POST",
						url : "/ScanSeeWeb/retailer/updateRegRetAssociateProd.htm",
						data : {
							'data' : stringObj
						},

						success : function(response) {
							if (response == 'FAILURE') {
								alert('Error occurred while updating product details!');
							} else {
								alert(response)
							}
						},
						error : function(e) {
							alert('Error occurred while updating product details!');
						}
					});

		} else if (salePrice != "") {

			if ((saleStartDate == "" && saleEndDate == "")) {
				alert("Please Enter Start Date and End Date")
			} else if (saleStartDate == "") {
				alert("Please Enter Start Date")
			} else if (saleEndDate == "") {
				alert("Please Enter End Date")
			}

		} else if ((saleStartDate != "" || saleEndDate != "")) {
			if (salePrice == "") {
				alert("Please Enter Sale Price");
			}
		}

	}
}

function fetchBatchLocationSetup() {
	document.locationform.action = "fetchbatchlocationlist.htm";
	document.locationform.method = "POST";
	document.locationform.submit();
}
/*
 * function batchLocationSetup() { document.addlocationform.action =
 * "fetchbatchlocationlist.htm"; document.addlocationform.method = "POST";
 * document.addlocationform.submit(); }
 */

/* Validate phone number for 10 digit */
function validatePhoneForLoc(num) {
	if (num.length != 10) {
		return false;
	} else {
		return true;
	}
}

function editManageLocation(pagenumber) {
	var productJson = [];
	var productObj = {};
	var validationFlag = true;
	var validurlFlag = true;
	var vRetLocList = [];
	var vRetailerLocID = $("#retailerLocID").val();
	if (vRetailerLocID != "null" && vRetailerLocID != "") {
		vRetLocList = vRetailerLocID.split(",");
	}
	var objArr = [];
	$("#locSetUpGrd")
			.find("tr")
			.each(
					function(index) {
						if (index > 0) {
							var highlightObj = $(
									$("#locSetUpGrd").find("tr")[index])
									.children();
							var object = {};
							var stringObj;

							highlightObj
									.find("input")
									.each(
											function(rVal) {

												var inputTagVal = $(this)[0].value;
												var name = $(this).attr("name")

												if (name == 'uploadRetaillocationsID') {
													object.uploadRetaillocationsID = inputTagVal;

												} else if (name == 'retailLocationID') {
													object.retailLocationID = inputTagVal;

												} else if (name == 'storeIdentification') {
													object.storeIdentification = inputTagVal;

												} else if (name == 'address1') {
													object.address1 = inputTagVal;

												} else if (name == 'retailerLocationLatitude') {
													object.retailerLocationLatitude = inputTagVal;

												} else if (name == 'retailerLocationLongitude') {
													object.retailerLocationLongitude = inputTagVal;

												} else if (name == 'city') {
													object.city = inputTagVal

												} else if (name == 'state') {
													object.state = inputTagVal

												} else if (name == 'postalCode') {
													object.postalCode = inputTagVal

												} else if (name == 'phonenumber') {
													object.phonenumber = inputTagVal
													var phoneNum = validatePhoneForLoc(object.phonenumber);
													if (!phoneNum) {
														$(this)
																.parent('tr')
																.find(
																		'#phonenumber')
																.val("");
														validationFlag = false;
													}

												} else if (name == 'retailLocationUrl') {
													object.retailLocationUrl = inputTagVal

												} else if (name == 'keyword') {
													object.keyword = inputTagVal

												}else if (name == 'gridImgLocationPath') {
													object.gridImgLocationPath = inputTagVal
												} else if (name == 'imgLocationPath') {
													object.imgLocationPath = inputTagVal
												} else if (name == 'uploadImage') {
													object.uploadImage = inputTagVal
												}

											});

							objArr[index - 1] = object;

						}
					});

	if (objArr.length > 0) {
		for ( var k = 0; k < objArr.length; k++) {
			$("tr#" + retailID).removeClass("hilite");
			var object = objArr[k];
			var retailID = object.retailLocationID;
			var storeIdentification = object.storeIdentification;
			var city = object.city;
			var state = object.state;
			var postalCode = object.postalCode;
			var address1 = object.address1;
			var phonenumber = object.phonenumber;
			/*
			 * var contactFirstName = object.contactFirstName; var
			 * contactLastName = object.contactLastName; var contactTitle =
			 * object.contactTitle; var contactType = object.contactType; var
			 * contactEmail = object.contactEmail;
			 */
			var retailLocationUrl = object.retailLocationUrl;
			var keyword = object.keyword;
			var vLatitude = object.retailerLocationLatitude;
			var vLongitude = object.retailerLocationLongitude;

			if (vRetailerLocID != "null" && vRetailerLocID != "") {
				if (storeIdentification == ''
						|| storeIdentification == 'undefined'
						|| storeIdentification == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (city == '' || city == 'undefined' || city == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (state == '' || state == 'undefined'
						|| state == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (postalCode == '' || postalCode == 'undefined'
						|| postalCode == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (address1 == '' || address1 == 'undefined'
						|| address1 == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (phonenumber == '' || phonenumber == 'undefined'
						|| phonenumber == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (vLatitude == '' || vLatitude == 'undefined'
						|| vLatitude == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (vLongitude == '' || vLongitude == 'undefined'
						|| vLongitude == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else {
					$("tr#" + retailID).removeClass("requiredVal");
				}
			} else {
				if (storeIdentification == ''
						|| storeIdentification == 'undefined'
						|| storeIdentification == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (city == '' || city == 'undefined' || city == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (state == '' || state == 'undefined'
						|| state == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (postalCode == '' || postalCode == 'undefined'
						|| postalCode == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (address1 == '' || address1 == 'undefined'
						|| address1 == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;

				} else if (phonenumber == '' || phonenumber == 'undefined'
						|| phonenumber == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (vLatitude == '' || vLatitude == 'undefined'
						|| vLatitude == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (vLongitude == '' || vLongitude == 'undefined'
						|| vLongitude == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else {
					$("tr#" + retailID).removeClass("requiredVal");
				}
			}

			//var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (retailLocationUrl != '') {
				if (!regexp.test(retailLocationUrl)) {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");

					validurlFlag = false;
				}
			}
		}

		if (validationFlag) {
			if (!validurlFlag) {
				alert("Please enter a valid URL.");
				return false;
			}

			var strinObjArr = []
			for ( var k = 0; k < objArr.length; k++) {
				var object = objArr[k];

				var stringObj;

				if (vRetailerLocID != "null" && vRetailerLocID != "") {
					stringObj = "{\"retailLocationID\":\""
							+ object.retailLocationID + "\","
							+ "\"storeIdentification\":\""
							+ object.storeIdentification + "\","
						    + "\"address1\":\"" + object.address1
							+ "\"," + "\"city\":\"" + object.city + "\","
							+ "\"state\":\"" + object.state + "\","
							+ "\"postalCode\":\"" + object.postalCode + "\","
							+ "\"phonenumber\":\"" + object.phonenumber + "\","
							+ "\"retailLocationUrl\":\""
							+ object.retailLocationUrl + "\","
							+ "\"keyword\":\"" + object.keyword + "\","
							+ "\"retailerLocationLatitude\":\""
							+ object.retailerLocationLatitude + "\","
							+ "\"retailerLocationLongitude\":\""
							+ object.retailerLocationLongitude + "\","
							+ "\"imgLocationPath\":\""
							+ object.imgLocationPath + "\","
							+ "\"gridImgLocationPath\":\""
							+ object.gridImgLocationPath + "\","
							+ "\"uploadImage\":\""
							+ object.uploadImage + "\"}";
				} else {
					stringObj = "{\"retailLocationID\":\""
							+ object.retailLocationID + "\","
							+ "\"storeIdentification\":\""
							+ object.storeIdentification + "\","
							+ "\"address1\":\"" + object.address1
							+ "\"," + "\"retailerLocationLatitude\":\""
							+ object.retailerLocationLatitude + "\","
							+ "\"retailerLocationLongitude\":\""
							+ object.retailerLocationLongitude + "\","
							+ "\"city\":\"" + object.city + "\","
							+ "\"state\":\"" + object.state + "\","
							+ "\"postalCode\":\"" + object.postalCode + "\","
							+ "\"phonenumber\":\"" + object.phonenumber + "\","
							+ "\"retailLocationUrl\":\""
							+ object.retailLocationUrl + "\","
							+ "\"keyword\":\"" + object.keyword + "\","
							+ "\"imgLocationPath\":\""
							+ object.imgLocationPath + "\","
							+ "\"gridImgLocationPath\":\""
							+ object.gridImgLocationPath + "\","
							+ "\"uploadImage\":\""
							+ object.uploadImage + "\"}";
				}
				strinObjArr[k] = stringObj;
			}

			// Submtitting to save the changes.

			saveLocationList(strinObjArr.toString(), pagenumber)
		} else {
			alert("Please Enter values for the mandatory fields for the highlighted rows in the screen");
			return false;
		}
	} else {
		alert("Please Enter values for the mandatory fields and Click on Save to Save the Changes")
	}
}

function saveLocationList(objArr, pagenumber) {

	if (pagenumber == null || pagenumber == undefined) {
		document.locationform.pageFlag.value = "false";
	} else {
		document.locationform.pageNumber.value = pagenumber;
		document.locationform.pageFlag.value = "true";
	}
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.locationform.prodJson.value = "{\"locationData\":[" + objArr
			+ "]}";
	document.locationform.action = "savelocationlist.htm";
	document.locationform.method = "POST";
	document.locationform.submit();

}

function deleteLocationList() {
	document.locationform.action = "deletebatchlocationlist.htm";
	document.locationform.method = "POST";
	document.locationform.submit();
}

function regAssociateRetProd(productID, obj) {
	var locId = document.retprodsetupform.LocID.value;
	if (locId == "") {
		alert("Please select Location");
	} else {
		var retLocID = document.getElementById("retLocID");
		var locIds = "";
		for ( var i = 0; i < retLocID.length; i++) {

			if (retLocID.options[i].selected) {
				var val = retLocID.options[i].value;
				locIds = locIds + val + ",";
			}
		}
		var par = obj.parentNode;
		while (par.nodeName.toLowerCase() != 'tr') {
			par = par.parentNode;
		}
		var desc = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[3])
				.children().val();
		var price = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[4])
				.children().val();
		var salePrice = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[5])
				.children().val();
		var saleStartDate = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[6])
				.children().val();
		var saleEndDate = $(
				$($("#prodinfotbl").find("tr")[par.rowIndex]).children()[7])
				.children().val();

		if ((saleStartDate == "" && saleEndDate == "" && salePrice == "")
				|| (saleStartDate != "" && saleEndDate != "" && salePrice != "")) {
			var stringObj = "{\"productID\":\"" + productID + "\","
					+ "\"description\":\"" + desc + "\"," + "\"price\":\""
					+ price + "\"," + "\"salePrice\":\"" + salePrice + "\","
					+ "\"saleStartDate\":\"" + saleStartDate + "\","
					+ "\"saleEndDate\":\"" + saleEndDate + "\","
					+ "\"location\":\"" + locIds + "\"}";

			$.ajaxSetup({
				cache : false
			});
			$.ajax({
				type : "POST",
				url : "/ScanSeeWeb/retailer/regassociateprod.htm",
				data : {
					'data' : stringObj
				},

				success : function(response) {
					if (response == 'FAILURE') {
						alert('Error While Adding Product');
					} else {
						alert(response)
					}
				},
				error : function(e) {
					alert('Error While Adding Product');
				}
			});
		} else if (salePrice != "") {

			if ((saleStartDate == "" && saleEndDate == "")) {
				alert("Please Enter Start Date and End Date")
			} else if (saleStartDate == "") {
				alert("Please Enter Start Date")
			} else if (saleEndDate == "") {
				alert("Please Enter End Date")
			}
		} else if ((saleStartDate != "" || saleEndDate != "")) {
			if (salePrice == "") {
				alert("Please Enter Sale Price");
			}
		}
	}
}

function saveNcallNextManageLocation(pageNumber) {
	editManageLocation(pageNumber)
}
// ///////Start of Share Methods/////////////////
function fbShareProductInfo(productId) {
	document.productsummary.productId.value = productId;
	document.productsummary.action = "/ScanSeeWeb/shopper/fbShareProductInfo.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
}

function fbShareCLRDetails(clrType) {
	document.clrForm.clrType.value = clrType;
	document.clrForm.action = "/ScanSeeWeb/shopper/fbShareclrInfo.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}
// /////End of Share Methods//////////////////////

function openQRCodePopUp(productId, productName) {

	openIframePopup('ifrmPopup', 'ifrm', 'supplierQRCode.htm?productID='
			+ productId + '&productName=' + productName, 440, 600, 'QR Code');
}

/* Forgot Password */
function forgetPwd(evt) {
	var vUsername = document.getElementById("usn").value;
	if (evt && evt.which == 13) {
		if (vUsername == null || vUsername == "") {
			alert("Please enter Username");
			return false;
		}
		document.loginform.action = "forgetPwd.htm";
		document.loginform.method = "POST";
		document.loginform.submit();
	} else if (evt == '') {
		if (vUsername == null || vUsername == "") {
			alert("Please enter Username");
			return false;
		}
		document.loginform.action = "forgetPwd.htm";
		document.loginform.method = "POST";
		document.loginform.submit();
	} else {
		return true;
	}
}
/* End */
function generateQRCode(productId, productName) {

	var audio = $('input:radio[name=audio_Qr]:checked').val();
	var video = $('input:radio[name=video_Qr]:checked').val();
	var fileLink = $('input[name$="fileLink"]').val();
	document.qrcode.productId.value = productId;
	document.qrcode.QRAudioId.value = audio;
	document.qrcode.QRVideoId.value = video;
	document.qrcode.fileLink.value = fileLink;
	document.qrcode.productName.value = productName;
	var audios = document.getElementsByName('audio_Qr');
	var videos = document.getElementsByName('video_Qr');
	var product = document.getElementsByName('product');
	var count = 0;
	for ( var i = 0; i < product.length; i++) {
		if (product[i].checked == true) {
			document.qrcode.QRProduct.value = productId;
			count++;
		} else {
			document.qrcode.QRProduct.value = null;
		}
	}
	for ( var i = 0; i < audios.length; i++) {
		if (audios[i].checked == true) {
			count++;
		}
	}
	for ( var i = 0; i < videos.length; i++) {
		if (videos[i].checked == true) {
			count++;
		}
	}
	if (count == 0) {
		alert("Please select atleast one item to generate QR code!");
	} else {
		document.qrcode.textFlag.value = true;
		document.qrcode.action = "supplierQRCodeGen.htm";
		document.qrcode.method = "POST";
		document.qrcode.submit();
	}
}
var audioFlag = false;
var videoFlag = false;

function checkAudio() {
	$('#audioQr').attr('checked', true);
	audioFlag = true;
}

function checkVideo() {
	$('#videoQr').attr('checked', true);
	videoFlag = true;
}

function unCheckAudio() {
	if ($('#audioQr').attr('checked')) {
		$('INPUT:radio[name="audio_Qr"]:visible:first').attr('checked',
				'checked');
		audioFlag = true;
	} else {
		$('input[name=audio_Qr]').attr('checked', false);
		audioFlag = false;
	}
}

function unCheckVideo() {
	if ($('#videoQr').attr('checked')) {
		$('INPUT:radio[name="video_Qr"]:visible:first').attr('checked',
				'checked');
		videoFlag = true;
	} else {
		$('input[name=video_Qr]').attr('checked', false);
		videoFlag = false;
	}
}

function TestAudio() {
	if (audioFlag) {
		$('input[name=audio_Qr]').attr('checked', true);
		$('#audioQr').attr('checked', true);
	}
	if (videoFlag) {
		$('input[name=video_Qr]').attr('checked', true);
		$('#videoQr').attr('checked', true);
	}
}

function openShowRetlrQrPopUp(pageId) {
	openIframePopup('ifrmPopup', 'ifrm', 'getpageqr?pageId=' + pageId, 440,
			600, 'QR Code')
}
function openShowGiveAwayQrPopUp(pageId) {
	openIframePopup('ifrmPopup', 'ifrm', 'getgiveawaypageqr?pageId=' + pageId,
			440, 600, 'QR Code')
}
/* End */

// //Create Custom Page Method/////
/*
 * function saveRetailerCreatedPage() {
 * document.createCustomPage.landigPageType.value = "AttachLink";
 * document.createCustomPage.action = "retailercreatedpage.htm";
 * document.createCustomPage.method = "POST";
 * document.createCustomPage.submit(); }
 */
function saveRetailerPage() {

	var retailerLocId = document.createCustomPage.retLocId.value;
	var title = document.createCustomPage.retPageTitle.value;
	var locid = document.createCustomPage.retLocId.value;
	document.createCustomPage.hiddenLocId.value = retailerLocId;

	$.ajaxSetup({
		cache : false
	});
	$
			.ajax({
				type : "GET",
				url : "validateretpage.htm",
				data : {
					'retailerLocId' : retailerLocId,
					'pageID' : 0,
					'qrType' : 3
				},
				success : function(response) {

					if (response == 'FAILURE') {
						document.createCustomPage.hiddenLocId.value = retailerLocId;
						document.createCustomPage.action = "createretailerpage.htm";
						document.createCustomPage.method = "POST";
						document.createCustomPage.submit();

					} else {

						var status = confirm("Page has already been created for the selected location, Click OK to override");
						var pageId = parseFloat(response)
						if (status) {
							document.createCustomPage.pageId.value = pageId;
							document.createCustomPage.action = "updatemainmenupage.htm";
							document.createCustomPage.method = "POST";
							document.createCustomPage.submit();

						}
					}

				},
				error : function(e) {
					alert('Error:' + e);
				}
			});

}
function saveRetailerCreatedPage() {

	// var retailerLocId = document.createCustomPage.retCreatedPageLocId.value;
	// var title = document.createCustomPage.retPageTitle.value;
	// var locid = document.createCustomPage.retCreatedPageLocId.value;
	// document.createCustomPage.hiddenLocId.value = retailerLocId;
	showProgressBar();
	document.createCustomPage.hiddenLocId.value = $('#retCreatedPageLocId')
			.val();
	var splOfferLocId = document.createCustomPage.hiddenLocId.value;

	document.createCustomPage.action = "createretailerpage.htm";
	document.createCustomPage.method = "POST";
	document.createCustomPage.submit();

	/*
	 * $.ajaxSetup({ cache : false }); $ .ajax({ type : "GET", url :
	 * "validateretpage.htm", data : { 'retailerLocId' : splOfferLocId, 'pageID' :
	 * 0, 'qrType' : 2 }, success : function(response) {
	 * 
	 * if (response == 'FAILURE') {
	 * //document.createCustomPage.hiddenLocId.value = retailerLocId;
	 * document.createCustomPage.action = "createretailerpage.htm";
	 * document.createCustomPage.method = "POST";
	 * document.createCustomPage.submit(); } else {
	 * 
	 * var status = confirm("Page has already been created for the selected
	 * location, Click OK to override"); var pageId = parseFloat(response) if
	 * (status) { document.createCustomPage.pageId.value = pageId;
	 * document.createCustomPage.action = "updatemainmenupage.htm";
	 * document.createCustomPage.method = "POST";
	 * document.createCustomPage.submit(); } } }, error : function(e) {
	 * alert('Error:' + e); } });
	 */

}
function saveSplOfferPage() {
	document.createCustomPage.hiddenLocId.value = $('#splOfferLocId').val();
	// var splOfferLocId = document.createCustomPage.hiddenLocId.value;
	// document.createCustomPage.hiddenLocId.value = "";
	document.createCustomPage.action = "createsploffrpage.htm";
	document.createCustomPage.method = "POST";
	document.createCustomPage.submit();

}
function getCustomPageInfo(pageId) {
	document.customPageForm.pageId.value = pageId;
	document.customPageForm.action = "getretailerpageinfo.htm";
	document.customPageForm.method = "GET";
	document.customPageForm.submit();

}

function getRetCreatedPageInfo(pageId) {
	document.customPageForm.pageId.value = pageId;
	document.customPageForm.action = "getretailercrtdpageinfo.htm";
	document.customPageForm.method = "GET";
	document.customPageForm.submit();

}

function printQRcode() {
	$('body').css('visibility', 'hidden');
	window.print();
	$('body').css('visibility', 'visible');
}

function updateRetailerCreatedPage() {

	var retCreatedPageFileLength = document.createCustomPage.retCreatedPageFile.length;
	var uploadedFileLength = document.createCustomPage.uploadedFileLength.value;
	var length;
	if (retCreatedPageFileLength) {
		length = parseInt(retCreatedPageFileLength)
				+ parseInt(uploadedFileLength) - 1;
	} else {
		length = parseInt(uploadedFileLength);
	}

	if (length > 3) {
		alert('Number of files to upload limited to 3')
	} else {
		document.createCustomPage.action = "updateretailercrtdpage.htm";
		document.createCustomPage.method = "POST";
		document.createCustomPage.submit();

	}

}
function getSpecialOfferPageInfo(pageId) {
	document.customPageForm.pageId.value = pageId;
	document.customPageForm.action = "getsplofferpageinfo.htm";
	document.customPageForm.method = "GET";
	document.customPageForm.submit();

}
function updateSpecialOfferPage() {
	document.createCustomPage.action = "updatesploffrpage.htm";
	document.createCustomPage.method = "POST";
	document.createCustomPage.submit();

}
// ///End//////////
// Mey Related Code
function validateCard(selectedTab) {
	// alert(selectedTab);
	if (selectedTab == 'CreditCardPay' || selectedTab == 'BankPay') {
		if (validateCreditCard()) {
			if (!document.getElementById("checkbox").checked) {
				$('#checkBoxError').show();
			} else {
				$('#checkBoxError').hide();
				// get the form values
				var cardNumber = $('#creditCardNumber').val();
				var month = $('#month').val();
				var year = $('#year').val();
				var cVV = $('#cVV').val();
				// var cardholderName = $('#cardholderName').val();
				var firstName = $('#firstName').val();
				var lastName = $('#lastName').val();

				var billingAddress = $('#billingAddress').val();
				var State = $('#Country').val();
				var City = $('#City').val();
				var zip = $('#zip').val();
				var phoneNumber = $('#phoneNumber').val();
				/*
				 * var totalString = cardNumber + ":" + month + ":" + year + ":" +
				 * cVV + ":" + cardholderName + ":" + billingAddress + ":" +
				 * State + ":" + City + ":" + zip + ":" + phoneNumber;
				 */
				var totalString = cardNumber + ":" + month + ":" + year + ":"
						+ cVV + ":" + firstName + ":" + lastName + ":"
						+ billingAddress + ":" + State + ":" + City + ":" + zip
						+ ":" + phoneNumber;// +":"+paymentType;
				// alert(totalString);
				$
						.ajax({
							type : "POST",
							url : "processCC.htm",
							data : {
								'totalString' : totalString,
							},

							success : function(response) {
								var strResponse = response.toString();

								if (strResponse.toLowerCase().indexOf("error") != -1
										|| strResponse.toLowerCase().indexOf(
												"invalid") != -1) {
									alert(strResponse);
								} else {
									// alert("Success");
									// alert(response.toString());

									// $('#processCardResponse').innerHTML =
									// response.toString();
									document
											.getElementById('processCardResponse').innerHTML = strResponse;

									// var w =
									// window.open('/ScanSeeWeb/paymentConfirm.jsp',
									// 'Payment Confirmation',
									// 'width=400,height=400,resizeable,scrollbars');
									// w.document.getElementById('content').innerHTML
									// = response.toString();
									// w.document.close(); // needed for chrome
									// and safari

									// LOADING POPUP
									// Click the button event!
									// $("#button").click(function(){
									// centering with css
									centerPopup();
									// load popup
									loadPopup();
									// });

									// CLOSING POPUP
									// Click the x event!
									$("#popupContactClose").click(function() {
										disablePopup();
									});
									// Click out event!
									$("#backgroundPopup").click(function() {
										disablePopup();
									});
									// Press Escape event!
									$(document).keypress(
											function(e) {
												if (e.keyCode == 27
														&& popupStatus == 1) {
													disablePopup();
												}
											});

									var cardNumber = $('#creditCardNumber')
											.val();
									var maskedCC = cardNumber
											.substr(cardNumber.length - 4);
									$('#creditCardNumber').val(
											'***********' + maskedCC);
									$('#cVV').val('***');
									$('#month').val('00');
									$('#year').val('0000');

									// Enable to Continue button once payment is
									// successfull and disable payment button

									if (strResponse.toLowerCase().indexOf(
											"dashboard") >= 0) {
										changeButtonToEnableBackground('continueToDashboard');
										changeButtonToDisableBackground('processPaymentButton');
										$('#continueToDashboard').removeAttr(
												'disabled');
										$('#processPaymentButton').attr(
												"disabled", "disabled");
										$('#withDashboardLink').show();
										$('#withoutDashboardLink').hide();
									}
								}

							},
							error : function(e) {
								alert("Error in processing credit card!!");
								// alert(e);
							}
						});
			}
		}
	}
}

function validateCardForRetailer(selectedTab) {
	if (selectedTab == 'CreditCardPay' || selectedTab == 'BankPay') {
		if (validateCreditCard()) {
			if (!document.getElementById("checkbox").checked) {
				$('#checkBoxError').show();
			} else {
				$('#checkBoxError').hide();
				// get the form values
				var cardNumber = $('#creditCardNumber').val();
				var month = $('#month').val();
				var year = $('#year').val();
				var cVV = $('#cVV').val();
				var firstName = $('#firstName').val();
				var lastName = $('#lastName').val();
				var billingAddress = $('#billingAddress').val();
				var State = $('#Country').val();
				var City = $('#City').val();
				var zip = $('#zip').val();
				var phoneNumber = $('#phoneNumber').val();
				// var paymentType =
				// $('input:radio[name=radio1]:checked').val();
				// alert("paymentType"+paymentType);
				var totalString = cardNumber + ":" + month + ":" + year + ":"
						+ cVV + ":" + firstName + ":" + lastName + ":"
						+ billingAddress + ":" + State + ":" + City + ":" + zip
						+ ":" + phoneNumber;// +":"+paymentType;
				// alert(totalString);
				$
						.ajax({
							type : "POST",
							url : "processRetailerCC.htm",
							data : {
								'totalString' : totalString,
							},

							success : function(response) {

								var strResponse = response.toString();

								if (strResponse.toLowerCase().indexOf("error") != -1
										|| strResponse.toLowerCase().indexOf(
												"invalid") != -1) {
									alert(strResponse);
								} else {
									// alert("Success");
									// alert(response.toString());

									// $('#processCardResponse').innerHTML =
									// response.toString();
									var responseString = response.toString();
									document
											.getElementById('processCardResponse').innerHTML = responseString;

									// var w =
									// window.open('/ScanSeeWeb/paymentConfirm.jsp',
									// 'Payment Confirmation',
									// 'width=400,height=400,resizeable,scrollbars');
									// w.document.getElementById('content').innerHTML
									// = response.toString();
									// w.document.close(); // needed for chrome
									// and safari

									// LOADING POPUP
									// Click the button event!
									// $("#button").click(function(){
									// centering with css
									centerPopup();
									// load popup
									loadPopup();
									// });

									// CLOSING POPUP
									// Click the x event!
									$("#popupContactClose").click(function() {
										disablePopup();
									});
									// Click out event!
									$("#backgroundPopup").click(function() {
										disablePopup();
									});
									// Press Escape event!
									$(document).keypress(
											function(e) {
												if (e.keyCode == 27
														&& popupStatus == 1) {
													disablePopup();
												}
											});

									var cardNumber = $('#creditCardNumber')
											.val();
									var maskedCC = cardNumber
											.substr(cardNumber.length - 4);
									$('#creditCardNumber').val(
											'***********' + maskedCC);
									$('#cVV').val('****');
									$('#month').val('00');
									$('#year').val('0000');

									// Enable to Continue button once payment is
									// successfull and disable payment button
									if (responseString.toLowerCase().indexOf(
											"dashboard") >= 0) {
										changeButtonToEnableBackground('continueToDashboard');
										changeButtonToDisableBackground('processPaymentButton');
										$('#continueToDashboard').removeAttr(
												'disabled');
										$('#processPaymentButton').attr(
												"disabled", "disabled");
										$('#withDashboardLink').show();
										$('#withoutDashboardLink').hide();
									}
								}
							},
							error : function(e) {
								alert("Error in processing credit card!!");
								// alert(e);
							}
						});
			}
		}
	}
}

function validateCreditCard() {
	// get the form values
	var cardNumber = $('#creditCardNumber').val();
	var month = $('#month').val();
	var year = $('#year').val();
	var cVV = $('#cVV').val();
	var firstName = $('#firstName').val();
	var middleName = $('#middleName').val();
	var lastName = $('#lastName').val();
	var billingAddress = $('#billingAddress').val();
	var State = $('#Country').val();
	var City = $('#City').val();
	var zip = $('#zip').val();
	var phoneNumber = $('#phoneNumber').val();
	var errorCount = 0;

	// alert("State :"+State);
	// alert("City :"+City);
	// alert("month :"+month);
	// alert("year :"+year);

	// cardNumber Validation
	if (cardNumber == null || cardNumber == '') {
		$('#CreditCardError').show();
		errorCount++;
	} else
		$('#CreditCardError').hide();
	// Exp Date Validation || month == 00 || year == 0000
	if (month == null || month == '' || year == null || year == ''
			|| month == '00' || year == '0000') {
		$('#ExpDateError').show();
		errorCount++;
	} else
		$('#ExpDateError').hide();
	// CVV Validation
	if (cVV == null || cVV == '') {
		$('#CVVError').show();
		errorCount++;
	} else
		$('#CVVError').hide();
	// cardholderName Validation
	if (firstName == null || firstName == '') {
		$('#FirstNameError').show();
		errorCount++;
	} else
		$('#FirstNameError').hide();
	if (lastName == null || lastName == '') {
		$('#LastNameError').show();
		errorCount++;
	} else
		$('#LastNameError').hide();
	// billingAddress Validation
	if (billingAddress == null || billingAddress == '') {
		$('#BillingAddressError').show();
		errorCount++;
	} else
		$('#BillingAddressError').hide();
	// State Validation || State == 0
	if (State == null || State == '' || State == '0') {
		$('#StateError').show();
		errorCount++;
	} else
		$('#StateError').hide();
	// City Validation || City == 0
	if (City == null || City == '' || City == '0') {
		$('#CityError').show();
		errorCount++;
	} else
		$('#CityError').hide();
	// Zip Code Validation
	if (zip == null || zip == '') {
		$('#ZipCodeError').show();
		errorCount++;
	} else
		$('#ZipCodeError').hide();
	// Phone number validation
	if (phoneNumber == null || phoneNumber == '') {
		$('#phoneError').show();
		errorCount++;
	} else
		$('#phoneError').hide();

	// alert('ErrorCount' + errorCount);
	if (errorCount == 0)
		return true;

	return false;

}

function printPage() {
	var html = "<html>";
	html += document.getElementById('popupContact').innerHTML;
	html += "</html>";

	var printWin = window
			.open('', '',
					'left=0,top=0,width=400px,height=400px,toolbar=0,scrollbars=1,status  =0');
	printWin.document.write(html);
	printWin.document.close();
	printWin.focus();
	printWin.print();
	printWin.close();
}

function getRetailerDiscount() {

	// get the form values
	var discountCode = $('#discountCode').val();
	var subtotal = document.getElementById("sub").value;

	$.ajax({
		type : "GET",
		url : "fetchretailerdiscount.htm",
		data : {
			'discountcode' : discountCode

		},

		success : function(response) {
			var fields = response.split('&');
			var discount1 = fields[0];
			var discount2 = fields[1];

			$('#myAjax').html(discount1);
			getTotalValue(discount2);
		},
		error : function(e) {
			alert("Please give a correct discountcode");
			$('#discountCode').val("");
			$('input[name$="discount"]').val("");
			// $('#discountsubtotal').val("");
			document.getElementById("discountsubtotal").value = subtotal;
		}
	});

}

function validateCheckbox() {
	if (document.getElementById("checkbox").checked) {
		$('#checkBoxError').hide();
	} else {
		$('#checkBoxError').show();
	}
}

function changeButtonToEnableBackground(id) {
	document.getElementById(id).style.background = 'url(../images/btnBG.png) repeat-x 0 0';
}

function changeButtonToDisableBackground(id) {
	document.getElementById(id).style.backgroundColor = "#808080";
}

function openUploadPreviewPopUp_forDashboard() {
	var temp = document.retaileruploadlogoinfoform.retailerID.value;
	window.open(
			'/ScanSeeWeb/retailer/uploadRetailerLogo/previewretaler.htm?retailerID='
					+ temp, 'RetailerUpload',
			'height=760,width=400,screenX=50,left=400,screenY=50,top=200');
}

function fetchBatchLocationDashboardSetup() {
	document.locationform.action = "manageLocationDashboard.htm";
	document.locationform.method = "GET";
	document.locationform.submit();
}

function addLocationDashboardSetup() {
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.addlocationform.action = "saveLocationDashboard.htm";
	document.addlocationform.method = "POST";
	document.addlocationform.submit();
}

/*
 * function addLocationDashboardSetup() {
 * 
 * var locCordinates = ""; if (document.getElementById("locCoordinates")) {
 * locCordinates = document.getElementById("locCoordinates").value; }
 * document.addlocationform.action =
 * "saveLocationDashboard.htm?locationCoordinates=" + locCordinates;
 * document.addlocationform.method = "POST"; document.addlocationform.submit(); }
 * 
 * function getAddLocationCoordinatesDashboardSetup() { // alert("Inside
 * Location Co-ordinates!!!"); var streetAddress = $('#storeAddress').val(); var
 * state = $('#Country').val(); var city = $('#City').val(); var zipCode =
 * $('#pCode').val();
 * 
 * var address = streetAddress + " " + city + " " + state + " " + zipCode; //
 * alert("Address "+address); var geocoder = new google.maps.Geocoder();
 * geocoder .geocode( { 'address' : address }, function(results, status) { if
 * (status == google.maps.GeocoderStatus.OK) { // alert("Location
 * :"+results[0].geometry.location);
 * document.getElementById("locCoordinates").value =
 * results[0].geometry.location; addLocationDashboardSetup(); } else {
 * alert("Unable to find address: " + status); addLocationDashboardSetup(); }
 * }); }
 */

function loadPricePerLocation(radioButton, selectedPrice, planInfoSize,
		priceId, flag) {
	if ($('input:radio[name=' + radioButton + ']:checked').val() != ""
			&& document.getElementById(selectedPrice)
			&& document.getElementById(selectedPrice).value != "") {
		var currentPrice = document.getElementById(selectedPrice).value;
		// alert($('input:radio[name='+radioButton+']:checked').val() + " ;
		// Current Price : " + currentPrice + " ; PriceId : " + priceId);
		document.getElementById(priceId).value = currentPrice;
		if (planInfoSize != "")
			totalMonthlyYearlyRetailerPrice(planInfoSize, flag);
	}

}

function totalMonthlyYearlyRetailerPrice(size, flag) {

	var qantity = document.getElementsByName("qty");
	var priceValue = document.getElementsByName("price");
	var total = document.getElementsByName("total");
	var discountCode = document.getElementById("discountCode").value;
	var subtotal = 0;

	for ( var i = 0; i < size; i++) {
		total[i].value = (qantity[i].value) * (priceValue[i].value);
		subtotal += parseFloat(total[i].value)

	}
	document.getElementById("sub").value = subtotal;
	document.getElementById("discountsubtotal").value = subtotal;
	// alert('SetupFee : '+document.getElementById("setupFee").value);
	// document.getElementById("discountsubtotal").value = subtotal +
	// document.getElementById("setupFee").value;
	if (flag) {
		if (!discountCode) {
			// document.getElementById("sub").value=subtotal;
			document.getElementById("discountsubtotal").value = subtotal;
		} else {
			// getRetailerDiscount();
			document.getElementById("discountsubtotal").value = subtotal;

		}
	}
}

function getRetailerDiscountMonthlyAndYearly(count, planInfoSize) {

	var defaultSessionMonthlyPrice = $('#defaultMonthlyPrice1').val();
	var defaultSessionYearlyPrice = $('#defaultYearlyPrice1').val();
	// get the form values
	var discountCode = $('#discountCode').val();
	var subtotal = document.getElementById("sub").value;

	$
			.ajax({
				type : "GET",
				url : "fetchretailerdiscountMonYearly.htm",
				data : {
					'discountcode' : discountCode

				},

				success : function(response) {
					if (response == "skippayment") {
						// alert("skippayment");
						document.getElementById("skipPayment").value = response;
					} else {
						// alert("processPayment");
						document.getElementById("skipPayment").value = "processPayment"
						for ( var i = 1; i <= planInfoSize; i++) {
							processDiscountPlanAjaxResponse(response, i,
									defaultSessionMonthlyPrice,
									defaultSessionYearlyPrice);

							var radioButtonName = "radio" + i;
							var selectedPrice = "selectedMonthlyPrice" + i;
							if (radioButtonName) {
								var radioButtonValue = $(
										'input:radio[name=' + radioButtonName
												+ ']:checked').val();
								if (radioButtonValue == "Yearly") {
									selectedPrice = "selectedYearlyPrice" + i;
								}
							}
							var priceName = "price" + i;
							// alert("radioButtonValue : "+selectedPrice);
							loadPricePerLocation(radioButtonName,
									selectedPrice, planInfoSize, priceName,
									false);
						}
					}
				},
				error : function(e) {
					alert("Please give a correct discountcode");
					$('#discountCode').val("");
					$('input[name$="discount"]').val("");
					// $('#discountsubtotal').val("");

					for ( var j = 1; j <= planInfoSize; j++) {
						var spanId = "defaultMonthlyPriceSpanId" + j;
						var yearlySpanId = "defaultYearlyPriceSpanId" + j;
						if (defaultSessionMonthlyPrice) {
							document.getElementById(spanId).innerHTML = "($"
									+ defaultSessionMonthlyPrice + ")";
							document.getElementById("selectedMonthlyPrice" + j).value = defaultSessionMonthlyPrice;
						} else {
							document.getElementById(spanId).innerHTML = "";
							document.getElementById("selectedMonthlyPrice" + j).value = "0";
						}

						if (defaultSessionYearlyPrice) {
							document.getElementById(yearlySpanId).innerHTML = "($"
									+ defaultSessionYearlyPrice + ")";
							document.getElementById("selectedYearlyPrice" + j).value = defaultSessionYearlyPrice;
						} else {
							document.getElementById(yearlySpanId).innerHTML = "";
							document.getElementById("selectedYearlyPrice" + j).value = "0";
						}

						document.getElementById("discountsubtotal").value = subtotal;

						var radioButtonName = "radio" + j;
						var selectedPrice = "selectedMonthlyPrice" + j;
						if (radioButtonName) {
							var radioButtonValue = $(
									'input:radio[name=' + radioButtonName
											+ ']:checked').val();
							if (radioButtonValue == "Yearly") {
								selectedPrice = "selectedYearlyPrice" + j;
							}
						}
						var priceName = "price" + j;
						// alert("radioButtonValue : "+selectedPrice);
						loadPricePerLocation(radioButtonName, selectedPrice,
								planInfoSize, priceName, false);
					}
				}
			});

}

function getSupplierDiscountMonthlyAndYearly(count, planInfoSize) {

	var defaultSessionMonthlyPrice = $('#defaultMonthlyPrice1').val();
	var defaultSessionYearlyPrice = $('#defaultYearlyPrice1').val();
	// get the form values
	var discountCode = $('#discountCode').val();
	var subtotal = document.getElementById("sub").value;

	$
			.ajax({
				type : "GET",
				url : "fetchSupplierdiscountMonYearly.htm",
				data : {
					'discountcode' : discountCode

				},

				success : function(response) {
					if (response == "skippayment") {
						// alert("skippayment");
						document.getElementById("skipPayment").value = response;
					} else {
						// alert("processPayment");
						document.getElementById("skipPayment").value = "processPayment"
						for ( var i = 1; i <= planInfoSize; i++) {
							processDiscountPlanAjaxResponse(response, i,
									defaultSessionMonthlyPrice,
									defaultSessionYearlyPrice);

							var radioButtonName = "radio" + i;
							var selectedPrice = "selectedMonthlyPrice" + i;
							if (radioButtonName) {
								var radioButtonValue = $(
										'input:radio[name=' + radioButtonName
												+ ']:checked').val();
								if (radioButtonValue == "Yearly") {
									selectedPrice = "selectedYearlyPrice" + i;
								}
							}
							var priceName = "price" + i;
							// alert("radioButtonValue : "+selectedPrice);
							loadPricePerLocation(radioButtonName,
									selectedPrice, planInfoSize, priceName,
									false);
						}
					}
				},
				error : function(e) {
					alert("Please give a correct discountcode");
					$('#discountCode').val("");
					$('input[name$="discount"]').val("");
					// $('#discountsubtotal').val("");

					for ( var j = 1; j <= planInfoSize; j++) {
						var spanId = "defaultMonthlyPriceSpanId" + j;
						var yearlySpanId = "defaultYearlyPriceSpanId" + j;
						if (defaultSessionMonthlyPrice) {
							document.getElementById(spanId).innerHTML = "($"
									+ defaultSessionMonthlyPrice + ")";
							document.getElementById("selectedMonthlyPrice" + j).value = defaultSessionMonthlyPrice;
						} else {
							document.getElementById(spanId).innerHTML = "";
							document.getElementById("selectedMonthlyPrice" + j).value = "0";
						}

						if (defaultSessionYearlyPrice) {
							document.getElementById(yearlySpanId).innerHTML = "($"
									+ defaultSessionYearlyPrice + ")";
							document.getElementById("selectedYearlyPrice" + j).value = defaultSessionYearlyPrice;
						} else {
							document.getElementById(yearlySpanId).innerHTML = "";
							document.getElementById("selectedYearlyPrice" + j).value = "0";
						}

						document.getElementById("discountsubtotal").value = subtotal;

						var radioButtonName = "radio" + j;
						var selectedPrice = "selectedMonthlyPrice" + j;
						if (radioButtonName) {
							var radioButtonValue = $(
									'input:radio[name=' + radioButtonName
											+ ']:checked').val();
							if (radioButtonValue == "Yearly") {
								selectedPrice = "selectedYearlyPrice" + j;
							}
						}
						var priceName = "price" + j;
						// alert("radioButtonValue : "+selectedPrice);
						loadPricePerLocation(radioButtonName, selectedPrice,
								planInfoSize, priceName, false);
					}
				}
			});

}

function processDiscountPlanAjaxResponse(response, count,
		defaultSessionMonthlyPrice, defaultSessionYearlyPrice) {

	var fields = response.split('&');
	var defaultMonthlyPrice = fields[0];
	var defaultYearlyPrice = fields[1];
	var discountMonthlyPrice = fields[2];
	var discountYearlyPrice = fields[3];
	var spanId = "defaultMonthlyPriceSpanId" + count;
	var yearlySpanId = "defaultYearlyPriceSpanId" + count;
	if (defaultMonthlyPrice) {
		if (discountMonthlyPrice) {
			var innerMonthlySpan = "(<s>$" + defaultMonthlyPrice
					+ "</s>) &nbsp; ($" + discountMonthlyPrice
					+ ") &nbsp;&nbsp;  "
			document.getElementById("selectedMonthlyPrice" + count).value = discountMonthlyPrice;
			document.getElementById(spanId).innerHTML = innerMonthlySpan;
		} else {
			document.getElementById(spanId).innerHTML = "($"
					+ defaultMonthlyPrice + ")";
			document.getElementById("selectedMonthlyPrice" + count).value = defaultMonthlyPrice;
		}

	} else if (defaultSessionMonthlyPrice) {
		document.getElementById(spanId).innerHTML = "($"
				+ defaultSessionMonthlyPrice + ")";
		document.getElementById("selectedMonthlyPrice" + count).value = defaultSessionMonthlyPrice;
	} else {
		document.getElementById(spanId).innerHTML = "";
		document.getElementById("selectedMonthlyPrice" + count).value = "0";
	}

	if (defaultYearlyPrice) {
		if (discountYearlyPrice) {
			var innerYearlySpan = "(<s>$" + defaultYearlyPrice
					+ "</s>) &nbsp; ($" + discountYearlyPrice
					+ ") &nbsp;&nbsp;  "
			document.getElementById("selectedYearlyPrice" + count).value = discountYearlyPrice;
			document.getElementById(yearlySpanId).innerHTML = innerYearlySpan;
		} else {
			document.getElementById(yearlySpanId).innerHTML = "($"
					+ defaultYearlyPrice + ")";
			document.getElementById("selectedYearlyPrice" + count).value = defaultYearlyPrice;
		}
	} else if (defaultSessionYearlyPrice) {
		document.getElementById(yearlySpanId).innerHTML = "($"
				+ defaultSessionYearlyPrice + ")";
		document.getElementById("selectedYearlyPrice" + count).value = defaultSessionYearlyPrice;
	} else {
		document.getElementById(yearlySpanId).innerHTML = "";
		document.getElementById("selectedYearlyPrice" + count).value = "0";
	}

}

function imgExceedSize(image, w, h) {
	// var w = '900';
	// var h = '900';
	if (image) {
		alert("Image Jquery Width :" + $("#retailerLogo").width());
		alert("Image Jquery height :" + $("#retailerLogo").height());
		if (image.width > w || image.height > h) {

			alert("Image size:" + image.width + "X" + image.height
					+ "?Image too big??Please make it less than " + w + "�" + h);

			alert("Image size:" + image.width + "X" + image.height
					+ "Image too big?Please make it less than " + w + "�" + h);

			return true;
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function deleteCustomPage(pageId) {
	var vWelcomePage = confirm("Are you sure you want to delete this Anything page ?")
	if (vWelcomePage) {
		document.customPageForm.pageId.value = pageId;
		document.customPageForm.action = "deleteCustomPage.htm";
		document.customPageForm.method = "GET";
		document.customPageForm.submit();
	}
}

function editManageAds(retailLocationAdsID) {
	document.manageadform.retailLocationAdvertisementID.value = retailLocationAdsID;
	document.manageadform.action = "editWelcomePage.htm";
	document.manageadform.method = "POST";
	document.manageadform.submit();
}

function saveEditAds() {
	document.editManageAdsForm.action = "updateWelcomePage.htm";
	document.editManageAdsForm.method = "POST";
	document.editManageAdsForm.submit();
}

function deleteWelcomePage(retailLocationAdsID, expireFlag, deleteFlag,
		msgParam) {
	if (msgParam == 'Stop Campaign') {
		var vWelcomePage = confirm("Are you sure you want to Stop Campaigning this welcome page ?")
	} else {
		var vWelcomePage = confirm("Are you sure you want to delete this welcome page ?")
	}
	if (vWelcomePage == true) {
		document.manageadform.retailLocationAdvertisementID.value = retailLocationAdsID;
		document.manageadform.expireFlag.value = expireFlag;
		document.manageadform.deleteFlag.value = deleteFlag;
		document.manageadform.action = "deleteWelcomePage.htm";
		document.manageadform.method = "POST";
		document.manageadform.submit();
	}
}

function showLocation(adsID) {
	openIframePopup('ifrmPopup', 'ifrm',
			'showLocation.htm?retailLocationAdvertisementID=' + adsID, 320,
			600, 'Show Locations');
}

function previewEditRetailerAds() {
	document.editManageAdsForm.action = "/ScanSeeWeb/retailer/previewRetailerAds.htm";
	document.editManageAdsForm.method = "POST";
	document.editManageAdsForm.submit();
}

function searchBannerAds() {
	document.managebanneradform.action = "managebannerads.htm";
	document.managebanneradform.method = "POST";
	document.managebanneradform.submit();
}

function deleteBannerPage(retailLocationAdsID, expireFlag, deleteFlag, msgParam) {
	if (msgParam == 'Stop Campaign') {
		var vBannerPage = confirm("Are you sure you want to Stop Campaigning this banner page ?")
	} else {
		var vBannerPage = confirm("Are you sure you want to delete this banner page ?")
	}
	if (vBannerPage == true) {
		document.managebanneradform.retailLocationAdvertisementID.value = retailLocationAdsID;
		document.managebanneradform.expireFlag.value = expireFlag;
		document.managebanneradform.deleteFlag.value = deleteFlag;
		document.managebanneradform.action = "deleteBannerPage.htm";
		document.managebanneradform.method = "POST";
		document.managebanneradform.submit();
	}
}

function showBannerLocation(adsID) {
	openIframePopup('ifrmPopup', 'ifrm',
			'showBannerLocation.htm?retailLocationAdvertisementID=' + adsID,
			320, 600, 'Show Locations');
}

function previewBannerRetailerAds() {
	document.buildbanneradform.action = "/ScanSeeWeb/retailer/previewBannerAds.htm";
	document.buildbanneradform.method = "POST";
	document.buildbanneradform.submit();
}

function previewEditBannerRetailerAds() {
	document.editBannerAdsForm.action = "/ScanSeeWeb/retailer/previewBannerAds.htm";
	document.editBannerAdsForm.method = "POST";
	document.editBannerAdsForm.submit();
}

function saveEditBannerAds() {
	document.editBannerAdsForm.action = "updateBannerAd.htm";
	document.editBannerAdsForm.method = "POST";
	document.editBannerAdsForm.submit();
}

function editBannerAds(retailLocationAdsID) {
	document.managebanneradform.retailLocationAdvertisementID.value = retailLocationAdsID;
	document.managebanneradform.action = "editBannerAd.htm";
	document.managebanneradform.method = "POST";
	document.managebanneradform.submit();
}

function submitBannerAds() {
	showProgressBar();
	document.buildbanneradform.action = "/ScanSeeWeb/retailer/buildbannerad.htm";
	document.buildbanneradform.method = "POST";
	document.buildbanneradform.submit();
}

function uploadWelcomePageCroppedImage(x, y, w, h) {

	top.document.createbanneradform.action = "/ScanSeeWeb/retailer/uploadCroppedBannerAd.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.createbanneradform.method = "POST";
	top.document.createbanneradform.submit();
}

function uploadEditWelcomePageCroppedImage(x, y, w, h) {

	top.document.editManageAdsForm.action = "/ScanSeeWeb/retailer/editBannerAdImage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.editManageAdsForm.method = "POST";
	top.document.editManageAdsForm.submit();
}

function uploadAnythingPageCroppedImage(x, y, w, h) {

	top.document.createCustomPage.action = "/ScanSeeWeb/retailer/uploadCroppedImage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.createCustomPage.method = "POST";
	top.document.createCustomPage.submit();
}

function uploadEditAnythingPageCroppedImage(x, y, w, h) {

	top.document.createCustomPage.action = "/ScanSeeWeb/retailer/editAnythinbgPageImage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.createCustomPage.method = "POST";
	top.document.createCustomPage.submit();
}

function uploadBannerAdPageCroppedImage(x, y, w, h) {

	top.document.buildbanneradform.action = "/ScanSeeWeb/retailer/uploadCroppedBannderAdImage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.buildbanneradform.method = "POST";
	top.document.buildbanneradform.submit();
}

function uploadEditBannerAdPageCroppedImage(x, y, w, h) {

	top.document.editBannerAdsForm.action = "/ScanSeeWeb/retailer/uploadEditCroppedBannderAdImage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.editBannerAdsForm.method = "POST";
	top.document.editBannerAdsForm.submit();
}

function uploadSpecialOfferPageCroppedImage(x, y, w, h) {

	top.document.createCustomPage.action = "/ScanSeeWeb/retailer/uploadCroppedSplOffImage.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.createCustomPage.method = "POST";
	top.document.createCustomPage.submit();
}

function openShowRetlrLocQrPopUp(pageId) {
	openIframePopup('ifrmPopup', 'ifrm', 'getlocqr?locId=' + pageId, 440, 600,
			'QR Code')
}

function populateDurationPeriod(checked) {
	if (checked == true) {
		// $('#datepicker1').val("");
		$('#datepicker2').val("");
		// $("#datepicker1").css({"background-color": "#494949"});
		$("#datepicker2").css({
			"background-color" : "#494949"
		});
		// $("#datepicker1").attr("disabled", "disabled");
		$("#datepicker2").attr("disabled", "disabled");
		// $("#datepicker1").datepicker( "disable" );
		$("#datepicker2").datepicker("disable");
	} else {
		// $("#datepicker1").css({"background-color": "#000"});
		$("#datepicker2").css({
			"background-color" : "#000"
		});
		// $("#datepicker1").removeAttr('disabled');
		$("#datepicker2").removeAttr('disabled');
		// $("#datepicker1").datepicker( "enable" );
		$("#datepicker2").datepicker("enable");
	}

}

function populateDurationPeriod_CustomerPage(checked) {
	if (checked == true) {
		// $('#StrtDT').val("");
		$('#EndDT').val("");
		// $("#StrtDT").css({"background-color": "#494949"});
		$("#EndDT").css({
			"background-color" : "#494949"
		});
		// $("#StrtDT").attr("disabled", "disabled");
		$("#EndDT").attr("disabled", "disabled");
		// $("#StrtDT").datepicker( "disable" );
		$("#EndDT").datepicker("disable");
	} else {
		// $("#StrtDT").css({"background-color": "#000"});
		$("#EndDT").css({
			"background-color" : "#000"
		});
		// $("#StrtDT").removeAttr('disabled');
		$("#EndDT").removeAttr('disabled');
		// $("#StrtDT").datepicker( "enable" );
		$("#EndDT").datepicker("enable");
	}
}

function populateDurationPeriod_SpecialOfferPage(checked) {
	if (checked == true) {
		// $('#StrtDT').val("");
		$('#EndDT').val("");

		// $("#splofferStartTimeHrs").val("00");
		// $("#splofferStartTimeMin").val("00");
		$("#splOfferEndTimeHrs").val("00");
		$("#splOfferEndTimeMin").val("00");

		// $("#StrtDT").css({"background-color": "#494949"});
		$("#EndDT").css({
			"background-color" : "#494949"
		});
		// $("#StrtDT").attr("disabled", "disabled");
		$("#EndDT").attr("disabled", "disabled");
		// $('#splofferStartTimeHrs').attr('disabled', 'disabled');
		// $('#splofferStartTimeMin').attr('disabled', 'disabled');
		$('#splOfferEndTimeHrs').attr('disabled', 'disabled');
		$('#splOfferEndTimeMin').attr('disabled', 'disabled');
		// $("#StrtDT").datepicker( "disable" );
		$("#EndDT").datepicker("disable");
	} else {
		// $("#StrtDT").css({"background-color": "#000"});
		$("#EndDT").css({
			"background-color" : "#000"
		});
		// $("#StrtDT").removeAttr('disabled');
		$("#EndDT").removeAttr('disabled');
		// $('#splofferStartTimeHrs').attr('disabled', false);
		// $('#splofferStartTimeMin').attr('disabled', false);
		$('#splOfferEndTimeHrs').attr('disabled', false);
		$('#splOfferEndTimeMin').attr('disabled', false);
		// $("#StrtDT").datepicker( "enable" );
		$("#EndDT").datepicker("enable");
	}
}

/* Implementing Web 1.3 */
function associateRetProd(productID, obj) {
	var vRetLocID = document.getElementById("retLocID");
	if (0 < vRetLocID.length) {
		openIframePopup('ifrmPopup', 'ifrm',
				'addProdLocationPop.htm?productID=' + productID, 496, 800,
				'Manage Product');
	} else {
		alert("Please add retailer location(s).");
		return false;
	}
}

function fetchRetailerLocation() {
	document.manageProductForm.action = "fetchretaillocationlist.htm";
	document.manageProductForm.method = "POST";
	document.manageProductForm.submit();
}

function saveRetailLocationDetails(searchValue) {
	var vProductId = document.manageProductForm.productID.value;
	var price = $('#aPrice').val();
	var salePrice = $('#aSalePrice').val();
	var saleStartDate = $('#aSaleStartDate').val();
	var saleEndDate = $('#aSaleEndDate').val();
	if ((saleStartDate == "" && saleEndDate == "" && salePrice == "")
			|| (saleStartDate != "" && saleEndDate != "" && salePrice != "")) {
		var stringObj = "{\"productID\":\"" + vProductId + "\","
				+ "\"price\":\"" + price + "\"," + "\"salePrice\":\""
				+ salePrice + "\"," + "\"saleStartDate\":\"" + saleStartDate
				+ "\"," + "\"saleEndDate\":\"" + saleEndDate + "\"}";
		$.ajaxSetup({
			cache : false
		});
		$
				.ajax({
					type : "POST",
					url : "/ScanSeeWeb/retailer/saveretlocation.htm",
					data : {
						'data' : stringObj,
						'search' : searchValue
					},

					success : function(response) {
						if (response == 'SUCCESS') {
							alert("product details saved successfully for location(s).");
							fetchRetailerLocation();
						} else {
							var vResponse = response
									.match(/Your session has been expired./g);
							if (vResponse === null || vResponse == null) {
								alert('Error While Adding Product');
							} else {
								sessionExpire();
							}
						}
					},
					error : function(e) {
						alert('Error While Adding Product');
					}
				});

	} else if (salePrice != "") {
		if ((saleStartDate == "" && saleEndDate == "")) {
			alert("Please Enter Start Date and End Date")
		} else if (saleStartDate == "") {
			alert("Please Enter Start Date")
		} else if (saleEndDate == "") {
			alert("Please Enter End Date")
		}
	} else if ((saleStartDate != "" || saleEndDate != "")) {
		if (salePrice == "") {
			alert("Please Enter Sale Price");
		}
	}
}

function editLocationDetails() {
	var productJson = [];
	var productObj = {};
	var validationFlag = true;
	var validurlFlag = true;
	var objArr = [];
	var counter = 0;
	var vProductId = document.manageProductForm.productID.value;
	var vLocsltd = $('input[name$="locationSelected"]:checked').length;

	var checkedFlag = false;
	if (vLocsltd > 0) {
		$("#addProdLoctable")
				.find("tr")
				.each(
						function(index) {

							if (index >= 0) {

								checkedFlag = false;
								var vHighlightObj = $(
										$("#addProdLoctable").find("tr")[index])
										.children();
								var vLocsltd = $('input[name$="locationSelected"]:checked').length;

								var vObject = {};
								var vStringObj;

								vHighlightObj
										.find("input")
										.each(
												function(rVal) {

													var vInputTagVal = $(this)[0].value;
													var vName = $(this).attr(
															"name")

													if (vName == 'locationSelected') {
														if ($(this).attr(
																'checked')) {

															checkedFlag = true;
															vObject.retailLocationID = vInputTagVal;
														} else {

															return;
														}

													}

													if (vName == 'price') {
														vObject.price = vInputTagVal;
													} else if (vName == 'salePrice') {
														vObject.salePrice = vInputTagVal;
													} else if (vName == 'saleStartDate') {
														vObject.saleStartDate = vInputTagVal;
													} else if (vName == 'saleEndDate') {
														vObject.saleEndDate = vInputTagVal
													}

												});
								if (checkedFlag) {

									objArr[counter++] = vObject;
								}

							}
						});

		if (objArr.length > 0) {

			for ( var k = 0; k < objArr.length; k++) {

				var object = objArr[k];
				var rlocationID = object.retailLocationID;
				var price = object.price;
				var salePrice = object.salePrice;
				var saleStartDate = object.saleStartDate;
				var saleEndDate = object.saleEndDate;
				$("tr#" + rlocationID).removeClass("hilite");
				if (salePrice != "") {
					if ((saleStartDate == "" && saleEndDate == "")) {
						$("tr#" + rlocationID).removeClass("hilite").addClass(
								"requiredVal");
						validationFlag = false;
						// alert("Please Enter Start Date and End Date")
					} else if (saleStartDate == "") {
						$("tr#" + rlocationID).removeClass("hilite").addClass(
								"requiredVal");
						validationFlag = false;
						// alert("Please Enter Start Date")
					} else if (saleEndDate == "") {
						$("tr#" + rlocationID).removeClass("hilite").addClass(
								"requiredVal");
						validationFlag = false;
						// alert("Please Enter End Date")
					} else {

						$("tr#" + rlocationID).removeClass("requiredVal");
					}
				} else if ((saleStartDate != "" || saleEndDate != "")) {
					if (salePrice == "") {
						$("tr#" + rlocationID).removeClass("hilite").addClass(
								"requiredVal");
						validationFlag = false;
						// alert("Please Enter Sale Price");
					} else {

						$("tr#" + rlocationID).removeClass("requiredVal");
					}
				}
			}
			if (validationFlag) {
				var strinObjArr = []
				for ( var k = 0; k < objArr.length; k++) {

					var object = objArr[k];

					var stringObj = "{\"location\":\""
							+ object.retailLocationID + "\","
							+ "\"productID\":\"" + vProductId + "\","
							+ "\"price\":\"" + object.price + "\","
							+ "\"salePrice\":\"" + object.salePrice + "\","
							+ "\"saleStartDate\":\"" + object.saleStartDate
							+ "\"," + "\"saleEndDate\":\"" + object.saleEndDate
							+ "\"}";
					strinObjArr[k] = stringObj;
				}
				$.ajaxSetup({
					cache : false
				});
				$
						.ajax({
							type : "POST",
							url : "/ScanSeeWeb/retailer/saveretlocation.htm",
							data : {
								'data' : "{\"retailerLocationProductJson\":["
										+ strinObjArr.toString() + "]}",
								'search' : 'submitSelected'
							},

							success : function(response) {
								if (response == 'SUCCESS') {
									alert("product details saved successfully for location(s).");
									fetchRetailerLocation();
								} else {
									var vResponse = response
											.match(/Your session has been expired./g);
									if (vResponse === null || vResponse == null) {
										alert('Error While Adding Product');
									} else {
										sessionExpire();
									}
								}
							},
							error : function(e) {
								alert('Error While Adding Product');
							}
						});
				// saveRetailLocationDetails('submitAll',strinObjArr.toString())
			} else {
				alert("Please Enter values for the mandatory fields for the highlighted rows in the screen");
				return false;
			}
		}
	} else {
		alert("Please select atleast one location")
	}
}

function callProgressBar(url) {
	showProgressBar();
	window.location.href = url;
}

function validateFileExtension(file, extns) {

	document.getElementById('uploadFileDiv').style.display = "none";
	var flag = 0;

	var ext = file.substring(file.lastIndexOf('.') + 1);
	for (i = 0; i < extns.length; i++) {
		if (ext == extns[i]) {
			flag = 0;
			break;
		} else {
			flag = 1;
		}
	}
	if (flag != 0) {
		alert('Please select valid file');
		document.getElementById('retFile').value = "";
		return false;

	} else {
		return true;
	}

}

function saveRetailerCreatedPagePDFLink() {
	showProgressBar();
	document.createCustomPage.hiddenLocId.value = $('#retCreatedPageLocId')
			.val();
	var splOfferLocId = document.createCustomPage.hiddenLocId.value;

	document.createCustomPage.action = "createretailerpage.htm";
	document.createCustomPage.method = "POST";
	document.createCustomPage.submit();
}

function updateRetailerPage() {

	showProgressBar();
	document.createCustomPage.hiddenLocId.value = $('#retCreatedPageLocId')
			.val();
	var retailerLocId = document.createCustomPage.hiddenLocId.value;

	var pageID = document.createCustomPage.pageId.value;

	document.createCustomPage.pageId.value = pageID;
	document.createCustomPage.action = "updateretailerpage.htm";
	document.createCustomPage.method = "POST";
	document.createCustomPage.submit();

}

function saveAppSiteLoc(isLocationFlag, btnVal, corporateStoreId,
		appSiteLocGeoErr) {

	var locationJson = [];
	var locationObj = {};
	var validationFlag = true;
	var validurlFlag = true;
	var validurlphone = true;
	var objArr = [];
	var storeIDArr = [];
	$("#locSetUpGrd").find("tr").each(
			function(index) {
				if (index > 0) {
					var highlightObj = $($("#locSetUpGrd").find("tr")[index])
							.children();
					var object = {};
					var stringObj;

					highlightObj.find("input").each(function(rVal) {

						var inputTagVal = $(this)[0].value;
						var name = $(this).attr("name")

						if (name == 'retailID') {
							object.retailID = inputTagVal;

						} else if (name == 'storeIdentification') {
							object.storeIdentification = inputTagVal;

						} else if (name == 'address1') {
							object.address1 = inputTagVal;

						} else if (name == 'postalCode') {
							object.postalCode = inputTagVal

						} else if (name == 'phonenumber') {
							object.phonenumber = inputTagVal

						} else if (name == 'contactFirstName') {
							object.contactFirstName = inputTagVal

						} else if (name == 'contactLastName') {
							object.contactLastName = inputTagVal

						} else if (name == 'contactMobilePhone') {
							object.contactMobilePhone = inputTagVal

						} else if (name == 'contactType') {
							object.contactType = inputTagVal

						} else if (name == 'retailLocationUrl') {
							object.retailLocationUrl = inputTagVal

						} else if (name == 'keyword') {
							object.keyword = inputTagVal

						} else if (name == 'retailerLocationLatitude') {
							object.retailerLocationLatitude = inputTagVal

						} else if (name == 'retailerLocationLongitude') {
							object.retailerLocationLongitude = inputTagVal

						}

						// }
					});

					highlightObj.find("select").each(function() {
						var inputTagVal = $(this)[0].value;
						var name = $(this).attr("name")

						if (name == 'city') {
							object.city = inputTagVal

						} else if (name == 'state') {
							object.state = inputTagVal

						}
					});

					objArr[index - 1] = object;

				}
			});
	var dataRequired = [];
	var dataRequiredIndex = 0;
	var storeIDArrIndex = 0;
	var emptyFlag = false;
	if (objArr.length > 0) {

		if (isLocationFlag == 'true') {
			objArr.splice(0, 1);
		}

		for ( var k = 0; k < objArr.length; k++) {
			emptyFlag = false;
			$("tr#" + k).removeClass("hilite");
			var object = objArr[k];
			var storeIdentification = object.storeIdentification;
			var city = object.city;
			var state = object.state;
			var postalCode = object.postalCode;
			var address1 = object.address1;
			var phonenumber = object.phonenumber;
			var retailLocationUrl = object.retailLocationUrl;
			var keyword = object.keyword;
			var latitude = object.retailerLocationLatitude;
			var longitude = object.retailerLocationLongitude;

			if ((storeIdentification == ''
					|| storeIdentification == 'undefined' || storeIdentification == 'null')
					&& (city == '' || city == 'undefined' || city == 'null' || city == '0')
					&& (state == '' || state == 'undefined' || state == 'null' || state == '0')
					&& (postalCode == '' || postalCode == 'undefined' || postalCode == 'null')
					&& (address1 == '' || address1 == 'undefined' || address1 == 'null')
					&& (phonenumber == '' || phonenumber == 'undefined' || phonenumber == 'null')
					&& (latitude == '' || latitude == 'undefined' || latitude == 'null')
					&& (longitude == '' || longitude == 'undefined' || longitude == 'null')) {

				$("tr#" + k).removeClass("requiredVal");
				emptyFlag = true

			}

			if (emptyFlag && (retailLocationUrl != '' || keyword != '')) {
				alert('hi')
				$("tr#" + k).removeClass("hilite").addClass("requiredVal");
				validationFlag = false;

			}
			if (!emptyFlag) {
				dataRequired[dataRequiredIndex++] = k;
				storeIDArr[storeIDArrIndex++] = storeIdentification;
				if (storeIdentification == ''
						|| storeIdentification == 'undefined'
						|| storeIdentification == 'null') {
					$("tr#" + k).removeClass("hilite").addClass("requiredVal");
					validationFlag = false;

				} else if (city == '' || city == 'undefined' || city == 'null'
						|| city == '0') {
					$("tr#" + k).removeClass("hilite").addClass("requiredVal");
					validationFlag = false;

				} else if (state == '' || state == 'undefined'
						|| state == 'null' || state == '0') {
					$("tr#" + k).removeClass("hilite").addClass("requiredVal");
					validationFlag = false;

				} else if (postalCode == '' || postalCode == 'undefined'
						|| postalCode == 'null') {
					$("tr#" + k).removeClass("hilite").addClass("requiredVal");
					validationFlag = false;

				} else if (address1 == '' || address1 == 'undefined'
						|| address1 == 'null') {
					$("tr#" + k).removeClass("hilite").addClass("requiredVal");
					validationFlag = false;

				} else if (phonenumber == '' || phonenumber == 'undefined'
						|| phonenumber == 'null') {
					$("tr#" + k).removeClass("hilite").addClass("requiredVal");
					validationFlag = false;

				} else {
					$("tr#" + k).removeClass("requiredVal");
				}
				var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
				if (retailLocationUrl != '') {
					if (!regexp.test(retailLocationUrl)) {
						$("tr#" + k).removeClass("hilite").addClass(
								"requiredVal");

						validurlFlag = false;

					}
				}
				var regexpphone = /\(\d{3}\)\d{3}-\d{4}/;
				if (phonenumber != '') {
					if (!regexpphone.test(phonenumber)) {
						$("tr#" + k).removeClass("hilite").addClass(
								"requiredVal");

						validurlphone = false;

					}
				}

			}

		}

		if (validationFlag) {
			if (!validurlphone) {
				alert("Invalid phone number");
				return false;
			}

			if (!validurlFlag) {
				alert("Please enter a valid URL.");
				return false;
			}

			var duplicateIdArr = [];

			if (storeIDArr.length > 0) {
				var firstVal;
				var index = 0;

				var duplcateFlg = false;
				for ( var i = 0; i <= storeIDArr.length; i++) {
					duplcateFlg = false;
					for ( var j = i + 1; j <= storeIDArr.length; j++) {

						firstVal = storeIDArr[i];
						if (firstVal == storeIDArr[j]) {
							duplicateIdArr[index++] = j;
							duplcateFlg = true;
						}

					}
					if (duplcateFlg == true) {
						duplicateIdArr[index++] = i;

					}

				}

			}
			if (corporateStoreId != null && corporateStoreId != '') {

				if (storeIDArr.length > 0) {
					for ( var i = 0; i <= storeIDArr.length; i++) {
						if (storeIDArr[i] == corporateStoreId) {

							alert("Store id " + "'" + corporateStoreId + "'"
									+ " is already exist");
							return false;
						}

					}

				}
			}

			if (duplicateIdArr.length > 0) {

				for ( var k = 0; k < duplicateIdArr.length; k++) {

					$("tr#" + duplicateIdArr[k]).removeClass("hilite")
							.addClass("requiredVal");
				}
				alert("Store Identification must be unique");
				return false;
			}

			var strinObjArr = []

			if (dataRequired.length > 0) {

				for ( var m = 0; m < dataRequired.length; m++) {

					var object = objArr[dataRequired[m]];
					var stringObj = "{\"retailerID\":\"" + object.retailID
							+ "\","

							+ "\"storeIdentification\":\""
							+ object.storeIdentification + "\","
							+ "\"address1\":\"" + object.address1 + "\","
							+ "\"city\":\"" + object.city + "\","
							+ "\"state\":\"" + object.state + "\","
							+ "\"postalCode\":\"" + object.postalCode + "\","
							+ "\"phonenumber\":\"" + object.phonenumber + "\","
							+ "\"retailLocationUrl\":\""
							+ object.retailLocationUrl + "\","
							+ "\"keyword\":\"" + object.keyword + "\","
							+ "\"retailerLocationLatitude\":\""
							+ object.retailerLocationLatitude + "\","
							+ "\"retailerLocationLongitude\":\""
							+ object.retailerLocationLongitude + "\"}";

					strinObjArr[m] = stringObj;

				}
				if (btnVal == 'skip') {
					var save = confirm("Do you want to save")
					if (save == true) {
						saveAppListLocationList(strinObjArr.toString())
					} else {
						window.location.href = "/ScanSeeWeb/retailer/createAppSite.htm";
					}
				} else {
					// Submtitting to save the changes.
					saveAppListLocationList(strinObjArr.toString())

				}

			} else if (isLocationFlag == 'false') {

				alert("Please create atleast one location")
			} else {
				if (btnVal != 'skip') {
					var saveorskip = confirm("No records to save, Do you want to skip?")
					if (saveorskip == true) {
						window.location.href = "/ScanSeeWeb/retailer/createAppSite.htm";
					}
				} else {

					window.location.href = "/ScanSeeWeb/retailer/createAppSite.htm";
				}

			}
		} else {
			alert("Please Enter values for the mandatory fields for the highlighted rows in the screen");
			return false;
		}
	}

}

function saveAppListLocationList(objArr) {
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.locationsetupform.prodJson.value = "{\"locationData\":[" + objArr
			+ "]}";
	document.locationsetupform.action = "createappsiteloc.htm";
	document.locationsetupform.method = "POST";
	document.locationsetupform.submit();
}

function previewRetailerManageProductPopUp(productId) {
	var cnt = $('#retLocID option:selected').length;
	var val = "";
	if (cnt == 1) {
		val = $('#retLocID option:selected').attr("value");
		window.open("/ScanSeeWeb/retailer/previewretailprod.htm?productID="
				+ productId + "&retailLocationID=" + val);
	} else {
		alert("Please select at most one location to product preview");
	}
}

function updateLocationInfo(pagenumber) {

	var productJson = [];
	var productObj = {};
	var validurlFlag = true;
	var objArr = [];
	var vRetailerLocID = $("#retailerLocID").val();
	if (vRetailerLocID != "null" && vRetailerLocID != "") {
		vRetLocList = vRetailerLocID.split(",");
	}
	$("#locSetUpGrd").find("tr").each(
			function(index) {
				if (index > 0) {
					var highlightObj = $($("#locSetUpGrd").find("tr")[index])
							.children();
					var object = {};
					var stringObj;

					highlightObj.find("input").each(function(rVal) {

						var inputTagVal = $(this)[0].value;
						var name = $(this).attr("name")

						if (name == 'uploadRetaillocationsID') {
							object.uploadRetaillocationsID = inputTagVal;

						} else if (name == 'retailID') {
							object.retailID = inputTagVal;

						} else if (name == 'storeIdentification') {
							object.storeIdentification = inputTagVal;

						} else if (name == 'address1') {
							object.address1 = inputTagVal;

						} else if (name == 'retailerLocationLatitude') {
							object.retailerLocationLatitude = inputTagVal;

						} else if (name == 'retailerLocationLongitude') {
							object.retailerLocationLongitude = inputTagVal;

						} else if (name == 'city') {
							object.city = inputTagVal

						} else if (name == 'state') {
							object.state = inputTagVal

						} else if (name == 'postalCode') {
							object.postalCode = inputTagVal

						} else if (name == 'phonenumber') {
							object.phonenumber = inputTagVal

						} else if (name == 'retailLocationUrl') {
							object.retailLocationUrl = inputTagVal
						} else if (name == 'keyword') {
							object.keyword = inputTagVal
						} else if (name == 'gridImgLocationPath') {
							object.gridImgLocationPath = inputTagVal
						} else if (name == 'imgLocationPath') {
							object.imgLocationPath = inputTagVal
						} else if (name == 'uploadImage') {
							object.uploadImage = inputTagVal
						}
						// }
					});

					highlightObj.find("select").each(function(rVal) {

						var inputTagVal = $(this)[0].value;
						var name = $(this).attr("name")

						if (name == 'contactTitle') {
							object.contactTitle = inputTagVal;

						}

					});

					objArr[index - 1] = object;
					/*
					 * }else{ // no change }
					 */

				}
			});

	if (objArr.length > 0) {
		var validationFlag = true;
		for ( var k = 0; k < objArr.length; k++) {
			var object = objArr[k];
			var retailID = object.retailID;
			var uploadRetaillocationsID = object.uploadRetaillocationsID;
			$("tr#" + uploadRetaillocationsID).removeClass("hilite");
			var storeIdentification = object.storeIdentification;
			var city = object.city;
			var state = object.state;
			var postalCode = object.postalCode;
			var address1 = object.address1;
			/*
			 * var contactFirstName = object.contactFirstName; var
			 * contactLastName = object.contactLastName; var contactTitle =
			 * object.contactTitle; var contactMobilePhone =
			 * object.contactMobilePhone; var contactEmail =
			 * object.contactEmail;
			 */
			var retailLocationUrl = object.retailLocationUrl;
			var keyword = object.keyword;
			var vLatitude = object.retailerLocationLatitude;
			var vLongitude = object.retailerLocationLongitude;
			var phonenumber = object.phonenumber;

			if (vRetailerLocID != "null" && vRetailerLocID != "") {
				if (storeIdentification == ''
						|| storeIdentification == 'undefined'
						|| storeIdentification == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (city == '' || city == 'undefined' || city == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (state == '' || state == 'undefined'
						|| state == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (postalCode == '' || postalCode == 'undefined'
						|| postalCode == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (address1 == '' || address1 == 'undefined'
						|| address1 == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (phonenumber == '' || phonenumber == 'undefined'
						|| phonenumber == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				}/*
				 * else if (contactFirstName == '' || contactFirstName ==
				 * 'undefined' || contactFirstName == 'null') { $("tr#" +
				 * uploadRetaillocationsID).addClass("requiredVal");
				 * validationFlag = false; } else if (contactLastName == '' ||
				 * contactLastName == 'undefined' || contactLastName ==
				 * 'null') { $("tr#" +
				 * uploadRetaillocationsID).addClass("requiredVal");
				 * validationFlag = false; } else if (contactTitle == '' ||
				 * contactTitle == 'undefined' || contactTitle == 'null') {
				 * $("tr#" +
				 * uploadRetaillocationsID).addClass("requiredVal");
				 * validationFlag = false; } else if (contactEmail == '' ||
				 * contactEmail == 'undefined' || contactEmail == 'null') {
				 * $("tr#" +
				 * uploadRetaillocationsID).addClass("requiredVal");
				 * validationFlag = false; }
				 */else if (vLatitude == '' || vLatitude == 'undefined'
						|| vLatitude == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else if (vLongitude == '' || vLongitude == 'undefined'
						|| vLongitude == 'null') {
					$("tr#" + retailID).removeClass("hilite").addClass(
							"requiredVal");
					validationFlag = false;
				} else {
					$("tr#" + uploadRetaillocationsID).removeClass(
							"requiredVal");
				}
			} else {
				if (storeIdentification == ''
						|| storeIdentification == 'undefined'
						|| storeIdentification == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (city == '' || city == 'undefined' || city == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (state == '' || state == 'undefined'
						|| state == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (postalCode == '' || postalCode == 'undefined'
						|| postalCode == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (address1 == '' || address1 == 'undefined'
						|| address1 == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else if (phonenumber == '' || phonenumber == 'undefined'
						|| phonenumber == 'null') {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validationFlag = false;
				} else {
					$("tr#" + uploadRetaillocationsID).removeClass(
							"requiredVal");
				}
			}
			var regexp = /(ftp\:\/\/|http\:\/\/|https\:\/\/|www\.)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (retailLocationUrl != '') {
				if (!regexp.test(retailLocationUrl)) {
					$("tr#" + uploadRetaillocationsID).addClass("requiredVal");
					validurlFlag = false;
				}
			}

		}

		if (validationFlag) {
			if (!validurlFlag) {
				alert("Please enter a valid URL.");
				return false;
			}
			var strinObjArr = []
			for ( var k = 0; k < objArr.length; k++) {
				var object = objArr[k];
				var stringObj;

				if (vRetailerLocID != "null" && vRetailerLocID != "") {
					stringObj = "{\"retailID\":\"" + object.retailID + "\","
							+ "\"uploadRetaillocationsID\":\""
							+ object.uploadRetaillocationsID + "\","
							+ "\"storeIdentification\":\""
							+ object.storeIdentification + "\","
							+ "\"address1\":\"" + object.address1 + "\","
							+ "\"city\":\"" + object.city + "\","
							+ "\"state\":\"" + object.state + "\","
							+ "\"postalCode\":\"" + object.postalCode + "\","
							+ "\"phonenumber\":\"" + object.phonenumber + "\","
							+ "\"contactFirstName\":\""
							+ object.contactFirstName + "\","
							+ "\"contactLastName\":\"" + object.contactLastName
							+ "\"," + "\"contactTitle\":\""
							+ object.contactTitle + "\","
							+ "\"contactMobilePhone\":\""
							+ object.contactMobilePhone + "\","
							+ "\"contactEmail\":\"" + object.contactEmail
							+ "\"," + "\"retailLocationUrl\":\""
							+ object.retailLocationUrl + "\","
							+ "\"keyword\":\"" + object.keyword + "\","
							+ "\"retailerLocationLatitude\":\""
							+ object.retailerLocationLatitude + "\","
							+ "\"retailerLocationLongitude\":\""
							+ object.retailerLocationLongitude + "\","
							+ "\"imgLocationPath\":\""
							+ object.imgLocationPath + "\","
							+ "\"gridImgLocationPath\":\""
							+ object.gridImgLocationPath + "\","
							+ "\"uploadImage\":\""
							+ object.uploadImage + "\"}";
				} else {
					stringObj = "{\"retailID\":\"" + object.retailID + "\","
					+ "\"uploadRetaillocationsID\":\""
					+ object.uploadRetaillocationsID + "\","
					+ "\"storeIdentification\":\""
					+ object.storeIdentification + "\","
					+ "\"address1\":\"" + object.address1 + "\","
					+ "\"city\":\"" + object.city + "\","
					+ "\"state\":\"" + object.state + "\","
					+ "\"postalCode\":\"" + object.postalCode + "\","
					+ "\"phonenumber\":\"" + object.phonenumber + "\","
					+ "\"contactFirstName\":\""
					+ object.contactFirstName + "\","
					+ "\"contactLastName\":\"" + object.contactLastName
					+ "\"," + "\"contactTitle\":\""
					+ object.contactTitle + "\","
					+ "\"contactMobilePhone\":\""
					+ object.contactMobilePhone + "\","
					+ "\"contactEmail\":\"" + object.contactEmail
					+ "\"," + "\"retailLocationUrl\":\""
					+ object.retailLocationUrl + "\","
					+ "\"keyword\":\"" + object.keyword + "\","
					+ "\"retailerLocationLatitude\":\""
					+ object.retailerLocationLatitude + "\","
					+ "\"retailerLocationLongitude\":\""
					+ object.retailerLocationLongitude + "\","
					+ "\"imgLocationPath\":\""
					+ object.imgLocationPath + "\","
					+ "\"gridImgLocationPath\":\""
					+ object.gridImgLocationPath + "\","
					+ "\"uploadImage\":\""
					+ object.uploadImage + "\"}";
				}
				strinObjArr[k] = stringObj;
			}
			// Submtitting to save the changes.
			saveLocationListPage(strinObjArr.toString(), pagenumber)
		} else {
			alert("Please Enter values for the mandatory fields for the highlighted rows in the screen");
			return false;
		}
	} else {
		alert("Please upload locations and Click on Save to Save the Changes")
	}

}

function saveLocationListPage(objArr, pagenumber) {
	if (pagenumber == null || pagenumber == undefined) {
		document.locationsetupform.pageFlag.value = "false";
	} else {

		document.locationsetupform.pageNumber.value = pagenumber;
		document.locationsetupform.pageFlag.value = "true";
	}
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.locationsetupform.prodJson.value = "{\"locationData\":[" + objArr
			+ "]}";
	document.locationsetupform.action = "updatelocationpageinfo.htm";
	document.locationsetupform.method = "POST";
	document.locationsetupform.submit();
}

function finalizeAppSiteRetProfileSetUp() {
	/*
	 * var locCordinates = ""; if (document.getElementById("locCoordinates")) {
	 * locCordinates = document.getElementById("locCoordinates").value; }
	 * document.createretailerprofileform.action =
	 * "createRetailerProfile.htm?locationCoordinates=" + locCordinates;
	 */
	document.appsiteform.action = "finalizereg.htm";
	document.appsiteform.method = "POST";
	document.appsiteform.submit();
}

function saveGiveawayPage() {
	document.createCustomPage.action = "savegiveawaypage.htm";
	document.createCustomPage.method = "POST";
	document.createCustomPage.submit();

}

function updateGiveawayInfo() {
	document.createCustomPage.action = "updategiveawaypage.htm";
	document.createCustomPage.method = "POST";
	document.createCustomPage.submit();

}

function uploadAddHotDealCroppedImage(x, y, w, h) {
	top.document.addhotdealretailerform.action = "/ScanSeeWeb/retailer/uploadcroppedaddhotdealimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.addhotdealretailerform.method = "POST";
	top.document.addhotdealretailerform.submit();
}

function uploadEditHotDealCroppedImage(x, y, w, h) {

	top.document.edithotdealretailerform.action = "/ScanSeeWeb/retailer/uploadcroppededithotdealimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.edithotdealretailerform.method = "POST";
	top.document.edithotdealretailerform.submit();
}
function uploadReRunHotDealCroppedImage(x, y, w, h) {
	top.document.retailerHotDealsReRunForm.action = "/ScanSeeWeb/retailer/uploadcroppedrerunhotdealimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.retailerHotDealsReRunForm.method = "POST";
	top.document.retailerHotDealsReRunForm.submit();
}

function uploadAddCouponCroppedImage(x, y, w, h) {
	top.document.createcouponform.action = "/ScanSeeWeb/retailer/uploadcroppedaddcouponimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.createcouponform.method = "POST";
	top.document.createcouponform.submit();
}

function uploadEditCouponCroppedImage(x, y, w, h) {
	top.document.editcouponform.action = "/ScanSeeWeb/retailer/uploadcroppededitcouponimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.editcouponform.method = "POST";
	top.document.editcouponform.submit();
}
function uploadReRunCouponCroppedImage(x, y, w, h) {
	top.document.reruncouponform.action = "/ScanSeeWeb/retailer/uploadcroppedreruncouponimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.reruncouponform.method = "POST";
	top.document.reruncouponform.submit();
}

function sessionExpire() {
	alert("Your session has been expired");
	parent.window.location = "/ScanSeeWeb/login.htm";
	closeIframePopup('ifrmPopup', 'ifrm', ' ');
}

function hotDealRetailerViewReport(vHDealID) {
	document.retailerDealsForm.hotDealID.value = vHDealID;
	document.retailerDealsForm.action = "hotdealreportview.htm";
	document.retailerDealsForm.method = "GET";
	document.retailerDealsForm.submit();
}

/* Anything page show locations */

function showAnythingPageLocation(pageID) {
	openIframePopup('ifrmPopup', 'ifrm',
			'showAnythingPageLocation.htm?QRRetailerCustomPageID=' + pageID,
			320, 600, 'Show Locations');
}


function uploadAddLocationCroppedImage(x, y, w, h) {
	top.document.addlocationform.action = "/ScanSeeWeb/retailer/uploadcroppedaddlocationimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.addlocationform.method = "POST";
	top.document.addlocationform.submit();
}



function uploadMultipleImage() {
	var selFile = document.locationsetupform.imageFilePath.value
	/*if (document.getElementById('imageFilePath.errors') != null) {
		document.getElementById('imageFilePath.errors').style.display = 'none';
	}*/
	var vLocMsg = $("#locMsg").text();
	 if (vLocMsg.length > 0) {
		 $("#locMsg").text("");
	 }
	 var vLocMsg = $("#locSuccessMsg").text();
	 if (vLocMsg.length > 0) {
		 $("#locSuccessMsg").text("");
	 }
	 
	if (selFile == 'null' || selFile == '') {
		alert('Please select Image file');
	} else {
		var file = validateUploadLocationImageFileExt(selFile);
		if (file) {
			document.locationsetupform.action = "uploadlocationimages.htm";
			document.locationsetupform.method = "POST";
			document.locationsetupform.submit();
		} else {
			alert('You must upload Location image with following extensions : .png, .gif, .bmp, .jpg, .jpeg, .zip"');
			document.locationsetupform.imageFilePath.value = "";
			document.getElementById("btchUpldMedia").focus();
			return false;
		}
	}
}

function validateUploadLocationImageFileExt(file) {
	if (file != '' && file != 'undefined') {
		var extn = file.toLowerCase();
		if (extn.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp|\.zip)$/)) {
			return true;
		} else {
			return false;
		}
	}
}

function validateUploadImageFileExt(file) {
	if (file != '' && file != 'undefined') {
		var extn = file.toLowerCase();
		if (extn.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
			return true;
		} else {
			return false;
		}
	}
}


function uploadLocationCroppedImage(x, y, w, h) {
	top.document.locationsetupform.action = "/ScanSeeWeb/retailer/uploadcroppedlocationimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.locationsetupform.method = "POST";
	top.document.locationsetupform.submit();
}

/*Upload Retailer Location grid screen*/
function gridUploadLocationSetUp(vLocationID) {
	var objArr = [];

	$("#locSetUpGrd").find("tr").each(
			function(index) {
				if (index > 0) {
					var highlightObj = $($("#locSetUpGrd").find("tr")[index]).children();
					var object = {};
					var stringObj;

					highlightObj.find("input").each(function(rVal) {
						var inputTagVal = $(this)[0].value;
					
						var name = $(this).attr("name")
						
						if (name == 'uploadRetaillocationsID') {
							object.uploadRetaillocationsID = inputTagVal;
						} else if (name == 'retailID') {
							object.retailID = inputTagVal;
						} else if (name == 'storeIdentification') {
							object.storeIdentification = inputTagVal;
						} else if (name == 'address1') {
							object.address1 = inputTagVal;
						} else if (name == 'retailerLocationLatitude') {
							object.retailerLocationLatitude = inputTagVal;
						} else if (name == 'retailerLocationLongitude') {
							object.retailerLocationLongitude = inputTagVal;
						} else if (name == 'city') {
							object.city = inputTagVal
						} else if (name == 'state') {
							object.state = inputTagVal
						} else if (name == 'postalCode') {
							object.postalCode = inputTagVal
						} else if (name == 'phonenumber') {
							object.phonenumber = inputTagVal
						}else if (name == 'retailLocationUrl') {
							object.retailLocationUrl = inputTagVal
						} else if (name == 'keyword') {
							object.keyword = inputTagVal
						} else if (name == 'gridImgLocationPath') {
							object.gridImgLocationPath = inputTagVal
						} else if (name == 'imgLocationPath') {
							object.imgLocationPath = inputTagVal
						} else if (name == 'uploadImage') {
							object.uploadImage = inputTagVal
						}
					});
					objArr[index - 1] = object;
				}
			});

	
	if (objArr.length > 0) {	
			var strinObjArr = []
			var strinObjArr1
			for ( var k = 0; k < objArr.length; k++) {
				var object = objArr[k];
				var stringObj
				/*if (vRetailerLocID != "null" && vRetailerLocID != "") {*/
					stringObj = "{\"retailID\":\"" + object.retailID + "\","
							+ "\"uploadRetaillocationsID\":\""
							+ object.uploadRetaillocationsID + "\","
							+ "\"storeIdentification\":\""
							+ object.storeIdentification + "\","
							+ "\"address1\":\"" + object.address1 + "\","
							+ "\"city\":\"" + object.city + "\","
							+ "\"state\":\"" + object.state + "\","
							+ "\"postalCode\":\"" + object.postalCode + "\","
							+ "\"phonenumber\":\"" + object.phonenumber + "\","
							+ "\"retailLocationUrl\":\""
							+ object.retailLocationUrl + "\","
							+ "\"keyword\":\"" + object.keyword + "\","
							+ "\"retailerLocationLatitude\":\""
							+ object.retailerLocationLatitude + "\","
							+ "\"retailerLocationLongitude\":\""
							+ object.retailerLocationLongitude + "\","
							+ "\"imgLocationPath\":\""
							+ object.imgLocationPath + "\","
							+ "\"gridImgLocationPath\":\""
							+ object.gridImgLocationPath + "\","
							+ "\"uploadImage\":\""
							+ object.uploadImage + "\"}";
				/*}*/
				if (object.uploadRetaillocationsID === vLocationID) {
					strinObjArr1 = stringObj;
				}
				strinObjArr[k] = stringObj;
			}
				document.locationsetupform.prodJson.value = "{\"locationData\":[" + strinObjArr.toString()+ "]}";
				document.locationsetupform.prodJson1.value = "{\"locationData\":[" + strinObjArr1.toString()+"]}";
	} 
}


/*Batch Upload Retailer Location  Screen*/
function checkLocationImgValidate(input,rowIndex) {
	
	var vLocationID = $(input).parents('tr').find(
			"input[name='uploadRetaillocationsID']").val();
	var vLocationImg = $(input).parents('tr').find(
			"input[name='imageFile']").val();
	var vInput = $(input).parents('tr').find("input[name='imageFile']")
			.attr("id");
	
	$("#rowIndex").val(rowIndex);
	
	if (vLocationImg != '') {
		var checkLocationImg = vLocationImg.toLowerCase();
		if (!checkLocationImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
			alert("You must upload location image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
			$(input).parents('tr').find("input[name='imageFile']").val("");
			$(input).parents('tr').find("input[name='imageFile']").focus();
			return false;
		} else {
			/*show progress bar : ETA for Web 1.3*/
			//showProgressBar();/* Commented to fix body scroll disable issue*/
			/*End*/
	
			gridUploadLocationSetUp(vLocationID);
			
			$.ajaxSetup({
				cache : false
			});
			
			$("#locationsetupform")
					.ajaxForm(
							{
								success : function(response) {
									$('#loading-image').css("visibility",
											"hidden");
									var imgRes = response
											.getElementsByTagName('imageScr')[0].firstChild.nodeValue
									if (imgRes == 'UploadLogoMaxSize') {
										alert("Image Dimension should not exceed Width: 800px Height: 600px");
										$(input).parents('tr').find("input[name='imageFile']").val("");
										$(input).parents('tr').find("input[name='imageFile']").focus();
									} else if (imgRes == 'UploadLogoMinSize') {
										alert("Image Dimension should be Minimum Width: 70px Height: 70px");
										$(input).parents('tr').find("input[name='imageFile']").val("");
										$(input).parents('tr').find("input[name='imageFile']").focus();
									} else {
										var substr = imgRes.split('|');
										if (substr[0] == 'ValidImageDimention') {
											$('.img-preview').width('28px');
											$('.img-preview')
													.height('28px');
											var imgName = substr[1];
											$(this).parents('tr').find("img.img-preview").attr("src", substr[2]);
											$(input).parents('tr').find("input[name='gridImgLocationPath']").val(imgName);
										} else {
											/*commented to fix iframe popup scroll issue
											/$('body').css("overflow-y","hidden");*/
											openIframePopupForImage(
													'ifrmPopup',
													'ifrm',
													'/ScanSeeWeb/retailer/cropImageGeneral.htm',
													100, 99.5, 'Crop Image');
										}
									}
								}
							}).submit();
		}
	}
}


/*Manage Retailer Location  Screen*/
function manageLocationImgValidate(input,rowIndex) {
	
	var vLocationID = $(input).parents('tr').find(
			"input[name='retailLocationID']").val();
	var vLocationImg = $(input).parents('tr').find(
			"input[name='imageFile']").val();
	var vInput = $(input).parents('tr').find("input[name='imageFile']")
			.attr("id");
	
	$("#rowIndex").val(rowIndex);
	
	if (vLocationImg != '') {
		var checkLocationImg = vLocationImg.toLowerCase();
		if (!checkLocationImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
			alert("You must upload location image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
			$(input).parents('tr').find("input[name='imageFile']").val("");
			$(input).parents('tr').find("input[name='imageFile']").focus();
			return false;
		} else {
			/*show progress bar : ETA for Web 1.3*/
			//showProgressBar();/* Commented to fix body scroll disable issue*/
			/*End*/
			gridManageLocation(vLocationID);
			
			$.ajaxSetup({
				cache : false
			});
			
			$("#locationform")
					.ajaxForm(
							{
								success : function(response) {
									$('#loading-image').css("visibility",
											"hidden");
									var imgRes = response
											.getElementsByTagName('imageScr')[0].firstChild.nodeValue
											
									if (imgRes == 'UploadLogoMaxSize') {
										alert("Image Dimension should not exceed Width: 800px Height: 600px");
										$(input).parents('tr').find("input[name='imageFile']").val("");
										$(input).parents('tr').find("input[name='imageFile']").focus();
									} else if (imgRes == 'UploadLogoMinSize') {
										alert("Image Dimension should be Minimum Width: 70px Height: 70px");
										$(input).parents('tr').find("input[name='imageFile']").val("");
										$(input).parents('tr').find("input[name='imageFile']").focus();
									} else {
										var substr = imgRes.split('|');
										if (substr[0] == 'ValidImageDimention') {
											$('.img-preview').width('28px');
											$('.img-preview').height('28px');
											var imgName = substr[1];
											$(this).parents('tr').find("img.img-preview").attr("src", substr[2]);
											$(input).parents('tr').find("input[name='gridImgLocationPath']").val(imgName);
										} else {
											/*commented to fix iframe popup scroll issue
											/$('body').css("overflow-y","hidden");*/
											openIframePopupForImage(
													'ifrmPopup2',
													'ifrm2',
													'/ScanSeeWeb/retailer/cropImageGeneral.htm',
													100, 99.5, 'Crop Image');
										}
									}
								}
							}).submit();
		}
	}
}


function manageLocationCroppedImage(x, y, w, h) {
	top.document.locationform.action = "/ScanSeeWeb/retailer/managecroppedlocationimg.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.locationform.method = "POST";
	top.document.locationform.submit();
}


/*Manage Retailer Location grid screen*/
function gridManageLocation(vLocationID) {
	var objArr = [];
	$("#locSetUpGrd")
			.find("tr")
			.each(
					function(index) {
						if (index > 0) {
							var highlightObj = $(
									$("#locSetUpGrd").find("tr")[index])
									.children();
							var object = {};
							var stringObj;

							highlightObj
									.find("input")
									.each(
											function(rVal) {

												var inputTagVal = $(this)[0].value;
												var name = $(this).attr("name")

												if (name == 'uploadRetaillocationsID') {
													object.uploadRetaillocationsID = inputTagVal;

												} else if (name == 'retailLocationID') {
													object.retailLocationID = inputTagVal;

												} else if (name == 'storeIdentification') {
													object.storeIdentification = inputTagVal;

												} else if (name == 'address1') {
													object.address1 = inputTagVal;

												} else if (name == 'retailerLocationLatitude') {
													object.retailerLocationLatitude = inputTagVal;

												} else if (name == 'retailerLocationLongitude') {
													object.retailerLocationLongitude = inputTagVal;

												} else if (name == 'city') {
													object.city = inputTagVal

												} else if (name == 'state') {
													object.state = inputTagVal

												} else if (name == 'postalCode') {
													object.postalCode = inputTagVal

												} else if (name == 'phonenumber') {
													object.phonenumber = inputTagVal
													var phoneNum = validatePhoneForLoc(object.phonenumber);
													if (!phoneNum) {
														$(this).parent('tr').find('#phonenumber').val("");
														validationFlag = false;
													}

												} else if (name == 'retailLocationUrl') {
													object.retailLocationUrl = inputTagVal

												} else if (name == 'keyword') {
													object.keyword = inputTagVal

												}else if (name == 'gridImgLocationPath') {
													object.gridImgLocationPath = inputTagVal
													
												} else if (name == 'imgLocationPath') {
													object.imgLocationPath = inputTagVal
													
												} else if (name == 'uploadImage') {
													object.uploadImage = inputTagVal
												}

											});

							objArr[index - 1] = object;

						}
					});

	if (objArr.length > 0) {
		for ( var k = 0; k < objArr.length; k++) {
			var object = objArr[k];
			var strinObjArr = []
			for ( var k = 0; k < objArr.length; k++) {
				var object = objArr[k];
				var stringObj;
					stringObj = "{\"retailLocationID\":\""
							+ object.retailLocationID + "\","
							+ "\"storeIdentification\":\""
							+ object.storeIdentification + "\","
							+ "\"address1\":\"" + object.address1
							+ "\"," + "\"retailerLocationLatitude\":\""
							+ object.retailerLocationLatitude + "\","
							+ "\"retailerLocationLongitude\":\""
							+ object.retailerLocationLongitude + "\","
							+ "\"city\":\"" + object.city + "\","
							+ "\"state\":\"" + object.state + "\","
							+ "\"postalCode\":\"" + object.postalCode + "\","
							+ "\"phonenumber\":\"" + object.phonenumber + "\","
							+ "\"retailLocationUrl\":\""
							+ object.retailLocationUrl + "\","
							+ "\"keyword\":\"" + object.keyword + "\","
							+ "\"imgLocationPath\":\""
							+ object.imgLocationPath + "\","
							+ "\"gridImgLocationPath\":\""
							+ object.gridImgLocationPath + "\","
							+ "\"uploadImage\":\""
							+ object.uploadImage + "\"}";

					if (object.retailLocationID === vLocationID) {
						strinObjArr1 = stringObj;
					}
					strinObjArr[k] = stringObj;
			}

			document.locationform.prodJson.value = "{\"locationData\":[" + strinObjArr.toString()+ "]}";
			document.locationform.prodJson1.value = "{\"locationData\":[" + strinObjArr1.toString()+"]}";
		} 
	} 
}


function deleteEvent(eventID) {
	var vDeleteEvent = confirm("Are you sure you want to delete this Event page ?")
	if (vDeleteEvent) {
		document.manageeventform.eventID.value = eventID;
		document.manageeventform.action = "deleteevent.htm";
		document.manageeventform.method = "POST";
		document.manageeventform.submit();
	}
}


function showEventLocation(eventID) {
	$("#message label").html("");
	openIframePopup('ifrmPopup', 'ifrm',
			'/ScanSeeWeb/retailer/showeventlocation.htm?eventID=' + eventID, 320,
			600, 'Show Event Locations');
}


function searchEvents() {
	document.manageeventform.action = "manageevents.htm";
	document.manageeventform.method = "POST";
	document.manageeventform.submit();
}

function editEvent(eventID) {
	document.manageeventform.eventID.value = eventID;
	document.manageeventform.action = "editevent.htm";
	document.manageeventform.method = "POST";
	document.manageeventform.submit();
}

function backManageEvents() {
	var vManageEvent = confirm("Are you sure you want to leave this page without saving the changes!");
	if (vManageEvent) {
		document.addediteventform.action = "manageevents.htm";
		document.addediteventform.method = "POST";
		document.addediteventform.submit();
	}
}


function addEditEventCroppedImage(x, y, w, h) {
	top.document.addediteventform.action = "/ScanSeeWeb/retailer/uploadcroppedaddeditevent.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.addediteventform.method = "POST";
	top.document.addediteventform.submit();
}


function checkEventImgValidate(input) {
	var vEventImg = document.getElementById("trgrUpld").value;
	if (vEventImg != '') {
		var checkEventImg = vEventImg.toLowerCase();
		if (!checkEventImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
			alert("You must upload Event image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
			document.addediteventform.eventImageFile.value = "";
			document.getElementById("trgrUpld").focus();
			return false;
		} else {
			/*show progress bar : ETA for Web 1.3*/
			//showProgressBar();/* Commented to fix body scroll disable issue*/
			/*End*/
			
			$.ajaxSetup({
				cache : false
			});
			$("#addediteventform")
					.ajaxForm(
							{
								success : function(response) {
									$('#loading-image').css(
											"visibility", "hidden");
									var imgRes = response
											.getElementsByTagName('imageScr')[0].firstChild.nodeValue;
									if (imgRes == 'UploadLogoMaxSize') {
										$('#eventImagePathErr')
												.text(
														"Image Dimension should not exceed Width: 800px Height: 600px");
									} else if (imgRes == 'UploadLogoMinSize') {
										$('#eventImagePathErr')
												.text(
														"Image Dimension should be Minimum Width: 70px Height: 70px");
									} else {
										$('#eventImagePathErr')
												.text("");
										var substr = imgRes
												.split('|');
										if (substr[0] == 'ValidImageDimention') {
											$('#eventImg').width(
													'70px');
											$('#eventImg').height(
													'70px');
											var imgName = substr[1];
											$('#eventImgPath').val(
													imgName);
											$('#eventImg').attr(
													"src",
													substr[2]);
										} else {
											/*commented to fix iframe popup scroll issue
											/$('body').css("overflow-y","hidden");*/
											openIframePopupForImage(
													'ifrmPopup',
													'ifrm',
													'/ScanSeeWeb/retailer/cropImageGeneral.htm',
													100, 99.5,
													'Crop Image');
										}
									}
								}
							}).submit();
		}
	}
}



function saveEvent() {
	document.addediteventform.action = "saveevent.htm";
	document.addediteventform.method = "POST";
	document.addediteventform.submit();
}

function showFundraiserEventLocation(eventID) {
	$("#message label").html("");
	$('body,html').animate({scrollTop: $(".content").attr("scrollHeight")}, 1400);
	openIframePopup('ifrmPopup', 'ifrm','/ScanSeeWeb/retailer/showfundraisereventlocation.htm?eventID=' + eventID, 320, 600, '&nbsp;Fundraiser Location');
}

function checkFundraiserImgValidate(input) {
	var vEventImg = document.getElementById("trgrUpld").value;
	if (vEventImg != '') {
		var checkEventImg = vEventImg.toLowerCase();
		if (!checkEventImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
			alert("You must upload Event image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
			document.addediteventform.eventImageFile.value = "";
			document.getElementById("trgrUpld").focus();
			return false;
		} else {
			/*show progress bar : ETA for Web 1.3*/
			//showProgressBar();/* Commented to fix body scroll disable issue*/
			/*End*/
			
			$.ajaxSetup({
				cache : false
			});
			$("#addeditfundraiserform").ajaxForm({
					success : function(response) {
					$('#loading-image').css("visibility", "hidden");
					var imgRes = response.getElementsByTagName('imageScr')[0].firstChild.nodeValue;
					if (imgRes == 'UploadLogoMaxSize') {
						$('#eventImagePathErr').text("Image Dimension should not exceed Width: 800px Height: 600px");
					} else if (imgRes == 'UploadLogoMinSize') {
						$('#eventImagePathErr').text("Image Dimension should be Minimum Width: 70px Height: 70px");
					} else {
						$('#eventImagePathErr').text("");
						var substr = imgRes.split('|');
						if (substr[0] == 'ValidImageDimention') {
							$('#fundImg').width('70px');
							$('#fundImg').height('70px');
							var imgName = substr[1];
							$('#fundImgPath').val(imgName);
							$('#fundImg').attr("src", substr[2]);
						} else {
							/*commented to fix iframe popup scroll issue
							/$('body').css("overflow-y","hidden");*/
							openIframePopupForImage('ifrmPopup', 'ifrm', '/ScanSeeWeb/retailer/cropImageGeneral.htm', 100, 99.5, 'Crop Image');
						}
					}
				}
			}).submit();
		}
	}
}

function addEditFundraiserCroppedImage(x, y, w, h) {
	top.document.addeditfundraiserform.action = "/ScanSeeWeb/retailer/uploadcroppedaddeditfund.htm?x="
			+ x + "&y=" + y + "&w=" + w + "&h=" + h;// +"&imgValue="+img;
	top.document.addeditfundraiserform.method = "POST";
	top.document.addeditfundraiserform.submit();
}

/*function saveFundraiserDept()	{
	
	var deptName = $("#deptName").val();
	if(deptName.trim().length == 0)	{
		alert("Please enter department name")
	} else	{
		$.ajaxSetup({
			cache : false
		});
		
		$.ajax({
			type : "POST",
			url : "saveFundraiserDept.htm",
			data : {
				'deptName' : deptName
			},
			success : function(response)	{
				if (response === "Department name exist")	{
					alert("Department name exist");
				} else	{
//					$('#department').append('<option value="' + response + '" selected="selected">' + deptName + '</option>');
					closeIframePopup('ifrmPopupDept','ifrmDept');
					//alert(response)
					$(parent.document).find('#department').append('<option value="' + response + '" selected="selected">' + deptName + '</option>');
					var deptVal = $('#ifrmDept').contents().find("#deptName").val();
					$("#department").find('option:last').append('<option value="' + response + '" selected="selected">' + deptName + '</option>');
				}
				
			},
			error : function(e) {
				alert("Error occurred while saving department");
			}
		});
	}
}*/

function deleteFundraiser(eventID) {
	var vDeleteEvent = confirm("Are you sure you want to delete this Fundraiser Event ?")
	if (vDeleteEvent) {
		document.managefundraiserform.eventID.value = eventID;
		document.managefundraiserform.action = "deletefundraiser.htm";
		document.managefundraiserform.method = "POST";
		document.managefundraiserform.submit();
	}
}

function editFundraiser(eventID) {
	document.managefundraiserform.eventID.value = eventID;
	document.managefundraiserform.action = "editFundraiser.htm";
	document.managefundraiserform.method = "POST";
	document.managefundraiserform.submit();
}

function backManageFundraisingEvents() {
	var vManageFundEvent = confirm("Are you sure you want to leave this page without saving the changes!");
	if (vManageFundEvent) {
		document.addeditfundraiserform.action = "managefundraiser.htm";
		document.addeditfundraiserform.method = "POST";
		document.addeditfundraiserform.submit();
	}
}

function cityAutocomplete(postalId){

	$('#City').autocomplete({
		minLength: 4,
		delay : 500,
		source: '/ScanSeeWeb/retailer/displayCityStateZipCode.htm',
		select: function( event, ui ) {
					if(ui.item.value == "No Records Found"){
					$( "#"+postalId ).val("");
					$( "#Country" ).val("");
					$( "#City" ).val("");
					$('#stateHidden').val("");
					$('#stateCodeHidden').val("");
					}else{
					$('#citySelectedFlag').val('selected');
					$( "#"+postalId ).val( ui.item.zip);
					$( "#Country" ).val(ui.item.state);
					$( "#City" ).val( ui.item.city );
					$('#stateHidden').val(ui.item.state);
					$('#stateCodeHidden').val(ui.item.statecode);
					}
					return false;
		}
	});
}

function zipCodeAutocomplete(postalId){
	$("#"+postalId).autocomplete({
		minLength: 3,
		delay : 500,
		source: '/ScanSeeWeb/retailer/displayZipCodeStateCity.htm',
		select: function( event, ui ) {	
					if(ui.item.value == "No Records Found"){
					$( "#"+postalId ).val("");
					$( "#Country" ).val("");
					$( "#City" ).val("");
					$('#stateHidden').val("");
					$('#stateCodeHidden').val("");
					}else{
					$(  "#"+postalId ).val( ui.item.zip);
					$( "#Country" ).val(ui.item.state);
					$( "#City" ).val( ui.item.city );
					$('#stateHidden').val(ui.item.state);
					$('#stateCodeHidden').val(ui.item.statecode);
					}
					return false;
		}
	});
}

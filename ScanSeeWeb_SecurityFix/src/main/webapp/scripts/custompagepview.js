/**
 * 
 */

function showMainPagePview(form, imagePath, retStoreName, retStoreImage) {
	// form.retPageDescription.value
	// form.retPageShortDescription.value;
	// form.retPageLongDescription.value;
	// Fixed Retailer Logo is displaying with Red-X issue in preview.
	if (retStoreImage == '' || retStoreImage == "") {
		retStoreImage = "../images/blankImage.gif";
	}

	if (imagePath == '' || imagePath == "") {
		imagePath = "../images/logoBg.png";
	}
	

	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+
			/*
			 * " $(function() { var actText = $(\"td.genTitle\").text();" +
			 * "$(\"td.genTitle\").attr('title',actText);" + "var limit = 20;" +
			 * "var chars = $(\"td.genTitle\").text();" + "if (chars.length >
			 * limit) {" + "var visibleArea = $(\"<span> \"+ chars.substr(0,
			 * limit-1) +\"</span>\");" + "var dots = $(\"<span
			 * class='dots'>...</span>\");" +
			 * "$(\"td.genTitle\").empty().append(visibleArea).append(dots);" + "}
			 * });" +
			 */
			"</script></head>"
			+ "<body style=\"background:#FFFFFF\">"
						
			+ "<div id=\"iphonePanel\"><div class=\"navBar iphone\">"
			+ "<table width=\"100%\"  cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			+ "<tbody><tr><td width=\"19%\" style=\"border-right: 1px solid rgb(218, 218, 218);\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\" /></td>"
			+ "<td width=\"54%\" class=\"genTitle\" style=\"border-right: 1px solid rgb(218, 218, 218);\">Main Menu Page</td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\" /><br></td></tr>"
			+ "</tbody></table></div>"												
			+"<div class=\"viewArea\">"
			+"<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">"
			+"<tbody><tr>"
				+ "<tr><td colspan=\"4\">"
			+"<h2 class=\"rtlrTitle\">"
			+ form.retPageTitle.value
			+"</h2>"
			+ "<br></td></tr>"
				+ "<tr>"
			+ "<td align=\"center\" colspan=\"4\">"
			+"<div class=\"image\">"
			+"<img height=\"125\" width=\"130\" src=\""
			+ imagePath
			+ "\" alt=\"image\" /></div>"
			+ "<br></td></tr>"			
				+ "<tr><td colspan=\"4\">"		+"<div class=\"shrtDesc\">"
			+ form.retPageShortDescription.value
			+ "</div><br></td></tr>"
			+"<br>"
			+ "<tr> <td colspan=\"4\">"
			+"<div class=\"lngDesc\">"
			+ form.retPageLongDescription.value
			+ "</div></td></tr>"
			+ "</tbody></table></div>" + "</div></body>";
	day = new Date();
	id = day.getTime();
	// eval("page" + id + " = window.open(URL, '" + id + "',
	// 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left
	// = 390,top = 100');");
	pView = window
			.open(
					'',
					'',
					'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left = 390,top = 100');
	pView.document.write(strHtml);
	pView.document.body.onload = function() {

		if ($('td.rowone div').length >= 2) {

			$('.imgarea,.txtarea').css({
				'width' : '50%',
				'word-wrap' : 'break-word'
			});
		} else {
			$('.imgarea,.txtarea').css('width', '100%');
		}
	};
	pView.focus();

}

function showLoadingPagePview(form, landingImagepath, retStoreName,
		retStoreImage) {
	
	// Fixed Retailer Logo is displaying with Red-X issue in preview.
	if (retStoreImage == '' || retStoreImage == "") {
		retStoreImage = "../images/blankImage.gif";
	}
	if (landingImagepath == '' || landingImagepath == "") {
		landingImagepath = "../images/logoBg.png";
	}
	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+ "</script></head>"
			+ "<body style=\"background:#FFFFFF\">"

			+ "<div id=\"iphonePanel\"><div class=\"navBar iphone\">"
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			+ "<tr><td width=\"19%\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\" /></td>"
			+ "<td width=\"54%\" class=\"genTitle\">Landing Page</td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\" /></td></tr>"
			+ "</table></div>"
			+ "<div class=\"cstmBtn\"><img src=\"../images/downloadAppicon.png\" alt=\"download\" width=\"15\" height=\"17\" />Download App</div>"
			+ "<div class=\"infoStrip\">" + "<ul><li><img src=\""
			+ retStoreImage
			+ "\"alt=\"\"/></li>"
			+ "<li>"
			+ retStoreName
			+ "</li></ul></div>"
			+ "<div class=\"viewAreaHlf\"> <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"zeroBrdrTbl\">"
			+ "<tr class=\"noBg\">"
			+ "<td align=\"left\" class=\"rowone\"><div class=\"floatL imgarea\"><img height=\"125\" width=\"130\" src=\""
			+ landingImagepath
			+ "\" alt=\"image\" /></div>"
			+"</td>"
			+ "</tr>"
			+ "<tr>"
			+ "<td  align=\"left\" class=\"rowone\"><span>"
			+ form.retCreatedPageShortDescription.value
			+ "</span></td></tr>"
				+"<br/><br/>"
			+ "<tr><td   align=\"left\" class=\"rowone\"><span>"
			+ form.retCreatedPageLongDescription.value
			+ "</span></td></tr>"
			+ "</table></div>" + "</div></body>";
	
	pView = window
			.open(
					'',
					'',
					'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left = 390,top = 100');
	pView.document.write(strHtml);
	pView.document.body.onload = function() {

		if ($('td.rowone div').length >= 2) {

			$('.imgarea,.txtarea').css({
				'width' : '50%',
				'word-wrap' : 'break-word'
			});
		} else {
			$('.imgarea,.txtarea').css('width', '100%');
		}
	};
	pView.focus();

}

function showSpecialOffPagePview(form, specialImagepath) {

	// form.retPageDescription.value
	// form.retPageShortDescription.value;
	// form.retPageLongDescription.value;

	/*var startDate = "StartDate:";
	var endDate = "EndDate:";

	var audio1 = "audio1";
	var vedio1 = "vedio1";
	var file1 = "file1";
	var vSTime = "";
	var vETime = "";

	if (form.splofferStartTimeHrs.value > 11) {
		vSTime = "PM";
	} else {
		vSTime = "AM";
	}
	if (form.splOfferEndTimeHrs.value > 11) {
		vETime = "PM";
	} else {
		vETime = "AM";
	}

	var x1 = "";
	var x2 = "";
	var x3 = "";
	var files = "";
	if (form.splOfferFile.length >= 1) {
		files = "Files:";
	}

	if (form.splOfferFile.length - 1 == 3) {
		x1 = form.splOfferFile[0];
		x2 = form.splOfferFile[1];
		x3 = form.splOfferFile[2];
		if (x1 != "") {
			x1 = x1.value;
		}
		if (x2 != "") {
			x2 = x2.value;
		}
		if (x3 != "") {
			x3 = x3.value;
		}
	} else if (form.splOfferFile.length - 1 == 2) {
		x1 = form.splOfferFile[0];
		x2 = form.splOfferFile[1];

		if (x1 != "") {
			x1 = x1.value;
		}

		if (x2 != "") {
			x2 = x2.value;
		}
	} else if (form.splOfferFile.length - 1 == 1) {
		x1 = form.splOfferFile[0];
		if (x1 != "") {
			x1 = x1.value;
		}

	}
	// Fixed Retailer Logo is displaying with Red-X issue in preview.
	if (retStoreImage == '' || retStoreImage == "") {
		retStoreImage = "../images/blankImage.gif";
	}*/
	if (specialImagepath == '' || specialImagepath == "") {
		specialImagepath = "../images/logoBg.png";
	}
	
	
	
	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+
			/*
			 * " $(function() { var actText = $(\"td.genTitle\").text();" +
			 * "$(\"td.genTitle\").attr('title',actText);" + "var limit = 20;" +
			 * "var chars = $(\"td.genTitle\").text();" + "if (chars.length >
			 * limit) {" + "var visibleArea = $(\"<span> \"+ chars.substr(0,
			 * limit-1) +\"</span>\");" + "var dots = $(\"<span
			 * class='dots'>...</span>\");" +
			 * "$(\"td.genTitle\").empty().append(visibleArea).append(dots);" + "}
			 * });" +
			 */
			"</script></head>"
			+ "<body style=\"background:#FFFFFF;color:#000;\">"
						
			+ "<div id=\"iphonePanel\"><div class=\"navBar iphone\">"
			+ "<table width=\"100%\"  cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			+ "<tbody><tr><td width=\"19%\" style=\"border-right: 1px solid rgb(218, 218, 218);\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\" /></td>"
			+ "<td width=\"54%\" class=\"genTitle\" style=\"border-right: 1px solid rgb(218, 218, 218);\">Special Offer Page</td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\" /><br></td></tr>"
			+ "</tbody></table></div>"	
											
			+"<div class=\"viewArea\">"
			+"<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">"
			+"<tbody><tr>"
				+ "<tr><td colspan=\"4\">"
			+"<h2 class=\"rtlrTitle\">"
			+ form.splOfferTitle.value
			+"</h2>"
			+ "<br></td></tr>"
				+ "<tr>"
			+ "<td align=\"center\" colspan=\"4\">"
			+"<div class=\"image\">"
			+"<img height=\"70\" width=\"70\" src=\""
			+ specialImagepath
			+ "\" alt=\"image\" /></div>"
			+ "<br></td></tr>"
					+ "<tr><td colspan=\"4\">"
			+"<div class=\"shrtDesc\">"
			+ form.spclOffrshrtDesc.value
			+ "</div><br></td></tr>"
			+"<br>"
			+ "<tr> <td colspan=\"4\">"
			+"<div class=\"lngDesc\">"
			+ form.splOfferLongDescription.value
			+ "</div></td></tr>"
			+ "</tbody></table></div>" + "</div></body>";
	
	
	
	pView = window
			.open(
					'',
					'',
					'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left = 390,top = 100');
	pView.document.write(strHtml);
	pView.document.body.onload = function() {

		if ($('td.rowone div').length >= 2) {

			$('.imgarea,.txtarea').css({
				'width' : '50%',
				'word-wrap' : 'break-word'
			});
		} else {
			$('.imgarea,.txtarea').css('width', '100%');
		}
	};
	pView.focus();

}

// Meyy related code

function previewTitleMainPage(titleDesc, retStoreName, retailStoreAddress,
		retailContactNumber) {
	// alert("Main Page" + titleDesc);
	var imagePath = "";

	if ($("#customPageImg").attr('src'))
		imagePath = $("#customPageImg").attr('src');

	var strHtml = previewTitle(titleDesc, retStoreName, retailStoreAddress,
			retailContactNumber, imagePath);
	document.getElementById("titlePreviewId").innerHTML = strHtml;
}

function previewTitleLandingPage(titleDesc, retStoreName, retailStoreAddress,
		retailContactNumber) {
	// alert("Landing Page" + titleDesc);
	var imagePath = "";

	if ($("#createRetCustomPage").attr('src'))
		imagePath = $("#createRetCustomPage").attr('src');

	var strHtml = previewTitle(titleDesc, retStoreName, retailStoreAddress,
			retailContactNumber, imagePath);
	document.getElementById("titlePreviewIdLandingPage").innerHTML = strHtml;
}

function previewTitleSpecialOffPage(titleDesc, retStoreName,
		retailStoreAddress, retailContactNumber) {

	// alert(titleDesc);
	// alert(titleDesc.value);
	// var tileValue = titleDesc.value;
	// if(isOnLoad || titleDesc != "")
	// {
	var imagePath = "";
	/*
	 * if (imagePathId == '' || imagePathId == "") { imagePath =
	 * "../images/logoBg.png"; }
	 */
	if ($("#customPageImg").attr('src'))
		imagePath = $("#customPageImg").attr('src');
	else
		imagePath = "../images/logoBg.png";

	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+ "</script></head>"
			+ "<body style=\"background:#FFFFFF\">"
			+ "<div id=\"iphonePanel_SpecialPage_Dashboard\"><div class=\"navBar_SpecialPage_Dashboard iphone_Dashboard\">"
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			// + "<tr style=\"height: 45px;\">"

			// + "</tr>"
			+ "<tr class=\"tdStyle\"><td width=\"19%\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\"  class=\"tdStyle\"/></td>"
			+ "<td width=\"54%\"  class=\"tdStyle\"><font style=\"font-size: 14px;font-weight: bold;color: #FFFFFF;text-align: center;line-height: 44px;\"><center>"
			+ retStoreName
			+ "</center></font></td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\"  class=\"tdStyle\"/></td></tr>"
			+ "</table></div>"
			// + "<div class=\"cstmBtn\"><img
			// src=\"../images/downloadAppicon.png\" alt=\"download\"
			// width=\"15\" height=\"17\" />Download App</div>"
			// + "<div class=\"infoStrip\">"
			// + "<ul><li><img src=\""
			// + "retStoreImage"
			// + "\"alt=\"\"/></li>"
			// + "<li>"
			// + "retStoreName"
			// + "</li></ul>"
			// + "</div>"
			+ "<div class=\"viewAreaHlf_Dashboard\"> "
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"zeroBrdrTbl\">"
			+ "<tr>"
			+ "<td bgcolor=\"#424242\" colspan=\"2\" align=\"left\" class=\"rowone\"><span><font style=\"color: #FFFFFF\">Special Offers</font></span></td>"
			+ "</tr>"
			+ "<tr class=\"noBg\">"
			+ "<td align=\"left\" class=\"rowone\"><span><img width=\"50\" height=\"30\"  src=\"../images/productInfo_Icon.png\" alt=\"image\" /> &nbsp; "
			+ titleDesc
			+ "<br> <span style= \"padding-left: 52px\" ></span></span></td>"
			+ "<td align=\"right\" class=\"rowone\"><span><img src=\"../images/linkArrw.png\" alt=\"Link Arrow\" /></span></td>"
			+ "</tr>" + "</table>" + "</div>" + "</div>" + "</body>";

	/*
	 * pView =
	 * window.open('','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left =
	 * 390,top = 100'); pView.document.write(strHtml);
	 * pView.document.body.onload = function() {
	 * 
	 * if($('td.rowone div').length >= 2) {
	 * 
	 * $('.imgarea,.txtarea').css({'width':'50%','word-wrap':'break-word'}); }
	 * else { $('.imgarea,.txtarea').css('width','100%'); } }; pView.focus();
	 */

	// }
	document.getElementById("titlePreviewIdSpecialPage").innerHTML = strHtml;
}

function previewTitle(titleDesc, retStoreName, retailStoreAddress,
		retailContactNumber, imagePath) {
	// alert(titleDesc);
	// alert(titleDesc.value);
	// var tileValue = titleDesc.value;
	// if(isOnLoad || titleDesc != "")
	// {

	if (imagePath == '' || imagePath == "") {
		imagePath = "../images/logoBg.png";
	}

	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+ "</script></head>"
			+ "<body style=\"background:#FFFFFF\">"
			+ "<div id=\"iphonePanel_Dashboard\"><div class=\"navBar_Dashboard iphone_Dashboard\">"
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			// + "<tr style=\"height: 45px;\">"

			// + "</tr>"
			+ "<tr class=\"tdStyle\"><td width=\"19%\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\"  class=\"tdStyle\"/></td>"
			+ "<td width=\"54%\"  class=\"tdStyle\"><font style=\"font-size: 14px;font-weight: bold;color: #FFFFFF;text-align: center;line-height: 44px;\"><center>"
			+ retStoreName
			+ "</center></font></td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\"  class=\"tdStyle\"/></td></tr>"
			+ "</table></div>"
			// + "<div class=\"cstmBtn\"><img
			// src=\"../images/downloadAppicon.png\" alt=\"download\"
			// width=\"15\" height=\"17\" />Download App</div>"
			// + "<div class=\"infoStrip\">"
			// + "<ul><li><img src=\""
			// + "retStoreImage"
			// + "\"alt=\"\"/></li>"
			// + "<li>"
			// + "retStoreName"
			// + "</li></ul>"
			// + "</div>"
			+ "<div class=\"viewAreaHlf_Dashboard\"> "
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"zeroBrdrTbl\">"
			+ "<tr>"
			+ "<td align=\"left\" class=\"rowone\"><span><img src=\"../images/curntSpcls.png\" alt=\"Current Sales\" width=\"50\" height=\"30\"  class=\"tdStyle\"/> No Current Sales <br> <span style= \"padding-left: 52px\" >Notify Retailer Here</span></span></td>"
			+ "<td align=\"right\" class=\"rowone\"><span><img src=\"../images/linkArrw.png\" alt=\"Link Arrow\" /></span></td>"
			+ "</tr>"
			+ "<tr>"
			+ "<td align=\"left\" class=\"rowone\"><span><img src=\"../images/FNB_getdirectIcon.png\" alt=\"Ged Directions\" width=\"50\" height=\"30\"  class=\"tdStyle\"/> Get Directions <br> <span style= \"padding-left: 52px\" >"
			+ retailStoreAddress
			+ "</span></span></td>"
			+ "<td align=\"right\" class=\"rowone\"><span><img src=\"../images/linkArrw.png\" alt=\"Link Arrow\" /></span></td>"
			+ "</tr>"
			+ "<tr>"
			+ "<td align=\"left\" class=\"rowone\"><span><img src=\"../images/phone_icon.png\" alt=\"Call Store\" width=\"50\" height=\"30\"  class=\"tdStyle\"/> Call Store <br> <span style= \"padding-left: 52px\" >"
			+ retailContactNumber
			+ "</span> </span></td>"
			+ "<td align=\"right\" class=\"rowone\"><span><img src=\"../images/linkArrw.png\" alt=\"Link Arrow\" /></span></td>"
			+ "</tr>"
			+ "<tr class=\"noBg\">"
			+ "<td align=\"left\" class=\"rowone\"><span><img width=\"50\" height=\"30\"  src=\""
			+ imagePath
			+ "\" alt=\"image\" /> "
			+ titleDesc
			+ "<br> <span style= \"padding-left: 52px\" ></span></span></td>"
			+ "<td align=\"right\" class=\"rowone\"><span><img src=\"../images/linkArrw.png\" alt=\"Link Arrow\" /></span></td>"
			+ "</tr>" + "</table>" + "</div>" + "</div>" + "</body>";

	/*
	 * pView =
	 * window.open('','','toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left =
	 * 390,top = 100'); pView.document.write(strHtml);
	 * pView.document.body.onload = function() {
	 * 
	 * if($('td.rowone div').length >= 2) {
	 * 
	 * $('.imgarea,.txtarea').css({'width':'50%','word-wrap':'break-word'}); }
	 * else { $('.imgarea,.txtarea').css('width','100%'); } }; pView.focus();
	 */

	// }
	return strHtml;
}

function showMainPagePreview_Dashboard(form, imagePath, retStoreName,
		retStoreImage) {

	// Fixed Retailer Logo is displaying with Red-X issue in preview.
	if (retStoreImage == '' || retStoreImage == "") {
		retStoreImage = "../images/blankImage.gif";
	}

	if (imagePath == '' || imagePath == "") {
		imagePath = "../images/logoBg.png";
	}
	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+
			/*
			 * " $(function() { var actText = $(\"td.genTitle\").text();" +
			 * "$(\"td.genTitle\").attr('title',actText);" + "var limit = 20;" +
			 * "var chars = $(\"td.genTitle\").text();" + "if (chars.length >
			 * limit) {" + "var visibleArea = $(\"<span> \"+ chars.substr(0,
			 * limit-1) +\"</span>\");" + "var dots = $(\"<span
			 * class='dots'>...</span>\");" +
			 * "$(\"td.genTitle\").empty().append(visibleArea).append(dots);" + "}
			 * });" +
			 */
			"</script></head>"
			+ "<body style=\"background:#FFFFFF\">"
			+ "<div id=\"iphonePanel\"><div class=\"navBar iphone\">"
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			+ "<tr><td width=\"19%\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\" class=\"tdStyle\"/></td>"
			+ "<td width=\"54%\"  class=\"tdStyle\"><font style=\"font-size: 14px;font-weight: bold;color: #FFFFFF;text-align: center;line-height: 44px;\"><center> Details </center></font></td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\" class=\"tdStyle\"/></td></tr>"
			+ "</table></div>"
			+ "<div class=\"cstmBtn\"><img src=\"../images/downloadAppicon.png\" alt=\"download\" width=\"15\" height=\"17\" />Download App</div>"
			// + "<div class=\"infoStrip\">"
			// + "<ul><li><img src=\""
			// + retStoreImage
			// + "\"alt=\"\"/></li>"
			/*
			 * + "<li>" + retStoreName + "</li>"
			 */
			// + "</ul></div>"
			+ "<div class=\"viewAreaHlf\"> <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"zeroBrdrTbl\">"
			+ "<tr class=\"noBg\">"
			+ "<td align=\"left\" class=\"rowone\"><div class=\"floatL imgarea\"><img height=\"125\" width=\"130\" src=\""
			+ imagePath
			+ "\" alt=\"image\" /></div><br><br><br>"
			+ "<div style=\"text-align:center;display:block;\" >"
			+ form.retPageDescription.value
			+ "</div></td></tr>"
			+ "<tr><td align=\"left\" class=\"rowone\"><span>"
			+ form.retPageShortDescription.value
			+ "</span></td></tr>"
			+ "<tr><td align=\"left\" class=\"rowone\"><span>"
			+ form.retPageLongDescription.value
			+ "</span></td></tr>"
			+ " </table></div>" + "</div></body>";

	document.getElementById("iphonePreviewSpace").innerHTML = strHtml;

}

function showLandingPagePreview_Dashboard(form, imagePath, retStoreName,
		retStoreImage) {

	// Fixed Retailer Logo is displaying with Red-X issue in preview.
	if (retStoreImage == '' || retStoreImage == "") {
		retStoreImage = "../images/blankImage.gif";
	}

	if (imagePath == '' || imagePath == "") {
		imagePath = "../images/logoBg.png";
	}

	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+
			/*
			 * " $(function() { var actText = $(\"td.genTitle\").text();" +
			 * "$(\"td.genTitle\").attr('title',actText);" + "var limit = 20;" +
			 * "var chars = $(\"td.genTitle\").text();" + "if (chars.length >
			 * limit) {" + "var visibleArea = $(\"<span> \"+ chars.substr(0,
			 * limit-1) +\"</span>\");" + "var dots = $(\"<span
			 * class='dots'>...</span>\");" +
			 * "$(\"td.genTitle\").empty().append(visibleArea).append(dots);" + "}
			 * });" +
			 */
			"</script></head>"
			+ "<body style=\"background:#FFFFFF\">"
			+ "<div id=\"iphonePanel\"><div class=\"navBar iphone\">"
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			+ "<tr><td width=\"19%\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\" class=\"tdStyle\"/></td>"
			+ "<td width=\"54%\"  class=\"tdStyle\"><font style=\"font-size: 14px;font-weight: bold;color: #FFFFFF;text-align: center;line-height: 44px;\"><center> Details </center></font></td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\" class=\"tdStyle\"/></td></tr>"
			+ "</table></div>"
			+ "<div class=\"cstmBtn\"><img src=\"../images/downloadAppicon.png\" alt=\"download\" width=\"15\" height=\"17\" />Download App</div>"
			// + "<div class=\"infoStrip\">"
			// + "<ul><li><img src=\""
			// + retStoreImage
			// + "\"alt=\"\"/></li>"
			/*
			 * + "<li>" + retStoreName + "</li>"
			 */
			// + "</ul></div>"
			+ "<div class=\"viewAreaHlf\"> <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"zeroBrdrTbl\">"
			+ "<tr class=\"noBg\">"
			+ "<td align=\"left\" class=\"rowone\"><div class=\"floatL imgarea\"><img height=\"125\" width=\"130\" src=\""
			+ imagePath
			+ "\" alt=\"image\" /></div><br><br><br>"
			+ "<div style=\"text-align:center;display:block;\" >"
			// + form.retCreatedPageDescription.value
			+ form.retCreatedPageTitle.value
			+ "</div></td></tr>"
			+ "<tr><td align=\"left\" class=\"rowone\"><span>"
			+ form.retCreatedPageShortDescription.value
			+ "</span></td></tr>"
			+ "<tr><td align=\"left\" class=\"rowone\"><span>"
			+ form.rtlrcrtdlngDesc.value
			+ "</span></td></tr>"
			+ " </table></div>" + "</div></body>";

	document.getElementById("iphonePreviewSpace_LandingPage").innerHTML = strHtml;

}

function showSpecialPagePreview_Dashboard(form, imagePath, retStoreName,
		retStoreImage) {
	// Fixed Retailer Logo is displaying with Red-X issue in preview.
	if (retStoreImage == '' || retStoreImage == "") {
		retStoreImage = "../images/blankImage.gif";
	}
	if (imagePath == '' || imagePath == "") {
		imagePath = "../images/logoBg.png";
	}
	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+
			/*
			 * " $(function() { var actText = $(\"td.genTitle\").text();" +
			 * "$(\"td.genTitle\").attr('title',actText);" + "var limit = 20;" +
			 * "var chars = $(\"td.genTitle\").text();" + "if (chars.length >
			 * limit) {" + "var visibleArea = $(\"<span> \"+ chars.substr(0,
			 * limit-1) +\"</span>\");" + "var dots = $(\"<span
			 * class='dots'>...</span>\");" +
			 * "$(\"td.genTitle\").empty().append(visibleArea).append(dots);" + "}
			 * });" +
			 */
			"</script></head>"
			+ "<body style=\"background:#FFFFFF\">"
			+ "<div id=\"iphonePanel\"><div class=\"navBar iphone\">"
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			+ "<tr><td width=\"19%\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\" class=\"tdStyle\"/></td>"
			+ "<td width=\"54%\"  class=\"tdStyle\"><font style=\"font-size: 14px;font-weight: bold;color: #FFFFFF;text-align: center;line-height: 44px;\"><center> Details </center></font></td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\" class=\"tdStyle\"/></td></tr>"
			+ "</table></div>"
			+ "<div class=\"cstmBtn\"><img src=\"../images/downloadAppicon.png\" alt=\"download\" width=\"15\" height=\"17\" />Download App</div>"
			// + "<div class=\"infoStrip\">"
			// + "<ul><li><img src=\""
			// + retStoreImage
			// + "\"alt=\"\"/></li>"
			/*
			 * + "<li>" + retStoreName + "</li>"
			 */
			// + "</ul></div>"
			+ "<div class=\"viewAreaHlf\"> <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"zeroBrdrTbl\">"
			+ "<tr class=\"noBg\">"
			+ "<td align=\"left\" class=\"rowone\"><div class=\"floatL imgarea\"><img height=\"125\" width=\"130\" src=\""
			+ imagePath
			+ "\" alt=\"image\" /></div><br><br><br>"
			+ "<div style=\"text-align:center;display:block;\" >"
			// + form.spclOffrPgDesc.value
			+ form.spclOffrTitle.value
			+ "</div></td></tr>"
			+ "<tr><td align=\"left\" class=\"rowone\"><span>"
			+ form.spclOffrshrtDesc.value
			+ "</span></td></tr>"
			+ "<tr><td align=\"left\" class=\"rowone\"><span>"
			+ form.spclOffrlngDesc.value
			+ "</span><br><br><br></td></tr>"
			+ "<tr><td align=\"left\" class=\"rowone\"><span> Start Date : "
			+ form.SplOffrStrtDT.value
			+ "</span></td></tr>"
			+ "<tr><td align=\"left\" class=\"rowone\"><span> End Date : "
			+ form.SplOffrEndDT.value + "</span></td></tr>";
	+" </table></div>" + "</div></body>"

	document.getElementById("iphonePreviewSpace_SpecialPage").innerHTML = strHtml;

}

function showWelcomePagePreview(imgSrc) {
	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">$(document).ready(function(){$(\"#ribbonAd\").css('border-bottom','1px solid #e3e3e3');});"
			+ "</script></head><body style=\"background:#FFFFFF\"><div id=\"iphonePanel\">"
			+ "<div class=\"viewFull\">"
			+ "<img src=\""
			+ imgSrc
			+ "\" alt=\"RibbonAd\" id=\"ribbonAd\" width=\"320\" height=\"460\" title=\"Ribbon Ad\"/></div>"
			+ "</div></body></html>"
	pView = window
			.open(
					'',
					'',
					'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left = 390,top = 100');
	pView.document.write(strHtml);

}
function showBannerPagePreview(imgSrc) {
	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">$(document).ready(function(){$(\"#ribbonAd\").css('border-bottom','1px solid #e3e3e3');});"
			+ "</script></head><body style=\"background:#FFFFFF\"><div id=\"iphonePanel\">"
			+ "<div class=\"viewFull\">"
			+ "<img src=\""
			+ imgSrc
			+ "\" alt=\"RibbonAd\" id=\"ribbonAd\" width=\"320\" height=\"50\" title=\"Ribbon Ad\"/></div>"
			+ "</div></body></html>"
	pView = window
			.open(
					'',
					'',
					'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left = 390,top = 100');
	pView.document.write(strHtml);

}

function showGiveawayPagePview(form, giveAwayImagepath) {

	
	if (giveAwayImagepath == '' || giveAwayImagepath == "") {
		giveAwayImagepath = "../images/logoBg.png";
	}
	
	
	
	var strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
			+ "<head><title>ScanSee</title>"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Enter\"/>"
			+ "<meta content=\"blendTrans(Duration=0.0)\" http-equiv=\"Page-Exit\"/>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../styles/style.css\"/>"
			+ "<script type=\"text/javascript\" src=\"../scripts/jquery-1.6.2.min.js\"></script>"
			+ "<script src=\"../scripts/global.js\" type=\"text/javascript\"></script>"
			+ "<script type=\"text/javascript\">"
			+
			/*
			 * " $(function() { var actText = $(\"td.genTitle\").text();" +
			 * "$(\"td.genTitle\").attr('title',actText);" + "var limit = 20;" +
			 * "var chars = $(\"td.genTitle\").text();" + "if (chars.length >
			 * limit) {" + "var visibleArea = $(\"<span> \"+ chars.substr(0,
			 * limit-1) +\"</span>\");" + "var dots = $(\"<span
			 * class='dots'>...</span>\");" +
			 * "$(\"td.genTitle\").empty().append(visibleArea).append(dots);" + "}
			 * });" +
			 */
			"</script></head>"
			+ "<body style=\"background:#FFFFFF\">"
						
			+ "<div id=\"iphonePanel\"><div class=\"navBar iphone\">"
			+ "<table width=\"100%\"  cellspacing=\"0\" cellpadding=\"0\" class=\"titleGrd\">"
			+ "<tbody><tr><td width=\"19%\" style=\"border-right: 1px solid rgb(218, 218, 218);\"><img src=\"../images/backBtn.png\" alt=\"back\" width=\"50\" height=\"30\" /></td>"
			+ "<td width=\"54%\" class=\"genTitle\" style=\"border-right: 1px solid rgb(218, 218, 218);\">Giveaway</td>"
			+ "<td width=\"27%\"><img src=\"../images/mainMenuBtn.png\" alt=\"mainmenu\" width=\"78\" height=\"30\" /><br></td></tr>"
			+ "</tbody></table></div>"	
											
			+"<div class=\"viewArea\">"
			+"<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">"
			+"<tbody><tr>"
				+ "<tr><td colspan=\"4\">"
			+"<h2 class=\"rtlrTitle\">"
			+ form.retPageTitle.value
			+"</h2>"
			+ "<br></td></tr>"
				+ "<tr>"
			+ "<td align=\"center\" colspan=\"4\">"
			+"<div class=\"image\">"
			+"<img height=\"125\" width=\"130\" src=\""
			+ giveAwayImagepath
			+ "\" alt=\"image\" /></div>"
			+ "<br></td></tr>"
					+ "<tr><td colspan=\"4\">"
			+"<div class=\"shrtDesc\">"
			+ form.shortDescription.value
			+ "</div><br></td></tr>"
			+"<br>"
			+ "<tr> <td colspan=\"4\">"
			+"<div class=\"lngDesc\">"
			+ form.longDescription.value
			+ "</div></td></tr>"
			+ "</tbody></table></div>" + "</div></body>";
	
	
	
	pView = window
			.open(
					'',
					'',
					'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=420,height=760,left = 390,top = 100');
	pView.document.write(strHtml);
	pView.document.body.onload = function() {

		if ($('td.rowone div').length >= 2) {

			$('.imgarea,.txtarea').css({
				'width' : '50%',
				'word-wrap' : 'break-word'
			});
		} else {
			$('.imgarea,.txtarea').css('width', '100%');
		}
	};
	pView.focus();

}
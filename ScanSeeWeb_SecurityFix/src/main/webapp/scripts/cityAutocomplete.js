function cityAutocomplete(postalId){

$('#City').autocomplete({
		minLength: 4,
		delay : 500,
		source: '/ScanSeeWeb/retailer/displayCityStateZipCode.htm',
		select: function( event, ui ) {
			
					$('#citySelectedFlag').val('selected');
					$( "#"+postalId ).val( ui.item.zip);
					$( "#City" ).val( ui.item.city );
					$('#stateHidden').val(ui.item.state);
					$('#stateCodeHidden').val(ui.item.statecode);
					
					var ddl = document.getElementById('Country');
					var opts = ddl.options.length;
					for (var i=0; i<opts; i++){
						if (ddl.options[i].value == ui.item.statecode){
							ddl.options[i].selected = true;
							break;
						}
					}
					
					
					
					return false;
		}
	});
}
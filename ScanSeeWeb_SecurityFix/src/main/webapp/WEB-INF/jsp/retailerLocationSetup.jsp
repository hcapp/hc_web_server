<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>  
<%@page session="true"%>
<%@ page import="common.pojo.ManageProducts"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script>
$(document)
		.ready(
				function() {
				
					var getRow = $("#locSetUpGrd tr:gt(1)").length;
					if(getRow == 0)
					{
						$(".searchGrd").removeClass("hrzVrtScrollTbl"); 
					}
					else {
						$(".searchGrd").addClass("hrzVrtScrollTbl"); 
					}
					});
					</script>
<div id="bubble_tooltip">
  <div class="bubble_top"><span></span></div>
  <div class="bubble_middle"><span id="bubble_tooltip_content">Content is comming here as you probably can see.Content is comming here as you probably can see.</span></div>
  <div class="bubble_bottom"></div>
</div>

<form:form name="locationform" commandName="locationsetupform" enctype="multipart/form-data">
<div id="wrapper">
 
  <div id="dockPanel">
    <ul id="prgMtr" class="tabs">
     <li> <a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee" target="_blank" rel="About ScanSee">About ScanSee</a> </li>
      <!--<li> <a title="Create Profile" href="Retailer_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->
      <li> <a title="Upload Ads/Logos" href="uploadRetailerLogo.htm" rel="Upload Ads/Logos">Upload Logo</a> </li>
      <li> <a class="tabActive" title="Location Setup" href="addlocation.htm" rel="Location Setup">Location Setup</a> </li>
      <li> <a title="Product Setup" href="#" rel="Product Setup">Product Setup</a> </li>
      <li> <a title="Choose Plan" href="#" rel="Choose Plan">Choose Plan</a> </li>
      <li> <a title="Dashboard" href="/ScanSeeWeb/retailer/dashboard.htm" rel="Dashboard">Dashboard</a> </li>
    </ul>
    <a id="Mid" name="Mid"></a>
    <div class="floatR tglSec" id="tabdPanelDesc">
      <div id="filledGlass"> <img src="../images/Step5.png"/>
        <div id="nextNav"> <a href="../prodMgmt.html"> <img class="NextNav_R" alt="Next" src="../images/nextBtn.png" border="0"/> <span>Next</span></a> </div>
      </div>
    </div>
    <div class="floatL tglSec" id="tabdPanel"> <img height="283" alt="Flow1" src="../images/Flow5_ret.png" width="782"/> </div>
  </div>
  <!-- <div id="togglePnl"><a href="#"> <img src="../images/downBtn.png" alt="down" width="9" height="8" /> Show Pane</a></div> -->
  <div class="clear"></div>
  <div class="topMrgn" id="content">
    <div class="grdSec brdrTop">
      <h2>Choose a File to Upload or manually enter your location(s) information below:</h2>
      <ul class="lstInfo">
        <li>
          <input class="textboxBig" type="file" name="locationSetUpFile"/><form:errors cssStyle="${requestScope.locationuploadFile}" path="locationSetUpFile"/>
        </li>
        <li>
          <input class="btn" type="button" value="Upload File" name="Cancel2" onclick="batchUpdateLocationSetup();"/>
        </li>
        <li>For upload instructions,<a href="Retailer_LocSetup_instructions.html"> click here.</a></li>
        <li>For recurring XML or API setups, please contact <a href="mailto:support@scansee.com?Subject=XML/API%20setup&body=I%20am%20interested%20in%20a%20XML/API%20setup.">support@scansee.com.</a></li>
      </ul>
      <div class="searchGrd grdCont">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tblEffect">
          <tr class="">
            <td width="16%"><a href="#" onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false" onmouseout="hideToolTip()">Store Identification</a><a href="#" onmousemove="showToolTip(event,'Store Identification can either be your store number or other unique identifier.');return false" onmouseout="hideToolTip()"><img alt="helpIcon" src="../images/helpIcon.png" /></a></td>
            <td width="17%">Store Address</td>
            <td width="17%">City</td>
            <td width="20%">State</td>
            <td width="10%">Zip Code</td>
            <td width="20%">Phone Number (Optional)</td>
          </tr>
          <tr>
            <td><input type="text" class="textboxSmaller" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
          </tr>
          <tr>
            <td><input type="text" class="textboxSmaller" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
          </tr>
          <tr>
            <td><input type="text" class="textboxSmaller" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
          </tr>
          <tr>
            <td><input type="text" class="textboxSmaller" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
            <td><input type="text" class="textboxSmall" /></td>
          </tr>
        </table>
        <div class="pagination">
          <p><a href="#" title="BackWard"><span id="backWard"></span></a> <a href="#" title="Previous"><span id="previous"></span></a>
            <label class="paginationText" >Page 1 of 4</label>
            <a href="#" title="Next"><span id="nxt"></span></a><a href="#" title="ForWard"><span id="forWard"></span></a> </p>
        </div>
      </div>
      <div class="navTabSec RtMrgn LtMrgn">
        <div align="right">
          <input class="btn floatL"  type="button" value="+ Add" name="Cancel3" title="Add"/>
          <input class="btn" onclick="javascript:history.back()" type="button" value="Back" name="Cancel2" title="Back"/>
          <input class="btn" onclick="window.location.href='../prodMgmt.html';" type="button" value="Next" name="Cancel" title="Next"/>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  
</div>

</form:form>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ScanSee</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)" />
<link href="styles/style.css" type="text/css" rel="stylesheet" />
<link href="styles/ticker-style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="scripts/global.js" type="text/javascript"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>
</head>
<body>

<div id="verticalNav" class="floatL">
      <ul>
        <li class="mainTab"><img src="images/stats.png" width="16" height="16" /> Dashboard</li>
        <li><a href="adminpage_mfg.html">Insights &amp; Analytics</a></li>
        <li><a href="rebates_mfg_demo.html">Rebates </a>
            <ul>
              <li><a href="rebates_mfg.html">Manage Rebates</a></li>
              <!--<li><a href="rebatesedit_mfg.html"><img src="images/editIcon.png" width="10" height="11" title="edit" /> Edit</a></li>-->
            </ul>
        </li>
        <li><a href="dailyDeals_mfg.html">Hot Deals </a>
          <ul>
            <li><a href="dailyDealsadd_mfg.html"><img src="images/addIcon.png" alt="add" title="add" width="10" height="8" />Add</a></li>
            <li><a href="dailyDealsedit_mfg.html"><img src="images/editIcon.png" width="10" height="11" title="edit" />Edit</a></li>
          </ul>
        </li>
<!--         <li><a href="bannerAdview_mfg.html">Ads </a>
          <ul>
            <li><a href="bannerAdcreate_mfg.html"><img src="images/addIcon.png" alt="add" title="add" width="10" height="8" /> Add</a></li>
            <li><a href="bannerAdviewEdit_mfg.html"><img src="images/editIcon.png" width="10" height="11" title="edit" /> Edit</a></li>
          </ul>
        </li>-->
        <li><a href="products_mfg.html">Manage Products</a></li>
        <li><a href="profile_mfg.html">Update Profile</a></li>
        <li><a href="account_mfg.html">Account</a></li>
      </ul>
    </div>
		<form:form name="editprofileform" commandName="editprofileform">
		<div id="secInfo">Rebates </div>
		    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
		      <tr>
		        <td colspan="2" class="header">Search Rebates</td>
		      </tr>
		      <tr>
		        <td class="Label" width="18%">Product</td>
		        <td><input type="text" name="textfield" id="textfield" />
		          <a href="#"><img src="images/searchIcon.png" alt="Search" title="Search"  width="25" height="24"/></a></td>
		      </tr>
		    </table>
	    	<div class="searchGrd">
	      		<h1 class="searchHeaderExpand"><a href="#" class="floatR">&nbsp;</a>Current Rebates</h1>
	      			<div class="grdCont">
		        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="stripeMe" id="rebateLst">
		          <tr class="header">
		            <td width="6%">SNO</td>
		            <td width="25%">Rebates Name</td>
		            <td width="17%">Rebate Amount</td>
		            <td width="17%">Rebate Start Date</td>
		            <td width="17%">Rebate End Date</td>
		            <td width="18%">Rebate Description</td>
		          </tr>
		        </table>
		     </div>
		    </div>
		</form:form>
</body>
</html>

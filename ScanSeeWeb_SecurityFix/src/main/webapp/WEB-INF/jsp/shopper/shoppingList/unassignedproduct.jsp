<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body>
<form:form name="shoppingList" commandName="ShoppingList">

<div id="content" class="topMrgn">

			<div class="navBar">
				<ul>
				<li><a href="#"><img src="../images/backBtn.png" onclick="searchShoppingListBack()" /></a>
				</li>
					<li class="titletxt">Unassigned Product</li>
				</ul>
			</div>
			
	<div class="clear"></div>
    <div class="mobCont noBg" >
	  <div class="splitDsply floatR" id = "shoppingView">
	<div class="clear">
	</div>
	 <div >
          <div>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" id="slctItems">
		  <tr>
		  	<td width="20%">
		  		<span>Product Name</span>
		  	</td>
		  	<td>
		  		<input type="text" id="searchValue" name="searchValue"  value="${requestScope.searchValue}"/>
		  	</td>
		  </tr>
		  <tr>
		  	<td width="20%">
		  		<span>Product Description</span>
		  	</td>
		  	<td>
		  		<input type="text" id="prodDesc" name="prodDesc" />
		  	</td>
		  </tr>
		  <tr>
		  <td colspan="2">
		  		<input type="button" class="mobBtn" value="Add" onclick="AddToList()" title="Add"/>
		  	</td>
		  </tr>
           </table>
           <input type="hidden" name="productName" id="productName" value="" />
		   <input type="hidden" name="productDescription" id="productDescription" value="" />
		   <input type="hidden" name="addedTo" id="addedTo" value="${requestScope.addedTo}" />
		    <form:hidden path="back" id="back" value="" />
	</div>
	</div>
	</div>
<%@include file="../leftmenu.jsp"%>
<div class="clear"></div>
</div>
<div class="tabBar" />
</div>
</form:form>
</body>
</html>
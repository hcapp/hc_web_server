<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
window.onload = function() {
		favLoad('${fn:escapeXml(sladdprodtlexists)}');
	}
</script>

</head>
<body>
<form:form name="shoppingList" commandName="ShoppingListRequest">
<div id="content" class="topMrgn" style="min-height: 34px;">
    <div class="navBar">
      <ul>
        <li><a href="shoppingList.htm"><img src="../images/backBtn.png" />
						</a></li>
        <li class="titletxt">Favorites</li>
      </ul>
    </div>
    <div class="clear"></div>
    <div class="mobCont noBg">
      <div class="splitDsply floatR">
        <table class="brdrLsTbl topSubBar" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody><tr>
          <td>
           
			<input type="text" name = "searchValue" id = "searchValue" class="textboxSmall focustxt"  onkeypress="return searchShoppingList(event)"  /></td>
            <td width="24%">
			<input type="button" class="mobBtn" value="Search" onclick="searchShoppingList('')"  title="Search"/></td>
            <td style="border-right: 1px solid rgb(218, 218, 218);" width="28%">Add selected itmes to:</td>
            <td style="border-right: 1px solid rgb(218, 218, 218);" width="22%"><select name="select" class="textboxSmall" id="addItemsTo">
              <option value="Added to List">Add to List</option>
            </select></td>
            <td width="59%" onclick="addRemoveFavSLProducts();">
            <a href="#"><img src="../images/btn_chked.png" alt="unchecked" title="Add" ="24"="" class="addItemsImg" height="24"/></a> </td>
          </tr>
        </tbody></table>
        <div class="clear">
        </div>
      <input type="hidden" name="userId" id="userId" value="<c:out value='${sessionScope.loginuser.userID}'/>" ></input>
		<div class="fluidView" id = "favDiv">
          <div style="height: 408px; overflow-x: hidden;" class="stretch">
          	<input type="hidden" name="fromSLFAV" id="fromSLFAV" value="SLFAV" />
			<input type="hidden" name="productId" id="productId" value="" />
			<input type="hidden" name="addedTo" id="addedTo" value="F" />
			<input type="hidden" name="fav" id="fav" value="true" />
            <table class="gnrlGrd mobGrd htlt" id="hstryItems" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
		
			 <c:forEach items="${requestScope.shoppingListDetails.categoryInfolst}" var="favCategoryInfolst">

              <tr style="cursor: pointer;" class="">
                <td style="cursor: default;" colspan="4" class="mobSubTitle"> <c:out value ="${favCategoryInfolst.parentCategoryName}"/></td>
              </tr>
			   <c:forEach items="${favCategoryInfolst.productDetails}" var="favListProductDetails">
              <tr style="cursor: pointer;" class="">
                <td style="border-right: 1px solid rgb(218, 218, 218);" width="5%" align="center">
                <input type="checkbox" value="${favListProductDetails.userProductId}" id="favCheck" name="favCheck"/></td>
                <td style="border-right: 1px solid rgb(218, 218, 218);" class="wrpWord" onclick="callproductSummarySC(${favListProductDetails.productId})">
                <c:out value ="${favListProductDetails.productName}"/></td>
                <td style="border-right: 1px solid rgb(218, 218, 218);" width="15%">&nbsp;</td>
                <td width="6%" onclick="deleteProductMainShopListFav(${favListProductDetails.userProductId},${sessionScope.loginuser.userID})">
                <img style="display: none;" src="../images/deleteProdRow.png" alt="delete" name="DeleteRow" title="delete" height="29" width="29"/></td>
              </tr>
			  </c:forEach>
            
			  </c:forEach>
            </tbody>
            </table>
          </div>
          </div>
      </div>
       <%@include file="../leftmenu.jsp"%>
      <div class="clear">
	  </div>
    </div>
    <div class="tabBar">
    	<ul id="sLTab">
        	<li><a href="shoppingList.htm"><img src="../images/tab_btn_up_shpngLst.png" alt="Favorites" class="active" height="50" width="80"/></a></li>
          <li><a href="retrieveUserNotes.htm"><img src="../images/tab_btn_up_notes.png" alt="Notes"/></a></li>
          <li><a href="#"><img src="../images/tab_btn_up_history.png" alt="History" onclick="onSubmitSLHistory();"/></a></li>
            <li><a href="coupongallery.htm"><img src="../images/tab_btn_up_cpnGlry.png" alt="Coupon Gallery"/></a></li>
      </ul>
    </div>
  </div>
    </form:form>
    </body>
    </html>
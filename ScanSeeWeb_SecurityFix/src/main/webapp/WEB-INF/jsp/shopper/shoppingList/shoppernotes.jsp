<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
function clearNote() {
           
            var r = confirm("Note: Deleting All Notes...");
        	if (r == true) {
        		$('#notes').val("");
        	}
         }
function clearData(){
	$('#notes').text("");
}
</script>
<form:form name="shoppernotes" commandName="shoppernotes">
 <div id="content" class="topMrgn">
    <div class="navBar">
    	<ul>
        	<li><a href="#"><img src="../images/backBtn.png" onclick="javascript:history.go(-1);" /></a></li>
            <li class="titletxt">User Notes</li>
          <!--<li class="floatR mrgnRt"><a href="index.html"><img src="images/logoutBtn.png" alt="logout" /></a></li>-->
      </ul>
    </div>
    <div class="clear"></div>
    <div class="mobCont noBg">
<div class="splitDsply floatR">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
    <tr>
      <td width="5%" align="center"><img src="../images/notepad-write.png" alt="Notes" width="16" height="16" /></td>
      <td width="78%"><strong><a href="#"> Add Notes</a></strong></td>
      <td width="4%" ><a href="#" ><img src="../images/notepadDel.png" alt="Clear Notes" width="16" height="16" /></a></td>
      <td width="13%" onclick="clearNote();"><strong><a href="#">Clear Notes</a></strong></td>
    </tr>
  </table>
  <div class="clear"></div>
  <div class="fluidView note">
    <div class="noteBg textView">
	<ul id="edit">
	<c:choose>
      <c:when test="${requestScope.userNotesDetails.userNotes ne null && !empty requestScope.userNotesDetails.userNotes}">
		<li>
		  <textarea name="notes" id="notes" class="transTextArea" onclick="toggleConfirm();"><c:out value="${requestScope.userNotesDetails.userNotes}"/></textarea>
		</li>
		</c:when>
      <c:otherwise>
		<li>
			<textarea name="notes" id="notes" class="transTextArea" onclick="clearData();toggleConfirm();">Enter your notes...</textarea>
		</li>
		</c:otherwise>
		</c:choose>
    </ul>

      <input type="hidden" name="userNotes" id="userNotes" value="" />
    </div>
  </div>
</div>
		<%@include file="../leftmenu.jsp"%>
    </div>
    
    <div class="tabBar">
  
        <p><img src="../images/saveBtn.png" alt="save"  style="cursor:pointer;" width="53" height="30"  align="right" class="MrgnTop RtMrgn" title="Save Notes" onclick="saveUserNotes()"/></p>

 </div>
  </div>
  </form:form>
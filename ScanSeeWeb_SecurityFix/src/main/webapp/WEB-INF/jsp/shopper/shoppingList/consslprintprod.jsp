<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html>
<body>
<script type="text/javascript">
window.onload = function() {
window.print();
window.close();
}
</script>
<div id="wrapper">
 <div id="header">
    
    <div class="clear"></div>
 </div>
  <div class="clear"></div>
	<div id="content" class="shdwBg">
			<div class="">
				
				<div class="grdSec brdrTop">
					
<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl"  align="center" style="font-family:verdana; color:purple;">
						<!--<tr>
						<td colspan="4" class="header" width="100"><span>
						<TH><FONT COLOR=GREEN FACE="Geneva, Arial" SIZE=6>SHOPPING LIST PRODUCTS</FONT></TH> 
						</span></td></tr>-->
						<tr>
							<td colspan="4" class="header" align="center">
								<span style="color:green; font-family:Geneva, Arial; font-size:18px;">SHOPPING LIST PRODUCTS</span>
							</td>
						</tr>
						<tr class="sub-hdr" style="font-weight:bold;">
							<td width="25%"  align="center" bgcolor="yellow">Product Name</td>
							<td width="20%"  align="center" bgcolor="yellow">Product Image</td>
							<td width="25%"  align="center" bgcolor="yellow">Product Description</td>
								<td width="15%"  align="center" bgcolor="yellow">Discount</td>
								<td width="15%"  align="center" bgcolor="yellow"></td>
							
							
						</tr>

								<c:choose>
									<c:when
										test="${sessionScope.shopplistprods.cartProducts ne null}">

										<c:forEach
											items="${shopplistprods.cartProducts.categoryDetails}"
											var="shopCatName">
<tr>
<td align="left"><span><b>${shopCatName.parentCategoryName}</b></span></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										</tr>
										

											<c:forEach items="${shopCatName.productDetails}"
												var="shopProd">

<tr>
							<td align="left"><c:out value="${shopProd.productName}"/></td>
							<td class="imgDsply" align="center"><img
														src="${shopProd.productImagePath }"
														onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
														alt="product1" width="70" height="70"
														 /></td>
										
														
							<td align="left"><c:out value="${shopProd.productShortDescription}"/></td>
							
						
							<td  align="center"><c:if test="${shopProd.CLRFlag eq 1}">

															<img src="/ScanSeeWeb/images/consumer/cpntabIcon.png"
																alt="coupon" width="30" height="30" />
														</c:if>
													</td>
													<td  align="center"><c:if test="${shopProd.favProductFlag eq 1}">

															<img src="/ScanSeeWeb/images/consumer/favtabIcon.png"
																alt="coupon" width="30" height="30" />
														</c:if>
													</td> 
						</tr>		


											

											</c:forEach>


										</c:forEach>

									</c:when>
									<c:otherwise>
										<tr>
											<td class="zeroPdng"><img
												src="/ScanSeeWeb/images/consumer/noneAvail_medium.png"
												alt="common_noneAvail" width="611" height="373"  />
											</td>
										</tr>
									</c:otherwise>
								</c:choose>
							</table>
						</div>
					
					
				</div>
			</div>
	</div>
</div>
<div class="clear"></div>
</body>


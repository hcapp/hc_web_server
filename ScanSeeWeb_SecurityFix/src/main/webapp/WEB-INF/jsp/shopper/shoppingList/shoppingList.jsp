
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form:form name="shoppingList" commandName="ShoppingListRequest">

<div id="content" class="topMrgn">

			<div class="navBar">
				<ul>
					<li class="titletxt">Shopping List</li>
				</ul>
			</div>
			
	<div class="clear"></div>
    <div class="mobCont noBg" >
     
	  
	  <div class="splitDsply floatR" id = "shoppingView"  >
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
          <tr>
         <!--<td width="13%"><input type="button" class="mobBtn" value="Scan" title="Scan"/></td> -->
        	
            <td width="22%">
            <input type="hidden" name="userId" id="userId" value="<c:out value='${sessionScope.loginuser.userID}'/>" ></input>
			<input  name = "searchValue" id = "searchValue" class="textboxSmall" onkeypress="return searchShoppingList(event)"  /></td>
			<td width="24%">
			<input type="button" class="mobBtn" value="Search" onclick="return searchShoppingList('')" title="Search"/></td>
            <td width="4%">
					
			<img src="../images/cart.png" title="List Items" alt="cart list" width="16" height="16" /></td>
            <td width="15%">
			
			<c:set var="listTotal" value="${0}" />
			<c:set var="cartTotal" value="${0}" />
			 <c:forEach items="${requestScope.addRemoveSBProducts.cartProducts.categoryDetails}" var="listDetails">
			<c:set var="listTotal" value="${listTotal + fn:length(listDetails.productDetails)}" />
			</c:forEach>

			 <c:forEach items="${requestScope.addRemoveSBProducts.basketProducts.categoryDetails}" var="cartDetails">
			<c:set var="cartTotal" value="${cartTotal  + fn:length(cartDetails.productDetails)}" />
			</c:forEach>

			<span class="cntItems"> 
			${listTotal} </span> Items in List
			</td>
            <td width="5%">
			<img src="../images/cartAdd.png" title="Cart Items" alt="cart added" width="16" height="16" /></td>
            <td width="17%"><span class="cntCart"> ${cartTotal} </span> Items in Cart</td>
          </tr>
           
        </table>
	<div class="clear">
	</div>
	 <div class="fluidView">
          <div>
          <input type="hidden" name="productId" id="productId" value="" />
			  <input type="hidden" name="addedTo" id="addedTo" value="L" />
			  <input type="hidden" name="fromSL" id="fromSL" value="SLMain" />
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd mobGrd htlt" id="slctItems">
            <col width="13%"/>
 			<col width="49%"/>
            <col width="17%"/>
            <col width="7%"/>
             <col width="14%"/>
			 
			 <c:forEach items="${requestScope.addRemoveSBProducts.cartProducts.categoryDetails}" var="listDetails">
              <tr name="${listDetails.parentCategoryName}"  class="noHvr">
                <td colspan="5" class="mobSubTitle"><c:out value ="${listDetails.parentCategoryName}"/></td>
              </tr>
            
			  <c:forEach items="${listDetails.productDetails}" var="listproductDetails">
			    <tr name="${listDetails.parentCategoryName}">
                <td width="13%" >
                <img src="../images/cartAdd.png" alt="add" width="34" height="25" name="tglImgs" onclick="addRemoveSBProducts('',${listproductDetails.userProductId},${sessionScope.loginuser.userID})" />
                </td>
                <td width="49%" class="wrpWord" onclick="callproductSummarySC(${listproductDetails.productId})" ><c:out value ="${listproductDetails.productName}"/></td>
                <td width="17%">
	                <c:if test="${listproductDetails.productImagePath == 'NotApplicable'}" >
							<img src="../images/imgIcon.png" alt="Img" width="50" height="50" />
							</c:if>
							<c:if test="${listproductDetails.productImagePath != 'NotApplicable'}" >
							<img src="${listproductDetails.productImagePath}" width="50" height="50" />
					</c:if>
                </td>
                <td width="7%"><ul class="clr">
                  <li><img src="../images/C-${listproductDetails.coupon_Status}.png" alt="c" width="14" height="14" /></li>
                  <li><img src="../images/L-${listproductDetails.loyalty_Status}.png" alt="c" width="14" height="14" /></li>
                  <li><img src="../images/R-${listproductDetails.rebate_Status}.png" alt="c" width="14" height="14" /></li>
                </ul>
				</td>
                <td width="14%">
				<img src="../images/deleteProdRow.png" alt="delete" name="DeleteRow" width="29" height="29" title="delete" onclick="addRemoveTodaySLProducts(${listproductDetails.userProductId},${sessionScope.loginuser.userID})"/></td>
				  </tr>
				</c:forEach>
            
			  </c:forEach>
		     </table>
           </div>
          <div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd mobGrd htlt" id="cartItems">
                   <col width="13%"/>
 					<col width="49%"/>
            		<col width="17%"/>
            		<col width="7%"/>
       		  <col width="14%"/>
 <c:forEach items="${requestScope.addRemoveSBProducts.basketProducts.categoryDetails}" var="cartDetails">
			   <tr name="${cartDetails.parentCategoryName}" class="noHvr">
                <td colspan="5"  class="mobSubTitle"><c:out value ="${cartDetails.parentCategoryName}"/>
				</td>
              </tr>
            
<c:forEach items="${cartDetails.productDetails}" var="cartproductDetails">
  <tr name="${cartDetails.parentCategoryName}">
	<td width="15%" >
	<img src="../images/cartDelete.png" alt="add" width="34" height="25" name="tglImgs" onclick="addRemoveSBProducts(${cartproductDetails.userProductId},' ',${sessionScope.loginuser.userID})"/></td>
                <td class="wrpWord" onclick="callproductSummarySC(${cartproductDetails.productId})"><c:out value ="${cartproductDetails.productName}"/></td>
                <td>
	                <c:if test="${cartproductDetails.productImagePath == 'NotApplicable'}" >
						<img src="../images/imgIcon.png" alt="Img" width="50" height="50" />
						</c:if>
						<c:if test="${cartproductDetails.productImagePath != 'NotApplicable'}" >
						<img src="${cartproductDetails.productImagePath}" alt="" width="50" height="50" />
					</c:if>
                </td>
                <td width="7%"><ul class="clr">
                    <li><img src="../images/C-${cartproductDetails.coupon_Status}.png" alt="c" width="14" height="14"/></li>
                  <li><img src="../images/L-${cartproductDetails.loyalty_Status}.png" alt="c" width="14" height="14"/></li>
                  <li><img src="../images/R-${cartproductDetails.rebate_Status}.png" alt="c" width="14" height="14"/></li>
                </ul>
				</td>
                <td>
				&nbsp;
				</td>
				  </tr>
			</c:forEach>
            

  </c:forEach>
			 </table>
			 <c:if test="${requestScope.addRemoveSBProducts.basketProducts.categoryDetails ne null && !empty requestScope.addRemoveSBProducts.basketProducts.categoryDetails}">
		     <ul id="btnBar">
            <li>
              <input type="button" class="mobBtn" value="Check Out" onclick="moveToHstry()" title="Remove Items from Cart" /> <img src="../images/cart-remove.png" alt="Remove Cart Items"/>
            </li>
          </ul>
		  </c:if>
	</div>

	</div>
	</div>
<%@include file="../leftmenu.jsp"%>
<div class="clear"></div>
</div>

<div class="tabBar">
    	<ul id="sLTab">
        	<li>
        	<a href="favCategoryProducts.htm">
        	<img src="../images/tab_btn_up_fav.png" alt="Favorites" width="80" height="50" /></a>
        	</li>
          <li>
          <a href="retrieveUserNotes.htm"><img src="../images/tab_btn_up_notes.png" alt="Notes" /></a></li>
          <li><a href="#"><img src="../images/tab_btn_up_history.png" alt="History" onclick="onSubmitSLHistory();"/></a></li>
          <li><a href="coupongallery.htm"><img src="../images/tab_btn_up_cpnGlry.png" alt="Coupon Gallery" /></a></li>
      </ul>
    </div>
</div>

 
</form:form>

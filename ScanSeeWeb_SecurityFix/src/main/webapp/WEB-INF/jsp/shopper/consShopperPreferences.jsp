<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<body onload="resizeDoc();" onresize="resizeDoc();">
<script type="text/javascript">	
		window.onload = function()
		{
			displayCategory(); 
		}
		function displayCategory()
		{
			 $('#preferedCatgry_view').html("${sessionScope.preferenceCategories}");
			 var cat = ("${sessionScope.expandCategory}");
			 var mainCat = cat.split(",");
			if($('.preferencesLst tbody tr').hasClass('mainCtgry')) {
				$('.preferencesLst tbody tr:not(.mainCtgry)').hide();	 
			}
			for(var i = 0;i<mainCat.length;i++)
			{
				$('.preferencesLst tr[name="'+mainCat[i]+'"]').show();
				$('.preferencesLst tr[name="'+mainCat[i]+',"]').show();
			}
			 
		}
	</script>
<form:form name="editdealsform" commandName="preferencesform">
<div id="contWrpr">
    <div class="block mrgnBtm">
      <h3 class="zeromrgnBtm">Here we capture your preferences.</h3>
      <p>ScanSee will never send you a hot-deal on something that does not fit your lifestyle!</p>
    </div>
    <div class="breadCrumb">
      <ul>
        <li class="brcIcon"><img src="../images/consumer/find_prefIcon.png" alt="find" /></li>
        <li class="leftPdng"><strong>Preferences</strong></li>
      </ul>
      <span class="rtCrnr">&nbsp;</span> </div>
    <div class="contBlks" id="">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="brdrLsTbl topMrgn">
        <tbody>
          <tr>
            <td colspan="4" class="cmnTitle">Alerts Preference?</td>
          </tr>
          <tr>
            <td width="9%" >Email:</td>
            <td width="41%" ><label>
              <form:checkbox path="email"/>
              </label></td>
            <td width="9%" >Cell Phone:</td>
            <td width="41%"><label>
              <form:checkbox path="cellPhone" />
              </label></td>
          </tr>
          <tr>
            <td colspan="4" class="cmnTitle">What type of things are you interested in receiveing deals on?</td>
          </tr>
        </tbody>
      </table>
      <div class="sctmScroll_small brdrTop">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd preferencesLst listCntrl" id="preferedCatgry_view">
          <col width="4%">
          <col width="4%">
          <col width="92%">
          <thead>
            <tr class="subHdr">
              <th align="center" width="4%"><img src="../images/prefIconSml.png" alt="Preferences" width="14" height="13" /></th>
              <th colspan="2" align="left" width="96%">Select Your Preferences</th>
            </tr>
          </thead>
          <tbody>			
			<c:forEach items="${sessionScope.categoryDetailresponse.mainCategoryDetaillst}" var="item">
			
            <tr name="${item.mainCategoryName}" class="mainCtgry">
              <td align="center" width="4%" class="wrpWord"><input type="checkbox"/></td>
              <td colspan="2"  class="wrpWord" width="96%"><c:out value ="${item.mainCategoryName}"/></td>
            </tr>
            <!--   -->
            <c:forEach items="${item.subCategoryDetailLst}" var="item1">
              <tr name="${item.mainCategoryName}">
                <td align="center" width="4%" class="wrpWord">&nbsp;</td>
                <td width="4%" class="wrpWord">
				<c:choose>				
				<c:when test="${item1.displayed == 0}">
				<form:checkbox path="favCatId" value="${item1.categoryId}"/>
				</c:when>
				<c:otherwise>
				<form:checkbox path="favCatId" checked = "checked" value="${item1.categoryId}"/>
				</c:otherwise>
				</c:choose>				
				</td>
                <td width="92%" class="wrpWord">
				 <c:out value ="${item1.subCategoryName}"/>				
				</td>
              </tr>
            </c:forEach>
          	</c:forEach>              
            </tbody>
        </table>
      </div>
      <div class="btnStrip" align="right">
        <input class="btn btn-success " name="Cancel3" value="Next" type="button" onclick="preferencesSubmit()" title="Next"/>
      </div>
    </div>
  </div>
<div class="clear"></div>
</form:form>
</body>
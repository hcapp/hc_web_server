<div class="mobLink floatL">
	<ul id="mainMenu">
		<li><a href="thislocation.htm" class="tl" title="This Location">This
				Location</a>
		</li>
	<li><a href="findDisplay.htm" class="find" 
			title="Find">Find</a>
		</li>
	<li><a href="scannow.htm" class="sn" title="Scan Now">Scan
				Now</a>
		</li>

		<li><a href="shoppingList.htm" class="sl" title="Shopping List">Shopping
				List</a>
		</li>
		<li><a href="hotdealprodlist.htm" class="hd" title="Hot Deals">Hot
				Deals</a>
		</li>
		<li><a href="wishlistdisplay.htm" class="wl" title="Wish List">Wish
				List</a>
		</li>
		<li><a href="coupongallery.htm" class="Glry" title="Gallery">My
				Coupons</a>
		</li>
		<!--<li><a href="#" class="" title="Tutorial" id="tutorial">Tutorial</a>-->
		<!--</li>-->
		<li><a href="preferencesmain.htm" class="Pref"
			title="Preferences">Preferences</a>
		</li>


		<li style="visibility: hidden"></li>
		<li style="visibility: hidden"></li>
		<li style="visibility: hidden"></li>
		<li style="visibility: hidden"></li>
	</ul>
	<ul id="mainMenuTutorial">
		<li videoSrc="../video/This_Location_Button_7_avi-_camrec.mp4"><a
			href="#" class="tl">This location <img
				src="../images/btn_down_video.png" alt="video" /> </a>
		</li>
		<li videoSrc="../video/Video_Button-Find_030512.mp4"><a href="#"
			class="find">Find <img src="../images/btn_down_video.png"
				alt="video" /> </a>
		</li>
		<li videoSrc="../video/Video_ScanNow_031212.mp4"><a href="#"
			class="sn">Scan Now <img src="../images/btn_down_video.png"
				alt="video" /> </a>
		</li>
		<li videoSrc="../video/Video_ Buttons_ Under_ Construction.mp4"><a
			href="#" class="sl">Shopping List <img
				src="../images/btn_down_video.png" alt="video" /> </a>
		</li>
		<li videoSrc="../video/Hot_Deals_030512-e_Without_Echo.mp4"><a
			href="#" class="hd">Hot Deals <img
				src="../images/btn_down_video.png" alt="video" /> </a>
		</li>
		<li videoSrc="../video/Video_ Buttons_ Under_ Construction.mp4"><a
			href="#" class="wl">Wish List <img
				src="../images/btn_down_video.png" alt="video" /> </a>
		</li>
		<li videoSrc="../video/Video_ Buttons_ Under_ Construction.mp4"><a
			href="#" class="Glry">Gallery <img
				src="../images/btn_down_video.png" alt="video" /> </a>
		</li>
		<li videoSrc="../video/Video_ Buttons_ Under_ Construction.mp4"><a
			href="#" class="ma">My Account <img
				src="../images/btn_down_video.png" alt="video" /> </a>
		</li>
		<li><a href="#" class="" id="tglMM">Main Menu</a>
		</li>
	</ul>
</div>
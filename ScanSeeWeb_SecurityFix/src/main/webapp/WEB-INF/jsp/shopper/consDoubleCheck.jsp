<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/autocomplete.js"></script>
<script>
$(document).ready(function() {
	$("#City").live("keydown",function(e)
			{
				var s = String.fromCharCode(e.which);
				if (s.match(/[a-zA-Z0-9\.]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#pstlCd').val("");
				   cityAutocomplete('pstlCd');
				}else if(s.match(/[\b]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#pstlCd').val("");
					cityAutocomplete('pstlCd');
				}
							
			});
var stateCode = '';
var state = '';
if($('#pstlCd').val().length == 5){
	stateCode = $('#stateCodeHidden').val();
	state = $('#stateHidden').val();
	$( "#Country" ).val(state);		 
	}else{
	$('#stateCodeHidden').val("");
	$('#stateHidden').val("");
	$( "#Country" ).val("");
	}
});
</script>
<script type="text/javascript">
	
	function getCityTrigger(val) {	
		if(val == ""){
			$( "#Country" ).val("");
			$( "#pstlCd" ).val( "" );
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		document.createshopperprofileform.cityHidden.value = val;
		document.createshopperprofileform.city.value = val;
	}
	function clearOnCityChange() {	
		//alert($('#citySelectedFlag').val())
		if($('#citySelectedFlag').val() != 'selected'){
		$( "#Country" ).val("");
		$( "#pstlCd" ).val( "" );
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
		}else{
		$('#citySelectedFlag').val('');
		}
}
	function isEmpty(vNum) {
		if(vNum.length < 5){
			//$( "#state" ).val("");
			$( "#Country" ).val("");
			$( "#City" ).val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		return true;
	}
	</script>
<script language="JavaScript">

function callShop() {
	var r = confirm("Do you want to save changes?");
	if(r == true){
		document.doublecheckinfoform.state.value = document.doublecheckinfoform.stateCodeHidden.value;
		document.doublecheckinfoform.universityHidden.value = $('#College').val();
		document.doublecheckinfoform.action = "shopperdashboard.htm";
		document.doublecheckinfoform.method = "POST";
		document.doublecheckinfoform.submit();
	}
}

function loadCity() {
	var stateCode = $('#Country').val();
	$.ajaxSetup({cache:false})
	$.ajax({
		type : "GET",
		url : "shopperfetchcity.htm",
		data : {
			'statecode' : stateCode,
			'city' : null
		},

		success : function(response) {
			$('#myAjax').html(response);
		},
		error : function(e) {
			
		}
	});
}
var ajxRes;
function loadUniversityDisplay() {
	ajxRes=document.doublecheckinfoform.innerText.value;
	$('#myAjax').html(ajxRes);
}

	function loadCollege() {
		var stateCode = $('#Country1').val();
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/shopper/shopperfetccollege.htm",
			data : {
				'statecode' : stateCode,
				'college' : null
			},

			success : function(response) {
				$('#myAjax1').html(response);
			},
			error : function(e) {

			}
		});
	}

	function setCollege() {
			var stateCode = $('#Country1').val();
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/shopper/shopperfetccollege.htm",
			data : {
				'statecode' : stateCode,
				'college' : 'setFlag'
			},

			success : function(response) {
							
				$('#myAjax1').html(response);
			},
			error : function(e) {

			}
		});
	}

	
	function onCollegeSelLoad() {
		var vStateID = '${sessionScope.universityId}';
		var sel = document.getElementById("College");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vStateID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}
	
</script>
<body onload="resizeDoc();" onresize="resizeDoc();">
<script type="text/javascript">	
		window.onload = function()
		{
			displayCategory(); 
		}
		function displayCategory()
		{
			 $('#preferedCatgry_view').html("${sessionScope.preferenceCategories}");
			 var cat = ("${sessionScope.expandCategory}");
			 var mainCat = cat.split(",");
			if($('.preferencesLst tbody tr').hasClass('mainCtgry')) {
				$('.preferencesLst tbody tr:not(.mainCtgry)').hide();	 
			}
			for(var i = 0;i<mainCat.length;i++)
			{
				$('.preferencesLst tr[name="'+mainCat[i]+'"]').show();
				$('.preferencesLst tr[name="'+mainCat[i]+',"]').show();
			}
			 
		}
	</script>
<form:form name="doublecheckinfoform" commandName="doublecheckinfoform" >
	<form:hidden path="states.stateHidden" id="collegeStateHidden" name="collegeStateHidden"/>
	<form:hidden path="states.universityHidden" id="universityHidden" name="universityHidden"/>	
	<form:hidden path="userPreferenceInfoList.stateHidden" id="stateHidden" name="stateHidden"/>
	<form:hidden path="userPreferenceInfoList.stateCodeHidden" id="stateCodeHidden" name="stateCodeHidden"/>
	<form:hidden path="userPreferenceInfoList.state" id="state" name="state"/>
	<input type="hidden" name="innerText" id="innerText"/>
	<div id="contWrpr">
    <div class="block mrgnBtm">
      <h3 class="zeromrgnBtm">Congratulations! You are almost there.</h3>
      <p>Please take a look at your summary below to make sure that we have everything accurate.
        If you see something you would like to change, just change it below.</p>
    </div>
    <div class="breadCrumb">
      <ul>
        <li class="brcIcon"><img src="../images/consumer/find_dchkIcon.png" alt="find" /></li>
        <li class="leftPdng"><strong>Double Check</strong></li>
      </ul>
      <span class="rtCrnr">&nbsp;</span> </div>
    <div class="contBlks" id="">
      <table class="grdTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
          <tr>
            <td class="Label"><label for="contFName" class="mand">First Name</label></td>
            <td align="left"><form:errors
						cssStyle="color:red" path="userPreferenceInfoList.firstName" cssClass="error"></form:errors>
						<form:input path="userPreferenceInfoList.firstName" type="text"
						name="couponName3" id="rtlrName" maxlength="20" tabindex="1"/></td>
            <td class="Label"><label for="contLName" class="mand">Last Name</label>
            </td>
            <td align="left"><form:errors cssStyle="color:red"
						path="userPreferenceInfoList.lastName" cssClass="error"></form:errors>
						<form:input path="userPreferenceInfoList.lastName" type="text"
						id="rtlrName" maxlength="50" tabindex="2"/></td>
          </tr>
          <tr>
            <td class="Label" width="17%"><label for="addrs1" class="mand" >Address 1</label>
            </td>
            <td width="33%"><form:errors
						cssStyle="color:red" path="userPreferenceInfoList.address1"></form:errors>
						<form:textarea path="userPreferenceInfoList.address1"
						name="textarea2" id="addrs1" cols="45" rows="5" tabindex="3"
						class="txtAreaSmall" onkeyup="checkMaxLength(this,'50');" cssStyle="height:60px;"></form:textarea> 
			</td>
            <td class="Label"><label for="addrs3">Address 2</label>
            </td>
            <td>
            <form:textarea path="userPreferenceInfoList.address2" name="textarea" tabindex="4"
						id="addrs3" cols="45" rows="5" class="txtAreaSmall" onkeyup="checkMaxLength(this,'50');" cssStyle="height:60px;"></form:textarea>
				</td>
          </tr>
          <tr>
            <td class="Label" align="left"><label for="pCode" class="mand">Postal Code</label>
            </td>
            <td align="left">
            <form:errors cssStyle="color:red" path="userPreferenceInfoList.postalCode"></form:errors>
			<form:input path="userPreferenceInfoList.postalCode" type="text" class="loadingInput" maxlength="5" tabindex="5" name="couponName7"
						id="pstlCd" onkeypress="zipCodeAutocomplete('pstlCd');return isNumberKey(event)" 
						onchange="isNumeric(this.value);" onkeyup="isEmpty(this.value);"/>
              			</td>
            <td class="Label"><label for="cty" class="mand">City</label>
            </td>
            <td align="left">             
             <form:errors cssStyle="color:red" path="userPreferenceInfoList.city"></form:errors>
				<form:input path="userPreferenceInfoList.city" id="City" tabindex="6" class="loadingInput"/>
			</td>
          </tr>
          <tr>
            <td class="Label"><label for="sts" class="mand">State</label>
            </td>
            <td align="left">
            <form:errors cssStyle="color:red" path="userPreferenceInfoList.stateHidden"></form:errors>
			<form:input path="userPreferenceInfoList.state" id="Country" tabindex="7" disabled="true"/>
			</td>
            <td class="Label" align="left"><label for="phnNum">Phone #</label>
            </td>
            <td align="left"><form:errors cssStyle="color:red" path="userPreferenceInfoList.mobilePhone"></form:errors>
				<form:input path="userPreferenceInfoList.mobilePhone" type="text" tabindex="8"
						name="couponName8" id="phnNum" onkeyup="javascript:backspacerUP(this,event);" onkeydown="javascript:backspacerDOWN(this,event);"/><label class="Label">(xxx)xxx-xxxx</label>
				</td>
          </tr>
        </tbody>
      </table>
    </div>
    <h3 class="subheader mrgnTopSmall">Preferences</h3>
    <div class="contBlks" id="">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="brdrLsTbl topMrgn">
        <tbody>
          <tr>
            <td colspan="4" class="cmnTitle">Alerts Preference?</td>
          </tr>
          <tr>
            <td width="9%" >Email:</td>
            <td width="41%" ><label>
             <form:checkbox path="alertpreferencesInfo.email" value="${sessionScope.categoryDetailresponse.email}"/> 
              </label></td>
            <td width="9%" >Cell Phone:</td>
            <td width="41%"><label>
              <form:checkbox path="alertpreferencesInfo.cellPhone" value="${sessionScope.categoryDetailresponse.cellPhone}"/> 
              </label></td>
          </tr>
          <tr>
            <td colspan="4" class="cmnTitle">What type of things are you interested in receiveing deals on?</td>
          </tr>
        </tbody>
      </table>
      <div class="sctmScroll_small brdrTop">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd preferencesLst listCntrl" id="preferedCatgry_view">
          <col width="4%">
          <col width="4%">
          <col width="92%">
        </table>
      </div>
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="brdrLsTbl topMrgn brdrTop">
        <tr>
          <td colspan="4" class="cmnTitle">Have a college in mind?</td>
        </tr>
        <tr>
          <td width="19%">State:</td>
          <td colspan="3"><form:select path="states.state" class="textboxBig"
								id="Country1" onchange="loadCollege()" tabindex="9">
								<form:option value="0" label="--Select--">--Select-</form:option>
								<c:forEach items="${sessionScope.statesList}" var="s">
									<form:option value="${s.stateabbr}" label="${s.state}" />
								</c:forEach>
							</form:select></td>
        </tr>
        <tr>
          <td width="19%">College or University Name:</td>
          <td colspan="3"><div id="myAjax1">
		<form:select  path="states.college" id="College" tabindex="10"
			class="textboxBig">
			<form:option  value="0">--Select--</form:option>
		</form:select>
		</div></td>
        </tr>
      </table>
      <div class="btnStrip" align="right">

			<input name="Cancel3" value="Next" type="submit" class="btn btn-success" onclick="callShop()" tabindex="11"/>
			</div>
    </div>
  </div>
<div class="clear"></div>
</form:form>
<script type="text/javascript">
		setCollege();
</script>
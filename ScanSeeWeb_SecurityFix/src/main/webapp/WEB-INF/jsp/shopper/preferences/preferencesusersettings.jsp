<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<script type="text/javascript">
	window.onload = function() {
	
		favLoad('${fn:escapeXml(RadiusSaved)}');
		}
</script>
		</head>

<body onload="load()" onunload="GUnload()">
	<form:form name="preferencesusersettings"
		commandName="preferencesusersettings">
		<div id="content" class="topMrgn">
			<div class="navBar">
				<ul>
					<li><a href="preferencesmain.htm"><img src="../images/backBtn.png"
							 alt="back" /> </a></li>
					<li class="titletxt">Settings</li>
					<!--<li class="floatR mrgnRt"><a href="index.html"><img src="images/logoutBtn.png" alt="logout" /></a></li>-->
				</ul>
			</div>

			<div class="clear"></div>
			<div class="mobCont noBg">
				<div class="splitDsply floatR">


					<div class="clear"></div>
					<div class="fluidView">
						<div class="stretch ">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="brdrLsTbl zeroBtmMrgn">
								<tr>
									<td colspan="2" class="Label"><strong>Global
											Application Settings</strong></td>
								</tr>
								<tr>
									<td width="30%">Local Radius</td>
										<td align="center"><form:errors cssStyle="color:red"
										path="localeRadius"></form:errors>
										 <form:input onkeypress="return isNumberKey(event)"  maxlength="2" cssClass="textboxDate"
										 path="localeRadius" onclick="toggleConfirm();" />
									</td>
																<td width="65%"></td>
								</tr>


								<tr>

								<td colspan="2"><input type="button" class="mobBtn LstRtlr"
									value="Save" onclick="saveUserSettings('');"  title="Save"/>
								</td>

								</tr>
							</table>
						</div>

					</div>
				</div>
				<%@include file="../leftmenu.jsp"%>
			</div>
			
		</div>
	</form:form>
</body>
</html>
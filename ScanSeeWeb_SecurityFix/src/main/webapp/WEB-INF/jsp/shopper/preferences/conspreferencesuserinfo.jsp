<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/autocomplete.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>

<script>
$(document).ready(function() {	
	$("#datepicker2").datepicker({
		showOn : 'both',
		buttonImageOnly : true,
		buttonText : 'Click to show the calendar',
		buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
	});
	
	$("#City").live("keydown",function(e)
			{
				var s = String.fromCharCode(e.which);
				if (s.match(/[a-zA-Z0-9\.]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#pstlCd').val("");
				   cityAutocomplete('pstlCd');
				}else if(s.match(/[\b]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#pstlCd').val("");
					cityAutocomplete('pstlCd');
				}
							
			});
	var stateCode = '';
	var state = '';
	if($('#pstlCd').val().length == 5)
	{
		stateCode = $('#stateCodeHidden').val();
		state = $('#stateHidden').val();
		$( "#Country" ).val(state);			 
		}else{
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
		$( "#Country" ).val("");
	}
});
</script>
<script type="text/javascript">
function getCityTrigger(val) 
{	
	if(val == "")
	{
		$( "#Country" ).val("");
		$( "#pstlCd" ).val( "" );
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
	}
	document.createshopperprofileform.cityHidden.value = val;
	document.createshopperprofileform.city.value = val;
}
function clearOnCityChange() 
{
	if($('#citySelectedFlag').val() != 'selected')
	{
		$( "#Country" ).val("");
		$( "#pstlCd" ).val( "" );
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
	}
	else
	{
		$('#citySelectedFlag').val('');
	}
}
function isEmpty(vNum) 
{
	if(vNum.length < 5)
	{
		$( "#Country" ).val("");
		$( "#City" ).val("");
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
	}
	return true;
}
</script>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<script type="text/javascript">
		window.onload = function()
		{
			favLoad('${fn:escapeXml(SavedUserInfo)}');
		}
	</script>
	<div id="wrapper">
		<form:form name="preferencesuserinfo" commandName="preferencesuserinfo" acceptCharset="ISO-8859-1">
		<form:hidden path="stateHidden" id ="stateHidden"/>
		<form:hidden path="stateCodeHidden" id = "stateCodeHidden" />
			<div id="contWrpr" style="min-height: 262px;">
				<div class="breadCrumb">
					<ul>
						<li class="brcIcon"><img src="../images/consumer/find_prefIcon.png" alt="find" /></li>
						<li class="leftPdng">User Settings</li>
					</ul>
					<span class="rtCrnr">&nbsp;</span> 
				</div>
				<div class="contBlks relative">
					<div class="btnCntrl">
						<ul>
							<li>
								<a href="#"><img src="../images/consumer/user.png" alt="user" />
									User Settings
								</a>
							</li>
							<li>
								<a href="conspreferences.htm">
									<img src="../images/consumer/ctgry.png" alt="category" />
									Preferred Category
								</a>
							</li>
							<li>
								<a href="consfetchusersettings.htm">
									<img src="../images/consumer/radius.png" alt="radius" />
									Select Radius
								</a>
							</li>
						</ul>
					</div>
					<div class="clear"></div>
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn">
						<tr>
							<td width="16%" class="Label">
								<label class="mand" for="contFName">First Name</label>
							</td>
							<td width="34%">
								<form:errors cssStyle="color:red" path="firstName"></form:errors>
								<form:input path="firstName" id="lFName" maxlength="20" tabindex="1" />
							</td>
							<td width="14%" class="Label">
								<label class="mand" for="contFName">Last Name</label>
							</td>
							<td width="36%">
								<form:errors cssStyle="color:red" path="lastName"></form:errors> 
								<form:input path="lastName"	id="llName" value="" maxlength="30" tabindex="2" />
							</td>
						</tr>
						<tr>
							<td class="Label" >
								&nbsp;
							</td>
							<td colspan="3" class="Label">
								<a href="javascript:void(0);" onclick="editPswd(${requestScope.userId});">
									Change Password
								</a>
							</td>
						</tr>
						<tr>
							<td class="Label">
								&nbsp;
							</td>
							<td colspan="3">
								Tell ScanSee which area to search for deals(Typically Home Address)
							</td>
						</tr> 
						<tr>
							<td class="Label">
								<label class="mand" for="contFName">Address1</label>
							</td>
							<td>
								<form:errors cssStyle="color:red" path="address1"></form:errors> 
								<form:textarea class="txtAreaSmall" rows="4" cols="45" path="address1" id="addrs1"
											onkeyup="checkMaxLength(this,'50');" tabindex="3" cssStyle="height:60px;" />
							</td>
							<td class="Label">
								Address2
							</td>
							<td>
								<form:errors cssStyle="color:red" path="address2"></form:errors> 
								<form:textarea class="txtAreaSmall" rows="4" cols="45" path="address2" id="address2"
										onkeyup="checkMaxLength(this,'50');" cssStyle="height:60px;" tabindex="4" />
							</td>
						</tr>							
						<tr>
							<td class="Label">
								<label class="mand" for="contFName">Postal Code</label>
							</td>
							<td>
								<form:errors cssStyle="color:red" path="postalCode"></form:errors> 
								<form:input path="postalCode" type="text" class="loadingInput dsblContxMenu" maxlength="5" 
											id="pstlCd" onkeypress="zipCodeAutocomplete('pstlCd');return isNumberKey(event)" 
											onchange="isNumeric(this.value);" onkeyup="isEmpty(this.value);" tabindex="5"/>
							</td>
							<td class="Label">
								<label class="mand" for="contFName">City</label>
							</td>
							<td>
								<form:errors cssStyle="color:red" path="city"></form:errors>
								<form:input path="city" id="City" class="loadingInput dsblContxMenu" tabindex="6" />
							</td>
						</tr>		
						<tr>
							<td class="Label">
								<label class="mand" for="contFName">State</label>
							</td>
							<td>
								<form:errors cssStyle="color:red" path="state"></form:errors>
								<form:input path="state" id="Country" disabled="true" tabindex="7"/>
							</td>
							<td class="Label">
								Mobile Number
							</td>
							<td colspan="3">
								<form:errors cssStyle="color:red" path="mobileNumber"></form:errors>
								<form:input	path="mobileNumber" type="text" id="phnNum" onkeyup="javascript:backspacerUP(this,event);" 			
											onkeydown="javascript:backspacerDOWN(this,event);" tabindex="8" />
								<label class="Label">(xxx)xxx-xxxx</label>
							</td>
						</tr>	
						<tr>
							<td class="Label">
								Gender
							</td>
							<form:errors cssStyle="color:red" path="gender"></form:errors>
							<td>
								<form:radiobutton path="gender" value="0" label="Male" tabindex="9"/>
								<form:radiobutton path="gender" value="1" label="Female" tabindex="10"/>
							</td>
							<td class="Label">
								Date of Birth
							</td>
							<td align="left">
								<form:errors cssStyle="color:red" path="dob"></form:errors> 
								<form:input path="dob" name="csd" type="text" class="textboxDate" id="datepicker2" tabindex="11"/>(mm/dd/yyyy)
							</td>
						</tr>
						<tr>
							<td class="Label">
								Educational level
							</td>
							<td colspan="3">
								<form:errors cssStyle="color:red" path="educationLevelId"></form:errors> 
								<form:select path="educationLevelId" class="selecBx" id="eduid" tabindex="12">
									<form:option value="0" label="--Select--">--Select--</form:option>
									<c:forEach items="${sessionScope.educationlst.educationalInfo}" var="edu">
									<form:option value="${edu.educationalLevelId}" label="${edu.educationalLevelDesc}" />
									</c:forEach>
								</form:select>
							</td>
						</tr>	
						<tr>
							<td class="Label">
								Home Owner
							</td>
							<td colspan="3">
								<form:radiobutton path="homeOwner" value="0" label="Yes" tabindex="13"/> 
								<form:radiobutton path="homeOwner" value="1" label="No" tabindex="14"/>
							</td>
						</tr>
						<tr>
							<td class="Label">
								Marital Status
							</td>
							<td align="left">
								<form:errors cssStyle="color:red" path="maritalStatus"></form:errors> 
								<form:select path="maritalStatus" class="selecBx" id="Country1" tabindex="15">
									<form:option value="0" label="--Select--">--Select--</form:option>
									<c:forEach items="${sessionScope.matitalstatuslst.maritalInfo}" var="mari">
									<form:option value="${mari.maritalStatusId}" label="${mari.maritalStatusName}" />
									</c:forEach>
								</form:select>
							</td>
							<td class="Label">
								# Of Children
							</td>
							<td>
								<form:input path="children" id="noOfchi" onkeypress="return isNumberKey(event)" maxlength="2" cssClass="textboxDate" tabindex="16"/>
							</td>
						</tr>
						<tr>
							<td class="Label">
								Income Range
							</td>
							<td colspan="3">
								<form:errors cssStyle="color:red" path="incomeRangeID"></form:errors> 
								<form:select path="incomeRangeID" class="selecBx" id="icomeid" tabindex="17">
									<form:option value="0" label="--Select--">--Select--</form:option>
									<c:forEach items="${sessionScope.incomerangelst.incomeRange}" var="income">
									<form:option value="${income.incomeRangeId}" label="${income.incomeRange}" />
									</c:forEach>
								</form:select>
							</td>
						</tr>
						<tr>
							<td class="Label">
								&nbsp;
							</td>
							<td colspan="3" class="Label">
								<input type="button" value="Save" onclick="saveUserInfo()" class="btn btn-success " tabindex="18"/>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</form:form>
	</div>
</body>
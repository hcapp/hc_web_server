<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;"><!--<h3>List of Retailers Based on Search Criteria</h3>-->
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobGrd detView zeroBtmMrgn" id="staticView">
         
          <tr class="noHvr">
            <td width="3%" align="center">&nbsp;</td>
          <td width="72%"><span><a href="#"><c:out value="${requestScope.retailer.retailName}"/></a></span>
                <ul><li><span><c:out value="${requestScope.distance}"/> Miles</span></li>
                  <li><span>Price:</span>
                  <c:if test="${requestScope.productPrice ne null && !empty requestScope.productPrice && requestScope.productPrice ne 'NotApplicable'}">
                   $<c:out value="${requestScope.productPrice}"/>
                  </c:if>
                  </li>
                  
                </ul></td>
            <td width="25%"><input type="button" name="mapview" value="Locate Store" class="btn"  onclick="load(${requestScope.latitude} , ${requestScope.longitude})" title="Map View"/></td>
          </tr>

  <tr>
    <td align="center">&nbsp;</td>
    <td><span><a href="#">Web </a></span>
      <ul>
        <li><span>URL:</span><a href="#" target="_blank"><c:out value="${requestScope.retailer.retailURL}"/></a></li>
    </ul></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td><span><a href="#">Address</a></span>
      <ul>
      <c:if test="${requestScope.retailer.address1 ne null && !empty requestScope.retailer.address1}">
      <li><span><c:out value="${requestScope.retailer.address1}"/>,<c:out value="${requestScope.retailer.city}"/>
     <c:if test="${requestScope.retailer.postalCode ne null && !empty requestScope.retailer.postalCode}"> 
     ,<c:out value="${requestScope.retailer.postalCode}"/></c:if>
      </span></li>
      </c:if>
      </ul></td>
    <td><!--<input type="button" name="Cancel2" value="Call Store" class="btn" />--></td>
  </tr>
</table>

        </div>
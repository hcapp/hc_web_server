<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" 
                                                  prefix="fn" %>



<div class="dataView" style="height: 450px; overflow-x: hidden;">




	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="mobGrd detView zeroBtmMrgn">
		
				<c:if
			test="${requestScope.CommiJunctionData ne null && !empty requestScope.CommiJunctionData}">

<td colspan="4"  class="mobSubTitle">Commission Junction</td>

			<c:forEach items="${requestScope.CommiJunctionData}" var="item">

				<tr>

					<td width="81%"><span> <a
							href="<c:out value="${item.buyURL}"/>" target="_blank">
							<c:out 	value="${item.retailerName}" /> </a>
					</span>
						<ul>
							<li><span>Price:</span>
							<c:out value="${item.salePrice}" />
							</li>
						</ul>
					</td>

				</tr>


			</c:forEach>
		</c:if>
		
		
		
     



		<c:if
			test="${requestScope.OnlineStoresList ne null && !empty requestScope.OnlineStoresList}">

  <td colspan="4"  class="mobSubTitle">Shopzilla</td>

			<c:forEach items="${requestScope.OnlineStoresList}" var="item">

				<tr>

					<td width="81%"><span>
					<c:set var="strLength" value="${fn:length(item.url)}" />
					<c:set var="url" value="${fn:substring(item.url,9,strLength-3)}" />
					 <a
							href="<c:out value="${url}"/>" target="_blank"><c:out
									value="${item.merchantName}" /> </a>
					</span>
						<ul>
							<li><span>Price:</span>
							<c:out value="${item.salePrice}" />
							</li>
						</ul>
					</td>

				</tr>


			</c:forEach>
		</c:if>
	</table>

</div>




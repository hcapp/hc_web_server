<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html>
<body>
<script type="text/javascript">
window.onload = function() {
window.print();
window.close(); 
}
</script>
<div id="wrapper">
 <div id="header">
    
    <div class="clear"></div>
 </div>
  <div class="clear"></div>
	<div id="content" class="shdwBg">
			<div class="">
				
				<div class="grdSec brdrTop">
					
<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl"  align="center" style="font-family:verdana; color:purple;">
												
						<tr>
							<td colspan="4" class="header" align="center">
								<span style="color:green; font-family:Geneva, Arial; font-size:18px;">WISH LIST PRODUCTS</span>
							</td>
						</tr>
						
						<tr class="sub-hdr" ><b>
						
							<td width="45%"  align="center" bgcolor="yellow">Product Name</td>
							<td width="30%"  align="center" bgcolor="yellow">Product Image</td>
							<td width="25%"  align="center" bgcolor="yellow">Discount</td>
							
							</b>

								<c:if
									test="${sessionScope.wishlistprods.alertProducts eq null && empty sessionScope.wishlistprods.alertProducts
									&& sessionScope.wishlistprods.productInfo eq null && empty sessionScope.wishlistprods.productInfo}">
									<tr>
										<td class="zeroPdng"><img
											src="/ScanSeeWeb/images/consumer/noneAvail_medium.png"
											alt="common_noneAvail" width="611" height="373" /></td>
									</tr>
								</c:if>

								<!-- DISPLAYING ALERTED PRODUCT DETAILS. -->
								<c:if
									test="${sessionScope.wishlistprods.alertProducts ne null && !empty sessionScope.wishlistprods.alertProducts}">
									<tr class="subHdr" name="Alerted Items">
										<td align="left">Alerted Items</td>
										<td></td>
										<td></td>
										
									</tr>


									<c:forEach
										items="${sessionScope.wishlistprods.alertProducts.alertProductDetails}"
										var="alertedProd">
										<%-- Displaying Wish list Alerted product coupon details --%>

									
										<c:if test="${alertedProd.couponSaleFlag>0}">
										<tr>
									<td><c:out value="${alertedProd.productName}"/></td>
											<td class="imgDsply" align="center"><img
														src="${alertedProd.productImagePath}"
														onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
														alt="product1" width="70" height="70"
														 /></td>
										
														
							<td class="imgDsply" align="center">
								
													<img
													src="/ScanSeeWeb/images/consumer/MM_GalleryIcon.png"
													alt="coupon"  width="30" height="30"  />
												</td> 										</tr>
										</c:if>

										<%-- Displaying Wish list Alerted product Hotdeal details --%>

										<c:if test="${alertedProd.hotDealFlag>0}">

											<tr>
											<td><c:out value="${alertedProd.productName}"/></td>
											<td class="imgDsply" align="center"><img
														src="${alertedProd.productImagePath }"
														onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
														alt="product1" width="70" height="70"
														 /></td>
										
														
							<td class="imgDsply" align="center">
											
												
										
												<img
													src="/ScanSeeWeb/images/consumer/MM_HotDeals_Icon.png"
													alt="hotdeals" width="30" height="30" />
												</td>
												
											</tr>
										</c:if>


										<%-- Displaying Wish list Alerted product with sale price details --%>

										<c:if test="${alertedProd.retailFlag>0}">

											<tr>
											<td><c:out value="${alertedProd.productName}"/>
											<c:if
														test="${alertedProd.salePrice ne null && alertedProd.salePrice ne '' }">
														<p class="priceInfo mrgnTopSmall" style="color:red;">
															<label >Sale:</label> ${alertedProd.salePrice}
														</p>
													</c:if> 
										</td>
											<td class="imgDsply" align="center"><img
														src="${alertedProd.productImagePath }"
														onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
														alt="product1"  width="70" height="70" 
														 /></td>
										
										
								
										
														
						<td class="imgDsply" align="center">
											
												
											
											<img
													src="/ScanSeeWeb/images/consumer/salesIcon.png"
													alt="hotdeals"  width="30" height="30"  />
												</td>	 				
											

												
											</tr>
										</c:if>

									</c:forEach>

								</c:if>


								<!-- DISPLAYING NORMAL PRODUCT DETAILS. -->
								<c:if
									test="${sessionScope.wishlistprods.productInfo ne null && !empty sessionScope.wishlistprods.productInfo}">
<tr class="subHdr" name="Regular Items">
										<td align="left">Regular Items</td>
										<td></td>
										<td></td>
										
									</tr>
									<c:forEach items="${sessionScope.wishlistprods.productInfo}"
										var="regularProducts">
										<tr>
<td align="left"><span><b>${regularProducts.wishListAddDate}</b></span></td>
										<td></td>
										<td></td>
										</tr>
									
										<c:forEach items="${regularProducts.productDetail}"
											var="productAttributes">
											
	
	
											<td><c:out value="${productAttributes.productName}"/></td>
											
										
												
												<td class="imgDsply" align="center"><img
													src="${productAttributes.productImagePath}"
													onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
													alt="product1"  width="70" height="70" 
													/></td>
												
						<td></td>
												
											</tr>


										</c:forEach>
									</c:forEach>

								</c:if>




							</table>
						</div>
					
					
				</div>
			</div>
	</div>
</div>
<div class="clear"></div>
</body>


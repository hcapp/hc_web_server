<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>


<script type="text/javascript">
	//<!--Below script method is used for pagination record count implementation. -->
	function getPerPgaVal() {

		var selValue = $('#selPerPage :selected').val();
		document.ShoppingListHome.recordCount.value = selValue;
		//Call the method which populates grid values
		shoppinglistdisplay();

	}
	//<!--Below script method is used for pagination implementation. -->
	function callNextPage(pagenumber, url) {
		var selValue = $('#selPerPage :selected').val();

		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}
</script>

<body onresize="resizeDoc();" onload="resizeDoc();">

	<form name="WishListHotDeals" commandName="WishListHotDeals">
		<input type="hidden" name="productId" id="productId" value="" /> <input
			type="hidden" name="redirectUrl" id="redirectUrl" value="" /> <input
			type="hidden" name="recordCount" id="recordCount" value="" /> <input
			type="hidden" name="screenName" id="screenName" value="" /> <input
			type="hidden" name="slscreenName" id="slscreenName"
			value="${requestScope.slscreenName}" />


		<div id="contWrpr">
			<div class="spliView" id="wishLst">
				<div class="">
					<div class="breadCrumb">
						<ul>
							<li class="brcIcon"><img
								src="/ScanSeeWeb/images/consumer/wl_bcIcon.png" alt="find" />
							</li>
							<li><a href="conswishlisthome.htm">Wish List</a>
							</li>
							<li class="active">Alerted Items</li>
							<li class="clear"></li>
						</ul>
						<span class="rtCrnr">&nbsp;</span>
					</div>
					<div class="splitCont">
						<div class="subHdr-control">
							<h3>
								<b>${requestScope.alertProdName}</b> <span>Total Item's :<i
									id="wltotCnt">${requestScope.dealcount}</i>
								</span>

							</h3>
						</div>
						<div class="sctmScroll">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="brdrLsTbl rowStrip" id="">

								<c:choose>
									<c:when
										test="${requestScope.conswlhotdeals ne null && !empty requestScope.conswlhotdeals}">
										<tr class="subHdr" name="Alerted Items">
											<td colspan="3" align="left">Hot Deals</td>
										</tr>
										<c:forEach items="${requestScope.conswlhotdeals}"
											var="dealInfo">

											<tr name="Alerted Items">

												<td align="center"><span class="imgDsply"> <img
														src="${dealInfo.hotDealImagePath}"
														onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
														alt="p" width="57" height="67" />
												</span>
												</td>

												<td width="84%" align="left" valign="middle"
													class="imgDsply">
													<ul class="list-detail">
														<li><a
															href="/ScanSeeWeb/shopper/conshddetails.htm?hotdealId=${dealInfo.hotDealId}&page=WL&hdlstid=${dealInfo.hotdealLstId}">
																${dealInfo.hotDealName}</a>
														</li>
														<li><span> ${dealInfo.hDPrice}</span><span
															class="priceInfo lftPdng"> <label>Sale
																	Price:</label> ${dealInfo.hDSalePrice} </span>
														</li>
														<li>${dealInfo.hDLognDescription}</li>
													</ul>
												</td>
												<td width="10%" align="left" valign="top" class="imgDsply">&nbsp;</td>

											</tr>

										</c:forEach>

									</c:when>
									<c:otherwise>

									</c:otherwise>

								</c:choose>



							</table>
						</div>
					</div>
					<ul class="infoCntrl">
					</ul>

				</div>
			</div>
			<div class="clear"></div>
		</div>
		</div>
		<div class="clear"></div>

		<div class="clear"></div>
	</form>
</body>
</html>

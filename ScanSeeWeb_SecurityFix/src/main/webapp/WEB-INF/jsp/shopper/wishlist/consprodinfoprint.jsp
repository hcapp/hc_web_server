<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html>
<body>
<script type="text/javascript">
window.onload = function() {
	window.print();
	window.close();
}
</script>
<div id="wrapper">
 <div id="header">
    
    <div class="clear"></div>
 </div>
  <div class="clear"></div>
	<div id="content" class="shdwBg">
			<div class="">
				
				<div class="grdSec brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl"  align="center">
						<tr ><td colspan="7" class="header"><span>Product Details</span></td></tr>
						<tr class="sub-hdr">
							<td align="left" width="25%">Product Name</td>
							<td width="32%">Product Description</td>
							<td width="20%">Product Price</td>
							<td width="20%">Bar code</td>
							<td width="20%">Model Number</td>
							<td width="20%">Warranty Service Info</td>
							<td width="20%">Product Expiration Date</td>
							
							
						</tr>
						
								<c:choose>
									<c:when
										test="${sessionScope.printprodinfo ne null && !empty sessionScope.printprodinfo}">
									
										<c:forEach items="${sessionScope.printprodinfo}"
											var="printInfo">
						<tr>
							<td align="left"><c:out value="${printInfo.productName}"/></td>
							<td><c:out value="${printInfo.productDescription}" /></td>
							<td><c:out value="${printInfo.productPrice}"  /></td>
							<td><c:out value="${printInfo.scanCode}"  /></td>
							<td><c:out value="${printInfo.modelNumber}"  /></td>
							<td><c:out value="${printInfo.warrantyServiceInfo}"  /></td>
							<td><c:out value="${printInfo.productExpDate}"  /></td>
						</tr>
						
						</c:forEach>
						</c:when>
						</c:choose>
					</table>
					
					
					
				</div>
			</div>
	</div>
</div>
<div class="clear"></div>
</body>


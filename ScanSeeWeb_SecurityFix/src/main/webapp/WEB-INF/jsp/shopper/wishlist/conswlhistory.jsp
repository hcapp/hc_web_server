<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>
<script type="text/javascript">
	//<!--Below script method is used for pagination record count implementation. -->
	function getPerPgaVal() {

		var selValue = $('#selPerPage :selected').val();
		document.WishListHome.recordCount.value = selValue;
		//Call the method which populates grid values
		wishlisthomedisplay('history');

	}
	//<!--Below script method is used for pagination implementation. -->
	function callNextPage(pagenumber, url) {
		var selValue = $('#selPerPage :selected').val();
		document.paginationForm.screenName.value = 'history';
		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}
</script>
<body onload="resizeDoc();" onrize="resizeDoc();">
	<form name="WishListHome" commandName="WishListHome">
	<input
			type="hidden" name="redirectUrl" id="redirectUrl" value="" /> <input
			type="hidden" name="recordCount" id="recordCount" value="" />
	<input type="hidden"
			name="screenName" id="screenName" value="" />
		<div id="contWrpr">
			<div class="spliView" id="wishLst">
				<div class="splitL">
					<div class="breadCrumb">
						` <span class="ltCrnr">&nbsp;</span> <span class="rtCrnr">&nbsp;</span>
					</div>
					<div class="splitCont relative">
						<div class="tabdPnl">
							<ul class="tabdCntrl">
								<!-- <li class="active"><a href="#" class="favorites">Favorites</a></li>-->
								<li class=""><a href="conswlsearchhome.htm"
									class="search"><img
										src="/ScanSeeWeb/images/consumer/srchIcon.png" alt="search"
										align="absmiddle" /> Search</a>
								</li>
								<li class="active"><a href="conswlhistory.htm" class="history"><img
										src="/ScanSeeWeb/images/consumer/hstryIcon.png" alt="history"
										align="absmiddle" /> History</a>
								</li>
							</ul>
						</div>


						<!--  DISPLAYING WISH LIST HISTORY PRODUCTS... -->
						<div class="sctmScroll">
							<div class="tbdcnt history">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="brdrTbl" id="wishLsthistory">
									<c:if
										test="${sessionScope.wlhistoryprods.productHistoryInfo eq null && empty sessionScope.wlhistoryprods.productHistoryInfo}">

										<tr>
											<td class="zeroPdng"><img
												src="/ScanSeeWeb/images/consumer/common_noneAvail.png"
												alt="common_noneAvail" width="316" height="401" /></td>
										</tr>
									</c:if>
									<c:if
										test="${sessionScope.wlhistoryprods.productHistoryInfo ne null && !empty sessionScope.wlhistoryprods.productHistoryInfo}">

										<c:forEach
											items="${sessionScope.wlhistoryprods.productHistoryInfo}"
											var="regularProducts">

											<tr class="subHdr" name="${regularProducts.wishListAddDate}">
												<td colspan="4" align="left">${regularProducts.wishListAddDate}</td>
												<td>&nbsp;</td>
											</tr>
											<c:forEach items="${regularProducts.productDetail}"
												var="productAttributes">

												<tr name="${regularProducts.wishListAddDate}"
													style="cursor: pointer;">
													<td align="center" width="11%"><label> <input
															type="checkbox" name="historychkbxGrp" class="historychkbxGrp"
															value="${productAttributes.userProductId}," /> </label></td>

													<td width="14%" class="img"><img
														src="${productAttributes.productImagePath}"
														onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
														alt="product1" width="200" height="150"
														title="${productAttributes.productName}" /></td>
													<td width="74%">
														<p>
															<a
																href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${productAttributes.productId}&key2=1&page=WL"
																class="title productTrim active"
																title="${productAttributes.productName}">${productAttributes.productName}

															</a>

														</p>
													<td width="12%" align="right"></td>
													</td>
													<td>&nbsp;</td>
												</tr>


											</c:forEach>
										</c:forEach>

									</c:if>




								</table>
							</div>
						</div>
					</div>
					<div class="clear"></div>

					<ul class="actnCntrl">
						<c:if
							test="${sessionScope.wlhistoryprods.productHistoryInfo ne null && !empty sessionScope.wlhistoryprods.productHistoryInfo}">

							<li class="stretchTab"><a href="javascript:void(0);" class="del-btn"><span
									class="actn-del" onclick="deletewlhistoryprod()"></span>Delete</a>
							</li>
						</c:if>
					</ul>
				</div>

				<!--  DISPLAYING WISH LIST  PRODUCTS... -->
				<div class="splitR">
					<div class="breadCrumb">
						<ul>
							<li class="brcIcon"><img
								src="/ScanSeeWeb/images/consumer/wl_bcIcon.png" alt="find" />
							</li>
							<li class="active">Wish List</li>
						</ul>
						<span class="rtCrnr">&nbsp;</span>
					</div>
					<div class="splitCont">
						<div class="subHdr-control">
							<h3>
								Total Item's in list:<i id="ttlCnt">7</i><span><i
									class="print" style="display: none;">Print Coupon</i><i
									class="Trash" style="display: none;">Delete Coupon</i><a
									class="icon-bg" href="javascript:void(0);"><img width="20" height="20"
										alt="print" src="/ScanSeeWeb/images/consumer/print.png" onclick="printwlProductInfo()" />
								</a><a class="icon-bg" href="javascript:void(0);"><img width="20" height="20"
										alt="Trash" src="/ScanSeeWeb/images/consumer/trash.png"
										onclick="deletewishlstprod('history')" />
								</a>
								</span>
							</h3>
						</div>
						<div class="sctmScroll">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="brdrLsTbl rowStrip cstmChkbx" id="wishLst">
								<!--IF NO ALERTED AND NORMAL PRODUCT FOUND ,THEN DISPLAYING NONE AVAILABLE IMAGE. -->
								<c:if
									test="${sessionScope.wishlistprods.alertProducts eq null && empty sessionScope.wishlistprods.alertProducts
									&& sessionScope.wishlistprods.productInfo eq null && empty sessionScope.wishlistprods.productInfo}">
									<tr>
										<td class="zeroPdng"><img
											src="/ScanSeeWeb/images/consumer/noneAvail_medium.png"
											alt="common_noneAvail" width="611" height="373" /></td>
									</tr>
								</c:if>

								<!-- DISPLAYING ALERTED PRODUCT DETAILS. -->
								<c:if
									test="${sessionScope.wishlistprods.alertProducts ne null && !empty sessionScope.wishlistprods.alertProducts}">
									<tr class="subHdr" name="Alerted Items">
										<td colspan="4" align="left">Alerted Items</td>
										<td>&nbsp;</td>
									</tr>


									<c:forEach
										items="${sessionScope.wishlistprods.alertProducts.alertProductDetails}"
										var="alertedProd">
										<%-- Displaying Wish list Alerted product coupon details --%>


										<c:if test="${alertedProd.couponSaleFlag>0}">

											<tr style="cursor: pointer;" name="Alerted Items">
												<td align="center"><div class="tglBx">
														<label> <input type="checkbox" name="chkbxGrp"
															class="chkbxGrp" value="${alertedProd.userProductId}," />
														</label>
													</div>
												</td>
												<td class="imgDsply"><img
													src="${alertedProd.productImagePath}"
													onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
													alt="product1" width="200" height="150"
													title="${alertedProd.productName}" /></td>
												<td>
													<p class="primaryText">
														<a
															href="/ScanSeeWeb/shopper/conswlcouponinfo.htm?key1=${alertedProd.productId}&
					key2=${alertedProd.alertProdID}&key3=${alertedProd.couponSaleFlag}">${alertedProd.productName}</a>
													</p></td>
												<td><img
													src="/ScanSeeWeb/images/consumer/MM_GalleryIcon.png"
													alt="coupon" width="36" height="36" />
												</td>
												<td>&nbsp;</td>
											</tr>
										</c:if>

										<%-- Displaying Wish list Alerted product Hotdeal details --%>

										<c:if test="${alertedProd.hotDealFlag>0}">

											<tr style="cursor: pointer;" name="Alerted Items">
												<td align="center"><div class="tglBx">
														<label> <input type="checkbox" name="chkbxGrp"
															class="chkbxGrp" value="${alertedProd.userProductId}," />
														</label>
													</div>
												</td>
												<td class="imgDsply"><img
													src="${alertedProd.productImagePath}"
													onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
													alt="product1" width="200" height="150"
													title="${alertedProd.productName}" /></td>
												<td>
													<p class="primaryText">

														<a
															href="/ScanSeeWeb/shopper/conswlhotdealinfo.htm?key1=${alertedProd.productId}&key2=${alertedProd.alertProdID}&key3=${alertedProd.hotDealFlag}"
															class="title productTrim active"
															title="${shopProd.productName}">${alertedProd.productName}

														</a>
													</p></td>
												<td><img
													src="/ScanSeeWeb/images/consumer/MM_HotDeals_Icon.png"
													alt="hotdeals" width="36" height="36" />
												</td>
												<td>&nbsp;</td>
											</tr>
										</c:if>


										<%-- Displaying Wish list Alerted product with sale price details --%>

										<c:if test="${alertedProd.retailFlag>0}">

											<tr style="cursor: pointer;" name="Alerted Items">
												<td align="center"><div class="tglBx">
														<label> <input type="checkbox" name="chkbxGrp"
															class="chkbxGrp" value="${alertedProd.userProductId}," />
														</label>
													</div>
												</td>
												<td class="imgDsply"><img
													src="${alertedProd.productImagePath}"
													onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
													alt="product1" width="200" height="150"
													title="${alertedProd.productName}" /></td>
												<td>
													<p class="primaryText">

														<a
															href="/ScanSeeWeb/shopper/conswllocalstore.htm?key1=${alertedProd.productId}"
															class="title productTrim active"
															title="${shopProd.productName}">${alertedProd.productName}

														</a>
													</p> <c:if
														test="${alertedProd.salePrice ne null && alertedProd.salePrice ne '' }">
														<p class="priceInfo mrgnTopSmall">
															<label>Sale:</label> ${alertedProd.salePrice}
														</p>
													</c:if></td>
<td><img
													src="/ScanSeeWeb/images/consumer/salesIcon.png"
													alt="saleicon" width="36" height="36" />
												</td>
													
													
												<td>&nbsp;</td>
											</tr>
										</c:if>

									</c:forEach>

								</c:if>


								<!-- DISPLAYING NORMAL PRODUCT DETAILS. -->
								<c:if
									test="${sessionScope.wishlistprods.productInfo ne null && !empty sessionScope.wishlistprods.productInfo}">

									<c:forEach items="${sessionScope.wishlistprods.productInfo}"
										var="regularProducts">

										<tr class="subHdr" name="${regularProducts.wishListAddDate}">
											<td colspan="4" align="left">${regularProducts.wishListAddDate}</td>
											<td>&nbsp;</td>
										</tr>
										<c:forEach items="${regularProducts.productDetail}"
											var="productAttributes">

											<tr name="${regularProducts.wishListAddDate}"
												style="cursor: pointer;">
												<td align="center"><div class="tglBx">
														<label> <input type="checkbox" name="chkbxGrp"
															class="chkbxGrp"
															value="${productAttributes.userProductId}," /> </label>
													</div>
												</td>
												<td class="imgDsply"><img
													src="${productAttributes.productImagePath}"
													onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
													alt="product1" width="200" height="150"
													title="${productAttributes.productName}" /></td>
												<td><p class="primaryText">

														<a
															href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${productAttributes.productId}&key2=1&page=WL"
															class="title productTrim active"
															title="${productAttributes.productName}">${productAttributes.productName}

														</a>


													</p></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>


										</c:forEach>
									</c:forEach>

								</c:if>




							</table>
						</div>
					</div>
					<ul class="infoCntrl" id="threeTab">
						<li><span class="cpnIcon">&nbsp;</span>Coupon Available</li>
          <li><span class="dealIcon">&nbsp;</span>Hot Deal Available</li>
           <li><span class="saleIcon">&nbsp;</span>Sale Available</li>
					</ul>
					<c:if test="${sessionScope.wishlistprods ne null}">
						<div class="pagination mrgnTopSmall">

							<table width="100%" border="0" cellpadding="0" cellspacing="0"
								class="noBrdr" id="perpage">
								<tr>
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" enablePerPage="true" />

								</tr>

							</table>

						</div>

					</c:if>
					
				</div>
			</div>
			<div class="clear"></div>
		</div>
		</div>

	</form>
</body>
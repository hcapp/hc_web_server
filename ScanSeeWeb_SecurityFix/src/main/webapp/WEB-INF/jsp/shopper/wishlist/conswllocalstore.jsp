<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>


<body onresize="resizeDoc();" onload="resizeDoc();">

	<form name="WishListHotDeals" commandName="WishListHotDeals">
		<input type="hidden" name="productId" id="productId" value="" /> <input
			type="hidden" name="redirectUrl" id="redirectUrl" value="" /> <input
			type="hidden" name="recordCount" id="recordCount" value="" /> <input
			type="hidden" name="screenName" id="screenName" value="" /> <input
			type="hidden" name="slscreenName" id="slscreenName"
			value="${requestScope.slscreenName}" />



		<div id="contWrpr">
			<div class="spliView" id="wishLst">
				<div class="">
					<div class="breadCrumb">
						<ul>
							<li class="brcIcon"><img
								src="/ScanSeeWeb/images/consumer/wl_bcIcon.png" alt="find" /></li>
							<li><a href="conswishlisthome.htm">Wish List</a></li>
							<li class="active">Alerted Items</li>
							<li class="clear"></li>
						</ul>
						<span class="rtCrnr">&nbsp;</span>
					</div>
					<div class="splitCont">
						<div class="tabdPnl">
							<ul class="tabdCntrl cart">
								<li><a href="javascript:void(0);" class="actv"> <img
										src="/ScanSeeWeb/images/consumer/localCart.png" width="28"
										height="28" alt="local" />Local Stores</a></li>
								<li><a class=""
									href="conswlonlinestore.htm?key1=${requestScope.wlproductId}"><img
										src="/ScanSeeWeb/images/consumer/onlineCart.png" width="28"
										height="28" alt="online" />Online Stores</a></li>
							</ul>
						</div>

						<div class="sctmScroll">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="brdrLsTbl rowStrip" id="">

								
									<c:if
							test="${sessionScope.findNearByDetails eq null && empty sessionScope.findNearByDetails && 
									sessionScope.ExtApifindNearByDetails eq null && empty sessionScope.ExtApifindNearByDetails}">
								<tr>
										<td class="zeroPdng"><img
											src="/ScanSeeWeb/images/consumer/wishlist_store_NA.png"
											alt="common_noneAvail" width="954" height="375" /></td>
									</tr>	
									
			</c:if>		
								
								<!--DISPLAYING SCANSEE FIND NEAR BY RETAILER INFORMATION -->
								<c:choose>

									<c:when
										test="${sessionScope.findNearByDetails ne null && !empty sessionScope.findNearByDetails}">

										<tr class="subHdr" name="Alerted Items">
											<td colspan="2" align="left">Retailer Information</td>
										</tr>
										<tr name="Alerted Items">
											<td align="l" width="8%"><span class="imgDsply"><img
													src="${requestScope.wlprodimage}" alt="p" width="57"
													height="67" /> </span></td>

											<td><span>${requestScope.wlprodname}</span>
											${requestScope.wlproddesc}
											</td>
											</tr>

											<c:forEach
												items="${sessionScope.findNearByDetails.findNearByDetail}"
												var="item">

												<tr>
													<td align="left" valign="top" class="imgDsply" colspan="2"><ul
															class="list-detail">


															<li><span>${item.retailerName}</span></li>
															<c:if
																test="${item.productPrice ne null && item.salePrice ne null}">
																<li><span> <c:if
																			test="${item.productPrice ne null && ! empty item.productPrice}">

																			<label>Old Price:</label>
                      ${item.productPrice}
																
																</span><span>
															</c:if>
															<c:if
																test="${item.salePrice ne null && ! empty item.salePrice}">
																<label>New Price:</label>
                      ${item.salePrice}					  
					  </c:if>
															</span>
															</li>
															</c:if>
															<li>${item.address}</li>
															<c:if
																test="${item.retailerUrl ne null && ! empty item.retailerUrl}">
																<li>WebSite: <a href="javascript:void(0);">${item.retailerUrl}</a>
																</li>
															</c:if>
															<li><label>Powered by:</label><label class="labeltxt">  ScanSee</label></li>


														</ul></td>
												</tr>


											</c:forEach>
									</c:when>


									<c:when
										test="${sessionScope.ExtApifindNearByDetails ne null && !empty sessionScope.ExtApifindNearByDetails}">

										<tr name="Alerted Items">
											<td align="center"><span class="imgDsply"><img
													src="${requestScope.wlprodimage}" alt="p" width="57"
													height="67" />${requestScope.wlprodname}</span></td>
										</tr>

										<c:forEach
											items="${sessionScope.ExtApifindNearByDetails.findNearByDetails}"
											var="item">


											<tr>
												<td align="left" valign="top" class="imgDsply"><ul
														class="list-detail">


														<li><span>${item.retailerName}</span></li>

														<c:if
															test="${item.productPrice ne null && ! empty item.productPrice}">
															<li><span> <label>Price:</label>
																	${item.productPrice}</span></li>
														</c:if>
														<c:if
															test="${item.address ne null && ! empty item.address }">
															<li>${item.address}</li>

														</c:if>
														<c:if
															test="${item.retailerUrl ne null && ! empty item.retailerUrl }">
															<li>WebSite: <a href="javascript:void(0);">${item.retailerUrl}</a>
															</li>


														</c:if>

														<li><label>Powered by:</label><label class="labeltxt"> Retailigence</label></li>
													</ul></td>
											</tr>


										</c:forEach>
									</c:when>
									<c:otherwise>

									</c:otherwise>

								</c:choose>


							</table>
						</div>
					</div>
					<ul class="infoCntrl">
						<!--<li>Coupon Available</li>
          <li><span class=""></span>Hot Deal Available</li>
          <li><span class=""></span>Sale Available</li>-->
					</ul>

				</div>
			</div>
			<div class="clear"></div>
		</div>
		</div>
		<div class="clear"></div>
	</form>
</body>
</html>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<div class="dataView" style="height: 450px; overflow-x: hidden;"><!--<h3>List of Retailers Based on Search Criteria</h3>-->
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="prodSummary">

            <tr class="title">
              <td align="center">
              <c:if test="${requestScope.productinfo.productImagePath == 'NotApplicable'}" >
						<img src="../images/imgIcon.png" alt="Img" width="38" height="38" />
						</c:if>
						<c:if test="${requestScope.productinfo.productImagePath != 'NotApplicable'}" >
						<img src="${requestScope.productinfo.productImagePath}" alt="Image" width="38" height="38" />
						</c:if>
              
              </td>
              <td colspan="1"><c:out value="${requestScope.productinfo.productName}"/></td>
            </tr>
           
            <tr>
              <td>&nbsp;</td>
              <td><c:out value="${requestScope.productinfo.productShortDescription}"/></td>
            </tr>
			<tr><td>&nbsp;</td><td>
			
						<c:if test="${requestScope.productinfo.productLongDescription != 'NotApplicable'}" >
						<c:out value="${requestScope.productinfo.productLongDescription}"/>
						</c:if>
			</td>
			</tr>
            <tr><td>&nbsp;</td><td>
			
						<c:if test="${requestScope.productinfo.modelNumber != 'NotApplicable'}" >
						
						
						<c:out value="${requestScope.productinfo.modelNumber}"/>
						</c:if>
			</td>
			</tr>
            <tr>
              <td>&nbsp;</td>
              <td><c:out value="${requestScope.productinfo.manufacturersName}"/></td>
            </tr>
                 <tr>
			<td>&nbsp;</td>
			 <td class="oddRow">
            <c:if test="${requestScope.productinfo.warrantyServiceInfo ne null && !empty requestScope.productinfo.warrantyServiceInfo && requestScope.productinfo.warrantyServiceInfo ne 'NotApplicable'}">
                  <c:out value="${requestScope.productinfo.warrantyServiceInfo}"/></td>
              </c:if>
            </tr>
			
			
            <tr>
              <td>&nbsp;</td>
              <td class="oddRow">Expiration Date:</td>
            </tr>
            <tr>
            <c:if test="${requestScope.productinfo.productExpDate ne null && !empty requestScope.productinfo.productExpDate && requestScope.productinfo.productExpDate ne 'NotApplicable'}">
              <td>&nbsp;</td>
              <td><c:out value="${requestScope.productinfo.productExpDate}"/></td>
              </c:if>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>
              <c:if test="${requestScope.productinfo.audioFlag ne 'N/A'}" >
	              <a href="#"><img src="../images/Audio.png" alt="audio" width="88" height="30" title="Audios" onclick="callMediaFile(${requestScope.productinfo.productId},'audio')" /> </a>
			  </c:if>
              <c:if test="${requestScope.productinfo.audioFlag eq 'N/A'}" >
	              <a href="#"><img src="../images/Audio.png" alt="audio" width="88" height="30" title="Audios" class="btnOpacity"/> </a>
			  </c:if>
			  <c:if test="${requestScope.productinfo.videoFlag ne 'N/A'}" >
               <a href="#"><img src="../images/video.png" alt="Video" width="88" height="30" title="Videos" onclick="callMediaFile(${requestScope.productinfo.productId},'video')"/></a> 
               </c:if>
               <c:if test="${requestScope.productinfo.videoFlag eq 'N/A'}" >
               <a href="#"><img src="../images/video.png" alt="Video" width="88" height="30" title="Videos" class="btnOpacity" /></a> 
               </c:if>
			   <c:if test="${requestScope.productinfo.fileFlag ne 'N/A'}" >
               <a href="#"><img src="../images/OtherInfo.png" alt="Other Info" width="88" height="30" title="Other Info" onclick="callMediaFile(${requestScope.productinfo.productId},'other')" /> </a>
			   </c:if>
			   <c:if test="${requestScope.productinfo.fileFlag eq 'N/A'}" >
               <a href="#"><img src="../images/OtherInfo.png" alt="Other Info" width="88" height="30" title="Other Info" class="btnOpacity" /> </a>
			   </c:if>
               </td>
               
            </tr>
          </table>
			
        </div>
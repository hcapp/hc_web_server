<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript">
function twitterShareCoupon() {

	var tweetText;
	var couponURL = '${requestScope.coupondetails.couponInfo.couponURL}';

	var couponName = '${requestScope.coupondetails.couponInfo.couponName}';
	if (null != couponURL&&couponURL!='NotApplicable' && couponURL!="") {

		tweetText = 'Great find at ScanSee!!! ' + couponName +" "+ "Start Date:"
				+ '${requestScope.coupondetails.couponInfo.couponStartDate}'+" "
				+ 'Expiry Date:'
				+ '${requestScope.coupondetails.couponInfo.couponExpireDate}'
				+ "  Visit:" + couponURL

	} else {
		tweetText = 'Great find at ScanSee!!! ' + couponName+" " + "Start Date:"
				+ '${requestScope.coupondetails.couponInfo.couponStartDate}'+" "
				+ 'Expiry Date:'
				+ '${requestScope.coupondetails.couponInfo.couponExpireDate}'

	}

	var tweeturl = 'http://twitter.com/share?text=' + escape(tweetText);

	window.open(tweeturl);

}

function back(){	
   document.clrForm.action = "getpagedclr.htm";
   document.clrForm.method = "POST";
   document.clrForm.submit();
   
}
function addCoupon(couponId, usage,id,from) {
		// get the form values
	
      $.ajaxSetup({cache:false})
		

		$.ajax({
			type : "GET",
			url : "addcoupon.htm",
			data : {
				'couponId' : couponId,
				'usage' : usage,
				'from'	: from
			},

			success : function(response) {
				$('#'+id).replaceWith(response);
			},
			error : function(e) {
				alert('error')
			}
		});

	}
	
function open_in_new_tab(url )
{
  window.open(url, '_blank');
  window.focus();
}
</script>
<div class="clear"></div>
<div id="content" class="topMrgn">

	<form:form commandName="clrForm" name="clrForm">
		<form:hidden path="couponId"
			value="${requestScope.coupondetails.couponInfo.couponId}" />
		<form:hidden path="rebateId" />
		<form:hidden path="loyaltyDealID" />
		<form:hidden path="requestType" />
		<form:hidden path="lowerLimit" />
		<form:hidden path="pageFlowType" />
		<form:hidden path="clrType" />
		<form:hidden path="added" />
		<form:hidden path="viewableOnWeb"/>
		<div class="navBar">
			<ul>				
				<li><a href="#"><img src="../images/backBtn.png" onclick="back();" alt="back"/></a></li>
				<li class="titletxt">Redeem</li>
				<!--<li class="floatR mrgnRt"><a href="index.html"><img src="images/logoutBtn.png" alt="logout" /></a></li>-->
			</ul>
		</div>
		<div class="clear"></div>
		<div class="mobCont noBg">
			<div class="splitDsply floatR">
				<div class="fluidViewHD">
					<div class="stretch cmnPnl relatvDiv">
						<h3 class="overallPadding">
							${requestScope.coupondetails.couponInfo.couponName}
						</h3>
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="zeroBrdrTbl dashedBrdr">
							<tr>
								<td width="13%" align="center"><img
									src="${requestScope.coupondetails.couponInfo.couponImagePath}"
									alt="product" width="130" height="130"
									onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';" />
								</td>
								<td width="87%" colspan="2" align="left" valign="top"
									class="wrpWord">
									${requestScope.coupondetails.couponInfo.couponLongDescription}
								</td>
							</tr>

							<tr>
								<td colspan="3" align="left">Valid From <b>
										${requestScope.coupondetails.couponInfo.couponStartDate} </b> to <b>
										${requestScope.coupondetails.couponInfo.couponExpireDate} </b>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="left">
									<ul class="sublinkList">
										<li class="linkBoldHdr"><a href="#"
											onclick="getCouponProductDetails(${requestScope.coupondetails.couponInfo.couponId})">Qualifying
												Products<span>&nbsp;</span> </a></li>
										<c:if test="${requestScope.coupondetails.couponInfo.viewableOnWeb == 'true'}">
										<li><a href="#" onclick="open_in_new_tab('http://www.scansee.com');">Click here to view<span>&nbsp;</span> </a></li>
										</c:if>
										<c:if test="${requestScope.coupondetails.couponInfo.viewableOnWeb == 'false'}">
										<li></li>
										</c:if>
									</ul>
								</td>
							</tr>
							<tr>
								<c:if test="${requestScope.requestType eq 'myGallery' }">
									<td colspan="3" align="left"><a href="#"><img
											src="../images/markAsUsed.png" alt="mark as used" width="140"
											height="30" onclick="userRedeemCLR('coupon')" /> </a><span
										class="wrpWord"><a href="#"><img class="fade"
												src="../images/clipCoupon.png" alt="clip coupon" width="140"
												height="30" /> </a> </span></td>
								</c:if>
								<c:if test="${requestScope.requestType eq 'used' }">
									<td colspan="3" align="left"><a href="#"><img
											class="fade" src="../images/markAsUsed.png"
											alt="mark as used" width="140" height="30" /> </a><span
										class="wrpWord"><a href="#"><img class="fade"
												src="../images/clipCoupon.png" alt="clip coupon" width="140"
												height="30" /> </a> </span></td>
								</c:if>
								<c:if test="${requestScope.requestType eq 'expired' }">
									<td colspan="3" align="left"><a href="#"><img
											class="fade" src="../images/markAsUsed.png"
											alt="mark as used" width="140" height="30" /> </a><span
										class="wrpWord"><a href="#"><img class="fade"
												src="../images/clipCoupon.png" alt="clip coupon" width="140"
												height="30" /> </a> </span></td>
								</c:if>
								<c:if test="${requestScope.requestType eq 'all' }">
									<c:if test="${requestScope.coupondetails.couponInfo.usedFlag eq '0' }">
										<td colspan="3" align="left">
											<div id="cpnTg${requestScope.coupondetails.couponInfo.couponId}">
												<a href="#">
													<img class="fade" src="../images/markAsUsed.png" alt="mark as used" width="140" height="30" /> 
												</a>
												<span class="wrpWord">
												<a href="#">
													<img src="../images/clipCoupon.png"	alt="clip coupon" width="140" height="30" onclick="addCoupon(${requestScope.coupondetails.couponInfo.couponId},'${requestScope.added}','cpnTg'+${requestScope.coupondetails.couponInfo.couponId},'reedem')" />
												</a> </span>
											</div>
										</td>
									</c:if>
									<c:if test="${requestScope.coupondetails.couponInfo.usedFlag eq '1' }">
										<td colspan="3" align="left">
											<div id="cpnTg${requestScope.coupondetails.couponInfo.couponId}">
												<a href="#">
													<img src="../images/markAsUsed.png" alt="mark as used" width="140" height="30" onclick="userRedeemCLR('coupon')" /> 
												</a>
												<span class="wrpWord">
												<a href="#">
													<img class="fade" src="../images/clipCoupon.png"	alt="clip coupon" width="140" height="30" />
												</a> </span>
											</div>
										</td>
									</c:if>	
									<c:if test="${requestScope.coupondetails.couponInfo.usedFlag eq '2' }">
										<td colspan="3" align="left">
											<div id="cpnTg${requestScope.coupondetails.couponInfo.couponId}">
												<a href="#">
													<img class="fade" src="../images/markAsUsed.png" alt="mark as used" width="140" height="30" /> 
												</a>
												<span class="wrpWord">
												<a href="#">
													<img class="fade" src="../images/clipCoupon.png"	alt="clip coupon" width="140" height="30" />
												</a> </span>
											</div>
										</td>
									</c:if>	
								</c:if>
							</tr>
							<tr>
								<td colspan="3" align="left" class="redeemRow"><div
										class="redeemIcon">&nbsp;</div></td>
							</tr>
						</table>

						<div id="actionSheet">
							<div class="actnCncl" align="center">
								<a href="#"><img src="../images/actionSheetCancel.png"
									alt="cancel" title="cancel" name="actnCncl" /> </a>
							</div>
							<ul class="actnShtLst">
								<li><a href="#" onclick="fbShareCLRDetails('C')">facebook</a>
								</li>
								<li><a href="#" onclick="twitterShareCoupon();">twitter</a>
								</li>
								<li><a href="#"
									onclick="openEMailSharePopUp(${requestScope.coupondetails.couponInfo.couponId},'coupon')">send
										mail</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../leftmenu.jsp"></jsp:include>
			<div class="clear"></div>
		</div>
		<div class="tabBar">
			<ul id="sLTab">
				<li><a href="wishlistdisplay.htm"><img
						src="../images/tab_btn_up_goTowishlist.png" alt="coupongalry"
						width="80" height="50" /> </a></li>
				<li><a href="shoppingList.htm"><img
						src="../images/tab_btn_up_shpngLst.png" alt="shoppingLst"
						width="80" height="50" /> </a></li>
				<li><a href="coupongallery.htm"><img
						src="../images/tab_btn_up_cpnGlry.png" alt="mycoupons" width="80"
						height="50" /> </a></li>
				<li><a href="#" id="actSht"><img
						src="../images/tab_btn_up_rateshare.png" alt="rateshare"
						width="80" height="50" /> </a></li>
			</ul>
		</div>
	</form:form>
</div>
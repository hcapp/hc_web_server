<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript">
function twitterShareCoupon() {

	var tweetText;
	var couponURL = '${requestScope.coupondetails.couponInfo.couponURL}';

	var couponName = '${requestScope.coupondetails.couponInfo.couponName}';
	if (null != couponURL&&couponURL!='NotApplicable'&&couponURL!="") {

		tweetText = 'Great find at ScanSee!!! ' + couponName +" "+ "Start Date:"
				+ '${requestScope.coupondetails.couponInfo.couponStartDate}'+" "
				+ 'Expiry Date:'
				+ '${requestScope.coupondetails.couponInfo.couponExpireDate}'
				+ "  Visit:" + couponURL

	} else {
		tweetText = 'Great find at ScanSee!!! ' + couponName+" " + "Start Date:"
				+ '${requestScope.coupondetails.couponInfo.couponStartDate}'+" "
				+ 'Expiry Date:'
				+ '${requestScope.coupondetails.couponInfo.couponExpireDate}'

	}

	var tweeturl = 'http://twitter.com/share?text=' + escape(tweetText);

	window.open(tweeturl);

}
</script>
<div id="content" class="topMrgn">
<form:form commandName="clrForm" name="clrForm">
<form:hidden path="couponId"  value="${requestScope.coupondetails.couponInfo.couponId}"/>
<form:hidden path="rebateId"/>
<form:hidden path="loyaltyDealID"/> 
<form:hidden path="requestType"/>
<form:hidden path="lowerLimit"/>
<form:hidden path="pageFlowType"/>
<form:hidden path="clrType"/>    
<form:hidden path="added"/>  
    <div class="navBar">
      <ul>
        <li><a  onclick="getNextClrRecords('')"><img src="../images/backBtn.png"  title="Back"/></a></li>
        <li class="titletxt">${requestScope.coupondetails.couponInfo.couponName}</li>
        <!--<li class="floatR mrgnRt"><a href="index.html"><img src="images/logoutBtn.png" alt="logout" /></a></li>-->
      </ul>
    </div>
    <div class="clear"></div>
    <div class="mobCont noBg">
      <div class="splitDsply floatR">
        <div class="fluidViewHD">
          <div class="stretch cmnPnl relatvDiv">
             <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
             	<tr class="noBg">
                	<td colspan="3" align="center"><img src="${requestScope.coupondetails.couponInfo.couponImagePath}"  width="105" height="73" onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';"/></td>
              	</tr>
              	<tr>
                	<td>&nbsp;</td>
                	<td width="10%">Start Date:</td>
                	<td width="60%" class="wrpWord">${requestScope.coupondetails.couponInfo.couponStartDate}</td>
              	</tr>
              	<tr>
                	<td>&nbsp;</td>
                	<td width="10%">Expiry Date:</td>
                	<td width="60%" class="wrpWord">${requestScope.coupondetails.couponInfo.couponExpireDate}</td>
              	</tr>
              	<tr>
                	<td colspan="3" align="left" class="iphoneTitle">PRODUCTS:</td>
               </tr>
               <tr>
                <td width="4%" align="center">&nbsp;</td>
                <td width="96%" class="wrpWord" colspan="2">
                		<span> 
                		<c:forEach items="${requestScope.coupondetails.productLst}" var="products">
                   						${products.productName} 
                   	    </c:forEach> 
                   	   </span>
               </td>
              </tr>
              <tr>
                <td colspan="3" align="left" class="iphoneTitle">DETAILS:</td>
               
               </tr>
               
               <tr>
                <td align="center">&nbsp;</td>
                <td class="wrpWord" colspan="2"><p class="description">${requestScope.coupondetails.couponInfo.couponLongDescription} </p></td>
              </tr>
              
               
               
               <tr>
               
                  <td align="center">&nbsp;</td>
              <td class="wrpWord" colspan="2">
                <c:if test="${requestScope.expired eq 'expired' || requestScope.used eq 'used' }">
                <img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map" />
                
                </c:if>
                </td>
                
                
              </tr>
              
                <c:choose>
                <c:when test="${requestScope.requestType eq 'myGallery'}">
                <tr>
                <td align="center">&nbsp;</td>
                	<td class="wrpWord" colspan="2">
                	
                	<c:if test="${requestScope.addedFlag eq '0' || requestScope.addedFlag eq ''}">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map"  onclick="userRedeemCLR('coupon','${requestScope.addedFlag}')"/>
                	</c:if>
                	<c:if test="${requestScope.addedFlag eq '1'}">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map" />
                	</c:if>
                	</td>
                </tr>
                </c:when>
                <c:otherwise>
                <c:if test="${requestScope.requestType eq 'all'}">
                <td class="wrpWord" colspan="2">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map" />
                	</td>
                </c:if>
                </c:otherwise>
                </c:choose>
                
             
              
            </table>
            
            <div id="actionSheet">
            	<div class="actnCncl" align="center"><a href="#"><img src="../images/actionSheetCancel.png" alt="cancel" title="cancel" name="actnCncl" /></a></div>
            	<ul class="actnShtLst">
                	<li><a href="#" onclick="fbShareCLRDetails('C')">facebook</a></li>
                    <li><a href="#" onclick="twitterShareCoupon();">twitter</a></li>
                    <li><a href="#" onclick="openEMailSharePopUp(${requestScope.coupondetails.couponInfo.couponId},'coupon')">send mail</a></li>
                </ul>
            </div>
          </div>
        </div>
      </div>
      <jsp:include page="../leftmenu.jsp"></jsp:include>
      <div class="clear"></div>
    </div>
  <div class="tabBar">
  <c:choose>
 
  <c:when test="${requestScope.expired eq 'expired' || requestScope.used eq 'used' }">
   <ul id="sLTab">
        <li> <a ><img src="../images/tab_btn_up_wishlist.png"  title="Wish List" width="80" height="50" /></a></li>
        <li ><a  ><img src="../images/tab_up_add_cpns.png"  width="80" height="50" "/></a></li>
        <li><a href="coupongallery.htm"><img src="../images/tab_btn_up_cpnGlry.png"  title="Gallery" width="80" height="50"  /></a></li>
        <li> <a ><img src="../images/tab_btn_up_rateshare.png"  title="Rate/Share" width="80" height="50" /></a></li>
     
  
  </ul>
  </c:when>
  <c:otherwise>
    <ul id="sLTab"> 
     	
    <li> <a href="#"><img src="../images/tab_btn_up_wishlist.png" title="Wish List" width="80" height="50" onclick="addWishListWithProduct('${requestScope.productIdWithComma}')" /></a></li>
     
                    <c:if test="${requestScope.addedFlag == '0'}">
                	 <li id="myAjax"><a href="#" ><img src="../images/tab_up_add_cpns.png"  width="80" height="50" onclick ="addCLR(${requestScope.coupondetails.couponInfo.couponId},'coupon','${requestScope.addedFlag}')"/></a></li>
                	</c:if>
                	<c:if test="${requestScope.addedFlag == '1' || requestScope.addedFlag eq ''}">
                	 <li class="noHvr"><a ><img src="../images/tab_up_add_cpns.png"  width="80" height="50" /></a></li>
                	</c:if>
       
        <li><a href="coupongallery.htm"><img src="../images/tab_btn_up_cpnGlry.png"  title="Gallery" width="80" height="50"  /></a></li>
        <li> <a href="#" id="actSht"><img src="../images/tab_btn_up_rateshare.png"  title="Rate/Share" width="80" height="50" /></a></li>
    </ul> 
   </c:otherwise> 
    
   </c:choose>   
      
  </div>
  </form:form>
  </div>
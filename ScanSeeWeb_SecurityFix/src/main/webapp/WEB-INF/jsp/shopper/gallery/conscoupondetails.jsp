<%@page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>
<body onresize="resizeDoc();" onload="resizeDoc();">

<script type="text/javascript">
	function printCoupon() 
	{
		var couponName = "${requestScope.coupondetails.couponInfo.couponName}";
		var html = "<html><body><table align='center'><tr><td><img src='../images/scansee-logo.png'/></td></tr><tr><td>";
		html += '${requestScope.coupondetails.couponInfo.couponImagePath}' + "</td></tr>";
		html += "<tr><td> Coupon Name: "
				+ couponName + "</td></tr>";
		html += "<tr><td> Coupon Start Date: '${requestScope.coupondetails.couponInfo.couponStartDate}'</td></tr>";
		html += "<tr><td> Coupon End Date: '${requestScope.coupondetails.couponInfo.couponExpireDate}'</td></tr></table>"
		html += "</body></html>";

		var printWin = window.open('', '_blank');
		printWin.document.write(html);
		printWin.document.close();
		printWin.focus();
		printWin.print();
		printWin.close();

	}
	function sharePin(couponId){
		var base = "http://pinterest.com/pin/create/button/";
		$.ajaxSetup({cache:false});
		$.ajax({
		type : "GET",
		url : "/ScanSeeWeb/shopper/pinterestShareCouponInfo.htm",
		data : {
			'couponId'  : couponId
		},
		success : function(response) {
				var responseJSON = JSON.parse(response);
				var encodedpinurl=responseJSON.couponURL;
				encodedpinurl=encodedpinurl.replace("+", "%2B");
			    encodedpinurl=encodedpinurl.replace("/", "%2F");		
				var prodName=responseJSON.couponName;
				var encodedproductName=escape(prodName);
	            encodedproductName=encodedproductName.replace("+", "%2B");
	            encodedproductName=encodedproductName.replace("/", "%2F");
	            var encodedimagePath=responseJSON.couponImagePath;
	            encodedimagePath=encodedimagePath.replace("+", "%2B");
	            encodedimagePath=encodedimagePath.replace("/", "%2F");
	            finalUrl = base + "?url=" + encodedpinurl + "&media=" + encodedimagePath + "&description=" + encodedproductName;
	            inner = "<a href='" + finalUrl + "'class='pin-it-button' count-layout='none' target='_blank'><img id='pin' src='/ScanSeeWeb/images/consumer/pin_down.png' alt='pinterest'title='Pinterest'/></a>";
	            $("#pin_it").html(inner);
	  			$("#pin").click();
		},
		error : function(e) {
			alert('Error Occured');
		}
		});
	}
	
	function twitterShareCoupon() {
		var tweetText;
		var couponURL = "${requestScope.coupondetails.couponInfo.couponURL}";
		var couponName = "${requestScope.coupondetails.couponInfo.couponName}";
		if (null != couponURL&&couponURL!='NotApplicable' && couponURL!="") 
		{
			tweetText = 'Great find at ScanSee!!! ' + couponName +" "+ "Start Date:"
						+ '${requestScope.coupondetails.couponInfo.couponStartDate}'+" "
						+ 'Expiry Date:'
						+ '${requestScope.coupondetails.couponInfo.couponExpireDate}'
						+ "  Visit:" + couponURL

		} else 
		{
			tweetText = 'Great find at ScanSee!!! ' + couponName+" " + "Start Date:"
						+ '${requestScope.coupondetails.couponInfo.couponStartDate}'+" "
						+ 'Expiry Date:'
						+ '${requestScope.coupondetails.couponInfo.couponExpireDate}'

		}
		var tweeturl = 'http://twitter.com/share?text=' + escape(tweetText);
		window.open(tweeturl);
	}

	function back(){	
		document.clrForm.action = "getpagedclr.htm";
		document.clrForm.method = "POST";
		document.clrForm.submit();
	   
	}
	
	function addCoupon(){
		var couponId = ${requestScope.coupondetails.couponInfo.couponId};
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "consaddcoupon.htm",
			data : {
				'couponIds' : couponId			
			},

			success : function(response) {				
				alert("Coupon Added Successfully");
				document.clrForm.action = "conscoupondetails.htm";
				document.clrForm.method = "POST";
				document.clrForm.submit();
			},
			error : function(e) {
				alert('error')
			}
		});
	}
	
	function deleteCoupon(url){
		var couponId = ${requestScope.coupondetails.couponInfo.couponId};
		lowerLimit = document.clrForm.lowerLimit.value;
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "consdeletecoupons.htm",
			data : {
				'couponIds' : couponId			
			},
			success : function(response) {
				alert('Coupon deleted successfully');
				document.clrForm.lowerLimit.value = lowerLimit;				
				document.clrForm.action = url;
				document.clrForm.method = "GET";
				document.clrForm.submit();
			},
			error : function(e) {
				alert('error')
			}
		});
	}
	function redeemCoupon(){
		var couponId = ${requestScope.coupondetails.couponInfo.couponId};
		lowerLimit = document.clrForm.lowerLimit.value;
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "consredeemcoupon.htm",
			data : {
				'couponIds' : couponId			
			},

			success : function(response) {
				alert("Coupon Redeemed Successfully");
				document.clrForm.lowerLimit.value = lowerLimit;
				<c:if test="${requestScope.couponType eq 'Clipped'}">
					document.clrForm.action = "consmygallery.htm";
					document.clrForm.method = "GET";
				</c:if>
				<c:if test="${requestScope.couponType eq 'All'}">
					document.clrForm.action = "conscoupondetails.htm";
					document.clrForm.method = "POST";
				</c:if>
				document.clrForm.submit();
			},
			error : function(e) {
				alert('error')
			}
		});
	}
		
	function open_in_new_tab(url )
	{
		window.open(url, '_blank');
		window.focus();
	}
	function myGallery()
	{		
		document.clrForm.action = "consmygallery.htm";
		document.clrForm.method = "GET";
		document.clrForm.submit();
	}
	function galleryUsed()
	{			
		document.clrForm.action = "consgalleryused.htm";
		document.clrForm.method = "GET";
		document.clrForm.submit();
	}
	function galleryExpired()
	{			
		document.clrForm.action = "consgalleryexp.htm";
		document.clrForm.method = "GET";
		document.clrForm.submit();
	}
	function galleryAll()
	{			
		document.clrForm.action = "consgalleryall.htm";
		document.clrForm.method = "GET";
		document.clrForm.submit();
	}
</script>
<div class="clear"></div>
<div id="contWrpr" class="">
	<form:form commandName="clrForm" name="clrForm">
		<form:hidden path="couponId" value="${requestScope.coupondetails.couponInfo.couponId}" />
		<form:hidden path="clrImagePath" value="${requestScope.coupondetails.couponInfo.couponImagePath}"/>
		<form:hidden path="rebateId" />
		<form:hidden path="loyaltyDealID" />
		<form:hidden path="requestType" value="${requestScope.couponType}"/>
		<form:hidden path="lowerLimit" value="${requestScope.lowerLimit}"/>
		<form:hidden path="pageFlowType" />
		<form:hidden path="clrType" />
		<form:hidden path="added" />
		<form:hidden path="viewableOnWeb"/>
		<form:hidden path="returnURL" value="conscoupondetails.htm"/>
		<form:hidden path="couponName" value="${requestScope.coupondetails.couponInfo.couponDiscountAmount} ${requestScope.coupondetails.couponInfo.couponName}"/>
		<input type="hidden" name="emailId" id="emailId" value="${sessionScope.emailId}" />
		<div class="breadCrumb">
			<ul>
				<li class="brcIcon">
					<img src="../images/consumer/mg_bcIcon.png" alt="mygallery" />
				</li>
				<li title="My Gallery">
					<a href="consmygallery.htm">My Gallery</a>
				</li>			
				<c:if test="${requestScope.couponType eq 'Clipped'}">
				<li title="Clipped">	
					<a href="#" onclick="myGallery();">Clipped</a>
				</li>
				</c:if>
				<c:if test="${requestScope.couponType eq 'Used'}">
				<li title="Used">
					<a href="#" onclick="galleryUsed();">Used</a>
				</li>
				</c:if>
				<c:if test="${requestScope.couponType eq 'Expired'}">
				<li title="Expired">
					<a href="#" onclick="galleryExpired();">Expired</a>
				</li>
				</c:if>
				<c:if test="${requestScope.couponType eq 'All'}">
				<li title="All">
					<a href="#" onclick="galleryAll();">All</a>
				</li>
				</c:if>
				<li class="active">
					${requestScope.couponName} 
				</li>
			</ul>
			<span class="rtCrnr">&nbsp;</span> 
		</div>
		<div class="contBlks" id="cpnDetails">
			<div class="imgView splitView contBox relative">				
				<c:if test="${!empty requestScope.coupondetails.couponInfo.couponImagePath}">
				<img src="${requestScope.coupondetails.couponInfo.couponImagePath}" alt="planters" class="mrgnTopSmall" onerror="this.src = '../images/consumer/noImg.png';"  width="160px" height="160px"/>
				</c:if>					
				<c:if test="${empty requestScope.coupondetails.couponInfo.couponImagePath}">
				<img src='/ScanSeeWeb/images/consumer/noImg.png' alt="planters" class="mrgnTopSmall" width="160px" height="160px"/>
				</c:if>
				<c:if test="${requestScope.couponType ne 'Expired'}">
				<div align="center" class="contrlStrip zeroBrdr">
					<span> 
						<img alt="facebook" src="../images/consumer/fb_down.png" onclick="fbShareCLRDetails('C','${requestScope.coupondetails.couponInfo.couponId}')" title="Facebook"/> 
						<img alt="twitter" src="../images/consumer/twitter_down.png" onclick="twitterShareCoupon();" title="Twitter"/> 
						<c:if test="${requestScope.coupondetails.couponInfo.couponImagePath ne null && !empty requestScope.coupondetails.couponInfo.couponImagePath}">
							<span id="pin_it">
							<img src='/ScanSeeWeb/images/consumer/pin_down.png' onclick="sharePin(${requestScope.coupondetails.couponInfo.couponId})" alt='pinterest' title="Pinterest"/>
							</span>
						</c:if>
						<img class="couponEmailActn" name="${requestScope.coupondetails.couponInfo.couponId}" alt="email" src="../images/consumer/email_down.png" title="Email"/>
					</span>
				</div>	
				</c:if>
				<c:if test="${requestScope.coupondetails.couponInfo.couponProductExist eq true }">
				<div class="min-margin-top"><input type="button" value="Qualifying Products  >" class="hltBtn" onclick="getcoupassoiatedprods(${requestScope.coupondetails.couponInfo.couponId})"></div>
				
				</c:if>
				 
			</div>	
			
			<div class="contView splitView">
				<div class="subHdr-control">
					<h3 class="zeroBg sctnHdr">
					<b>
						${requestScope.coupondetails.couponInfo.couponDiscountAmount} off ${requestScope.coupondetails.couponInfo.couponName} </b>
						<span>
							<c:if test="${requestScope.couponType eq 'All' && requestScope.coupondetails.couponInfo.usedFlag == 0}">
							<i class="cpn" style="display: none;">
								Clip Coupon
							</i>
							</c:if>
							<c:if test="${requestScope.couponType eq 'Clipped' || 
							(requestScope.couponType eq 'All' && requestScope.coupondetails.couponInfo.usedFlag eq '1')}">
							<i class="cpn" style="display: none;">
								Redeem Coupon
							</i>
							</c:if>
							<i class="print" style="display: none;">Print Coupon</i>
							<i class="external" style="display: none;">Get Coupon</i>
							<c:if test="${requestScope.couponType ne 'Expired' && requestScope.couponType ne 'All'}">
							<i class="Trash" style="display: none;">Delete Coupon</i>
							</c:if>
							<c:if test="${requestScope.couponType eq 'All' && requestScope.coupondetails.couponInfo.usedFlag == 0}">
							<a href="#" class="icon-bg">
								<img src="../images/consumer/cpnIcon.png" alt="cpn" width="20" height="20" onclick="addCoupon()"/>
							</c:if>
							<c:if test="${(requestScope.couponType eq 'All' && requestScope.coupondetails.couponInfo.usedFlag == 1) || requestScope.couponType eq 'Clipped'}">
							<a href="#" class="icon-bg">
								<img src="../images/consumer/cpnIcon.png" alt="cpn" width="20" height="20" onclick="redeemCoupon()"/>
							</c:if>
							<c:choose>
							<c:when test="${requestScope.couponType ne 'Expired' && null != requestScope.coupondetails.couponInfo.couponURL && !empty requestScope.coupondetails.couponInfo.couponURL}"> 
								<a class="icon-bg" href="${requestScope.coupondetails.couponInfo.couponURL}" target="_blank">
									<img width="20" height="20" alt="external" src="../images/consumer/button_coupon_online.png">
								</a>
							</c:when>
							<c:otherwise>
								<c:if test="${requestScope.couponType ne 'Expired'}">
								<a class="icon-bg" href="javascript:void(0);">
									<img width="20" height="20" alt="print" src="../images/consumer/print.png" onclick='printCoupon();'>
								</a>
								</c:if>
							</c:otherwise>
							</c:choose>
							<c:if test="${requestScope.couponType eq 'Clipped'}">
							<a class="icon-bg" href="#">
								<img width="20" height="20" alt="Trash" src="../images/consumer/trash.png" onclick="deleteCoupon('consmygallery.htm')"/>
							</a>
							</c:if>
							<c:if test="${requestScope.couponType eq 'Used'}">
							<a class="icon-bg" href="#">
								<img width="20" height="20" alt="Trash" src="../images/consumer/trash.png" onclick="deleteCoupon('consgalleryused.htm')"/>
							</a>
							</c:if>
							
						</span>
					</h3>
				</div>
				<dl>
					<dt>Title: </dt>
					<dd>${requestScope.coupondetails.couponInfo.couponName}</dd>
					<c:if test="${!empty requestScope.coupondetails.couponInfo.retailName && requestScope.coupondetails.couponInfo.retailName ne null}">
					<dt>Retailer : </dt>
					<dd>
						${requestScope.coupondetails.couponInfo.retailName},
						${requestScope.coupondetails.couponInfo.address},
						${requestScope.coupondetails.couponInfo.city},
						${requestScope.coupondetails.couponInfo.state},
						${requestScope.coupondetails.couponInfo.postalCode}
					</dd>
					</c:if>
					<c:if test="${!empty requestScope.coupondetails.couponInfo.couponShortDescription && requestScope.coupondetails.couponInfo.couponShortDescription ne null}">
					<dt>Short Description: </dt>           
					<dd>${requestScope.coupondetails.couponInfo.couponShortDescription}</dd>
					</c:if>
					<c:if test="${!empty requestScope.coupondetails.couponInfo.couponLongDescription && requestScope.coupondetails.couponInfo.couponLongDescription ne null}">
					<dt>Long Description: </dt>
					<dd>${requestScope.coupondetails.couponInfo.couponLongDescription}</dd>
					</c:if>
					<c:if test="${!empty requestScope.coupondetails.couponInfo.couponStartDate && requestScope.coupondetails.couponInfo.couponStartDate ne null}">
					<dt>Start Date:</dt>
					<dd>${requestScope.coupondetails.couponInfo.couponStartDate} </dd>
					</c:if>
					<c:if test="${!empty requestScope.coupondetails.couponInfo.couponExpireDate && requestScope.coupondetails.couponInfo.couponExpireDate ne null}">
					<dt>End Date:</dt>
					<dd>${requestScope.coupondetails.couponInfo.couponExpireDate} </dd>
					</c:if>
				</dl>
			</div>
		</div>
	</form:form>
</div>
<body>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>



<table width="100%" class="gnrlGrd detView" id="" border="0"
	cellSpacing="0" cellPadding="0">

	<tr>
		<c:if test="${sessionScope.pagination.totalSize >0}">
			<td colspan="3">Search Results for:
				${sessionScope.scannowform.searchKey}<b><font size=3> </font> </b></td>

		</c:if>

	</tr>

	<tr>
		<c:if
			test="${sessionScope.pagination.totalSize >0 && sessionScope.pagination.currentPage==1 && sessionScope.pagination.totalSize > 20}">
			<td colspan="3">${sessionScope.pagination.totalSize} results
				found, top ${sessionScope.pagination.pageRange} displayed
			</td>
		</c:if>
	</tr>

	<tr>
		<c:if
			test="${sessionScope.pagination.totalSize >0 && sessionScope.pagination.currentPage==1 && sessionScope.pagination.totalSize <=20 }">
			<td colspan="3">${sessionScope.pagination.totalSize} results
				found
			</td>
		</c:if>
	</tr>
	
	


	<c:choose>
		<c:when test="${requestScope.searchresults ne null}">


			<c:forEach items="${searchresults.productDetail}" var="prodDetail">

				<tr style="cursor: pointer;" class="noHvr">

					<td width="7%" align="center"><img
						src="${prodDetail.imagePath}" alt="Image" width="38" height="38" />
					</td>
					<td 
						onclick="callScannowProductSummary(${prodDetail.productId})">
						<span><c:out
							value="${prodDetail.productName}" /></span>
							


<c:choose>		  
<c:when test="${prodDetail.productShortDescription ne null || 
				!empty prodDetail.productShortDescription
	|| prodDetail.productShortDescription ne 'NotApplicable'
}">
<c:out value="${prodDetail.productShortDescription}" />
</c:when>
<c:otherwise>
<c:out value="${prodDetail.productLongDescription}" />
</c:otherwise>

	</c:choose>		
							</td>
					<td width="5%"><img alt="Arrow" src="../images/rightArrow.png"
						height="15" />
					</td>

				</tr>
			</c:forEach>

		</c:when>
		<c:otherwise>
			<span id="NoDataInfo" >No Product found!</span>
		</c:otherwise>
	</c:choose>
</table>






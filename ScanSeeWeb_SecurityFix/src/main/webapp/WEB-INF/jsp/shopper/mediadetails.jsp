<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;">
	<!--<h3>List of Retailers Based on Search Criteria</h3>-->

	 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd" id="findPnl">
			<c:forEach items="${requestScope.mediaDetails.productDetail}"
				var="item">
				<tr>
					<td width="30%" ><a href="${item.productMediaPath}">${item.productMediaName}</a></td>
				</tr>
			</c:forEach>
	 </table>
</div>
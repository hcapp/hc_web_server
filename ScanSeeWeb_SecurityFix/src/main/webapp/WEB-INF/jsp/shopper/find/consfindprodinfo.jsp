<!--This page used to display the   find  search product details ,it includes the product summary details. -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product Summary page</title>
</head>
<body>

	<div id="contWrpr">
		<form name="consfindprodfrom" commandName="consfindprodfrom">
			<div class="breadCrumb">
				<ul>
				
				<c:choose>
				<c:when test="${requestScope.breadCrumLink  eq 'SL'}">
				
				<li class="brcIcon"><img
						src="/ScanSeeWeb/images/consumer/sl_bcIcon.png" alt="find" /></li>
					<li><a href="consdisplayslprod.htm">ShoppingList</a></li>
				</c:when>
				<c:when test="${requestScope.breadCrumLink  eq 'WL'}">
				
				<li class="brcIcon"><img
						src="/ScanSeeWeb/images/consumer/wl_bcIcon.png" alt="wishlist" /></li>
					<li><a href="conswishlisthome.htm">Wish List</a></li>
				</c:when>
				<c:when test="${requestScope.breadCrumLink  eq 'CG'}">
				
				<li class="brcIcon"><img
						src="/ScanSeeWeb/images/consumer/mg_bcIcon.png" alt="find" /></li>
					<li><a href="consmygallery.htm">My Gallery</a></li>
				</c:when>
				<c:otherwise>
				<li class="brcIcon"><img
						src="/ScanSeeWeb/images/consumer/find_bcIcon.png" alt="find" /></li>
					<li><a href="consfindhome.htm?searchType=Products">Find</a></li>

				</c:otherwise>
				</c:choose>	
				
				
				
				<c:choose>
					<c:when test="${requestScope.breadCrumLink  eq 'SL'}">
				<li><a href="javascript:void(0);"
						onclick="javascript:history.go(-1);"> Products</a></li>
				</c:when>
				<c:when test="${requestScope.breadCrumLink  eq 'WL'}">
				<li><a href="javascript:void(0);"
						onclick="javascript:history.go(-1);"> Products</a></li>
				</c:when>
				<c:when test="${requestScope.breadCrumLink  eq 'CG'}">
				<li><a href="javascript:void(0);"
						onclick="javascript:history.go(-1);"> Products</a></li>
				</c:when>
				<c:when test="${requestScope.shareEmailFlag=='No'}">
				<li><a href="javascript:void(0);"
							onclick="findprodlstsearch()"> Products</a></li>
				</c:when>
				
			
				<c:otherwise>
							</c:otherwise>
				</c:choose>	
				
				
					<c:if test="${requestScope.shareEmailFlag=='No'}">
						
					</c:if>
					

					<li class="title productTrim active"
						title="${sessionScope.consproddetails.productName}"><c:out
							value="${sessionScope.consproddetails.productName}"></c:out>
					</li>
				</ul>
				<span class="rtCrnr">&nbsp;</span>
			</div>

		</form>
		<%@include file="consprodsummary.jsp"%>

	</div>
</body>
</html>
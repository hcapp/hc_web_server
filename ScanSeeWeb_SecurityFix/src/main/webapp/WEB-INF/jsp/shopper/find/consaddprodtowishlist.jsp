<!-- This page used to adding product to wish  list and displaying the response message in product summary  screen.-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/consumerstyle.css" />


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<body class="whiteBG">



	<c:if
		test="${requestScope.addedtowl ne null && !empty requestScope.addedtowl}">

		<c:if test="${requestScope.addedtowl eq 'added'}">
			<div class="zeroPdng">
				<img alt="close"
					src="/ScanSeeWeb/images/consumer/addtoWishList_poster.png"
					width="584" height="333" />

			</div>

		</c:if>


		<c:if test="${requestScope.addedtowl eq 'exists'}">
			<div class="zeroPdng">
				<img alt="close"
					src="/ScanSeeWeb/images/consumer/prodexist_poster.png" width="584"
					height="333" />

			</div>

		</c:if>


	</c:if>




</body>
</html>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="/ScanSeeWeb/styles/consumerstyle.css" type="text/css"
	rel="stylesheet" />
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(
			function() {

				var selectSearchType = '${requestScope.searchType}'
				var selectCategory = '${requestScope.searchCategory}'
				if (selectSearchType == 'Products') {
					$("#Products").prop("checked", true)
				} else if (selectSearchType == 'Locations') {
					$("#Locations").prop("checked", true)
					$('#tglDsply').slideToggle('normal');

					$("#RtlrCtgry input[value = '" + selectCategory + "']")
							.prop('checked', true).click();

				} else if (selectSearchType == 'Deals') {
					$("#Deals").prop("checked", true)
				}

			});

	function isNumeric(vNum) {

		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;
		var vPostal = document.getElementById("fndzipcode");
		for (i = 0; i < vNum.length && IsNumber == true; i++) {
			Char = vNum.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				vPostal.value = "";
				vPostal.focus();
				IsNumber = false;
			}
		}
		return IsNumber;
	}
</script>
<form:form name="SearchForm" commandName="SearchForm">
	<form:hidden path="recordCount" />
	<form:hidden path="categoryName" />
	<div id="contWrpr" class="relative">
		<div class="breadCrumb">
			<ul>
				<li class="brcIcon"><img
					src="/ScanSeeWeb/images/consumer/find_bcIcon.png" alt="find"  />
				</li>
				<li class="leftPdng">Find</li>
				<!--  <li><a href="#">Products</a></li>-->

			</ul>
			<span class="rtCrnr">&nbsp;</span>
		</div>
		<div class="contBlks setHeight">
			<table width="100%" border="0" align="left" cellpadding="0"
				cellspacing="0" class="brdrLsTbl topMrgnBig" id="findCntrl">
				<tr>
					<td width="28%">&nbsp;</td>
					<td width="4%"><label class="bold">Find</label>
					</td>
					<td width="25%"><form:input path="searchKey" type="text"
							id="srch-find" title="Enter keyword to search" tabindex="1"
							onkeypress="return validateFind(event)" />
					</td>
					<td width="35%"><form:input type="text" path="zipCode"
							id="zipCode" placeholder="Zipcode" class="inputSmaller"
							title="Enter zipcode" maxlength="5" tabindex="2"
							onkeypress="return isNumberKey(event)"
							onchange="isNumeric(this.value);" /> <img
						src="/ScanSeeWeb/images/consumer/search.png" alt="search"
						tabindex="3" title="search" align="absmiddle" id="FindSearch"
						style="cursor: pointer" /></td>
					<td width="8%"></td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="3" align="">
						<div class="row srchOptns zeroPdng">
							<input name="srchOptns" type="radio" id="Products"
								value="Products" checked="checked" /> <label for="Products">Products</label>
							<input name="srchOptns" type="radio" value="Locations"
								id="Locations" /> <label for="Locations">Locations</label> <input
								name="srchOptns" type="radio" value="Deals" id="Deals" /> <label
								for="Deals">Deals</label>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="5" class=""></td>
				</tr>


			</table>
			<div class="options mrgnTopSmall" id="tglDsply">
				<ul id="RtlrCtgry">

					<c:if
						test="${requestScope.categoryInfo ne null &&  !empty requestScope.categoryInfo}">

						<c:forEach items="${requestScope.categoryInfo}" var="item">
							<li class=""><i></i> <input name=ctgryOptn type="radio"
								value="${item.catDisName}" /> <img src="${item.catImgPth}"
								alt="arts" /> <span>${item.catDisName}</span>
							</li>

						</c:forEach>

					</c:if>

				</ul>
			</div>
			<div class="clear"></div>
		</div>
		<div class="cstm-alert">
			Please enter search keyword<span class="alert-close">&nbsp;</span><i
				class="alert-nub"></i>
		</div>
	</div>
</form:form>

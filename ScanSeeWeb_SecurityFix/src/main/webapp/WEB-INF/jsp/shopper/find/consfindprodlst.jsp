<!-- This page used to display the find  search product list results  -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>

<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>



<script type="text/javascript">

function testEscape(prodName) {
 
var strInputString=	prodName;
strInputString=strInputString.replace(/'/g, "\\'");
return strInputString;

}


	//<!--Below script method is used for pagination record count implementation. -->
	function getPerPgaVal() {

	var selValue = $('#selPerPage :selected').val();
		document.FindProdLstForm.recordCount.value = selValue;
		//Call the method which populates grid values
		findprodlstdisplay();

	}
		//<!--Below script method is used for pagination implementation. -->
	function callNextPage(pagenumber, url) {
		var selValue = $('#selPerPage :selected').val();

		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}
	
//	<!--Below script method is used for sharing product information via twitter. -->
	function twitterShare(productId,productListId){

	
	$.ajaxSetup({cache:false});
	$.ajax({
	type : "GET",
	url : "/ScanSeeWeb/shopper/twitterShareProductInfo.htm",
	data : {
		'productId'  : productId,
		'productListId':productListId
	},
	success : function(response) {
		
		var tweetText;
		var shareURL=response;
		var splitRes=shareURL.split(";")
		var prodName=splitRes[1];
		prodName=testEscape(prodName);

		
		tweetText='Great find at ScanSee!  '+prodName+"  Visit:"+splitRes[0]

		var tweeturl = 'http://twitter.com/share?text='+escape(tweetText);

		window.open(tweeturl);	
		
	},
	error : function(e) {
		alert('Error Occured');
	}
});

}

//<!--Below script method is used for sharing product information via pinterest. -->
	
function sharePin(productId,i,productListId){
	var base = "http://pinterest.com/pin/create/button/";
	$.ajaxSetup({cache:false});
	$.ajax({
	type : "GET",
	url : "/ScanSeeWeb/shopper/pinterestShareProductInfo.htm",
	data : {
		'productId'  : productId,
		'productListId':productListId
	},
	success : function(response) {
			var responseJSON = JSON.parse(response);
			var encodedpinurl=responseJSON.productURL;
			encodedpinurl=encodedpinurl.replace("+", "%2B");
		    encodedpinurl=encodedpinurl.replace("/", "%2F");
		    encodedpinurl=encodedpinurl.replace("&", "%26");
		    encodedpinurl=encodedpinurl.replace("&", "%26");		
			var prodName=responseJSON.productName;
			var encodedproductName=escape(prodName);
            encodedproductName=encodedproductName.replace("+", "%2B");
            encodedproductName=encodedproductName.replace("/", "%2F");
            var encodedimagePath=responseJSON.productImagePath;
            encodedimagePath=encodedimagePath.replace("+", "%2B");
            encodedimagePath=encodedimagePath.replace("/", "%2F");
            finalUrl = base + "?url=" + encodedpinurl + "&media=" + encodedimagePath + "&description=" + encodedproductName;
            inner = "<a href='" + finalUrl + "'class='pin-it-button' count-layout='none' target='_blank'><img id='pin" + i + "' src='/ScanSeeWeb/images/consumer/pin_down.png' alt='pinterest' width='25' height='25'/></a>";
  			$("#pin_it_"+i).html(inner);
  			$("#pin"+i).click();
	},
	error : function(e) {
		alert('Error Occured');
	}
	});
}
 

</script>
<body onresize="resizeDoc();" onload="resizeDoc();">
	<div id="contWrpr">

		<form:form name="FindProdLstForm" commandName="FindProdLstForm">

			<input type="hidden" name="productListID" id="productListID" value="" />
			<input type="hidden" name="productId" id="productId" value="" />
			<input type="hidden" name="redirectUrl" id="redirectUrl" value="" />
			<input type="hidden" name="recordCount" id="recordCount" value="" />

			<input type="hidden" name="emailId" id="emailId"
				value="${sessionScope.userEmailId}" />


			<div class="breadCrumb">
				<ul>
					<li class="brcIcon"><img
						src="/ScanSeeWeb/images/consumer/find_bcIcon.png" alt="find" /></li>
					<li><a href="consfindhome.htm?">Find</a></li>
					<li><a href="consfindhome.htm?searchType=Products">Products</a>
					</li>
					<li class="active productTrim"><c:out
							value="${sessionScope.searchKey}"></c:out>
					</li>
					<c:if test="${sessionScope.searchresults ne null}">

						<li>( Showing <c:out value="${sessionScope.totalresults}"></c:out>
							results )</li>
					</c:if>
				</ul>
				<span class="rtCrnr">&nbsp;</span>
			</div>
			<div class="contBlks" id="cart">
				<ul id="imgslide">

					<c:choose>
						<c:when test="${sessionScope.searchresults ne null}">

							<c:set var="i" value="0" />
							<!--This for loop used to display the product details. -->
							<c:forEach items="${searchresults.productDetail}"
								var="prodDetail">
								<c:choose>

									<c:when test="${fn:length(prodDetail.imagelst) gt 1}">

										<li>
											<div class="contBox">
												<!--This for loop used to  display the product multiple images. -->

												<ins>
													<a href="javascript:void(0);" class="lt">&nbsp;</a>
												</ins>
												<ins>
													<a href="javascript:void(0);" class="rt">&nbsp;</a>
												</ins>
												<div align="center" class="imageBlock">
													<div class="imgWrp">
														<c:forEach items="${prodDetail.imagelst}" var='item'>


															<c:if test="${item ne null}">
																<a
																	href="/ScanSeeWeb/shopper/consfindprodinfo.htm?
																	key1=${prodDetail.productId}&key2=${prodDetail.productListID}"
																	title="${prodDetail.productName}"
																	class="${prodDetail.productId}"> <img src="${item}"
																	onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
																	alt="product1" width="200" height="150" /> </a>
															</c:if>



														</c:forEach>

													</div>
												</div>
									</c:when>
									<c:otherwise>
										<c:forEach items="${prodDetail.imagelst}" var='item'>

											<li><div class="contBox">
													<c:if test="${item ne null}">
														<a
															href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${prodDetail.productId}&key2=${prodDetail.productListID}"
															title="${prodDetail.productName}"
															class="${prodDetail.productId}"> <img src="${item}"
															onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
															alt="product1" width="200" height="150" /> </a>

													</c:if>
										</c:forEach>
									</c:otherwise>


								</c:choose>

								<p align="center" class="title">
									<a
										href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${prodDetail.productId}&key2=${prodDetail.productListID}"
										class="title dealtrim" title="${prodDetail.productName}">
										${prodDetail.productName} </a>
								</p>

								<p align="left" class="title">
									<span class="title proddesctrim"
										title="${prodDetail.productShortDescription}">
										${prodDetail.productShortDescription} </span>
								</p>
								<p></p>

								<span> <a href="javascript:void(0);"> <img
										src="/ScanSeeWeb/images/consumer/fb_down.png" alt="facebook"
										width="25" height="25"
										onClick="consprodfacebookshare(${prodDetail.productId},${prodDetail.productListID})" />
								</a> <img src="/ScanSeeWeb/images/consumer/twitter_down.png"
									width="25" height="25" alt="twitter"
									onClick="twitterShare(${prodDetail.productId},${prodDetail.productListID})" />
									<c:if
										test="${prodDetail.productImagePath ne null && !empty prodDetail.productImagePath}">
										<span id="pin_it_${i}" class="inline"> <img
											src='/ScanSeeWeb/images/consumer/pin_down.png'
											onclick="sharePin(${prodDetail.productId},${i},${prodDetail.productListID})"
											alt='pinterest' width="25" height="25" /> <c:set var="i"
												value="${i + 1}"></c:set> </span>
									</c:if> <img src="/ScanSeeWeb/images/consumer/email_down.png"
									width="25" height="25" class="emailActn"
									name="${prodDetail.productId},${prodDetail.productListID}"
									alt="email" /> </span>
			</div>
			</li>
			</c:forEach>
			</c:when>

			<c:otherwise>
				<div class="zeroBg contBlks ">
					<img alt="close"
						src="/ScanSeeWeb/images/consumer/noproducts_poster.png" />

				</div>


			</c:otherwise>
			</c:choose>
			</ul>
			<div class="clear"></div>
	</div>

	</div>
	</form:form>
	<c:if
		test="${sessionScope.searchresults ne null && !empty sessionScope.searchresults}">


		<div class="pagination brdrTop">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="noBrdr" id="perpage">
				<tr>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
						nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
						pageRange="${sessionScope.pagination.pageRange}"
						url="${sessionScope.pagination.url}" enablePerPage="true" />
				</tr>
			</table>
		</div>
	</c:if>

	<div class="clear"></div>
	</div>
</body>

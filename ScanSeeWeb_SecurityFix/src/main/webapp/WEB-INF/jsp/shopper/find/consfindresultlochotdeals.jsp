<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script type="text/javascript">
	function breadCrumbLoc() {

		var pagenumber = $('#currentPageNumber').val()
		var recordCount = $('#locSearchRecCount').val()
		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = recordCount;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = "findresultloctnlist.htm";
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}
	function showSpecialPage() {

		document.findLocSearchResultForm.action = "findresultloctnlistspecials.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}
	function showSalePage() {

		document.findLocSearchResultForm.action = "findresultloctnlistsales.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}

	function showHotDealPage() {

		document.findLocSearchResultForm.action = "findresultloctnlistdeals.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}
	function callNextPage(pagenumber, url) {
		var selValue = $('#selPerPage :selected').val();

		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}

	function getPerPgaVal() {

		var selValue = $('#selPerPage :selected').val();
		document.findLocSearchResultForm.recordCount.value = selValue;

		//Call the method which populates grid values
		searchRetLocDeals();

	}

	function searchRetLocDeals() {
		document.findLocSearchResultForm.action = "findresultloctnlistdeals.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}
</script>
<div id="contWrpr">
	<div class="breadCrumb">
		<ul>
			<li class="brcIcon"><img
				src="/ScanSeeWeb/images/consumer/find_bcIcon.png" alt="find" /></li>
			<li class=""><a href="consfindhome.htm">Find</a>
			</li>
			<li><a href="#" onclick="breadCrumbLoc();">Location</a>
			</li>
			<li class="active">${sessionScope.retailerInfo.retailerName }</li>
		</ul>
		<span class="rtCrnr">&nbsp;</span>
	</div>
	<div class="contBlks">
		<form:form name="findLocSearchResultForm"
			commandName="findLocSearchResultForm">
			<form:hidden path="recordCount" />
			<form:hidden path="zipCode" />
			<form:hidden path="searchKey" />
			<form:hidden path="categoryName" />
			<form:hidden path="retailerId" />
			<form:hidden path="retailLocationID" />
			<form:hidden path="retListID" />
			<form:hidden path="currentPageNumber" />
			<form:hidden path="locSearchRecCount" />
			<ul class="cstmAccordion">
				<c:choose>
					<c:when
						test="${sessionScope.retailerInfo.retailerURL ne null && sessionScope.retailerInfo.retailerURL!=''}">
						<li class="active  cstm-opt">
					</c:when>
					<c:when
						test="${sessionScope.retailerInfo.contactPhone ne null && sessionScope.retailerInfo.contactPhone!=''}">
						<li class="active  cstm-opt">
					</c:when>
					<c:otherwise>
						<li class="active">
					</c:otherwise>
				</c:choose>
				<dl>
					<dt class="cstm-img">
						<img src="${sessionScope.retailerInfo.logoImagePath}" alt="image"
							width="48" height="48"
							onerror="this.src='/ScanSeeWeb/images/consumer/imgIcon.png'" />
					</dt>
					<dt class="cstm-title">
						<span class="title">${sessionScope.retailerInfo.retailerName}</span><span
							class="subTitle">${sessionScope.retailerInfo.retaileraddress1},${sessionScope.retailerInfo.city},${sessionScope.retailerInfo.postalCode},${sessionScope.retailerInfo.state}</span>
						<c:if
							test="${sessionScope.retailerInfo.retailerURL ne null && sessionScope.retailerInfo.retailerURL!=''}">
							<span class="col_actn"><img
								src="/ScanSeeWeb/images/consumer/FNB_browseIcon.png" width="22"
								height="22" /> <a href="${sessionScope.retailerInfo.retailerURL}"
								target="_blank" class="innerLink">Browse Website </a> </span>
						</c:if>
						<c:if
							test="${sessionScope.retailerInfo.contactPhone ne null && sessionScope.retailerInfo.contactPhone!=''}">
							<span class="col_actn"><img
								src="/ScanSeeWeb/images/consumer/phone_icon.png" alt="call" width="24"
								height="24" /> <c:out value="${sessionScope.retailerInfo.contactPhone}"></c:out>
							</span>
						</c:if>
					</dt>
					<dt class="cstm-spcl">&nbsp;</dt>
					<dt class="cstm-rtarrow">&nbsp;</dt>
				</dl>
				<div class="cstm-Cont dsply-block zeroBrdr">
					<div class="ltPnl floatL stretch">
						<ul class="four-up icon-txt brdr-inset topBrdr" id="">
							<li><a href="#" onclick="showSalePage();" class="Sales"><img
									src="/ScanSeeWeb/images/consumer/salesTab-icon.png" alt="sales"
									align="absmiddle" />Sales</a></li>
							<li><a href="#" onclick="showSpecialPage();" class="Specials"><img
									src="/ScanSeeWeb/images/consumer/specialTab-icon.png" alt="specials"
									align="absmiddle" />Specials</a></li>
							<li class="active"><a href="#" onclick="showHotDealPage();"
								class="HotDeals"><img
									src="/ScanSeeWeb/images/consumer/hotdealTab-icon.png" alt="hotdeals"
									align="absmiddle" />Hot Deals</a></li>
							<li><a href="#" class="Coupons"><img
									src="/ScanSeeWeb/images/consumer/cpnTab-icon.png" alt="coupons"
									align="absmiddle" />Coupons</a></li>
						</ul>
						<div class="hotdeals cmnPnlShow">
							<c:choose>
								<c:when test="${requestScope.hotDealList eq null}">
									<img width="970" height="373" alt="none"
										src="/ScanSeeWeb/images/consumer/noneAvailable.png">
								</c:when>
								<c:otherwise>
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="cstmTbl">
										<c:forEach items="${requestScope.hotDealList}" var="item">
											<tr>
												<td width="8%"><img src="${item.hotDealImagePath}"
													onerror="this.src='/ScanSeeWeb/images/imageNotFound.png'"
													alt="image" width="48" height="48" />
												</td>
												<td width="86%"><a
													href="/ScanSeeWeb/shopper/conshddetails.htm?hotdealId=${item.hotDealID}&hdlstid=${item.hotDealListID}&page=SpclHotDeal">
														<c:out value="${item.hotDealName}"></c:out> </a></td>
												<td width="6%"><img src="/ScanSeeWeb/images/rightArrow.png"
													alt="arrow" />
												</td>
											</tr>
										</c:forEach>
									</table>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				</li>
			</ul>
		</form:form>
	</div>
	<div class="pagination brdrTop">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="noBrdr"
			id="perpage">
			<tr>
				<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
					nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
					pageRange="${sessionScope.pagination.pageRange}"
					url="${sessionScope.pagination.url}" enablePerPage="true" />
			</tr>
		</table>
	</div>
</div>
<div class="clear"></div>
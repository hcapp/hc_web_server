<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<!--<h3>List of Retailers Based on Search Criteria</h3>-->
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="mobGrd infoPnl zeroBtmMrgn">
	<thead>
		<tr>
			<th colspan="4">
				<ul class="tabdBtnTgl">

					<li><a 
						onclick="callWLLocalStores(${requestScope.productId})">Local</a></li>
					<li class="noRtbrdr active" onclick="">Online</li>

				</ul>
		</tr>

	</thead>


</table>
<c:choose>
	<c:when
		test="${requestScope.wishListOnlineStoresList eq null && empty requestScope.wishListOnlineStoresList}">

		<thead>
			<tr class="noHvr">
				<td>
				
				<span id="NoDataInfo" style="color: red; text-align: center;">
					No Online stores found!</span></td>
			</tr>
				</thead>
	</c:when>
	<c:when
		test="${requestScope.wishListOnlineStoresList ne null && !empty requestScope.wishListOnlineStoresList}">



		<!--<h3>List of local stores  Based on product </h3>-->

		<!--

   <tr>
          	  <td width="11%" class="mobGrd detView zeroBtmMrgn"><img src="images/imgIcon.png" alt="Img" width="38" height="38" /></td>
          	  <td colspan="2"><span>Product 1</span>
                  </td>
          	  </tr>

   -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="mobGrd detView zeroBtmMrgn" id="rtlrLst">
			<tr>
				<td colspan="4" class="mobSubTitle" align="center">Shopzilla</td>
			</tr>
			<c:forEach items="${requestScope.wishListOnlineStoresList}"
				var="item">

				<tr>

					<td colspan="2"><span><c:out
								value="${item.merchantName}" /> </span>
						<ul>
							<li><span>Old Price:</span> <c:out
									value="${item.originalPrice}" />
							</li>
							<li><span>Sale Price:</span> <c:out
									value="${item.salePrice}" />
							</li>
						</ul>



						<div class="newLnView">
							<span>URL:</span> <a href="<c:out value="${item.url}"/>"><c:out
									value="${item.merchantName}" /> </a>


						</div></td>
					<td width="3%">&nbsp;</td>


				</tr>


			</c:forEach>
		</table>
	
	</c:when>
</c:choose>
<input type="hidden" name="wishlist" id="wishlist" value="" />
		<input type="hidden" name="wlonlineratereview" id="wlonlineratereview" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
	function wlcouponOperation(couponId, usage,id) {
		// get the form values
	
        
$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "wlcouponOperation.htm",
			data : {
				'couponId' : couponId,
				'usage' : usage
			},

			success : function(response) {
			
				$('#'+id).replaceWith(response);
				
			
			
			},
			error : function(e) {
				
			}
		});

	
	}

	
	</script>



<div class="dataView" style="height: 450px; overflow-x: hidden;">

	<div class="dataView">


		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="mobGrd detView zeroBtmMrgn">




			<c:if
				test="${requestScope.couponlist ne null && !empty requestScope.couponlist}">

				<tr>
					<td colspan="4" class="mobSubTitle">Coupons</td>
				</tr>

				<c:forEach items="${requestScope.couponlist}" var="couponinfo">
					<tr>

						<td width="11%" align="center" id="couponAjax"><c:if
								test="${couponinfo.usage == 'Red'}">

								<img src="/ScanSeeWeb/images/cpnAddicon.png" alt="Cpn"
									width="27" id="cpnTg${couponinfo.couponId}" height="25"
									name="cpnTg${couponinfo.couponId}"
									onclick="wlcouponOperation(${couponinfo.couponId},'${couponinfo.usage}','cpnTg'+${couponinfo.couponId})" />
							</c:if> <c:if test="${couponinfo.usage == 'Green'}">

								<img src="/ScanSeeWeb/images/cpnIcon.png" alt="Cpn" width="27"
									height="25" id="cpnTg${couponinfo.couponId}"
									name="cpnTg${couponinfo.couponId}"
									onclick="wlcouponOperation(${couponinfo.couponId}, '${couponinfo.usage}' ,'cpnTg'+${couponinfo.couponId})" />
							</c:if></td>
						<td width="70%"><span> <a href="#"><c:out
										value="${couponinfo.couponName}" />
							</a> </span>
							<ul>
								<li class="stretch"><span>Price:</span><c:out
										value="${couponinfo.couponDiscountAmount}" /> off Expires <c:out
										value="${couponinfo.couponExpireDate}" />
								</li>
							</ul></td>
						<td width="11%"><c:choose>
								<c:when
									test="${couponinfo.couponImagePath eq null || couponinfo.couponImagePath eq 'NotApplicable'}">
									<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img" width="46"
										height="34" />

								</c:when>
								<c:otherwise>
									<img src=<c:out value="${couponinfo.couponImagePath}"/> alt="watch"
										width="46" height="34" />

								</c:otherwise>
							</c:choose></td>
						<td width="8%"></td>
					</tr>
				</c:forEach>
			</c:if>

		</table>

	</div>

</div>



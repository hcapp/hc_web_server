<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcoZEmDQJTP5iDa_aZIaHS5H9xdAdWG6w&sensor=true"></script>
<script type="text/javascript">
	window.onload = function() {
		if(null== '${requestScope.cityNameTL}' || '' ==  '${requestScope.cityNameTL}'){

				showAddress('${sessionScope.loginuser.city}');
				
			}else{

				showAddress('${requestScope.cityNameTL}'  );
				}
	}

	$(function(){
		var myOptions = {center: new google.maps.LatLng(-34.397, 150.644),
					zoom: 8,          
					mapTypeId: google.maps.MapTypeId.ROADMAP};
		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		
		var marker = new google.maps.Marker({
		map: map,
		draggable: true,
		position: map.getCenter()
		});
		var location = map.getCenter();
		var lat = location.lat();
		var longt = location.lng();
		
		var lattitude = '${sessionScope.objAreaInfoVO.lattitude}';
		var longitude = '${sessionScope.objAreaInfoVO.longitude}';
		
		if(lattitude != "" || longitude != ""){
		var myOptions = {center: new google.maps.LatLng(lattitude,longitude),
		zoom: 8,          
		mapTypeId: google.maps.MapTypeId.ROADMAP};
		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		var marker = new google.maps.Marker({
		map: map,
		draggable: true,
		position: map.getCenter()
		});
		}
		if('${sessionScope.objAreaInfoVO.zipCode}' != ""){
		
		var lattitude = '${sessionScope.zipcodeLat}';
		var longitude = '${sessionScope.zipcodeLong}';
		
		if(lattitude != "" || longitude != ""){
			var myOptions = {center: new google.maps.LatLng(lattitude,longitude),
			zoom: 8,          
			mapTypeId: google.maps.MapTypeId.ROADMAP};
			var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
			var marker = new google.maps.Marker({
			map: map,
			draggable: true,
			position: map.getCenter()
			});
		}
		}
	});
</script>

</head>
<body onload="load()" >
	<form:form name="wishlistdisplay" commandName="wishlistdisplay">
		<div id="content" class="topMrgn">
			<div align="center" id="airports"></div>
			<input type="hidden" name="latitude" id="latitude" value="" /> <input
				type="hidden" name="longitude" id="longitude" value="" />
			<div class="navBar">

				<ul>
					<c:if
						test="${requestScope.retailersDetails ne null && !empty requestScope.retailersDetails}">
						<li><img style="cursor: pointer;" src="../images/backBtn.png"
							onclick="javascript:history.go(-1);" /></li>
					</c:if>
					<c:if
						test="${requestScope.hotDealsDetails ne null && !empty requestScope.hotDealsDetails}">
						<li><img style="cursor: pointer;" src="../images/backBtn.png"
							onclick="javascript:history.go(-1);" /></li>
					</c:if>
					<c:if test="${requestScope.online eq 'online'}">
						<li><img style="cursor: pointer;" src="../images/backBtn.png"
							onclick="javascript:history.go(-1);" /></li>
					</c:if>
					<c:if
						test="${requestScope.couponlist ne null && !empty requestScope.couponlist}">
						<li><img style="cursor: pointer;" src="../images/backBtn.png"
							onclick="javascript:history.go(-1);" /></li>
					</c:if>
					<c:choose>
						<c:when
							test="${requestScope.retailersDetails ne null && !empty requestScope.retailersDetails}">
							<li class="titletxt">Retailer Details</li>
						</c:when>
						<c:when
							test="${requestScope.hotDealsDetails ne null && !empty requestScope.hotDealsDetails}">
							<li class="titletxt">Hot Deal Details</li>
						</c:when>
						<c:when test="${requestScope.online eq 'online'}">
							<li class="titletxt">Retailer Details</li>
						</c:when>
						<c:when
							test="${requestScope.couponlist ne null && !empty requestScope.couponlist}">
							<li class="titletxt">Coupons</li>
						</c:when>

						<c:otherwise>
							<li class="titletxt" style="padding-left: 50px;">Wish List</li>
						</c:otherwise>
					</c:choose>



				</ul>
			</div>
			<div class="clear"></div>

			<div class="mobCont relatvDiv">

				<div class="mobDsply floatR">


					<div class="dataView">
						<c:choose>
							<c:when
								test="${requestScope.retailersDetails ne null && !empty requestScope.retailersDetails}">
								<%@include file="wishlistlocalstores.jsp"%>
							</c:when>
							<c:when
								test="${requestScope.hotDealsDetails ne null && !empty requestScope.hotDealsDetails}">
								<%@include file="wlhotdealinfo.jsp"%>
							</c:when>
							<c:when test="${requestScope.online eq 'online'}">
								<%@include file="wishListOnlineStore.jsp"%>
							</c:when>
							<c:when
								test="${requestScope.couponlist ne null && !empty requestScope.couponlist}">
								<%@include file="wlcoupondisplay.jsp"%>
							</c:when>
							<c:otherwise>
								<table width="100%" cellspacing="0" cellpadding="0" border="0"
									class="brdrLsTbl topSubBarInside">
									<tbody>

										<tr>
											<td width="30%"
												style="border-right: 1px solid rgb(218, 218, 218);"><input
												type="text" name="searchValue" id="searchValue"
												class="textboxSmall focustxt"
												onkeypress="return searchWishListevent(event)"
												value="<c:out value='${requestScope.wishlistdisplay.searchValue}'/>" />
											</td>

											<td width="70%"><input type="button" value="Search"
												onclick="searchWishList(IsWishlst)" class="mobBtn"
												title="Search"></td>
											<input type="hidden" name="userId" id="userId"
												value="<c:out value='${sessionScope.loginuser.userID}'/>"></input>
											<input type="hidden" name="IsWishlst" id="IsWishlst"
												value="true"></input>
											<input type="hidden" name="fromWL" id="fromWL" value="WLMain" />
										</tr>
									</tbody>
								</table>
								<%-- Displaying Wishlist Alerted products --%>
								<c:if
									test="${sessionScope.wishlistproducts.alertProducts eq null && empty sessionScope.wishlistproducts.alertProducts
									&& sessionScope.wishlistproducts.productInfo eq null && empty sessionScope.wishlistproducts.productInfo}">
									<span id="NoDataInfo" style="color: red; text-align: center;">
										No data found!</span>


								</c:if>
								<c:if
									test="${sessionScope.wishlistproducts.alertProducts ne null && !empty sessionScope.wishlistproducts.alertProducts}">

									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="mobGrd detView zeroBtmMrgn" id="rtlrLst">
										<colgroup>
											<col width="10%">
											<col width="10%">
											<col width="63%">
											<col width="10%">
											<col width="7%">
										</colgroup>
										<tr name="Alerted Items">
											<td colspan="5" class="grpdtxt">Alerted Items</td>
										</tr>


										<c:forEach
											items="${sessionScope.wishlistproducts.alertProducts.alertProductDetails}"
											var="wishlistdate">
											<%-- Displaying Wish list Alerted product coupon details --%>


											<c:if test="${wishlistdate.couponSaleFlag>0}">
												<tr style="cursor: pointer;" name="Alerted Items">
													<td align="center"><img
														src="../images/MM_GalleryIcon.png" alt="coupon" width="28"
														height="28" title="Coupons" /></td>
													<td align="center"><c:choose>
															<c:when
																test="${wishlistdate.productImagePath eq null || wishlistdate.productImagePath eq 'NotApplicable' }">
																<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img"
																	width="38" height="38" />

															</c:when>
															<c:otherwise>
																<img
																	src="<c:out value="${wishlistdate.productImagePath}" />"
																	alt="Img" width="38" height="38" />

															</c:otherwise>
														</c:choose>
													</td>
													<td onclick="getWLCouponInfo(${wishlistdate.productId})"><span>
															<c:out value="${wishlistdate.productName}" /> </span>
														<ul>
															<c:if
																test="${wishlistdate.salePrice ne null && wishlistdate.salePrice ne '' }">
																<li><span>Sale Price:</span> <c:out
																		value="${wishlistdate.salePrice}" /></li>
															</c:if>
														</ul></td>



													<td
														onclick="deleteWishListItem(${wishlistdate.userProductId},'wshLst'+${wishlistdate.userProductId})"><img
														src="../images/deleteRow1.png" alt="delete"
														name="DeleteRow" width="29" height="29" title="delete" />
													</td>
													<td onclick="callproductSummary(${wishlistdate.productId})"><img
														src="../images/rightArrow.png" alt="Arrow" width="11"
														height="15" /></td>
												</tr>
											</c:if>

											<%-- Displaying Wish list Alerted product hot deal details --%>

											<c:if test="${wishlistdate.hotDealFlag>0}">
												<tr style="cursor: pointer;" name="Alerted Items">
													<td align="center"><img
														src="../images/MM_HotDeals_Icon.png" alt="hotdeal"
														width="28" height="28" title="Hot Deals" /></td>
													<td align="center"><c:choose>
															<c:when
																test="${wishlistdate.productImagePath eq null || wishlistdate.productImagePath eq 'NotApplicable'}">
																<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img"
																	width="38" height="38" />

															</c:when>
															<c:otherwise>
																<img
																	src="<c:out value="${wishlistdate.productImagePath}" />"
																	alt="Img" width="38" height="38" />

															</c:otherwise>
														</c:choose>
													</td>
													<td onclick="getWLHotdealInfo(${wishlistdate.productId})"><span>
															<c:out value="${wishlistdate.productName}" /> </span>
														<ul>
															<c:if
																test="${wishlistdate.salePrice ne null && wishlistdate.salePrice ne '' }">
																<li><span>Sale Price:</span> <c:out
																		value="${wishlistdate.salePrice}" /></li>
															</c:if>


														</ul></td>
													<td><img src="../images/deleteRow1.png" alt="delete"
														name="DeleteRow" width="29" height="29" title="delete"
														onclick="deleteWishListItem(${wishlistdate.userProductId},'wshLst'+${wishlistdate.userProductId})" />
													</td>
													<td onclick="callproductSummary(${wishlistdate.productId})"><img
														src="../images/rightArrow.png" alt="Arrow" width="11"
														height="15" /></td>
												</tr>
											</c:if>

											<%-- Displaying Wish list Alerted product with sale price details --%>

											<c:if test="${wishlistdate.retailFlag>0}">
												<tr style="cursor: pointer;" name="Alerted Items">
													<td align="center">&nbsp;</td>
													<td align="center"><c:choose>
															<c:when
																test="${wishlistdate.productImagePath eq null || wishlistdate.productImagePath eq 'NotApplicable'}">
																<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img"
																	width="38" height="38" />

															</c:when>
															<c:otherwise>
																<img
																	src="<c:out value="${wishlistdate.productImagePath}" />"
																	alt="Img" width="38" height="38" />

															</c:otherwise>
														</c:choose>
													</td>
													<td onclick="callWLLocalStores(${wishlistdate.productId})"><span>
															<c:out value="${wishlistdate.productName} " /> </span>
														<ul>
															<c:if
																test="${wishlistdate.salePrice ne null && wishlistdate.salePrice ne '' }">
																<li><span>Sale Price:</span> <c:out
																		value="${wishlistdate.salePrice}" /></li>
															</c:if>


														</ul></td>
													<td><img src="../images/deleteRow1.png" alt="delete"
														name="DeleteRow" width="29" height="29" title="delete"
														onclick="deleteWishListItem(${wishlistdate.userProductId},'wshLst'+${wishlistdate.userProductId})" />
													</td>
													<td onclick="callproductSummary(${wishlistdate.productId})"><img
														src="../images/rightArrow.png" alt="Arrow" width="11"
														height="15" /></td>
												</tr>
											</c:if>

										</c:forEach>
									</table>
								</c:if>
								<%-- Displaying Wish list  product without sale price details --%>
								<c:if
									test="${sessionScope.wishlistproducts.productInfo ne null && !empty sessionScope.wishlistproducts.productInfo}">

									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="mobGrd detView zeroBtmMrgn" id="rtlrLst">
										<colgroup>
											<col width="10%">
											<col width="10%">
											<col width="63%">
											<col width="10%">
											<col width="7%">
										</colgroup>
										<c:forEach
											items="${sessionScope.wishlistproducts.productInfo}"
											var="regularProducts">
											<tr name="${regularProducts.wishListAddDate}">
												<td colspan="5" class="grpdtxt"><c:out
														value="${regularProducts.wishListAddDate}" />
											</tr>
											<c:forEach items="${regularProducts.productDetail}"
												var="productAttributes">



												<tr id="wshLst${productAttributes.userProductId}"
													name="${regularProducts.wishListAddDate}"
													style="cursor: pointer;">
													<td align="center" width="28">&nbsp;</td>


													<td align="center"><c:choose>
															<c:when
																test="${productAttributes.productImagePath eq null || productAttributes.productImagePath eq 'NotApplicable'}">
																<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img"
																	width="38" height="38" />


															</c:when>
															<c:otherwise>
																<img
																	src="<c:out value="${productAttributes.productImagePath}" />"
																	alt="Img" width="38" height="38" />

															</c:otherwise>
														</c:choose>
													</td>
													<td
														onclick="callproductSummary(${productAttributes.productId})"><span><c:out
																value="${productAttributes.productName}" /> </span> <!--<ul>

											<td onclick="callproductSummary(${productAttributes.productId})"><span><a href="thisLocation_prodSum.htm"><c:out
															value="${productAttributes.productName}" /> </a>
											</span> <!--<ul>

                    <li><span>Product:</span> $0.5</li>
                    <li><span>Product:</span> $20</li>
                  </ul>--></td>




													<td
														onclick="deleteWishListItem(${productAttributes.userProductId},'wshLst'+${productAttributes.userProductId})"><img
														src="../images/deleteRow1.png" alt="delete"
														name="DeleteRow" width="29" height="29" title="delete" />
													</td>
													<td
														onclick="callproductSummary(${productAttributes.productId})"><img
														src="../images/rightArrow.png" alt="Arrow" width="11"
														height="15" />
													</td>
												</tr>


											</c:forEach>
										</c:forEach>
									</table>

								</c:if>
							</c:otherwise>
						</c:choose>
					</div>
					<input type="hidden" name="productId" id="productId" value="" /> <input
						type="hidden" name="productImagePath" id="productImagePath" />
					<input type="hidden" name="productName" id="productName" /> <input
						type="hidden" name="salePrice" id="salePrice" />
					<div class="optnPnl">
						<div>
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="brdrLsTbl">
								<tr>
									<td width="70%">City Name:</td>
									<td width="20%"><input type="text" class="textboxDate"
										name="cityNameTL" value='${requestScope.cityNameTL}' /></td>
									<td width="5%"><input type="button" class="mobBtn LstRtlr"
										value="Go" onclick="showAddress(''); return false" title="Go" />
									</td>
								</tr>
							</table>
						</div>
						<div align="center" id="map_canvas" style="width: 260px; height: 260px"></div>
						<table width="98%" border="0" cellspacing="0" cellpadding="0"
							class="brdrLsTbl">


							<tr>
								<td width="40%">Zip Code:</td>

								<td width="70%"><input type="text" class="textboxSmall"
									name="zipcodeWL" value="${sessionScope.objAreaInfoVO.zipCode}"
									onkeypress="return isNumberKey(event);return ValidateWishList(event)" /></td>
							</tr>
							<tr>
								<td colspan="2"><input type="button" class="mobBtn LstRtlr"
									value="Refresh" onclick="return ValidateWishList('');"
									title="Refresh" /></td>
							</tr>
						</table>
					</div>
					<div class="clear"></div>
				</div>
				<%@include file="leftmenu.jsp"%>
			</div>
			<div class="clear"></div>
			<%-- Displaying Wish list Alerted product with sale price details --%>


			<div class="clear"></div>
			<div class="tabBar">



				<ul id="sLTab">


					<c:choose>
						<c:when
							test="${requestScope.retailersDetails ne null && !empty requestScope.retailersDetails}">
							<li></li>
							<li></li>
							<li><img src="../images/tab_btn_up_history.png"
								alt="History"
								onclick="callWLHistoryFrmLocalStores(${requestScope.productId})" />
							</li>
							<li><img src="../images/tab_btn_up_rateshare.png"
								alt="Rate Share"
								onclick="callWLReviews(${requestScope.productId})" /></li>
							<li><a
								href="preferencesmain.htm"><img src="../images/tab_settings_up.png" alt="Settings" />
							</a></li>
							<li><a
								href="http://208.39.97.1:8080/html/About_Wishlist.html"><img
									src="../images/tab_btn_up_Abtwishlist.png" alt="wishlist" target="_blank" /> </a>
							</li>
						</c:when>
						<c:when
							test="${requestScope.hotDealsDetails ne null && !empty requestScope.hotDealsDetails}">
							<li></li>
							<li></li>
							<li><a href="#"><img height="50" width="80"
									alt="facebook" src="../images/tab_btn_up_facebook.png"> </a>
							</li>
							<li><a href="#"><img alt="twitter"
									src="../images/tab_btn_up_twitter.png"> </a></li>
							<li><a href="#"><img alt="sendtext"
									src="../images/tab_btn_up_sendtxt.png"> </a></li>
							<li><a href="#"><img alt="sendmail"
									src="../images/tab_btn_up_sendmail.png"
									onclick="openEMailSharePopUpGeneral(<c:out
											value="${hotDealsDetails.productHotDealID}" />);">
							</a></li>
						</c:when>
						<c:when test="${requestScope.online eq 'online'}">
							<li></li>
							<li></li>
							<li><a href="#"
								onclick="callWLHistoryFrmOnlineStores(${requestScope.productId})"><img
									src="../images/tab_btn_up_history.png" alt="History" /> </a></li>
							<li><a href="#"
								onclick="callWLOnlineReviews(${requestScope.productId})"> <img
									src="../images/tab_btn_up_rateshare.png" alt="Rate Share" />
							</li>
							<li><a href="preferencesmain.htm"><img src="../images/tab_settings_up.png"
									alt="Settings" /> </a></li>
							<li><a
								href="http://208.39.97.1:8080/html/About_Wishlist.html"><img
									src="../images/tab_btn_up_Abtwishlist.png" alt="wishlist" target="_blank"/> </a>
							</li>
						</c:when>
						<c:when
							test="${requestScope.couponlist ne null && !empty requestScope.couponlist}">

						</c:when>

						<c:otherwise>
							<li></li>
							<li></li>
							<li><a href="wishlisthistorydisplay.htm"><img
									src="../images/tab_btn_up_history.png" alt="History" /> </a></li>
							<li><a href="coupongallery.htm"><img
									src="../images/tab_btn_up_cpnGlry.png" alt="Coupon Gallery" />
							</a>
							</li>
							<li><a href="preferencesmain.htm"><img src="../images/tab_settings_up.png"
									alt="Settings" /> </a></li>
							<li><a
								href="http://208.39.97.1:8080/html/About_Wishlist.html" target="_blank"><img
									src="../images/tab_btn_up_Abtwishlist.png" alt="wishlist" /> </a>
							</li>
						</c:otherwise>
					</c:choose>

				</ul>
			</div>
		</div>

	</form:form>
</body>
</html>

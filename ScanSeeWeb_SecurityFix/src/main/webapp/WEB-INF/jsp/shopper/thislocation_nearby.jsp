<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;"><!--<h3>List of Retailers Based on Search Criteria</h3>-->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobGrd detView zeroBtmMrgn" id="allRetailers">
  
  <c:forEach items="${sessionScope.retailerDetails.findNearByDetail}" var="prodattributes">
  <tr>
    <td align="center">&nbsp;</td>
    <td onclick="callNearByRetSummary(${prodattributes.retLocId},${prodattributes.retailerId},'${prodattributes.retailerName}')">
	<span><c:out value="${prodattributes.retailerName}" /></span>
      <ul>
      <li><span><c:out value="${prodattributes.distance}" /> Miles</span></li>
	  
	  <c:if test="${prodattributes.productPrice ne null && prodattributes.productPrice ne 'NotApplicable' }">
																<li><c:out
																		value="${prodattributes.productPrice}" />
																</li>
															</c:if>
	       
    </ul></td>
	 <td width="25%"><input type="button" name="mapview" value="Locate Store" class="btn"  onclick="load(${prodattributes.latitude} , ${prodattributes.longitude})" title="Map View"/></td>
    <td><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
  </tr>
  </c:forEach>
</table>
</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
	function couponOperation(couponId, usage,id) {
		// get the form values
	
        

		$.ajax({
			type : "GET",
			url : "couponoperation.htm",
			data : {
				'couponId' : couponId,
				'usage' : usage
			},

			success : function(response) {
			
				$('#'+id).replaceWith(response);
				
			
			
			},
			error : function(e) {
				
			}
		});

	
	}

	function rebateOperation(rebateId, usage,id) {
		// get the form values
	     

		$.ajax({
			type : "GET",
			url : "rebateoperation.htm",
			data : {
				'rebateId' : rebateId,
				'usage' : usage
			},

			success : function(response) {
				
					$('#'+id).replaceWith(response);
					
				
				
				},
				error : function(e) {
					
				}
			});

	
	}

	function loyalityOperation(loyaltyDealId, usage,id) {
		// get the form values
	

		$.ajax({
			type : "GET",
			url : "loyalityoperation.htm",
			data : {
				'loyaltyDealId' : loyaltyDealId,
				'usage' : usage
			},

			success : function(response) {
				
					
					$('#'+id).replaceWith(response);
					
				
				
				},
				error : function(e) {
					
				}
			});

	
	}
	
	</script>



<div class="dataView" style="height: 450px; overflow-x: hidden;">

<div class="dataView">

<form:hidden path="couponID"/>
<form:hidden path="rebateId"/>
<form:hidden path="loyaltyDealID"/>
<form:hidden path="clrType"/>
<form:hidden path="added"/> 
<form:hidden path="clrImagePath"/> 


	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="mobGrd detView zeroBtmMrgn">
		
		
		
		
	<c:if test="${requestScope.discountinfo.couponDetail ne null && !empty requestScope.discountinfo.couponDetail}">
		
		<tr>
			<td colspan="4" class="mobSubTitle">Coupons</td>
		</tr>
	
		<c:forEach items="${requestScope.discountinfo.couponDetail}" var="item">			
		<tr>
			
			<td width="11%" align="center" id="couponAjax">
			
			<c:if test="${item.usage == 'Red'}">
			
			<img src="/ScanSeeWeb/images/cpnAddicon.png" alt="Cpn" width="27" id="cpnTg${item.couponId}" height="25" name="cpnTg${item.couponId}" onclick="couponOperation(${item.couponId},'${item.usage}','cpnTg'+${item.couponId})" />
			</c:if>
			
			<c:if test="${item.usage == 'Green'}">
			
			<img src="/ScanSeeWeb/images/cpnIcon.png" alt="Cpn" width="27" height="25" id="cpnTg${item.couponId}" name="cpnTg${item.couponId}" onclick="couponOperation(${item.couponId}, '${item.usage}' ,'cpnTg'+${item.couponId})" />
			</c:if>
			
			
			
			
			</td>
			<td width="70%"><span>
			
			<c:choose>
                		<%-- Checking whether coupon is External or internal coupon if External coupon link to coupon site else internal Coupons info page--%>
                		<c:when test="${item.couponURL ne null && item.couponURL ne 'NotApplicable'}">
                			<a href="${item.couponURL}" target="_blank">${item.couponName}</a>
                		</c:when>
                		<c:otherwise>
                			<a href="#" onclick="getSummaryCouponDetails(${item.couponId},'${item.usage}','${item.couponImagePath}')">${item.couponName}</a>
                	    </c:otherwise>
                		</c:choose>
			
			</span>
				<ul>
					<li class="stretch"><span>Price:</span><c:out value="${item.couponDiscountAmount}"/> off
					 Expires 
					 <c:out value="${item.couponExpireDate}"/></li>
				</ul>
			</td>
			<td width="11%"><img src=<c:out value="${item.imagePath}"/> 
				width="46" height="34" />
			</td>
			<td width="8%">
			</td>
		</tr>
		</c:forEach>
		</c:if>
		
	
	
	
	
	
		
		
		
		<c:if test ="${requestScope.discountinfo.rebateDetail ne null && !empty requestScope.discountinfo.rebateDetail}">
		<tr>
		
			<td colspan="4" class="mobSubTitle">Rebate</td>
		</tr>
		<c:forEach items="${requestScope.discountinfo.rebateDetail}" var="item">			
			
		<tr>
		<td width="11%" align="center" id="rebateAjax">
			
			
			
			<c:if test="${item.usage == 'Red'}">
			
			
			<img src="/ScanSeeWeb/images/cpnAddicon.png" alt="Cpn" width="27" id="cpnTg${item.rebateId}" height="25" name="cpnTg${item.rebateId}" onclick="rebateOperation(${item.rebateId},'${item.usage}','cpnTg'+${item.rebateId})" />
			</c:if>
			
			<c:if test="${item.usage == 'Green'}">
			
			
			<img src="/ScanSeeWeb/images/cpnIcon.png" alt="Cpn" width="27" height="25" id="cpnTg${item.rebateId}" name="cpnTg${item.rebateId}" onclick="rebateOperation(${item.rebateId}, '${item.usage}' ,'cpnTg'+${item.rebateId})" />
			
			</c:if>
			
			
			</td>
				

			<td width="70%"><span>
		
			<a href="#" onclick="getSummaryRebateDetails(${item.rebateId},'${item.usage}','${item.imagePath}')"><c:out value="${item.rebateName}"/></a>
			
			</span>
				<ul>
					<li class="stretch"><span>Price:</span><c:out value="${item.rebateAmount}"/> off
					 Expires 
					 <c:out value="${item.rebateEndDate}"/></li>
				</ul>
			</td>
			<td width="11%"><img src=<c:out value="${item.imagePath}"/> 
				width="46" height="34" />
			</td>
			<td width="8%">
			</td>
		</tr>
		</c:forEach>
	
	</c:if>
	
	<c:if test="${requestScope.discountinfo.loyaltyDetail ne null && !empty requestScope.discountinfo.loyaltyDetail}">
	<tr>
		
			<td colspan="4" class="mobSubTitle">Loyalty</td>
		</tr>
	
	
	<c:forEach items="${requestScope.discountinfo.loyaltyDetail}" var="item">			
			
		<tr>
			<td width="11%" align="center" id="loyaltyAjax">
			
			
			<c:if test="${item.usage == 'Red'}">
			
			
			<img src="/ScanSeeWeb/images/cpnAddicon.png" alt="Cpn" width="27" id="cpnTg${item.loyaltyDealId}" height="25" name="cpnTg${item.loyaltyDealId}" onclick="loyalityOperation(${item.loyaltyDealId},'${item.usage}','cpnTg'+${item.loyaltyDealId})" />
			</c:if>
			
			<c:if test="${item.usage == 'Green'}">
			
			<img src="/ScanSeeWeb/images/cpnIcon.png" alt="Cpn" width="27" height="25" id="cpnTg${item.loyaltyDealId}" name="cpnTg${item.loyaltyDealId}" onclick="loyalityOperation(${item.loyaltyDealId}, '${item.usage}' ,'cpnTg'+${item.loyaltyDealId})" />
			</c:if>
			
			
			
			</td>
			

			<td width="70%"><span>
			
	    	
			<a href="#" onclick="getSummaryLoyaltyDetails(${item.loyaltyDealId},'${item.usage}','${item.imagePath}')"><c:out value="${item.loyaltyDealName}"/></a>
			
			</span>
				<ul>
					<li class="stretch"><span>Price:</span><c:out value="${item.loyaltyDealDiscountAmount}"/> off
					 Expires 
					 <c:out value="${item.loyaltyDealExpireDate}"/></li>
				</ul>
			</td>
			<td width="11%"><img src=<c:out value="${item.imagePath}"/> 
				width="46" height="34" />
			</td>
			<td width="8%">
			</td>
		</tr>
		</c:forEach>
		</c:if>
	</table>

</div>

</div>


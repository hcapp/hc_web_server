<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;">
	<!--<h3>List of Retailers Based on Search Criteria</h3>-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="mobGrd detView zeroBtmMrgn">
		<c:set var="retailer" value="0" />
		<c:if
			test="${requestScope.retailID ne null && !empty requestScope.retailID}">
			<c:set var="retailer" value="${requestScope.retailID}" />
		</c:if>
		<c:set var="retailer" value="0" />
		<tr class="noHvr">
			<td width="13%" align="center">&nbsp;</td>
			<td align="right" width="81%">
				<ul>
					<li><span><c:out value="${retailerNameProductSum}" />
					</span>
					</li>
					<br>
					<c:if
						test="${salepriceProductSum ne null && !empty salepriceProductSum}">
						<li><span>Sale Price: <c:out
									value="${salepriceProductSum}" />
						</span>
						</li>
					</c:if>
				</ul></td>
			<td width="6%">&nbsp;</td>
			<td width="6%">&nbsp;</td>
		</tr>
		<tr class="noHvr"
			onclick="callProductInfo(${requestScope.productId},${retailer})">
			<td width="13%" align="center"><c:if
					test="${requestScope.productSummaryVO.productDetail.imagePath == 'NotApplicable'}">
					<img src="../images/imgIcon.png" alt="Img" width="38" height="38" />
				</c:if> <c:if
					test="${requestScope.productSummaryVO.productDetail.imagePath != 'NotApplicable'}">
					<img src="${requestScope.productSummaryVO.productDetail.imagePath}"
						alt="Img" width="38" height="38" />
				</c:if></td>
			<td width="81%"><span><c:out
						value="${requestScope.productSummaryVO.productDetail.productName}" />
			</span> <c:choose>
					<c:when
						test="${requestScope.productSummaryVO.productDetail.productShortDescription ne null || 
				!empty requestScope.productSummaryVO.productDetail.productShortDescription
				|| requestScope.productSummaryVO.productDetail.productShortDescription ne 'NotApplicable'}">
						<c:out
							value="${requestScope.productSummaryVO.productDetail.productShortDescription}" />
					</c:when>
					<c:otherwise>
						<c:out
							value="${requestScope.productSummaryVO.productDetail.productLongDescription}" />
					</c:otherwise>

				</c:choose></td>
			<td width="6%">&nbsp;</td>
			<td><img src="../images/rightArrow.png" alt="Arrow" width="11"
				height="15" /></td>
		</tr>
		<c:forEach items="${requestScope.productSummaryVO.appConfigData}"
			var="prodattributes">
			<c:if
				test="${requestScope.productSummaryVO.findNearByDetailsResultSet.findNearByDetails ne null && ! empty requestScope.productSummaryVO.findNearByDetailsResultSet.findNearByDetails}">
				<c:if test="${prodattributes.key == 0}">
					<tr class="noHvr" onclick="callNearBy(${requestScope.productId})">
						<td width="13%" align="center"><img
							src="${prodattributes.value}" alt="Img" width="38" height="38" />
						</td>
				</c:if>
				<c:if test="${prodattributes.key == 1}">
					<td width="81%"><span><c:out
								value="${prodattributes.value}" />
					</span>
						<ul>
							<li><span class="stretch"> <c:out
										value="${fn:length(requestScope.productSummaryVO.findNearByDetailsResultSet.findNearByDetails)}" /> Stores
							</span>
							</li>
							<c:if test="${requestScope.price ne null && !empty requestScope.price}">
							<li><span>Price: <c:out value="${requestScope.price}"/></span></li>
							</c:if>
						</ul></td>
					<td width="6%">&nbsp;</td>
					<td><img src="../images/rightArrow.png" alt="Arrow" width="11"
						height="15" /></td>
					</tr>
				</c:if>
			</c:if>
			<c:if test="${prodattributes.key == 2}">
				<tr class="noHvr"
					onclick="callProductInfo(${requestScope.productId},${retailer})">
					<td width="13%" align="center"><img
						src="${prodattributes.value}" alt="Img" width="38" height="38" />
					</td>
			</c:if>
			<c:if test="${prodattributes.key == 3}">
				<td width="81%"><span><c:out
							value="${prodattributes.value}" />
				</span>
					<ul>
						<li><span class="stretch">Product Description</span></li>
					</ul></td>
				<td width="6%">&nbsp;</td>
				<td><img src="../images/rightArrow.png" alt="Arrow" width="11"
					height="15" /></td>
				</tr>
			</c:if>
			<c:if
				test="${(requestScope.productSummaryVO.productReviewslist ne null && !empty requestScope.productSummaryVO.productReviewslist) || (requestScope.productSummaryVO.userRatingInfo.avgRating ne null && requestScope.productSummaryVO.userRatingInfo.avgRating >0)}">
				<c:if test="${prodattributes.key == 4}">
					<tr class="noHvr" onclick="callReviews(${requestScope.productId})">
						<td width="13%" align="center"><img
							src="${prodattributes.value}" alt="Img" width="38" height="38" />
						</td>
				</c:if>
				<c:if test="${prodattributes.key == 5}">
					<td width="81%">
						<ul>
							<li class="titleTxt"><c:out value="${prodattributes.value}" />
							</li>
							<li><ul class="avgrating">


									<c:forEach begin="1"
										end="${requestScope.productSummaryVO.userRatingInfo.avgRating}"
										var="current">
										<li><img src="../images/Red_fullstar.png" alt="Yellow"
											width="15" height="15" />
										</li>
									</c:forEach>
									<c:forEach
										begin="${requestScope.productSummaryVO.userRatingInfo.avgRating+1}"
										end="5" var="current">
										<li><img src="../images/greyStar.png" alt="Yellow"
											width="15" height="15" />
										</li>
									</c:forEach>


								</ul>
							</li>
						</ul>

						<ul>
							<li><span class="stretch">View Review <c:out
										value="${fn:length(requestScope.productSummaryVO.productReviewslist)}" />
							</span></li>
							<li class="titleTxt"><c:out
									value="${requestScope.productSummaryVO.userRatingInfo.noOfUsersRated}" />
								ratings</li>
						</ul></td>
					<td width="6%">&nbsp;</td>
					<td><img src="../images/rightArrow.png" alt="Arrow" width="11"
						height="15" /></td>
					</tr>
				</c:if>
			</c:if>
			<c:set var="coupongrey"
				value="${requestScope.productSummaryVO.productDetail.coupon_Status eq 'Grey'}" />

			<c:set var="loyalitygrey"
				value="${requestScope.productSummaryVO.productDetail.loyalty_Status eq 'Grey'}" />
			<c:set var="rebategrey"
				value="${requestScope.productSummaryVO.productDetail.rebate_Status eq 'Grey'}" />
			<c:set var="statusChk" value="Grey" />
			<c:choose>

				<c:when
					test="${(requestScope.productSummaryVO.productDetail.coupon_Status eq statusChk) && (requestScope.productSummaryVO.productDetail.loyalty_Status eq statusChk) && (requestScope.productSummaryVO.productDetail.rebate_Status eq statusChk)}">

				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${prodattributes.key == 6}">
							<tr class="noHvr"
								onclick="getDiscount(${requestScope.productId},${retailer})">
								<td width="13%" align="center"><img
									src="${prodattributes.value}" alt="Img" width="38" height="38" />
								</td>
						</c:when>
						<c:when test="${prodattributes.key == 7}">
							<td width="81%"><span><c:out
										value="${prodattributes.value}" />
							</span></td>
							<td width="6%">
								<ul class="clr">
									<li><img
										src="../images/C-${requestScope.productSummaryVO.productDetail.coupon_Status}.png"
										alt="c" width="14" height="14" />
									</li>
									<li><img
										src="../images/L-${requestScope.productSummaryVO.productDetail.loyalty_Status}.png"
										alt="c" width="14" height="14" />
									</li>
									<li><img
										src="../images/R-${requestScope.productSummaryVO.productDetail.rebate_Status}.png"
										alt="c" width="14" height="14" />
									</li>
								</ul></td>
							<td><img src="../images/rightArrow.png" alt="Arrow"
								width="11" height="15" /></td>
							</tr>
						</c:when>
					</c:choose>
				</c:otherwise>
			</c:choose>

			<c:if
				test="${requestScope.productSummaryVO.objOnlineStores.onlineStoresMetaData.totalStores ne null && ! empty requestScope.productSummaryVO.objOnlineStores.onlineStoresMetaData.totalStores}">

				<c:if test="${prodattributes.key == 8}">
					<tr class="noHvr"
						onclick="getOnlineStoreInfo(${requestScope.productId},${retailer})">
						<td width="13%" align="center"><img
							src="${prodattributes.value}" alt="Img" width="38" height="38" />
						</td>
				</c:if>
				<c:if test="${prodattributes.key == 9}">
					<td width="81%"><span><c:out
								value="${prodattributes.value}" />
					</span>
						<ul>
							<li><span class="stretch"> <c:out
										value="${requestScope.productSummaryVO.totalStores}" />
							</span>
							</li>
							<li><span class="stretch"> <c:out
										value="${requestScope.productSummaryVO.minimumPrice}" />
							</span>
							</li>
						</ul></td>
					<td width="6%">&nbsp;</td>
					<td><img src="../images/rightArrow.png" alt="Arrow" width="11"
						height="15" /></td>
					</tr>
				</c:if>
			</c:if>
		</c:forEach>
	</table>
</div>
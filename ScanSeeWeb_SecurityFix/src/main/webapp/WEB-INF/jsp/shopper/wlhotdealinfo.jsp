<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="dataView">
	<%-- Displaying Wish list Alerted product with hot deal details --%>
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="gnrlGrd detView lstNm">
		<tr>
			<th width="22%" align="center"><img
				src="../images/MM_HotDeals_Icon.png" alt="hotdeals" width="28"
				height="28" /></th>
			<th width="74%" align="left"><a href="#"><c:out
						value="${requestScope.hotDealsDetails.hotDealName}" /> </a></th>
			<th width="4%" align="left">&nbsp;</th>
		</tr>
		<tbody>

			<tr class="mobGrd zeroBtmMrgn">
				<td align="center" valign="top" class="imgBlk">
				<c:choose>
														<c:when
															test="${requestScope.hotDealsDetails.hotDealImagePath eq null || requestScope.hotDealsDetails.hotDealImagePath eq 'NotApplicable'}">
															<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img"
																width="64" height="90" />

														</c:when>
														<c:otherwise>
															<img src="<c:out value="${wishlistdate.hotDealImagePath}" />"
																alt="Img" width="64" height="90" />

														</c:otherwise>
													</c:choose>
				
				
				
				
				
				</td>
				<td><ul class="infoView">

						<li><span>Description:</span><strong>
								${requestScope.hotDealsDetails.hDLognDescription} </strong>

						</li>
						<li><span>Price: $</span>
						<c:out value="${requestScope.hotDealsDetails.hDPrice}" /></li>
						<li><span>Sale Price: $</span>
						<c:out value="${requestScope.hotDealsDetails.hDSalePrice}" /></li>
						<li><span>Start Date:</span>
						<c:out value="${requestScope.hotDealsDetails.hDStartDate}" /></li>
						<li><span>End Date:</span>
						<c:out value="${requestScope.hotDealsDetails.hDEndDate}" /></li>
						<li><span>Terms & Condition:</span>
						<c:out value="${requestScope.hotDealsDetails.hDTermsConditions}" />
						</li>


						<c:if
							test="${requestScope.hotDealsDetails.hdURL ne null && requestScope.hotDealsDetails.hdURL ne '' }">


							<li><span></span><a
								href="http://${requestScope.hotDealsDetails.hdURL}"
								target="_blank"><img src="../images/getDeal.png"
									alt="getdeal" width="76" height="19" title="Get Deal" /> </a></li>

						</c:if>



					</ul></td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>

</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcoZEmDQJTP5iDa_aZIaHS5H9xdAdWG6w&sensor=true"></script>
<script type="text/javascript">

window.onload = function() {
	//alert(${sessionScope.loginuser.city})
	showAddress('${sessionScope.loginuser.city}');
}
$(function(){
	var myOptions = {center: new google.maps.LatLng(-34.397, 150.644),
				zoom: 8,          
				mapTypeId: google.maps.MapTypeId.ROADMAP};
	var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
	
	var marker = new google.maps.Marker({
	map: map,
	draggable: true,
	position: map.getCenter()
	});
	var location = map.getCenter();
	var lat = location.lat();
	var longt = location.lng();
	
	var lattitude = '${sessionScope.objAreaInfoVO.lattitude}';
	var longitude = '${sessionScope.objAreaInfoVO.longitude}';
	
	if(lattitude != "" || longitude != ""){
	var myOptions = {center: new google.maps.LatLng(lattitude,longitude),
	zoom: 8,          
	mapTypeId: google.maps.MapTypeId.ROADMAP};
	var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
	var marker = new google.maps.Marker({
	map: map,
	draggable: true,
	position: map.getCenter()
	});
	}
	if('${sessionScope.objAreaInfoVO.zipCode}' != ""){
	
	var lattitude = '${sessionScope.zipcodeLat}';
	var longitude = '${sessionScope.zipcodeLong}';
	
	if(lattitude != "" || longitude != ""){
		var myOptions = {center: new google.maps.LatLng(lattitude,longitude),
		zoom: 8,          
		mapTypeId: google.maps.MapTypeId.ROADMAP};
		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		var marker = new google.maps.Marker({
		map: map,
		draggable: true,
		position: map.getCenter()
		});
	}
	}
});

</script>

</head>
<body onload="load()">
	<form:form name="wishlisthistorydisplay"
		commandName="wishlisthistorydisplay">
		<div id="content" class="topMrgn">
			<div align="center" id="airports"></div>
			<input type="hidden" name="latitude" id="latitude" value="" /> <input
				type="hidden" name="longitude" id="longitude" value="" />
			<div class="navBar">

				<ul>
					<c:choose>
						<c:when test="${WishListMainPage ne null}">
							<li><a href="wishlistdisplay.htm"> <img
									src="../images/backBtn.png" /> </a></li>
							
						</c:when>

						<c:otherwise>
							<li><img src="../images/backBtn.png"
								onclick="javascript:history.go(-1);" /></li>
							

						</c:otherwise>
					</c:choose>
					
					<li class="titletxt">History</li>

				</ul>
			</div>
			<div class="clear"></div>
			<div class="mobCont">

				<div class="mobDsply floatR">
					<div class="dataView rtlrPnl"
						style="height: 450px; overflow-x: hidden;">
						<!--<h3>List of Retailers Based on Search Criteria</h3>-->
						<c:choose>
							<c:when
								test="${requestScope.wishListHistDetails.productHistoryInfo ne null && !empty requestScope.wishListHistDetails.productHistoryInfo}">

								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="mobGrd detView zeroBtmMrgn" id="rtlrLst">
									<c:forEach
										items="${requestScope.wishListHistDetails.productHistoryInfo}"
										var="wishlistdate1">

										<tr>
											<td colspan="4" class="grpdtxt"><c:out
													value="${wishlistdate1.wishListAddDate}" /></td>
										</tr>
										<c:forEach items="${wishlistdate1.productDetail}"
											var="wishlistdate">

											<tr id="wshLst${wishlistdate.userProductId}"
												name="wshLst${wishlistdate.userProductId}">
												<td align="center"><c:choose>
														<c:when
															test="${wishlistdate.productImagePath eq null || wishlistdate.productImagePath eq 'NotApplicable'}">
															<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img"
																width="38" height="38" />

														</c:when>
														<c:otherwise>
															<img
																src="<c:out value="${wishlistdate.productImagePath}" />"
																alt="Img" width="38" height="38" />

														</c:otherwise>
													</c:choose>
												</td>
												<td><span><a href="#"
														onclick="callproductSummaryWL(${wishlistdate.productId})"><c:out
																value="${wishlistdate.productName}" /> </a> </span>
													<ul>

														<c:if
															test="${wishlistdate.salePrice ne null && wishlistdate.salePrice ne '' }">
															<li><span>Sale Price:</span> <c:out
																	value="${wishlistdate.salePrice}" />
															</li>
														</c:if>
														<c:if
															test="${wishlistdate.productPrice ne null && wishlistdate.productPrice ne '' }">
															<li><span> Price:</span> <c:out
																	value="${wishlistdate.productPrice}" />
															</li>
														</c:if>



													</ul>
												</td>

											</tr>

										</c:forEach>

									</c:forEach>

								</table>
							</c:when>

							<c:otherwise></c:otherwise>
						</c:choose>
					</div>
					<div class="optnPnl">
						<div>
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="brdrLsTbl">
								<tr>
									<td width="70%">City Name:</td>
									<td width="20%"><input type="text" class="textboxDate"
										name="cityNameTL"  value = '${requestScope.cityNameTL}'/>
									</td>
									<td width="5%"><input type="button" class="mobBtn LstRtlr"
										value="Go" onclick="showAddress(''); return false" title="Go"/></td>
								</tr>
							</table>
						</div>
						<div align="center" id="map_canvas" style="width: 260px; height: 260px"></div>
						<table width="98%" border="0" cellspacing="0" cellpadding="0"
							class="brdrLsTbl">
							<tr>
								<td width="40%">Zip Code:</td>
								<c:choose>
									<c:when
										test="${sessionScope.objAreaInfoVO.zipCode ne null && !empty sessionScope.objAreaInfoVO.zipCode}">
										<td width="70%"><input type="text" class="textboxSmall"
											name="zipcodeTL" onkeypress="return isNumberKey(event)"
											value="${sessionScope.objAreaInfoVO.zipCode}" /></td>
									</c:when>
									<c:otherwise>
										<td width="70%"><input type="text" class="textboxSmall"
											name="zipcodeTL" onkeypress="return isNumberKey(event)" /></td>
									</c:otherwise>
								</c:choose>
							</tr>
<tr>
								<td colspan="2"><input type="button" class="mobBtn LstRtlr"
									value="Refresh" onclick="return ValidateWLFromHistory('');" title="Refresh"/>
								</td>
							</tr>
							
						</table>
					</div>
					<div class="clear"></div>
				</div>
				<%@include file="leftmenu.jsp"%>
			</div>
			<div class="clear"></div>
			<c:if
				test="${wishListHistDetails ne null && !empty wishListHistDetails}">
				<div class="pagination algnMnt">
					<p>
						<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
							nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
							pageRange="${sessionScope.pagination.pageRange}"
							url="${sessionScope.pagination.url}" enablePerPage="false"/>
					</p>
				</div>
			</c:if>

		</div>
		<input type="hidden" name="productId" id="productId" value="" />
		<input type="hidden" name="fromWLHistory" id="fromWLHistory"
			value="WLHistory" />
	</form:form>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="gnrlGrd preferencesLst" id="preferedCatgry_view">
	<thead>
		<tr>
			<th align="center"><img src="../images/prefIconSml.png"
				alt="Preferences" width="14" height="13" /></th>
			<th colspan="2" align="left">Select Your Preferences</th>
		</tr>
	</thead>

	<c:forEach items="${dealsCategoryList.mainCategoryDetaillst}"
		var="dealsCategories">
		<tr name="${dealsCategories.mainCategoryName}" class="mainCtgry">
			<td width="10%" align="center" class="wrpWord"><input
				type="checkbox" /></td>

			<td colspan="2" class="wrpWord"><c:out
					value="${dealsCategories.mainCategoryName}" /></td>
		</tr>
		<c:forEach items="${dealsCategories.subCategoryDetailLst}"
			var="dealsSubCategories">

			<tr name="${dealsCategories.mainCategoryName}">
				<td align="center" class="wrpWord">&nbsp;</td>
			
				<c:choose>
				
				<c:when test="${dealsSubCategories.displayed==0}">
				<td width="10%" class="wrpWord"><form:checkbox path="favCatId" value="${dealsSubCategories.categoryId}"/></td>
				</c:when>
				<c:otherwise>
				<td width="10%" class="wrpWord"><form:checkbox path="favCatId" checked = "checked" value="${dealsSubCategories.categoryId}"/></td>
				</c:otherwise>
				</c:choose>
								
				</td>
							
				<td width="80%" class="wrpWord"><c:out
						value="${dealsSubCategories.subCategoryName}" />
				</td>
			</tr>
		</c:forEach>
	</c:forEach>
</table>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link rel="stylesheet" type="text/css"  href="/ScanSeeWeb/styles/bubble-tooltip.css"  media="screen" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>

<SCRIPT LANGUAGE="JavaScript"> 


</script>

	<form:form name="hotdealspreferenceform" commandName="hotdealspreferenceform">
		<div id="content" class="topMrgn">
		<div class="navBar">
    		<ul>
        		<li><a href="#"><img src="../images/backBtn.png" onclick="javascript:back();" alt="back" /></a></li>
            	<li class="titletxt">Preferences</li>
            	<li class="floatR mrgnRt"></li>
      		</ul>
   	   </div>
	   <div class="clear"></div>
   <div class="splitDsply floatR">  
    	<div class="fluidView">
        <div class="cmnPnl">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd" id="Note">
  <thead>
    <tr>
      <th width="34%" align="right"><img src="../images/alert.png" alt="alert" width="16" height="16" /></th>
      <th align="left" class="wrpWord">GPS Disabled
      </th>
    </tr>
  </thead>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd preferencesLst" id="preferedCatgry_view">
  <thead>
    <tr>
      <th align="center"><img src="../images/prefIconSml.png" alt="Preferences" width="14" height="13" /></th>
      <th colspan="2" align="left">Select Your Preferences</th>
    </tr>
  </thead>
  
  <c:forEach items="${sessionScope.dealsCategoryList}" var="dealsCategories">
  <tbody>
    <tr name="Category 1" class="mainCtgry">
      <td width="10%" align="center"  class="wrpWord">
      	<input type="checkbox"/></td>
      <td colspan="2"  class="wrpWord"><c:out value='{dealsCategories.categoryName}' /></td>
    </tr>
    <tr name="Category 1">
      <td align="center" class="wrpWord">&nbsp;</td>
      <td width="10%" class="wrpWord"><input type="checkbox"/></td>
      <td width="80%" class="wrpWord">Category 1-Sub</td>
    </tr>
    <tr name="Category 1">
      <td align="center" class="wrpWord">&nbsp;</td>
      <td class="wrpWord"><input type="checkbox"/></td>
      <td class="wrpWord">Category 1-Sub</td>
    </tr>
    <tr name="Category 1">
      <td align="center" class="wrpWord">&nbsp;</td>
      <td class="wrpWord"><input type="checkbox"/></td>
      <td class="wrpWord">Category 1-Sub</td>
    </tr>
    <tr name="Category 2" class="mainCtgry">
      <td align="center" class="wrpWord"><input type="checkbox"/></td>
      <td colspan="2" class="wrpWord">Category 2</td>
    </tr>
    <tr name="category 3" class="mainCtgry">
      <td align="center" class="wrpWord"><input type="checkbox"/></td>
      <td colspan="2" class="wrpWord" >Category 3</td>
    </tr>
    <tr name="category 3">
      <td align="center" class="wrpWord">&nbsp;</td>
      <td class="wrpWord"><input type="checkbox"/></td>
      <td class="wrpWord">Category 3-sub</td>
    </tr>
    <tr name="category 3">
      <td align="center" class="wrpWord">&nbsp;</td>
      <td class="wrpWord"><input type="checkbox"/></td>
      <td class="wrpWord">Category 3-sub</td>
    </tr>
    <tr name="category 3">
      <td align="center" class="wrpWord">&nbsp;</td>
      <td class="wrpWord"><input type="checkbox"/></td>
      <td class="wrpWord">Category 3-sub</td>
    </tr>
    <tr name="category 4" class="mainCtgry">
      <td align="center" class="wrpWord"><input type="checkbox"/></td>
      <td colspan="2" class="wrpWord" >Category 4</td>
    </tr>
    <tr name="category 4">
      <td align="center" class="wrpWord">&nbsp;</td>
      <td class="wrpWord"><input type="checkbox"/></td>
      <td class="wrpWord">Category 4-sub</td>
    </tr>
    <tr name="category 4">
      <td align="center" class="wrpWord">&nbsp;</td>
      <td class="wrpWord"><input type="checkbox"/></td>
      <td class="wrpWord">Category 4-sub</td>
    </tr>
  </tbody>
  
  </c:forEach>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd detView lstNm htlt" id="HotDeal_listView">
         
           <tr>
          	 <th align="center"><img src="../images/MM_HotDeals_Icon.png" alt="hotdeals" width="28" height="28" /></th>
             <th align="left">Great News !!! Many Hot Deals in Nashville</th>
             <th align="left">&nbsp;</th>
           </tr>
       <tr>
                <td colspan="4"  class="mobSubTitle">Clothing Shoes &amp; Jewelery</td>
              </tr>
               <tr>
                <td colspan="4"  class="mobApi">Koala</td>
              </tr>
           <tr class="mobGrd detView zeroBtmMrgn">
             <td align="center"><img src="../images/imgIcon.png" alt="Img" width="38" height="38" /></td>
             <td><span>Product 2</span>
                 <ul>
                   <li><span>Regular Price:</span>$100</li>
                   <li><span>Sale Price:</span>$100</li>
                 </ul></td>
             <td><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
           </tr>
           <tr class="mobGrd detView zeroBtmMrgn">
             <td align="center"><img src="../images/imgIcon.png" alt="Img" width="38" height="38" /></td>
             <td><span>Product 3</span>
                 <ul>
                   <li><span>Regular Price:</span>$100</li>
                 </ul>
               <ul>
                   <li><span>Sale Price:</span>$100</li>
               </ul></td>
             <td><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
           </tr>
           <tr class="mobGrd detView zeroBtmMrgn">
             <td align="center"><img src="../images/imgIcon.png" alt="Img" width="38" height="38" /></td>
             <td><span><a href="#">Product 4</a></span>
                 <ul>
                   <li><span>Regular Price:</span>$100</li>
                   <li><span>Sale Price:</span>$100</li>
                 </ul></td>
             <td><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
           </tr>
        <tr>
                <td colspan="4"  class="mobSubTitle">Grocery, Health &amp; Beauty</td>
              </tr>
              <tr>
                <td colspan="4"  class="mobApi">Koala</td>
              </tr>
           <tr class="mobGrd detView zeroBtmMrgn">
             <td align="center"><img src="../images/imgIcon.png" alt="Img" width="38" height="38" /></td>
             <td><span><a href="#">Product 1</a></span>
                 <ul>
                   <li><span>Regular Price:</span>$100</li>
                   <li><span>Sale Price:</span>$100</li>
                 </ul></td>
             <td><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
           </tr>
            <tr class="mobGrd detView zeroBtmMrgn">
              <td width="8%" align="center"><img src="../images/imgIcon.png" alt="Img" width="38" height="38" /></td>
              <td><span>Product 1</span>
                  <ul>
                    <li><span>Regular Price:</span>$100</li>
                    </ul>
                  <ul>
                    <li><span>Sale Price:</span>$100</li>
                  </ul>                  </td>
              <td width="4%"><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
            </tr>
            <tr class="mobGrd detView zeroBtmMrgn">
              <td align="center"><img src="../images/imgIcon.png" alt="Img" width="38" height="38" /></td>
              <td><span><a href="#">Product 1</a></span>
                  <ul>
                    <li><span>Regular Price:</span>$100</li>
                    <li><span>Sale Price:</span>$100</li>
                </ul></td>
              <td><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
            </tr>
            <tr class="mobGrd detView zeroBtmMrgn">
              <td align="center"><img src="../images/imgIcon.png" alt="Img" width="38" height="38" /></td>
              <td><span><a href="#">Product 1</a></span>
                  <ul>
                    <li><span>Regular Price:</span>$100</li>
                    <li><span>Sale Price:</span>$100</li>
                </ul></td>
              <td><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
            </tr>
          </table>
        </div>
        <div class="cmnPnl">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd detView lstNm" id="hotDealDetails">
            <tr>
              <th width="26%" align="center"><img src="../images/MM_HotDeals_Icon.png" alt="hotdeals" width="28" height="28" /></th>
              <th width="70%" align="left">Great News !!! Many Hot Deals in Nashville</th>
              <th width="4%" align="left">&nbsp;</th>
            </tr>
            <tbody>
              <tr class="mobGrd zeroBtmMrgn">
                <td align="center" valign="top" class="imgBlk"><img src="../images/WatchImg.png" width="64" height="90" alt="WatchImg"/></td>
                <td><span class="names"><a href="#">Product 2</a></span>
                    <ul class="infoView">
                      <li><span>Deal:</span>$100</li>
                      <li><span>Regular Price:</span>$100</li>
                      <li><span>Time Period:</span>Jan 10 - Feb 10</li>
                      <li><span>Details:</span><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.</li>
                      <li><span>Powered By:</span><img src="../images/Yipit.png" width="60" height="21" align="middle" alt="Yipit"/></li>
                    </ul></td>
                <td>&nbsp;</td>
              </tr>
            </tbody>
          </table>
        </div>
    	</div></div>
    	<div class="tabBar">
    		<ul id="sLTab">
    			<li class="MrgnTop"><img src="../images/saveBtn.png" alt="save" id="savePrefCtgry" /></li>
        	</ul>
    	</div>
    	
	</form:form>

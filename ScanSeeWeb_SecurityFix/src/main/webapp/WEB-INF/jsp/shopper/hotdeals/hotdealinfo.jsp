<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script src="https://platform.twitter.com/widgets.js"
	type="text/javascript"></script>

<script>

function twitterShareClick(){
var tweetText;
var hdName='${hotDealDetails.hotDealName}';
var hdURL='${hotDealDetails.hdURL}';
if(hdURL==""){
tweetText='Great find at ScanSee!'+hdName
}else{
tweetText='Great find at ScanSee! '+hdName+"Visit:"+hdURL
}
var tweeturl = 'http://twitter.com/share?text='+escape(tweetText);

window.open(tweeturl);

 twttr.events.bind('tweet', function(event) {
       alert("Lottery Entry Successful");
       window.location = "http://www.mysite.com"
    });
}

</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	id="hotDealDetails" class="gnrlGrd detView lstNm">

	<tr>
		<th width="26%" align="center"><img
			src="../images/MM_HotDeals_Icon.png" alt="hotdeals" width="28"
			height="28" />
		</th>
		<th width="70%" align="left"><c:out
				value="${hotDealDetails.hotDealName}" /></th>
		<th width="4%" align="left">&nbsp;</th>
	</tr>
	<tbody>

		<tr class="mobGrd zeroBtmMrgn">
			<td align="center" valign="top" class="imgBlk"><img
				src="<c:out
							value="${hotDealDetails.hotDealImagePath}" />"
				width="64" height="90" />
			</td>
			<td><span class="names"><a href="#"><c:out
							value="${hotDealDetails.hotDealName}" /> </a> </span>
				<ul class="infoView">
					<li><span>Deal:$</span> <c:out
							value="${hotDealDetails.hDSalePrice}" /></li>
					<li><span>Regular Price:$</span> <c:out
							value="${hotDealDetails.hDPrice}" /></li>
					<li><span>Time Period:</span> <c:out
							value="${hotDealDetails.hDStartDate}" />-<c:out
							value="${hotDealDetails.hDEndDate}" />
					</li>
					<li><span>Details:</span><strong>
							${hotDealDetails.hDLognDescription} </strong></li>



					<c:if
						test="${hotDealDetails.hdURL ne null&& hotDealDetails.hdURL ne ''}">
						<li><span></span><a href="${hotDealDetails.hdURL}"
							target="_blank"><img src="../images/getDeal.png"
								alt="getdeal" width="76" height="19" title="Get Deal" /> </a></li>
					</c:if>
					<li><span>Powered By:</span><img
						src="<c:out
						value="${hotDealDetails.apiPartnerImagePath}" />" />
					</li>
				</ul>
			</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<ul id="subnavTab">
	<li><a href="#"><img height="50" width="80"
			onclick="location.href='/ScanSeeWeb/shopper/fbShareHotDealInfo.htm'"
			alt="facebook" src="../images/tab_btn_up_facebook.png"> </a></li>

	<li><img style="cursor:pointer;" alt="twitter"
		src="../images/tab_btn_up_twitter.png" onclick="twitterShareClick();">
	</li>

	<li><a href="#"><img alt="sendmail"
			src="../images/tab_btn_up_sendmail.png"
			onclick="openEMailSharePopUpGeneral(<c:out
				value="${hotDealDetails.hotDealId}" />);">
	</a></li>
</ul>


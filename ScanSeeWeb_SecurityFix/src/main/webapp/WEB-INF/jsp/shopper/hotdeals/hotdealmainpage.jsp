<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcoZEmDQJTP5iDa_aZIaHS5H9xdAdWG6w&sensor=true">
	
</script>
<form:form commandName="hotdealmainpage" name="hotdealmainpage"
	id="hotdealmainpage">
	<c:choose>
		<c:when test="${displaymap eq true }">
			<script type="text/javascript">
				window.onload = function() {
					setCityValue();
					showMap();
					$('#searchhdbtm').show();
				}
			</script>
		</c:when>

		<c:otherwise>
			<script type="text/javascript">
				window.onload = function() {

					$('#mapviews').hide();
				}
			</script>
		</c:otherwise>
	</c:choose>


	<script type="text/javascript">
		function showMap() {

			var cityName = document.hotdealmainpage.cityNameTL.value;

			if (cityName != "") {
				showAddress(cityName);
			} else {
				showAddress('${sessionScope.loginuser.city}');
			}
		}

		function setCityValue() {
			document.hotdealmainpage.cityNameTL.value = '${sessionScope.searchCity}';
		}
	</script>

	<div id="content" class="topMrgn">
		<div class="navBar">
			<ul>

				<li class="titletxt">Hot Deals</li>


				<li class="floatR mrgnRt"><a href="#"
					onclick="location.href='/ScanSeeWeb/shopper/hdcategoryPref.htm'"><img
						src="/ScanSeeWeb/images/prefrCatg_btn.png" alt="preferedCtgry" />
				</a>
				</li>

			</ul>
		</div>
		<div class="clear"></div>
		<div class="mobCont noBg">

			<div class="splitDsply floatR">

				<form:hidden path="populationCenterID" id="populationCenterID" />
				<form:hidden path="hotDealID" id="hotDealID" />
				<form:hidden path="popCntCity" id="popCntCity" />
				<input type="hidden" name="latitude" id="latitude" value="" /> <input
					type="hidden" name="longitude" id="longitude" value="" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="brdrLsTbl topSubBar" id="srchBytxt">
					<tr>
						<td width="8%">By City:</td>
						<td width="20%"><form:input path="dMAName" type="text"
								class="textboxGenSrch" />
						</td>
						<td width="7%"><a href="#"><img
								src="../images/byCitySearch.png" alt="ByCity" name="ByCity"
								width="34" height="24" title="Search By City"
								onclick="getPopulationCenters();" /> </a>
						</td>
						<td width="9%"><a href="#" class="smlLinkstxt">By Map</a></td>
						<td width="20%"><input type="text" name="cityNameTL"
							class="textboxGenSrch" />
						</td>
						<td width="7%"><a href="#"> <img
								src="../images/byMapSearch.png" alt="ByMap" width="34"
								height="24" title="Search By Map" name="ByMap"
								onclick="showMap();" /> </a>
						</td>
						<td width="21%"><form:input path="searchKey" type="text"
								class="textboxGenSrch" />
						</td>
						<td width="8%"><a href="#"><img
								src="../images/byTextSearch.png" alt="SearchByText"
								name="SearchByText" width="34" onclick="searchHotDeal();"
								height="24" title="SearchByText" /> </a>
						</td>
					</tr>
				</table>

				<div class="clear"></div>


				<div class="fluidViewHD">

					<div class="cmnPnl">

						<c:if test="${FavCatFlag == 1 && displayNarrowList eq true}">
							<img src="/ScanSeeWeb/images/NarrowListImg.png" alt="Img"
								class="imgLinks"
								onclick="location.href='/ScanSeeWeb/shopper/hdcategoryPref.htm'" />

						</c:if>



						<c:if test="${hotDealsList ne null}">
							<p class="subSearch">
								&nbsp;
								<form:select path="categoryId" id="categoryId" class=""
									onchange="getHotDealforCat()">
									<form:option value="">Select By Category</form:option>
									<form:option value="0">All</form:option>

									<c:if test="${ categoryList ne null}">
										<c:forEach items="${categoryList}" var="s">
											<form:option value="${s.categoryId}"
												label="${s.categoryName}" />
										</c:forEach>
									</c:if>
								</form:select>
							</p>

						</c:if>


						<c:choose>
							<c:when test="${hotDealsList ne null && !empty hotDealsList}">
								<%@include file="hotdealprodlist.jsp"%>
							</c:when>
							<c:when
								test="${populationcenterList ne null && !empty populationcenterList}">
								<%@include file="pupulationcenterlist.jsp"%>
							</c:when>


							<c:when
								test="${dealsCategoryList ne null && !empty dealsCategoryList}">
								<%@include file="hotdealcategorylist.jsp"%>
							</c:when>
							<c:otherwise>

								<span id="NoDataInfo" style="color: red; text-align: center;">No
									Hot Deal found!</span>
							</c:otherwise>


						</c:choose>

					</div>

					<div class="cmnPnl">

						<c:if test="${hotDealDetails ne null}">
							<%@include file="hotdealinfo.jsp"%>
						</c:if>

						<div id="mapviews">
							<div align="center" id="map_canvas"
								style="width: 347px; height: 367px"></div>
							<div id="searchhdbtm">
								<input type="button" name="hdSearchbtn"
									onclick="searchHotDealLocation();" value="Search"
									class="mobBtn" title="Search">
							</div>

						</div>
					</div>

				</div>



			</div>

			<%@include file="../leftmenu.jsp"%>
		</div>

		<c:if test="${hotDealsList ne null && !empty hotDealsList}">
			<div class="pagination algnMnt">
				<p>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
						nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
						pageRange="${sessionScope.pagination.pageRange}"
						url="${sessionScope.pagination.url}" enablePerPage="false"/>
				</p>
			</div>
		</c:if>

		<div class="tabBar">

			<c:if test="${dealsCategoryList ne null && !empty dealsCategoryList}">
				<ul id="sLTab">
					<li class="MrgnTop"><img id="savePrefCtgry"
						onclick="saveFavCat();" alt="save"
						src="/ScanSeeWeb/images/saveBtn.png"></li>
				</ul>
			</c:if>
		</div>
	</div>
</form:form>
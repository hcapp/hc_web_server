<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;">
	<!--<h3>List of Retailers Based on Search Criteria</h3>-->

	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="mobGrd detView zeroBtmMrgn" id="prodView">
		
		
		<c:if
			test="${requestScope.prodcutDetails eq null && empty requestScope.prodcutDetails}">
			<span id="NoDataInfo" style="color: red; text-align: center;">
				No data found!</span>


		</c:if>
		
		
		<!-- Displaying if retailer has ribbenAdImage and ribbenAdurl otherwise retailer name.  -->
		<c:choose>
			<c:when
				test="${requestScope.prodcutDetails ne null && !empty requestScope.prodcutDetails}">
				<tr class="noHvr">
					<td colspan="3" align="center" class="zeroPadding">
						<div class="topBnr">
							<!--Retailer Name-->


							<c:if
								test="${sessionScope.retRibbenAd == 'NotApplicable'}">
								<img src="../images/imgIcon.png" alt="ad" width="320"
									height="50" />
									</c:if>
							<c:if
								test="${sessionScope.retRibbenAd != 'NotApplicable'}">
								<img
									src="${sessionScope.retRibbenAd}"
									alt="ad" width="320" height="50" />
						
							</c:if>
						</div></td>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="4" align="center">${sessionScope.retName}</td>
				</tr>
			</c:otherwise>
		</c:choose>
		
		<c:forEach items="${requestScope.prodcutDetails.productDetail}" var="prodattributes">
			<tr onclick="callprodSummary(${prodattributes.productId},${sessionScope.retailerId},'${prodattributes.salePrice}',${sessionScope.retLocationId},'${sessionScope.retName}')">
				<td width="13%" align="center">
				<c:if test="${prodattributes.imagePath == 'NotApplicable'}" >
						<img src="../images/imgIcon.png" alt="Img" width="38" height="38" />
						</c:if>
						<c:if test="${prodattributes.imagePath != 'NotApplicable'}" >
						<img src="${prodattributes.imagePath}" alt="Img" width="38" height="38"  onerror="this.src = '../images/blankImage.gif';"/>
						</c:if>
				
				</td>
				<td width="81%"><span><c:out value="${prodattributes.productName}" /></span>
					<ul>	
					<li>
						<c:out value="${prodattributes.regularPrice}" />
						</li>
						<li><span>Sale:</span>
						<c:out value="${prodattributes.salePrice}" />
						</li>
					</ul>
				</td>
				<td width="6%"><img src="../images/rightArrow.png" alt="Arrow"
					width="11" height="15" />
				</td>
			</tr>
		</c:forEach>
	</table>
</div>
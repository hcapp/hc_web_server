<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<div class="dataView LRbrdr" style="height: 450px; overflow-x: hidden;"><!--<h3>List of Retailers Based on Search Criteria</h3>-->
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="prodSummary">
            <tr class="subBar">
              <td colspan="2"><span>Rate</span></td>
            </tr>
            <tr class="title">
              <td align="center"><img src="../images/imgIcon.png" alt="Image" width="38" height="38" /></td>
              <td colspan="1"><c:out value="${productNameReview}"/></td>
            </tr>
            <tr>
              <td width="13%">&nbsp;</td>
              <td width="87%">Your Rating:</td>
            </tr>
           <tr>
              <td>&nbsp;</td>
              <td>
			  <div id="formChk" >
			  
			  
                <div onclick="saveUserRating('${requestScope.ratingproductid}')" style="width:120px; height:22px;">
				<input class="star {split:1,callback:function() {$('.test').text(this.value)}}" type="radio" name="Rating" value="1" <c:if test="${requestScope.productRatingReview.userRatingInfo.currentRating eq 1}">checked="checked"</c:if> /> 
                    <input class="star" type="radio" name="Rating" id="Rating" value="2" <c:if test="${requestScope.productRatingReview.userRatingInfo.currentRating eq 2}">checked="checked"</c:if> />
                    <input class="star" type="radio" name="Rating" id="Rating" value="3" <c:if test="${requestScope.productRatingReview.userRatingInfo.currentRating eq 3}">checked="checked"</c:if>/>
                    <input class="star" type="radio" name="Rating" id="Rating" value="4" <c:if test="${requestScope.productRatingReview.userRatingInfo.currentRating eq 4}">checked="checked"</c:if>/>
                    <input class="star" type="radio" name="Rating" id="Rating" value="5" <c:if test="${requestScope.productRatingReview.userRatingInfo.currentRating eq 5}">checked="checked"</c:if>/>
                    
					</div>
					<div style="width:100px"><!--<div style="line-height:22px;">Rating:</div>--><div class="test Smaller valText">&nbsp;</div></div>
          	   </div>
			  </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Average Customer Rating:</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><ul class="rating">
              <c:forEach begin="1" end="${requestScope.productRatingReview.userRatingInfo.avgRating}" var="current">
                <li> <img src="../images/Red_fullstar.png" alt="Yellow" width="20" height="21" /></li>
                </c:forEach>
                 <c:forEach begin="${requestScope.productRatingReview.userRatingInfo.avgRating+1}" end="5" var="current">
                <li> <img src="../images/greyStar.png" alt="Yellow" width="20" height="21" /></li>
                </c:forEach>
                <li class="valText"><c:out value="${requestScope.productRatingReview.userRatingInfo.avgRating}"/></li>
                 <li class="clrRtng"></li>
              </ul>              </td>
            </tr>
          </table>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="reviewGrd">
          
            <tr>
              <td colspan="2" class="subBar"><span>Reviews</span></td>
            </tr>
           <c:forEach items="${requestScope.productRatingReview.productReviews}" var="prodattributes">
            <tr>
              <td align="center"><img src="../images/imgIcon.png" alt="Img" width="38" height="38" /></td>
              <td><strong>Lorem Ipsum</strong><c:out value="${prodattributes.reviewComments}"/></td>
            </tr>
            </c:forEach>
          </table>
        </div>
        
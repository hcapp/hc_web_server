<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;">
	<!--<h3>List of Retailers Based on Search Criteria</h3>-->

	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="mobGrd detView zeroBtmMrgn" id="coupons">

		<!-- Displaying if retailer has ribbenAdImage and ribbenAdurl otherwise retailer name.  -->
		<c:choose>



			<c:when test="${sessionScope.retRibbenAd == 'NotApplicable'}">
				<tr class="noHvr">
					<td colspan="3" align="center" class="zeroPadding">
						<div class="topBnr">
							<!--Retailer Name-->
							<img src="../images/imgIcon.png" alt="ad" width="320" height="50" />
							</div></td></tr>
			</c:when>

			<c:when test="${sessionScope.retRibbenAd != 'NotApplicable'}">
				<tr class="noHvr">
					<td colspan="3" align="center" class="zeroPadding">
						<div class="topBnr">
				<img src="${sessionScope.retRibbenAd}" alt="ad" width="320"
					height="50" />
					</div></td></tr>

			</c:when>
			
			<c:otherwise>
			<tr>
					<td colspan="4" align="center">${sessionScope.retName}</td>
					
	</tr>
			</c:otherwise>
		</c:choose>
	</table>
</div>
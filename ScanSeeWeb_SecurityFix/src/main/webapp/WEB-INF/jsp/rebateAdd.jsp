<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/calendarIcon.png'
		});
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/calendarIcon.png'
		});
	});
</script>
<script type="text/javascript">
	function loadRetailerLoc() {
	$.ajaxSetup({cache:false});
		var retailerId = $('#retailID').val();
		//alert(retailerId);
		if (name == null) {
			alert("Select Retailer;");
		}
		$.ajax({
			type : "GET",
			url : "fetchretailerlocation.htm",
			data : {
				'retailerId' : retailerId,
			},
			success : function(response) {
				$('#myAjax').html(response);

			},
			error : function(e) {
				alert("Eror occurred while Showing Retail Locations");
			}
		});
	}

	function checkAssociatedProd(){
		 var $prdID = $('#prodcutID').val();
		if($prdID!=""){
			$.ajax({
					type : "GET",
					url : "/ScanSeeWeb/checkAssociatedProd.htm",
					data : {
						'productId'  : $prdID
					},
					success : function(response) {
						openIframePopupRebate('ifrmPopup','ifrm','rebateprodupclist.htm',420,600,'View Product/UPC')
						},
					error : function(e) {
						alert('Error:' + 'Error Occured');
					}
				});

		
	}else{
		openIframePopupRebate('ifrmPopup','ifrm','rebateprodupclist.htm',420,600,'View Product/UPC')
	}
	}

	function getRetailerLocTrigger() {
		document.addRebatesform.productName.value = "";
		document.addRebatesform.productID.value = "";
	}
</script>
<script type="text/javascript">
	function validateRebateForm() {
		var startDate = document.getElementById("datepicker1").value;
		var endDate = document.getElementById("datepicker2").value;
		if (compareDate(startDate, endDate, "yes")) {
			addRebates();
			return true;
		}
		return false;
	}
</script>
<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="leftnavigation.jsp"%>

		<form:form name="addRebatesform" commandName="addRebatesform"
			acceptCharset="ISO-8859-1">
			<form:hidden path="retailerLocationID" id="retailerLocationID" />
			<form:hidden path="productID" name="textfield2" id="prodcutID" />
			<form:hidden path="locationHidden" />
			<div class="grdSec contDsply floatR">

				<div id="secInfo">Add Rebates</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="grdTbl">
					<div align="center" style="font-style: 45">
						<label><form:errors cssStyle="color:red" />
						</label>
					</div>
					<tr>
						<td colspan="4" class="header">Add Rebate</td>
					</tr>
					<tr>
						<td width="17%" class="Label"><label for="rbtName"
							class="mand">Rebate Name</label></td>
						<td width="34%"><form:errors path="rebName"
								cssStyle="color:red"></form:errors> <form:input path="rebName"
								type="text" name="rbtName" id="rbtName" maxlength="100" />
						</td>
						<td width="16%" class="Label"><label for="rbtAmount"
							class="mand">Rebate AMT $ </label></td>
						<td width="33%"><form:errors path="rebAmount"
								cssStyle="color:red"></form:errors> <form:input path="rebAmount"
								type="text" name="rbtAmount" id="rbtAmount"
								onkeypress="return isNumberKeyPhone(event)" />
						</td>
					</tr>
					<tr>
						<td class="Label"><label for="rbtAmount" class="mand">Rebate
								Description</label></td>

						<td><form:errors path="rebDescription" cssStyle="color:red"></form:errors>
							<form:textarea path="rebDescription" name="rbtDesc" id="rbtDesc"
								class="txtAreaSmall" onkeyup="checkMaxLength(this,'255');"></form:textarea>
						</td>
						<td class="Label"><label for="rbtAmount" class="mand">Rebate
								Terms &amp; Conditions</label></td>
						<td><form:errors path="rebTermCondtn" cssStyle="color:red"></form:errors>
							<form:textarea path="rebTermCondtn" name="rbtTC" id="rbtTC"
								class="txtAreaSmall" onkeyup="checkMaxLength(this,'1000');"></form:textarea>
						</td>
					</tr>
					<tr>
						<td class="Label"><label for="rbtAmount" class="mand">Number
								of Rebates Issued</label>
						</td>
						<td colspan="3"><form:errors path="noOfrebatesIsued"
								cssStyle="color:red"></form:errors> <form:input
								path="noOfrebatesIsued" name="noofrebatesisuued"
								class="textboxDate" type="text" id="rebateIsued"
								onkeypress="return isNumberKey(event)" /></td>
					</tr>

					<tr>
						<td class="Label"><label for="rebStartDate" class="mand">
								Rebate Start Date</label></td>
						<td align="left"><form:errors path="rebStartDate"
								cssStyle="color:red"></form:errors> <form:input
								path="rebStartDate" name="startDate" type="text"
								class="textboxDate" id="datepicker1" />(mm/dd/yyyy)</td>
						<td class="Label"><label for="rebEndDate" class="mand">Rebate
								End Date</label></td>
						<td align="left"><form:errors path="rebEndDate"
								cssStyle="color:red"></form:errors> <form:input
								path="rebEndDate" name="endDate" type="text" class="textboxDate"
								id="datepicker2" />(mm/dd/yyyy)</td>
					</tr>
					<!--<tr>
						<td class="Label">Rebate Start Time</td>
						<td><form:errors path="rebStartTime"></form:errors> <form:select
								path="rebStartTime" name="select5" class="slctSmall" >
								<c:forEach var="num" begin="00" end="23" step="1">
									<form:option value="${num}">
										<c:out value="${num}" />
									</form:option>
								</c:forEach>
							</form:select>Hrs <form:select path="rebStartTime" name="select5"
								class="slctSmall">
								<c:forEach var="num" begin="00" end="60" step="5">
									<form:option value="${num}">
										<c:out value="${num}" />
									</form:option>
								</c:forEach>
							</form:select>Mins</td>
						<td class="Label">Rebate End Time</td>
						<td><form:errors cssClass="errorMsg" path="rebEndTime"></form:errors>
							<form:select path="rebEndTime" name="select6" class="slctSmall">
								<c:forEach var="num" begin="00" end="23" step="01">
									<form:option value="${num}">
										<c:out value="${num}" />
									</form:option>
								</c:forEach>
							</form:select>Hrs <form:select path="rebEndTime" name="select6"
								class="slctSmall">
								<c:forEach var="num" begin="00" end="55" step="5">
									<form:option value="${num}">
										<c:out value="${num}" />
									</form:option>
								</c:forEach>
							</form:select>Mins
					</tr>
					-->
					<tr>
						<td class="Label"><label for="cst">Rebate Start Time
						</label>
						</td>
						<td><form:select path="rebStartHrs" class="slctSmall">
								<form:options items="${RebateStartHrs}" />
							</form:select> Hrs <form:select path="rebStartMins" class="slctSmall">
								<form:options items="${RebateStartMins}" />
							</form:select> Mins</td>
						<td class="Label"><label for="cet">Rebate End Time</label></td>
						<td><form:select path="rebEndhrs" class="slctSmall">
								<form:options items="${RebateStartHrs}" />
							</form:select> Hrs <form:select path="rebEndMins" class="slctSmall">
								<form:options items="${RebateStartMins}" />
							</form:select> Mins</td>
					</tr>
					<tr>

						<td class="Label"><label for="timeZone">Time Zone</label>
						</td>
						<td colspan="3"><form:select path="rebateTimeZoneId"
								class="selecBx">
								<form:option value="0" label="--Select--">Please Select Time Zone</form:option>
								<c:forEach items="${sessionScope.rebateTimeZoneslst}" var="tz">
									<form:option value="${tz.timeZoneId}"
										label="${tz.timeZoneName}" />
								</c:forEach>

							</form:select>
					</tr>
					<tr>
						<td class="Label"><label for="retailID" class="mand">Select
								Retailer 
						</td>
						<td><form:errors path="retailID" cssStyle="color:red"></form:errors>
							<form:select path="retailID" class="selecBx" id="retailID"
								onchange="loadRetailerLoc()" >
								<form:option value="0" label="--Select--">--Select--</form:option>
								<c:forEach items="${sessionScope.retailerList}" var="s">
									<form:option value="${s.retailID}" label="${s.retailName}" />
								</c:forEach>
							</form:select>
						</td>
						<td class="Label"><label for="retailerLocID" class="mand">Select
								Retailer Loc 
						</td>
						<td><form:errors path="retailerLocID" cssStyle="color:red"></form:errors>
							<div id="myAjax">
								<form:select path="retailerLocID" id="retailerLocID">
									<form:option value="0">--Select--</form:option>
								</form:select>
							</div>
						</td>
					</tr>
					<tr>

						<td class="Label"><label for="productName" class="mand">Select
								Product 
						</td>
						<td colspan="3"><form:errors path="productName"
								cssStyle="color:red"></form:errors> <form:input
								path="productName" type="text" name="textfield" id="couponName"
								readonly="true" /> <a href="#"><img
								src="images/searchIcon.png" alt="Search" width="25" height="24"
								onclick=" checkAssociatedProd()"
								title="Click here to View Product/UPC List" /> </a></td>
					</tr>

				</table>
				<div class="navTabSec mrgnRt" align="right">
					<input name="Back" value="Back" type="button" class="btn"
						onclick="location.href='/ScanSeeWeb/rebatesMfg.htm'" title="Back" />
					<input class="btn" onclick="previewRebAdd()" value="Preview"
						type="button" name="Cancel" title="Preview" /> <input class="btn"
						value="Submit" onclick="addRebates();" type="button" name="Cancel"
						title="Submit" />
				</div>

			</div>
			<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
						alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
		</form:form>
	</div>
</div>

<script>
loadRetailerLoc();
</script>

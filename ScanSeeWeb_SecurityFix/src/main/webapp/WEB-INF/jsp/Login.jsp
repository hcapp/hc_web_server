<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<html>

<head>
<title>ScanSee</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)" />
<link href="/ScanSeeWeb/styles/style.css" type="text/css"
	rel="stylesheet" />
<link href="/ScanSeeWeb/styles/style2.css" type="text/css"
	rel="stylesheet" />
<link href="/ScanSeeWeb/styles/ticker-style.css" type="text/css"
	rel="stylesheet" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>
<script type="text/javascript">
function clearMsg(){
	if (document.getElementById('resMsg') != null) {
		document.getElementById('resMsg').style.display = 'none';
	}
}


function forgotForm(){
	document.loginform.linkTab.value="ForgotPwd";
	document.loginform.action = "linkTab.htm";
	document.loginform.method = "post";
	document.loginform.submit();
	}

function loginForm(){
	document.loginform.linkTab.value="Login";
	document.loginform.action = "linkTab.htm";
	document.loginform.method = "post";
	document.loginform.submit();
	}

</script>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>
</head>
<body onload="resizeDoc();">
<body>
	<div class="clear"></div>
	<c:choose>
		<c:when test="${sessionScope.linkTab eq 'Login'&& empty sessionScope.registration }">
			<div id="content" class="topMrgn stretch">
				<form:form name="loginform" commandName="loginform"
					acceptCharset="ISO-8859-1">
					<form:hidden path="fbUserId" name="fbUserId" />
					<form:hidden path="linkTab" />
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="brdrLsTbl">
						<tr>
						<tr>
							<td align="left" valign="top"><strong>Sign in with
									your Facebook account:</strong>
							</td>
							<td width="9%" align="left" valign="top">&nbsp;</td>
							<td width="7%" align="left" valign="top">&nbsp;</td>
							<td width="47%" align="left" valign="top"><strong>Sign
									in with your ScanSee<sup>&#174;</sup> account:</strong>
							</td>
						</tr>
						<tr>
							<td width="37%" align="left" valign="top"><p>Connect
									your Facebook account to sign in to ScanSee<sup>&#174;</sup>.</p>
								<p>&nbsp;</p>
								<p>
									<a href="/ScanSeeWeb/shopper/facebookLogin.htm"><img
										src="/ScanSeeWeb/images/LoginWithFacebookButton.png" alt="Facebook"
										width="152" height="22" /> </a>
								</p>
							</td>
							<td align="left" valign="top"><img src="images/or.png"
								alt="or" />
							</td>
							<td colspan="2" align="left" valign="top">
							
								<div id="loginSec" class="mainLogin">

									<h1>&nbsp;</h1>
									<ul>
										<li><form:errors path="*" cssClass="error"></form:errors>
										</li>
										<li><form:input path="userName" name="UsrName"
												type="text" class="floatR" id="userName" /> <span
											class="floatL"> <label class="mand"><spring:message
														code="label.username" /> </label> </span> <span class="clear"></span> <i
											class="errDisp">Please enter User Name</i></li>
										<form:errors path="userName" cssClass="error"></form:errors>
										<li><form:password path="password" name="loginId"
												type="password" class="floatR" id="pswd" /> <span
											class="floatL"> <label class="mand"><spring:message
														code="label.password" /> </label> </span> <span class="clear"></span> <i
											class="errDisp">Please enter Password </i></li>
										<form:errors path="password" cssClass="error"></form:errors>
										<li><a href="#"  onclick="forgotForm();" class="frgtLnk"><span class="floatR smlTxt">Forgot
													Password?</span> </a><img src="/ScanSeeWeb/images/loginBtn.png"
											alt="login" width="81" height="25" align="right"
											class="loginBtn"
											onclick="validateUserForm(['#pswd','#userName'],'li',chkUser)"
											id="login" title="Login" />
										</li>
									</ul>
									<div class="loginBtm">&nbsp;</div>

								</div>
							</td>
						</tr>
				</table>
				</form:form>
			</div>
			<div class="clear"></div>
		</c:when>
		
		<c:when test="${sessionScope.linkTab eq 'ForgotPwd'|| empty sessionScope.registration }">
			<div id="content" class="topMrgn stretch">
				<form:form name="loginform" commandName="loginform"
					acceptCharset="ISO-8859-1">
					<form:hidden path="fbUserId" name="fbUserId" />
					<form:hidden path="linkTab" />
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="brdrLsTbl">
						<tr>
						<tr>
							<td align="left" valign="top"><strong>Sign in with
									your Facebook account:</strong>
							</td>
							<td width="9%" align="left" valign="top">&nbsp;</td>
							<td width="7%" align="left" valign="top">&nbsp;</td>
							<td width="47%" align="left" valign="top"><strong>Sign
									in with your ScanSee<sup>&#174;</sup> account:</strong>
									
							</td>
						</tr>
						<tr>
							<td width="37%" align="left" valign="top"><p>Connect
									your Facebook account to sign in to ScanSee<sup>&#174;</sup>.</p>
								<p>&nbsp;</p>
								<p>
									<a href="/ScanSeeWeb/shopper/facebookLogin.htm"><img
										src="/ScanSeeWeb/images/LoginWithFacebookButton.png" alt="Facebook"
										width="152" height="22" /> </a>
								</p>
							</td>
							<td align="left" valign="top"><img src="images/or.png"
								alt="or" />
							</td>
							<td colspan="2" align="left" valign="top">
								<div id="loginSec" class="forgotPswd">
						              <h1 class="frgtPswd">&nbsp;</h1>
							              <ul>
							              
								           <c:if test="${requestScope.msgResponse ne null }">
								           <li>
													<div id="message">
														<center>
															 <label  style="${requestScope.fontStyle}">${requestScope.msgResponse}</label>
														</center>
													</div>
													<script>var PAGE_MESSAGE = true;</script>
										   </li>
										  </c:if>
							              
							              <li>Please enter your User Name and Auto-Generated password  will be email to you.</li>
							              <li>
							                <input name="UsrName" type="text"  class="floatR" id="usn"  onKeyPress="return forgetPwd(event)"/>
							                <span class="floatL">
							                <label class="mand">User Name</label>
							                </span>
							                 <span class="clear"></span>
							                <!--   <span class="clear"></span> <i class="errMsg">Please enter User Name</i>-->
							                </li>
							                <li><a href="#" id="send" onclick="loginForm();"><span class="floatR smlTxt">Go back to login</span></a>
							                <img src="images/sendBtn.png" alt="login" width="81" height="25" align="right" class="loginBtn"  id="send" title="Send"  onclick="return forgetPwd('')"/></li>
							                <li class="clear"></li>
							              </ul>
						              <div class="loginBtm">&nbsp;</div>
					            </div>
							</td>
						</tr>
				 </table>
				</form:form>
			</div>
			<div class="clear"></div>
		</c:when>
		
		<c:otherwise>
			<div id="content" class="topMrgn">
				<div class="section shadowImg topMrgn">
					<div class="infoSec">Thank you! We have sent a confirmation
						link to your primary email address.Please check your in-box, add
						us to your safe-senders, and meet us back right here!</div>
					<div id="stretch relatvDiv cmnPnl">
						<div id="loginSec">
						<form:form commandName="loginform" name="loginform">
							<form:hidden path="fbUserId" name="fbUserId" />
							<form:hidden path="linkTab" />

							<h1>&nbsp;</h1>
							<ul>
								<li><form:errors path="*" cssClass="error"></form:errors>
								</li>
								<li><form:input path="userName" name="UsrName" type="text"
										class="floatR" id="userName" tabindex="1" /> <span
									class="floatL"><label class="mand">User Name</label> </span> <span
									class="clear"></span> <i class="errDisp">Please enter User
										Name</i>
								</li>

								<li><form:password path="password" name="password"
										type="password" class="floatR" id="pswd" tabindex="2" /> <span
									class="floatL"><label class="mand">Password</label> </span> <span
									class="clear"></span> <i class="errDisp">Please enter
										Password</i>
								</li>
								<li><a href="#" onclick="forgotForm();"><span class="floatR smlTxt">Forgot
											Password?</span> </a><img src="/ScanSeeWeb/images/loginBtn.png"
									alt="login" width="81" height="25" align="right"
									class="loginBtn"
									onclick="validateUserForm(['#pswd','#userName'],'li',chkUser)"
									id="login" title="Login" />
								</li>
							</ul>
							<div class="loginBtm">&nbsp;</div>
						</form:form>
						</div>
					</div>

				</div>


				<div class="clear"></div>
				<div class="section topMrgn">
					<!-- <div class="block floatL">

					<h3>
						Funding Higher Education<span>Through Commerce Networking </span>
					</h3>
					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and typesetting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img src="/ScanSeeWeb/images/linkArrow.png" alt="LinkBullet" width="4"
								height="6" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
				<div class="block floatL">
					<h3>
						Privacy<span>Respected</span>
					</h3>

					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and typesetting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img src="/ScanSeeWeb/images/linkArrow.png" alt="LinkBullet" width="4"
								height="6" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
				<div class="block floatL clrMargin">
					<h3>
						Customer Testimonials<span>Social</span>
					</h3>
					<div class="section_info">

						<p>
							<img src="/ScanSeeWeb/images/testimonial-img.png" alt="Testimonial" />
						</p>
					</div>
					</div> -->
					<div class="clear"></div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.PlanInfo"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css"  href="/ScanSeeWeb/styles/bubble-tooltip.css"  media="screen" />
<link rel="stylesheet" type="text/css"  href="/ScanSeeWeb/styles/popup.css"  media="screen" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<script src="/ScanSeeWeb/scripts/popup.js" type="text/javascript"></script>
<script type="text/javascript">
	function loadCity() {

		// get the form values
		var stateCode = $('#Country').val();

		if (name == null) {
			alert("Select one Class;");
		}

		$.ajax({
			type : "GET",
			url : "fetchcity.htm",
			data : {
				'statecode' : stateCode,
				'city' : null,
				'tabIndex': 8
			},

			success : function(response) {

				$('#myAjax').html(response);
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}

	function loadBankForm(){
		document.chooseplanform.selectedTab.value="BankPay";
		document.chooseplanform.action = "loadTab.htm";
		document.chooseplanform.method = "post";
		document.chooseplanform.submit();
		}
	
	function loadCreditCardForm(){
		document.chooseplanform.selectedTab.value="CreditCardPay";
		document.chooseplanform.action = "loadTab.htm";
		document.chooseplanform.method = "post";
		document.chooseplanform.submit();
		}

	function loadAlternatePaymentForm(){
		document.chooseplanform.selectedTab.value="AlternatePayment";
		document.chooseplanform.action = "loadTab.htm";
		document.chooseplanform.method = "post";
		document.chooseplanform.submit();
		}
	
	$(document).ready(function() {
		  $('#CreditCardError').hide();
		  $('#ExpDateError').hide();
		  $('#CVVError').hide();
		  $('#FirstNameError').hide();
		  $('#LastNameError').hide();
		  $('#BillingAddressError').hide();
		  $('#StateError').hide();
		  $('#CityError').hide();
		  $('#ZipCodeError').hide();
		  $('#phoneError').hide();
		  $('#checkBoxError').hide();
		  $('#withDashboardLink').hide();
		  $('#withoutDashboardLink').show();
		  document.getElementById('continueToDashboard').style.backgroundColor = "#808080";
		//  $('#ProcessingPaymentresponse').hide();
		  
		  
		});
	
	
	
</script>


<form:form name="chooseplanform" commandName="chooseplanform">
<div id="bubble_tooltip" sty>
  <div class="bubble_top"><span></span></div>
  <div class="bubble_middle"><span id="bubble_tooltip_content">Content is comming here as you probably can see.Content is comming here as you probably can see.</span></div>
  <div class="bubble_bottom"></div>
</div>
<form:hidden path="selectedTab" value="CreditCard"/>
	<div id="wrapper">
		<div id="dockPanel">
			<ul id="prgMtr" class="tabs">
				 <!--   <li> <a title="Home" href="#" rel="home">Home</a> </li> -->
		<!-- 		<li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
				target="_blank" rel="About ScanSee">About ScanSee</a>
			</li> -->
				<!-- <li> <a title="Create Profile" href="Mfg_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->
			<!-- 	<li><a title="Product Setup" href="registprodsetup.htm"
					rel="Product Setup">Product Setup</a></li> -->
				<li><a class="tabActive" title="Choose Your Plan"
					href="choosePlan.htm"  rel="Choose Your Plan">Choose Plan</a></li>
				<li>
					<span id="withDashboardLink">
	      				<a title="Dashboard" href="dashboard.htm" rel="Dashboard">Dashboard</a>
	      			</span>
	      			<span id="withoutDashboardLink">
	      				<a title="Dashboard" href="#" rel="Dashboard">Dashboard</a>
	      			</span>
				</li>
			</ul>
			<a id="Mid" name="Mid"></a>
			<div id="tabdPanelDesc" class="floatR tglSec">
				<div id="filledGlass">
					<img src="images/Step4.png" />
					<div id="nextNav">
						<a href="#" onclick="savePlanInfo()"> <img class="NextNav_R"
							border="0" alt="Next" src="images/nextBtn.png" /> <span>Check-out
								and<br />Get started!</span> </a>
					</div>
				</div>
			</div>
			<div id="tabdPanel" class="floatL tglSec">
				<img alt="Flow5" src="images/Flow5_mfg.png" />
			</div>
		</div>
		<!-- <div id="togglePnl">
			<a href="#"> <img src="images/downBtn.png" alt="down" width="9"
				height="8" /> Show Panel</a>
		</div> -->
		<div id="content" class="MrgnTop">
			<div class="section shadowImg">
				<table class="brdrLsTbl" border="0" cellspacing="0" cellpadding="0"
					width="100%">
					<tbody>
						<tr>
							<td><h5>Thank you! Modular Advertising opportunities to
									fit your budget, season or individual locations. Learn about
									upgrades on your Dashboard:</h5>
							</td>
							<td valign="top" width="43%" align="right"><img
								alt="shopping cart" src="images/shopingCart-img.png"
								width="71" height="46" /></td>
						</tr>
					</tbody>
				</table>
				<div class="grdSec brdrTop">
					<table class="cstmTbl" border="0" cellspacing="0" cellpadding="0"
						width="100%">
						<tbody>
							<tr class="header">
								<td class="editLink">PRODUCT</td>
								<td class="editLink">DESCRIPTION</td>
								<td class="editLink">QTY</td>
								<td class="editLink">PRICE</td>
								<td class="editLink">SUBTOTAL</td>
							</tr>


							<c:forEach items="${sessionScope.planInfolist}" var="item">
								<tr>
									<td class="editLink"><c:out value="${item.productName}" />
									</td>
									<td class="editLink"><c:out value="${item.description}" />
									</td>
									<td class="editLink"><c:out value="${item.quantity}" />
									</td>
									<td class="editLink">$<c:out value="${item.price}" />
									</td>
									<td class="editLink">$<c:out value="${item.subTotal}" />
									</td>
								</tr>
							</c:forEach>

							<tr class="dottedLine">
								<td class="dottedLine"></td>
								<td></td>
								<td></td>
								<td align="center"></td>
								<td></td>
							</tr>

							<tr class="dottedLine">
								<td></td>
								<td></td>
								<td></td>

								<td><strong>8.25% Tax</strong></td>

								<td><strong>$<c:out
											value="${sessionScope.taxAmount}" />
								</strong></td>
							</tr>

							<tr class="dottedLine">
								<td></td>
								<td></td>
								<td></td>

								<td><strong>Total</strong></td>

								<td><strong>$<c:out
											value="${sessionScope.totalPricelist}" />
								</strong></td>
							</tr>
						</tbody>
					</table>




					<c:choose>
					
						<%-- <c:when test="${sessionScope.selectedTab == 'BankPay'}">
							<div id="subnav">
								<ul>
									<!-- <li><a href="#" class="active" rel="ACH/Bank Payment"
										title="ACH/Bank Payment"><span>ACH/Bank Payment</span>
									</a>
									</li> -->
									<li><a  rel="Credit Card Payment" class="active" onClick="loadCreditCardForm();"
										title="Credit Card Payment"><span>Credit Card
												Payment</span>
									</a>
									</li>
									<!-- <li><a href="#" rel="Alternate Payment" onClick="loadAlternatePaymentForm()"
										title="Alternate Payment"><span>Alternate Payment</span>
									</a>
									</li> -->
								</ul>
							</div>

							 <table class="cstmTbl" border="0" cellspacing="0" cellpadding="0"
								width="100%">
								<tbody>
									<tr class="header">
										<td colspan="5" class="editLink">ACH/Bank Information:</td>
									</tr>
									<tr>
										<td width="24%">Bank Name:</td>
										<td width="28%"><form:input path="bankName" type="text"
												id="bankName" name="bankName" maxlength="100" tabindex="1" />
										</td>
										<td width="22%" class="editLink">&nbsp;</td>
										<td width="26%" class="editLink">&nbsp;</td>
									</tr>
									<tr>
										<td>Account Type:</td>
										<td><form:select path="accountType" class="selecBx"
												id="accountType" tabindex="2">
												<form:option value="0" label="--Select--">--Select--</form:option>
												<c:forEach items="${sessionScope.accountTypes}" var="a">
													<form:option value="${a.accountTypeID}"
														label="${a.accountType}" />
												</c:forEach>
											</form:select></td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>Routing Number:</td>
										<td><form:input path="routingNumber" type="text"
												id="routingNumber" name="routingNumber" tabindex="3" /></td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Account Number:</td>
										<td><form:input path="accountNumber" type="text"
												id="accountNumber" name="accountNumber" tabindex="4" /></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>

									<tr>
										<td>Account Holder Name:</td>
										<td><form:input path="accountHolderName" type="text"
												id="accountHolderName" name="accountHolderName" tabindex="5" />
										</td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>Billing Address:</td>
										<td><form:input path="billingAddress" type="text"
												id="billingAddress" name="billingAddress" tabindex="6" /></td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>State:</td>
										<td><form:select path="state" class="selecBx"
												id="Country" onchange="loadCity()" tabindex="7">
												<form:option value="0" label="--Select--">--Select-</form:option>
												<c:forEach items="${sessionScope.statesList}" var="s">
													<form:option value="${s.stateabbr}" label="${s.stateName}" />
												</c:forEach>
											</form:select> &nbsp;</td>
										<td>City:&nbsp;
											<div id="myAjax">
												<form:select path="city" id="City" tabindex="8">
													<form:option value="0">--Select--</form:option>
												</form:select>
											</div></td>
										</td>
										<td>Zip: <form:input path="zip" type="text" id="zip"
												name="zip" tabindex="9" /></td>
									</tr>
									<tr>
										<td>Phone Number:</td>
										<td><form:input path="phoneNumber" type="text"
												id="phoneNumber" name="phoneNumber" tabindex="10"
												onkeyup="javascript:backspacerUP(this,event);"
												onkeydown="javascript:backspacerDOWN(this,event);" /></td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
								</tbody>
							</table> 
						</c:when> --%>
						<c:when test="${sessionScope.selectedTab == 'CreditCardPay' || sessionScope.selectedTab == 'BankPay'}">
							<div id="subnav">
								<ul>
									<!-- <li><a onClick="loadBankForm()"
										rel="ACH/Bank Payment" title="ACH/Bank Payment"><span>ACH/Bank
												Payment</span>
									</a>
									</li> -->
									<li><a href="#" class="active"  rel="Credit Card Payment"
										title="Credit Card Payment"><span>Credit Card
												Payment</span>
									</a>
									</li>
									<!-- <li><a onClick="loadAlternatePaymentForm()"
										rel="Alternate Payment" title="Alternate Payment"><span>Alternate
												Payment</span>
									</a>
									</li> -->
								</ul>
							</div>

							<table class="cstmTbl" border="0" cellspacing="0" cellpadding="0"
								width="100%">
								<tbody>
									<tr class="header">
										<td colspan="5" class="editLink">Credit Card Information:</td>
									</tr>
									<tr>
										<td width="24%">Credit Card Number:</td>
										<td width="28%"><div id="CreditCardError" style="color:red"  >Credit Card Number is invalid</div>
											<form:input path="creditCardNumber"
												type="text" id="creditCardNumber" name="creditCardNumber"
												tabindex="1" autocomplete="off" /></td>
										<td width="22%" class="editLink">&nbsp;</td>
										<td width="26%" class="editLink">&nbsp;</td>
									</tr>
									<tr>
										<td>Expiration Date:</td>
										<td>
										<div id="ExpDateError" style="color:red"  >Expiration Date is invalid</div>
										<form:select id="month" path="month" style="width: 45px; height: 20px" tabindex="2">
												<form:option value="00">MM</form:option>
												<form:option value="01">01</form:option>
												<form:option value="02">02</form:option>
												<form:option value="03">03</form:option>
												<form:option value="04">04</form:option>
												<form:option value="05">05</form:option>
												<form:option value="06">06</form:option>
												<form:option value="07">07</form:option>
												<form:option value="08">08</form:option>
												<form:option value="09">09</form:option>
												<form:option value="10">10</form:option>
												<form:option value="11">11</form:option>
												<form:option value="12">12</form:option>
											</form:select> &nbsp;/&nbsp; <form:select id="year" path="year" style="width: 60px; height: 20px" tabindex="3">
												<form:option value="0000">YYYY</form:option>
												<form:option value="2012">2012</form:option>
												<form:option value="2013">2013</form:option>
												<form:option value="2014">2014</form:option>
												<form:option value="2015">2015</form:option>
												<form:option value="2016">2016</form:option>
												<form:option value="2017">2017</form:option>
												<form:option value="2018">2018</form:option>
												<form:option value="2019">2019</form:option>
												<form:option value="2020">2020</form:option>
												<form:option value="2021">2021</form:option>
												<form:option value="2022">2022</form:option>
												<form:option value="2023">2023</form:option>
												<form:option value="2024">2024</form:option>
												<form:option value="2025">2025</form:option>
											</form:select>
										<td>CVV: <div id="CVVError" style="color:red"  >CVV is invalid</div>
										<form:input path="cVV" type="text" id="cVV"
												name="cVV" tabindex="4" style="width: 100px;"  autocomplete="off"/>
										</td>
										<td>&nbsp;</td>
									</tr>
									<%-- <tr>
										<td>Cardholder Name:</td>
										<td>
										<div id="CardHolderNameError" style="color:red"  >Cardholder Name is invalid</div>
										<form:input path="cardholderName" type="text"
												id="cardholderName" name="cardholderName" tabindex="5" autocomplete="off"/></td>
										<td></td>
										<td>&nbsp;</td>
									</tr> --%>
									
									<tr>
										<td>Cardholder Name:</td>
										<td>
										<div id="FirstNameError" style="color:red"  >First Name is invalid</div>
										First Name:
										<form:input path="firstName" type="text"	id="firstName" name="firstName" tabindex="5" autocomplete="off" cssStyle="width : 150px"/>
										</td>										
										<td>
										Middle Initial:
										<form:input path="middleName" type="text"	id="middleName" name="middleName" tabindex="6" autocomplete="off" cssStyle="width : 50px"/>
										</td>
										<td style="border-right-width: 1px; border-right-style: solid; border-right-color: rgb(218, 218, 218);padding-left: 0px;">
										<div id="LastNameError" style="color:red"  >Last Name is invalid</div>
										Last Name:
										<form:input path="lastName" type="text"	id="lastName" name="lastName" tabindex="7" autocomplete="off" cssStyle="width : 140px"/>
										</td>
										<td>&nbsp;</td>
									</tr>
									
									
									<tr>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Billing Address:</td>
										<td>
										<div id="BillingAddressError" style="color:red"  >Billing Address is invalid</div>
										<form:input path="billingAddress" type="text"
												id="billingAddress" name="billingAddress" tabindex="8" autocomplete="off"/></td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>State:</td>
										<td>
										<div id="StateError" style="color:red"  >State is invalid</div>
										<form:select path="state" class="selecBx" id="Country"
												onchange="loadCity()" tabindex="9">
												<form:option value="0" label="--Select--">--Select--</form:option>
												<c:forEach items="${sessionScope.statesList}" var="s">
													<form:option value="${s.stateabbr}" label="${s.stateName}" />
												</c:forEach>
											</form:select> &nbsp;</td>
										<td>City:&nbsp;
											<div id="CityError" style="color:red"  >City is invalid</div>
											<div id="myAjax">
												<form:select path="city" id="City" tabindex="10">
													<form:option value="0">--Select--</form:option>
												</form:select>
											</div></td>
										
										<td>Zip:
										<div id="ZipCodeError" style="color:red"  >ZipCode is invalid</div>
										 <form:input path="zip" type="text" id="zip"
												name="zip" tabindex="11" style="width: 100px;" autocomplete="off"/></td>
									</tr>
									<tr>
										<td>Phone Number:</td>
										<td>
										<div id="phoneError" style="color:red"  >Phone Number is invalid</div>
										<form:input path="phoneNumber" type="text"
												id="phoneNumber" name="phoneNumber" tabindex="12"
												onkeyup="javascript:backspacerUP(this,event);"
												onkeydown="javascript:backspacerDOWN(this,event);" autocomplete="off"/></td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr><td></td></tr>
									<tr><td></td></tr>
									<tr><td></td></tr>
									<tr>
										<td colspan="4"><font color="#465259"><strong>AUTHORIZATION</strong></font><br>
											<span style="font-size:11px;"><font color="#465259">I hereby authorize ScanSee, Inc.  to charge the indicated credit card monthly for fees associated with services provided, including, if necessary, adjustments for any changes to my account. I agree that this periodic charge will be made according to my account billing cycle, and in order to cancel the recurring billing process, I am required to contact ScanSee, Inc.  one (1) month in advance to either cancel the associated ScanSee, Inc. account, or arrange for an alternative method of payment. I understand that ScanSee, Inc. may not send me any invoices or bills. I agree that if I have any problems or questions regarding my account or any services provided by ScanSee, Inc., I will contact ScanSee, Inc., for assistance using the contact information on their web site at www.scansee.com. I also agree that I will not dispute any charges with my credit card company without first making a good faith effort to remedy the situation directly with ScanSee, Inc. I guarantee and warrant that I am the legal card holder for this credit card and that I am legally authorized to enter into this recurring credit card billing agreement with ScanSee, Inc. 
										</font></span>
										</td>
									</tr>									
									<tr>										
										<td>
										 	<div id="checkBoxError" style="color:red">Please check the checkbox</div>
                    						<input id="checkbox" value="on" type="checkbox" name="checkbox" onclick="validateCheckbox()"/>
                    						<font color="#465259"> <strong>I Agree</strong></font>									
										</td> 
									</tr>
								</tbody>
							</table>
						</c:when>
						
						<c:otherwise>
							<div id="subnav">
								<ul>
									<!-- <li><a onClick="loadBankForm()"
										rel="ACH/Bank Payment" title="ACH/Bank Payment"><span>ACH/Bank
												Payment</span>
									</a>
									</li> -->
									<li><a onClick="loadCreditCardForm();" class="active"
										rel="Credit Card Payment" title="Credit Card Payment"><span>Credit
												Card Payment</span>
									</a>
									</li>
									<!-- <li><a href="#" class="active" rel="Alternate Payment"
										title="Alternate Payment"><span>Alternate Payment</span>
									</a>
									</li> -->
								</ul>
							</div>

							<table class="cstmTbl" border="0" cellspacing="0" cellpadding="0"
								width="100%">
								<tbody>
									<tr class="header">
										<td colspan="5" class="editLink"><a
											onmousemove="showToolTip(event,'To request an invoicing payment option, please submit to accounting@scansee.com. Please remember your username and password. Your progress has been saved.');return false"
											onmouseout="hideToolTip()">Alternate payment option: <img
												alt="helpIcon" src="/ScanSeeWeb/images/helpIcon.png">
										</a>
										</td>
									</tr>
									<tr>
										<td width="24%">Access Code:</td>
										<td width="28%"><form:input path="accessCode" type="text"
												id="accessCode" name="accessCode" /></td>
										<td width="22%" class="editLink">&nbsp;</td>
										<td width="26%" class="editLink">&nbsp;</td>
									</tr>
									<tr>
               <td colspan="4">Please contact ScanSee, Inc. Accounting department for an Access Code via email <a href="mailto:accounting@scansee.com">accounting@scansee.com</a> or phone (214) 217-1491</td>
             </tr>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>

					<div id="popupContact">
						<a id="popupContactClose">X</a>
						<div id="header">
						  <!--<div id="header_link" class="floatR">
						    <ul>
						      <li> <img alt="User" align="left" src="images/icon_user.png"/> Welcome <span>Stephanie Blackwell</span> </li>
						      <li>Aug 10, 2011 </li>
						      <li> <img alt="Logut" align="left" src="images/logoutIcon.gif"/> <a href="index.html">Logout</a> </li>
						    </ul>
						  </div>-->
						  <div id="header_logo" class="floatL"> <a href="index.html"> <img alt="ScanSee" src="images/hubciti-logo_ret.png"/></a> </div>
						  <div class="clear"></div>
						</div>
						<div class="clear"></div>
						<!-- <div class="grdSec"> -->
						  <div id="processCardResponse"> <br />
							    <br />
							    Confirm Payment Receipt <br />
							    <br />
							    Your payment has been authorized. A receipt will be sent to the email provided. <br />
							    <br />
							    Please print the following information for your records: <br />
							    <br />
							    Amount: $1200.00 <br />
							    <br />
							    Contact Information: <br />
							    Name: James Johnson <br />
							    Phone: 972-222-3456 <br />
							    <br />
							    Payment Information: <br />
							    Card: VISA - ************2525 <br />
							    <br />
							    <img src="images/print_icon.jpg" width="15" height="15"> Print 
						 <!--  </div>  -->
						  <div class="clear"></div>
						  <div class="navTabSec">
						    <div align="right">
						      <!--<input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Cancel3"/> <input class="btn" onclick="window.location.href='Manufacturer/adminpage.html'" value="Preview" type="button" name="Cancel"/> <input class="btn" onclick="window.location.href='Manufacturer/adminpage.html'" value="Submit" type="button" name="Cancel"/>-->
						    </div>
						  </div>
						</div>
						    <div class="clear"></div>
					
					
					
					</div>
					<div id="backgroundPopup"></div>
					
					
					
					<div class="navTabSec RtMrgn">
						<div align="right"><!-- validateCard('${sessionScope.selectedTab}') -->
							<input class="planConfirmBtn" onclick="validateCard('${sessionScope.selectedTab}')" value="Process Payment" id="processPaymentButton"
								type="button" name="Cancel3" title="Process Payment" /> 
						    <input
								class="planConfirmBtn" onclick="savePlanInfo();" value="Continue"
								type="button" id="continueToDashboard" name="Cancel3" title="Continue" disabled="disabled"/>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</form:form>

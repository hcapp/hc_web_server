<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.ManageProducts"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
	function checkFileSize(input) {

		if (input.files && input.files[0].size > (1024 * 1024)) {

			alert("File too large. Max 1MB allowed.");
			input.value = null;
		}

	}
</script>
<form:form name="batchform" commandName="batchUploadform"
	enctype="multipart/form-data">
	<form:hidden path="key" value="${sessionScope.autoGenKey}" />
	<div id="wrapper">
		<div id="content" class="shdwBg">

			<%@include file="leftnavigation.jsp"%>
			<div class="grdSec contDsply floatR">
				<div id="secInfo">Manage Products</div>


				<div id="subnav">
					<ul>
						<li><a href="upload.htm" title="Add Product"><span>Add
									Product</span> </a></li>
						<li><a href="batchupload.htm" class="active"
							title="Batch Upload"><span>Batch Upload</span> </a></li>
						<li><a href="manageProduct.htm" title="Manage Products"><span>Manage
									Products</span> </a></li>
					</ul>
				</div>
				<div class="grdSec">

					<table class="tranTbl" border="0" cellspacing="0" cellpadding="0"
						width="100%" align="right">
						<tbody>
							<tr>
								<td class="header brdBtmZero">1) First, upload a file
									containing all of your product information. Choose a File to
									Upload:</td>
							</tr>
							<tr>
								<td><input type="file" class="textboxBig" id="btchUpld"
									name="productuploadFilePath" /><span class="instTxt nonBlk">[
										Please Upload .csv format file ]</span></td>


							</tr>
							<tr>
								<td><form:errors
										cssStyle="${requestScope.productuploadFile}"
										path="productuploadFilePath" />
								</td>
							</tr>
							<tr>
								<td><input class="btn" value="Upload File" type="button"
									name="upldBtn" onclick="uploadProductFile();"
									title="Upload File" /> <a
									href="/ScanSeeWeb/fileController/download.htm?TType=sproductupload"><img
										alt="Download Template" src="images/download.png" /> Download
										template</a>
								</td>
							</tr>
							<tr>
								<td class="header brdBtmZero">2) Next, upload all of your
									product images, audio, and video files. Choose File(s) to
									Upload:</td>
							</tr>
							<tr>
								<td><input type="file" class="textboxBig"
									id="btchUpldMedia" multiple="multiple"
									name="imageUploadFilePath" /><span class="instTxt nonBlk">[
										Please Upload .png, .mp3, .mp4 format file ]</span></td>
							</tr>
							<tr>
								<td><form:errors cssStyle="${requestScope.imageUploadFile}"
										path="imageUploadFilePath" /></td>

							</tr>
							<tr>
								<td><input class="btn" value="Upload File" type="button"
									name="upldBtn" onclick="uploadAudioVideoImageFile();"
									title="Upload File" /></td>
							</tr>

							<tr>
								<td class="header brdBtmZero">3) Next, upload a file
									containing all of your attribute information. Choose File to
									Upload:</td>
							</tr>

							<tr>
								<td><input type="file" class="textboxBig"
									id="btchUpldattribute" name="attributeuploadFile" /><span
									class="instTxt nonBlk">[ Please Upload .csv format file
										]</span></td>
							</tr>
							<tr>
								<td><form:errors cssStyle="${requestScope.attributeupload}"
										path="attributeuploadFile" />
								</td>

							</tr>
							<tr>
								<td><input class="btn" value="Upload File" type="button"
									name="upldBtn" onclick="uploadAttribute();" title="Upload File" />
									<a
									href="/ScanSeeWeb/fileController/download.htm?TType=sattributeupload"><img
										alt="Download Template" src="images/download.png" /> Download
										template</a>
								</td>
							</tr>
							<tr>
								<td class="header brdBtmZero">4) Finally, save your changes
									and preview your upload below.</td>
							</tr>
							<tr>
								<td>For upload instructions, <a href="javaScript:void(0);"
									onClick="window.open('/ScanSeeWeb/html/Mfg_ProdSetup_instructions.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">click
										here.</a></td>
							</tr>
							<tr>
								<td>For frequently asked questions, <a
									href="javaScript:void(0);"
									onClick="window.open('/ScanSeeWeb/html/Mfg_ProdSetup_Faq.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">click
										here.</a></td>
							</tr>
							<tr>
								<td>For recurring XML or API setups, please contact <a
									href="mailto:support@scansee.com?Subject=XML/API%20setup&body=I%20am%20interested%20in%20a%20XML/API%20setup.">support@scansee.com</a>
								</td>
							</tr>

						</tbody>
					</table>
					<div class="clear"></div>
					<div class="navTabSec mrgnRt" align="right">
						<input class="btn" value="Clear" type="button" name="Cancel3"
							onclick="clearBacthUploadForm()" title="Clear the form" /> <input
							class="btn" value="Submit" onclick="saveUploadFile();"
							type="button" name="Cancel" title="Submit" />
					</div>
				</div>
			</div>

		</div>
	</div>
</form:form>

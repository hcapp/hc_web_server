<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- below conditional css is used for Manage product grid screen for IE 9 -->
<!--[if IE 9]>
<style type="text/css" rel="stylesheet">
#batchGrid input[type='text'] {
width:120px!important;
}
</style>
<![endif]-->
<link rel="shortcut icon" href="/ScanSeeWeb/images/ScanSeeLogo.png" />
<link href="/ScanSeeWeb/styles/style.css" type="text/css" rel="stylesheet" />
<link href="/ScanSeeWeb/styles/retailerdashboard.css" type="text/css" rel="stylesheet" />
<link href="/ScanSeeWeb/styles/ticker-style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.MultiFile.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js" type="text/javascript"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/bubble-tooltip.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/validate.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/thislocation.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/shoppinglist.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/shopper.js" type="text/javascript"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>
<body onload="resizeDoc();">
<div id="wrapper">
<div><tiles:insertAttribute name="header" ignore="true" /></div>
<div><tiles:insertAttribute name="body" /></div>
<div id="loading-image"><img src="/ScanSeeWeb/images/ajaxblue.gif" alt="Loading..." title="Loading" width="128" height="15" id="anmtdLdr"/><span id='loading-txt'>Loading</span></div>
<div><tiles:insertAttribute name="footer" /></div>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>  
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld" %>
<%@page session="true"%>
<%@ page import="common.pojo.ManageProducts"%>

<script src="scripts/web.js" type="text/javascript"></script>

<%@taglib prefix="page" uri="/WEB-INF/pagination.tld" %>
<script>
 saveManageProductsPage = 'true';

 submitFlag = 'registration';
</script>
<script type="text/javascript">
$(document).ready(function() {
if ($('#highLight tr').length == "1") { $('input[name$="sub"]').css('visibility','hidden'); } else { $('input[name$="sub"]').css('visibility','visible');}
});
</script>
<div id="wrapper">
 
  <div class="clear"></div>
  <div id="dockPanel">
    <ul id="prgMtr" class="tabs">
    <!--  <li><a title="Home" href="#" rel="home">Home</a></li> -->

				<li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
				target="_blank" rel="About ScanSee">About ScanSee</a>
			</li>
				<!-- <li> <a title="Create Profile" href="Manufacturer/Mfg_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->
				<li><a class="tabActive" title="Product Setup"
					href="registprodsetup.htm" rel="Product Setup">Product Setup</a></li>
				<li><a title="Choose Your Plan"
					href="choosePlan.htm" rel="Choose Your Plan">Choose
						Plan</a></li>

				<li><a title="Dashboard" href="dashboard.htm"
					rel="Dashboard">Dashboard</a></li>
    </ul>
    <div id="tabdPanelDesc" class="floatR tglSec">
      <div id="filledGlass"> <img src="images/Step3.png"/>
        <div id="nextNav"> 
        	<a href="#"  onclick="location.href='/ScanSeeWeb/choosePlan.htm'" > 
        		<img class="NextNav_R" alt="Next" src="images/nextBtn.png"/> <span>Next</span>
        	</a> 
        </div>

      </div>
    </div>
    <div id="tabdPanel" class="floatL tglSec"> <img alt="Flow1" src="images/Flow4_mfg.png"/> </div>
  </div>
 <!--  <div id="togglePnl"><a href="#"> <img src="images/downBtn.png" alt="down" width="9" height="8" /> Show Panel</a></div> -->
  <div id="content" class="">

  		<div id="subnav">
        	<ul><li><a href="registprodsetup.htm" ><span>Add Products</span></a></li><li><a href="batchuploadsupplier.htm"><span>Batch Upload</span></a></li><li><a href="#" class="active"><span>Manage Product</span></a></li></ul>
        </div>
    
    
    <div class="clear"></div>
    <div class="grdSec ">
    <form:form name="myform" commandName="manageform" acceptCharset="ISO-8859-1">
    <form:hidden path="prodJson" />
	<form:hidden path="pageNumber" />
	<form:hidden path="pageFlag" />
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
        <tr>
          <td colspan="2" class="header">Search Products</td>
        </tr>
        <tr>
          <td class="Label" width="23%">Product  name or UPC code</td>
          <td>
          <form:errors cssStyle="color:red" path="productName" cssClass="error"></form:errors>
          <form:input path="productName" type="text" name="productName" id="textfield" />
            <img src="images/searchIcon.png"  alt="Search" title="Search" style="cursor: pointer;"  width="25" height="24" onclick="registeredSearchResult();"/></td>
        </tr>
     
      </table>
	  <div align="center" style="font-style: 90">
						<label><form:errors cssStyle="${requestScope.manageProductFont}" />
						</label>
					</div>
  </form:form>
		
      <div id="scrlGrd" class="brdrTop">
     
        <table class="grdTbl" border="0" cellspacing="0" cellpadding="0" width="100%" id="highLight">
            <tbody>
              <tr>
              <td class="Label" width="18%">Preview</td>
              <td class="Label" width="18%">Create QR</td>
              <td class="Label" width="18%"><label  class="mand">Product UPC</label></td>
              <td class="Label" width="18%"><label  class="mand">Product Name</label></td>
              <td class="Label" width="18%">Model Number</td>
              <td class="Label" width="18%"><label  class="mand">Suggested Retail Price</label></td>
              <td class="Label" width="18%"><label  class="mand">Long Description</label></td>
              <td class="Label" width="18%">Warranty / Service Information</td>
              <td class="Label" width="18%">Short Description</td>
              <td class="Label" width="18%">Category of Product</td>
              <td class="Label" width="18%">Manage Attributes</td>
              <td class="Label" width="18%">Manage Media</td>
              <td class="Label" width="18%">Preview</td>
            </tr>
               <c:forEach items="${sessionScope.seacrhList.manageProductList}" var="item">
            <tr>
              <td align="center"><a href="#" onclick="previewManageProductPopUp(${item.productID},'${item.prodNameEscape}')"><img src="images/searchIcon.png" width="15" height="15" title="Preview"/></a></td>
              <td align="center"><a href="#" onclick="openQRCodePopUp(${item.productID},'${item.prodNameEscape}')"><img src="images/QR-icon.png" alt="Qrcode" width="16" height="16" /></a></td>
              <td>
               <input type="hidden" id="textfield"  size="10" name="productID"  value="${item.productID}"/>
			   <input type="hidden" id="textfield"  size="10" name="productNm" id="productNm" value="${item.productName}"/>
              <input type="text" id="textfield"  size="10" name="productUpc"  maxlength="20" value="${item.scanCode}"/>
              </td>
              <td>
              <input type="text" id="textfield"  size="10" name="prodName"  maxlength="500" value="${item.productName}" />
              </td>
              <td> <input type="text" id="textfield"  size="10" name="modelNumber" maxlength="50" value="${item.modelNumber}" />
              </td>
              <td><input type="text" id="textfield"  size="10" name="suggestedRetailPrice" onkeypress="return isNumberKeyPhone(event)" value="${item.suggestedRetailPrice}"  /></td>
              <td><input type="text" id="textfield"  size="10" name="longDesc" onKeyup="checkMaxLength(this,'1000');" value="${item.longDescription}"/>
              </td>
              <td><input type="text" id="textfield"  size="10" name="warranty" onKeyup="checkMaxLength(this,'255');" value="${item.warranty}"  />
              </td>
              <td><input type="text" id="textfield"  size="10" name="shortDesc" maxlength="255" value="${item.shortDescription}"  />
              </td>
             <td>
           <select  class="selecBx"	id="retailID"  name="category">
										<option value="0" label="--Select--">--Select-</option>
										<c:forEach items="${sessionScope.categoryList}" var="s">
										
											<option  value="${s.categoryID}" ${s.categoryID == item.category ? 'selected' : ''} title="${s.parentSubCategory}"} >${s.parentSubCategory}</option>
										</c:forEach>
									</select>        
		</td>
       <td><input type="button" class="btn" value="Add/Edit" onclick="openManageAttrPopUp(${item.productID})" title="Click here to Manage Attributes" /></td>
       		<td align="center"><input type="button" class="btn" value="Add/Edit"  onclick="openManageAdioVideoPopUp(${item.productID})" title="Click Here To Get Product Media" /></td>
              <td align="center"><a href="#" onclick="previewManageProductPopUp(${item.productID},'${item.prodNameEscape}')"><img src="images/searchIcon.png" width="15" height="15" title="Preview"/></a> </td>
            </tr>
             </c:forEach>
            </tbody>
          </table>
      </div>

    </div>
    
     <div class="pagination">
				<p>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
						nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
						pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" enablePerPage="false"/>
				</p>
			</div>
   <div align="right">
     <!--  <input class="btn"  value="Clear" type="button" name="Cancel3"/> -->
      <input class="btn"  value="Save" onclick="saveMangageProducts('registration');" type="button"  name="sub" title="Save"/>
	  <input class="btn" type="submit" value="Continue" onclick="location.href='/ScanSeeWeb/choosePlan.htm'" name="Cancel3" title="Continue"/>
    </div>
    
    <div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
      <div class="headerIframe"> <img  src="images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"title="Click here to Close" align="middle" /> <span id="popupHeader"></span> </div>
      <iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color:#ffffff" > </iframe>
    </div>
 </div>
 </div>
   

  
 
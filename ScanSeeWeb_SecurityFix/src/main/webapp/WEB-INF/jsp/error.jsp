<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ScanSee</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)"/>
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)"/>
<link href="/ScanSeeWeb/styles/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>

</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
<div id="wrapper">
  <div id="header">
    <!--<div class="floatR" id="header_link">
      <ul>
        <li> <img alt="User" src="images/icon_user.png" align="left"/> Welcome <span>Stephanie Blackwell</span> </li>
        <li>Nov 2, 2011 </li>
        <li> <img alt="Logout" src="images/logoutIcon.gif" align="left"/> <a href="../index.html">Logout</a> </li>
      </ul>
    </div>-->
    <div class="floatL" id="header_logo"> <a href="http://www.scansee.com"> <img alt="ScanSee" src="/ScanSeeWeb/images/scansee-logo.png"/></a> </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
  <div id="content" class="PageNf">
		
        <div align="center"><img src="/ScanSeeWeb/images/ErrorOccured.jpg" alt="404" width="958" height="460" border="0" usemap="#Map" title="Return To Home Page" />
            <map name="Map" id="Map">
              <area shape="rect" coords="887,9,929,54" href="http://www.scansee.com" alt="Home Page" />
              <area shape="rect" coords="730,28,872,47" href="http://www.scansee.com" alt="Home Page" title="Return To Home Page" />
            </map>
          
              </div>
  </div>
  <div id="footer">
    <div id="ourpartners_info">
      <div class="floatL" id="followus_section">
        <div class="section_title">Connect with us!</div>
        <p> <img alt="followus" src="/ScanSeeWe/images/followus-img.png" border="0"/> </p>
      </div>
      <div class="clear"></div>
    </div>
    <div id="footer_nav">
      <ul>
       <li> <a href="http://www.scansee.com/links/aboutus.html">About Us</a> </li>
        <li> <a href="http://www.scansee.com/links/termsuse.html">Terms of Use</a> </li>
        <li> <a href="http://www.scansee.com/links/privacypolicy.html">Privacy Policy</a> </li>
        <li class="last"><a href="http://www.scansee.com/links/contactus.html">Contact Us</a> </li>
        <li class="clear"></li>
      </ul>
      <div class="clear"></div>
      <div id="footer_info">Copyright &copy; 2011 ScanSee. All rights reserved </div>
    </div>
  </div>
</div>
</body>
</html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		var uploadFileName = "${sessionScope.UploadedPDFFile}";
		var uploadFilePath = "${sessionScope.UploadedPDFpath}";
		var displayFlag = false;
		if (uploadFilePath != null && uploadFilePath != "") {

			document.getElementById('uploadFileDiv').style.display = "none";
		} else {

			displayFlag = true;

		}
		if (displayFlag) {

			document.getElementById('upldList').style.display = "none";

		}

		$('#retCreatedPageLocId option').click(function() {
			var totOpt = $('#retCreatedPageLocId option').length;
			var totOptSlctd = $('#retCreatedPageLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		$("#retCreatedPageLocId").change(function() {
			var totOpt = $('#retCreatedPageLocId option').length;
			var totOptSlctd = $('#retCreatedPageLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		var iconId = document.createCustomPage.imageIconID.value;
		$('.iconsPnl li a img').removeClass('active');
		$("#" + iconId).addClass("active");
		var uploadType = document.createCustomPage.imageUplType.value;
		//initially show icons panel	
		$("#" + uploadType).show();
		$('input[value="' + uploadType + '"]').attr('checked', 'checked');

		$(".upldTyp").hide();
		$("#" + uploadType).show();

		$('input[name="upldType"]').change(function() {
			var curSlctn = $(this).attr('value');
			document.createCustomPage.imageUplType.value = curSlctn;
			$(".upldTyp").hide();
			$("#" + curSlctn).show();
		});

	});

	function showPagePreview(pageTyp) {

		var createForm = document.createCustomPage;
		var imagePath = $("#customPageImg").attr('src')
		var retStoreName = '${sessionScope.retailStoreName}';
		var retStoreImage = '${sessionScope.retailStoreImage}';

		// Do validation before you call for Preview

		if (pageTyp == 'Menu') {
			//For main page validtion
			var title = document.createCustomPage.retPageTitle.value;
			var locid = document.createCustomPage.retCreatedPageLocId.value;
			var retShortDesc = document.createCustomPage.retPageShortDescription.value;
			if (title == "") {
				alert('Please enter title');
				return;
			} else if (locid == "" || locid == '0') {
				alert('Please select location');
				return;
			} else if (retShortDesc == "") {
				alert('Please enter  short description');
				return;
			} else {
				showMainPagePview(createForm, imagePath, retStoreName,
						retStoreImage);
			}

		}
	}
	function selectRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("retCreatedPageLocId");
		var vCategoryList = [];
		if (vLocID != "null" && vLocID != "") {
			vCategoryList = vLocID.split(",");
		}
		if (vLocVal.length == vCategoryList.length) {
			document.getElementById('chkAllLoc').checked = true;
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for ( var j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function saveImage(imagepath) {
		document.createCustomPage.imageIconID.value = imagepath;
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("retCreatedPageLocId");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}


	function hideUploadDiv() {
		$("#upldList").hide();
		$("#uploadFileDiv").hide();
	}
</script>
<div class="shdwBg" id="content" style="min-height: 73px;">
	<%@include file="retailerLeftNavigation.jsp"%>
	<div class="grdSec rtContPnl floatR">
		<div id="secInfo">Edit Page</div>
		<form:form commandName="createCustomPage" id="createCustomPage"
			name="createCustomPage" enctype="multipart/form-data"
			action="uploadtempretimg.htm" acceptCharset="ISO-8859-1">
			<form:hidden path="pageId" id="pageId" />
			<form:hidden path="existingPageId" id="existingPageId" />
			<form:hidden path="hiddenLocId" />
			<form:hidden path="imageName" id="imageName" />
			<form:hidden path="retailerImg" id="retailerImg" />
			<form:hidden path="imageUplType" />
			<form:hidden path="imageIconID" />
			<form:hidden path="landigPageType" value="UploadPdf" />
			<form:hidden path="viewName" value="editUploadPDFPage" />
			<form:hidden path="oldPdfFileName" value="${sessionScope.oldUploadedPDFFile}"/>
			<form:hidden path="pdfFileName" value="${sessionScope.UploadedPDFFile}"/>
			<input type="hidden" id="uploadBtn" name="uploadBtn">

			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="rtlr" class="grdTbl cstmpg">
				<tbody>
					<tr>
						<td class="header" colspan="4">Edit Anything Page</td>
					</tr>
					<tr>
						<td width="17%" class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrTitle" class="mand">Title</label></td>
						<td colspan="3"><form:input path="retPageTitle" type="text"
								id="retPageTitle" name="rtlrTitle" /> <form:errors
								path="retPageTitle" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr>
						<td align="left" valign="top" class="Label">Upload Your Own Image or PDF File</td>
						<td colspan="2" class="grdTbl cstmpg"><form:input
								path="retFile" type="file" name="fileField" id="retFile"
								onchange="validateFileExtension(this.value, new Array('pdf', 'jpeg', 'png','PDF','JPEG','PNG','jpg','JPG'));hideUploadDiv();"
								class="textboxBigger" /> <span class="instTxt nonBlk">[
								Please Upload JPEG,PNG,PDF  ]</span> <form:errors path="retFile"
								cssStyle="color:red"></form:errors>
							<div id="upldList" class="upldList">
								<a target="_blank" href="${sessionScope.UploadedPDFpath}">${sessionScope.UploadedPDFFile}<img
									src="../images/imgPreview.png" width="21" height="19"
									title="Preview" alt="Preview" /> </a>
							</div>
							<div id="uploadFileDiv">
								<span>Uploaded File:</span>${sessionScope.UploadedPDFFile}
							</div></td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrLocn" class="mand">Location(s)</label></td>
						<td colspan="1"><label> <form:select
									path="retCreatedPageLocId" id="retCreatedPageLocId"
									name="select" class="txtAreaBox" multiple="true">

									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>

								</form:select> 
								<br/>
								<form:label path="retCreatedPageLocId">Hold Ctrl to select more than one location</form:label>
								<br/> <form:errors path="retCreatedPageLocId" cssStyle="color:red">
								</form:errors> </label></td>

						<td colspan="1" align="left" valign="top" class="Label"><label>
								<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
								onclick="SelectAllLocation(this.checked);" tabindex="2" />
								Select All Locations </label></td>
					</tr>
					<tr>
						<td colspan="4" align="left" valign="top" class="Label"><strong>
								<label> <input name="upldType" type="radio"
									value="slctdUpld" checked="checked" /> </label> Select an icon to
								appear &nbsp; <input type="radio" name="upldType"
								value="usrUpld" /> Upload your own</strong><strong> <label></label>
						</strong><span class="errNtfctn"><form:errors path="retImage"
									cssStyle="color:red"></form:errors> </span> <span class="errNtfctn">
								<label id="customPageImgErr" style="color: red; font-style: 45"></label>
						</span>
						</td>
					</tr>

					<tr>
						<td colspan="4" align="left" valign="top"><ul
								class="iconsPnl upldTyp" id="slctdUpld">
								<c:forEach items="${sessionScope.imageList}" var="s">
									<li class="active"><a href="#"><img
											src="${s.imagePath}" alt="facebook"
											id="${s.qRRetailerCustomPageIconID}" name="retPageImg"
											onclick="javascript:saveImage('${s.qRRetailerCustomPageIconID}');" />
									</a>
									</li>
								</c:forEach>

							</ul>
							<div class="upldTyp" id="usrUpld">
								<label for="hdSp"></label> <label><img
									id="customPageImg" width="80" height="80" alt="upload"
									src="${sessionScope.customPageRetImgPath}"> </label><span
									class="topPadding"><label for="trgrUpld"><input
										type="button" value="Upload" id="trgrUpldBtn"
										class="btn trgrUpld" title="Upload File" tabindex="4">
										<form:input type="file" id="trgrUpld" class="textboxBig"
											path="retImage" /> </label> </span>
							</div></td>

					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
						<td colspan="3"><input class="btn"
							onclick="javascript:history.back()" value="Back" type="button"
							name="Back" /> <input type="button"
							onclick="updateRetailerPage();" value="Save" title="Save"
							class="btn" /></td>
					</tr>

				</tbody>
			</table>
			<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
						alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="no" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
		</form:form>
	</div>

</div>
<div class="clear"></div>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {

						$("#uploadBtn").val("trgrUpldBtn")

						$("#createCustomPage")
								.ajaxForm(
										{

											success : function(response) {
												if (null != document
														.getElementById('retImage.errors')) {
													document
															.getElementById('retImage.errors').style.display = 'none';
												}
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue

												if (imgRes == 'UploadLogoMaxSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {

													$('#customPageImgErr')
															.text("");

													var substr = imgRes
															.split('|');

													if (substr[0] == 'ValidImageDimention') {
														var imgName = substr[1];
														$('#retailerImg').val(
																imgName);
														$('#customPageImg')
																.attr(
																		"src",
																		substr[2]);

													} else {
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}

											}

										}).submit();

					});
</script>
<script>
	selectRtlLocations();
</script>
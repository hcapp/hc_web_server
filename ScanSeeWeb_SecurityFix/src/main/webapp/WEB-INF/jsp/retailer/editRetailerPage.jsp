<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<script src="/ScanSeeWeb/ckeditor/config.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		
		 CKEDITOR.config.uiColor = '#FFFFFF'; 
		 CKEDITOR.replace( 'rtlrshrtDsc', {
			  	width:"660",
			 	toolbar :
					[
					    /* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
					    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
					    { name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
					    '/',
					    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
					    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
					    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
					    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
					    '/',
					    { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
					    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
					    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
					    { name: 'basicstyles', items : [ 'Bold','Italic','Underline'] },
						{ name: 'paragraph',   items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
						{ name: 'styles',    items : [ 'Styles','Format' ] },
						'/',
						{ name: 'tools',    items : [ 'Font','FontSize','RemoveFormat'] },
						{ name: 'colors',      items : [ 'BGColor' ] },
						{ name: 'paragraph',   items : [ 'Outdent','Indent'] },		
						{ name: 'links',       items : [ 'Link','Unlink'] }
					],
		 			removePlugins : 'resize'
	        });
		  CKEDITOR.replace( 'retPageShortDescription', {
			 width:"660",
			 toolbar :
					[
						{ name: 'basicstyles', items : [ 'Bold','Italic','Underline'] },
						{ name: 'paragraph',   items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
						{ name: 'styles',    items : [ 'Styles','Format' ] },
						'/',
						{ name: 'tools',    items : [ 'Font','FontSize','RemoveFormat'] },
						{ name: 'colors',      items : [ 'BGColor' ] },
						{ name: 'paragraph',   items : [ 'Outdent','Indent'] },		
						{ name: 'links',       items : [ 'Link','Unlink'] }
					],
			removePlugins : 'resize'
	        }); 
		

		$('#retCreatedPageLocId option').click(function() {
			var totOpt = $('#retCreatedPageLocId option').length;
			var totOptSlctd = $('#retCreatedPageLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		$("#retCreatedPageLocId").change(function() {
			var totOpt = $('#retCreatedPageLocId option').length;
			var totOptSlctd = $('#retCreatedPageLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		$("#StrtDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$('.ui-datepicker-trigger').css("padding-left", "5px");
		$("#EndDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$('.ui-datepicker-trigger').css("padding-left", "5px");

		//populateDurationPeriod_CustomerPage(document.getElementById("durationCheck").checked);
	});

	function showPagePreview(pageTyp) {

		var createForm = document.createCustomPage;
		var imagePath = $("#customPageImg").attr('src')
		var retStoreName = '${sessionScope.retailStoreName}';
		var retStoreImage = '${sessionScope.retailStoreImage}';

		// Do validation before you call for Preview

		if (pageTyp == 'Menu') {

			//For main page validtion
			var title = document.createCustomPage.retPageTitle.value;
			var locid = document.createCustomPage.retCreatedPageLocId.value;
			var retShortDesc = document.createCustomPage.retPageShortDescription.value;
			if (title == "") {
				alert('Please enter title');
				return;
			} else if (locid == "" || locid == '0') {
				alert('Please select location');
				return;
			} else if (retShortDesc == "") {
				alert('Please enter  short description');
				return;
			} else {

				showMainPagePview(createForm, imagePath, retStoreName,
						retStoreImage);
			}

		}
	}
	function selectRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("retCreatedPageLocId");
		var vCategoryList = [];
		if (vLocID != "null" && vLocID != "") {
			vCategoryList = vLocID.split(",");
		}
		if (vLocVal.length == vCategoryList.length) {
			document.getElementById('chkAllLoc').checked = true;
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for ( var j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function SelectAllLocation(checked) {
		var sel = document.getElementById("retCreatedPageLocId");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
</script>
<div class="shdwBg" id="content" style="min-height: 73px;">
	<%@include file="retailerLeftNavigation.jsp"%>
	<div class="grdSec rtContPnl floatR">
		<div id="secInfo">Edit Page</div>
		<form:form commandName="createCustomPage" id="createCustomPage"
			name="createCustomPage" enctype="multipart/form-data"
			action="uploadtempretimg.htm" acceptCharset="ISO-8859-1">
			<form:hidden path="pageId" id="pageId" />
			<form:hidden path="existingPageId" id="existingPageId" />
			<form:hidden path="hiddenLocId" />
			<form:hidden path="imageName" id="imageName" />
			<form:hidden path="retailerImg" id="retailerImg" />
			<form:hidden path="imageUplType" value="usrUpld" />
			<form:hidden path="landigPageType" value="MakeOwnPage" />
			<form:hidden path="viewName" value="editRetailerPage" />
			<input type="hidden" id="uploadBtn" name="uploadBtn">

			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="rtlr" class="lblSpan cstmpg">
				<tbody>
					<tr>
						<td class="header" colspan="4">Edit Anything Page:</td>
					</tr>

					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrLocn" class="mand">Location(s)</label>
						</td>
						<td colspan="1"><label> <form:select
									path="retCreatedPageLocId" id="retCreatedPageLocId"
									name="select" class="txtAreaBox" multiple="true">

									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>

								</form:select> <br /> <form:label path="retCreatedPageLocId">Hold Ctrl to select more than one location</form:label>
								<br /> <form:errors path="retCreatedPageLocId"
									cssStyle="color:red">
								</form:errors> </label>
						</td>

						<td colspan="2" align="left" valign="top" class="Label"><label>
								<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
								onclick="SelectAllLocation(this.checked);" tabindex="2" />
								Select All Locations </label>
						</td>
					</tr>
					<tr>
						<td width="17%" class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrTitle" class="mand">Title</label>
						</td>
						<td colspan="3"><form:input path="retPageTitle" type="text"
								id="retPageTitle" name="rtlrTitle" /> <form:errors
								path="retPageTitle" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="hdRp" class="mand">Photo</label>
						</td>

						<td colspan="2"><label><img id="customPageImg"
								width="80" height="80" alt="upload"
								src="${sessionScope.customPageRetImgPath}"
								onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
						</label><span class="topPadding forceBlock"><label for="trgrUpld"><input
									type="button" value="${customPageBtnVal}" title="Upload Photo"
									class="btn trgrUpld" id="trgrUpldBtn" />
								<!-- <span
									class="instTxt nonBlk">[ Please Upload .png format
										images ]</span> --> <form:input type="file" id="trgrUpld"
										path="retImage" /> </label> <form:errors path="retImage"
									cssStyle="color:red">
								</form:errors><label id="customPageImgErr" style="color: red; font-style: 45"></label>
						</span></td>
						<td>
							<ul class="actnLst">

								<li><strong>Upload Image Size:</strong><br>Suggested
									Minimum Size:70px/70px<br>Maximum Size:800px/600px<br>
								<!--  Maximum Size:950px/1024px -->
								</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrlngDsc" class="mand">Short Description</label>
						</td>
						<td colspan="3"><form:textarea path="retPageShortDescription"
								id="retPageShortDescription"
								name="retPageShortDescription" /> <form:errors
								path="retPageShortDescription" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrshrtDsc" class="mand">Long Description</label>
						</td>
						<td colspan="3"><form:textarea id="rtlrshrtDsc"
								path="retPageLongDescription"
								name="rtlrshrtDsc" /> <form:errors
								path="retPageLongDescription" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr>
						<td class="Label">Start Date</td>
						<td align="left" class="brdRt"><form:input
								path="retCreatedPageStartDate" type="text" id="StrtDT"
								class="textboxDate" tabindex="9" /> <form:errors
								path="retCreatedPageStartDate" cssStyle="color:red">
							</form:errors><br>
						<span class="instTxt nonBlk">[Start date is not required]</span></td>
						<td class="Label">End Date</td>
						<td class="brdRt"><form:input path="retCreatedPageEndDate"
								type="text" id="EndDT" class="textboxDate" name="EndDT2"
								tabindex="10" />
							<form:errors path="retCreatedPageEndDate" cssStyle="color:red">
							</form:errors><br>
						<span class="instTxt nonBlk">[End date is not required]</span></td>
					</tr>
					<!-- <tr>
						<td></td>
						<td></td>
						<td></td>
						<td align="left"><form:checkbox path="indefiniteAdDurationFlag" id="durationCheck" name="durationCheck" onchange="populateDurationPeriod_CustomerPage(this.checked);"/> No end date
						</td>
						
					</tr> -->
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
						<td colspan="3"><input class="btn"
							onclick="javascript:history.back()" value="Back" type="button"
							name="Back" /> <input type="button" title="Preview"
							value="Preview" class="btn"
							onclick="javascript:showPagePreview('Menu')" /> <input
							type="button" onclick="updateRetailerPage();" value="Save"
							title="Save" class="btn" /></td>

					</tr>

				</tbody>
			</table>
			<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
						alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="no" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
		</form:form>
	</div>

</div>
<div class="clear"></div>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {

						$("#uploadBtn").val("trgrUpldBtn")

						$("#createCustomPage")
								.ajaxForm(
										{

											success : function(response) {

												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue

												if (imgRes == 'UploadLogoMaxSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {

													$('#customPageImgErr')
															.text("");

													var substr = imgRes
															.split('|');

													if (substr[0] == 'ValidImageDimention') {
														var imgName = substr[1];
														$('#retailerImg').val(
																imgName);
														$('#customPageImg')
																.attr(
																		"src",
																		substr[2]);

													} else {
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}
											}

										}).submit();

					});
</script>
<script>
	selectRtlLocations();
</script>
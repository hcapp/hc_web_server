<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<div id="wrapper">

	<form:form name="createsplofferform" commandName="createsplofferform">

		<div id="content" class="shdwBg">
			<%@ include file="retailerLeftNavigation.jsp"%>
			 <div class="rtContPnl floatR">
      <div class="grpTitles">
        <h1 class="mainTitle">Build Special Offer Page </h1>
      </div>
      <div class="section">
        <div class="grdSec brdrTop">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
          
            <tr>
              <td colspan="4"><ul class="imgtxt">
                  <li class="floatL"><img src="../images/buildSpclOffrPgs.png" alt="buildAnythingPgs" width="96" height="96" /></li>
                  <li>
                    <h3>Special Offer Pages </h3>
                  </li>
              </ul>
              <p>Please choose the type of page you would like to create:&#13;</p></td>
            </tr>
         <tr>
              <td width="5%" class="Label">
                 <input type="radio" name="pageType1" id="existingWebPg" value="splofferlinktoExstngPg.htm"/>
             </td>
              <td width="45%" align="left"><label for="existingWebPg">Link  to an Existing Website Page</label></td>
              <td width="5%" class="Label">
                <input type="radio" name="pageType1" id="makeYourOwnPg" value="cstmMainSplPg.htm" />
             </td>
              <td width="45%" align="left"><label for="makeYourOwnPg">Make Your Own</label></td>
            </tr>
          </table>
          </div>
      </div>
      </div>
		</div>
		<div class="clear"></div>
	</form:form>


</div>

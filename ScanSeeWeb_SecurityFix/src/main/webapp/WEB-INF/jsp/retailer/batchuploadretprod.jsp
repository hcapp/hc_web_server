<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="common.pojo.ProductVO"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script src="scripts/web.js" type="text/javascript"></script>

<script type="text/javascript">
	function validateFile() {

		var fileName = document.batchUploadretprodform.fileName.value
		if (fileName == null || fileName == "") {
			alert("Please Select file");
		}

	}
</script>
<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR">
		<div class="grpTitles">
        		<h1 class="mainTitle">Batch Upload</h1>
            </div>
            <div class="grdSec brdrTop">
			<div id="subnav">

				<ul>
					<li><a href="/ScanSeeWeb/retailer/batchuploadretprod.htm"
						class="active"><span>Batch Upload</span> </a></li>
					<li><a href="/ScanSeeWeb/retailer/addseachprod.htm"><span>Search
								/ Add Product</span> </a></li>
				</ul>
			</div>
			<ul class="lstInfo">
				<form:form commandName="batchUploadretprodform"
					name="batchUploadretprodform" enctype="multipart/form-data">
					<form:hidden path="key" value="${sessionScope.autoGenKey}" />
					<li>Please upload a file with all of your product information,
						or scroll below to manually enter each location.</li>

					<li>Uploading your product information will allow consumers to
						view your product(s) online and on the mobile application as shown
						in the preview below.</li>
					<li>Choose a Product File to Upload:</li>
					<li><form:input class="textboxBig" id="fileName"
							path="productuploadFilePath" type="file" /><span
						class="instTxt nonBlk">[ Please Upload .csv format file ]</span>
					</li>
					<li><form:errors cssStyle="${requestScope.productuploadFile}"
							path="productuploadFilePath" />
					</li>
					<li><input class="btn" type="button" value="Upload File"
						name="Cancel2" title="Submit"
						onclick="uploadDashRetProductFile();" /> <a
						href="/ScanSeeWeb/fileController/download.htm?TType=rproductpload"><img
							alt="Download Template" src="../images/download.png" />&nbsp
							Download template</a></li>

					<li>For upload instructions,<a href="javascript:void(0);"
						onClick="window.open('/ScanSeeWeb/html/Retailer_ProdSetup_instructions.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">
							click here.</a>
					</li>
					<li>For frequently asked questions,<a
						href="javascript:void(0);"
						onClick="window.open('/ScanSeeWeb/html/Retailer_LocSetup_Faq.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">
							click here.</a>
					</li>
					<li>For recurring XML or API setups, please contact <a
						href="mailto:support@scansee.com?Subject=XML/API%20setup&body=I%20am%20interested%20in%20a%20XML/API%20setup.">support@scansee.com.</a>
					</li>

				</form:form>
			</ul>
			</div>
		</div>

		<div class="clear"></div>
	</div>
</div>
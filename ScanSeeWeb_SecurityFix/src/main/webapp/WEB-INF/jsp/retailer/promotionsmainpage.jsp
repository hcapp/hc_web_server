<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="content">
	<%@include file="retailerLeftNavigation.jsp"%>
	<div class="rtContPnl floatR">
		<div class="grpTitles">
			<h1 class="mainTitle">Promotions</h1>
		</div>
		<div class="section">
			<div class="textSec brdrTop">
				<p>Promotions allow you to interact directly with
					existing and new customers. The four main types of promotional
					opportunities are:</p>
				<h4 class="MrgnTop">Choose the type of promotion from menu on the left</h4>
				<ol class="infoList MrgnTop orderedLst">
						<li>Special Offers - Let customers know about upcoming or
						recurring specials like Happy Hour or Kids Eat Free.</li>
						<li>Hot Deals - Any discount over 50% off</li>
						<li>Coupons - Tied to locations or products and available to
						be tied into your Point of Sale System.</li>
						<li>Giveaway - Use this for promotional contest or giving away
						incentives to draw customers into your location.</li>
				</ol>
				<!--  <p class="MrgnTop">To Begin building your own promotion choose
					an option below:</p>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="brdrLsTbl">
					<tr>
						<td width="25%"><a href="/ScanSeeWeb/retailer/specialOffer.htm"><img
								src="../images/buildSpclOffr.png" width="146" height="146"
								alt="build special offer" />
						</a>
						</td>
						<td width="25%"><a href="/ScanSeeWeb/retailer/hotDealRetailer.htm"><img
								src="../images/buildHotdeal.png" alt="Hot Deals" width="146"
								height="146" title="Hot Deals" />
						</a>
						</td>
						<td width="25%"><a href="/ScanSeeWeb/retailer/managecoupons.htm"><img
								src="../images/buildCoupon.png" alt="Coupons" width="146"
								height="146" title="Coupons" />
						</a>
						</td>
						<td width="25%"><a href="/ScanSeeWeb/retailer/managegiveawaypages.htm"><img
								src="../images/buildGiveaway.png" alt="Giveaway" width="146"
								height="146" title="Giveaway" />
						</a>
						</td>
					</tr>
					<tr>
						<td><img src="../images/iphone_uploadlogo.png"
							alt="uploadLogo_iPhone" width="124" height="242" />
						</td>
						<td><img src="../images/iphone_buildWelcomePg.png"
							alt="buildWelcomePage" width="124" height="242" />
						</td>
						<td><img src="../images/iphone_buildBannerAd.png"
							alt="BuildBannerAd" width="124" height="242" />
						</td>
						<td><img src="../images/iphone_buildBannerAd.png"
							alt="BuildBannerAd" width="124" height="242" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">Your Logo appears many places
							in your AppSite<sup class="smallsup">TM</sup></td>
						<td align="left" valign="top">Welcome Page is a great way to
							advertise your value proposition or communicate a brand message.
							</td>
						<td align="left" valign="top">are a great waBanners y to tell
							your story and direct consumers where you want them to go</td>
						<td align="left" valign="top">are a great waBanners y to tell
							your story and direct consumers where you want them to go</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				
				</table>
				-->
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
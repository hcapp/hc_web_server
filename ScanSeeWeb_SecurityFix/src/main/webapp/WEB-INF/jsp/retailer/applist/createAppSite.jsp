<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<script type="text/javascript">
	function DisableBackButton() {
		window.history.forward()
	}
	DisableBackButton();
	window.onload = DisableBackButton;
	window.onpageshow = function(evt) {
		if (evt.persisted)
			DisableBackButton()
	}
	window.onunload = function() {
		void (0)
	}
</script>

<script type="text/javascript">

	function submitForm() {
		var wordCount = $('#aboutustxt').val().split(/\s+/).length;
		if (wordCount > 50) {

			alert('Maximum 50 words are allowed')
			return false;

		} else {
			document.createAppSite.action = "createAppSite.htm";
			document.createAppSite.method = "POST";
			document.createAppSite.submit();

		}

	}
	function clearFrom() {

		var r = confirm("Do you really want to clear the form")
		if (r == true) {
			document.createAppSite.aboutUs.value = "";
			if (document.getElementById('aboutUs.errors') != null) {
				document.getElementById('aboutUs.errors').style.display = 'none';
			}
		}
	}
</script>

<div id="dockPanel"></div>
<div id="togglePnl">&nbsp;</div>
<div class="clear"></div>
<div id="content" class="topMrgn">
	<div class="section shadowImg">
		<div class="infoSecB">
			<p>You're almost done!</p>
			<p>Please fill in the box below with information you want people
				to know about your business.</p>
		</div>
		<div class="grdSec brdrTop">
			<form:form commandName="createAppSite" name="createAppSite">
				<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
					width="100%">
					<tbody>
						<tr>
							<td class="header" colspan="2">App Listing:</td>
						</tr>
						<tr>
							<td width="11%" valign="middle" class="Label"><label
								for="addrs1" class="mand">About Us</label></td>
							<td width="89%"><form:errors cssStyle="color:red"
									path="aboutUs"></form:errors> <form:textarea id="aboutustxt"
									path="aboutUs" class="txtAreaLargest" style="height: 100px;	 width: 600px;" rows="5" cols="45"
									name="textarea2"></form:textarea>
								<p>[ Maximum 50 words ]</p>

								<p class="instTxtwrp">
									<b>Things to include: Hours of operation, why your business
										stands out (quality, selection, knowledge, location, price, no
										hassle returns, flexibility, past successes, current events,
										customer service, etc. ) Other contact information Email,
										phones and website address.</b>
								</p> <label for="addrs3" class="titletxt"> </label></td>
						</tr>
						<tr>
							<td colspan="2" align="center" class="Label"><input
								class="btn" value="Clear" type="button" onclick="clearFrom()"
								name="Cancel2" /> <!--<input class="btn" onclick="validateUserForm(['#bsnsInfo'],'td','Retailer_createAppListing3.html')" value="Submit" type="button" name="Cancel"/>-->
								<input class="btn" value="Submit" type="button"
								onclick="return submitForm();" name="Cancel" />
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>
			<div class="navTabSec RtMrgn">
				<div align="right"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="section topMrgn">
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$("#aboutustxt").keydown(
			function(event) {
				var wordCount = $('#aboutustxt').val().split(/\s+/).length;
				//alert(event.keyCode)
				if (wordCount <= 50 || event.keyCode == 8
						|| event.keyCode == 46 || event.keyCode == 37
						|| event.keyCode == 38 || event.keyCode == 39
						|| event.keyCode == 40 || event.keyCode == 98
						|| event.keyCode == 100 || event.keyCode == 102
						|| event.keyCode == 104) {
					return true;
				} else {
					return false;
				}

			});
</script>
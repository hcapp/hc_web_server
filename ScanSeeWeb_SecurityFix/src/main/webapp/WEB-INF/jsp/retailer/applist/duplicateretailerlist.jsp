<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>

<script type="text/javascript">
	function updateDuplicateRet() {

		if ($('#checkboxDel').is(':checked')) {
			document.location.href = '/ScanSeeWeb/retailer/finalizereg.htm'
		} else {

			document.getElementById('validateDuplicateRet').submit();
		}
	}
	function markDuplicate(locId){
		$.ajax({
				type : "GET",
				url : "markAsDuplicateRetailer.htm",
				data : {
					'LocId' : locId
				},
				success : function(response) {
				alert('retailer duplicate successful')
				$('#duplicate'+locId).attr('disabled','disabled').attr('value','Duplicated').attr('style','border: 0;opacity: 0.5;');
				},
				error : function(e) {
					alert('Error: ' + e);
				}
		});
	}
</script>

<div class="clear"></div>

<div id="content" class="topMrgn">
	<div class="section shadowImg">
		<div class="infoSecB">
			Is this you? Check the one that applies most closely to your
			business.</br> If it is a duplicate,Select Report as Duplicate
		</div>
		<form action="validateretailer.htm" id="validateDuplicateRet"
			method="post">
			<div class="grdSec brdrTop">
				<h2 class="zeroBtmMrgn">Retailer Names</h2>
				<div class="searchGrd tableScroll zeroPadding">
				
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="tblEffect" id="duplictVal">

						<tr class="header">
							<td width="7%" align="center"><label></label>
							</td>
							<td width="12%">Retailer Name</td>
							<td width="15%">Address</td>
							<td width="15%">Address2</td>
							<td width="11%">City</td>
							<td width="7%">State</td>
							<td width="8%">Zip Code</td>
							<td width="14%">Phone Number</td>

							<td width="11%">Report</td>
						</tr>
						<c:forEach items="${sessionScope.duplicateRetList}" var="item">

							<tr>
								<td align="center"><label> <input type="radio"
										value="${item.retailID},${item.retailLocationID}" name="retailId" />  </label></td>
								<td><c:out value="${item.retailName}" />
								</td>
								<td><c:out value="${item.address1}" /></td>
								<td><c:out value="${item.address2}" /></td>
								<td><c:out value="${item.city}" /></td>
								<td><c:out value="${item.state}" /></td>
								<td><c:out value="${item.postalCode}" /></td>
								<td><c:out value="${item.corporatePhoneNo}" /></td>
								<td><input type="button" class="btn" value="Duplicate" id = 'duplicate${item.retailLocationID}' onclick="markDuplicate('${item.retailLocationID}');"/>
								</td>
							</tr>

						</c:forEach>

						<tr>
							<td align="center"><input type="radio" name="retailId"
								id="checkboxDel" value="none"/></td>
							<td colspan="8">None of the above</td>
						</tr>
					</table>
				</div>
				<div class="navTabSec RtMrgn">
					<div align="right">
						<input type="submit" class="btn" value="Next"  />
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
	<div class="section topMrgn">
		<div class="clear"></div>
	</div>
</div>

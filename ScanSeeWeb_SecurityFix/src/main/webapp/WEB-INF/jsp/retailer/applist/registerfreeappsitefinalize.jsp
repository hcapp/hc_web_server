<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		if ($('input[name="islocation"]').attr('checked')) {
			$('#storeNum').show();
		} else {
			$('#storeNum').hide();
		}

	});
	function changeStoreNum(checked) {
		if (checked == false) {
			if (document.getElementById('storeNum.errors') != null) {
				document.getElementById('storeNum.errors').style.display = 'none';
			}
		}
	}

	function clearProfile() {
		var r = confirm("Do you really want to clear the form")

		if (r == true) {

			document.appsiteform.webUrl.value = "";

			document.appsiteform.referralName.value = "";
			document.appsiteform.keyword.value = "";
			document.getElementById('bCategory').selectedIndex = -1;
			document.appsiteform.contactFName.value = "";
			document.appsiteform.contactLName.value = "";

			document.appsiteform.terms.checked = false;
			document.appsiteform.islocation.checked = false;
			document.getElementById('contactPhone').value = "";
			document.getElementById('contactPhoneNo').value = "";
			document.appsiteform.contactEmail.value = "";

			document.appsiteform.storeNum.value = "";
			document.getElementById('storeNum').style.display = "none";

			if (document.getElementById('bCategory.errors') != null) {
				document.getElementById('bCategory.errors').style.display = 'none';
			}
			if (document.getElementById('storeNum.errors') != null) {
				document.getElementById('storeNum.errors').style.display = 'none';
			}

			if (document.getElementById('contactFName.errors') != null) {
				document.getElementById('contactFName.errors').style.display = 'none';
			}
			if (document.getElementById('contactLName.errors') != null) {
				document.getElementById('contactLName.errors').style.display = 'none';
			}
			if (document.getElementById('contactPhoneNo.errors') != null) {
				document.getElementById('contactPhoneNo.errors').style.display = 'none';
			}
			if (document.getElementById('contactEmail.errors') != null) {
				document.getElementById('contactEmail.errors').style.display = 'none';
			}

			if (document.getElementById('terms.errors') != null) {
				document.getElementById('terms.errors').style.display = 'none';
			}
			if (document.getElementById('webUrl.errors') != null) {
				document.getElementById('webUrl.errors').style.display = 'none';
			}
		}
	}

	function onLoad() {
		var vCategoryID = document.appsiteform.bCategoryHidden.value;
		var vCategoryVal = document.getElementById("bCategory");
		if (vCategoryID != "null") {
			var vCategoryList = vCategoryID.split(",");
		}
		for ( var i = 0; i < vCategoryVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vCategoryVal.options[i].value == vCategoryList[j]) {
					vCategoryVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function isNumeric(vNum) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;
		var vPostal = document.getElementById("postalCode");
		for (i = 0; i < vNum.length && IsNumber == true; i++) {
			Char = vNum.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				vPostal.value = "";
				vPostal.focus();
				IsNumber = false;
			}
		}
		return IsNumber;
	}
</script>
<div id="dockPanel">
	<ul id="prgMtr" class="tabs">
		<li><a class="tabActive" title="Create Profile" href="#"
			rel="Create Profile">Create Profile</a>
		</li>
	</ul>
</div>
<div id="content" class="topMrgn">
	<div class="section shadowImg">
		<div class="infoSecB">
			You are almost there!<br /> Please enter the below information to
			finalize your setup
		</div>

		<div class="grdSec brdrTop">
			<form:form name="appsiteform" id="appsiteform"
				commandName="appsiteform" acceptCharset="ISO-8859-1">
				<form:hidden path="bCategoryHidden" />				
				<form:hidden path="filters" id ="filters"/>
				<form:hidden path="filterValues" id ="filterValues"/>
				<form:hidden path="filterCategory" id ="filterCategory"/>
				<form:hidden path="hiddenFilterCategory" id ="hiddenFilterCategory"/>
				<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
					width="100%">
					<tbody>
						<tr>
							<td class="header" colspan="4">Retailer Registration Form</td>
						</tr>
						<tr>
							<td class="Label"><label for="url">Website URL</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="webUrl" /> <form:input id="webUrl" tabindex="8"
									path="webUrl" />
							</td>

							<td class="Label"><label for="url">Referral<span
									class="desctxt">(How did you learn about ScanSee's free
										listing for Austin (or enter code if available)?)</span> </label>
							</td>
							<td align="left"><form:input id="referralName" tabindex="9"
									path="referralName" />
							</td>
						</tr>						
						<tr>
							<td class="Label" align="left" rowspan="2"><label for="bcategory" class="mand">Business
									Category</label> <br>(Hold CTRL to select multiple categories)</td>
							<td align="left" rowspan="2"><form:errors cssStyle="color:red" path="bCategory"></form:errors>
								<form:select path="bCategory" id="bCategory" class="txtAreaBox" size="1"
									multiple="true" tabindex="1">
									<c:forEach items="${sessionScope.categoryList}" var="c">
										<c:choose> 
											<c:when test="${c.isAssociated eq true}">
												<form:option value="${c.businessCategoryID}" label="${c.businessCategoryName}" cssClass="toggleFltr"/>
											</c:when>
											<c:otherwise>
												<form:option value="${c.businessCategoryID}" label="${c.businessCategoryName}"/>
											</c:otherwise>
										</c:choose>											
									</c:forEach>
								</form:select>
							</td>
							<td colspan="2" align="left" valign="middle" class="rmvBrd"><label>
				                <input type="checkbox" name="chkAllOptns" id="chkAllCtrgy" />
				                Select All </label> Categories 
				            </td>
						</tr>
						<tr>
			            	<td colspan="2" align="left" valign="middle" class="">
			            		<input type="button" name="selectFilters" id="selectFilters" title="Select Filter(s)" value="Select Filter(s)" class="btn"/>
			            	</td>
			            </tr>
						<tr>
							<td class="Label">Is the business address also a store
								location?</td>
							<td class="left"><form:checkbox path="islocation" id="isLoc"
									name="islocation" tabindex="12"
									onclick="changeStoreNum(this.checked);" /> <form:errors
									cssStyle="color:red" path="storeNum"></form:errors>
								<div id="storeNum" style="display: none">
									<i id="storeNum"> <label for="storeNum" class="mand">Enter
											Store Number</label> <form:input type="text" class="textboxBig"
											id="storeNum" path="storeNum" maxlength="20" tabindex="13" />
									</i>
								</div>
							</td>
							<td class="Label">Associate Organizations
								<p>(e.g. Foursquare,Twitter,Yelp,etc)</p>
							</td>
							<td align="left"><form:textarea id="associateOrg" class="txtAreaSmall"
									rows="5" cols="45" path="associateOrg" tabindex="14"
									cssStyle="height:60px;" /></td>
						</tr>
						<tr>							
							<td class="Label" colspan="2"><label for="keywords"></label>Keywords<span
								class="desctxt">Please enter words people might use to find your
									business while searching the internet.<br> (e.g. the service or
									different types of products you offer, top selling items and
									categories, variations in your store's name, etc)<br> Example:
									restaurant, food, chicken, fried chicken, fries, french fries ,A
									Chicken Store, ChickenStore,The Chicken Store, wide selection, friendly
									staff, competitive pricing </span></td>
							<td align="left" colspan="2"><form:textarea id="keywords" class="txtAreaSmall layoutFix"
									rows="5" cols="45" path="keyword" tabindex="2" cssStyle="height:60px;" />
								<!--<td class="Label" align="left"> <label for="nonprofit">Non-profit Status</label> -->
							</td>							
							
						</tr>
						
						<!--	<tr>
							<td class="Label">Is corporate address also a store
								location?</td>
							<td colspan="3"><form:checkbox path="islocation" id="isLoc"
									name="islocation" tabindex="11"
									onclick="changeStoreNum(this.checked);" /> <form:errors
									cssStyle="color:red" path="storeNum"></form:errors>
								<div id="storeNum" style="display: none">
									<i id="storeNum"> <label for="storeNum" class="mand">Enter
											Store Number</label> <form:input type="text" class="textboxBig"
											id="storeNum" path="storeNum" maxlength="20" tabindex="12" />
									</i>
								</div>
							</td>
						</tr>

						<tr>
							<td class="Label"><label for="numOfStores" class="mand">Number
									of stores</label></td>
							<td colspan="3" align="left"><form:errors
									cssStyle="color:red" path="numOfStores" /> <form:input
									id="numOfStores" path="numOfStores" name="numOfStores"
									onkeypress="return isNumberKey(event)" maxlength="3"
									tabindex="13" />
							</td>
						</tr>

						<tr>
							<td class="Label" colspan="4">&nbsp;</td>
						</tr>
						<tr>
              <td class="Label" colspan="4"><a href="javascript:void(0)" onmousemove="showToolTip(event,'The name of the person that has given you authority to act.');return false" onmouseout="hideToolTip()"> Legal Authority Name <img alt="helpIcon" src="../images/helpIcon.png"></a></td>
            </tr>
            <tr>
              <td class="Label"><label for="legalAuthorityFName" class="mand">First Name</label></td>
              <td align="left"><form:errors cssStyle="color:red" path="legalAuthorityFName"></form:errors>
              <form:input path="legalAuthorityFName" id="legalAuthorityFName" maxlength="20" tabindex="13" /></td>
              <td class="Label" align="left"><label for="legalAuthorityLName" class="mand">Last Name</label></td>
              <td align="left"><form:errors cssStyle="color:red" path="legalAuthorityLName"></form:errors>
              <form:input path="legalAuthorityLName" id="legalAuthorityLName" maxlength="30" tabindex="14"/></td>
            </tr>-->
						<tr>
							<td class="Label" colspan="4"><b>Your ScanSee Account
									Information:</b></td>
						</tr>
						<tr>
							<td class="Label"><label for="contactFName" class="mand">First
									Name</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactFName"></form:errors> <form:input
									path="contactFName" id="contactFName" maxlength="20"
									tabindex="15" />
							</td>
							<td class="Label"><label for="contactLName" class="mand">Last
									Name</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactLName"></form:errors> <form:input
									path="contactLName" id="contactLName" maxlength="30"
									tabindex="16" /></td>
						</tr>
						<tr>
							<td class="Label" align="left"><label for="contactPhoneNo"
								class="mand">Contact Phone #</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactPhoneNo"></form:errors> <form:input
									path="contactPhoneNo" id="contactPhoneNo"
									onkeyup="javascript:backspacerUP(this,event);"
									onkeydown="javascript:backspacerDOWN(this,event);"
									tabindex="17" />(xxx)xxx-xxxx</td>
							<td class="Label" align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>

						<tr>
							<td class="Label"><label for="contactEmail" class="mand">Contact
									Email</label></td>
							<td align="left" colspan="3"><form:errors
									cssStyle="color:red" path="contactEmail"></form:errors> <form:input
									path="contactEmail" id="contactEmail" name="contactEmail"
									maxlength="100" onpaste="return false" ondrop="return false"
									ondrag="return false" oncopy="return false" autocomplete="off"
									tabindex="18" /></td>
						</tr>
						<tr>
							<!--  <td class="Label"><label for="contREml" class="mand">Retype Email</label>              </td>
            		<td align="left" colspan="3"><form:errors cssStyle="color:red" path="retypeEmail"></form:errors>
            		<form:input path="retypeEmail" id="retypeEmail"  name="retypeEmail" maxlength="100" onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false" autocomplete="off" tabindex="18"/> 
            	
							<td class="Label"><label class="mand" for="rtlrName">UserName</label>
							</td>
							<td colspan="3" align="left"><form:errors
									cssStyle="color:red" path="userName"></form:errors> <form:input
									path="userName" type="text" maxlength="100" name="userName"
									id="userName" tabindex="18" />
							</td>
						</tr>
						<tr>
							<td class="Label">&nbsp;</td>
							<td align="left">
								<font color="#50940d">For company authorization, ScanSee verifies companies and non-profits through &nbsp;Lexis Nexis. You must have legal authority with your organization to initiate the &nbsp;ScanSee program.</font> 
								<em>-->
							<td class="Label"
								style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
							<td colspan="3"><input type="hidden" id="locCoordinates"
								value="" /> <form:errors cssStyle="color:red" path="terms"></form:errors>
								<form:checkbox path="terms" name="terms" tabindex="19" /> <font
								color="#465259"> <strong>Accept Terms of Service
										and Use </strong> </font>
								<div id="acptTrms">
									<A HREF="http://www.scansee.com/links/termsconditions.html"
										target="_blank" tabindex="20"> <u>Terms &amp;
											conditions of ScanSee</u> </a>
								</div> <input class="btn" value="Clear" type="button"
								onclick="clearProfile();" tabindex="21" title="Clear the form" />
								<input class="btn" value="Submit" type="button"
								onclick="finalizeAppSiteRetProfileSetUp();" tabindex="22"
								title="Submit" />
							</td>
						</tr>
					</tbody>
				</table>
			</form:form>
			<div class="navTabSec RtMrgn">
				<div align="right"></div>
			</div>
		</div>
	</div>
</div>
<div style="display: none; height: 0px; width: 0px; margin-left: 0px; margin-top: 0px;" id="ifrmPopup" class="ifrmPopupPannel">
	<div class="headerIframe"> 
		<img align="middle" title="Click here to Close" onclick="closeIframePopup('ifrmPopup','ifrm')" alt="close" class="closeIframe" src="/ScanSeeWeb/images/popupClose.png"> 
		<span id="popupHeader">Select Filters</span> 
	</div>
	<iframe width="100%" height="273px" frameborder="0" style="background-color:White" allowtransparency="yes" id="ifrm" scrolling="auto"> </iframe>
</div>
<script>
	onLoad();
</script>
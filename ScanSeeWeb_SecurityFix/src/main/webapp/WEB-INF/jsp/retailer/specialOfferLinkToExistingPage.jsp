<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script>
	$(document).ready(function() {

		$('#splOfferLocId option').click(function() {
			var totOpt = $('#splOfferLocId option').length;
			var totOptSlctd = $('#splOfferLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
		$("#splOfferLocId").change(function() {
			var totOpt = $('#splOfferLocId option').length;
			var totOptSlctd = $('#splOfferLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
		var iconId = document.createCustomPage.imageIconID.value;
		$('.iconsPnl li a img').removeClass('active');
		$("#" + iconId).addClass("active");

		var uploadType = document.createCustomPage.imageUplType.value;
		//initially show icons panel	
		$("#" + uploadType).show();
		$('input[value="' + uploadType + '"]').attr('checked', 'checked');

		$(".upldTyp").hide();
		$("#" + uploadType).show();

		$('input[name="upldType"]').change(function() {
			var curSlctn = $(this).attr('value');
			document.createCustomPage.imageUplType.value = curSlctn;
			$(".upldTyp").hide();
			$("#" + curSlctn).show();
		});

	});

	function saveImage(imagepath) {
		document.createCustomPage.imageIconID.value = imagepath;
	}
	function showPagePreview(pageTyp) {

		var createForm = document.createCustomPage;
		var retStoreName = '${sessionScope.retailStoreName}';
		var landingImagepath = $("#createRetCustomPage").attr('src');
		// Do validation before you call for Preview
		var title = document.createCustomPage.retCreatedPageTitle.value;
		var locid = document.createCustomPage.rtlrcrtdLoc.value;
		if (title == "") {
			alert('Please enter title');
			return;
		} else if (locid = "" || locid == '0') {
			alert('Please select location');
			return;
		} else {
			showLoadingPagePview(createForm, landingImagepath, retStoreName,
					retStoreImage);
		}

	}
	function selectAttachLinkRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;
		var vLocVal = document.getElementById("splOfferLocId");
		var vCategoryList = [];
		if (vLocID != "null" && vLocID != "") {
			vCategoryList = vLocID.split(",");
		}
		if (vLocVal.length != 0 && vCategoryList.length != 0) {
			if (vLocVal.length == vCategoryList.length) {
				document.getElementById('chkAllLoc').checked = true;
			}
			ff
		}

		for ( var i = 0; i < vLocVal.length; i++) {
			for ( var j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("splOfferLocId");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}

	function checkImageSize(input) {

		var bannerImage = document.getElementById("trgrUpld").value;
		//alert("bannerImage" + bannerImage);
		/*if (input.files && input.files[0].size > (100 * 1024)) {
			alert("File too large. Max 100 KB allowed.");
			input.value = null;
		} else*/
		if (bannerImage != '') {
			var checkbannerimg = bannerImage.toLowerCase();
			if (!checkbannerimg.match(/(\.png)$/)) {
				alert("You must upload Welcome Page image with following extensions : .png ");
				document.createCustomPage.retImage.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}
</script>
<div id="wrapper">

	<form:form name="createCustomPage" commandName="createCustomPage"
		id="createCustomPage" action="uploadtempretimg.htm"
		enctype="multipart/form-data" acceptCharset="ISO-8859-1">
		<form:hidden path="hiddenLocId" />
		<form:hidden path="landigPageType" value="AttachLink" />
		<input type="hidden" id="retailerImage" name="retailerImage" />
		<input type="hidden" id="uploadBtn" name="uploadBtn">
		<form:hidden path="splOfferType" value="AttachLink" />
		<form:hidden path="viewName" value="splOfferLinktoExstngPage" />
		<form:hidden path="retailerImg" id="retailerImg" />
		<form:hidden path="imageUplType" />
		<form:hidden path="imageIconID" />
		<form:hidden path="pageId" id="pageId" />
		<div id="content" class="shdwBg">
			<%@ include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">Link to an Existing Website Page?</h1>
				</div>
				<div class="section">
					<div class="grdSec brdrTop">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="grdTbl">
							<tr>
								<td colspan="3"><ul class="imgtxt">
										<li class="floatL"><img
											src="../images/buildSpclOffrPgs.png" alt="" width="96"
											height="96" /></li>
										<li>
											<h3>It is as easy as picking an image. Try it out!</h3>
										</li>
									</ul></td>
							</tr>
							<tr>
								<td align="left" valign="top"><label class="mand">Special
										Offer Page Title</label></td>
								<td colspan="2"><form:input path="splOfferTitle"
										type="text" id="splOfferTitle" tabindex="3" /> <form:errors
										path="splOfferTitle" cssStyle="color:red">
									</form:errors></td>
							</tr>
							<tr class="AttachLink" style="display: table-row;">
								<td class="Label"><label for="label2" class="mand">Web
										Link</label>
								</td>
								<td colspan="3"><form:input path="splOfferattachLink"
										type="text" id="label" name="spclOffrAtF" tabindex="4" /> <form:errors
										path="splOfferattachLink" cssStyle="color:red">
									</form:errors></td>
							</tr>
							<tr>
								<td width="19%" align="left" valign="top" class="Label"><label
									for="productId" class="mand">Location(s)</label></td>
								<td width="31%"><div id="retailLocation"><label> <form:select
											path="splOfferLocId" id="splOfferLocId" name="select"
											class="txtAreaBox" multiple="true" tabindex="2">


											<c:forEach items="${sessionScope.retailerLocList}" var="s">
												<form:option value="${s.retailerLocationID}"
													label="${s.address1}" />
											</c:forEach>

										</form:select>
										<br/>
										<form:label path="splOfferLocId">Hold Ctrl to select more than one location</form:label><br/>
										<form:errors path="splOfferLocId" cssStyle="color:red">
										</form:errors> </label></div>
								</td>
								<td width="50%" align="left" valign="top" class="Label"><label>
										<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
										onclick="SelectAllLocation(this.checked);" /> Select All </label>
									Locations</td>
							</tr>

							<tr>
								<td colspan="3" align="left" valign="top" class="Label"><strong>
										<label> <input name="upldType" type="radio"
											value="slctdUpld" checked="checked" /> </label> Select an icon to
										appear <input type="radio" name="upldType" value="usrUpld" />
										Upload your own</strong><strong> <label></label> </strong><span
									class="errNtfctn"> <form:errors path="retImage"
											cssStyle="color:red"></form:errors> </span> <span class="errNtfctn">
										<label id="customPageImgErr"
										style="color: red; font-style: 45"></label> </span></td>
							</tr>
							<tr>
								<td colspan="4" align="left" valign="top"><ul
										class="iconsPnl upldTyp" id="slctdUpld">
										<c:forEach items="${sessionScope.imageList}" var="s">
											<li class="active"><a href="#"><img
													src="${s.imagePath}" alt="facebook"
													id="${s.qRRetailerCustomPageIconID}" name="retPageImg"
													onclick="javascript:saveImage('${s.qRRetailerCustomPageIconID}');" />
											</a></li>
										</c:forEach>

									</ul>
									<div class="upldTyp" id="usrUpld">


										<table width="100%" border="0" cellspacing="0" cellpadding="0"
											class="brdrLsTbl">
											<tr>
												<td width="20%">
													<ul class="imgInfoSplit">
														<li><label for="hdSp"></label> <label><img
																id="customPageImg" width="80" height="80" alt="upload"
																src="${sessionScope.customPageRetImgPath}"> </label><span
															class="topPadding"><label for="trgrUpld"><input
																	type="button" value="Upload" id="trgrUpldBtn"
																	class="btn trgrUpld" title="Upload File" tabindex="4">
																	<form:input type="file" id="trgrUpld"
																		class="textboxBig" path="retImage" /> </label> </span></li>
													</ul></td>
												<td width="80%" align="left" valign="top">
													<ul>

														<li><strong>Upload Image Size:</strong></li>
														<li>Suggested Minimum Size:70px/70px</li>
														<li>Maximum Size:800px/600px</li>
														<!-- <li>Maximum Size:950px/1024px</li> -->

													</ul></td>
											</tr>
										</table>











									</div>
								</td>

							</tr>

						</table>
						<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
						<div class="navTabSec mrgnRt" align="right">
							<input class="btn" onclick="javascript:history.back()"
								value="Back" type="button" name="Back" /> <input class="btn"
								value="Submit" type="button" name="Cancel"
								onclick="saveSplOfferPage()" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</form:form>
</div>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {
						$("#uploadBtn").val("trgrUpldBtn");
						$("#createCustomPage")
								.ajaxForm(
										{
											success : function(response) {

												if (null != document
														.getElementById('retImage.errors')) {
													document
															.getElementById('retImage.errors').style.display = 'none';
												}
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue

												if (imgRes == 'UploadLogoMaxSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {

													//$('#customPageImg').attr("src",imgRes);
													$('#customPageImgErr')
															.text("");
													var substr = imgRes
															.split('|');

													if (substr[0] == 'ValidImageDimention') {
														var imgName = substr[1];
														$('#retailerImg').val(
																imgName);
														$('#customPageImg')
																.attr(
																		"src",
																		substr[2]);

													} else {
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}

											}
										}).submit();

					});
</script>
<script type="text/javascript">
	selectAttachLinkRtlLocations();
</script>

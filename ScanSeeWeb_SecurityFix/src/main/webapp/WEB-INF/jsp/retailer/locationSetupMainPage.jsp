<div id="wrapper">
	<div id="content">
		<%@include file="retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR">
			<div class="grpTitles">
				<h1 class="mainTitle">Manage Locations</h1>
			</div>
			<div class="section">
				<div class="grdSec brdrTop">
					<div class="textSec">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="brdrLsTbl">
							<tr>
								<td colspan="4"><ul class="splitViewList">
										<li class="splitCel2">Input one location at a time or
											upload a file with all of your locations and let HubCiti&trade;
											Tell your Story to people in your location, looking for your
											location or near your location!</li>
										<!--<li class="splitCel1"> <img src="images/qrcode.png" alt="qrcode" width="49" height="52" /> </li>-->
									</ul></td>
							</tr>
							<tr>
								<td width="19%" align="center">&nbsp;</td>
								<td width="31%" align="center"><img
									src="/ScanSeeWeb/images/inputIcon.png" alt="input" width="76"
									style="cursor: pointer;" height="71"
									onclick="window.location.href='/ScanSeeWeb/retailer/addlocation_dashboard.htm'" />
								</td>
								<td width="34%" align="center"><img
									style="cursor: pointer;"
									src="/ScanSeeWeb/images/uploadIcon.png" alt="uploadIcon"
									onclick="window.location.href='/ScanSeeWeb/retailer/location_dashboardsetup.htm'"
									width="76" height="71" /></td>
								<td width="16%">&nbsp;</td>
							</tr>
							<tr>
								<td align="center">&nbsp;</td>
								<td align="center"><span class="MrgnTop btmPadding">
										<input type="button" value="Input" class="btn" title="Input"
										onclick="window.location.href='/ScanSeeWeb/retailer/addlocation_dashboard.htm'" />
								</span></td>
								<td align="center"><span class="MrgnTop btmPadding">
										<input type="button" value="Upload" class="btn" title="Upload"
										onclick="window.location.href='/ScanSeeWeb/retailer/location_dashboardsetup.htm'" />
								</span></td>

							</tr>
							<tr>
								<td align="center">&nbsp;</td>
								<td align="center"><img
									src="/ScanSeeWeb/images/mngLoctnImg1.png"
									alt="uploadLogo_iPhone" width="149" height="290" /></td>
								<td align="center"><img
									src="/ScanSeeWeb/images/mngLoctnImg2.png" width="149"
									height="290" /></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</div>
				</div>
				<div id="results"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ page
	import="java.util.List,common.pojo.SearchResultInfo,common.pojo.RetailerCustomPage"%>
<script src="/ScanSeeWeb/scripts/jquery.tablednd.js"></script>
<script>
/* Drag and Drop/Reorder/Renumber table row functionality. */
window.onload = function() {
	<%SearchResultInfo retailersList = (SearchResultInfo) request
					.getAttribute("customPageList");
			if (retailersList != null) {
				StringBuffer strSortIndex = new StringBuffer();
				RetailerCustomPage objPage = new RetailerCustomPage();
				List retPageList = retailersList.getPageList();
				if (retPageList != null && !retPageList.isEmpty()) {
					if (retPageList.size() > 1) {
						for (int i = 0; i < retPageList.size(); i++) {
							objPage = (RetailerCustomPage) retPageList.get(i);
							if (objPage.getiSortOrderID() == null) {
								strSortIndex.append(Integer.toString(objPage
										.getiRowId()));
								strSortIndex.append(",");
							} else {
								strSortIndex.append(Integer.toString(objPage
										.getiSortOrderID()));
								strSortIndex.append(",");
							}
						}%>
				document.getElementById("sortOrderID").value = "<%=strSortIndex.toString()%>";
				<%} else {%>
				document.getElementById("displayOrderBtn").style.display = "none";
				<%}%>
			<%} else {%>
				document.getElementById("displayOrderBtn").style.display = "none";
			<%}%>
	<%}%>
}


function reorderPage() {
	var rCount = $('#table-1 tr').length;
	if (rCount == 2) {
		alert("Single record cannot be reorder");
		return false;
	}
	var result = confirm("Do you want to save the reorder changes");
	if (result) {
		saveCustomerPage();
	}
}

function saveCustomerPage() {
	var vPageID = new Array() ;
	$('#table-1 tr').each(function(index) {
		if (index > 0){
			$(this).attr("id"); 
			vPageID.push($(this).find('a').attr("id"));
		}
	});
	var pageNumber = document.customPageForm.pageNumber.value;
	if (pageNumber == null || pageNumber == undefined) {
		document.customPageForm.pageFlag.value = "false";
	} else {
		document.customPageForm.pageNumber.value = pageNumber;
		document.customPageForm.pageFlag.value = "true";
	}
		document.getElementById("noMsg").value = "noMSGDisplay"
		document.getElementById("primaryKeys").value = vPageID.toString();
		showProgressBar();
		document.customPageForm.action = "saveReorderlist.htm";
		document.customPageForm.method = "POST";
		document.customPageForm.submit();
}
</script>

<script>
function buildPage(){
	document.customPageForm.action = "retailercreatedpage.htm";
	document.customPageForm.method = "GET";
	document.customPageForm.submit();
}
function searchPage(){
	showProgressBar();
	var searchKey =  $('input[name$="searchKey1"]').val();
	document.customPageForm.searchKey.value = searchKey;
	document.customPageForm.pageFlag.value = "false";
	document.customPageForm.action = "buildAnythingPage.htm";
	document.customPageForm.method = "GET";
	document.customPageForm.submit();
}

function getPerPgaVal(){

	var selValue=$('#selPerPage :selected').val();
	document.customPageForm.recordCount.value=selValue;
	searchPage();
			
}
</script>
<div id="wrapper">
	<form:form commandName="customPageForm" name="customPageForm">
		<input type="hidden" id="pageId" name="pageId" value="0" />
		<input type="hidden" id="searchKey" name="searchKey" />
		<input type="hidden" id="sortOrderID" name="sortOrderID" />
		<input type="hidden" id="primaryKeys" name="primaryKeys" />
		<input type="hidden" id="noMsg" name="noMsg" />
		<form:hidden path="pageNumber" />
		<form:hidden path="pageFlag" />
		<form:hidden path="recordCount" />
		<div id="content" class="shdwBg">
			<%@include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">
						D. Anything Pages<sup class="smallsup">TM</sup></sup>
					</h1>
				</div>
				<div class="grdSec brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<tr>
							<td width="27%" align="left"><label for="gnrlSrch">Search
									Custom Pages</label></td>
							<td width="52%"><input type="text" name="searchKey1"
								id="searchKey1" maxlength="1000" /> <a href="#"
								onclick="javascript:searchPage();"><img
									src="../images/searchIcon.png" alt="Search" title="Search"
									width="20" height="17" /> </a> <label for="gnrlSrch"></label></td>
							<td width="21%"><input class="btn"
								value="Build Anything Page" type="button" name="Cancel"
								title="Build Anything Page" onclick="buildPage()" /></td>
						</tr>

					</table>
					<div align="center" style="font-style: 90">
						<label style="${requestScope.manageAnyThing}"><c:out
								value="${requestScope.successMSG}" /> </label>
					</div>
					<div class="searchGrd">
						<h1 class="searchHeaderExpand">
							<a href="#" class="floatR">&nbsp;</a>Custom Pages
						</h1>

						<div class="grdCont tableScroll zeroPadding">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="stripeMe" id="table-1">
								<thead>
									<tr class="header">
										<td width="25%">Page Title</td>
										<td width="18%">Start Date</td>
										<td width="18%">End Date</td>
										<td width="39%">Action</td>
									</tr>
								</thead>
								<tbody>
									<c:if test="${message ne null }">
										<div id="message">
											<center>
												<label style="color: red; font-weight: regular;"> <c:out
														value="${message}" /> </label>
											</center>
										</div>

									</c:if>
									<c:if test="${requestScope.customPageList.pageList ne null }">
										<c:forEach items="${requestScope.customPageList.pageList}"
											var="item">

											<!--  <tr id="<c:out value='${item.QRRetailerCustomPageID}'/>">-->
											<tr id="<c:out value='${item.QRRetailerCustomPageID}'/>">
												<td><a href="#"
													id="<c:out value='${item.QRRetailerCustomPageID}'/>"
													onclick="getCustomPageInfo(${item.QRRetailerCustomPageID})">${item.pageTitle}</a>
												</td>
												<td><c:choose>
														<c:when
															test="${item.startDate == null || item.startDate eq 'null' || item.startDate eq ''}">
													No start Date
												</c:when>
														<c:otherwise>
											       ${item.startDate}
											    </c:otherwise>
													</c:choose>
												</td>
												<td><c:choose>
														<c:when
															test="${item.endDate == null || item.endDate eq 'null' || item.endDate eq ''}">
											       No End Date
											    </c:when>
														<c:otherwise>
											       ${item.endDate}
											    </c:otherwise>
													</c:choose>
												</td>
												<td>
												<input type="button" name="button"
															class="btn" id="button" value="Show Locations"
															title="Show Locations"
															onclick="javascript:showAnythingPageLocation(${item.QRRetailerCustomPageID})" />
												<input type="button" class="btn" value="Show QR"
													title="Show QR"
													onclick="openShowRetlrQrPopUp(${item.QRRetailerCustomPageID})" />
													<img src="/ScanSeeWeb/images/deleteRedIcon.png"
													style="cursor: pointer;" title="Delete" alt="Delete"
													width="24" height="22"
													onclick="javascript:deleteCustomPage(${item.QRRetailerCustomPageID})" />
												</td>



											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
							
						</div>
						<div class="pagination brdrTop">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="noBrdr" id="perpage">
									<tr>
										<page:pageTag
											currentPage="${sessionScope.pagination.currentPage}"
											nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
											pageRange="${sessionScope.pagination.pageRange}"
											url="${sessionScope.pagination.url}" enablePerPage="true" />
									</tr>
								</table>
							</div>
							<!--  <div id ="displayOrderBtn" style="display: " align="right" class="navTabSec mrgnRt">
								<input type="button" onclick="reorderPage();" class="btn"
									value="Save Order" name="Save">
							</div>-->
							<ul class="descPnl" id="displayOrderBtn" style="display: ">
								<li class="floatL">To change the order and sort of the
									pages, drag and drop the page to the desired position.</li>
								<li class="floatR"><input type="button"
									onclick="reorderPage();" class="btn" value="Save Order"
									name="Save">
								</li>
								<li class="clear"></li>
							</ul>


							<div class="MrgnTop">
								<img src="/ScanSeeWeb/images/QR-icon.png" alt="qr-icon" /> <a
									class='boldLink'
									href="/ScanSeeWeb/retailer/manageLocationDashboard.htm">Create
									QR for your locations </a>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;background-color: White">
			<div class="headerIframe">
				<img src="../images/popupClose.png" class="closeIframe" alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm');"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
				height="100%" allowtransparency="yes" width="100%"
				style="background-color: White"> </iframe>
		</div>

		<div class="clear"></div>
	</form:form>
</div>


<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<div id="vNav" class="floatL">
	<div class="vNavtopBg"></div>
	<ul>
		<li class="${sessionScope.retlrLeftNav.homeTabStyle}" name="Home">
			<a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/retailerhome.htm')"
			title="Home"> Home </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.appSiteStyle}" name="MyAppsite">
			<a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/retailerhome.htm?appsite=Y')"
			title="My Appsite TM"> My Appsite <sup class="smallsup">TM</sup> <img
				src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
				height="16" /> </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.logoStyle}" name="MyAppsite">
			<a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/uploadRetailerLogo/uploadRetailerLogoDashboard.htm')"
			title="Logo"> Logo </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.welocomePageStyle}"
			name="MyAppsite"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/manageads.htm')"
			title="Welcome Page"> Welcome Page </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.bannerStyle}" name="MyAppsite">
			<a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/managebannerads.htm')"
			title="Banner"> Banner </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.anythingPageStyle}"
			name="MyAppsite"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/buildAnythingPage.htm')"
			title="Anything Page"> Anything Page <sup class="smallsup">TM</sup> </a>
		</li>
		 <li class="${sessionScope.retlrLeftNav.promotionStyle}"
			name="Promotions"><a href="#" onclick="callProgressBar('/ScanSeeWeb/retailer/promotionsdashboard.htm')"  title="Promotions">Promotions<img
				src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
				height="16"/ > </a>
		</li>
		<!-- <li class="${sessionScope.retlrLeftNav.promotionStyle}"
			name="Promotions"><a href="#"  title="Promotions">Promotions<img
				src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
				height="16"/ > </a>
		</li>-->
		<li class="${sessionScope.retlrLeftNav.splPageStyle}"
			name="Promotions"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/specialOffer.htm')"
			title="Special Offer Page"> Special Offer Page </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.dealsStyle}" name="Promotions">
			<a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/hotDealRetailer.htm')"
			title="Hot Deals"> Hot Deals </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.couponStyle}" name="Promotions">
			<a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/managecoupons.htm')"
			title="Coupons"> Coupons</a>
		</li>
		<li class="${sessionScope.retlrLeftNav.giveAwaySubStyle}" name="Promotions">
			<a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/managegiveawaypages.htm')"
			title="Giveaway"> Giveaway </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.analyticsStyle}"
			name="InsightAnalytic"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/dashboard.htm')"
			title="Insights & Analytics"> Insights & Analytics <img
				src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
				height="16" /> </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.manageLocStyle}"
			name="LocationSetup"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/manageLocation.htm')"
			title="Manage Locations"> Manage Locations <img
				src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
				height="16" /> </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.addLocationStyle}"
			name="LocationSetup"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/addlocation_dashboard.htm')">
				Add Location </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.uploadLocationStyle}"
			name="LocationSetup"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/location_dashboardsetup.htm')">
				Upload Locations </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.manageLocationSubStyle}"
			name="LocationSetup"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/manageLocationDashboard.htm')">
				Manage Locations </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.manageProStyle}"
			name="ManageProd"><a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/batchuploadretprod.htm')"
			title="Manage Products"> Manage Products <img
				src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
				height="16" /> </a>
		</li>
		<li class="${sessionScope.retlrLeftNav.accountStyle}" name="UpdtAccnt">
			<a href="#"
			onclick="callProgressBar('/ScanSeeWeb/retailer/editRetailerProfile.htm')"
			title="Update Account"> Update Account <img
				src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
				height="16" /> </a>
		</li>
		<c:if test="${sessionScope.eventActive eq 'true'}">
			<li class="${sessionScope.retlrLeftNav.eventStyle}" name="Events">
				<a href="#"
				onclick="callProgressBar('/ScanSeeWeb/retailer/manageevents.htm')"
				title="Events"> Events <img
					src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
					height="16" />
			</a>
			</li>
		</c:if>
		<c:if test="${sessionScope.eventActive eq 'true'}">
			<li class="${sessionScope.retlrLeftNav.fundraiserStyle}" name="Fundraiser">
				<a href="#"
				onclick="callProgressBar('/ScanSeeWeb/retailer/managefundraiser.htm')"
				title="Fundraiser"> Fundraisers <img
					src="/ScanSeeWeb/images/vertTabDown.png" alt="down" width="17"
					height="16" />
			</a>
			</li>
		</c:if>
	</ul>
	<div class="vNavbtmBg"></div>
</div>

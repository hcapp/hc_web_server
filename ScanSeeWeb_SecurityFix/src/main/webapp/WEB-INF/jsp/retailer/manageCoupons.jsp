<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.Coupon"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<script type="text/javascript">
function getPerPgaVal(){
	var selValue=$('#selPerPage :selected').val();
	document.managecouponform.recordCount.value=selValue;
      //Call the method which populates grid values
       searchCoupon();
}

</script>

<div id="wrapper">
<form:form commandName="managecouponform" name="managecouponform" acceptCharset="ISO-8859-1">
<input type="hidden" name="couponID" id="couponID"/>
<form:hidden path="recordCount" />
	<div id="content" class="shdwBg">

		<%@ include file="retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR">
		<div class="grpTitles">
        	<h1 class="mainTitle">Coupons</h1>
      </div>
			
	<div class="grdSec brdrTop">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="grdTbl">
				<tr>
					<td colspan="3" class="header">Search Coupon</td>
				</tr>
				<tr>
					<td class="Label" width="18%">Coupon Name</td>
					<td width="58%"><form:input path="couponName" type="text" name="textfield"
						id="textfield" /> <a href="#"><img
							src="/ScanSeeWeb/images/searchIcon.png" alt="Search" onclick="searchCoupon();"
							title="Search" width="25" height="24" /> </a></td>

					<td width="24%"><input type="button" class="btn" value="Add"
						onclick="window.location.href='createcoupon.htm'" title="Add"/></td>
				</tr>
			</table>
			<div class="searchGrd">
				<h1 class="searchHeaderExpand">
					<a href="#" class="floatR">&nbsp;</a>Current Coupon
				</h1>
				<div class="grdCont">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="stripeMe" id="cpnLst">
						<tbody>
								
						<tr class="header">
							<td width="19%">Coupon Name</td>
							<td width="14%">Coupon Amount($)</td>
							<td width="19%">Coupon Start Date</td>
							<td width="20%">Coupon End Date</td>
							<td width="23%">Actions</td>
						</tr>
                         <div align="center" style="font-style: 45">
									<label><form:errors
											cssStyle="${requestScope.manageCpnsFont}" /> </label>
						</div>
						<!--<c:if test="${message ne null }">
							<div id="message">
								<center>
									<h2>
										<c:out value="${message}" />
									</h2>
								</center>
							</div>
							<script>var PAGE_MESSAGE = true;</script>
						</c:if>-->

						<c:if test="${couponList ne null && ! empty couponList}">
							<c:forEach items='${sessionScope.couponList.couponsList}' var='item' varStatus="indexnum">
								<tr>
								<td width="18%"><a href="#"
                                        onclick="editCoupon(<c:out value='${item.couponID}' />);" title="Click Here to Edit"><c:out
                                       value='${item.couponName}' /> </a>
                                   </td>
									<td><c:out value="${item.couponDiscountAmt}" />
									</td>
									<td><c:out value="${item.couponStartDate}" />
									</td>
									<td><c:out value="${item.couponExpireDate}" />
									</td>
									<td><input name="rerun" value="Re-Run" type="button"
										class="btn"
										onclick="rerunCoupon(<c:out value='${item.couponID}'/>);"  title="ReRun"/>
										<input name="Edit" value="Edit" type="button" class="btn"
										onclick="editCoupon(<c:out value='${item.couponID}'/>);" title="Edit"  />
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</table>
					<div class="pagination brdrTop">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="noBrdr" id="perpage">
								<tr>
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" enablePerPage="true" />
								</tr>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	</div>
</form:form>
</div>


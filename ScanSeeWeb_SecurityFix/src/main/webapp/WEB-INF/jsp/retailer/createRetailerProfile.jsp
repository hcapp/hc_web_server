<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link rel="stylesheet" type="text/css" href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/ScanSeeWeb/styles/style.css" media="screen" />
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/autocomplete.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>

<script>
$(document).ready(function() {
	$("#City").live("keydown",function(e)
			{
				var s = String.fromCharCode(e.which);
				if (s.match(/[a-zA-Z0-9\.]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#postalCode').val("");
				   cityAutocomplete('postalCode');
				}else if(s.match(/[\b]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#postalCode').val("");
					cityAutocomplete('postalCode');
				}
							
			});
var stateCode = '';
var state = '';
if($('#postalCode').val().length == 5){
	stateCode = $('#stateCodeHidden').val();
	state = $('#stateHidden').val();
	$( "#Country" ).val(state);				 
	}else{
	$('#stateCodeHidden').val("");
	$('#stateHidden').val("");
	$( "#Country" ).val("");
	}
});
</script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=<spring:message code='googleApiKey'/>&sensor=true">
	<script language="javascript">

	function popUp(URL) {
		day = new Date();
		id = day.getTime();
		eval("page"
				+ id
				+ " = window.open(URL, '"
				+ id
				+ "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=1000,height=600,left = 390,top = 162');");
	}
</script>

<script type="text/javascript">
	window.onload = function() {
		if (document.createretailerprofileform.islocation.checked) {
			document.getElementById("storeNum").style.display = "";
		}
	}

	function changeStoreNum(checked) {
		if (checked == false) {
			if (document.getElementById('storeNum.errors') != null) {
				document.getElementById('storeNum.errors').style.display = 'none';
			}
		}
	}
	/*function loadCity() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		var selCity = document.createretailerprofileform.cityHidden.value;

		$
				.ajax({
					type : "GET",
					url : "retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : selCity
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.createretailerprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						if (cityList.length > 1) {
							document.createretailerprofileform.City.options.length = cityList.length + 1;
							for ( var i = 0; i < cityList.length; i++) {
								cityDropDown.options[optIndex].value = cityList[i];
								cityDropDown.options[optIndex].text = cityList[i];
								optIndex++;
							}
						}

						//$('#myAjax').html(response);
						onCitySelectedLoad();
					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}*/
/*
	function onChangeState() {
		// get the form values
		var stateCode = $('#Country').val();
		var tabIndex = $('#tabindex').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		document.createretailerprofileform.cityHidden.value = "";
		$
				.ajax({
					type : "GET",
					url : "retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : ""
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.createretailerprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						cityDropDown.options[0].selected = true;

						for ( var i = 0; i < cityList.length; i++) {
							cityDropDown.options[optIndex].value = cityList[i];
							cityDropDown.options[optIndex].text = cityList[i];
							optIndex++;

						}

					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}*/

	function verifyEmail() {
		var status = false;
		if (document.createretailerprofileform.contactEmail.value != document.createretailerprofileform.retypeEmail.value) {
			alert("Email addresses do not match.  Please retype them to make sure they are the same.");
		} else {
			status = true;
		}
		return status;
	}

	function validatePassword() {
		var isDigits = /[0-9]/;
		var password = document.createretailerprofileform.password.value;
		if (password != "") {
			if (document.createretailerprofileform.password.value.length < 8) {
				alert("Password must contain at least eight characters!");
				document.createretailerprofileform.password.focus();
				return false;
			} else if (!isDigits
					.test(document.createretailerprofileform.password.value)) {
				alert("Password must contain at least one number (0-9)!");
				document.createretailerprofileform.password.focus();
				return false;
			} else if (document.createretailerprofileform.password.value != document.createretailerprofileform.retypePassword.value) {
				alert("Your password and retype password do not match.");
				document.createretailerprofileform.password.focus();
				return false;
			}
			return true;
		}
	}

	/*
	 * function verifyPassword(){ var status = false; if
	 * (document.createretailerprofileform.password.value !=
	 * document.createretailerprofileform.retypePassword.value) {
	 * alert("Password do not match. Please retype them to make sure they are
	 * the same."); document.createretailerprofileform.password.value ="";
	 * document.createretailerprofileform.retypePassword.value =""; } else {
	 * status = true; } return status; }
	 */

	function clearProfile() {
		var r = confirm("Do you really want to clear the form")
		//var cityHtml = "<select name='city' id='City' tabindex='5'><option value='0'>--Select--</option></select>"
		if (r == true) {
			document.createretailerprofileform.retailerName.value = "";
			document.createretailerprofileform.address1.value = "";
			document.createretailerprofileform.address2.value = "";
			document.createretailerprofileform.postalCode.value = "";
			document.createretailerprofileform.state.value = "";
			document.createretailerprofileform.city.value = "";
			//$('#myAjax').html(cityHtml);
			document.createretailerprofileform.postalCode.value = "";
			document.createretailerprofileform.webUrl.value = "";
			//document.createretailerprofileform.nonProfit.checked = false;
			document.createretailerprofileform.islocation.checked = false;
			document.createretailerprofileform.numOfStores.value = "";
			document.createretailerprofileform.keyword.value = "";
			document.getElementById('bCategory').selectedIndex = -1;
			document.createretailerprofileform.contactFName.value = "";
			document.createretailerprofileform.contactLName.value = "";
			/*document.createretailerprofileform.legalAuthorityFName.value="";
			document.createretailerprofileform.legalAuthorityLName.value="";*/
			document.createretailerprofileform.terms.checked = false;
			document.getElementById('contactPhone').value = "";
			document.getElementById('contactPhoneNo').value = "";
			document.createretailerprofileform.contactEmail.value = "";
			document.createretailerprofileform.userName.value = "";

			document.createretailerprofileform.storeNum.value = "";
			document.getElementById('storeNum').style.display = "none";

			if (document.getElementById('retailerName.errors') != null) {
				document.getElementById('retailerName.errors').style.display = 'none';
			}
			if (document.getElementById('address1.errors') != null) {
				document.getElementById('address1.errors').style.display = 'none';
			}
			if (document.getElementById('state.errors') != null) {
				document.getElementById('state.errors').style.display = 'none';
			}
			if (document.getElementById('city.errors') != null) {
				document.getElementById('city.errors').style.display = 'none';
			}
			if (document.getElementById('postalCode.errors') != null) {
				document.getElementById('postalCode.errors').style.display = 'none';
			}
			if (document.getElementById('contactPhone.errors') != null) {
				document.getElementById('contactPhone.errors').style.display = 'none';
			}
			if (document.getElementById('bCategory.errors') != null) {
				document.getElementById('bCategory.errors').style.display = 'none';
			}
			if (document.getElementById('storeNum.errors') != null) {
				document.getElementById('storeNum.errors').style.display = 'none';
			}
			/*if(document.getElementById('legalAuthorityFName.errors')!= null){
			document.getElementById('legalAuthorityFName.errors').style.display='none';
			}
			if(document.getElementById('legalAuthorityLName.errors')!= null){
			document.getElementById('legalAuthorityLName.errors').style.display='none';
			}*/
			if (document.getElementById('contactFName.errors') != null) {
				document.getElementById('contactFName.errors').style.display = 'none';
			}
			if (document.getElementById('contactLName.errors') != null) {
				document.getElementById('contactLName.errors').style.display = 'none';
			}
			if (document.getElementById('contactPhoneNo.errors') != null) {
				document.getElementById('contactPhoneNo.errors').style.display = 'none';
			}
			if (document.getElementById('contactEmail.errors') != null) {
				document.getElementById('contactEmail.errors').style.display = 'none';
			}
			if (document.getElementById('userName.errors') != null) {
				document.getElementById('userName.errors').style.display = 'none';
			}
			if (document.getElementById('terms.errors') != null) {
				document.getElementById('terms.errors').style.display = 'none';
			}
			if (document.getElementById('webUrl.errors') != null) {
				document.getElementById('webUrl.errors').style.display = 'none';
			}
			if (document.getElementById('numOfStores.errors') != null) {
				document.getElementById('numOfStores.errors').style.display = 'none';
			}
			if (document.getElementById('createretailerprofileform.errors') != null) {
				document.getElementById('createretailerprofileform.errors').style.display = 'none';
			}
		}
	}

	function onLoad() {
		var vCategoryID = document.createretailerprofileform.bCategoryHidden.value;
		var vCategoryVal = document.getElementById("bCategory");
		if (vCategoryID != "null") {
			var vCategoryList = vCategoryID.split(",");
		}
		for ( var i = 0; i < vCategoryVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vCategoryVal.options[i].value == vCategoryList[j]) {
					vCategoryVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	
	function getCityTrigger(val) {	
		if(val == ""){
			$( "#Country" ).html('<option id = "state">----------------------Select---------------------</option>');
			$( "#postalCode" ).val( "" );
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		document.createretailerprofileform.cityHidden.value = val;
		document.createretailerprofileform.city.value = val;
	}
	
	function clearOnCityChange() {	
			//alert($('#citySelectedFlag').val())
			if($('#citySelectedFlag').val() != 'selected'){
			$( "#Country" ).html('<option id = "state">----------------------Select---------------------</option>');
			$( "#postalCode" ).val( "" );
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
			}else{
			$('#citySelectedFlag').val('');
			}
	}

	/*function onCitySelectedLoad() {
		var vCityID = document.createretailerprofileform.cityHidden.value;
		var sel = document.getElementById("City");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCityID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}*/

	function isNumeric(vNum) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;
		var vPostal = document.getElementById("postalCode");
		for (i = 0; i < vNum.length && IsNumber == true; i++) {
			Char = vNum.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				vPostal.value = "";
				vPostal.focus();
				IsNumber = false;
			}
		}
		return IsNumber;
	}
	function isEmpty(vNum) {
		if(vNum.length < 5){
			//$( "#state" ).val("");
			$( "#Country" ).html('<option id = "state">----------------------Select---------------------</option>');
			$( "#City" ).val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		return true;
	}
</script>
<div id="dockPanel">
	<ul id="prgMtr" class="tabs">
		 <!--<li> <a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee" target="_blank" rel="About ScanSee">About ScanSee</a> </li>--> 
		<li><a class="tabActive" title="Create Profile" href="#"
			rel="Create Profile">Create Profile</a></li>
		    <!--<li> <a title="Upload Ads/Logos" href="../uploadLogo.html" rel="Upload Ads/Logos">Upload Logo</a> </li>
      <li> <a title="Location Setup" href="Retailer_LocSetup.html" rel="Location Setup">Location Setup</a> </li>
      <li> <a title="Product Setup" href="../prodMgmt.html" rel="Product Setup">Product Setup</a> </li>
      <li> <a title="Choose Plan" href="Retailer_choosePlan.html" rel="Choose Plan">Choose Plan</a> </li>
      <li> <a title="Dashboard" href="../adminpage_ret.html" rel="Dashboard">Dashboard</a> </li>-->
	</ul>
	<!--
	<div id="tabdPanelDesc" class="floatR tglSec">
		<div id="filledGlass">
			<img src="/ScanSeeWeb/images/Step3.png" />
			<div id="nextNav">
				<a href="#" onclick="createRetailerProfile();"> <img
					class="NextNav_R" alt="Next" src="/ScanSeeWeb/images/nextBtn.png" />
					<span>Set up your profile so that customers can find you!</span> </a>
			</div>
		</div>
	</div>
	<div id="tabdPanel" class="floatL tglSec">
		<img alt="Flow1" src="/ScanSeeWeb/images/Flow3a_ret.png" />
	</div>-->
</div>
<div id="togglePnl"><!-- <a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down" width="9" height="8" /> Show Panel</a>--></div> 
<div class="clear"></div>
<div id="content" class="topMrgn">
	<div class="section shadowImg">
		<div class="infoSecB">HubCiti helps you deliver the right
			message to your customer. To provide you with the best plan options,
			we need to learn little bit more about you.</div>
		<div class="grdSec brdrTop">
			<form:form name="createretailerprofileform"
				id="createretailerprofileform"
				commandName="createretailerprofileform" acceptCharset="ISO-8859-1">
				<form:hidden path="bCategoryHidden" />
				<form:hidden path="cityHidden" />				
				<form:hidden path="stateHidden" id ="stateHidden"/>
				<form:hidden path="stateCodeHidden" id = "stateCodeHidden" />
				<form:hidden path="citySelectedFlag" id = "citySelectedFlag" />
				<div id="bubble_tooltip">
					<div class="bubble_top">
						<span></span>
					</div>
					<div class="bubble_middle">
						<span id="bubble_tooltip_content">Content is comming here
							as you probably can see.Content is comming here as you probably
							can see.</span>
					</div>
					<div class="bubble_bottom"></div>
				</div>
				<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
					width="100%">
					<tbody>
						<tr>
							<td class="header">Retailer Registration Form</td>
							<td colspan="3" style="color: red;"><form:errors
									cssStyle="color:red"></form:errors></td>
						</tr>
						<tr>
							<td class="Label"><label for="rtlrName" class="mand"><a
									href="javascript:void(0)"
									onmousemove="showToolTip(event,'This is your storefront name or corporate name.');return false"
									onmouseout="hideToolTip()">Retailer Name<img alt="helpIcon"
										src="../images/helpIcon.png" />
								</a>
							</label>
							</td>
							<td colspan="3"><form:errors cssStyle="color:red"
									path="retailerName"></form:errors> <form:input
									path="retailerName" id="retailerName" maxlength="100"
									tabindex="1" /></td>
						</tr>
						<tr>
							<td class="Label" width="17%"><label for="addrs1"
								class="mand">Corporate Address</label>
							</td>

							<td width="33%"><form:errors cssStyle="color:red"
									path="address1"></form:errors> <form:textarea path="address1"
									id="address1" class="txtAreaSmall" rows="5" cols="45"
									onkeyup="checkMaxLength(this,'100');" tabindex="2"
									cssStyle="height:60px;" /></td>
							<td class="Label"><label for="addrs3">Address 2</label></td>
							<td><form:textarea path="address2" id="address2"
									class="txtAreaSmall" rows="5" cols="45"
									onkeyup="checkMaxLength(this,'50');" tabindex="3"
									cssStyle="height:60px;"/>
							</td>
						</tr>
						<tr>
							<td class="Label"><label for="pCode" class="mand">Postal
									Code</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="postalCode"></form:errors> <form:input path="postalCode" 
									id="postalCode" maxlength="5" class="loadingInput" style="background-color:none!important"
									onkeypress="zipCodeAutocomplete('postalCode');return isNumberKey(event);"
									onchange="isNumeric(this.value);isEmpty(this.value);"  onkeyup="isEmpty(this.value);" tabindex="4" />
							</td>
							<form:hidden path="tabIndex" value="5" id="tabindex" />
							<td class="Label"><label for="cty" class="mand">City</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="city"></form:errors>
									<form:input path="city" id="City"  tabindex="5" class="loadingInput"/>
								</td>
						</tr>
						
						<tr>
							<td class="Label" align="left"><label for="sts" class="mand">State</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="state"></form:errors>
									<form:input path="state" id="Country" tabindex="6" disabled="true"/> 
									<!--<form:select id="Country"
									path="state" tabindex="6" disabled="true">
									<form:option id="state" value="state"></form:option>
									</form:select>-->
							</td>
							<td class="Label" align="left"><label for="phnNum"
								class="mand">Corporate Phone #</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactPhone" onkeypress="return isNumberKeyPhone(evt)"></form:errors>
								<form:input path="contactPhone" id="contactPhone" value=""
									tabindex="7" onkeyup="javascript:backspacerUP(this,event);"
									onkeydown="javascript:backspacerDOWN(this,event);" />(xxx)xxx-xxxx</td>
						</tr>
						<tr>
							<td class="Label">
								<!-- <label for="url" class="mand"> -->
								<label for="url">Website URL</label>
							</td>
							<td colspan="3" align="left"><form:errors
									cssStyle="color:red" path="webUrl" /> <form:input id="webUrl"
									tabindex="8" path="webUrl" name="webUrl" /></td>
						</tr>

						<tr>
							<td class="Label" align="left"><label for="bcategory"
								class="mand">Business Category</label><br>(Hold CTRL to
								select multiple categories)</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="bCategory"></form:errors> <form:select path="bCategory"
									id="bCategory" class="txtAreaBox" size="1" multiple="true"
									tabindex="9">
									<c:forEach items="${sessionScope.categoryList}" var="c">
										<form:option value="${c.businessCategoryID}"
											label="${c.businessCategoryName}" />
									</c:forEach>
								</form:select></td>
							<td class="Label" align="left"><label for="keywords"></label>Keywords</td>
							<td align="left"><form:textarea id="keywords"
									class="txtAreaSmall" rows="5" cols="45" path="keyword"
									tabindex="10" cssStyle="height:60px;" /> <!--<td class="Label" align="left"> <label for="nonprofit">Non-profit Status</label> -->
							</td>
							<%--<td align="left"> <form:errors cssStyle="color:red" path="nonProfit"></form:errors>
					        <form:checkbox  path="nonProfit" id="nonProfit" tabindex="9"/>
					          My organization is a registered and recognized<br />
					          501(c)(3) non-profit.  --%>
							</td>
						</tr>
						<tr>
							<td class="Label">Is corporate address also a store
								location?</td>
							<td colspan="3"><form:checkbox path="islocation" id="isLoc"
									name="islocation" tabindex="11"
									onclick="changeStoreNum(this.checked);" />
								<form:errors cssStyle="color:red" path="storeNum"></form:errors>
								<div id="storeNum" style="display: none">
									<i id="storeNum"> <label for="storeNum" class="mand">Enter
											Store Number</label> <form:input type="text" class="textboxBig"
											id="storeNum" path="storeNum" maxlength="20" tabindex="12" />
									</i>
								</div></td>
						</tr>

						<tr>
							<td class="Label"><label for="numOfStores" class="mand">Number
									of stores</label>
							</td>
							<td colspan="3" align="left"><form:errors
									cssStyle="color:red" path="numOfStores" /> <form:input
									id="numOfStores" path="numOfStores" name="numOfStores"
									onkeypress="return isNumberKey(event)" maxlength="3"
									tabindex="13" /></td>
						</tr>

						<tr>
							<td class="Label" colspan="4">&nbsp;</td>
						</tr>
						<!--<tr>
              <td class="Label" colspan="4"><a href="javascript:void(0)" onmousemove="showToolTip(event,'The name of the person that has given you authority to act.');return false" onmouseout="hideToolTip()"> Legal Authority Name <img alt="helpIcon" src="../images/helpIcon.png"></a></td>
            </tr>
            <tr>
              <td class="Label"><label for="legalAuthorityFName" class="mand">First Name</label></td>
              <td align="left"><form:errors cssStyle="color:red" path="legalAuthorityFName"></form:errors>
              <form:input path="legalAuthorityFName" id="legalAuthorityFName" maxlength="20" tabindex="13" /></td>
              <td class="Label" align="left"><label for="legalAuthorityLName" class="mand">Last Name</label></td>
              <td align="left"><form:errors cssStyle="color:red" path="legalAuthorityLName"></form:errors>
              <form:input path="legalAuthorityLName" id="legalAuthorityLName" maxlength="30" tabindex="14"/></td>
            </tr>-->
						<tr>
							<td class="Label hdrTxt" colspan="4">Contact Details</td>
						</tr>
						<tr>
							<td class="Label"><label for="contactFName" class="mand">First
									Name</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactFName"></form:errors> <form:input
									path="contactFName" id="contactFName" maxlength="20"
									tabindex="14" />
							</td>
							<td class="Label"><label for="contactLName" class="mand">Last
									Name</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactLName"></form:errors> <form:input
									path="contactLName" id="contactLName" maxlength="30"
									tabindex="15" /></td>
						</tr>
						<tr>
							<td class="Label" align="left"><label for="contactPhoneNo"
								class="mand">Contact Phone #</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactPhoneNo"></form:errors> <form:input
									path="contactPhoneNo" id="contactPhoneNo"
									onkeyup="javascript:backspacerUP(this,event);"
									onkeydown="javascript:backspacerDOWN(this,event);"
									tabindex="16" />(xxx)xxx-xxxx</td>
							<td class="Label" align="left">&nbsp;</td>
							<td align="left">&nbsp;</td>
						</tr>

						<tr>
							<td class="Label"><label for="contactEmail" class="mand">Contact
									Email</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactEmail"></form:errors> <form:input
									path="contactEmail" id="contactEmail" name="contactEmail"
									maxlength="100" onpaste="return false" ondrop="return false"
									ondrag="return false" oncopy="return false" autocomplete="off"
									tabindex="17" /></td>
							<td class="Label" colspan="2"><label for="">This
									email address will be used to receive your temporary password
									and any future password requests.</label></td>
						</tr>
						<tr>
							<!--  <td class="Label"><label for="contREml" class="mand">Retype Email</label>              </td>
            		<td align="left" colspan="3"><form:errors cssStyle="color:red" path="retypeEmail"></form:errors>
            		<form:input path="retypeEmail" id="retypeEmail"  name="retypeEmail" maxlength="100" onpaste="return false" ondrop="return false" ondrag="return false" oncopy="return false" autocomplete="off" tabindex="18"/> 
            	-->
							<td class="Label"><label class="mand" for="rtlrName">UserName</label>
							</td>
							<td colspan="3" align="left"><form:errors
									cssStyle="color:red" path="userName"></form:errors> <form:input
									path="userName" type="text" maxlength="100" name="userName"
									id="userName" tabindex="18" /></td>
						</tr>
						<tr>
							<td class="Label">&nbsp;</td>
							<td align="left">
								<!-- <font color="#50940d">For company authorization, ScanSee verifies companies and non-profits through &nbsp;Lexis Nexis. You must have legal authority with your organization to initiate the &nbsp;ScanSee program.</font> -->
								<em> <form:errors cssStyle="color:red" path="terms"></form:errors>


									<input type="hidden" id="locCoordinates" value="" /> <form:checkbox
										path="terms" name="terms" tabindex="19" /> <font
									color="#465259"> <strong>Accept Terms of
											Service and Use </strong> </font>
									<div id="acptTrms">
										<A HREF="http://www.scansee.com/links/termsconditions.html"
											target="_blank" tabindex="20"> <u>Terms &amp;
												conditions of ScanSee</u>
										</a>
									</div> <input class="btn" value="Clear" type="button"
									onclick="clearProfile();" tabindex="21" title="Clear the form" />
									<input class="btn" value="Submit" type="button"
									onclick="createRetailerProfile();" tabindex="22" title="Submit" />
							</em></td>
							<td colspan="2" align="left">
								<%-- <em>
              
              <form:errors cssStyle="color:red" path="terms"></form:errors>
                            
            
                
                <form:checkbox path="terms" name="terms" tabindex="19" /> <font color="#465259">
											<strong>Accept Terms of Service and Use </strong> </font>
                <div id="acptTrms"><A HREF="http://www.scansee.com/links/termsconditions.html" target="_blank" tabindex="20">
	                                <u>Terms &amp; conditions of ScanSee</u></a></div>
                <input class="btn" value="Clear" type="button" onclick="clearProfile();" tabindex="21" title="Clear the form"/>
                <input class="btn" value="Submit" type="button" onclick="createRetailerProfile();" tabindex="22" title="Submit"/>
                </em> --%></td>
						</tr>
					</tbody>
				</table>
			</form:form>
			<div class="navTabSec RtMrgn">
				<div align="right"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="section topMrgn">
		<div class="clear"></div>
	</div>
</div>
<script>
	onLoad();
</script>
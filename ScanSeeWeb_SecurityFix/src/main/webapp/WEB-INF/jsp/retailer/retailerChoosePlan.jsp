 <meta http-equiv="Expires" CONTENT="0">
  <meta http-equiv="Cache-Control" CONTENT="no-cache">
  <meta http-equiv="Pragma" CONTENT="no-cache">
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>  
<%@page session="true"%>
<%@ page import="common.pojo.PlanInfo"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script src="scripts/web.js" type="text/javascript"></script>

<script type="text/javascript">


<% ArrayList<PlanInfo> planInfoarr = (ArrayList<PlanInfo>)session.getAttribute("planinfolist"); %>
var planInfoSize = <%= planInfoarr.size()%>;

$(document).ready(function() {
		
	$('#checkBoxError').hide();
	  if(planInfoSize)
	  	totalRetailerPrice(planInfoSize);
	  
	  
	});

</script>

<form:form name="myForm" commandName="myForm">
<form:form name="plandetailform" commandName="chooseplanform">
  <input type="hidden"  name="planJson"/>
  <input type="hidden"  name="grandTotal"/>
 <div id="dockPanel">
    <ul id="prgMtr" class="tabs">
    <!--   <li> <a title="Home" href="#" rel="home">Home</a> </li> -->
     <!-- <li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
				 target="_blank" rel="About ScanSee">About ScanSee</a> 
			</li>-->
      <li> <a title="Upload Ads/Logos" href="uploadRetailerLogo.htm" rel="Upload Ads/Logos">Upload Logo</a> </li>
      <li> <a title="Location Setup" href="addlocation.htm" rel="Location Setup">Location Setup</a> </li>
    <!--   <li> <a title="Product Setup" href="regbatchuploadretprod.htm" rel="Product Setup">Product Setup</a> </li> -->
      <li> <a class="tabActive" title="Choose Plan" href="retailerchoosePlan.htm" rel="Choose Plan">Choose Plan</a> </li>
      <li>
      	 <c:choose>
			<c:when test="${isPaymentDone == null}">
				<a title="Dashboard" href="/ScanSeeWeb/retailer/retailerhome.htm" rel="Dashboard">Dashboard</a>
			</c:when>
			<c:otherwise>
				<a title="Dashboard" href="#" rel="Dashboard">Dashboard</a>
			</c:otherwise>
		 </c:choose>	 
      </li>
    </ul>
 
<!-- <a id="Mid" name="Mid"></a>
<div id="tabdPanelDesc" class="floatR tglSec">
        <div id="filledGlass"> <img src="../images/Step4.png"/>
          <div id="nextNav"> 
          	<a href="#" onclick="location.href='/ScanSeeWeb/retailer/dashboard.htm'"> <img class="NextNav_R" alt="Next" src="../images/nextBtn.png"/> 
          		<span>Check-out and Get started!</span>
          	</a> 
          </div>
        </div>
    </div>
      <div id="tabdPanel" class="floatL tglSec"> <img alt="Flow5" src="../images/Flow5_mfg.png"/> </div>
 -->
  </div> 
  <div class="clear"></div>
  <!-- <div id="togglePnl"><a href="#">  <img src="../images/downBtn.png" alt="down" width="9" height="8" /> Show Panel</a></div> -->
  <div id="content" class="">
    <div class="section shadowImg ">
      <table class="brdrLsTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
          <tr>
            <td width="85%"><h5>Thank you! Modular Advertising opportunities to fit your budget, season or individual locations. 
                Learn about upgrades on your Dashboard:
              </h5></td>
            <td valign="top" width="15%" align="right"><img alt="shopping cart" src="../images/shopingCart-img.png" width="71" height="46"/> </td>
          </tr>
        </tbody>
      </table>
      <div class="grdSec brdrTop">
        <table class="cstmTbl"  border="0" cellspacing="0" cellpadding="0" width="100%" id="choosePln">
          <tbody>
            <tr class="header">
              <td style="width: 90px;">PRODUCT</td>
              <td style="width: 260px;">DESCRIPTION</td>
              <td style="width: 175px;">PAYMENT PLAN</td>
              <td style="width: 113px;">NO. OF <br>LOCATIONS</td>
              <td style="width: 82px;">PRICE PER LOCATION($)</td>
              <td style="width: 84px;">SUBTOTAL($)</td>
            </tr>
          <% ArrayList<PlanInfo> planinfolist = (ArrayList<PlanInfo>)session.getAttribute("planinfolist"); %>
            <% int count = 0; %>
		   <c:forEach items="${sessionScope.planinfolist}" var="item" >
            <% count++;%>
            <input type="hidden"  id="defaultMonthlyPrice<%=count%>"  value="${item.retailerBillingMonthly}"/>
            <input type="hidden"  id="defaultYearlyPrice<%=count%>"  value="${item.retailerBillingYearly}"/>
            <tr id="<%=count%>" >
			  
              <td class="editLink"><c:out value="${item.productName}"/></td>
               <td  class="editLink"><p style="font-size: 11px; " align="justify">
                    <c:out value="${item.description}"/></p> 
              </td>
              <td  class="editLink">
              
              <%-- <c:out value="${item.description}"/> --%>
              
                  <p>
                     <input type="hidden"  id="selectedMonthlyPrice<%=count%>"  value="${item.retailerBillingMonthly}"/>
                     <input type="hidden"  id="selectedYearlyPrice<%=count%>"  value="${item.retailerBillingYearly}"/>
                  	<label><input name="radio<%=count%>" type="radio" id="monthly" value="monthly" checked="checked" onclick="loadPricePerLocation('radio<%=count%>','selectedMonthlyPrice<%=count%>',<%= planinfolist.size()%>,'price<%=count%>',true)"/> Monthly</label>
                  	&nbsp; 
                  	<span id="defaultMonthlyPriceSpanId<%=count%>">
                  		($${item.retailerBillingMonthly})
                  	</span>                
                  	              
				  </p>
                  <p>&nbsp;</p>
                  <p>
                  	<input type="radio" name="radio<%=count%>" id="Yearly" value="Yearly" onclick="loadPricePerLocation('radio<%=count%>','selectedYearlyPrice<%=count%>',<%= planinfolist.size()%>,'price<%=count%>',true)" />
                  	<label for="Yearly"> Yearly</label> &nbsp; 
                  	<span id="defaultYearlyPriceSpanId<%=count%>">
                  		($${item.retailerBillingYearly})
                  	</span>                
                  	         
                  	
                  </p>
              
              
              </td>
              
              <td class="editLink">
			  <input type="hidden"  name="productId" value="<c:out value='${item.productId}'/>"/>
              <input type="text"   name="qty"  onkeypress="return isNumberKey(event)" onchange ="totalMonthlyYearlyRetailerPrice(<%= planinfolist.size()%>,false);"  value="${sessionScope.retailerProfile.numOfStores}" style="color: #465259;	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;  width: 50px;"/> 
              </td>
              
              <td class="editLink">
              <label class="textbox"></label>
              	<%-- <input  readonly="readonly" class="textboxSmall"  name="price" value="${item.price}"/> --%>
              	<input  type="text" readonly="readonly" style="color: #465259;	font-family: Arial, Helvetica, sans-serif; height: 22px; line-height: 20px;	font-size: 12px;  width: 50px;"  name="price" id="price<%=count%>" value="${item.retailerBillingMonthly}"/>
              </td>
              <td class="editLink">
            
               <input type="text" readonly="readonly"   name="total" value="${item.total}" style="color: #465259; font-family: Arial, Helvetica, sans-serif; height: 22px; line-height: 20px; font-size: 12px;  width: 50px;"/>
               </td>
               </tr>
              </c:forEach>
           
           
            <tr class="dottedLine">
              <td class="dottedLine"></td>
              <td></td>
              <td></td>
              <td class="highlightCol"></td>
              <td align="center"></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
               <td><!-- <strong>Setup Fee:</strong>&nbsp;&nbsp;&nbsp;<br>
               <input type="text"  id= "discountCode" class="textboxSmall" size="3" onkeypress="return isNumberKey(event)" onchange="getRetailerDiscount()"/>
               <input readonly="readonly" id="setupFee" class="textboxSmall" value="25" />
                    -->            
			   </td>
              <td><p align="left">&nbsp;</p>Subtotal($)</td>
              <td><p align="left">&nbsp;</p> <input readonly="readonly" id="sub" class="textboxSmall" value="${item.subTotal}" />
			  </td>
            </tr>
            <!-- <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><p align="left">&nbsp;</p>Setup Fee($)</td>
              <td><p align="left">&nbsp;</p><input readonly="readonly" id="setupFee" class="textboxSmall" value="25" />
			  </td>
            </tr>
             -->
			<!-- Starts : Meyy related code -->
			<tr>
              <td></td>
              <td></td>              
              <td></td>
              <td><strong>Discount Code:</strong>&nbsp;&nbsp;&nbsp;<br>
               <!-- <input type="text"  id= "discountCode" class="textboxSmall" size="3" onkeypress="return isNumberKey(event)" onchange="getRetailerDiscount()"/> -->
               															<!-- onkeypress="return isNumberKey(event)" -->
              <input type="text"  id= "discountCode" style="width: 106px;" size="3"  onchange="getRetailerDiscountMonthlyAndYearly(<%=count%>,<%= planinfolist.size()%>)"/>
                               
			   </td>
			   <td><p align="left">&nbsp;</p><strong>Total before Taxes($)</strong></td>
              <td><p align="left">&nbsp;</p><strong><input readonly="readonly" id="discountsubtotal" class="textboxSmall" value="${item.discountSubTotal}"/></strong></td>
            <!--  <td>Discount($)</td>
              <td>
             <div id="myAjax">
          
             <input readonly="readonly" name="discount"  class="textboxSmall" value=""/>
             
              </div>
            </td>
           -->  
           </tr>
			<!-- Ends : Meyy related code -->
            <tr class="dottedLine">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <%-- <td><strong>Total before Taxes($)</strong></td>
              <td><strong><input readonly="readonly" id="discountsubtotal" class="textboxSmall" value="${item.discountSubTotal}"/></strong></td> --%>
            </tr>
            <!-- <tr height="10px"/>  -->
            
            
           <%-- <tr class="header">
				<td colspan="6" class="editLink">PRODUCT DESCRIPTION</td>
		   </tr>
		   <c:forEach items="${sessionScope.planinfolist}" var="item" >
	            <% count++;%>
	            <tr id="<%=count%>" >
				  <td class="editLink" style="width: 180px;"><c:out value="${item.productName}"/></td>
	              <td style="width: 500px;" class="editLink" colspan="4"><c:out value="${item.description}"/></td>
	            </tr>
           </c:forEach> --%>
            
            
            
          
            
             <!-- <tr class="header">
				<td colspan="5" class="editLink">MONTHLY INVOICE ITEMS</td>
			</tr>
           <tr>

              <td>Per Store Advertising </td>
              <td><span class="editLink" style="width: 500px;">This will allow a user to click &quot;This Location&quot; and find your store. </span></td>
              <td></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>

                <tr>  
                  <td></td>
                  <td><a href="#" onclick="openIframePopup('ifrmPopup','ifrm','../perstoreadvertising.html',420,860,'Monthly Invoice Items')" 
                                  title="View details">View details</a></td>
                  <td></td>
                 </tr> 
                <tr>


                <tr>

                  <td></td>
                  <td><span class="editLink" style="width: 500px;">
                    <div id="checkBoxError" style="color:red"  >Please check the checkbox</div>
                    <input id="checkbox" value="on" type="checkbox" name="checkbox" onclick="validateCheckbox()"/>
                    <font color="#465259"> <strong>I Agree to Pay for Per Store Advertising</strong></font></span></td>
                  <td></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr> -->
          
            
            
          </tbody>
        </table>
        <div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;background-color: White">
		<div class="headerIframe">
			<img src="images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
			title="Click here to Close" align="middle" /> 
			<span id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color: White"> </iframe>
		</div>
        <div class="navTabSec mrgnRt" align="right">
          <input type="hidden"  id="skipPayment"/>
          <input class="btn"  onclick="retailerPlanInfo()" value="Next" type="button" name="Cancel3" title="Next"/>

        </div>
      </div>

    </div>
    <div class="clear"></div>
    <div class="section topMrgn">
      <div class="clear"></div>
    </div>
  </div>
 
</form:form>
</form:form>



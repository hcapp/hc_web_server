<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ScanSee</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/global.js" type="text/javascript"></script>
</head>

<body style="background:#FFFFFF">
<script type="text/javascript">
$(function() {
    //Get actual content and display as title on mouse hover on it.
    var actText = $("td.genTitle").text();
    $("td.genTitle").attr('title',actText);
    var limit = 20;// character limit restricted to 20
    var chars = $("td.genTitle").text(); 
    if (chars.length > limit) {
        var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
        var dots = $("<span class='dots'>...</span>");
        $("td.genTitle").empty()
            .append(visibleArea)
            .append(dots);//append trailing dots to the visibile part 
    }                            
});
</script>
<div id="iphonePanel">
<form:form name="previeweditcouponform" commandname="previeweditcouponform">
 
   <div class="navBar iphone">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
            <tr>
              <td width="19%"><img src="../images/backBtn.png" alt="back" width="50" height="30" /></td>
              <td width="54%" class="genTitle">Claim</td>
              <td width="27%"><img src="../images/mainMenuBtn.png" alt="mainmenu" width="78" height="30" /></td>
            </tr>
          </table>
    </div>

  <div class="previewAreaScroll newScrollArea">
   	<div class="stretch cmnPnl relatvDiv">   	
    <h3 class="overallPadding">${sessionScope.couponDetails.couponName}</h3>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="zeroBrdrTbl dashedBrdr">
	 <tr class="noBg">
		<c:choose>
        	<c:when test="${(sessionScope.editCouponImage ne null && sessionScope.editCouponImage ne '') || (sessionScope.editProductName ne null && sessionScope.editProductName ne '')}">
        		<c:choose>
        			<c:when test="${sessionScope.editCouponImage ne null && sessionScope.editCouponImage ne ''}">
		              <td width="13%" align="center" valign="top"><img src="${sessionScope.editCouponImage}" alt="Image" width="130" height="130" /></td>
		           </c:when>
		           <c:otherwise>
		             <td width="13%" align="center" valign="top"><img src="${sessionScope.editProductName}" alt="Image" width="130" height="130" /></td>
		           </c:otherwise>
        		</c:choose>        	
        	</c:when>
        	<c:otherwise>
        		<td width="13%" align="center" valign="top"><img src="../images/imagePreview.png" alt="Image" width="130" height="130" /></td>
        	</c:otherwise>
        </c:choose>
		<td width="87%" colspan="2" align="left" valign="top" class="wrpWord">
          	${sessionScope.couponDetails.couponLongDesc} 
          </td>  
     </tr>
	  <tr>
          <td colspan="3" align="left">Valid From ${sessionScope.couponDetails.couponStartDate} to ${sessionScope.couponDetails.couponExpireDate}</td>
      </tr>
      <tr>
	     <td colspan="3" align="left">
	     	<ul class="sublinkList">
	       		<li class="linkBoldHdr">
	       			<a href="javascript:void(0);">Qualifying Products<span>&nbsp;</span></a>
	       		</li>
	       		<li>
	       			<a href="javascript:void(0);">This is a web coupon, Click here to view<span>&nbsp;</span></a>
	       		</li>
	     	</ul>
	     </td>
      </tr>
      <tr>
      	<td colspan="3" align="left">
	      	<div class="btmPadding imgtabFix">
	      		<a href="javascript:void(0);" class="dsbl">
	      			<img src="../images/redeemCoupon@2x.png" alt="mark as used" width="140" height="30"/>
	      		</a><a href="javascript:void(0);">
	      				<img src="../images/claimCoupons@2x.png" alt="clip coupon" width="140" height="30" />
	      			</a>
	      	</div>
      	</td>
      </tr>
      <tr>
      	<td colspan="3" align="left" class="redeemRow"><div class="redeemIcon">&nbsp;</div></td>
      </tr>
     </table>
	</div>  
    </table>
  </div>
  </form:form>
  
  <div id="TabBarIphone">
    <ul id="iphoneTab">
      <li><img src="../images/tab_btn_up_goTowishlist_prv.png" alt="wish list" width="80" height="50" /></li>
	  <li><img src="../images/tab_btn_up_shpngLst_prv.png" alt="twitter" width="80" height="50" /></li>
      <li><img src="../images/tab_btn_up_cpns.png" alt="sendtext" width="80" height="50" /></li>
      <li><img src="../images/tab_btn_up_rateshare.png" alt="twitter" width="80" height="50" /></li>
    </ul>
  </div> 
  
</div>
         <div class="clear"></div>

			<div align="center">
			     <input class="btn" onclick="javascript:history.back()" type="button" value="Back" name="Cancel2" title="Back"/>
			</div>

</body>
</html>
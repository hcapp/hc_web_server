<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="common.pojo.Rebates"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script src="scripts/web.js" type="text/javascript"></script>

<div id="wrapper">
<div id="content" class="shdwBg">
<%@include file="retailerLeftNavigation.jsp" %>
<div class="rtContPnl floatR">
				<div class="grpTitles">
        			<h1 class="mainTitle">Rebates</h1>
     			 </div>
				
				<form:form name="displayretailerrebatesform" commandName="RebatesearchForm" acceptCharset="ISO-8859-1">
				<form:hidden path="rebateID"/>
				<form:hidden path="rebproductId" name="rebproductId"/>
				<div class="grdSec brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<tr>
							<td colspan="3" class="header">Search Rebates</td>
						</tr>
						<tr>
							<td class="Label" width="18%">Rebate Name</td>
							<td width="60%">
							<form:input path="rebName"  name="searchKey" id="searchKey" />
							<a href="#"><img src="/ScanSeeWeb/images/searchIcon.png"
								alt="Search" width="25" height="24" onclick="searchRebate();"
								title="Search" /></a></td>
							<td width="22%"><form:input path="" type="button" class="btn" value="Add" title="Add"
								onclick="window.location.href='addretrebates.htm'" /></td>
						</tr>
					</table>
					<div class="searchGrd">
						<h1 class="searchHeaderExpand">
							<a href="#" class="floatR">&nbsp;</a>Current Rebates
						</h1>
						<div class="grdCont">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="stripeMe" id="rebateLst">
								<tbody>
								
								<tr class="header">
									<td width="18%">Rebates Name</td>
									<td width="15%">Rebate Amount($)</td>

									<td width="16%">Rebate Start Date</td>
									<td width="16%">Rebate End Date</td>
									<td width="22%">Actions</td>
								</tr>
								<div align="center" >
									<label><form:errors
											cssStyle="${requestScope.manageRbtssFont}" /> 
									</label>
								</div>
								<!--<c:if test="${message ne null }">
									<div id="message"><center><h2><c:out value="${message}" /></h2></center></div>
									<script>var PAGE_MESSAGE = true;</script>
								</c:if>
								
								--><c:if test="${listOfRebates ne null && ! empty listOfRebates}">
									<c:forEach items="${sessionScope.listOfRebates.rebatesList}" var='rebates'>
										<tr>
											<!--<c:set var="style" value="rowodd" />
											<c:set var="rowColor" value="" />
											<c:if test="${indexnum.index%2 eq 1}">
												<c:set var="style" value="roweven" />
											</c:if>
											<td width="3%"><c:out value='${indexnum.index+1}' /></td>
											--><td width="18%"><a href="#"
                                        onclick="editretRebates(<c:out value='${rebates.rebateID}' />);"title="Click Here to Edit"><c:out
                                       value='${rebates.rebName}' /> </a>
                                            </td>
											<td width="15%"><c:out value='${rebates.rebAmount}' />
											</td>
											<td width="16%"><c:out value='${rebates.rebStartDate}' />
											</td>
											<td width="16%"><c:out value='${rebates.rebEndDate}' />
											</td>
											<td width="22%">
 										   <input name="Re-Run4" value="Re-Run" type="button" class="btn" title="Re-Run"
										onclick="retailerRerunRebates(<c:out value='${rebates.rebateID}'/>);" />
										<input name="edit" value="Edit" type="button" class="btn" title="Edit"
										onclick="editretRebates(<c:out value='${rebates.rebateID}' />);" />
											</td>
										</tr>
									</c:forEach>
								</c:if>
								
								</tbody>
							</table>
							<div class="pagination">
							<p>
								<page:pageTag
									currentPage="${sessionScope.pagination.currentPage}"
									nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
									pageRange="${sessionScope.pagination.pageRange}"
									url="${sessionScope.pagination.url}" enablePerPage="false"/>
							</p>
						</div>
						</div>
						</div>
				</div>
				</form:form>
			</div>
		</div>
		</div>
		<div class="clear"></div>

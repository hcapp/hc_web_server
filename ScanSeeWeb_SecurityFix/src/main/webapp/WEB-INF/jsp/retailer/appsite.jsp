<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="content">
    <%@include file="retailerLeftNavigation.jsp"%>
    <div class="rtContPnl floatR">
      <div class="grpTitles">
        <h1 class="mainTitle">Tell Your Story</h1>
      </div>
      <div class="section">
        <div class="textSec brdrTop">
          <p>Your AppSite<sup class="smallsup">TM&nbsp;</sup> is a custom app and website rolled into one!  Just like any app and website there is a "menu" with buttons that connect to "Pages" in your AppSite <sup class="smallsup">TM</sup>.  
          In this section, you will create (or change) each of your "Pages" and "Menu".  It is easy and fun to watch your creation come to life as you build it!. </p>
          <h4 class="MrgnTop">Step by Step</h4>
          <ul class="MrgnTop">
          	<li>A. Upload your Logo</li>
            <li>B. Build your Welcome Page </li>
            <li>C. Build Your Banner</li>
            <li>D. Build your AnythingPages <sup class="smallsup">TM</sup></li>
          </ul>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
  <tr>
    <td width="25%" >
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" style="height:128px; width:126px;">
    		<tr>
    			<td><b>A.</b></td>
    			<td align="center"><a href="/ScanSeeWeb/retailer/uploadRetailerLogo/uploadRetailerLogoDashboard.htm">
    			<img src="../images/yourLogo.png" alt="YourLogo" width="110" height="110" /></a></td>
    		</tr>    		
    	</table>
    </td>
     <td width="25%">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" style="height:128px; width:126px;">
    		<tr>
    			<td><b>B.</b></td>
    			<td><a href="/ScanSeeWeb/retailer/manageads.htm"><img src="../images/buildWelcomePage.png" alt="BuildWelcomePage" width="110" height="110" /></a></td>
    		</tr>    		
    	</table>
    </td>
     <td width="25%">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" style="height:128px; width:126px;">
    		<tr>
    			<td><b>C.</b></td>
    			<td><a href="/ScanSeeWeb/retailer/managebannerads.htm"><img src="../images/buildBannerAd.png" alt="BuildBannerAd" width="110" height="110" /></a></td>
    		</tr>    		
    	</table>
    </td>
     <td width="25%">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" style="height:128px; width:126px;">
    		<tr>
    			<td><b>D.</b></td>
    			<td><a href="/ScanSeeWeb/retailer/buildAnythingPage.htm"><img src="../images/buildAnythingPgs.png" alt="BuildYourAnythingPage" width="110" height="110" /></a></td>
    		</tr>    		
    	</table>
    </td>    
  </tr>
  <tr>
    <td align="center"><a href="/ScanSeeWeb/retailer/uploadRetailerLogo/uploadRetailerLogoDashboard.htm"><img src="../images/iphone_uploadlogo.png" alt="uploadLogo_iPhone" width="124" height="242" /></a></td>
    <td align="center"><a href="/ScanSeeWeb/retailer/manageads.htm"><img src="../images/iphone_buildWelcomePg.png" alt="buildWelcomePage" width="124" height="242" /></a></td>
    <td align="center"><a href="/ScanSeeWeb/retailer/managebannerads.htm"><img src="../images/iphone_buildBannerAd.png" alt="BuildBannerAd" width="124" height="242" /></a></td>
    <td align="center"><a href="/ScanSeeWeb/retailer/buildAnythingPage.htm"><img src="../images/iphone_buildAnythingPg.png" alt="BuilAnythingPage" width="124" height="242" /></a></td>
  </tr>
  <tr>
    <td align="left" valign="top">Your Logo appears many places in your AppSite<sup class="smallsup">TM</sup></td>
    <td align="left" valign="top">Welcome Page reinforces your branding and can present your value proposition</td>
    <td align="left" valign="top">Banner displays your Company's Name &amp; Brand</td>
    <td><p>Anything Pages<sup class="smallsup">TM&nbsp;</sup> are pages you create that link to an existing website or any webpage.You can create several AnythingPages<sup class="smallsup">TM&nbsp;</sup></p>
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

        </div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
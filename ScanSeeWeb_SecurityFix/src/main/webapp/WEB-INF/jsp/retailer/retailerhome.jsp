<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="content">
<%@include file="retailerLeftNavigation.jsp"%>
 <div class="rtContPnl floatR">
      <div class="mainPgTitle">
        <h1 class="mainTitle">Welcome Retailers!</h1>
        <h4 class="subTitle">BE SEEN, BE FOUND, BE HEARD! Tell your Story!</h4>
        <h5 class="caption MrgnBtm">Please choose from the menu on the left, or select a topic below:</h5>
      </div>
      <div class="triContpnl MrgnTop">
        <div class="contBlkDsply">
          <h3>1. Learn Now</h3>
          <span><a href="#"><img src="../images/RtlrwatchourTutorial.png" alt="watc video" width="146" height="146" onclick="openIframePopup('ifrmPopup','ifrm','https://player.vimeo.com/video/48036530?autoplay=1',750, 950,'Tutorial Video'),setiframe('ifrmPopup')" /></a></span>
          <div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
          
          </div>
        <div class="contBlkDsply">
          <h3>2. Get Started</h3>
          <span><a href="/ScanSeeWeb/retailer/retailerhome.htm?appsite=Y"><img src="../images/RtlrBuildYourAppsite.png" alt="buil your appsite" width="146" height="146" /></a></span></div>
        <div class="contBlkDsply">
          <h3>3. Drive Traffic</h3>
          <span><a href="/ScanSeeWeb/retailer/promotionsdashboard.htm"><img src="../images/RtlrbuildPromotions.png" alt="builPromotions" width="146" height="146" /></a></span></div>
      </div>
      <div class="clear"></div>
      <div class="splitPnl MrgnBtm zeroBrdr">
        <div class="split1 RtMrgn">
          <p>Fear not! If you can find files on your            </p>
          <p>computer you can do this</p>
        </div>
        <div class="split2">
         <!--  <p>Click here to see sample AppSites and</p>
          <p> Appsite
            of the Month!</p> -->
        </div>
          <div class="clear"></div>
      </div>
    
      <div class="section MrgnTop">
      <!--<div class="block floatL">
           <ul>
            <li class="splt1">
              <h3>Funding Higher Education<span>Through Commerce Networking</span></h3>
            </li>
            <li class="splt2"><img src="../images/scanseebadge.png" width="43" height="42" /></li>
          </ul>
          <div class="clear"></div>
          <div class="section_info">
            At ScanSee, we give back. 50% Back.
            That's right; we donate
           50% of our profits to qualifying colleges and universities in the U.S. Also, if you wish, you can direct up to 100% of your earnings through ScanSee to any educational institution of your choice, or use them as you wish. 
            <p class="topPadding linkMore"> <img height="6" alt="LinkBullet" src="../images/linkArrow.png" width="4"/> <a href="http://www.scansee.com/scansee_our_philosophy_Give_Back_Donate.html">Click here for details</a></p>
          </div>
        </div>-->
        <div class="block floatL">
          <h3>Privacy<span>Respected</span></h3>
          <div class="section_info">All of your shopping behavior is private at ScanSee. We promise never to sell or share your information.
We understand your concerns for privacy and we feel the same way<p class="topPadding linkMore"> <img height="6" alt="LinkBullet" src="../images/linkArrow.png" width="4"/> <a href="http://www.scansee.com/scansee_privacypolicy.html">Click here for details</a></p>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
</div>
<script type="text/javascript">


</script>
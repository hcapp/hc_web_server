<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<script type="text/javascript">
function printQRCode() {

	var html = "<html>";
	
	html +="<head><script>window.onload= function () { window.print();window.close();}  <\/script></head><body>";
	
	
	html +="<table align='center' cellspacing='0' cellpadding='0' ><tr><td align='center'><img src='../images/hubciti-logo_ret.png'/></td></tr><td align='center'>";
	var baseUrl = location.href.substring(0, nth_ocurrence(location.href,
			"/", 3));

	html += "<img src="+"'"+baseUrl+'${requestScope.pageDetails.qrImagePath}'+"'"+" width=\"111\" height=\"113\"/>"
			+ "</td></tr>";

	html += "<tr><td align='center'>"
			+ '${requestScope.pageDetails.retPageTitle}'
			+ "</td></tr></table>"
	html += "</body></html>";

	var printWin = window.open('', '_blank');
	printWin.document.write(html);
	printWin.document.close();
	//printWin.focus();
	//printWin.print();
	//setTimeout('printWin.close()', 1); 
	//printWin.close();

}

function nth_ocurrence(str, needle, nth) {
	for (i = 0; i < str.length; i++) {
		if (str.charAt(i) == needle) {
			if (!--nth) {
				return i;
			}
		}
	}
	return false;
}
</script>
<div id="wrapper">
	<div id="header">
		<div id="header_logo" class="floatL">
			<a href="index.html"> <img alt="ScanSee"
				src="../images/hubciti-logo_ret.png" />
			</a>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div class="grdSec">
		<div id="content" class="floatL">
			<table width="94%" border="0" cellspacing="0" cellpadding="0"
				class="brdrLsTbl">
				<tr>
					<c:if test="${requestScope.QRType ne 'GiveAway'}">
						<td colspan="3" class="title">QR code for your selection:</td>
					</c:if>
					<c:if test="${requestScope.QRType eq 'GiveAway'}">
						<td colspan="3" class="title">QR code for GiveAway Page:</td>
					</c:if>
				</tr>
				<tr class="print">
					<td colspan="2">
						<ul class="qrImg">
							<li id="prod_qr"><div id="areaToPrint">
									<img src="${requestScope.pageDetails.qrImagePath}" width="111"
										height="113">
								</div></li>
							<li id="audio_qr"></li>
						</ul>
					</td>
					<td><h4>
							ScanSee provides you with QR codes that can link consumers <br>directly
							to pages within your Appsite. Use the ScanSee scanner <br>
							in the ScanSee app and give it a try!
						</h4></td>
				</tr>
				<tr class="print">
					<td colspan="3"><span class="title">${requestScope.pageDetails.retPageTitle}</span>
					</td>
				</tr>
				<tr>
					<td><img src="../images/download.png" alt="download"
						width="16" height="16" /></td>
					<td colspan="2">To download, right-click on the image, then
						select &quot;Save Picture As...&quot; from the pop-up menu.</td>
				</tr>
				<tr>
					<td width="4%"><img src="../images/print.gif" alt="print"
						width="16" height="16" /></td>
					<td width="10%"><a href="#" onclick="printQRCode();">Print</a>
					</td>
					<td></td>
				</tr>
			</table>

		</div>
		<div class="clear"></div>
	</div>
</div>

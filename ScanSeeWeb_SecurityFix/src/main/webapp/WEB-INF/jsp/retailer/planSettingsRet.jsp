<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css"  href="/ScanSeeWeb/styles/bubble-tooltip.css"  media="screen" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>

<form:form name="retplansettingform" commandName="retplansettingform" acceptCharset="ISO-8859-1">
<div id="wrapper">
   <div id="content" class="shdwBg">
    <%@include file="retailerLeftNavigation.jsp"%>
<div class="grdSec rtContPnl floatR">
      <div id="secInfo">Update Plans</div>
      <div id="subnav">
        <ul>
         <li><a href="/ScanSeeWeb/retailer/editRetailerProfile.htm" title="Profile Settings" rel="Profile Settings"><span>Profile Settings</span></a></li>
         <li><a href="" class="active" title="Plan Settings" rel="Plan Settings"><span>Plan Settings</span></a></li>
        <li><a href="/ScanSeeWeb/retailer/accountPaymentRet.htm" title="Payment Settings" rel="Payment Settings"><span>Payment Settings</span></a></li>
        </ul>
      </div>
	<h2>Plan Settings:</h2>
      <table class="cstmTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
          <tr class="header">
            <td class="editLink">PRODUCT</td>
            <td class="editLink">DESCRIPTION</td>
            <td class="editLink">QTY</td>
            <td class="editLink">PRICE</td>
            <td class="editLink">SUBTOTAL</td>
          </tr>
          <tr>
            <td class="editLink">Basic Plan - Per Store Access</td>
            <td class="editLink">The basic plan allows you to have access to ...</td>
            <td class="editLink">1</td>
            <td class="editLink">$150.00</td>
            <td class="editLink">$150.00</td>
          </tr>
          <tr>
            <td>Initiation Fee</td>
            <td>Waived</td>
            <td></td>
            <td>$0.00</td>
            <td>$0.00</td>
          </tr>
          <tr>
            <td>API Integration&nbsp;Fee</td>
            <td>Waived</td>
            <td></td>
            <td>$0.00</td>
            <td>$0.00</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><!--<img title="check" alt="check" src="../images/check.png" width="16" height="16"/>--></td>
            <td><!--<img title="check" alt="check" src="../images/check.png" width="16" height="16"/>--></td>
          </tr>
          <tr class="dottedLine">
            <td class="dottedLine"></td>
            <td></td>
            <td></td>
            <td align="center"></td>
            <td></td>
          </tr>
          <tr class="dottedLine">
            <td></td>
            <td></td>
            <td></td>
            <td><strong>Total</strong> </td>
            <td><strong>$150.00</strong> </td>
          </tr>
        </tbody>
      </table>
      <div class="navTabSec mrgnRt" align="right">
        <!--<input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Cancel3"/>-->
        <input class="btn" onclick="" value="Update" type="button" name="Cancel"/>
      </div>
    </div>
  </div>  </form:form>
  <div class="clear"></div>


  

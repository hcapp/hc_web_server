<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>

<script type="text/javascript">
function getPerPgaVal(){

	var selValue=$('#selPerPage :selected').val();
	document.retailerDealsForm.recordCount.value=selValue;
	
      //Call the method which populates grid values
       getRetailersDealsByName();
			
}
/*Same Code in 2218
function getRetailersDealsByName() {
	showProgressBar();
	document.retailerDealsForm.action = "hotDealRetailer.htm";
	document.retailerDealsForm.method = "POST";
	document.retailerDealsForm.submit();
}*/

</script>
<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="retailerLeftNavigation.jsp"%>
		<form:form name="retailerDealsForm" commandName="retailerDealsForm"
			acceptCharset="ISO-8859-1">
			<form:hidden path="hotDealID"/>
			<form:hidden path="recordCount"/>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">Hot Deals</h1>
				</div>
				<div class="grdSec brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<tr>
							<td colspan="3" class="header">Search HotDeal</td>
						</tr>
						<tr>
							<td class="Label" width="18%">HotDeal Name</td>
							 <!-- <td width="59%"><form:input path="hotDealName"
									name="hotDealName" id="hotDealName" /> <a href="#"><img
									src="/ScanSeeWeb/images/searchIcon.png" alt="Search"
									title="Search" onclick="getRetailersDealsByName();" width="25"
									height="24" /> </a></td>-->
									<td width="59%"><form:select path="hotDealName" id="hotDealName" class="selecBx" onchange="getRetailersDealsByName();">
										<form:option value="0" label="--Select--">--Select--</form:option>
										<c:forEach items="${sessionScope.hotDealsList}" var="h">
										<form:option value="${h.hotDealID}" label="${h.hotDealName}" />
								</c:forEach>
							</form:select></td>
							<td width="23%"><input type="button" class="btn"
								name="addDeals" value="Add"
								onclick="window.location.href='hotDealRetailorShowPage.htm'"
								title="Add" /></td>
						</tr>
					</table>
					<div class="searchGrd">
						<h1 class="searchHeaderExpand">
							<a href="#" class="floatR">&nbsp;</a>Current Deals
						</h1>
						<div class="grdCont tableScroll zeroPadding">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="stripeMe" id="dealLst">
								<tr class="header">
									<td width="18%">Deal Name</td>
									<td width="10%">Sale Price($)</td>
									<td width="15%">Deal Start Date</td>
									<td width="15%">Deal End Date</td>
									<td width="37%">Actions</td>
								</tr>

								<div align="center" style="font-style: 45">
									<label><form:errors
											cssStyle="${requestScope.manageDealsFont}" /> </label>
								</div>
								<!--<c:if test="${message ne null }">
							<div id="message">
								<center>
									<h2>
										<c:out value="${message}" />
									</h2>
								</center>
							</div>
							<script>var PAGE_MESSAGE = true;</script>
						</c:if>-->
								<c:forEach items="${sessionScope.listOfDeals.dealList}"
									var="hotDeals">

									<tr>
										<!--<td width="25%"><c:out value='${hotDeals.hotDealName}' /></td>-->
										<td ><a href="#"
											onclick="hotDealRetailerEdit(<c:out value='${hotDeals.hotDealID}' />);"
											title="Click Here to Edit"><c:out
													value='${hotDeals.hotDealName}' /> </a>
										</td>
										<td ><c:out value='${hotDeals.salePrice}' />
										</td>
										<td ><c:out value='${hotDeals.dealStartDate}' />
										</td>
										<td ><c:out value='${hotDeals.dealEndDate}' />
										</td>
										<td ><input name="Re-Run4" value="Re-Run"
											type="button" name="rerunbutton" id="rerunbutton" class="btn"
											onclick="hotDealRetailerReRun(<c:out value='${hotDeals.hotDealID}' />);"
											title="ReRun" /><input name="edit" value="Edit"
											type="button" name="editbutton" id="editbutton" class="btn"
											onclick="hotDealRetailerEdit(<c:out value='${hotDeals.hotDealID}' />);"
											title="Edit" /><input name="View Report" value="View Report"
											type="button" name="View Report" id="View Report" class="btn"
											onclick="javascript:hotDealRetailerViewReport(<c:out value='${hotDeals.hotDealID}'/>);"
											title="View Report" />
										</td>
									</tr>
								</c:forEach>

							</table>

						</div>
						<div class="pagination brdrTop">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="noBrdr" id="perpage">
								<tr>
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" enablePerPage="true" />
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</form:form>
	</div>
</div>
<div class="clear"></div>


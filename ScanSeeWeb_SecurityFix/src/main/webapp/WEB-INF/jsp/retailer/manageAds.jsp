<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script type="text/javascript">

	$(document).ready(function() {
		$("#date").datepicker({ showOn: 'both', buttonImageOnly: true ,buttonImage: '/ScanSeeWeb/images/calendarIcon.png' });
	});

	function getPerPgaVal(){

		var selValue=$('#selPerPage :selected').val();
		document.manageadform.recordCount.value=selValue;
		
	      //Call the method which populates grid values
	       searchAds();
				
	}
		
</script>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">B. Welcome Page View</h1>
				</div>
				<form:form commandName="manageadform" name="manageadform"
					acceptCharset="ISO-8859-1">
					<form:hidden path="retailLocationIDHidden" />
					<form:hidden path="retailLocationAdvertisementID" />
					<form:hidden path="deleteFlag" />
					<form:hidden path="expireFlag" />
					<form:hidden path="recordCount" />
					<div class="grdSec brdrTop">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="tranTbl">
							<tr>
								<td colspan="3" class="header"><span class="floatR"><a
										href="#" title="Calendar View"><input type="hidden"
											id="date" /> Calendar View</a> </span>Welcome Page</td>
							</tr>
							<tr>
								<td width="20%" align="left"><label for="label">Search
										Welcome Pages</label></td>
								<td width="60%"><form:input path="advertisementName"
										type="text" name="textfield" id="gnrlSrch" /> <a href="#">
										<img src="/ScanSeeWeb/images/searchIcon.png" alt="Search"
										title="Search" onclick="searchAds();" width="25" height="24" />
								</a> <label for="gnrlSrch"></label></td>
								<td width="20%"><input type="button" class="btn"
									value="Build Welcome Page"
									onclick="window.location.href='createbannerad.htm'" title="Add" />
								</td>
							</tr>
						</table>

						<div class="searchGrd">
							<h1 class="searchHeaderExpand">
								<a href="#" class="floatR">&nbsp;</a>Current Ads
							</h1>
							<div class="grdCont tableScroll zeroPadding">

								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="gr">
									<tbody>

										<tr class="header">
											<td width="25%">Welcome Pages Name</td>
											<td>Upload Image</td>
											<td width="12%">Start Date</td>
											<td width="12%">End Date</td>
											<td width="26%">Action</td>

										</tr>
										<c:if test="${message ne null }">
											<div id="message">
												<center>
													<label style="color: red; font-weight: regular;"> <c:out
															value="${message}" /> </label>
												</center>
											</div>
											<script>var PAGE_MESSAGE = true;</script>
										</c:if>

										<c:if test="${adsList ne null && ! empty adsList}">
											<c:forEach items='${sessionScope.adsList.addsList}'
												var='item' varStatus="indexnum">
												<tr>
													<td><a href="#"
														onclick="javascript:editManageAds(<c:out value='${item.retailLocationAdvertisementID}'/>);"
														title="Click Here to Edit"> <c:out
																value="${item.advertisementName}" /> </a>
													</td>
													<td><center>
															<img alt="${item.advertisementName}"
																src="${item.strBannerAdImagePath}" height="72"
																width="50">
														</center>
													</td>
													<td><c:out value="${item.advertisementDate}" />
													</td>
													<td><c:choose>
															<c:when
																test="${item.advertisementEndDate == null || item.advertisementEndDate eq 'null'}">
										       No End Date
										    </c:when>
															<c:otherwise>
																<c:out value="${item.advertisementEndDate}" />
															</c:otherwise>
														</c:choose>
													</td>
													<td><label> <input type="button" name="button"
															class="btn" id="button" value="Show Locations"
															title="Show Locations"
															onclick="javascript:showLocation(${item.retailLocationAdvertisementID})" />
															<c:if
																test="${item.expireFlag == 1 && item.deleteFlag == 0}">
 	 										&nbsp;<a href="#"><img
																	src="/ScanSeeWeb/images/stopIcon.png"
																	title="Stop Campaign" alt="Stop Campaign" width="24"
																	height="22"
																	onclick="javascript:deleteWelcomePage(<c:out value='${item.retailLocationAdvertisementID}'/>, <c:out value='${item.expireFlag}'/>, <c:out value='${item.deleteFlag}'/>, 'Stop Campaign');" />
																</a>
															</c:if> <c:if
																test="${item.expireFlag == 0 && item.deleteFlag == 1}">
 	 										&nbsp;<a href="#"><img
																	src="/ScanSeeWeb/images/deleteRedIcon.png"
																	title="delete" alt="delete" width="24" height="22"
																	onclick="javascript:deleteWelcomePage(<c:out value='${item.retailLocationAdvertisementID}'/>, <c:out value='${item.expireFlag}'/>, <c:out value='${item.deleteFlag}'/>, 'delete');" />
																</a>
															</c:if> </label>
													</td>
												</tr>
											</c:forEach>
										</c:if>
								</table>

							</div>
							<div class="pagination brdrTop">

								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="noBrdr" id="perpage">
									<tr>
										<page:pageTag
											currentPage="${sessionScope.pagination.currentPage}"
											nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
											pageRange="${sessionScope.pagination.pageRange}"
											url="${sessionScope.pagination.url}" enablePerPage="true" />
									</tr>
								</table>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;background-color: White">
			<div class="headerIframe">
				<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
					alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
				height="100%" allowtransparency="yes" width="100%"
				style="background-color: White"> </iframe>
		</div>

		<div class="clear"></div>
	</div>
</body>

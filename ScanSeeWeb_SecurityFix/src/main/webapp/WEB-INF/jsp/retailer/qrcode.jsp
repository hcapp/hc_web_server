<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
	function printQRCode() {

		var html = "<html><table align='center' cellspacing='0' cellpadding='0' ><tr><td align='center'><img src='../images/hubciti-logo_ret.png'/></td></tr><td align='center'>";
		html += document.getElementById('areaToPrint').innerHTML + "</td></tr>";
		html += "<tr><td align='center'>" + '${qrCodeImageTitle}'
				+ "</td></tr></table>"
		html += "</html>";

		var printWin = window.open('', '_blank');
		printWin.document.write(html);
		printWin.document.close();
		printWin.focus();
		printWin.print();
		printWin.close();

	}
</script>
<div class="shdwBg" id="content" style="min-height: 73px;">
	<%@include file="retailerLeftNavigation.jsp"%>
	<div class="grdSec rtContPnl floatR">
	<!-- 	<div id="secInfo">QR Code</div>  -->
		<div id="secInfo">This is a unique QR Code specifically for this AnyThingPage<sup class="smallsup">TM</sup>:${sessionScope.pageTitle}</div>
		<table width="100%" cellspacing="0" cellpadding="0" border="0"
			id="rtlr" class="grdTbl cstmpg" class="print">
			<tbody>
				<tr>
				<!-- 	<td class="header" colspan="4">QR code for your selection:</td> -->
				<td class="header" colspan="4">When scanned, the user is directed to this page in your AppSite<sup class="smallsup">TM</sup> </td>
				</tr>

				<tr border="none">
     <td align="left" colspan="4"> 
	
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
  <tr  class="remBrdr" style="border-bottom:1px solid #fff">
    <td width="10%" id="prod_qr" style="border-bottom:1px solid #fff"><div id="areaToPrint"><img width="100" height="100" src="${qrCodeImagePath}"></div></td>
    <td width="90%" style="border-bottom:1px solid #fff"> <p>Print this QR code on your printed materials and other media to direct viewers to the above AppSite page</p></td>
  </tr>
</table>
</td>
</tr>
			
			<!-- 	<tr>
					<td align="left" colspan="4">
						<ul class="qrImg">
							<li id="prod_qr" class="firstCel"><div id="areaToPrint">
									<img width="100" height="100" src="${qrCodeImagePath}">
								</div>
								
								</li>
								
						<li>
						<br></br><br></br>
							<p>Print this QR code on your printed materials and other media to direct viewers to the above AppSite page</p> 
						</li>
							
						 	<p>ScanSee provides you with QR
									codes that can link consumers directly to pages within your
									Appsite.  Use the ScanSee scanner in the ScanSee app and give
									it a try!&#13;</p>
									
						</ul></td>

				</tr>-->


				<tr>
					<td class="Label"
						style="border-right: 1px solid rgb(218, 218, 218);"><img
						width="16" height="16" alt="download" src="../images/download.png">
					</td>
					<td colspan="3">Download: Right-click on the QR code, then
						select "Save Picture As" from the pop-up menu.</td>
				</tr>
				<tr>
					<td class="Label"
						style="border-right: 1px solid rgb(218, 218, 218);"><img
						width="16" height="16" alt="print" src="../images/print.gif">
					</td>
					<td colspan="3"><a href="#" onclick="printQRCode();">Print</a>
					</td>
				</tr>
				<tr>
					<td width="4%" class="Label"
						style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>

					<c:choose>
						<c:when test="${requestScope.splOffer eq 'splOffer' }">
							<td width="96%" colspan="3"><input type="button"
								onclick="location.href='/ScanSeeWeb/retailer/specialOffer.htm'"
								value="Back" class="btn"></td>
						</c:when>
						<c:when test="${requestScope.splOffer eq 'addLocation' }">
							<td width="96%" colspan="3"><input type="button"
								onclick="location.href='/ScanSeeWeb/retailer/addlocation_dashboard.htm'"
								value="Back" class="btn"></td>
						</c:when>
							<c:when test="${requestScope.splOffer eq 'giveaway' }">
							<td width="96%" colspan="3"><input type="button"
								onclick="location.href='/ScanSeeWeb/retailer/managegiveawaypages.htm'"
								value="Back" class="btn"></td>
						</c:when>
						<c:otherwise>
							<td width="96%" colspan="3"><input type="button"
								onclick="location.href='/ScanSeeWeb/retailer/buildAnythingPage.htm'"
								value="Back" class="btn"></td>
						</c:otherwise>
					</c:choose>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="clear"></div>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page session="true"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css" href="styles/style.css" />
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
    $(function() {
           //Get actual content and display as title on mouse hover on it.
             var actText = $("td.genTitle").text();
               $("td.genTitle").attr('title',actText);
        var limit = 20;// character limit restricted to 20
        var chars = $("td.genTitle").text(); 
        if (chars.length > limit) {
            var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
            var dots = $("<span class='dots'>...</span>");
            $("td.genTitle").empty()
                .append(visibleArea)
                .append(dots);//append trailing dots to the visibile part 
        }
                                
    });
</script>
<div id="iphonePanel">
	<div class="navBar iphone" id="onlyAds">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
            <tr>
              <td width="19%"><img src="../images/backBtn.png" alt="back" width="50" height="30" /></td>
              <td width="54%" class="genTitle"><%=session.getAttribute("retailerName")%></td>
              <td width="27%"><img src="../images/mainMenuBtn.png" alt="mainmenu" width="78" height="30" /></td>
            </tr>
          </table>
    </div>
	<div class="viewAd" >
		<form:form name="previewBuildBannerform" commandName="previewBuildBannerform">
			<div id="ribbonAd"><img src="<%=session.getAttribute("fileRibbonAd")%>" alt="RibbonAd" width="320" height="50"  /></div>
		</form:form>
	</div>
</div>
<br />
<div align="center">
	<input class="btn" value="Back" type="button" name="Back" onclick="javascript:history.back()" />
</div>
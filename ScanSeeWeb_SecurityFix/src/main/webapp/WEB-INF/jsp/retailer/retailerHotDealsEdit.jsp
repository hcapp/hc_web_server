<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="scripts/web.js" type="text/javascript"></script>

<!--<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker();
	});

	$(document).ready(function() {
		$("#datepicker2").datepicker();
	});
	$(document).ready(function() {
		radioSel('slctOpt');
		$('input[name="slctOpt"]').click(function() {
			radioSel('slctOpt');
		});
	});
</script>
-->
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
	$(document).ready(function() {
		$("#datepicker3").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});

		/* onClick Save button, Validate for discount percentage.*/
		buttonClicked = false;
		$('#btn').click(function(event) {
			buttonClicked = true;
			validatePrice();
		});
	});
</script>
<script type="text/javascript">
	/* On Tab out click, Validate for discount percentage.*/
	function validatePrice() {
		var vRegPrice = $("#regPrice").val();
		var vSalePrice = $("#salePrice").val();
		var vPriceCal = vSalePrice / vRegPrice;
		var vPricePercent = parseInt(vPriceCal * 100);
		if (!isNaN(vPricePercent)) {
			var vHalfRegPrice = vRegPrice - vSalePrice;
			var vPercentage = (vHalfRegPrice * 100) / vRegPrice;

			if (vPercentage < 50) {
				alert("Discount percentage should be greater than or equal to 50%");
				$("#salePrice").val("").focus();
				buttonClicked = false;
				return false;
			} else {
				if (buttonClicked) {
					hotDealEditRetailer();
				} else {
					return true;
				}
			}
		} else {
			$("#salePrice").val("").focus();
			return false;
		}
	}

	var changeImgDim = '${sessionScope.ChangeImageDim}';
	if (null != changeImgDim && changeImgDim == 'true') {
		$('#hotDealImg').width('70');
		$('#hotDealImg').height('70px');
	}

	function checkHotDealImgValidate(input) {
		var vHotDealImg = document.getElementById("trgrUpld").value;
		if (vHotDealImg != '') {
			var checkHotDealImg = vHotDealImg.toLowerCase();
			if (!checkHotDealImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload HotDeal image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
				document.edithotdealretailerform.imageFile.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}

	function radioSel(radioName) {
		var vCity = document.edithotdealretailerform.cityHiddenChecked.value;
		$('tr.slctCty').hide();
		$('tr.slctLoc').hide();
		var cls = $('input[name ="' + radioName + '"]:checked').val();
		if (cls == undefined || cls == null || cls == '') {
			cls = vCity;
		}
		if (cls == "slctLoc") {
			//var productId = document.edithotdealretailerform.prodcutID.value;
			//document.getElementById('slctLoc').checked = true;
			document.edithotdealretailerform.dealForCityLoc.value = 'Location';

			//$('tr.' + cls).show();
			loadRetailerLocation();
			

		} else if (cls == "slctCty") {
			//document.getElementById('slctCty').checked = true;
			document.edithotdealretailerform.dealForCityLoc.value = 'City';
			loadPopulationCenters();
			$('tr.' + cls).show();
		}
	}

	function loadPopulationCenters() {
		var vTabIndexCity = $('#tabIndexCity').val();
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/retailer/fetchhotdealpopcenters.htm",
			data : {
				'tabIndexCity' : vTabIndexCity
			},

			success : function(response) {
				$('#myAjax1').html(response);
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}
	function loadRetailerLocation() {
		var vTabIndexLoc = $('#tabIndexLoc').val();
		var productId = $('#prodcutID').val();
		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/retailer/fetchretailerlocation.htm",
			data : {
				'productId' : productId,
				'tabIndexLoc' : vTabIndexLoc
			},
			success : function(response) {
				$('#myAjax3').html(response);
				$('tr.slctLoc').show();
			},
			error : function(e) {
				alert("Error occurred while Showing Retail Loc Ids");
			}
		});
	}
	function checkAssociatedProd() {
		var $prdID = $('#prodcutID').val();
		var $retailId = $('#retailID').val();
		$.ajaxSetup({
			cache : false
		});
		if ($prdID != "") {
			$.ajax({
				type : "GET",
				url : "/ScanSeeWeb/retailer/checkRetAssociatedProd.htm",
				data : {
					'productId' : $prdID,
					'retailID' : $retailId,
				},
				success : function(response) {
					openIframePopup('ifrmPopup2', 'ifrm2',
							'produpclisthotdealadd.htm', 420, 600,
							'View Product/UPC')
				},
				error : function(e) {
					alert('Error:' + 'Error Occured');
				}
			});

		} else {
			openIframePopup('ifrmPopup2', 'ifrm2', 'produpclisthotdealadd.htm',
					420, 600, 'View Product/UPC')
		}
	}
</script>
<script type="text/javascript">
	function onCategoryLoad() {
		var vCategoryID = document.edithotdealretailerform.bCategoryHidden.value;
		var sel = document.getElementById("bCategory");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCategoryID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}

	function checkAlphaNumeric(evt) {
		var keycode;
		if (window.event)
			keycode = window.event.keyCode;
		else if (event)
			keycode = event.keyCode;
		else if (e)
			keycode = e.which;
		else
			return true;
		if ((keycode >= 47 && keycode <= 57)
				|| (keycode >= 65 && keycode <= 90)
				|| (keycode >= 95 && keycode <= 122)) {
			return true;
		} else {
			alert("Please do not use special characters")
			return false;
		}
		return true;
	}
</script>


<div id="wrapper">
	<div id="content" class="topMrgn">
		<div class="section topMrgn">
			<div class="grdSec brdrTop">
				<form:form name="edithotdealretailerform"
					commandName="edithotdealretailerform"
					action="/ScanSeeWeb/retailer/uploadedithotdealsimg.htm"
					acceptCharset="ISO-8859-1" enctype="multipart/form-data">
					<form:hidden path="productId" name="prodcutID" id="prodcutID" />
					<form:hidden path="productImage" name="productImage"
						id="productImage" />
					<form:hidden path="productName" id="productName" />
					<form:hidden path="dealForCityLoc" name="dealForCityLoc"
						id="dealForCityLoc" />
					<form:hidden path="retailID" id="retailID" />
					<form:hidden path="hotDealID" />
					<form:hidden path="bCategoryHidden" />
					<form:hidden path="viewName" value="retailerHotDealsEdit" />
					<form:hidden path="dealImgPath" id="dealImgPath" />
					<form:hidden path="cityHiddenChecked" />
					<!--<form:hidden path="price" id="regPrice"/>-->
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">

						<tr>
							<td colspan="4" class="header">Edit Deal <c:if
									test="${message ne null }">
									<span class="alertTxt-dsply leftPdng"> <c:out
											value="${message}" /> </span>
									<script>
										var PAGE_MESSAGE = true;
									</script>
								</c:if>
							</td>
						</tr>
						<tr>
							<td width="18%" class="Label"><label for="dealName"
								class="mand">Hot Deal Name</label>
							</td>
							<td><form:errors cssStyle="color:red" path="hotDealName"></form:errors>
								<form:input path="hotDealName" type="text" name="imprsns"
									id="hotdealname" tabindex="1" />
							</td>
							<td class="Label">Product</td>
							<td><form:input path="scanCode" type="text"
									name="couponName" id="couponName" readonly="true" tabindex="2"
									class="textboxMedium" /> <a href="#"><img
									src="/ScanSeeWeb/images/searchIcon.png" alt="Search" width="20"
									height="17" onclick="checkAssociatedProd();"
									title="Click here to View Product List" /> </a></td>
						</tr>
						<tr>
							<td class="Label"><label for="Clicks"></label><label
								for="regPrice" class="mand">Regular Price $</label>
							</td>
							<td width="33%"><form:errors cssStyle="color:red"
									path="price"></form:errors> <form:input path="price"
									type="text" onkeypress="return isNumberKeyPhone(event)"
									name="regular price" id="regPrice" tabindex="3" />
							</td>

							<td width="17%" class="Label"><label for="salePrice"
								class="mand">Sale Price $</label></td>
							<td width="32%"><form:errors cssStyle="color:red"
									path="salePrice"></form:errors> <form:input path="salePrice"
									type="text" name="textfield" id="salePrice" tabindex="4"
									onkeypress="return isNumberKeyPhone(event)"
									onkeyup="validatePrice();" />
							</td>
						</tr>
						<tr>
							<td class="Label"><label for="dealDesc" class="mand">Hot
									Deal Description</label></td>
							<td><form:errors cssStyle="color:red"
									path="hotDealShortDescription"></form:errors> <form:textarea
									path="hotDealShortDescription" name="textarea2" id="htdealDesc"
									cols="45" rows="5" class="txtAreaSmall" tabindex="5"></form:textarea>
							</td>

							<td class="Label"><label for="dealDesc" class="mand">Coupon
									Code</label></td>
							<td><form:errors cssStyle="color:red" path="couponCode"></form:errors>
								<form:input type="text" path="couponCode" name="couponCode"
									id="couponCode" onkeypress="return checkAlphaNumeric(event)"
									tabindex="6" />
							</td>
						</tr>
						<tr>
							<td class="Label">Hot Deal Long Description</td>
							<td colspan="3"><label> <form:textarea
										path="hotDealLongDescription" name="textarea" cols="45"
										rows="5" class="txtAreaLarge" id="htdealLngDesc" tabindex="7"></form:textarea>
							</label>
							</td>
						</tr>
						<tr>
							<td class="Label"><label for="dealT&C" class="mand">Hot
									Deal Terms</label></td>

							<td colspan="3"><form:errors cssStyle="color:red"
									path="hotDealTermsConditions"></form:errors> <form:textarea
									path="hotDealTermsConditions" name="textarea3" cols="45"
									rows="5" class="txtAreaLarge" id="htdealTerms" tabindex="8"></form:textarea>
							</td>
						</tr>
						<tr>
							<!--  <td class="Label">URL</td>
							<td><form:input path="url" type="text"
									name="age2" id="url" onkeyup="checkMaxLength(this,'1000');" tabindex="9"/>
							</td>-->
							<td class="Label"><label for="upldImg">Hot Deal
									Image</label>
							</td>
							<td colspan="3">
								<ul class="imgInfoSplit" id="hotdealUpld">
									<li><label><img id="hotDealImg" alt="upload"
											src="${sessionScope.addHotDealImagePath}" height="80"
											width="80"
											onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
									</label><span class="topPadding forceBlock"><label
											for="trgrUpld"> <input type="button" value="Upload"
												id="trgrUpldBtn" width="350" height="460"
												class="btn trgrUpld" title="Upload Image File" tabindex="10">
												<form:input path="imageFile" type="file" class="textboxBig"
													id="trgrUpld" onchange="checkHotDealImgValidate(this);" />
												<form:errors path="imageFile" cssStyle="color:red"></form:errors>
										</label> <label id="hotDealImagePathErr"
											style="color: red; font-style: 45"></label> </span></span>
									</li>
									<li>Suggested Minimum Size:<br>70px/70px<br>Maximum Size:800px/600px<br>
								</ul></td>

						</tr>
						<tr>
							<td class="Label"><label for="bcategory" class="mand">
									Product Categories</label></td>
							<td align="left" colspan="3"><form:errors
									cssStyle="color:red" path="bCategory"></form:errors> <form:select
									path="bCategory" id="bCategory" class="selecBx" tabindex="11">
									<form:option value="0" label="--Select--">--Select-</form:option>
									<c:forEach items="${sessionScope.categoryList}" var="c">
										<form:option value="${c.categoryID}"
											label="${c.parentSubCategory}" />
									</c:forEach>
								</form:select>
							</td>
						</tr>

						<tr>
							<td class="Label"><label for="csd"><label
									for="dealEndDate" class="mand">Deal Start Date</label> </label>
							</td>

							<td align="left"><form:errors cssStyle="color:red"
									path="dealStartDate"></form:errors> <form:input
									path="dealStartDate" name="csd" type="text" class="textboxDate"
									id="datepicker1" tabindex="12" />(mm/dd/yyyy)</td>
							<td class="Label"><label for="ced"><label
									for="dealEndDate" >Deal End Date</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="dealEndDate"></form:errors> <form:input
									path="dealEndDate" name="ced" type="text" class="textboxDate"
									id="datepicker2" tabindex="13" />(mm/dd/yyyy)</td>
						</tr>
						<tr>
							<td class="Label"><label for="cst">Deal Start Time</label></td>
							<td><form:select path="dealStartHrs" class="slctSmall"
									tabindex="14">
									<form:options items="${DealStartHours}" />
								</form:select> Hrs <form:select path="dealStartMins" class="slctSmall"
									tabindex="15">
									<form:options items="${DealStartMinutes}" />
								</form:select> Mins</td>
							<td class="Label"><label for="cet">Deal End Time</label>
							</td>
							<td><form:select path="dealEndhrs" class="slctSmall"
									tabindex="16">
									<form:options items="${DealStartHours}" />
								</form:select> Hrs <form:select path="dealEndMins" class="slctSmall"
									tabindex="17">
									<form:options items="${DealStartMinutes}" />
								</form:select> Mins</td>

						</tr>

						<tr>
							<td class="Label"><label for="dealStDate" class="mand">Expiration
									Date</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="expireDate"></form:errors> <form:input path="expireDate"
									name="datepicker3" id="datepicker3" class="textboxDate"
									tabindex="18" />(mm/dd/yyyy)</td>
							<td class="Label"><label for="numofCpn">#
									of Hot Deals Available</label></td>
							<td><!--<form:errors path="numOfHotDeals" cssStyle="color:red"></form:errors>-->
								<form:input path="numOfHotDeals" id="numofhotdeals" type="text"
									onkeypress="return isNumberKey(event)" tabindex="19" />
							</td>
						</tr>
						<tr>
							<td class="Label"><label for="cst">Expiration Time</label></td>
							<td colspan="3"><form:select path="expireHrs"
									class="slctSmall" tabindex="20">
									<form:options items="${DealStartHours}" />
								</form:select> Hrs <form:select path="expireMins" class="slctSmall"
									tabindex="21">
									<form:options items="${DealStartMinutes}" />
								</form:select> Mins</td>
						</tr>
						<tr>

							<td class="Label"><label for="timeZone">Time Zone</label></td>
							<td colspan="3"><form:select path="dealTimeZoneId"
									class="selecBx" tabindex="22">
									<form:option value="0" label="--Select--">Please Select Time Zone</form:option>
									<c:forEach items="${sessionScope.retailerTimeZoneslst}"
										var="tz">
										<form:option value="${tz.timeZoneId}"
											label="${tz.timeZoneName}" />
									</c:forEach>
								</form:select>
						</tr>
						<tr>
							<td class="Label">&nbsp;</td>
							<td><input name="slctOpt" type="radio" value="slctCty"
								id="slctCty" tabindex="23" /> Deal for Specific City?</td>
							<td align="center" class="Label"><strong>OR</strong>
							</td>
							<td><input type="radio" name="slctOpt" value="slctLoc"
								id="slctLoc" tabindex="25" /> Deal for Specific Location?</td>
						</tr>
						<tr class="slctCty">
							<form:hidden path="tabIndexCity" value="24" id="tabIndexCity" />
							<td class="Label">&nbsp;</td>
							<td colspan="3" class="Label" align="left"><form:errors
									cssStyle="color:red" path="city"></form:errors>
								<div id="myAjax1" class="floatL">
									<form:select path="city" id="City" class="textboxBig">
										<form:option value="0" label="">Please Select Population Center</form:option>
									</form:select>
								</div> <span class="floatL"><label class="mand"
									for="dealStDate">&nbsp;</label> </span></td>
						</tr>
						<tr class="slctLoc">
							<td class="Label">&nbsp;</td>
							<td class="Label">&nbsp;</td>
							<form:hidden path="tabIndexLoc" value="26" id="tabIndexLoc" />
							<td colspan="2"><form:errors cssStyle="color:red"
									path="retailerLocID"></form:errors>
								<div id="myAjax3" class="floatL">
									<form:select path="retailerLocID" class="textboxBig"
										id="retailerLocID">
										<form:option value="0">Please Select Location</form:option>
									</form:select>
								</div> <span class="floatL"><label class="mand"
									for="dealStDate">&nbsp;</label> </span></td>
						</tr>
					</table>

					<div class="ifrmPopupPannel" id="ifrmPopup2" style="display: none;background-color: White">
					<div class="headerIframe">
						<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
							alt="close"
							onclick="javascript:closeIframePopup('ifrmPopup2','ifrm')"
							title="Click here to Close" align="middle" /> <span
							id="popupHeader"></span>
					</div>
					<iframe frameborder="0" scrolling="auto" id="ifrm2" src=""
						height="100%" allowtransparency="yes" width="100%"
						style="background-color: White"> </iframe>
				</div>

					<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
						<div class="headerIframe">
							<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
								alt="close"
								onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
								title="Click here to Close" align="middle" /> <span
								id="popupHeader"></span>
						</div>
						<iframe frameborder="0" scrolling="no" id="ifrm" src=""
							height="100%" allowtransparency="yes" width="100%"
							style="background-color: White"> </iframe>
					</div>
				</form:form>
			</div>
			<div class="navTabSec">
				<div align="right">
					<input name="Back" value="Back" type="button" class="btn"
						onclick="location='/ScanSeeWeb/retailer/hotDealRetailer.htm'"
						title="Back" tabindex="27" /> <input name="Preview"
						value="Preview" type="button" class="btn"
						onclick="previewEditdealPopUp()" title="Preview" tabindex="28" />
					<input name="Save" value="Save" type="button" class="btn" id="btn"
						title="Save" tabindex="29" />
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var dealForCityLoc = document.edithotdealretailerform.dealForCityLoc.value;
	if (dealForCityLoc == 'City') {
		document.getElementById('slctCty').checked = 'checked';
		//loadPopulationCenters();
	} else if (dealForCityLoc == 'Location') {
		
		document.getElementById('slctLoc').checked = 'checked';
		//loadRetailerLocation();
	}
</script>
<script type="text/javascript">
	var hashVal = location.hash;
	hashVal = hashVal.substring(1, hashVal.length)
	if (hashVal) {
		var items = hashVal.split('&&');
		$('#snNo').val(items[0])
		$('#dlNm').val(items[1])
		$('#sp').val(items[2])
		$('#dsd').val(items[3])
		$('#ded').val(items[4])
	}
</script>
<script>
<!-- On load call to fetch city values -->
	onCategoryLoad();
	function checkHotDealSize(input) {
		var hDealImage = document.getElementById("trgrUpld").value;
		if (hDealImage != '') {
			var checkbannerimg = hDealImage.toLowerCase();
			if (!checkbannerimg.match(/(\.png)$/)) {
				alert("You must upload HotDeal image with following extensions : .png");
				document.edithotdealretailerform.imageFile.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}

	$('#trgrUpld')
			.bind(
					'change',
					function() {
						/*show progress bar : ETA for Web 1.3*/
						//showProgressBar();/* Commented to fix body scroll disable issue*/
						/*End*/
						$("#uploadBtn").val("trgrUpldBtn")
						$.ajaxSetup({
							cache : false
						});
						$("#edithotdealretailerform")
								.ajaxForm(
										{
											success : function(response) {
												$('#loading-image').css(
														"visibility", "hidden");
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue
												if (imgRes == 'UploadLogoMaxSize') {
													$('#hotDealImagePathErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#hotDealImagePathErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {
													$('#hotDealImagePathErr')
															.text("");
													var substr = imgRes
															.split('|');
													if (substr[0] == 'ValidImageDimention') {
														$('#hotDealImg').width(
																'70px');
														$('#hotDealImg')
																.height('70px');
														var imgName = substr[1];
														$('#dealImgPath').val(
																imgName);
														$('#hotDealImg').attr(
																"src",
																substr[2]);
													} else {
														/*commented to fix iframe popup scroll issue
														/$('body').css("overflow-y","hidden");*/
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}
											}
										}).submit();
					});
</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ScanSee</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)" />
<link href="styles/style.css" type="text/css" rel="stylesheet" />
<link href="styles/ticker-style.css" type="text/css" rel="stylesheet" />
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>

<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>

<script src="scripts/global.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">

<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
	/*$(document).ready(function() {
		$('#locationID').change(function(){
		 var len = $(this).find('option').length;
		 var sLen = $(this).find('option:selected').length;
		 if(len < sLen) {
		   $('#chkAllLoc').attr('checked','checked');
		 }else {
			$('#chkAllLoc').removeAttr('checked');
		 }
		});
		$('#locationID').trigger('change');
	});*/

	$(document).ready(function() {
		$('#locationID option').click(function() {
			var totOpt = $('#locationID option').length;
			var totOptSlctd = $('#locationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		$("#locationID").change(function() {
			var totOpt = $('#locationID option').length;
			var totOptSlctd = $('#locationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
	});
</script>

<script type="text/javascript">
	window.onload = function() {
		var vBackBtn = document.createcouponform.backButton.value;
		if (vBackBtn == 'no') {
			document.getElementById('back').style.visibility = 'hidden';
		}
	}


var changeImgDim = '${sessionScope.ChangeImageDim}';
if (null != changeImgDim && changeImgDim == 'true') {
	$('#couponImg').width('70px');
	$('#couponImg').height('70px');
}


function checkCouponImgValidate(input) {
	var vCouponImg = document.getElementById("trgrUpld").value;
	if (vCouponImg != '') {
		var checkCouponImg = vCouponImg.toLowerCase();
		if (!checkCouponImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
			alert("You must upload Coupon image with following extensions :  .png, .gif, .bmp, .jpg, .jpeg");
			document.createcouponform.imageFile.value = "";
			document.getElementById("trgrUpld").focus();
			return false;
		}
	}
}

	function checkAssociatedCouponProd() {
		var $prdID = $('#productID').val();
		$.ajaxSetup({
			cache : false
		});
		if ($prdID != "") {
			$.ajax({
				type : "GET",
				url : "/ScanSeeWeb/checkAssociatedCouponProd.htm",
				data : {
					'productId' : $prdID
				},
				success : function(response) {
					openIframePopup('ifrmPopup2', 'ifrm2', 'produpclist.htm',
							420, 600, 'View Product/UPC')
				},
				error : function(e) {
					alert('Error:' + 'Error Occured');
				}
			});

		} else {
			openIframePopup('ifrmPopup2', 'ifrm2', 'produpclist.htm', 420, 600,
					'View Product/UPC')
		}
	}

	function onLoadRetLocationID() {
		var vRetLocID = document.createcouponform.retLocationIDs.value;
		var vRetLocVal = document.getElementById("locationID");
		var vRetLocList = [];
		if (vRetLocID != "null" && vRetLocID != "") {
			vRetLocList = vRetLocID.split(",");
		}
		if (vRetLocVal.length != 0 && vRetLocList.length != 0) {
			if (vRetLocVal.length == vRetLocList.length) {
				document.getElementById('chkAllLoc').checked = true;
			}
		}

		for ( var i = 0; i < vRetLocVal.length; i++) {
			for (j = 0; j < vRetLocList.length; j++) {
				if (vRetLocVal.options[i].value == vRetLocList[j]) {
					vRetLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("locationID");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
</script>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>

</head>
<body>
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@include file="retailerLeftNavigation.jsp"%>
			<form:form name="createcouponform" commandName="createcouponform"
				method="POST" action="/ScanSeeWeb/retailer/uploadcouponimg.htm"
				acceptCharset="ISO-8859-1" enctype="multipart/form-data">
				<form:hidden path="retLocationID" id="retLocationID" />
				<form:hidden path="retLocationIDs" id="retLocationIDs" />
				<input type="hidden" name="formName" value="createCoupon" />
				<input type="hidden" id="selRelLoc" name="selRelLoc" />
				<form:hidden path="viewName" value="createcoupon" />
				<form:hidden path="couponImgPath" id="createcoupon" />
				<form:hidden path="productID" id="productID" />
				<form:hidden path="backButton" id="backButton" />
				<div class="rtContPnl floatR">

					<div class="grpTitles">
						<h1 class="mainTitle">Build Coupon</h1>
					</div>

					<div class="section">
						<div class="grdSec brdrTop">
							<table width="100%" cellspacing="0" cellpadding="0" border="0"
								class="grdTbl zeroBtmMrgn">

								<tbody>
									<tr>
										<td width="100%"><ul class="imgtxt">
												<li class="floatL"><img width="96" height="96"
													alt="buildBannerAd"
													src="/ScanSeeWeb/images/buildCoupon.png">
												</li>
												<li>
													<h3>Please fill out the information below to build</h3></li>
												<li>

													<h3>your Coupon</h3></li>
											</ul>
										</td>
									</tr>
								</tbody>
							</table>
							<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
								width="100%">
								<tbody>
									<tr>
										<td class="header" colspan="4">Create New Coupon <c:if
												test="${message ne null }">
												<span class="alertTxt-dsply leftPdng"> <c:out
														value="${message}" /> </span>
												<script>
													var PAGE_MESSAGE = true;
												</script>
											</c:if></td>
									</tr>

									<tr>
										<td width="18%" class="Label"><label for="cpnName"
											class="mand">Coupon Name</label>
										</td>

										<td width="23%"><form:errors path="couponName"
												cssStyle="color:red"></form:errors> <form:input
												path="couponName" id="cpnName" name="textfield2" type="text"
												maxlength="100" tabindex="1" /></td>
										<td class="Label"><label for="couponAmt" class="mand">Coupon
												Face Value $</label>
										</td>

										<td><form:errors path="couponDiscountAmt"
												cssStyle="color:red">
											</form:errors> <form:input path="couponDiscountAmt" id="couponAmt"
												type="text" name="couponDiscountAmt"
												onkeypress="return isNumberKeyPhone(event)" tabindex="2" />
										</td>
									</tr>
									<tr>
										<td class="Label"><label for="numofCpn" class="mand">#
												of Coupons to issue</label>
										</td>
										<td><form:errors path="numOfCouponIssue"
												cssStyle="color:red"></form:errors> <form:input
												path="numOfCouponIssue" id="numofCpn" type="text"
												name="numOfCouponIssue"
												onkeypress="return isNumberKey(event)" tabindex="3" /></td>
										<td class="Label"><label for="slctLoc" class="mand">Location(s)</label>
										</td>
										<td>
											<div>
												<form:errors path="locationID" cssStyle="color:red">
												</form:errors>
												<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
													onclick="SelectAllLocation(this.checked);" tabindex="4" />
												Select All Locations </label>
											</div> <form:select path="locationID" id="locationID"
												class="txtAreaBox" multiple="true" tabindex="5">
												<!--<form:option value="" label="--Select Location--" />-->
												<c:forEach items="${sessionScope.retailerLocList}" var="s">
													<form:option value="${s.retailerLocationID}"
														label="${s.address1}" />
												</c:forEach>
											</form:select>
											<p>Hold ctrl to select more than one location</p></td>
									</tr>
									<tr>
										<td class="Label"><label for="cpnDesc">Coupon
												Description</label>
										</td>
										<td><form:textarea path="couponLongDesc" id="cpnDesc"
												class="txtAreaSmall" rows="5" cols="45" name="textarea"
												onkeyup="checkMaxLength(this,'250');" tabindex="6"></form:textarea>
										</td>
										<td class="Label"><label for="cpnTC">Coupon Terms
												&amp; Conditions</label>
										</td>
										<td colspan="3"><form:textarea path="couponTermsCondt"
												id="cpnTC" class="txtAreaSmall" rows="5" cols="45"
												name="textarea2" onkeyup="checkMaxLength(this,'50');"
												tabindex="7"></form:textarea></td>
									</tr>


									<tr>
										<td class="Label"><label for="upldImg">Coupon
												Image</label></td>
										<td><ul class="imgInfoSplit">
											<li><label><img id="couponImg" alt="upload"
											src="${sessionScope.couponImagePath}" height="80" width="80"
											onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
												</label><span class="topPadding forceBlock"><label for="trgrUpld">
													<input type="button" value="Upload" id="trgrUpldBtn"
													width="350" height="460" class="btn trgrUpld"
													title="Upload Image File" tabindex="8"> <form:input
														path="imageFile" type="file" class="textboxBig"
														id="trgrUpld" onchange="checkCouponImgValidate(this);" />
														</label> </span>
														</li>
														<li>Suggested Minimum Size:<br>70px/70px<br>Maximum Size:800px/600px<br></li>
														<li> <br> <form:errors path="imageFile" cssStyle="color:red"></form:errors>
														<label id="couponImagePathErr"
															style="color: red; font-style: 45"></label> </li>
												</ul>
										</td>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td class="Label"><label for="csd" class="mand">Coupon
												Start Date</label>
										</td>

										<td align="left"><form:errors path="couponStartDate"
												cssStyle="color:red"></form:errors> <form:input
												path="couponStartDate" id="datepicker1" class="textboxDate"
												name="csd" tabindex="9" />(mm/dd/yyyy)</td>

										<td class="Label"><label for="ced" class="mand">Coupon
												End Date</label>
										</td>
										<td align="left"><form:errors path="couponExpireDate"
												cssStyle="color:red"></form:errors> <form:input
												path="couponExpireDate" id="datepicker2" class="textboxDate"
												name="ced" tabindex="10" />(mm/dd/yyyy)</td>
									</tr>
									<tr>
										<td class="Label"><label for="cst">Coupon Start
												Time</label></td>
										<td><form:select path="couponStartTimeHrs"
												class="slctSmall" tabindex="11">
												<form:options items="${CouponStartHrs}" />
											</form:select> Hrs <form:select path="couponStartTimeMins"
												class="slctSmall" tabindex="12">
												<form:options items="${CouponStartMin}" />
											</form:select> Mins</td>
										<td class="Label"><label for="cet">Coupon End
												Time</label></td>
										<td><form:select path="couponEndTimeHrs"
												class="slctSmall" tabindex="13">
												<form:options items="${CouponStartHrs}" />
											</form:select> Hrs <form:select path="couponEndTimeMins" class="slctSmall"
												tabindex="14">
												<form:options items="${CouponStartMin}" />
											</form:select> Mins</td>
									</tr>
									<tr>

										<td class="Label"><label for="timeZone">Time Zone</label>
										</td>
										<td colspan="3"><form:select path="timeZoneId"
												class="selecBx" tabindex="15">
												<form:option value="0" label="--Select--">Please Select Time Zone</form:option>
												<c:forEach items="${sessionScope.timeZoneslst}" var="tz">
													<form:option value="${tz.timeZoneId}"
														label="${tz.timeZoneName}" />
												</c:forEach>

											</form:select>
									</tr>
									<tr>
										<!--  <td class="Label"><label for="clmType">Claim Type</label></td>
								<td><form:select path="couponDiscountType" id="clmType"
										class="textboxBig" name="select4" tabindex="15">
										<option value="">Please Select</option>
										<option value="Print">Print</option>
										<option value="Loyalty">Loyalty</option>

										<option value="Store">Store</option>
									</form:select></td>-->
										<td><label for="couponName">Select Product</label></td>
										<td><form:input path="scanCode" type="text"
												name="textfield" id="couponName" readonly="true"
												tabindex="16" class="textboxMedium" /> <a href="#"><img
												src="/ScanSeeWeb/images/searchIcon.png" alt="Search"
												width="20" height="17" onclick="checkAssociatedCouponProd()"
												title="Click here to View Product/UPC List" /> </a></td>
										<td colspan="2">&nbsp;</td>
									</tr>
									<!--  <tr>
								<td class="Label">Do you like to integrate with your POS</td>
								<td colspan="3"><label> <form:radiobutton path="strPos" id="radio1"
										value="1"  type="radio" name="radio" tabindex="17"/>
								</label> Yes <label> <form:radiobutton path="strPos" id="radio2" value="0"
										type="radio" name="radio" tabindex="18"/> No</label>
								</td>
							</tr>-->
								</tbody>
							</table>

							<div class="navTabSec mrgnRt" align="right">

								<c:if test="${couponList ne null && ! empty couponList}">
									<input class="btn"
										onclick="location='/ScanSeeWeb/retailer/managecoupons.htm'"
										id="back" value="Back" type="button" name="Cancel3"
										title="Back" tabindex="17" />
								</c:if>
								<input class="btn" value="Preview" type="button" name="Cancel"
									title="Preview" onclick="previewCouponPopUp();" tabindex="18" />
								<input class="btn" value="Submit" type="button"
									onclick="submitCouponInfo()" name="Cancel" title="Submit"
									tabindex="19" />
							</div>
						</div>
						<div class="ifrmPopupPannel" id="ifrmPopup2" style="display: none;background-color: White">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup2','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="auto" id="ifrm2" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
						<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
			</form:form>
		</div>
	</div>
	</div>
	</div>
	<div class="clear"></div>
</body>
<script>
	onLoadRetLocationID();
</script>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {
						$("#createcouponform").attr('action', '/ScanSeeWeb/retailer/uploadcouponimg.htm');
						/*show progress bar : ETA for Web 1.3*/
						//showProgressBar();/* Commented to fix body scroll disable issue*/
						/*End*/
						$("#uploadBtn").val("trgrUpldBtn")
						$.ajaxSetup({
							cache : false
						});
						$("#createcouponform")
								.ajaxForm(
										{
											success : function(response) {
												$('#loading-image').css(
														"visibility", "hidden");
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue
												if (imgRes == 'UploadLogoMaxSize') {
													$('#couponImagePathErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#couponImagePathErr').text("Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {
													$('#couponImagePathErr')
															.text("");
													var substr = imgRes
															.split('|');
													if (substr[0] == 'ValidImageDimention') {

														$('#couponImg').width('70px');
														$('#couponImg').height('70px');

														var imgName = substr[1];
														$('#createcoupon').val(
																imgName);
														$('#couponImg').attr(
																"src",
																substr[2]);
													} else {

														/*commented to fix iframe popup scroll issue
														/$('body').css("overflow-y","hidden");*/ 
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
															100, 99.5,
																'Crop Image');
													}
												}
											}
										}).submit();
					});
</script>
</html>




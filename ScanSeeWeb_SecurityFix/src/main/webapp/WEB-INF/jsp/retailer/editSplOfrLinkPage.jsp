<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script>
	$(document).ready(function() {
		$('#splOfferLocId option').click(function() {
			var totOpt = $('#splOfferLocId option').length;
			var totOptSlctd = $('#splOfferLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
		$("#splOfferLocId").change(function() {
			var totOpt = $('#splOfferLocId option').length;
			var totOptSlctd = $('#splOfferLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
		var iconId = document.createCustomPage.imageIconID.value;
		$('.iconsPnl li a img').removeClass('active');
		$("#" + iconId).addClass("active");
		var uploadType = document.createCustomPage.imageUplType.value;
		//initially show icons panel	
		$("#" + uploadType).show();
		$('input[value="' + uploadType + '"]').attr('checked', 'checked');

		$(".upldTyp").hide();
		$("#" + uploadType).show();

		$('input[name="upldType"]').change(function() {
			var curSlctn = $(this).attr('value');
			document.createCustomPage.imageUplType.value = curSlctn;
			$(".upldTyp").hide();
			$("#" + curSlctn).show();
		});

	});

	function selectsplLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;
		var vLocVal = document.getElementById("splOfferLocId");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function selectAttchLinkLocations() {
		var vLocID = document.createCustomPage.hiddenLocId.value;
		var vLocVal = document.getElementById("splOfferAttatchLinkLocId");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function showPagePreview(pageTyp) {

		var createForm = document.createCustomPage;
		var imagePath = $("#customPageImg").attr('src')
		var retStoreName = '${sessionScope.retailStoreName}';
		var retStoreImage = '${sessionScope.retailStoreImage}';
		var specialImagepath = $("#customSplPageImg").attr('src');
		// Do validation before you call for Preview

		if (pageTyp == 'SplOffer') {

			//For specail offer page validtion

			var title = document.createCustomPage.spclOffrTitle.value;
			var locid = document.createCustomPage.splOfferAttatchLinkLocId.value;
			var retShortDesc = document.createCustomPage.spclOffrPgDesc.value;
			var retPageDesc = document.createCustomPage.spclOffrshrtDesc.value;
			var specialStartDate = document.createCustomPage.SplOffrStrtDT.value;
			var specialEndDate = document.createCustomPage.SplOffrEndDT.value;

			if (title == "") {
				alert('Please enter title');
				return;
			} else if (locid = "" || locid == '0') {
				alert('Please select location');
				return;
			} else if (retPageDesc == "") {
				alert('Please enter  page description');
				return;
			} else if (retShortDesc == "") {
				alert('Please enter  short description');
				return;
			} else if (specialStartDate == "") {
				alert('Please enter special offer start date');
				return;
			} else if (specialEndDate == "") {
				alert('Please enter special offer end date');
				return;
			} else {
				showSpecialOffPagePview(createForm, specialImagepath,
						retStoreName, retStoreImage);
			}

		}
	}
	function selectRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("splOfferLocId");
		var vCategoryList = [];
		if (vLocID != "null" && vLocID != "") {
			vCategoryList = vLocID.split(",");
		}
		if (vLocVal.length == vCategoryList.length) {
			document.getElementById('chkAllLoc').checked = true;
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for ( var j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function saveImage(imagepath) {
		document.createCustomPage.imageIconID.value = imagepath;
	}
	function SelectAllLocation(checked) {
		var sel = document.getElementById("splOfferLocId");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
</script>
<div class="shdwBg" id="content" style="min-height: 73px;">
	<%@include file="retailerLeftNavigation.jsp"%>
	<div class="grdSec rtContPnl floatR">
		<div id="secInfo">Edit Special Offer Page</div>
		<form:form commandName="createCustomPage" id="createCustomPage"
			name="createCustomPage" enctype="multipart/form-data"
			action="uploadtempretimg.htm" acceptCharset="ISO-8859-1">
			<form:hidden path="pageId" id="pageId" />
			<form:hidden path="hiddenLocId" />
			<form:hidden path="imageName" id="imageName" />
			<form:hidden path="retailerImg" id="retailerImg" />
			<form:hidden path="imageUplType" />
			<form:hidden path="imageIconID" />
			<form:hidden path="landigPageType" value="AttachLink" />
			<form:hidden path="viewName" value="editSplOfrLinkPage" />
			<input type="hidden" id="selSplOfferPageType"
				name="selSplOfferPageType">
			<input type="hidden" id="uploadBtn" name="uploadBtn">
			<input type="hidden" id="uploadedFileLength"
				name="uploadedFileLength">
			<input type="hidden" name="editSlctSpclOffr" id="editSlctSpclOffr"
				value="AttachLink" />
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="spclOffr" class="grdTbl cstmpg" style="display: table;">
				<tbody>
					<tr>
						<td class="header" colspan="4">Edit Special Offer Page</td>
					</tr>


					<tr class="AttachLink">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="atchLinkTitle" class="mand">Title</label></td>
						<td colspan="3"><form:input path="splOfferTitle" type="text"
								id="splOfferAttatchLinkTitle" /> <form:errors
								path="splOfferTitle" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr class="AttachLink" style="display: table-row;">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="label2" class="mand">URL</label>
						</td>
						<td colspan="3"><form:input path="splOfferattachLink"
								type="text" id="label" name="spclOffrAtF" /> <form:errors
								path="splOfferattachLink" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr class="AttachLink">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="spclOffrML" class="mand">Locations</label></td>
						<td colspan="1"><label> <form:select
									class="txtAreaBox" path="splOfferLocId" id="splOfferLocId"
									multiple="true">
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>
								</form:select> </label> 
								<br/>
								<form:label path="splOfferLocId">Hold Ctrl to select more than one location</form:label>
								<br/> 
								<form:errors path="splOfferLocId" cssStyle="color:red">
							</form:errors>
						</td>
						<td colspan="2" align="left" valign="top" class="Label"><label>
								<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
								onclick="SelectAllLocation(this.checked);" tabindex="2" />
								Select All Locations </label>
						</td>
					</tr>

					<tr>
						<td colspan="4" align="left" valign="top" class="Label"><strong>
								<label> <input name="upldType" type="radio"
									value="slctdUpld" checked="checked" /> </label> Select an icon to
								appear <input type="radio" name="upldType" value="usrUpld" />
								Upload your own</strong><strong> <label></label> </strong><span
							class="errNtfctn"> <form:errors path="retImage"
									cssStyle="color:red"></form:errors> </span> <span class="errNtfctn">
								<label id="customPageImgErr" style="color: red; font-style: 45"></label>
						</span>
						</td>
					</tr>


					<tr>
						<td colspan="4" align="left" valign="top"><ul
								class="iconsPnl upldTyp" id="slctdUpld">
								<c:forEach items="${sessionScope.imageList}" var="s">
									<li class="active"><a href="#"><img
											src="${s.imagePath}" alt="facebook"
											id="${s.qRRetailerCustomPageIconID}" name="retPageImg"
											onclick="javascript:saveImage('${s.qRRetailerCustomPageIconID}');" />
									</a></li>
								</c:forEach>

							</ul>
							<div class="upldTyp" id="usrUpld">


								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="brdrLsTbl">
									<tr>
										<td width="20%">
											<ul class="imgInfoSplit">
												<li><label for="hdSp"></label> <label><img
														id="customPageImg" width="80" height="80" alt="upload"
														src="${sessionScope.customPageRetImgPath}"> </label><span
													class="topPadding"><label for="trgrUpld"><input
															type="button" value="Upload" id="trgrUpldBtn"
															class="btn trgrUpld" title="Upload File" tabindex="4">
															<form:input type="file" id="trgrUpld" class="textboxBig"
																path="retImage" /> </label> </span></li>
											</ul></td>
										<td width="80%" align="left" valign="top">
											<ul>

												<li><strong>Upload Image Size:</strong></li>
												<li>Suggested Minimum Size:70px/70px</li>
												<li>Maximum Size:800px/600px</li>
												<!-- <li>Maximum Size:950px/1024px</li> -->

											</ul></td>
									</tr>
								</table>

							</div></td>
					</tr>
					<tr class="cmnActn" style="display: table-row;">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
						<td colspan="3"><input class="btn"
							onclick="javascript:history.back()" value="Back" type="button"
							name="Back" /> <input type="button"
							onclick="updateSpecialOfferPage();" value="Save" title="Save"
							class="btn" /></td>
					</tr>



				</tbody>
			</table>
			<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
						alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="no" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
		</form:form>
	</div>
</div>
<div class="clear"></div>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {
						$("#uploadBtn").val("trgrUpldBtn")
						$("#createCustomPage")
								.ajaxForm(
										{

											success : function(response) {

												//alert(response.getElementsByTagName('imageScr')[0].firstChild.nodeValue)
												//	$('#customSplPageImg')
												//		.attr(
												//			"src",
												//			response
												//			.getElementsByTagName('imageScr')[0].firstChild.nodeValue);

												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue

												if (imgRes == 'UploadLogoMaxSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {

													//$('#customPageImg').attr("src",imgRes);
													//	$('#customImg').attr("src", imgRes);
													$('#customPageImgErr')
															.text("");
													var substr = imgRes
															.split('|');

													if (substr[0] == 'ValidImageDimention') {
														var imgName = substr[1];
														$('#retailerImg').val(imgName);
														$('#customPageImg')
																.attr(
																		"src",
																		substr[2]);

													} else {
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}
											}

										}).submit();

					});
</script>
<script>
	selectRtlLocations();
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Image Crop</title>
<link href="/ScanSeeWeb/styles/style.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery.Jcrop.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="../scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="../scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="../scripts/jquery.tablescroll.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.Jcrop.min.js"></script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

</head>
<body class="whiteBG">
	<form:form name="associateFilters" commandName="associateFilters">
		<div class="cntrlPnl">
			<input type="button" id="associate" value="Associate" title="Associate" class="btn"/>
		</div>
		<div id="message">
			<center>
				Filters Not found !!!
			</center>
		</div>
		<div class="contBlock">
			<fieldset class="">
				<div class="tabularPnl" id="tabularPnl">
					
				</div>
			</fieldset>
		</div>
	</form:form>
</body>
</html>

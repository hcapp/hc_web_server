<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<script type="text/javascript">
	$(document).ready(function() {
		//Table scroll with fixed header. Check if that element exists than set the properties.
		if ($.fn.tableScroll) {
			if (document.getElementById('thetable')) {
				$('#thetable').tableScroll({
					height : 200
				});//Height for customization
			}
		}
	});
</script>


<form:form name="eventlocationform" commandName="eventlocationform"
	acceptCharset="ISO-8859-1">
	<div class="contBlock">
		<fieldset class="popUpSrch">
		
			<c:choose>
			
				<c:when test="${empty sessionScope.eventAssocLocationList}">
					<label for="Event Location">No Location Found</label>
				</c:when>
				
				<c:otherwise>
				
					<div class="grdCont searchGrd">
						<table id="thetable" class="stripeMe" border="0" cellspacing="0"
							cellpadding="0" width="100%">
							
							<thead>
								<tr class="header">
									<td width="44%">Event Locations</td>
									<td width="50%">Event Location Address</td>
								</tr>
							</thead>
							
							<tbody>
								<c:forEach items="${sessionScope.eventAssocLocationList}"
									var="arEventAssocLocationList">
									<tr>
										<td align="left"><label for="location">${arEventAssocLocationList.storeIdentification}</label></td>
										<td align="left">${arEventAssocLocationList.address1}</td>
									</tr>
								</c:forEach>
							</tbody>
							
						</table>
						
					</div>
				</c:otherwise>
				
			</c:choose>
			
		</fieldset>
	</div>
</form:form>


<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.RetailerLocation"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
	<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>

<script>
	saveRetalLocation = 'true';
	
	var changeImgDim = '${sessionScope.ChangeImageDim}';
	if (null != changeImgDim && changeImgDim == 'true') {
		$('#upldLocatnImg').width('28px');
		$('#upldLocatnImg').height('28px');
	}
</script>

<script type="text/javascript">
	function numRows() {
		var rowCount = $('#locSetUpGrd tr').length;
		if (rowCount <= 1) {
			alert("No records to save");
			return false;
		}
		var result = confirm("Do you want to save the changes");
		if (result) {
			editLocationSetUp();
		}
	}

	function locationSetupTOProductSetup() {
		document.locationsetupform.action = "showbatchuploadprod.htm";
		document.locationsetupform.method = "GET";
		document.locationsetupform.submit();
	}

	function isLatLong(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31 ||charCode == 45 || charCode == 43)
			return true;
		return false;
	}

	window.onload = function() {
		var vRetailerLocID = document.locationsetupform.retailerLocID.value;
		var vRetLocList = [];
		var objArr = [];
		var vAllLocationList = [];
		if (vRetailerLocID != "null" && vRetailerLocID != "") {
			vRetLocList = vRetailerLocID.split(",");
			for(i=0; i<vRetLocList.length ; i++){
			$("tr#" + vRetLocList[i]).removeClass("hilite").addClass("requiredVal");	
			}
		}
	}
</script>
<!--To avoid On F5 key pressed, Location upload will happen repeatedly -->
<!--<script language="JavaScript">
    function keyPressed(evt) {
        var F5 = 116;       
        if (evt.which == F5) {
            //alert("F5 pressed");
            return false;
        }
    }
    document.onkeydown=keyPressed;
  </script>
<!--to avoid On F5 key pressed, Location upload will happen repeatedly -->
<form:form name="locationsetupform" commandName="locationsetupform"
	action="/ScanSeeWeb/retailer/uploadlocationimg.htm"
	acceptCharset="ISO-8859-1" enctype="multipart/form-data">
	<form:hidden path="viewName" value="updateLocationDash"/>
	<form:hidden path="rowIndex" />
	<form:hidden path="prodJson1" />
	<div id="content" class="shdwBg">
		<%@ include file="retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR">
			<div class="grpTitles">
				<h1 class="mainTitle"><img width="36" height="36" alt="uploadLoctns" src="../images/upldLocicon.png">Upload Locations</h1>
			</div>

			<div id="bubble_tooltip">
				<div class="bubble_top">
					<span></span>
				</div>
				<div class="bubble_middle">
					<span id="bubble_tooltip_content"></span>
				</div>
				<div class="bubble_bottom"></div>
			</div>
			<div id="">


				<!--<form:hidden path="retailID" value="${item.retailID}" />
				<form:hidden path="uploadRetaillocationsID"
					value="${item.uploadRetaillocationsID}" />-->
				<form:hidden path="key" value="${sessionScope.autoGenKey}" />
				<input type="hidden" name="dashBoardFlag" id="dashBoardFlag"
					value="fromDashBoard" />
				
				<form:hidden path="prodJson" />
				<form:hidden path="pageNumber" />
				<form:hidden path="pageFlag" />
				<form:hidden path="retailerLocID" id ="retailerLocID" value="${requestScope.rejectedLocations}" />
				<div class="clear"></div>
				<div class="grdSec brdrTop">
					<h2>Choose a File to upload your Location(s) information
						below:</h2>
					<ul class="lstInfo">
						<li><input class="textboxBig" type="file"
							name="locationSetUpFile"  id="locationSetUpFile"/> <span class="instTxt nonBlk">[
								Please Upload .csv format file ]</span>
						<!--<form:errors cssStyle="${requestScope.locationuploadFile}"
								path="locationSetUpFile" />-->
								<label style="${requestScope.locationuploadFile}" id="locMsg"><c:out value="${requestScope.locationuploadMsg}" /></label>
						</li>
						<li><input class="btn" type="button" value="Upload File"
							name="Cancel2" onclick="batchUpdateLocationSetup();" /> <a
							href="/ScanSeeWeb/fileController/download.htm?TType=rlocationupload"><img
								alt="Download Template" src="../images/download.png" />&nbsp
								Download template</a></li>

						</ul>
						<h2 class="MrgnTop">2) Next, upload all of your location images. Choose your image File(s) to Upload:</h2>
						<ul class="lstInfo">
						<li><input type="file" class="textboxBig" id="btchUpldMedia"  multiple="multiple" name="imageFilePath"/> <span class="instTxt nonBlk"> [Please Upload Location(s) image(s) with following extensions : .png, .gif, .bmp, .jpg, .jpeg format file ]</span></li>
						<c:if test="${requestScope.imageUploadFile ne null && !empty requestScope.imageUploadFile && requestScope.rejectedLocations ne ''}">
						<li><label
						style="${requestScope.imageUploadFile}" id="locSuccessMsg"><c:out
								value="${requestScope.imageFilePath}" /> </label></li>
						</c:if>
                            <li><input  class="btn" value="Upload File" type="button" name="upldBtn" onclick="uploadMultipleImage();" title="Upload File"/></li>		

					<li>For upload instructions,<a href="javascript:void(0);"
						onClick="window.open('/ScanSeeWeb/html/Retailer_LocSetup_instructions.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">
							click here.</a>
					</li>
					<li>For recurring XML or API setups, please contact <a
						href="mailto:support@scansee.com?Subject=XML/API%20setup&body=I%20am%20interested%20in%20a%20XML/API%20setup.">support@scansee.com.</a>
					</li>
				</ul>
				
					<div align="center" style="font-style: 90">
						<label style="${requestScope.uploadLocation}" ><c:out
											value="${requestScope.successMSG}" /> </label>
					</div>
				 <c:choose>
						<c:when test="${requestScope.rejectedLocations ne null && requestScope.rejectedLocations ne ''}">
					
					<div class="searchGrd scrollingDashboard ">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="tblEffect" id="locSetUpGrd">
							<tr class="header">
								<!--<td width="10%" align="center"><label> <input
									type="checkbox" name="deleteAll" /> </label>
							</td>-->
								<td width="11%"><p>
										<a href="#" class="hdr-white"
											onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
											onmouseout="hideToolTip()">Store </a>
									</p>
									<p>
										<a href="#" class="hdr-white"
											onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
											onmouseout="hideToolTip()">Identification</a><a href="#"
											onmousemove="showToolTip(event,'Store Identification can either be your store number or other unique identifier.');return false"
											onmouseout="hideToolTip()"><label class="mand"><img
												alt="helpIcon" src="../images/helpIcon.png" /> </label> </a>
									</p></td>
								<td width="17%"><label class="mand">Store Address</label></td>
								<td width="17%"><label class="mand">Latitude</label></td>
								<td width="17%"><label class="mand">Longitude</label></td>
								<td width="17%"><label class="mand">City</label></td>
								<td width="10%"><label class="mand">State</label></td>
								<td width="10%"><label class="mand">Postal Code</label></td>
								<td width="20%"><label class="mand">Phone Number</label></td>
								<td width="17%">Website URL</td>
								<td width="17%">Keywords</td>
								<td width="17%">Image Upload</td>
							</tr>
							<c:set var="rowIndex" value="0"></c:set>
							<c:forEach items="${sessionScope.locationlist.locationList}"
								var="item">

								<tr id="${item.uploadRetaillocationsID}">
									<!--<td align="center"><label> <input type="checkbox"
										name="checkboxDel" /> </label>
								</td>-->
									<td><form:hidden path="uploadRetaillocationsID"
											value="${item.uploadRetaillocationsID}" /> <form:hidden
											path="retailID" value="${item.retailID}" /> <form:input
											path="storeIdentification" cssClass="textboxSmaller"
											value="${item.storeIdentification}" /></td>

									<td><form:input path="address1" cssClass="textboxSmall"
											maxlength="150" value="${item.address1}" /></td>
									<td><form:input path="retailerLocationLatitude" cssClass="textboxSmall latChk"
													value="${item.retailerLocationLatitude}" id="locLatitude"/></td>
									<td><form:input path="retailerLocationLongitude" cssClass="textboxSmall lonChk"
													value="${item.retailerLocationLongitude}" id="locLongitude"/></td>
									<td><form:input path="city" cssClass="textboxSmall"
											maxlength="50" value="${item.city}" /></td>
									<td><form:input path="state" cssClass="textboxSmall"
											maxlength="2" value="${item.state}" /></td>
									<td><form:input path="postalCode"
											cssClass="textboxSmaller" maxlength="10"
											value="${item.postalCode}"
											onkeypress="return isNumberKey(event)" /></td>
									<td><form:input path="phonenumber" cssClass="textboxSmall"
											value="${item.phonenumber}"
											onkeypress="return isNumberKey(event)" /></td>
									<td><form:input path="retailLocationUrl" maxlength="75"
											cssClass="textboxSmall" value="${item.retailLocationUrl}" />
									</td>
									<td><form:input path="keyword"
											cssClass="textboxSmall" value="${item.keyword}" />
									</td>
									
									<td><form:input path="keyword"
											cssClass="textboxSmall" value="${item.keyword}" />
									</td>
									
									<td>
											<div class="img-row">
												<!--  <span class="col"><img width="30" height="30" title="click to edit" alt="image" src="images/dfltImg.png" class="img-preview"></span>-->
												<span class="col"><img width="30" height="30"
													 alt="image"
													name="logoPreview+${item.uploadRetaillocationsID}"
													src="${item.imgLocationPath}" class="img-preview"> <form:hidden
														path="gridImgLocationPath"
														value="${item.gridImgLocationPath}" /> <form:hidden
														path="imgLocationPath" value="${item.imgLocationPath}" />
													<form:hidden path="uploadImage" value="${item.uploadImage}" />
													<span class="col"><input type="file"
														class="textboxBig" id="img${item.uploadRetaillocationsID}"
														name="imageFile"
														onChange="checkLocationImgValidate(this, ${rowIndex });"></span>
											</div>
										</td>
								</tr>
								<c:set var="rowIndex" value="${rowIndex+1}"></c:set>
							</c:forEach>

						</table>
					</div>
				</c:when>
				<c:otherwise>
					<div class="searchGrd scrollingDashboard ">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="tblEffect" id="locSetUpGrd">
							<tr class="header">
								<!--<td width="10%" align="center"><label> <input
									type="checkbox" name="deleteAll" /> </label>
							</td>-->
								<td width="11%"><p>
										<a href="#" class="hdr-white"
											onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
											onmouseout="hideToolTip()">Store </a>
									</p>
									<p>
										<a href="#" class="hdr-white"
											onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
											onmouseout="hideToolTip()">Identification</a><a href="#"
											onmousemove="showToolTip(event,'Store Identification can either be your store number or other unique identifier.');return false"
											onmouseout="hideToolTip()"><label class="mand"><img
												alt="helpIcon" src="../images/helpIcon.png" /> </label> </a>
									</p></td>
								<td width="17%"><label class="mand">Store Address</label></td>
								<td width="17%"><label class="mand">Latitude</label></td>
								<td width="17%"><label class="mand">Longitude</label></td>
								<td width="17%"><label class="mand">City</label></td>
								<td width="10%"><label class="mand">State</label></td>
								<td width="10%"><label class="mand">Postal Code</label></td>
								<td width="20%"><label class="mand">Phone Number</label></td>
								<td width="17%">Website URL</td>
								<td width="17%">Keywords</td>
								<td width="17%">Image Upload</td>
							</tr>
							<c:set var="rowIndex" value="0"></c:set>
							<c:forEach items="${sessionScope.locationlist.locationList}"
								var="item">

								<tr id="${item.uploadRetaillocationsID}">
									<!--<td align="center"><label> <input type="checkbox"
										name="checkboxDel" /> </label>
								</td>-->
									<td><form:hidden path="uploadRetaillocationsID"
											value="${item.uploadRetaillocationsID}" /> <form:hidden
											path="retailID" value="${item.retailID}" /> <form:input
											path="storeIdentification" cssClass="textboxSmaller"
											value="${item.storeIdentification}" /></td>

									<td><form:input path="address1" cssClass="textboxSmall"
											maxlength="150" value="${item.address1}" /></td>
									<td><form:input path="retailerLocationLatitude" cssClass="textboxSmall latChk"
													value="${item.retailerLocationLatitude}" id="locLatitude"/></td>
									<td><form:input path="retailerLocationLongitude" cssClass="textboxSmall lonChk"
													value="${item.retailerLocationLongitude}" id="locLongitude"/></td>
									<td><form:input path="city" cssClass="textboxSmall"
											maxlength="50" value="${item.city}" /></td>
									<td><form:input path="state" cssClass="textboxSmall"
											maxlength="2" value="${item.state}" /></td>
									<td><form:input path="postalCode"
											cssClass="textboxSmaller" maxlength="10"
											value="${item.postalCode}"
											onkeypress="return isNumberKey(event)" /></td>
									<td><form:input path="phonenumber" cssClass="textboxSmall"
											value="${item.phonenumber}"
											onkeypress="return isNumberKey(event)" /></td>
									<td><form:input path="retailLocationUrl"
											cssClass="textboxSmall" value="${item.retailLocationUrl}" />
									</td>
									<td><form:input path="keyword"
											cssClass="textboxSmall" value="${item.keyword}" />
									</td>
									
									<td>
											<div class="img-row">
												<!--  <span class="col"><img width="30" height="30" title="click to edit" alt="image" src="images/dfltImg.png" class="img-preview"></span>-->
												<form:hidden path="gridImgLocationPath"
													value="${item.gridImgLocationPath}" />
												<form:hidden path="imgLocationPath"
													value="${item.imgLocationPath}" />
												<form:hidden path="uploadImage" value="${item.uploadImage}" />
												<span class="col"><img width="30" height="30"
													 alt="image"
													name="logoPreview+${item.uploadRetaillocationsID}"
													src="${item.imgLocationPath}" class="img-preview"></span> <span
													class="col"><input type="file" class="textboxBig"
													id="img${item.uploadRetaillocationsID}" name="imageFile"
													onChange="checkLocationImgValidate(this, ${rowIndex });"></span>
											</div>
										</td>
								</tr>
									<c:set var="rowIndex" value="${rowIndex+1}"></c:set>
							</c:forEach>

						</table>
						
						<div class="ifrmPopupPannelImage" id="ifrmPopup"
								style="display: none;">
								<div class="headerIframe">
									<img src="/ScanSeeWeb/images/popupClose.png"
										class="closeIframe" alt="close"
										onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
										title="Click here to Close" align="middle" /> <span
										id="popupHeader"></span>
								</div>
								<iframe frameborder="0" scrolling="no" id="ifrm" src=""
									height="100%" allowtransparency="yes" width="100%"
									style="background-color: White"> </iframe>
							</div>
					</div>
				</c:otherwise>
				</c:choose>
					<div class="pagination">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="noBrdr" id="perpage">
								<tr>
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" enablePerPage="false" />
								</tr>
							</table>
							
							
						</div>
					<div class="navTabSec RtMrgn LtMrgn">
						<div align="right">
							<!--
         <input class="btn floatL"  type="button" value="+ Add" name="Cancel3" onclick="addNewRow()"/>
          -->
							<!--<input class="btn floatL" type="button" value="Delete"
							name="delete" onclick="delChkdRow()" /> 
							<a href="/ScanSeeWeb/retailer/regbatchuploadretprod.htm">
							<input class="btn" type="button" value="Continue" name="Continue" title="Continue" /></a>-->
							<!-- <input class="btn" onclick="window.location.href='/ScanSeeWeb/retailer/regbatchuploadretprod.htm'" value="Continue" type="button" name="Continue" title="Continue"/> -->
							<!-- 	<input class="btn" onclick="window.location.href='/ScanSeeWeb/retailer/retailerchoosePlan.htm'" value="Continue" type="button" name="Continue" title="Continue"/> -->
							<input class="btn" onclick="numRows();" type="button"
								value="Save" name="Cancel" title="Save" />

						</div>
					</div>
				</div>
				<div class="clear"></div>


			</div>
		</div>
	</div>


</form:form>

<div class="clear"></div>
<script>
$('.latChk').bind('blur',function() {
/* Matches	 90.0,-90.9,1.0,-23.343342
Non-Matches	 90, 91.0, -945.0,-90.3422309*/
	var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
			  var vLat = this.value;
			  //Validate for Latitude.
			  if (0 === vLat.length || !vLat || vLat == "") {
				return false;
			 } else {
				if(!vLatLngVal.test(vLat)) {
			      alert("Latitude are not correctly typed");
			      this.value = '';
			      return false;
			  }
			 }
});

$('.lonChk').bind('blur',function() {
/* Matches	180.0, -180.0, 98.092391
Non-Matches	181, 180, -98.0923913*/
var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
			  var vLong = this.value;
			  //Validate for Longitude.
			  if (0 === vLong.length || !vLong || vLong == "") {
				return false;
			 } else {
				if(!vLatLngVal.test(vLong)) {
			      alert("Longitude are not correctly typed");
			      this.value = '';
			      return false;
			  }
			 }
});

</script>
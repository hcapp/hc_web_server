<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.1)" />
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.1)" />
<title>ScanSee</title>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/style.css" />
<script type="text/javascript" src="scripts/global.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.gr tr td').each(function() {
			var val = $.trim($(this).text());
			if (/\s/.test(val)) {
				$(this).css("word-break", "keeep-all");
			} else {
				$(this).css("word-break", "break-all");
			}
		});
	});


</script>

<script type="text/javascript">
	function searchFundraiser()	{
		document.managefundraiserform.action = "managefundraiser.htm";
		document.managefundraiserform.method = "POST";
		document.managefundraiserform.submit();
	}
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<!-- <div id="header">
    <div class="floatL" id="header_logo"><a href="index.html"><img src="images/scansee-logo.png" alt="ScanSee" width="208" height="54"/></a></div>
    <div class="clear"></div>
  </div> -->
		<div id="content">
			<%@include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">Manage Fundraisers</h1>
				</div>
				<form:form commandName="managefundraiserform" name="managefundraiserform" acceptCharset="ISO-8859-1">
				<form:hidden path="eventID" />
				<input type="hidden" name="searchKey" />
				<div class="section">
					<div class="grdSec brdrTop">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="tranTbl">
							<tr>
								<td colspan="3" class="header">Search Fundraiser</td>
							</tr>
							<tr>
								<td width="19%" align="left"><label for="gnrlSrch">Fundraiser Name</label></td>
								<td width="61%">
									<!-- <input type="text" name="gnrlSrch" id="gnrlSrch" /> --> 
									<form:input type="text" name="fundSearchKey" path="fundSearchKey" id="gnrlSrch" />
									<a href="#">
										<img src="/ScanSeeWeb/images/searchIcon.png" alt="Search" title="Search" onclick="searchFundraiser();" width="20" height="17" />
									</a> 
									<label for="gnrlSrch"></label>
								</td>
								<td width="20%"><input type="button" class="btn"
									value="Add Fundraiser" title="Add Fundraiser"
									onclick="window.location.href='addFundraiser.htm'" /></td>
							</tr>
						</table>
						<div class="searchGrd">
							<h1 class="searchHeaderExpand">
								<a href="#" class="floatR">&nbsp;</a>Current Fundraisers
							</h1>
							<div class="grdCont tableScroll zeroPadding">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="gr">
									<tr class="header">
										<td width="16%">Fundraiser Name</td>
										<!-- <td width="18%">Category Name</td> -->
										<td width="18%">Location / Organization Hosting</td>
										<td width="16%">Start Date</td>
										<td width="20%">End Date</td>
										<td width="12%">Actions</td>
									</tr>
									
									<c:if test="${sessionScope.responseFundMsg ne null }">
										<div id="message">
											<center>
												<label style="${sessionScope.fundMessageFont}" >
													<c:out value="${sessionScope.responseFundMsg}" />
												</label>
											</center>
										</div>
										<script>var PAGE_MESSAGE = true;</script>
									</c:if>
									
									<c:if test="${sessionScope.fundraiserEventList ne null && !empty sessionScope.fundraiserEventList}">
									<tbody class="scrollContent">
										<c:forEach items='${sessionScope.fundraiserEventList.eventList}' var='item' varStatus="indexnum">
											<tr>
												<td><c:out value="${item.eventName}" /></td>
												<%-- <td><c:out value="${item.eventCategoryName}" /></td> --%>
												<td>
													<c:choose>
														<c:when test="${item.organization eq null || item.organization eq ''}">
															<label> 
																<input type="button" name="button" class="btn tgl-overlay" id="button" value="Show Location"
																title="Fundraiser Event Location" onclick="javascript:showFundraiserEventLocation(${item.eventID})"  />
															</label>
														</c:when>
														<c:otherwise>
															<c:out value="${item.organization}" />
														</c:otherwise>
													</c:choose>
												</td>
												<td><c:out value="${item.eventStartDate}" /></td>
												<td><c:out value="${item.eventEndDate}" /></td>
												<td>
													<a href="#" title="edit">
														<img width="25" height="20" src="/ScanSeeWeb/images/edit_icon.png" alt="edit" class="actn-icon" onclick="javascript:editFundraiser('${item.eventID}');">
													</a>
													<a href="#" title="delete">
														<img width="20" height="20" src="/ScanSeeWeb/images/deleteRedIcon.png" alt="delete" class="actn-icon" onclick="javascript:deleteFundraiser('${item.eventID}');">
													</a>
												</td>
											</tr>
										</c:forEach>
									</tbody>
									</c:if>
								</table>
							</div>
						</div>
						<c:if test="${sessionScope.fundraiserEventList ne null && !empty sessionScope.fundraiserEventList}">
							<div class="pagination brdrTop">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="noBrdr" id="perpage">
									 <tr>
										 <page:pageTag currentPage="${sessionScope.pagination.currentPage}" nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
												pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" enablePerPage="false" />
									</tr>
								</table>
							</div>
						</c:if>
					</div>
				</div>
				</form:form>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe" alt="close"
				onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span
				id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
			height="100%" allowtransparency="yes" width="100%"
			style="background-color: White"> </iframe>
	</div>
</body>
</body>
</html>
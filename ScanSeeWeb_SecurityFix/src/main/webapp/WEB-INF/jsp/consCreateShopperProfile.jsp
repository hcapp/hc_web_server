<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.Date.*"%>
<head>
<link href="/ScanSeeWeb/styles/consumerstyle.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/cityAutocomplete.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/zipcodeAutocomplete.js"></script>
<script>
$(document).ready(function() {
	$("#City").live("keydown",function(e){
		cityAutocomplete('pstlCd');					
	});
});
</script>
<script type="text/javascript">

	function clearShopperForm() {
		var r = confirm("Are you really want to clear the form")

		if (r == true) {
			
			document.createshopperprofileform.firstName.value = "";
			document.createshopperprofileform.lastName.value = "";
			document.createshopperprofileform.address1.value = "";
			document.createshopperprofileform.address2.value = "";
			document.createshopperprofileform.state.value = "";
			document.createshopperprofileform.stateHidden.value = "";			
			document.createshopperprofileform.stateCodeHidden.value = "";
			document.createshopperprofileform.city.value = "";
			document.createshopperprofileform.postalCode.value = "";
			document.createshopperprofileform.mobilePhone.value = "";
			document.createshopperprofileform.email.value = "";
			document.createshopperprofileform.password.value = "";
			document.createshopperprofileform.rePassword.value = "";
			document.createshopperprofileform.terms.checked = false;
			
			$(".error").each(function(){
				$(this).hide();
			});

		}

	}
	function callPreferences() {
		document.createshopperprofileform.action = "preferences.htm";
		document.createshopperprofileform.method = "POST";
		document.createshopperprofileform.submit();

	}
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
  		<div id="contWrpr">
    		<div class="block mrgnBtm">
	     		<h3 class="zeromrgnBtm">
	     			Please tell us about yourself so ScanSee can deliver savings today!
	     		</h3>
	    	</div>
	    	<div class="breadCrumb">
	      		<ul>
	        		<li class="brcIcon">
	        			<img src="../images/consumer/find_abtuIcon.png" alt="find" />
	        		</li>
	        		<li class="leftPdng">
	        			<strong>Shopper Registration Form</strong>
	        		</li>
	      		</ul>
	      		<span class="rtCrnr">&nbsp;</span> 
	      	</div>
	    	<div class="contBlks" id="">
	    	<form:form commandName="createshopperprofileform"
						name="createshopperprofileform" acceptCharset="ISO-8859-1">
						<form:hidden path="cityHidden" />
						<form:hidden path="stateHidden" id ="stateHidden"/>
						<form:hidden path="stateCodeHidden" id = "stateCodeHidden" />
						<form:hidden path="citySelectedFlag" id = "citySelectedFlag" />
	    	<table class="grdTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
	    	
	    		<tbody>	    
	    			<tr>
	    				<center><form:errors cssStyle="color:red" cssClass="error"></form:errors></center>
	    			</tr>     
	            	<tr>
	              		<td class="Label">
	              			<label for="contFName" class="mand">
	              				First Name
	              			</label>
	              		</td>
	              		<td align="left">
	              			<form:errors cssStyle="color:red" path="firstName" cssClass="error"></form:errors> 
	              			<form:input path="firstName" type="text" name="couponName3" id="rtlrName" maxlength="20" tabindex="1" />
	                  	</td>
		              	<td class="Label">
		              		<label for="contLName" class="mand">Last Name</label>
		                </td>
		              	<td align="left">
		              		<form:errors cssStyle="color:red" path="lastName" cssClass="error"></form:errors> 
		              		<form:input path="lastName" type="text" id="rtlrName" maxlength="50" tabindex="2" />
		              	</td>
	            	</tr>
		           	<tr>
		            	<td class="Label" width="17%">
		            		<label for="addrs1" class="mand" >Address 1</label>              
		            	</td>
		              	<td width="33%">
		              		<form:errors cssStyle="color:red" path="address1" cssClass="error"></form:errors> 
		              		<form:textarea path="address1"
										name="textarea2" id="addrs1" cols="45" rows="5" tabindex="3"
										class="txtAreaSmall" onkeyup="checkMaxLength(this,'50');"
										cssStyle="height:60px;"></form:textarea>
						</td>
		              	<td class="Label">
		              		<label for="addrs3">Address 2</label>              
		              	</td>
		              	<td>
		              		<form:textarea path="address2" name="textarea"
										tabindex="4" id="addrs3" cols="45" rows="5"
										class="txtAreaSmall" onkeyup="checkMaxLength(this,'50');"
										cssStyle="height:60px;"></form:textarea>             
		              	</td>
		            </tr>
            		<tr>
             	 		<td class="Label" align="left">
             	 			<label for="pCode" class="mand">
             	 				Postal Code
             	 			</label>             
             	 		</td>
              			<td align="left">
              				<form:errors cssStyle="color:red" path="postalCode" cssClass="error"></form:errors>
							<form:input path="postalCode" 
										type="text" class="loadingInput dsblContxMenu" maxlength="5" tabindex="5" name="couponName7"
										id="pstlCd" onkeypress="zipCodeAutocomplete('pstlCd');return isNumberKey(event)" 
										onchange="isNumeric(this.value);" onkeyup="isEmpty(this.value);"/>
              			</td>
              			<td class="Label">		
              				<label for="cty" class="mand">City</label>              
              			</td>
              			<td align="left">
              				<form:errors cssStyle="color:red"
										path="city" cssClass="error"></form:errors> 
							<form:input path="city" id="City" tabindex="6" class="loadingInput dsblContxMenu"/>
              			</td>
            		</tr>
            		<tr>
              			<td class="Label">
              				<label for="sts" class="mand">State</label> 
              			</td>
              			<td align="left">
              				<form:errors cssStyle="color:red" path="state" cssClass="error"></form:errors> 
              				<form:select path="state" id="Country" tabindex="7">
	              				<form:option value="">--Select--</form:option>
								<c:forEach items="${sessionScope.states}" var="s">
									<form:option value="${s.stateabbr}" label="${s.stateName}" />
								</c:forEach>
							</form:select>
              			</td>
              			<td class="Label" align="left">
              				<label for="phnNum">Phone #</label>              
              			</td>
		              	<td align="left">
		              		<form:errors cssStyle="color:red"
										path="mobilePhone" cssClass="error"></form:errors>
							<form:input	path="mobilePhone" 
										type="text" tabindex="8" name="couponName8"
										id="phnNum" onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);" />
							<label class="Label">(xxx)xxx-xxxx</label>
		              	</td>
		            </tr>
		            <tr>
		            	<td class="Label">
		            		<label for="contEml" class="mand">Contact Email</label>              
		            	</td>
		              	<td colspan="3" align="left">
		              		<form:errors cssStyle="color:red" path="email" cssClass="error"></form:errors> 
		              		<form:input	path="email" tabindex="9" 
		              				type="text" name="contactemail"	id="contEml" maxlength="100" />
		              	</td>
		            </tr>
		            <tr>
		              	<td class="Label">
		              		<label for="pswd" class="mand">Password</label>
		              	</td>
		              	<td align="left">
		              		<form:errors cssStyle="color:red" path="password" cssClass="error"></form:errors> 
		              		<form:input path="password"	name="password" 
		              					type="password" id="rtlrName" maxlength="100" tabindex="10" />
						</td>
						<td class="left" colspan="2">
							<label>Must be at least 8 characters long and have at least one number.</label>
		              	</td>
		            </tr>
		            <tr>
		              	<td class="Label">
		              		<label for="rtPswd" class="mand">Confirm Password</label>
		              	</td>
		              	<td colspan="3" align="left" class="instTxt">
		              		<form:errors cssStyle="color:red" path="rePassword" cssClass="error"></form:errors> 
		              		<form:input	path="rePassword" name="rePassword" type="password"
										id="rtlrName" maxlength="100" tabindex="11" />
		              	</td>
					</tr>
		            <tr>
		            	<td rowspan="2" class="Label">&nbsp;</td>
		              	<td colspan="3" align="left" class="instTxt">
		              		<em>
			                	<form:errors cssStyle="color:red" path="terms" cssClass="error"></form:errors> 
		            			<form:checkbox path="terms" tabindex="12" />
			                	<font color="#465259"> 
			                		<strong>Accept Terms </strong>
			                	</font>
		                	</em>
		                	<div id="acptTrms" class="mrgnTopSmall">
		                		<a href="http://www.scansee.com/links/privacypolicy.html" target="_blank" tabindex="13">
		                			<u>Privacy Policy</u>
		                		</a>
		                	</div>
		                </td>
					</tr>
		            <tr>
		              	<td colspan="3" align="left" class="instTxt">
		                	<input class="btn" value="Clear" type="button"	
		                			onclick="clearShopperForm();" name="Cancel2" 
		                			tabindex="14" title="Clear the form" /> 
							<input value="Submit" type="button"	
									onclick="validateShooperProfile();" 
									class="btn btn-success" tabindex="15" title="Submit" />                     
						</td>
		            </tr>
          		</tbody>
        	</table> 
          	</form:form>
    	</div>
  	</div>
</div>
<div class="clear"></div>
</body>
</html>

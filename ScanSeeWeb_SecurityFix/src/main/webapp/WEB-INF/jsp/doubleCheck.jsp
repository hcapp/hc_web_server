<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<script language="JavaScript">

function callShop() {
	var r = confirm("Do you want to save changes?");
		document.doublecheckinfoform.action = "shopperdashboard.htm";
		document.doublecheckinfoform.method = "POST";
		document.doublecheckinfoform.submit();
}

function loadCity() {
	// get the form values
	
	var stateCode = $('#Country1').val();
	$.ajaxSetup({cache:false})
	$.ajax({
		type : "GET",
		url : "shopperfetchcity.htm",
		data : {
			'statecode' : stateCode,
			'city' : null
		},

		success : function(response) {

			$('#myAjax').html(response);
		},
		error : function(e) {
			
		}
	});
}
var ajxRes;
function loadUniversityDisplay() {
	ajxRes=document.doublecheckinfoform.innerText.value;
	$('#myAjax1').html(ajxRes);
}

	function loadCollege() {
		var stateCode = $('#Country').val();
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/shopper/shopperfetccollege.htm",
			data : {
				'statecode' : stateCode,
				'college' : null
			},

			success : function(response) {
				$('#myAjax1').html(response);
			},
			error : function(e) {

			}
		});
	}

	function onStateSelLoad() {
		loadUniversityDisplay();
		var vStateID = document.doublecheckinfoform.stateHidden.value;
		var sel = document.getElementById("Country");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vStateID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}
	function onCollegeSelLoad() {
		var vStateID = '${sessionScope.universityId}';
		var sel = document.getElementById("College");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vStateID) {
			alert(sel.options[i].value);
				sel.options[i].selected = true;
				return;
			}
		}
	}
	
</script>
</head>
<form:form name="doublecheckinfoform" commandName="doublecheckinfoform" >
<form:hidden path="states.stateHidden" id="stateHidden" name="stateHidden"/>
	<form:hidden path="states.universityHidden" id="universityHidden" name="universityHidden"/>
	<input type="hidden" name="innerText" id="innerText"/>

		 <div id="dockPanel">
    <ul class="tabs" id="prgMtr">
    <!--  <li><a href="/ScanSeeWeb/welcome.htm" rel="home" title="Home">Home</a></li> -->

			<li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
				target="_blank" rel="About ScanSee">About ScanSee</a></li>
			
			<li><a href="/ScanSeeWeb/shopper/preferences.htm" rel="Preferences"
				title="Preferences">Preferences</a></li>
			<li><a href="/ScanSeeWeb/shopper/selectschool.htm" rel="Pick Your Charity/School"
				title="Pick Your Charity/School">Select School</a></li>
			<li><a href="/ScanSeeWeb/shopper/doublecheck.htm" rel="Double Check"
				title="Double Check" class="tabActive">Double Check</a></li>

			<li><a href="#" rel="Shop!" title="Shop!" onclick="callShop()">Shop</a></li>
    </ul>
   <!--   <a name="Mid" id="Mid"></a>
			<div id="tabdPanelDesc" class="floatR tglSec">
				<div id="filledGlass">
					<img src="/ScanSeeWeb/images/Step6.png" />
					<div id="nextNav">
						<a href="#" onclick="callShop()"><img src="../images/nextBtn.png" alt="Next"
							class="NextNav" /><span>Everything is accurate, Let's go
								shopping!</span> </a>
					</div>
				</div>

			</div>
			<div id="tabdPanel" class="floatL tglSec">
				<img src="../images/ShopperFlow1.png" alt="Shopper" />
			</div>-->
			
		</div>
		<!-- <div id="togglePnl">
			<a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down" width="9"
				height="8" /> Show Panel</a>
		</div> -->
		<div class="clear"></div>
		<div id="content" class="topMrgn">
		
		<div class="section topMrgn">
	<h5>Congratulations! You are almost there.</h5>
	<h5>Please take a look at your summary below to make sure that
		we have everything accurate.</h5>
	<h5>If you see something you would like to change,  just change it below.</h5>
					
	 <div class="subsecTitle topMrgn"><strong>About Me:</strong></div>
      <div class="grdSec brdrTop topMrgn">
      <table class="grdTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
         <tr>
				<td class="Label"><label class="mand" for="rtlrName">First Name </label>
				</td>
				<td align="left"><form:errors
						cssStyle="color:red" path="userPreferenceInfoList.firstName" cssClass="error"></form:errors>
						<form:input path="userPreferenceInfoList.firstName" type="text"
						name="couponName3" id="rtlrName" maxlength="20" tabindex="1"/>
				</td>
				<td class="Label"><label class="mand" for="rtlrName">Last Name</label>
				</td>
				<td align="left"> <form:errors cssStyle="color:red"
						path="userPreferenceInfoList.lastName" cssClass="error"></form:errors>
						<form:input path="userPreferenceInfoList.lastName" type="text"
						id="rtlrName" maxlength="50" tabindex="2"/></td>
			</tr>
			<tr>
				<td width="17%" class="Label"><label class="mand" for="addrs1">Address1</label>
				</td>
				<td width="33%"><form:errors
						cssStyle="color:red" path="userPreferenceInfoList.address1"></form:errors>
						<form:textarea path="userPreferenceInfoList.address1"
						name="textarea2" id="addrs1" cols="45" rows="5" tabindex="3"
						class="txtAreaSmall" onkeyup="checkMaxLength(this,'50');" cssStyle="height:60px;"></form:textarea> </td>
				<td class="Label">
				
				<label for="addrs3">Address2</label>
				</td>
				<td><form:textarea path="userPreferenceInfoList.address2" name="textarea" tabindex="4"
						id="addrs3" cols="45" rows="5" class="txtAreaSmall" onkeyup="checkMaxLength(this,'50');" cssStyle="height:60px;"></form:textarea>
				</td>
			</tr>

			<tr>
				<td class="Label"><label class="mand" for="state">State</label>
				</td>
				<td align="left"><form:errors
						cssStyle="color:red" path="userPreferenceInfoList.state"></form:errors>
				<form:select path="userPreferenceInfoList.state" class="selecBx"
						id="Country1" onchange="loadCity()" tabindex="5">
						<form:option value="0" label="--Select--">--Select-</form:option>
						<c:forEach items="${sessionScope.statesList}" var="s">
							<form:option value="${s.stateabbr}" label="${s.state}" />
						</c:forEach>
					</form:select>
				</td>
				<td class="Label"><label class="mand" for="cty">City</label>
				</td>
				<td align="left"><form:errors
						cssStyle="color:red" path="userPreferenceInfoList.city"></form:errors>
				<div id="myAjax">
						<form:select path="userPreferenceInfoList.city" id="City" tabindex="6">
							<form:option value="0">--Select--</form:option>
						</form:select>
					</div>
				</td>

			</tr>
			<tr>
				<td align="left" class="Label"><label class="mand" for="pstlCd">Postal
						Code</label>
				</td>
				<td align="left"><form:errors
						cssStyle="color:red" path="userPreferenceInfoList.postalCode"></form:errors>
				<form:input path="userPreferenceInfoList.postalCode" type="text" maxlength="10" tabindex="7"
						name="couponName7" id="pstlCd" onkeypress="return isNumberKey(event)" />
				</td>
				<td align="left" class="Label"><label for="phnNum">Phone
						#</label>
				</td>
				<td align="left"><form:errors cssStyle="color:red" path="userPreferenceInfoList.mobilePhone"></form:errors>
				<form:input path="userPreferenceInfoList.mobilePhone" type="text" tabindex="8"
						name="couponName8" id="phnNum" onkeyup="javascript:backspacerUP(this,event);" onkeydown="javascript:backspacerDOWN(this,event);"/><label class="Label">(xxx)xxx-xxxx</label>
				</td>
			</tr>
        </tbody>
      </table>
    
	</div>
	    </div>
		<div class="section topMrgn">
		<div class="subsecTitle"><strong>Preferences:</strong></div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
          <tr>
            <td colspan="4"><strong>Alerts Preference?</strong></td>
          </tr>
          <tr>
        
						<td width="30%"><form:checkbox path="alertpreferencesInfo.email" /> </label>  Email
						</td>
						<td width="30%"><form:checkbox path="alertpreferencesInfo.cellPhone" /> </label> Cell Phone</td>

					
        </tr>
          <tr>
            <td colspan="4"><strong>What type of things are you interested in receiveing deals on?</strong></td>
          </tr>
        </table>	
      <div class="scrollViewShpr">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd preferencesLst shprView" >
            <thead>
              <tr>
                <th align="center" class="shprHdr"><img src="/ScanSeeWeb/images/setting.png" alt="settings" width="16" height="16" /></th>
                <th colspan="2" align="left" class="shprHdr">Select Your Preferences</th>
              </tr>
            </thead>
            <tbody>
			<c:forEach items="${sessionScope.categoryDetailresponse.mainCategoryDetaillst}" var="item">
			  <tr name="${item.mainCategoryName}" class="mainCtgry">
                <td width="4%" align="center"  class="wrpWord">
			
				<input type="checkbox" />
				</td>
                <td colspan="2"  class="wrpWord">
				<c:out value ="${item.mainCategoryName}"/>
				</td>
              </tr>
			  <c:forEach items="${item.subCategoryDetailLst}" var="item1">
              <tr name="${item.mainCategoryName}">
                <td align="center" class="wrpWord">&nbsp;</td>
                <td width="4%" class="wrpWord">
				<c:choose>
				<c:when test="${item1.displayed==0}">
				<form:checkbox path="categoryDetailresponse.favCatId" value="${item1.categoryId}"/>
				</c:when>
				<c:otherwise>
				<form:checkbox path="categoryDetailresponse.favCatId" checked = "checked" value="${item1.categoryId}"/>
				</c:otherwise>
				</c:choose>
				</td>
                <td width="92%" class="wrpWord">
				 <c:out value ="${item1.subCategoryName}"/>
				</td>
              </tr>
            </c:forEach>
            </c:forEach>  
        </table>
      </div>
		</div>
		
	
		
		<div class="section topMrgn">
		<div class="subsecTitle"><strong>Giving Back:</strong></div>
		 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
		<tr>
			<td colspan="4"><strong>College that you selected</strong></td>						
		</tr>
		<tr>
						<td width="19%">State:</td>
						<td colspan="3"><form:select path="states.state" class="textboxBig"
								id="Country" onchange="loadCollege()" tabindex="1">
								<form:option value="0" label="--Select--">--Select-</form:option>
								<c:forEach items="${sessionScope.statesList}" var="s">
									<form:option value="${s.stateabbr}" label="${s.state}" />
								</c:forEach>
							</form:select></td>
					</tr>
		<tr>
		<td width="19%">College or University Name:</td>
		<td colspan="3">
		<div id="myAjax1">
		<form:select  path="states.college" id="College" tabindex="2"
			class="textboxBig">
			<form:option  value="0">--Select--</form:option>
		</form:select>
		</div></td>
			
		</tr>
			</table>
		</div>				
		<div class="navTabSec RtMrgn">
			<div align="right">

			<input name="Cancel3" value="Next" type="submit" class="btn" onclick="callShop()"/>
			</div>
		</div>
		
		</div>
	</form:form>
<script type="text/javascript">
		loadCity();
		onStateSelLoad();
		loadCollege();
		onCollegeSelLoad();
</script>
		</html>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Image Crop</title>
<link rel="stylesheet" type="text/css" href="../styles/style.css" />
<script type="text/javascript" src="../scripts/jquery-1.6.2.min.js"></script>
<script src="../scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="../scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="../scripts/global.js" type="text/javascript"></script>
<script src="../scripts/jquery.tablescroll.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery.Jcrop.css" />
<style type="text/css">
		.centeredImage
	    {
		    text-align:center;
		    margin-top:0px;
		    margin-bottom:0px;
		    margin-right:298px;
		    padding:0px;
	    }
		.image { 
				position: relative; 	
				width: 99%; /* for IE 6 */
				margin-left: auto ;
  				margin-right: auto ;
		}
		h2 { 
		   position: absolute; 		  	  
		   width: 100%; 
		}
		h2 span { 
		   color: white; 
		   font: bold 7px/7px Helvetica, Sans-Serif; 
		   letter-spacing: 2px;  
		   background: rgb(0, 0, 0); /* fallback color */
		   background: rgba(0, 0, 0, 0.0);
		   padding: 5px; 
		}
		.arrowDownImgId { 
		   color: white;	     
		   background: rgb(0, 0, 0); /* fallback color */
		   background: rgba(0, 0, 0, 0.0);
		   padding: 5px; 
		}
		h2 span.spacer {
   				padding:0 142px;
		}
		h2 span.spacer1 {   				
   				padding-left: 2pt;
		}
	</style>

<script type="text/javascript">
	var imgHt;
	var imgWd;
	var flag = true;
	$(document).ready(function() {
		imgHt = '${sessionScope.imageHt}';
		imgWd = '${sessionScope.imageWd}';
		imgHt=(imgHt/2)+30;
		imgWd=(imgWd/2)+30;
		setTimeout(cropImage, 200);	
		var topPixel = imgWd-140;
		var rightPixelValue;
		if(topPixel <= 180)
		{
			rightPixelValue = '-14px';
		}
		else
		{
			rightPixelValue = '-58px';
		}
		$('h2').css({"right": rightPixelValue});
	    $('h2').css({"top": topPixel+'px'});	
		//cropImage();
	});

	function submitCroppedImage() {
		var logoSrc = "${sessionScope.logoSrc}";
		//	alert("logoSrc : "+logoSrc);
		uploadRetLogoWithCroppedImage(xPixel, yPixel, wPixel, hPixel);
		/* var imagePath = jQuery('#retailerLogo').val();
		alert("Image Path :"+ imagePath)
		if (imagePath == '' || imagePath == 'undefined') {
			alert('Please Select Retailer Logo to upload')
			return false;
		} else {
			var validImage = validateUploadImageFileExt(imagePath);
			if (validImage) {
				uploadRetLogoWithCroppedImage(xPixel,yPixel,wPixel,hPixel);
				//uploadRetLogo();
				window.close();
			} else {
				alert('Image file format should be png')
			}
		}		 */
	}


	function showCoords(c) {
		
		if(flag)
		  {
			$('#initialText').show();
			$('#arrowDownImgId').show();
			//$('h2').css({"left": c.x-50+'px'});
		    //$('h2').css({"top": (c.y2-110)+'px'});		    
			flag = false;	
		  }
		  else
		  {
			$('#initialText').hide();
			$('#arrowDownImgId').hide();
		  }	  
		
		xPixel = c.x;
		yPixel = c.y;
		wPixel = c.w;
		hPixel = c.h;
		//var y2Pixel = c.y2;
		// jQuery('#x').val(c.x);
		// jQuery('#y').val(c.y);
		// jQuery('#x2').val(c.x2);
		// jQuery('#y2').val(c.y2);
		// jQuery('#w').val(c.w);
		// jQuery('#h').val(c.h);
	};
	function cropImage() {
		jQuery(function($) {
			$('#imgCrop').Jcrop({
				onSelect : showCoords,
				bgColor : 'black',
				bgOpacity : .2,
				minSize: [ 70, 70 ],
				setSelect : [ imgHt, imgWd, imgHt - 50, imgWd - 50 ],
				aspectRatio :1
			});
		});
	}

</script>
</head>
<body class="whiteBG">
	<div class="contBlock">
		<fieldset class="popUpSrch">

			<div class="grdCont searchGrd">

				<p align="center">
					To crop this image, drag the region below and then click "Set as
					logo" <br/> <br/>
					Click and drag the handles to resize the crop box <br/> <br/>
					<input type="button" class="btn" id="saveButton" value="Set as logo" onclick="submitCroppedImage();"/><br/>
					</p>
					
					<br/>
					<div class="image" >
						<img src='<%=session.getAttribute("croppedlogosrc")%>' id="imgCrop" />						
						<span id="initialText" class="centeredImage">
							<!-- <h2>
								<br/>
								<span class='spacer'><img src='/ScanSeeWeb/images/arrow1.png'/>
								<span style="padding-left: 12em;"><img src='/ScanSeeWeb/images/arrow11.png'/></span>
								</span>							
							</h2>	 -->					
						</span>
					</div>
					<br/>
						<p align="center" class="MrgnTop">
					 <input type="button" class="btn"	id="saveButton" value="Set as logo" onclick="submitCroppedImage();"/>
					 </p>
				
			</div>
		</fieldset>
	</div>
	<script>
		/*$(window).load(function() {
			
			//var bodyHt = $("body").height();
			//$("#ifrmPopup").css("height",bodyHt+"px!important");
			
			$('#ifrmPopup', parent.document).height($("body").height());
			$('#ifrm', parent.document).height($("body").height());
			// $("#ifrm").height($("#ifrmPopup").height());
				}); */
		$(window).load(
				function() {

					var bodyHt = $("body").height();
					var ifrmHt = $('#ifrm', parent.document).height();

					if (bodyHt > ifrmHt) {

						$('#ifrmPopup', parent.document).height(
								$("body").height());
						$('#ifrm', parent.document).height($("body").height());
					} else {

						$("body").height($('#ifrm', parent.document).height());
						$('#ifrmPopup', parent.document).height(
								$("body").height() + 20);

					}

				});
</script>
</body>
</html>

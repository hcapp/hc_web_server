<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<body>
	<div id="wrapper">

		<div id="banner">
			<img height="289" alt="Banner" src="images/scansee-banner.png"
				width="1000" usemap="#Map" border="0" />
			<map id="Map" name="Map">
				<area title="Download Free App" alt="Download Free App"
					href="welcome.htm" />
				<area href="welcome.htm" />
				<area href="welcome.htm" />
				<area href="welcome.htm" />
				<area href="welcome.htm" />
				<area alt="Sign Up" href="welcome.htm" />
				<area title="Sign Up" alt="Sign Up" href="welcome.htm" />
			</map>
			<div class="rotator">
				<ul>
					<li class="show"><a href="#"> <img height="207" alt="pic1"
							src="images/slideImg1.png" width="336" />
					</a></li>
					<li><a href="#"> <img height="207" alt="pic2"
							src="images/slideImg2.png" width="336" />
					</a></li>
					<li><a href="#"> <img height="207" alt="pic3"
							src="images/slideImg3.png" width="336" />
					</a></li>
					<li><a href="#"> <img height="207" alt="pic4"
							src="images/slideImg4.png" width="336" />
					</a></li>
					<li><a href="#"> <img height="207" alt="pic5"
							src="images/slideImg5.png" width="336" />
					</a></li>
				</ul>
			</div>
		</div>
		<div id="content">
			<div id="news_section">

				<div class="no-js" id="ticker-wrapper">
					</br>

			 <h4><a href="/ScanSeeWeb/retailer/appsite.htm" style="text-decoration: underline;">Free App Listing</a></h4>
			 
		
			 <h4><a href="/ScanSeeWeb/shopper/conshome.htm" style="text-decoration: underline;">Consumer Home</a></h4>
					<ul class="js-hidden" id="js-news">
						<li class="news-item"><a href="#">ScanSee announces new
								QR Code Generator, Free for Non-Profits! Click now for more
								information.</a></li>
						<li class="news-item"><a href="#">ScanSee supports over
								6,000,000 retail locations, do we have your product in our
								directory?</a></li>
						<li class="news-item"><a href="#">ScanSee donates 50% of
								all profits to Higher Education and Non-Profits, join us in
								making a difference today.</a></li>
						<li class="news-item"><a href="#">.</a></li>
					</ul>
				</div>
			</div>
			<div class="section btmMrgn">
				<div class="blockCont floatL">
					<div class="blockContwrap">
						<h2>
							I am a<span>Shopper</span>
						</h2>
						<div class="btnLink">
							<img height="7" alt="link" src="images/linkArrw.png" width="4" />
							<a href="shopper/createShopperProfile.htm"
								title="Click Here to View">Click Here to View</a>
						</div>
					</div>
				</div>
				<div class="blockCont floatL">
					<div class="blockContwrap">
						<h2 class="manufacturer">
							I am a<span>Supplier</span>
						</h2>
						<div class="btnLink">
							<img height="7" alt="link" src="images/linkArrw.png" width="4" />
							<a href="createProfile.htm" title="Click Here to View">Click
								Here to View</a>
						</div>
					</div>
				</div>
				<div class="blockCont floatL clrMargin">
					<div class="blockContwrap">
						<h2 class="retailer">
							I am a<span>Retailer</span>
						</h2>
						<div class="btnLink">
							<img height="7" alt="link" src="images/linkArrw.png" width="4" />
							<a href="retailer/registerretailer.htm"
								title="Click Here to View">Click Here to View</a>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div class="clear"></div>
			<div class="section">
				<div class="block homePgblock floatL">
					<h3>
						Funding Higher Education<span>Through Commerce Networking </span>
					</h3>
					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and typesetting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img height="6" alt="LinkBullet" src="images/linkArrow.png"
								width="4" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
				<div class="block homePgblock floatL">
					<h3>
						Privacy<span>Respected</span>
					</h3>
					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and typesetting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img height="6" alt="LinkBullet" src="images/linkArrow.png"
								width="4" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
				<div class="block  homePgblock floatL clrMargin">
					<p>
						<img alt="ScanSee Man" src="/ScanSeeWeb/images/scanseeMan.png"
							align="absMiddle" />100% Happiness Guarantee
					</p>
					<p>
						<img alt="Testimonial"
							src="/ScanSeeWeb/images/testimonial-img.png" />
					</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>


	</div>

</body>
</html>

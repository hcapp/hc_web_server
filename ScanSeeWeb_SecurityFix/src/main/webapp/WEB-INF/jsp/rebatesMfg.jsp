<link rel="stylesheet" type="text/css" href="styles/style.css" />
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.Rebates"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script src="scripts/web.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).keypress(function(e) {
    if(e.which == 13) {
     //  alert('You pressed enter!');
    	getRebateByName();
    }
 });
</script>
<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="leftnavigation.jsp"%>
		<div class="grdSec contDsply floatR">
			<div id="secInfo">Rebates</div>
			<form:form name="displayrebatesform" commandName="rebatessearchForm"
				acceptCharset="ISO-8859-1">
				<form:hidden path="rebateID" />
				<form:hidden path="rebproductId" name="rebproductId" />

				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="grdTbl">
					<tr>
						<td colspan="3" class="header">Search Rebates</td>
					</tr>
					<tr>
						<td class="Label" width="17%">Rebate Name</td>
						<td width="60%"><form:input path="rebName" name="rebateName"
								id="rebateName" /> <a href="#"><img
								src="images/searchIcon.png" alt="Search" width="25" height="24"
								onclick="getRebateByName();" title="Search" />
						</a></td>
						<td width="22%"><form:input path="" type="button" class="btn"
								value="Add" onclick="window.location.href='rebatesaddmfg.htm'"
								title="Add" /></td>
					</tr>
				</table>
				<div class="searchGrd">
					<h1 class="searchHeaderExpand">
						<a href="#" class="floatR">&nbsp;</a>Current Rebates
					</h1>
					<div class="grdCont">
						<div align="center" style="font-style: 45">
							<label><form:errors
									cssStyle="${requestScope.manageRbtssFont}" /> </label>
						</div>
						<!--<div align="center" style="font-style:45"><label><form:errors cssStyle="color:red"/></label></div>
					-->
						<!--<c:if test="${message ne null }">
							<div id="message">
								<center>
									<h2>
										<c:out value="${message}" />
									</h2>
								</center>
							</div>
							<script>var PAGE_MESSAGE = true;</script>
						</c:if>
					-->
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="stripeMe" id="rebateLst">
							<tbody>

								<tr class="header">
									<td width="18%">Rebates Name</td>
									<td width="15%">Rebate Amount($)</td>

									<td width="16%">Rebate Start Date</td>
									<td width="16%">Rebate End Date</td>
									<td width="22%">Actions</td>
								</tr>


								<c:if test="${listOfRebates ne null && ! empty listOfRebates}">
									<c:forEach items='${sessionScope.listOfRebates.rebatesList}'
										var='rebates'>
										<tr>
											<!--<c:set var="style" value="rowodd" />
									<c:set var="rowColor" value="" />
									<c:if test="${indexnum.index%2 eq 1}">
										<c:set var="style" value="roweven" />
									</c:if>
									<td width="3%"><c:out value='${indexnum.index+1}' />
									</td>
									-->
											<td width="18%"><a href="#"
												onclick="editRebates(<c:out value='${rebates.rebateID}' />);"
												title="Click Here to Edit"><c:out
														value='${rebates.rebName}' /> </a></td>
											<!--<td width="18%"><input type="text" onclick="editRebates(<c:out value='${rebates.rebateID}' />);" value= "${rebates.rebName}" style="border:0"></td>
									-->
											<!--<td width="18%"><c:out value='${rebates.rebName}' />
									</td>
									-->
											<td width="15%"><c:out value='${rebates.rebAmount}' />
											</td>
											<td width="16%"><c:out value='${rebates.rebStartDate}' />
											</td>
											<td width="16%"><c:out value='${rebates.rebEndDate}' />
											</td>
											<td width="22%">
												<!--    <input type="button" name="Re-Run2"  value="Re-Run"  onclick="location.href='/ScanSeeWeb/rerunrebate.htm'"  class="btn"/> -->
												<input name="Re-Run4" value="Re-Run" type="button"
												class="btn" title="ReRun"
												onclick="rerunRebates(<c:out value='${rebates.rebateID}' />);" />
												<input name="edit" value="Edit" type="button" class="btn"
												title="Edit"
												onclick="editRebates(<c:out value='${rebates.rebateID}' />);" />
											</td>
										</tr>

									</c:forEach>
								</c:if>
							</tbody>
						</table>
						<div class="pagination">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="noBrdr" id="perpage">
								<tr>
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" enablePerPage="false" />
								</tr>
							</table>
						</div>
					</div>
				</div>
			</form:form>
			<div class="clear"></div>
		</div>
	</div>
</div>



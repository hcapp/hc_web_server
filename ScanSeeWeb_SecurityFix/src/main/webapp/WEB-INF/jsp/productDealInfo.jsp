<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="java.util.*"%>
<%@page session="true"%>
<%@ page import="common.pojo.ProductVO"%>
<%@ page import="common.pojo.HotDealInfo"%>
<div id="wrapper">
  
  <div id="content" class="topMrgn separator">
	<div class="detailPnl floatL">
    	<div class="section">
    	
    		
               <div class="section topMrgn" align="center">
        	     <img src="${sessionScope.productdealinfo.hotDealImagePath}" alt="Hot Deal" onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';"/>
               </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
                  <tr class="mainTitle">
                    <tr><td>Name:
                    <c:out value="${sessionScope.productdealinfo.hotDealName}"/></td>
                    </tr>
                    <tr><td>Description:
                    <c:out value="${sessionScope.productdealinfo.productShortDescription}"/></td>
                    </tr>
                    <tr> <td>Manufacture Name:  
                         <c:out value="${sessionScope.productdealinfo.manufName}"/></td>
                    </tr>
                    <tr><td>Weight:  
                          <c:out value="${sessionScope.productdealinfo.weight}"/></td>
                    </tr>
                    <tr><td>Weight Unit:  
                          <c:out value="${sessionScope.productdealinfo.weightUnit}"/></td>
                    </tr>
                  
                </table>
				
        </div>
        
        <div id="subnav">
            <ul>
              <li><a href="#" class="active"><span>Hot Deal Details</span></a></li>
              <li><a href="#"><span>By City</span></a></li>
              <li><a href="#"><span>Categories</span></a></li>
          </ul>
          </div>
          
 				<div class="navTabSec mrgnRt" align="right">
					<div align="center">
					
						<input class="btn" type="button" name="Cancel3" value="Back"
							onclick="javascript:history.back()">
					</div>
				</div>

			</div>
         <div class="clear"></div>
    </div>
	
    <c:if test="${sessionScope.hotdealassociatedproductlist ne null && !empty sessionScope.hotdealassociatedproductlist}">
    <div class="miscPnlProduct floatR">
    <h3 class="rtpnlTitle"><img src="images/MM_HotDeals_Icon.png" width="38" height="38" align="left" />Associated Hot Deals</h3>
    <ul class="dotdLists">
   
   	<c:forEach items="${sessionScope.hotdealassociatedproductlist}" var="item">
    	
    	<li><c:out value="${item.productName}" /></li> 
       
      </c:forEach>
      </ul>
     
    
    <div class="clear"></div>
  </div>
  </c:if>
  
</div>


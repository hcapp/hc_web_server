<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>  
<%@page session="true"%>
<%@ page import="common.pojo.ManageProducts"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript">
function checkFileSize(input) {

    if (input.files && input.files[0].size > (1024 * 1024)) {
	
        alert("File too large. Max 1MB allowed.");
        input.value = null;
    }
	
}
	

</script>
<form:form name="batchform" commandName="batchUploadform" enctype="multipart/form-data">
<div id="wrapper">

<div id="dockPanel">
			<ul id="prgMtr" class="tabs">
			<!-- 	<li><a title="Home" href="#" rel="home">Home</a></li> -->

				<li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
					target="_blank" rel="About ScanSee">About ScanSee</a></li>
				<!-- <li> <a title="Create Profile" href="Manufacturer/Mfg_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->
				<li><a class="tabActive" title="Product Setup"
					href="registprodsetup.htm" rel="Product Setup">Product Setup</a></li>
				<li><a title="Choose Your Plan"
					href="choosePlan.htm" rel="Choose Your Plan">Choose
						Plan</a></li>

				<li><a title="Dashboard" href="dashboard.htm"
					rel="Dashboard">Dashboard</a></li>
			</ul>
			<div id="tabdPanelDesc" class="floatR tglSec">
				<div id="filledGlass">
					<img src="images/Step3.png" />
					<div id="nextNav">
						<a href="#" onclick="location.href='/ScanSeeWeb/choosePlan.htm'"> 
							<img class="NextNav_R" alt="Next" src="images/nextBtn.png" /> <span>Next</span>
						</a>
					</div>

				</div>
			</div>
			<div id="tabdPanel" class="floatL tglSec">
				<img alt="Flow1" src="images/Flow4_mfg.png" />
			</div>
		</div>
		<!-- <div id="togglePnl">
			<a href="#"> <img src="images/downBtn.png" alt="down" width="9"
				height="8" /> Show Panel</a>
		</div> -->
   <div id="content" class="" style="min-height: 289px;">
    <div id="subnav">
      <ul>
        <li><a href="registprodsetup.htm" title="Add Product"><span>Add Product</span></a></li>
        <li><a href="batchuploadsupplier.htm" class="active" title="Batch Upload"><span>Batch Upload</span></a></li>
        <li><a href="registprodmanageprod.htm" title="Manage Products"><span>Manage Products</span></a></li>
      </ul>
   </div>
    <div class="grdSec">
     
      <table class="tranTbl" border="0" cellspacing="0" cellpadding="0" width="100%" align="right">
        <tbody>
          <tr>
            <td class="header brdBtmZero">1) First, upload a file containing all of your product information.
              Choose a File to Upload:</td>
          </tr>
          <tr>
            <td><input type="file" class="textboxBig" id="btchUpld" name="productuploadFilePath" /><span class="instTxt nonBlk">[ Please Upload .csv format file ]</span><form:errors cssStyle="${requestScope.productuploadFile}" path="productuploadFilePath"/></td>
          </tr>
          <tr>
            <td><input  class="btn" value="Upload File" type="button"  name="upldBtn" onclick="uploadFileSupplier();" title="Upload File"/>
			<a href="/ScanSeeWeb/fileController/download.htm?TType=sproductupload"><img alt="Download Template" src="images/download.png">&nbsp Download template</a>
			</td>
          </tr>
          <tr>
            <td class="header brdBtmZero">2) Next, upload all of your product images, audio, and video files.
              Choose File(s) to Upload:</td>
          </tr>
          <tr>
            <td><input type="file" class="textboxBig" id="btchUpldMedia"  multiple="multiple" name="imageUploadFilePath"/><span class="instTxt nonBlk">[ Please Upload .png, .mp3, .mp4 format file ]</span> <form:errors cssStyle="${requestScope.imageUploadFile}" path="imageUploadFilePath"/><form:errors cssStyle="${requestScope.imageUploadFile}" path="imageUploadFilePath"/></td>
          </tr>
          <tr>
            <td><input  class="btn" value="Upload File" type="button" name="upldBtn" onclick="uploadImageSupplier();" title="Upload File"/></td>
          </tr>
          
            <tr>
            <td class="header brdBtmZero">3) Next, upload a file containing all of your attribute information.
              Choose File to Upload:</td>
          </tr>
           
          <tr>
            <td><input type="file" class="textboxBig" id="btchUpldattribute" name="attributeuploadFile" /> <form:errors cssStyle="${requestScope.attributeupload}" path="attributeuploadFile"/><span class="instTxt nonBlk">[ Please Upload .csv format file ]</span> </td>
          </tr>
          <tr>
            <td><input  class="btn" value="Upload File" type="button"  name="upldBtn" onclick="uploadAttributeSupplier();" title="Upload File"/>
			<a href="/ScanSeeWeb/fileController/download.htm?TType=sattributeupload"><img alt="Download Template" src="images/download.png">&nbsp Download template</a>
			</td>
          </tr>
          <tr>
                <td>For upload instructions, <a href="javaScript:void(0);" onClick="window.open('/ScanSeeWeb/html/Mfg_ProdSetup_instructions.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">click here.</a></td>
              </tr>
              <tr>
                <td>For frequently asked questions, <a href="javaScript:void(0);" onClick="window.open('/ScanSeeWeb/html/Mfg_ProdSetup_Faq.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">click here.</a></td>
              </tr>
              <tr>
                <td>For recurring XML or API setups, please contact <a href="mailto:support@scansee.com?Subject=XML/API%20setup&body=I%20am%20interested%20in%20a%20XML/API%20setup.">support@scansee.com</a> </td>
              </tr>

        </tbody>
      </table>
      <div class="clear"></div>
      <div class="navTabSec mrgnRt" align="right">
        <input class="btn" value="Clear" type="button" name="Cancel3" onclick="clearBacthUploadForm();" title="Clear the form"/>
        <input class="btn" value="Save" onclick="saveSupplierUploadFile();" type="button" name="Cancel" title="Save"/>
		 <input class="btn" value="Continue" onclick="location.href='/ScanSeeWeb/choosePlan.htm'" type="button" name="Cancel" title="Continue"/>
       
      </div>
    </div>
  </div>
 </div>

</form:form>

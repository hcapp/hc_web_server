<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)" />
<title>ScanSee</title>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/style.css" />
<link href="/ScanSeeWeb/styles/ticker-style.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/includes/jquery.ticker.js"
	type="text/javascript"></script>
<script type="text/javascript" src="/ScanSeeWeb/js/jquery.jqDock.js"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>

</head>
<body>

<form:form name="shopform" action="shop.htm" >
	<div id="wrapper">
		<div id="dockPanel">
			<ul class="tabs" id="prgMtr">
			<!-- 	<li><a href="/ScanSeeWeb/welcome.htm" rel="home" title="Home">Home</a></li> -->

			<li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
				target="_blank" rel="About ScanSee">About ScanSee</a></li>
			<!--<li><a href="/ScanSeeWeb/shopper/createShopperProfile.htm" rel="About You" title="About You">About
					You</a></li>
			-->
			<li><a href="/ScanSeeWeb/shopper/preferences.htm" rel="Preferences"
				title="Preferences">Preferences</a></li>
			<li><a href="/ScanSeeWeb/shopper/selectschool.htm" rel="Pick Your Charity/School"
				title="Pick Your Charity/School">Select School</a></li>
			<li><a href="/ScanSeeWeb/shopper/doublecheck.htm" rel="Double Check"
				title="Double Check">Double Check</a></li>

			<li><a href="/ScanSeeWeb/shopper/shop.htm" rel="Shop!" title="Shop!" class="tabActive"
				>Shop</a></li>
			</ul>
			<a name="Mid" id="Mid"></a>
			<div id="tabdPanelDesc" class="floatR tglSec">
				<div id="filledGlass">
					<img src="/ScanSeeWeb/images/Step7.png" />
					<div id="nextNav">
						<a href="#"><img src="/ScanSeeWeb/images/nextBtn.png" alt="Next"
							class="NextNav" /><span>Lets Shop Now</span> </a>
					</div>
				</div>

			</div>
			<div id="tabdPanel" class="floatL tglSec">
				<img src="/ScanSeeWeb/images/ShopperFlow1.png" alt="Shopper" />
			</div>
		</div>
		<!-- <div id="togglePnl">
			<a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down" width="9"
				height="8" /> Show Panel</a>
		</div> -->
		<div class="clear"></div>
		<div id="content" class="topMrgn">

			<div class="section shadowImg topMrgn">
				<h5>Thanks for registering, lets go now to your main Shopper
					Dashboard.</h5>
				<h5>We look forward to serving you!</h5>
				<p>
					<img src="/ScanSeeWeb/images/iphone.png" name="iphoneMenu" border="0"
						usemap="#iphoneMenuMap" id="iphoneMenu" />
					<map name="iphoneMenuMap" id="iphoneMenuMap">
						<area shape="rect" coords="121,269,284,299" href="shopperdashboard.htm"
							alt="Preferences" title="Preferences" />
						<area shape="rect" coords="121,239,284,269" href="shopperdashboard.htm"
							alt="WishList" title="WishList" />
						<area shape="rect" coords="121,209,284,239" href="shopperdashboard.htm"
							alt="HotDeals" title="HotDeals" />

						<area shape="rect" coords="121,178,284,208" href="shopperdashboard.htm" alt="ScanNow"
							title="ScanNow" />
						<area shape="rect" coords="121,147,284,177" href="shopperdashboard.htm"
							alt="ShoppingList" title="ShoppingList" />
						<area shape="rect" coords="121,117,284,147" href="shopperdashboard.htm"
							alt="Thislocation" title="Thislocation" />
					</map>
				</p>
			</div>
			<div class="clear"></div>
			<!-- <div class="section topMrgn">
				<div class="block floatL">

					<h3>
						Funding Higher Education<span>Through Commerce Networking </span>
					</h3>
					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and type setting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img src="/ScanSeeWeb/images/linkArrow.png" alt="LinkBullet" width="4"
								height="6" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
				<div class="block floatL">
					<h3>
						Privacy<span>Respected</span>
					</h3>

					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and typesetting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img src="/ScanSeeWeb/images/linkArrow.png" alt="LinkBullet" width="4"
								height="6" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
				<div class="block floatL clrMargin">
					<p>
						<img src="../images/scanseeMan.png" alt="ScanSee Man"
							align="absmiddle" />100% Happiness Guarantee
					</p>
					<p>
						<img src="../images/testimonial-img.png" alt="Testimonial" />
					</p>

				</div> -->
				<div class="clear"></div>
			</div>
		</div>
		<div id="footer">
			<div id="ourpartners_info">
				<div id="partners_section" class="floatL">
					<div class="section_title">Our Partners</div>

					<p>
						<img src="/ScanSeeWeb/images/scansee-partners-logo.png"
							alt="scansee_partners_logo" />
					</p>
				</div>
				<div id="followus_section" class="floatR">
					<div class="section_title">Follow Us On</div>
					<p>
						<img src="/ScanSeeWeb/images/followus-img.png" alt="followus" border="0"
							usemap="#Map2" />
						<map name="Map2" id="Map2">
							<area shape="rect" coords="125,16,161,49" href="#" />
							<area shape="rect" coords="86,16,122,49" href="#" alt="Linked in"
								title="Linked in" />

							<area shape="rect" coords="46,17,82,50" href="http://www.scansee.com/links/twitter.html" alt="twitter"
								title="twitter" />
							<area shape="rect" coords="6,16,42,49" href="http://www.scansee.com/links/facebook.html" alt="Facebook"
								title="Facebook" />
						</map>
					</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	<!-- </div> -->
</form:form>
</body>
</html>
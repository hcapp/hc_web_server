<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>



<script type="text/javascript">

var ajxRes;
function loadUniversityDisplay() {
	ajxRes=document.selectschoolform.innerText.value;
	$('#myAjax').html(ajxRes);
}

	function loadCollege() {
		var stateCode = $('#Country').val();
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/shopper/shopperfetccollege.htm",
			data : {
				'statecode' : stateCode,
				'college' : null
			},

			success : function(response) {
				$('#myAjax').html(response);
			},
			error : function(e) {

			}
		});
	}

	function onStateSelLoad() {
		loadUniversityDisplay();
		var vStateID = document.selectschoolform.stateHidden.value;
		
		var sel = document.getElementById("Country");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vStateID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}
	
</script>

<form:form name="selectschoolform" commandName="selectschoolform">
	<form:hidden path="stateHidden"/>
	<form:hidden path="universityHidden"/>
	<input type="hidden" name="innerText" id="innerText"/>

	<div id="wrapper">
		<div id="dockPanel">
			<ul class="tabs" id="prgMtr">
			 <!--  <li><a href="/ScanSeeWeb/welcome.htm" rel="home" title="Home">Home</a></li> -->

				<li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
				target="_blank" rel="About ScanSee">About ScanSee</a></li>
				<!--<li><a href="/ScanSeeWeb/shopper/createShopperProfile.htm"
					rel="About You" title="About You">About You</a>
				</li>
				-->
				<li><a href="/ScanSeeWeb/shopper/preferences.htm"
					rel="Preferences" title="Preferences">Preferences</a>
				</li>
				<li><a href="/ScanSeeWeb/shopper/selectschool.htm"
					rel="Pick Your Charity/School" title="Pick Your Charity/School"
					class="tabActive">Select School</a>
				</li>
				<li><a href="/ScanSeeWeb/shopper/doublecheck.htm"
					rel="Double Check" title="Double Check">Double Check</a>
				</li>

				<li><a href="/ScanSeeWeb/shopper/shopperdashboard.htm" rel="Shop!"
					title="Shop!">Shop</a>
				</li>
			</ul>
		<!--Commented this code for Show panel issue --Manju-->	
		<!--	<a name="Mid" id="Mid"></a>
			<div id="tabdPanelDesc" class="floatR tglSec">
				<div id="filledGlass">
					<img src="/ScanSeeWeb/images/Step5.png" />
					<div id="nextNav">
						<a href="#" onclick="schoolOnsubmit()"><img
							src="/ScanSeeWeb/images/nextBtn.png" alt="Next" class="NextNav" /><span>Next</span>
						</a>
					</div>
				</div>

			</div>
			<div id="tabdPanel" class="floatL tglSec">
				<img src="/ScanSeeWeb//images/ShopperFlow1.png" alt="Shopper" />
			</div>-->
		</div>
		<!-- <div id="togglePnl">
			<a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down" width="9"
				height="8" /> Show Panel</a>
		</div> -->
		<div class="clear"></div>
		<div id="content" class="topMrgn">

			<div class="section shadowImg topMrgn">
				<h5>At Scansee we give back.</h5>
				<h5>50% of Profits go to Higher Education. Tell us your college of choice:</h5>


				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="brdrLsTbl topMrgn">
					<tr>
						<td colspan="4"><strong>Have a college in mind?</strong>
						</td>
					</tr>



					<tr>
						<td width="19%">State:</td>
						<td colspan="3"><form:select path="state" class="textboxBig"
								id="Country" onchange="loadCollege()" tabindex="1">
								<form:option value="0" label="--Select--">--Select-</form:option>
								<c:forEach items="${sessionScope.statesList}" var="s">
									<form:option value="${s.stateabbr}" label="${s.state}" />
								</c:forEach>
							</form:select></td>
					</tr>
					<tr>
						<td width="19%">College or University Name:</td>
						<td colspan="3">
							<div id="myAjax">
								<form:select  path="college" id="College" tabindex="2"
									class="textboxBig">
									<form:option  value="0">--Select--</form:option>
								</form:select>
							</div></td>
					</tr>
				</table>

				<div class="navTabSec RtMrgn">
					<div align="right">
						<input name="Cancel3" value="Next" type="submit" class="btn"
							onclick="schoolOnsubmit()" />
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<div class="section topMrgn">
			<!-- 	<div class="block floatL">

					<h3>
						Funding Higher Education<span>Through Commerce Networking </span>
					</h3>
					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and typesetting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img src="images/linkArrow.png" alt="LinkBullet" width="4"
								height="6" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
				<div class="block floatL">
					<h3>
						Privacy<span>Respected</span>
					</h3>

					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and typesetting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img src="images/linkArrow.png" alt="LinkBullet" width="4"
								height="6" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
				<div class="block floatL clrMargin">
					<p>
						<img src="../images/scanseeMan.png" alt="ScanSee Man"
							align="absmiddle" />100% Happiness Guarantee
					</p>
					<p>
						<img src="../images/testimonial-img.png" alt="Testimonial" />
					</p>

				</div>
				<div class="clear"></div>-->
			</div>
		</div>
	</div>


</form:form>
<script>

loadCollege();onStateSelLoad();
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ScanSee</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="scripts/global.js" type="text/javascript"></script>
<script type="text/javascript">
	window.onload = function() {

	var textFlag = document.qrcode.textFlag.value;
	
		if(textFlag){
			document.getElementById('msg').style.display = 'none';
			document.qrcode.textFlag.value = false;
		}
	}
	</script>
</head>
<body class="whiteBG">

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<div id="wrapper">
  <div id="header">
    <div id="header_logo" class="floatL"> <a href="index.html"> <img alt="ScanSee" src="images/hubciti-logo_ret.png"/></a> </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
  <div class="grdSec">
    <div id="content" class="floatL">
    <form name="qrcode" id="qrcode">

      <table width="124%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
        <tr>
          <td width="9%"><input id="product" value="on" type="checkbox" name="product" checked="${prodattributes.productID}"></td>
          <td colspan="2"> <c:out value="${requestScope.productName}" /></td>
        </tr>
        <c:if test="${sessionScope.audioSize eq true}">
        
        <tr>
          <td><input id="audioQr" value="on" type="checkbox" name="audioQr" onclick="unCheckAudio()"/></td>
          <td colspan="2"> Link to Audio QR</td>
        </tr>
        <c:if test="${sessionScope.ProductMediaList ne null && ! empty sessionScope.ProductMediaList}">
	   <c:forEach items="${sessionScope.ProductMediaList}" var="prodattributes" >
	   <c:if test="${prodattributes.audioPath ne null && ! empty prodattributes.audioPath}">								
		<tr>
		<td>&nbsp;</td>
          <td width="9%"><input type="radio" name="audio_Qr" value="${prodattributes.productMediaID}" onclick="checkAudio()"/></td>								
           <td width="76%"><c:out value="${prodattributes.audioPath}" /></td>
        </tr>
          </c:if> 
	   </c:forEach>
	     </c:if> 
	     </c:if>
	     <c:if test="${sessionScope.videoSize eq true}">
        <tr>
          <td><input id="videoQr" value="on" type="checkbox" name="videoQr" onclick="unCheckVideo()"/></td>
          <td colspan="2">Link to video QR </td>
        </tr>
		<c:if test="${sessionScope.ProductMediaList ne null && ! empty sessionScope.ProductMediaList}">
	   <c:forEach items="${sessionScope.ProductMediaList}" var="prodattributes" >
	   <c:if test="${prodattributes.videoPath ne null && ! empty prodattributes.videoPath}">								
		<tr>
		<td>&nbsp;</td>
          <td width="9%"><input type="radio" name="video_Qr" value="${prodattributes.productMediaID}" onclick="checkVideo()"/></td>								
           <td width="76%"><c:out value="${prodattributes.videoPath}" /></td>
        </tr>
          </c:if> 
	   </c:forEach>
	     </c:if> 
	     </c:if>
        <tr>
          <td colspan="2">File Link</td>
		<td><label>
            <input type="text" name="fileLink" id="fileLink"  />
          </label></td>
        </tr>
         <tr>
          <td colspan="2"></td>
		<td >
		<div id="msg"><label style="color:red;" >
            <c:out value="${requestScope.message}" />
            </label>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="3"><input class="btn" onclick="generateQRCode(${requestScope.productId},'${requestScope.productNameEscapeSpcl}')" value="Next" type="button" name="next"/></td>
        </tr>
      </table>
      <input type="hidden" id="productId" name="productID"  value=""/>
      <input type="hidden" id="productName" name="productName"  value=""/>
       <input type="hidden" id="QRAudioId" name="QRAudioId"  value=""/>
        <input type="hidden" id="QRVideoId" name="QRVideoId"  value=""/>
          <input type="hidden" id="fileLink" name="fileLink"  value=""/>
           <input type="hidden" id="QRProduct" name="QRProduct"  value=""/>
		   <input type="hidden" id="textFlag" name="textFlag"  value=""/>
</form>
    </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
  <div id="footer">
    <div id="ourpartners_info">
      
    </div>
  </div>
</div>
</body>
</html>

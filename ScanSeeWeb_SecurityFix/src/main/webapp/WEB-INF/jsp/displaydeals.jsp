<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld" %>
<div class="grdSec contDsply floatR">
      <div id="secInfo">Hot Deals</div>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
        <tr>
          <td colspan="3" class="header">Search Daily Deals</td>
        </tr>
        <tr>
          <td class="Label" width="18%">HotDeal Name</td>
          <td width="59%"><input type="text" name="textfield" id="textfield" />
            <a href="#"><img src="images/searchIcon.png" alt="Search" title="Search"  width="25" height="24"/></a></td>
          <td width="23%"><input type="button" class="btn" name="addDeals" value="Add" onclick="window.location.href='dailyDealsadd_mfg.html'" title="Add Deals"/></td>
        </tr>
      </table>
      <div class="searchGrd">
        <h1 class="searchHeaderExpand"><a href="#" class="floatR">&nbsp;</a>Current Deals</h1>
        <div class="grdCont">
          <table width="800" border="0" cellspacing="0" cellpadding="0" class="stripeMe" id="dealLst">
            <tr class="header">
              <td width="25%">Deal Name</td>
              <td width="13%">Sale Price($)</td>
              <td width="20%">Deal Start Date</td>
              <td width="19%">Deal End Date</td>
              <td width="23%">Actions</td>
            </tr>
            <tr imgsrc ="images/jacket.jpeg">
              <td>Leather Jacket</td>
              <td>1200</td>
              <td>7-8-2011</td>
              <td>7-8-2011</td>
              <td><input name="Re-Run" value="Re-Run" type="button" class="btn" onclick="window.location.href='dailyDealsService2.html#101&amp;&amp;Leather Jacket&amp;&amp;1200&amp;&amp;7-8-2011&amp;&amp;8-7-2011'" title="ReRun" />
              <input name="Edit" value="Edit" type="button" class="btn" onclick="window.location.href='dailyDealsService2.html#101&amp;&amp;Leather Jacket&amp;&amp;1200&amp;&amp;7-8-2011&amp;&amp;8-7-2011'" title="Edit"/></td>
            </tr>
            <tr imgsrc ="images/rayban.jpeg">
              <td>RayBan Glasses</td>
              <td>900</td>
              <td>7-8-2011</td>
              <td>7-8-2011</td>
              <td><input name="Re-Run" value="Re-Run" type="button" class="btn" onclick="window.location.href='dailyDealsService2.html#102&amp;&amp;RayBan Glasses&amp;&amp;900&amp;&amp;7-8-2011&amp;&amp;8-7-2011'" title="ReRun"/>
              <input name="Edit" value="Edit" type="button" class="btn" onclick="window.location.href='dailyDealsService2.html#102&amp;&amp;RayBan Glasses&amp;&amp;900&amp;&amp;7-8-2011&amp;&amp;8-7-2011'" title="Edit"/></td>
            </tr>
            <tr imgsrc ="images/rayban.jpeg">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </div>
      </div>
        <!--<div class="navTabSec RtMrgn" align="right">
          <input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Cancel3"/>
        <input class="btn" onclick="window.location.href='Manufacturer/adminpage.html'" value="Preview" type="button" name="Cancel"/>
        <input class="btn" onclick="window.location.href='Manufacturer/adminpage.html'" value="Submit" type="button" name="Cancel"/>
        </div>-->
    </div>
  </div>
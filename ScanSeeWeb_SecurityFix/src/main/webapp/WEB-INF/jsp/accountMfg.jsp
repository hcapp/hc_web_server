<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.PlanInfo"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css"  href="/ScanSeeWeb/styles/bubble-tooltip.css"  media="screen" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<script type="text/javascript">


	function loadACHBank(){
		document.accountpaymentsettingform.selectedTab.value="BankPay";
		document.accountpaymentsettingform.action = "loadPaymentType.htm";
		document.accountpaymentsettingform.method = "post";
		document.accountpaymentsettingform.submit();
		}

	function loadCreditCard(){
		document.accountpaymentsettingform.selectedTab.value="CreditCardPay";
		document.accountpaymentsettingform.action = "loadPaymentType.htm";
		document.accountpaymentsettingform.method = "post";
		document.accountpaymentsettingform.submit();
		}
</script>

<form:form name="accountpaymentsettingform" commandName="accountpaymentsettingform" acceptCharset="ISO-8859-1">
<form:hidden path="selectedTab"/>

   <div id="content" class="shdwBg">
    <%@include file="leftnavigation.jsp"%>
	<div class="grdSec contDsply floatR">
      <div id="secInfo">Manage Account</div>
      <div id="subnav">
        <ul>
         <li><a href="editProfile.htm" title="Profile Settings" rel="Profile Settings"><span>Profile Settings</span></a></li>
         <li><a href="planSettingsMfg.htm"  title="Plan Settings" rel="Plan Settings"><span>Plan Settings</span></a></li>
        <li><a href=""  class="active" title="Payment Settings" rel="Payment Settings"><span>Payment Settings</span></a></li>
        </ul>
      </div> 
      
      			<c:choose>
      					<c:when test="${sessionScope.selectedTab == 'CreditCardPay'}">
						<table class="grdTbl brdrLsTbl zeroBtmMrgn" border="0" cellspacing="0" cellpadding="0" width="100%">
								<tbody>
       								 <tr>
        								<td width="20%" class="header">Select Payment Type:</td>
								        <td width="5%" class="header" ><input name="paymenttype" type="radio" value="achBank"  onclick="loadACHBank();"/></td>
								        <td width="25%" class="header"> ACH/Bank Information: </td>
								        <td width="5%" class="header"><input name="paymenttype" type="radio" value="crdtCrd" checked="checked" onclick="loadCreditCard();" /></td>	
								        <td width="45%" class="header">Credit Card:</td>
								      </tr>
    							</tbody>
							</table>
							<table class="cstmTbl" border="0" cellspacing="0" cellpadding="0"
								width="100%">
								
								
								<tbody>
									<tr class="header">
										<td colspan="4" class="editLink">Credit Card Information:</td>
									</tr>
									<tr><td width="25%">Credit Card Number:</td>
										<td width="31%"><form:input path="creditCardNumber"
												type="text" id="creditCardNumber" name="creditCardNumber"
												tabindex="1"/></td>
										<td width="18%" class="editLink">&nbsp;</td>
										<td width="26%" class="editLink">&nbsp;</td>
									</tr>
									<tr>
										<td height="37">Expiration Date:</td>
										<td><form:select path="month" style="width: 45px; height: 20px" tabindex="2">
												<form:option value="00">MM</form:option>
												<form:option value="01">01</form:option>
												<form:option value="02">02</form:option>
												<form:option value="03">03</form:option>
												<form:option value="04">04</form:option>
												<form:option value="05">05</form:option>
												<form:option value="06">06</form:option>
												<form:option value="07">07</form:option>
												<form:option value="08">08</form:option>
												<form:option value="09">09</form:option>
												<form:option value="10">10</form:option>
												<form:option value="11">11</form:option>
												<form:option value="12">12</form:option>
											</form:select> &nbsp;/&nbsp; <form:select path="year" style="width: 60px; height: 20px" tabindex="3">
												<form:option value="0000">YYYY</form:option>
												<form:option value="2012">2012</form:option>
												<form:option value="2013">2013</form:option>
												<form:option value="2014">2014</form:option>
												<form:option value="2015">2015</form:option>
												<form:option value="2016">2016</form:option>
												<form:option value="2017">2017</form:option>
												<form:option value="2018">2018</form:option>
												<form:option value="2019">2019</form:option>
												<form:option value="2020">2020</form:option>
												<form:option value="2021">2021</form:option>
												<form:option value="2022">2022</form:option>
												<form:option value="2023">2023</form:option>
												<form:option value="2024">2024</form:option>
												<form:option value="2025">2025</form:option>
											</form:select></td>
										<td>CVV:        </td>
										<td><form:input path="cVV" type="text" id="cVV" class="textboxSmaller"
												name="cVV" tabindex="4" /></td>
									</tr>
									<tr>
										<td>Cardholder Name:</td>
										<td><form:input path="cardholderName" type="text"
												id="cardholderName" name="cardholderName" tabindex="5" /> </td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
							
									<tr>
										<td>Billing Address:</td>
										<td><form:input path="billingAddress" type="text"
												id="billingAddress" name="billingAddress" tabindex="6" /></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
									
										<td>City:&nbsp;</td>
										<td><form:input path="city" type="text"
												id="city" name="city" tabindex="7" />
										  &nbsp; </td>
										<td>State:</td>
										<td><form:input path="state" type="text"
												class="textboxMedium" id="state" name="state" tabindex="8" /></td>
										<!--<form:select path="state" class="selecBx"
												id="Country" onchange="loadCity()" tabindex="7">
												<form:option value="0" label="--Select--">--Select-</form:option>
												<c:forEach items="${sessionScope.statesList}" var="s">
													<form:option value="${s.stateabbr}" label="${s.stateName}" />
												</c:forEach>
											</form:select> &nbsp;</td>-->
										
											<!--<div id="myAjax">
												<form:select path="city" id="City" tabindex="8">
													<form:option value="0">--Select--</form:option>
												</form:select>
											</div></td>-->
									</tr>
									<tr>
										<td>Zip: </td>
										<td><form:input path="zip" type="text" id="zip"
												name="zip" tabindex="9" /></td>
										<td>Phone Number:</td>
										<td><form:input path="phoneNumber" type="text"
												id="phoneNumber" name="phoneNumber" tabindex="10"
												onkeyup="javascript:backspacerUP(this,event);"
												onkeydown="javascript:backspacerDOWN(this,event);" /></td>					
									</tr>
								</tbody>
							</table>
						</c:when>
						
						<c:otherwise>
							<table class="grdTbl brdrLsTbl zeroBtmMrgn" border="0" cellspacing="0" cellpadding="0" width="100%">
								<tbody>
       								 <tr>
        								<td width="20%" class="header">Select Payment Type:</td>
								        <td width="5%" class="header" ><input name="paymenttype" type="radio" value="achBank" checked="checked" onclick="loadACHBank();"/></td>
								        <td width="25%" class="header"> ACH/Bank Information: </td>
								        <td width="5%" class="header"><input name="paymenttype" type="radio" value="crdtCrd" onclick="loadCreditCard();"/></td>	
								        <td width="45%" class="header">Credit Card:</td>
								      </tr>
    							</tbody>
							</table>

							<table class="cstmTbl" border="0" cellspacing="0" cellpadding="0"
								width="100%">
								<tbody>
									<tr class="header">
										<td colspan="5" class="editLink">ACH/Bank Information:</td>
									</tr>
									<tr>
										<td width="25%">Bank Name:</td>
										<td width="31%"><form:input path="bankName" type="text"
												id="bankName" name="bankName" maxlength="100" tabindex="1" /> </td>
										<td width="18%" class="editLink">&nbsp;</td>
										<td width="26%" class="editLink">&nbsp;</td>
									</tr>
									<tr>
										<td>Account Type:</td>
											<td><form:select path="accountType" class="selecBx"
												id="accountType" tabindex="2">
												<form:option value="0" label="--Select--">--Select--</form:option>
												<c:forEach items="${sessionScope.accountTypes}" var="a">
													<form:option value="${a.accountTypeID}"
														label="${a.accountType}" />
												</c:forEach>
											</form:select> </td>
											<td></td>
											<td>&nbsp;</td>
									</tr>
									<tr>
										<td>Routing Number:</td>
										<td><form:input path="routingNumber" type="text"
												id="routingNumber" name="routingNumber" tabindex="3" /></td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Account Number:</td>
										<td><form:input path="accountNumber" type="text"
												id="accountNumber" name="accountNumber" tabindex="4" /></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>

									<tr>
										<td>Account Holder Name:</td>
										<td><form:input path="accountHolderName" type="text"
												id="accountHolderName" name="accountHolderName" tabindex="5" />
										</td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>Billing Address:</td>
										<td><form:input path="billingAddress" type="text"
												id="billingAddress" name="billingAddress" tabindex="6" /></td>
										<td></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
									
										<td>City:&nbsp;</td>
										<td><form:input path="city" type="text"
												id="city" name="city" tabindex="7" /></td>
										<td>State:</td>
										<td><form:input path="state" type="text"
												class="textboxMedium" id="state" name="state" tabindex="8" />
										</td>
										<!--<form:select path="state" class="selecBx"
												id="Country" onchange="loadCity()" tabindex="7">
												<form:option value="0" label="--Select--">--Select-</form:option>
												<c:forEach items="${sessionScope.statesList}" var="s">
													<form:option value="${s.stateabbr}" label="${s.stateName}" />
												</c:forEach>
											</form:select> &nbsp;</td>-->
										
											<!--<div id="myAjax">
												<form:select path="city" id="City" tabindex="8">
													<form:option value="0">--Select--</form:option>
												</form:select>
											</div></td>-->
									</tr>
									<tr>
										<td>Zip:</td>
										<td><form:input path="zip" type="text" id="zip"
												name="zip" tabindex="9" /></td>
										<td>Phone Number:</td>
										<td><form:input path="phoneNumber" type="text"
												id="phoneNumber" name="phoneNumber" tabindex="10"
												onkeyup="javascript:backspacerUP(this,event);"
												onkeydown="javascript:backspacerDOWN(this,event);" /></td>
										
										<td>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</c:otherwise>
				</c:choose>
    	
    	
    	<div class="navTabSec RtMrgn">
						<div align="right">
							<input class="btn" onclick="" value="Process Payment"
								type="button" name="Cancel3" title="Process Payment" /> 
							<input class="btn" onclick="window.location.href='dashboard.htm'"  
								value="Continue" type="button" name="Cancel3" title="Continue" />
						</div>
		</div>
</div></div>
</form:form>
  <div class="clear"></div>
 

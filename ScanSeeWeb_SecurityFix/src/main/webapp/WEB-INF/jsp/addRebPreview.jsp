<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ScanSee</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
</head>
<body style="background:#FFFFFF">

<script type="text/javascript">
$(function() {
    //Get actual content and display as title on mouse hover on it.
    var actText = $("td.genTitle").text();
    $("td.genTitle").attr('title',actText);
    var limit = 20;// character limit restricted to 20
    var chars = $("td.genTitle").text(); 
    if (chars.length > limit) {
        var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
        var dots = $("<span class='dots'>...</span>");
        $("td.genTitle").empty()
            .append(visibleArea)
            .append(dots);//append trailing dots to the visibile part 
    }                            
});
</script>

<div id="iphonePanel">
<form:form name="previewsuppladdrebform" commandName="previewsuppladdrebform">
	<form:hidden path="locationHidden" />
     <div class="navBar iphone">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
            <tr>
              <td width="19%"><img src="/ScanSeeWeb/images/backBtn.png" alt="back" width="50" height="30" /></td>
              <td width="54%" class="genTitle">${sessionScope.addRebateName}</td>
              <td width="27%"><img src="/ScanSeeWeb/images/mainMenuBtn.png" alt="mainmenu" width="78" height="30" /></td>
            </tr>
          </table>
    </div>

  <div class="previewAreaScroll">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="zeroBrdrTbl">
      <tr class="noBg">
        <td colspan="2" align="center"><img src="${sessionScope.prdAddImage}" alt="Image" width="105" height="73" /></td>
      </tr>
      <tr>
        <td colspan="2" align="left" class="iphoneTitle">PRODUCTS:</td>
      </tr>
       <tr>
        <td width="4%" align="center">&nbsp;</td>
        <td width="96%" class="wrpWord"><span>${sessionScope.addProductName}</span></td>
      </tr>
	   <tr>
        <td colspan="2" align="left" class="iphoneTitle">DETAILS:</td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
        <td class="wrpWord"><p class="description">${sessionScope.addRebateDescription}</p></td>
      </tr>
    </table>
	<p align="center"><img src="/ScanSeeWeb/images/CPN_galleryBg.png" alt="CouponRedeem" width="303" height="144" /></p>
   </div>
  </form:form>
  <div id="TabBarIphone">
    <ul id="iphoneTab">
      <li><img src="/ScanSeeWeb/images/tab_btn_up_wishlist.png" alt="wish list" width="80" height="50" /></li>
	  <li><img src="/ScanSeeWeb/images/tab_btn_up_galleryAdd.png" alt="twitter" width="80" height="50" /></li>
      <li><img src="/ScanSeeWeb/images/tab_btn_up_cpnGlry.png" alt="sendtext" width="80" height="50" /></li>
      <li><img src="/ScanSeeWeb/images/tab_btn_up_rateshare.png" alt="twitter" width="80" height="50" /></li>
    </ul>
  </div>      
  
 </div>
        <div class="clear"></div>
		<div align="center">
			<input class="btn" onclick="javascript:history.back()" type="button" value="Back" name="Cancel2" title="Back"/>
		</div>
</body>

</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/autocomplete.js"></script>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>
<script>
$(document).ready(function() {
	$("#City").live("keydown",function(e)
			{
				var s = String.fromCharCode(e.which);
				if (s.match(/[a-zA-Z0-9\.]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#pCode').val("");
				   cityAutocomplete('pCode');
				}else if(s.match(/[\b]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#pCode').val("");
					cityAutocomplete('pCode');
				}
							
			});
var stateCode = '';
var state = '';
if($('#pCode').val().length == 5){
	stateCode = $('#stateCodeHidden').val();
	state = $('#stateHidden').val();
	$( "#Country" ).val(state);				 
	}else{
	$('#stateCodeHidden').val("");
	$('#stateHidden').val("");
	$( "#Country" ).val("");
	}
});
</script>
<script type="text/javascript">
	/*function loadCity() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		var selCity = document.editprofileform.cityHidden.value;

		$
				.ajax({
					type : "GET",
					url : "/ScanSeeWeb/retailer/displayselectedcity.htm",
					data : {
						'statecode' : stateCode
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.editprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						if (cityList.length > 1) {
							document.editprofileform.City.options.length = cityList.length + 1;
							for ( var i = 0; i < cityList.length; i++) {
								cityDropDown.options[optIndex].value = cityList[i];
								cityDropDown.options[optIndex].text = cityList[i];
								optIndex++;
							}
						}

						//$('#myAjax').html(response);
						onCitySelectedLoad();
					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}
	function onChangeState() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		document.editprofileform.cityHidden.value="";
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/retailer/retailerfetchcity.htm",
			data : {
				'statecode' : stateCode,
				'city' : ""
			},

			success : function(response) {
				var responseJSON = JSON.parse(response);
				var cityStr = responseJSON.City;
				var cityList = cityStr.split(",");
				document.editprofileform.City.options.length = cityList.length;
				cityDropDown.options[0].value = '0';
				cityDropDown.options[0].text = '--Select--';
				cityDropDown.options[0].selected = true;

				for ( var i = 0; i < cityList.length; i++) {
					cityDropDown.options[optIndex].value = cityList[i];
					cityDropDown.options[optIndex].text = cityList[i];
					optIndex++;

				}

			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}*/

	function onLoad() {
		var vCategoryID = document.editprofileform.bCategoryHidden.value;
		var vCategoryVal = document.getElementById("bCategory");
		if (vCategoryID != "null") {
			var vCategoryList = vCategoryID.split(",");
		}

		for ( var i = 0; i < vCategoryVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {

				if (vCategoryVal.options[i].value == vCategoryList[j]) {
					vCategoryVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function getCityTrigger(val) {		
		if(val == ""){
			$( "#Country" ).val("");
			$( "#pCode" ).val( "" );
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
	}
	function clearOnCityChange() {	
		//alert($('#citySelectedFlag').val())
		if($('#citySelectedFlag').val() != 'selected'){
		$( "#Country" ).val("");
		$( "#pCode" ).val( "" );
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
		}else{
		$('#citySelectedFlag').val('');
		}
}
	function isEmpty(vNum) {
		if(vNum.length < 5){
			//$( "#state" ).val("");
			$( "#Country" ).val("");
			$( "#City" ).val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		return true;
	}

	/*function onCitySelectedLoad() {
		var vCityID = document.editprofileform.cityHidden.value;
		var sel = document.getElementById("City");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCityID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}*/
</script>
<div id="content" class="shdwBg">
	<%@include file="leftnavigation.jsp"%>
	<div class="grdSec contDsply floatR">
		<div id="secInfo">Update Profile</div>
		<div id="subnav">
			<ul>
				<li><a href="" class="active" title="Profile Settings"
					rel="Profile Settings"><span>Profile Settings</span>
				</a>
				</li>
				<li><a href="planSettingsMfg.htm" title="Plan Settings"
					rel="Plan Settings"><span>Plan Settings</span>
				</a>
				</li>
				<li><a href="accountPaymentMfg.htm" title="Payment Settings"
					rel="Payment Settings"><span>Payment Settings</span>
				</a>
				</li>
			</ul>
		</div>
		<form:form name="editprofileform" commandName="editprofileform"
			acceptCharset="ISO-8859-1">
			<div id="bubble_tooltip">
				<div class="bubble_top">
					<span></span>
				</div>
				<div class="bubble_middle">
					<span id="bubble_tooltip_content">Content is comming here as
						you probably can see.Content is comming here as you probably can
						see.</span>
				</div>
				<div class="bubble_bottom"></div>
			</div>
			<form:hidden path="cityHidden" />
			<form:hidden path="bCategoryHidden" />
			<form:hidden path="stateHidden" id ="stateHidden"/>
			<form:hidden path="stateCodeHidden" id = "stateCodeHidden" />
			<form:hidden path="citySelectedFlag" id = "citySelectedFlag" />
			<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
				width="100%">
				<tbody>
					<tr>
						<td class="header" colspan="1">Profile Settings</td>
						<td colspan="3" style="color: red;"><form:errors
									cssStyle="color:red"></form:errors></td>
					</tr>
					<tr>
						<td class="Label"><label for="rtlrName" class="mand">Supplier
								Name</label></td>
						<td colspan="3"><form:errors cssStyle="color:red"
								path="companyName"></form:errors> <form:input path="companyName"
								type="text" id="rtlrName" tabindex="1" />
						</td>
					</tr>
					<tr>
						<td class="Label" width="17%"><label for="addrs1"
							class="mand">Corporate Address</label></td>
						<td width="33%"><form:errors cssStyle="color:red"
								path="corporateAddress">
							</form:errors>
							<form:textarea rows="5" cols="45" path="corporateAddress"
								id="addrs1" onkeydown="checkMaxLength(this,'50');" tabindex="2"
								cssStyle="height:60px;" />
						</td>
						<td class="Label"><label for="addrs3">Address 2</label></td>
						<td><form:textarea rows="5" cols="45" path="address"
								id="addrs1" onkeyup="checkMaxLength(this,'50');" tabindex="3"
								cssStyle="height:60px;" /></td>
					</tr>
							<tr>
								<td class="Label">
									<label for="pCode" class="mand">Postal Code</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="postalCode"></form:errors>
									<form:input id="pCode" tabindex="4" path="postalCode" class="loadingInput"
										maxlength="5" onkeypress="zipCodeAutocomplete('pCode');return isNumberKey(event);"
										onchange="isNumeric(this.value);" onkeyup="isEmpty(this.value);"/>
								</td>
								<form:hidden path="tabIndex" value="5" id="tabindex" />
								<td class="Label">
									<label for="cty" class="mand">City</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="city"></form:errors> 
									<form:input path="city" id="City" tabindex="5" class="loadingInput"/>
								</td>
							</tr>
							<tr>
								<td class="Label" align="left">
									<label for="sts" class="mand">State</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="state"></form:errors> 
									<form:input path="state" id="Country" tabindex="6" disabled="true"/>
								</td>
						<td class="Label" align="left"><label for="phnNum"
							class="mand">Corporate Phone #</label></td>
						<td align="left"><form:errors cssStyle="color:red"
								path="phone"></form:errors> <form:input id="phnNum" path="phone"
								tabindex="7" onkeyup="javascript:backspacerUP(this,event);"
								onkeydown="javascript:backspacerDOWN(this,event);" /><label
							class="label">(xxx)xxx-xxxx</label></td>
					</tr>

					<tr>
						<td class="Label" align="left"><label for="bcategory"
							class="mand">Business Category</label></td>
						<td align="left"><form:errors cssStyle="color:red"
								path="bCategory"></form:errors> <form:select path="bCategory"
								id="bCategory" class="txtAreaBox" size="1" multiple="true"
								tabindex="8">
								<c:forEach items="${sessionScope.categoryList}" var="c">
									<form:option value="${c.categoryID}"
										label="${c.parentSubCategory}" />
								</c:forEach>
							</form:select></td>
						<td class="Label" align="left"><label for="nonprofit">
								<!--Non-profit Status</label>-->
						</td>
						<td align="left">
							<!--  <form:errors cssStyle="color:red" path="nonProfit"></form:errors>
					              <form:checkbox  path="nonProfit"  tabindex="9" id="nonProfit"/>
					                My organization is a registered and recognized<br />
					                501(c)(3) non-profit. </td>-->
					</tr>
					<tr>
						<td class="Label" colspan="4">&nbsp;</td>
					</tr>

					<!-- <tr>
							<td class="Label" colspan="4"><a href="#"
									onmousemove="showToolTip(event,'The name of the person that has given you authority to act.');return false"
									onmouseout="hideToolTip()"> Legal Authority Name <img
										alt="helpIcon" src="images/helpIcon.png"> </a></td>
							</tr>
							<tr>
								<td class="Label"><label for="lFName" class="mand">First
										Name</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red" path="firstName"></form:errors>
								<form:input path="firstName" id="lFName" maxlength="20"   tabindex="10"/>
								</td>
								<td class="Label" align="left"><label for="llName"
									class="mand">Last Name</label>
								</td>
								<td align="left"> <form:errors cssStyle="color:red" path="lastName"></form:errors>
								<form:input path="lastName" id="llName"
										value="" maxlength="30"  tabindex="11"/></td>
							</tr>-->
					<tr>
						<td class="Label" colspan="4"><a href="#">Contact Details</a>
						</td>
					</tr>
					<tr>
						<td class="Label"><label for="contactFName" class="mand">First
								Name</label>
						</td>
						<td align="left"><form:errors cssStyle="color:red"
								path="contactFName"></form:errors> <form:input
								path="contactFName" id="contFName" maxlength="30" tabindex="9" />
						</td>
						<td class="Label"><label for="contLName" class="mand" />Last
							Name</td>
						<td align="left"><form:errors cssStyle="color:red"
								path="contactLName"></form:errors> <form:input
								path="contactLName" id="lname" maxlength="30" tabindex="10" /></td>
					</tr>
					<tr>
						<td class="Label" align="left"><label for="contPhNm"
							class="mand">Contact Phone #</label></td>
						<td align="left"><form:errors cssStyle="color:red"
								path="contactPhone"></form:errors> <form:input
								path="contactPhone" id="contPhNm" tabindex="11"
								onkeyup="javascript:backspacerUP(this,event);"
								onkeydown="javascript:backspacerDOWN(this,event);" /><label
							class="label">(xxx)xxx-xxxx</label></td>
						<td class="Label" align="left"><label for="contPhNm"
							class="mand">Contact Email #</label></td>
						<td align="left"><form:errors cssStyle="color:red"
								path="email"></form:errors> <form:input path="email"
								id="contPhNm" maxlength="100" tabindex="12" readonly="true" /></td>
					</tr>
					<tr>
						<td colspan="4" align="right"><em> <input class="btn"
								value="Update" type="submit" name="Update" tabindex="13"
								title="Update" /> </em></td>
					</tr>
				</tbody>
			</table>
		</form:form>
	</div>
</div>

<script type="text/javascript">
	var cityID = document.editprofileform.cityHidden.value;
	var CategoryID = document.editprofileform.bCategoryHidden.value;

	onLoad();
	/*loadCityDD();
	function loadCityDD() {
		loadCity();
	}*/
</script>

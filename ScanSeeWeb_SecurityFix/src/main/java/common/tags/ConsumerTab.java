package common.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.pojo.Users;
import common.util.Utility;

public class ConsumerTab extends SimpleTagSupport
{
	// this tag will take module name which user has clicked and based on value
	// it will form the html and return.
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(PaginationTagHandler.class);

	public static final String HOME = "Home";
	public static final String FIND = "Find";
	public static final String HOTDEALS = "Hot Deals";
	public static final String Gallery = "My Gallery";
	public static final String ONLINECODE = "Online Codes";
	public static final String SL = "Shopping List";
	public static final String WL = "Wish List";
	Users loginUser = null;
	String[][] modulesDetails = new String[][] { { HOME, "conshome.htm" }, { FIND, "consfindhome.htm" }, { HOTDEALS, "consdisplayhotdeals.htm" },
			{ Gallery, "consmygallery.htm" }, { SL, "consdisplayslprod.htm" }, { WL, "conswishlisthome.htm" } };

	/**
	 * This variable holds selects menu name.
	 */

	public String menuTitle = null;

	/**
	 * @return the menuTitle
	 */
	public String getMenuTitle()
	{
		return menuTitle;
	}

	/**
	 * @param menuTitle
	 *            the menuTitle to set
	 */
	public void setMenuTitle(String menuTitle)
	{
		this.menuTitle = menuTitle;
	}

	@Override
	public void doTag() throws JspException, IOException
	{
		LOG.info("Inside ConsumerTab tag handler : doTag ");
		PageContext pageContext = (PageContext) getJspContext();
		JspWriter out = pageContext.getOut();
		loginUser = (Users) pageContext.getSession().getAttribute("loginuser");
		StringBuffer menuStrBuffer = new StringBuffer("<ul class=\"tabs\">");
		if (Utility.checkNull(this.menuTitle) != "")
		{
			/*
			 * if (HOME.equals(this.menuTitle)) {
			 */
			menuStrBuffer.append(getMenuItem());
			menuStrBuffer.append("</ul>");
			LOG.info("Pagination tag body :: " + menuStrBuffer.toString());
			out.write(menuStrBuffer.toString());

			// }
		}
		else
		{
			LOG.error("No Menu items Provided");

		}

	}

	public String getMenuItem()
	{
		StringBuffer itemStrBuffer = new StringBuffer();
		for (int i = 0; i < modulesDetails.length; i++)
		{

			if (menuTitle.equals(modulesDetails[i][0]))
			{
				if (modulesDetails[i][0].equals("Shopping List") || modulesDetails[i][0].equals("Wish List"))
				{
					if (loginUser == null || "".equals(loginUser))
					{
						itemStrBuffer.append("<li class=\"disable\" title=\"please log in or sign up - it's free \" >");
						
					}
					else
					{
						itemStrBuffer.append("<li class=\"active\">");
					}
				}
				else
				{
					itemStrBuffer.append("<li class=\"active\">");
				}
			}
			else
			{
				if (modulesDetails[i][0].equals("Shopping List") || modulesDetails[i][0].equals("Wish List"))
				{
					if (loginUser == null || "".equals(loginUser))
					{
						itemStrBuffer.append("<li class=\"disable\" title=\"please log in or sign up - it's free \" >");
						
					}
					else
					{
						itemStrBuffer.append("<li>");
					}
				}
				else
				{
					itemStrBuffer.append("<li>");
				}
			}
			if (modulesDetails[i][0].equals("Shopping List") || modulesDetails[i][0].equals("Wish List"))
			{
				if (loginUser == null || "".equals(loginUser))
				{
					itemStrBuffer.append("<a href=\"javascript:void(0);\" onclick=\"shopwlhome()\">");
					itemStrBuffer.append(modulesDetails[i][0]);
					itemStrBuffer.append("</a></li>");
				}else
				{
					itemStrBuffer.append("<a href=\"" + modulesDetails[i][1] + "\">");
					itemStrBuffer.append(modulesDetails[i][0]);
					itemStrBuffer.append("</a></li>");
					
				}
			}else{
			itemStrBuffer.append("<a href=\"" + modulesDetails[i][1] + "\">");
			// itemStrBuffer.append("<img alt=\"find\" src=\"/ScanSeeWeb/images/consumer/"
			// + modulesDetails[i][2] + "\"/>");
			itemStrBuffer.append(modulesDetails[i][0]);
			itemStrBuffer.append("</a></li>");
			}
		}
		return itemStrBuffer.toString();
	}

	 public static void main(String a[]) { ConsumerTab c = new ConsumerTab();
	  c.getMenuItem(); }
	 
}

package common.pojo;

public class LoginVO {
	
    private String fbUserId;
	private String userName;
	private String password;
	private String confrmPassword;
	private String newPassword;
	private String changePwd;
	private String linkTab;
	
	public String getFbUserId() {
		return fbUserId;
	}
	public void setFbUserId(String fbUserId) {
		this.fbUserId = fbUserId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the confrmPassword
	 */
	public String getConfrmPassword() {
		return confrmPassword;
	}
	/**
	 * @param confrmPassword the confrmPassword to set
	 */
	public void setConfrmPassword(String confrmPassword) {
		this.confrmPassword = confrmPassword;
	}
	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}
	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	/**
	 * @return the changePwd
	 */
	public String getChangePwd() {
		return changePwd;
	}
	/**
	 * @param changePwd the changePwd to set
	 */
	public void setChangePwd(String changePwd) {
		this.changePwd = changePwd;
	}

	/**
	 * @return the linkTab
	 */
	public String getLinkTab()
	{
		return linkTab;
	}
	/**
	 * @param linkTab the linkTab to set
	 */
	public void setLinkTab(String linkTab)
	{
		this.linkTab = linkTab;
	}
}

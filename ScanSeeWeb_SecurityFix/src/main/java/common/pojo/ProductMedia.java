/**
 * 
 */
package common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class ProductMedia {
	/**
	 * 
	 */
	private long productID;
	/**
	 * 
	 */
	private long productMediaID;
	/**
	 * 
	 */
	private long productMediatypeID;
	
	/**
	 * 
	 */
	private String productMediaPath;
	/**
	 * 
	 */
	private Product product;
	/**
	 * 
	 */
	public ProductMedia() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param productMediaID
	 * @param productMediatypeID
	 * @param productMediaPath
	 * @param product
	 */
	public ProductMedia(long productID,long productMediaID, long productMediatypeID,
			String productMediaPath, Product product) {
		super();
		this.productMediaID = productMediaID;
		this.productMediatypeID = productMediatypeID;
		this.productMediaPath = productMediaPath;
		this.product = product;
	}
	
	/**
	 * @return the productID
	 */
	public long getProductID() {
		if (product != null) {
			this.productID = product.getProductID();
		}
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(long productID) {
		this.productID = productID;
	}
	/**
	 * @return the productMediaID
	 */
	public long getProductMediaID() {
		return productMediaID;
	}
	/**
	 * @param productMediaID the productMediaID to set
	 */
	public void setProductMediaID(long productMediaID) {
		this.productMediaID = productMediaID;
	}
	/**
	 * @return the productMediatypeID
	 */
	public long getProductMediatypeID() {
		return productMediatypeID;
	}
	/**
	 * @param productMediatypeID the productMediatypeID to set
	 */
	public void setProductMediatypeID(long productMediatypeID) {
		this.productMediatypeID = productMediatypeID;
	}
	/**
	 * @return the productMediaPath
	 */
	public String getProductMediaPath() {
		return productMediaPath;
	}
	/**
	 * @param productMediaPath the productMediaPath to set
	 */
	public void setProductMediaPath(String productMediaPath) {
		this.productMediaPath = productMediaPath;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
}

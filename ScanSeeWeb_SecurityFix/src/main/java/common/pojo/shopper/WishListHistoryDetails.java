package common.pojo.shopper;

import java.util.ArrayList;

/**
 * This pojo contains wish list product history list.
 * @author malathi_lr
 *
 */
public class WishListHistoryDetails {


	/**
	 * for pagination.
	 */
	private Integer totalSize;
	
	/**
	 * for wish list history details.
	 */
	private ArrayList<WishListResultSet> productHistoryInfo;

	/**
	 * Gets the value of productHistoryInfo property.
	 * @return the productHistoryInfo.
	 */
	public ArrayList<WishListResultSet> getProductHistoryInfo()
	{
		return productHistoryInfo;
	}

	/**
	 *  Sets the value of productHistoryInfo property.
	 * @param productHistoryInfo the productHistoryInfo to set
	 */
	public void setProductHistoryInfo(ArrayList<WishListResultSet> productHistoryInfo)
	{
		this.productHistoryInfo = productHistoryInfo;
	}

	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize() {
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

	
}

package common.pojo.shopper;

public class DoubleCheck
{
	
	
    private Integer categoryID;
	private Integer parentCategoryID;
	
	private Integer subCategoryID;
	private String parentCategoryName;
	private String subCategoryName;
	private Integer  displayed;
	/**
	 * @return the categoryID
	 */
	public Integer getCategoryID()
	{
		return categoryID;
	}
	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}
	/**
	 * @return the parentCategoryID
	 */
	public Integer getParentCategoryID()
	{
		return parentCategoryID;
	}
	/**
	 * @param parentCategoryID the parentCategoryID to set
	 */
	public void setParentCategoryID(Integer parentCategoryID)
	{
		this.parentCategoryID = parentCategoryID;
	}
	/**
	 * @return the subCategoryID
	 */
	public Integer getSubCategoryID()
	{
		return subCategoryID;
	}
	/**
	 * @param subCategoryID the subCategoryID to set
	 */
	public void setSubCategoryID(Integer subCategoryID)
	{
		this.subCategoryID = subCategoryID;
	}
	/**
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}
	/**
	 * @param parentCategoryName the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}
	/**
	 * @return the subCategoryName
	 */
	public String getSubCategoryName()
	{
		return subCategoryName;
	}
	/**
	 * @param subCategoryName the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName)
	{
		this.subCategoryName = subCategoryName;
	}
	/**
	 * @return the displayed
	 */
	public Integer getDisplayed()
	{
		return displayed;
	}
	/**
	 * @param displayed the displayed to set
	 */
	public void setDisplayed(Integer displayed)
	{
		this.displayed = displayed;
	}
	
	
	

}

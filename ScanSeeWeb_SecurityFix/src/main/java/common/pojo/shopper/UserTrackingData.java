package common.pojo.shopper;

/**
 * POJO class for user tracking parameters
 * @author dhruvanath_mm
 *
 */
public class UserTrackingData {
	
	/**
	 * for user tracking maduleId
	 */
	private Integer moduleID;
	
	/**
	 * for user tracking moduleName
	 */
	private String moduleName;
	
	/**
	 * for user tracking userID
	 */
	private Integer userID;
	
	private Double latitude;
	
	private Double longitude;
	
	private String postalCode;
	
	private Boolean locateOnMap;
	
	private Integer mainMenuID;
	
	private Integer locDetailsID;
	
	private Integer retDetailsID;
	
	private Integer anythingPageListID;
	
	private Integer retListID;
	
	private Boolean banADClick;
	
	private Boolean callStoreClick;
	
	private Boolean browseWebClick;
	
	private Boolean getDirClick;
	
	private Boolean anythingPageClick;
	
	private Integer speListID;
	
	private Boolean speOffClick;
	
	private Integer hotDealID;
	
	private Integer hotDealListID;
	
	private String prodSearch;
	
	private Integer prodSmaSeaID;
	
	private Integer prodID;
	
	private Integer coupID;
	
	private Integer rebID;
	
	private String tarAddr;
	
	private Integer shrTypID;
	
	private Integer retLocID;
	
	private String shrTypNam;
	
	private Integer loyID;
	
	private String zipcode;
	
	private Integer lastVisited;
	
	/**
	 * for qrRetCustPageID.
	 */
	private Integer qrRetCustPageID;
	/**
	 * For scanTypeID
	 */
	private Integer scanTypeID;
	/**
	 * To get moduleId for user tracking.
	 * @return
	 */
	public Integer getModuleID() {
		return moduleID;
	}
	
	/**
	 * to set moduleId for user tracking.
	 * @param moduleId
	 */
	public void setModuleID(Integer moduleID) {
		this.moduleID = moduleID;
	}
	
	/**
	 * To get module name for user tracking.
	 * @return
	 */
	public String getModuleName() {
		return moduleName;
	}
	
	/**
	 * To set module name for user tracking.
	 * @param moduleName
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * To get userID
	 * @return Integer userID
	 */
	public Integer getUserID() {
		return userID;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Boolean getLocateOnMap() {
		return locateOnMap;
	}

	public void setLocateOnMap(Boolean locateOnMap) {
		this.locateOnMap = locateOnMap;
	}

	public Integer getMainMenuID() {
		return mainMenuID;
	}

	public void setMainMenuID(Integer mainMenuID) {
		this.mainMenuID = mainMenuID;
	}

	public Integer getLocDetailsID() {
		return locDetailsID;
	}

	public void setLocDetailsID(Integer locDetailsID) {
		this.locDetailsID = locDetailsID;
	}

	public Integer getRetDetailsID() {
		return retDetailsID;
	}

	public void setRetDetailsID(Integer retDetailsID) {
		this.retDetailsID = retDetailsID;
	}

	public Integer getAnythingPageListID() {
		return anythingPageListID;
	}

	public void setAnythingPageListID(Integer anythingPageListID) {
		this.anythingPageListID = anythingPageListID;
	}

	public Integer getRetListID() {
		return retListID;
	}

	public void setRetListID(Integer retListID) {
		this.retListID = retListID;
	}

	public Boolean getBanADClick() {
		return banADClick;
	}

	public void setBanADClick(Boolean banADClick) {
		this.banADClick = banADClick;
	}

	public Boolean getCallStoreClick() {
		return callStoreClick;
	}

	public void setCallStoreClick(Boolean callStoreClick) {
		this.callStoreClick = callStoreClick;
	}

	public Boolean getBrowseWebClick() {
		return browseWebClick;
	}

	public void setBrowseWebClick(Boolean browseWebClick) {
		this.browseWebClick = browseWebClick;
	}

	public Boolean getGetDirClick() {
		return getDirClick;
	}

	public void setGetDirClick(Boolean getDirClick) {
		this.getDirClick = getDirClick;
	}

	public Boolean getAnythingPageClick() {
		return anythingPageClick;
	}

	public void setAnythingPageClick(Boolean anythingPageClick) {
		this.anythingPageClick = anythingPageClick;
	}

	public Integer getSpeListID() {
		return speListID;
	}

	public void setSpeListID(Integer speListID) {
		this.speListID = speListID;
	}

	public Boolean getSpeOffClick() {
		return speOffClick;
	}

	public void setSpeOffClick(Boolean speOffClick) {
		this.speOffClick = speOffClick;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public Integer getHotDealID() {
		return hotDealID;
	}

	public void setHotDealID(Integer hotDealID) {
		this.hotDealID = hotDealID;
	}

	public Integer getHotDealListID() {
		return hotDealListID;
	}

	public void setHotDealListID(Integer hotDealListID) {
		this.hotDealListID = hotDealListID;
	}
	
	public String getProdSearch() {
		return prodSearch;
	}

	public void setProdSearch(String prodSearch) {
		this.prodSearch = prodSearch;
	}
	
	public Integer getProdSmaSeaID() {
		return prodSmaSeaID;
	}

	public void setProdSmaSeaID(Integer prodSmaSeaID) {
		this.prodSmaSeaID = prodSmaSeaID;
	}

	public Integer getProdID() {
		return prodID;
	}

	public void setProdID(Integer prodID) {
		this.prodID = prodID;
	}

	public Integer getCoupID() {
		return coupID;
	}

	public void setCoupID(Integer coupID) {
		this.coupID = coupID;
	}

	public Integer getRebID() {
		return rebID;
	}

	public void setRebID(Integer rebID) {
		this.rebID = rebID;
	}

	public String getTarAddr() {
		return tarAddr;
	}

	public void setTarAddr(String tarAddr) {
		this.tarAddr = tarAddr;
	}

	public Integer getShrTypID() {
		return shrTypID;
	}

	public void setShrTypID(Integer shrTypID) {
		this.shrTypID = shrTypID;
	}

	public Integer getRetLocID() {
		return retLocID;
	}

	public void setRetLocID(Integer retLocID) {
		this.retLocID = retLocID;
	}

	public String getShrTypNam() {
		return shrTypNam;
	}

	public void setShrTypNam(String shrTypNam) {
		this.shrTypNam = shrTypNam;
	}
	
	public Integer getLoyID() {
		return loyID;
	}

	public void setLoyID(Integer loyID) {
		this.loyID = loyID;
	}

	/**
	 * @return the zipcode
	 */
	public String getZipcode()
	{
		return zipcode;
	}

	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}

	/**
	 * @return the qrRetCustPageID
	 */
	public Integer getQrRetCustPageID()
	{
		return qrRetCustPageID;
	}

	/**
	 * @param qrRetCustPageID the qrRetCustPageID to set
	 */
	public void setQrRetCustPageID(Integer qrRetCustPageID)
	{
		this.qrRetCustPageID = qrRetCustPageID;
	}

	/**
	 * @return the scanTypeID
	 */
	public Integer getScanTypeID()
	{
		return scanTypeID;
	}

	/**
	 * @param scanTypeID the scanTypeID to set
	 */
	public void setScanTypeID(Integer scanTypeID)
	{
		this.scanTypeID = scanTypeID;
	}

	/**
	 * @return the lastVisited
	 */
	public Integer getLastVisited()
	{
		return lastVisited;
	}

	/**
	 * @param lastVisited the lastVisited to set
	 */
	public void setLastVisited(Integer lastVisited)
	{
		this.lastVisited = lastVisited;
	}
}

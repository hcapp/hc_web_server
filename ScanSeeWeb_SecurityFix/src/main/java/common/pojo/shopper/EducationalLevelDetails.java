package common.pojo.shopper;

import java.util.List;

/**
 * The EducationalLevelDetails contains setetr and getetr methods.
 * @author sourab_r
 *
 */
public class EducationalLevelDetails
{
	

	/**
	 * The educationalInfo declared as list.
	 */
	private List<EducationalLevelInfo> educationalInfo;

	/**
	 * To get educationalInfo.
	 * @return educationalInfo To get
	 */
	public List<EducationalLevelInfo> getEducationalInfo()
	{
	return educationalInfo;
	}

	/**
	 * To set educationalInfo.
	 * @param educationalInfo
	 *         To set
	 */
	public void setEducationalInfo(List<EducationalLevelInfo> educationalInfo)
	{
	this.educationalInfo = educationalInfo;
	}
}

package common.pojo.shopper;

import java.util.ArrayList;

/**
 * This pojo for adding shoppinglist products.
 * @author malathi_lr
 *
 */
public class AddSLRequest {

	 /**
	  * for shopping list history add product.
	  */
	 private String addTo;
	/**
	 * for userId.
	 */
	private Long userId;
	/**
	 * for isFromThisLocation.
	 */
	private Boolean isFromThisLocation;
	/**
	 * for isWishlst.
	 */
	private Boolean isWishlst;
	/**
	 * for retailID.
	 */
	private Integer retailID;
	/**
	 * for isaddToTSL.
	 */
	private Boolean isaddToTSL;
	/**
	 * for productDetails.
	 */
	private ArrayList<ProductDetail> productDetails;

	/**
	 * to get retailer id.
	 * 
	 * @return the retailID
	 */
	public Integer getRetailID()
	{
		return retailID;
	}

	/**
	 * for setting retailerid.
	 * 
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Integer retailID)
	{
		this.retailID = retailID;
	}
	/**
	 * for getting isFromThisLocation.
	 * 
	 * @return the isFromThisLocation
	 */
	public Boolean getIsFromThisLocation()
	{
		return isFromThisLocation;
	}

	/**
	 * for setting isFromThisLocation.
	 * 
	 * @param isFromThisLocation
	 *            the isFromThisLocation to set
	 */
	public void setIsFromThisLocation(Boolean isFromThisLocation)
	{
		this.isFromThisLocation = isFromThisLocation;
	}

	/**
	 * to get isaddToTSL.
	 * 
	 * @return the isaddToTSL
	 */
	public Boolean getIsaddToTSL()
	{
		return isaddToTSL;
	}

	/**
	 * to set isaddToTSL.
	 * 
	 * @param isaddToTSL
	 *            the isaddToTSL to set
	 */
	public void setIsaddToTSL(Boolean isaddToTSL)
	{
		this.isaddToTSL = isaddToTSL;
	}
	/**
	 * for getting userId.
	 * 
	 * @return userId.
	 */
	public Long getUserId()
	{
		return userId;
	}
	/**
	 * for setting userId.
	 * 
	 * @param userId
	 *            to be set.
	 */
	public void setUserId(Long userId)
	{
		this.userId = userId;
	}
	/**
	 * for getting array product detail.
	 * 
	 * @return productDetails
	 */
	public ArrayList<ProductDetail> getProductDetails()
	{
		return productDetails;
	}
	/**
	 * for setting array of productDetails.
	 * 
	 * @param productDetails
	 *            to be set.
	 */
	public void setProductDetails(ArrayList<ProductDetail> productDetails)
	{
		this.productDetails = productDetails;
	}

	/**
	 * to get isWishlst.
	 * 
	 * @return the isWishlst
	 */
	public Boolean getIsWishlst()
	{
		return isWishlst;
	}

	/**
	 * to set isWishlst.
	 * 
	 * @param isWishlst
	 *            the isWishlst to set
	 */
	public void setIsWishlst(Boolean isWishlst)
	{
		this.isWishlst = isWishlst;
	}

	/**
	 * To get addTo.
	 * @return the addTo
	 */
	public String getAddTo()
	{
		return addTo;
	}

	/**
	 * To set addTo.
	 * @param addTo the addTo to set
	 */
	public void setAddTo(String addTo)
	{
		this.addTo = addTo;
	}
	
	
}

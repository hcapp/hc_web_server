package common.pojo.shopper;

import java.util.ArrayList;

/**
 * POJO class for Main catagory
 * @author malathi_lr
 *
 */
public class MainSLRetailerCategories {

	/**
	 * 
	 */
	private Integer totalSize;
	
	/**
	 * 
	 */
	private ArrayList<MainSLRetailerCategory> mainSLRetailerCategory;

	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize() {
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

	/**
	 * @return the mainSLRetailerCategory
	 */
	public ArrayList<MainSLRetailerCategory> getMainSLRetailerCategory() {
		return mainSLRetailerCategory;
	}

	/**
	 * @param mainSLRetailerCategory the mainSLRetailerCategory to set
	 */
	public void setMainSLRetailerCategory(
			ArrayList<MainSLRetailerCategory> mainSLRetailerCategory) {
		this.mainSLRetailerCategory = mainSLRetailerCategory;
	}
}

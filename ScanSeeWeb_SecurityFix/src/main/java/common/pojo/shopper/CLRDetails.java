package common.pojo.shopper;

import java.util.List;

/**
 * pojo for CLRDetails.
 * 
 * @author pradip_k
 */
public class CLRDetails
{

	private String couponName;
	private Integer couponListID;
	private Integer LoyaltyListID;
	private Integer rebateListID;
	
	private Integer nextPageFlag;
	/**
	 * /** for coupon next page
	 */
	private Integer clrC;
	/**
	 * for rebate next page
	 */
	private Integer clrR;
	/**
	 * for loyalty next page
	 */
	private Integer clrL;

	/**
	 * for couponId.
	 */
	private Integer couponId;
	/**
	 * for rebateId.
	 */
	private Integer rebateId;
	/**
	 * for loyaltyId.
	 */
	private Integer loyaltyDealID;
	/**
	 * for productId.
	 */
	private Integer productId;
	/**
	 * for userId.
	 */
	private Integer userId;
	/**
	 * for retailerId.
	 */
	private Integer retailerId;

	/**
	 * Variable nextPage declared as of type Integer.
	 */
	private Integer nextPage;

	/**
	 * Variable galleryFlag declared as of type String.
	 */
	private String type;

	/**
	 * for lowerLimit.
	 */
	private Integer lowerLimit;

	/**
	 * variable declare couponDetails as List.
	 */
	protected List<CouponDetails> couponDetails;

	/**
	 * for isCouponthere is there or not..
	 */
	private Boolean isCouponthere;

	/**
	 * for rebate is there or not..
	 */
	private Boolean isRebatethere;

	/**
	 * for rebate is there or not..
	 */
	private Boolean isLoyaltythere;

	/**
	 * variable declare loyaltyDetails as List.
	 */

	private List<LoyaltyDetail> loyaltyDetails;

	/**
	 * variable declare rebateDetails as List.
	 */

	private List<RebateDetail> rebateDetails;

	private String clrType;

	private CouponsDetails coupDetails;

	private LoyaltyDetails loyDetails;

	private RebateDetails rebDetails;
	/**
	 * Parameter to hold the value of event triggerred from. ex:
	 * all,expired,used,myGallery
	 */
	private String requestType;

	private String added;

	/**
	 * This variable tells whether previous or next set of records
	 */
	private String pageFlowType;

	private String clrImagePath;

	/**
	 * for couponId.
	 */
	private Integer couponID;

	private Integer categoryID;

	private String searchKey;
	private String searchType;

	private List<RetailerDetail> loygrpbyRetlst = null;

	/**
	 * for ViewableOnWeb to check coupon is added from web
	 */
	private boolean viewableOnWeb;
	/**
	 * 
	 */
	private int totalSize;
	/**
	 * 
	 */
	private String returnURL;
	/**
	 * 
	 */
	private Integer recordCount;
	/**
	 * 
	 */
	private Integer mainMenuId;
	/**
	 * 
	 * @return
	 */

	public List<CouponDetails> getCouponDetails()
	{
		return couponDetails;
	}

	/**
	 * for setting couponDetails.
	 * 
	 * @param couponDetails
	 *            set as couponDetails
	 */
	public void setCouponDetails(List<CouponDetails> couponDetails)
	{

		this.couponDetails = couponDetails;
	}

	/**
	 * for getting getLoyaltyDetails list.
	 * 
	 * @return loyaltyDetails
	 */

	public List<LoyaltyDetail> getLoyaltyDetails()
	{
		return loyaltyDetails;
	}

	/**
	 * for setting loyaltyDetails.
	 * 
	 * @param loyaltyDetails
	 *            set as couponDetails
	 */
	public void setLoyaltyDetails(List<LoyaltyDetail> loyaltyDetails)
	{
		this.loyaltyDetails = loyaltyDetails;
	}

	/**
	 * for getting rebateDetails list.
	 * 
	 * @return rebateDetails
	 */

	public List<RebateDetail> getRebateDetails()
	{
		return rebateDetails;
	}

	/**
	 * for setting rebateDetails.
	 * 
	 * @param rebateDetails
	 *            set as rebateDetails
	 */

	public void setRebateDetails(List<RebateDetail> rebateDetails)
	{
		this.rebateDetails = rebateDetails;
	}

	/**
	 * for getting nextPage.
	 * 
	 * @return the nextPage
	 */
	public Integer getNextPage()
	{
		return nextPage;
	}

	/**
	 * for setting nextPage.
	 * 
	 * @param nextPage
	 *            the nextPage to set
	 */
	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}

	/**
	 * to fetch couponId.
	 * 
	 * @return the couponId
	 */
	public Integer getCouponId()
	{
		return couponId;
	}

	/**
	 * to set couponId.
	 * 
	 * @param couponId
	 *            the couponId to set
	 */
	public void setCouponId(Integer couponId)
	{
		this.couponId = couponId;
	}

	/**
	 * to fetch rebateId.
	 * 
	 * @return the rebateId
	 */
	public Integer getRebateId()
	{
		return rebateId;
	}

	/**
	 * to set rebateId.
	 * 
	 * @param rebateId
	 *            the rebateId to set
	 */
	public void setRebateId(Integer rebateId)
	{
		this.rebateId = rebateId;
	}

	/**
	 * to fetch productId.
	 * 
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * to set productId.
	 * 
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * to fetch userId.
	 * 
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * to get userId.
	 * 
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * to get retailerId.
	 * 
	 * @return the retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * to set retailerId.
	 * 
	 * @param retailerId
	 *            the retailerId to set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * for getting lowerLimit.
	 * 
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit()
	{
		return lowerLimit;
	}

	/**
	 * for setting lowerLimit.
	 * 
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLastVistedProductNo(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	/**
	 * for getting type.
	 * 
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * for setting type.
	 * 
	 * @param type
	 *            the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @return the clrC
	 */
	public Integer getClrC()
	{
		return clrC;
	}

	/**
	 * @return the loyaltyDealID
	 */
	public Integer getLoyaltyDealID()
	{
		return loyaltyDealID;
	}

	/**
	 * @param loyaltyDealID
	 *            the loyaltyDealID to set
	 */
	public void setLoyaltyDealID(Integer loyaltyDealID)
	{
		this.loyaltyDealID = loyaltyDealID;
	}

	/**
	 * @return the nextPageFlag
	 */
	public Integer getNextPageFlag()
	{
		return nextPageFlag;
	}

	/**
	 * @param nextPageFlag
	 *            the nextPageFlag to set
	 */
	public void setNextPageFlag(Integer nextPageFlag)
	{
		this.nextPageFlag = nextPageFlag;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @param clrC
	 *            the clrC to set
	 */
	public void setClrC(Integer clrC)
	{
		this.clrC = clrC;
	}

	/**
	 * @return the clrR
	 */
	public Integer getClrR()
	{
		return clrR;
	}

	/**
	 * @param clrR
	 *            the clrR to set
	 */
	public void setClrR(Integer clrR)
	{
		this.clrR = clrR;
	}

	/**
	 * @return the clrL
	 */
	public Integer getClrL()
	{
		return clrL;
	}

	/**
	 * @param clrL
	 *            the clrL to set
	 */
	public void setClrL(Integer clrL)
	{
		this.clrL = clrL;
	}

	/**
	 * @return the clrType
	 */
	public String getClrType()
	{
		return clrType;
	}

	/**
	 * @param clrType
	 *            the clrType to set
	 */
	public void setClrType(String clrType)
	{
		this.clrType = clrType;
	}

	/**
	 * @return the coupDetails
	 */
	public CouponsDetails getCoupDetails()
	{
		return coupDetails;
	}

	/**
	 * @param coupDetails
	 *            the coupDetails to set
	 */
	public void setCoupDetails(CouponsDetails coupDetails)
	{
		this.coupDetails = coupDetails;
	}

	/**
	 * @return the loyDetails
	 */
	public LoyaltyDetails getLoyDetails()
	{
		return loyDetails;
	}

	/**
	 * @param loyDetails
	 *            the loyDetails to set
	 */
	public void setLoyDetails(LoyaltyDetails loyDetails)
	{
		this.loyDetails = loyDetails;
	}

	/**
	 * @return the rebDetails
	 */
	public RebateDetails getRebDetails()
	{
		return rebDetails;
	}

	/**
	 * @param rebDetails
	 *            the rebDetails to set
	 */
	public void setRebDetails(RebateDetails rebDetails)
	{
		this.rebDetails = rebDetails;
	}

	/**
	 * @return the requestType
	 */
	public String getRequestType()
	{
		return requestType;
	}

	/**
	 * @param requestType
	 *            the requestType to set
	 */
	public void setRequestType(String requestType)
	{
		this.requestType = requestType;
	}

	/**
	 * @return the added
	 */
	public String getAdded()
	{
		return added;
	}

	/**
	 * @param added
	 *            the added to set
	 */
	public void setAdded(String added)
	{
		this.added = added;
	}

	/**
	 * @return the pageFlowType
	 */
	public String getPageFlowType()
	{
		return pageFlowType;
	}

	/**
	 * @param pageFlowType
	 *            the pageFlowType to set
	 */
	public void setPageFlowType(String pageFlowType)
	{
		this.pageFlowType = pageFlowType;
	}

	/**
	 * @return the clrImagePath
	 */
	public String getClrImagePath()
	{
		return clrImagePath;
	}

	/**
	 * @param clrImagePath
	 *            the clrImagePath to set
	 */
	public void setClrImagePath(String clrImagePath)
	{
		this.clrImagePath = clrImagePath;
	}

	/**
	 * @return the couponID
	 */
	public Integer getCouponID()
	{
		return couponID;
	}

	/**
	 * @param couponID
	 *            the couponID to set
	 */
	public void setCouponID(Integer couponID)
	{
		this.couponID = couponID;
	}

	public Boolean getIsCouponthere()
	{
		return isCouponthere;
	}

	public void setIsCouponthere(Boolean isCouponthere)
	{
		this.isCouponthere = isCouponthere;
	}

	public Boolean getIsRebatethere()
	{
		return isRebatethere;
	}

	public void setIsRebatethere(Boolean isRebatethere)
	{
		this.isRebatethere = isRebatethere;
	}

	public Boolean getIsLoyaltythere()
	{
		return isLoyaltythere;
	}

	public void setIsLoyaltythere(Boolean isLoyaltythere)
	{
		this.isLoyaltythere = isLoyaltythere;
	}

	/**
	 * @param categoryID
	 *            the categoryID to set
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}

	public Integer getCategoryID()
	{
		return categoryID;
	}

	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	public List<RetailerDetail> getLoygrpbyRetlst()
	{
		return loygrpbyRetlst;
	}

	public void setLoygrpbyRetlst(List<RetailerDetail> loygrpbyRetlst)
	{
		this.loygrpbyRetlst = loygrpbyRetlst;
	}

	public boolean isViewableOnWeb()
	{
		return viewableOnWeb;
	}

	public void setViewableOnWeb(boolean viewableOnWeb)
	{
		this.viewableOnWeb = viewableOnWeb;
	}

	public String getSearchType()
	{
		return searchType;
	}

	public void setSearchType(String searchType)
	{
		this.searchType = searchType;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	/**
	 * @return the totalSize
	 */
	public int getTotalSize() {
		return totalSize;
	}

	/**
	 * @param returnURL the returnURL to set
	 */
	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	/**
	 * @return the returnURL
	 */
	public String getReturnURL() {
		return returnURL;
	}

	/**
	 * @param recordCount the recordCount to set
	 */
	public void setRecordCount(Integer recordCount)
	{
		this.recordCount = recordCount;
	}

	/**
	 * @return the recordCount
	 */
	public Integer getRecordCount()
	{
		return recordCount;
	}

	/**
	 * @param mainMenuId the mainMenuId to set
	 */
	public void setMainMenuId(Integer mainMenuId)
	{
		this.mainMenuId = mainMenuId;
	}

	/**
	 * @return the mainMenuId
	 */
	public Integer getMainMenuId()
	{
		return mainMenuId;
	}

	public Integer getCouponListID()
	{
		return couponListID;
	}

	public void setCouponListID(Integer couponListID)
	{
		this.couponListID = couponListID;
	}

	public Integer getLoyaltyListID()
	{
		return LoyaltyListID;
	}

	public void setLoyaltyListID(Integer loyaltyListID)
	{
		LoyaltyListID = loyaltyListID;
	}

	public Integer getRebateListID()
	{
		return rebateListID;
	}

	public void setRebateListID(Integer rebateListID)
	{
		this.rebateListID = rebateListID;
	}

	public String getCouponName()
	{
		return couponName;
	}

	public void setCouponName(String couponName)
	{
		this.couponName = couponName;
	}

}

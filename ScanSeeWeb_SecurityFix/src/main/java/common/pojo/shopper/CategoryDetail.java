/**
 * Project     : ScanSeeWeb
 * File        : CategoryDetail.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 15th Feburary 2012
 */

package common.pojo.shopper;
import java.util.List;


/**
 * @author kumar_dodda
 *
 */
public class CategoryDetail
{
	
	
	private boolean email;
	private boolean cellPhone;
	
	/**
	 * 
	 */
	private String favCatId;
	/**
	 * for next page
	 */
	private Integer nextPage;
	/**
	 * for row num
	 */
	private Integer rowNum;
	/**
	 * for userId.
	 */
	private Integer userId;
	/**
	 * for categoryID.
	 */
	private Integer categoryID;
	/**
	 * for parentCategoryID.
	 */
	private Integer parentCategoryID;
	/**
	 * for parentCategoryName.
	 */
	private String parentCategoryName;
	/**
	 * for parentCategoryDescription.
	 */
	private String parentCategoryDescription;
	/**
	 * for subCategoryID.
	 */
	private Integer subCategoryID;
	/**
	 * for subCategoryName.
	 */
	private String subCategoryName;
	/**
	 * for subCategoryDescription.
	 */
	private String subCategoryDescription;
	/**
	 * for displayed.
	 */
	private Integer displayed;
	/**
	 * for mainCategoryDetaillst.
	 */

	private List<MainCategoryDetail> mainCategoryDetaillst;

	/**
	 * this is getting userId.
	 * 
	 * @return the userId.
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * This is for setting userId.
	 * 
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * This for getting mainCategoryDetaillst.
	 * 
	 * @return the mainCategoryDetaillst
	 */
	public List<MainCategoryDetail> getMainCategoryDetaillst()
	{
		return mainCategoryDetaillst;
	}

	/**
	 * This is for setting mainCategoryDetaillst.
	 * 
	 * @param mainCategoryDetaillst
	 *            the mainCategoryDetaillst to set
	 */
	public void setMainCategoryDetaillst(List<MainCategoryDetail> mainCategoryDetaillst)
	{
		this.mainCategoryDetaillst = mainCategoryDetaillst;
	}

	/**
	 * This is for getting categoryID.
	 * 
	 * @return the categoryID
	 */
	public Integer getCategoryID()
	{
		return categoryID;
	}

	/**
	 * This is for setting userID.
	 * 
	 * @param categoryID
	 *            the categoryID to set
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * This is for getting parentCategoryID.
	 * 
	 * @return the parentCategoryID
	 */
	public Integer getParentCategoryID()
	{
		return parentCategoryID;
	}

	/**
	 * This is for setting parentCategoryID.
	 * 
	 * @param parentCategoryID
	 *            the parentCategoryID to set
	 */
	public void setParentCategoryID(Integer parentCategoryID)
	{
		this.parentCategoryID = parentCategoryID;
	}

	/**
	 * this is for getting parentCategoryName.
	 * 
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}

	/**
	 * This is for setting parentCategoryName.
	 * 
	 * @param parentCategoryName
	 *            the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}

	/**
	 * This is for getting parentCategoryDescription.
	 * 
	 * @return the parentCategoryDescription
	 */
	public String getParentCategoryDescription()
	{
		return parentCategoryDescription;
	}

	/**
	 * This is for setting parentCategoryDescription.
	 * 
	 * @param parentCategoryDescription
	 *            the parentCategoryDescription to set
	 */
	public void setParentCategoryDescription(String parentCategoryDescription)
	{
		this.parentCategoryDescription = parentCategoryDescription;
	}

	/**
	 * This is for getting subCategoryID.
	 * 
	 * @return the subCategoryID
	 */
	public Integer getSubCategoryID()
	{
		return subCategoryID;
	}

	/**
	 * This is for setting subCategoryID.
	 * 
	 * @param subCategoryID
	 *            the subCategoryID to set
	 */
	public void setSubCategoryID(Integer subCategoryID)
	{
		this.subCategoryID = subCategoryID;
	}

	/**
	 * This is for getting subCategoryName.
	 * 
	 * @return the subCategoryName
	 */
	public String getSubCategoryName()
	{
		return subCategoryName;
	}

	/**
	 * This is for setting subCategoryName.
	 * 
	 * @param subCategoryName
	 *            the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName)
	{
		this.subCategoryName = subCategoryName;
	}

	/**
	 * This is for getting subCategoryDescription.
	 * 
	 * @return the subCategoryDescription
	 */
	public String getSubCategoryDescription()
	{
		return subCategoryDescription;
	}

	/**
	 * This is for setting subCategoryDescription.
	 * 
	 * @param subCategoryDescription
	 *            the subCategoryDescription to set
	 */
	public void setSubCategoryDescription(String subCategoryDescription)
	{
		this.subCategoryDescription = subCategoryDescription;
	}

	/**
	 * this is for getting displayed.
	 * 
	 * @return displayed
	 */
	public Integer getDisplayed()
	{
		return displayed;
	}

	/**
	 * this is for setting displayed.
	 * 
	 * @param displayed
	 *            as request parameter.
	 */
	public void setDisplayed(Integer displayed)
	{
		this.displayed = displayed;
	}

	public Integer getRowNum()
	{
		return rowNum;
	}

	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}

	public Integer getNextPage()
	{
		return nextPage;
	}

	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}

	/**
	 * @return the favCatId
	 */
	public String getFavCatId()
	{
		return favCatId;
	}

	/**
	 * @param favCatId the favCatId to set
	 */
	public void setFavCatId(String favCatId)
	{
		this.favCatId = favCatId;
	}

	/**
	 * @return the email
	 */
	public boolean isEmail()
	{
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(boolean email)
	{
		this.email = email;
	}

	/**
	 * @return the cellPhone
	 */
	public boolean isCellPhone()
	{
		return cellPhone;
	}

	/**
	 * @param cellPhone the cellPhone to set
	 */
	public void setCellPhone(boolean cellPhone)
	{
		this.cellPhone = cellPhone;
	}
	
	
	
}
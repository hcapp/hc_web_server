
/**
 * Project     : ScanSeeWeb
 * File        : HotDealAPIResultSet.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 15th Feburary 2012
 */
package common.pojo.shopper;


import java.util.ArrayList;

import common.pojo.shopper.HotDealsDetails;

/**
 * pojo class for Hot deals API result set.
 * @author kumar_dodda
 *
 */
public class HotDealAPIResultSet
{


	/**
	 * Variable apiPartnerName declared as String.
	 */
	private String apiPartnerName;
	/**
	 * Variable hotDealsDetailslst list.
	 */
	private ArrayList<HotDealsDetails> hotDealsDetailslst;
	/**
	 * To get apiPartnerName. 
	 * @return the apiPartnerName
	 */
	public String getApiPartnerName()
	{
		return apiPartnerName;
	}
	/**
	 * To set apiPartnerName.
	 * @param apiPartnerName the apiPartnerName to set
	 */
	public void setApiPartnerName(String apiPartnerName)
	{
		this.apiPartnerName = apiPartnerName;
	}
	/**
	 * To get hotDealsDetailslst.
	 * @return the hotDealsDetailslst
	 */
	public ArrayList<HotDealsDetails> getHotDealsDetailslst()
	{
		return hotDealsDetailslst;
	}
	/**
	 * To set hotDealsDetailslst.
	 * @param hotDealsDetailslst the hotDealsDetailslst to set
	 */
	public void setHotDealsDetailslst(ArrayList<HotDealsDetails> hotDealsDetailslst)
	{
		this.hotDealsDetailslst = hotDealsDetailslst;
	}
}

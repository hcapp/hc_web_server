package common.pojo.shopper;

import java.util.ArrayList;
import java.util.List;

import common.pojo.Category;
import common.pojo.PreferencesInfo;
import common.pojo.SchoolInfo;
import common.pojo.UserPreference;

/**
 * This pojo class is used for storing the data in the double check page
 * @author malathi_lr
 *
 */
public class DoubleCheckData {

	/**
	 * variable declared for DoubleCheck list
	 */
	List<DoubleCheck> preferencesList = new ArrayList<DoubleCheck>();
	
	/**
	 * variable declared for UserPreference list
	 */
	UserPreference userPreferenceInfoList = new UserPreference();
	
	/**
	 * variable declared for univerSityName
	 */
	String univerSityName = "";
	
	/**
	 * 
	 */
	CategoryDetail categoryDetailresponse = new CategoryDetail();
	
	/**
	 * 
	 */
	List<Category> checkboxprefrencesList = new ArrayList<Category>();
	
	/**
	 * 
	 */
	SchoolInfo states = new SchoolInfo();
	
	

	/**
	 * @return the states
	 */
	public SchoolInfo getStates() {
		return states;
	}

	/**
	 * @param states the states to set
	 */
	public void setStates(SchoolInfo states) {
		this.states = states;
	}

	/**
	 * @return the checkboxprefrencesList
	 */
	public List<Category> getCheckboxprefrencesList() {
		return checkboxprefrencesList;
	}

	/**
	 * @param checkboxprefrencesList the checkboxprefrencesList to set
	 */
	public void setCheckboxprefrencesList(List<Category> checkboxprefrencesList) {
		this.checkboxprefrencesList = checkboxprefrencesList;
	}

	/**
	 * @return the categoryDetailresponse
	 */
	public CategoryDetail getCategoryDetailresponse() {
		return categoryDetailresponse;
	}

	/**
	 * @param categoryDetailresponse the categoryDetailresponse to set
	 */
	public void setCategoryDetailresponse(CategoryDetail categoryDetailresponse) {
		this.categoryDetailresponse = categoryDetailresponse;
	}

	/**
	 * @return the alertpreferencesInfo
	 */
	public PreferencesInfo getAlertpreferencesInfo() {
		return alertpreferencesInfo;
	}

	/**
	 * @param alertpreferencesInfo the alertpreferencesInfo to set
	 */
	public void setAlertpreferencesInfo(PreferencesInfo alertpreferencesInfo) {
		this.alertpreferencesInfo = alertpreferencesInfo;
	}

	/**
	 * 
	 */
	PreferencesInfo alertpreferencesInfo = new PreferencesInfo();

	/**
	 * @return the preferencesList
	 */
	public List<DoubleCheck> getPreferencesList() {
		return preferencesList;
	}

	/**
	 * @return the userPreferenceInfoList
	 */
	public UserPreference getUserPreferenceInfoList() {
		return userPreferenceInfoList;
	}

	/**
	 * @param userPreferenceInfoList the userPreferenceInfoList to set
	 */
	public void setUserPreferenceInfoList(UserPreference userPreferenceInfoList) {
		this.userPreferenceInfoList = userPreferenceInfoList;
	}

	/**
	 * @return the univerSityName
	 */
	public String getUniverSityName() {
		return univerSityName;
	}

	/**
	 * @param preferencesList the preferencesList to set
	 */
	public void setPreferencesList(List<DoubleCheck> preferencesList) {
		this.preferencesList = preferencesList;
	}

	/**
	 * @param univerSityName the univerSityName to set
	 */
	public void setUniverSityName(String univerSityName) {
		this.univerSityName = univerSityName;
	}
}

package common.pojo.shopper;



import java.util.List;

import common.pojo.RetailerInfo;

/**
 * 
 * The POJO class for ShoppingListDetails.
 * @author shyamsundara_hm
 *
 */
public class ShoppingListDetails 
{

	/**
	 * for pagination next page
	 */
	private Integer nextPage;
	/**
	 * for list containing category list.
	 */
	private List<Category> categoryInfolst;

	/**
	 * the list of type retailerDetails.
	 */
	private List<RetailerInfo> retailerDetails;

	/**
	 * for getting retailers details.
	 * @return the retailerDetails
	 */
	public List<RetailerInfo> getRetailerDetails()
	{
		return retailerDetails;
	}

	/**for setting retailers details.
	 * @param retailerDetails the retailerDetails to set
	 */
	public void setRetailerDetails(List<RetailerInfo> retailerDetails)
	{
		this.retailerDetails = retailerDetails;
	}

	/**
	 * @return the nextPage
	 */
	public Integer getNextPage() {
		return nextPage;
	}

	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}

	/**
	 * @return the categoryInfolst
	 */
	public List<Category> getCategoryInfolst() {
		return categoryInfolst;
	}

	/**
	 * @param categoryInfolst the categoryInfolst to set
	 */
	public void setCategoryInfolst(List<Category> categoryInfolst) {
		this.categoryInfolst = categoryInfolst;
	}
	
	

}

package common.pojo.shopper;

import java.util.List;

/**
 * The MaritalStatusDetails contains setter and getter methods.
 * @author sourab_r
 *
 */
public class MaritalStatusDetails
{

	/**
	 * The maritalInfo declared as List.
	 */
	private List<MaritalStatusInfo> maritalInfo;

	/**
	 * To get maritalInfo list.
	 * @return maritalInfo To get
	 */
	public List<MaritalStatusInfo> getMaritalInfo()
	{
		return maritalInfo;
	}

	/**
	 * To set maritalInfo list.
	 * @param maritalInfo To set
	 */
	public void setMaritalInfo(List<MaritalStatusInfo> maritalInfo)
	{
		this.maritalInfo = maritalInfo;
	}

}

package common.pojo.shopper;

/**
 * The class has getter and setter methods for IncomeRange.
 * 
 * @author sowjanya_d
 */
public class IncomeRange extends BaseObject
{
	/**
	 * Variable incomeRangeId declared as of type Integer.
	 */
	protected String incomeRangeId;
	/**
	 * Variable incomeRange declared as of type String.
	 */
	protected String incomeRange;

	/**
	 * Gets the value of the incomeRangeId property.
	 * 
	 * @return incomeRangeId object is {@link Integer }
	 */
	public String getIncomeRangeId()
	{
		return incomeRangeId;
	}

	/**
	 * Sets the value of the incomeRangeId property.
	 * 
	 * @param incomeRangeId
	 *            allowed object is {@link Integer }
	 */
	public void setIncomeRangeId(String incomeRangeId)
	{

		if (null == incomeRangeId)
		{

			this.incomeRangeId = "N/A";
		}
		else
		{

			this.incomeRangeId = incomeRangeId;
		}

	}

	/**
	 * Gets the value of the incomeRange property.
	 * 
	 * @return incomeRange object is {@link String }
	 */
	public String getIncomeRange()
	{
		return incomeRange;
	}

	/**
	 * Sets the value of the incomeRange property.
	 * 
	 * @param incomeRange
	 *            allowed object is {@link String }
	 */
	public void setIncomeRange(String incomeRange)
	{

		if (null == incomeRange)
		{

			this.incomeRange = "N/A";
		}
		else
		{

			this.incomeRange = incomeRange;
		}

	}

}

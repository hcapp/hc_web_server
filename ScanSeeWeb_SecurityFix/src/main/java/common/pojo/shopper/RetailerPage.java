package common.pojo.shopper;

import common.constatns.ApplicationConstants;

/**
 * @author chandrasekaran_j
 */
public class RetailerPage
{
	/**
	 * logo.
	 */
	private String logo;
	/**
	 * pageTitle.
	 */
	private String pageTitle;
	/**
	 * image.
	 */
	private String image;
	/**
	 * pageDescription.
	 */
	private String pageDescription;
	/**
	 * shortDescription.
	 */
	private String shortDescription;
	/**
	 * longDescription.
	 */
	private String longDescription;
	/**
	 * retailName.
	 */
	private String retailName;
	/**
	 * appDownloadLink.
	 */
	private String appDownloadLink;

	/**
	 * for retailer ribben Add image
	 */
	private String ribbonAdImagePath;

	/**
	 * for ribben add url
	 */
	private String ribbonAdURL;

	/**
	 * for retailer created page link
	 */
	private String pageLink;

	/**
	 * for retailer contact no
	 */
	private String contactPhone;
	/**
	 * for page image
	 */
	private String pageImage;
	/**
	 * for retailer address
	 */
	private String retaileraddress1;

	/**
	 * for retailer name
	 */
	private String retailerName;
	/**
	 * for sale flag
	 */

	private Integer pageID;

	private Integer saleFlag;

	private Integer anythingPageListID;

	private boolean externalQR;

	private Integer specialsListID;

	public RetailerPage()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * constructor.
	 * 
	 * @param logo
	 *            logo
	 * @param pageTitle
	 *            pageTitle
	 * @param image
	 *            image
	 * @param pageDescription
	 *            pageDescription
	 * @param shortDescription
	 *            shortDescription
	 * @param longDescription
	 *            longDescription
	 * @param retailName
	 *            retailName
	 * @param appDownloadLink
	 *            appDownloadLink
	 */
	public RetailerPage(String logo, String pageTitle, String image, String pageDescription, String shortDescription, String longDescription,
			String retailName, String appDownloadLink)
	{
		super();
		this.logo = logo;
		this.pageTitle = pageTitle;
		this.image = image;
		this.pageDescription = pageDescription;
		this.shortDescription = shortDescription;
		this.longDescription = longDescription;
		this.retailName = retailName;
		this.appDownloadLink = appDownloadLink;
	}

	/**
	 * @return the appDownloadLink
	 */
	public String getAppDownloadLink()
	{
		return appDownloadLink;
	}

	/**
	 * @param appDownloadLink
	 *            the appDownloadLink to set
	 */
	public void setAppDownloadLink(String appDownloadLink)
	{
		this.appDownloadLink = appDownloadLink;
	}

	/**
	 * @return the retailName
	 */
	public String getRetailName()
	{
		return retailName;
	}

	/**
	 * @param retailName
	 *            the retailName to set
	 */
	public void setRetailName(String retailName)
	{
		this.retailName = retailName;
	}

	/**
	 * @return the logo
	 */
	public String getLogo()
	{
		return logo;
	}

	/**
	 * @param logo
	 *            the logo to set
	 */
	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	/**
	 * @return the pageTitle
	 */
	public String getPageTitle()
	{
		return pageTitle;
	}

	/**
	 * @param pageTitle
	 *            the pageTitle to set
	 */
	public void setPageTitle(String pageTitle)
	{
		this.pageTitle = pageTitle;
	}

	/**
	 * @return the image
	 */
	public String getImage()
	{
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image)
	{
		this.image = image;
	}

	/**
	 * @return the pageDescription
	 */
	public String getPageDescription()
	{
		return pageDescription;
	}

	/**
	 * @param pageDescription
	 *            the pageDescription to set
	 */
	public void setPageDescription(String pageDescription)
	{
		this.pageDescription = pageDescription;
	}

	/**
	 * @return the shortDescription
	 */
	public String getShortDescription()
	{
		return shortDescription;
	}

	/**
	 * @param shortDescription
	 *            the shortDescription to set
	 */
	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	/**
	 * @return the longDescription
	 */
	public String getLongDescription()
	{
		return longDescription;
	}

	/**
	 * @param longDescription
	 *            the longDescription to set
	 */
	public void setLongDescription(String longDescription)
	{
		this.longDescription = longDescription;
	}

	public String getPageLink()
	{
		return pageLink;
	}

	public void setPageLink(String pageLink)
	{
		this.pageLink = pageLink;
	}

	public String getRibbonAdImagePath()
	{
		return ribbonAdImagePath;
	}

	public void setRibbonAdImagePath(String ribbonAdImagePath)
	{
		this.ribbonAdImagePath = ribbonAdImagePath;
	}

	public String getRibbonAdURL()
	{
		return ribbonAdURL;
	}

	public void setRibbonAdURL(String ribbonAdURL)
	{
		this.ribbonAdURL = ribbonAdURL;
	}

	public String getPageImage()
	{
		return pageImage;
	}

	public void setPageImage(String pageImage)
	{

		if (null != pageImage && !pageImage.equals(""))
		{
			this.pageImage = pageImage;
		}
		else
		{
			this.pageImage = ApplicationConstants.IMAGEICON;
		}

	}

	public String getContactPhone()
	{
		return contactPhone;
	}

	public void setContactPhone(String contactPhone)
	{
		this.contactPhone = contactPhone;
	}

	public String getRetaileraddress1()
	{
		return retaileraddress1;
	}

	public void setRetaileraddress1(String retaileraddress1)
	{
		this.retaileraddress1 = retaileraddress1;
	}

	public String getRetailerName()
	{
		return retailerName;
	}

	public void setRetailerName(String retailerName)
	{
		this.retailerName = retailerName;
	}

	public Integer getSaleFlag()
	{
		return saleFlag;
	}

	public void setSaleFlag(Integer saleFlag)
	{
		this.saleFlag = saleFlag;
	}

	public Integer getPageID()
	{
		return pageID;
	}

	public void setPageID(Integer pageID)
	{
		this.pageID = pageID;
	}

	public Integer getAnythingPageListID()
	{
		return anythingPageListID;
	}

	public void setAnythingPageListID(Integer anythingPageListID)
	{
		this.anythingPageListID = anythingPageListID;
	}

	public boolean isExternalQR()
	{
		return externalQR;
	}

	public void setExternalQR(boolean externalQR)
	{
		this.externalQR = externalQR;
	}

	public Integer getSpecialsListID()
	{
		return specialsListID;
	}

	public void setSpecialsListID(Integer specialsListID)
	{
		this.specialsListID = specialsListID;
	}

}

package common.pojo.shopper;

import common.constatns.ApplicationConstants;
import common.util.Utility;

public class LoyaltyDetail
{

	
	/***
	 * for LoyaltyURL
	 */
	private String loyaltyURL;
	
	/**
	 * for UserClaimTypeID
	 */
	private Integer userClaimTypeID;
	/**
	 * for APIPartnerID.
	 */
	private Integer aPIPartnerID;

	/**
	 * for used flag
	 */
	private Integer usedFlag;
	/**
	 * for user gallery userLoyaltyGalleryID id
	 */
	private Integer userLoyaltyGalleryID;
	/**
	 * for userId
	 */
	private Integer userId;
	/**
	 * for LoyaltyDealPayoutMethod
	 */
	private String loyaltyDealPayoutMethod;
	/**
	 * for LoyaltyDealRedemptionDate
	 */
	private String loyaltyDealRedemptionDate;
	/**
	 * for product id.
	 */
	private Integer productId;
	/**
	 * for product name.
	 */
	private String productName;
	/**
	 * for rowNum.
	 * 
	 * @return the rowNum
	 */
	private Integer rowNum;

	/**
	 * variable loyaltyDealName declared as String.
	 */
	private String loyaltyDealName;
	/**
	 * variable loyaltyDealSource declared as String.
	 */
	private String loyaltyDealSource;
	/**
	 * variable loyaltyDealDiscountType declared as String.
	 */
	private String loyaltyDealDiscountType;
	/**
	 * variable loyaltyDealDiscountAmount declared as String.
	 */
	private String loyaltyDealDiscountAmount;
	/**
	 * variable loyaltyDealDiscountPct declared as String.
	 */
	private String loyaltyDealDiscountPct;
	/**
	 * variable loyaltyDealDescription declared as String.
	 */
	private String loyaltyDealDescription;
	/**
	 * variable loyaltyDealDateAdded declared as String.
	 */
	private String loyaltyDealDateAdded;
	/**
	 * variable loyaltyDealStartDate declared as String.
	 */
	private String loyaltyDealStartDate;
	/**
	 * variable loyaltyDealExpireDate declared as String.
	 */
	private String loyaltyDealExpireDate;
	/**
	 * variable usage declared as String.
	 */
	private String usage;

	/**
	 * variable usage declared as String.
	 */
	private String imagePath;

	/**
	 * variable loyaltyDealId declared as Integer.
	 */

	private Integer loyaltyDealId;

	/**
	 * Variable added declared as of type String.
	 */
	private String added;

	/**
	 * @return the userClaimTypeID
	 */
	public Integer getUserClaimTypeID()
	{
		return userClaimTypeID;
	}

	/**
	 * @param userClaimTypeID the userClaimTypeID to set
	 */
	public void setUserClaimTypeID(Integer userClaimTypeID)
	{
		this.userClaimTypeID = userClaimTypeID;
	}

	/**
	 * @return the aPIPartnerID
	 */
	public Integer getaPIPartnerID()
	{
		return aPIPartnerID;
	}

	/**
	 * @param aPIPartnerID the aPIPartnerID to set
	 */
	public void setaPIPartnerID(Integer aPIPartnerID)
	{
		this.aPIPartnerID = aPIPartnerID;
	}

	/**
	 * @return the usedFlag
	 */
	public Integer getUsedFlag()
	{
		return usedFlag;
	}

	/**
	 * @param usedFlag the usedFlag to set
	 */
	public void setUsedFlag(Integer usedFlag)
	{
		this.usedFlag = usedFlag;
	}

	/**
	 * @return the userLoyaltyGalleryID
	 */
	public Integer getUserLoyaltyGalleryID()
	{
		return userLoyaltyGalleryID;
	}

	/**
	 * @param userLoyaltyGalleryID the userLoyaltyGalleryID to set
	 */
	public void setUserLoyaltyGalleryID(Integer userLoyaltyGalleryID)
	{
		this.userLoyaltyGalleryID = userLoyaltyGalleryID;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the loyaltyDealPayoutMethod
	 */
	public String getLoyaltyDealPayoutMethod()
	{
		return loyaltyDealPayoutMethod;
	}

	/**
	 * @param loyaltyDealPayoutMethod the loyaltyDealPayoutMethod to set
	 */
	public void setLoyaltyDealPayoutMethod(String loyaltyDealPayoutMethod)
	{
		this.loyaltyDealPayoutMethod = loyaltyDealPayoutMethod;
	}

	/**
	 * @return the loyaltyDealRedemptionDate
	 */
	public String getLoyaltyDealRedemptionDate()
	{
		return loyaltyDealRedemptionDate;
	}

	/**
	 * @param loyaltyDealRedemptionDate the loyaltyDealRedemptionDate to set
	 */
	public void setLoyaltyDealRedemptionDate(String loyaltyDealRedemptionDate)
	{
		this.loyaltyDealRedemptionDate = loyaltyDealRedemptionDate;
	}

	/**
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the rowNum
	 */
	public Integer getRowNum()
	{
		return rowNum;
	}

	/**
	 * @param rowNum the rowNum to set
	 */
	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}

	/**
	 * @return the loyaltyDealName
	 */
	public String getLoyaltyDealName()
	{
		return loyaltyDealName;
	}

	/**
	 * @param loyaltyDealName the loyaltyDealName to set
	 */
	public void setLoyaltyDealName(String loyaltyDealName)
	{
		this.loyaltyDealName = loyaltyDealName;
	}

	/**
	 * @return the loyaltyDealSource
	 */
	public String getLoyaltyDealSource()
	{
		return loyaltyDealSource;
	}

	/**
	 * @param loyaltyDealSource the loyaltyDealSource to set
	 */
	public void setLoyaltyDealSource(String loyaltyDealSource)
	{
		this.loyaltyDealSource = loyaltyDealSource;
	}

	/**
	 * @return the loyaltyDealDiscountType
	 */
	public String getLoyaltyDealDiscountType()
	{
		return loyaltyDealDiscountType;
	}

	/**
	 * @param loyaltyDealDiscountType the loyaltyDealDiscountType to set
	 */
	public void setLoyaltyDealDiscountType(String loyaltyDealDiscountType)
	{
		this.loyaltyDealDiscountType = loyaltyDealDiscountType;
	}

	/**
	 * @return the loyaltyDealDiscountAmount
	 */
	public String getLoyaltyDealDiscountAmount()
	{
		return loyaltyDealDiscountAmount;
	}

	/**
	 * @param loyaltyDealDiscountAmount the loyaltyDealDiscountAmount to set
	 */
	public void setLoyaltyDealDiscountAmount(String loyaltyDealDiscountAmount)
	{
		if (loyaltyDealDiscountAmount == null)
		{
			this.loyaltyDealDiscountAmount = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{

			if (!loyaltyDealDiscountAmount.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(loyaltyDealDiscountAmount))
			{
				this.loyaltyDealDiscountAmount = "$" + Utility.formatDecimalValue(loyaltyDealDiscountAmount);
			}
			else
			{
				this.loyaltyDealDiscountAmount = loyaltyDealDiscountAmount;
			}
		}
	}

	/**
	 * @return the loyaltyDealDiscountPct
	 */
	public String getLoyaltyDealDiscountPct()
	{
		return loyaltyDealDiscountPct;
	}

	/**
	 * @param loyaltyDealDiscountPct the loyaltyDealDiscountPct to set
	 */
	public void setLoyaltyDealDiscountPct(String loyaltyDealDiscountPct)
	{
		this.loyaltyDealDiscountPct = loyaltyDealDiscountPct;
	}

	/**
	 * @return the loyaltyDealDescription
	 */
	public String getLoyaltyDealDescription()
	{
		return loyaltyDealDescription;
	}

	/**
	 * @param loyaltyDealDescription the loyaltyDealDescription to set
	 */
	public void setLoyaltyDealDescription(String loyaltyDealDescription)
	{
		this.loyaltyDealDescription = loyaltyDealDescription;
	}

	/**
	 * @return the loyaltyDealDateAdded
	 */
	public String getLoyaltyDealDateAdded()
	{
		return loyaltyDealDateAdded;
	}

	/**
	 * @param loyaltyDealDateAdded the loyaltyDealDateAdded to set
	 */
	public void setLoyaltyDealDateAdded(String loyaltyDealDateAdded)
	{
		this.loyaltyDealDateAdded = loyaltyDealDateAdded;
	}

	/**
	 * @return the loyaltyDealStartDate
	 */
	public String getLoyaltyDealStartDate()
	{
		return loyaltyDealStartDate;
	}

	/**
	 * @param loyaltyDealStartDate the loyaltyDealStartDate to set
	 */
	public void setLoyaltyDealStartDate(String loyaltyDealStartDate)
	{
		if (loyaltyDealStartDate != null)
		{
			this.loyaltyDealStartDate = Utility.convertDBdate(loyaltyDealStartDate);
		}
		else
		{
			this.loyaltyDealStartDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * @return the loyaltyDealExpireDate
	 */
	public String getLoyaltyDealExpireDate()
	{
		return loyaltyDealExpireDate;
	}

	/**
	 * @param loyaltyDealExpireDate the loyaltyDealExpireDate to set
	 */
	public void setLoyaltyDealExpireDate(String loyaltyDealExpireDate)
	{
		if (loyaltyDealExpireDate != null)
		{
			this.loyaltyDealExpireDate = Utility.convertDBdate(loyaltyDealExpireDate);
		}
		else
		{
			this.loyaltyDealExpireDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * @return the usage
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * @param usage the usage to set
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		if (imagePath == null || imagePath == "" )
		{
			//this.imagePath = imagePath;
			//Changed below code as IE browser showing broken image if it did not find image
			this.imagePath = ApplicationConstants.BLANKIMAGE;
		}
		else
		{
			this.imagePath = imagePath;
		}
	}

	/**
	 * @return the loyaltyDealId
	 */
	public Integer getLoyaltyDealId()
	{
		return loyaltyDealId;
	}

	/**
	 * @param loyaltyDealId the loyaltyDealId to set
	 */
	public void setLoyaltyDealId(Integer loyaltyDealId)
	{
		this.loyaltyDealId = loyaltyDealId;
	}

	/**
	 * @return the added
	 */
	public String getAdded()
	{
		return added;
	}

	/**
	 * @param added the added to set
	 */
	public void setAdded(String added)
	{
		this.added = added;
	}

	/**
	 * @return the loyaltyURL
	 */
	public String getLoyaltyURL()
	{
		return loyaltyURL;
	}

	/**
	 * @param loyaltyURL the loyaltyURL to set
	 */
	public void setLoyaltyURL(String loyaltyURL)
	{
		this.loyaltyURL = loyaltyURL;
	}

	/**
	 * * for getting loyaltyDealName.
	 * 
	 * @return loyaltyDealName
	 */
	
	
	
}

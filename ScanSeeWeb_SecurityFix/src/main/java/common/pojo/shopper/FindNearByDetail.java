package common.pojo.shopper;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * This pojo for handling find near by details.
 * 
 * @author malathi_lr
 */
public class FindNearByDetail
{

	private String retailerUrl;
	private String salePrice;
	private String address;
	private Integer productId;

	/**
	 * for retailerName.
	 */
	private String retailerName;
	/**
	 * for distance.
	 */
	private String distance;
	/**
	 * for productName.
	 */
	private String productName;
	/**
	 * for productPrice.
	 */
	private String productPrice;
	/**
	 * for retailerId.
	 */
	private Integer retailerId;
	/**
	 * for imagePath.
	 */
	private String imagePath;
	/**
	 * for latitude.
	 */
	private String latitude;
	/**
	 * for longitude.
	 */
	private String longitude;

	private Integer retLocId;

	/**
	 * to get retailerName.
	 * 
	 * @return retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * for setting retailerName.
	 * 
	 * @param retailerName
	 *            to be set.
	 */
	public void setRetailerName(String retailerName)
	{
		this.retailerName = retailerName;
	}

	/**
	 * for getting productName.
	 * 
	 * @return productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * for setting productName.
	 * 
	 * @param productName
	 *            to be set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * for getting productPrice.
	 * 
	 * @return productPrice
	 */
	public String getProductPrice()
	{
		return productPrice;
	}

	/**
	 * for setting productPrice.
	 * 
	 * @param productPrice
	 *            to be set
	 */
	public void setProductPrice(String productPrice)
	{
		if  (null != productPrice && !"".equals(productPrice))
		{		
			if (!productPrice.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(productPrice))
			{
				// this.productPrice = Utility.formatDecimalValue(productPrice);
				this.productPrice = "$" + Utility.formatDecimalValue(productPrice);
			}
			else
			{
				this.productPrice = productPrice;
			}
		}
	}

	/**
	 * for getting retailerId.
	 * 
	 * @return retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * for setting retailerId.
	 * 
	 * @param retailerId
	 *            to be set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * to get imagePath.
	 * 
	 * @return imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * for setting imagePath.
	 * 
	 * @param imagePath
	 *            to be set
	 */
	public void setImagePath(String imagePath)
	{
		this.imagePath = imagePath;
	}

	/**
	 * for getting latitude.
	 * 
	 * @return latitude
	 */
	public String getLatitude()
	{
		return latitude;
	}

	/**
	 * for setting latitude.
	 * 
	 * @param latitude
	 *            to be set
	 */
	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * for getting longitude.
	 * 
	 * @return longitude
	 */
	public String getLongitude()
	{
		return longitude;
	}

	/**
	 * for setting longitude.
	 * 
	 * @param longitude
	 *            to be set
	 */
	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public Integer getProductId()
	{
		return productId;
	}

	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		if (distance != null)
		{
			this.distance = Utility.formatDecimalValue(distance);
		}
	}

	public Integer getRetLocId()
	{
		return retLocId;
	}

	public void setRetLocId(Integer retLocId)
	{
		this.retLocId = retLocId;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public String getSalePrice()
	{
		return salePrice;
	}

	public void setSalePrice(String salePrice)
	{
		if (null != salePrice && !"".equals(salePrice))
		{
			if (!salePrice.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(salePrice))
			{
				this.salePrice = "$" + Utility.formatDecimalValue(salePrice);
			}
			else
			{
				this.salePrice = salePrice;
			}
		}
	}

	public String getRetailerUrl()
	{
		return retailerUrl;
	}

	public void setRetailerUrl(String retailerUrl)
	{
		this.retailerUrl = retailerUrl;
	}

}

package common.pojo.shopper;

/**
 * The POJO for SubCategory.
 * @author malathi_lr
 *
 */
public class SubCategory {
	/**
	 * The subCategoryId property.
	 */

	private Integer subCategoryId;

	/**
	 * To get subCategoryId.
	 * @return The subCategoryId property.
	 */

	public Integer getSubCategoryId()
	{
		return subCategoryId;
	}

	/**
	 * To set subCategoryId.
	 * @param subCategoryId
	 *            The subCategoryId property.
	 */

	public void setSubCategoryId(Integer subCategoryId)
	{
		this.subCategoryId = subCategoryId;
	}

}

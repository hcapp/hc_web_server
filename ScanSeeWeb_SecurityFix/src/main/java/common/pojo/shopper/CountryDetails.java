/**
 * 
 */
package common.pojo.shopper;

import java.util.List;

import common.pojo.CountryCode;

/**
 * The Class for Country Details.
 * 
 * @author pradip_k
 */
public class CountryDetails extends BaseObject
{

	/**
	 * Variable countryInfo declared as of type CountryCodes.
	 */

	private List<CountryCode> countryInfo;

	/**
	 * Variable stateInfo declared as of type CountryCodes.
	 */

	private List<CountryCode> stateInfo;

	/**
	 * Variable citiInfo declared as of type CountryCodes.
	 */

	private List<CountryCode> cityInfo;

	/**
	 * Gets Lis of countries.
	 * 
	 * @return the countryInfo
	 */
	public List<CountryCode> getCountryInfo()
	{
		return countryInfo;
	}

	/**
	 * This Method sets the value for countryInfo list.
	 * 
	 * @param countryInfo
	 *            the countryInfo to set
	 */
	public void setCountryInfo(List<CountryCode> countryInfo)
	{
		this.countryInfo = countryInfo;
	}

	/**
	 * This method gets the stateinfo property.
	 * 
	 * @return the stateInfo
	 */
	public List<CountryCode> getStateInfo()
	{
		return stateInfo;
	}

	/**
	 * This method sets stateInfo property.
	 * 
	 * @param stateInfo
	 *            the stateInfo to set
	 */
	public void setStateInfo(List<CountryCode> stateInfo)
	{
		this.stateInfo = stateInfo;
	}

	/**
	 * This methods gets the value of cityInfo property.
	 * 
	 * @return the cityInfo
	 */
	public List<CountryCode> getCityInfo()
	{
		return cityInfo;
	}

	/**
	 * This methods sets the value of cityInfo property.
	 * 
	 * @param cityInfo
	 *            the citiInfo to set
	 */
	public void setCityInfo(List<CountryCode> cityInfo)
	{
		this.cityInfo = cityInfo;
	}

}

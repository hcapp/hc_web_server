package common.pojo.shopper;

/**
 * This is a pojo class for Media information
 * @author malathi_lr
 *
 */
public class MediaVO {

	/**
	 * 
	 */
	private Integer productId;
	
	/**
	 * 
	 */
	private String mediaType;

	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mediaType;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * @param mediaType the mediaType to set
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
}

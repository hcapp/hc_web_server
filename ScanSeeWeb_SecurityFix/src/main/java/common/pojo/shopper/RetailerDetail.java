package common.pojo.shopper;

import java.util.List;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * The class has getter and setter methods for RetailerDetail fields.
 * 
 * @author manjunatha_gh
 */

public class RetailerDetail
{

	/**
	 * The rowNumber property.
	 */

	private Integer rowNumber;

	/**
	 * The retailerId property.
	 */

	private Integer retailerId;

	/**
	 * The retailerName property.
	 */

	private String retailerName;

	/**
	 * The retailLocationID property.
	 */
	private Integer retailLocationID;

	/**
	 * Variable for Retailer Address1.
	 */
	private String retaileraddress1;

	/**
	 * Variable for Retailer Address2.
	 */

	private String retaileraddress2;

	/**
	 * Variable for Retailer Address3.
	 */
	private String retaileraddress3;

	/**
	 * Variable for Retailer Address4.
	 */
	private String retaileraddress4;

	/**
	 * Variable for City.
	 */
	private String city;

	/**
	 * Variable for state.
	 */
	private String state;

	/**
	 * Variable for postalCode.
	 */
	private String postalCode;
	/**
	 * Variable for distance.
	 */
	private String distance;
	/**
	 * Variable for completeAddress.
	 */
	private String completeAddress;
	/**
	 * Variable for retailAddress.
	 */
	private String retailAddress;

	/**
	 * The list for storing type productDetails.
	 */

	private List<ProductDetail> productDetails;

	/**
	 * The imagePath property.
	 */

	private String logoImagePath;

	/**
	 * The displayed property.
	 */

	private Integer displayed;

	/**
	 * The bannerAdImagePath property.
	 */

	private String bannerAdImagePath;

	/**
	 * The ribbonAdImagePath property.
	 */

	private String ribbonAdImagePath;

	/**
	 * The ribbonAdURL property.
	 */

	private String ribbonAdURL;

	/**
	 * The advertisementID property.
	 */

	private String advertisementID;

	/**
	 * The price declared as string.
	 */
	private String price;

	/**
	 * The salePrice declared as string.
	 */
	private String salePrice;

	/**
	 * for online BuyURL
	 */
	private String buyURL;

	private String cateName;

	private Integer cateId;

	private List<CouponDetails> couponDetails;

	private Integer retListID;
	/**
	 * for user Id
	 */
	private Long userId;
	/**
	 * for retailer latitude
	 */
	private String retLat;
	/**
	 * fro retailer longitude
	 */
	private String retLng;

	/**
	 * for retailer contact phone
	 */
	private String contactPhone;

	/**
	 * for retailer url
	 */
	private String retailerURL;
	/**
	 * for sale flag
	 */
	private boolean saleFlag;
	/**
	 * for last visited no
	 */
	private Integer lastVisitedNo;

	/**
	 * for qr page id
	 */
	private Long pageID;
	/**
	 * Created for specail offer flag
	 */
	private Boolean specialOffFlag;
	/**
	 * creted for hotdeal flag
	 */
	private Boolean hotDealFlag;
	/**
	 * created for couponFlag;
	 */
	private Boolean clrFlag;
	/**
	 * for page link
	 */
	private String pageLink;
	/*
	 * for pageTitle
	 */
	private String pageTitle;
	/**
	 * for row num
	 */
	private Integer rowNum;

	private Integer retailerListID;

	private Integer mainMenuId;

	private Integer lowerLimit;
	private Integer recordCount;

	private boolean anyThingPageFlag;

	private String shipmentCost;

	public String getRetailAddress()
	{

		return retailAddress;
	}

	/**
	 * To set RetailAddress.
	 */
	public void setRetailAddress()
	{
		if (retaileraddress1 != null)
		{
			completeAddress = retaileraddress1 + ",";
		}
		if (city != null)
		{
			completeAddress = completeAddress + city + ",";
		}
		if (postalCode != null && !ApplicationConstants.NOTAPPLICABLE.equals(postalCode))
		{
			completeAddress = completeAddress + postalCode + ",";
		}
		if (state != null)
		{
			completeAddress = completeAddress + state;
		}
		this.retailAddress = completeAddress;
	}

	/**
	 * To get completeAddress.
	 * 
	 * @return the completeAddress
	 */
	public String getCompleteAddress()
	{
		return completeAddress;
	}

	/**
	 * To set completeAddress.
	 * 
	 * @param completeAddress
	 *            the completeAddress to set
	 */
	public void setCompleteAddress(String completeAddress)
	{

		this.completeAddress = completeAddress;
	}

	/**
	 * To get price value.
	 * 
	 * @return the price
	 */
	public String getPrice()
	{
		return price;
	}

	/**
	 * To sety price value.
	 * 
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price)
	{

		if (null != price && !"".equals(price))
		{
			if (!price.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(price))
			{
				this.price = "$" + Utility.formatDecimalValue(price);
			}
			else
			{
				this.price = price;
			}
		}
	}

	/**
	 * To get salePrice value.
	 * 
	 * @return the salePrice
	 */
	public String getSalePrice()
	{
		return salePrice;
	}

	/**
	 * To set salePrice value.
	 * 
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(String salePrice)
	{

		if (salePrice != null)
		{
			if (!salePrice.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(salePrice))
			{
				salePrice = Utility.formatDecimalValue(salePrice);
				this.salePrice = "$" + salePrice;
			}
			else
			{
				this.salePrice = salePrice;
			}
		}
		else
		{
			this.salePrice = salePrice;
		}

	}

	/**
	 * This method return rowNumber value.
	 * 
	 * @return the rowNumber
	 */
	public Integer getRowNumber()
	{
		return rowNumber;
	}

	/**
	 * This method set the value to rowNumber.
	 * 
	 * @param rowNumber
	 *            the rowNumber to set
	 */
	public void setRowNumber(Integer rowNumber)
	{
		this.rowNumber = rowNumber;
	}

	/**
	 * This method return retailerId value.
	 * 
	 * @return the retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * This method set the value to retailerId.
	 * 
	 * @param retailerId
	 *            the retailerId to set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * This method return retailerName value.
	 * 
	 * @return the retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * This method set the value to retailerName.
	 * 
	 * @param retailerName
	 *            the retailerName to set
	 */
	public void setRetailerName(String retailerName)
	{
		if (null == retailerName)
		{
			this.retailerName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.retailerName = retailerName;
		}
	}

	/**
	 * This method return retailLocationID value.
	 * 
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * This method set the value to retailLocationID.
	 * 
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID)
	{

		this.retailLocationID = retailLocationID;
	}

	/**
	 * This method return retaileraddress1 value.
	 * 
	 * @return the retaileraddress1
	 */
	public String getRetaileraddress1()
	{
		return retaileraddress1;
	}

	/**
	 * This method set the value to retaileraddress1.
	 * 
	 * @param retaileraddress1
	 *            the retaileraddress1 to set
	 */
	public void setRetaileraddress1(String retaileraddress1)
	{
		if (null == retaileraddress1)
		{
			this.retaileraddress1 = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.retaileraddress1 = retaileraddress1;
		}
	}

	/**
	 * This method return retaileraddress2 value.
	 * 
	 * @return the retaileraddress2
	 */
	public String getRetaileraddress2()
	{
		return retaileraddress2;
	}

	/**
	 * This method set the value to retaileraddress2.
	 * 
	 * @param retaileraddress2
	 *            the retaileraddress2 to set
	 */
	public void setRetaileraddress2(String retaileraddress2)
	{
		if (null == retaileraddress2)
		{
			this.retaileraddress2 = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.retaileraddress2 = retaileraddress2;
		}
	}

	/**
	 * This method return retaileraddress3 value.
	 * 
	 * @return the retaileraddress3
	 */
	public String getRetaileraddress3()
	{
		return retaileraddress3;
	}

	/**
	 * This method set the value to retaileraddress3.
	 * 
	 * @param retaileraddress3
	 *            the retaileraddress3 to set
	 */
	public void setRetaileraddress3(String retaileraddress3)
	{
		if (null == retaileraddress3)
		{
			this.retaileraddress3 = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.retaileraddress3 = retaileraddress3;
		}
	}

	/**
	 * This method return retaileraddress4 value.
	 * 
	 * @return the retaileraddress4
	 */
	public String getRetaileraddress4()
	{
		return retaileraddress4;
	}

	/**
	 * This method set the value to retaileraddress4.
	 * 
	 * @param retaileraddress4
	 *            the retaileraddress4 to set
	 */
	public void setRetaileraddress4(String retaileraddress4)
	{
		if (null == retaileraddress4)
		{
			this.retaileraddress4 = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.retaileraddress4 = retaileraddress4;
		}
	}

	/**
	 * This method return city value.
	 * 
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * This method set the value to city.
	 * 
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * This method return state value.
	 * 
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * This method set the value to state.
	 * 
	 * @param state
	 *            the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * This method return postalCode value.
	 * 
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * This method set the value to postalCode.
	 * 
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		if (null == postalCode || "".equals(postalCode))
		{
			// this.logoImagePath = ApplicationConstants.NOIMGPATH;
			this.postalCode = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.postalCode = postalCode;
		}

	}

	/**
	 * This method return distance value.
	 * 
	 * @return the distance
	 */
	public String getDistance()
	{
		return distance;
	}

	/**
	 * This method set the value to distance.
	 * 
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(String distance)
	{
		if (null == distance || "".equals(distance))
		{
			this.distance = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.distance = Utility.formatDecimalValue(distance);
		}

	}

	/**
	 * This method return productDetails value.
	 * 
	 * @return the productDetails
	 */
	public List<ProductDetail> getProductDetails()
	{
		return productDetails;
	}

	/**
	 * This method set the value to productDetails.
	 * 
	 * @param productDetails
	 *            the productDetails to set
	 */
	public void setProductDetails(List<ProductDetail> productDetails)
	{
		this.productDetails = productDetails;
	}

	/**
	 * This method return logoImagePath value.
	 * 
	 * @return the logoImagePath
	 */
	public String getLogoImagePath()
	{
		return logoImagePath;
	}

	/**
	 * This method set the value to logoImagePath.
	 * 
	 * @param logoImagePath
	 *            the logoImagePath to set
	 */
	public void setLogoImagePath(String logoImagePath)
	{
		if (null == logoImagePath || "".equals(logoImagePath))
		{
			// this.logoImagePath = ApplicationConstants.NOIMGPATH;
			this.logoImagePath = ApplicationConstants.IMAGEICON;
		}
		else
		{
			this.logoImagePath = logoImagePath;
		}
	}

	/**
	 * This method return displayed value.
	 * 
	 * @return the displayed
	 */
	public Integer getDisplayed()
	{
		return displayed;
	}

	/**
	 * This method set the value to displayed.
	 * 
	 * @param displayed
	 *            the displayed to set
	 */
	public void setDisplayed(Integer displayed)
	{
		this.displayed = displayed;
	}

	/**
	 * This method return bannerAdImagePath value.
	 * 
	 * @return the bannerAdImagePath
	 */
	public String getBannerAdImagePath()
	{
		return bannerAdImagePath;
	}

	/**
	 * This method set the value to bannerAdImagePath.
	 * 
	 * @param bannerAdImagePath
	 *            the bannerAdImagePath to set
	 */
	public void setBannerAdImagePath(String bannerAdImagePath)
	{
		if (null == bannerAdImagePath)
		{
			this.bannerAdImagePath = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.bannerAdImagePath = bannerAdImagePath;
		}
	}

	/**
	 * This method return ribbonAdImagePath value.
	 * 
	 * @return the ribbonAdImagePath
	 */
	public String getRibbonAdImagePath()
	{
		return ribbonAdImagePath;
	}

	/**
	 * This method set the value to ribbonAdImagePath.
	 * 
	 * @param ribbonAdImagePath
	 *            the ribbonAdImagePath to set
	 */
	public void setRibbonAdImagePath(String ribbonAdImagePath)
	{
		if (null == ribbonAdImagePath)
		{
			this.ribbonAdImagePath = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.ribbonAdImagePath = ribbonAdImagePath;
		}
	}

	/**
	 * This method return ribbonAdURL value.
	 * 
	 * @return the ribbonAdURL
	 */
	public String getRibbonAdURL()
	{
		return ribbonAdURL;
	}

	/**
	 * This method set the value to ribbonAdURL.
	 * 
	 * @param ribbonAdURL
	 *            the ribbonAdURL to set
	 */
	public void setRibbonAdURL(String ribbonAdURL)
	{
		if (null == ribbonAdURL)
		{
			this.ribbonAdURL = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.ribbonAdURL = ribbonAdURL;
		}
	}

	/**
	 * This method return advertisementID value.
	 * 
	 * @return the advertisementID
	 */
	public String getAdvertisementID()
	{
		return advertisementID;
	}

	/**
	 * This method set the value to advertisementID.
	 * 
	 * @param advertisementID
	 *            the advertisementID to set
	 */
	public void setAdvertisementID(String advertisementID)
	{
		this.advertisementID = advertisementID;
	}

	public String getBuyURL()
	{
		return buyURL;
	}

	public void setBuyURL(String buyURL)
	{
		this.buyURL = buyURL;
	}

	public String getCateName()
	{
		return cateName;
	}

	public void setCateName(String cateName)
	{
		this.cateName = cateName;
	}

	public List<CouponDetails> getCouponDetails()
	{
		return couponDetails;
	}

	public void setCouponDetails(List<CouponDetails> couponDetails)
	{
		this.couponDetails = couponDetails;
	}

	public Integer getCateId()
	{
		return cateId;
	}

	public void setCateId(Integer cateId)
	{
		this.cateId = cateId;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public String getRetLat()
	{
		return retLat;
	}

	public void setRetLat(String retLat)
	{
		this.retLat = retLat;
	}

	public String getRetLng()
	{
		return retLng;
	}

	public void setRetLng(String retLng)
	{
		this.retLng = retLng;
	}

	public String getContactPhone()
	{
		return contactPhone;
	}

	public void setContactPhone(String contactPhone)
	{

		if (null != contactPhone && !"".equals(contactPhone))
		{

			this.contactPhone = Utility.getPhoneFormate(contactPhone);
		}
		else
		{

			this.contactPhone = contactPhone;
		}

	}

	public String getRetailerURL()
	{
		return retailerURL;
	}

	public void setRetailerURL(String retailerURL)
	{
		this.retailerURL = retailerURL;
	}

	public Integer getLastVisitedNo()
	{
		return lastVisitedNo;
	}

	public void setLastVisitedNo(Integer lastVisitedNo)
	{
		this.lastVisitedNo = lastVisitedNo;
	}

	public Long getPageID()
	{
		return pageID;
	}

	public void setPageID(Long pageID)
	{
		this.pageID = pageID;
	}

	public Boolean getSpecialOffFlag()
	{
		return specialOffFlag;
	}

	public void setSpecialOffFlag(Boolean specialOffFlag)
	{
		this.specialOffFlag = specialOffFlag;
	}

	public Boolean getHotDealFlag()
	{
		return hotDealFlag;
	}

	public void setHotDealFlag(Boolean hotDealFlag)
	{
		this.hotDealFlag = hotDealFlag;
	}

	public Boolean getClrFlag()
	{
		return clrFlag;
	}

	public void setClrFlag(Boolean clrFlag)
	{
		this.clrFlag = clrFlag;
	}

	public boolean isSaleFlag()
	{
		return saleFlag;
	}

	public void setSaleFlag(boolean saleFlag)
	{
		this.saleFlag = saleFlag;
	}

	public String getPageLink()
	{
		return pageLink;
	}

	public void setPageLink(String pageLink)
	{
		this.pageLink = pageLink;
	}

	public String getPageTitle()
	{
		return pageTitle;
	}

	public void setPageTitle(String pageTitle)
	{
		this.pageTitle = pageTitle;
	}

	public Integer getRowNum()
	{
		return rowNum;
	}

	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}

	public Integer getRetListID()
	{
		return retListID;
	}

	public void setRetListID(Integer retListID)
	{
		this.retListID = retListID;
	}

	public void setRetailAddress(String retailAddress)
	{
		this.retailAddress = retailAddress;
	}

	public Integer getRetailerListID()
	{
		return retailerListID;
	}

	public void setRetailerListID(Integer retailerListID)
	{
		this.retailerListID = retailerListID;
	}

	public Integer getMainMenuId()
	{
		return mainMenuId;
	}

	public void setMainMenuId(Integer mainMenuId)
	{
		this.mainMenuId = mainMenuId;
	}

	public Integer getLowerLimit()
	{
		return lowerLimit;
	}

	public void setLowerLimit(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	public Integer getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(Integer recordCount)
	{
		this.recordCount = recordCount;
	}

	public boolean isAnyThingPageFlag()
	{
		return anyThingPageFlag;
	}

	public void setAnyThingPageFlag(boolean anyThingPageFlag)
	{
		this.anyThingPageFlag = anyThingPageFlag;
	}

	public final String getShipmentCost()
	{
		return shipmentCost;
	}

	public final void setShipmentCost(String shipmentCost)
	{
		if(shipmentCost !=null)
		{
			
			if (!shipmentCost.contains("$")&& !ApplicationConstants.NOTAPPLICABLE.equals(shipmentCost)&& shipmentCost.matches("[0-9.]+")) {
		

				this.shipmentCost = "$" + Utility.formatDecimalValue(shipmentCost);
								
			}else{
				this.shipmentCost = shipmentCost;
			}
		}else{
			this.shipmentCost = shipmentCost;
		}

			}

}

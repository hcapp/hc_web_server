package common.pojo.shopper;

import java.util.ArrayList;

/**
 * POJO class for ProductDetailsRequest which is need for getting the product details from the retailers
 * 
 * @author Malathi
 */

public class ProductDetailsRequest {
	/**
	 * The postalcode declared as String.
	 */
	private String postalcode;
	
	/**
	 * for adding from wish list history.
	 */
	
	private Boolean isWishlstHistory;
	
	/**
	 * userRetailPreferenceID declared as Integer.
	 */
	private Integer userRetailPreferenceID;
	/**
	 * retailID declared as Integer.
	 */
	private Integer retailID;

	/**
	 * retailLocationID declared as Integer.
	 */

	private Integer retailLocationID;

	/**
	 * lowerLimit declared as Integer.
	 */
	private Integer lowerLimit;

	/**
	 * parentCategoryID declared as Integer.
	 */
	private Integer parentCategoryID;

	/**
	 * for userId.
	 */
	private Long userId;
	/**
	 * for radius
	 */
	private Integer radius;
	/**
	 * for categoryId.
	 */
	private Integer categoryId;
	/**
	 * for lastVisitedProductNo.
	 */
	private Integer lastVisitedProductNo;
	/**
	 * for productName.
	 */
	private String productName;
	/**
	 * for productDescription.
	 */
	private String productDescription;
	/**
	 * for productId.
	 */
	private Integer productId;
	/**
	 * for productDetails.
	 */
	private ArrayList<ProductDetail> productDetails;
	
	/**
	 * for latitude.
	 */
	private Double latitude;
	
	/**
	 * for longitude.
	 */
	private Double longitude;
	
	/**
	 * for flag to know flow.
	 */
	private String addedTo;

	
	/**
	 * To get postalcode.
	 * @return the postalcode
	 */
	public String getPostalcode()
	{
		return postalcode;
	}

	/**
	 * To set postalcode.
	 * @param postalcode the postalcode to set
	 */
	public void setPostalcode(String postalcode)
	{
		this.postalcode = postalcode;
	}

	
	/**
	 * for getting lowerLimit.
	 * 
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit()
	{
		return lowerLimit;
	}

	/**
	 * for setting lowerLimit.
	 * 
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	/**
	 * for getting retailLocationID.
	 * 
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * for setting retailLocationID.
	 * 
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	/**
	 * for getting userRetailPreferenceID.
	 * 
	 * @return the userRetailPreferenceID
	 */
	public Integer getUserRetailPreferenceID()
	{
		return userRetailPreferenceID;
	}

	/**
	 * for setting userRetailPreferenceID.
	 * 
	 * @param userRetailPreferenceID
	 *            the userRetailPreferenceID to set
	 */
	public void setUserRetailPreferenceID(Integer userRetailPreferenceID)
	{
		this.userRetailPreferenceID = userRetailPreferenceID;
	}

	/**
	 * for getting retailID.
	 * 
	 * @return the retailID
	 */
	public Integer getRetailID()
	{
		return retailID;
	}

	/**
	 * for setting retailID.
	 * 
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Integer retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * for retrieving product details.
	 * 
	 * @return productDetails
	 */
	public ArrayList<ProductDetail> getProductDetails()
	{
		return productDetails;
	}

	/**
	 * for setting product details.
	 * 
	 * @param productDetails
	 *            as request.
	 */
	public void setProductDetails(ArrayList<ProductDetail> productDetails)
	{
		this.productDetails = productDetails;
	}

	/**
	 * this method for getting productId.
	 * 
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * this method for setting productId.
	 * 
	 * @param productId
	 *            as request.
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * this method for getting userId.
	 * 
	 * @return the userId
	 */
	public Long getUserId()
	{
		return userId;
	}

	public Integer getRadius() {
		return radius;
	}

	public void setRadius(Integer radius) {
		this.radius = radius;
	}

	/**
	 * this method for setting userId.
	 * 
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	/**
	 * this method for getting categoryId.
	 * 
	 * @return the categoryId
	 */
	public Integer getCategoryId()
	{
		return categoryId;
	}

	/**
	 * this method for setting category id.
	 * 
	 * @param categoryId
	 *            the categoryId to set
	 */
	public void setCategoryId(Integer categoryId)
	{
		this.categoryId = categoryId;
	}

	/**
	 * This method for getting productName.
	 * 
	 * @return productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * this method for setting product name.
	 * 
	 * @param productName
	 *            as request parameter.
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * this method for getting product description.
	 * 
	 * @return productDescription
	 */
	public String getProductDescription()
	{
		return productDescription;
	}

	/**
	 * this method for setting product description.
	 * 
	 * @param productDescription
	 *            as the request.
	 */
	public void setProductDescription(String productDescription)
	{
		this.productDescription = productDescription;
	}

	/**
	 * This method for getting lastVisitedProductNo.
	 * 
	 * @return lastVisitedProductNo
	 */
	public Integer getLastVisitedProductNo()
	{
		return lastVisitedProductNo;
	}

	/**
	 * this method for setting lastVisitedProductNo.
	 * 
	 * @param lastVisitedProductNo
	 *            as request parameter.
	 */
	public void setLastVisitedProductNo(Integer lastVisitedProductNo)
	{
		this.lastVisitedProductNo = lastVisitedProductNo;
	}

	/**
	 * for getting parentCategoryID.
	 * 
	 * @return the parentCategoryID
	 */
	public Integer getParentCategoryID()
	{
		return parentCategoryID;
	}

	/**
	 * for setting parentCategoryID.
	 * 
	 * @param parentCategoryID
	 *            the parentCategoryID to set
	 */
	public void setParentCategoryID(Integer parentCategoryID)
	{
		this.parentCategoryID = parentCategoryID;
	}

	/**This method return latitude value.
	 * @return the latitude
	 */
	public Double getLatitude()
	{
		return latitude;
	}

	/** This method set the value to latitude.
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude)
	{
		this.latitude = latitude;
	}

	/**This method return longitude value.
	 * @return the longitude
	 */
	public Double getLongitude()
	{
		return longitude;
	}

	/** This method set the value to longitude.
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude)
	{
		this.longitude = longitude;
	}

	/**This method return addedTo value.
	 * @return the addedTo
	 */
	public String getAddedTo()
	{
		return addedTo;
	}

	/** This method set the value to addedTo.
	 * @param addedTo the addedTo to set
	 */
	public void setAddedTo(String addedTo)
	{
		this.addedTo = addedTo;
	}

	/**
	 * To get isWishlstHistory.
	 * @return the isWishlstHistory
	 */
	public Boolean getIsWishlstHistory()
	{
		return isWishlstHistory;
	}

	/**
	 * To set isWishlstHistory value.
	 * @param isWishlstHistory the isWishlstHistory to set
	 */
	public void setIsWishlstHistory(Boolean isWishlstHistory)
	{
		this.isWishlstHistory = isWishlstHistory;
	}

}



package common.pojo.shopper;

/**
 * The POJO class for UserNotesDetails.
 * @author malathi_lr
 *
 */
public class UserNotesDetails {

	/**
	 * the userNotes.
	 */

	private String userNotes;

	/**
	 * the userId in request.
	 */

	private Integer userId;

	/**
	 * the userNoteId.
	 */

	private Integer userNoteId;

	/**
	 * for getting UserId.
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * for setting UserId.
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	
	/**
	 * for getting UserNotes.
	 * @return the userNotes
	 */
	public String getUserNotes()
	{
		return userNotes;
	}

	/**
	 * for setting UserNotes.
	 * @param userNotes
	 *            the userNotes to set
	 */
	public void setUserNotes(String userNotes)
	{
		if("".equals(userNotes))
		{
			this.userNotes=null;
		}else
		{
		this.userNotes = userNotes;
		}
	}

	/**
	 * for getting userNoteId.
	 * @return userNoteId The userNoteId
	 */
	public Integer getUserNoteId()
	{
		return userNoteId;
	}

	/**
	 * for setting userNoteId.
	 * @param userNoteId
	 *            The userNoteId
	 */
	public void setUserNoteId(Integer userNoteId)
	{
		this.userNoteId = userNoteId;
	}

}

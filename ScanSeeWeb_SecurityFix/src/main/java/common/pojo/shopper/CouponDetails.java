package common.pojo.shopper;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * The class has getter and setter methods for CouponDetails.
 * 
 * @author sowjanya_d
 */
public class CouponDetails extends BaseObject
{
	
	private Boolean couponProductExist;
	private Integer couponListID;
	/**
	 * for retailer address
	 */
	private String address;
	/**
	 * for retailer city
	 */
	private String city;
	/**
	 * for retailer state
	 */
	private String state;
	/**
	 * for retailer postal code
	 */
	private String postalCode;
	/**
	 * for used flag
	 */
	private Integer usedFlag;
	/**
	 * for expired flag
	 */
	private Integer expFlag;
	/**
	 * for user gallery coupon id
	 */
	private Integer userCouponGalleryID;
	/**
	 * for coupon share info through twitter
	 */
	private String titleText;	
	/**
	 * for coupon share info
	 */
	private String titleText2;	
	/**
	 * for userId
	 */
	private Integer userId;
	/**
	 * for user coupon claim type id
	 */
	private Integer userCouponClaimTypeID;
	/**
	 * for CouponPayoutMethod
	 */
	private String couponPayoutMethod;
	/**
	 * for RetailLocationID
	 */
	private Integer retailLocationID;
	/**
	 * for CouponClaimDate
	 */
	private String couponClaimDate;
	/**
	 * for coupon url
	 */
	
	private String couponURL;
	/**
	 * for coupon expired or not
	 */
	private Integer couponExpired;
	/**
	 * for product Id.
	 */
	private Integer productId;
	/**
	 * for coupon image path.
	 */
	private String imagePath;
	/**
	 * for product name.
	 */
	private String productName;
	/**
	 * for rowNum.
	 * 
	 * @return the rowNum
	 */
	private Integer rowNum;

	/**
	 * Variable couponId declared as of type Integer.
	 */
	private Integer couponId;
	/**
	 * Variable couponName declared as of type String.
	 */
	private String couponName;
	/**
	 * Variable couponDiscountType declared as of type String.
	 */
	private String couponDiscountType;
	/**
	 * Variable couponDiscountAmount declared as of type String.
	 */
	private String couponDiscountAmount;
	/**
	 * Variable couponDiscountPct declared as of type String.
	 */
	private String couponDiscountPct;
	/**
	 * Variable couponDescription declared as of type String.
	 */
	private String couponShortDescription;
	/**
	 * Variable couponDescription declared as of type String.
	 */
	private String couponLongDescription;
	/**
	 * Variable couponDateAdded declared as of type String.
	 */
	private String couponDateAdded;
	/**
	 * Variable couponStartDate declared as of type String.
	 */
	private String couponStartDate;
	/**
	 * Variable couponExpireDate declared as of type String.
	 */
	private String couponExpireDate;
	/**
	 * Variable usage declared as of type String.
	 */
	private String usage;

	/**
	 * Variable usage declared as of type String.
	 */
	private String couponImagePath;

	/**
	 * Variable added declared as of type String.
	 */
	private String added;
	
	private String cateName;
	
	/**
	 * for coupon description
	 */
	private String coupDesptn;
	
	/**
	 * Variable coupDateAdded declared as of type String.
	 *  
	 */
	private String coupDateAdded;
	
	/**
	 * Variable couponStartDate declared as of type String.
	 */
	private String coupStartDate;
	
	/**
	 * Variable couponExpireDate declared as of type String.
	 */
	private String coupExpireDate;
	
	/**
	 * for Fav_Flag
	 */
	private Boolean favFlag;
	
	/**
	 * for ViewableOnWeb to check coupon is added from web
	 */
	private boolean viewableOnWeb;

	/**
	 * @return the couponImagePath
	 */
	
	/**
	 * for category id
	 */
	private Integer cateId;
	/**
	 * 
	 */
	private String retailName;
	/**
	 * 
	 * @return
	 */
	
	public String getCouponImagePath()
	{
		return couponImagePath;
	}

	/**
	 * @param couponImagePath
	 *            the couponImagePath to set
	 */
	public void setCouponImagePath(String couponImagePath)
	{
		
		this.couponImagePath = couponImagePath;
		/*if (!Utility.checkNull(couponImagePath).equals(""))
		{
			this.couponImagePath = couponImagePath;
		}
		else
		{
			this.couponImagePath = ApplicationConstants.BLANKIMAGE;
		}*/
	}

	/**
	 * Gets the value of the couponId property.
	 * 
	 * @return possible object is {@link Integer }
	 */
	public Integer getCouponId()
	{
		return couponId;
	}

	/**
	 * Sets the value of the couponId property.
	 * 
	 * @param couponId
	 *            as of type Integer.
	 */
	public void setCouponId(Integer couponId)
	{
		this.couponId = couponId;
	}

	/**
	 * Gets the value of the couponName property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponName()
	{
		return couponName;
	}

	/**
	 * Sets the value of the couponName property.
	 * 
	 * @param couponName
	 *            as of type String.
	 */
	public void setCouponName(String couponName)
	{
		this.couponName = couponName;
	}

	/**
	 * Gets the value of the couponDiscountType property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponDiscountType()
	{
		return couponDiscountType;
	}

	/**
	 * Sets the value of the couponDiscountType property.
	 * 
	 * @param couponDiscountType
	 *            as of type String.
	 */
	public void setCouponDiscountType(String couponDiscountType)
	{
		this.couponDiscountType = couponDiscountType;
	}

	/**
	 * Gets the value of the couponDiscountAmount property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponDiscountAmount()
	{
		return couponDiscountAmount;
	}

	/**
	 * Sets the value of the couponDiscountAmount property.
	 * 
	 * @param couponDiscountAmount
	 *            as of type String.
	 */
	public void setCouponDiscountAmount(String couponDiscountAmount)
	{
		if(null != couponDiscountAmount)
		{
			if (!couponDiscountAmount.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(couponDiscountAmount))
			{
				this.couponDiscountAmount = "$" + Utility.formatDecimalValue(couponDiscountAmount);
				;
			}
			else
			{
				this.couponDiscountAmount = couponDiscountAmount;
			}
		}
		else
		{
			this.couponDiscountAmount = couponDiscountAmount;
		}

	}

	/**
	 * Gets the value of the couponDiscountPct property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponDiscountPct()
	{
		return couponDiscountPct;
	}

	/**
	 * Sets the value of the couponDiscountPct property.
	 * 
	 * @param couponDiscountPct
	 *            as of type String.
	 */
	public void setCouponDiscountPct(String couponDiscountPct)
	{
		this.couponDiscountPct = couponDiscountPct;
	}

	/**
	 * Gets the value of the couponDateAdded property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponDateAdded()
	{
		return couponDateAdded;
	}

	/**
	 * Sets the value of the couponDateAdded property.
	 * 
	 * @param couponDateAdded
	 *            as of type String.
	 */
	public void setCouponDateAdded(String couponDateAdded)
	{
		if (couponDateAdded != null)
		{
			this.couponDateAdded = Utility.convertDBdate(couponDateAdded);
		}
		else
		{
			this.couponDateAdded = couponDateAdded;
		}
	}

	/**
	 * Gets the value of the couponStartDate property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponStartDate()
	{
		return couponStartDate;
	}

	/**
	 * Sets the value of the couponStartDate property.
	 * 
	 * @param couponStartDate
	 *            as of type String.
	 */
	public void setCouponStartDate(String couponStartDate)
	{

		if (couponStartDate != null)
		{

			this.couponStartDate = Utility.convertDBdate(couponStartDate);

		}
		else
		{
			this.couponStartDate = couponStartDate;
		}

	}

	/**
	 * Gets the value of the couponExpireDate property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponExpireDate()
	{
		return couponExpireDate;
	}

	/**
	 * Sets the value of the couponExpireDate property.
	 * 
	 * @param couponExpireDate
	 *            as of type String.
	 */
	public void setCouponExpireDate(String couponExpireDate)
	{
		if (couponExpireDate != null)
		{
			this.couponExpireDate = Utility.convertDBdate(couponExpireDate);
		}
		else
		{
			this.couponExpireDate = couponExpireDate;
		}
	}

	/**
	 * Gets the value of the usage property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * 
	 * @param usage
	 *            as of type String.
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}

	/**
	 * @return the couponShortDescription
	 */
	public String getCouponShortDescription()
	{
		return couponShortDescription;
	}

	/**
	 * @param couponShortDescription
	 *            the couponShortDescription to set
	 */
	public void setCouponShortDescription(String couponShortDescription)
	{
		this.couponShortDescription = couponShortDescription;
	}

	/**
	 * @return the couponLongDescription
	 */
	public String getCouponLongDescription()
	{
		return couponLongDescription;
	}

	/**
	 * @param couponLongDescription
	 *            the couponLongDescription to set
	 */
	public void setCouponLongDescription(String couponLongDescription)
	{		
		this.couponLongDescription = couponLongDescription;
	}

	/**
	 * to fetch rowNum.
	 * 
	 * @return the rowNum
	 */
	public Integer getRowNum()
	{
		return rowNum;
	}

	/**
	 * to set rowNum.
	 * 
	 * @param rowNum
	 *            the rowNum to set
	 */
	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}
	/**
	 * to fetch added.
	 * 
	 * @return the added
	 */
	public String getAdded()
	{
		return added;
	}
	/**
	 * to set added.
	 * 
	 * @param added
	 *            the added to set
	 */
	public void setAdded(String added)
	{
		this.added = added;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		this.imagePath = imagePath;
	}

	public Integer getProductId()
	{
		return productId;
	}

	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	public Integer getCouponExpired()
	{
		return couponExpired;
	}

	public void setCouponExpired(Integer couponExpired)
	{
		this.couponExpired = couponExpired;
	}

	public String getCouponURL()
	{
		return couponURL;
	}

	public void setCouponURL(String couponURL)
	{
		this.couponURL = couponURL;
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public Integer getUserCouponClaimTypeID()
	{
		return userCouponClaimTypeID;
	}

	public void setUserCouponClaimTypeID(Integer userCouponClaimTypeID)
	{
		this.userCouponClaimTypeID = userCouponClaimTypeID;
	}

	public String getCouponPayoutMethod()
	{
		return couponPayoutMethod;
	}

	public void setCouponPayoutMethod(String couponPayoutMethod)
	{
		this.couponPayoutMethod = couponPayoutMethod;
	}

	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}

	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	public String getCouponClaimDate()
	{
		return couponClaimDate;
	}

	public void setCouponClaimDate(String couponClaimDate)
	{
		this.couponClaimDate = couponClaimDate;
	}

	public String getTitleText()
	{
		return titleText;
	}

	public void setTitleText(String titleText)
	{
		this.titleText = titleText;
	}

	public String getTitleText2()
	{
		return titleText2;
	}

	public void setTitleText2(String titleText2)
	{
		this.titleText2 = titleText2;
	}

	public Integer getUserCouponGalleryID()
	{
		return userCouponGalleryID;
	}

	public void setUserCouponGalleryID(Integer userCouponGalleryID)
	{
		this.userCouponGalleryID = userCouponGalleryID;
	}

	public Integer getUsedFlag()
	{
		return usedFlag;
	}

	public void setUsedFlag(Integer usedFlag)
	{
		this.usedFlag = usedFlag;
	}

	public String getCateName() {
		return cateName;
	}

	public void setCateName(String cateName) {
		this.cateName = cateName;
	}

	public String getCoupDesptn() {
		return coupDesptn;
	}

	public void setCoupDesptn(String coupDesptn) {
		this.coupDesptn = coupDesptn;
	}

	public String getCoupDateAdded() {
		return coupDateAdded;
	}

	public void setCoupDateAdded(String coupDateAdded) {
		this.coupDateAdded = coupDateAdded;
	}

	public String getCoupStartDate() {
		return coupStartDate;
	}

	public void setCoupStartDate(String coupStartDate) {
		this.coupStartDate = coupStartDate;
	}

	public String getCoupExpireDate() {
		return coupExpireDate;
	}

	public void setCoupExpireDate(String coupExpireDate) {
		this.coupExpireDate = coupExpireDate;
	}

	public Boolean getFavFlag() {
		return favFlag;
	}

	public void setFavFlag(Boolean favFlag) {
		this.favFlag = favFlag;
	}

	public boolean isViewableOnWeb()
	{
		return viewableOnWeb;
	}
	
	public void setViewableOnWeb(boolean viewableOnWeb)
	{
		this.viewableOnWeb = viewableOnWeb;
	}

	public Integer getCateId() {
		return cateId;
	}

	public void setCateId(Integer cateId) {
		this.cateId = cateId;
	}
	/**
	 * @param retailName the retailName to set
	 */
	public void setRetailName(String retailName) {
		this.retailName = retailName;
	}

	/**
	 * @return the retailName
	 */
	public String getRetailName() {
		return retailName;
	}

	/**
	 * @param expFlag the expFlag to set
	 */
	public void setExpFlag(Integer expFlag) {
		this.expFlag = expFlag;
	}

	/**
	 * @return the expFlag
	 */
	public Integer getExpFlag() {
		return expFlag;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Integer getCouponListID()
	{
		return couponListID;
	}

	public void setCouponListID(Integer couponListID)
	{
		this.couponListID = couponListID;
	}

	public Boolean getCouponProductExist()
	{
		return couponProductExist;
	}

	public void setCouponProductExist(Boolean couponProductExist)
	{
		this.couponProductExist = couponProductExist;
	}
	
	

}

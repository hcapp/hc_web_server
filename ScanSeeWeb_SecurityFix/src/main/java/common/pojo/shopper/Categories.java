/**
 * Project     : ScanSeeWeb
 * File        : Categories.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 16th Feburary 2012
 */

package common.pojo.shopper;
import java.util.List;




/**
 * @author kumar_dodda
 *
 */
public class Categories
{
	/**
	 * Variable userId declared as Integer.
	 */
	private Integer userId;
	/**
	 * Variable category declared as List.
	 */
	private List<Category> categoryInfo;
	/**
	 * Gets the value of the userId property.
	 * 
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}
	/**
	 * Sets the value of the userId property.
	 * 
	 * @param userId
	 *            as of type Integer.
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	/**
	 * @return the categoryInfo
	 */
	public List<Category> getCategoryInfo()
	{
		return categoryInfo;
	}
	/**
	 * @param categoryInfo the categoryInfo to set
	 */
	public void setCategoryInfo(List<Category> categoryInfo)
	{
		this.categoryInfo = categoryInfo;
	}
}
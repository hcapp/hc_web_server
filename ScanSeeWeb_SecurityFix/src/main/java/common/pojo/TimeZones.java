package common.pojo;
/**
 * This POJO getting various time zones from the data base for add hotdeals screen.
 * @author shyamsundara_hm
 *
 */
public class TimeZones
{
	/**
	 * for time zone name
	 */
	private String timeZoneName;
	/**
	 * for time zone id.
	 */
	private Integer timeZoneId;
	public String getTimeZoneName()
	{
		return timeZoneName;
	}
	public void setTimeZoneName(String timeZoneName)
	{
		this.timeZoneName = timeZoneName;
	}
	public Integer getTimeZoneId()
	{
		return timeZoneId;
	}
	public void setTimeZoneId(Integer timeZoneId)
	{
		this.timeZoneId = timeZoneId;
	}

}

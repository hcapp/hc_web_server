/**
 * Project     : Scan See
 * File        : NonProfitPartner.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 03th January 2012
 */
package common.pojo;

/**
 * Bean is used in NonProfitPartner details.
 * @author kumar_dodda
 */
public class NonProfitPartner
{
	/**
	 * 
	 */
	private Long nonProfitPartnerID;
	/**
	 * 
	 */
	private String nonProfitPartnerName;
	/**
	 * 
	 */
	private int activeFlag;
	/**
	 * 
	 */
	private String dateCreated;
	/**
	 * 
	 */
	private String dateModified;

	/**
	 * 
	 */
	public NonProfitPartner()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param nonProfitPartnerID
	 * @param nonProfitPartnerName
	 * @param activeFlag
	 * @param dateCreated
	 * @param dateModified
	 */
	public NonProfitPartner(Long nonProfitPartnerID, String nonProfitPartnerName, int activeFlag, String dateCreated, String dateModified)
	{
		super();
		this.nonProfitPartnerID = nonProfitPartnerID;
		this.nonProfitPartnerName = nonProfitPartnerName;
		this.activeFlag = activeFlag;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
	}

	/**
	 * @return the nonProfitPartnerID
	 */
	public Long getNonProfitPartnerID()
	{
		return nonProfitPartnerID;
	}

	/**
	 * @param nonProfitPartnerID
	 *            the nonProfitPartnerID to set
	 */
	public void setNonProfitPartnerID(Long nonProfitPartnerID)
	{
		this.nonProfitPartnerID = nonProfitPartnerID;
	}

	/**
	 * @return the nonProfitPartnerName
	 */
	public String getNonProfitPartnerName()
	{
		return nonProfitPartnerName;
	}

	/**
	 * @param nonProfitPartnerName
	 *            the nonProfitPartnerName to set
	 */
	public void setNonProfitPartnerName(String nonProfitPartnerName)
	{
		this.nonProfitPartnerName = nonProfitPartnerName;
	}

	/**
	 * @return the activeFlag
	 */
	public int getActiveFlag()
	{
		return activeFlag;
	}

	/**
	 * @param activeFlag
	 *            the activeFlag to set
	 */
	public void setActiveFlag(int activeFlag)
	{
		this.activeFlag = activeFlag;
	}

	/**
	 * @return the dateCreated
	 */
	public String getDateCreated()
	{
		return dateCreated;
	}

	/**
	 * @param dateCreated
	 *            the dateCreated to set
	 */
	public void setDateCreated(String dateCreated)
	{
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the dateModified
	 */
	public String getDateModified()
	{
		return dateModified;
	}

	/**
	 * @param dateModified
	 *            the dateModified to set
	 */
	public void setDateModified(String dateModified)
	{
		this.dateModified = dateModified;
	}

}

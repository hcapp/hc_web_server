/**
 * Project     : Scan See
 * File        : Coupon.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 26th December 2011
 */
package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.constatns.ApplicationConstants;
import common.util.Utility;


/**
 * Bean used in Coupon.
 * 
 * @author Created by SPAN.
 */
public class Coupon
{
	/**
	 * 
	 */
	private String couponStartTime;
	/**
	 * 
	 */
	private String couponEndTime;
	/**
	 * 
	 */
	private CommonsMultipartFile imageFile;
	/**
	 * 
	 */
	private String couponDiscountTypehidden;
	/**
	 * 
	 */
	private String strPos;
	/**
	 * 
	 */
	private Long couponID;
	/**
	 * 
	 */
	private Long retailID;
	/**
	 * 
	 */
	private Long manufacturerID;
	/**
	 * 
	 */
	private Long apiPartnerID;
	/**
	 * 
	 */
	private String couponName;
	/**
	 * 
	 */
	private Long numOfCouponIssue;
	/**
	 * 
	 */
	private String locationID;
	/**
	 * 
	 */
	private String retLocationIDs;
	/**
	 * 
	 */
	private String couponDiscountType;
	/**
	 * 
	 */
	private String couponDiscountAmt;
	/**
	 * 
	 */
	private double couponDiscountPct;
	/**
	 * 
	 */
	private String couponShortDesc;
	/**
	 * 
	 */
	private String couponLongDesc;
	/**
	 * 
	 */
	private String couponImagePath;
	/**
	 * 
	 */
	private String couponTermsCondt;
	/**
	 * 
	 */
	private String couponDateAdded;
	/**
	 * 
	 */
	private String couponStartDate;
	/**
	 * 
	 */
	private String couponExpireDate;
	/**
	 * 
	 */
	private String couponDisplayExpiratnDate;
	/**
	 * 
	 */
	private String couponStartTimeHrs;
	/**
	 * 
	 */
	private String couponEndTimeHrs;
	/**
	 * 
	 */
	private String couponStartTimeMins;
	/**
	 * 
	 */
	private String couponEndTimeMins;
	/**
	 * 
	 */
	private String couponURL;
	/**
	 * 
	 */
	private int externalCoupon;
	/**
	 * 
	 */
	private String claimType;

	/**
	 * 
	 */
	private Retailer retailer;
	/**
	 * 
	 */
	private Manufacture manufacture;
	/**
	 * 
	 */
	private Product product;
	/**
	 * 
	 */
	private Users user;
	/**
	 * 
	 */
	private String productName; 
	/**
	 * 
	 */
	private String income; 
	/**
	 * 
	 */
	private String productID;
	/**
	 * 
	 */
	private String scanCode; 
	/**
	 * 
	 */
	private Long retLocationID;
	/**
	 * 
	 */
	private String productImagePath;
	/**
	 * 
	 */
	private String couponImgPath;
	/**
	 * 
	 */
	private String viewName;
	/**
	 * 
	 */
	private String productshortDescription;
	/**
	 * 
	 */
	private String couponDiscountPercentage;
	/**
	 * 
	 */
	private String backButton;
	private String recordCount;
	
	public String getScanCode()
	{
		return scanCode;
	}

	public void setScanCode(String scanCode)
	{
		this.scanCode = scanCode;
	}


	/**
	 * 
	 * for time zones list
	 */
	private Integer timeZoneId;
	
	
	public Long getRetLocationID() {
		return retLocationID;
	}

	public void setRetLocationID(Long retLocationID) {
		this.retLocationID = retLocationID;
	}

	private String retailLocationID;
	
	
	public String getRetailLocationID() {
		return retailLocationID;
	}

	public void setRetailLocationID(String retailLocationID) {
		this.retailLocationID = retailLocationID;
	}

	public Coupon()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param couponID
	 * @param retailID
	 * @param manufacturerID
	 * @param apiPartnerID
	 * @param couponName
	 * @param numOfCouponIssue
	 * @param locationID
	 * @param couponDiscountType
	 * @param couponDiscountAmt
	 * @param couponDiscountPct
	 * @param couponShortDesc
	 * @param couponLongDesc
	 * @param couponImagePath
	 * @param couponTermsCondt
	 * @param couponDateAdded
	 * @param couponStartDate
	 * @param couponExpireDate
	 * @param couponDisplayExpiratnDate
	 * @param couponStartTimeHrs
	 * @param couponEndTimeHrs
	 * @param couponStartTimeMins
	 * @param couponEndTimeMins
	 * @param couponURL
	 * @param externalCoupon
	 * @param claimType
	 * @param retailer
	 * @param manufacture
	 * @param product
	 * @param user
	 * @param productName
	 * @param income
	 */
	public Coupon(Long couponID, Long retailID, Long manufacturerID, Long apiPartnerID, String couponName, Long numOfCouponIssue, String locationID,
			String couponDiscountType, String couponDiscountAmt, double couponDiscountPct, String couponShortDesc, String couponLongDesc,
			String couponImagePath, String couponTermsCondt, String couponDateAdded, String couponStartDate, String couponExpireDate,
			String couponDisplayExpiratnDate, String couponStartTimeHrs, String couponEndTimeHrs, String couponStartTimeMins,
			String couponEndTimeMins, String couponURL, int externalCoupon, String claimType,
			Retailer retailer, Manufacture manufacture, Product product, Users user, String productName, String income)
	{
		super();
		this.couponID = couponID;
		this.retailID = retailID;
		this.manufacturerID = manufacturerID;
		this.apiPartnerID = apiPartnerID;
		this.couponName = couponName;
		this.numOfCouponIssue = numOfCouponIssue;
		this.locationID = locationID;
		this.couponDiscountType = couponDiscountType;
		this.couponDiscountAmt = couponDiscountAmt;
		this.couponDiscountPct = couponDiscountPct;
		this.couponShortDesc = couponShortDesc;
		this.couponLongDesc = couponLongDesc;
		this.couponImagePath = couponImagePath;
		this.couponTermsCondt = couponTermsCondt;
		this.couponDateAdded = couponDateAdded;
		this.couponStartDate = couponStartDate;
		this.couponExpireDate = couponExpireDate;
		this.couponDisplayExpiratnDate = couponDisplayExpiratnDate;
		this.couponStartTimeHrs = couponStartTimeHrs;
		this.couponEndTimeHrs = couponEndTimeHrs;
		this.couponStartTimeMins = couponStartTimeMins;
		this.couponEndTimeMins = couponEndTimeMins;
		this.couponURL = couponURL;
		this.externalCoupon = externalCoupon;
		this.claimType = claimType;
		this.retailer = retailer;
		this.manufacture = manufacture;
		this.product = product;
		this.user = user;
		this.productName = productName;
		this.income = income;
	}

	/**
	 * @return the couponID
	 */
	public Long getCouponID()
	{
		return couponID;
	}

	/**
	 * @param couponID
	 *            the couponID to set
	 */
	public void setCouponID(Long couponID)
	{
		this.couponID = couponID;
	}

	/**
	 * @return the retailID
	 */
	public Long getRetailID()
	{
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Long retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * @return the manufacturerID
	 */
	public Long getManufacturerID()
	{
		return manufacturerID;
	}

	/**
	 * @param manufacturerID
	 *            the manufacturerID to set
	 */
	public void setManufacturerID(Long manufacturerID)
	{
		this.manufacturerID = manufacturerID;
	}

	/**
	 * @return the apiPartnerID
	 */
	public Long getApiPartnerID()
	{
		return apiPartnerID;
	}

	/**
	 * @param apiPartnerID
	 *            the apiPartnerID to set
	 */
	public void setApiPartnerID(Long apiPartnerID)
	{
		this.apiPartnerID = apiPartnerID;
	}

	/**
	 * @return the couponName
	 */
	public String getCouponName()
	{
		return couponName;
	}

	/**
	 * @param couponName
	 *            the couponName to set
	 */
	public void setCouponName(String couponName)
	{
		this.couponName = couponName;
	}

	/**
	 * @return the numOfCouponIssue
	 */
	public Long getNumOfCouponIssue()
	{
		return numOfCouponIssue;
	}

	/**
	 * @param numOfCouponIssue
	 *            the numOfCouponIssue to set
	 */
	public void setNumOfCouponIssue(Long numOfCouponIssue)
	{
		this.numOfCouponIssue = numOfCouponIssue;
	}

	/**
	 * @return the locationID
	 */
	public String getLocationID()
	{
		return locationID;
	}

	/**
	 * @param locationID
	 *            the locationID to set
	 */
	public void setLocationID(String locationID)
	{
		this.locationID = locationID;
	}

	/**
	 * @return the couponDiscountType
	 */
	public String getCouponDiscountType()
	{
		return couponDiscountType;
	}

	/**
	 * @param couponDiscountType
	 *            the couponDiscountType to set
	 */
	public void setCouponDiscountType(String couponDiscountType)
	{
		this.couponDiscountType = couponDiscountType;
	}

	/**
	 * @return the couponDiscountAmt
	 */
	public String getCouponDiscountAmt()
	{
		return couponDiscountAmt;
	}

	/**
	 * @param couponDiscountAmt
	 *            the couponDiscountAmt to set
	 */
	public void setCouponDiscountAmt(String couponDiscountAmt)
	{
		this.couponDiscountAmt = couponDiscountAmt;
	}

	/**
	 * @return the couponDiscountPct
	 */
	public double getCouponDiscountPct()
	{
		return couponDiscountPct;
	}

	
	public String getCouponStartTime() {
		return couponStartTime;
	}

	public void setCouponStartTime(String couponStartTime) {
		this.couponStartTime = couponStartTime;
	}

	public String getCouponEndTime() {
		return couponEndTime;
	}

	public void setCouponEndTime(String couponEndTime) {
		this.couponEndTime = couponEndTime;
	}

	/**
	 * @param couponDiscountPct
	 *            the couponDiscountPct to set
	 */
	public void setCouponDiscountPct(double couponDiscountPct)
	{
		this.couponDiscountPct = couponDiscountPct;
	}

	/**
	 * @return the couponShortDesc
	 */
	public String getCouponShortDesc()
	{
		return couponShortDesc;
	}

	/**
	 * @param couponShortDesc
	 *            the couponShortDesc to set
	 */
	public void setCouponShortDesc(String couponShortDesc)
	{
		this.couponShortDesc = couponShortDesc;
	}

	/**
	 * @return the couponLongDesc
	 */
	public String getCouponLongDesc()
	{
		return couponLongDesc;
	}

	/**
	 * @param couponLongDesc
	 *            the couponLongDesc to set
	 */
	public void setCouponLongDesc(String couponLongDesc)
	{
		this.couponLongDesc = couponLongDesc;
	}

	/**
	 * @return the couponImagePath
	 */
	public String getCouponImagePath()
	{
		return couponImagePath;
	}

	/**
	 * @param couponImagePath
	 *            the couponImagePath to set
	 */
	public void setCouponImagePath(String couponImagePath)
	{
		if (!Utility.checkNull(couponImagePath).equals(""))
		{
			this.couponImagePath = couponImagePath;
		}
		else
		{
			this.couponImagePath = ApplicationConstants.BLANKIMAGE;
		}
	}

	/**
	 * @return the couponTermsCondt
	 */
	public String getCouponTermsCondt()
	{
		return couponTermsCondt;
	}

	/**
	 * @param couponTermsCondt
	 *            the couponTermsCondt to set
	 */
	public void setCouponTermsCondt(String couponTermsCondt)
	{
		this.couponTermsCondt = couponTermsCondt;
	}

	/**
	 * @return the couponDateAdded
	 */
	public String getCouponDateAdded()
	{
		return couponDateAdded;
	}

	/**
	 * @param couponDateAdded
	 *            the couponDateAdded to set
	 */
	public void setCouponDateAdded(String couponDateAdded)
	{
		this.couponDateAdded = couponDateAdded;
	}

	/**
	 * @return the couponStartDate
	 */
	public String getCouponStartDate()
	{
		return couponStartDate;
	}

	/**
	 * @param couponStartDate
	 *            the couponStartDate to set
	 */
	public void setCouponStartDate(String couponStartDate)
	{
		this.couponStartDate = couponStartDate;
	}

	/**
	 * @return the couponExpireDate
	 */
	public String getCouponExpireDate()
	{
		return couponExpireDate;
	}

	/**
	 * @param couponExpireDate
	 *            the couponExpireDate to set
	 */
	public void setCouponExpireDate(String couponExpireDate)
	{
		this.couponExpireDate = couponExpireDate;
	}

	/**
	 * @return the couponDisplayExpiratnDate
	 */
	public String getCouponDisplayExpiratnDate()
	{
		return couponDisplayExpiratnDate;
	}

	/**
	 * @param couponDisplayExpiratnDate
	 *            the couponDisplayExpiratnDate to set
	 */
	public void setCouponDisplayExpiratnDate(String couponDisplayExpiratnDate)
	{
		this.couponDisplayExpiratnDate = couponDisplayExpiratnDate;
	}

	/**
	 * @return the couponStartTimeHrs
	 */
	public String getCouponStartTimeHrs()
	{
		return couponStartTimeHrs;
	}

	/**
	 * @param couponStartTimeHrs the couponStartTimeHrs to set
	 */
	public void setCouponStartTimeHrs(String couponStartTimeHrs)
	{
		this.couponStartTimeHrs = couponStartTimeHrs;
	}

	/**
	 * @return the couponEndTimeHrs
	 */
	public String getCouponEndTimeHrs()
	{
		return couponEndTimeHrs;
	}

	/**
	 * @param couponEndTimeHrs the couponEndTimeHrs to set
	 */
	public void setCouponEndTimeHrs(String couponEndTimeHrs)
	{
		this.couponEndTimeHrs = couponEndTimeHrs;
	}

	/**
	 * @return the couponStartTimeMins
	 */
	public String getCouponStartTimeMins()
	{
		return couponStartTimeMins;
	}

	/**
	 * @param couponStartTimeMins the couponStartTimeMins to set
	 */
	public void setCouponStartTimeMins(String couponStartTimeMins)
	{
		this.couponStartTimeMins = couponStartTimeMins;
	}

	/**
	 * @return the couponEndTimeMins
	 */
	public String getCouponEndTimeMins()
	{
		return couponEndTimeMins;
	}

	/**
	 * @param couponEndTimeMins the couponEndTimeMins to set
	 */
	public void setCouponEndTimeMins(String couponEndTimeMins)
	{
		this.couponEndTimeMins = couponEndTimeMins;
	}

	/**
	 * @return the couponURL
	 */
	public String getCouponURL()
	{
		return couponURL;
	}

	/**
	 * @param couponURL
	 *            the couponURL to set
	 */
	public void setCouponURL(String couponURL)
	{
		this.couponURL = couponURL;
	}

	/**
	 * @return the externalCoupon
	 */
	public int getExternalCoupon()
	{
		return externalCoupon;
	}

	/**
	 * @param externalCoupon
	 *            the externalCoupon to set
	 */
	public void setExternalCoupon(int externalCoupon)
	{
		this.externalCoupon = externalCoupon;
	}

	/**
	 * @return the claimType
	 */
	public String getClaimType()
	{
		return claimType;
	}

	/**
	 * @param claimType
	 *            the claimType to set
	 */
	public void setClaimType(String claimType)
	{
		this.claimType = claimType;
	}

	/**
	 * @return the retailer
	 */
	public Retailer getRetailer()
	{
		return retailer;
	}

	/**
	 * @param retailer
	 *            the retailer to set
	 */
	public void setRetailer(Retailer retailer)
	{
		this.retailer = retailer;
	}

	/**
	 * @return the manufacture
	 */
	public Manufacture getManufacture()
	{
		return manufacture;
	}

	/**
	 * @param manufacture
	 *            the manufacture to set
	 */
	public void setManufacture(Manufacture manufacture)
	{
		this.manufacture = manufacture;
	}

	/**
	 * @return the product
	 */
	public Product getProduct()
	{
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(Product product)
	{
		this.product = product;
	}

	/**
	 * @return the user
	 */
	public Users getUser()
	{
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(Users user)
	{
		this.user = user;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}


	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}


	/**
	 * @return the income
	 */
	public String getIncome()
	{
		return income;
	}


	/**
	 * @param income the income to set
	 */
	public void setIncome(String income)
	{
		this.income = income;
	}

	public CommonsMultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(CommonsMultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public Integer getTimeZoneId()
	{
		return timeZoneId;
	}

	public void setTimeZoneId(Integer timeZoneId)
	{
		this.timeZoneId = timeZoneId;
	}




	
	public String getStrPos() {
		return strPos;

	}

	public void setStrPos(String strPos) {
		this.strPos = strPos;
	}

	public void setCouponDiscountTypehidden(String couponDiscountTypehidden) {
		this.couponDiscountTypehidden = couponDiscountTypehidden;
	}

	public String getCouponDiscountTypehidden() {
		return couponDiscountTypehidden;
	}

	/**
	 * @return the productID
	 */
	public String getProductID() {
		return productID;
	}

	/**
	 * @param productID the productID to set
	 */
	public void setProductID(String productID) {
		this.productID = productID;
	}

	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath() {
		return productImagePath;
	}

	/**
	 * @param productImagePath the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath) {
		if (null==productImagePath || "".equals(productImagePath)){
			this.productImagePath = ApplicationConstants.BLANKIMAGE;
		}
		else{	
		this.productImagePath = productImagePath;
		}
	}


	/**
	 * @return the productshortDescription
	 */
	public String getProductshortDescription() {
		return productshortDescription;
	}

	/**
	 * @param productshortDescription the productshortDescription to set
	 */
	public void setProductshortDescription(String productshortDescription) {
		this.productshortDescription = productshortDescription;
	}

	/**
	 * @return the couponImgPath
	 */
	public String getCouponImgPath()
	{
		return couponImgPath;
	}

	/**
	 * @param couponImgPath the couponImgPath to set
	 */
	public void setCouponImgPath(String couponImgPath)
	{
		this.couponImgPath = couponImgPath;
		if ("".equals(Utility.checkNull(couponImgPath))) {
			// Changed below code as IE browser showing broken image if it did not find image.
			this.couponImgPath = ApplicationConstants.BLANKIMAGE;
		} else {
			this.couponImgPath = couponImgPath;
		}
	}

	/**
	 * @return the viewName
	 */
	public String getViewName()
	{
		return viewName;
	}

	/**
	 * @param viewName the viewName to set
	 */
	public void setViewName(String viewName)
	{
		this.viewName = viewName;
	}

	/**
	 * @return the couponDiscountPercentage
	 */
	public String getCouponDiscountPercentage()
	{
		return couponDiscountPercentage;
	}

	/**
	 * @param couponDiscountPercentage the couponDiscountPercentage to set
	 */
	public void setCouponDiscountPercentage(String couponDiscountPercentage)
	{
		this.couponDiscountPercentage = couponDiscountPercentage;
	}

	/**
	 * @return the retLocationIDs
	 */
	public String getRetLocationIDs()
	{
		return retLocationIDs;
	}

	/**
	 * @param retLocationIDs the retLocationIDs to set
	 */
	public void setRetLocationIDs(String retLocationIDs)
	{
		this.retLocationIDs = retLocationIDs;
	}

	/**
	 * @return the recordCount
	 */
	public String getRecordCount()
	{
		return recordCount;
	}

	/**
	 * @param recordCount the recordCount to set
	 */
	public void setRecordCount(String recordCount)
	{
		this.recordCount = recordCount;
	}

	/**
	 * @return the backButton
	 */
	public String getBackButton()
	{
		return backButton;
	}

	/**
	 * @param backButton the backButton to set
	 */
	public void setBackButton(String backButton)
	{
		this.backButton = backButton;
	}

	
	
}

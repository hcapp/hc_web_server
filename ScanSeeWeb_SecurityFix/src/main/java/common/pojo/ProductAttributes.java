/**
 * 
 */
package common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class ProductAttributes {
	/**
	 * 
	 */
	private long prodAttributesID;
	/**
	 * 
	 */
	private long productID;
	/**
	 * 
	 */
	private String prodAttributeName;
	/**
	 * 
	 */
	private String prodDisplayValue; 
	
	/**
	 * 
	 */
	private Product product;
	/**
	 * 
	 */
	public ProductAttributes() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param prodAttributesID
	 * @param prodAttributeName
	 * @param prodDisplayValue
	 * @param product
	 */
	public ProductAttributes(long prodAttributesID, String prodAttributeName,
			String prodDisplayValue, Product product) {
		super();
		this.prodAttributesID = prodAttributesID;
		this.prodAttributeName = prodAttributeName;
		this.prodDisplayValue = prodDisplayValue;
		this.product = product;
	}

	/**
	 * @return the prodAttributesID
	 */
	public long getProdAttributesID() {
		return prodAttributesID;
	}

	/**
	 * @param prodAttributesID the prodAttributesID to set
	 */
	public void setProdAttributesID(long prodAttributesID) {
		this.prodAttributesID = prodAttributesID;
	}
	/**
	 * @return the productID
	 */
	public long getProductID() {
		if (product != null) {
			this.productID = product.getProductID();
		}
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(long productID) {
		this.productID = productID;
	}
	/**
	 * @return the productMediaID
	 */

	/**
	 * @return the prodAttributeName
	 */
	public String getProdAttributeName() {
		return prodAttributeName;
	}

	/**
	 * @param prodAttributeName the prodAttributeName to set
	 */
	public void setProdAttributeName(String prodAttributeName) {
		this.prodAttributeName = prodAttributeName;
	}

	/**
	 * @return the prodDisplayValue
	 */
	public String getProdDisplayValue() {
		return prodDisplayValue;
	}

	/**
	 * @param prodDisplayValue the prodDisplayValue to set
	 */
	public void setProdDisplayValue(String prodDisplayValue) {
		this.prodDisplayValue = prodDisplayValue;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
}

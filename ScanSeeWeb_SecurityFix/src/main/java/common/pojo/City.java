/**
 * @ (#) City.java 21-Dec-2011
 * Project       :ScanSeeWeb
 * File          : City.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 21-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package common.pojo;

public class City {
	private String geoPositionID;
	private String cityName;
	/**
	 * for PopulationCenterID
	 */
	private Integer populationCenterID;
	/**
	 * for DMAName.
	 */
	private String dMAName;
	

	public String getGeoPositionID() {
		return geoPositionID;
	}

	public void setGeoPositionID(String geoPositionID) {
		this.geoPositionID = geoPositionID;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Integer getPopulationCenterID()
	{
		return populationCenterID;
	}

	public void setPopulationCenterID(Integer populationCenterID)
	{
		this.populationCenterID = populationCenterID;
	}

	public String getdMAName()
	{
		return dMAName;
	}

	public void setdMAName(String dMAName)
	{
		this.dMAName = dMAName;
	}

}

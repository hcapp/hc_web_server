package common.pojo;

public class ShopperRegistrationInfo
{

	private String name;
	private String address1;
	private String address2;
	private String address3;
	private String city;
	private String postalCode;
	private String state;
	private String phone;
	private String legalAuthorityName;
	private String contactName;
	private String contactPhone;
	private String contactEmail;
	private boolean checkbox;
	
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getAddress1()
	{
		return address1;
	}
	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}
	public String getAddress2()
	{
		return address2;
	}
	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}
	public String getAddress3()
	{
		return address3;
	}
	public void setAddress3(String address3)
	{
		this.address3 = address3;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public String getPostalCode()
	{
		return postalCode;
	}
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getPhone()
	{
		return phone;
	}
	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	public String getLegalAuthorityName()
	{
		return legalAuthorityName;
	}
	public void setLegalAuthorityName(String legalAuthorityName)
	{
		this.legalAuthorityName = legalAuthorityName;
	}
	public String getContactName()
	{
		return contactName;
	}
	public void setContactName(String contactName)
	{
		this.contactName = contactName;
	}
	public String getContactPhone()
	{
		return contactPhone;
	}
	public void setContactPhone(String contactPhone)
	{
		this.contactPhone = contactPhone;
	}
	public String getContactEmail()
	{
		return contactEmail;
	}
	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = contactEmail;
	}
	public boolean isCheckbox()
	{
		return checkbox;
	}
	public void setCheckbox(boolean checkbox)
	{
		this.checkbox = checkbox;
	}
	
}

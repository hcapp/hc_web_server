package common.pojo;

public class SupplierQRCodeData {

	private int userId;
	
	private Integer audioID;
	
	private Integer videoID;
	
	private int productId;
	
	private String filePath;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @return the audioID
	 */
	public Integer getAudioID() {
		return audioID;
	}

	/**
	 * @return the videoID
	 */
	public Integer getVideoID() {
		return videoID;
	}

	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @param audioID the audioID to set
	 */
	public void setAudioID(Integer audioID) {
		this.audioID = audioID;
	}

	/**
	 * @param videoID the videoID to set
	 */
	public void setVideoID(Integer videoID) {
		this.videoID = videoID;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}

	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}

/**
 * @ (#) DashBoard.java 12-Feb-2012
 * Project       :ScanSeeWeb
 * File          : DashBoard.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 12-Feb-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */


package common.pojo;

public class DashBoard
{

	
	private String reportServerURL;
	
	private int userID;
	
	private String fromDate;
	
	private String toDate;
	
	private String moduleId;
	
	private String exportFormat;
	
	private String reportName;
	
	private String productID;
	
	private String productModuleId;
	
	private String id;
	
	private String moduleName;
	
	private String ssrsReportName;
	
	public int getUserID()
	{
		return userID;
	}

	public void setUserID(int userID)
	{
		this.userID = userID;
	}

	public String getReportServerURL()
	{
		return reportServerURL;
	}

	public void setReportServerURL(String reportServerURL)
	{
		this.reportServerURL = reportServerURL;
	}

	public String getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(String fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public String getModuleId()
	{
		return moduleId;
	}

	public void setModuleId(String moduleId)
	{
		this.moduleId = moduleId;
	}

	public String getExportFormat()
	{
		return exportFormat;
	}

	public void setExportFormat(String exportFormat)
	{
		this.exportFormat = exportFormat;
	}

	public String getReportName()
	{
		return reportName;
	}

	public void setReportName(String reportName)
	{
		this.reportName = reportName;
	}

	public String getProductModuleId()
	{
		return productModuleId;
	}

	public void setProductModuleId(String productModuleId)
	{
		this.productModuleId = productModuleId;
	}

	public String getProductID()
	{
		return productID;
	}

	public void setProductID(String productID)
	{
		this.productID = productID;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public String getSsrsReportName()
	{
		return ssrsReportName;
	}

	public void setSsrsReportName(String ssrsReportName)
	{
		this.ssrsReportName = ssrsReportName;
	}
	
	
}

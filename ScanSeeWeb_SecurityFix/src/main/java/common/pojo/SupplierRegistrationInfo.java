package common.pojo;
/**
 * This class used to capture supplier registration details.
 * @author shyamsundara_hm
 *
 */
public class SupplierRegistrationInfo {

	public int supplierId;
	
	/**
	 * For supplier name
	 */
	private String supplierName;
	
	/**
	 * for corporate address
	 */
	private String corpAdderess;
	
	/**
	 * for Address2
	 */
	private String Address2;
	
	/**
	 * for city
	 */
	private String city;
	/**
	 * for state.
	 */
	private String state;
	/**
	 * for state.
	 */
	private String stateHidden;
	/**
	 * for state.
	 */
	private String stateCodeHidden;
	/**
	 * for postal code
	 */
	private String postalCode;
	
	/**
	 * for corporate phone number
	 */
	private String corpPhoneNo;
	/**
	 * for legal authority first name
	 */
	private String legalAuthorityFName;
	/**
	 * for legal authority second name
	 */
	private String legalAuthorityLName;
	/**
	 * for contact first name
	 */
	private String contactFName;
	/**
	 * for contact last name
	 */
	private String contactLName;
	/**
	 * for contact phone number
	 */
	private String contactPhoneNo;
	/**
	 * for contact email
	 */
	private String contactEmail;
	/**
	 * 
	 */
	private String retypeEmail;
	/**
	 * 
	 */
	private String retypePassword;
	/**
	 * 
	 */
	private boolean acceptTerm;
	/**
	 * 
	 */
	private String password;
	/**
	 * 
	 */
	private String bCategory;
	/**
	 * 
	 */
	private boolean nonProfit;
	/**
	 * 
	 */
	private String tabIndex;
	/**
	 * 
	 */
	private String bCategoryHidden;
	/**
	 * 
	 */
	private String cityHidden;
	/**
	 * 
	 */
	
	private String  scanSeeUrl;	
	 /**
	 * 
	 */
    private String userName;
    /**
	 * 
	 */
    private String captch;
    /**
     * 
     */
    private String citySelectedFlag;
    
	public SupplierRegistrationInfo()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param supplierId
	 * @param supplierName
	 * @param corpAdderess
	 * @param address2
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param corpPhoneNo
	 * @param legalAuthorityFName
	 * @param legalAuthorityLName
	 * @param contactFName
	 * @param contactLName
	 * @param contactPhoneNo
	 * @param contactEmail
	 * @param retypeEmail
	 * @param retypePassword
	 * @param acceptTerm
	 * @param password
	 * @param bCategory
	 * @param nonProfit
	 * @param tabIndex
	 * @param bCategoryHidden
	 * @param cityHidden
	 */
	public SupplierRegistrationInfo(int supplierId, String supplierName, String corpAdderess, String address2, String city, String state,
			String postalCode, String corpPhoneNo, String legalAuthorityFName, String legalAuthorityLName, String contactFName, String contactLName,
			String contactPhoneNo, String contactEmail, String retypeEmail, String retypePassword, boolean acceptTerm, String password,
			String bCategory, boolean nonProfit, String tabIndex, String bCategoryHidden, String cityHidden)
	{
		super();
		this.supplierId = supplierId;
		this.supplierName = supplierName;
		this.corpAdderess = corpAdderess;
		Address2 = address2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.corpPhoneNo = corpPhoneNo;
		this.legalAuthorityFName = legalAuthorityFName;
		this.legalAuthorityLName = legalAuthorityLName;
		this.contactFName = contactFName;
		this.contactLName = contactLName;
		this.contactPhoneNo = contactPhoneNo;
		this.contactEmail = contactEmail;
		this.retypeEmail = retypeEmail;
		this.retypePassword = retypePassword;
		this.acceptTerm = acceptTerm;
		this.password = password;
		this.bCategory = bCategory;
		this.nonProfit = nonProfit;
		this.tabIndex = tabIndex;
		this.bCategoryHidden = bCategoryHidden;
		this.cityHidden = cityHidden;
	}
	/**
	 * @return the supplierId
	 */
	public int getSupplierId()
	{
		return supplierId;
	}
	/**
	 * @param supplierId the supplierId to set
	 */
	public void setSupplierId(int supplierId)
	{
		this.supplierId = supplierId;
	}
	/**
	 * @return the supplierName
	 */
	public String getSupplierName()
	{
		return supplierName;
	}
	/**
	 * @param supplierName the supplierName to set
	 */
	public void setSupplierName(String supplierName)
	{
		this.supplierName = supplierName;
	}
	/**
	 * @return the corpAdderess
	 */
	public String getCorpAdderess()
	{
		return corpAdderess;
	}
	/**
	 * @param corpAdderess the corpAdderess to set
	 */
	public void setCorpAdderess(String corpAdderess)
	{
		this.corpAdderess = corpAdderess;
	}
	/**
	 * @return the address2
	 */
	public String getAddress2()
	{
		return Address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2)
	{
		Address2 = address2;
	}
	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}
	/**
	 * @return the corpPhoneNo
	 */
	public String getCorpPhoneNo()
	{
		return corpPhoneNo;
	}
	/**
	 * @param corpPhoneNo the corpPhoneNo to set
	 */
	public void setCorpPhoneNo(String corpPhoneNo)
	{
		this.corpPhoneNo = corpPhoneNo;
	}
	/**
	 * @return the legalAuthorityFName
	 */
	public String getLegalAuthorityFName()
	{
		return legalAuthorityFName;
	}
	/**
	 * @param legalAuthorityFName the legalAuthorityFName to set
	 */
	public void setLegalAuthorityFName(String legalAuthorityFName)
	{
		this.legalAuthorityFName = legalAuthorityFName;
	}
	/**
	 * @return the legalAuthorityLName
	 */
	public String getLegalAuthorityLName()
	{
		return legalAuthorityLName;
	}
	/**
	 * @param legalAuthorityLName the legalAuthorityLName to set
	 */
	public void setLegalAuthorityLName(String legalAuthorityLName)
	{
		this.legalAuthorityLName = legalAuthorityLName;
	}
	/**
	 * @return the contactFName
	 */
	public String getContactFName()
	{
		return contactFName;
	}
	/**
	 * @param contactFName the contactFName to set
	 */
	public void setContactFName(String contactFName)
	{
		this.contactFName = contactFName;
	}
	/**
	 * @return the contactLName
	 */
	public String getContactLName()
	{
		return contactLName;
	}
	/**
	 * @param contactLName the contactLName to set
	 */
	public void setContactLName(String contactLName)
	{
		this.contactLName = contactLName;
	}
	/**
	 * @return the contactPhoneNo
	 */
	public String getContactPhoneNo()
	{
		return contactPhoneNo;
	}
	/**
	 * @param contactPhoneNo the contactPhoneNo to set
	 */
	public void setContactPhoneNo(String contactPhoneNo)
	{
		this.contactPhoneNo = contactPhoneNo;
	}
	/**
	 * @return the contactEmail
	 */
	public String getContactEmail()
	{
		return contactEmail;
	}
	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = contactEmail;
	}
	/**
	 * @return the retypeEmail
	 */
	public String getRetypeEmail()
	{
		return retypeEmail;
	}
	/**
	 * @param retypeEmail the retypeEmail to set
	 */
	public void setRetypeEmail(String retypeEmail)
	{
		this.retypeEmail = retypeEmail;
	}
	/**
	 * @return the retypePassword
	 */
	public String getRetypePassword()
	{
		return retypePassword;
	}
	/**
	 * @param retypePassword the retypePassword to set
	 */
	public void setRetypePassword(String retypePassword)
	{
		this.retypePassword = retypePassword;
	}
	/**
	 * @return the acceptTerm
	 */
	public boolean isAcceptTerm()
	{
		return acceptTerm;
	}
	/**
	 * @param acceptTerm the acceptTerm to set
	 */
	public void setAcceptTerm(boolean acceptTerm)
	{
		this.acceptTerm = acceptTerm;
	}
	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}
	/**
	 * @return the bCategory
	 */
	public String getbCategory()
	{
		return bCategory;
	}
	/**
	 * @param bCategory the bCategory to set
	 */
	public void setbCategory(String bCategory)
	{
		this.bCategory = bCategory;
	}
	/**
	 * @return the nonProfit
	 */
	public boolean isNonProfit()
	{
		return nonProfit;
	}
	/**
	 * @param nonProfit the nonProfit to set
	 */
	public void setNonProfit(boolean nonProfit)
	{
		this.nonProfit = nonProfit;
	}
	/**
	 * @return the tabIndex
	 */
	public String getTabIndex()
	{
		return tabIndex;
	}
	/**
	 * @param tabIndex the tabIndex to set
	 */
	public void setTabIndex(String tabIndex)
	{
		this.tabIndex = tabIndex;
	}
	/**
	 * @return the bCategoryHidden
	 */
	public String getbCategoryHidden()
	{
		return bCategoryHidden;
	}
	/**
	 * @param bCategoryHidden the bCategoryHidden to set
	 */
	public void setbCategoryHidden(String bCategoryHidden)
	{
		this.bCategoryHidden = bCategoryHidden;
	}
	/**
	 * @return the cityHidden
	 */
	public String getCityHidden()
	{
		return cityHidden;
	}
	/**
	 * @param cityHidden the cityHidden to set
	 */
	public void setCityHidden(String cityHidden)
	{
		this.cityHidden = cityHidden;
	}
	/**
	 * @return the scanSeeUrl
	 */
	public String getScanSeeUrl() {
		return scanSeeUrl;
	}
	/**
	 * @param scanSeeUrl the scanSeeUrl to set
	 */
	public void setScanSeeUrl(String scanSeeUrl) {
		this.scanSeeUrl = scanSeeUrl;
	}
	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getCaptch()
	{
		return captch;
	}
	public void setCaptch(String captch)
	{
		this.captch = captch;
	}
	public String getStateHidden()
	{
		return stateHidden;
	}
	public void setStateHidden(String stateHidden)
	{
		this.stateHidden = stateHidden;
	}
	public String getStateCodeHidden()
	{
		return stateCodeHidden;
	}
	public void setStateCodeHidden(String stateCodeHidden)
	{
		this.stateCodeHidden = stateCodeHidden;
	}
	public String getCitySelectedFlag()
	{
		return citySelectedFlag;
	}
	public void setCitySelectedFlag(String citySelectedFlag)
	{
		this.citySelectedFlag = citySelectedFlag;
	}
	
}

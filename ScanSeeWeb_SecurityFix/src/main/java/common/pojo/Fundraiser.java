package common.pojo;

/**
 * POJO class for fundraiser.
 * 
 * @author dhruvanath_mm
 *
 */
public class Fundraiser {

	private String title;
	private String shortDescription;
	private String longDescription;
	private String moreINformation;
	private String fundraisingGoal;
	private String purchaseProduct;
	private String currentLevel;
}

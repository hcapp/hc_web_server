/**
 * Project     : Scan See
 * File        : UserPreferenceDisplay.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 03th January 2012
 */
package common.pojo;


import java.util.List;

import common.pojo.University;
import common.pojo.shopper.CategoryDetail;

/**
 * Bean is used in Shopper UserPreferenceDisplay details.
 * @author kumar_dodda
 */
public class UserPreferenceDisplay
{
	/**
	 * 
	 */
	private List<UserPreference> userPreferenceList;
	/**
	 * 
	 */
	private List<Category> categoryList;
	/**
	 * 
	 */
	private List<SchoolInfo> schollList; 
	/**
	 * 
	 */
	private List<University> universityList;
	/**
	 * 
	 */
	private List<NonProfitPartner> nonProfitPartnersList;
	/**
	 * 
	 */
	CategoryDetail categoryDetailresponse = new CategoryDetail();

	/**
	 * 
	 */
	public UserPreferenceDisplay()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param userPreferenceList
	 * @param categoryList
	 * @param universityList
	 * @param nonProfitPartnersList
	 */
	public UserPreferenceDisplay(List<UserPreference> userPreferenceList, List<Category> categoryList, List<University> universityList,
			List<NonProfitPartner> nonProfitPartnersList)
	{
		super();
		this.userPreferenceList = userPreferenceList;
		this.categoryList = categoryList;
		this.universityList = universityList;
		this.nonProfitPartnersList = nonProfitPartnersList;
	}

	/**
	 * @return the userPreferenceList
	 */
	public List<UserPreference> getUserPreferenceList()
	{
		return userPreferenceList;
	}

	/**
	 * @param userPreferenceList the userPreferenceList to set
	 */
	public void setUserPreferenceList(List<UserPreference> userPreferenceList)
	{
		this.userPreferenceList = userPreferenceList;
	}

	/**
	 * @return the categoryList
	 */
	public List<Category> getCategoryList()
	{
		return categoryList;
	}

	/**
	 * @param categoryList the categoryList to set
	 */
	public void setCategoryList(List<Category> categoryList)
	{
		this.categoryList = categoryList;
	}

	/**
	 * @return the universityList
	 */
	public List<University> getUniversityList()
	{
		return universityList;
	}

	/**
	 * @param universityList the universityList to set
	 */
	public void setUniversityList(List<University> universityList)
	{
		this.universityList = universityList;
	}

	/**
	 * @return the nonProfitPartnersList
	 */
	public List<NonProfitPartner> getNonProfitPartnersList()
	{
		return nonProfitPartnersList;
	}

	/**
	 * @param nonProfitPartnersList the nonProfitPartnersList to set
	 */
	public void setNonProfitPartnersList(List<NonProfitPartner> nonProfitPartnersList)
	{
		this.nonProfitPartnersList = nonProfitPartnersList;
	}
	/**
	 * @return the categoryDetailresponse
	 */
	public CategoryDetail getCategoryDetailresponse() {
		return categoryDetailresponse;
	}

	/**
	 * @param categoryDetailresponse the categoryDetailresponse to set
	 */
	public void setCategoryDetailresponse(CategoryDetail categoryDetailresponse) {
		this.categoryDetailresponse = categoryDetailresponse;
	}

	/**
	 * @param schollList the schollList to set
	 */
	public void setSchollList(List<SchoolInfo> schollList) {
		this.schollList = schollList;
	}

	/**
	 * @return the schollList
	 */
	public List<SchoolInfo> getSchollList() {
		return schollList;
	}
}

package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class UploadCroppedImageBean
{ 
	

	/*
	 * This setter method implemented for getting the Retailer Uploaded Logo. 
    */
	private CommonsMultipartFile retailerLogo;
	private int xPixel;
	private int yPixel;
	private int wPixel;
	private int hPixel;
	
	public CommonsMultipartFile getRetailerLogo()
	{
		return retailerLogo;
	}

	public void setRetailerLogo(CommonsMultipartFile retailerLogo)
	{
		this.retailerLogo = retailerLogo;
	}
	
	public int getxPixel()
	{
		return xPixel;
	}

	public void setxPixel(int xPixel)
	{
		this.xPixel = xPixel;
	}

	public int getyPixel()
	{
		return yPixel;
	}

	public void setyPixel(int yPixel)
	{
		this.yPixel = yPixel;
	}

	public int getwPixel()
	{
		return wPixel;
	}

	public void setwPixel(int wPixel)
	{
		this.wPixel = wPixel;
	}

	public int gethPixel()
	{
		return hPixel;
	}

	public void sethPixel(int hPixel)
	{
		this.hPixel = hPixel;
	}

	
}

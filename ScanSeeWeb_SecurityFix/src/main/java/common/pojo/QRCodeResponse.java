package common.pojo;

public class QRCodeResponse
{
	private String qrCodeImagePath;
	private String response;

	public String getQrCodeImagePath()
	{
		return qrCodeImagePath;
	}

	public void setQrCodeImagePath(String qrCodeImagePath)
	{
		this.qrCodeImagePath = qrCodeImagePath;
	}

	public String getResponse()
	{
		return response;
	}

	public void setResponse(String response)
	{
		this.response = response;
	}

}

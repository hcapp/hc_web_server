package common.pojo;

import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * This pojo class contains setter and getter methods for hot deals.
 * 
 * @author nanda_b
 */
public class HotDealInfo
{
	/**
	 * For hotDealName.
	 */
	private String hotDealName;
	/**
	 * Variable hotDealID declared as int.
	 */
	private int hotDealID;
	/**
	 * Variable price declared as String.
	 */
	private String price;
	/**
	 * Variable hotDealShortDescription declared as String.
	 */
	private String hotDealShortDescription;
	/**
	 * Variable hotDealLongDescription declared as String.
	 */
	private String hotDealLongDescription;
	/**
	 * Variable hotDealTermsConditions declared as String.
	 */
	private String hotDealTermsConditions;
	/**
	 * Variable url declared as String.
	 */
	private String url;
	/**
	 * Variable dealStartDate declared as String.
	 */
	private String dealStartDate;
	/**
	 * Variable dealEndDate declared as String.
	 */
	private String dealEndDate;
	/**
	 * Variable salePrice declared as String.
	 */
	private String salePrice;
	/**
	 * Variable dealStartTime declared as String.
	 */
	private String dealStartTime;
	/**
	 * Variable dealEndTime declared as String.
	 */
	private String dealEndTime;

	private String productName;

	private String cityHidden;

	private String locationHidden;

	private String retailerHidden;
	
	//private String retailerLocID;

	private Integer dealTimeZoneId;

	private String productImagePath;
	/**
	 * 
	 */
	private Long numOfHotDeals;
	/**
	 * 
	 */
	private String expireDate;
	/**
	 * 
	 */
	private String expireTime;
	/**
	 * 
	 */
	private CommonsMultipartFile imageFile;
	/**
	 * 
	 */
	private String expireHrs;
	/**
	 * 
	 */
	
	private String expireMins;
	/**
	 * 
	 */
	private String couponCode;
	/**
	 * 
	 */
	private String productImage;
	/**
	 * 
	 */
	private String viewName;
	/**
	 * 
	 */
	private String tabIndexCity;
	/**
	 * 
	 */
	private String tabIndexLoc;
	/**
	 * 
	 */
	private boolean percentageFlag;
	/**
	 * 
	 */
	
	private Integer hotDealListID;
	
	private boolean productImageFlag;
	
	/**
	 * Variable viewReport declared as String.
	 */
	private String viewReport;
	
	public String getRetailerHidden()
	{
		return retailerHidden;
	}

	public void setRetailerHidden(String retailerHidden)
	{
		this.retailerHidden = retailerHidden;
	}

	public String getLocationHidden()
	{
		return locationHidden;
	}

	public void setLocationHidden(String locationHidden)
	{
		this.locationHidden = locationHidden;
	}

	private String state;

	private String city;

	private Integer retailID;

	private Integer retailerLocID;

	private String productId;

	private String existingProductIds;
	private long totalSize;

	public String getExistingProductIds()
	{
		return existingProductIds;
	}

	public void setExistingProductIds(String existingProductIds)
	{
		this.existingProductIds = existingProductIds;
	}

	private String scanCode;

	private String productShortDescription;

	private String hotDealDiscountAmt;

	private String hotDealDiscountPct;

	private String dealForCityLoc;

	private String recordCount;

	public String getCityHidden()
	{
		return cityHidden;
	}

	public String getDealForCityLoc()
	{
		return dealForCityLoc;
	}

	public void setDealForCityLoc(String dealForCityLoc)
	{
		this.dealForCityLoc = dealForCityLoc;
	}

	public void setCityHidden(String cityHidden)
	{
		this.cityHidden = cityHidden;
	}

	public String getHotDealDiscountAmt()
	{
		return hotDealDiscountAmt;
	}

	public void setHotDealDiscountAmt(String hotDealDiscountAmt)
	{
		this.hotDealDiscountAmt = hotDealDiscountAmt;
	}

	public void setHotDealDiscountPct(String hotDealDiscountPct)
	{
		this.hotDealDiscountPct = hotDealDiscountPct;
	}

	private String weight;

	private String weightUnit;

	private String hotDealImagePath;

	private String manufName;
	/**
	 * Variable category declared as List.
	 */
	private List<Contact> contact;
	/**
	 * @return the manufName
	 */
	public String getManufName()
	{
		return manufName;
	}

	/**
	 * @param manufName
	 *            the manufName to set
	 */
	public void setManufName(String manufName)
	{
		this.manufName = manufName;
	}

	/**
	 * @return the hotDealImagePath
	 */
	public String getHotDealImagePath()
	{
		return hotDealImagePath;
	}

	/**
	 * @param hotDealImagePath
	 *            the hotDealImagePath to set
	 */
	public void setHotDealImagePath(String hotDealImagePath)
	{
		if (null != hotDealImagePath && !"".equals(hotDealImagePath))
		{
			this.hotDealImagePath = hotDealImagePath;
		}
		else
		{

			this.hotDealImagePath = ApplicationConstants.IMAGEICON;
		}

	}

	/**
	 * @return the weight
	 */
	public String getWeight()
	{
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(String weight)
	{
		this.weight = weight;
	}

	/**
	 * @return the weightUnit
	 */
	public String getWeightUnit()
	{
		return weightUnit;
	}

	/**
	 * @param weightUnit
	 *            the weightUnit to set
	 */
	public void setWeightUnit(String weightUnit)
	{
		this.weightUnit = weightUnit;
	}

	public String getProductShortDescription()
	{
		return productShortDescription;
	}

	public void setProductShortDescription(String productShortDescription)
	{
		this.productShortDescription = productShortDescription;
	}

	public String getScanCode()
	{
		return scanCode;
	}

	public void setScanCode(String scanCode)
	{
		this.scanCode = scanCode;
	}

	private String dealStartHrs;

	private String dealStartMins;

	public String getDealStartHrs()
	{
		return dealStartHrs;
	}

	public void setDealStartHrs(String dealStartHrs)
	{
		this.dealStartHrs = dealStartHrs;
	}

	public String getDealStartMins()
	{
		return dealStartMins;
	}

	public void setDealStartMins(String dealStartMins)
	{
		this.dealStartMins = dealStartMins;
	}

	public String getDealEndhrs()
	{
		return dealEndhrs;
	}

	public void setDealEndhrs(String dealEndhrs)
	{
		this.dealEndhrs = dealEndhrs;
	}

	public String getDealEndMins()
	{
		return dealEndMins;
	}

	public void setDealEndMins(String dealEndMins)
	{
		this.dealEndMins = dealEndMins;
	}

	private String dealEndhrs;

	private String dealEndMins;

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(String productId)
	{
		this.productId = productId;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	public Integer getRetailID()
	{
		return retailID;
	}

	public void setRetailID(Integer retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * Variable dealImgPath declared as String.
	 */
	private String dealImgPath;

	private String actionType;

	private String dealDiscountAmt;

	private String HotDealDiscountPct;
	/**
	 * @return the claimCount
	 */
	private int claimCount;
	/**
	 * @return the redeemCount
	 */
	private int redeemCount;
	/**
	 * @return the redeemCount
	 */
	private String hDealName;
	private String backButton;
	private String RetailLogoImage;
	/**
	 * @return the hotDealName
	 */
	public String getHotDealName()
	{
		return hotDealName;
	}

	/**
	 * @param hotDealName
	 *            the hotDealName to set
	 */
	public void setHotDealName(String hotDealName)
	{
		this.hotDealName = Utility.removeWhiteSpaces(hotDealName);
	}

	/**
	 * @return the hotDealID
	 */
	public int getHotDealID()
	{
		return hotDealID;
	}

	/**
	 * @param hotDealID
	 *            the hotDealID to set
	 */
	public void setHotDealID(int hotDealID)
	{
		this.hotDealID = hotDealID;
	}

	/**
	 * @return the price
	 */
	public String getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price)
	{
		this.price = price;
	}

	/**
	 * @return the hotDealShortDescription
	 */
	public String getHotDealShortDescription()
	{
		return hotDealShortDescription;
	}

	/**
	 * @param hotDealShortDescription
	 *            the hotDealShortDescription to set
	 */
	public void setHotDealShortDescription(String hotDealShortDescription)
	{
		this.hotDealShortDescription = Utility.removeWhiteSpaces(hotDealShortDescription);
	}

	/**
	 * @return the hotDealLongDescription
	 */
	public String getHotDealLongDescription()
	{
		return hotDealLongDescription;
	}

	/**
	 * @param hotDealLongDescription
	 *            the hotDealLongDescription to set
	 */
	public void setHotDealLongDescription(String hotDealLongDescription)
	{
		this.hotDealLongDescription = Utility.removeWhiteSpaces(hotDealLongDescription);
	}

	/**
	 * @return the hotDealTermsConditions
	 */
	public String getHotDealTermsConditions()
	{
		return hotDealTermsConditions;
	}

	/**
	 * @param hotDealTermsConditions
	 *            the hotDealTermsConditions to set
	 */
	public void setHotDealTermsConditions(String hotDealTermsConditions)
	{
		this.hotDealTermsConditions = Utility.removeWhiteSpaces(hotDealTermsConditions);
	}

	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url)
	{
		this.url = Utility.removeWhiteSpaces(url);
	}

	/**
	 * @return the dealStartDate
	 */
	public String getDealStartDate()
	{
		return dealStartDate;
	}

	/**
	 * @param dealStartDate
	 *            the dealStartDate to set
	 */
	public void setDealStartDate(String dealStartDate)
	{
		this.dealStartDate = dealStartDate;
	}

	/**
	 * @return the dealEndDate
	 */
	public String getDealEndDate()
	{
		return dealEndDate;
	}

	/**
	 * @param dealEndDate
	 *            the dealEndDate to set
	 */
	public void setDealEndDate(String dealEndDate)
	{
		this.dealEndDate = dealEndDate;
	}

	/**
	 * @return the salePrice
	 */
	public String getSalePrice()
	{
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(String salePrice)
	{
		this.salePrice = salePrice;
	}

	/**
	 * @return the dealStartTime
	 */
	public String getDealStartTime()
	{
		return dealStartTime;
	}

	/**
	 * @param dealStartTime
	 *            the dealStartTime to set
	 */
	public void setDealStartTime(String dealStartTime)
	{
		this.dealStartTime = dealStartTime;
	}

	/**
	 * @return the dealEndTime
	 */
	public String getDealEndTime()
	{
		return dealEndTime;
	}

	/**
	 * @param dealEndTime
	 *            the dealEndTime to set
	 */
	public void setDealEndTime(String dealEndTime)
	{
		this.dealEndTime = dealEndTime;
	}

	/**
	 * @return the dealImgPath
	 */
	public String getDealImgPath()
	{
		return dealImgPath;
	}

	/**
	 * @param dealImgPath
	 *            the dealImgPath to set
	 */
	public void setDealImgPath(String dealImgPath)
	{
		if ("".equals(Utility.checkNull(dealImgPath))) {
			// Changed below code as IE browser showing broken image if it did
			// not find image.
			this.dealImgPath = ApplicationConstants.BLANKIMAGE;
		} else {
			this.dealImgPath = dealImgPath;
		}
	}

	/**
	 * @return the actionType
	 */
	public String getActionType()
	{
		return actionType;
	}

	/**
	 * @param actionType
	 *            the actionType to set
	 */
	public void setActionType(String actionType)
	{
		this.actionType = actionType;
	}

	/**
	 * @return the dealDiscountAmt
	 */
	public String getDealDiscountAmt()
	{
		return dealDiscountAmt;
	}

	/**
	 * @param dealDiscountAmt
	 *            the dealDiscountAmt to set
	 */
	public void setDealDiscountAmt(String dealDiscountAmt)
	{
		this.dealDiscountAmt = dealDiscountAmt;
	}

	/**
	 * @return the hotDealDiscountPct
	 */
	public String getHotDealDiscountPct()
	{
		return HotDealDiscountPct;
	}

	public Integer getDealTimeZoneId()
	{
		return dealTimeZoneId;
	}

	public void setDealTimeZoneId(Integer dealTimeZoneId)
	{
		this.dealTimeZoneId = dealTimeZoneId;
	}

	/**
	 * 
	 */
	private String bCategory;

	/**
	 * 
	 */
	private String bCategoryHidden;

	/**
	 * @return the bCategory
	 */
	public String getbCategory()
	{
		return bCategory;
	}

	/**
	 * @param bCategory
	 *            the bCategory to set
	 */
	public void setbCategory(String bCategory)
	{
		this.bCategory = bCategory;
	}

	/**
	 * @return the bCategoryHidden
	 */
	public String getbCategoryHidden()
	{
		return bCategoryHidden;
	}

	/**
	 * @param bCategoryHidden
	 *            the bCategoryHidden to set
	 */
	public void setbCategoryHidden(String bCategoryHidden)
	{
		this.bCategoryHidden = bCategoryHidden;
	}

	private String cityHiddenChecked;

	private String locationHiddenChecked;

	/**
	 * @return the cityHiddenChecked
	 */
	public String getCityHiddenChecked()
	{
		return cityHiddenChecked;
	}

	/**
	 * @param cityHiddenChecked
	 *            the cityHiddenChecked to set
	 */
	public void setCityHiddenChecked(String cityHiddenChecked)
	{
		this.cityHiddenChecked = cityHiddenChecked;
	}

	/**
	 * @return the locationHiddenChecked
	 */
	public String getLocationHiddenChecked()
	{
		return locationHiddenChecked;
	}

	/**
	 * @param locationHiddenChecked
	 *            the locationHiddenChecked to set
	 */
	public void setLocationHiddenChecked(String locationHiddenChecked)
	{
		this.locationHiddenChecked = locationHiddenChecked;
	}

	/**
	 * 
	 */
	private String productNameHidden;

	/**
	 * @return the productNameHidden
	 */
	public String getProductNameHidden()
	{
		return productNameHidden;
	}

	/**
	 * @param productNameHidden
	 *            the productNameHidden to set
	 */
	public void setProductNameHidden(String productNameHidden)
	{
		this.productNameHidden = productNameHidden;
	}

	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath()
	{
		return productImagePath;
	}

	/**
	 * @param productImagePath
	 *            the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath)
	{

		if (null == productImagePath || "".equals(productImagePath))
		{
			// Changed below code as IE browser showing broken image if it did
			// not find image
			this.productImagePath = ApplicationConstants.BLANKIMAGE;
		}
		else
		{
			this.productImagePath = productImagePath;
		}
	}

	/**
	 * @return the retailerLocID
	 */
	public Integer getRetailerLocID()
	{
		return retailerLocID;
	}

	/**
	 * @param retailerLocID
	 *            the retailerLocID to set
	 */
	public void setRetailerLocID(Integer retailerLocID)
	{
		this.retailerLocID = retailerLocID;
	}

	public String getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(String recordCount)
	{
		this.recordCount = recordCount;
	}

	/**
	 * @return the numOfHotDeals
	 */
	public Long getNumOfHotDeals()
	{
		return numOfHotDeals;
	}

	/**
	 * @param numOfHotDeals the numOfHotDeals to set
	 */
	public void setNumOfHotDeals(Long numOfHotDeals)
	{
		this.numOfHotDeals = numOfHotDeals;
	}

	/**
	 * @return the expireDate
	 */
	public String getExpireDate()
	{
		return expireDate;
	}

	/**
	 * @param expireDate the expireDate to set
	 */
	public void setExpireDate(String expireDate)
	{
		this.expireDate = expireDate;
	}

	/**
	 * @return the expireTime
	 */
	public String getExpireTime()
	{
		return expireTime;
	}

	/**
	 * @param expireTime the expireTime to set
	 */
	public void setExpireTime(String expireTime)
	{
		this.expireTime = expireTime;
	}

	/**
	 * @return the imageFile
	 */
	public CommonsMultipartFile getImageFile()
	{
		return imageFile;
	}

	/**
	 * @param imageFile the imageFile to set
	 */
	public void setImageFile(CommonsMultipartFile imageFile)
	{
		this.imageFile = imageFile;
	}

	/**
	 * @return the expireHrs
	 */
	public String getExpireHrs()
	{
		return expireHrs;
	}

	/**
	 * @param expireHrs the expireHrs to set
	 */
	public void setExpireHrs(String expireHrs)
	{
		this.expireHrs = expireHrs;
	}

	/**
	 * @return the expireMins
	 */
	public String getExpireMins()
	{
		return expireMins;
	}

	/**
	 * @param expireMins the expireMins to set
	 */
	public void setExpireMins(String expireMins)
	{
		this.expireMins = expireMins;
	}

	/**
	 * @return the couponCode
	 */
	public String getCouponCode()
	{
		return couponCode;
	}

	/**
	 * @param couponCode the couponCode to set
	 */
	public void setCouponCode(String couponCode)
	{
		this.couponCode = couponCode;
	}

	/**
	 * @return the productImage
	 */
	public String getProductImage()
	{
		return productImage;
	}

	/**
	 * @param productImage the productImage to set
	 */
	public void setProductImage(String productImage)
	{
		this.productImage = productImage;
	}

	/**
	 * @return the viewName
	 */
	public String getViewName()
	{
		return viewName;
	}

	/**
	 * @param viewName the viewName to set
	 */
	public void setViewName(String viewName)
	{
		this.viewName = viewName;
	}

	/**
	 * @return the percentageFlag
	 */
	public boolean isPercentageFlag()
	{
		return percentageFlag;
	}

	/**
	 * @param percentageFlag the percentageFlag to set
	 */
	public void setPercentageFlag(boolean percentageFlag)
	{
		this.percentageFlag = percentageFlag;
	}

	/**
	 * @return the tabIndexCity
	 */
	public String getTabIndexCity()
	{
		return tabIndexCity;
	}

	/**
	 * @param tabIndexCity the tabIndexCity to set
	 */
	public void setTabIndexCity(String tabIndexCity)
	{
		this.tabIndexCity = tabIndexCity;
	}

	/**
	 * @return the tabIndexLoc
	 */
	public String getTabIndexLoc()
	{
		return tabIndexLoc;
	}

	/**
	 * @param tabIndexLoc the tabIndexLoc to set
	 */
	public void setTabIndexLoc(String tabIndexLoc)
	{
		this.tabIndexLoc = tabIndexLoc;
	}

	/**
	 * @return the backButton
	 */
	public String getBackButton()
	{
		return backButton;
	}

	/**
	 * @param backButton the backButton to set
	 */
	public void setBackButton(String backButton)
	{
		this.backButton = backButton;
	}

	/**
	 * @return the retailLogoImage
	 */
	public String getRetailLogoImage()
	{
		return RetailLogoImage;
	}

	/**
	 * @param retailLogoImage the retailLogoImage to set
	 */
	public void setRetailLogoImage(String retailLogoImage)
	{
		RetailLogoImage = retailLogoImage;
	}

	/**
	 * @return the productImageFlag
	 */
	public boolean isProductImageFlag()
	{
		return productImageFlag;
	}

	/**
	 * @param productImageFlag the productImageFlag to set
	 */
	public void setProductImageFlag(boolean productImageFlag)
	{
		this.productImageFlag = productImageFlag;
	}

	/**
	 * @return the contact
	 */
	public List<Contact> getContact()
	{
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(List<Contact> contact)
	{
		this.contact = contact;
	}

	/**
	 * @return the claimCount
	 */
	public int getClaimCount()
	{
		return claimCount;
	}

	/**
	 * @param claimCount the claimCount to set
	 */
	public void setClaimCount(int claimCount)
	{
		this.claimCount = claimCount;
	}

	/**
	 * @return the redeemCount
	 */
	public int getRedeemCount()
	{
		return redeemCount;
	}

	/**
	 * @param redeemCount the redeemCount to set
	 */
	public void setRedeemCount(int redeemCount)
	{
		this.redeemCount = redeemCount;
	}

	/**
	 * @return the viewReport
	 */
	public String getViewReport()
	{
		return viewReport;
	}

	/**
	 * @param viewReport the viewReport to set
	 */
	public void setViewReport(String viewReport)
	{
		this.viewReport = viewReport;
	}

	/**
	 * @return the totalSize
	 */
	public long getTotalSize()
	{
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(long totalSize)
	{
		this.totalSize = totalSize;
	}

	/**
	 * @return the hDealName
	 */
	public String gethDealName()
	{
		return hDealName;
	}

	/**
	 * @param hDealName the hDealName to set
	 */
	public void sethDealName(String hDealName)
	{
		this.hDealName = hDealName;
	}

	public Integer getHotDealListID()
	{
		return hotDealListID;
	}

	public void setHotDealListID(Integer hotDealListID)
	{
		this.hotDealListID = hotDealListID;
	}

}

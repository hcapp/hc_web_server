/**
 * 
 */
package common.pojo;

import java.util.ArrayList;
import java.util.List;

import common.pojo.Event;

/**
 * @author manjunatha_gh
 *
 */
public class SearchResultInfo
{
	private long totalSize;
	
	private List <ProductVO>  productList = new ArrayList<ProductVO>();
	private List <Rebates>  rebatesList = new ArrayList<Rebates>();
	private List <RetailerLocationAdvertisement>  addsList = new ArrayList<RetailerLocationAdvertisement>();

	private List <Coupon>  couponsList = new ArrayList<Coupon>();
	private List <RetailerLocation>  locationList = new ArrayList<RetailerLocation>();
	
	private List <HotDealInfo> dealList = new ArrayList<HotDealInfo>();
	
	private List<SearchProductRetailer> retailerproductList=new ArrayList<SearchProductRetailer>();
	
	private List<ManageProducts> manageProductList =new ArrayList<ManageProducts>();
	
	private List<RetailerCustomPage> pageList ;
	
	private List<Event> eventList = new ArrayList<Event>();
	
	private Integer nextPage;
	private String  locationID;
	/**
	 * @return the locationList
	 */
	public List<RetailerLocation> getLocationList()
	{
		return locationList;
	}

	/**
	 * @param locationList the locationList to set
	 */
	public void setLocationList(List<RetailerLocation> locationList)
	{
		this.locationList = locationList;
	}

	private String searchType;
	/**
	 * @return the totalSize
	 */
	public long getTotalSize()
	{
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(long totalSize)
	{
		this.totalSize = totalSize;
	}

	/**
	 * @return the productList
	 */
	public List<ProductVO> getProductList()
	{
		return productList;
	}

	/**
	 * @param productList the productList to set
	 */
	public void setProductList(List<ProductVO> productList)
	{
		this.productList = productList;
	}

	/**
	 * @return the dealList
	 */
	public List<HotDealInfo> getDealList()
	{
		return dealList;
	}

	/**
	 * @param dealList the dealList to set
	 */
	public void setDealList(List<HotDealInfo> dealList)
	{
		this.dealList = dealList;
	}

	/**
	 * @return the searchType
	 */
	public String getSearchType()
	{
		return searchType;
	}

	/**
	 * @param searchType the searchType to set
	 */
	public void setSearchType(String searchType)
	{
		this.searchType = searchType;
	}

	public List<SearchProductRetailer> getRetailerproductList() {
		return retailerproductList;
	}

	public void setRetailerproductList(
			List<SearchProductRetailer> retailerproductList) {
		this.retailerproductList = retailerproductList;
	}

	public void setRebatesList(List <Rebates> rebatesList) {
		this.rebatesList = rebatesList;
	}

	public List <Rebates> getRebatesList() {
		return rebatesList;
	}

	public void setAddsList(List <RetailerLocationAdvertisement> addsList) {
		this.addsList = addsList;
	}

	public List <RetailerLocationAdvertisement> getAddsList() {
		return addsList;
	}

	public void setCouponsList(List <Coupon> couponsList) {
		this.couponsList = couponsList;
	}

	public List <Coupon> getCouponsList() {
		return couponsList;
	}

	/**
	 * @return the manageProductList
	 */
	public List<ManageProducts> getManageProductList()
	{
		return manageProductList;
	}

	/**
	 * @param manageProductList the manageProductList to set
	 */
	public void setManageProductList(List<ManageProducts> manageProductList)
	{
		this.manageProductList = manageProductList;
	}

	/**
	 * @return the pageList
	 */
	public List<RetailerCustomPage> getPageList()
	{
		return pageList;
	}

	/**
	 * @param pageList the pageList to set
	 */
	public void setPageList(List<RetailerCustomPage> pageList)
	{
		this.pageList = pageList;
	}

	public Integer getNextPage()
	{
		return nextPage;
	}

	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}

	/**
	 * @return the locationID
	 */
	public String getLocationID()
	{
		return locationID;
	}

	/**
	 * @param locationID the locationID to set
	 */
	public void setLocationID(String locationID)
	{
		this.locationID = locationID;
	}

	/**
	 * @return the eventList
	 */
	public List<Event> getEventList() {
		return eventList;
	}

	/**
	 * @param eventList the eventList to set
	 */
	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}
	
}



package common.pojo;

public class NonProfitPartnerInfo
{

	private String nonProfitPartnerKey;
	private String nonProfitPartnerValue;
	
	public NonProfitPartnerInfo(String nonProfitPartnerKey, String nonProfitPartnerValue)
	{
	this.nonProfitPartnerKey = nonProfitPartnerKey;
	this.nonProfitPartnerValue = nonProfitPartnerValue;
	}
	
	
	public String getNonProfitPartnerKey()
	{
		return nonProfitPartnerKey;
	}
	public void setNonProfitPartnerKey(String nonProfitPartnerKey)
	{
		this.nonProfitPartnerKey = nonProfitPartnerKey;
	}


	public String getNonProfitPartnerValue()
	{
		return nonProfitPartnerValue;
	}


	public void setNonProfitPartnerValue(String nonProfitPartnerValue)
	{
		this.nonProfitPartnerValue = nonProfitPartnerValue;
	}
	
	
	
}

package common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class SupplierProfile {
		/**
		 * 
		 */
		private Long UserID;
		/**
		 * 
		 */
		private Long supProfileID;
		/**
		 * 
		 */
	    private String companyName;
	    /**
		 * 
		 */
	    private String address;
	    /**
		 * 
		 */
	    private String corporateAddress;
	    /**
		 * 
		 */
	    private String state;
	    /**
		 * for state.
		 */
		private String stateHidden;
		/**
		 * for state.
		 */
		private String stateCodeHidden;
	    /**
		 * 
		 */
	    private String city;
	    /**
		 * 
		 */
	    private String postalCode;
	    /**
		 * 
		 */
	    private String phone;
	    /**
		 * 
		 */
	    private String contactPhone;
	    /**
		 * 
		 */
	    private String firstName;
	    /**
		 * 
		 */
		private String contactFName;
		/**
		 * 
		 */
	    private String lastName;
	    /**
		 * 
		 */
	    private String contactLName;
	    /**
		 * 
		 */
	    private String email;
		/**
		 * 
		 */
		private Users users;
		/**
		 * 
		 */
		private Manufacture manufacture;
		/**
		 * 
		 */
		private Contact contact;
		/**
		 * 
		 */
		private String cityHidden;
		/**
		 * 
		 */
		private String bCategory;
		/**
		 * 
		 */
		private boolean nonProfit;
		/**
		 * 
		 */
		private String bCategoryHidden;
		
		private String tabIndex;
		
		private String citySelectedFlag;
		
		/**
		 * 
		 */
		public SupplierProfile() {
			super();
			// TODO Auto-generated constructor stub
		}
		/**
		 * @param userID
		 * @param supProfileID
		 * @param companyName
		 * @param address
		 * @param corporateAddress
		 * @param state
		 * @param city
		 * @param postalCode
		 * @param phone
		 * @param contactPhone
		 * @param firstName
		 * @param contactFName
		 * @param lastName
		 * @param contactLName
		 * @param email
		 * @param users
		 * @param manufacture
		 * @param contact
		 * @param cityHidden
		 * @param bCategory
		 * @param nonProfit
		 * @param bCategoryHidden
		 */
		public SupplierProfile(Long userID, Long supProfileID, String companyName, String address, String corporateAddress, String state,
				String city, String postalCode, String phone, String contactPhone, String firstName, String contactFName, String lastName,
				String contactLName, String email, Users users, Manufacture manufacture, Contact contact, String cityHidden, String bCategory,
				boolean nonProfit, String bCategoryHidden)
		{
			super();
			UserID = userID;
			this.supProfileID = supProfileID;
			this.companyName = companyName;
			this.address = address;
			this.corporateAddress = corporateAddress;
			this.state = state;
			this.city = city;
			this.postalCode = postalCode;
			this.phone = phone;
			this.contactPhone = contactPhone;
			this.firstName = firstName;
			this.contactFName = contactFName;
			this.lastName = lastName;
			this.contactLName = contactLName;
			this.email = email;
			this.users = users;
			this.manufacture = manufacture;
			this.contact = contact;
			this.cityHidden = cityHidden;
			this.bCategory = bCategory;
			this.nonProfit = nonProfit;
			this.bCategoryHidden = bCategoryHidden;
		}
		/**
		 * @return the userID
		 */
		public Long getUserID()
		{
			return UserID;
		}
		/**
		 * @param userID the userID to set
		 */
		public void setUserID(Long userID)
		{
			UserID = userID;
		}
		/**
		 * @return the supProfileID
		 */
		public Long getSupProfileID()
		{
			return supProfileID;
		}
		/**
		 * @param supProfileID the supProfileID to set
		 */
		public void setSupProfileID(Long supProfileID)
		{
			this.supProfileID = supProfileID;
		}
		/**
		 * @return the companyName
		 */
		public String getCompanyName()
		{
			return companyName;
		}
		/**
		 * @param companyName the companyName to set
		 */
		public void setCompanyName(String companyName)
		{
			this.companyName = companyName;
		}
		/**
		 * @return the address
		 */
		public String getAddress()
		{
			return address;
		}
		/**
		 * @param address the address to set
		 */
		public void setAddress(String address)
		{
			this.address = address;
		}
		/**
		 * @return the corporateAddress
		 */
		public String getCorporateAddress()
		{
			return corporateAddress;
		}
		/**
		 * @param corporateAddress the corporateAddress to set
		 */
		public void setCorporateAddress(String corporateAddress)
		{
			this.corporateAddress = corporateAddress;
		}
		/**
		 * @return the state
		 */
		public String getState()
		{
			return state;
		}
		/**
		 * @param state the state to set
		 */
		public void setState(String state)
		{
			this.state = state;
		}
		/**
		 * @return the city
		 */
		public String getCity()
		{
			return city;
		}
		/**
		 * @param city the city to set
		 */
		public void setCity(String city)
		{
			this.city = city;
		}
		/**
		 * @return the postalCode
		 */
		public String getPostalCode()
		{
			return postalCode;
		}
		/**
		 * @param postalCode the postalCode to set
		 */
		public void setPostalCode(String postalCode)
		{
			this.postalCode = postalCode;
		}
		/**
		 * @return the phone
		 */
		public String getPhone()
		{
			return phone;
		}
		/**
		 * @param phone the phone to set
		 */
		public void setPhone(String phone)
		{
			this.phone = phone;
		}
		/**
		 * @return the contactPhone
		 */
		public String getContactPhone()
		{
			return contactPhone;
		}
		/**
		 * @param contactPhone the contactPhone to set
		 */
		public void setContactPhone(String contactPhone)
		{
			this.contactPhone = contactPhone;
		}
		/**
		 * @return the firstName
		 */
		public String getFirstName()
		{
			return firstName;
		}
		/**
		 * @param firstName the firstName to set
		 */
		public void setFirstName(String firstName)
		{
			this.firstName = firstName;
		}
		/**
		 * @return the contactFName
		 */
		public String getContactFName()
		{
			return contactFName;
		}
		/**
		 * @param contactFName the contactFName to set
		 */
		public void setContactFName(String contactFName)
		{
			this.contactFName = contactFName;
		}
		/**
		 * @return the lastName
		 */
		public String getLastName()
		{
			return lastName;
		}
		/**
		 * @param lastName the lastName to set
		 */
		public void setLastName(String lastName)
		{
			this.lastName = lastName;
		}
		/**
		 * @return the contactLName
		 */
		public String getContactLName()
		{
			return contactLName;
		}
		/**
		 * @param contactLName the contactLName to set
		 */
		public void setContactLName(String contactLName)
		{
			this.contactLName = contactLName;
		}
		/**
		 * @return the email
		 */
		public String getEmail()
		{
			return email;
		}
		/**
		 * @param email the email to set
		 */
		public void setEmail(String email)
		{
			this.email = email;
		}
		/**
		 * @return the users
		 */
		public Users getUsers()
		{
			return users;
		}
		/**
		 * @param users the users to set
		 */
		public void setUsers(Users users)
		{
			this.users = users;
		}
		/**
		 * @return the manufacture
		 */
		public Manufacture getManufacture()
		{
			return manufacture;
		}
		/**
		 * @param manufacture the manufacture to set
		 */
		public void setManufacture(Manufacture manufacture)
		{
			this.manufacture = manufacture;
		}
		/**
		 * @return the contact
		 */
		public Contact getContact()
		{
			return contact;
		}
		/**
		 * @param contact the contact to set
		 */
		public void setContact(Contact contact)
		{
			this.contact = contact;
		}
		/**
		 * @return the cityHidden
		 */
		public String getCityHidden()
		{
			return cityHidden;
		}
		/**
		 * @param cityHidden the cityHidden to set
		 */
		public void setCityHidden(String cityHidden)
		{
			this.cityHidden = cityHidden;
		}
		/**
		 * @return the bCategory
		 */
		public String getbCategory()
		{
			return bCategory;
		}
		/**
		 * @param bCategory the bCategory to set
		 */
		public void setbCategory(String bCategory)
		{
			this.bCategory = bCategory;
		}
		/**
		 * @return the nonProfit
		 */
		public boolean isNonProfit()
		{
			return nonProfit;
		}
		/**
		 * @param nonProfit the nonProfit to set
		 */
		public void setNonProfit(boolean nonProfit)
		{
			this.nonProfit = nonProfit;
		}
		/**
		 * @return the bCategoryHidden
		 */
		public String getbCategoryHidden()
		{
			return bCategoryHidden;
		}
		/**
		 * @param bCategoryHidden the bCategoryHidden to set
		 */
		public void setbCategoryHidden(String bCategoryHidden)
		{
			this.bCategoryHidden = bCategoryHidden;
		}
		public void setTabIndex(String tabIndex) {
			this.tabIndex = tabIndex;
		}
		public String getTabIndex() {
			return tabIndex;
		}	
		public String getStateHidden()
		{
			return stateHidden;
		}
		public void setStateHidden(String stateHidden)
		{
			this.stateHidden = stateHidden;
		}
		public String getStateCodeHidden()
		{
			return stateCodeHidden;
		}
		public void setStateCodeHidden(String stateCodeHidden)
		{
			this.stateCodeHidden = stateCodeHidden;
		}
		/**
		 * @param citySelectedFlag the citySelectedFlag to set
		 */
		public void setCitySelectedFlag(String citySelectedFlag)
		{
			this.citySelectedFlag = citySelectedFlag;
		}
		/**
		 * @return the citySelectedFlag
		 */
		public String getCitySelectedFlag()
		{
			return citySelectedFlag;
		}
}

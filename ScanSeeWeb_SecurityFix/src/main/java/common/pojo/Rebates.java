package common.pojo;

import common.constatns.ApplicationConstants;

/**
 * Bean used in Rebates.
 * @author Created by SPAN.
 */

public class Rebates {

	/**
	 * 
	 */
	private Long userID;
	/**
	 * 
	 */
	private int rebateID;
	/**
	 * 
	 */
	private Long retailID;
	/**
	 * 
	 */
	private Long rebCreatedID;
	/**
	 * 
	 */
	private String rebName;
	/**
	 * 
	 */
	private String rebShortDescription;
	/**
	 * 
	 */
	private String rebLongDescription;
	/**
	 * 
	 */
	private String rebDescription;
	/**
	 * 
	 */
	private String rebAmount;
	/**
	 * 
	 */
	private String rebStartDate;
	/**
	 * 
	 */
	private String rebEndDate;
	/**
	 * 
	 */
	private String rebTermCondtn;
	/**
	 * 
	 */
	private String rebStartTime;
	/**
	 * 
	 */
	private String rebEndTime;
	
	private String rebStartHrs;
	
	private String rebStartMins;
	
	private String rebEndhrs;
	
	private String rebEndMins;

    private int  RetailerID;
    
    private int  retailerLocationID;
    
    private int retailerLocID;
    private Integer noOfrebatesIsued;
    private Integer noOfRebatesUsed;
    
    private String productName; 
    
    private Users user;
    
   private Product product;
   private String retailerLocation ;
   
  private int supplierId;
  private String searchKey;
  private String imagePath;
  
  private String productNames;
  
  /**
   * for rebateTimeZoneId
   */
  private Integer rebateTimeZoneId;
    
    /**
	 * 
	 */
	private String longDescription;
	/**
	 * 
	 */
	private String shortDescription;
    /**
	 * 
	 */
	private String productID;
	/**
	 * 
	 */
	private String scanCode;
	
	private String rebproductId;
    private String productImagePath;


	private String locationHidden;

    
	public int getRetailerLocID() {
		return retailerLocID;
	}
	public void setRetailerLocID(int retailerLocID) {
		this.retailerLocID = retailerLocID;
	}
	/**
	 * 
	 */
	private Users users;
	//private int productId;
	

	/**
	 * 
	 */
	public Rebates() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param rebateID
	 * @param rebName
	 * @param rebShortDescription
	 * @param rebLongDescription
	 * @param rebDescription
	 * @param rebAmount
	 * @param rebStartDate
	 * @param rebEndDate
	 * @param rebTermCondtn
	 * @param rebStartTime
	 * @param rebEndTime
	 */
	public Rebates(Long userID,int rebateID, Long retailID, Long rebCreatedID,String rebName, String rebShortDescription,
			String rebLongDescription, String rebDescription, String rebAmount,
			String rebStartDate, String rebEndDate, String rebTermCondtn,
			String rebStartTime, String rebEndTime,Users users) {
		super();
		this.userID = userID;
		this.rebateID = rebateID;
		this.retailID = retailID;
		this.rebCreatedID = rebCreatedID;
		this.rebName = rebName;
		this.rebShortDescription = rebShortDescription;
		this.rebLongDescription = rebLongDescription;
		this.rebDescription = rebDescription;
		this.rebAmount = rebAmount;
		this.rebStartDate = rebStartDate;
		this.rebEndDate = rebEndDate;
		this.rebTermCondtn = rebTermCondtn;
		this.rebStartTime = rebStartTime;
		this.rebEndTime = rebEndTime;
		this.users=users;
	}
	/**
	 * @return the userID
	 */
	public Long getUserID() {
		return userID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	/**
	 * @return the rebateID
	 */
	public int getRebateID() {
		return rebateID;
	}
	/**
	 * @param rebateID the rebateID to set
	 */
	public void setRebateID(int rebateID) {
		this.rebateID = rebateID;
	}
	/**
	 * @return the retailID
	 */
	public Long getRetailID() {
		return retailID;
	}
	/**
	 * @param retailID the retailID to set
	 */
	public void setRetailID(Long retailID) {
		this.retailID = retailID;
	}
	/**
	 * @return the rebCreatedID
	 */
	public Long getRebCreatedID() {
		return rebCreatedID;
	}
	/**
	 * @param rebCreatedID the rebCreatedID to set
	 */
	public void setRebCreatedID(Long rebCreatedID) {
		this.rebCreatedID = rebCreatedID;
	}
	/**
	 * @return the rebName
	 */
	public String getRebName() {
		return rebName;
	}
	/**
	 * @param rebName the rebName to set
	 */
	public void setRebName(String rebName) {
		this.rebName = rebName;
	}
	/**
	 * @return the rebShortDescription
	 */
	public String getRebShortDescription() {
		return rebShortDescription;
	}
	/**
	 * @param rebShortDescription the rebShortDescription to set
	 */
	public void setRebShortDescription(String rebShortDescription) {
		this.rebShortDescription = rebShortDescription;
	}
	/**
	 * @return the rebLongDescription
	 */
	public String getRebLongDescription() {
		return rebLongDescription;
	}
	/**
	 * @param rebLongDescription the rebLongDescription to set
	 */
	public void setRebLongDescription(String rebLongDescription) {
		this.rebLongDescription = rebLongDescription;
	}
	/**
	 * @return the rebDescription
	 */
	public String getRebDescription() {
		return rebDescription;
	}
	/**
	 * @param rebDescription the rebDescription to set
	 */
	public void setRebDescription(String rebDescription) {
		this.rebDescription = rebDescription;
	}
	/**
	 * @return the rebAmount
	 */
	public String getRebAmount() {
		return rebAmount;
	}
	/**
	 * @param rebAmount the rebAmount to set
	 */
	public void setRebAmount(String rebAmount) {
		this.rebAmount = rebAmount;
	}
	/**
	 * @return the rebStartDate
	 */
	public String getRebStartDate() {
		return rebStartDate;
	}
	/**
	 * @param rebStartDate the rebStartDate to set
	 */
	public void setRebStartDate(String rebStartDate) {
		this.rebStartDate = rebStartDate;
	}
	/**
	 * @return the rebEndDate
	 */
	public String getRebEndDate() {
		return rebEndDate;
	}
	/**
	 * @param rebEndDate the rebEndDate to set
	 */
	public void setRebEndDate(String rebEndDate) {
		this.rebEndDate = rebEndDate;
	}
	/**
	 * @return the rebTermCondtn
	 */
	public String getRebTermCondtn() {
		return rebTermCondtn;
	}
	/**
	 * @param rebTermCondtn the rebTermCondtn to set
	 */
	public void setRebTermCondtn(String rebTermCondtn) {
		this.rebTermCondtn = rebTermCondtn;
	}
	/**
	 * @return the rebStartTime
	 */
	public String getRebStartTime() {
		return rebStartTime;
	}
	/**
	 * @param rebStartTime the rebStartTime to set
	 */
	public void setRebStartTime(String rebStartTime) {
		this.rebStartTime = rebStartTime;
	}
	/**
	 * @return the rebEndTime
	 */
	public String getRebEndTime() {
		return rebEndTime;
	}
	/**
	 * @param rebEndTime the rebEndTime to set
	 */
	public void setRebEndTime(String rebEndTime) {
		this.rebEndTime = rebEndTime;
	}
	/**
	 * @return the users
	 */
	public Users getUsers() {
		return users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(Users users) {
		this.users = users;
	}
      /**
	 * @return the retailerID
	 */
	public int getRetailerID() {
		return RetailerID;
	}
	/**
	 * @param retailerID the retailerID to set
	 */
	public void setRetailerID(int retailerID) {
		RetailerID = retailerID;
	}
	
	/**
	 * 
	 * @param rebStartHrs
	 */
	public void setRebStartHrs(String rebStartHrs) {
		this.rebStartHrs = rebStartHrs;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getRebStartHrs() {
		return rebStartHrs;
	}
	/**
	 * 
	 * @param rebStartMins
	 */
	public void setRebStartMins(String rebStartMins) {
		this.rebStartMins = rebStartMins;
	}
	/**
	 * 
	 * @return
	 */
	public String getRebStartMins() {
		return rebStartMins;
	}
	/**
	 * 
	 * @param rebEndhrs
	 */
	public void setRebEndhrs(String rebEndhrs) {
		this.rebEndhrs = rebEndhrs;
	}
	/**
	 * 
	 * @return
	 */
	public String getRebEndhrs() {
		return rebEndhrs;
	}
	/**
	 * 
	 * @param rebEndMins
	 */
	public void setRebEndMins(String rebEndMins) {
		this.rebEndMins = rebEndMins;
	}
	/**
	 * 
	 * @return
	 */
	public String getRebEndMins() {
		return rebEndMins;
	}
	public int getRetailerLocationID() {
		return retailerLocationID;
	}
	public void setRetailerLocationID(int retailerLocationID) {
		this.retailerLocationID = retailerLocationID;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductName() {
		return productName;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public Users getUser() {
		return user;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Product getProduct() {
		return product;
	}
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	public String getLongDescription() {
		return longDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public String getProductID() {
		return productID;
	}
	public void setScanCode(String scanCode) {
		this.scanCode = scanCode;
	}
	public String getScanCode() {
		return scanCode;
	}
	public String getRetailerLocation() {
		return retailerLocation;
	}
	public void setRetailerLocation(String retailerLocation) {
		this.retailerLocation = retailerLocation;
	}
	
	public void setRebproductId(String rebproductId) {
		this.rebproductId = rebproductId;
	}
	public String getRebproductId() {
		return rebproductId;
	}
	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}
	public int getSupplierId() {
		return supplierId;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setNoOfrebatesIsued(Integer noOfrebatesIsued) {
		this.noOfrebatesIsued = noOfrebatesIsued;
	}
	public Integer getNoOfrebatesIsued() {
		return noOfrebatesIsued;
	}
	public void setNoOfRebatesUsed(Integer noOfRebatesUsed) {
		this.noOfRebatesUsed = noOfRebatesUsed;
	}
	public Integer getNoOfRebatesUsed() {
		return noOfRebatesUsed;
	}
	public Integer getRebateTimeZoneId()
	{
		return rebateTimeZoneId;
	}
	public void setRebateTimeZoneId(Integer rebateTimeZoneId)
	{
		this.rebateTimeZoneId = rebateTimeZoneId;
	}

	/**
	 * @return the locationHidden
	 */
	public String getLocationHidden()
	{
		return locationHidden;
	}
	/**
	 * @param locationHidden the locationHidden to set
	 */
	public void setLocationHidden(String locationHidden)
	{
		this.locationHidden = locationHidden;
	}

	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath() {
		return productImagePath;
	}
	/**
	 * @param productImagePath the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath) {
		if (null == productImagePath || "".equals(productImagePath))
		{
			//Changed below code as IE browser showing broken image if it did not find image
			this.productImagePath = ApplicationConstants.BLANKIMAGE;
		}
		else
		{
			this.productImagePath = productImagePath;
		}
		
		
	}
	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}
	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		if (null==imagePath || "".equals(imagePath))
		{
			//Changed below code as IE browser showing broken image if it did not find image
			this.imagePath = ApplicationConstants.BLANKIMAGE;
		}
		else
		{
			this.imagePath = imagePath;
		}
		
	}
	/**
	 * @return the productNames
	 */
	public String getProductNames() {
		return productNames;
	}
	/**
	 * @param productNames the productNames to set
	 */
	public void setProductNames(String productNames) {
		this.productNames = productNames;
	}
	
	}





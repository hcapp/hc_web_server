/**
 * 
 */
package common.pojo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.constatns.ApplicationConstants;

/**
 * @author kumar_dodda
 */

public class ManageProducts
{

	private String scanCode;
	/**
	 * 
	 */
	private String productName;
	/**
	 * 
	 */
	private String modelNumber;
	/**
	 * 
	 */
	private Double suggestedRetailPrice;
	/**
	 * 
	 */
	private String imagePath;
	/**
	 * 
	 */
	private String longDescription;
	/**
	 * 
	 */
	private String shortDescription;
	/**
	 * 
	 */
	private String warranty;
	/**
	 * 
	 */
	private long productID;
	/**
	 * 
	 */
	private long productMediatypeID;

	/**
	 * 
	 */
	private String productMediaPath;

	private String productMediaType;
	/**
	 * 
	 */
	private String prodAttributeName;
	/**
	 * 
	 */
	private String prodDisplayValue;
	/**
	 * 
	 */
	private String discardReason;

	/**
	 * 
	 */
	private Product product;

	private CommonsMultipartFile[] imageFile;
	private CommonsMultipartFile[] audioFile;
	private CommonsMultipartFile[] videoFile;

	private String imagePaths;

	private String mediaType;
	/**
	 * 
	 */
	private ProductMedia productMedia;
	/**
	 * 
	 */
	private ProductAttributes productAttributes;

	private String searchKey;

	private String categoryName;
	private int imageCount;
	private int audioCount;
	private int videoCount;

	private String prodNameEscape;
	/**
	 * 
	 */

	private List<String> categoryList = new ArrayList<String>();

	/**
	 * 
	 */
	private String category;
	/**
	 * 
	 */
	private String audioMediaPath;
	/**
	 * 
	 */
	private String videoMediaPath;

	private String audioFilePath;

	private String videoFilePath;

	/**
	 * 
	 */
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String nutritionalInfo;
	/**
	 * 
	 */
	private double weight;
	/**
	 * 
	 */
	private String weightUnits;
	/**
	 * 
	 */
	private String scanType;
	/**
	 * 
	 */
	private String prodExpirationDate;
	/**
	 * 
	 */
	private String manufProuductURL;
	/**
     * 
     */
	private String productImagePath;
	/**
	 * 
	 */
	private Long manufactureID;

	/**
     * 
     */
	private CommonsMultipartFile productuploadFilePath;

	/**
	 * 
	 */
	private CommonsMultipartFile[] imageUploadFilePath;

	private CommonsMultipartFile attributeuploadFile;

	private CommonsMultipartFile[] mediaFiles;

	private String prodJson;
	private String attributeName;
	private String attributeValue;

	private String pageNumber;

	private String pageFlag;

	/**
     * 
     */
	private String fileLink;

	/**
     * 
     */
	private Integer QRAudioId;

	/**
     * 
     */
	private Integer QRVideoId;

	/**
     * 
     */
	private Integer QRProduct;
	/**
	 * 
	 */
	private String productWeight;

	private ArrayList<ManageProducts> validProductList;

	private ArrayList<ManageProducts> invalidProductList;
	private String errMsg;
	private String discardedRecordsFIleName;
	private String discardedAttrRecordsFIleName;
	private String strweight;

	/**
	 * 
	 */
	private String strSuggestedRetailPrice;
	/**
	 * 
	 */
	private String key;

	/**
	 * @return the qRProduct
	 */
	public Integer getQRProduct()
	{
		return QRProduct;
	}

	/**
	 * @param qRProduct
	 *            the qRProduct to set
	 */
	public void setQRProduct(Integer qRProduct)
	{
		QRProduct = qRProduct;
	}

	/**
	 * @return the qRAudioId
	 */
	public Integer getQRAudioId()
	{
		return QRAudioId;
	}

	/**
	 * @return the qRVideoId
	 */
	public Integer getQRVideoId()
	{
		return QRVideoId;
	}

	/**
	 * @param qRAudioId
	 *            the qRAudioId to set
	 */
	public void setQRAudioId(Integer qRAudioId)
	{
		QRAudioId = qRAudioId;
	}

	/**
	 * @param qRVideoId
	 *            the qRVideoId to set
	 */
	public void setQRVideoId(Integer qRVideoId)
	{
		QRVideoId = qRVideoId;
	}

	/**
	 * @return the fileLink
	 */
	public String getFileLink()
	{
		return fileLink;
	}

	/**
	 * @param fileLink
	 *            the fileLink to set
	 */
	public void setFileLink(String fileLink)
	{
		this.fileLink = fileLink;
	}

	/**
	 * 
	 */
	private int success;
	/**
	 * 
	 */
	private int failure;
	/**
	 * 
	 */
	private int total;
	/**
	 * 
	 */
	private int logId;
	/**
	 * 
	 */
	private String contantEmail;
	/**
	 * 
	 */
	private String firstName;
	/**
	 * 
	 */
	private String suggestedRetPrice;

	/**
	 * @return the attributeName
	 */
	public String getAttributeName()
	{
		return attributeName;
	}

	/**
	 * @param attributeName
	 *            the attributeName to set
	 */
	public void setAttributeName(String attributeName)
	{
		this.attributeName = attributeName;
	}

	/**
	 * @return the attributeValue
	 */
	public String getAttributeValue()
	{
		return attributeValue;
	}

	/**
	 * @param attributeValue
	 *            the attributeValue to set
	 */
	public void setAttributeValue(String attributeValue)
	{
		this.attributeValue = attributeValue;
	}

	/**
	 * @return the attributeuploadFile
	 */
	public CommonsMultipartFile getAttributeuploadFile()
	{
		return attributeuploadFile;
	}

	/**
	 * @param attributeuploadFile
	 *            the attributeuploadFile to set
	 */
	public void setAttributeuploadFile(CommonsMultipartFile attributeuploadFile)
	{
		this.attributeuploadFile = attributeuploadFile;
	}

	/**
	 * @return the imageUploadFilePath
	 */
	public CommonsMultipartFile[] getImageUploadFilePath()
	{
		return imageUploadFilePath;
	}

	/**
	 * @param imageUploadFilePath
	 *            the imageUploadFilePath to set
	 */
	public void setImageUploadFilePath(CommonsMultipartFile[] imageUploadFilePath)
	{
		this.imageUploadFilePath = imageUploadFilePath;
	}

	/**
	 * @return the productuploadFilePath
	 */
	public CommonsMultipartFile getProductuploadFilePath()
	{
		return productuploadFilePath;
	}

	/**
	 * @param productuploadFilePath
	 *            the productuploadFilePath to set
	 */
	public void setProductuploadFilePath(CommonsMultipartFile productuploadFilePath)
	{
		this.productuploadFilePath = productuploadFilePath;
	}

	/**
	 * @return the imageUploadFilePath
	 */

	/*
	 * public CommonsMultipartFile getImageUploadFilePath() { return
	 * imageUploadFilePath; }
	 *//**
	 * @param imageUploadFilePath
	 *            the imageUploadFilePath to set
	 */
	/*
	 * public void setImageUploadFilePath(CommonsMultipartFile
	 * imageUploadFilePath) { this.imageUploadFilePath = imageUploadFilePath; }
	 */

	/**
	 * 
	 */
	public ManageProducts()
	{

		// TODO Auto-generated constructor stub
	}

	/**
	 * @param scanCode
	 * @param productName
	 * @param modelNumber
	 * @param suggestedRetailPrice
	 * @param imagePath
	 * @param longDescription
	 * @param shortDescription
	 * @param warranty
	 * @param productMediatypeID
	 * @param productMediaPath
	 * @param prodAttributeName
	 * @param prodDisplayValue
	 * @param product
	 * @param productMedia
	 * @param productAttributes
	 * @param categoryList
	 * @param category
	 * @param audioMediaPath
	 * @param videoMediaPath
	 * @param description
	 * @param nutritionalInfo
	 * @param scanType
	 * @param prodExpirationDate
	 * @param manufProuductURL
	 * @param productImagePath
	 *            ;
	 */
	public ManageProducts(String scanCode, String productName, String modelNumber, Double suggestedRetailPrice, String imagePath,
			String longDescription, String shortDescription, String warranty, long productID, long productMediatypeID, String productMediaPath,
			String prodAttributeName, String prodDisplayValue, Product product, ProductMedia productMedia, ProductAttributes productAttributes,
			List<String> categoryList, String category, String videoMediaPath, String audioMediaPath, String description, String nutritionalInfo,
			double weight, String weightUnits, String scanType, String prodExpirationDate, String manufProuductURL, String productImagePath,
			Long manufactureID)
	{
		super();
		this.scanCode = scanCode;
		this.productName = productName;
		this.modelNumber = modelNumber;
		this.suggestedRetailPrice = suggestedRetailPrice;
		this.imagePath = imagePath;
		this.longDescription = longDescription;
		this.shortDescription = shortDescription;
		this.warranty = warranty;
		this.productID = productID;
		this.productMediatypeID = productMediatypeID;
		this.productMediaPath = productMediaPath;
		this.prodAttributeName = prodAttributeName;
		this.prodDisplayValue = prodDisplayValue;
		this.product = product;
		this.productMedia = productMedia;
		this.productAttributes = productAttributes;
		this.categoryList = categoryList;
		this.category = category;
		this.videoMediaPath = videoMediaPath;
		this.audioMediaPath = audioMediaPath;
		this.description = description;
		this.nutritionalInfo = nutritionalInfo;
		this.weight = weight;
		this.weightUnits = weightUnits;
		this.scanType = scanType;
		this.prodExpirationDate = prodExpirationDate;
		this.manufProuductURL = manufProuductURL;
		this.productImagePath = productImagePath;
		this.manufactureID = manufactureID;
	}

	/**
	 * @return the scanCode
	 */
	public String getScanCode()
	{
		if (product != null)
		{
			this.scanCode = product.getScanCode();
		}
		return scanCode;
	}

	/**
	 * @param scanCode
	 *            the scanCode to set
	 */
	public void setScanCode(String scanCode)
	{
		this.scanCode = scanCode;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		if (product != null)
		{
			this.productName = product.getProductName();
		}
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;

		setProdNameEscape(productName);
	}

	/**
	 * @return the modelNumber
	 */
	public String getModelNumber()
	{
		if (product != null)
		{
			this.modelNumber = product.getModelNumber();
		}
		return modelNumber;
	}

	/**
	 * @param modelNumber
	 *            the modelNumber to set
	 */
	public void setModelNumber(String modelNumber)
	{
		this.modelNumber = modelNumber;
	}

	/**
	 * @return the suggestedRetailPrice
	 */
	public Double getSuggestedRetailPrice()
	{
		if (product != null)
		{
			this.suggestedRetailPrice = product.getSuggestedRetailPrice();
		}
		return suggestedRetailPrice;
	}

	/**
	 * @param suggestedRetailPrice
	 *            the suggestedRetailPrice to set
	 */
	public void setSuggestedRetailPrice(Double suggestedRetailPrice)
	{
		this.suggestedRetailPrice = suggestedRetailPrice;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		if (product != null)
		{
			this.imagePath = product.getImagePath();
		}
		return imagePath;
	}

	/**
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		this.imagePath = imagePath;
	}

	/**
	 * @return the longDescription
	 */
	public String getLongDescription()
	{
		if (product != null)
		{
			this.longDescription = product.getLongDescription();
		}
		return longDescription;
	}

	/**
	 * @param longDescription
	 *            the longDescription to set
	 */
	public void setLongDescription(String longDescription)
	{
		this.longDescription = longDescription;
	}

	/**
	 * @return the shortDescription
	 */
	public String getShortDescription()
	{
		if (product != null)
		{
			this.shortDescription = product.getShortDescription();
		}
		return shortDescription;
	}

	/**
	 * @param shortDescription
	 *            the shortDescription to set
	 */
	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	/**
	 * @return the warranty
	 */
	public String getWarranty()
	{
		if (product != null)
		{
			this.warranty = product.getWarranty();
		}
		return warranty;
	}

	/**
	 * @param warranty
	 *            the warranty to set
	 */
	public void setWarranty(String warranty)
	{
		this.warranty = warranty;
	}

	/**
	 * @return the productID
	 */
	public long getProductID()
	{
		return productID;
	}

	/**
	 * @param productID
	 *            the productID to set
	 */
	public void setProductID(long productID)
	{
		this.productID = productID;
	}

	/**
	 * @return the productMediatypeID
	 */
	public long getProductMediatypeID()
	{
		if (productMedia != null)
		{
			this.productMediatypeID = productMedia.getProductMediatypeID();
		}
		return productMediatypeID;
	}

	/**
	 * @param productMediatypeID
	 *            the productMediatypeID to set
	 */
	public void setProductMediatypeID(long productMediatypeID)
	{
		this.productMediatypeID = productMediatypeID;
	}

	/**
	 * @return the productMediaPath
	 */
	public String getProductMediaPath()
	{
		if (productMedia != null)
		{
			this.productMediaPath = productMedia.getProductMediaPath();
		}
		return productMediaPath;
	}

	/**
	 * @param productMediaPath
	 *            the productMediaPath to set
	 */
	public void setProductMediaPath(String productMediaPath)
	{
		this.productMediaPath = productMediaPath;
	}

	/**
	 * @return the prodAttributeName
	 */
	public String getProdAttributeName()
	{
		if (productAttributes != null)
		{
			this.prodAttributeName = productAttributes.getProdAttributeName();
		}
		return prodAttributeName;
	}

	/**
	 * @param prodAttributeName
	 *            the prodAttributeName to set
	 */
	public void setProdAttributeName(String prodAttributeName)
	{
		this.prodAttributeName = prodAttributeName;
	}

	/**
	 * @return the prodDisplayValue
	 */
	public String getProdDisplayValue()
	{
		if (productAttributes != null)
		{
			this.prodDisplayValue = productAttributes.getProdDisplayValue();
		}
		return prodDisplayValue;
	}

	/**
	 * @param prodDisplayValue
	 *            the prodDisplayValue to set
	 */
	public void setProdDisplayValue(String prodDisplayValue)
	{
		this.prodDisplayValue = prodDisplayValue;
	}

	/**
	 * @return the product
	 */
	public Product getProduct()
	{
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(Product product)
	{
		this.product = product;
	}

	/**
	 * @return the productMedia
	 */
	public ProductMedia getProductMedia()
	{
		return productMedia;
	}

	/**
	 * @param productMedia
	 *            the productMedia to set
	 */
	public void setProductMedia(ProductMedia productMedia)
	{
		this.productMedia = productMedia;
	}

	/**
	 * @return the productAttributes
	 */
	public ProductAttributes getProductAttributes()
	{
		return productAttributes;
	}

	/**
	 * @param productAttributes
	 *            the productAttributes to set
	 */
	public void setProductAttributes(ProductAttributes productAttributes)
	{
		this.productAttributes = productAttributes;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	/**
	 * @return the categoryList
	 */
	public List<String> getCategoryList()
	{
		return categoryList;
	}

	/**
	 * @param categoryList
	 *            the categoryList to set
	 */
	public void setCategoryList(List<String> categoryList)
	{
		this.categoryList = categoryList;
	}

	/**
	 * @return the category
	 */
	public String getCategory()
	{
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category)
	{
		this.category = category;
	}

	/**
	 * @return the audioMediaPath
	 */
	public String getAudioMediaPath()
	{
		return audioMediaPath;
	}

	/**
	 * @param audioMediaPath
	 *            the audioMediaPath to set
	 */
	public void setAudioMediaPath(String audioMediaPath)
	{
		if (null == audioMediaPath || "".equals(audioMediaPath))
		{
			this.audioMediaPath = null;
		}
		else
		{
			this.audioMediaPath = audioMediaPath.trim();
		}
	}

	/**
	 * @return the videoMediaPath
	 */
	public String getVideoMediaPath()
	{
		return videoMediaPath;
	}

	/**
	 * @param videoMediaPath
	 *            the videoMediaPath to set
	 */
	public void setVideoMediaPath(String videoMediaPath)
	{
		if (null == videoMediaPath || "".equals(videoMediaPath))
		{
			this.videoMediaPath = null;
		}
		else
		{
			this.videoMediaPath = videoMediaPath.trim();
		}

	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @return the nutritionalInfo
	 */
	public String getNutritionalInfo()
	{
		return nutritionalInfo;
	}

	/**
	 * @param nutritionalInfo
	 *            the nutritionalInfo to set
	 */
	public void setNutritionalInfo(String nutritionalInfo)
	{
		this.nutritionalInfo = nutritionalInfo;
	}

	/**
	 * @return the weight
	 */
	public double getWeight()
	{
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(double weight)
	{
		this.weight = weight;
	}

	/**
	 * @return the weightUnits
	 */
	public String getWeightUnits()
	{
		return weightUnits;
	}

	/**
	 * @param weightUnits
	 *            the weightUnits to set
	 */
	public void setWeightUnits(String weightUnits)
	{
		this.weightUnits = weightUnits;
	}

	/**
	 * @return the scanType
	 */
	public String getScanType()
	{
		return scanType;
	}

	/**
	 * @param scanType
	 *            the scanType to set
	 */
	public void setScanType(String scanType)
	{
		this.scanType = scanType;
	}

	/**
	 * @return the prodExpirationDate
	 */
	public String getProdExpirationDate()
	{
		return prodExpirationDate;
	}

	/**
	 * @param prodExpirationDate
	 *            the prodExpirationDate to set
	 */
	public void setProdExpirationDate(String prodExpirationDate)
	{
		this.prodExpirationDate = prodExpirationDate;
	}

	/**
	 * @return the manufProuductURL
	 */
	public String getManufProuductURL()
	{
		return manufProuductURL;
	}

	/**
	 * @param manufProuductURL
	 *            the manufProuductURL to set
	 */
	public void setManufProuductURL(String manufProuductURL)
	{
		this.manufProuductURL = manufProuductURL;
	}

	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath()
	{
		return productImagePath;
	}

	/**
	 * @param productImagePath
	 *            the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath)
	{
		if (null == productImagePath || "".equals(productImagePath))
		{
			// Changed below code as IE browser showing broken image if it did
			// not find image
			this.productImagePath = ApplicationConstants.BLANKIMAGE;
		}
		else
		{
			this.productImagePath = productImagePath;
		}
	}

	/**
	 * @return the manufactureID
	 */
	public Long getManufactureID()
	{
		return manufactureID;
	}

	/**
	 * @param manufactureID
	 *            the manufactureID to set
	 */
	public void setManufactureID(Long manufactureID)
	{
		this.manufactureID = manufactureID;
	}

	/**
	 * @return the prodJson
	 */
	public String getProdJson()
	{
		return prodJson;
	}

	/**
	 * @param prodJson
	 *            the prodJson to set
	 */
	public void setProdJson(String prodJson)
	{
		this.prodJson = prodJson;
	}

	/**
	 * @return the mediaFiles
	 */
	public CommonsMultipartFile[] getMediaFiles()
	{
		return mediaFiles;
	}

	/**
	 * @param mediaFiles
	 *            the mediaFiles to set
	 */
	public void setMediaFiles(CommonsMultipartFile[] mediaFiles)
	{
		this.mediaFiles = mediaFiles;
	}

	public void setImageFile(CommonsMultipartFile[] imageFile)
	{
		this.imageFile = imageFile;
	}

	public CommonsMultipartFile[] getImageFile()
	{
		return imageFile;
	}

	public void setAudioFile(CommonsMultipartFile[] audioFile)
	{
		this.audioFile = audioFile;
	}

	public CommonsMultipartFile[] getAudioFile()
	{
		return audioFile;
	}

	public void setVideoFile(CommonsMultipartFile[] videoFile)
	{
		this.videoFile = videoFile;
	}

	public CommonsMultipartFile[] getVideoFile()
	{
		return videoFile;
	}

	public void setImagePaths(String imagePaths)
	{
		this.imagePaths = imagePaths;
	}

	public String getImagePaths()
	{
		return imagePaths;
	}

	public void setAudioFilePath(String audioFilePath)
	{
		audioFilePath = audioFilePath;
	}

	public String getAudioFilePath()
	{
		return audioFilePath;
	}

	public void setVideoFilePath(String videoFilePath)
	{
		this.videoFilePath = videoFilePath;
	}

	public String getVideoFilePath()
	{
		return videoFilePath;
	}

	/**
	 * @return the pageNumber
	 */
	public String getPageNumber()
	{
		return pageNumber;
	}

	/**
	 * @param pageNumber
	 *            the pageNumber to set
	 */
	public void setPageNumber(String pageNumber)
	{
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the pageFlag
	 */
	public String getPageFlag()
	{
		return pageFlag;
	}

	/**
	 * @param pageFlag
	 *            the pageFlag to set
	 */
	public void setPageFlag(String pageFlag)
	{
		this.pageFlag = pageFlag;
	}

	public void setProductMediaType(String productMediaType)
	{
		this.productMediaType = productMediaType;
	}

	public String getProductMediaType()
	{
		return productMediaType;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName()
	{
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	public void setImageCount(int imageCount)
	{
		this.imageCount = imageCount;
	}

	public int getImageCount()
	{
		return imageCount;
	}

	public void setVideoCount(int videoCount)
	{
		this.videoCount = videoCount;
	}

	public int getVideoCount()
	{
		return videoCount;
	}

	public void setAudioCount(int audioCount)
	{
		this.audioCount = audioCount;
	}

	public int getAudioCount()
	{
		return audioCount;
	}

	public void setMediaType(String mediaType)
	{
		this.mediaType = mediaType;
	}

	public String getMediaType()
	{
		return mediaType;
	}

	/**
	 * @return the success
	 */
	public int getSuccess()
	{
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(int success)
	{
		this.success = success;
	}

	/**
	 * @return the failure
	 */
	public int getFailure()
	{
		return failure;
	}

	/**
	 * @param failure
	 *            the failure to set
	 */
	public void setFailure(int failure)
	{
		this.failure = failure;
	}

	/**
	 * @return the total
	 */
	public int getTotal()
	{
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total)
	{
		this.total = total;
	}

	/**
	 * @return the logId
	 */
	public int getLogId()
	{
		return logId;
	}

	/**
	 * @param logId
	 *            the logId to set
	 */
	public void setLogId(int logId)
	{
		this.logId = logId;
	}

	/**
	 * @return the discardReason
	 */
	public String getDiscardReason()
	{
		return discardReason;
	}

	/**
	 * @param discardReason
	 *            the discardReason to set
	 */
	public void setDiscardReason(String discardReason)
	{
		this.discardReason = discardReason;
	}

	/**
	 * @return the contantEmail
	 */
	public String getContantEmail()
	{
		return contantEmail;
	}

	/**
	 * @param contantEmail
	 *            the contantEmail to set
	 */
	public void setContantEmail(String contantEmail)
	{
		this.contantEmail = contantEmail;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the suggestedRetPrice
	 */
	public String getSuggestedRetPrice()
	{
		return suggestedRetPrice;
	}

	/**
	 * @param suggestedRetPrice
	 *            the suggestedRetPrice to set
	 */
	public void setSuggestedRetPrice(String suggestedRetPrice)
	{
		this.suggestedRetPrice = suggestedRetPrice;
	}

	/**
	 * @return the productWeigth
	 */
	public String getProductWeight()
	{
		return productWeight;
	}

	/**
	 * @param productWeigth
	 *            the productWeigth to set
	 */
	public void setProductWeight(String productWeight)
	{
		this.productWeight = productWeight;
	}

	public ArrayList<ManageProducts> getValidProductList()
	{
		return validProductList;
	}

	public void setValidProductList(ArrayList<ManageProducts> validProductList)
	{
		this.validProductList = validProductList;
	}

	public ArrayList<ManageProducts> getInvalidProductList()
	{
		return invalidProductList;
	}

	public void setInvalidProductList(ArrayList<ManageProducts> invalidProductList)
	{
		this.invalidProductList = invalidProductList;
	}

	public String getErrMsg()
	{
		return errMsg;
	}

	public void setErrMsg(String errMsg)
	{
		this.errMsg = errMsg;
	}

	public String getDiscardedRecordsFIleName()
	{
		return discardedRecordsFIleName;
	}

	public void setDiscardedRecordsFIleName(String discardedRecordsFIleName)
	{
		this.discardedRecordsFIleName = discardedRecordsFIleName;
	}

	public String getStrweight()
	{
		return strweight;
	}

	public void setStrweight(String strweight)
	{
		this.strweight = strweight;
	}

	public String getDiscardedAttrRecordsFIleName()
	{
		return discardedAttrRecordsFIleName;
	}

	public void setDiscardedAttrRecordsFIleName(String discardedAttrRecordsFIleName)
	{
		this.discardedAttrRecordsFIleName = discardedAttrRecordsFIleName;
	}

	public String getStrSuggestedRetailPrice()
	{
		return strSuggestedRetailPrice;
	}

	public void setStrSuggestedRetailPrice(String strSuggestedRetailPrice)
	{
		this.strSuggestedRetailPrice = strSuggestedRetailPrice;
	}

	/**
	 * @return the key
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key)
	{
		this.key = key;
	}

	public String getProdNameEscape()
	{
		return prodNameEscape;
	}

	public void setProdNameEscape(String prodNameEscape)
	{

		if (null != prodNameEscape && !"".equals(prodNameEscape))
		{
			this.prodNameEscape = prodNameEscape.replace("'", "\\'");
		}
		else
		{

			this.prodNameEscape = prodNameEscape;
		}

	}
}

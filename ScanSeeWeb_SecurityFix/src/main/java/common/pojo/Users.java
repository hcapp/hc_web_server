package common.pojo;

/**
 * @author kumar_dodda
 * 
 */
public class Users {
	private int userType;
	private int supplierId;
	private int retailerId;
	private int retailLocId;
	private String rePassword;
	private boolean terms;
	private String pageForwardValue;
	private Boolean resetPassword;
	private String linkTab;	
	private String isProfileCreated;
	private String retailerLogoImage;
	
	private Boolean eventActive;
	/**
	 * 
	 */
	private String cityHidden;
	
	public String getIsProfileCreated()
	{
		return isProfileCreated;
	}
	public void setIsProfileCreated(String isProfileCreated)
	{
		this.isProfileCreated = isProfileCreated;
	}
	
	public boolean isTerms()
	{
		return terms;
	}
	public void setTerms(boolean terms)
	{
		this.terms = terms;
	}
	public String getRePassword()
	{
		return rePassword;
	}
	public void setRePassword(String rePassword)
	{
		this.rePassword = rePassword;
	}


	/**
	 * 
	 */
	private Long userID;
	/**
	 * 
	 */
	private String firstName;
	/**
	 * 
	 */
	private String lastName;

	 /**
	 * 
	 */
    private String address1;
	 /**
	 * 
	 */
    private String address2;
	 /**
	 * 
	 */
    private String address3;
	 /**
	 * 
	 */
    private String address4;
    /**
	 * 
	 */
    private String city;
    /**
	 * 
	 */
    private String state;
    /**
     * 
     */
    private String stateHidden;
    /**
     * 
     */
    private String stateCodeHidden;
    /**
	 * 
	 */
    private String postalCode;
    /**
	 * 
	 */
    private String citySelectedFlag;
    /**
     * 
     */
    private Long countryID;
    /**
	 * 
	 */
    private String mobilePhone;
    /**
	 * 
	 */
    private String deviceID;
    /**
	 * 
	 */
    private String email;
    /**
	 * 
	 */
    private String password;
    /**
	 * 
	 */
    private int pushNotify;
    /**
	 * 
	 */
    private int firstUseComplete;
    /**
	 * 
	 */
    private int fieldAgent;
    /**
	 * 
	 */
    private int averageFieldAgentRating;
    /**
	 * 
	 */
    private String createddate;
    /**
	 * 
	 */
    private String modifiedDate;
    /**
	 * 
	 */
    private int faceBookAuthenticatedUser;
    /**
	 * 
	 */
    private String userName;
    /**
     * 
     */
    private CountryCode countryCode;
    /**
     * 
     */
    private State states;
	/**
	 * 
	 */
    private String fbUserId;
	
	/**
	 * Login Status.
	 */
    private String captch;
	 
	 public Users() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param userID
	 * @param firstName
	 * @param lastName
	 * @param address1
	 * @param address2
	 * @param address3
	 * @param address4
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param countryID
	 * @param mobilePhone
	 * @param deviceID
	 * @param email
	 * @param password
	 * @param pushNotify
	 * @param firstUseComplete
	 * @param fieldAgent
	 * @param averageFieldAgentRating
	 * @param createddate
	 * @param modifiedDate
	 * @param faceBookAuthenticatedUser
	 * @param userName
	 * @param countryCode
	 */
	public Users(Long userID, String firstName, String lastName, String address1, String address2, String address3, String address4, String city,
			String state, String postalCode, Long countryID, String mobilePhone, String deviceID, String email, String password, int pushNotify,
			int firstUseComplete, int fieldAgent, int averageFieldAgentRating, String createddate, String modifiedDate,
			int faceBookAuthenticatedUser, String userName, CountryCode countryCode, State states)
	{
		super();
		this.userID = userID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.address4 = address4;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.countryID = countryID;
		this.mobilePhone = mobilePhone;
		this.deviceID = deviceID;
		this.email = email;
		this.password = password;
		this.pushNotify = pushNotify;
		this.firstUseComplete = firstUseComplete;
		this.fieldAgent = fieldAgent;
		this.averageFieldAgentRating = averageFieldAgentRating;
		this.createddate = createddate;
		this.modifiedDate = modifiedDate;
		this.faceBookAuthenticatedUser = faceBookAuthenticatedUser;
		this.userName = userName;
		this.countryCode = countryCode;
		this.states = states;
	}
	
	
	private boolean LoginSuccess;

	
	
	public boolean isLoginSuccess() {
		return LoginSuccess;
	}

	public void setLoginSuccess(boolean loginSuccess) {
		LoginSuccess = loginSuccess;
	}
	/**
	 * @param usersid
	 */
	public Users(long userID) {
		super();
		this.userID = userID;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	/**
	 * @return the supplierId
	 */
	public int getSupplierId()
	{
		return supplierId;
	}

	/**
	 * @param supplierId the supplierId to set
	 */
	public void setSupplierId(int supplierId)
	{
		this.supplierId = supplierId;
	}

	/**
	 * @return the retailerId
	 */
	public int getRetailerId()
	{
		return retailerId;
	}

	/**
	 * @param retailerId the retailerId to set
	 */
	public void setRetailerId(int retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * @return the retailLocId
	 */
	public int getRetailLocId()
	{
		return retailLocId;
	}

	/**
	 * @param retailLocId the retailLocId to set
	 */
	public void setRetailLocId(int retailLocId)
	{
		this.retailLocId = retailLocId;
	}
	
	/**
	 * @return the userID
	 */
	public Long getUserID()
	{
		return userID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Long userID)
	{
		this.userID = userID;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	/**
	 * @return the address1
	 */
	public String getAddress1()
	{
		return address1;
	}
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}
	/**
	 * @return the address2
	 */
	public String getAddress2()
	{
		return address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}
	/**
	 * @return the address3
	 */
	public String getAddress3()
	{
		return address3;
	}
	/**
	 * @param address3 the address3 to set
	 */
	public void setAddress3(String address3)
	{
		this.address3 = address3;
	}
	/**
	 * @return the address4
	 */
	public String getAddress4()
	{
		return address4;
	}
	/**
	 * @param address4 the address4 to set
	 */
	public void setAddress4(String address4)
	{
		this.address4 = address4;
	}
	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}
	/**
	 * @return the countryID
	 */
	public Long getCountryID()
	{
		if(countryCode!=null){
			this.countryID = countryCode.getCountryID();
		}
		return countryID;
	}
	/**
	 * @param countryID the countryID to set
	 */
	public void setCountryID(Long countryID)
	{
		this.countryID = countryID;
	}
	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone()
	{
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the deviceID
	 */
	public String getDeviceID()
	{
		return deviceID;
	}
	/**
	 * @param deviceID the deviceID to set
	 */
	public void setDeviceID(String deviceID)
	{
		this.deviceID = deviceID;
	}
	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}
	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}
	/**
	 * @return the pushNotify
	 */
	public int getPushNotify()
	{
		return pushNotify;
	}
	/**
	 * @param pushNotify the pushNotify to set
	 */
	public void setPushNotify(int pushNotify)
	{
		this.pushNotify = pushNotify;
	}
	/**
	 * @return the firstUseComplete
	 */
	public int getFirstUseComplete()
	{
		return firstUseComplete;
	}
	/**
	 * @param firstUseComplete the firstUseComplete to set
	 */
	public void setFirstUseComplete(int firstUseComplete)
	{
		this.firstUseComplete = firstUseComplete;
	}
	/**
	 * @return the fieldAgent
	 */
	public int getFieldAgent()
	{
		return fieldAgent;
	}
	/**
	 * @param fieldAgent the fieldAgent to set
	 */
	public void setFieldAgent(int fieldAgent)
	{
		this.fieldAgent = fieldAgent;
	}
	/**
	 * @return the averageFieldAgentRating
	 */
	public int getAverageFieldAgentRating()
	{
		return averageFieldAgentRating;
	}
	/**
	 * @param averageFieldAgentRating the averageFieldAgentRating to set
	 */
	public void setAverageFieldAgentRating(int averageFieldAgentRating)
	{
		this.averageFieldAgentRating = averageFieldAgentRating;
	}
	/**
	 * @return the createddate
	 */
	public String getCreateddate()
	{
		return createddate;
	}
	/**
	 * @param createddate the createddate to set
	 */
	public void setCreateddate(String createddate)
	{
		this.createddate = createddate;
	}
	/**
	 * @return the modifiedDate
	 */
	public String getModifiedDate()
	{
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(String modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the faceBookAuthenticatedUser
	 */
	public int getFaceBookAuthenticatedUser()
	{
		return faceBookAuthenticatedUser;
	}
	/**
	 * @param faceBookAuthenticatedUser the faceBookAuthenticatedUser to set
	 */
	public void setFaceBookAuthenticatedUser(int faceBookAuthenticatedUser)
	{
		this.faceBookAuthenticatedUser = faceBookAuthenticatedUser;
	}
	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName)
	{
		if(null != userName){
			userName = userName.trim();
			this.userName = userName;
		}
		else{
			this.userName = userName;
		}
		
	}
	/**
	 * @return the countryCode
	 */
	public CountryCode getCountryCode()
	{
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(CountryCode countryCode)
	{
		this.countryCode = countryCode;
	}
	/**
	 * @return the states
	 */
	public State getStates()
	{
		return states;
	}
	/**
	 * @param states the states to set
	 */
	public void setStates(State states)
	{
		this.states = states;
	}
	/**
	 * @return the pageForwardValue
	 */
	public String getPageForwardValue() {
		return pageForwardValue;
	}
	/**
	 * @param pageForwardValue the pageForwardValue to set
	 */
	public void setPageForwardValue(String pageForwardValue) {
		this.pageForwardValue = pageForwardValue;
	}
	
	/**
	 * @return the resetPassword
	 */
	public Boolean getResetPassword() {
		return resetPassword;
	}
	/**
	 * @param resetPassword the resetPassword to set
	 */
	public void setResetPassword(Boolean resetPassword) {
		this.resetPassword = resetPassword;
	}
	/**
	 * @return the linkTab
	 */
	public String getLinkTab()
	{
		return linkTab;
	}
	/**
	 * @param linkTab the linkTab to set
	 */
	public void setLinkTab(String linkTab)
	{
		this.linkTab = linkTab;
	}
	/**
	 * @return the fbUserId
	 */
	public String getFbUserId()
	{
		return fbUserId;
	}
	/**
	 * @param fbUserId the fbUserId to set
	 */
	public void setFbUserId(String fbUserId)
	{
		this.fbUserId = fbUserId;
	}
	public String getCaptch()
	{
		return captch;
	}
	public void setCaptch(String captch)
	{
		this.captch = captch;
	}
	public String getCityHidden()
	{
		return cityHidden;
	}
	public void setCityHidden(String cityHidden)
	{
		this.cityHidden = cityHidden;
	}
	public String getStateHidden()
	{
		return stateHidden;
	}
	public void setStateHidden(String stateHidden)
	{
		this.stateHidden = stateHidden;
	}
	public String getStateCodeHidden()
	{
		return stateCodeHidden;
	}
	public void setStateCodeHidden(String stateCodeHidden)
	{
		this.stateCodeHidden = stateCodeHidden;
	}
	/**
	 * @param citySelectedFlag the citySelectedFlag to set
	 */
	public void setCitySelectedFlag(String citySelectedFlag)
	{
		this.citySelectedFlag = citySelectedFlag;
	}
	/**
	 * @return the citySelectedFlag
	 */
	public String getCitySelectedFlag()
	{
		return citySelectedFlag;
	}
	/**
	 * @return the retailerLogoImage
	 */
	public String getRetailerLogoImage()
	{
		return retailerLogoImage;
	}
	/**
	 * @param retailerLogoImage the retailerLogoImage to set
	 */
	public void setRetailerLogoImage(String retailerLogoImage)
	{
		this.retailerLogoImage = retailerLogoImage;
	}
	/**
	 * @return the eventActive
	 */
	public Boolean getEventActive() {
		return eventActive;
	}
	/**
	 * @param eventActive the eventActive to set
	 */
	public void setEventActive(Boolean eventActive) {
		this.eventActive = eventActive;
	}
}

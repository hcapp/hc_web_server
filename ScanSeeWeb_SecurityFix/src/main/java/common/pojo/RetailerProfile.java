package common.pojo;
import common.util.Utility;
public class RetailerProfile
{
	
	private int retailID;
	private String retailerName;
	private String address1;
	private String address2;
    private String city;
    private String state;
    private String postalCode;
    private String contactPhone;
	private String legalAuthorityFName;
	private String legalAuthorityLName;
	private String contactFName;
	private String contactLName;
	private String contactPhoneNo;
	private String contactEmail;
	private String cityHidden;
	private String stateHidden;
	private String stateCodeHidden;
	private String tabIndex;
	private String password;
	private String retypePassword;
	private String retypeEmail;
	private String terms;
	private String isLocation;
	private boolean islocation;
	private boolean ishiddenlocation;
	/**
	 * 
	 */
   private String webUrl = null;
   /**
	 * 
	 */
	private Long numOfStores;
	/**
	 * 
	 */
   private String url = null;
	/**
	 * 
	 */
	private String bCategory;
	/**
	 * 
	 */
	private boolean nonProfit;
	/**
	 * 
	 */
	private String bCategoryHidden;
	 /**
	 * 
	 */
	private String keyword;
	/**
	 * 
	 */
	private Double retailerLocationLatitude;
	/**
	 * 
	 */
	private Double retailerLocationLongitude;
	
	/**
	 * 
	 */
	private String citySelectedFlag;
	/**
	 * 
	 */
	private String geoError;
	/**
	 * 
	 */
	private String filters;
	
	/**
	 * 
	 */
	private String filterValues;
	
	/**
	 * 
	 */
	private String filterCategory;
	/**
	 * 
	 */
	private String hiddenFilterCategory;
	
	/**
	 * 
	 */
	public RetailerProfile()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param retailID
	 * @param retailerName
	 * @param address1
	 * @param address2
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param contactPhone
	 * @param legalAuthorityFName
	 * @param legalAuthorityLName
	 * @param contactFName
	 * @param contactLName
	 * @param contactPhoneNo
	 * @param contactEmail
	 * @param cityHidden
	 * @param bCategory
	 * @param nonProfit
	 * @param bCategoryHidden
	 */
	public RetailerProfile(int retailID, String retailerName, String address1, String address2, String city, String state, String postalCode,
			String contactPhone, String legalAuthorityFName, String legalAuthorityLName, String contactFName, String contactLName,
			String contactPhoneNo, String contactEmail, String cityHidden, String bCategory, boolean nonProfit, String bCategoryHidden ,String password,String retypePassword,String retypeEmail,String terms)
	{
		super();
		this.retailID = retailID;
		this.retailerName = retailerName;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.contactPhone = contactPhone;
		this.legalAuthorityFName = legalAuthorityFName;
		this.legalAuthorityLName = legalAuthorityLName;
		this.contactFName = contactFName;
		this.contactLName = contactLName;
		this.contactPhoneNo = contactPhoneNo;
		this.contactEmail = contactEmail;
		this.cityHidden = cityHidden;
		this.bCategory = bCategory;
		this.nonProfit = nonProfit;
		this.bCategoryHidden = bCategoryHidden;
		this.password=password;
		this.retypePassword=retypePassword;
		this.retypeEmail=retypeEmail;
		this.terms=terms;
	}
	/**
	 * @return the retailID
	 */
	public int getRetailID()
	{
		return retailID;
	}
	/**
	 * @param retailID the retailID to set
	 */
	public void setRetailID(int retailID)
	{
		this.retailID = retailID;
	}
	/**
	 * @return the retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}
	/**
	 * @param retailerName the retailerName to set
	 */
	public void setRetailerName(String retailerName)
	{
		this.retailerName = Utility.removeWhiteSpaces(retailerName);
	}
	/**
	 * @return the address1
	 */
	public String getAddress1()
	{
		return address1;
	}
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1)
	{
		this.address1 = Utility.removeWhiteSpaces(address1);
	}
	/**
	 * @return the address2
	 */
	public String getAddress2()
	{
		return address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2)
	{
		this.address2 = Utility.removeWhiteSpaces(address2);
	}
	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}
	/**
	 * @return the contactPhone
	 */
	public String getContactPhone()
	{
		return contactPhone;
	}
	/**
	 * @param contactPhone the contactPhone to set
	 */
	public void setContactPhone(String contactPhone)
	{
		this.contactPhone = contactPhone;
	}
	/**
	 * @return the legalAuthorityFName
	 */
	public String getLegalAuthorityFName()
	{
		return legalAuthorityFName;
	}
	/**
	 * @param legalAuthorityFName the legalAuthorityFName to set
	 */
	public void setLegalAuthorityFName(String legalAuthorityFName)
	{
		this.legalAuthorityFName = legalAuthorityFName;
	}
	/**
	 * @return the legalAuthorityLName
	 */
	public String getLegalAuthorityLName()
	{
		return legalAuthorityLName;
	}
	/**
	 * @param legalAuthorityLName the legalAuthorityLName to set
	 */
	public void setLegalAuthorityLName(String legalAuthorityLName)
	{
		this.legalAuthorityLName = legalAuthorityLName;
	}
	/**
	 * @return the contactFName
	 */
	public String getContactFName()
	{
		return contactFName;
	}
	/**
	 * @param contactFName the contactFName to set
	 */
	public void setContactFName(String contactFName)
	{
		this.contactFName = Utility.removeWhiteSpaces(contactFName);
	}
	/**
	 * @return the contactLName
	 */
	public String getContactLName()
	{
		return contactLName;
	}
	/**
	 * @param contactLName the contactLName to set
	 */
	public void setContactLName(String contactLName)
	{
		this.contactLName = Utility.removeWhiteSpaces(contactLName);
	}
	/**
	 * @return the contactPhoneNo
	 */
	public String getContactPhoneNo()
	{
		return contactPhoneNo;
	}
	/**
	 * @param contactPhoneNo the contactPhoneNo to set
	 */
	public void setContactPhoneNo(String contactPhoneNo)
	{
		this.contactPhoneNo = contactPhoneNo;
	}
	/**
	 * @return the contactEmail
	 */
	public String getContactEmail()
	{
		return contactEmail;
	}
	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = Utility.removeWhiteSpaces(contactEmail);
	}
	/**
	 * @return the cityHidden
	 */
	public String getCityHidden()
	{
		return cityHidden;
	}
	/**
	 * @param cityHidden the cityHidden to set
	 */
	public void setCityHidden(String cityHidden)
	{
		this.cityHidden = cityHidden;
	}
	/**
	 * @return the bCategory
	 */
	public String getbCategory()
	{
		return bCategory;
	}
	/**
	 * @param bCategory the bCategory to set
	 */
	public void setbCategory(String bCategory)
	{
		this.bCategory = bCategory;
	}
	/**
	 * @return the nonProfit
	 */
	public boolean isNonProfit()
	{
		return nonProfit;
	}
	/**
	 * @param nonProfit the nonProfit to set
	 */
	public void setNonProfit(boolean nonProfit)
	{
		this.nonProfit = nonProfit;
	}
	/**
	 * @return the bCategoryHidden
	 */
	public String getbCategoryHidden()
	{
		return bCategoryHidden;
	}
	/**
	 * @param bCategoryHidden the bCategoryHidden to set
	 */
	public void setbCategoryHidden(String bCategoryHidden)
	{
		this.bCategoryHidden = bCategoryHidden;
	}
	public void setTabIndex(String tabIndex) {
		this.tabIndex = tabIndex;
	}
	public String getTabIndex() {
		return tabIndex;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getPassword()
	{
		return password;
	}
	public String getRetypePassword()
	{
		return retypePassword;
	}
	public void setRetypePassword(String retypePassword)
	{
		this.retypePassword = retypePassword;
	}
	public String getRetypeEmail()
	{
		return retypeEmail;
	}
	public void setRetypeEmail(String retypeEmail)
	{
		this.retypeEmail = retypeEmail;
	}
	public String getTerms()
	{
		return terms;
	}
	public void setTerms(String terms)
	{
		this.terms = terms;
	}
	public String getIsLocation()
	{
		return isLocation;
	}
	public void setIsLocation(String isLocation)
	{
		this.isLocation = isLocation;
	}
	public boolean isIslocation() {
		return islocation;
	}
	public void setIslocation(boolean islocation) {
		this.islocation = islocation;
	}
	/**
	 * @return the webUrl
	 */
	public String getWebUrl()
	{
		return webUrl;
	}
	/**
	 * @param webUrl the webUrl to set
	 */
	public void setWebUrl(String webUrl)
	{
		this.webUrl = Utility.removeWhiteSpaces(webUrl);
	}
	/**
	 * @return the numOfStores
	 */
	public Long getNumOfStores()
	{
		return numOfStores;
	}
	/**
	 * @param numOfStores the numOfStores to set
	 */
	public void setNumOfStores(Long numOfStores)
	{
		this.numOfStores = numOfStores;
	}
	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}
	/**
	 * @return the ishiddenlocation
	 */
	public boolean isIshiddenlocation()
	{
		return ishiddenlocation;
	}
	/**
	 * @param ishiddenlocation the ishiddenlocation to set
	 */
	public void setIshiddenlocation(boolean ishiddenlocation)
	{
		this.ishiddenlocation = ishiddenlocation;
	}
	/**
	 * @return the keyword
	 */
	public String getKeyword()
	{
		return keyword;
	}
	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(String keyword)
	{
		this.keyword = Utility.removeWhiteSpaces(keyword);
	}
	/**
	 * @return the retailerLocationLatitude
	 */
	public Double getRetailerLocationLatitude()
	{
		return retailerLocationLatitude;
	}
	/**
	 * @param retailerLocationLatitude the retailerLocationLatitude to set
	 */
	public void setRetailerLocationLatitude(Double retailerLocationLatitude)
	{
		this.retailerLocationLatitude = retailerLocationLatitude;
	}
	/**
	 * @return the retailerLocationLongitude
	 */
	public Double getRetailerLocationLongitude()
	{
		return retailerLocationLongitude;
	}
	/**
	 * @param retailerLocationLongitude the retailerLocationLongitude to set
	 */
	public void setRetailerLocationLongitude(Double retailerLocationLongitude)
	{
		this.retailerLocationLongitude = retailerLocationLongitude;
	}
	public String getStateHidden()
	{
		return stateHidden;
	}
	public void setStateHidden(String stateHidden)
	{
		this.stateHidden = stateHidden;
	}
	public String getStateCodeHidden()
	{
		return stateCodeHidden;
	}
	public void setStateCodeHidden(String stateCodeHidden)
	{
		this.stateCodeHidden = stateCodeHidden;
	}
	/**
	 * @param citySelectedFlag the citySelectedFlag to set
	 */
	public void setCitySelectedFlag(String citySelectedFlag)
	{
		this.citySelectedFlag = citySelectedFlag;
	}
	/**
	 * @return the citySelectedFlag
	 */
	public String getCitySelectedFlag()
	{
		return citySelectedFlag;
	}
	/**
	 * @return the geoError
	 */
	public String getGeoError()
	{
		return geoError;
	}
	/**
	 * @param geoError the geoError to set
	 */
	public void setGeoError(String geoError)
	{
		this.geoError = geoError;
	}
	/**
	 * @return the filters
	 */
	public String getFilters() {
		return filters;
	}
	/**
	 * @param filters the filters to set
	 */
	public void setFilters(String filters) {
		this.filters = filters;
	}
	
	/**
	 * @return the filterValues
	 */
	public String getFilterValues() {
		return filterValues;
	}
	/**
	 * @param filterValues the filterValues to set
	 */
	public void setFilterValues(String filterValues) {
		this.filterValues = filterValues;
	}
	/**
	 * @return the filterCategory
	 */
	public String getFilterCategory() {
		return filterCategory;
	}
	/**
	 * @param filterCategory the filterCategory to set
	 */
	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}
	/**
	 * @return the hiddenFilterCategory
	 */
	public String getHiddenFilterCategory() {
		return hiddenFilterCategory;
	}
	/**
	 * @param hiddenFilterCategory the hiddenFilterCategory to set
	 */
	public void setHiddenFilterCategory(String hiddenFilterCategory) {
		this.hiddenFilterCategory = hiddenFilterCategory;
	}
}

package common.pojo;

public class SupplierQRCodeResponse {

	private int productPageID;
	
	private int audioPageID;
	
	private int videoPageID;
	
	private int fileLinkPageID;

	/**
	 * @return the productPageID
	 */
	public int getProductPageID() {
		return productPageID;
	}

	/**
	 * @return the audioPageID
	 */
	public int getAudioPageID() {
		return audioPageID;
	}

	/**
	 * @return the videoPageID
	 */
	public int getVideoPageID() {
		return videoPageID;
	}

	/**
	 * @return the fileLinkPageID
	 */
	public int getFileLinkPageID() {
		return fileLinkPageID;
	}

	/**
	 * @param productPageID the productPageID to set
	 */
	public void setProductPageID(int productPageID) {
		this.productPageID = productPageID;
	}

	/**
	 * @param audioPageID the audioPageID to set
	 */
	public void setAudioPageID(int audioPageID) {
		this.audioPageID = audioPageID;
	}

	/**
	 * @param videoPageID the videoPageID to set
	 */
	public void setVideoPageID(int videoPageID) {
		this.videoPageID = videoPageID;
	}

	/**
	 * @param fileLinkPageID the fileLinkPageID to set
	 */
	public void setFileLinkPageID(int fileLinkPageID) {
		this.fileLinkPageID = fileLinkPageID;
	}
	
}

package common.pojo;

import java.util.ArrayList;

public class ProductJson
{
	private ArrayList<ManageProducts> productData;

	
	private ArrayList<RetailerLocation> locationData;
	
	

	
	private ArrayList<PlanInfo> planData;


	/**
	 * @return the locationData
	 */
	public ArrayList<RetailerLocation> getLocationData()
	{
		return locationData;
	}

	/**
	 * @param locationData the locationData to set
	 */
	public void setLocationData(ArrayList<RetailerLocation> locationData)
	{
		this.locationData = locationData;
	}

	/**
	 * @return the productData
	 */
	public ArrayList<ManageProducts> getProductData()
	{
		return productData;
	}

	/**
	 * @param productData the productData to set
	 */
	public void setProductData(ArrayList<ManageProducts> productData)
	{
		this.productData = productData;
	}

	/**
	 * @return the planData
	 */
	public ArrayList<PlanInfo> getPlanData()
	{
		return planData;
	}

	/**
	 * @param planData the planData to set
	 */
	public void setPlanData(ArrayList<PlanInfo> planData)
	{
		this.planData = planData;
	}
}

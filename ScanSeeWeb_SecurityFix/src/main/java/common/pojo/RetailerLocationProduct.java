/**
 * Project     : Scan See
 * File        : RetailerLocationProduct.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;


import java.util.ArrayList;

import common.util.Utility;


/**
 * Bean used in RetailerLocationProduct.
 * 
 * @author kumar_dodda
 */
public class RetailerLocationProduct {

	/**
	 * 
	 *//*
	private Long retailID;*/
	/**
	 * 
	 */
	private Long retailLocationProductID;
	/**
	 * 
	 */
	private Long retailLocationID;
	/**
	 * 
	 */
	private Long productID;
	/**
	 * 
	 */
	private String retailLocationProductDesc;
	/**
	 * 
	 */
	private int quantity;
	/**
	 * 
	 */
	private Double price;
	/**
	 * 
	 */
	private Double salePrice;
	/**
	 * 
	 */
	private String saleStartDate;
	/**
	 * 
	 */
	private String saleEndDate;
	/**
	 * 
	 */
	private int hotDeal;
	/**
	 * 
	 */
	private String createdDate;
	/**
	 * 
	 */
	private String modifiedDate;
	/**
	 * 
	 */
	private int availableToOrderOnline;
	/**
	 * 
	 */
	private Product product;
	/**
	 * 
	 */
	private RetailerLocation retailerLocation;
	/**
	 * 
	 */
	private Retailer retailer;
	/**
	 * 
	 */
	private Long retailID;
	
	/**
	 * 
	 */
	private String productName;
	/**
	 * 
	 */
	private String retailLocIDHidden;
	/**
	 * 
	 */
	private String LocID; 
	/**
	 * 
	 */
	private String searchKey;
	/**
	 * 
	 */
	private String storeIdentification;
	/**
	 * 
	 */
    private String productImagePath;
    /**
	 * 
	 */
	private int productAdded;
    /**
	 * 
	 */
	private int applyAll;
	 /**
	 * 
	 */
	private String dbSaleDate;
	/**
	 * The list for storing type RetailerLocation .
	 */
	
	private int searchStatus;
	  /**
	 * 
	 */
	private int applyStatus;
	  /**
	 * 
	 */
	private ArrayList<RetailerLocation> locationDetails;
	/**
     * 
     */
	public String retailLocationIDs;
	/**
     * 
     */
	private String categoryName;
	/**
	 * 
	 */
	private String modelNumber;
	/**
	 * 
	 */
	private String scanCode;
	/**
     * 
     */
	private String longDescription;
	/**
	 * 
	 */
	private int rowNumber;
	
	private String recordCount;
	
	public int getRowNumber()
	{
		return rowNumber;
	}

	public void setRowNumber(int rowNumber)
	{
		this.rowNumber = rowNumber;
	}

	private ArrayList<RetailerLocationProductJson> retailerLocationProductJson;
	
	private String strRetailerLocationProductJson;
	
	
	public String getStrRetailerLocationProductJson()
	{
		return strRetailerLocationProductJson;
	}

	public void setStrRetailerLocationProductJson(String strRetailerLocationProductJson)
	{
		this.strRetailerLocationProductJson = strRetailerLocationProductJson;
	}

	public ArrayList<RetailerLocationProductJson> getRetailerLocationProductJson()
	{
		return retailerLocationProductJson;
	}

	public void setRetailerLocationProductJson(ArrayList<RetailerLocationProductJson> retailerLocationProductJson)
	{
		this.retailerLocationProductJson = retailerLocationProductJson;
	}

	public String getLocID() {
		return LocID;
	}

	public void setLocID(String locID) {
		LocID = locID;
	}

	
	public RetailerLocationProduct() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param retailLocationProductID
	 * @param retailLocationID
	 * @param productID
	 * @param retailLocationProductDesc
	 * @param quantity
	 * @param price
	 * @param salePrice
	 * @param saleStartDate
	 * @param saleEndDate
	 * @param hotDeal
	 * @param createdDate
	 * @param modifiedDate
	 * @param availableToOrderOnline
	 * @param product
	 * @param retailerLocation
	 * @param retailer
	 * @param retailID
	 */
	public RetailerLocationProduct(Long retailLocationProductID,
			Long retailLocationID, Long productID,
			String retailLocationProductDesc, int quantity, Double price,
			Double salePrice, String saleStartDate, String saleEndDate,
			int hotDeal, String createdDate, String modifiedDate,
			int availableToOrderOnline, Product product,
			RetailerLocation retailerLocation, Retailer retailer, Long retailID) {
		super();
		this.retailLocationProductID = retailLocationProductID;
		this.retailLocationID = retailLocationID;
		this.productID = productID;
		this.retailLocationProductDesc = retailLocationProductDesc;
		this.quantity = quantity;
		this.price = price;
		this.salePrice = salePrice;
		this.saleStartDate = saleStartDate;
		this.saleEndDate = saleEndDate;
		this.hotDeal = hotDeal;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.availableToOrderOnline = availableToOrderOnline;
		this.product = product;
		this.retailerLocation = retailerLocation;
		this.retailer = retailer;
		this.retailID = retailID;
	}

	/*public Long getRetailID() {
		return retailID;
	}

	public void setRetailID(Long retailID) {
		this.retailID = retailID;
	}*/

	/**
	 * @return the retailLocationProductID
	 */
	public Long getRetailLocationProductID() {
		return retailLocationProductID;
	}

	/**
	 * @param retailLocationProductID
	 *            the retailLocationProductID to set
	 */
	public void setRetailLocationProductID(Long retailLocationProductID) {
		this.retailLocationProductID = retailLocationProductID;
	}

	/**
	 * @return the retailLocationID
	 */
	public Long getRetailLocationID() {
		return retailLocationID;
	}

	/**
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(Long retailLocationID) {
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the productID
	 */
	public Long getProductID() {
		return productID;
	}

	/**
	 * @param productID
	 *            the productID to set
	 */
	public void setProductID(Long productID) {
		this.productID = productID;
	}

	/**
	 * @return the retailLocationProductDesc
	 */
	public String getRetailLocationProductDesc() {
		return retailLocationProductDesc;
	}

	/**
	 * @param retailLocationProductDesc
	 *            the retailLocationProductDesc to set
	 */
	public void setRetailLocationProductDesc(String retailLocationProductDesc) {
		this.retailLocationProductDesc = retailLocationProductDesc;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice() {
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * @return the saleStartDate
	 */
	public String getSaleStartDate() {
		return saleStartDate;
	}

	/**
	 * @param saleStartDate
	 *            the saleStartDate to set
	 */
	public void setSaleStartDate(String saleStartDate) {
		this.saleStartDate = saleStartDate;
	}

	/**
	 * @return the saleEndDate
	 */
	public String getSaleEndDate() {
		return saleEndDate;
	}

	/**
	 * @param saleEndDate
	 *            the saleEndDate to set
	 */
	public void setSaleEndDate(String saleEndDate) {
		this.saleEndDate = saleEndDate;
	}

	/**
	 * @return the hotDeal
	 */
	public int getHotDeal() {
		return hotDeal;
	}

	/**
	 * @param hotDeal
	 *            the hotDeal to set
	 */
	public void setHotDeal(int hotDeal) {
		this.hotDeal = hotDeal;
	}

	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the modifiedDate
	 */
	public String getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            the modifiedDate to set
	 */
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the availableToOrderOnline
	 */
	public int getAvailableToOrderOnline() {
		return availableToOrderOnline;
	}

	/**
	 * @param availableToOrderOnline
	 *            the availableToOrderOnline to set
	 */
	public void setAvailableToOrderOnline(int availableToOrderOnline) {
		this.availableToOrderOnline = availableToOrderOnline;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * @return the retailerLocation
	 */
	public RetailerLocation getRetailerLocation() {
		return retailerLocation;
	}

	/**
	 * @param retailerLocation
	 *            the retailerLocation to set
	 */
	public void setRetailerLocation(RetailerLocation retailerLocation) {
		this.retailerLocation = retailerLocation;
	}

	/**
	 * @return the retailer
	 */
	public Retailer getRetailer() {
		return retailer;
	}

	/**
	 * @param retailer
	 *            the retailer to set
	 */
	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}

	/**
	 * @return the retailID
	 */
	public Long getRetailID() {
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Long retailID) {
		this.retailID = retailID;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}

	/**
	 * @return the retailLocIDHidden
	 */
	public String getRetailLocIDHidden()
	{
		return retailLocIDHidden;
	}

	/**
	 * @param retailLocIDHidden the retailLocIDHidden to set
	 */
	public void setRetailLocIDHidden(String retailLocIDHidden)
	{
		this.retailLocIDHidden = retailLocIDHidden;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	/**
	 * @return the storeIdentification
	 */
	public String getStoreIdentification()
	{
		return storeIdentification;
	}

	/**
	 * @param storeIdentification the storeIdentification to set
	 */
	public void setStoreIdentification(String storeIdentification)
	{
		this.storeIdentification = storeIdentification;
	}

	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath()
	{
		return productImagePath;
	}

	/**
	 * @param productImagePath the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath)
	{
		this.productImagePath = productImagePath;
	}

	/**
	 * @return the locationDetails
	 */
	public ArrayList<RetailerLocation> getLocationDetails()
	{
		return locationDetails;
	}

	/**
	 * @param locationDetails the locationDetails to set
	 */
	public void setLocationDetails(ArrayList<RetailerLocation> locationDetails)
	{
		this.locationDetails = locationDetails;
	}

	/**
	 * @return the productAdded
	 */
	public int getProductAdded()
	{
		return productAdded;
	}

	/**
	 * @param productAdded the productAdded to set
	 */
	public void setProductAdded(int productAdded)
	{
		this.productAdded = productAdded;
	}

	/**
	 * @return the applyAll
	 */
	public int getApplyAll()
	{
		return applyAll;
	}

	/**
	 * @param applyAll the applyAll to set
	 */
	public void setApplyAll(int applyAll)
	{
		this.applyAll = applyAll;
	}
	
	/**
	 * @return the dbSaleDate
	 */
	public String getDbSaleDate()
	{
		return dbSaleDate;
	}

	/**
	 * @param dbSaleDate the dbSaleDate to set
	 */
	public void setDbSaleDate(String dbSaleDate)
	{
		if (!"".equals(Utility.checkNull(dbSaleDate))) {
			this.dbSaleDate = Utility.convertDBdate(dbSaleDate);
		} else if ("".equals(Utility.checkNull(dbSaleDate))){
			this.dbSaleDate = dbSaleDate;
		}
	}

	/**
	 * @return the searchStatus
	 */
	public int getSearchStatus()
	{
		return searchStatus;
	}

	/**
	 * @param searchStatus the searchStatus to set
	 */
	public void setSearchStatus(int searchStatus)
	{
		this.searchStatus = searchStatus;
	}

	/**
	 * @return the applyStatus
	 */
	public int getApplyStatus()
	{
		return applyStatus;
	}

	/**
	 * @param applyStatus the applyStatus to set
	 */
	public void setApplyStatus(int applyStatus)
	{
		this.applyStatus = applyStatus;
	}

	/**
	 * @return the retailLocationIDs
	 */
	public String getRetailLocationIDs()
	{
		return retailLocationIDs;
	}

	/**
	 * @param retailLocationIDs the retailLocationIDs to set
	 */
	public void setRetailLocationIDs(String retailLocationIDs)
	{
		this.retailLocationIDs = retailLocationIDs;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName()
	{
		return categoryName;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	/**
	 * @return the modelNumber
	 */
	public String getModelNumber()
	{
		return modelNumber;
	}

	/**
	 * @param modelNumber the modelNumber to set
	 */
	public void setModelNumber(String modelNumber)
	{
		this.modelNumber = modelNumber;
	}

	/**
	 * @return the scanCode
	 */
	public String getScanCode()
	{
		return scanCode;
	}

	/**
	 * @param scanCode the scanCode to set
	 */
	public void setScanCode(String scanCode)
	{
		this.scanCode = scanCode;
	}

	/**
	 * @return the longDescription
	 */
	public String getLongDescription()
	{
		return longDescription;
	}

	/**
	 * @param longDescription the longDescription to set
	 */
	public void setLongDescription(String longDescription)
	{
		this.longDescription = longDescription;
	}

	public String getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(String recordCount)
	{
		this.recordCount = recordCount;
	}

	
	
}

/**
 * 
 */
package common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class Manufacture {
	/**
	 * 
	 */
	private Long manufactureID;
	/**
	 * 
	 */
	private String manufName;
	/**
	 * 
	 */
	private String ManufDescription;
	/**
	 * 
	 */
	private String Address1; 
	/**
	 * 
	 */
	private String Address2; 
	/**
	 * 
	 */
	private String Address3; 
	/**
	 * 
	 */
	private String Address4;
	/**
	 * 
	 */
	private String city;
	/**
	 * 
	 */
	private String state;
	/**
	 * 
	 */
	private String postalCode;
	/**
	 * 
	 */
	private String createDate;
	/**
	 * 
	 */
	private Long countryID;
	/**
	 * 
	 */
	private String modifyDate; 
	/**
	 * 
	 */
	private Long modifyUserID;
	/**
	 * 
	 */
	private String progParticipant;
	/**
	 * 
	 */
	private Long etilizeManufID;
	/**
	 * 
	 */
	private String corporatePhoneNo;
	/**
	 * 
	 */
	private String legalAuthorityFname;
	/**
	 * 
	 */
	private String legalAuthorityLname;
	/**
	 * 
	 */
	private Users user;

	/**
	 * 
	 */
	public Manufacture() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param manufactureID
	 * @param manufName
	 * @param manufDescription
	 * @param address1
	 * @param address2
	 * @param address3
	 * @param address4
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param createDate
	 * @param countryID
	 * @param modifyDate
	 * @param modifyUserID
	 * @param progParticipant
	 * @param etilizeManufID
	 * @param corporatePhoneNo
	 * @param legalAuthorityFname
	 * @param legalAuthorityLname
	 * @param user
	 */
	public Manufacture(Long manufactureID, String manufName,
			String manufDescription, String address1, String address2,
			String address3, String address4, String city, String state,
			String postalCode, String createDate, Long countryID,
			String modifyDate, Long modifyUserID, String progParticipant,
			Long etilizeManufID, String corporatePhoneNo,
			String legalAuthorityFname, String legalAuthorityLname, Users user) {
		super();
		this.manufactureID = manufactureID;
		this.manufName = manufName;
		ManufDescription = manufDescription;
		Address1 = address1;
		Address2 = address2;
		Address3 = address3;
		Address4 = address4;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.createDate = createDate;
		this.countryID = countryID;
		this.modifyDate = modifyDate;
		this.modifyUserID = modifyUserID;
		this.progParticipant = progParticipant;
		this.etilizeManufID = etilizeManufID;
		this.corporatePhoneNo = corporatePhoneNo;
		this.legalAuthorityFname = legalAuthorityFname;
		this.legalAuthorityLname = legalAuthorityLname;
		this.user = user;
	}

	/**
	 * @return the manufactureID
	 */
	public Long getManufactureID() {
		return manufactureID;
	}

	/**
	 * @param manufactureID the manufactureID to set
	 */
	public void setManufactureID(Long manufactureID) {
		this.manufactureID = manufactureID;
	}

	/**
	 * @return the manufName
	 */
	public String getManufName() {
		return manufName;
	}

	/**
	 * @param manufName the manufName to set
	 */
	public void setManufName(String manufName) {
		this.manufName = manufName;
	}

	/**
	 * @return the manufDescription
	 */
	public String getManufDescription() {
		return ManufDescription;
	}

	/**
	 * @param manufDescription the manufDescription to set
	 */
	public void setManufDescription(String manufDescription) {
		ManufDescription = manufDescription;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return Address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		Address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return Address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		Address2 = address2;
	}

	/**
	 * @return the address3
	 */
	public String getAddress3() {
		return Address3;
	}

	/**
	 * @param address3 the address3 to set
	 */
	public void setAddress3(String address3) {
		Address3 = address3;
	}

	/**
	 * @return the address4
	 */
	public String getAddress4() {
		return Address4;
	}

	/**
	 * @param address4 the address4 to set
	 */
	public void setAddress4(String address4) {
		Address4 = address4;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the countryID
	 */
	public Long getCountryID() {
		return countryID;
	}

	/**
	 * @param countryID the countryID to set
	 */
	public void setCountryID(Long countryID) {
		this.countryID = countryID;
	}

	/**
	 * @return the modifyDate
	 */
	public String getModifyDate() {
		return modifyDate;
	}

	/**
	 * @param modifyDate the modifyDate to set
	 */
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

	/**
	 * @return the modifyUserID
	 */
	public Long getModifyUserID() {
		if (user != null) {
			this.modifyUserID = user.getUserID();
		}
		return modifyUserID;
	}

	/**
	 * @param modifyUserID the modifyUserID to set
	 */
	public void setModifyUserID(Long modifyUserID) {
		this.modifyUserID = modifyUserID;
	}

	/**
	 * @return the progParticipant
	 */
	public String getProgParticipant() {
		return progParticipant;
	}

	/**
	 * @param progParticipant the progParticipant to set
	 */
	public void setProgParticipant(String progParticipant) {
		this.progParticipant = progParticipant;
	}

	/**
	 * @return the etilizeManufID
	 */
	public Long getEtilizeManufID() {
		return etilizeManufID;
	}

	/**
	 * @param etilizeManufID the etilizeManufID to set
	 */
	public void setEtilizeManufID(Long etilizeManufID) {
		this.etilizeManufID = etilizeManufID;
	}

	/**
	 * @return the corporatePhoneNo
	 */
	public String getCorporatePhoneNo() {
		return corporatePhoneNo;
	}

	/**
	 * @param corporatePhoneNo the corporatePhoneNo to set
	 */
	public void setCorporatePhoneNo(String corporatePhoneNo) {
		this.corporatePhoneNo = corporatePhoneNo;
	}

	/**
	 * @return the legalAuthorityFname
	 */
	public String getLegalAuthorityFname() {
		return legalAuthorityFname;
	}

	/**
	 * @param legalAuthorityFname the legalAuthorityFname to set
	 */
	public void setLegalAuthorityFname(String legalAuthorityFname) {
		this.legalAuthorityFname = legalAuthorityFname;
	}

	/**
	 * @return the legalAuthorityLname
	 */
	public String getLegalAuthorityLname() {
		return legalAuthorityLname;
	}

	/**
	 * @param legalAuthorityLname the legalAuthorityLname to set
	 */
	public void setLegalAuthorityLname(String legalAuthorityLname) {
		this.legalAuthorityLname = legalAuthorityLname;
	}

	/**
	 * @return the user
	 */
	public Users getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(Users user) {
		this.user = user;
	}
}

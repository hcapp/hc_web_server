package common.pojo;

import java.util.List;
import java.util.Map;

public class PreferencesInfo
{

	private boolean email;
	private boolean cellPhone;
	private Long userId;
	private String prefrences;

	private int emailAlert;
	/**
	 * emailStatus for storing the String value yes or No based on emailAlert
	 * one or Zero respectively
	 */
	private String emailStatus;

	private int cellPhoneAlert;
	/**
	 * cellPhoneStatus for storing the String value yes or No based on
	 * cellPhoneAlert one or Zero respectively
	 */

	private String cellPhoneStatus;

	/**
	 * @return the prefrences
	 */
	
	
	
	
	
	public String getPrefrences()
	{
		return prefrences;
	}

	/**
	 * @return the emailAlert
	 */
	public int getEmailAlert()
	{
		return emailAlert;
	}

	/**
	 * @param emailAlert the emailAlert to set
	 */
	public void setEmailAlert(int emailAlert)
	{
		this.emailAlert = emailAlert;
	}

	/**
	 * @return the emailStatus
	 */
	public String getEmailStatus()
	{
		return emailStatus;
	}

	/**
	 * @param emailStatus the emailStatus to set
	 */
	public void setEmailStatus(String emailStatus)
	{
		this.emailStatus = emailStatus;
	}

	/**
	 * @return the cellPhoneAlert
	 */
	public int getCellPhoneAlert()
	{
		return cellPhoneAlert;
	}

	/**
	 * @param cellPhoneAlert the cellPhoneAlert to set
	 */
	public void setCellPhoneAlert(int cellPhoneAlert)
	{
		this.cellPhoneAlert = cellPhoneAlert;
	}

	/**
	 * @return the cellPhoneStatus
	 */
	public String getCellPhoneStatus()
	{
		return cellPhoneStatus;
	}

	/**
	 * @param cellPhoneStatus the cellPhoneStatus to set
	 */
	public void setCellPhoneStatus(String cellPhoneStatus)
	{
		this.cellPhoneStatus = cellPhoneStatus;
	}

	/**
	 * @param prefrences
	 *            the prefrences to set
	 */
	public void setPrefrences(String prefrences)
	{
		this.prefrences = prefrences;
	}

	private List preferencesList;

	public List getPreferencesList()
	{
		return preferencesList;
	}

	public void setPreferencesList(List preferencesList)
	{
		this.preferencesList = preferencesList;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public boolean isEmail()
	{
		return email;
	}

	public void setEmail(boolean email)
	{
		this.email = email;
	}

	public boolean isCellPhone()
	{
		return cellPhone;
	}

	public void setCellPhone(boolean cellPhone)
	{
		this.cellPhone = cellPhone;
	}

}

package common.constatns;

/**
 * Class to store all application related constants.
 * 
 * @author manjunatha_gh
 */
public class ApplicationConstants {

	/**
	 * MethodStart declared as String for logger messages.
	 */
	public static final String METHODSTART = "In side method >>> ";
	/**
	 * MethodEnd declared as String for logger messages.
	 */
	public static final String METHODEND = " Exiting method >>> ";

	/**
	 * for database status.
	 */
	public static final String STATUS = "Status";

	/**
	 * for database error number.
	 */
	public static final String ERRORNUMBER = "ErrorNumber";

	/**
	 * for database Error Message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";

	/**
	 * for database Error Message.
	 */
	public static final String SUCCESS = "SUCCESS";
	/**
	 * for database Error Message.
	 */
	public static final String FLAG = "Success";
	/**
	 * Declared USERNAME for first use.
	 */
	public static final String USERNAME = "UserName";
	/**
	 * Declared password for first use.
	 */
	public static final String FACEBOOK = "FaceBookAuthenticatedUser";

	/**
	 * FAILURE declared as String for displaying message in FAILURE.
	 */
	public static final String FAILURE = "FAILURE";

	/**
	 * userID declared as String for UserID text.
	 */
	public static final String USERID = "UserID";
	/**
	 * This constant is used for This Location Retailer Pagination.
	 */
	public static final String THISLOCATIONRETAILERSCREEN = "This Location - Retailer List";
	/**
	 * This constant is used for This Location Product Pagination.
	 */
	public static final String THISLOCATIONPRODUCTSSCREEN = "This Location - Product List";
	/**
	 * for empty string.
	 */
	public static final String EMPTYSTR = " ";
	/**
	 * Declared Comma.
	 */
	public static final String COMMA = ",";
	/**
	 * NoRecordsFound declared as String for getting response code.
	 */
	public static final String NORECORDSFOUND = "10005";
	/**
	 * NoRecordsFoundText declared as String for getting response text.
	 */
	public static final String NORECORDSFOUNDTEXT = "No Records Found.";
	/**
	 * NoRetailerFoundText declared as String for getting response text.
	 */
	public static final String NORETAILERFOUNDTEXT = "No Retailer Found.";
	public static final String DUPLICATE_USER = "DuplicateUser";

	public static final String DUPLICATE_RETAILERNAME = "DuplicateRetailerFlag";

	public static final String DUPLICATE_SUPPLIERNAME = "DuplicateSupplierFlag";
	public static final String EMAIL = "EmailNotMatch";
	public static final String PASSWORD = "passwordNotMatch";
	public static final String PASSWORDLength = "passwordLength";
	public static final String PASSWORDMATCH = "passwordMatch";
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static final String INVALIDEMAIL = "invalidemail";
	public static final String INVALIDCONTPHONE = "invalidcontphone";
	public static final String INVALIDDOB = "invalidDob";
	public static final String INVALIDCORPPHONE = "invalidcorphone";
	public static final String STATE = "selectstate";
	public static final String CITY = "selectcity";
	public static final String INVALIDCONTACTTITLE = "selectContactTitle";
	public static final String STORE_COUNT = "storeCount";
	/**
	 * INVALIDURL declared as String constant variable .
	 */
	public static final String INVALIDURL = "invalidurl";
	/**
	 * TERM declared as String constant variable .
	 */
	public static final String TERM = "termcondition";
	public static final String PHONENUM_PATTERN = "\\(\\d{3}\\)\\d{3}-\\d{4}";
	public static final String ERROR_SAVE = "errorsave";
	public static final String ERROR_FETCH = "errorfetch";
	public static final String DIGIT = "^(?=.*\\d).{4,60}$";

	public static final String DUPLICATE_STOREID = "DuplicateStoreId";

	/**
	 * Declared NOTAPPLICABLE for use.
	 */
	public static final String NOTAPPLICABLE = "NotApplicable";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String EMAILCONFIG = "Email";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPHOST = "SMTP_Host";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPPORT = "SMTP_Port";

	public static final String ACHBNKTAB = "BankPay";
	public static final String CREDITCARDPAY = "CreditCardPay";
	public static final String ALTERNATEPAYMENT = "AlternatePayment";

	/**
     * 
     */
	public static final String BUSINESSCATEGORY = "selectcategory";
	public static final String POPULATIONCENTER = "selectCity";
	public static final String DEALRETAILER = "selectRet";
	public static final String DEALRETAILERLOC = "selectRetLoc";
	/**
	 * for findOnlineStores.
	 */
	public static final String FINDONLINESTOESMODULENAME = "FindOnlineStores";
	/**
	 * for external api failure.
	 */
	public static final int EXTERNALAPIFAILURE = 11002;

	public static final String DATEAFTER = "after";
	public static final String DATEBEFORE = "before";
	public static final String DATEEQUAL = "equal";

	public static final String DATESTARTCURRENT = "startcurrent";
	public static final String DATEENDCURRENT = "endcurrent";
	public static final String EXPIREDDATE = "expireDate";

	public static final String JBOSS_HOME = "JBOSS_HOME";
	public static final String DEFAULT = "default";
	public static final String SERVER = "server";
	public static final String DEPLOY = "deploy";
	public static final String ROOTWAR = "ROOT.war";
	public static final String IMAGES = "Images";
	public static final String SUPPLIER = "manufacturer";
	public static final String RETAILER = "retailer";
	public static final String QRPATH = "qrtemp";
	public static final String TEMP = "temp";

	public static final String STANDALONE = "standalone";

	public static final String DEPLOYMENT = "deployments";
	/**
	 * ExceptionOccurred declared as String for logger messages.
	 */
	public static final String EXCEPTIONOCCURRED = "Exception Occurred in  >>> ";

	/**
	 * Declared CONFIGURATIONTYPESTOCK as String for App Configuration.
	 */
	public static final String CONFIGURATIONTYPESTOCK = "Stock";

	/**
	 * for stores.
	 */
	public static final String STORES = " Stores";

	/**
	 * for empty string.
	 */
	public static final String EMPTYSTRING = " ";

	/**
	 * Variable for External API response format.
	 */
	public static final String EXTERNALAPIFORMAT = "xml";

	/**
	 * for Find near by .
	 */
	public static final String FINDNEARBYMODULENAME = "findNearBy";

	/**
	 * ErrorOccurred declared as String for logger messages.
	 */
	public static final String ERROROCCURRED = "Error Occurred in  >>> ";

	/**
	 * InsuffientData declared as String for getting error code.
	 */
	public static final String INSUFFICIENTDATA = "10007";
	/**
	 * Can be used for all insufficient requests.
	 */
	public static final String INSUFFICENTTEXT = "Insufficient request";
	/**
	 * This constant is used for Hot deals search pagination.
	 */
	public static final String HOTDEALSSEARCHSCREEN = "Hot Deals - Hot Deals List";

	/**
	 * Declared ADMINEMAILID as String for Web Configuration.
	 */
	public static final String ADMINEMAILID = "AdminMailID";
	/**
	 * Declared WEBREGISTRATION as String for Web Configuration.
	 */
	public static final String WEBREGISTRATION = "Website Registration";

	/**
	 * Declared ScreenName for first use.
	 */
	public static final String SCREENNAME = "ScreenName";

	/**
	 * for constructor.
	 */
	public static final String WLHISTORYSCREENNAME = "Wish List History";

	/**
	 * Declared CLRSCREENNAME for my gallery.
	 */
	public static final String CLRSCREENNAME = "CLR Screen";

	/**
	 * Declared LOWERLIMIT for pagination.
	 */
	public static final String LOWERLIMIT = "LowerLimit";

	/**
	 * for scan now search product pagination..
	 */
	public static final String SCANNOWSEARCHPRODPAGINATION = "General - Search Product Name";

	/**
	 * DBSTATUS declared for database out variable.
	 */
	public static final String DBSTATUS = "Status";
	/**
	 * SuccessCode declared as String for getting success response code.
	 */
	public static final String SUCCESSCODE = "10000";

	/**
	 * for constructor.
	 */

	public static final String SLHISTORYSCREENNAME = "Shopping List History";

	/**
	 * Declared For adding shopping list history product to list and favorites.
	 */
	public static final String SLHISTORYPRODUCTEXISTINLIST = "Some item(s) were not added because they were already in List";
	/**
	 * for adding unassigned product.
	 */
	public static final String ADDINGUNASSINGEDPRODUCT = "Added to list";
	/**
	 * Declared For adding shopping list history product to list and favorites.
	 */
	public static final String SLHISTORYPRODUCTEXISTINFAVORITES = "Some item(s) were not added because they were already in Favorites";
	/**
	 * ADDPRODUCTMAINTOSHOPPINGLIST declared as String for getting success
	 * response text.
	 */
	public static final String ADDPRODUCTMAINTOSHOPPINGLIST = "Added to Favorites";
	/**
	 * Declared For adding shopping list history product to list and favorites.
	 * if product already exists we are sending this message.
	 */
	public static final String SLHISTORYPRODUCTEXISTINLISTANDFAVORITES = "Some item(s) were not added because they were already in Favorites and List";
	/**
	 * Declared For adding shopping list history product to list and favorites.
	 */
	public static final String SLHISTORYPRODADDEDTOLISTFAVORITESTEXT = "Added to List and Favorites";

	/**
	 * Declared For adding shopping list history product to list and favorites.
	 * if product already exists we are sending this message.
	 */
	public static final String SLHSTADDEDTOLISTNOTFAVORITES = "Added to List and some item(s) were not added to Favorites  because they were already in Favorites";

	/**
	 * Declared For adding shopping list history product to list and favorites.
	 * if product already exists we are sending this message.
	 */
	public static final String SLHSTADDEDTOFAVORITESNOTLIST = "Added to Favorites and some item(s) were not added to List  because they were already in List";

	public static final String SUPPLIERDASHBORAD = "SUPPLIERDASHBOARD";
	public static final String NOPRODUCTSETUPDONE = "NOPRODUCT";
	public static final String NOCHOOSEPLANSETUPDONE = "NOCHOOSEPLAN";

	public static final String RETAILERDASHBORAD = "RETAILERDASHBOARD";
	public static final String NORETAILERLOGO = "NORETAILERLOGO";
	public static final String NORETAILERLOCATION = "NORETAILERLOCATION";
	public static final String NORETAILERPRODUCT = "NORETAILERPRODUCT";
	public static final String NORETAILERCHOOSEPLAN = "NORETAILERCHOOSEPLAN";

	public static final String NOSHOPPERPREFERENCES = "NOSHOPPERPREFERENCES";
	public static final String NOSHOPPERSELECTSCHOOL = "NOSHOPPERSELECTSCHOOL";
	public static final String NOSHOPPERDOUBLECHECK = "NOSHOPPERDOUBLECHECK";
	public static final String SHOPPERDASHBOARD = "SHOPPERDASHBOARD";

	/**
	 * Declared For adding product to today shopping list . if product already
	 * exists we are sending this message.
	 */
	public static final String SLADDPRODTLEXISTS = "Some item(s) were not added to List  because they were already in List";

	/**
	 * DuplicateUserText declared as String for getting response text.
	 */
	public static final String DUPLICATPRODUCTTEXT = "Product already exists in WishList";

	/**
	 * DuplicateUserText declared as String for getting response text.
	 */
	public static final String DUPLICATEFAVPRODUCTTEXT = "Product already added to Favorites.";

	/*
	 * DuplicateUserText declared as String for getting response text.
	 */
	public static final String DUPLICATESLPRODUCTTEXT = "Product already exists in ShoppingList";

	/**
	 * FOR sharing coupon by email subject
	 */
	public static final String SHARECOUPONBYEMAILSUBJECT = "Coupon Information Shared Via ScanSeeWeb";

	/**
	 * FOR sharing Loyalty by email subject
	 */
	public static final String SHARELOYALTYBYEMAILSUBJECT = "Loyalty Information Shared Via ScanSeeWeb";
	/**
	 * FOR sharing Loyalty by email subject
	 */
	public static final String SHAREREBATEBYEMAILSUBJECT = "Rebate Information Shared Via ScanSeeWeb";

	/**
	 * SHAREPRODUCTINFO declared as String for getting success response text.
	 */
	public static final String SHAREPRODUCTINFO = "Email sent successfully.";

	/**
	 * InvalidEmailAddessText declared as String for invalid email address error
	 * text.
	 */
	public static final String INVALIDEMAILADDRESSTEXT = "Invalid Email Address";

	public static final String USEREMAILIDNOTEXISTTOSHARE = "Please Enter your email Id in user information screen to share the Item.";

	public static final String QUOTE = "\"\"";
	public static final String SHAREHOTDEALINFO = "HotDeal Information Shared Via ScanSee";

	/**
	 * Declared for Duplicate product exists for wish list search add screen.
	 */
	public static final String PRODUCTEXISTS = "Exists";

	public static final String PRODUCTADDEDSUCCESSTEXT = "Product Successfully added";

	/**
	 * Blank Image space holder
	 */

	public static final String BLANKIMAGE = "/ScanSeeWeb/images/blankImage.gif";

	/**
	 * Declared for sharing product info via Email
	 */
	public static final String SHAREPRODUCTINFOBYEMAIL = "Product Information Shared Via ScanSee";
	/**
	 * Declared CONFIGURATIONTYPESTOCK as String for App Configuration.
	 */
	public static final String CONFIGURATIONTYPESHAREURL = "ShareURL";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String EMAILSHAREURL = "EmailShareURL";

	/**
	 * Declared Alternate Image as String for App Configuration.
	 */
	public static final String ALTERNATEIMG = "/ScanSeeWeb/images/alternateImg.png";

	/**
	 * variable declared for constant DuplicateStore
	 */
	public static final String DUPLICATE_STORE = "DuplicateStore";

	/**
	 * Blank Image space holder
	 */

	public static final String IMAGENOTFOUND = "/ScanSeeWeb/images/imageNotFound.png";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String FACEBOOKCONFG = "FaceBook";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SCANSEEBASEURL = "Base_URL";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String FBAPPID = "App_Id";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String FBAPPSECRETID = "App_Secret";

	/**
	 * Declared UPDATELOCALEUSRRADIUS for user settings to save radius.
	 */
	public static final String UPDATELOCALEUSRRADIUS = "Global Settings Updated Successfully.";

	/**
	 * PASSWORDUPDATED declared as String for getting success response text.
	 */
	public static final String PASSWORDUPDATED = "Password updated";

	/**
	 * Declared UPDATELOCALEUSRRADIUS for user settings to save radius.
	 */
	public static final String ERROROCCURED = "Error occurred while updating data.";

	/**
	 * SAVEUSERINFORMATION declared as String for getting success response text.
	 */
	public static final String SAVEUSERINFORMATION = "User information updated";

	/**
	 * SETUSERFAVCATEGORIES declared as String for getting success response
	 * text.
	 */
	public static final String SETUSERFAVCATEGORIES = "Preferred favourite categories are updated";

	/**
	 * Declared FORGOTPWD as String for Login Screen.
	 */
	public static final String FORGOT_PWD = "ForgotPwd";

	/**
	 * Declared LOGIN as String for Login Screen.
	 */
	public static final String LOGIN = "Login";

	/**
	 * PNGIMAGEFORMAT declared as string to get the image format
	 */
	public static final String PNGIMAGEFORMAT = ".png";

	/**
	 * PNGIMAGE declared as string to get the image format
	 */
	public static final String PNGIMAGE = "png";
	/**
	 * QRCODEIMAGEHEIGHT declared as int
	 */
	public static final int QRCODEIMAGEHEIGHT = 200;

	/**
	 * QRCODEIMAGEWIDTH declared as int
	 */
	public static final int QRCODEIMAGEWIDTH = 200;
	public static final String QRCODECONFIGURATION = "QR Code Configuration";
	// public static final String URL_PATTERN =
	// "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	public static final String URL_PATTERN = "^(https://|http://|ftp://|file://|www.)[-a-zA-Z0-9+&@#/%?=~_|!:.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	public static final String ENDDATE = "ENDDATE";
	public static final String STARTDATE = "STARTDATE";
	public static final String ROOTIMAGEFOLDER = "images";
	public static final String RETAILERPAGEURL = "2000.htm?";
	public static final String RETAILERCREATEDPAGEURL = "2100.htm?";
	public static final String SPECIALOFFERPAGEURL = "2200.htm?";
	public static final String SUPPLIERPRODUCTPAGEURL = "1000.htm?";
	public static final String AMPERSAND = "&";
	public static final String SUBJECT_MSG_FOR_ATTRIBUTE_MAILSEND = "Supplier Rejected Attributes List shared from ScanSee";
	public static final String CONTENT_MSG_FOR_ATTRIBUTE_MAILSEND = "Please find the attached file for rejected Attribute(s) and correct them.";
	public static final String SUBJECT_MSG_FOR_PRODUCT_MAILSEND = "Supplier Rejected Product(s) List shared from ScanSee";
	public static final String CONTENT_MSG_FOR_PRODUCT_MAILSEND = "Please find the attached file for rejected Product(s) and correct them.";
	public static final String TEMPFOLDER = "temp";
	public static final String UPLOADIMAGEPATH = "../images/upload_imageRtlr.png";
	public static final String QRTYPEMAINMENUPAGE = "Main Menu Page";
	public static final String QRTYPELANDINGPAGE = "Landing Page";
	public static final String QRTYPESPLOFFERPAGE = "Special Offer Page";
	public static final String CONTENT_MSG_FOR_FORGOTPWD_MAILSEND = "We've sent information about resetting your password to your recovery email address. Please find the username and auto generated password to login.";
	public static final String SUBJECT_MSG_FOR_FORGOTPWD_MAILSEND = "ScanSee : Forgot Password";
	public static final String SUBJECT_MSG_FOR_SUCCESS_REGISTRATION = "ScanSee : Registration Successful";
	public static final String SCANSEE_LOGO_FOR_MAILSENDING = "ScanSeeWeb/images/ScanSeeMailLogo.png";
	public static final String SUBJECT_MSG_FOR_LOCATION_MAILSEND = "Please find the attached file for rejected location list and correct them.";
	public static final String SERVER_CONFIGURATION = "Configuration of server";
	public static final String GENERAL_IMAGES = "General-images";
	public static final String INVALIDPOSTALCODE = "invalidpostalcode";
	public static final String GEOERROR = "GEOERROR";
	/**
	 * Declared WEBURL as String for RetailerRegistration screen.
	 */
	public static final String WEBURL = "invalidurl";

	/**
	 * Declared MANAGE_RET_LOCATION_QUERY as String for ManageLocation.
	 */
	public static final String MANAGE_RET_LOCATION_QUERY = "IF EXISTS(SELECT 1 FROM RetailLocation WHERE RetailLocationID = ? AND CorporateAndStore = 1) "
			+ "BEGIN "
			+ "\n"
			+ " UPDATE Retailer SET CorporatePhoneNo = ? "
			+ " WHERE RetailID = (SELECT RetailID FROM RetailLocation WHERE RetailLocationID = ?) "

			+ " UPDATE RetailLocation SET StoreIdentification = ? "
			+ ", Address1 = ? "
			+ ", State = ? "
			+ ", City = ? "
			+ ", PostalCode = ? "
			/*
			 * +
			 * ", RetailLocationLatitude = (SELECT Latitude FROM GeoPosition WHERE PostalCode = ? ) "
			 * +
			 * ", RetailLocationLongitude = (SELECT Longitude FROM GeoPosition WHERE PostalCode = ? ) "
			 */
			+ ", RetailLocationLatitude = ? "
			+ ", RetailLocationLongitude = ? "
			+ ", RetailLocationUrl = ? "
			+ " WHERE RetailLocationID = ? "
			+ "\n"
			+ "END "

			+ "ELSE "
			+ "BEGIN "
			+ "\n"
			+ " UPDATE RetailLocation SET StoreIdentification =  ? "
			+ ", Address1 =  ? "
			+ ", State =  ? "
			+ ", City = ? "
			+ ", PostalCode = ? "
			/*
			 * +
			 * ", RetailLocationLatitude = (SELECT Latitude FROM GeoPosition WHERE PostalCode = ? ) "
			 * +
			 * ", RetailLocationLongitude = (SELECT Longitude FROM GeoPosition WHERE PostalCode = ? ) "
			 */
			+ ", RetailLocationLatitude = ? "
			+ ", RetailLocationLongitude = ? "
			+ ", RetailLocationUrl = ? "
			+ " WHERE RetailLocationID = ? "

			+ " UPDATE Contact "
			+ " SET ContactPhone = ? "
			+ " WHERE ContactID = (SELECT ContactID FROM RetailContact WHERE RetailLocationID = ? ) "
			+ "\n" + " END";

	/**
	 * SPRODUCTTEMPLATE declared as String constant variable .
	 */

	public static final String SPRODUCTTEMPLATE = "Supplier_Product_Upload_Templet.csv";
	/**
	 * SPRODUCTTEMPLATE declared as String constant variable .
	 */
	public static final String RPRODUCTTEMPLATE = "ProductTemplateRetailer.csv";
	/**
	 * SATTRIBUTETEMPLATE declared as String constant variable .
	 */
	public static final String SATTRIBUTETEMPLATE = "Supplier_Product_Attribute_Upload_Templet.csv";

	/**
	 * RLOCATIONTEMPLATE declared as String constant variable .
	 */
	public static final String RLOCATIONTEMPLATE = "RetailLocationnew.csv";
	public static final String INVALID_CAPTCHA = "InavalidCaptcha";
	public static final String RECAPTCHA = "reCaptcha";
	public static final String RECAPTCHAPUBLICKEY = "reCaptchaPublicKey";
	public static final String RECAPTCHAPRIVATEKEY = "reCaptchaPrivateKey";

	public static final String SUBJECT_MSG_FOR_RETAILER_PRODUCT_MAILSEND = "Please find the attached file for rejected products list and correct them.";
	public static final String RECORDSDISCARDED = "Records Disacarded";

	/**
	 * ZIPFILE declared as String constant variable .
	 */
	public static final String ZIPFILE = "zip";
	/**
	 * MP3FORMAT declared as String constant variable .
	 */
	public static final String MP3FORMAT = "mp3";
	/**
	 * MP4FORMAT declared as String constant variable .
	 */
	public static final String MP4FORMAT = "mp4";
	/**
	 * UPLOAD_DEFAULT_IMAGE declared as String constant variable .
	 */
	public static final String UPLOAD_IMAGE_PATH = "/ScanSeeWeb/images/upload_imageRtlr.png";

	/**
	 * Schema name for Store procedures!
	 */
	public static final String WEBSCHEMA = "dbo";
	/**
	 * for empty list.
	 */
	public static final String EMPTY_LIST = "empty";

	public static final String AD_END_DATE = "01/01/2900";

	public static final String AD_END_TIME = "00";

	public static final String RETAILERMEDIAPATH = "Web Retailer Media Server Configuration";

	/**
	 * NO_MSG_DISPLAY declared as String for displaying message in noMSGDisplay.
	 */
	public static final String NO_MSG_DISPLAY = "noMSGDisplay";

	public static final String APPSITEEMAILID = "Free App Listing EmailID";

	public static final String ABOUTUSICON = "info_uploadicon.png";

	public static final String ABOUTUSTEXT = "About Us";

	public static final String DUPLICATE_BUSINESSNAME = "DuplicateBusinessName";

	/**
	 * Schema name for Store procedures!
	 */
	public static final String APPSCHEMA = "dbo";

	/**
	 * supplierService declared as String constant variable.
	 */
	public static final String SUPPLIERSERVICE = "supplierService";

	/**
	 * retailerService declared as String constant variable.
	 */
	public static final String RETAILERSERVICE = "retailerService";

	/**
	 * exception occurred declared as String constant variable.
	 */
	public static final String EXCEPTION_OCCURED = "an exception occurred! ";

	public static final String SLCLRSCREENNAME = "clr screen";

	public static final String RETAILERHOTDEALS = "RetailerHotDeals";

	/**
	 * PAGINATION declared as String constant variable.
	 */
	public static final String PAGINATION = "pagination";

	/**
	 * LOGINUSER declared as String constant variable.
	 */
	public static final String LOGINUSER = "loginuser";
	/**
	 * FALSE declared as String constant variable.
	 */
	public static final String FALSE = "false";
	/**
	 * ZERO declared as String constant variable.
	 */
	public static final String ZERO = "0";
	/**
	 * ZERO declared as String constant variable.
	 */
	public static final String MESSAGE = "message";

	/**
	 * Private constructor to avoid instantiating this object from other
	 * classes.
	 */

	public static final String REPORT_URL = "Reports URL";

	public static final String REPORT_PATH = "Report Path";

	public static final String REPORTSERVER_PASSWORD = "Report Server Password";

	public static final String REPORTSERVER_USERNAME = "Report Server User Name";

	public static final String CONSFNDPRODSEARCHPAGINATION = "Consumer - Find Product Smart Search";

	public static final String CONSFNDLOCSEARCHPAGINATION = "Consumer - Find Product Smart Search";

	public static final String CONSFNDDEALSSEARCHPAGINATION = "Consumer - Find Deal Search";

	public static final String CONSFNDPRODREVIEWPAGINATION = "Consumer - Find Deal Search";

	/**
	 * find retailer list pagination
	 */
	public static final String FINDAPIPATNERNAME = "ScanSee";

	public static final String GIVEAWAYPAGEURL = "2300.htm?";

	public static final String QUANTITY = "Quantity";

	public static final String GUESTUSER = "GuestUser";

	public static final String FINDMODULE = "Find";

	public static final String GALLERYMODULE = "My Gallery";

	public static final String COUPONMODULE = "My Coupons";

	public static final String HOTDEALMODULE = "Hot Deals";

	public static final String ANYTHINGPAGEDETAILDIV = "<div class=\"rtPnl floatL\"><div class=\"viewAreaiPhn no-scroll\"><iframe class=\"gnrl\" src=\"/ScanSeeWeb/images/nodata.png\" width=\"320px\" height=\"416px\"></iframe></div></div>";

	public static final String EXTERNALLINK = "EL=";

	public static final String CONSADDPRODTOSHOPPLIST = "Added to Shopping List";

	public static final String CONSADDPRODTOFAVORITES = "Added to Favorites";

	public static final String CONSADDPRODTOWISHLIST = "Added to Wish list";

	public static final String CONSFINDONLINESTORE = "FindOnlineStores";

	public static final String IMAGEICON = "/ScanSeeWeb/images/consumer/imgIcon.png";

	public static final String SALEFLAG = "SaleFlag";
	public static final String SPECIALFLAG = "SpecialFlag";
	public static final String HOTDEALFLAG = "HotDealFlag";
	public static final String COUPONFLAG = "CouponFlag";

	public static final String HOMEMODULE = "Home";

	public static final String EXPIREDDATEBEFORE = "expireddatebefore";
	public static final String STARTDATEAFTEREXPIREDATE = "startdateafterexpiredate";
	public static final String PERCENTAGEFLAG = "PercentageFlag";
	public static final String PERCENTAGEDISCOUNTFLAG = "Discount percentage should be greater than or equal to 50%";
	public static final String ADDHOTDEALSCREENERRORMSG = "Error While Adding HotDeal";
	public static final String UPDATEHOTDEALSCREENERRORMSG = "Error While Updating HotDeal";
	/**
	 * /** HTTP declared as String constant variable .
	 */
	public static final String HTTP = "http:";

	public static final String MAINMENUID = "mainMenuID";
	public static final String SCANTYPEID = "scanTypeID";

	public static final String CONSPRODUCTEXISTS = "exists";
	public static final String CONSPRODUCTADDED = "Added to List";
	public static final String CONSWLPRODUCTADDED = "Added to Wish List";

	public static final String CONSCOUPONPINSHARE = "shopper/consmygallery.htm";
	public static final String CONSDEALPINSHARE = "shopper/consdisplayhotdeals.htm";

	/**
	 * PAGINATION declared as String constant variable.
	 */
	public static final String PAGINATIONLOC = "paginationloc";

	public static final String CONFIGURATIONTYPESHAREURLFORWEB = "WebShareURL";

	public static final String PRODUCTIONWEBURL = "https://www.scansee.net";

	public static final String PRODEMAILSHARETYPETEXT = "Email";
	public static final String PRODFACEBOOKSHARETYPETEXT = "Facebook";
	public static final String PRODTWITTERSHARETYPETEXT = "Twitter";
	public static final String PRODPINTERESTSHARETYPETEXT = "Text";

	/**
	 * FINDSERVICEBEAN declared as string constant variable.
	 */
	public static final String FINDSERVICEBEAN = "findService";

	/**
	 * SEARCKKEYTEXT declared as string constant variable.
	 */
	public static final String SEARCHTYPETEXT = "searchType";

	public static final String SHOWHEADERSEARCH = "showHeaderSearch";

	public static final String SHOWTEXT = "show";

	public static final String CONSUMERREQOBJ = "consumerReqObj";

	public static final String SEARCHRESULTS = "searchresults";

	public static final String COMMONSERVICE = "commonService";

	public static final String USEREMAIID = "userEmailId";

	public static final String SEARCHFORM = "searchForm";

	public static final boolean TRUE = true;
	public static final String TRUETEXT = "true";

	public static final String TOTALRESULTS = "totalresults";
	public static final String SEARCHKEY = "searchKey";

	public static final String ZIPCODE = "zipcode";

	public static final String CONSPRODDETALS = "consproddetails";

	public static final String SPCHOTDEAL = "SpclHotDeal";

	public static final String PRODUCTRATINGREVIEW = "productRatingReview";

	/**
	 * SHOPPINGLISTBEAN declared as string variable.
	 */
	public static final String SHOPPINGLISTBEAN = "findService";

	public static final String SHOPPINGLISTMODULE = "Shopping List";
	public static final String EXPIREDDATE_GRTN_STARTDATE = "expiredateafter";

	public static final String WISHLISTMODULE = "Wish List";

	public static final String DELETEPRODUCTTEXT = "Deleted";

	/**
	 * for REJECTED Error Message.
	 */
	public static final String REJECTED = "Rejected";
	/**
	 * for UPDATE_LAT_LONG Error Message.
	 */
	public static final String UPDATE_LAT_LONG = "We couldn't locate your address. Please update latitude and Longitude for saving.";

	public static final String SHOPPINGLISTCHECKOUTTEXT = "All your checked items are moved to history.";
	/**
	 * Declared For adding product to today shopping list . if product already
	 * exists we are sending this message.
	 */
	public static final String FAVADDPRODTLEXISTS = "Some item(s) were not added to Favorites  because they were already in Favorites";
	/**
	 * LATITUDE declared as String constant variable .
	 */
	public static final String LATITUDE = "latitude";
	/**
	 * LONGITUDE declared as String constant variable .
	 */
	public static final String LONGITUDE = "longitude";

	public static final String ADDEDTOLISTFAVORITES = "Added to List and Favorites";

	public static final String WLADDPRODTLEXISTS = "Some item(s) were not added to List  because they were already in Wish List";

	public static final Integer CROPIMAGEWIDTH = 800;

	public static final Integer CROPIMAGEHEIGHT = 600;
	/**
	 * LOCATIONLOGO declared as String constant variable .
	 */
	public static final String LOCATIONLOGO = "locationlogo";
	
	/**
	 * IMAGES_SLASH_RETAILER declared as String constant variable .
	 */
	public static final String IMAGES_SLASH_RETAILER = "/Images/retailer/";
	
	/**
	 * SLASH_LOCATIONLOGO_SLASH declared as String constant variable .
	 */
	public static final String SLASH_LOCATIONLOGO_SLASH = "/locationlogo/";
	
	/**
	 * UPLOAD GRID IMAGE Image space holder
	 */
	public static final String UPLOADGRIDIMAGE = "/ScanSeeWeb/images/dfltImg.png";
	/**
	 * VALIDSTARTDATE declared as String constant variable.
	 */
	public static final String VALIDSTARTDATE = "validStartDate";
	/**
	 * VALIDENDDATE declared as String constant variable.
	 */
	public static final String VALIDENDDATE = "validEndDate";
	/**
	 * VALIDDATE declared as String constant variable.
	 */
	public static final String VALIDDATE = "validDate";
	/**
	 * VALIDEDATE declared as String constant variable.
	 */
	public static final String VALIDEDATE = "validEDate";
	/**
	 * DATENOGSTARTCURRENT declared as String constant variable.
	 */
	public static final String DATENOGSTARTCURRENT = "startnogcurrent";
	/**
	 * DATENOGENDCURRENT declared as String constant variable.
	 */
	public static final String DATENOGENDCURRENT = "endnogcurrent";
	/**
	 * DATENOGAFTER declared as String constant variable.
	 */
	public static final String DATENOGAFTER = "nogafter";
	/**
	 * INVALIDURL declared as String constant variable .
	 */
	public static final String INVALIDEVENTURL = "invalideventurl";
	/**
	 * DUPLICATEDEPARTMENT declared as String constant variable .
	 */
	public static final String DUPLICATE_DEPARTMENT = "Department name exist";
	/**
	 * DUPLICATEDEPARTMENT declared as String constant variable .
	 */
	public static final String NOFUNDRAISINGEVENTFOUND = "No Fundraiser Event Found";
	public static final String HUBCITI_LOGO_FOR_MAILSENDING = "ScanSeeWeb/images/HubCitiMailLogo.png";
	public static final String HUBCITI_SUBJECT_MSG_FOR_SUCCESS_REGISTRATION = "HubCiti : Registration Successful";
	/**
	 * 
	 */
	private ApplicationConstants() {

	}
}

package common.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;

import net.authorize.Environment;
import net.authorize.Merchant;
import net.authorize.TransactionType;
import net.authorize.aim.Result;
import net.authorize.aim.Transaction;
import net.authorize.data.Customer;
import net.authorize.data.EmailReceipt;
import net.authorize.data.Order;
import net.authorize.data.arb.PaymentSchedule;
import net.authorize.data.arb.Subscription;
import net.authorize.data.arb.SubscriptionUnitType;
import net.authorize.data.creditcard.CreditCard;
import net.authorize.data.xml.Address;
import net.authorize.data.xml.CustomerType;
import net.authorize.data.xml.Payment;
import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import au.com.bytecode.opencsv.CSVReader;
import common.constatns.ApplicationConstants;
import common.email.EmailComponent;
import common.exception.ScanSeeServiceException;
import common.pojo.GAddress;
import common.pojo.ManageProducts;
import common.pojo.PlanInfo;
import common.pojo.ProductJson;
import common.pojo.RetailProduct;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerInfo;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.RetailerLocationProduct;
import common.pojo.RetailerLocationProductJson;
import common.pojo.RetailerRegistration;
import common.pojo.SupplierRegistrationInfo;
import common.pojo.Users;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.LoyaltyDetail;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.RebateDetail;
import common.tags.Pagination;

/**
 * This class contains all the Helper or util methods used throughout the
 * application.
 * 
 * @author manjunatha_gh
 */
public class Utility {

	// Test Environment Keys
	// private static final String AUTHORIZE_API_LOGIN_ID = "5m64U8dfDj";
	// private static final String TRANSACTION_KEY = "5zcSVs62U8mJ75jK";

	private static final String MONTHLY = "monthly";
	// Production Environment Keys
	private static final String AUTHORIZE_API_LOGIN_ID = "92bcTATL3Z";
	private static final String TRANSACTION_KEY = "9C594a8J2xF7GKbP";
	

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(Utility.class);

	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	public static final boolean defaultEmptyOK = true;

	/** decimal point character differs by language and culture */
	public static final String decimalPointDelimiter = ".";

	/**
	 * The constant for HTML Paragraph opening tag.
	 */

	private static final char[] HTMLPARAGRAPHOPEN = "<p>".toCharArray();
	/**
	 * The constant for HTML Paragraph Closing tag.
	 */

	private static final char[] HTMLPARAGRAPHCLOSE = "</p>".toCharArray();
	/**
	 * The constant for HTML Bold Opening tag.
	 */
	private static final char[] HTMLBOLDOPEN = "<b>".toCharArray();

	/**
	 * The constant for HTML Bold Closing tag.
	 */
	private static final char[] HTMLBOLDCLOSE = "</b>".toCharArray();
	/**
	 * The constant for HTML Break tag.
	 */
	private static final char[] HTMLBREAKCLOSE = "<br/>".toCharArray();

	/**
	 * The constant for HTML Break tag.
	 */
	private static final char[] HTMLBREAK = "<br>".toCharArray();

	/**
	 * The constant for HTML List Open tag.
	 */
	private static final char[] HTMLLISTITEMOPEN = "<li>".toCharArray();
	/**
	 * The constant for HTML List closing tag.
	 */
	private static final char[] HTMLLISTITEMCLOSE = "</li>".toCharArray();
	/**
	 * The constant for HTML Unordered List Opening tag.
	 */
	private static final char[] HTMUNORDEREDLISTOPEN = "<ul>".toCharArray();

	/**
	 * The constant for HTML Unordered List Closing tag.
	 */
	private static final char[] HTMLUNORDEREDLISTCLOSE = "</ul>".toCharArray();

	/**
	 * variable for checking Debugging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	public static final String autoGeneratedPswd = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static final String autoGeneratedUserName = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	private static final int IMG_WIDTH = 70;
	private static final int IMG_HEIGHT = 70;
	/**
	 * variable for URL.
	 */
	private static final String URL = "http://maps.google.com/maps/geo?output=json";
	/**
	 * variable for DEFAULT_KEY.
	 */
	private static final String DEFAULT_KEY = "YOUR_GOOGLE_API_KEY";
	
	/**
	 * The constant for &amp.
	 */
	private static final char[] AMP = "&amp;".toCharArray();
	
	/**
	 * The constant for &quot.
	 */
	private static final char[] QUOT = "&quot;".toCharArray();
		/**
	 * The constant for &apos.
	 */
	private static final char[] APOS = "&apos;".toCharArray();
	
		/**
	 * The constant for &lt.
	 */
	private static final char[] LT = "&lt;".toCharArray();

	/**
	 * The constant for &gt.
	 */
	private static final char[] GT = "&gt;".toCharArray();

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 * @throws java.text.ParseException
	 *             The exception of type java.text.ParseException
	 */

	// public static String [] months = ["Janua"]
	public static java.sql.Timestamp getFormattedDate()
			throws java.text.ParseException {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		java.sql.Timestamp sqltDate = null;
		/*
		 * formatting the current date.
		 */
		// final DateFormat formater = new SimpleDateFormat("yyyy-dd-MM");
		final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
		java.util.Date parsedUtilDate;
		parsedUtilDate = formater.parse(formater.format(date));
		sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return sqltDate;
	}

	public static String writeFileData(CommonsMultipartFile file,
			String destinationPath) throws ScanSeeServiceException {
		final String methoName = "writeFileData";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		InputStream inputStream = null;
		OutputStream outputStream = null;
		CommonsMultipartFile commonsMultipartFile = null;
		commonsMultipartFile = file;
		// byte[] buffer=file.getFileData();
		byte[] buffer = new byte[32];

		if (commonsMultipartFile.getSize() > 0) {
			// destinationPath=file.getFilePath();
			String outputPath = destinationPath;
			try {
				LOG.info("Content Type : " + file.getContentType());
				if (file.getContentType().contains("image/")) {
					String extension = FilenameUtils
							.getExtension(destinationPath);
					if (!isEmptyOrNullString(extension)) {
						if (!extension.equals("png")) {
							outputPath = FilenameUtils
									.removeExtension(destinationPath);

							if (!isEmptyOrNullString(outputPath)) {
								outputPath = outputPath + ".png";
							}
						}
					}
				}
				LOG.info("Output path in writeFileData method :" + outputPath);
				outputStream = new FileOutputStream(outputPath);
				inputStream = commonsMultipartFile.getInputStream();
				while (inputStream.read(buffer) != -1) {
					outputStream.write(buffer);
				}

				LOG.info("File has been successfully written to " + outputPath);
			} catch (FileNotFoundException exception) {
				LOG.error("Exception Occurred in writeFileData {}",
						exception.getMessage());
				throw new ScanSeeServiceException(exception.getMessage(),
						exception);
			}

			catch (IOException exception) {
				LOG.error("Exception Occurred in writeFileData {}",
						exception.getMessage());
				throw new ScanSeeServiceException(exception.getMessage(),
						exception);
			}

			finally {
				if (outputStream != null) {
					try {
						outputStream.close();
					} catch (IOException exception) {
						LOG.error("Exception Occurred in writeFileData {}",
								exception.getMessage());
						throw new ScanSeeServiceException(
								exception.getMessage(), exception);
					}
				}
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException exception) {
						LOG.error("Exception Occurred in writeFileData {}",
								exception.getMessage());
						throw new ScanSeeServiceException(
								exception.getMessage(), exception);
					}

				}
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return "success";

	}

	public static String writeImage(BufferedImage file, String destinationPath)
			throws ScanSeeServiceException {
		final String methoName = "writeImage";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		InputStream inputStream = null;
		OutputStream outputStream = null;

		// byte[] buffer=file.getFileData();
		// byte[] buffer = new byte[32];

		if (null != file) {
			// destinationPath=file.getFilePath();
			String outputPath = destinationPath;
			try {
				// LOG.info("Content Type : " + file.getContentType());
				// if (file.getContentType().contains("image/"))
				// {
				String extension = FilenameUtils.getExtension(destinationPath);
				if (!isEmptyOrNullString(extension)) {
					if (!extension.equals("png")) {
						outputPath = FilenameUtils
								.removeExtension(destinationPath);

						if (!isEmptyOrNullString(outputPath)) {
							outputPath = outputPath + ".png";
						}
					}
					// }
				}
				LOG.info("Output path in writeFileData method :" + outputPath);
				outputStream = new FileOutputStream(outputPath);
				// inputStream = file.getInputStream();
				/*
				 * while (inputStream.read(buffer) != -1) {
				 * outputStream.write(buffer); }
				 */
				ImageIO.write(file, "PNG", outputStream);
			} catch (FileNotFoundException exception) {
				LOG.error("Exception Occurred in writeFileData {}",
						exception.getMessage());
				throw new ScanSeeServiceException(exception.getMessage(),
						exception);
			}

			catch (IOException exception) {
				LOG.error("Exception Occurred in writeFileData {}",
						exception.getMessage());
				throw new ScanSeeServiceException(exception.getMessage(),
						exception);
			}

			finally {
				if (outputStream != null) {
					try {
						outputStream.close();
					} catch (IOException exception) {
						LOG.error("Exception Occurred in writeFileData {}",
								exception.getMessage());
						throw new ScanSeeServiceException(
								exception.getMessage(), exception);
					}
				}
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException exception) {
						LOG.error("Exception Occurred in writeFileData {}",
								exception.getMessage());
						throw new ScanSeeServiceException(
								exception.getMessage(), exception);
					}

				}
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return "success";

	}

	public static String writeCroppedFileData(CommonsMultipartFile file,
			String sourcePath, String destinationPath, int xPixel, int yPixel,
			int wPixel, int hPixel) throws ScanSeeServiceException {
		final String methoName = "writeCroppedFileData";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		InputStream inputStream = null;
		OutputStream outputStream = null;
		// CommonsMultipartFile commonsMultipartFile = null;
		// commonsMultipartFile = file;
		// byte[] buffer=file.getFileData();
		byte[] buffer = new byte[32];

		if (file.getSize() > 0) {
			String fileName = file.getFileItem().getName();
			try {
				File sourceimage = new File(sourcePath);

				BufferedImage outImage = ImageIO.read(sourceimage);
				BufferedImage cropped = outImage.getSubimage(xPixel, yPixel,
						wPixel, hPixel);

				ByteArrayOutputStream os = new ByteArrayOutputStream();
				// ImageIO.write(cropped, "png", os);

				int type = cropped.getType() == 0 ? BufferedImage.TYPE_INT_ARGB
						: cropped.getType();

				BufferedImage resizeImagePng = resizeImage(cropped, type,
						wPixel, hPixel);

				ImageIO.write(resizeImagePng, "png", os);

				inputStream = new ByteArrayInputStream(os.toByteArray());
				String outputPath = destinationPath;
				outputPath = FilenameUtils.removeExtension(destinationPath);

				if (!isEmptyOrNullString(outputPath)) {
					outputPath = outputPath + ".png";
				}

				LOG.info("OutputPath : " + outputPath);

				outputStream = new FileOutputStream(outputPath);
				// inputStream = commonsMultipartFile.getInputStream();
				while (inputStream.read(buffer) != -1) {
					outputStream.write(buffer);
				}
			} catch (FileNotFoundException exception) {
				LOG.error("Exception Occurred in writeFileData {}",
						exception.getMessage());
				throw new ScanSeeServiceException(exception.getMessage(),
						exception);
			}

			catch (IOException exception) {
				LOG.error("Exception Occurred in writeFileData {}",
						exception.getMessage());
				throw new ScanSeeServiceException(exception.getMessage(),
						exception);
			}

			finally {
				if (outputStream != null) {
					try {
						outputStream.close();
					} catch (IOException exception) {
						LOG.error("Exception Occurred in writeFileData {}",
								exception.getMessage());
						throw new ScanSeeServiceException(
								exception.getMessage(), exception);
					}
				}
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException exception) {
						LOG.error("Exception Occurred in writeFileData {}",
								exception.getMessage());
						throw new ScanSeeServiceException(
								exception.getMessage(), exception);
					}

				}
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return "success";

	}

	private static BufferedImage resizeImage(BufferedImage originalImage,
			int type, int wPixel, int hPixel) {
		BufferedImage resizedImage = new BufferedImage(wPixel, hPixel, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, wPixel, hPixel, null);
		g.dispose();

		return resizedImage;
	}

	private static String changeExtension(String originalName,
			String newExtension) {
		int lastDot = originalName.lastIndexOf(".");
		if (lastDot != -1) {
			return originalName.substring(0, lastDot) + newExtension;
		} else {
			return originalName + newExtension;
		}
	}// end changeExtension

	/**
	 * The method to convert current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 * @throws java.text.ParseException
	 *             The exception of type java.text.ParseException
	 */

	public static String getCurrentDate() {
		String currentDate = null;
		try {
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = new Date();
			currentDate = df.format(date1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentDate;
	}

	/*
	 * public static void main(String[] args) { // getCurrentDate();
	 * LOG.info(formatDecimalValue("10.1099")); }
	 */

	/**
	 * This method is used for pagination.
	 * 
	 * @param totalSize
	 * @param currentPage
	 * @param url
	 * @return
	 */
	public static Pagination getPagination(long totalSize, int currentPage,
			String url) {
		Pagination objPage = new Pagination();
		objPage.setTotalSize(totalSize);
		objPage.setCurrentPage(currentPage);
		/*
		 * Application is displays pagination like �Page 1 of 1� after creating
		 * 20th record in AnyThing Pages if (totalSize==20){
		 * objPage.setPageRange(21); } else { objPage.setPageRange(20); } End
		 */
		objPage.setPageRange(20);
		objPage.setUrl(url);
		return objPage;
	}

	/**
	 * This method is used for pagination.
	 * 
	 * @param totalSize
	 * @param currentPage
	 * @param url
	 * @param pageSize
	 * @return
	 */
	public static Pagination getPagination(long totalSize, int currentPage,
			String url, int pageSize) {
		Pagination objPage = new Pagination();
		objPage.setTotalSize(totalSize);
		objPage.setCurrentPage(currentPage);
		objPage.setPageRange(pageSize);
		objPage.setUrl(url);
		return objPage;
	}

	/**
	 * This method is used for pagination for shopper.
	 * 
	 * @param totalSize
	 * @param currentPage
	 * @param url
	 * @return
	 */
	public static Pagination getPaginationForShopper(long totalSize,
			int currentPage, String url) {
		Pagination objPage = new Pagination();
		objPage.setTotalSize(totalSize);
		objPage.setCurrentPage(currentPage);
		objPage.setPageRange(10);
		objPage.setUrl(url);
		return objPage;
	}

	/**
	 * getFormattedDate method will convert date format to yyyy-MM-dd.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */

	public static String getFormattedDate(String enteredDate)
			throws java.text.ParseException {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		DateFormat oldFormatter = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Date convertedDate = (Date) oldFormatter.parse(enteredDate);
		String cDate = formatter.format(convertedDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return cDate;
	}

	/**
	 * This method is used to get the calendar formatted date.
	 * 
	 * @param enteredDate
	 * @return
	 * @throws java.text.ParseException
	 */
	public static String getCalenderFormattedDate(String enteredDate)
			throws java.text.ParseException {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (null != enteredDate && !"".equals(enteredDate)) {
			DateFormat oldFormatter = new SimpleDateFormat("MM-dd-yyyy");
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			String cDate = formatter.format(convertedDate);
			LOG.info(ApplicationConstants.METHODEND + methodName);
			return cDate;

		} else {

			return null;
		}

	}

	/**
	 * getFormattedDate method will convert date format to yyyy-MM-dd.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */

	public static String getFormattedDateTime(String enteredDate)
			throws java.text.ParseException {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String cDate = null;
		DateFormat oldFormatter = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (null != enteredDate && !enteredDate.equals("")) {
			Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			cDate = formatter.format(convertedDate);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return cDate;
	}

	/**
	 * getFormattedDate method will convert date format to MM/dd/yyyy.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */

	public static String formattedDate(String enteredDate)
			throws java.text.ParseException {
		final String methodName = "formattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		DateFormat oldFormatter = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		Date convertedDate = (Date) oldFormatter.parse(enteredDate);
		String cDate = formatter.format(convertedDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return cDate;
	}

	/**
	 * THis method is used to format date with time.
	 * 
	 * @param enteredDate
	 * @return
	 * @throws java.text.ParseException
	 */
	public static String formattedDateWithTime(String enteredDate)
			throws java.text.ParseException {
		final String methodName = "formattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		DateFormat oldFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		Date convertedDate = (Date) oldFormatter.parse(enteredDate);
		String cDate = formatter.format(convertedDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return cDate;
	}

	/**
	 * getUsDateFormat method will convert date format to MM-dd-yyyy.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */
	public static String getUsDateFormat(String enteredDate)
			throws java.text.ParseException {
		final String methodName = "getUsDateFormat";
		// LOG.info(ApplicationConstants.METHODSTART + methodName);

		DateFormat oldFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		DateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

		Date convertedDate = (Date) oldFormatter.parse(enteredDate);
		String cDate = formatter.format(convertedDate);
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return cDate;
	}

	/**
	 * This method is used to get us date format without time.
	 * 
	 * @param enteredDate
	 * @return
	 * @throws java.text.ParseException
	 */
	public static String getUsDateFormatwithoutTime(String enteredDate)
			throws java.text.ParseException {
		final String methodName = "getUsDateFormat";
		// LOG.info(ApplicationConstants.METHODSTART + methodName);

		DateFormat oldFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

		Date convertedDate = (Date) oldFormatter.parse(enteredDate);
		String cDate = formatter.format(convertedDate);
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return cDate;
	}

	/**
	 * This method is used to verify whether enter string is a number or not.
	 * 
	 * @param price
	 * @return
	 */
	public static boolean isNumber(String price) {
		try {
			if (price != null) {
				if (!price.equals("")) {
					BigDecimal bigDec = new BigDecimal(price);
				}
			}
		} catch (NumberFormatException ex) {
			return false;
		}
		return true;
	}

	/**
	 * This method is used to get product details.
	 * 
	 * @param productuploadFilePath
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ScanSeeServiceException
	 */
	public static ArrayList<ManageProducts> getProductDetails(
			CommonsMultipartFile productuploadFilePath)
			throws FileNotFoundException, IOException, ScanSeeServiceException {
		ManageProducts manageProducts = null;
		ArrayList<ManageProducts> list = new ArrayList<ManageProducts>();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					productuploadFilePath.getInputStream()));
			StringTokenizer tokenizer = null;
			StringTokenizer comatokenizer = null;
			String fileInfo = null;
			String info = "";
			String semiColunSeperator = "";
			// read semicolun separated file line by line
			while ((fileInfo = br.readLine()) != null) {
				info += fileInfo;

			}

			LOG.info("------------------------------");
			LOG.info("fileInfo=======" + info);
			tokenizer = new StringTokenizer(info, ";");
			while (tokenizer.hasMoreTokens()) {

				semiColunSeperator = tokenizer.nextToken();
				LOG.info("semiColunSeperator=======" + semiColunSeperator);
				comatokenizer = new StringTokenizer(semiColunSeperator, ",");
				while (comatokenizer.hasMoreTokens()) {
					manageProducts = new ManageProducts();
					manageProducts.setProductName(comatokenizer.nextToken());
					manageProducts.setScanCode(comatokenizer.nextToken());
					manageProducts.setScanType(comatokenizer.nextToken());
					manageProducts.setProdExpirationDate(comatokenizer
							.nextToken());
					manageProducts.setImagePath(comatokenizer.nextToken());
					manageProducts.setCategory(comatokenizer.nextToken());
					manageProducts.setShortDescription(comatokenizer
							.nextToken());
					manageProducts
							.setLongDescription(comatokenizer.nextToken());
					manageProducts.setModelNumber(comatokenizer.nextToken());
					manageProducts.setSuggestedRetailPrice(Double
							.parseDouble(comatokenizer.nextToken()));
					manageProducts.setWeight(Double.parseDouble(comatokenizer
							.nextToken()));
					manageProducts.setWeightUnits(comatokenizer.nextToken());
					manageProducts.setManufProuductURL(comatokenizer
							.nextToken());

					String audioPath = comatokenizer.nextToken();

					if (ApplicationConstants.QUOTE.equals(audioPath)) {
						manageProducts.setAudioMediaPath(null);
					} else {
						manageProducts.setAudioMediaPath(audioPath);
					}

					String videoPath = comatokenizer.nextToken();
					if (ApplicationConstants.QUOTE.equals(videoPath)) {
						manageProducts.setVideoMediaPath(null);
					} else {
						manageProducts.setVideoMediaPath(videoPath);
					}

					String imagePath = comatokenizer.nextToken();
					if (ApplicationConstants.QUOTE.equals(imagePath)) {
						manageProducts.setProductImagePath(null);
					} else {
						manageProducts.setProductImagePath(imagePath);
					}

					list.add(manageProducts);
				}

			}

		} catch (Exception exception) {
			throw new ScanSeeServiceException(exception);
		}
		return list;

	}

	/**
	 * This method is used to upload attributes
	 * 
	 * @param productuploadFilePath
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ScanSeeServiceException
	 */
	public static ArrayList<ManageProducts> uploadAttribute(
			CommonsMultipartFile productuploadFilePath)
			throws FileNotFoundException, IOException, ScanSeeServiceException {
		ManageProducts manageProducts = null;
		ArrayList<ManageProducts> list = new ArrayList<ManageProducts>();

		try {

			// File file=new File(productInfoFileName) ;

			// create BufferedReader to read csv file
			BufferedReader br = new BufferedReader(new InputStreamReader(
					productuploadFilePath.getInputStream()));
			StringTokenizer tokenizer = null;
			StringTokenizer comatokenizer = null;
			String fileInfo = null;
			String info = "";
			String semiColunSeperator = "";
			// read semicolun separated file line by line
			while ((fileInfo = br.readLine()) != null) {
				info += fileInfo;

			}

			LOG.info("------------------------------");
			LOG.info("fileInfo=======" + info);
			tokenizer = new StringTokenizer(info, ";");
			while (tokenizer.hasMoreTokens()) {
				semiColunSeperator = tokenizer.nextToken();
				comatokenizer = new StringTokenizer(semiColunSeperator, ",");
				while (comatokenizer.hasMoreTokens()) {
					manageProducts = new ManageProducts();
					manageProducts.setScanCode(comatokenizer.nextToken());
					manageProducts.setAttributeName(comatokenizer.nextToken());
					manageProducts.setAttributeValue(comatokenizer.nextToken());
					list.add(manageProducts);
				}

			}

		} catch (Exception exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return list;

	}

	/**
	 * This method is used to validate phone number.
	 * 
	 * @param strPhoneNum
	 * @return
	 */
	public static boolean validatePhoneNum(String strPhoneNum) {
		final String methodName = "validatePhoneNum";
		// String expression ="(\\d{3})(\\[-])(\\d{4})$";
		boolean returnVal = true;

		// String inputStr = "(124) 123-4567";

		if (strPhoneNum != null) {
			if (!strPhoneNum.equals("")) {

				Pattern pattern = Pattern
						.compile(ApplicationConstants.PHONENUM_PATTERN);
				Matcher matcher = pattern.matcher(strPhoneNum);
				returnVal = matcher.matches();
			}
		}

		return returnVal;
	}

	/**
	 * This method is used for validating password.
	 * 
	 * @param strPassword
	 * @return
	 */
	public static boolean validatePassword(String strPassword) {

		boolean returnVal = true;
		if (strPassword != null) {
			if (!strPassword.equals("")) {

				Pattern pattern = Pattern.compile(ApplicationConstants.DIGIT);
				Matcher matcher = pattern.matcher(strPassword);
				returnVal = matcher.matches();
			}
		}

		return returnVal;
	}

	/**
	 * THis method is used to compare date.
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public static String compareDate(String fromDate, String toDate)
			throws ScanSeeServiceException {
		// DateFormat formatter;
		Date startDate;
		Date endDate;
		int result = 0;
		String comResult = null;
		DateFormat oldFormatter = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if (!isEmptyOrNullString(fromDate) && !isEmptyOrNullString(toDate)) {
				startDate = (Date) oldFormatter.parse(fromDate);
				endDate = (Date) oldFormatter.parse(toDate);
				result = endDate.compareTo(startDate);
				if (result < 0)
					comResult = ApplicationConstants.DATEAFTER;
			}
		} catch (ParseException e) {
			throw new ScanSeeServiceException(e.getMessage());
		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage());
		}
		return comResult;
	}

	/**
	 * This method is used for comparing current date.
	 * 
	 * @param fromDate
	 * @param currentDate
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public static String compareCurrentDate(String fromDate, Date currentDate)
			throws ScanSeeServiceException {
		Date startDate;
		Date endDate;
		int result = 0;
		String comResult = null;
		// DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			DateFormat oldFormatter = new SimpleDateFormat("MM/dd/yyyy");
			// DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			startDate = (Date) oldFormatter.parse(fromDate);
			// endDate = (Date) oldFormatter.parse(getFormattedCurrentDate());
			result = startDate.compareTo(getFormattedCurrentDate());
			if (result < 0)
				comResult = ApplicationConstants.DATEAFTER;
		} catch (ParseException e) {
			throw new ScanSeeServiceException(e.getMessage());
		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage());
		}
		return comResult;
	}

	/**
	 * This method is used for formatting user date.
	 * 
	 * @param userDate
	 * @return
	 * @throws java.text.ParseException
	 * @throws ScanSeeServiceException
	 */
	public static Date getFormattedUserDate(String userDate)
			throws java.text.ParseException, ScanSeeServiceException {

		final String methodName = "getFormattedUserDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName + userDate);

		java.util.Date utilDate = null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		utilDate = dateFormat.parse(userDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new java.sql.Timestamp(utilDate.getTime());

	}

	/*
	 * public static List<ManageProducts> jsonToObject(String jsonStr){ final
	 * String methodName = "getDeals"; ProductJson prodJson = new ProductJson();
	 * try { ObjectMapper om = new ObjectMapper(); prodJson =
	 * om.readValue(jsonStr.getBytes(), ProductJson.class);
	 * LOG.info("Size "+prodJson.getProductData().size()); } catch
	 * (JsonMappingException e) { e.printStackTrace(); } catch (Exception e) {
	 * e.printStackTrace(); } return prodJson.getProductData(); }
	 */

	/**
	 * THis method is used for fetching location list.
	 * 
	 * @param batchFile
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static ArrayList<RetailerLocation> getLocationList(
			CommonsMultipartFile batchFile) throws FileNotFoundException,
			IOException {
		RetailerLocation retailerLocation = null;
		ArrayList<RetailerLocation> list = new ArrayList<RetailerLocation>();

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					batchFile.getInputStream()));
			StringTokenizer tokenizer = null;
			StringTokenizer comatokenizer = null;
			String fileInfo = null;
			String info = "";
			String semiColunSeperator = "";
			// read semicolun separated file line by line
			while ((fileInfo = br.readLine()) != null) {
				info += fileInfo;
			}
			LOG.info("------------------------------");
			LOG.info("fileInfo=======" + info);
			tokenizer = new StringTokenizer(info, ";");
			while (tokenizer.hasMoreTokens()) {
				semiColunSeperator = tokenizer.nextToken();
				LOG.info("semiColunSeperator=======" + semiColunSeperator);
				comatokenizer = new StringTokenizer(semiColunSeperator, ",");
				while (comatokenizer.hasMoreTokens()) {
					retailerLocation = new RetailerLocation();
					retailerLocation.setStoreIdentification(comatokenizer
							.nextToken());
					retailerLocation.setAddress1(comatokenizer.nextToken());
					retailerLocation.setCity(comatokenizer.nextToken());
					retailerLocation.setState(comatokenizer.nextToken());
					retailerLocation.setPostalCode(comatokenizer.nextToken());
					retailerLocation.setPhonenumber(comatokenizer.nextToken());
					list.add(retailerLocation);
				}

			}

		} catch (Exception e) {
			LOG.info("Exception while reading csv file: " + e);
			LOG.error(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * Validate Email Id with regular expression
	 * 
	 * @param strEmailId
	 *            strEmailId for validation
	 * @return true valid strEmailId, false invalid strEmailId
	 */
	public static boolean validateEmailId(final String strEmailId) {

		Pattern pattern = null;
		Matcher matcher;
		final String EMAIL_PATTERN = ApplicationConstants.EMAIL_PATTERN;

		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(strEmailId);
		return matcher.matches();
	}

	/**
	 * This method is used for conerting json to plan object.
	 * 
	 * @param jsonStr
	 * @return
	 */
	public static List<PlanInfo> jsonToPlanObject(String jsonStr) {

		final String methodName = "getDeals";
		ProductJson prodJson = new ProductJson();
		try {
			ObjectMapper om = new ObjectMapper();
			prodJson = om.readValue(jsonStr.getBytes(), ProductJson.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prodJson.getPlanData();

	}

	/**
	 * This method is used forfetching retailer product details.
	 * 
	 * @param productuploadFilePath
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public static ArrayList<RetailProduct> getRetProdDetails(
			CommonsMultipartFile productuploadFilePath)
			throws ScanSeeServiceException {

		RetailProduct retailerProd = null;
		ArrayList<RetailProduct> retailerLocationsProd = new ArrayList<RetailProduct>();

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(
					productuploadFilePath.getInputStream()));
			StringTokenizer tokenizer = null;
			StringTokenizer comatokenizer = null;
			String fileInfo = null;
			String info = "";
			String semiColunSeperator = "";
			// read semicolun separated file line by line
			while ((fileInfo = br.readLine()) != null) {
				info += fileInfo;

			}

			LOG.info("------------------------------");
			LOG.info("fileInfo=======" + info);
			tokenizer = new StringTokenizer(info, ";");
			while (tokenizer.hasMoreTokens()) {
				semiColunSeperator = tokenizer.nextToken();
				comatokenizer = new StringTokenizer(semiColunSeperator, ",");
				while (comatokenizer.hasMoreTokens()) {
					retailerProd = new RetailProduct();
					retailerProd.setStoreIdentificationID(comatokenizer
							.nextToken());
					retailerProd.setScanCode(comatokenizer.nextToken());
					retailerProd.setLongDescription(comatokenizer.nextToken());
					retailerProd.setPrice(Double.valueOf(comatokenizer
							.nextToken()));
					retailerLocationsProd.add(retailerProd);
				}
			}
		} catch (Exception e) {
			LOG.info("Inside Utility : getRetProdDetails " + e.getMessage());
			throw new ScanSeeServiceException(e.getMessage());
		}

		return retailerLocationsProd;
	}

	/**
	 * This method is used for converting json to object list.
	 * 
	 * @param jsonStr
	 * @return
	 */
	public static List<RetailerLocation> jsonToObjectList(String jsonStr) {

		final String methodName = "jsonToObjectList";
		LOG.info("methodName" + methodName);
		ProductJson prodJson = new ProductJson();
		try {
			ObjectMapper om = new ObjectMapper();
			prodJson = om.readValue(jsonStr.getBytes(), ProductJson.class);
			LOG.info("Size " + prodJson.getLocationData().size());
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prodJson.getLocationData();

	}

	/**
	 * This method is used for converting json to object
	 * 
	 * @param jsonStr
	 * @return
	 */
	public static List<ManageProducts> jsonToObject(String jsonStr) {

		final String methodName = "getDeals";
		ProductJson prodJson = new ProductJson();
		try {
			ObjectMapper om = new ObjectMapper();
			prodJson = om.readValue(jsonStr.getBytes(), ProductJson.class);
			LOG.info("Size " + prodJson.getProductData().size());
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prodJson.getProductData();

	}

	public static RetailerLocationProductJson jsonToProdInfo(String jsonStr) {

		final String methodName = "RetailerLocationProductJson";
		RetailerLocationProductJson prodJson = new RetailerLocationProductJson();
		try {
			ObjectMapper om = new ObjectMapper();
			prodJson = om.readValue(jsonStr.getBytes(),
					RetailerLocationProductJson.class);
			// LOG.info("Size " + prodJson.getProductData().size());
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prodJson;

	}

	/** Check whether string s is empty. */
	public static boolean isEmpty(String s) {
		return ((s == null) || (s.trim().length() == 0));
	}

	/** Returns true if character c is a digit (0 .. 9). */
	public static boolean isDigit(char c) {
		return Character.isDigit(c);
	}

	public static boolean isDouble(Double d) {

		String s = d.toString();
		if (isEmpty(s))
			return defaultEmptyOK;

		boolean seenDecimalPoint = false;

		if (s.startsWith(decimalPointDelimiter))
			return false;

		// Search through string's characters one by one
		// until we find a non-numeric character.
		// When we do, return false; if we don't, return true.
		for (int i = 0; i < s.length(); i++) {
			// Check that current character is number.
			char c = s.charAt(i);

			if (c == decimalPointDelimiter.charAt(0)) {
				if (!seenDecimalPoint) {
					seenDecimalPoint = true;
				} else {
					return false;
				}
			} else {
				if (!isDigit(c))
					return false;
			}
		}
		// All characters are numbers.
		return true;
	}

	public static boolean isValidDate(String date) {
		final String methodName = "isValidDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String errorMessage = null;
		Date testDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		try {
			testDate = sdf.parse(date);
			if (!sdf.format(testDate).equals(date)) {
				return false;
			} else {
				return true;
			}

		} catch (ParseException e) {

			return false;
		}

	}

	public static String getCommaSeparatedVal(String[] str) {
		String finalValu = null;
		try {
			if (str != null && str.length > 0) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < str.length; i++) {
					if (i < str.length - 1) {
						sb.append(str[i] + ",");
					} else {
						sb.append(str[i]);
					}
				}

				finalValu = sb.toString();
			} else {
				return finalValu;
			}
		} catch (Exception e) {
			return finalValu;
		}

		return finalValu;
	}

	/**
	 * This method return phone number in (123)456-7890.
	 * 
	 * @param strPhone
	 * @return
	 */
	public static String getPhoneFormate(String strPhoneNo) {

		StringBuilder strPhoneNum = null;
		String formattedPhNo = null;
		if (null != strPhoneNo && !"".equals(strPhoneNo)) {
			strPhoneNo.trim();
			strPhoneNum = new StringBuilder(strPhoneNo).insert(0, "(")
					.insert(4, ")").insert(8, "-");

			formattedPhNo = strPhoneNum.toString();
		}

		return formattedPhNo;
	}

	/**
	 * This method return phone number in 1234567890.
	 * 
	 * @param strPhone
	 * @return
	 */
	public static String removePhoneFormate(String strPhoneNo) {
		String strPhoneNoFormat = "";
		if ("".equals(strPhoneNo)) {
			return null;
		} else {
			for (int i = 0; i < strPhoneNo.length(); i++) {
				if (strPhoneNo.charAt(i) != '(' && strPhoneNo.charAt(i) != ')'
						&& strPhoneNo.charAt(i) != '-') {
					strPhoneNoFormat += strPhoneNo.charAt(i);
				}
			}
			return strPhoneNoFormat.toString();
		}
	}

	/**
	 * Registration Success mail .
	 * 
	 * @param supplierRegistrationInfo
	 * @param strloginusername
	 * @param strTMailId
	 * @throws ScanSeeServiceException
	 * @throws MessagingException
	 */

	public static String sendMailSupplierLoginSuccess(
			SupplierRegistrationInfo supplierRegistrationInfo, String smtpHost,
			String smtpPort, String strFromEmailId, String strLogoImage)
			throws ScanSeeServiceException {
		LOG.info("Inside Utility : sendMailSupplierLoginSuccess ");
		String strToMailId = supplierRegistrationInfo.getContactEmail();
		String strSubject = ApplicationConstants.SUBJECT_MSG_FOR_SUCCESS_REGISTRATION;
		String respone = null;
		String autogenpswd = supplierRegistrationInfo.getPassword();
		String strloginusername = supplierRegistrationInfo.getContactFName();
		String userlogin = supplierRegistrationInfo.getUserName();
		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate.append("<body>\n");
		emailTemplate.append("<table width= \"100%\">");
		emailTemplate.append("<tr>");
		emailTemplate.append("<td colspan=\"2\">Hi <span>" + strloginusername
				+ " ,</span></td>");
		emailTemplate.append("</tr>");
		emailTemplate.append("<br/>");
		emailTemplate
				.append("<tr><td colspan=\"2\"><p>"
						+ "You have been successfully registered at ScanSee as  Supplier. Please find the username and auto generated password to login."
						+ "</p></td></tr>");
		emailTemplate.append("<tr><td width=\"5%\">UserName:" + "</td>");
		emailTemplate.append("<td width=\"95%\"><strong>" + userlogin
				+ "</strong></td></tr>");
		emailTemplate.append("<tr><td width=\"5%\">Password:" + "</td>");
		emailTemplate.append("<td width=\"95%\"><strong>" + autogenpswd
				+ "</strong></td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\"><a href=\""
				+ supplierRegistrationInfo.getScanSeeUrl() + "login.htm"
				+ "\">" + "Click here to login" + "</a></td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\">&nbsp;" + "</td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\">" + "Regards,"
				+ "</td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\">" + "ScanSee Team"
				+ "</td></tr><br/>");
		emailTemplate.append(" <img src= " + strLogoImage
				+ " alt=\"scansee logo\"  border=\"0\"><br/>");
		emailTemplate.append("</table>");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");
		LOG.info("In Supplier HTML Body" + emailTemplate.toString());
		try {
			MailBean.mailingComponent(strFromEmailId, strToMailId, strSubject,
					emailTemplate.toString(), smtpHost, smtpPort);
			respone = ApplicationConstants.SUCCESS;
		} catch (MessagingException me) {
			LOG.info("Inside Utility : sendMailSupplierLoginSuccess : "
					+ me.getMessage());
		}
		return respone;
	}

	/**
	 * Registration Success mail .
	 * 
	 * @param objRetailerRegistration
	 * @param smtpHost
	 * @param smtpPort
	 * @throws ScanSeeServiceException
	 * @throws MessagingException
	 */
	public static String sendMailRetailerLoginSuccess(
			RetailerRegistration objRetailerRegistration, String smtpHost,
			String smtpPort, String strAdminEmailId, String strLogoImage)
			throws ScanSeeServiceException {
		LOG.info("Inside Utility : sendMailRetailerLoginSuccess ");
		String strToMailId = objRetailerRegistration.getContactEmail();
		String strSubject = ApplicationConstants.HUBCITI_SUBJECT_MSG_FOR_SUCCESS_REGISTRATION;
		String respone = null;
		String autogenpswd = objRetailerRegistration.getPassword();
		String strloginusername = objRetailerRegistration.getContactFName();
		String userlogin = objRetailerRegistration.getUserName();
		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate.append("<body>\n");
		emailTemplate.append("<table width= \"100%\">");
		emailTemplate.append("<tr>");
		emailTemplate.append("<td colspan=\"2\">Hi <span>" + strloginusername
				+ " ,</span></td>");
		emailTemplate.append("</tr>");
		emailTemplate.append("<br/>");
		emailTemplate
				.append("<tr><td colspan=\"2\"><p>"
						+ "You have been successfully registered at HubCiti as  Retailer. Please find the username and  auto generated password to login."
						+ "</p></td></tr>");
		emailTemplate.append("<tr><td width=\"5%\">UserName:" + "</td>");
		emailTemplate.append("<td width=\"95%\"><strong>" + userlogin
				+ "</strong></td></tr>");
		emailTemplate.append("<tr><td width=\"5%\">Password:" + "</td>");
		emailTemplate.append("<td width=\"95%\"><strong>" + autogenpswd
				+ "</strong></td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\"><a href=\""
				+ objRetailerRegistration.getScanSeeUrl() + "login.htm" + "\">"
				+ "Click here to login" + "</a></td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\">&nbsp;" + "</td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\">" + "Regards,"
				+ "</td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\">" + "HubCiti Team"
				+ "</td></tr><br/>");
		emailTemplate.append("<tr><td colspan=\"2\"><img src=\"" + strLogoImage
				+ "\" alt=\"hubciti logo\"  border=\"0\"></td></tr>");
		emailTemplate.append("</table>");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");
		LOG.info("In Retailer HTML Body" + emailTemplate.toString());
		try {
			MailBean.mailingComponent(strAdminEmailId, strToMailId, strSubject,
					emailTemplate.toString(), smtpHost, smtpPort);
			respone = ApplicationConstants.SUCCESS;
		} catch (MessagingException me) {
			LOG.info("Inside Utility : sendMailRetailerLoginSuccess : "
					+ me.getMessage());
		}
		return respone;
	}

	/**
	 * The method to remove HTML tags.
	 * 
	 * @param inPutXml
	 *            The input xml.
	 * @return the xml with special charcters removed.
	 */

	public static String removeHTMLTags(String inPutXml) {

		final String methodName = "removeHTMLTags";

		String outPutxML = "";
		if (null != inPutXml) {
			outPutxML = null;
			outPutxML = inPutXml.replaceAll(new String(HTMLPARAGRAPHOPEN), "");
			outPutxML = outPutxML
					.replaceAll(new String(HTMLPARAGRAPHCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBREAK), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBREAKCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLLISTITEMOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLLISTITEMCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMUNORDEREDLISTOPEN),
					"");
			outPutxML = outPutxML.replaceAll(
					new String(HTMLUNORDEREDLISTCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBOLDOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBOLDCLOSE), "");
		}

		return outPutxML;
	}

	/**
	 * This method takes any number and sets precision of 2 decimal with
	 * rounding.
	 * 
	 * @param value
	 *            values which will be formated.
	 * @return value formated values.
	 */
	public static String formatDecimalValue(String value) {
		final String methodName = "formatDecimalValue";
		if (ISDEBUGENABLED) {
			LOG.info(ApplicationConstants.METHODSTART + methodName);
		}
		if (null != value && !"".equals(value)
				&& !(value.equals(ApplicationConstants.NOTAPPLICABLE))) {
			final Double price = new Double(value);
			final DecimalFormat df = new DecimalFormat("#########.00");
			value = df.format(price);
		} else {
			value = ApplicationConstants.NOTAPPLICABLE;
		}
		if (ISDEBUGENABLED) {
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		return value;

	}

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @param dbdate
	 *            -As parameter
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 */

	public static String convertDBdate(String dbdate) {
		String javaDate = null;
		try {

			final String methodName = "convertDBdate";
			LOG.info(ApplicationConstants.METHODSTART + methodName);
			SimpleDateFormat formatter, FORMATTER;
			// formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = formatter.parse(dbdate);
			FORMATTER = new SimpleDateFormat("MM-dd-yyyy");
			LOG.info("NewDate-->" + FORMATTER.format(date));
			javaDate = FORMATTER.format(date);
		} catch (ParseException exception) {
			LOG.info("Exception in convertDBdate method"
					+ exception.getMessage());
			return null;
		}
		return javaDate;
	}

	public static Date getFormattedCurrentDate() {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		Date parsedUtilDate = null;
		/*
		 * formatting the current date.
		 */
		String currentDateStr = null;
		try {
			final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
			parsedUtilDate = formater.parse(formater.format(date));
			currentDateStr = formater.format(parsedUtilDate);
		} catch (ParseException exception) {
			LOG.info("Exception in convertDBdate method"
					+ exception.getMessage());
			// return ApplicationConstants.NOTAPPLICABLE;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return parsedUtilDate;
	}

	public static StringBuilder getMediaPath() {
		String jbossPath = System.getenv(ApplicationConstants.JBOSS_HOME);
		String fileSeparator = System.getProperty("file.separator");
		StringBuilder mediaPathBuilder = new StringBuilder();
		mediaPathBuilder.append(jbossPath);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.STANDALONE);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.DEPLOYMENT);
		mediaPathBuilder.append(fileSeparator);
		/*
		 * mediaPathBuilder.append(ApplicationConstants.DEPLOY);
		 * mediaPathBuilder.append(fileSeparator);
		 */
		mediaPathBuilder.append(ApplicationConstants.ROOTWAR);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.IMAGES);
		mediaPathBuilder.append(fileSeparator);
		return mediaPathBuilder;
	}

	public static StringBuilder getMediaPath(String folderType, int fodlerID) {
		String jbossPath = System.getenv(ApplicationConstants.JBOSS_HOME);
		String fileSeparator = System.getProperty("file.separator");
		StringBuilder mediaPathBuilder = new StringBuilder();
		mediaPathBuilder.append(jbossPath);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.STANDALONE);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.DEPLOYMENT);
		mediaPathBuilder.append(fileSeparator);
		/*
		 * mediaPathBuilder.append(ApplicationConstants.DEPLOY);
		 * mediaPathBuilder.append(fileSeparator);
		 */
		mediaPathBuilder.append(ApplicationConstants.ROOTWAR);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.IMAGES);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(folderType);
		File obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(fodlerID);
		obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		return mediaPathBuilder;
	}

	/**
	 * This method is used to round the values to nearest value.
	 * 
	 * @param distance
	 *            -As String parameter
	 * @return distance rounded value
	 */
	public static String roundNearestValues(String distance) {
		if (null != distance && !"".equals(distance)) {
			try {
				final Float fltDistance = new Float(distance);
				distance = String.valueOf(Math.round(fltDistance));
			} catch (NumberFormatException exception) {
				LOG.info("Exception in roundNearestValues method"
						+ exception.getMessage());
				distance = ApplicationConstants.NOTAPPLICABLE;
			}

		} else {
			distance = ApplicationConstants.NOTAPPLICABLE;
		}
		return distance;
	}

	/**
	 * The method for null checks.
	 * 
	 * @param arg
	 *            the input string
	 * @return the response string.
	 */

	public static String nullCheck(String arg) {

		final String methodName = "nullCheck";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != arg && !"".equals(arg)) {
			arg.trim();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arg;
	}

	/**
	 * This method is used to check nulls.
	 * 
	 * @param arg
	 *            -As parameter
	 * @return arg
	 */
	public static String isNull(String arg) {

		final String methodName = "nullCheck";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != arg && !"".equals(arg)) {
			arg.trim();
		} else {
			arg = "";
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arg;
	}

	/**
	 * This method is used to check Empty strings or "".
	 * 
	 * @param arg
	 *            -As parameter
	 * @return arg
	 */
	public static boolean isEmptyOrNullString(String arg) {

		final String methodName = "isEmptyString";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final boolean emptyString;

		if (null == arg || "".equals(arg.trim())) {
			emptyString = true;
		} else {
			emptyString = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emptyString;
	}

	public static boolean isCorrectFileExtForImage(String filename) {
		final String methodName = "isEmptyString";
		boolean status = true;
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (filename != null) {
			int mid = filename.lastIndexOf(".");

			String ext = filename.substring(mid + 1, filename.length());
			if (ext.equalsIgnoreCase("png")) {
				status = true;
			}
		} else {
			status = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	public static boolean isCorrectFileExtForAudioVideo(String filename) {
		final String methodName = "isEmptyString";
		boolean status = true;
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (filename != null) {
			int mid = filename.lastIndexOf(".");
			String ext = filename.substring(mid + 1, filename.length());
			if (ext.equalsIgnoreCase("mp4") || ext.equalsIgnoreCase("MP4")) {
				status = true;
			}
		} else {
			status = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	public static boolean isCorrectFileExtForAudio(String filename) {
		final String methodName = "isEmptyString";
		boolean status = true;
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (filename != null) {
			int mid = filename.lastIndexOf(".");
			String ext = filename.substring(mid + 1, filename.length());
			if (ext.equalsIgnoreCase("mp4") || ext.equalsIgnoreCase("MP4")
					|| ext.equalsIgnoreCase("mp3")
					|| ext.equalsIgnoreCase("MP3")) {
				status = true;
			}
		} else {
			status = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return status;
	}

	/**
	 * Method to Check if the String object is null
	 * 
	 * @param strValue
	 *            String
	 * @return strValue String
	 */
	public static String checkNull(String strValue) {
		if (strValue == null || strValue.equals("null") || strValue == "null"
				|| "".equals(strValue) || "undefined".equals(strValue)) {
			return "";
		} else {
			return strValue.trim();
		}
	}

	/**
	 * The method to get Comma Separated Product ID values.
	 * 
	 * @param shoppingProductDetails
	 *            The Product Details list to be made as comma separated.
	 * @return The string with comma separated values.
	 */

	public static String getCommaSepartedUserProdIdValues(
			List<ProductDetail> shoppingProductDetails) {

		final String methodName = "getCommaSepartedUserProdIdValues";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final int listSize = shoppingProductDetails.size();
		final StringBuilder userProductId = new StringBuilder();
		int i = 0;
		for (ProductDetail productDetail : shoppingProductDetails) {
			i++;

			userProductId.append(productDetail.getUserProductId());
			if (i != listSize) {
				userProductId.append(",");
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userProductId.toString();
	}

	/**
	 * The method for getting comma separated values.
	 * 
	 * @param shoppingProductDetails
	 *            The List of product details.
	 * @return The string with comma separated values.
	 */

	public static String getCommaSepartedValues(
			List<ProductDetail> shoppingProductDetails) {

		final String methodName = "getCommaSepartedValues";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final int listSize = shoppingProductDetails.size();
		final StringBuilder productId = new StringBuilder();
		int i = 0;
		for (ProductDetail productDetail : shoppingProductDetails) {
			i++;

			productId.append(productDetail.getProductId());
			if (i != listSize) {
				productId.append(",");
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productId.toString();
	}

	/**
	 * This method for sharing coupon information through mail.
	 * 
	 * @param shareCouponLst
	 *            contains coupon information
	 * @return response
	 */
	public static String formCouponInfoHTML(List<CouponDetails> shareCouponLst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formCouponInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr>");

		emailTemplate.append("<td  height=\"36\"><b>Coupon Name:</b> "
				+ shareCouponLst.get(0).getCouponName() + "</td></tr>");

		if (shareCouponLst.get(0).getCouponURL() != null
				&& !shareCouponLst.get(0).getCouponURL().equals("N/A")) {
			emailTemplate.append("<tr><td  height=\"36\"><b>Coupon URL:</b> "
					+ shareCouponLst.get(0).getCouponURL() + "</td></tr>");
		}
		emailTemplate
				.append("<tr><td  height=\"36\"><b>Coupon Start Date:</b> "
						+ shareCouponLst.get(0).getCouponStartDate()
						+ "</td></tr>");
		emailTemplate
				.append("<tr><td  height=\"36\"><b>Coupon Expire Date:</b> "
						+ shareCouponLst.get(0).getCouponExpireDate()
						+ "</td></tr>");
		emailTemplate.append("</table>");

		emailTemplate
				.append("<h4>Check out  <a href=\"www.ScanSee.com\">www.ScanSee.com</a> for more.</h4>\n");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * This method for sharing coupon information through mail.
	 * 
	 * @param shareCouponLst
	 *            contains coupon information
	 * @return response
	 */
	public static String formRebateInfoHTML(List<RebateDetail> rebateDetaillst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formRebateInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr>");

		emailTemplate.append("<td  height=\"36\"><b>Rebate Name :</b> "
				+ rebateDetaillst.get(0).getRebateName() + "</td></tr>");

		if (rebateDetaillst.get(0).getRebateURL() != null
				&& !rebateDetaillst.get(0).getRebateURL().equals("N/A")) {
			emailTemplate.append("<tr><td  height=\"36\"><b>Rebate URL :</b> "
					+ rebateDetaillst.get(0).getRebateURL() + "</td></tr>");
		}
		emailTemplate
				.append("<tr><td  height=\"36\"><b>Rebate Start Date :</b> "
						+ rebateDetaillst.get(0).getRebateStartDate()
						+ "</td></tr>");
		emailTemplate.append("<tr><td  height=\"36\"><b>Rebate End Date :</b> "
				+ rebateDetaillst.get(0).getRebateEndDate() + "</td></tr>");
		emailTemplate.append("</table>");

		emailTemplate
				.append("<h4>Check out  <a href=\"www.ScanSee.com\">www.ScanSee.com</a> for more.</h4>\n");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * This method for sharing loyalty information through mail.
	 * 
	 * @param shareCouponLst
	 *            contains coupon information
	 * @return response
	 */
	public static String formLoyaltyInfoHTML(
			List<LoyaltyDetail> loyaltyDetaillst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formLoyaltyInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr>");

		emailTemplate.append("<td  height=\"36\"><b>LoyaltyDeal Name :</b> "
				+ loyaltyDetaillst.get(0).getLoyaltyDealName() + "</td></tr>");

		if (loyaltyDetaillst.get(0).getLoyaltyURL() != null
				&& !loyaltyDetaillst.get(0).getLoyaltyURL().equals("N/A")) {
			emailTemplate
					.append("<tr><td  height=\"36\"><b>LoyaltyDeal URL:</b> "
							+ loyaltyDetaillst.get(0).getLoyaltyURL()
							+ "</td></tr>");
		}

		emailTemplate
				.append("<tr><td  height=\"36\"><b>LoyaltyDeal Start Date :</b> "
						+ loyaltyDetaillst.get(0).getLoyaltyDealStartDate()
						+ "</td></tr>");
		emailTemplate
				.append("<tr><td  height=\"36\"><b>LoyaltyDeal Expire Date :</b> "
						+ loyaltyDetaillst.get(0).getLoyaltyDealExpireDate()
						+ "</td></tr>");
		emailTemplate.append("</table>");

		emailTemplate
				.append("<h4>Check out  <a href=\"www.ScanSee.com\">www.ScanSee.com</a> for more.</h4>\n");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * Registration Success mail .
	 * 
	 * @param objUsers
	 * @param smtpHost
	 * @param smtpPort
	 * @throws ScanSeeServiceException
	 * @throws MessagingException
	 */
	public static String sendMailShopperLoginSuccess(Users objUsers,
			String smtpHost, String smtpPort, String strAdminEmailId,
			String strLogoImage) throws ScanSeeServiceException {
		LOG.info("Inside Utility : sendMailShopperLoginSuccess ");
		String strToMailId = objUsers.getEmail();
		String strSubject = ApplicationConstants.SUBJECT_MSG_FOR_SUCCESS_REGISTRATION;
		String respone = null;
		String strloginusername = objUsers.getFirstName();
		String strMatter = "<Html>Hi " + strloginusername + ",<br/><br/>"
				+ "Welcome <b>" + strloginusername
				+ "</b>, you are successfully registered!!!!." + "<br/><br/>"
				+ "Regards," + "<br/>" + "ScanSee Team<br/>" + "<img src= "
				+ strLogoImage + " alt=\"scansee logo\"  border=\"0\"></Html>";
		try {
			MailBean.mailingComponent(strAdminEmailId, strToMailId, strSubject,
					strMatter, smtpHost, smtpPort);
			respone = ApplicationConstants.SUCCESS;
		} catch (MessagingException me) {
			LOG.info("Inside Utility : sendMailShopperLoginSuccess : "
					+ me.getMessage());
		}
		return respone;
	}

	public static String truncate(String value, int length) {
		if (value != null && value.length() > length)
			value = value.substring(0, length);
		return value;
	}

	public static boolean validDimension(int height, int width,
			InputStream image) throws IOException {
		final String methodName = "validDimension";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		BufferedImage img = ImageIO.read(image);
		int w = img.getWidth(null);
		int h = img.getHeight(null);
		LOG.info("Image Width*********" + w);
		LOG.info("Image Hieght*********" + h);
		if (w <= width && h <= height) {
			return true;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return false;
	}

	public static String formHotDealInfoHTML(List<HotDealsDetails> hotDeallst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formHotDealInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");

		emailTemplate.append("<tr><td width=\"300\" rowspan=\"7\">");
		emailTemplate.append("<img height=\"250\" width=\"250\" src=\""
				+ hotDeallst.get(0).getHotDealImagePath() + "\"/>");

		if (hotDeallst.get(0).getHotDealName() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>HotDeal Name:</b> "
							+ hotDeallst.get(0).getHotDealName() + "</td></tr>");
		}
		if (hotDeallst.get(0).getHdURL() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>URL :</b> "
							+ hotDeallst.get(0).getHdURL() + "</td></tr>");
		}
		if (hotDeallst.get(0).gethDStartDate() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>HotDeal Start Date:</b> "
							+ hotDeallst.get(0).gethDStartDate() + "</td></tr>");
		}
		if (hotDeallst.get(0).gethDEndDate() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\">< b>HotDeal End Date :</b> "
							+ hotDeallst.get(0).gethDEndDate() + "</td></tr>");
		}
		emailTemplate.append("</table>");

		emailTemplate.append("</table>");
		emailTemplate
				.append("<h4>Check out  <a href=\"www.ScanSee.com\">www.ScanSee.com</a> for more.</h4>\n");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * The method for forming HTML with product Info.
	 * 
	 * @param productDetail
	 *            product object.
	 * @param retailerDetail
	 *            retailer object
	 * @param userId
	 *            int parameter
	 * @return The email template.
	 * @throws NoSuchAlgorithmException
	 *             - Exception related to algorithm
	 * @throws NoSuchPaddingException
	 *             - Exception related to Padding
	 * @throws InvalidKeyException
	 *             - Exception related to InvalidKey
	 * @throws InvalidAlgorithmParameterException
	 *             - Exception related to InvalidAlgorithmParameter
	 * @throws InvalidKeySpecException
	 *             - Exception related to InvalidKeySpec
	 * @throws IllegalBlockSizeException
	 *             - Exception related to IllegalBlockSize
	 * @throws BadPaddingException
	 *             - Exception related to BadPaddings
	 */

	public static String formProductInfoHTML(ProductDetail productDetail,
			Integer userId, String shareURL) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, InvalidKeySpecException,
			IllegalBlockSizeException, BadPaddingException {

		final String methodName = "formProductInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String productKey = null;
		String productId = Integer.toString(productDetail.getProductId());
		/*
		 * String decryptedUserId = null; if (userId != null) { decryptedUserId
		 * = Integer.toString(userId); } String key = decryptedUserId + "/" +
		 * productId; EncryptDecryptPwd enryptDecryptpwd = null;
		 * enryptDecryptpwd = new EncryptDecryptPwd(); productKey =
		 * enryptDecryptpwd.encrypt(key);
		 */

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr><td width=\"300\" rowspan=\"7\">");
		if (productDetail.getProductImagePath() != null) {
			emailTemplate.append("<img height=\"250\" width=\"250\" src=\""
					+ productDetail.getProductImagePath() + "\"/>");
		}
		emailTemplate
				.append("<td width=\"300\" height=\"36\"><b>Product Name:</b> "
						+ productDetail.getProductName() + "</td></tr>");
		if (productDetail.getProductShortDescription() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>Product Information:</b> "
							+ productDetail.getProductShortDescription()
							+ "</td></tr>");
		}
		if (productDetail.getManufacturersName() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>Manufacturer Name:</b> "
							+ productDetail.getManufacturersName()
							+ "</td></tr>");
		}
		if (productDetail.getWeight() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>Weight:</b> "
							+ productDetail.getWeight() + "</td></tr>");
		}
		if (productDetail.getWeightUnits() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>Weight Units:</b> "
							+ productDetail.getWeightUnits() + "</td></tr>");
		}
		if (productDetail.getProductExpDate() != null) {
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\">< b>Product Expiration Date:</b> "
							+ productDetail.getProductExpDate() + "</td></tr>");
		}
		emailTemplate.append("</table>");
		if (userId != null) {
			emailTemplate.append("<h4>More info <a href=\"" + shareURL
					+ "key1=" + productId + "&key2="
					+ productDetail.getProductListID() + "&share=yes"
					+ "\"/>Click here</a></h4>\n");
		}
		emailTemplate
				.append("<h4>Check out  <a href=\"www.ScanSee.com\">www.ScanSee.com</a> for more.</h4>\n");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * Method to Check if the String object is null
	 * 
	 * @param strValue
	 *            String
	 * @return strValue String
	 */
	public static String checkNull(Object strValue) {
		if (strValue == null || strValue.toString().equals("null")
				|| strValue.toString() == "null"
				|| "undefined".equals(strValue)) {
			return "";
		} else {
			return strValue.toString().trim();
		}
	}

	public static ManageProducts getProductList(
			CommonsMultipartFile productuploadFilePath)
			throws ScanSeeServiceException {
		final String methodName = "getProductList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		BufferedReader br;
		ManageProducts manageProducts = null;
		StringBuffer errMsg = null;
		boolean validationFlg = false;
		ManageProducts objManageProducts = new ManageProducts();
		ArrayList<ManageProducts> validProductList = new ArrayList<ManageProducts>();
		ArrayList<ManageProducts> invalidProductList = new ArrayList<ManageProducts>();

		try {
			br = new BufferedReader(new InputStreamReader(
					productuploadFilePath.getInputStream()));
			CSVReader reader = new CSVReader(br);
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				errMsg = new StringBuffer();
				validationFlg = false;
				manageProducts = new ManageProducts();
				manageProducts.setProductName(nextLine[0]);
				manageProducts.setScanCode(nextLine[1]);
				manageProducts.setScanType(nextLine[2]);
				manageProducts.setProdExpirationDate(nextLine[3]);
				if (!"".equals(Utility.checkNull(nextLine[4]))) {
					manageProducts.setImagePath(nextLine[4]);
				} else {
					manageProducts.setImagePath(null);
				}
				manageProducts.setCategory(nextLine[5]);
				manageProducts.setShortDescription(nextLine[6]);
				manageProducts.setLongDescription(nextLine[7]);
				manageProducts.setModelNumber(nextLine[8]);
				manageProducts.setSuggestedRetPrice(nextLine[9]);
				manageProducts.setProductWeight(nextLine[10]);
				manageProducts.setWeightUnits(nextLine[11]);
				manageProducts.setManufProuductURL(nextLine[12]);
				if (!nextLine[0].equals("")) {

					String productName = nextLine[0];

					if (null == productName || "".equals(productName)) {

						validationFlg = true;
						errMsg.append("Product Name Can't be empty,");
					}

					String scanCode = nextLine[1];
					if (null == scanCode || "".equals(scanCode)) {

						validationFlg = true;
						errMsg.append("Scan Code Can't be empty,");
					}

					String scanType = nextLine[2];

					if (null != scanType && !scanType.equals("")) {

						try {
							Integer.parseInt(scanType);
						} catch (NumberFormatException e) {
							validationFlg = true;
							errMsg.append("Scan Type should be a number,");
						}

					}

					manageProducts.setProdExpirationDate(nextLine[3]);

					String prodExpirationDate = nextLine[3];

					if (null != prodExpirationDate
							&& !prodExpirationDate.equals("")) {
						try {
							DateFormat oldFormatter = new SimpleDateFormat(
									"MM/dd/yyyy");
							oldFormatter.parse(prodExpirationDate);
						} catch (ParseException exception) {

							validationFlg = true;
							errMsg.append("Product Expiry date should be a date,");

						}
					}

					String suggestedRetPrice = nextLine[9];

					if (null == suggestedRetPrice
							|| "".equals(suggestedRetPrice)) {

						validationFlg = true;
						errMsg.append("Suggested Retailer Price Can't be empty,");
					}

					String longDescription = nextLine[7];
					if (null == longDescription || "".equals(longDescription)) {

						validationFlg = true;
						errMsg.append("Long Description Can't be empty,");
					}

					String productWeight = nextLine[10];
					if (null != productWeight && !"".equals(productWeight)) {
						try {
							Float.parseFloat(productWeight);
						} catch (NumberFormatException e) {
							validationFlg = true;
							errMsg.append("Product Weight should be a number,");
						}
					}

					String manufProuductURL = nextLine[12];
					if (null != manufProuductURL
							&& !"".equals(manufProuductURL)) {
						boolean isValid = validateURL(manufProuductURL);
						if (!isValid) {
							validationFlg = true;
							errMsg.append("Invalid Product manufacture URL,");
						}
					}

					String audioPath = nextLine[13];

					if (ApplicationConstants.QUOTE.equals(audioPath)
							|| "".equals(audioPath)) {
						manageProducts.setAudioMediaPath(null);
					} else {
						manageProducts.setAudioMediaPath(audioPath);
					}

					String videoPath = nextLine[14];
					if (ApplicationConstants.QUOTE.equals(videoPath)
							|| "".equals(videoPath)) {
						manageProducts.setVideoMediaPath(null);
					} else {
						manageProducts.setVideoMediaPath(videoPath);
					}
					if (nextLine.length == 16) {
						String imagePath = nextLine[15];
						if (ApplicationConstants.QUOTE.equals(imagePath)
								|| "".equals(imagePath)) {
							manageProducts.setProductImagePath(null);
						} else {
							manageProducts.setProductImagePath(imagePath);
						}
					}

				}

				if (validationFlg) {
					manageProducts.setErrMsg(errMsg.toString());
					invalidProductList.add(manageProducts);
				} else {

					validProductList.add(manageProducts);
				}

				// list.add(manageProducts);
			}
			/* Remove header instruction names from *.csv file */

			if (!invalidProductList.isEmpty()) {
				invalidProductList.remove(0);
			}

			objManageProducts.setInvalidProductList(invalidProductList);
			objManageProducts.setValidProductList(validProductList);
			LOG.debug(" Product file {} validation is Completed",
					productuploadFilePath.getOriginalFilename());
		} catch (IOException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);
		} catch (Exception exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objManageProducts;

	}

	public static ManageProducts getAttributeList(
			CommonsMultipartFile productuploadFilePath)
			throws ScanSeeServiceException {
		ManageProducts manageProducts = null;
		BufferedReader br;
		ArrayList<ManageProducts> validProductList = new ArrayList<ManageProducts>();
		ArrayList<ManageProducts> invalidProductList = new ArrayList<ManageProducts>();
		StringBuffer errMsg = null;
		boolean validationFlg = false;
		ManageProducts objManageProducts = new ManageProducts();
		try {
			br = new BufferedReader(new InputStreamReader(
					productuploadFilePath.getInputStream()));
			CSVReader reader = new CSVReader(br);
			String[] nextLine;

			while ((nextLine = reader.readNext()) != null) {
				manageProducts = new ManageProducts();
				errMsg = new StringBuffer();
				validationFlg = false;
				if (!nextLine[0].equals("") && nextLine.length == 3) {
					manageProducts.setScanCode(nextLine[0]);
					manageProducts.setAttributeName(nextLine[1]);
					manageProducts.setAttributeValue(nextLine[2]);

					String scanCode = nextLine[0];

					if (scanCode == null || "".equals(scanCode)) {
						validationFlg = true;
						errMsg.append("ScanCode can't be Empty,");

					}

					String attrbtName = nextLine[1];

					if (attrbtName == null || "".equals(attrbtName)) {
						validationFlg = true;
						errMsg.append("Attribute Name can't be Empty,");

					}

					String attrbtVal = nextLine[2];

					if (attrbtVal == null || "".equals(attrbtVal)) {
						validationFlg = true;
						errMsg.append("Attribute Value can't be Empty");

					}

					if (validationFlg) {
						manageProducts.setErrMsg(errMsg.toString());
						invalidProductList.add(manageProducts);
					} else {

						validProductList.add(manageProducts);
					}

				}
			}
			if (!validProductList.isEmpty()) {
				/* Remove header instruction names from *.csv file */
				validProductList.remove(0);
			}
			objManageProducts.setInvalidProductList(invalidProductList);
			objManageProducts.setValidProductList(validProductList);

		} catch (IOException exception) {
			throw new ScanSeeServiceException(exception);
		} catch (Exception exception) {
			throw new ScanSeeServiceException(exception);
		}
		return objManageProducts;

	}

	public static ArrayList<RetailerLocation> getRetailLocationList(
			CommonsMultipartFile locationFile) throws ScanSeeServiceException {
		LOG.info("Inside Utility : getRetailLocationList ");
		RetailerLocation retailerLocation = null;
		ArrayList<RetailerLocation> arLocationList = new ArrayList<RetailerLocation>();
		BufferedReader br;
		int count = 0;
		try {
			// create BufferedReader to read csv file
			br = new BufferedReader(new InputStreamReader(
					locationFile.getInputStream()));
			CSVReader reader = new CSVReader(br);
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				retailerLocation = new RetailerLocation();
				// below condition is been added to avoid array out of bound
				// exception
				if (!nextLine[0].equals("") && nextLine.length != 0) {
					if (!nextLine[0].equals(" ")) {
						retailerLocation.setStoreIdentification(nextLine[0]);
						retailerLocation.setAddress1(nextLine[1]);
						retailerLocation.setCity(nextLine[2]);
						retailerLocation.setState(nextLine[3]);
						retailerLocation.setPostalCode(nextLine[4]);
						retailerLocation.setPhonenumber(nextLine[5]);
						retailerLocation.setRetailLocationUrl(nextLine[6]);
						retailerLocation.setKeyword(nextLine[7]);
						retailerLocation.setUploadImgName(nextLine[8]);
						if (count != 0) {
							StringBuffer strAddress = new StringBuffer();
							if (!"".equals(Utility.checkNull(retailerLocation.getAddress1().trim()))) {
								strAddress.append(retailerLocation.getAddress1().trim()) ;
								strAddress.append(", ") ;
				
							}
							if (!"".equals(Utility.checkNull(retailerLocation.getState().trim()))) {
								strAddress.append(retailerLocation.getState().trim()) ;
								strAddress.append(", ") ;
							}
							if (!"".equals(Utility.checkNull(retailerLocation.getCity().trim()))) {
								strAddress.append(retailerLocation.getCity().trim()) ;
								strAddress.append(", ") ;
							}
							if (!"".equals(Utility.checkNull(retailerLocation.getPostalCode().trim()))) {
								strAddress.append(retailerLocation.getPostalCode().trim()) ;
							}
							/*strAddress = retailerLocation.getAddress1().trim()
									+ ", " + retailerLocation.getState() + ", "
									+ retailerLocation.getCity() + ", "
									+ retailerLocation.getPostalCode();*/
							if (!"".equals(Utility.checkNull(strAddress.toString()))) {
								GAddress objGAddress = UtilCode
										.getGeoDetails(strAddress.toString());
								if (null != objGAddress) {
									retailerLocation
											.setRetailerLocationLongitude(objGAddress
													.getLng());
									retailerLocation
											.setRetailerLocationLatitude(objGAddress
													.getLat());
								} else {
									LOG.error("Could not find Lat and Lang for the address:"
											+ objGAddress);
								}
							}
						}
						arLocationList.add(retailerLocation);
						count++;
					}
				}
			}
			/* Remove header instruction names from *.csv file */
			if (!arLocationList.isEmpty()) {
				arLocationList.remove(0);
			}
		} catch (IOException exception) {
			LOG.info("Inside Utility : getRetailLocationList : "
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception);
		} catch (Exception exception) {
			LOG.info("Inside Utility : getRetailLocationList : "
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}
		return arLocationList;
	}

	/*
	 * public static ArrayList<RetailProduct>
	 * getUploadedProductsList(CommonsMultipartFile locationFile) throws
	 * ScanSeeServiceException { RetailProduct retailerProd = null;
	 * ArrayList<RetailProduct> retailerLocationsProd = new
	 * ArrayList<RetailProduct>(); BufferedReader br; try { br = new
	 * BufferedReader(new InputStreamReader(locationFile.getInputStream()));
	 * CSVReader reader = new CSVReader(br); String[] nextLine; while ((nextLine
	 * = reader.readNext()) != null) { if (!nextLine[0].equals("")) { if
	 * (!nextLine[0].equals(" ")) { retailerProd = new RetailProduct();
	 * retailerProd.setStoreIdentificationID(nextLine[0]);
	 * retailerProd.setScanCode(nextLine[1]);
	 * retailerProd.setLongDescription(nextLine[2]);
	 * retailerProd.setProductPrice(nextLine[3]);
	 * retailerProd.setProductSalePrice(nextLine[4]);
	 * retailerProd.setSaleStartDate(nextLine[5]);
	 * retailerProd.setSaleEndDate(nextLine[6]);
	 * retailerLocationsProd.add(retailerProd); } } } Remove header instruction
	 * names from *.csv file retailerLocationsProd.remove(0); } catch
	 * (IOException exception) { LOG.error(exception.getMessage(), exception);
	 * throw new ScanSeeServiceException(exception); } catch (Exception
	 * exception) { LOG.error(exception.getMessage(), exception); throw new
	 * ScanSeeServiceException(exception.getMessage()); } return
	 * retailerLocationsProd; }
	 */

	// used for generating auto generator password
	public static String randomString(int len) {

		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(autoGeneratedPswd.charAt(rnd.nextInt(autoGeneratedPswd
					.length())));
		return sb.toString();
	}

	/**
	 * The method for writing Store & Offer nodes data into the excel file.
	 * 
	 * @param whaleSharOfferList
	 *            as request parameter.
	 * @param whaleSharkOfferList
	 *            as request parameter.
	 * @throws IOException
	 */
	public static void writeDatToCSV(
			ArrayList<RetailerLocation> retailerLocationList, String filename) {
		File f = new File(filename);
		String strLocationImg  = null;
		FileWriter writer;
		try {
			if (!f.exists()) {
				f.createNewFile();
			}
			writer = new FileWriter(filename);
			writer.append("Store Id");
			writer.append(',');
			writer.append("Store Address");
			writer.append(',');
			writer.append("City");
			writer.append(',');
			writer.append("State Code");
			writer.append(',');
			writer.append("Postal Code");
			writer.append(',');
			writer.append("Phone Number");
			writer.append(',');
			writer.append("Website URL");
			writer.append(',');
			writer.append("Keywords");
			writer.append(',');
			writer.append("Location Logo Name");
			writer.append(',');
			writer.append("Reason for Discarding");
			writer.append('\n');
			for (int i = 0; i < retailerLocationList.size(); i++) {
				// LOG.info(".......After for loop : Stores");
				writer.append(retailerLocationList.get(i)
						.getStoreIdentification() == null ? ""
						: retailerLocationList.get(i).getStoreIdentification());
				writer.append(',');
				writer.append(retailerLocationList.get(i).getAddress1() == null ? ""
						: StringEscapeUtils.escapeCsv(retailerLocationList.get(
								i).getAddress1()));
				writer.append(',');
				writer.append(retailerLocationList.get(i).getCity() == null ? ""
						: retailerLocationList.get(i).getCity());
				writer.append(',');
				writer.append(retailerLocationList.get(i).getState() == null ? ""
						: retailerLocationList.get(i).getState());
				writer.append(',');
				writer.append(retailerLocationList.get(i).getPostalCode() == null ? ""
						: retailerLocationList.get(i).getPostalCode());
				writer.append(',');
				writer.append(retailerLocationList.get(i).getPhonenumber() == null ? ""
						: retailerLocationList.get(i).getPhonenumber());
				writer.append(',');
				writer.append(retailerLocationList.get(i).getRetailLocationUrl() == null ? "": retailerLocationList.get(i).getRetailLocationUrl());
				writer.append(',');
				writer.append(retailerLocationList.get(i).getKeyword() == null ? "": retailerLocationList.get(i).getKeyword());
				writer.append(',');
				if (!"".equals(Utility.checkNull(retailerLocationList.get(i).getGridImgLocationPath()))) {
					strLocationImg = retailerLocationList.get(i).getGridImgLocationPath();
					final int dotIndex = strLocationImg.lastIndexOf('.');
					final int slashIndex = strLocationImg.lastIndexOf("/");
					if (dotIndex != -1) {
						retailerLocationList.get(i).setGridImgLocationPath(strLocationImg.substring(slashIndex + 1, strLocationImg.length()));
					} 
				}
				writer.append(retailerLocationList.get(i).getGridImgLocationPath() == null ? "": retailerLocationList.get(i).getGridImgLocationPath());
				writer.append(',');
				writer.append(retailerLocationList.get(i).getReasonForDiscarding() == null ? "": retailerLocationList.get(i).getReasonForDiscarding());
				writer.append('\n');
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to send mail to Forgot Password.
	 * 
	 * @param objUsers
	 *            ,
	 * @param strloginusername
	 * @param strFromEmailId
	 * @param strTMailId
	 * @throws ScanSeeServiceException
	 * @throws MessagingException
	 * @return strResponse
	 */
	public static String sendMailForgetPassword(Users objUsers,
			String smtpHost, String smtpPort, String strFromEmailId,
			String strLogoImage) throws ScanSeeServiceException {
		LOG.info("Inside Utility : sendMailForgetPassword ");
		String strToMailId = objUsers.getEmail();
		String strSubject = ApplicationConstants.SUBJECT_MSG_FOR_FORGOTPWD_MAILSEND;
		String strResponse = null;
		String strContent = null;
		String strFName = null;

		String autogenpswd = objUsers.getPassword();
		String strloginusername = objUsers.getUserName();
		strFName = objUsers.getFirstName();
		strContent = "<Html>Hi " + strFName + "," + "<br/><br/>"
				+ ApplicationConstants.CONTENT_MSG_FOR_FORGOTPWD_MAILSEND
				+ "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UserName : <b>"
				+ strloginusername
				+ "</b><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Password : <b>"
				+ autogenpswd + "</b><br/><br/>"
				+ " Regards,<br/>ScanSee Team <br/>" + " <img src= "
				+ strLogoImage + " alt=\"scansee logo\"  border=\"0\"></Html>";
		try {
			EmailComponent.sendMailWithOutAttachmentWithoutCC(strFromEmailId,
					strToMailId, strSubject, strContent, smtpHost, smtpPort);
			strResponse = ApplicationConstants.SUCCESS;
		} catch (MessagingException me) {
			LOG.info("Inside Utility : sendMailForgetPassword : "
					+ me.getMessage());
			throw new ScanSeeServiceException(me);
		}
		return strResponse;
	}

	public static boolean validateURL(String strURL) {
		LOG.info("Inside Utility : validateURL");
		Pattern pattern = null;
		Matcher matcher;
		if (!"".equals(Utility.checkNull(strURL))) {
				strURL = strURL.trim();
				pattern = Pattern.compile(ApplicationConstants.URL_PATTERN);
				matcher = pattern.matcher(strURL);
				return matcher.matches();
		}
		return true;
	}

	public static String getQrStorePath() {
		String jbossPath = System.getenv(ApplicationConstants.JBOSS_HOME);
		String fileSeparator = System.getProperty("file.separator");
		StringBuilder mediaPathBuilder = new StringBuilder();
		mediaPathBuilder.append(jbossPath);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.STANDALONE);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.DEPLOYMENT);
		mediaPathBuilder.append(fileSeparator);
		/*
		 * mediaPathBuilder.append(ApplicationConstants.DEPLOY);
		 * mediaPathBuilder.append(fileSeparator);
		 */
		mediaPathBuilder.append(ApplicationConstants.ROOTWAR);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.IMAGES);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.QRPATH);
		File obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		return mediaPathBuilder.toString();
	}

	public static StringBuilder getTempMediaPath(String folderType) {
		String jbossPath = System.getenv(ApplicationConstants.JBOSS_HOME);
		String fileSeparator = System.getProperty("file.separator");
		StringBuilder mediaPathBuilder = new StringBuilder();
		mediaPathBuilder.append(jbossPath);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.STANDALONE);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.DEPLOYMENT);
		mediaPathBuilder.append(fileSeparator);
		/*
		 * mediaPathBuilder.append(ApplicationConstants.DEPLOY);
		 * mediaPathBuilder.append(fileSeparator);
		 */
		mediaPathBuilder.append(ApplicationConstants.ROOTWAR);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.IMAGES);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(folderType);
		File obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		return mediaPathBuilder;
	}

	public static String writeFileData(InputStream inputStream,
			String destinationPath) throws ScanSeeServiceException {
		final String methoName = "writeFileData";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		OutputStream outputStream = null;

		// byte[] buffer=file.getFileData();
		byte[] buffer = new byte[32];

		// destinationPath=file.getFilePath();

		try {
			outputStream = new FileOutputStream(destinationPath);

			while (inputStream.read(buffer) != -1) {
				outputStream.write(buffer);
			}
		} catch (FileNotFoundException exception) {
			LOG.error("Exception Occurred in writeFileData {}",
					exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		} catch (IOException exception) {
			LOG.error("Exception Occurred in writeFileData {}",
					exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException exception) {
					LOG.error("Exception Occurred in writeFileData {}",
							exception.getMessage());
					throw new ScanSeeServiceException(exception.getMessage(),
							exception);
				}
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException exception) {
					LOG.error("Exception Occurred in writeFileData {}",
							exception.getMessage());
					throw new ScanSeeServiceException(exception.getMessage(),
							exception);
				}
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return "success";
	}

	/**
	 * This method is used for Writing Discarded Attributes Data to CSV file.
	 * 
	 * @param arMgProductsList
	 * @param strFileName
	 */
	public static void writeDiscardedAttributesDataToCSV(
			List<ManageProducts> arMgProductsList, String strFileName) {
		LOG.info("Inside Utility : writeDiscardedAttributesDataToCSV");
		File objFile = new File(strFileName);
		FileWriter objFileWriter = null;
		try {
			if (!objFile.exists()) {
				objFile.createNewFile();
			}
			objFileWriter = new FileWriter(strFileName);
			objFileWriter.append("ScanCode");
			objFileWriter.append(",");
			objFileWriter.append("Attribute Name");
			objFileWriter.append(",");
			objFileWriter.append("Display Value");
			objFileWriter.append(",");
			objFileWriter.append("ReasonForDiscarding");
			objFileWriter.append("\n");
			for (int i = 0; i < arMgProductsList.size(); i++) {
				objFileWriter
						.append(arMgProductsList.get(i).getScanCode() == null ? ""
								: arMgProductsList.get(i).getScanCode());
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getAttributeName() == null ? ""
								: arMgProductsList.get(i).getAttributeName());
				objFileWriter.append(",");
				objFileWriter.append(arMgProductsList.get(i)
						.getProdDisplayValue() == null ? "" : arMgProductsList
						.get(i).getProdDisplayValue());
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getDiscardReason() == null ? ""
								: arMgProductsList.get(i).getDiscardReason());
				objFileWriter.append("\n");
			}
			objFileWriter.flush();
			objFileWriter.close();
		} catch (IOException e) {
			LOG.info("Inside Utility : writeDiscardedAttributesDataToCSV : "
					+ e);
		}
	}

	/**
	 * This method is used for Writing Discarded Products Data to CSV file.
	 * 
	 * @param arMgProductsList
	 * @param strFileName
	 */
	public static void writeDiscardedProductsDataToCSV(
			List<ManageProducts> arMgProductsList, String strFileName) {
		LOG.info("Inside Utility : writeDiscardedProductsDataToCSV");
		File objFile = new File(strFileName);
		FileWriter objFileWriter = null;
		try {
			if (!objFile.exists()) {
				objFile.createNewFile();
			}
			objFileWriter = new FileWriter(strFileName);
			objFileWriter.append("Product Name");
			objFileWriter.append(",");
			objFileWriter.append("Scan Code");
			objFileWriter.append(",");
			objFileWriter.append("Scan Type");
			objFileWriter.append(",");
			objFileWriter.append("Product Expire date");
			objFileWriter.append(",");
			objFileWriter.append("Product Image Path");
			objFileWriter.append(",");
			objFileWriter.append("Category");
			objFileWriter.append(",");
			objFileWriter.append("Short Description");
			objFileWriter.append(",");
			objFileWriter.append("Long Description");
			objFileWriter.append(",");
			objFileWriter.append("Model Number");
			objFileWriter.append(",");
			objFileWriter.append("Suggested Retail Price");
			objFileWriter.append(",");
			objFileWriter.append("Weight");
			objFileWriter.append(",");
			objFileWriter.append("Weight Units");
			objFileWriter.append(",");
			objFileWriter.append("Manufacture product URL");
			objFileWriter.append(",");
			objFileWriter.append("Audio Path");
			objFileWriter.append(",");
			objFileWriter.append("Video Path");
			objFileWriter.append(",");
			objFileWriter.append("Image Path");
			objFileWriter.append(",");
			objFileWriter.append("ReasonForDiscarding");
			objFileWriter.append("\n");
			for (int i = 0; i < arMgProductsList.size(); i++) {
				objFileWriter
						.append(arMgProductsList.get(i).getProductName() == null ? ""
								: StringEscapeUtils.escapeCsv(arMgProductsList
										.get(i).getProductName()));
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getScanCode() == null ? ""
								: arMgProductsList.get(i).getScanCode());
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getScanType() == null ? ""
								: arMgProductsList.get(i).getScanType());
				objFileWriter.append(",");
				objFileWriter.append(arMgProductsList.get(i)
						.getProdExpirationDate() == null ? ""
						: arMgProductsList.get(i).getProdExpirationDate());
				objFileWriter.append(",");
				objFileWriter.append(arMgProductsList.get(i)
						.getProductImagePath() == null ? "" : arMgProductsList
						.get(i).getProductImagePath());
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getCategoryName() == null ? ""
								: arMgProductsList.get(i).getCategoryName());
				objFileWriter.append(",");
				objFileWriter.append(arMgProductsList.get(i)
						.getShortDescription() == null ? "" : StringEscapeUtils
						.escapeCsv(arMgProductsList.get(i)
								.getShortDescription()));
				objFileWriter.append(",");
				objFileWriter.append(arMgProductsList.get(i)
						.getLongDescription() == null ? ""
						: StringEscapeUtils.escapeCsv(arMgProductsList.get(i)
								.getLongDescription()));
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getModelNumber() == null ? ""
								: arMgProductsList.get(i).getModelNumber());
				objFileWriter.append(",");
				objFileWriter
						.append((""
								+ arMgProductsList.get(i)
										.getStrSuggestedRetailPrice() == null ? ""
								: arMgProductsList.get(i)
										.getStrSuggestedRetailPrice() + ""));
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getStrweight() == null ? ""
								: arMgProductsList.get(i).getStrweight());
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getWeightUnits() == null ? ""
								: arMgProductsList.get(i).getWeightUnits());
				objFileWriter.append(",");
				objFileWriter.append(arMgProductsList.get(i)
						.getManufProuductURL() == null ? "" : arMgProductsList
						.get(i).getManufProuductURL());
				objFileWriter.append(",");
				objFileWriter.append(arMgProductsList.get(i)
						.getAudioMediaPath() == null ? "" : arMgProductsList
						.get(i).getAudioMediaPath());
				objFileWriter.append(",");
				objFileWriter.append(arMgProductsList.get(i)
						.getVideoMediaPath() == null ? "" : arMgProductsList
						.get(i).getVideoMediaPath());
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getImagePath() == null ? ""
								: arMgProductsList.get(i).getImagePath());
				objFileWriter.append(",");
				objFileWriter
						.append(arMgProductsList.get(i).getDiscardReason() == null ? ""
								: arMgProductsList.get(i).getDiscardReason());
				objFileWriter.append("\n");
			}
			objFileWriter.flush();
			objFileWriter.close();
		} catch (IOException e) {
			LOG.info("Inside Utility : writeDiscardedProductsDataToCSV : " + e);
		}
	}

	public static int getArrayLength(CommonsMultipartFile[] imageFile) {
		int length = 0;
		for (int i = 0; i < imageFile.length; i++) {
			if (!imageFile[i].isEmpty()) {
				length++;
			}
		}
		return length;

	}

	public static String convertDBdateTime(String dbdate) {
		String javaDate = null;
		try {
			LOG.info("dbdate " + dbdate);
			final String methodName = "convertDBdate";
			LOG.info(ApplicationConstants.METHODSTART + methodName);
			SimpleDateFormat formatter, FORMATTER;
			formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			// formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = formatter.parse(dbdate);
			FORMATTER = new SimpleDateFormat("MM-dd-yyyy HH:mm");
			javaDate = FORMATTER.format(date);
		} catch (ParseException exception) {
			LOG.info("Exception in convertDBdate method"
					+ exception.getMessage());
			return null;
		}
		return javaDate;
	}

	/*
	 * Meyy related code
	 */

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static Result<Transaction> authenticateCreditCard(
			String[] strCardArr, String totalAmount, Customer customer) {
		Result<Transaction> result = null;
		if (null != strCardArr && strCardArr.length == 10) {

			Date myDate = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyyMMddHHmmssSS");
			String invoice = dateFormat.format(myDate);
			// String apiLoginID = "4dp7R7Lmnp";
			// String transactionKey = "94F5hDba89hM63V9";
			/*
			 * Test Environment use
			 */
			// Merchant merchant =
			// Merchant.createMerchant(Environment.SANDBOX,AUTHORIZE_API_LOGIN_ID,
			// TRANSACTION_KEY);

			/*
			 * Production Environment use
			 */
			Merchant merchant = Merchant.createMerchant(Environment.PRODUCTION,
					AUTHORIZE_API_LOGIN_ID, TRANSACTION_KEY);

			// create credit card
			CreditCard creditCard = CreditCard.createCreditCard();
			creditCard.setCreditCardNumber(strCardArr[0]);
			creditCard.setExpirationMonth(strCardArr[1]);
			creditCard.setExpirationYear(strCardArr[2]);
			creditCard.setCardCodeVerification(strCardArr[3]);

			// Create Order
			Order order = Order.createOrder();
			order.setInvoiceNumber(invoice);

			// Create EmailReceipt
			EmailReceipt emailReceipt = EmailReceipt.createEmailReceipt();
			emailReceipt.setEmail(customer.getEmail());
			emailReceipt.setEmailCustomer(true);
			emailReceipt.setMerchantEmail("support@scansee.com");

			// create transaction
			Transaction authCaptureTransaction = merchant.createAIMTransaction(
					TransactionType.AUTH_CAPTURE, new BigDecimal(totalAmount));
			authCaptureTransaction.setCreditCard(creditCard);
			authCaptureTransaction.setOrder(order);
			authCaptureTransaction.setEmailReceipt(emailReceipt);
			authCaptureTransaction.setCustomer(customer);

			result = (Result<Transaction>) merchant
					.postTransaction(authCaptureTransaction);

			/*
			 * if (result.isApproved()) { LOG.info("Approved!</br>");
			 * LOG.info("Transaction Id: " +
			 * result.getTarget().getTransactionId());
			 * LOG.info("Response Text: " + result.getResponseText());
			 * LOG.info("getReasonResponseCode: " +
			 * result.getReasonResponseCode()); LOG.info("ResponseCode: " +
			 * result.getResponseCode()); LOG.info("Targetgggggggggggg: " +
			 * result.getTarget().toNVPString()); LOG.info("Invoice Number :" +
			 * result.getTarget().getOrder().getInvoiceNumber()); } else if
			 * (result.isDeclined()) { LOG.info("Declined.</br>");
			 * LOG.info(result.getReasonResponseCode() + " : " +
			 * result.getResponseText()); } else { LOG.info("Error.</br>");
			 * LOG.info(result.getReasonResponseCode() + " : " +
			 * result.getResponseText()); }
			 */
		}
		return result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public static net.authorize.arb.Result<Transaction> authenticateARBCreditCard(
			String[] strCardArr, String totalAmount, Customer customer,
			String paymentType) {
		if (isEmptyOrNullString(paymentType)) {
			paymentType = MONTHLY;
		}
		net.authorize.arb.Result<Transaction> result = null;
		final Date date = new Date();
		if (null != strCardArr && strCardArr.length == 11) {

			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyyMMddHHmmssSS");
			String invoice = dateFormat.format(date);
			/*
			 * String apiLoginID = "4dp7R7Lmnp"; String transactionKey =
			 * "94F5hDba89hM63V9";
			 *//*
				 * Test Environment use
				 */
			// Merchant merchant =
			// Merchant.createMerchant(Environment.SANDBOX,AUTHORIZE_API_LOGIN_ID,
			// TRANSACTION_KEY);

			/*
			 * Production Environment use
			 */
			Merchant merchant = Merchant.createMerchant(Environment.PRODUCTION,
					AUTHORIZE_API_LOGIN_ID, TRANSACTION_KEY);

			// create credit card
			CreditCard creditCard = CreditCard.createCreditCard();
			creditCard.setCreditCardNumber(strCardArr[0]);
			creditCard.setExpirationMonth(strCardArr[1]);
			creditCard.setExpirationYear(strCardArr[2]);
			creditCard.setCardCodeVerification(strCardArr[3]);

			// Create Order
			Order order = Order.createOrder();
			order.setInvoiceNumber(invoice);

			// Create EmailReceipt
			/*
			 * EmailReceipt emailReceipt = EmailReceipt.createEmailReceipt();
			 * emailReceipt.setEmail(customer.getEmail());
			 * emailReceipt.setEmailCustomer(true);
			 * emailReceipt.setMerchantEmail("support@scansee.com");
			 */

			// create transaction

			Subscription subscription = Subscription.createSubscription();

			// Payment Schedule
			PaymentSchedule schedule = PaymentSchedule.createPaymentSchedule();
			LOG.info("Recurring Billing Payment Type : " + paymentType);
			if (paymentType.equalsIgnoreCase(MONTHLY)) {
				schedule.setIntervalLength(1);
			} else {
				schedule.setIntervalLength(12);
			}

			LOG.info("Recurring Billing Start Date : " + date.toString());
			schedule.setStartDate(date);
			schedule.setSubscriptionUnit(SubscriptionUnitType.MONTHS);
			schedule.setTotalOccurrences(9999);

			// Customer
			net.authorize.data.xml.Customer customer1 = net.authorize.data.xml.Customer
					.createCustomer();
			customer1.setCustomerType(CustomerType.BUSINESS);
			customer1.setDriversLicenseSpecified(false);
			customer1.setEmail(customer.getEmail());
			customer1.setFaxNumber(customer.getFax());
			customer1.setPhoneNumber(customer.getPhone());
			customer1.setId(customer.getCustomerId());
			Address bill_to = Address.createAddress();
			bill_to.setAddress(customer.getAddress());
			bill_to.setCity(customer.getCity());
			bill_to.setCompany(customer.getCompany());
			bill_to.setCountry(customer.getCountry());
			bill_to.setFaxNumber(customer.getFax());
			bill_to.setFirstName(customer.getFirstName());
			bill_to.setLastName(customer.getLastName());
			bill_to.setPhoneNumber(customer.getPhone());
			bill_to.setState(customer.getState());
			bill_to.setZipPostalCode(customer.getZipPostalCode());
			customer1.setBillTo(bill_to);

			// Payment
			Payment payment = Payment.createPayment(creditCard);

			subscription.setPayment(payment);
			subscription.setSchedule(schedule);
			subscription.setCustomer(customer1);
			subscription.setAmount(new BigDecimal(totalAmount));
			subscription.setTrialAmount(new BigDecimal(0.00));
			subscription.setOrder(order);

			net.authorize.arb.Transaction authCaptureTransaction = merchant
					.createARBTransaction(
							net.authorize.arb.TransactionType.CREATE_SUBSCRIPTION,
							subscription);

			result = (net.authorize.arb.Result<Transaction>) merchant
					.postTransaction(authCaptureTransaction);
			LOG.info("Credit Card Processing Result :" + result);
			if (null != result) {
				LOG.info("Credit Card Processing Result OK?:" + result.isOk());
				LOG.info("Credit Card Processing Result Error?:"
						+ result.isError());
				LOG.info("Credit Card Processing Result Subscription Status:"
						+ result.getSubscriptionStatus());
			}

		}
		return result;
	}

	public static String calculateTotalWithTax(String totalPrice) {
		String strTaxAmount = "0.00";
		double taxAmount = 0.00;
		if (!Utility.isEmptyOrNullString(totalPrice)
				&& Utility.isNumber(totalPrice)) {
			taxAmount = ((Double.parseDouble(totalPrice) * 8.25) / 100);
			strTaxAmount = String.format("%.2f", taxAmount);
		}

		return strTaxAmount;

	}

	public static String validateCaptcha(String challengeField,
			String responseField, String remoteAddr, String reCaptchaPrivateKey)
			throws ScanSeeServiceException {
		final String methodName = "convertDBdate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
		reCaptcha.setPrivateKey(reCaptchaPrivateKey);
		ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr,
				challengeField, responseField);

		if (!reCaptchaResponse.isValid()) {
			// model.addAttribute("invalidCaptcha", "Captcha Is Invalid");
			return ApplicationConstants.FAILURE;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return ApplicationConstants.SUCCESS;
	}

	public static RetailerInfo getUploadedProductsList(
			CommonsMultipartFile locationFile) throws ScanSeeServiceException {
		RetailProduct retailerProd = null;
		ArrayList<RetailProduct> inValidProdList = new ArrayList<RetailProduct>();
		ArrayList<RetailProduct> validProdList = new ArrayList<RetailProduct>();
		BufferedReader br;
		RetailerInfo produdtList = new RetailerInfo();
		try {

			br = new BufferedReader(new InputStreamReader(
					locationFile.getInputStream()));
			CSVReader reader = new CSVReader(br);
			String[] nextLine;
			boolean validationFlg;
			boolean startDateValidationFlg;
			boolean endDateValidationFlg;
			String compStartDate = "";
			String compEndDate = "";
			String compDate = null;
			StringBuffer errorMsg = null;
			Date currentDate = new Date();
			while ((nextLine = reader.readNext()) != null) {

				startDateValidationFlg = false;
				endDateValidationFlg = false;
				validationFlg = false;
				compStartDate = "";
				compEndDate = "";
				// Exclude header and read
				if (!nextLine[1].equals("")) {
					if (nextLine.length == 7) {
						retailerProd = new RetailProduct();
						retailerProd.setStoreIdentificationID(nextLine[0]);
						retailerProd.setScanCode(nextLine[1]);
						retailerProd.setLongDescription(nextLine[2]);
						retailerProd.setProductPrice(nextLine[3]);
						retailerProd.setProductSalePrice(nextLine[4]);
						retailerProd.setSaleStartDate(nextLine[5]);
						retailerProd.setSaleEndDate(nextLine[6]);
						errorMsg = new StringBuffer();
						String storeId = nextLine[0];
						if (null == storeId || "".equals(storeId)) {
							validationFlg = true;
							errorMsg.append("Store number cannot be null or Empty,");

						}
						String scanCode = nextLine[1];
						if (null == scanCode || "".equals(scanCode)) {
							validationFlg = true;
							errorMsg.append("ScanCode cannot be null or Empty,");

						}

						retailerProd.setLongDescription(nextLine[2]);
						String prodPrice = nextLine[3];
						if (null == prodPrice || "".equals(prodPrice)) {
							validationFlg = true;
							errorMsg.append("Product Price cannot be null or Empty,");

						} else {

							try {
								Float.parseFloat(prodPrice);
							} catch (NumberFormatException exception) {
								validationFlg = true;
								errorMsg.append("Product Price should be a number,");

							}
						}

						String prodSalePrice = nextLine[4];
						String stardDate = nextLine[5];
						String endDate = nextLine[6];

						if (!(null == prodSalePrice || "".equals(prodSalePrice))) {
							try {
								Float.parseFloat(prodPrice);
							} catch (NumberFormatException exception) {
								validationFlg = true;
								errorMsg.append("Product Sale Price should be a number");

							}
						}

						if (!(null == stardDate || "".equals(stardDate))) {
							try {
								DateFormat oldFormatter = new SimpleDateFormat(
										"MM/dd/yyyy");
								oldFormatter.parse(stardDate);
							} catch (ParseException exception) {

								startDateValidationFlg = true;
								validationFlg = true;
								errorMsg.append("End date should be a date,");

							}
						}
						if (!(null == endDate || "".equals(endDate))) {
							try {
								DateFormat oldFormatter = new SimpleDateFormat(
										"MM/dd/yyyy");
								oldFormatter.parse(endDate);
							} catch (ParseException exception) {

								endDateValidationFlg = true;
								validationFlg = true;
								errorMsg.append("End date should be a date,");

							}

						}

						if (!startDateValidationFlg) {
							if (!(null == stardDate || "".equals(stardDate))) {
								compStartDate = Utility.compareCurrentDate(
										stardDate, currentDate);
								if (null != compStartDate) {

									validationFlg = true;
									errorMsg.append("Start date should be greater than or equal to current date,");

								}

							}

						}

						if (!endDateValidationFlg) {

							if (!(null == endDate || "".equals(endDate))) {
								compEndDate = Utility.compareCurrentDate(
										endDate, currentDate);

								if (null != compEndDate) {

									validationFlg = true;
									errorMsg.append("End date should be greater than or equal to current date,");

								}

							}

						}
						if (null == compStartDate && null == compEndDate) {

							compDate = Utility.compareDate(stardDate, endDate);
							if (null != compDate) {
								validationFlg = true;
								errorMsg.append("End date should be greater than or equal to Start date,");

							}
						}

						if (!validationFlg) {

							if (!(null == prodSalePrice || ""
									.equals(prodSalePrice))) {
								if ((null == stardDate || "".equals(stardDate))
										&& (null == endDate || ""
												.equals(endDate))) {
									validationFlg = true;
									errorMsg.append("Please Enter Start Date and End Date,");
								} else if (null == stardDate
										|| "".equals(stardDate)) {
									validationFlg = true;
									errorMsg.append("Please Enter Start Date,");

								} else if (null == endDate
										|| "".equals(endDate)) {
									validationFlg = true;
									errorMsg.append("Please Enter End Date,");
								}

							} else if (!(null == stardDate || ""
									.equals(stardDate))
									|| !(null == endDate || "".equals(endDate))) {

								if (null == prodSalePrice
										|| "".equals(prodSalePrice)) {
									validationFlg = true;
									errorMsg.append("Please Enter Sale Price,");

								}
							}

						}

						if (validationFlg) {
							// add product to Error product arrayList
							retailerProd.setErrorMessage(errorMsg.toString());
							inValidProdList.add(retailerProd);
						} else {
							// add product valid product list
							validProdList.add(retailerProd);

						}

					} else {

					}

				}
			}

			if (inValidProdList != null && inValidProdList.size() > 0) {
				inValidProdList.remove(0);
			}

			produdtList.setInValidProdlist(inValidProdList);
			produdtList.setValidProdlist(validProdList);
			/* Remove header instruction names from *.csv file */
			// retailerLocationsProd.remove(0);
		} catch (IOException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);
		} catch (Exception exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return produdtList;
	}

	/**
	 * The method for writing Store & Offer nodes data into the excel file.
	 * 
	 * @param whaleSharOfferList
	 *            as request parameter.
	 * @param whaleSharkOfferList
	 *            as request parameter.
	 * @throws IOException
	 */
	public static void writeRejectedRecordsToCSV(
			ArrayList<RetailProduct> retailerProdList, String filename) {
		File f = new File(filename);
		FileWriter writer;
		try {

			if (!f.exists()) {
				f.createNewFile();
			}
			writer = new FileWriter(filename);
			writer.append("Store Id");
			writer.append(',');
			writer.append("Product UPC");
			writer.append(',');
			writer.append("Product Description");
			writer.append(',');
			writer.append("Regular Price");
			writer.append(',');
			writer.append("Sale Price");
			writer.append(',');
			writer.append("Start Date");
			writer.append(',');
			writer.append("End Date");
			writer.append(',');
			writer.append("Reason for Discarding");
			writer.append('\n');
			for (int i = 0; i < retailerProdList.size(); i++) {
				// LOG.info(".......After for loop : Stores");
				writer.append(retailerProdList.get(i)
						.getStoreIdentificationID() == null ? ""
						: retailerProdList.get(i).getStoreIdentificationID());
				writer.append(',');
				writer.append(retailerProdList.get(i).getScanCode() == null ? ""
						: StringEscapeUtils.escapeCsv(retailerProdList.get(i)
								.getScanCode()));
				writer.append(',');
				writer.append(retailerProdList.get(i).getLongDescription() == null ? ""
						: retailerProdList.get(i).getLongDescription());
				writer.append(',');
				writer.append(String.valueOf(retailerProdList.get(i)
						.getProductPrice()) == null ? "" : String
						.valueOf(retailerProdList.get(i).getProductPrice()));
				writer.append(',');
				writer.append(String.valueOf(retailerProdList.get(i)
						.getProductSalePrice()) == null ? "" : String
						.valueOf(retailerProdList.get(i).getProductSalePrice()));
				writer.append(',');
				writer.append(retailerProdList.get(i).getSaleStartDate() == null ? ""
						: retailerProdList.get(i).getSaleStartDate());
				writer.append(',');
				writer.append(retailerProdList.get(i).getSaleEndDate() == null ? ""
						: retailerProdList.get(i).getSaleEndDate());
				writer.append(',');
				writer.append(retailerProdList.get(i).getErrorMessage() == null ? ""
						: retailerProdList.get(i).getErrorMessage());
				writer.append('\n');
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String checkNullString(String strValue) {
		if (strValue == null || strValue.equals("null") || "".equals(strValue)) {
			return "";
		} else {
			return strValue.trim();
		}
	}

	public static boolean validMinDimension(int height, int width,
			InputStream image) throws IOException {
		final String methodName = "validDimension";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		BufferedImage img = ImageIO.read(image);
		int w = img.getWidth(null);
		int h = img.getHeight(null);
		LOG.info("Image Width*********" + w);
		LOG.info("Image Hieght*********" + h);

		if (w > width || h > height) {
			return true;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return false;
	}

	public static boolean validImageDimension(int height, int width,
			InputStream image) throws IOException {
		final String methodName = "validDimension";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		BufferedImage img = ImageIO.read(image);
		int w = img.getWidth(null);
		int h = img.getHeight(null);
		LOG.info("Image Width*********" + w);
		LOG.info("Image Hieght*********" + h);
		if (w == width && h == height) {
			return true;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return false;
	}

	public static BufferedImage getBufferedImageForMinDimension(
			InputStream inputStream, String realPath, String fileName,
			String flowpath) throws IOException {

		LOG.info("getBufferedImageForMinDimension ********** realPath : "
				+ realPath);
		BufferedImage image = null;

		if (null != inputStream && !isEmptyOrNullString(fileName)
				&& !isEmptyOrNullString(realPath)) {
			/*
			 * File whiteBackground = new
			 * File(realPath+"/White_BackGround.png"); int imposeImgpixel = 120;
			 * if(!isEmptyOrNullString(flowpath)) {
			 * if(flowpath.equalsIgnoreCase("WelcomePage") ||
			 * flowpath.equalsIgnoreCase("Banner")) { whiteBackground = new
			 * File(realPath+"/White_BackGround_WelcomePage.png");
			 * imposeImgpixel = 250; } }
			 */

			BufferedImage small = ImageIO.read(inputStream);
			int hPixel = small.getHeight();
			int wPixel = small.getWidth();

			int imposeImgWpixel = 430;
			int imposeImgHpixel = 455;
			int imageType;
			File whiteBackground = new File(realPath + "/White_BackGround.png");
			BufferedImage img1 = ImageIO.read(whiteBackground);

			// some time image type was returning 0.

			if (img1.getType() == 0) {
				imageType = 5;
			} else {
				imageType = img1.getType();
			}
			// imageType = img1.getType();
			BufferedImage large = resizeImage(img1, imageType, wPixel + 1000,
					hPixel + 1000);
			// ImageIO.write(resizeImagePng, "png", new
			// File(realPath+"/White_BackGround.png"));

			// File updateWhiteBackground = new
			// File(realPath+"/White_BackGround.png");
			// BufferedImage large = ImageIO.read(updateWhiteBackground);
			// BufferedImage small = ImageIO.read(inputStream);
			int w = large.getWidth();
			int h = large.getHeight();
			int type = BufferedImage.TYPE_INT_RGB;
			image = new BufferedImage(w, h, type);
			Graphics2D g2 = image.createGraphics();
			g2.drawImage(large, 0, 0, null);
			g2.drawImage(small, imposeImgWpixel, imposeImgHpixel, null);
			g2.dispose();
			ImageIO.write(image, "png", new File(fileName));
		}
		return image;
	}

	/**
	 * @param retailerLocationAds
	 */
	public static void setAdStartAndEndDate(
			RetailerLocationAdvertisement retailerLocationAds) {
		if (null != retailerLocationAds) {
			if (retailerLocationAds.isIndefiniteAdDurationFlag()) {
				// DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				// Date date = new Date();
				// retailerLocationAds.setAdvertisementDate(dateFormat.format(date));
				retailerLocationAds
						.setAdvertisementEndDate(ApplicationConstants.AD_END_DATE);
				LOG.info("Inside Utility :  setAdStartAndEndDate Method : retailerLocationAds Start date : "
						+ retailerLocationAds.getAdvertisementDate());
				LOG.info("Inside Utility :  setAdStartAndEndDate Method : retailerLocationAds End date : "
						+ retailerLocationAds.getAdvertisementEndDate());
			}
		}
	}

	/**
	 * @param retailerLocationAds
	 */
	public static void setCustomPageStartAndEndDate(
			RetailerCustomPage retailerCustomPage) {
		if (null != retailerCustomPage) {
			if (retailerCustomPage.isIndefiniteAdDurationFlag()) {
				// DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				// Date date = new Date();
				// retailerCustomPage.setRetCreatedPageStartDate(dateFormat.format(date));
				retailerCustomPage
						.setRetCreatedPageEndDate(ApplicationConstants.AD_END_DATE);
				LOG.info("Inside Utility :  setCustomPageStartAndEndDate Method : Start date : "
						+ retailerCustomPage.getRetCreatedPageStartDate());
				LOG.info("Inside Utility :  setCustomPageStartAndEndDate Method : End date : "
						+ retailerCustomPage.getRetCreatedPageEndDate());
			}
		}
	}

	public static GAddress geocode(String address) throws Exception {
		LOG.info("Inside Utility : geocode");
		URL url = new URL(URL + "&q=" + URLEncoder.encode(address, "UTF-8")
				+ "&key=" + DEFAULT_KEY);
		URLConnection conn = url.openConnection();
		ByteArrayOutputStream output = new ByteArrayOutputStream(1024);
		IOUtils.copy(conn.getInputStream(), output);
		output.close();

		JSONObject json = new JSONObject(output.toString());
		GAddress gaddr = new GAddress();
		try {
			int code = json.getJSONObject("Status").getInt("code");
			if (code != 200) {
				return null;
			}
			JSONArray placemarks = json.getJSONArray("Placemark");
			JSONObject placemark = placemarks.getJSONObject(0);
			JSONArray coordinates = placemark.getJSONObject("Point")
					.getJSONArray("coordinates");
			gaddr.lng = coordinates.getDouble(0);
			gaddr.lat = coordinates.getDouble(1);

			JSONObject addressDetails = placemark
					.getJSONObject("AddressDetails");
			JSONObject country = addressDetails.getJSONObject("Country");
			gaddr.country = country.getString("CountryName");
			gaddr.countryCode = country.getString("CountryNameCode");

			JSONObject adminArea = country.getJSONObject("AdministrativeArea");
			gaddr.state = adminArea.getString("AdministrativeAreaName");

			JSONObject subAdminArea;
			if (adminArea.has("SubAdministrativeArea")) {
				subAdminArea = adminArea.getJSONObject("SubAdministrativeArea");
			} else {
				subAdminArea = adminArea;
			}
			JSONObject locality = subAdminArea.getJSONObject("Locality");
			gaddr.city = locality.getString("LocalityName");
			gaddr.zipCode = locality.getJSONObject("PostalCode").getString(
					"PostalCodeNumber");
		} catch (Exception e) {
			LOG.info("Inside Utility : geocode :" + e.getMessage());
		}
		return gaddr;
	}

	public static ArrayList<RetailerLocationProductJson> jsonToProdInfoList(
			String jsonStr) {

		// final String methodName = "RetailerLocationProductJson";
		RetailerLocationProduct prodJson = new RetailerLocationProduct();
		try {
			ObjectMapper om = new ObjectMapper();
			prodJson = om.readValue(jsonStr.getBytes(),
					RetailerLocationProduct.class);
			// LOG.info("Size " + prodJson.getProductData().size());
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prodJson.getRetailerLocationProductJson();

	}

	public static String removeWhiteSpaces(String str) {

		if (null != str) {
			str = str.trim();
			return str;
		} else {
			return str;
		}
	}

	/*
	 * To compare previous and current auto generated key .
	 * 
	 * @param session instance of HttpSession.
	 * 
	 * @param strKey the value to be key value.
	 * 
	 * @return bStatus, true or false.
	 */
	public static boolean isKeySame(HttpSession session, String strKey) {
		LOG.info("Inside Utility : isKeySame");
		boolean bStatus = false;
		String key = null;
		String preKey = null;
		if (!"".equals(Utility.checkNull(session.getAttribute("autoGenKey")))) {
			preKey = (String) session.getAttribute("autoGenKey").toString();
		}
		if (!"".equals(Utility.checkNull(strKey))) {
			key = strKey;
		}
		if ((preKey == key) || (preKey != null && preKey.equals(key))) {
			bStatus = true;
		} else {
			bStatus = false;
		}
		return bStatus;
	}

	/**
	 * Registration Success mail .
	 * 
	 * @param objRetailerRegistration
	 * @param smtpHost
	 * @param smtpPort
	 * @throws ScanSeeServiceException
	 * @throws MessagingException
	 */
	public static String sendFreeAppRetailerRegMail(
			RetailerRegistration objRetailerRegistration, String smtpHost,
			String smtpPort, String strAdminEmailId, String strLogoImage,
			String appSiteEmailId) throws ScanSeeServiceException {
		LOG.info("Inside Utility : sendFreeAppRetailerRegMail ");
		String strSubject = "Free Site - "
				+ objRetailerRegistration.getRetailerName();
		String respone = null;
		String autogenpswd = objRetailerRegistration.getPassword();
		String userlogin = objRetailerRegistration.getUserName();
		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate.append("<body>\n");
		emailTemplate.append("<table width= \"100%\">");
		emailTemplate.append("<tr><td colspan=\"2\"><p>"
				+ "A free site has been setup." + "</p></td></tr>");
		emailTemplate.append("<br/>");
		emailTemplate.append("<tr><td width=\"5%\">Retail ID:" + "</td>");
		emailTemplate
				.append("<td width=\"95%\"><strong>"
						+ objRetailerRegistration.getRetailID()
						+ "</strong></td></tr>");

		emailTemplate.append("<tr><td width=\"10%\">Business Name:" + "</td>");
		emailTemplate.append("<td width=\"95%\"><strong>"
				+ objRetailerRegistration.getRetailerName()
				+ "</strong></td></tr>");

		emailTemplate.append("<tr><td width=\"5%\">UserName:" + "</td>");
		emailTemplate.append("<td width=\"95%\"><strong>" + userlogin
				+ "</strong></td></tr>");

		emailTemplate.append("<tr><td width=\"5%\">Password:" + "</td>");
		emailTemplate.append("<td width=\"95%\"><strong>" + autogenpswd
				+ "</strong></td></tr>");

		emailTemplate.append("<tr><td colspan=\"2\">&nbsp;" + "</td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\">" + "Sincerely,"
				+ "</td></tr>");
		emailTemplate.append("<tr><td colspan=\"2\">" + "ScanSee Support"
				+ "</td></tr><br/>");
		emailTemplate.append("<img src= " + strLogoImage
				+ " alt=\"scansee logo\"  border=\"0\"><br/>");
		emailTemplate.append("</table>");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");
		LOG.info("In Retailer HTML Body" + emailTemplate.toString());
		try {
			MailBean.mailingComponent(strAdminEmailId, appSiteEmailId,
					strSubject, emailTemplate.toString(), smtpHost, smtpPort);
			respone = ApplicationConstants.SUCCESS;
		} catch (MessagingException me) {
			LOG.info("Inside Utility : sendFreeAppRetailerRegMail : "
					+ me.getMessage());
		}
		return respone;
	}

	public static String randomUserName(int len) {

		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(autoGeneratedUserName.charAt(rnd
					.nextInt(autoGeneratedUserName.length())));
		return sb.toString();
	}

	/**
	 * this method is used to convert the long to integer.
	 * 
	 * @param input
	 *            which is a long value to be converted as integer
	 * @return Integer
	 */

	public static Integer longToInteger(Long input)
			throws ScanSeeServiceException {
		String methodName = "longToInteger";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String output = input.toString();
		Integer result = new Integer(output);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return result;
	}

	/**
	 * Default Constructor.
	 */
	private Utility() {

	}

	public static int getLowerLimit(Pagination pageSess, int pageNumber) {
		String methodName = "getLowerLimit";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int currentPage = 1;
		int lowerLimit = 0;
		if (Integer.valueOf(pageNumber) != 0) {
			currentPage = Integer.valueOf(pageNumber);
			final int number = Integer.valueOf(currentPage) - 1;
			final int pageSize = pageSess.getPageRange();
			lowerLimit = pageSize * Integer.valueOf(number);
		} else {
			lowerLimit = 0;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return lowerLimit;
	}

	public static StringBuilder getReportTempMediaPath() {
		String jbossPath = System.getenv(ApplicationConstants.JBOSS_HOME);
		String fileSeparator = System.getProperty("file.separator");
		StringBuilder mediaPathBuilder = new StringBuilder();
		mediaPathBuilder.append(jbossPath);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.STANDALONE);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.DEPLOYMENT);
		mediaPathBuilder.append(fileSeparator);
		/*
		 * mediaPathBuilder.append(ApplicationConstants.DEPLOY);
		 * mediaPathBuilder.append(fileSeparator);
		 */
		mediaPathBuilder.append(ApplicationConstants.ROOTWAR);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.IMAGES);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append("temp");
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append("ReportImages");
		File obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		return mediaPathBuilder;
	}

	public static Map<String, String> getQueryMap(String query) {
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			if (param.split("=").length >= 2) {
				String name = param.split("=")[0];
				String value = param.split("=")[1];
				map.put(name, value);

			}

		}
		return map;
	}

	public static boolean ValidateDOB(String dateToValidate, String dateFromat) {

		if (dateToValidate == null || dateToValidate.isEmpty()) {
			return true;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);

		try {
			Date date = sdf.parse(dateToValidate);
		} catch (ParseException e) {

			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * This controller method return sorted time(Start and end hrs).
	 * 
	 * @param unsortMap
	 *            instance of Map interface
	 * @return sortedMap map object.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map sortByComparator(Map unsortMap) {
		final List list = new LinkedList(unsortMap.entrySet());
		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) o1).getValue())
						.compareTo(((Map.Entry) o2).getValue());
			}
		});
		// put sorted list into map again
		final Map sortedMap = new LinkedHashMap();
		Map.Entry entry = null;
		for (final Iterator it = list.iterator(); it.hasNext();) {
			entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	public static StringBuilder getMediaLocationPath(String folderType, int fodlerID) {
		String jbossPath = System.getenv(ApplicationConstants.JBOSS_HOME);
		String fileSeparator = System.getProperty("file.separator");
		StringBuilder mediaPathBuilder = new StringBuilder();
		mediaPathBuilder.append(jbossPath);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.STANDALONE);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.DEPLOYMENT);
		mediaPathBuilder.append(fileSeparator);
		/*
		 * mediaPathBuilder.append(ApplicationConstants.DEPLOY);
		 * mediaPathBuilder.append(fileSeparator);
		 */
		mediaPathBuilder.append(ApplicationConstants.ROOTWAR);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.IMAGES);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(folderType);
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(fodlerID);
		File obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		
		mediaPathBuilder.append(fileSeparator);
		mediaPathBuilder.append(ApplicationConstants.LOCATIONLOGO);
		obj = new File(mediaPathBuilder.toString());
		if (!obj.exists()) {
			obj.mkdir();
		}
		return mediaPathBuilder;
	}
	
	
	
	
	public static String writeFileDataLocation(CommonsMultipartFile file,
			String destinationPath) throws ScanSeeServiceException {
		final String methoName = "writeFileData";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		InputStream inputStream = null;
		OutputStream outputStream = null;
		CommonsMultipartFile commonsMultipartFile = null;
		commonsMultipartFile = file;
		// byte[] buffer=file.getFileData();
		byte[] buffer = new byte[32];

		if (commonsMultipartFile.getSize() > 0) {
			// destinationPath=file.getFilePath();
			String outputPath = destinationPath;
			try {
				outputStream = new FileOutputStream(outputPath);
				inputStream = commonsMultipartFile.getInputStream();
				while (inputStream.read(buffer) != -1) {
					outputStream.write(buffer);
				}

			} catch (FileNotFoundException exception) {
				LOG.error("Exception Occurred in writeFileData {}",
						exception.getMessage());
				throw new ScanSeeServiceException(exception.getMessage(),
						exception);
			}
			catch (IOException exception) {
				LOG.error("Exception Occurred in writeFileData {}",
						exception.getMessage());
				throw new ScanSeeServiceException(exception.getMessage(),
						exception);
			}
			finally {
				if (outputStream != null) {
					try {
						outputStream.close();
					} catch (IOException exception) {
						LOG.error("Exception Occurred in writeFileData {}",
								exception.getMessage());
						throw new ScanSeeServiceException(
								exception.getMessage(), exception);
					}
				}
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException exception) {
						LOG.error("Exception Occurred in writeFileData {}",
								exception.getMessage());
						throw new ScanSeeServiceException(
								exception.getMessage(), exception);
					}
				}
			}
		}
		return "success";
	}
	
	
	/**
	 * The method to remove the special characters.
	 * 
	 * @param inPutXml
	 *            The input xml.
	 * @return the xml with special charcters removed.
	 */

	public static String removeSpecialChars(String inPutXml) {
		String strOutputXml = "";
		if (null != inPutXml) {
			strOutputXml = null;
			strOutputXml = inPutXml.replaceAll("&", new String(AMP));
			strOutputXml = strOutputXml.replaceAll(">", new String(GT));
			strOutputXml = strOutputXml.replaceAll("<", new String(LT));
			strOutputXml = strOutputXml.replaceAll("\"", new String(QUOT));
			strOutputXml = strOutputXml.replaceAll("'", new String(APOS));
		}
		return strOutputXml;
	}
	
	/**
	 * Method to check entered value is valid price or not.
	 *  
	 * @param price
	 * @return true or false
	 */
	public static Boolean isValidPrice(String price)	{
		String regex = "^[0-9]\\d*$";
		Boolean isValidPrice = true;
		if (null != price && !"".equals(price)) {
			final Pattern pattern = Pattern.compile(regex);
			Matcher matcher = null;
			String[] strArr = price.split("\\.");
			if (strArr.length == 0 || strArr.length > 2) {
				isValidPrice = false;
			} else {
				for (String str : strArr) {
					matcher = pattern.matcher(str);
					if (!matcher.matches()) {
						isValidPrice = false;
						break;
					}
				}
			}
		}
		return isValidPrice;
	}
	
}

package common.util;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.find.controller.ConsumerFindController;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import common.pojo.shopper.IpCityResponse;

/**
 * A simple Java client for the IpInfoDb API. <br>
 * License: Free for use in any way. <br>
 * <br>
 * This example is dependent on the following libraries:
 * 
 * <pre>
 * commons-logging-1.1.1.jar
 * httpclient-4.2.jar
 * httpcore-4.2.jar
 * jackson-annotations-2.1.0.jar
 * jackson-core-2.1.0.jar
 * jackson-databind-2.1.0.jar
 * </pre>
 * 
 * <br>
 * Created with IntelliJ IDEA. User: MosheElisha
 */
public class IpInfoDbClient
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(IpInfoDbClient.class);
	private static final ObjectMapper MAPPER = new ObjectMapper();
	static
	{
		// Add a handler to handle unknown properties (in case the API adds new
		// properties to the response)
		MAPPER.addHandler(new DeserializationProblemHandler() {
			@Override
			public boolean handleUnknownProperty(DeserializationContext context, JsonParser jp, JsonDeserializer<?> deserializer, Object beanOrClass,
					String propertyName) throws IOException
			{
				// Do not fail - just log
				String className = (beanOrClass instanceof Class) ? ((Class) beanOrClass).getName() : beanOrClass.getClass().getName();
				System.out.println("Unknown property while de-serializing: " + className + "." + propertyName);
				context.getParser().skipChildren();
				return true;
			}
		});
	}

	public static IpCityResponse getIpInfo(String requestIP) throws IOException
	{
		LOG.info("Inside getIpInfo method:");
		LOG.info("Requested IP:" + requestIP);
		String mode = "ip-city";
		String apiKey = "d473a8b24fcc8665aeb54af88b1ee45eb7e64bca8f6c8e19cb9a6125131a0f23";
		String ip = requestIP;
		String url = "http://api.ipinfodb.com/v3/" + mode + "/?format=json&key=" + apiKey;
		IpCityResponse ipCityResponse = null;
		url += "&ip=" + ip;
		/*
		 * if (args.length > 2) { String ip = args[2]; url += "&ip=" + ip; }
		 */
		HttpClient HTTP_CLIENT = new DefaultHttpClient();
		try
		{

			HttpGet request = new HttpGet(url);
			HttpResponse response = HTTP_CLIENT.execute(request, new BasicHttpContext());
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
			{
				LOG.error("API Error status message is '" + response.getStatusLine());
				throw new RuntimeException("IpInfoDb response is " + response.getStatusLine());
			}

			String responseBody = EntityUtils.toString(response.getEntity());
			if (response.getEntity() != null)
			{
				response.getEntity().consumeContent();
			}
			if (!responseBody.contains("ERROR;Invalid IP address."))
			{

				ipCityResponse = MAPPER.readValue(responseBody, IpCityResponse.class);
				if ("OK".equals(ipCityResponse.getStatusCode()))
				{
					LOG.info("API IP info:");
					LOG.info(ipCityResponse.getCountryCode() + ", " + ipCityResponse.getRegionName() + ", " + ipCityResponse.getCityName() + ","
							+ ipCityResponse.getZipCode() + "," + ipCityResponse.getLatitude() + "," + ipCityResponse.getLongitude());
				}
				else
				{
					LOG.info("API status message is '" + ipCityResponse.getStatusMessage() + "'");
				}
			}

		}
		finally
		{
			HTTP_CLIENT.getConnectionManager().shutdown();
		}
		LOG.info("Exit getIpInfo method:");
		return ipCityResponse;
	}

	/**
	 * <pre>
	 * Example request:
	 * http://api.ipinfodb.com/v3/ip-city/?format=json&key=API_KEY&ip=IP_ADDRESS
	 * 
	 * Example response:
	 * {
	 * 	"statusCode" : "OK",
	 * 	"statusMessage" : "",
	 * 	"ipAddress" : "74.125.45.100",
	 * 	"countryCode" : "US",
	 * 	"countryName" : "UNITED STATES",
	 * 	"regionName" : "CALIFORNIA",
	 * 	"cityName" : "MOUNTAIN VIEW",
	 * 	"zipCode" : "94043",
	 * 	"latitude" : "37.3861",
	 * 	"longitude" : "-122.084",
	 * 	"timeZone" : "-07:00"
	 * }
	 * </pre>
	 */

}

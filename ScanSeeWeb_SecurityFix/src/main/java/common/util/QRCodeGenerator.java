package common.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.QRCodeResponse;

/**
 * This Class is used to generate QR codes.
 * 
 * @author dileepa_cc
 */
public class QRCodeGenerator
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(QRCodeGenerator.class);

	/**
	 * This Method is used to generate QR code.
	 * 
	 * @param imageName
	 *            Name of the Image to be generated
	 * @param url
	 *            url to be encoded in the QR Code Image
	 * @return QR Code Image Path
	 * @throws ScanSeeServiceException if any exception while generating the qr code o saving the image.
	 */
	public static QRCodeResponse generateQRCode(String imageName, String url) throws ScanSeeServiceException
	{
		LOG.info("Inside the method generateQRCode ");
		final String fileSeparator = System.getProperty("file.separator");
		String imagePath = null;
		FileOutputStream fileoutput = null;
		File file = null;
		Writer writer = null;

		QRCodeResponse qrCodeResponse = new QRCodeResponse();
		try
		{
			// QRCode Image File Path
			imagePath = Utility.getQrStorePath() + fileSeparator + imageName + ApplicationConstants.PNGIMAGEFORMAT;
			file = new File(imagePath);
			fileoutput = new FileOutputStream(file);
			writer = new QRCodeWriter();
			BitMatrix bitMatrix = writer.encode(url, BarcodeFormat.QR_CODE, ApplicationConstants.QRCODEIMAGEWIDTH,
					ApplicationConstants.QRCODEIMAGEHEIGHT);
			MatrixToImageWriter.writeToStream(bitMatrix, ApplicationConstants.PNGIMAGE, fileoutput);
			LOG.info("QR Code images has been generated and saved successfully at {} ", imagePath);
			qrCodeResponse.setQrCodeImagePath(imagePath);
			qrCodeResponse.setResponse(ApplicationConstants.SUCCESS);
		}
		catch (FileNotFoundException e)
		{
			LOG.equals(ApplicationConstants.EXCEPTION_OCCURED + e);
			qrCodeResponse.setResponse(ApplicationConstants.FAILURE);
		}
		catch (WriterException e)
		{
			LOG.equals(ApplicationConstants.EXCEPTION_OCCURED + e);
			qrCodeResponse.setResponse(ApplicationConstants.FAILURE);
		}
		catch (IOException e)
		{
			LOG.equals(ApplicationConstants.EXCEPTION_OCCURED + e);
			qrCodeResponse.setResponse(ApplicationConstants.FAILURE);
		}

		LOG.info("Exit method generateQRCode");
		return qrCodeResponse;
	}

	/**
	 * default Constructor.
	 */
	private QRCodeGenerator()
	{

	}
	/*
	 * public static void main(String[] arg) throws ScanSeeServiceException {
	 * QRCodeGenerator.generateQRCode("MMM",
	 * "http://122.181.128.152:8080/Images/Video_ScanNow_031212.mp4"); }
	 */
}

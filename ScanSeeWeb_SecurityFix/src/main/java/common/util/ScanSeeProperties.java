package common.util;

import java.io.InputStream;
import java.util.Properties;

public class ScanSeeProperties
{
public static Properties	prop	= null;
	
	
	public void readProperties() 
	{
		prop	= null;
		if (prop == null) {			
			InputStream stream = this.getClass().getClassLoader().getResourceAsStream("application.properties");
			try {
				prop = new Properties();
				prop.load(stream);
			} catch (Exception ex) {
				prop	= null;
				ex.printStackTrace();
			}
		}
	}

	

	public static String getPropertyValue(String strPropertyName)
	{
		String strPropertyValue = "";
		try {
			strPropertyValue = ((String) prop.get(strPropertyName)).trim();
		}// end of try
		catch (Exception e) {
		}
		return strPropertyValue;
	}
}

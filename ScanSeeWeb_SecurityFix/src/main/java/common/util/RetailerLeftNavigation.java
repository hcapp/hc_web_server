package common.util;

public class RetailerLeftNavigation
{

	// Main module style values are stored in the below given variables.
	private String homeTabStyle;

	private String appSiteStyle;

	private String promotionStyle;

	private String analyticsStyle;

	private String manageLocStyle;

	private String manageProStyle;

	private String accountStyle;
	
	private String eventStyle;
	
	private String fundraiserStyle;

	// Sub module style values are stored in the below given variables.

	private String logoStyle = UtilCode.inactiveSubModStyle;

	private String welocomePageStyle = UtilCode.inactiveSubModStyle;

	private String bannerStyle = UtilCode.inactiveSubModStyle;

	private String anythingPageStyle = UtilCode.inactiveSubModStyle;

	private String splPageStyle = UtilCode.inactiveSubModStyle;

	private String couponStyle = UtilCode.inactiveSubModStyle;

	private String rebatesStyle = UtilCode.inactiveSubModStyle;

	private String dealsStyle = UtilCode.inactiveSubModStyle;

	private String addLocationStyle = UtilCode.inactiveSubModStyle;
	
	private String uploadLocationStyle = UtilCode.inactiveSubModStyle;
	
	private String manageLocationSubStyle = UtilCode.inactiveSubModStyle;

	
	private String giveAwaySubStyle = UtilCode.inactiveSubModStyle;
	/**
	 * @return the homeTabStyle
	 */
	public String getHomeTabStyle()
	{
		return homeTabStyle;
	}

	/**
	 * @param homeTabStyle
	 *            the homeTabStyle to set
	 */
	public void setHomeTabStyle(String homeTabStyle)
	{
		this.homeTabStyle = homeTabStyle;
	}

	/**
	 * @return the appSiteStyle
	 */
	public String getAppSiteStyle()
	{
		return appSiteStyle;
	}

	/**
	 * @param appSiteStyle
	 *            the appSiteStyle to set
	 */
	public void setAppSiteStyle(String appSiteStyle)
	{
		this.appSiteStyle = appSiteStyle;
	}

	/**
	 * @return the promotionStyle
	 */
	public String getPromotionStyle()
	{
		return promotionStyle;
	}

	/**
	 * @param promotionStyle
	 *            the promotionStyle to set
	 */
	public void setPromotionStyle(String promotionStyle)
	{
		this.promotionStyle = promotionStyle;
	}

	/**
	 * @return the analyticsStyle
	 */
	public String getAnalyticsStyle()
	{
		return analyticsStyle;
	}

	/**
	 * @param analyticsStyle
	 *            the analyticsStyle to set
	 */
	public void setAnalyticsStyle(String analyticsStyle)
	{
		this.analyticsStyle = analyticsStyle;
	}

	/**
	 * @return the manageLocStyle
	 */
	public String getManageLocStyle()
	{
		return manageLocStyle;
	}

	/**
	 * @param manageLocStyle
	 *            the manageLocStyle to set
	 */
	public void setManageLocStyle(String manageLocStyle)
	{
		this.manageLocStyle = manageLocStyle;
	}

	/**
	 * @return the manageProStyle
	 */
	public String getManageProStyle()
	{
		return manageProStyle;
	}

	/**
	 * @param manageProStyle
	 *            the manageProStyle to set
	 */
	public void setManageProStyle(String manageProStyle)
	{
		this.manageProStyle = manageProStyle;
	}

	/**
	 * @return the accountStyle
	 */
	public String getAccountStyle()
	{
		return accountStyle;
	}

	/**
	 * @param accountStyle
	 *            the accountStyle to set
	 */
	public void setAccountStyle(String accountStyle)
	{
		this.accountStyle = accountStyle;
	}

	/**
	 * @return the logoStyle
	 */
	public String getLogoStyle()
	{
		return logoStyle;
	}

	/**
	 * @param logoStyle
	 *            the logoStyle to set
	 */
	public void setLogoStyle(String logoStyle)
	{
		this.logoStyle = logoStyle;
	}

	/**
	 * @return the welocomePageStyle
	 */
	public String getWelocomePageStyle()
	{
		return welocomePageStyle;
	}

	/**
	 * @param welocomePageStyle
	 *            the welocomePageStyle to set
	 */
	public void setWelocomePageStyle(String welocomePageStyle)
	{
		this.welocomePageStyle = welocomePageStyle;
	}

	/**
	 * @return the bannerStyle
	 */
	public String getBannerStyle()
	{
		return bannerStyle;
	}

	/**
	 * @param bannerStyle
	 *            the bannerStyle to set
	 */
	public void setBannerStyle(String bannerStyle)
	{
		this.bannerStyle = bannerStyle;
	}

	/**
	 * @return the anythingPageStyle
	 */
	public String getAnythingPageStyle()
	{
		return anythingPageStyle;
	}

	/**
	 * @param anythingPageStyle
	 *            the anythingPageStyle to set
	 */
	public void setAnythingPageStyle(String anythingPageStyle)
	{
		this.anythingPageStyle = anythingPageStyle;
	}

	/**
	 * @return the splPageStyle
	 */
	public String getSplPageStyle()
	{
		return splPageStyle;
	}

	/**
	 * @param splPageStyle
	 *            the splPageStyle to set
	 */
	public void setSplPageStyle(String splPageStyle)
	{
		this.splPageStyle = splPageStyle;
	}

	/**
	 * @return the couponStyle
	 */
	public String getCouponStyle()
	{
		return couponStyle;
	}

	/**
	 * @param couponStyle
	 *            the couponStyle to set
	 */
	public void setCouponStyle(String couponStyle)
	{
		this.couponStyle = couponStyle;
	}

	/**
	 * @return the rebatesStyle
	 */
	public String getRebatesStyle()
	{
		return rebatesStyle;
	}

	/**
	 * @param rebatesStyle
	 *            the rebatesStyle to set
	 */
	public void setRebatesStyle(String rebatesStyle)
	{
		this.rebatesStyle = rebatesStyle;
	}

	/**
	 * @return the dealsStyle
	 */
	public String getDealsStyle()
	{
		return dealsStyle;
	}

	/**
	 * @param dealsStyle
	 *            the dealsStyle to set
	 */
	public void setDealsStyle(String dealsStyle)
	{
		this.dealsStyle = dealsStyle;
	}

	public String getAddLocationStyle()
	{
		return addLocationStyle;
	}

	public void setAddLocationStyle(String addLocationStyle)
	{
		this.addLocationStyle = addLocationStyle;
	}


	public String getUploadLocationStyle()
	{
		return uploadLocationStyle;
	}

	public void setUploadLocationStyle(String uploadLocationStyle)
	{
		this.uploadLocationStyle = uploadLocationStyle;
	}

	public String getManageLocationSubStyle()
	{
		return manageLocationSubStyle;
	}

	public void setManageLocationSubStyle(String manageLocationSubStyle)
	{
		this.manageLocationSubStyle = manageLocationSubStyle;
	}

	public String getGiveAwaySubStyle()
	{
		return giveAwaySubStyle;
	}

	public void setGiveAwaySubStyle(String giveAwaySubStyle)
	{
		this.giveAwaySubStyle = giveAwaySubStyle;
	}

	/**
	 * @return the eventStyle
	 */
	public String getEventStyle() {
		return eventStyle;
	}

	/**
	 * @param eventStyle the eventStyle to set
	 */
	public void setEventStyle(String eventStyle) {
		this.eventStyle = eventStyle;
	}
	
	public String getFundraiserStyle() {
		return fundraiserStyle;
	}

	public void setFundraiserStyle(String fundraiserStyle) {
		this.fundraiserStyle = fundraiserStyle;
	}

}

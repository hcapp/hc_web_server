package common.dashBoard;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.xml.rpc.holders.ByteArrayHolder;
import javax.xml.rpc.holders.StringHolder;
import javax.xml.soap.SOAPException;

import org.apache.axis.message.SOAPHeaderElement;
import org.htmlparser.Parser;
import org.htmlparser.Tag;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.SimpleNodeIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.report.ExecutionInfo;
import com.scansee.report.ParameterValue;
import com.scansee.report.ReportExecutionServiceLocator;
import com.scansee.report.ReportExecutionServiceSoapStub;
import com.scansee.report.holders.ArrayOfStringHolder;
import com.scansee.report.holders.ArrayOfWarningHolder;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.DashBoard;
import common.pojo.ReportConfigInfo;
import common.util.Utility;

public class ReportManager
{
	private static final Logger LOG = LoggerFactory.getLogger(ReportManager.class);

	final private String MODULE_SUPPLIER = "Supplier";

	final private String MODULE_RETAILER = "Retailer";

	private final String tempReportImagePath = "/Images/temp/ReportImages/";

	String format = "HTML4.0"; // Valid options are HTML4.0, MHTML, EXCEL, CSV,
								// PDF, etc
	String deviceInfo = "<DeviceInfo><Toolbar>False</Toolbar><HTMLFragment>True</HTMLFragment></DeviceInfo>"; // Only
																												// generate
																												// an
																												// HTML
																												// fragment

	/**
	 * Method to initialise supplier report parameters.
	 * 
	 * @param retailID
	 *            retailer id as a parameter.
	 * @param fromDate
	 *            From date as a parameter.
	 * @param toDate
	 *            To date as a parameter.
	 * @return parameters object
	 * @throws ScanSeeServiceException
	 */

	public static ParameterValue[] initializeParameters(String retailID, String fromDate, String toDate) throws ScanSeeServiceException
	{
		final String methodName = "initializeParameters";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ParameterValue[] parameters = new ParameterValue[3];
		parameters[0] = new ParameterValue();
		parameters[0].setName("RetailerID");
		parameters[0].setValue(retailID);
		parameters[1] = new ParameterValue();
		parameters[1].setName("From");
		parameters[1].setValue(fromDate);
		parameters[2] = new ParameterValue();
		parameters[2].setName("To");
		parameters[2].setValue(toDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return parameters;
	}

	/**
	 * Method to initialise supplier report parameters.
	 * 
	 * @param retailID
	 *            retailer id as a parameter.
	 * @param fromDate
	 *            From date as a parameter.
	 * @param toDate
	 *            To date as a parameter.
	 * @return parameters object
	 * @throws ScanSeeServiceException
	 */

	public static ParameterValue[] initializeSupplierParameters(String supplierId, String fromDate, String toDate) throws ScanSeeServiceException
	{
		final String methodName = "initializeSupplierParameters";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ParameterValue[] parameters = new ParameterValue[3];
		parameters[0] = new ParameterValue();
		parameters[0].setName("SupplierID");
		parameters[0].setValue(supplierId);
		parameters[1] = new ParameterValue();
		parameters[1].setName("From");
		parameters[1].setValue(fromDate);
		parameters[2] = new ParameterValue();
		parameters[2].setName("To");
		parameters[2].setValue(toDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return parameters;
	}

	/**
	 * Method to initialise retailer report parameters.
	 * 
	 * @param retailID
	 *            retailer id as a parameter.
	 * @param fromDate
	 *            From date as a parameter.
	 * @param toDate
	 *            To date as a parameter.
	 * @param moduleID
	 *            Module Id as a parameter
	 * @return parameters object
	 * @throws ScanSeeServiceException
	 */
	public static ParameterValue[] initializeParameters(String retailID, String fromDate, String toDate, String moduleID)
			throws ScanSeeServiceException
	{
		final String methodName = "initializeParameters";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ParameterValue[] parameters = new ParameterValue[4];
		parameters[0] = new ParameterValue();
		parameters[0].setName("RetailerID");
		parameters[0].setValue(retailID);
		parameters[1] = new ParameterValue();
		parameters[1].setName("From");
		parameters[1].setValue(fromDate);
		parameters[2] = new ParameterValue();
		parameters[2].setName("To");
		parameters[2].setValue(toDate);
		parameters[3] = new ParameterValue();
		parameters[3].setName("Module");
		parameters[3].setValue(moduleID);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return parameters;
	}

	/**
	 * Method to initialise retailer report parameters.
	 * 
	 * @param retailID
	 *            retailer id as a parameter.
	 * @param fromDate
	 *            From date as a parameter.
	 * @param toDate
	 *            To date as a parameter.
	 * @param moduleID
	 *            Module Id as a parameter
	 * @return parameters object
	 * @throws ScanSeeServiceException
	 */
	public static ParameterValue[] initializeSupplierParameters(String supplierID, String fromDate, String toDate, String moduleID)
			throws ScanSeeServiceException
	{
		final String methodName = "initializeSupplierParameters";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ParameterValue[] parameters = new ParameterValue[4];
		parameters[0] = new ParameterValue();
		parameters[0].setName("SupplierID");
		parameters[0].setValue(supplierID);
		parameters[1] = new ParameterValue();
		parameters[1].setName("From");
		parameters[1].setValue(fromDate);
		parameters[2] = new ParameterValue();
		parameters[2].setName("To");
		parameters[2].setValue(toDate);
		parameters[3] = new ParameterValue();
		parameters[3].setName("Module");
		parameters[3].setValue(moduleID);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return parameters;
	}

	/**
	 * Method to initialise supplier this location report parameters.
	 * 
	 * @param retailID
	 *            retailer id as a parameter.
	 * @param fromDate
	 *            From date as a parameter.
	 * @param toDate
	 *            To date as a parameter.
	 * @param moduleID
	 *            Module Id as a parameter
	 * @param ProductID
	 *            ProductID as a parameter
	 * @return parameters object
	 * @throws ScanSeeServiceException
	 */
	public static ParameterValue[] initializeSupplierThisLocParameters(String supplierID, String fromDate, String toDate, String moduleID,
			String ProductID)
	{
		final String methodName = "initializeSupplierThisLocParameters";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ParameterValue[] parameters = new ParameterValue[5];
		parameters[0] = new ParameterValue();
		parameters[0].setName("SupplierID");
		parameters[0].setValue(supplierID);
		parameters[1] = new ParameterValue();
		parameters[1].setName("From");
		parameters[1].setValue(fromDate);
		parameters[2] = new ParameterValue();
		parameters[2].setName("To");
		parameters[2].setValue(toDate);
		parameters[3] = new ParameterValue();
		parameters[3].setName("Module");
		parameters[3].setValue(moduleID);
		parameters[4] = new ParameterValue();
		parameters[4].setName("ProductID");
		parameters[4].setValue(ProductID);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return parameters;
	}

	public static void setExecutionId(ReportExecutionServiceSoapStub service, String id) throws SOAPException
	{
		final String methodName = "setExecutionId";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SOAPHeaderElement sessionHeader = new SOAPHeaderElement("http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices",
				"ExecutionHeader");
		sessionHeader.addChildElement("ExecutionID").addTextNode(id);
		service.setHeader(sessionHeader);
		LOG.info(ApplicationConstants.METHODEND + methodName);
	}

	public static ReportExecutionServiceSoapStub getService(ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{
		final String methodName = "ReportExecutionServiceSoapStub";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = null;
		try
		{
			String endpoint = reportConfigInfo.getReportURL();
			service = (ReportExecutionServiceSoapStub) new ReportExecutionServiceLocator(getEngineConfiguration())
					.getReportExecutionServiceSoap(new URL(endpoint));
			service.setUsername(reportConfigInfo.getReportServerUserName());
			service.setPassword(reportConfigInfo.getReortServerPassword());

		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in getService Method:" + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return service;
	}

	public static org.apache.axis.EngineConfiguration getEngineConfiguration()
	{

		final String methodName = "getEngineConfiguration";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		java.lang.StringBuffer sb = new java.lang.StringBuffer();

		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
		sb.append("<deployment name=\"defaultClientConfig\"\r\n");
		sb.append("xmlns=\"http://xml.apache.org/axis/wsdd/\"\r\n");
		sb.append("xmlns:java=\"http://xml.apache.org/axis/wsdd/providers/java\">\r\n");
		sb.append("<globalConfiguration>\r\n");
		sb.append("<parameter name=\"disablePrettyXML\" value=\"true\"/>\r\n");
		sb.append("<parameter name=\"enableNamespacePrefixOptimization\" value=\"true\"/>\r\n");
		sb.append("</globalConfiguration>\r\n");
		sb.append("<transport name=\"http\" pivot=\"java:org.apache.axis.transport.http.CommonsHTTPSender\"/>\r\n");
		sb.append("<transport name=\"local\" pivot=\"java:org.apache.axis.transport.local.LocalSender\"/>\r\n");
		sb.append("<transport name=\"java\" pivot=\"java:org.apache.axis.transport.java.JavaSender\"/>\r\n");
		sb.append("</deployment>\r\n");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new org.apache.axis.configuration.XMLStringProvider(sb.toString());
	}

	/**
	 * Method to render overview report report of Supplier/Retailer.
	 * 
	 * @param reportParams
	 *            as a parameter having report params.
	 * @param reportConfigInfo
	 *            as a parameter having report configuration info.
	 * @return HTML format of report content
	 * @throws ScanSeeServiceException
	 */
	public String getOverviewReport(DashBoard reportParams, ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{

		final String methodName = "getOverviewReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);
		String firstImagePath = null;
		String secondImagePath = null;
		String reportData = null;
		Date date = new Date();
		List<String> imgSource = new ArrayList<String>();
		try
		{
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportParams.getSsrsReportName(), null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing

			if (reportParams.getModuleName().equals(MODULE_RETAILER))
			{
				service.setExecutionParameters(initializeParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate()),
						"en-us"); // Set
				// report
				// parameters
			}
			else if (reportParams.getModuleName().equals(MODULE_SUPPLIER))
			{
				service.setExecutionParameters(
						initializeSupplierParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate()), "en-us"); // Set
				// report
				// parameters
			}

			ByteArrayHolder result = new ByteArrayHolder();
			ByteArrayHolder firstImageResult = new ByteArrayHolder();
			ByteArrayHolder secondImageresult = new ByteArrayHolder();
			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs); // Render
																											// report
			service.renderStream(format, streamIDs.value[0], deviceInfo, firstImageResult, encoding, mimeType);
			service.renderStream(format, streamIDs.value[1], deviceInfo, secondImageresult, encoding, mimeType);
			firstImagePath = Utility.getReportTempMediaPath() + "/" + streamIDs.value[0] + ".png";
			secondImagePath = Utility.getReportTempMediaPath() + "/" + streamIDs.value[1] + ".png";

			byte[] img = firstImageResult.value;

			BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(img));

			Utility.writeImage(bufferedImage, firstImagePath);

			img = secondImageresult.value;

			bufferedImage = ImageIO.read(new ByteArrayInputStream(img));

			Utility.writeImage(bufferedImage, secondImagePath);

			reportData = new String(result.value);
			reportData = reportData.replace("'", "\\'");
			Parser parser = new Parser();
			parser.setInputHTML(reportData);
			NodeList list = parser.parse(null);

			NodeList imgs = list.extractAllNodesThatMatch(new TagNameFilter("IMG"), true);

			for (SimpleNodeIterator iterator = imgs.elements(); iterator.hasMoreNodes();)
			{
				Tag tag = (Tag) iterator.nextNode();
				imgSource.add(tag.getAttribute("src"));
			}

			if (!imgSource.isEmpty())
			{

				if (imgSource.get(0) != null && !"".equals(imgSource.get(0)))
				{
					reportData = reportData.replace(imgSource.get(0), tempReportImagePath + streamIDs.value[0] + ".png" + "?" + date.getTime());
				}
				if (imgSource.get(1) != null && !"".equals(imgSource.get(1)))
				{
					reportData = reportData.replace(imgSource.get(1), tempReportImagePath + streamIDs.value[1] + ".png" + "?" + date.getTime());
				}
			}

		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in getOverviewReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return reportData; // Prints the report
		// HTML; this could
		// be embedded in a
		// JSP
	}

	/**
	 * Method to render demographic report report of Supplier/Retailer.
	 * 
	 * @param reportParams
	 *            as a parameter having report params.
	 * @param reportConfigInfo
	 *            as a parameter having report configuration info.
	 * @return HTML format of report content
	 * @throws ScanSeeServiceException
	 */
	public String getDemographicsReport(DashBoard reportParams, ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{

		final String methodName = "getDemographicsReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);
		String firstImagePath = null;
		String secondImagePath = null;
		String reportData = null;
		Date date = new Date();
		List<String> imgSource = new ArrayList<String>();
		try
		{
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportParams.getSsrsReportName(), null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing
			if (reportParams.getModuleName().equals(MODULE_RETAILER))
			{
				service.setExecutionParameters(
						initializeParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate(), reportParams.getModuleId()),
						"en-us"); // Set
				// report
				// parameters
			}
			else if (reportParams.getModuleName().equals(MODULE_SUPPLIER))
			{
				service.setExecutionParameters(
						initializeSupplierParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate(),
								reportParams.getModuleId()), "en-us"); // Set
																		// report
																		// parameters

			}

			ByteArrayHolder result = new ByteArrayHolder();
			ByteArrayHolder firstImageResult = new ByteArrayHolder();
			ByteArrayHolder secondImageresult = new ByteArrayHolder();
			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs); // Render
																											// report
			service.renderStream(format, streamIDs.value[0], deviceInfo, firstImageResult, encoding, mimeType);
			service.renderStream(format, streamIDs.value[1], deviceInfo, secondImageresult, encoding, mimeType);
			firstImagePath = Utility.getReportTempMediaPath() + "/" + streamIDs.value[0] + ".png";
			secondImagePath = Utility.getReportTempMediaPath() + "/" + streamIDs.value[1] + ".png";

			byte[] img = firstImageResult.value;

			BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(img));

			Utility.writeImage(bufferedImage, firstImagePath);

			img = secondImageresult.value;

			bufferedImage = ImageIO.read(new ByteArrayInputStream(img));

			Utility.writeImage(bufferedImage, secondImagePath);

			reportData = new String(result.value);
			reportData = reportData.replace("'", "\\'");
			Parser parser = new Parser();
			parser.setInputHTML(reportData);
			NodeList list = parser.parse(null);

			NodeList imgs = list.extractAllNodesThatMatch(new TagNameFilter("IMG"), true);

			for (SimpleNodeIterator iterator = imgs.elements(); iterator.hasMoreNodes();)
			{
				Tag tag = (Tag) iterator.nextNode();
				imgSource.add(tag.getAttribute("src"));
			}

			if (!imgSource.isEmpty())
			{

				if (imgSource.get(0) != null && !"".equals(imgSource.get(0)))
				{
					reportData = reportData.replace(imgSource.get(0), tempReportImagePath + streamIDs.value[0] + ".png" + "?" + date.getTime());
				}
				if (imgSource.get(1) != null && !"".equals(imgSource.get(1)))
				{
					reportData = reportData.replace(imgSource.get(1), tempReportImagePath + streamIDs.value[1] + ".png" + "?" + date.getTime());
				}
			}

		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in getDemographicsReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return reportData; // Prints the report HTML; this could be embedded in
							// a JSP

	}

	/**
	 * Method to render Retailer AppSite report .
	 * 
	 * @param reportParams
	 *            as a parameter having report params.
	 * @param reportConfigInfo
	 *            as a parameter having report configuration info.
	 * @return HTML format of report content
	 * @throws ScanSeeServiceException
	 */
	public String getRetailerAppSiteReport(DashBoard reportParams, ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{
		final String methodName = "getRetailerAppSiteReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);
		String reportData = null;
		try
		{
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportParams.getSsrsReportName(), null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing
			service.setExecutionParameters(initializeParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate()), "en-us"); // Set
			// report
			// parameters

			ByteArrayHolder result = new ByteArrayHolder();
			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs);
			reportData = new String(result.value);
			reportData = reportData.replace("'", "\\'");
		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in getRetailerAppSiteReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return reportData; // Prints the report
		// HTML; this could
		// be embedded in a
		// JSP

	}
	
	/**
	 * Method to render Retailer Coupon and Deals report .
	 * 
	 * @param reportParams
	 *            as a parameter having report params.
	 * @param reportConfigInfo
	 *            as a parameter having report configuration info.
	 * @return HTML format of report content
	 * @throws ScanSeeServiceException
	 */
	public String getRetailerCouponDealReport(DashBoard reportParams, ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{
		final String methodName = "getRetailerCouponDealReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);
		String reportData = null;
		try
		{
			//Retailer-CouponsHotDeals
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportParams.getSsrsReportName(), null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing
			service.setExecutionParameters(initializeParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate()), "en-us"); // Set
			// report
			// parameters

			ByteArrayHolder result = new ByteArrayHolder();
			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs);
			reportData = new String(result.value);
			reportData = reportData.replace("'", "\\'");
		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in getRetailerAppSiteReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return reportData; // Prints the report
		// HTML; this could
		// be embedded in a
		// JSP

	}

	/**
	 * Method to Export report as EXCEL/PDF .
	 * 
	 * @param reportName
	 *            as a parameter.
	 * @param fromDate
	 *            as a parameter.
	 * @param id
	 *            as a parameter.
	 * @param toDate
	 *            To date as a parameter.
	 * @param moduleName
	 *            as a parameter
	 * @param format
	 *            as a parameter
	 * @param reportConfigInfo
	 *            as a parameter
	 * @return Export file path
	 * @throws ScanSeeServiceException
	 */
	public String exportReport(String reportName, String id, String fromDate, String toDate, String format, String moduleName,
			ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{

		final String methodName = "exportReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);
		String exportfilePath = null;
		try
		{
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportName, null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing

			if (moduleName.equals(MODULE_RETAILER))
			{
				service.setExecutionParameters(initializeParameters(id, fromDate, toDate), "en-us"); // Set
				// report
				// parameters
			}
			else if (moduleName.equals(MODULE_SUPPLIER))
			{
				service.setExecutionParameters(initializeSupplierParameters(id, fromDate, toDate), "en-us"); // Set
				// report
				// parameters
			}

			ByteArrayHolder result = new ByteArrayHolder();
			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs);
			if (format.equals("PDF"))
			{
				exportfilePath = Utility.getReportTempMediaPath().toString() + "/" + reportName + ".pdf";
			}
			else if (format.equals("EXCEL"))
			{
				exportfilePath = Utility.getReportTempMediaPath().toString() + "/" + reportName + ".xlsx";
			}
			FileOutputStream fos = new FileOutputStream(exportfilePath);

			byte[] img = result.value;

			for (int i = 0; i < img.length; i++)
			{
				fos.write(img[i]);
			}

		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in exportReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return exportfilePath; // Prints the report
		// HTML; this could
		// be embedded in a
		// JSP

	}

	/**
	 * Method to Export report as EXCEL/PDF .
	 * 
	 * @param reportName
	 *            as a parameter.
	 * @param fromDate
	 *            as a parameter.
	 * @param id
	 *            as a parameter.
	 * @param toDate
	 *            To date as a parameter.
	 * @param moduleId
	 *            as a parameter
	 * @param moduleName
	 *            as a parameter
	 * @param format
	 *            as a parameter
	 * @param reportConfigInfo
	 *            as a parameter
	 * @return Export file path
	 * @throws ScanSeeServiceException
	 */
	public String exportModuleReport(String reportName, String id, String fromDate, String toDate, String format, String moduleId, String moduleName,
			ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{
		final String methodName = "exportModuleReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);
		String exportfilePath = null;
		try
		{
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportName, null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing
			if (moduleName.equals(MODULE_RETAILER))
			{
				service.setExecutionParameters(initializeParameters(id, fromDate, toDate, moduleId), "en-us"); // Set
				// report
				// parameters
			}
			else if (moduleName.equals(MODULE_SUPPLIER))
			{
				service.setExecutionParameters(initializeSupplierParameters(id, fromDate, toDate, moduleId), "en-us"); // Set
				// report
				// parameters
			}

			ByteArrayHolder result = new ByteArrayHolder();
			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs);
			if (format.equals("PDF"))
			{
				exportfilePath = Utility.getReportTempMediaPath().toString() + "/" + reportName + ".pdf";
			}
			else if (format.equals("EXCEL"))
			{
				exportfilePath = Utility.getReportTempMediaPath().toString() + "/" + reportName + ".xls";
			}

			FileOutputStream fos = new FileOutputStream(exportfilePath);

			byte[] img = result.value;

			for (int i = 0; i < img.length; i++)
			{
				fos.write(img[i]);
			}

		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in exportModuleReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return exportfilePath; // Prints the report
		// HTML; this could
		// be embedded in a
		// JSP

	}

	/**
	 * Method to render Supplier Product report .
	 * 
	 * @param reportParams
	 *            as a parameter having report params.
	 * @param reportConfigInfo
	 *            as a parameter having report configuration info.
	 * @return HTML format of report content
	 * @throws ScanSeeServiceException
	 */

	public String getSupplierProductReport(DashBoard reportParams, ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{
		final String methodName = "getSupplierProductReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);
		String reportData = null;
		List<String> imgSource = new ArrayList<String>();
		String productID = null;
		try
		{
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportParams.getSsrsReportName(), null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing
			service.setExecutionParameters(
					initializeSupplierParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate(),
							reportParams.getProductModuleId()), "en-us"); // Set
			// report
			// parameters

			ByteArrayHolder result = new ByteArrayHolder();
			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs);
			reportData = new String(result.value);
			reportData = reportData.replace("'", "\\'");
			Parser parser = new Parser();
			parser.setInputHTML(reportData);
			NodeList list = parser.parse(null);

			NodeList hyperLink = list.extractAllNodesThatMatch(new TagNameFilter("a"), true);

			for (SimpleNodeIterator iterator = hyperLink.elements(); iterator.hasMoreNodes();)
			{
				Tag tag = (Tag) iterator.nextNode();
				imgSource.add(tag.getAttribute("href"));
			}

			for (String url : imgSource)
			{

				Map<String, String> map = Utility.getQueryMap(url);

				productID = map.get("ProductID");
				// URL = "/ScanSeeWeb/getproductrep.htm?id=" + productID;
				reportData = reportData.replace(url, "#\"" + " onclick=\"loadProductDemOnChange(" + productID + ");");
			}

		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in getSupplierProductReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return reportData; // Prints the report
		// HTML; this could
		// be embedded in a
		// JSP

	}

	/**
	 * Method to render Supplier Interest report .
	 * 
	 * @param reportParams
	 *            as a parameter having report params.
	 * @param reportConfigInfo
	 *            as a parameter having report configuration info.
	 * @return HTML format of report content
	 * @throws ScanSeeServiceException
	 */
	public String getSupplierInterestReport(DashBoard reportParams, ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{
		final String methodName = "getSupplierInterestReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);
		String reportData = null;
		try
		{
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportParams.getSsrsReportName(), null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing
			service.setExecutionParameters(initializeSupplierParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate()),
					"en-us"); // Set
			// report
			// parameters

			ByteArrayHolder result = new ByteArrayHolder();

			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs); // Render
			reportData = new String(result.value);
			reportData = reportData.replace("'", "\\'");
		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in getSupplierInterestReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return reportData; // Prints the report
		// HTML; this could
		// be embedded in a
		// JSP

	}

	/**
	 * Method to render Supplier This Location report .
	 * 
	 * @param reportParams
	 *            as a parameter having report params.
	 * @param reportConfigInfo
	 *            as a parameter having report configuration info.
	 * @return HTML format of report content
	 * @throws ScanSeeServiceException
	 */
	public String getSupplierThisLocReport(DashBoard reportParams, ReportConfigInfo reportConfigInfo) throws ScanSeeServiceException
	{
		final String methodName = "getSupplierThisLocReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ReportExecutionServiceSoapStub service = getService(reportConfigInfo);

		String firstImagePath = null;
		String secondImagePath = null;
		String reportData = null;
		Date date = new Date();
		List<String> imgSource = new ArrayList<String>();
		List<String> hypetLinkList = new ArrayList<String>();
		String productId = null;
		try
		{
			ExecutionInfo info = service.loadReport(reportConfigInfo.getReportPath() + reportParams.getSsrsReportName(), null); // Load
			// report
			setExecutionId(service, info.getExecutionID()); // You must set the
															// session id before
															// continuing
			service.setExecutionParameters(
					initializeSupplierThisLocParameters(reportParams.getId(), reportParams.getFromDate(), reportParams.getToDate(),
							reportParams.getProductModuleId(), reportParams.getProductID()), "en-us"); // Set
			// report
			// parameters

			ByteArrayHolder result = new ByteArrayHolder();
			ByteArrayHolder firstImageResult = new ByteArrayHolder();
			ByteArrayHolder secondImageresult = new ByteArrayHolder();
			StringHolder extension = new StringHolder();
			StringHolder mimeType = new StringHolder();
			StringHolder encoding = new StringHolder();
			ArrayOfWarningHolder warnings = new ArrayOfWarningHolder();
			ArrayOfStringHolder streamIDs = new ArrayOfStringHolder();
			service.render(format, deviceInfo, result, extension, mimeType, encoding, warnings, streamIDs); // Render
																											// report
			service.renderStream(format, streamIDs.value[0], deviceInfo, firstImageResult, encoding, mimeType);
			service.renderStream(format, streamIDs.value[1], deviceInfo, secondImageresult, encoding, mimeType);
			firstImagePath = Utility.getReportTempMediaPath() + "/" + streamIDs.value[0] + ".png";
			secondImagePath = Utility.getReportTempMediaPath() + "/" + streamIDs.value[1] + ".png";

			byte[] img = firstImageResult.value;

			BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(img));

			Utility.writeImage(bufferedImage, firstImagePath);

			img = secondImageresult.value;

			bufferedImage = ImageIO.read(new ByteArrayInputStream(img));

			Utility.writeImage(bufferedImage, secondImagePath);

			reportData = new String(result.value);
			reportData = reportData.replace("'", "\\'");
			Parser parser = new Parser();
			parser.setInputHTML(reportData);
			NodeList list = parser.parse(null);

			NodeList hyperLink = list.extractAllNodesThatMatch(new TagNameFilter("a"), true);

			for (SimpleNodeIterator iterator = hyperLink.elements(); iterator.hasMoreNodes();)
			{
				Tag tag = (Tag) iterator.nextNode();
				hypetLinkList.add(tag.getAttribute("href"));
			}

			for (String url : hypetLinkList)
			{

				Map<String, String> map = Utility.getQueryMap(url);

				productId = map.get("ProductID");
				reportData = reportData.replace(url, "#\"" + " onclick=\"loadProductDemOnChange(" + productId + ");");
			}

			NodeList imgs = list.extractAllNodesThatMatch(new TagNameFilter("IMG"), true);

			for (SimpleNodeIterator iterator = imgs.elements(); iterator.hasMoreNodes();)
			{
				Tag tag = (Tag) iterator.nextNode();
				imgSource.add(tag.getAttribute("src"));
			}

			if (!imgSource.isEmpty())
			{

				if (imgSource.get(0) != null && !"".equals(imgSource.get(0)))
				{
					reportData = reportData.replace(imgSource.get(0), tempReportImagePath + streamIDs.value[0] + ".png" + "?" + date.getTime());
				}
				if (imgSource.get(1) != null && !"".equals(imgSource.get(1)))
				{
					reportData = reportData.replace(imgSource.get(1), tempReportImagePath + streamIDs.value[1] + ".png" + "?" + date.getTime());
				}
			}

		}
		catch (Exception e)
		{
			LOG.error("Excpetion Occurred in getSupplierThisLocReport Method:" + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return reportData; // Prints the report
		// HTML; this could
		// be embedded in a
		// JSP
	}
}
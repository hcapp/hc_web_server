/**
 * 
 */
package shopper.service;

import java.util.ArrayList;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.dao.CLRGalleryDAO;
import shopper.dao.MyGalleryDAO;
import shopper.helper.SortCouponByCategory;

import common.constatns.ApplicationConstants;
import common.email.EmailComponent;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CategoryInfo;
import common.pojo.shopper.CouponsDetails;
import common.pojo.shopper.LoyaltyDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.RebateDetails;
import common.pojo.shopper.ShareProductInfo;

/**
 * @author manjunatha_gh
 */
public class MyGalleryServiceImpl implements MyGalleryService
{

	/**
	 * Getting the logger Instance.
	 */
	public static final Logger LOG = LoggerFactory.getLogger(MyGalleryServiceImpl.class);

	/**
	 * The variable of type myGalleryDAO.
	 */
	private MyGalleryDAO myGalleryDAO;
	/**
	 * The variable of type clrGalleryDAO.
	 */
	private CLRGalleryDAO clrGalleryDAO;

	/**
	 * Setter method for myGalleryDAO.
	 * 
	 * @param myGalleryDAO
	 *            the Variable of type myGalleryDAO
	 */
	public final void setMyGalleryDAO(MyGalleryDAO myGalleryDAO)
	{
		this.myGalleryDAO = myGalleryDAO;
	}

	/**
	 * Setter method for clrGalleryDAO.
	 * 
	 * @param clrGalleryDAO
	 *            the Variable of type clrGalleryDAO
	 */
	public final void setClrGalleryDAO(CLRGalleryDAO clrGalleryDAO)
	{
		this.clrGalleryDAO = clrGalleryDAO;
	}

	/**
	 * getMyGalleryAllTypeInfo is a method to get the CLR information based on
	 * the category. To get the coupon based on category this method calls
	 * getCouponByCategory method of SortCouponByCategory helper class.
	 * 
	 * @param clrDetails
	 *            contains userId, retailerId,categoryId,searchKey and
	 *            lowerLimit.
	 * @param type
	 *            contains CLR type.
	 * @return CLRDetails returns CLR based on the category.
	 * @throws ScanSeeServiceException
	 */
	public CLRDetails getMyGalleryAllTypeInfo(CLRDetails clrDetails, String type) throws ScanSeeServiceException
	{
		final String methodName = "getMyGalleryAllTypeInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrResult = null;
		SortCouponByCategory sortCouponByCategory = null;
		try
		{
			sortCouponByCategory = new SortCouponByCategory();
			if (null != type && type.equalsIgnoreCase("ALL"))
			{
				clrResult = myGalleryDAO.getMyGalleryAllTypeInfo(clrDetails);
				if (null != clrResult.getIsCouponthere())
				{
					clrResult = SortCouponByCategory.getCouponByCategory(clrResult);
				}
			}
			else if (null != type && type.equalsIgnoreCase("EXPIRED"))
			{
				clrResult = myGalleryDAO.getMyGalleryExpiredTypeInfo(clrDetails);
				if (null != clrResult.getIsCouponthere())
				{
					clrResult = SortCouponByCategory.getCouponByCategory(clrResult);
				}
			}
			else if (null != type && type.equalsIgnoreCase("USED"))
			{
				clrResult = myGalleryDAO.getMyGalleryUsedTypeInfo(clrDetails);
				if (null != clrResult.getIsCouponthere())
				{
					clrResult = SortCouponByCategory.getCouponByCategory(clrResult);
				}

			}
			else if (null != type && type.equalsIgnoreCase("MYGALLERY"))
			{
				clrResult = myGalleryDAO.getMyGalleryInfo(clrDetails);
				if (null != clrResult.getIsCouponthere())
				{
					clrResult = SortCouponByCategory.getCouponByCategory(clrResult);
				}
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return clrResult;
	}

	/**
	 * getCLRInfoInfo is a method used to fetch CLR Details
	 * 
	 * @param clrDetails
	 *            contains userId,couponId in the request.
	 * @param clrType
	 *            contains clrType.
	 * @return CLRDetails returns the coupon details.
	 * @throws ScanSeeServiceException
	 */
	public CLRDetails getCLRInfoInfo(CLRDetails clrDetails, String clrType) throws ScanSeeServiceException
	{
		final String methodName = "getCLRInfoInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		CouponsDetails couponInfo = null;

		try
		{
			if ("C".equals(clrType))
			{
				couponInfo = clrGalleryDAO.fetchCouponDetails(clrDetails);
				clrDetails.setCoupDetails(couponInfo);
			}
			else if ("L".equals(clrType))
			{
				final LoyaltyDetails loyaltyDetails = clrGalleryDAO.getLoyaltyInfo(clrDetails);
				clrDetails.setLoyDetails(loyaltyDetails);
			}
			else
			{
				final RebateDetails rebateDetails = clrGalleryDAO.getRebateInfo(clrDetails);
				clrDetails.setRebDetails(rebateDetails);
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return clrDetails;
	}

	/**
	 * couponAdd is a method for adding the coupon to myCoupon.
	 * 
	 * @param userId
	 *            contains user id of the coupon associated with.
	 * @param couponId
	 *            contains coupon's id to be removed.
	 * @return String returns success or failure
	 * @throws ScanSeeServiceException
	 */
	public String couponAdd(Long userId, StringBuffer couponId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = clrGalleryDAO.addCoupon(userId, couponId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * rebateAdd is a method for adding the coupon to myRebate.
	 * 
	 * @param rebateId
	 * @param userId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String rebateAdd(Long userId, Integer rebateId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = clrGalleryDAO.addRebate(userId, rebateId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * rebateAdd is a method for adding the coupon to myLoyalty.
	 * 
	 * @param loyaltyDealId
	 * @param userId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String loyaltyAdd(Long userId, Integer loyaltyDealId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = clrGalleryDAO.addLoyalty(userId, loyaltyDealId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * couponRemove is a method used to remove coupon from myCoupons or
	 * allCoupons.
	 * 
	 * @param userId
	 *            contains user id of the coupon associated with.
	 * @param couponId
	 *            contains coupon's id to be removed.
	 * @return String returns success or failure
	 * @throws ScanSeeServiceException
	 */
	public String couponRemove(Long userId, StringBuffer couponId) throws ScanSeeServiceException
	{

		final String methodName = "couponRemove of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = clrGalleryDAO.removeCoupon(userId, couponId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * rebateRemove is a method used to remove coupon from myRebate or
	 * allRebate.
	 * 
	 * @param userId
	 * @param rebateId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String rebateRemove(Long userId, Integer rebateId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = clrGalleryDAO.removeRebate(userId, rebateId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * loyaltyRemove is a method used to remove coupon from myLoyalty or
	 * allLoyalty.
	 * 
	 * @param userId
	 * @param loyaltyDealId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String loyaltyRemove(Long userId, Integer loyaltyDealId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = clrGalleryDAO.removeLoyalty(userId, loyaltyDealId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * this method is used to mark the coupon as used coupon.
	 * 
	 * @param userId
	 *            contains user id of the coupon associated with.
	 * @param couponId
	 *            contains coupon's id to be removed.
	 * @return String returns success or failure.
	 * @throws ScanSeeServiceException
	 */
	public String userRedeemCoupon(Long userId, StringBuffer couponId) throws ScanSeeServiceException
	{
		final String methodName = "userRedeemCoupon ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			if (null != userId && null != couponId)

			{

				response = clrGalleryDAO.userRedeemCoupon(userId, couponId);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName);
			throw new ScanSeeServiceException(e.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * this method is used to mark the coupon as used coupon.
	 * 
	 * @param userId
	 * @param loyaltyDealId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String userRedeemLoyalty(Long userId, Integer loyaltyDealId) throws ScanSeeServiceException
	{

		String response = null;
		try
		{
			if (null != userId && null != loyaltyDealId)

			{

				response = clrGalleryDAO.userRedeemLoyalty(userId, loyaltyDealId);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		return response;
	}

	/**
	 * this method is used to mark the coupon as used coupon.
	 * 
	 * @param userId
	 * @param rebateId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String userRedeemRebate(Long userId, Integer rebateId) throws ScanSeeServiceException
	{
		String response = null;
		try
		{
			if (null != userId && null != rebateId)

			{

				response = clrGalleryDAO.userRedeemRebate(userId, rebateId);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		return response;
	}

	/**
	 * this method is used to share CLR via email.
	 * 
	 * @param shareProductInfoObj
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	public String shareCLRbyEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeServiceException
	{
		final String methodName = " shareCLRbyEmail of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String productInfo = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = null;
		String msgBody = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		try
		{
			if (null != shareProductInfoObj)
			{
				if (null != shareProductInfoObj.getCouponId())
				{
					productInfo = clrGalleryDAO.getCouponShareThruEmail(shareProductInfoObj);
					subject = ApplicationConstants.SHARECOUPONBYEMAILSUBJECT;

				}
				else if (null != shareProductInfoObj.getLoyaltyId())
				{
					productInfo = clrGalleryDAO.getLoyaltyShareThruEmail(shareProductInfoObj);
					subject = ApplicationConstants.SHARELOYALTYBYEMAILSUBJECT;
				}
				else
				{
					productInfo = clrGalleryDAO.getRebateShareThruEmail(shareProductInfoObj);
					subject = ApplicationConstants.SHAREREBATEBYEMAILSUBJECT;
				}

				fromAddress = shareProductInfoObj.getFromEmail();
				toAddress = shareProductInfoObj.getToEmail();

				emailConf = clrGalleryDAO.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				msgBody = productInfo;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}
				LOG.info("Mail delivered to:" + toAddress);
				response = ApplicationConstants.SHAREPRODUCTINFO;
			}

		}
		catch (MessagingException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = ApplicationConstants.INVALIDEMAILADDRESSTEXT;
		}
		catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * this method is used to add products to wishList.
	 * 
	 * @param areaInfoVO
	 *            contains userId.
	 * @param productId
	 *            contains product id's to be added to wishList
	 * @return String returns product already exists or added successfully.
	 * @throws ScanSeeServiceException
	 */
	public String addWishListProd(AreaInfoVO areaInfoVO, StringBuffer productId) throws ScanSeeServiceException
	{

		final String methodName = "addWishListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{

			response = clrGalleryDAO.addWishListProd(areaInfoVO, productId);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * 
	 */
	public String userRedeemCLRDiscount(Long userId, Integer clrId, String clrFlag) throws ScanSeeServiceException
	{
		String response = null;
		try
		{
			if (null != userId && null != clrId)

			{

				response = clrGalleryDAO.discountUserRedeemCLR(userId, clrId, clrFlag);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		return response;
	}

	/**
	 * This method is used to fetch the products details associated with the
	 * coupon . accepts a GET request in mime type text/plain
	 * 
	 * @param clrDetails
	 *            contains userId,couponId,rebateId and loyaltyId.
	 * @param clrType
	 *            contains CLR type.
	 * @return ArrayList<ProductDetail> returns list of product details
	 *         associated with the CLR.
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<ProductDetail> getCLRProductInfo(CLRDetails clrDetails, String clrType) throws ScanSeeServiceException
	{
		final String methodName = "getCLRProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> productList = null;

		try
		{
			if ("C".equals(clrType))
			{
				productList = clrGalleryDAO.fetchCouponProductDetails(clrDetails);
			}
			else if ("L".equals(clrType))
			{
				final LoyaltyDetails loyaltyDetails = clrGalleryDAO.getLoyaltyInfo(clrDetails);
				clrDetails.setLoyDetails(loyaltyDetails);
			}
			else
			{
				final RebateDetails rebateDetails = clrGalleryDAO.getRebateInfo(clrDetails);
				clrDetails.setRebDetails(rebateDetails);
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return productList;
	}

	/**
	 * this method is used to fetch the list of categories.
	 * 
	 * @return ArrayList<CategoryInfo> returns list of categories along with the
	 *         Category Information.
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<CategoryInfo> getCategories() throws ScanSeeServiceException
	{
		final String methodName = "retriveCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<CategoryInfo> categoryInfoList = null;

		try
		{

			categoryInfoList = myGalleryDAO.getCategories();

		}
		catch (Exception e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeServiceException(e);
		}

		return categoryInfoList;

	}

	/**
	 * this method is used to add products to shoppingList.
	 * 
	 * @param areaInfoVO
	 *            contains userId.
	 * @param productId
	 *            contains product id's to be added to wishList
	 * @return String returns product already exists or added successfully.
	 * @throws ScanSeeServiceException
	 */
	public String addShoppingListProd(AreaInfoVO areaInfoVO, StringBuffer productId) throws ScanSeeServiceException
	{

		final String methodName = "addShoppingListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		try
		{

			response = clrGalleryDAO.addShoppingListProd(areaInfoVO, productId);

		}
		catch (ScanSeeWebSqlException e)
		{

			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

}

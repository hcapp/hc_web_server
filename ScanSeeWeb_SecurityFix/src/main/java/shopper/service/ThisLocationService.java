package shopper.service;

import java.util.ArrayList;
import java.util.List;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.RetailerCreatedPages;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.ThisLocationRequest;
import common.pojo.shopper.ThisLocationRetailerInfo;

public interface ThisLocationService
{

	/**
	 * This method is used to get the online retailer details
	 */
	RetailersDetails getRetailersInfoForLocation(AreaInfoVO objAreaInfoVO);

	/**
	 * This method is used for getting the Products info for particular retailer
	 */
	ProductDetails getProductsInfo(ProductDetailsRequest productDetailsRequest);

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Sends email to recipients with product information.
	 * 
	 * @param xml
	 *            contains email id, product to be shared.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String shareProductInfo(ShareProductInfo shareProductInfoObj) throws ScanSeeServiceException;

	/**
	 * This is Rest Easy Webservice for getting radius informtion
	 * 
	 * @return returns response XML Containing radius if exception it will
	 *         return error message XML.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	ArrayList<ThisLocationRequest> getRediusInfo();

	/**
	 * This is service method is used for getting Latitude and Longitude for the
	 * given Zipcode.
	 * 
	 * @param zipcode
	 *            as request parameter for Zipcode.
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	ThisLocationRequest getLatLong(String zipcode);

	List<RetailerDetail> getRetailerSummary(ThisLocationRetailerInfo thisLocationRetailerInfo) throws ScanSeeServiceException;

	List<RetailerCreatedPages> getRetailerCreatedPages(ThisLocationRetailerInfo thisLocationRetailerInfo) throws ScanSeeServiceException;

	/**
	 * This Rest Easy method for fetching special offer,hot deals and
	 * coupon,loyalty and rebate discounts based on retailer and location id.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	List<RetailerDetail> fetchSpecialOffersHotDealDiscounts(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException;

	/**
	 * This method for fetching Retailer special offer list information for the
	 * specified retailer id and location ID. Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */
	List<RetailerDetail> fetchRetSpecialOfferlst(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException;

	/**
	 * This method for fetching Retailer hot deals list information for the
	 * specified retailer id and location ID. Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */
	List<HotDealsDetails> getRetailerHotDeals(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException;

	/**
	 * This method for fetching Retailer coupons list information for the
	 * specified retailer id and location ID. Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */
	CLRDetails getRetailerCLRDetails(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException;
}

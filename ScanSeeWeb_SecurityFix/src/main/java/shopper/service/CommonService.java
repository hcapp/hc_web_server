package shopper.service;

import java.util.ArrayList;
import java.util.List;

import com.scansee.externalapi.common.exception.ScanSeeExternalAPIException;
import com.scansee.externalapi.common.findnearby.pojos.FindNearByIntactResponse;
import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.Retailer;
import common.pojo.SearchForm;
import common.pojo.Users;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.ProductRatingReview;
import common.pojo.shopper.ProductSummaryVO;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.UserRatingInfo;
import common.pojo.shopper.UserTrackingData;
import common.pojo.shopper.WishListHistoryDetails;
import common.pojo.shopper.WishListProducts;

/**
 * This interface consists of service layer methods for shopping list.
 * 
 * @author malathi_lr
 */
public interface CommonService
{
	/**
	 * This method is used to fetch product details.
	 * 
	 * @param productDetailsRequest
	 *            productDetailsRequest.
	 * @return return.
	 */
	ProductSummaryVO getProductSummary(ProductDetailsRequest productDetailsRequest);

	/**
	 * This method is used to fetch product details.
	 * 
	 * @param objAreaInfoVO
	 *            productDetailsRequest.
	 * @param productId
	 *            productId.
	 * @return return.
	 */

	FindNearByDetails findNearBy(AreaInfoVO objAreaInfoVO, Integer productId);

	/**
	 * This is a service method used for retrieving the particular retailer
	 * details in the near by section
	 * 
	 * @param retailerId
	 *            retailerId of particular retailer
	 * @return Retailer Retailer instance which gives the details of the
	 *         retailer
	 */
	Retailer findNearByRetailerDetail(Integer retailerId);

	/**
	 * This is a Service Method for fetching product information for the given
	 * userId,productId and retailId.This method validates the userId, productId
	 * and retailId if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which product information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productId
	 *            for which product information need to be fetched.If productId
	 *            is null then it is invalid request.
	 * @param retailId
	 *            for which product information need to be fetched.If retailId
	 *            is null then it is invalid request.
	 * @return XML containing product information in the response.
	 */

	ProductDetail getProductsInfo(Long userId, Integer productId, Integer retailId);

	/**
	 * The Service Method for fetching product reviews and user ratings.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML String Containing Product Reviews and User Ratings.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductRatingReview getProductReviews(Long userId, Integer productId) throws ScanSeeWebSqlException;

	/**
	 * This is a service implementation method for saving user product rating
	 * information. Calls method in dao layer. accepts a POST request, MIME type
	 * is text/xml.
	 * 
	 * @param userRatingInfo
	 *            input request for which needed to save user product rating. If
	 *            userId or proudctId is null then it is invalid request.
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String saveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeWebSqlException;

	/**
	 * This method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the products media info
	 * based on the media type.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeServiceException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	ProductDetails getMediaDetails(Integer productID, String mediaType) throws ScanSeeServiceException;

	/**
	 * methods to get the products media info based on the media type from
	 * database.
	 * 
	 * @param productId
	 *            requested product.
	 * @param userId
	 *            type of media.
	 * @return ProductDetails with media details.
	 * @throws ScanSeeWebSqlException
	 *             throws if exception occurs
	 */
	ArrayList<RetailerDetail> getCommisJunctionData(Long userId, Integer productId) throws ScanSeeWebSqlException;

	/**
	 * the method fetches product Attributes information.
	 * 
	 * @param userId
	 *            as the request.
	 * @param productId
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetail fetchProductAttributes(Long userId, Integer productId) throws ScanSeeWebSqlException;

	// ##########NEWLY CREATED METHODS FOR
	// CONSUMER IMPLEMENTATION########################################

	/**
	 * This method is used to fetch product details for given product id.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeServiceException
	 *             which is caught in the controller layer.
	 */
	ProductDetail fetchConsumerProductDetails(ProductDetail productDetail) throws ScanSeeServiceException;

	/**
	 * This method is used to fetch product image list for given product id.
	 * 
	 * @param searchForm
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */

	List<ProductDetail> fetchConsumerProductImagelst(SearchForm searchForm) throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch product media details for given product id.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */

	List<ProductDetail> fetchConsumerProductMediaInfo(ProductDetail productDetail) throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch product attributes for given product id.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */
	List<ProductDetail> fetchConsumerProductAttributes(ProductDetail productDetail) throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch product attributes for given product id.
	 * 
	 * @param hotDealsListRequestObj
	 *            contains the hot deal id.
	 * @return product details.
	 * @throws ScanSeeServiceException
	 *             which is caught in the controller layer.
	 */

	HotDealsDetails getConsumerHdProdInfo(HotDealsListRequest hotDealsListRequestObj) throws ScanSeeServiceException;

	/**
	 * This method is used to fetch product find near by retailer for given
	 * product id.
	 * 
	 * @param productDetailObj
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */
	FindNearByDetails getConsFindNearByRetailer(ProductDetail productDetailObj) throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch external API information.
	 * 
	 * @param productDetailObj
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */
	FindNearByIntactResponse getConsFindRetailersExternalAPI(ProductDetail productDetailObj) throws ScanSeeWebSqlException,
			ScanSeeExternalAPIException;

	/**
	 * This method is used to fetch product reviews for given product id.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */
	List<ProductDetail> fetchConsumerProductReviews(ProductDetail productDetail) throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch product online stores for given product id.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */
	ArrayList<RetailerDetail> getConsumerCommisJunctionData(ProductDetail productDetail) throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch product associated coupons for given product
	 * id.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */
	List<CouponDetail> fetchConsumerProductCoupons(ProductDetail productDetail) throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch default radius.
	 * 
	 * @return radius details.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */
	public Integer getDefaultRadius() throws ScanSeeWebSqlException;

	/**
	 * This method is used to adding product to shopping list.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */

	ProductDetail consAddProdToShopp(ProductDetail productDetail) throws ScanSeeWebSqlException;

	/**
	 * This method is used to adding product to wish list.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */

	ProductDetail consAddProdToWishList(ProductDetail productDetail) throws ScanSeeWebSqlException;

	/**
	 * This method is used to sharing product information via Email id.
	 * 
	 * @param shareProductInfoObj
	 *            contains the product id.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             which is caught in the controller layer.
	 */
	String consShareProductInfoViaEmail(ProductDetail shareProductInfoObj) throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch external API information.
	 * 
	 * @param moduleName
	 *            contains the module name.
	 * @return
	 * @throws ScanSeeServiceException
	 */
	ExternalAPIInformation getConsExternalApiInfo(String moduleName) throws ScanSeeServiceException;

	UserRatingInfo consGetProductRatings(ProductDetail productRatings) throws ScanSeeWebSqlException;

	String consSaveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeWebSqlException;

	String getConsUserEmailId(Long userId) throws ScanSeeWebSqlException;

	String consShareHotDealInfoViaEmail(ProductDetail shareProductInfoObj) throws ScanSeeWebSqlException;

	List<HotDealsDetails> consDisplayPopulationCenters(HotDealsListRequest hotDealsListRequestObj) throws ScanSeeWebSqlException;

	String getConsShareEmailConfiguration() throws ScanSeeServiceException;

	/**
	 * This method returns the shopper details.
	 * 
	 * @param userID
	 * @return Users
	 * @throws ScanSeeServiceException
	 */
	Users getLoginUserDetails(Long userID) throws ScanSeeServiceException;

	List<UserTrackingData> getConsumerShareTypes() throws ScanSeeWebSqlException;

	ProductDetail saveConsShareProdDetails(ProductDetail ObjProductDetail) throws ScanSeeWebSqlException;

	ProductDetail getConsumerProdMultipleImages(ProductDetail productDetail) throws ScanSeeWebSqlException;

	// ####################################### CONSUMER SHOPPING LIST METHODS ######
	
	/**
	 * This method is used to fetch user Shopping list products.
	 * 
	 * @param objProductDetail
	 *            contains the userId,lower limit.
	 * @throws ScanSeeWebSqlException
	 *             which will be caught in controller layer.
	 */
	AddRemoveSBProducts getConsShopLstProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;

	AddRemoveSBProducts getConsSLHistoryProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;

	AddRemoveSBProducts getConsSLFavoriteProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;

	ProductDetails getSLSearchProducts(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;

	ProductDetails getConsShopSearchProd(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;

	ProductDetail consFavDeleteProd(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ProductDetail consAddHistoryProdToSL(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ProductDetail consAddFavProdToShopList(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ProductDetail consAddSearchProdToSL(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ProductDetail consDeleteTodayListProd(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ProductDetail consCheckOutTodayListProd(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ProductDetail consMoveToBasketProds(ProductDetail productDetail) throws ScanSeeWebSqlException;

	// ####################################### CONSUMER WISH LIST METHODS
	// #######################################
	WishListProducts getConsWishListProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	
	WishListHistoryDetails getConsWLHistoryProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	
	ProductDetail consDeleteWLProd(ProductDetail productDetail) throws ScanSeeWebSqlException;
	
	ProductDetail consDeleteWLHistoryProd(ProductDetail productDetail) throws ScanSeeWebSqlException;	
	
	ProductDetail consAddSearchProdToWL(ProductDetail productDetail) throws ScanSeeWebSqlException;
	
	ArrayList<CouponDetails> getConsWLCouponDetails(ProductDetail productDetail) throws ScanSeeWebSqlException;
	
	ArrayList<HotDealsDetails> getConsWLHotDealInfo(ProductDetail productDetail) throws ScanSeeWebSqlException;
	
	ProductDetail getProductInfoforExternalAPI(Integer productId) throws ScanSeeWebSqlException;
	
	String consWLAddCoupon(CouponDetail objCouponDetail) throws ScanSeeWebSqlException;
	
	String consWLRemoveCoupon(CouponDetail objCouponDetail) throws ScanSeeWebSqlException;
	
	
	ArrayList<ProductDetail> getPrintProductInfo(ProductDetail productDetail) throws ScanSeeWebSqlException;
	
	//################################### MY GALLERY METHODS ###########################################
	
	ProductDetails getConsCoupAssocitedProds(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException;
	
}

package shopper.service;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.dao.ShopperDAO;
import shopper.hotdeals.dao.HotDealsDAO;

import common.constatns.ApplicationConstants;
import common.email.EmailComponent;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.RetailerCreatedPages;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.ThisLocationRequest;
import common.pojo.shopper.ThisLocationRetailerInfo;

public class ThisLocationServiceImpl implements ThisLocationService
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ThisLocationServiceImpl.class);

	/**
	 * The variable of type ThisLocationDAO.
	 */

	private ShopperDAO shopperDAO;

	/**
	 * Setter method for thisLocationDao.
	 * 
	 * @param thisLocationDao
	 *            the Variable of type ThisLocationDAO
	 */

	public void setShopperDAO(ShopperDAO shopperDAO)
	{
		this.shopperDAO = shopperDAO;
	}

	/**
	 * The variable of type HotDealsDAO.
	 */

	private HotDealsDAO hotDealsDao;

	/**
	 * Setter method for HotDealsDAO.
	 * 
	 * @param hotDealsDao
	 *            the Variable of type HotDealsDAO
	 */

	public void setHotDealsDao(HotDealsDAO hotDealsDao)
	{
		this.hotDealsDao = hotDealsDao;
	}

	/**
	 * This method is used to get the online retailer details
	 */
	public RetailersDetails getRetailersInfoForLocation(AreaInfoVO objAreaInfoVO)
	{
		LOG.info("Entered in getRetailersInfoForLocation method in ServiceImpl class");
		String completeAddress = null;
		RetailersDetails retailsDetails = null;
		try
		{
			retailsDetails = shopperDAO.fetchRetailerDetails(objAreaInfoVO, ApplicationConstants.THISLOCATIONRETAILERSCREEN);
			List<ThisLocationRetailerInfo> thisLocationRetailerInfoList = null;

			if (null != retailsDetails)
			{
				final List<RetailerDetail> retailerDetail = retailsDetails.getRetailerDetail();
				if (!retailerDetail.isEmpty())
				{
					thisLocationRetailerInfoList = new ArrayList<ThisLocationRetailerInfo>();
					for (int i = 0; i < retailerDetail.size(); i++)
					{
						final ThisLocationRetailerInfo thisLocationRetailerInfo = new ThisLocationRetailerInfo();

						if (null != retailerDetail.get(i).getRetaileraddress1()
								&& !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getRetaileraddress1())
								&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getRetaileraddress1()))
						{
							completeAddress = retailerDetail.get(i).getRetaileraddress1() + ApplicationConstants.COMMA;
						}
						if (null != retailerDetail.get(i).getCity() && !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getCity())
								&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getCity()))
						{
							completeAddress = completeAddress + retailerDetail.get(i).getCity() + ApplicationConstants.COMMA;
						}
						if (null != retailerDetail.get(i).getPostalCode()
								&& !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getPostalCode())
								&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getPostalCode()))
						{
							completeAddress = completeAddress + retailerDetail.get(i).getPostalCode() + ApplicationConstants.COMMA;
						}
						if (null != retailerDetail.get(i).getState() && !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getState())
								&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getState()))
						{
							completeAddress = completeAddress + retailerDetail.get(i).getState();
						}

						thisLocationRetailerInfo.setRowNumber(retailerDetail.get(i).getRowNumber());
						thisLocationRetailerInfo.setRetailerId(retailerDetail.get(i).getRetailerId());
						thisLocationRetailerInfo.setRetailerName(retailerDetail.get(i).getRetailerName());
						thisLocationRetailerInfo.setRetailLocationID(retailerDetail.get(i).getRetailLocationID());
						thisLocationRetailerInfo.setRetailAddress(completeAddress);
						thisLocationRetailerInfo.setDistance(retailerDetail.get(i).getDistance());
						thisLocationRetailerInfo.setLogoImagePath(retailerDetail.get(i).getLogoImagePath());
						thisLocationRetailerInfo.setBannerAdImagePath(retailerDetail.get(i).getBannerAdImagePath());
						thisLocationRetailerInfo.setRibbonAdImagePath(retailerDetail.get(i).getRibbonAdImagePath());
						thisLocationRetailerInfo.setRibbonAdURL(retailerDetail.get(i).getRibbonAdURL());
						//thisLocationRetailerInfo.setSaleFlag(retailerDetail.get(i).getSaleFlag());

						thisLocationRetailerInfoList.add(thisLocationRetailerInfo);
					}

				}
				retailsDetails.setRetailerDetail(null);
				retailsDetails.setThisLocationRetailerInfo(thisLocationRetailerInfoList);
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Error occured in getRetailersInfoForLocation method in ServiceImpl class", e);
			e.printStackTrace();
		}
		LOG.info("Exited in getRetailersInfoForLocation method in ServiceImpl class");
		return retailsDetails;
	}

	/**
	 * This method is used for getting the Products info for particular retailer
	 */
	public ProductDetails getProductsInfo(ProductDetailsRequest productDetailsRequest)
	{
		LOG.info("Entered in getProductsInfo method in ServiceImpl class");
		ProductDetails productDetails = new ProductDetails();
		try
		{
			productDetails = shopperDAO.fetchProductDetails(productDetailsRequest, ApplicationConstants.THISLOCATIONPRODUCTSSCREEN);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Error Occured in getProductsInfo method in ServiceImpl class", e);
		}

		LOG.info("Exited in getProductsInfo method in ServiceImpl class");
		return productDetails;

	}

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Sends email to recipients with product information.
	 * 
	 * @param xml
	 *            contains email id, product to be shared.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String shareProductInfo(ShareProductInfo shareProductInfoObj) throws ScanSeeServiceException
	{

		final String methodName = " shareProductInfo of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String productInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = "Product information shared from Scansee";
		String msgBody = null;
		RetailerDetail retailerDetail = null;
		String shareURL = null;
		ArrayList<AppConfiguration> stockInfoList = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;

		try
		{
			if (shareProductInfoObj != null)
			{
				stockInfoList = shopperDAO.getAppConfig(ApplicationConstants.CONFIGURATIONTYPESHAREURL);

				for (int i = 0; i < stockInfoList.size(); i++)
				{
					if (stockInfoList.get(i).getScreenName().equals(ApplicationConstants.EMAILSHAREURL))
					{
						shareURL = stockInfoList.get(i).getScreenContent();
					}

				}

				productInfo = shopperDAO.getProductInfo(shareProductInfoObj, shareURL);

				subject = ApplicationConstants.SHAREPRODUCTINFOBYEMAIL;

				userEmailId = shopperDAO.getUserInfo(shareProductInfoObj);
				if (userEmailId == null)
				{
					LOG.info("Validation failed::::To Email is not available");

					response = ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE;
					return response;
				}
				toAddress = shareProductInfoObj.getToEmail();
				emailConf = shopperDAO.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				msgBody = productInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				LOG.info("Mail delivered to:" + toAddress);
				// response =
				// Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
				// ApplicationConstants.SHAREPRODUCTINFO);
				response = ApplicationConstants.SHAREPRODUCTINFO;
			}
		}
		catch (MessagingException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = ApplicationConstants.INVALIDEMAILADDRESSTEXT;
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : shareProductInfo : " + exception.getMessage());
			throw new ScanSeeServiceException(exception.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Rest Easy Webservice for getting radius informtion
	 * 
	 * @return returns response XML Containing radius if exception it will
	 *         return error message XML.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ArrayList<ThisLocationRequest> getRediusInfo()
	{
		final String methodName = "getRediusInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ThisLocationRequest> thislocationRediuslst = null;
		try
		{
			thislocationRediuslst = shopperDAO.getRediusDetails();
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : shareProductInfo : " + exception.getMessage());
			exception.printStackTrace();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return thislocationRediuslst;
	}

	/**
	 * This is service method is used for getting Latitude and Longitude for the
	 * given Zipcode.
	 * 
	 * @param zipcode
	 *            as request parameter for Zipcode.
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	public ThisLocationRequest getLatLong(String zipcode)
	{
		final String methodName = "getLatLong";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ThisLocationRequest thisLocationRequestObj = new ThisLocationRequest();
		try
		{
			thisLocationRequestObj = shopperDAO.fetchLatLong(zipcode);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Exception occurerd in getLatLong()", e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return thisLocationRequestObj;
	}

	/**
	 * This Rest Easy method for fetching retailer details information.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	public List<RetailerDetail> getRetailerSummary(ThisLocationRetailerInfo thisLocationRetailerInfo) throws ScanSeeServiceException
	{

		final String methodName = "getRetailerSummary";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> retailSummarylst = null;
		try
		{
			retailSummarylst = shopperDAO.getRetailerSummary(thisLocationRetailerInfo);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : getRetailerSummary : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailSummarylst;
	}

	/**
	 * This Rest Easy method for fetching retailer created pages information.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	public List<RetailerCreatedPages> getRetailerCreatedPages(ThisLocationRetailerInfo thisLocationRetailerInfo) throws ScanSeeServiceException
	{

		final String methodName = "getRetailerCreatedPages";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerCreatedPages> retailCreatedPageslst = null;

		try
		{

			retailCreatedPageslst = shopperDAO.getRetCreatePages(thisLocationRetailerInfo);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : getRetailerCreatedPages : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailCreatedPageslst;
	}

	/**
	 * This Rest Easy method for fetching retailer current special flags.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	public List<RetailerDetail> fetchSpecialOffersHotDealDiscounts(ThisLocationRetailerInfo thisLocationRetailerInfoObj)
			throws ScanSeeWebSqlException
	{
		final String methodName = "fetchSpecialOffersHotDealDiscounts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> specialOfferlst = null;
		try
		{

			specialOfferlst = shopperDAO.fetchSpecialOffersHotDealDiscounts(thisLocationRetailerInfoObj);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : fetchSpecialOffersHotDealDiscounts : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return specialOfferlst;
	}

	/**
	 * This Rest Easy method for fetching retailer special offer list.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	
	public List<RetailerDetail> fetchRetSpecialOfferlst(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchRetSpecialOfferlst";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> specialOfferlst = null;
		try
		{

			specialOfferlst = shopperDAO.fetchSpecialOffers(thisLocationRetailerInfoObj);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : fetchRetSpecialOfferlst : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return specialOfferlst;
	}

	/**
	 * This Rest Easy method for fetching retailer hotdeals list.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	public List<HotDealsDetails> getRetailerHotDeals(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetailerHotDeals";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> retHotdealslst = null;
		try
		{

			retHotdealslst = shopperDAO.getRetailerHotDeals(thisLocationRetailerInfoObj);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : getRetailerHotDeals : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retHotdealslst;
	}

	/**
	 * This Rest Easy method for fetching retailer coupons list.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	public CLRDetails getRetailerCLRDetails(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetailerCLRDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails cLRDetailsObj = null;
		try
		{

			cLRDetailsObj = shopperDAO.getRetailerCLRDetails(thisLocationRetailerInfoObj);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : getRetailerCLRDetails : " + exception.getMessage());
			exception.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return cLRDetailsObj;
	}
}

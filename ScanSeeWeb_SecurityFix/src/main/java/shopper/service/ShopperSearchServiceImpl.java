package shopper.service;

import java.util.ArrayList;
import java.util.List;

import shopper.dao.ShopperSearchDAO;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.HotDealInfo;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;

public class ShopperSearchServiceImpl implements ShopperSearchService
{
	
	private ShopperSearchDAO shoppersearchDAO;

	public ShopperSearchDAO getShoppersearchDAO() {
		return shoppersearchDAO;
	}

	public void setShoppersearchDAO(ShopperSearchDAO shoppersearchDAO) {
		this.shoppersearchDAO = shoppersearchDAO;
	}

	/**
	 * @return the searchDAO
	 *//*
	public ShopperSearchDAO getSearchDAO()
	{
		return shoppersearchDAO;
	}

	*//**
	 * @param searchDAO
	 *            the searchDAO to set
	 *//*
	public void setSearchDAO(ShopperSearchDAO shoppersearchDAO)
	{
		this.shoppersearchDAO = shoppersearchDAO;
	}*/

	public ProductVO getAllProductInfo(Integer retailLocationID,
			Integer productID) throws ScanSeeServiceException {

		ProductVO productInfo = null;
		try {

			productInfo = shoppersearchDAO.fetchProductInfo(retailLocationID,
					productID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return productInfo;

	}

	public SearchResultInfo searchProducts(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException
	{
		SearchResultInfo resultInfo = null;
		try
		{
			if (objForm.getSearchType().equalsIgnoreCase("products"))
			{
				// product Search
				resultInfo = shoppersearchDAO.fetchAllProduct(objForm, loginUser, lowerLimit);
			}
			else if(objForm.getSearchType().equalsIgnoreCase("deals"))
			{
				// HotDeals Search
				resultInfo = shoppersearchDAO.searchHotDeals(objForm, loginUser,lowerLimit);
			}else if(objForm.getSearchType().equalsIgnoreCase("producthotdeal"))
			{
				// product with HotDeals Search
				resultInfo = shoppersearchDAO.getProductWithDeal(objForm, loginUser,lowerLimit);
			}
			
			
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}
		
		return resultInfo;
	}
	
	
	
	public SearchResultInfo searchDeals(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException
	{
		SearchResultInfo resultInfo = null;
		try
		{

			resultInfo = shoppersearchDAO.searchHotDeals(objForm, loginUser, lowerLimit);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return resultInfo;
	}
	
	
	public ProductVO getAllProductInfoWithDeals(Integer productID) throws ScanSeeServiceException {

		ProductVO productInfo = null;
		try {

			productInfo = shoppersearchDAO.fetchProductInfoWithDeals(productID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return productInfo;

	}
	
	
	public HotDealInfo getDealsInfo(Integer hotDealID) throws ScanSeeServiceException {

		HotDealInfo hotDealInfo = null;
		try {

			hotDealInfo = shoppersearchDAO.fetchDealsInfo(hotDealID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return hotDealInfo;

	}

	
	
	public List<ProductVO> getProductAssociatedWithDeals(Integer productID) throws ScanSeeServiceException {

		List<ProductVO> prodAssociatedHotDealList = null;
		try {

			prodAssociatedHotDealList = shoppersearchDAO.fetchProductAssociatedWithDeals(productID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return prodAssociatedHotDealList;

	}


	public ArrayList<ProductVO> getAllProduct(String zipcode,
			String productName, int lowerLimit, String screenName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*public SearchResultInfo searchDeals(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException
	{
		SearchResultInfo resultInfo = null;
		try
		{

			resultInfo = searchDAO.searchHotDeals(objForm, loginUser, lowerLimit);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return resultInfo;
	}*/
	
}

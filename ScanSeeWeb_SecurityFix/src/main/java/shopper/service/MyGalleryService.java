/**
 * 
 */
package shopper.service;

import java.util.ArrayList;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CategoryInfo;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ShareProductInfo;

/**
 * @author manjunatha_gh
 */
public interface MyGalleryService
{
	/**
	 * getMyGalleryAllTypeInfo is a method to get the CLR information based on
	 * the category.
	 * 
	 * @param clrDetails
	 * @param type
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	CLRDetails getMyGalleryAllTypeInfo(CLRDetails clrDetails, String type) throws ScanSeeServiceException;

	/**
	 * getCLRInfoInfo is a method used to fetch CLR Details
	 * 
	 * @param clrDetails
	 * @param clrType
	 * @return CLRDetails
	 * @throws ScanSeeServiceException
	 */
	CLRDetails getCLRInfoInfo(CLRDetails clrDetails, String clrType) throws ScanSeeServiceException;

	/**
	 * couponAdd is a method for adding the coupon to myCoupon.
	 * 
	 * @param couponId
	 * @param userId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String couponAdd(Long userId, StringBuffer couponId) throws ScanSeeServiceException;

	/**
	 * rebateAdd is a method for adding the coupon to myRebate.
	 * 
	 * @param rebateId
	 * @param userId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String rebateAdd(Long userId, Integer rebateId) throws ScanSeeServiceException;

	/**
	 * rebateAdd is a method for adding the coupon to myLoyalty.
	 * 
	 * @param loyaltyDealId
	 * @param userId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String loyaltyAdd(Long userId, Integer loyaltyDealId) throws ScanSeeServiceException;

	/**
	 * couponRemove is a method used to remove coupon from myCoupons or
	 * allCoupons.
	 * 
	 * @param userId
	 * @param couponId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String couponRemove(Long userId, StringBuffer couponId) throws ScanSeeServiceException;

	/**
	 * rebateRemove is a method used to remove coupon from myRebate or
	 * allRebate.
	 * 
	 * @param userId
	 * @param rebateId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String rebateRemove(Long userId, Integer rebateId) throws ScanSeeServiceException;

	/**
	 * loyaltyRemove is a method used to remove coupon from myLoyalty or
	 * allLoyalty.
	 * 
	 * @param userId
	 * @param loyaltyDealId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String loyaltyRemove(Long userId, Integer loyaltyDealId) throws ScanSeeServiceException;

	/**
	 * this method is used to mark the coupon as used coupon.
	 * 
	 * @param userId
	 * @param couponId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String userRedeemCoupon(Long userId, StringBuffer couponId) throws ScanSeeServiceException;

	/**
	 * this method is used to mark the coupon as used coupon.
	 * 
	 * @param userId
	 * @param loyaltyDealId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String userRedeemLoyalty(Long userId, Integer loyaltyDealId) throws ScanSeeServiceException;

	/**
	 * this method is used to mark the coupon as used coupon.
	 * 
	 * @param userId
	 * @param rebateId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String userRedeemRebate(Long userId, Integer rebateId) throws ScanSeeServiceException;

	/**
	 * this method is used to share CLR via email.
	 * 
	 * @param shareProductInfoObj
	 * @return String
	 * @throws ScanSeeWebSqlException
	 * @throws ScanSeeServiceException 
	 */
	public String shareCLRbyEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeServiceException;

	/**
	 * this method is used to add products to wishList.
	 * 
	 * @param areaInfoVO
	 * @param productId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String addWishListProd(AreaInfoVO areaInfoVO, StringBuffer productId) throws ScanSeeServiceException;

	public String userRedeemCLRDiscount(Long userId, Integer clrId, String clrFlag) throws ScanSeeServiceException;

	/**
	 * This method is used to fetch the products details associated with the
	 * coupon . accepts a GET request in mime type text/plain
	 * 
	 * @param clrDetails
	 * @param clrType
	 * @return ArrayList<ProductDetail>
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<ProductDetail> getCLRProductInfo(CLRDetails clrDetails, String clrType) throws ScanSeeServiceException;

	/**
	 * this method is used to get the categories.
	 * 
	 * @return ArrayList<CategoryInfo>
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<CategoryInfo> getCategories() throws ScanSeeServiceException;

	/**
	 * this method is used to add products to shoppingList.
	 * 
	 * @param areaInfoVO
	 * @param productId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String addShoppingListProd(AreaInfoVO areaInfoVO, StringBuffer productId) throws ScanSeeServiceException;

}

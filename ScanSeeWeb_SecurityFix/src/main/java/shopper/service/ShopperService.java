package shopper.service;

import java.util.ArrayList;
import java.util.List;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import common.exception.ScanSeeServiceException;
import common.pojo.DoubleCheckInfo;
import common.pojo.FaceBookConfiguration;
import common.pojo.NonProfitPartnerInfo;
import common.pojo.Preferences;
import common.pojo.SchoolInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Universities;
import common.pojo.UserPreference;
import common.pojo.UserPreferenceDisplay;
import common.pojo.Users;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.DiscountInfo;
import common.pojo.shopper.DiscountUserInfo;
import common.pojo.shopper.DoubleCheck;
import common.pojo.shopper.ProductDetail;

/**
 * This interface consists of service layer methods for shopper.
 * @author manjunath_gh
 *
 */
public interface ShopperService
{
	/**
	 * The method for processFaceBookUser. Calls the XStreams helper class
	 * methods for parsing.
	 * 
	 * @param inputXml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	Users processFaceBookUser(String fbUserID) throws ScanSeeServiceException;

	/**
	 *
	 * This method saves shopper registration information.
	 *
	 * @param shopperRegistrationInfo
	 * @param strLogoImage
	 * @return
	 * @throws ScanSeeServiceException
	 */
	Users processShopperProfileDetails(Users shopperRegistrationInfo, String strLogoImage) throws ScanSeeServiceException;

	public String processPreferenceInfo(CategoryDetail categoryDetail, Long userId) throws ScanSeeServiceException;

	public String processSchoolInfo(SchoolInfo schoolInfo) throws ScanSeeServiceException;

	public String processDoubleCheckInfo(DoubleCheckInfo doubleCheckInfo) throws ScanSeeServiceException;

	public List<Preferences> getShopperPreferences() throws ScanSeeServiceException;

	public List<Universities> getUniversities() throws ScanSeeServiceException;

	/**
	 *  This method for getting the All Non Profit Partners List.
	 * @return List<NonProfitPartnerInfo>
	 * @throws ScanSeeServiceException
	 */
	public List<NonProfitPartnerInfo> getNonProfitPartners() throws ScanSeeServiceException;

	/**
	 * This method returns shopper registered information.
	 * @param shopperId
	 * @return UserPreferenceDisplay
	 * @throws ScanSeeServiceException
	 */
	public UserPreferenceDisplay getDoubleCheckDetails(Long shopperId) throws ScanSeeServiceException;

	/**
	 * This method for getting the All state List based .
	 * @return ArrayList<SchoolInfo>
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<SchoolInfo> getAllStates() throws ScanSeeServiceException;

	// public ArrayList<City> getAllCities(String stateCode) throws
	// ScanSeeServiceException;

	
	/**
	 * This method for getting the All College List based on the state code.
	 * @param stateCode
	 * @return ArrayList<SchoolInfo>
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<SchoolInfo> getAllColleges(String stateCode) throws ScanSeeServiceException;

	/**
	 * This method for saving the all the school information.
	 * @param schoolInfo
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String saveSchoolInfo(SchoolInfo schoolInfo) throws ScanSeeServiceException;
	/**
	 * This method for saving the all the school information.
	 * @param userId
	 * @return SchoolInfo
	 * @throws ScanSeeServiceException
	 */
	public SchoolInfo displaySchoolInfo(Long userId) throws ScanSeeServiceException;

	// public List<ProductVO> getAllSearchProductList()throws
	// ScanSeeServiceException;

	public SearchResultInfo searchProducts(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException;

	public DiscountInfo getDiscountInfo(DiscountUserInfo discountUserInfo) throws ScanSeeServiceException;

	public String couponAdd(Long userId, Integer couponId) throws ScanSeeServiceException;

	public String couponRemove(Long userId, Integer couponId) throws ScanSeeServiceException;

	public String rebateAdd(Long userId, Integer couponId) throws ScanSeeServiceException;

	public String rebateRemove(Long userId, Integer couponId) throws ScanSeeServiceException;

	public String loyaltyAdd(Long userId, Integer couponId) throws ScanSeeServiceException;

	public String loyaltyRemove(Long userId, Integer couponId) throws ScanSeeServiceException;

	public ExternalAPIInformation externalApiInfo(String moduleName) throws ScanSeeServiceException;

	public ProductDetail fetchProductDetails(String productId) throws ScanSeeServiceException;

	public String getUniversityName(Integer universityId) throws ScanSeeServiceException;

	/**
	 * The service method for fetching user favorite categories information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @param lowerlimit
	 *            for pagination
	 * @return XML containing user favourite categories information in the
	 *         response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	public CategoryDetail getUserFavCategories(Long userId, Integer lowerlimit) throws ScanSeeServiceException;

	/**
	 * The Service method for Saving User favourite categories Information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param Categories
	 *            Contains user favourite categories information.
	 * @return returns SUCCESS or ERROR.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	String setUserFavCategories(String categoryId, Long userID, boolean cellPhoneStatus, boolean emailStatus) throws ScanSeeServiceException;

	public List<DoubleCheck> getAllpreferences(Long UserId) throws ScanSeeServiceException;

	/**
	 * The Service method for updating the user information
	 * 
	 * @param UserPreference
	 * @return returns SUCCESS or ERROR.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	String updateUserData(UserPreference userPreference) throws ScanSeeServiceException;

	FaceBookConfiguration getFaceBookConfiguration() throws ScanSeeServiceException;

	String getShareEmailConfiguration() throws ScanSeeServiceException;
	
	/**
	 * This method is used to domain Name for Application configuration.
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String getDomainName() throws ScanSeeServiceException;
	
	
}

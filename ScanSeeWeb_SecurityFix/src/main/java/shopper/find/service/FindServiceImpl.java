package shopper.find.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.find.dao.FindDAO;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.shopper.GoogleCategory;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailerPage;
import common.pojo.shopper.RetailersDetails;

/**
 * @author pradip_k
 */
public class FindServiceImpl implements FindService
{

	private FindDAO findDAO;
	/**
	 * Getting the logger instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(FindServiceImpl.class.getName());

	/**
	 * This methods to get find DAO.
	 * 
	 * @return the findDAO
	 */
	public FindDAO getFindDAO()
	{
		return findDAO;
	}

	/**
	 * This method to set find DAO.
	 * 
	 * @param findDAO
	 *            the findDAO to set
	 */
	public void setFindDAO(FindDAO findDAO)
	{
		this.findDAO = findDAO;
	}

	/**
	 * This service implementation method for getting Category services. Calls
	 * the XStreams helper class methods for parsing.
	 * 
	 * @param userId
	 *            as request parameter
	 * @return response string with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public List<GoogleCategory> getCategoryInfo(Long userId) throws ScanSeeServiceException
	{
		final String methodName = "getCategoryInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<GoogleCategory> categorylst = null;

		try
		{
			categorylst = findDAO.getCategoryDetail();
		}
		catch (ScanSeeWebSqlException e)
		{

			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return categorylst;

	}

	public ProductDetails searchProducts(SearchForm searchFormObj) throws ScanSeeWebSqlException
	{
		final String methodName = "scanForProdName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetails productDetails = findDAO.fetchAllProduct(searchFormObj);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;

	}

	/**
	 * This service method is used to fetch retail locations.
	 * 
	 * @param consumerRequestDetails
	 *            as a parameter which holds the data required to fetch retail
	 *            locations
	 * @return retail location list
	 * @throws ScanSeeServiceException
	 */

	public RetailersDetails searchRetailLocation(ConsumerRequestDetails consumerRequestDetails) throws ScanSeeServiceException
	{
		final String methodName = "scanForProdName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailersDetails retailersDetails = null;
		try
		{
			retailersDetails = findDAO.searchRetailLocation(consumerRequestDetails);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Exception occurred in getanythingpagelist method:" + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailersDetails;

	}

	public HotDealsListResultSet findDealsSearch(SearchForm searchFormObj) throws ScanSeeWebSqlException
	{
		final String methodName = "findDealsSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		HotDealsListResultSet hotDealsListResultSetObj = null;
		hotDealsListResultSetObj = findDAO.findDealsSearch(searchFormObj);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsListResultSetObj;
	}

	/**
	 * This service method is used to get Main Menu id which is used for user
	 * tracking.
	 * 
	 * @param consumerRequestDetails
	 *            as a parameter which holds the details required to get main
	 *            menu id from database
	 * @return main menu id
	 * @throws ScanSeeServiceException
	 */
	public int getMainMenuID(ConsumerRequestDetails consumerRequestDetails) throws ScanSeeServiceException
	{
		final String methodName = "getMainMenuID";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int mainMenuId = 0;
		try
		{
			mainMenuId = findDAO.getMainMenuID(consumerRequestDetails);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED + " in getMainMenuID method ", e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return mainMenuId;
	}

	/**
	 * This service method is used to get retailer created anything pages.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get
	 *            anything pages
	 * @return anything page list in HTML format as a string
	 * @throws ScanSeeServiceException
	 */
	public String getRetailerAnythigPageList(RetailerDetail retailerDetail) throws ScanSeeServiceException
	{
		final String methodName = "getRetailerAnythigPageList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerPage> anythingPageList = null;
		StringBuilder anyThingPageListHtml = new StringBuilder();
		String scanTypeId = "0";
		RetailerPage page = null;
		try
		{
			anythingPageList = findDAO.getRetailerAnythigPageList(retailerDetail);

			if (null != anythingPageList && !anythingPageList.isEmpty())
			{
				anyThingPageListHtml.append("<ul class=\"ltPnl iphoneArea floatL listView\">");

				page = anythingPageList.get(0);

				if ((null != page.getRibbonAdImagePath() && !"".equals(page.getRibbonAdImagePath()))
						&& (null != page.getRibbonAdURL() && !"".equals(page.getRibbonAdURL())))
				{

					anyThingPageListHtml.append("<li class=\"bannerAd\">");
					anyThingPageListHtml.append("<a  href=\"" + page.getRibbonAdURL() + "\" target=\"_blank\">");
					anyThingPageListHtml.append("<img src=\"" + page.getRibbonAdImagePath()
							+ "\" width=\"320\" height=\"50\" align=\"absmiddle\" alt=\"image\" />");

					anyThingPageListHtml.append("</a>");
					anyThingPageListHtml.append("</li>");

				}

				for (RetailerPage retailerPage : anythingPageList)
				{
					anyThingPageListHtml.append("<li>");
					anyThingPageListHtml.append("<img src=\"" + retailerPage.getPageImage() + "\" width=\"46\" height=\"46\" align=\"absmiddle\" />");

					anyThingPageListHtml.append("<span>");

					if (retailerPage.isExternalQR())
					{

						anyThingPageListHtml
								.append("<a href=\"" + retailerPage.getPageLink() + "&" + ApplicationConstants.MAINMENUID + "="
										+ retailerDetail.getMainMenuId() + "&" + ApplicationConstants.SCANTYPEID + "=" + scanTypeId
										+ "\" target=\"_blank\">");
						anyThingPageListHtml.append(retailerPage.getPageTitle());
						anyThingPageListHtml.append("</a>");
					}
					else
					{

						anyThingPageListHtml.append("<a  href=\"#\" onclick=\"loadAnythingPageDetail('" + retailerPage.getPageLink() + "&"
								+ ApplicationConstants.MAINMENUID + "=" + retailerDetail.getMainMenuId() + "&" + ApplicationConstants.SCANTYPEID
								+ "=" + scanTypeId + "')\">");
						anyThingPageListHtml.append(retailerPage.getPageTitle());
						anyThingPageListHtml.append("</a>");
					}

					anyThingPageListHtml.append("</span>");
					anyThingPageListHtml.append("</li>");
				}
				anyThingPageListHtml.append("</ul>");
				anyThingPageListHtml.append(ApplicationConstants.ANYTHINGPAGEDETAILDIV);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED + " in getRetailerAnythigPageList method ", e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return anyThingPageListHtml.toString();
	}

	/**
	 * This service method is used to get special offers page list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get special
	 *            offers page list.
	 * @return retailersDetails object having special offers list
	 * @throws ScanSeeServiceException
	 */
	public RetailersDetails getRetailerSpecialOfferPageList(RetailerDetail retailerDetail) throws ScanSeeServiceException
	{
		final String methodName = "getRetailerSpecialOfferPageList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailersDetails retailersDetailsObj = null;

		try
		{
			retailersDetailsObj = findDAO.getRetailerSpecialOfferPageList(retailerDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED + " in getRetailerSpecialOfferPageList method ", e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailersDetailsObj;
	}

	/**
	 * This service method is used to get sale products list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get sale
	 *            products list.
	 * @return productDetailsObj object having sale product list
	 * @throws ScanSeeServiceException
	 */
	public ProductDetails getSaleProductList(RetailerDetail retailerDetail) throws ScanSeeServiceException
	{
		final String methodName = "getSaleProductList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetails productDetailsObj = null;

		try
		{
			productDetailsObj = findDAO.getSaleProductList(retailerDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED + " in getSaleProductList method ", e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetailsObj;
	}

	/**
	 * This service method is used to get hot deals list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get hot
	 *            deals list.
	 * @return searchResultInfo object having hot deals list
	 * @throws ScanSeeServiceException
	 */
	public SearchResultInfo getHotDealList(RetailerDetail retailerDetail) throws ScanSeeServiceException
	{
		final String methodName = "getHotDealList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo searchResultInfo = null;

		try
		{
			searchResultInfo = findDAO.getHotDealList(retailerDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED + " in getHotDealList method ", e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchResultInfo;
	}

	/**
	 * This service method is used to get special offer page to be displayed.
	 * 
	 * @param userId
	 *            as a parameter
	 * @param retailId
	 *            as a parameter
	 * @param retailLocId
	 *            as a parameter
	 * @param retailLocListId
	 *            as a parameter
	 * @return hashMap containing the flag for the page to be displayed
	 * @throws ScanSeeServiceException
	 */
	public HashMap<String, Boolean> checkDiscountPageToDisplay(Long userId, Integer retailId, Integer retailLocId, Integer retailLocListId)
			throws ScanSeeServiceException
	{
		final String methodName = "checkDiscountPageToDisplay";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		HashMap<String, Boolean> mapDiaplayPage = null;
		try
		{
			mapDiaplayPage = findDAO.checkDiscountPageToDisplay(userId, retailId, retailLocId, retailLocListId);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED + " in checkDiscountPageToDisplay method ", e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return mapDiaplayPage;
	}
}

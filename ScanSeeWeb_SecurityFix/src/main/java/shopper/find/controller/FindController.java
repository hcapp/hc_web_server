package shopper.find.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.find.service.FindService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.Users;
import common.pojo.shopper.GoogleCategory;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.tags.Pagination;
import common.util.Utility;

@Controller
public class FindController
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(FindController.class);

	@RequestMapping(value = "/findDisplay.htm", method = RequestMethod.GET)
	public ModelAndView showFind(@ModelAttribute("SearchForm") SearchForm searchForm, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws IOException
	{
		final String methodName = "showFind";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String view = null;
		session.removeAttribute("pagination");
		List<GoogleCategory> categoryInfo = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		FindService findService = (FindService) appContext.getBean("findService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		try
		{
			categoryInfo = findService.getCategoryInfo(userId);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Error occureed in findDisplay.htm", e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		request.setAttribute("categoryInfo", categoryInfo);
		view = "findDisplay";
		return new ModelAndView(view);
	}

	@RequestMapping(value = "/findproductsearch.htm", method = RequestMethod.POST)
	public ModelAndView productSearch(@ModelAttribute("SearchForm") SearchForm searchForm, HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1)
	{

		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		Integer objRadius = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";
		String view = null;
		Pagination objPage = null;
		// SearchForm objForm = null;
		if (searchForm.getSearchKey() == null)
		{
			searchForm = (SearchForm) session.getAttribute("searchForm");
		}
		// Integer retailerId = searchForm.getRetailerId();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		FindService findService = (FindService) appContext.getBean("findService");

		/*
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 */
		if (null != pageFlag && pageFlag.equals("true"))
		{
			searchForm = (SearchForm) session.getAttribute("searchForm");
			searchType = searchForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}
		else
		{

			searhKey = searchForm.getSearchKey();

		}
		try
		{
			searchForm.setUserId(loginUser.getUserID());
			searchForm.setLastVisitedNo(lowerLimit);
			ProductDetails productDetailsObj = findService.searchProducts(searchForm);

			
			// TODO: Remove after testing
			//ProductDetails productDetailsObj = null;
			productDetailsObj = populateProductDetails(productDetailsObj);
			
			
			
			if (null != productDetailsObj)
			{
				objPage = Utility.getPagination(productDetailsObj.getTotalSize(), currentPage, "findproductsearch.htm");

			}

		
			
			if (productDetailsObj != null)
			{
				request.setAttribute("searchresults", productDetailsObj);
			}
			else
			{
				request.setAttribute("message", "No product found.");
				request.setAttribute("noproductfound", "true");
			}
			view = "productList";
			view = "findDisplay";
			session.setAttribute("prodSearch", "true");
			session.setAttribute("pagination", objPage);
			session.setAttribute("searchForm", searchForm);
			session.setAttribute("fromFind", "fromFind");
			session.removeAttribute("FromSLSearch");
			session.removeAttribute("fromSLFavSearch");
			session.removeAttribute("FromWLSearch");
			session.removeAttribute("prodSearch");
			session.removeAttribute("fromWLHistory");
			session.removeAttribute("fromscannow");
			session.removeAttribute("fromSL");
			session.removeAttribute("fromSLHistory");
			session.removeAttribute("fromSLFAV");
			session.removeAttribute("fromWL");
			request.setAttribute("searchType", searchForm.getSearchType());

		}
		catch (ScanSeeWebSqlException e)
		{

			e.printStackTrace();
		}

		return new ModelAndView("findDisplay");

	}

	private ProductDetails populateProductDetails(ProductDetails productDetailsObj)throws ScanSeeWebSqlException
	{
		
		if(null == productDetailsObj)
		{
			productDetailsObj = new ProductDetails();
		}
		
		List<ProductDetail> arrList = new ArrayList<ProductDetail>();
		
		ProductDetail prodDetail = new ProductDetail();
		prodDetail.setProductId(37);
		prodDetail.setProductName("SkateBoard");
		prodDetail.setProductShortDescription("A skateboard is a type of sports equipment used primarily for the activity of skateboarding");
		prodDetail.setProductLongDescription("It usually consists of a specially designed maplewood board combined with a polyurethane coating used for making smoother slides and stronger durability." +
				"A skateboard is propelled by pushing with one foot while the other remains on the board, or by pumping one's legs in structures such as a pool or half pipe");
		arrList.add(prodDetail);
		
		
		prodDetail = new ProductDetail();
		prodDetail.setProductId(18);
		prodDetail.setProductName("Alienware");
		prodDetail.setProductShortDescription("Alienware is a United States computer hardware subsidiary of Dell, Inc");
		prodDetail.setProductLongDescription(" It mainly assembles third party components into desktops and laptops with custom enclosures. " +
				"Alienware also offers for sale rebadged computer peripherals, such as headsets, computer mic, monitors and keyboards. Their hardware has a distinctive 'sci-fi' style, typically including decorative lighting");
		arrList.add(prodDetail);
		
		prodDetail = new ProductDetail();
		prodDetail.setProductId(1837);
		prodDetail.setProductName("SLR cameras");
		prodDetail.setProductShortDescription("Canon began to develop and subsequently to produce film SLR cameras with its presentation of the Kwanon prototype in 1933.");
		prodDetail.setProductLongDescription("Canon partnered with US manufacturer Bell & Howell between 1961–1976 and a few Canon products were sold in the USA under the Bell & Howell brand e.g. Canon 7 Rangefinder, Canon EX-EE, and the Canon TX. " +
				"In 1987, Canon introduced the EOS Single-lens reflex camera system along with the EF lens-mount standard to replace the 16-year-old FD lens-mount standard; EOS became the sole SLR camera-system used by Canon today. Canon also used EOS for its digital SLR cameras");
		arrList.add(prodDetail);
		
		prodDetail = new ProductDetail();
		prodDetail.setProductId(3718);
		prodDetail.setProductName("PS3");
		prodDetail.setProductShortDescription("The PlayStation 3 is the third home video game console produced by Sony Computer Entertainment and the successor to the PlayStation 2 as part of the PlayStation series");
		prodDetail.setProductLongDescription("Sony officially unveiled the PlayStation 3 (then marketed as PLAYSTATION 3[15]) to the public on May 16, 2005 at the E3 2005 conference,[16] along with a 'boomerang' shaped prototype design of the Sixaxis controller.[17] A functional version of the system was not present there,[18] nor at the Tokyo Game Show in September 2005,[19] although demonstrations (such as Metal Gear Solid 4: Guns of the Patriots[18]) were held at both events on software development kits and comparable personal computer hardware");
		arrList.add(prodDetail);
		
		productDetailsObj.setProductDetail(arrList);
		
		return productDetailsObj;
	}
	
	@RequestMapping(value = "/findproduct.htm", method = RequestMethod.GET)
	public ModelAndView productDisplay(@ModelAttribute("SearchForm") SearchForm searchForm, HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1)
	{

//		int lowerLimit = 0;
//		String pageFlag = (String) request.getParameter("pageFlag");
//		Users loginUser = (Users) session.getAttribute("loginuser");
//		String zipCode = null;
//		Integer objRadius = null;
//		String searhKey = null;
//		String searchType = null;
//		int currentPage = 1;
//		String pageNumber = "0";
//		String view = null;
//		Pagination objPage = null;
//		// SearchForm objForm = null;
//		if (searchForm.getSearchKey() == null)
//		{
//			searchForm = (SearchForm) session.getAttribute("searchForm");
//		}
//		// Integer retailerId = searchForm.getRetailerId();
//		ServletContext servletContext = request.getSession().getServletContext();
//		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
//		FindService findService = (FindService) appContext.getBean("findService");
//
//		/*
//		 * 1) Store search parameters in Session object 2) check and get the
//		 * Parameters in session object 3) Check login state and deligate based
//		 * on state 4) Pagination
//		 */
//		if (null != pageFlag && pageFlag.equals("true"))
//		{
//			searchForm = (SearchForm) session.getAttribute("searchForm");
//			searchType = searchForm.getSearchType();
//			pageNumber = request.getParameter("pageNumber");
//			Pagination pageSess = (Pagination) session.getAttribute("pagination");
//			if (Integer.valueOf(pageNumber) != 0)
//			{
//				currentPage = Integer.valueOf(pageNumber);
//				int number = Integer.valueOf(currentPage) - 1;
//				int pageSize = pageSess.getPageRange();
//				lowerLimit = pageSize * Integer.valueOf(number);
//			}
//			else
//			{
//				lowerLimit = 0;
//			}
//
//		}
//		else
//		{
//
//			searhKey = searchForm.getSearchKey();
//
//		}
//		try
//		{
//			searchForm.setUserId(loginUser.getUserID());
//			searchForm.setLastVisitedNo(lowerLimit);
//			ProductDetails productDetailsObj = findService.searchProducts(searchForm);
//
//			
//			// TODO: Remove after testing
//			//ProductDetails productDetailsObj = null;
//			productDetailsObj = populateProductDetails(productDetailsObj);
//			
//			
//			
//			if (null != productDetailsObj)
//			{
//				objPage = Utility.getPagination(productDetailsObj.getTotalSize(), currentPage, "findproductsearch.htm");
//
//			}
//
//		
//			
//			if (productDetailsObj != null)
//			{
//				request.setAttribute("searchresults", productDetailsObj);
//			}
//			else
//			{
//				request.setAttribute("message", "No product found.");
//				request.setAttribute("noproductfound", "true");
//			}
//			view = "productList";
//			view = "findDisplay";
//			session.setAttribute("prodSearch", "true");
//			session.setAttribute("pagination", objPage);
//			session.setAttribute("searchForm", searchForm);
//			session.setAttribute("fromFind", "fromFind");
//			session.removeAttribute("FromSLSearch");
//			session.removeAttribute("fromSLFavSearch");
//			session.removeAttribute("FromWLSearch");
//			session.removeAttribute("prodSearch");
//			session.removeAttribute("fromWLHistory");
//			session.removeAttribute("fromscannow");
//			session.removeAttribute("fromSL");
//			session.removeAttribute("fromSLHistory");
//			session.removeAttribute("fromSLFAV");
//			session.removeAttribute("fromWL");
//			request.setAttribute("searchType", searchForm.getSearchType());
//
//		}
//		catch (ScanSeeWebSqlException e)
//		{
//
//			e.printStackTrace();
//		}

		return new ModelAndView("findProduct");

	}


}

package shopper.find;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.HotDealsResultSet;
import common.util.Utility;

public class FindHelper {

	/**
	 * Fetching hot deals list.
	 * @param hotDealsResultSet
	 * @return
	 */
	public static HotDealsListResultSet getHotDealsList(ArrayList<HotDealsResultSet> hotDealsResultSet)
	{
		
		final HotDealsListResultSet hotDealsListResultSet = new HotDealsListResultSet();
		String key = null;
		final HashMap<String, HotDealsCategoryInfo> hotDealsCategoryInfoMap = new HashMap<String, HotDealsCategoryInfo>();

		ArrayList<HotDealsDetails> hotDealsDetailslst;
		HotDealsCategoryInfo hotDealsCategoryInfo = null;
		for (HotDealsResultSet hotDealsDetailslist : hotDealsResultSet)
		{
			key = hotDealsDetailslist.getHdCategoryName();
			if (!"".equals(Utility.isNull(key)))
			{
				key = key.toUpperCase();
				if (hotDealsCategoryInfoMap.containsKey(key))
				{
					hotDealsCategoryInfo = hotDealsCategoryInfoMap.get(key);
					hotDealsDetailslst = hotDealsCategoryInfo.getHotDealsDetailsArrayLst();
					if (null != hotDealsDetailslst)
					{
						final HotDealsDetails hotDealsDetailsObj = new HotDealsDetails();
						//hotDealsDetailsObj.setApiPartnerId(hotDealsDetailslist.getApiPartnerId());
						hotDealsDetailsObj.setApiPartnerName(hotDealsDetailslist.getApiPartnerName());
						hotDealsDetailsObj.setHotDealName(hotDealsDetailslist.getHotDealName());
						hotDealsDetailsObj.setHotDealId(hotDealsDetailslist.getHotDealId());
						hotDealsDetailsObj.setHotDealImagePath(hotDealsDetailslist.getHotDealImagePath());
						hotDealsDetailsObj.sethDshortDescription(hotDealsDetailslist.gethDshortDescription());
						hotDealsDetailsObj.sethDLognDescription(hotDealsDetailslist.gethDLognDescription());
						hotDealsDetailsObj.sethDPrice(hotDealsDetailslist.gethDPrice());
						hotDealsDetailsObj.sethDSalePrice(hotDealsDetailslist.gethDSalePrice());
						//hotDealsDetailsObj.sethDDiscountAmount(hotDealsDetailslist.gethDDiscountAmount());
					//	hotDealsDetailsObj.sethDDiscountPct(hotDealsDetailslist.gethDDiscountPct());
						hotDealsDetailsObj.setRowNumber(hotDealsDetailslist.getRowNumber());
						hotDealsDetailsObj.setHdURL(hotDealsDetailslist.getHdURL());
					//	hotDealsDetailsObj.setDistance(hotDealsDetailslist.getDistance());
						hotDealsDetailsObj.setCity(hotDealsDetailslist.getCity());
						hotDealsDetailsObj.setHotdealLstId(hotDealsDetailslist.getHotdealLstId());
						hotDealsDetailslst.add(hotDealsDetailsObj);
						hotDealsCategoryInfo.setHotDealsDetailsArrayLst(hotDealsDetailslst);
					}
				}
				else
				{
					hotDealsCategoryInfo = new HotDealsCategoryInfo();
					hotDealsCategoryInfo.setCategoryId(hotDealsDetailslist.getHdCatgoryId());
					hotDealsCategoryInfo.setCategoryName(hotDealsDetailslist.getHdCategoryName());
					hotDealsDetailslst = new ArrayList<HotDealsDetails>();
					final HotDealsDetails hotDealsDetailsObj = new HotDealsDetails();
			//		hotDealsDetailsObj.setApiPartnerId(hotDealsDetailslist.getApiPartnerId());
					hotDealsDetailsObj.setApiPartnerName(hotDealsDetailslist.getApiPartnerName());
					hotDealsDetailsObj.setHotDealName(hotDealsDetailslist.getHotDealName());
					hotDealsDetailsObj.setHotDealId(hotDealsDetailslist.getHotDealId());
					hotDealsDetailsObj.setHotDealImagePath(hotDealsDetailslist.getHotDealImagePath());
					hotDealsDetailsObj.sethDshortDescription(hotDealsDetailslist.gethDshortDescription());
					hotDealsDetailsObj.sethDLognDescription(hotDealsDetailslist.gethDLognDescription());
					hotDealsDetailsObj.sethDPrice(hotDealsDetailslist.gethDPrice());
					hotDealsDetailsObj.sethDSalePrice(hotDealsDetailslist.gethDSalePrice());
					//hotDealsDetailsObj.sethDDiscountAmount(hotDealsDetailslist.gethDDiscountAmount());
					//hotDealsDetailsObj.sethDDiscountPct(hotDealsDetailslist.gethDDiscountPct());
					hotDealsDetailsObj.setRowNumber(hotDealsDetailslist.getRowNumber());
					hotDealsDetailsObj.setHdURL(hotDealsDetailslist.getHdURL());
				//	hotDealsDetailsObj.setDistance(hotDealsDetailslist.getDistance());
					hotDealsDetailsObj.setCity(hotDealsDetailslist.getCity());
					hotDealsDetailsObj.setHotdealLstId(hotDealsDetailslist.getHotdealLstId());
					hotDealsDetailslst.add(hotDealsDetailsObj);
					hotDealsCategoryInfo.setHotDealsDetailsArrayLst(hotDealsDetailslst);
				}

				hotDealsCategoryInfoMap.put(key, hotDealsCategoryInfo);
			}

		}
		final Set<Map.Entry<String, HotDealsCategoryInfo>> set = hotDealsCategoryInfoMap.entrySet();

		final ArrayList<HotDealsCategoryInfo> hotDealsCategoryInfolst = new ArrayList<HotDealsCategoryInfo>();

		for (Map.Entry<String, HotDealsCategoryInfo> entry : set)
		{
			hotDealsCategoryInfolst.add(entry.getValue());
		}

		hotDealsListResultSet.setHotDealsCategoryInfo(hotDealsCategoryInfolst);
		return hotDealsListResultSet;
	}
    
}

package shopper.hotdeals.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.helper.SortHotDealByCategory;

import common.pojo.shopper.HotDealAPIResultSet;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.HotDealsResultSet;
import common.util.Utility;



/**
 * This class for implementing hot deals list for displaying category wise.
 * 
 * @author shyamsundara_hm
 */
public class HotDealsHelper
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(HotDealsHelper.class);

	/**
	 * for HotDealsHelper constructor.
	 */
	protected HotDealsHelper()
	{
		LOG.info("Inside HotDealsHelper class");
	}

	/**
	 * For displaying hotdeal products category wise.
	 * 
	 * @param hotDealsResultSet
	 *            as request parameter.
	 * @return hotDealsListResultSet as response parameter.
	 */
	public static HotDealsListResultSet getHotDealsList(ArrayList<HotDealsResultSet> hotDealsResultSet)
	{
		final HotDealsListResultSet hotDealsListResultSet = new HotDealsListResultSet();
		String key = null;
		final HashMap<String, HotDealsCategoryInfo> hotDealsCategoryInfoMap = new HashMap<String, HotDealsCategoryInfo>();

		ArrayList<HotDealsDetails> hotDealsDetailslst;
		HotDealsCategoryInfo hotDealsCategoryInfo = null;
		for (HotDealsResultSet hotDealsDetailslist : hotDealsResultSet)
		{
			key = hotDealsDetailslist.getCategoryName();
			if (!"".equals(Utility.isNull(key)))
			{
				key = key.toUpperCase();
				if (hotDealsCategoryInfoMap.containsKey(key))
				{
					hotDealsCategoryInfo = hotDealsCategoryInfoMap.get(key);
					hotDealsDetailslst = hotDealsCategoryInfo.getHotDealsDetailsArrayLst();
					if (null != hotDealsDetailslst)
					{
						final HotDealsDetails hotDealsDetailsObj = new HotDealsDetails();
						hotDealsDetailsObj.setApiPartnerId(hotDealsDetailslist.getApiPartnerId());
						hotDealsDetailsObj.setApiPartnerName(hotDealsDetailslist.getApiPartnerName());
						hotDealsDetailsObj.setHotDealName(hotDealsDetailslist.getHotDealName());
						hotDealsDetailsObj.setHotDealId(hotDealsDetailslist.getHotDealId());
						hotDealsDetailsObj.setHotDealImagePath(hotDealsDetailslist.getHotDealImagePath());
						hotDealsDetailsObj.sethDshortDescription(hotDealsDetailslist.gethDshortDescription());
						hotDealsDetailsObj.sethDPrice(hotDealsDetailslist.gethDPrice());
						hotDealsDetailsObj.sethDSalePrice(hotDealsDetailslist.gethDSalePrice());
						hotDealsDetailsObj.sethDDiscountAmount(hotDealsDetailslist.gethDDiscountAmount());
						hotDealsDetailsObj.sethDDiscountPct(hotDealsDetailslist.gethDDiscountPct());
						hotDealsDetailsObj.setProductID(hotDealsDetailslist.getProductID());
						hotDealsDetailsObj.setRowNumber(hotDealsDetailslist.getRowNumber());
						hotDealsDetailsObj.setHdURL(hotDealsDetailslist.getHdURL());
						hotDealsDetailsObj.setDistance(hotDealsDetailslist.getDistance());
						hotDealsDetailsObj.setCity(hotDealsDetailslist.getCity());
						hotDealsDetailsObj.setHotdealLstId(hotDealsDetailslist.getHotdealLstId());
						hotDealsDetailslst.add(hotDealsDetailsObj);
						hotDealsCategoryInfo.setHotDealsDetailsArrayLst(hotDealsDetailslst);
					}
				}
				else
				{
					hotDealsCategoryInfo = new HotDealsCategoryInfo();
					hotDealsCategoryInfo.setCategoryId(hotDealsDetailslist.getCategoryId());
					hotDealsCategoryInfo.setCategoryName(hotDealsDetailslist.getCategoryName());
					hotDealsDetailslst = new ArrayList<HotDealsDetails>();
					final HotDealsDetails hotDealsDetailsObj = new HotDealsDetails();
					hotDealsDetailsObj.setApiPartnerId(hotDealsDetailslist.getApiPartnerId());
					hotDealsDetailsObj.setApiPartnerName(hotDealsDetailslist.getApiPartnerName());
					hotDealsDetailsObj.setHotDealName(hotDealsDetailslist.getHotDealName());
					hotDealsDetailsObj.setHotDealId(hotDealsDetailslist.getHotDealId());
					hotDealsDetailsObj.setHotDealImagePath(hotDealsDetailslist.getHotDealImagePath());
					hotDealsDetailsObj.sethDshortDescription(hotDealsDetailslist.gethDshortDescription());
					hotDealsDetailsObj.sethDPrice(hotDealsDetailslist.gethDPrice());
					hotDealsDetailsObj.sethDSalePrice(hotDealsDetailslist.gethDSalePrice());
					hotDealsDetailsObj.sethDDiscountAmount(hotDealsDetailslist.gethDDiscountAmount());
					hotDealsDetailsObj.sethDDiscountPct(hotDealsDetailslist.gethDDiscountPct());
					hotDealsDetailsObj.setProductID(hotDealsDetailslist.getProductID());
					hotDealsDetailsObj.setRowNumber(hotDealsDetailslist.getRowNumber());
					hotDealsDetailsObj.setHdURL(hotDealsDetailslist.getHdURL());
					hotDealsDetailsObj.setDistance(hotDealsDetailslist.getDistance());
					hotDealsDetailsObj.setCity(hotDealsDetailslist.getCity());
					hotDealsDetailsObj.setHotdealLstId(hotDealsDetailslist.getHotdealLstId());
					hotDealsDetailslst.add(hotDealsDetailsObj);
					hotDealsCategoryInfo.setHotDealsDetailsArrayLst(hotDealsDetailslst);
				}

				hotDealsCategoryInfoMap.put(key, hotDealsCategoryInfo);
			}

		}
		final Set<Map.Entry<String, HotDealsCategoryInfo>> set = hotDealsCategoryInfoMap.entrySet();

		final ArrayList<HotDealsCategoryInfo> hotDealsCategoryInfolst = new ArrayList<HotDealsCategoryInfo>();

		for (Map.Entry<String, HotDealsCategoryInfo> entry : set)
		{
			hotDealsCategoryInfolst.add(entry.getValue());
		}
		SortHotDealByCategory objSortHDbyCategory =  new SortHotDealByCategory();
		Collections.sort(hotDealsCategoryInfolst, objSortHDbyCategory);
		hotDealsListResultSet.setHotDealsCategoryInfo(hotDealsCategoryInfolst);

		
		return hotDealsListResultSet;
	}
    
	/**
	 * This method is used to get Hot deasl APi list.
	 * @param hotDealsDetailsArrayLst
	 *          -As HotDealsDetails object
	 * @return hotDealsCategoryInfolst
	 */
	public static ArrayList<HotDealAPIResultSet> getHotDealsAPIList(ArrayList<HotDealsDetails> hotDealsDetailsArrayLst)
	{
		String key = null;
		final HashMap<String, HotDealAPIResultSet> hotDealsAPIInfoMap = new HashMap<String, HotDealAPIResultSet>();

		HotDealAPIResultSet hotDealAPIResultSet = null;
		ArrayList<HotDealsDetails> hotDealsDetailsLst = null;
		for (HotDealsDetails hotDealsDetails : hotDealsDetailsArrayLst)
		{
			key = Utility.nullCheck(hotDealsDetails.getApiPartnerName());
			if (hotDealsAPIInfoMap.containsKey(key))
			{
				hotDealAPIResultSet = hotDealsAPIInfoMap.get(key);
				hotDealsDetailsLst = hotDealAPIResultSet.getHotDealsDetailslst();
				if (null != hotDealsDetailsLst)
				{
					final HotDealsDetails hotDealsDetailsObj = new HotDealsDetails();
					hotDealsDetailsObj.setApiPartnerId(hotDealsDetails.getApiPartnerId());
					hotDealsDetailsObj.setApiPartnerName(hotDealsDetails.getApiPartnerName());
					hotDealsDetailsObj.setHotDealName(hotDealsDetails.getHotDealName());
					hotDealsDetailsObj.setHotDealId(hotDealsDetails.getHotDealId());
					hotDealsDetailsObj.setHotDealImagePath(hotDealsDetails.getHotDealImagePath());
					hotDealsDetailsObj.sethDshortDescription(hotDealsDetails.gethDshortDescription());
					hotDealsDetailsObj.sethDPrice(hotDealsDetails.gethDPrice());
					hotDealsDetailsObj.sethDSalePrice(hotDealsDetails.gethDSalePrice());
					hotDealsDetailsObj.sethDDiscountAmount(hotDealsDetails.gethDDiscountAmount());
					hotDealsDetailsObj.sethDDiscountPct(hotDealsDetails.gethDDiscountPct());
					hotDealsDetailsObj.setProductID(hotDealsDetails.getProductID());
					hotDealsDetailsObj.setRowNumber(hotDealsDetails.getRowNumber());
					hotDealsDetailsObj.setHdURL(hotDealsDetails.getHdURL());
					hotDealsDetailsObj.setDistance(hotDealsDetails.getDistance());
					hotDealsDetailsObj.setCity(hotDealsDetails.getCity());
					hotDealsDetailsObj.setHotdealLstId(hotDealsDetails.getHotdealLstId());
					hotDealsDetailsLst.add(hotDealsDetailsObj);
					hotDealAPIResultSet.setHotDealsDetailslst(hotDealsDetailsLst);
				}

			}
			else
			{
				hotDealAPIResultSet = new HotDealAPIResultSet();
				hotDealAPIResultSet.setApiPartnerName(hotDealsDetails.getApiPartnerName());
				hotDealsDetailsLst = new ArrayList<HotDealsDetails>();
				final HotDealsDetails hotDealsDetailsObj = new HotDealsDetails();
				hotDealsDetailsObj.setApiPartnerId(hotDealsDetails.getApiPartnerId());
				hotDealsDetailsObj.setApiPartnerName(hotDealsDetails.getApiPartnerName());
				hotDealsDetailsObj.setHotDealName(hotDealsDetails.getHotDealName());
				hotDealsDetailsObj.setHotDealId(hotDealsDetails.getHotDealId());
				hotDealsDetailsObj.setHotDealImagePath(hotDealsDetails.getHotDealImagePath());
				hotDealsDetailsObj.sethDshortDescription(hotDealsDetails.gethDshortDescription());
				hotDealsDetailsObj.sethDPrice(hotDealsDetails.gethDPrice());
				hotDealsDetailsObj.sethDSalePrice(hotDealsDetails.gethDSalePrice());
				hotDealsDetailsObj.sethDDiscountAmount(hotDealsDetails.gethDDiscountAmount());
				hotDealsDetailsObj.sethDDiscountPct(hotDealsDetails.gethDDiscountPct());
				hotDealsDetailsObj.setProductID(hotDealsDetails.getProductID());
				hotDealsDetailsObj.setRowNumber(hotDealsDetails.getRowNumber());
				hotDealsDetailsObj.setHdURL(hotDealsDetails.getHdURL());
				hotDealsDetailsObj.setDistance(hotDealsDetails.getDistance());
				hotDealsDetailsObj.setCity(hotDealsDetails.getCity());
				hotDealsDetailsObj.setHotdealLstId(hotDealsDetails.getHotdealLstId());
				hotDealsDetailsLst.add(hotDealsDetailsObj);
				hotDealAPIResultSet.setHotDealsDetailslst(hotDealsDetailsLst);
			}
			hotDealsAPIInfoMap.put(key, hotDealAPIResultSet);
		}

		final Set<Map.Entry<String, HotDealAPIResultSet>> set = hotDealsAPIInfoMap.entrySet();

		final ArrayList<HotDealAPIResultSet> hotDealsCategoryInfolst = new ArrayList<HotDealAPIResultSet>();

		for (Map.Entry<String, HotDealAPIResultSet> entry : set)
		{
			hotDealsCategoryInfolst.add(entry.getValue());
		}

		return hotDealsCategoryInfolst;
	}
}
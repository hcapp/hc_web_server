package shopper.hotdeals.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.find.service.FindService;
import shopper.hotdeals.service.ConsumerHotDealService;
import shopper.service.CommonService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.Users;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.tags.Pagination;
import common.util.Utility;

@Controller
public class ConsumerHotdealController
{
	public static final Logger LOG = LoggerFactory.getLogger(HotdealController.class);

	@RequestMapping(value = "/consdisplayhotdeals.htm", method = RequestMethod.GET)
	public ModelAndView showPage(HttpServletRequest request, HttpSession session, ModelMap model)
	{

		String methodName = "showPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int lowerLimit = 0;
		int currentPage = 1;
		Integer recordCount = 20;
		String pageNumber = "0";
		ConsumerRequestDetails consumerRequestDetails = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = null;
		String latitude = null;
		String longitude = null;
		String postalCode = null;
		int mainMenuId = 0;
		String consumerType = null;
		if (loginUser != null)
		{
			userId = loginUser.getUserID();
		}
		hotDealsListRequest.setRecordCount(recordCount);
		hotDealsListRequest.setCategory("0");
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setLastVisitedProductNo(0);
		HotDealsCategoryInfo hotDealDetails = new HotDealsCategoryInfo();
		model.put("hotdealmainpage", hotDealDetails);
		session.removeAttribute("PopulationCenterID");
		session.removeAttribute("catId");
		
		
		try

		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ConsumerHotDealService hotDealsService = (ConsumerHotDealService) appContext.getBean("consumerHotDealsService");
			
			if (null != loginUser)
			{
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}
			consumerRequestDetails = new ConsumerRequestDetails(ApplicationConstants.HOTDEALMODULE);

			FindService findService = (FindService) appContext.getBean("findService");
			consumerRequestDetails = new ConsumerRequestDetails(userId, latitude, longitude, postalCode, ApplicationConstants.HOTDEALMODULE);
			mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			consumerRequestDetails.setMainMenuId(mainMenuId);
			consumerRequestDetails.setConsumerType(consumerType);
			hotDealsListRequest.setMainMenuID(mainMenuId);
			session.setAttribute("consumerReqObj", consumerRequestDetails);

			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}
			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			// populateHtCat(hotDealsList, session);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/consdisplayhotdeals.htm",
						recordCount);
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
				session.setAttribute("categoryList", hotDealsList.getDealCategories());
			}
			session.setAttribute("hotDealsList", hotDealsList);

			displayPopulationCenters(hotDealsListRequest, request, session, model);

		}
		catch (ScanSeeServiceException e)
		{
			e.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("consdisplayhotdeals");

	}

	@RequestMapping(value = "/consdisplayhotdeals.htm", method = RequestMethod.POST)
	public ModelAndView hotDealPagination(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		String consumerType = null;
		Integer recordCount = hotDealInfo.getRecordCount();
		ConsumerRequestDetails consumerRequestDetails = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = null;
		if (loginUser != null)
		{
			userId = loginUser.getUserID();
		}
		hotDealsListRequest.setCategory("0");
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setRecordCount(recordCount);
		session.removeAttribute("PopulationCenterID");
		session.removeAttribute("catId");
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ConsumerHotDealService hotDealsService = (ConsumerHotDealService) appContext.getBean("consumerHotDealsService");
			if (null != loginUser)
			{
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}
			consumerRequestDetails = new ConsumerRequestDetails(ApplicationConstants.HOTDEALMODULE, consumerType);
			session.setAttribute("consumerReqObj", consumerRequestDetails);
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			ConsumerRequestDetails consumerRequestDetailsObj = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (consumerRequestDetailsObj != null)
			{
				hotDealsListRequest.setMainMenuID(consumerRequestDetailsObj.getMainMenuId());
			}

			hotDealsListRequest.setLastVisitedProductNo(lowerLimit);
			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/consdisplayhotdeals.htm",
						recordCount);
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
				session.setAttribute("categoryList", hotDealsList.getDealCategories());
			}
			session.setAttribute("hotDealsList", hotDealsList);
			displayPopulationCenters(hotDealsListRequest, request, session, model);
		}
		catch (ScanSeeServiceException e)
		{
			e.printStackTrace();
		}

		return new ModelAndView("consdisplayhotdeals");

	}

	@RequestMapping(value = "/conshotdealforcat.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForCat(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		Integer recordCount = 20;
		String pageFlag = (String) request.getParameter("pageFlag");
		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = null;
		if (loginUser != null)
		{
			userId = loginUser.getUserID();
		}
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setLastVisitedProductNo(0);
		if (hotDealInfo.getCatID() != null)
		{
			hotDealsListRequest.setCategory(hotDealInfo.getCatID());
			session.setAttribute("hdCatID", hotDealInfo.getCatID());
		}
		if (null != hotDealInfo.getRecordCount())
		{
			recordCount = hotDealInfo.getRecordCount();
		}
		hotDealsListRequest.setRecordCount(recordCount);
		if (null != hotDealInfo.getPopulationCentId())
		{
			hotDealsListRequest.setPopulationCentreID(hotDealInfo.getPopulationCentId());
		}

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ConsumerHotDealService hotDealsService = (ConsumerHotDealService) appContext.getBean("consumerHotDealsService");

			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}
			ConsumerRequestDetails consumerRequestDetailsObj = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (consumerRequestDetailsObj != null)
			{
				hotDealsListRequest.setMainMenuID(consumerRequestDetailsObj.getMainMenuId());
			}

			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/conshotdealforcatpag.htm",
						recordCount);
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
			}
			session.setAttribute("hotDealsList", hotDealsList);
			session.setAttribute("catId", hotDealInfo.getCatID());

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("consdisplayhotdeals");

	}

	@RequestMapping(value = "/conshotdealforcatpag.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForCatPagination(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		Integer recordCount = 20;

		String pageFlag = (String) request.getParameter("pageFlag");
		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = null;
		if (loginUser != null)
		{
			userId = loginUser.getUserID();
		}

		hotDealsListRequest.setUserId(userId);

		String hdCatID = (String) session.getAttribute("hdCatID");
		hotDealsListRequest.setCategory(hdCatID);
		if (null != hotDealInfo.getRecordCount())
		{
			recordCount = hotDealInfo.getRecordCount();
		}
		hotDealsListRequest.setRecordCount(recordCount);
		if (null != hotDealInfo.getPopulationCentId())
		{
			hotDealsListRequest.setPopulationCentreID(hotDealInfo.getPopulationCentId());
		}
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ConsumerHotDealService hotDealsService = (ConsumerHotDealService) appContext.getBean("consumerHotDealsService");
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}
			hotDealsListRequest.setLastVisitedProductNo(lowerLimit);
			ConsumerRequestDetails consumerRequestDetailsObj = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (consumerRequestDetailsObj != null)
			{
				hotDealsListRequest.setMainMenuID(consumerRequestDetailsObj.getMainMenuId());
			}
			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/conshotdealforcatpag.htm",
						recordCount);
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
			}
			session.setAttribute("hotDealsList", hotDealsList);
			session.setAttribute("catId", hdCatID);
		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("consdisplayhotdeals");

	}

	public static void populateHtCat(HotDealsListResultSet hotDealsList, HttpSession session)
	{

		List<HotDealsCategoryInfo> hotDealsDetailsArrayLst = null;

		List<HotDealsCategoryInfo> categoryList = new ArrayList<HotDealsCategoryInfo>();

		if (null != hotDealsList && !hotDealsList.getHotDealsCategoryInfo().isEmpty())
		{
			hotDealsDetailsArrayLst = hotDealsList.getHotDealsCategoryInfo();
			for (int i = 0; i < hotDealsDetailsArrayLst.size(); i++)
			{
				HotDealsCategoryInfo categoryInfo = new HotDealsCategoryInfo();
				if (null != hotDealsDetailsArrayLst.get(i).getCategoryId())
				{

					categoryInfo.setCategoryId(hotDealsDetailsArrayLst.get(i).getCategoryId());
					categoryInfo.setCategoryName(hotDealsDetailsArrayLst.get(i).getCategoryName());
					categoryList.add(categoryInfo);
				}

			}

		}
		if (!categoryList.isEmpty())
		{
			session.setAttribute("categoryList", categoryList);
		}
		else
		{
			session.setAttribute("categoryList", null);
		}

	}

	public static void displayPopulationCenters(HotDealsListRequest hotDealsListRequestObj, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{
		final String methodName = "displayPopulationCenters";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Long loginUserId = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		List<HotDealsDetails> populcationCentersLst = null;
		try
		{
			Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				loginUserId = loginUser.getUserID();
				hotDealsListRequestObj.setUserId(loginUserId);
			}
			ConsumerRequestDetails consumerRequestDetailsObj = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (consumerRequestDetailsObj != null)
			{
				hotDealsListRequestObj.setMainMenuID(consumerRequestDetailsObj.getMainMenuId());
			}

			populcationCentersLst = commonServiceObj.consDisplayPopulationCenters(hotDealsListRequestObj);
			if (populcationCentersLst != null)
			{
				session.setAttribute("populcatonCenters", populcationCentersLst);

			}

		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

	}

	@RequestMapping(value = "/conshotdealfordma.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForDMA(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "showHotDealsForDMA";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		Integer recordCount = 20;
		String pageFlag = (String) request.getParameter("pageFlag");
		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = null;
		if (loginUser != null)
		{
			userId = loginUser.getUserID();
		}
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setLastVisitedProductNo(0);
		hotDealsListRequest.setCategory("0");

		if (hotDealInfo.getPopulationCentId() != null)
		{
			hotDealsListRequest.setPopulationCentreID(hotDealInfo.getPopulationCentId());
			session.setAttribute("PopulationCenterID", hotDealInfo.getPopulationCentId());
		}
		if (null != hotDealInfo.getRecordCount())
		{
			recordCount = hotDealInfo.getRecordCount();
		}
		hotDealsListRequest.setRecordCount(recordCount);
		

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ConsumerHotDealService hotDealsService = (ConsumerHotDealService) appContext.getBean("consumerHotDealsService");

			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}
			hotDealsListRequest.setLastVisitedProductNo(lowerLimit);
			ConsumerRequestDetails consumerRequestDetailsObj = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (consumerRequestDetailsObj != null)
			{
				hotDealsListRequest.setMainMenuID(consumerRequestDetailsObj.getMainMenuId());
			}

			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/conshotdealfordmapag.htm",
						recordCount);
				session.setAttribute("pagination", objPage);
				if (hotDealsList.getFavCat() != null)
				{
					session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				}
				session.setAttribute("displayNarrowList", true);
			}
			session.setAttribute("hotDealsList", hotDealsList);
			session.setAttribute("catId", hotDealInfo.getCategoryId());
			session.setAttribute("categoryList", hotDealsList.getDealCategories());

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("consdisplayhotdeals");

	}
	
	@RequestMapping(value = "/conshotdealfordmapag.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForDMAPagination(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{		
		final String methodName = "showHotDealsForDMA";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		Integer recordCount = 20;

		String pageFlag = (String) request.getParameter("pageFlag");
		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = null;
		if (loginUser != null)
		{
			userId = loginUser.getUserID();
		}

		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setCategory("0");
		Integer popCentId = (Integer) session.getAttribute("PopulationCenterID");
		if(popCentId !=null)
		{
		hotDealsListRequest.setPopulationCentreID(popCentId);
		}
		if (null != hotDealInfo.getRecordCount())
		{
			recordCount = hotDealInfo.getRecordCount();
		}
		hotDealsListRequest.setRecordCount(recordCount);
	
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ConsumerHotDealService hotDealsService = (ConsumerHotDealService) appContext.getBean("consumerHotDealsService");
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}
			hotDealsListRequest.setLastVisitedProductNo(lowerLimit);
			ConsumerRequestDetails consumerRequestDetailsObj = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (consumerRequestDetailsObj != null)
			{
				hotDealsListRequest.setMainMenuID(consumerRequestDetailsObj.getMainMenuId());
			}
			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/conshotdealfordmapag.htm",
						recordCount);
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
			}
			session.setAttribute("hotDealsList", hotDealsList);
			session.setAttribute("categoryList", hotDealsList.getDealCategories());
			session.setAttribute("PopulationCenterID", popCentId);
		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("consdisplayhotdeals");

	}

}

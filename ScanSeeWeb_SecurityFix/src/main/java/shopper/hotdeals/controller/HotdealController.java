/**
 * @ (#) HotdealController.java 15-Feb-2012
 * Project       :ScanSeeWeb
 * File          : HotdealController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 15-Feb-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package shopper.hotdeals.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.hotdeals.service.HotDealsService;
import shopper.service.MyGalleryService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.SearchForm;
import common.pojo.Users;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.ShareProductInfo;
import common.tags.Pagination;
import common.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class HotdealController
{

	public static final Logger LOG = LoggerFactory.getLogger(HotdealController.class);

	@RequestMapping(value = "/hotdealprodlist.htm", method = RequestMethod.GET)
	public ModelAndView showPage(HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String searhKey = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");
		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		hotDealsListRequest.setCategory("0");
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setLastVisitedProductNo(0);
		HotDealsCategoryInfo hotDealDetails = new HotDealsCategoryInfo();
		model.put("hotdealmainpage", hotDealDetails);
		session.setAttribute("hotDealDetails", null);
		session.setAttribute("dealsCategoryList", null);
		session.setAttribute("dMAName", null);
		session.setAttribute("displaymap", false);
		session.setAttribute("hotDealsList", null);
		session.setAttribute("populationCenterID", null);
		session.setAttribute("populationcenterList", null);
		try

		{
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}

			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			populateHtCat(hotDealsList, session);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/hotdealprodlist.htm");
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
			}
			session.setAttribute("hotDealsList", hotDealsList);

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/hotdealprodlist.htm", method = RequestMethod.POST)
	public ModelAndView hotDealPagination(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String searhKey = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		hotDealsListRequest.setCategory("0");
		hotDealsListRequest.setUserId(userId);
		session.setAttribute("displaymap", false);
		session.setAttribute("hotDealsList", null);
		session.setAttribute("populationCenterID", null);
		session.setAttribute("populationcenterList", null);
		if (null != hotDealInfo.getSearchKey())
		{
			hotDealsListRequest.setSearchItem(hotDealInfo.getSearchKey());
		}
		session.setAttribute("dMAName", null);
		session.setAttribute("dealsCategoryList", null);
		try
		{
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			hotDealsListRequest.setLastVisitedProductNo(lowerLimit);
			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			populateHtCat(hotDealsList, session);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/hotdealprodlist.htm");
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
			}
			session.setAttribute("hotDealsList", hotDealsList);

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/hotdealforcity.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForCity(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		session.setAttribute("pagination", null);

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String searhKey = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		hotDealsListRequest.setCategory("0");
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setLastVisitedProductNo(0);
		hotDealsListRequest.setPopulationCentreID(hotDealInfo.getPopulationCenterID());
		session.setAttribute("populationCenterID", String.valueOf(hotDealInfo.getPopulationCenterID()));
		session.setAttribute("dMAName", hotDealInfo.getPopCntCity());
		session.setAttribute("dealsCategoryList", null);
		session.setAttribute("displayNarrowList", false);
		session.setAttribute("displaymap", false);
		session.setAttribute("hotDealsList", null);
		try
		{
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}

			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			populateHtCat(hotDealsList, session);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/hotdealcityprodlist.htm");
				session.setAttribute("pagination", objPage);

			}
			session.setAttribute("hotDealsList", hotDealsList);

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/hotdealforloc.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForLoc(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String searhKey = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		hotDealsListRequest.setCategory("0");
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setLastVisitedProductNo(0);
		session.setAttribute("hotDealsList", null);
		session.setAttribute("displayNarrowList", false);
		session.setAttribute("hotDealDetails", null);
		session.setAttribute("populationcenterList", null);
		session.setAttribute("populationCenterID", null);
		String latitude = request.getParameter("latitude");
		String longitude = request.getParameter("longitude");
		String searchCity = request.getParameter("cityNameTL");

		hotDealsListRequest.setLatitude(latitude);

		hotDealsListRequest.setLongitude(longitude);

		session.setAttribute("displaymap", true);
		session.setAttribute("searchCity", searchCity);

		try
		{
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}

			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			populateHtCat(hotDealsList, session);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/hotdealforlocpag.htm");
				session.setAttribute("pagination", objPage);

			}
			session.setAttribute("hotDealsList", hotDealsList);

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/hotdealforlocpag.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForLocPag(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String searhKey = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		hotDealsListRequest.setCategory("0");
		hotDealsListRequest.setUserId(userId);

		session.setAttribute("hotDealsList", null);
		session.setAttribute("displayNarrowList", false);
		session.setAttribute("populationCenterID", null);
		session.setAttribute("populationcenterList", null);
		session.setAttribute("hotDealDetails", null);
		String latitude = request.getParameter("latitude");
		String longitude = request.getParameter("longitude");

		hotDealsListRequest.setLatitude(latitude);

		hotDealsListRequest.setLongitude(longitude);

		session.setAttribute("displaymap", true);

		try
		{
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			hotDealsListRequest.setLastVisitedProductNo(lowerLimit);
			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			populateHtCat(hotDealsList, session);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/hotdealcityprodlist.htm");
				session.setAttribute("pagination", objPage);

			}
			session.setAttribute("hotDealsList", hotDealsList);

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/hotdealcityprodlist.htm", method = RequestMethod.POST)
	public ModelAndView hotDealforCityPagination(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String searhKey = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");

		String populationCentrID = (String) session.getAttribute("populationCenterID");
		Long userId = loginUser.getUserID();
		hotDealsListRequest.setCategory("0");
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setPopulationCentreID(Integer.valueOf(populationCentrID));
		session.setAttribute("dealsCategoryList", null);
		session.setAttribute("displayNarrowList", false);
		session.setAttribute("displaymap", false);
		session.setAttribute("hotDealsList", null);
		try
		{
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			hotDealsListRequest.setLastVisitedProductNo(lowerLimit);
			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);
			populateHtCat(hotDealsList, session);
			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/hotdealcityprodlist.htm");
				session.setAttribute("pagination", objPage);

			}
			session.setAttribute("hotDealsList", hotDealsList);

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/hotdealinfo.htm", method = RequestMethod.GET)
	public ModelAndView showHotDealInfoGet(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		HotDealsDetails hotDealDetails = null;
		hotDealDetails = (HotDealsDetails) session.getAttribute("hotDealDetails");

		session.setAttribute("hotDealDetails", hotDealDetails);
		HotDealsListResultSet hotDealsList = (HotDealsListResultSet) session.getAttribute("hotDealsList");
		session.setAttribute("hotDealsList", hotDealsList);
		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/hotdealinfo.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealInfo(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");
		HotDealsDetails hotDealDetails = null;
		Users loginUser = (Users) session.getAttribute("loginuser");
		session.setAttribute("displaymap", false);

		Long userId = loginUser.getUserID();

		String dMAName = (String) session.getAttribute("dMAName");

		if (null != dMAName)
		{
			session.setAttribute("dMAName", dMAName);
		}

		try
		{

			request.setAttribute("populationcenterList", null);

			hotDealDetails = hotDealsService.getHdProdInfo(userId.intValue(), hotDealInfo.getHotDealID());

			hotDealDetails.setHotDealId(hotDealInfo.getHotDealID());
			session.setAttribute("hotDealDetails", hotDealDetails);
		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");
	}

	public static void populateHtCat(HotDealsListResultSet hotDealsList, HttpSession session)
	{

		List<HotDealsCategoryInfo> hotDealsDetailsArrayLst = null;

		List<HotDealsCategoryInfo> categoryList = new ArrayList<HotDealsCategoryInfo>();

		if (null != hotDealsList && !hotDealsList.getHotDealsCategoryInfo().isEmpty())
		{
			hotDealsDetailsArrayLst = hotDealsList.getHotDealsCategoryInfo();
			for (int i = 0; i < hotDealsDetailsArrayLst.size(); i++)
			{
				HotDealsCategoryInfo categoryInfo = new HotDealsCategoryInfo();
				if (null != hotDealsDetailsArrayLst.get(i).getCategoryId())
				{

					categoryInfo.setCategoryId(hotDealsDetailsArrayLst.get(i).getCategoryId());
					categoryInfo.setCategoryName(hotDealsDetailsArrayLst.get(i).getCategoryName());
					categoryList.add(categoryInfo);
				}

			}

		}
		if (!categoryList.isEmpty())
		{
			session.setAttribute("categoryList", categoryList);
		}
		else
		{
			session.setAttribute("categoryList", null);
		}

	}

	@RequestMapping(value = "/hotdealforcat.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForCat(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String searhKey = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		hotDealsListRequest.setUserId(userId);
		hotDealsListRequest.setLastVisitedProductNo(0);
		hotDealsListRequest.setCategory(String.valueOf(hotDealInfo.getCategoryId()));
		session.setAttribute("hdCatID", String.valueOf(hotDealInfo.getCategoryId()));
		session.setAttribute("hotDealDetails", null);
		session.setAttribute("dMAName", null);
		session.setAttribute("dealsCategoryList", null);
		session.setAttribute("displaymap", false);
		session.setAttribute("populationcenterList", null);

		String populationCentrID = (String) session.getAttribute("populationCenterID");

		if (null != populationCentrID)
		{
			hotDealsListRequest.setPopulationCentreID(Integer.valueOf(populationCentrID));
		}
		try
		{
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}

			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);

			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/hotdealforcatpag.htm");
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
			}
			session.setAttribute("hotDealsList", hotDealsList);

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/hotdealforcatpag.htm", method = RequestMethod.POST)
	public ModelAndView showHotDealsForCatPagination(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String searhKey = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		HotDealsListRequest hotDealsListRequest = new HotDealsListRequest();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		hotDealsListRequest.setUserId(userId);

		String hdCatID = (String) session.getAttribute("hdCatID");
		hotDealsListRequest.setCategory(hdCatID);
		session.setAttribute("hotDealDetails", null);
		session.setAttribute("dMAName", null);
		session.setAttribute("dealsCategoryList", null);
		session.setAttribute("displaymap", false);
		session.setAttribute("populationcenterList", null);
		String populationCentrID = (String) session.getAttribute("populationCenterID");

		if (null != populationCentrID)
		{
			hotDealsListRequest.setPopulationCentreID(Integer.valueOf(populationCentrID));
		}

		try
		{
			if (null != pageFlag && pageFlag.equals("true"))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}

			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			hotDealsListRequest.setLastVisitedProductNo(lowerLimit);
			HotDealsListResultSet hotDealsList = hotDealsService.getHotDealslst(hotDealsListRequest);

			if (hotDealsList != null)
			{
				session.setAttribute("hotDealsList", hotDealsList);
				Pagination objPage = Utility.getPagination(hotDealsList.getRowCount(), currentPage, "/ScanSeeWeb/shopper/hotdealforcatpag.htm");
				session.setAttribute("pagination", objPage);
				session.setAttribute("FavCatFlag", String.valueOf(hotDealsList.getFavCat()));
				session.setAttribute("displayNarrowList", true);
			}
			session.setAttribute("hotDealsList", hotDealsList);

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ModelAndView("hotdeals");

	}

	@RequestMapping(value = "/shareHotDealInfo.htm", method = RequestMethod.GET)
	public @ResponseBody
	String sendMail(@RequestParam(value = "toEmailId", required = true) String toEmailId,
			@RequestParam(value = "hotDealId", required = true) Integer hotDealId,

			HttpServletRequest request, HttpServletResponse response, HttpSession session)

	{
		final String methodName = "";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String daoResponse = null;

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

			Users loginUser = (Users) session.getAttribute("loginuser");

			Integer userId = (int) (loginUser.getUserID().longValue());

			ShareProductInfo shareProductInfoObj = new ShareProductInfo();
			shareProductInfoObj.setUserId(userId);
			shareProductInfoObj.setToEmail(toEmailId);
			shareProductInfoObj.setHotdealId(hotDealId);

			daoResponse = hotDealsService.sharHotDealbyEmail(shareProductInfoObj);

		}
		catch (Exception exception)
		{
			LOG.error("Error occureed in galleryhome.htm", exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

}
/**
 * @ (#) PopulationCenterController.java 16-Feb-2012
 * Project       :ScanSeeWeb
 * File          : PopulationCenterController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 16-Feb-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package shopper.hotdeals.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.hotdeals.service.HotDealsService;

import common.exception.ScanSeeServiceException;
import common.pojo.shopper.HotDealsCategoryInfo;

@Controller
public class PopulationCenterController
{
	@RequestMapping(value = "/populationcenterslist.htm", method = RequestMethod.GET)
	public ModelAndView showPopulationCenters(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		List<HotDealsCategoryInfo> populationcenterList = null;
		HotDealsCategoryInfo hotDealDetails = new HotDealsCategoryInfo();
		session.setAttribute("hotDealDetails", null);
		session.setAttribute("dMAName", "SearchCity");
		model.put("hotdealmainpage", hotDealDetails);
		session.setAttribute("displayNarrowList",false);
		session.setAttribute("displaymap", false);
		try
		{
			session.setAttribute("hotDealsList", null);
			
			populationcenterList = hotDealsService.getHdPopulationCenters();
			session.setAttribute("populationcenterList", populationcenterList);
		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ModelAndView("hotdeals");
	}

	@RequestMapping(value = "/populationcenterslist.htm", method = RequestMethod.POST)
	public ModelAndView searchPopulationCenters(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");

		List<HotDealsCategoryInfo> populationcenterList = null;
		HotDealsCategoryInfo hotDealDetails = new HotDealsCategoryInfo();
		session.setAttribute("displaymap", false);

		model.put("hotdealmainpage", hotDealDetails);
		try
		{
			session.setAttribute("hotDealsList", null);
			session.setAttribute("displayNarrowList",false);
			session.setAttribute("dMAName",  "SearchCity");
			populationcenterList = hotDealsService.searchHdPopulationCenters(hotDealInfo.getdMAName());
			session.setAttribute("populationcenterList", populationcenterList);
		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ModelAndView("hotdeals");
	}
}

package shopper.hotdeals.dao;

import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;

public interface ConsumerHotDealDao
{
	public HotDealsListResultSet fetchHotDealsDetails(HotDealsListRequest hotDealsListRequest, String screenName) throws ScanSeeWebSqlException;

}

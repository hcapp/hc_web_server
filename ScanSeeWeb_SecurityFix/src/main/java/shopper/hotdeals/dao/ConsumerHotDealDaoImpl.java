package shopper.hotdeals.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import shopper.find.dao.FindDAOImpl;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.Category;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.HotDealsResultSet;

public class ConsumerHotDealDaoImpl implements ConsumerHotDealDao
{
	
	private static final Logger log = LoggerFactory.getLogger(FindDAOImpl.class.getName());

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            for setDataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This DAO Implementation fir method fetches hotDeals list.
	 * 
	 * @param hotDealsListRequest
	 *            of HotDealsListRequest
	 * @param screenName
	 *            as query parameter.
	 * @return HotDealsListResultSet
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	public HotDealsListResultSet fetchHotDealsDetails(HotDealsListRequest hotDealsListRequest, String screenName) throws ScanSeeWebSqlException
	{

		List<HotDealsResultSet> hotDealsListResponselst = null;
		HotDealsListResultSet hotDealsListResultSet = null;
		List<Category> lstCategory= null;
		final String methodName = "fetchHotDealsDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName + "->" + hotDealsListRequest.getUserId());

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerHotDealSearchPagination");
			simpleJdbcCall.returningResultSet("hotDealslst", new BeanPropertyRowMapper<HotDealsResultSet>(HotDealsResultSet.class));
			simpleJdbcCall.returningResultSet("categoryList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource fetchHotDealsParameters = new MapSqlParameterSource();
			fetchHotDealsParameters.addValue(ApplicationConstants.USERID, hotDealsListRequest.getUserId());
			fetchHotDealsParameters.addValue("Category", hotDealsListRequest.getCategory());
			fetchHotDealsParameters.addValue("LowerLimit", hotDealsListRequest.getLastVisitedProductNo());
			fetchHotDealsParameters.addValue("PopulationCentreID", hotDealsListRequest.getPopulationCentreID());
			fetchHotDealsParameters.addValue("Radius", hotDealsListRequest.getRadius());			
			fetchHotDealsParameters.addValue("RecordCount", hotDealsListRequest.getRecordCount());
			fetchHotDealsParameters.addValue("PostalCode", null);
			fetchHotDealsParameters.addValue("MainMenuID", hotDealsListRequest.getMainMenuID());
			fetchHotDealsParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			fetchHotDealsParameters.addValue(ApplicationConstants.ERRORMESSAGE, null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHotDealsParameters);
			if (null != resultFromProcedure)
			{
				hotDealsListResultSet = new HotDealsListResultSet();
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsListResponselst = (List<HotDealsResultSet>) resultFromProcedure.get("hotDealslst");
					final Boolean favCat = (Boolean) resultFromProcedure.get("FavCatFlag");
					final Boolean categoryFlag = (Boolean) resultFromProcedure.get("ByCategoryFlag");
					if (null != hotDealsListResponselst && !hotDealsListResponselst.isEmpty())
					{
						lstCategory=(List<Category>) resultFromProcedure.get("categoryList");
						hotDealsListResultSet.setHotDealsListResponselst((ArrayList<HotDealsResultSet>) hotDealsListResponselst);
						hotDealsListResultSet.setDealCategories(lstCategory);
											
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						final Integer maxCount = (Integer) resultFromProcedure.get("MaxCnt");
						hotDealsListResultSet.setRowCount(maxCount);
						if (nextpage != null)
						{
							if (nextpage)
							{
								hotDealsListResultSet.setNextPage(1);
							}
							else
							{
								hotDealsListResultSet.setNextPage(0);
							}
						}
						if (favCat != null)
						{
							if (favCat)
							{
								hotDealsListResultSet.setFavCat(1);

							}
							else
							{
								hotDealsListResultSet.setFavCat(0);

							}
						}
					}
					if (favCat != null)
					{
						if (favCat)
						{
							hotDealsListResultSet.setFavCat(1);

						}
						else
						{
							hotDealsListResultSet.setFavCat(0);

						}
					}
					if (categoryFlag != null)
					{
						if (categoryFlag)
						{
							hotDealsListResultSet.setCategoryFlag(1);

						}
						else
						{
							hotDealsListResultSet.setCategoryFlag(0);

						}
					}
				}

			}

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_HotDealSearchPagination Store Procedure error number: {}" + errorNum + "  and error message: {}"
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchHotDealsDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info("Response from server is " + hotDealsListResponselst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsListResultSet;
	}
}

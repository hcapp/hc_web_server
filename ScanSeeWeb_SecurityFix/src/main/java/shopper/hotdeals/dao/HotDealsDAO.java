package shopper.hotdeals.dao;

import java.util.ArrayList;
import java.util.List;

import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.shopper.Categories;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.ShareProductInfo;



/**
 * This interface is used for HotDeals module.It has methods which are
 * implemented by HotDealsDAOImpl.
 * 
 * @author shyamsundara_hm
 */
public interface HotDealsDAO
{

	/**
	 * The DAO method fetches the HotDeals Details.
	 * 
	 * @param hotDealsListRequest
	 *            Instance of HotDealsListRequest.
	 * @param screenName
	 *            as query parameter
	 * @return HotDealsListResultSet.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	HotDealsListResultSet fetchHotDealsDetails(HotDealsListRequest hotDealsListRequest, String screenName) throws ScanSeeWebSqlException;

	/**
	 * The DAO method fetches the hotdeal products information.
	 * 
	 * @param userId
	 *            as Query parameter
	 * @param hotDealId
	 *            as Query parameter
	 * @return HotDealsListResultSet.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	HotDealsCategoryInfo getHotDealProductDetail(Integer userId, Integer hotDealId) throws ScanSeeWebSqlException;

	/**
	 * The DAO method removes hotDeal product.
	 * 
	 * @param hotDealsListRequest
	 *            as Query parameter
	 * @return response based on success or failure.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String removeHDProduct(HotDealsListRequest hotDealsListRequest) throws ScanSeeWebSqlException;

	/**
	 * The DAO method fetches the HotDeal Category Details. no Query parameter
	 * 
	 * @param userId
	 *            as request parameter
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	List<HotDealsCategoryInfo> getHdCategoryDetail(Long userId) throws ScanSeeWebSqlException;

	/**
	 * The DAO method fetches the HotDeal Category Details. no Query parameter
	 * 
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	List<HotDealsCategoryInfo> getHdPopulationCenters() throws ScanSeeWebSqlException;

	/**
	 * The DAO method fetches the HotDeal Category Details. no Query parameter
	 * 
	 * @param dmaName
	 *            as request parameter
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	List<HotDealsCategoryInfo> searchHdPopulationCenters(String dmaName) throws ScanSeeWebSqlException;
	public String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException;
	
	/**
	 * The DAO method for fetching user favourite categories.
	 * 
	 * @param userId
	 *            - for which the information to be fetched.
	 * @param lowerLimit
	 *            used for pagination
	 * @return CategoryDetail object.
	 * @throws ScanSeeWebSqlException
	 *             - If any exception occures ScanSeeException will be thrown.
	 */
	public CategoryDetail fetchUserFavCategories(Long userId, Integer lowerLimit) throws ScanSeeWebSqlException;
	
	/**
	 * The DAo method for saving user favourite categories in the database.
	 * 
	 * @param categoryDetail
	 *            categories details.
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeWebSqlException will be thrown.
	 */

	public String createOrUpdateFavCategories(String  categoryId, Long userID) throws ScanSeeWebSqlException;
	
	public String getHotDealShareThruEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeWebSqlException;
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException;
}

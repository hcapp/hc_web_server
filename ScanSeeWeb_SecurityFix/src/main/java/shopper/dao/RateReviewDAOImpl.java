package shopper.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.query.RateReviewQueries;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.ProductReview;
import common.pojo.shopper.UserRatingInfo;

/**
 * This is implementation class for RateReviewDAO. This class has methods for
 * RateReview Module. The methods of this class are called from the RateReview
 * Service layer.
 * 
 * @author malathi_lr
 *
 */
public class RateReviewDAOImpl implements RateReviewDAO {


	/**
	 * Getting the Logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RateReviewDAOImpl.class);
	/**
	 * To set ParameterizedBeanPropertyRowMapper to map POJO.
	 */

	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * This method for to get data source from xml.
	 * 
	 * @param dataSource
	 *            as input parameter.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This method fetches User product ratings.
	 * 
	 * @param userId
	 *            as query parameter
	 * @param productId
	 *            as query parameter
	 * @return UserRatingInfo object
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	public UserRatingInfo fecthUserProductRating(Long userId, Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "fecthUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		UserRatingInfo userRatingInfo = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetUserRatings");
			simpleJdbcCall.returningResultSet("userRatingInfo", new BeanPropertyRowMapper<UserRatingInfo>(UserRatingInfo.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("prProductID", productId);
			ratingParameters.addValue("prUserID", userId);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final List<UserRatingInfo> userRatingInfoList = (List<UserRatingInfo>) resultFromProcedure.get("userRatingInfo");
					if (null != userRatingInfoList && !userRatingInfoList.isEmpty())
					{
						userRatingInfo = userRatingInfoList.get(0);

						userRatingInfo.setProductId(String.valueOf(productId));
						final int userRating = userRatingInfo.getCurrentRating();
						final String avgRating = userRatingInfo.getAvgRating();
						final String noOfUserrated = userRatingInfo.getNoOfUsersRated();
						if (userRating>0)
						{
							userRatingInfo.setCurrentRating(userRating);
						}
						if (null == avgRating)
						{
							userRatingInfo.setAvgRating("0");
						}	if (null == noOfUserrated)
						{
							userRatingInfo.setNoOfUsersRated("0");
						}

					}
					else
					{
						
						userRatingInfo = new UserRatingInfo();
						userRatingInfo.setProductId(String.valueOf(productId));
						userRatingInfo.setCurrentRating(0);
						userRatingInfo.setAvgRating("0");
						userRatingInfo.setNoOfUsersRated("0");
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					LOG.error("Error occurred in usp_GetUserRatings Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userRatingInfo;
	}

	/**
	 * The DAO method for fetching product reviews.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return ProductReview list.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ArrayList<ProductReview> getProductReviews(Long userId, Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "getProductReviews in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductReview> productReviewslist = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			productReviewslist = (ArrayList<ProductReview>) simpleJdbcTemplate.query(RateReviewQueries.GETPRODUCTREVIEWS,
					new BeanPropertyRowMapper<ProductReview>(ProductReview.class), productId);

		}

		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productReviewslist;
	}
	
	/**
	 * This method fetches User product ratings.
	 * 
	 * @param userRatingInfo
	 *            as request parameter containing userId,produtId and rating
	 *            information
	 * @return xml containing success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public String saveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "saveUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_SaveUserRatings");
			simpleJdbcCall.returningResultSet("userRatingInfo", new BeanPropertyRowMapper<UserRatingInfo>(UserRatingInfo.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("prProductID", userRatingInfo.getProductId());
			ratingParameters.addValue("prUserID", userRatingInfo.getUserId());
			ratingParameters.addValue("prRating", userRatingInfo.getCurrentRating());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					response = ApplicationConstants.SUCCESS;
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					LOG.error("Error occurred in usp_SaveUserRatings Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
}

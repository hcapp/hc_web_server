package shopper.dao;

import java.util.ArrayList;

import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.CouponsDetails;
import common.pojo.shopper.LoyaltyDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.RebateDetails;
import common.pojo.shopper.ShareProductInfo;

/**
 * The Interface for CLRGalleryDAO. ImplementedBy {@link CLRGalleryDAOImpl}
 * 
 * @author shyamsundara_hm
 */
public interface CLRGalleryDAO
{
	/**
	 * This method get the Master Shopping list product CLR from the database.
	 * 
	 * @param userId
	 *            as request
	 * @param retailerId
	 *            as request
	 * @param productId
	 *            as request.
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	// CLRDetails fetchCLRDetails(Integer userId, Integer retailerId, Integer
	// productId) throws ScanSeeWebSqlException;

	/**
	 * This method get the Master Shopping list product CLR from the database.
	 * 
	 * @param userId
	 *            as request
	 * @param retailId
	 *            as request
	 * @param productId
	 *            as request.
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	CouponDetails getCouponInfo(Integer userId, Integer productId, Integer retailId) throws ScanSeeWebSqlException;

	/**
	 * This method fetches the loyalty information from the database for the
	 * given loyaltyId.
	 * 
	 * @param clrDetailsObj
	 *            , contains userId,loyaltyId which are needed for fetching
	 *            loyalty details.
	 * @return LoyaltyDetails with loyalty info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	LoyaltyDetails getLoyaltyInfo(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException;

	/**
	 * This method fetches the rebate information from the database for the
	 * given rebateId.
	 * 
	 * @param clrDetailsObj
	 *            , contains userId,rebateId which are needed for fetching
	 *            rebate details.
	 * @return RebateDetails with rebate info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	RebateDetails getRebateInfo(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException;

	/**
	 * Method for fetching coupon details for particular coupon id. The method
	 * is called from the service layer.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,couponId in the request.
	 * @return couponsDetails returns the coupon details.
	 * @throws ScanSeeWebSqlException
	 *             The exception defined for the application.
	 */
	CouponsDetails fetchCouponDetails(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException;

	/**
	 * couponAdd is a method for adding the coupon to myCoupon.
	 * 
	 * @param couponId
	 *            contains the id of the coupon to be added.
	 * @param userId
	 *            contains user id.
	 * @return String returns success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public String addCoupon(Long userId, StringBuffer couponId) throws ScanSeeWebSqlException;

	public String addLoyalty(Long userId, Integer loyaltyDealId) throws ScanSeeWebSqlException;

	public String addRebate(Long userId, Integer rebateId) throws ScanSeeWebSqlException;

	/**
	 * couponRemove is a method used to remove coupon from myCoupons or
	 * allCoupons.
	 * 
	 * @param userId
	 *            contains user id of the coupon associated with.
	 * @param couponId
	 *            contains coupon's id to be removed.
	 * @return String returns success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public String removeCoupon(Long userId, StringBuffer couponId) throws ScanSeeWebSqlException;

	public String removeRebate(Long userId, Integer rebateId) throws ScanSeeWebSqlException;

	public String removeLoyalty(Long userId, Integer loyaltyDealId) throws ScanSeeWebSqlException;

	/**
	 * this method is used to mark the coupon as used coupon.
	 * 
	 * @param userId
	 *            contains user id of the coupon associated with.
	 * @param couponId
	 *            contains coupon's id to be removed.
	 * @return String returns success or failure.
	 * @throws ScanSeeWebSqlException
	 */
	public String userRedeemCoupon(Long userId, StringBuffer couponId) throws ScanSeeWebSqlException;

	public String userRedeemRebate(Long userId, Integer rebateId) throws ScanSeeWebSqlException;

	/**
	 * This DAOImpl method is used to redeem loyalty information based on
	 * userId.
	 * 
	 * @param loyaltyDetailObj
	 *            contains user id,loyalty deal id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	public String userRedeemLoyalty(Long userId, Integer loyaltyDealId) throws ScanSeeWebSqlException;

	/**
	 * this method is used to share CLR via email.
	 * 
	 * @param shareProductInfoObj
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	public String getCouponShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException;

	/**
	 * This method fetches share coupon details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String getRebateShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException;

	/**
	 * This method fetches share coupon details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String getLoyaltyShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException;

	/**
	 * The method for fetching user information for sending mail to share
	 * product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException;

	/**
	 * This method Fetching App Configuration details.
	 * 
	 * @return ArrayList<AppConfiguration> .
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException;

	/**
	 * this method is used to add products to wishList.
	 * 
	 * @param areaInfoVO
	 *            contains userId.
	 * @param productId
	 *            contains product id's to be added to wishList
	 * @return String returns product already exists or added successfully.
	 * @throws ScanSeeWebSqlException
	 */
	public String addWishListProd(AreaInfoVO areaInfoVO, StringBuffer productId) throws ScanSeeWebSqlException;

	public String discountUserRedeemCLR(Long userId, Integer clrId, String clrFlag) throws ScanSeeWebSqlException;

	/**
	 * fetchCouponProductDetails is a method for fetching the products
	 * associated with the coupon.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,couponId,rebateId and loyaltyId.
	 * @return ArrayList<ProductDetail> returns list of product details
	 *         associated with the CLR.
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<ProductDetail> fetchCouponProductDetails(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException;

	/**
	 * addShoppingListProd is a Method for adding products associated with the
	 * coupon to shopping list.
	 * 
	 * @param areaInfoVO
	 *            contains userId.
	 * @param productId
	 *            contains product id's to be added to wishList
	 * @return String returns product already exists or added successfully.
	 * @throws ScanSeeWebSqlException
	 */
	public String addShoppingListProd(AreaInfoVO areaInfoVO, StringBuffer productId) throws ScanSeeWebSqlException;

}

package shopper.dao;

import java.util.ArrayList;

import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.ProductReview;
import common.pojo.shopper.UserRatingInfo;

public interface RateReviewDAO {

	/**
	 * The DAO method for fetching product reviews.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return ProductReview list.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ArrayList<ProductReview> getProductReviews(Long userId, Integer productId) throws ScanSeeWebSqlException;
	
	/**
	 * This method fetches user product rating information.
	 * @param userId as request parameter.
	 * @param productId as request parameter.
	 * @return xml containing user,average product ratings.
	 * @throws ScanSeeException
	 * 				The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	
	UserRatingInfo fecthUserProductRating(Long userId, Integer productId) throws ScanSeeWebSqlException;
	
	/**
	 * This method fetches User product ratings.
	 * 
	 * @param userRatingInfo
	 *            as request parameter containing userId,produtId and rating
	 *            information
	 * @return xml containing success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String saveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeWebSqlException;
}

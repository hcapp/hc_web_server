package shopper.dao;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.helper.SortHotDealByCategory;
import shopper.query.ShoppingListQueries;
import shopper.shoppingList.helper.SortShopListByCategory;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.common.pojos.ExternalAPISearchParameters;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.ConsumerRequestDetails;
import common.pojo.SearchForm;
import common.pojo.University;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.Category;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.FindNearByDetail;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.ProductAttributes;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.ShoppingCartProducts;
import common.pojo.shopper.UserRatingInfo;
import common.pojo.shopper.UserTrackingData;
import common.util.Utility;

public class BaseDAOImpl implements BaseDAO
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(BaseDAOImpl.class);

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * The method for fetching product details.
	 * 
	 * @param productId
	 *            request parameter
	 * @param userId
	 *            request parameter
	 * @param retailId
	 *            request parameter
	 * @return The ProductDetail as Product details.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ProductDetail getProductDetail(Long userId, Integer productId, Integer retailId) throws ScanSeeWebSqlException
	{
		final String methodName = "getProductDetail";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetail = null;
		String retailerId = null;
		try
		{

			if (retailId != null)
			{
				retailerId = String.valueOf(retailId);
			}
			log.info("fetching Product information for ID {}", productId);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ProductDetails");
			simpleJdbcCall.returningResultSet("shoppingProductDetail", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource shoppingProductParameters = new MapSqlParameterSource();
			shoppingProductParameters.addValue("ProductID", productId);
			shoppingProductParameters.addValue("UserID", userId);
			shoppingProductParameters.addValue("ScanLongitude", null);
			shoppingProductParameters.addValue("ScanLatitude", null);
			if ("0".equals(retailerId))
			{
				retailerId = null;
			}
			shoppingProductParameters.addValue("RetailID", retailerId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(shoppingProductParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					final List<ProductDetail> productDetaillst = (List<ProductDetail>) resultFromProcedure.get("shoppingProductDetail");
					if (null != productDetaillst && !productDetaillst.isEmpty())
					{
						productDetail = productDetaillst.get(0);
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
					final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
					log.error("Error occurred in getProductDetail Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getProductDetail", exception);
			return null;
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetail;
	}

	public ProductDetail fetchProductAttributeDetails(Long userId, Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductAttributeDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> productDetailslst = null;
		ProductDetail productDetailObj = null;

		ProductDetails productImageObj = null;

		final List<ProductDetails> productImageslst = new ArrayList<ProductDetails>();
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductDetailsDisplay");
			simpleJdbcCall.returningResultSet("productdetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("productid", productId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailslst = (ArrayList<ProductDetail>) resultFromProcedure.get("productdetails");

				}
				if (null != productDetailslst && !productDetailslst.isEmpty())
				{
					productDetailObj = new ProductDetail();
					for (int i = 0; i < productDetailslst.size(); i++)
					{
						productImageObj = new ProductDetails();
						productImageObj.setProductMediaPath(productDetailslst.get(i).getProductMediaPath());
						productImageslst.add(productImageObj);

					}

					productDetailObj.setProductId(productDetailslst.get(0).getProductId());
					productDetailObj.setProductName(productDetailslst.get(0).getProductName());
					productDetailObj.setProductImagePath(productDetailslst.get(0).getProductImagePath());
					productDetailObj.setProductName(productDetailslst.get(0).getProductName());
					productDetailObj.setProductShortDescription(productDetailslst.get(0).getProductShortDescription());
					productDetailObj.setProductLongDescription(productDetailslst.get(0).getProductLongDescription());
					productDetailObj.setVideoFlag(productDetailslst.get(0).getVideoFlag());
					productDetailObj.setAudioFlag(productDetailslst.get(0).getAudioFlag());
					productDetailObj.setFileFlag(productDetailslst.get(0).getFileFlag());
					productDetailObj.setModelNumber(productDetailslst.get(0).getModelNumber());
					productDetailObj.setWarrantyServiceInfo(productDetailslst.get(0).getWarrantyServiceInfo());
					productDetailObj.setProductImageslst(productImageslst);
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListProductHotDealInformation" + errorNum + "errorMsg.." + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		ProductAttributes productAttributesObj = null;
		final List<ProductAttributes> productAttributeslst = new ArrayList<ProductAttributes>();
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WishlistProductAttributeDisplay");
			simpleJdbcCall.returningResultSet("productAttributes", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("productid", productId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailslst = (ArrayList<ProductDetail>) resultFromProcedure.get("productAttributes");

				}
				if (null != productDetailslst && !productDetailslst.isEmpty())
				{

					for (int i = 0; i < productDetailslst.size(); i++)
					{
						productAttributesObj = new ProductAttributes();
						productAttributesObj.setAttributeName(productDetailslst.get(i).getAttributeName());
						productAttributesObj.setDisplayValue(productDetailslst.get(i).getDisplayValue());
						productAttributeslst.add(productAttributesObj);

					}

					productDetailObj.setProductAttributeslst(productAttributeslst);
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListProductHotDealInformation" + errorNum + "errorMsg.." + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailObj;
	}

	// ################# NEWLY CREATED METHODS FOR CONSUMER IMPLEMENTATION
	// ###################################.
	/**
	 * This method is used to display product details like name,image,audio
	 * ,video and attributes.
	 */
	public ProductDetail fetchConsumerProductDetails(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetail productDetails = null;
		Integer rowcount = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductDetailsDisplay");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			// scanQueryParams.addValue("UserId", productDetail.getUserId());
			scanQueryParams.addValue("ProductID", productDetail.getProductId());
			scanQueryParams.addValue("ProductListID", productDetail.getProductListID());
			scanQueryParams.addValue("SaleListID", productDetail.getSaleListID());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						log.info("Products found for the search");
						productDetails = new ProductDetail();
						productDetails.setProductId(productDetailList.get(0).getProductId());
						productDetails.setProductName(productDetailList.get(0).getProductName());
						productDetails.setScanCode(productDetailList.get(0).getScanCode());
						productDetails.setProductImagePath(productDetailList.get(0).getProductImagePath());
						productDetails.setProductShortDescription(productDetailList.get(0).getProductShortDescription());
						productDetails.setProductLongDescription(productDetailList.get(0).getProductLongDescription());
						productDetails.setModelNumber(productDetailList.get(0).getModelNumber());
						productDetails.setAudioFlag(productDetailList.get(0).getAudioFlag());
						productDetails.setVideoFlag(productDetailList.get(0).getVideoFlag());
						productDetails.setProdRatingCount(productDetailList.get(0).getProdRatingCount());
						productDetails.setProdReviewCount(productDetailList.get(0).getProdReviewCount());

					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerProductDetailsDisplay Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * This method is used to fetch product multiple images information.
	 */

	public List<ProductDetail> fetchConsumerProductImagelst(SearchForm searchForm) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductImagelst";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productImagelst = null;
		ProductDetail productDetails = null;
		Integer rowcount = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductImagesDisplay");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			// scanQueryParams.addValue("UserId", productDetail.getUserId());
			scanQueryParams.addValue("ProductID", 842044);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			productImagelst = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerProductImagesDisplay Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchConsumerProductImagelst", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productImagelst;
	}

	/**
	 * This method is used to fetch product media information like audio and
	 * videos.
	 */

	public List<ProductDetail> fetchConsumerProductMediaInfo(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductMediaInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;
		Integer rowcount = null;

		try
		{
			if (null == productDetail.getLastVistedProductNo())
			{
				productDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductMediaDisplay");
			simpleJdbcCall.returningResultSet("productmediainfo", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("ProductID", productDetail.getProductId());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());
			scanQueryParams.addValue("ProductListID", productDetail.getProductListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("productmediainfo");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					/*
					 * if (null != productDetailList &&
					 * !productDetailList.isEmpty()) {
					 * log.info("Products found for the search"); productDetails
					 * = new ProductDetails();
					 * productDetails.setProductDetail(productDetailList); }
					 */
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchConsumerProductMediaInfo", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailList;
	}

	/**
	 * This method is used to fetch product attributes.
	 */

	public List<ProductDetail> fetchConsumerProductAttributeDetails(ProductDetail productDetail) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchConsumerProductAttributeDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;
		Integer rowcount = null;

		try
		{
			if (null == productDetail.getLastVistedProductNo())
			{
				productDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductAttributeDisplay");
			simpleJdbcCall.returningResultSet("productmediainfo", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("ProductID", productDetail.getProductId());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("productmediainfo");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					/*
					 * if (null != productDetailList &&
					 * !productDetailList.isEmpty()) {
					 * log.info("Products found for the search"); productDetails
					 * = new ProductDetails();
					 * productDetails.setProductDetail(productDetailList); }
					 */

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerProductAttributeDisplay Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchConsumerProductAttributeDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailList;

	}

	public HotDealsDetails getConsumerHotDealDetail(HotDealsListRequest hotDealsListRequestObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsumerHotDealDetail in DAO layer";
		final List<HotDealsDetails> hotDealsListResponselst;
		log.info(ApplicationConstants.METHODSTART + methodName + "Requested user id is -->" + hotDealsListRequestObj.getUserId());
		HotDealsCategoryInfo hotDealsCategoryInfolst = null;
		HotDealsDetails hotDealsDetailsObj = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerHotDealDetails");
			simpleJdbcCall.returningResultSet("HotDealDetails", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource fetchHotdealParams = new MapSqlParameterSource();
			fetchHotdealParams.addValue("UserID", hotDealsListRequestObj.getUserId());
			fetchHotdealParams.addValue("HotDealID", hotDealsListRequestObj.getHotDealId());
			fetchHotdealParams.addValue("HotDealListID", hotDealsListRequestObj.getHotDealListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHotdealParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					hotDealsListResponselst = (List<HotDealsDetails>) resultFromProcedure.get("HotDealDetails");
					if (null != hotDealsListResponselst && !hotDealsListResponselst.isEmpty())
					{
						hotDealsDetailsObj = new HotDealsDetails();
						hotDealsDetailsObj.setHotDealId(hotDealsListResponselst.get(0).getHotDealId());
						hotDealsDetailsObj.setHotDealName(hotDealsListResponselst.get(0).getHotDealName());
						hotDealsDetailsObj.setHotDealImagePath(hotDealsListResponselst.get(0).getHotDealImagePath());
						hotDealsDetailsObj.sethDshortDescription(hotDealsListResponselst.get(0).gethDshortDescription());
						hotDealsDetailsObj.sethDLognDescription(hotDealsListResponselst.get(0).gethDLognDescription());
						hotDealsDetailsObj.sethDStartDate(hotDealsListResponselst.get(0).gethDStartDate());
						hotDealsDetailsObj.sethDEndDate(hotDealsListResponselst.get(0).gethDEndDate());
						hotDealsDetailsObj.setPoweredBy(hotDealsListResponselst.get(0).getPoweredBy());
						hotDealsDetailsObj.setRetName(hotDealsListResponselst.get(0).getRetName());
						hotDealsDetailsObj.setRetAddr(hotDealsListResponselst.get(0).getRetAddr());
						hotDealsDetailsObj.setCity(hotDealsListResponselst.get(0).getCity());
						hotDealsDetailsObj.setState(hotDealsListResponselst.get(0).getState());
						hotDealsDetailsObj.setZipcode(hotDealsListResponselst.get(0).getZipcode());
						hotDealsDetailsObj.setHdURL(hotDealsListResponselst.get(0).getHdURL());

					}
					else
					{
						log.info("No Products found for the search");
						return hotDealsDetailsObj;
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred  in usp_WebConsumerHotDealDetails Store Procedure error number: {} " + errorNum
							+ " and  error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getConsumerHotDealDetail ", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info("Response frm server in getConsumerHotDealDetail " + hotDealsCategoryInfolst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsDetailsObj;
	}

	@SuppressWarnings("unchecked")
	public FindNearByDetails getConsFindNearByRetailer(ProductDetail productDetailObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsFindNearByRetailer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		FindNearByDetails findNearByDetailsObj = null;
		List<FindNearByDetail> findNearByDetaillst = null;

		try
		{
			if (null == productDetailObj.getLastVistedProductNo() || "".equals(productDetailObj.getLastVistedProductNo()))
			{
				productDetailObj.setLastVistedProductNo(0);
			}

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerFindNearByRetailer");
			simpleJdbcCall.returningResultSet("findnearbyretailer", new BeanPropertyRowMapper<FindNearByDetail>(FindNearByDetail.class));

			final MapSqlParameterSource fetchNearbyRetParams = new MapSqlParameterSource();

			fetchNearbyRetParams.addValue("UserID", productDetailObj.getUserId());
			fetchNearbyRetParams.addValue("Latitude", productDetailObj.getLatitude());
			fetchNearbyRetParams.addValue("Longitude", productDetailObj.getLongitude());
			fetchNearbyRetParams.addValue("PostalCode", productDetailObj.getZipcode());
			fetchNearbyRetParams.addValue("ProductId", productDetailObj.getProductId());
			fetchNearbyRetParams.addValue("Radius", productDetailObj.getRadius());
			fetchNearbyRetParams.addValue("LowerLimit", productDetailObj.getLastVistedProductNo());
			fetchNearbyRetParams.addValue("ScreenName", ApplicationConstants.CONSFNDPRODSEARCHPAGINATION);
			fetchNearbyRetParams.addValue("MainMenuID", productDetailObj.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchNearbyRetParams);

			if (null != resultFromProcedure)
			{

				findNearByDetaillst = (List<FindNearByDetail>) resultFromProcedure.get("findnearbyretailer");
				if (findNearByDetaillst != null && !findNearByDetaillst.isEmpty())
				{
					findNearByDetailsObj = new FindNearByDetails();
					findNearByDetailsObj.setFindNearByDetail(findNearByDetaillst);
				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return findNearByDetailsObj;
	}

	public List<ProductDetail> fetchConsumerProductReviews(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductReviews";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productReviewList;
		try
		{
			if (null == productDetail.getLastVistedProductNo())
			{
				productDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerDisplayProductReviews");
			simpleJdbcCall.returningResultSet("productreviews", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("ProductID", productDetail.getProductId());
			scanQueryParams.addValue("LowerLimit", productDetail.getLastVistedProductNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.CONSFNDPRODREVIEWPAGINATION);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productReviewList = (List<ProductDetail>) resultFromProcedure.get("productreviews");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerDisplayProductReviews Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchConsumerProductReviews", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productReviewList;
	}

	public ArrayList<RetailerDetail> getConsumerCommisJunctionData(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsumerCommisJunctionData";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<RetailerDetail> onlineRetailerlst = null;
		try
		{

			if (null == productDetail.getLastVistedProductNo() || "".equals(productDetail.getLastVistedProductNo()))
			{
				productDetail.setLastVistedProductNo(0);
			}
			log.info(" Request received from Userid ", productDetail.getUserId());

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerOnlineRetailerInformation");
			simpleJdbcCall.returningResultSet("commissionjunction", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue(ApplicationConstants.USERID, productDetail.getUserId());
			fetchProductDetailsParameters.addValue("ProductID", productDetail.getProductId());
			fetchProductDetailsParameters.addValue("LowerLimit", productDetail.getLastVistedProductNo());
			fetchProductDetailsParameters.addValue("ProductListID", productDetail.getProductListID());
			fetchProductDetailsParameters.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{

				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{

					onlineRetailerlst = (ArrayList<RetailerDetail>) resultFromProcedure.get("commissionjunction");

				}
				else
				{

					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in getConsumerCommisJunctionData Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return onlineRetailerlst;
	}

	public List<CouponDetail> fetchConsumerProductCoupons(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductCoupons";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetail> productCouponsList;
		try
		{
			if (null == productDetail.getLastVistedProductNo())
			{
				productDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductCouponList");
			simpleJdbcCall.returningResultSet("productcoupons", new BeanPropertyRowMapper<CouponDetail>(CouponDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("ProductID", productDetail.getProductId());
			scanQueryParams.addValue("RetailID", productDetail.getRetailerId());
			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("LowerLimit", productDetail.getLastVistedProductNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.CONSFNDPRODREVIEWPAGINATION);
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productCouponsList = (List<CouponDetail>) resultFromProcedure.get("productcoupons");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerMasterShoppingListCoupon Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchConsumerProductCoupons", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productCouponsList;
	}

	public Integer getDefaultRadius() throws ScanSeeWebSqlException
	{
		final String methodName = "getDefaultRadius";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer defaultRadius = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGetUserModuleRadius");
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					defaultRadius = (Integer) resultFromProcedure.get("Radius");

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerGetUserModuleRadius Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getDefaultRadius", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return defaultRadius;
	}

	public ProductDetail consAddProdToShopp(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddProdToShopp";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		Boolean productExists = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerShoppingListSearchAddProduct");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("ProductID", productDetail.getProductIds());
			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			// addProdToShopp = (List<CouponDetail>)
			// resultFromProcedure.get("productcoupons");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					productExists = (Boolean) resultFromProcedure.get("ProductExists");
					addProdToShopp = new ProductDetail();
					addProdToShopp.setProductExists(productExists);

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerShoppingListSearchAddProduct Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consAddProdToShopp", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	public ProductDetail consAddProdToWishList(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddProdToWishList";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToWL = null;
		Boolean productExists = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerWishListSearchAddProduct");
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("ProductID", productDetail.getProductId());
			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			// addProdToShopp = (List<CouponDetail>)
			// resultFromProcedure.get("productcoupons");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					productExists = (Boolean) resultFromProcedure.get("ProductExists");
					addProdToWL = new ProductDetail();
					addProdToWL.setProductExists(productExists);
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerShoppingListSearchAddProduct Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consAddProdToWishList", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return addProdToWL;
	}

	public Long consGetUsrTrakingMainMenuId(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consGetUsrTrakingMainMenuId";
		log.info(ApplicationConstants.METHODSTART + methodName);

		Integer mainMenuId = null;
		Long lngMainMenuId = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_UserTrackingMainMenuCreation");
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("ModuleID", productDetail.getModuleID());
			scanQueryParams.addValue("Latitude", productDetail.getLatitude());
			scanQueryParams.addValue("Longitude", productDetail.getLongitude());
			scanQueryParams.addValue("Postalcode", productDetail.getZipcode());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			// addProdToShopp = (List<CouponDetail>)
			// resultFromProcedure.get("productcoupons");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					mainMenuId = (Integer) resultFromProcedure.get("MainMenuID");
					lngMainMenuId = mainMenuId.longValue();

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_UserTrackingMainMenuCreation Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consGetUsrTrakingMainMenuId", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return lngMainMenuId;
	}

	public ArrayList<AppConfiguration> consGetAppConfig(String configType) throws ScanSeeWebSqlException
	{
		final String methodName = "consGetAppConfig";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Inside consGetAppConfig : usp_WebConsumerGetScreenContent : Error occurred in  Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consGetAppConfig", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		// TODO Auto-generated method stub
		return appConfigurationList;
	}

	public String getConsUserInfo(ProductDetail shareProductInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsUserInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			response = this.jdbcTemplate.queryForObject(ShoppingListQueries.FETCHUSEREMAILID, new Object[] { shareProductInfo.getUserId() },
					String.class);

		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@SuppressWarnings("unused")
	public String ShareConsumerProductDetails(ProductDetail shareProductInfo, String shareURL) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetail productDetails = null;
		Integer rowcount = null;
		String response = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductDetailsDisplay");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			// scanQueryParams.addValue("UserId", productDetail.getUserId());
			scanQueryParams.addValue("ProductID", shareProductInfo.getProductId());
			scanQueryParams.addValue("ProductListID", shareProductInfo.getProductListID());
			scanQueryParams.addValue("SaleListID", shareProductInfo.getSaleListID());
			scanQueryParams.addValue("MainMenuID", shareProductInfo.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						int userId = 0;
						if (shareProductInfo.getUserId() != null)
						{
							userId = shareProductInfo.getUserId().intValue();
						}
						productDetails = productDetailList.get(0);
						productDetails.setProductListID(shareProductInfo.getProductListID());
						response = Utility.formProductInfoHTML(productDetails, userId, shareURL);

					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerProductDetailsDisplay Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (InvalidKeyException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (NoSuchAlgorithmException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (NoSuchPaddingException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (InvalidAlgorithmParameterException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (InvalidKeySpecException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (IllegalBlockSizeException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (BadPaddingException exception)
		{
			log.error("Exception occurred in fetchConsumerProductDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public ExternalAPIInformation getConsExternalApiInfo(String moduleName) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;

		ExternalAPIInformation exteApi = null;

		List<University> arUniversityList = null;

		List<ExternalAPIVendor> vendorList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIListOnline");
			simpleJdbcCall.returningResultSet("vendorList", new BeanPropertyRowMapper<ExternalAPIVendor>(ExternalAPIVendor.class));

			final MapSqlParameterSource vendorParameter = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			resultFromProcedure = simpleJdbcCall.execute(vendorParameter);
			vendorList = (ArrayList) resultFromProcedure.get("vendorList");

			ArrayList<ExternalAPISearchParameters> externalAPIInputParameters = null;

			final HashMap<Integer, ArrayList<ExternalAPISearchParameters>> apiParamMap = new HashMap<Integer, ArrayList<ExternalAPISearchParameters>>();

			exteApi = new ExternalAPIInformation();
			exteApi.setVendorList((ArrayList<ExternalAPIVendor>) vendorList);
			if ("FindOnlineStores".equals(moduleName))
			{
				for (ExternalAPIVendor obj : vendorList)
				{
					externalAPIInputParameters = getApiparams(Integer.valueOf(obj.getApiUsageID()), "FindOnlineStores");
					apiParamMap.put(Integer.valueOf(obj.getApiUsageID()), externalAPIInputParameters);
				}

			}

			exteApi.setSerchParameters(apiParamMap);

			// log.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" +
			// arUniversityList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllUniversity" + exception);
		}
		return exteApi;
	}

	private ArrayList<ExternalAPISearchParameters> getApiparams(int id, String moduleName) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;

		ExternalAPIInformation exteApi = null;

		List<University> arUniversityList = null;

		ArrayList<ExternalAPISearchParameters> paramList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIInputParameters");
			simpleJdbcCall.returningResultSet("aipInputList", new BeanPropertyRowMapper<ExternalAPISearchParameters>(
					ExternalAPISearchParameters.class));

			final MapSqlParameterSource APISearchParameters = new MapSqlParameterSource();

			APISearchParameters.addValue("prAPIUsageID", id);

			APISearchParameters.addValue("PrAPISubModuleName", moduleName);
			resultFromProcedure = simpleJdbcCall.execute(APISearchParameters);
			paramList = (ArrayList) resultFromProcedure.get("aipInputList");

			log.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" + paramList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllUniversity" + exception);
		}
		return paramList;
	}

	public UserRatingInfo consGetProductRatings(ProductDetail productRatings) throws ScanSeeWebSqlException
	{
		final String methodName = "inside consGetProductRatings";
		log.info(ApplicationConstants.METHODSTART + methodName);
		UserRatingInfo userRatingInfo = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGetUserRatings");
			simpleJdbcCall.returningResultSet("userRatingInfo", new BeanPropertyRowMapper<UserRatingInfo>(UserRatingInfo.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("ProductID", productRatings.getProductId());
			ratingParameters.addValue("UserID", productRatings.getUserId());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final List<UserRatingInfo> userRatingInfoList = (List<UserRatingInfo>) resultFromProcedure.get("userRatingInfo");
					if (null != userRatingInfoList && !userRatingInfoList.isEmpty())
					{
						userRatingInfo = userRatingInfoList.get(0);

						userRatingInfo.setProductId(String.valueOf(productRatings.getProductId()));
						final int userRating = userRatingInfo.getCurrentRating();
						final String avgRating = userRatingInfo.getAvgRating();
						final String noOfUserrated = userRatingInfo.getNoOfUsersRated();
						if (userRating > 0)
						{
							userRatingInfo.setCurrentRating(userRating);
						}
						if (null == avgRating)
						{
							userRatingInfo.setAvgRating("0");
						}
						if (null == noOfUserrated)
						{
							userRatingInfo.setNoOfUsersRated("0");
						}

					}
					else
					{

						userRatingInfo = new UserRatingInfo();
						userRatingInfo.setProductId(String.valueOf(productRatings.getProductId()));
						userRatingInfo.setCurrentRating(0);
						userRatingInfo.setAvgRating("0");
						userRatingInfo.setNoOfUsersRated("0");
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					log.error("Error occurred in usp_WebConsumerGetUserRatings Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return userRatingInfo;
	}

	public String consSaveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "consSaveUserProductRating";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Integer avgRating = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerSaveUserRatings");
			simpleJdbcCall.returningResultSet("userRatingInfo", new BeanPropertyRowMapper<UserRatingInfo>(UserRatingInfo.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("ProductID", userRatingInfo.getProductId());
			ratingParameters.addValue("UserID", userRatingInfo.getUserId());
			ratingParameters.addValue("Rating", userRatingInfo.getCurrentRating());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					avgRating = (Integer) resultFromProcedure.get("AvgRating");
					if (null == avgRating)
					{
						avgRating = 0;
					}

					response = ApplicationConstants.SUCCESS + "&" + avgRating;
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					log.error("Error occurred in usp_WebConsumerSaveUserRatings Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String getConsEmailId(Long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsUserInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			response = this.jdbcTemplate.queryForObject(ShoppingListQueries.FETCHUSEREMAILID, new Object[] { userId }, String.class);

		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	public String ShareConsumerHodDealDetails(ProductDetail productDetail, String shareURL) throws ScanSeeWebSqlException
	{
		final String methodName = "ShareConsumerHodDealDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> hotDealListlst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerHotDealDetails");
			simpleJdbcCall.returningResultSet("shareHotdealInfo", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource fetchHotdealParams = new MapSqlParameterSource();
			fetchHotdealParams.addValue("UserID", productDetail.getUserId());
			fetchHotdealParams.addValue("HotDealID", productDetail.getHotDealId());
			fetchHotdealParams.addValue("HotDealListID", productDetail.getHotDealListID());
			fetchHotdealParams.addValue(ApplicationConstants.ERRORNUMBER, null);
			fetchHotdealParams.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHotdealParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					hotDealListlst = (List<HotDealsDetails>) resultFromProcedure.get("shareHotdealInfo");
					if (!hotDealListlst.isEmpty() && hotDealListlst != null)
					{

						response = Utility.formHotDealInfoHTML(hotDealListlst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_WebConsumerHotDealDetails Store Procedure error number: {} " + errorNum
							+ " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}
		catch (InvalidKeyException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public List<HotDealsDetails> consDisplayPopulationCenters(HotDealsListRequest hotDealsListRequestObj) throws ScanSeeWebSqlException
	{
		final String methodName = "consDisplayPopulationCenters";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<HotDealsDetails> populcationCentersLst = null;
		try
		{
			log.info(" Request received from Userid ", hotDealsListRequestObj.getUserId());

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerHotDealPopulationCentres");
			simpleJdbcCall.returningResultSet("populationcenters", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue(ApplicationConstants.USERID, hotDealsListRequestObj.getUserId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{

				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{

					populcationCentersLst = (ArrayList<HotDealsDetails>) resultFromProcedure.get("populationcenters");

				}
				else
				{

					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in consDisplayPopulationCenters Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return populcationCentersLst;
	}

	/**
	 * The DAO method to fetch types of share
	 * 
	 * @param objUserTrackingData
	 * @return share types name and its ID
	 * @throws ScanSeeException
	 */
	public List<UserTrackingData> getConsumerShareTypes() throws ScanSeeWebSqlException
	{
		final String methodName = "gerShareTypes";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<UserTrackingData> objShareTypesList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerUserTrackingShareTypeDisplay");
			simpleJdbcCall.returningResultSet("shareTypes", new BeanPropertyRowMapper<UserTrackingData>(UserTrackingData.class));

			final MapSqlParameterSource shareParam = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(shareParam);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					objShareTypesList = (List<UserTrackingData>) resultFromProcedure.get("shareTypes");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_WebConsumerUserTrackingShareTypeDisplay Store Procedure error number: {} " + errorNum
							+ " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return objShareTypesList;
	}

	public ProductDetail saveConsShareProdDetails(ProductDetail ObjProductDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "saveConsShareProdDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> lstProduDetail = null;
		ProductDetail objProduDetail = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerUserTrackingProductShare");
			simpleJdbcCall.returningResultSet("shareProdInfo", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource shareParam = new MapSqlParameterSource();
			shareParam.addValue("MainMenuID", ObjProductDetail.getMainMenuID());
			shareParam.addValue("ShareTypeID", ObjProductDetail.getShareTypeID());
			shareParam.addValue("ProductID", ObjProductDetail.getProductId());
			shareParam.addValue("TargetAddress", ObjProductDetail.getTargetAddress());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(shareParam);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					lstProduDetail = (List<ProductDetail>) resultFromProcedure.get("shareProdInfo");
					if (lstProduDetail != null && !lstProduDetail.isEmpty())
					{
						objProduDetail = new ProductDetail();
						objProduDetail.setProductName(lstProduDetail.get(0).getProductName());

					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_WebConsumerUserTrackingProductShare Store Procedure error number: {} " + errorNum
							+ " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return objProduDetail;
	}

	public ProductDetail getConsumerProdMultipleImages(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsumerProdMultipleImages";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetail productDetails = null;
		Integer rowcount = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductMultipleImagesDisplay");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			// scanQueryParams.addValue("UserId", productDetail.getUserId());
			scanQueryParams.addValue("ProductID", productDetail.getProductId());
			scanQueryParams.addValue("ProductListID", productDetail.getProductListID());
			scanQueryParams.addValue("SaleListID", productDetail.getSaleListID());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						log.info("Products found for the search");
						productDetails = new ProductDetail();
						productDetails.setProductId(productDetailList.get(0).getProductId());
						productDetails.setProductName(productDetailList.get(0).getProductName());
						productDetails.setScanCode(productDetailList.get(0).getScanCode());
						productDetails.setProductImagePath(productDetailList.get(0).getProductImagePath());
						productDetails.setProductShortDescription(productDetailList.get(0).getProductShortDescription());
						productDetails.setProductLongDescription(productDetailList.get(0).getProductLongDescription());
						productDetails.setModelNumber(productDetailList.get(0).getModelNumber());
						productDetails.setAudioFlag(productDetailList.get(0).getAudioFlag());
						productDetails.setVideoFlag(productDetailList.get(0).getVideoFlag());
						productDetails.setProdRatingCount(productDetailList.get(0).getProdRatingCount());
						productDetails.setProdReviewCount(productDetailList.get(0).getProdReviewCount());

					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerProductDetailsDisplay Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * This method is used to fetch user Shopping list products.
	 * 
	 * @param objProductDetail
	 *            contains the userId,lower limit.
	 * @throws ScanSeeWebSqlException
	 *             which will be caught in controller layer.
	 */

	@SuppressWarnings("unchecked")
	public AddRemoveSBProducts getConsShopLstProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getConsShopLstProds";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		AddRemoveSBProducts addRemoveSBProducts = new AddRemoveSBProducts();
		List<ProductDetail> productDetailList = null;
		ShoppingCartProducts cartProducts = null;
		Integer iMaxCount = null;
		Integer intShopCnt = 0;

		if (null == objConsumerRequestDetails.getLowerLimit())
		{
			objConsumerRequestDetails.setLowerLimit(0);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerTodayShoppingListProducts");
			simpleJdbcCall.returningResultSet("shoppinglistprods", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			MapSqlParameterSource objMapSqlParameterSource = new MapSqlParameterSource();
			objMapSqlParameterSource.addValue("UserID", objConsumerRequestDetails.getUserId());
			objMapSqlParameterSource.addValue("LowerLimit", objConsumerRequestDetails.getLowerLimit());
			objMapSqlParameterSource.addValue("RecordCount", objConsumerRequestDetails.getRecordCount());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objMapSqlParameterSource);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("shoppinglistprods");

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					if (null != productDetailList && !productDetailList.isEmpty())
					{
						HashMap<String, ArrayList<ProductDetail>> mapShoppinglist = new HashMap<String, ArrayList<ProductDetail>>();

						iMaxCount = (Integer) resultFromProcedure.get("MaxCnt");
						intShopCnt = (Integer) resultFromProcedure.get("MaxShopCartCnt");
						addRemoveSBProducts.setMaxCount(iMaxCount);
						addRemoveSBProducts.setMaxShopCartCnt(intShopCnt);

						for (ProductDetail productDetail : productDetailList)
						{
							if (mapShoppinglist.containsKey(productDetail.getParentCategoryName()))
							{

								final ArrayList<ProductDetail> prodDetails = mapShoppinglist.get(productDetail.getParentCategoryName());
								prodDetails.add(productDetail);
							}
							else
							{
								final ArrayList<ProductDetail> productDetailsList = new ArrayList<ProductDetail>();
								productDetailsList.add(productDetail);
								mapShoppinglist.put(productDetail.getParentCategoryName(), productDetailsList);
							}
						}
						cartProducts = new ShoppingCartProducts();
						final ArrayList<Category> categorylist = new ArrayList<Category>();
						Category categoryDetails = null;
						ProductDetail obj = null;
						for (Map.Entry e : mapShoppinglist.entrySet())
						{
							categoryDetails = new Category();
							categoryDetails.setParentCategoryName(e.getKey().toString());

							categoryDetails.setProductDetails((ArrayList<ProductDetail>) e.getValue());
							obj = ((ArrayList<ProductDetail>) e.getValue()).get(0);
							categoryDetails.setCategoryId(obj.getCategoryID());
							categorylist.add(categoryDetails);
						}

						SortShopListByCategory objSortHDbyCategory = new SortShopListByCategory();
						Collections.sort(categorylist, objSortHDbyCategory);
						cartProducts.setCategoryDetails(categorylist);

					}

					if (cartProducts != null)
					{
						addRemoveSBProducts.setCartProducts(cartProducts);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_WebConsumerTodayShoppingListProducts Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + strMethodName);

		return addRemoveSBProducts;
	}

	public AddRemoveSBProducts getConsSLHistoryProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getConsSLHistoryProds";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		AddRemoveSBProducts addRemoveSBProducts = new AddRemoveSBProducts();
		List<ProductDetail> productDetailList = null;
		ShoppingCartProducts cartProducts = null;
		Integer iMaxCount = null;

		if (null == objConsumerRequestDetails.getLowerLimit())
		{
			objConsumerRequestDetails.setLowerLimit(0);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerShoppingListHistoryDisplay");
			simpleJdbcCall.returningResultSet("shoppinglistprods", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			MapSqlParameterSource objMapSqlParameterSource = new MapSqlParameterSource();
			objMapSqlParameterSource.addValue("UserID", objConsumerRequestDetails.getUserId());
			objMapSqlParameterSource.addValue("LowerLimit", objConsumerRequestDetails.getLowerLimit());
			objMapSqlParameterSource.addValue("RecordCount", objConsumerRequestDetails.getRecordCount());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objMapSqlParameterSource);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("shoppinglistprods");

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					if (null != productDetailList && !productDetailList.isEmpty())
					{
						HashMap<String, ArrayList<ProductDetail>> mapShoppinglist = new HashMap<String, ArrayList<ProductDetail>>();

						iMaxCount = (Integer) resultFromProcedure.get("MaxCnt");
						addRemoveSBProducts.setMaxCount(iMaxCount);

						for (ProductDetail productDetail : productDetailList)
						{
							if (mapShoppinglist.containsKey(productDetail.getParentCategoryName()))
							{

								final ArrayList<ProductDetail> prodDetails = mapShoppinglist.get(productDetail.getParentCategoryName());
								prodDetails.add(productDetail);
							}
							else
							{
								final ArrayList<ProductDetail> productDetailsList = new ArrayList<ProductDetail>();
								productDetailsList.add(productDetail);
								mapShoppinglist.put(productDetail.getParentCategoryName(), productDetailsList);
							}
						}
						cartProducts = new ShoppingCartProducts();
						final ArrayList<Category> categorylist = new ArrayList<Category>();
						Category categoryDetails = null;
						ProductDetail obj = null;
						for (Map.Entry e : mapShoppinglist.entrySet())
						{
							categoryDetails = new Category();
							categoryDetails.setParentCategoryName(e.getKey().toString());

							categoryDetails.setProductDetails((ArrayList<ProductDetail>) e.getValue());
							obj = ((ArrayList<ProductDetail>) e.getValue()).get(0);
							categoryDetails.setCategoryId(obj.getCategoryID());
							categorylist.add(categoryDetails);
						}
						SortShopListByCategory objSortHDbyCategory = new SortShopListByCategory();
						Collections.sort(categorylist, objSortHDbyCategory);
						cartProducts.setCategoryDetails(categorylist);

					}

					if (cartProducts != null)
					{
						addRemoveSBProducts.setCartProducts(cartProducts);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_WebConsumerShoppingListHistoryDisplay Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + strMethodName);

		return addRemoveSBProducts;
	}

	@SuppressWarnings("unchecked")
	
	public AddRemoveSBProducts getConsSLFavoriteProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getConsSLFavoriteProds";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		AddRemoveSBProducts addRemoveSBProducts = new AddRemoveSBProducts();
		List<ProductDetail> productDetailList = null;
		ShoppingCartProducts cartProducts = null;
		Integer iMaxCount = null;

		if (null == objConsumerRequestDetails.getLowerLimit())
		{
			objConsumerRequestDetails.setLowerLimit(0);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerFavriteProductsListDisplay");
			simpleJdbcCall.returningResultSet("shoppinglistprods", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			MapSqlParameterSource objMapSqlParameterSource = new MapSqlParameterSource();
			objMapSqlParameterSource.addValue("UserID", objConsumerRequestDetails.getUserId());
			objMapSqlParameterSource.addValue("LowerLimit", objConsumerRequestDetails.getLowerLimit());
			objMapSqlParameterSource.addValue("RecordCount", objConsumerRequestDetails.getRecordCount());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objMapSqlParameterSource);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("shoppinglistprods");

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					if (null != productDetailList && !productDetailList.isEmpty())
					{
						HashMap<String, ArrayList<ProductDetail>> mapShoppinglist = new HashMap<String, ArrayList<ProductDetail>>();

						iMaxCount = (Integer) resultFromProcedure.get("MaxCnt");
						addRemoveSBProducts.setMaxCount(iMaxCount);

						for (ProductDetail productDetail : productDetailList)
						{
							if (mapShoppinglist.containsKey(productDetail.getParentCategoryName()))
							{

								final ArrayList<ProductDetail> prodDetails = mapShoppinglist.get(productDetail.getParentCategoryName());
								prodDetails.add(productDetail);
							}
							else
							{
								final ArrayList<ProductDetail> productDetailsList = new ArrayList<ProductDetail>();
								productDetailsList.add(productDetail);
								mapShoppinglist.put(productDetail.getParentCategoryName(), productDetailsList);
							}
						}
						cartProducts = new ShoppingCartProducts();
						final ArrayList<Category> categorylist = new ArrayList<Category>();
						Category categoryDetails = null;
						ProductDetail obj = null;
						for (Map.Entry e : mapShoppinglist.entrySet())
						{
							categoryDetails = new Category();
							categoryDetails.setParentCategoryName(e.getKey().toString());

							categoryDetails.setProductDetails((ArrayList<ProductDetail>) e.getValue());
							obj = ((ArrayList<ProductDetail>) e.getValue()).get(0);
							categoryDetails.setCategoryId(obj.getCategoryID());
							categorylist.add(categoryDetails);
						}
						SortShopListByCategory objSortHDbyCategory = new SortShopListByCategory();
						Collections.sort(categorylist, objSortHDbyCategory);
						cartProducts.setCategoryDetails(categorylist);

					}

					if (cartProducts != null)
					{
						addRemoveSBProducts.setCartProducts(cartProducts);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_WebConsumerFavriteProductsListDisplay Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + strMethodName, exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + strMethodName);

		return addRemoveSBProducts;
	}

	public ProductDetails getSLSearchProducts(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "getSLSearchProducts";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;
		Integer rowcount = null;

		try
		{
			if (objConsumerRequestDetails.getSearchKey() != null)
			{
				if (objConsumerRequestDetails.getSearchKey().equals(""))
				{
					objConsumerRequestDetails.setSearchKey(null);
				}
			}

			if (null == objConsumerRequestDetails.getLowerLimit())
			{
				objConsumerRequestDetails.setLowerLimit(0);

			}

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductSmartSearchResults");
			simpleJdbcCall.returningResultSet("slsearchproducts", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", objConsumerRequestDetails.getUserId());
			scanQueryParams.addValue("ProdSearch", objConsumerRequestDetails.getSearchKey());
			scanQueryParams.addValue("LowerLimit", objConsumerRequestDetails.getLowerLimit());
			scanQueryParams.addValue("RecordCount", objConsumerRequestDetails.getRecordCount());
			scanQueryParams.addValue("MainmenuID", objConsumerRequestDetails.getMainMenuId());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("slsearchproducts");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{

						log.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);
						rowcount = (Integer) resultFromProcedure.get("MaxCnt");
						productDetails.setTotalSize(rowcount);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerProductSmartSearchResults Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				productDetails.setTotalSize(rowcount);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getSLSearchProducts", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	@SuppressWarnings("unused")
	public ProductDetails getConsShopSearchProd(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "inside getSLSearchProducts";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;
		Integer rowcount = null;

		try
		{
			if (objConsumerRequestDetails.getSearchKey() != null)
			{
				if (objConsumerRequestDetails.getSearchKey().equals(""))
				{
					objConsumerRequestDetails.setSearchKey(null);
				}
			}

			if (null == objConsumerRequestDetails.getLowerLimit())
			{
				objConsumerRequestDetails.setLowerLimit(0);

			}

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductSmartSearchResults");
			simpleJdbcCall.returningResultSet("slsearchproducts", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", objConsumerRequestDetails.getUserId());
			scanQueryParams.addValue("ProdSearch", objConsumerRequestDetails.getSearchKey());
			scanQueryParams.addValue("LowerLimit", objConsumerRequestDetails.getLowerLimit());
			scanQueryParams.addValue("RecordCount", objConsumerRequestDetails.getRecordCount());
			scanQueryParams.addValue("MainmenuID", objConsumerRequestDetails.getMainMenuId());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("slsearchproducts");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{

						log.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);
						rowcount = (Integer) resultFromProcedure.get("MaxCnt");
						productDetails.setTotalSize(rowcount);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerProductSmartSearchResults Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				productDetails.setTotalSize(rowcount);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getSLSearchProducts", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	public ProductDetail consFavDeleteProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consFavDeleteProd";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail favDeleteProd = null;
		Integer intStatus = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerMasterShoppingListDeleteProducts");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("UserProductID", productDetail.getProductIds());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					intStatus = (Integer) resultFromProcedure.get("Status");

					if (null != intStatus && intStatus == 0)
					{
						favDeleteProd = new ProductDetail();
						favDeleteProd.setResponse(ApplicationConstants.SUCCESS);
					}
					else
					{
						favDeleteProd = new ProductDetail();
						favDeleteProd.setResponse(ApplicationConstants.FAILURE);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerFavoriteSLDeleteProducts Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consFavDeleteProd", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return favDeleteProd;
	}

	public ProductDetail consAddHistoryProdToSL(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddHistoryProdToSL";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		Boolean productExists = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerShoppingHistoryMoveItems");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserProductID", productDetail.getUsrProdId());
			scanQueryParams.addValue("AddTo", productDetail.getAddTo());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					addProdToShopp = new ProductDetail();
					/**
					 * Created list and favorites flag to fetch output
					 * parameters ListFlag and FavoritesFlag. These flag
					 * indicates whether product added to list or favorites.
					 */
					Integer favoritesFlag = null;
					Integer listFlag = null;
					listFlag = (Integer) resultFromProcedure.get("ListFlag");
					if (listFlag != null)
					{
						if (listFlag == 1)
						{
							addProdToShopp.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINLIST);
						}
						else if (listFlag == 0)
						{
							addProdToShopp.setResponseFlag(ApplicationConstants.ADDINGUNASSINGEDPRODUCT);
						}
					}
					favoritesFlag = (Integer) resultFromProcedure.get("FavoritesFlag");

					if (favoritesFlag != null)
					{
						if (favoritesFlag == 1)
						{
							addProdToShopp.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINFAVORITES);
						}
						else if (favoritesFlag == 0)
						{
							addProdToShopp.setResponseFlag(ApplicationConstants.ADDPRODUCTMAINTOSHOPPINGLIST);
						}
						if (listFlag != null && favoritesFlag != null)
						{
							if (listFlag == 1 && favoritesFlag == 1)
							{
								addProdToShopp.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINLISTANDFAVORITES);
							}
							else if (listFlag == 0 && favoritesFlag == 0)
							{
								addProdToShopp.setResponseFlag(ApplicationConstants.SLHISTORYPRODADDEDTOLISTFAVORITESTEXT);
							}
							else if (listFlag == 0 && favoritesFlag == 1)
							{
								addProdToShopp.setResponseFlag(ApplicationConstants.SLHSTADDEDTOLISTNOTFAVORITES);
							}
							else
							{
								addProdToShopp.setResponseFlag(ApplicationConstants.SLHSTADDEDTOFAVORITESNOTLIST);
							}
						}
					}
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerShoppingHistoryMoveItems Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consAddHistoryProdToSL", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	public ProductDetail consAddFavProdToShopList(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddProdToShopp";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		Boolean productExists = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerAddtoShoppingListfromFavorite");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserProductID", productDetail.getUsrProdId());
			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			// addProdToShopp = (List<CouponDetail>)
			// resultFromProcedure.get("productcoupons");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					productExists = (Boolean) resultFromProcedure.get("ProductExists");
					addProdToShopp = new ProductDetail();
					addProdToShopp.setProductExists(productExists);

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerShoppingListSearchAddProduct Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consAddProdToShopp", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	public ProductDetail consAddSearchProdToSL(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddProdToShopp sdfl";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		Boolean productExists = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerSearchAddProdtoTodayListandFavorite");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("ProductID", productDetail.getProductIds());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());
			scanQueryParams.addValue("AddTo", productDetail.getAddTo());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			// addProdToShopp = (List<CouponDetail>)
			// resultFromProcedure.get("productcoupons");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					productExists = (Boolean) resultFromProcedure.get("ProductExists");
					addProdToShopp = new ProductDetail();
					addProdToShopp.setProductExists(productExists);

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerShoppingListSearchAddProduct Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consAddProdToShopp", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	@SuppressWarnings({ "unused", "unused" })
	public ProductDetails getConsWishListProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{

		final String methodName = "getConsWishListProds";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> productDetaillist;
		ProductDetails productDetails = null;
		Integer rowcount = null;
		try
		{
			if (objConsumerRequestDetails.getSearchKey() != null)
			{
				if (objConsumerRequestDetails.getSearchKey().equals(""))
				{
					objConsumerRequestDetails.setSearchKey(null);
				}
			}

			if (null == objConsumerRequestDetails.getLowerLimit())
			{
				objConsumerRequestDetails.setLowerLimit(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerWishListDisplay");
			simpleJdbcCall.returningResultSet("wlproducts", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", objConsumerRequestDetails.getUserId());
			scanQueryParams.addValue("LowerLimit", objConsumerRequestDetails.getLowerLimit());
			scanQueryParams.addValue("RecordCount", objConsumerRequestDetails.getRecordCount());
			scanQueryParams.addValue("MainmenuID", objConsumerRequestDetails.getMainMenuId());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetaillist = (ArrayList<ProductDetail>) resultFromProcedure.get("wlproducts");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetaillist && !productDetaillist.isEmpty())
					{

						log.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetaillist);
						rowcount = (Integer) resultFromProcedure.get("MaxCnt");
						productDetails.setTotalSize(rowcount);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerWishListDisplay Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				productDetails.setTotalSize(rowcount);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getConsWishListProds", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	public ProductDetail consDeleteTodayListProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consDeleteTodayListProd";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail favDeleteProd = null;
		Integer intStatus = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerShoppingListDeleteProducts");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("UserProductID", productDetail.getProductIds());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					intStatus = (Integer) resultFromProcedure.get("Status");

					if (null != intStatus && intStatus == 0)
					{
						favDeleteProd = new ProductDetail();
						favDeleteProd.setResponse(ApplicationConstants.SUCCESS);
					}
					else
					{
						favDeleteProd = new ProductDetail();
						favDeleteProd.setResponse(ApplicationConstants.FAILURE);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerFavoriteSLDeleteProducts Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consFavDeleteProd", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return favDeleteProd;
	}

	public ProductDetail consCheckOutTodayListProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consCheckOutTodayListProd";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail favDeleteProd = null;
		Integer intStatus = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerShoppingListCheckoutProducts");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					intStatus = (Integer) resultFromProcedure.get("Status");

					if (null != intStatus && intStatus == 0)
					{
						favDeleteProd = new ProductDetail();
						favDeleteProd.setResponse(ApplicationConstants.SUCCESS);
					}
					else
					{
						favDeleteProd = new ProductDetail();
						favDeleteProd.setResponse(ApplicationConstants.FAILURE);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerShoppingListCheckoutProducts Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consCheckOutTodayListProd", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return favDeleteProd;
	}

	public ProductDetail consMoveToBasketProds(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consMoveToBasketProds";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail favDeleteProd = null;
		Integer intStatus = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerShoppingCartAddDelete");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("UserProductID", productDetail.getUsrProdId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					intStatus = (Integer) resultFromProcedure.get("Status");

					if (null != intStatus && intStatus == 0)
					{
						favDeleteProd = new ProductDetail();
						favDeleteProd.setResponse(ApplicationConstants.SUCCESS);
					}
					else
					{
						favDeleteProd = new ProductDetail();
						favDeleteProd.setResponse(ApplicationConstants.FAILURE);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerShoppingCartAddDelete Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consFavDeleteProd", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return favDeleteProd;
	}

	@SuppressWarnings("unused")
	public ArrayList<ProductDetail> getConsWLHistoryProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsWLHistoryProds";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> productDetaillist;
		ProductDetails productDetails = null;
		Integer rowcount = null;
		try
		{
			if (objConsumerRequestDetails.getSearchKey() != null)
			{
				if (objConsumerRequestDetails.getSearchKey().equals(""))
				{
					objConsumerRequestDetails.setSearchKey(null);
				}
			}

			if (null == objConsumerRequestDetails.getLowerLimit())
			{
				objConsumerRequestDetails.setLowerLimit(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerWishListHistoryDisplay");
			simpleJdbcCall.returningResultSet("wlproducts", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserId", objConsumerRequestDetails.getUserId());
			scanQueryParams.addValue("LowerLimit", objConsumerRequestDetails.getLowerLimit());
			scanQueryParams.addValue("RecordCount", objConsumerRequestDetails.getRecordCount());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetaillist = (ArrayList<ProductDetail>) resultFromProcedure.get("wlproducts");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetaillist && !productDetaillist.isEmpty())
					{

						log.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetaillist);
						rowcount = (Integer) resultFromProcedure.get("MaxCnt");
						productDetails.setTotalSize(rowcount);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerWishListHistoryDisplay Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				productDetails.setTotalSize(rowcount);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getConsWLHistoryProds", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetaillist;
	}

	
	public ProductDetail consDeleteWLProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consDeleteWLProd";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail wishListDeleteProd = null;
		Integer intStatus = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerWishListDeleteProducts");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("UserProductID", productDetail.getUsrProdId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					intStatus = (Integer) resultFromProcedure.get("Status");

					if (null != intStatus && intStatus == 0)
					{
						wishListDeleteProd = new ProductDetail();
						wishListDeleteProd.setResponse(ApplicationConstants.SUCCESS);
					}
					else
					{
						wishListDeleteProd = new ProductDetail();
						wishListDeleteProd.setResponse(ApplicationConstants.FAILURE);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerWishListDeleteProducts Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consDeleteWLProd", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return wishListDeleteProd;
	}

	
	public ProductDetail consDeleteWLHistoryProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consDeleteWLHistoryProd";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail historyDeleteProd = null;
		Integer intStatus = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerWishListHistorytDelete");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("UserProductID", productDetail.getUsrProdId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					intStatus = (Integer) resultFromProcedure.get("Status");

					if (null != intStatus && intStatus == 0)
					{
						historyDeleteProd = new ProductDetail();
						historyDeleteProd.setResponse(ApplicationConstants.SUCCESS);
					}
					else
					{
						historyDeleteProd = new ProductDetail();
						historyDeleteProd.setResponse(ApplicationConstants.FAILURE);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerWishListHistorytDelete Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consDeleteWLHistoryProd", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return historyDeleteProd;
	}

	
	public ProductDetail consAddSearchProdToWL(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddSearchProdToWL";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToWL = null;
		Boolean productExists = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductSearchAddtoWishList");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", productDetail.getUserId());
			scanQueryParams.addValue("ProductID", productDetail.getProductIds());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					productExists = (Boolean) resultFromProcedure.get("ProductExists");
					addProdToWL = new ProductDetail();
					addProdToWL.setProductExists(productExists);
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerProductSearchAddtoWishList Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consAddSearchProdToWL", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return addProdToWL;
	}

	@SuppressWarnings("unchecked")
	
	public ArrayList<CouponDetails> getConsWLCouponDetails(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsWLCouponDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<CouponDetails> couponDetailslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerWishListCouponDisplay");
			simpleJdbcCall.returningResultSet("wistListCoupons", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", productDetail.getUserId());
			scanQueryParams.addValue("ProductId", productDetail.getProductId());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());
			scanQueryParams.addValue("AlertedProductID", productDetail.getAlertProdID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			couponDetailslst = (ArrayList<CouponDetails>) resultFromProcedure.get("wistListCoupons");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in getConsWLCouponDetails method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return couponDetailslst;
	}

	
	public ArrayList<HotDealsDetails> getConsWLHotDealInfo(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsWLHotDealInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<HotDealsDetails> lstHotDeals = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerWishListProductHotDealInformation");
			simpleJdbcCall.returningResultSet("wlhotdeals", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", productDetail.getUserId());
			scanQueryParams.addValue("ProductId", productDetail.getProductId());
			scanQueryParams.addValue("MainMenuID", productDetail.getMainMenuID());
			scanQueryParams.addValue("AlertedProductID", productDetail.getAlertProdID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			lstHotDeals = (ArrayList<HotDealsDetails>) resultFromProcedure.get("wlhotdeals");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in getConsWLHotDealInfo method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return lstHotDeals;
	}

	
	public Integer getPostalCode(Long lngUserId) throws ScanSeeWebSqlException
	{
		String response = ApplicationConstants.FAILURE;
		Integer intPostalcode = null;
		String getZipcodeQuery = "SELECT PostalCode from Users where UserID=?";
		try
		{
			intPostalcode = this.jdbcTemplate.queryForInt(getZipcodeQuery, lngUserId);

		}
		catch (DataAccessException exception)
		{
			exception.printStackTrace();
			log.error("Exception occurred in getPostalCode", ApplicationConstants.EXCEPTIONOCCURRED, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			log.error("Exception occurred in getPostalCode", ApplicationConstants.EXCEPTIONOCCURRED, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return intPostalcode;
	}

	
	public String consWLAddCoupon(CouponDetail objCouponDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consWLAddCoupon ";
		log.info(ApplicationConstants.METHODSTART + methodName);

		Integer intStatus = null;
		String strResponse = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerAddCoupon");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", objCouponDetail.getUserId());
			scanQueryParams.addValue("CouponID", objCouponDetail.getCouponId());
			scanQueryParams.addValue("CouponListID", objCouponDetail.getCouponListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					intStatus = (Integer) resultFromProcedure.get("Status");
					if (null != intStatus && intStatus == 0)
					{
						strResponse = ApplicationConstants.SUCCESS;
					}
					else
					{
						strResponse = ApplicationConstants.FAILURE;
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerAddCoupon Store Procedure error number: {} and error message: {}", errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consWLAddCoupon", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	
	public String consWLRemoveCoupon(CouponDetail objCouponDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consWLRemoveCoupon";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer intStatus = null;
		String strResponse = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerDeleteCoupon");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("UserID", objCouponDetail.getUserId());
			scanQueryParams.addValue("CouponID", objCouponDetail.getCouponId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					intStatus = (Integer) resultFromProcedure.get("Status");
					if (null != intStatus && intStatus == 0)
					{
						strResponse = ApplicationConstants.SUCCESS;
					}
					else
					{
						strResponse = ApplicationConstants.FAILURE;
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerDeleteCoupon Store Procedure error number: {} and error message: {}", errorNum, errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in consWLRemoveCoupon", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	@SuppressWarnings("unused")
	
	public ArrayList<ProductDetail> getPrintProductInfo(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getPrintProductInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> lstProductDetails = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerPrintProductInfo");
			simpleJdbcCall.returningResultSet("printprodinfo", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();

			scanQueryParams.addValue("ProductIDs", productDetail.getProductIds());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			lstProductDetails = (ArrayList<ProductDetail>) resultFromProcedure.get("printprodinfo");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_WebConsumerPrintProductInfo Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getPrintProductInfo", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return lstProductDetails;
	}

	
	public ProductDetails getConsCoupAssocitedProds(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsCoupAssocitedProds in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> productlst = null;
		ProductDetails objProductDetails = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGetCLRProductDetails");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();

			fetchProductDetailsParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchProductDetailsParameters.addValue("CouponID", clrDetailsObj.getCouponId());
			fetchProductDetailsParameters.addValue("LoyaltyID", clrDetailsObj.getLoyaltyDealID());
			fetchProductDetailsParameters.addValue("RebateID", clrDetailsObj.getRebateId());
			fetchProductDetailsParameters.addValue("MainMenuID", clrDetailsObj.getMainMenuId());
			fetchProductDetailsParameters.addValue("CouponListID", clrDetailsObj.getCouponListID());
			fetchProductDetailsParameters.addValue("LoyaltyListID", clrDetailsObj.getLoyaltyListID());
			fetchProductDetailsParameters.addValue("RebateListID", clrDetailsObj.getRebateListID());

			fetchProductDetailsParameters.addValue("ErrorNumber", null);
			fetchProductDetailsParameters.addValue("ErrorMessage", null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{
				productlst = (ArrayList<ProductDetail>) resultFromProcedure.get("productDetails");

				if (null != productlst && !productlst.isEmpty())
				{
					objProductDetails = new ProductDetails();
					objProductDetails.setProductDetail(productlst);
				}
			}

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_GetCLRProductDetails  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getConsCoupAssocitedProds", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return objProductDetails;
	}

}
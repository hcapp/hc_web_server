package shopper.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CategoryInfo;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.LoyaltyDetail;
import common.pojo.shopper.RebateDetail;

/**
 * This class for fetching my gallery coupons,loyalty and rebate information.
 * 
 * @author sowjanya_d
 */
public class MyGalleryDAOImpl implements MyGalleryDAO
{

	/**
	 * Logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(MyGalleryDAOImpl.class.getName());

	/**
	 * for jdbcTemplate connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * This method for to get data source Spring DI.
	 * 
	 * @param dataSource
	 *            for DB operations.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This DAOImpl method is used to fetch my gallery c,l,r information based
	 * on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings({ "unchecked" })
	public CLRDetails getMyGalleryInfo(CLRDetails clrDetails) throws ScanSeeWebSqlException
	{

		final String methodName = "getMyGalleryInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);

		CLRDetails clrDetatilsObj = null;

		final List<RebateDetail> rebateDetailList = null;
		final List<LoyaltyDetail> loyaltyDetailList = null;
		List<CouponDetails> couponDetailList = null;

		Boolean couponNextPage = false;
		final Boolean loyaltyNextPage = false;
		final Boolean rebateNextPage = false;
		int totalSize = 0;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGalleryCoupons");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());

			if (null == clrDetails.getCategoryID())
			{
				clrDetails.setCategoryID(0);
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			if (null == clrDetails.getSearchKey())
			{
				clrDetails.setCategoryID(null);
				fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());
			}
			else
			{
				fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());

			}

			if (null == clrDetails.getRetailerId())
			{
				clrDetails.setRetailerId(0);
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}

			if (null != clrDetails.getLowerLimit())
			{

				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			fetchCouponParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.CLRSCREENNAME);

			fetchCouponParameters.addValue("RecordCount", clrDetails.getRecordCount());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryCoupons method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					couponNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					totalSize = (Integer) resultFromProcedure.get("MaxCnt");
				}
			}
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		/*
		 * try { simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		 * simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
		 * simpleJdbcCall.withProcedureName("usp_GalleryLoyalty");
		 * simpleJdbcCall.returningResultSet("LoyaltyDetails", new
		 * BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class)); final
		 * MapSqlParameterSource fetchLoyaltyParameters = new
		 * MapSqlParameterSource(); //
		 * fetchLoyaltyParameters.addValue("RetailerID", 0);
		 * fetchLoyaltyParameters.addValue("SearchKey", null); //
		 * fetchLoyaltyParameters.addValue("CategoryIDs", 0);
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.USERID,
		 * clrDetails.getUserId()); if (clrDetails.getCategoryID() == null) {
		 * clrDetails.setCategoryID(0);
		 * fetchLoyaltyParameters.addValue("CategoryIDs",
		 * clrDetails.getCategoryID()); } else {
		 * fetchLoyaltyParameters.addValue("CategoryIDs",
		 * clrDetails.getCategoryID()); } // for retailer id validation ,if it
		 * is null ,setting zero to // category id. if
		 * (clrDetails.getRetailerId() == null) { clrDetails.setRetailerId(0);
		 * fetchLoyaltyParameters.addValue("RetailerID",
		 * clrDetails.getRetailerId()); } else {
		 * fetchLoyaltyParameters.addValue("RetailerID",
		 * clrDetails.getRetailerId()); } if (clrDetails.getLowerLimit() !=
		 * null) {
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT,
		 * clrDetails.getLowerLimit()); } else {
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
		 * } fetchLoyaltyParameters.addValue(ApplicationConstants.SCREENNAME,
		 * ApplicationConstants.CLRSCREENNAME); final Map<String, Object>
		 * resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);
		 * if (null !=
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
		 * Integer errorNum = (Integer)
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
		 * String errorMsg = (String)
		 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
		 * "Error Occured in usp_GalleryLoyalty method ..errorNum..{} errorMsg {}"
		 * , errorNum, errorMsg); throw new ScanSeeWebSqlException(errorMsg); }
		 * else { loyaltyDetailList = (List<LoyaltyDetail>)
		 * resultFromProcedure.get("LoyaltyDetails"); loyaltyNextPage =
		 * (Boolean) resultFromProcedure.get("NxtPageFlag"); } } catch
		 * (DataAccessException e) {
		 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		 * throw new ScanSeeWebSqlException(e); } try { simpleJdbcCall = new
		 * SimpleJdbcCall(jdbcTemplate);
		 * simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
		 * simpleJdbcCall.withProcedureName("usp_GalleryRebate");
		 * simpleJdbcCall.returningResultSet("RebateDetails", new
		 * BeanPropertyRowMapper<RebateDetail>(RebateDetail.class)); final
		 * MapSqlParameterSource fetchRebateParameters = new
		 * MapSqlParameterSource();
		 * fetchRebateParameters.addValue(ApplicationConstants.USERID,
		 * clrDetails.getUserId()); if (clrDetails.getLowerLimit() != null) {
		 * fetchRebateParameters.addValue(ApplicationConstants.LOWERLIMIT,
		 * clrDetails.getLowerLimit()); } else {
		 * fetchRebateParameters.addValue(ApplicationConstants.LOWERLIMIT, 0); }
		 * fetchRebateParameters.addValue(ApplicationConstants.SCREENNAME,
		 * ApplicationConstants.CLRSCREENNAME); final Map<String, Object>
		 * resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);
		 * if (null !=
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
		 * Integer errorNum = (Integer)
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
		 * String errorMsg = (String)
		 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
		 * log.info("Error Occured in usp_GalleryRebate  ..errorNum.{} errorMsg {}"
		 * , errorNum, errorMsg); throw new ScanSeeWebSqlException(errorMsg); }
		 * else { rebateDetailList = (List<RebateDetail>)
		 * resultFromProcedure.get("RebateDetails"); rebateNextPage = (Boolean)
		 * resultFromProcedure.get("NxtPageFlag"); } } catch
		 * (DataAccessException exception) {
		 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
		 * exception); throw new ScanSeeWebSqlException(exception); }
		 */

		if (couponDetailList != null || !couponDetailList.isEmpty() || rebateDetailList != null || !rebateDetailList.isEmpty()
				|| loyaltyDetailList != null || !loyaltyDetailList.isEmpty())
		{
			clrDetatilsObj = new CLRDetails();
			if (couponNextPage != null)
			{
				if (couponNextPage)
				{
					clrDetatilsObj.setClrC(1);
					clrDetatilsObj.setTotalSize(totalSize);
				}
				else
				{
					clrDetatilsObj.setClrC(0);
				}
			}
			if (loyaltyNextPage != null)
			{
				if (loyaltyNextPage)
				{
					clrDetatilsObj.setClrL(1);
				}
				else
				{
					clrDetatilsObj.setClrL(0);
				}
			}
			if (rebateNextPage != null)
			{
				if (rebateNextPage)
				{
					clrDetatilsObj.setClrR(1);
				}
				else
				{
					clrDetatilsObj.setClrR(0);
				}
			}

			if (couponNextPage || loyaltyNextPage || rebateNextPage)
			{
				clrDetatilsObj.setNextPage(1);
			}
			else
			{
				clrDetatilsObj.setNextPage(0);
			}

			if (couponDetailList != null && !couponDetailList.isEmpty())
			{
				clrDetatilsObj.setIsCouponthere(true);
				clrDetatilsObj.setCouponDetails(couponDetailList);
			}
			if (rebateDetailList != null && !rebateDetailList.isEmpty())
			{
				clrDetatilsObj.setIsRebatethere(true);
				clrDetatilsObj.setRebateDetails(rebateDetailList);
			}
			if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
			{
				clrDetatilsObj.setIsLoyaltythere(true);
				clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
			}

		}
		clrDetatilsObj.setTotalSize(totalSize);
		log.info(ApplicationConstants.METHODEND + methodName);
		return clrDetatilsObj;

	}

	/**
	 * This DAOImpl method is used to fetch my gallery of All type c,l,r
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the Service
	 *             layer
	 */
	public CLRDetails getMyGalleryAllTypeInfo(CLRDetails clrDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "getMyGalleryAllTypeInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;

		List<RebateDetail> rebateDetailList = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		List<CouponDetails> couponDetailList = null;

		Boolean couponNextPage = false;
		Boolean loyaltyNextPage = false;
		Boolean rebateNextPage = false;
		int totalSize = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGalleryAllCoupons");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			if (clrDetails.getLowerLimit() != null)
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			fetchCouponParameters.addValue("MainMenuID", clrDetails.getMainMenuId());
			fetchCouponParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.CLRSCREENNAME);
			if (clrDetails.getSearchKey() == null)
			{
				clrDetails.setCategoryID(null);
				fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());
			}
			else
			{
				fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());

			}
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}

			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			fetchCouponParameters.addValue("RecordCount", clrDetails.getRecordCount());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryAllCoupons method  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					couponNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					totalSize = (Integer) resultFromProcedure.get("MaxCnt");
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		/*
		 * try { simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		 * simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
		 * simpleJdbcCall.withProcedureName("usp_GalleryAllLoyaltyDeals");
		 * simpleJdbcCall.returningResultSet("LoyaltyDetails", new
		 * BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class)); final
		 * MapSqlParameterSource fetchLoyaltyParameters = new
		 * MapSqlParameterSource();
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.USERID,
		 * clrDetails.getUserId()); if (clrDetails.getLowerLimit() != null) {
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT,
		 * clrDetails.getLowerLimit()); } else {
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
		 * } fetchLoyaltyParameters.addValue(ApplicationConstants.SCREENNAME,
		 * ApplicationConstants.CLRSCREENNAME);
		 * fetchLoyaltyParameters.addValue("SearchKey", null); if
		 * (clrDetails.getCategoryID() == null) { clrDetails.setCategoryID(0);
		 * fetchLoyaltyParameters.addValue("CategoryIDs",
		 * clrDetails.getCategoryID()); } else {
		 * fetchLoyaltyParameters.addValue("CategoryIDs",
		 * clrDetails.getCategoryID()); } if (clrDetails.getRetailerId() ==
		 * null) { clrDetails.setRetailerId(0);
		 * fetchLoyaltyParameters.addValue("RetailerID",
		 * clrDetails.getRetailerId()); } else {
		 * fetchLoyaltyParameters.addValue("RetailerID",
		 * clrDetails.getRetailerId()); } final Map<String, Object>
		 * resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);
		 * if (null !=
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
		 * Integer errorNum = (Integer)
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
		 * String errorMsg = (String)
		 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
		 * " Error Occured in usp_GalleryAllLoyaltyDeals method ..errorNum..{} errorMsg {}"
		 * , errorNum, errorMsg); throw new ScanSeeWebSqlException(errorMsg); }
		 * else { loyaltyDetailList = (List<LoyaltyDetail>)
		 * resultFromProcedure.get("LoyaltyDetails"); loyaltyNextPage =
		 * (Boolean) resultFromProcedure.get("NxtPageFlag"); } } catch
		 * (DataAccessException e) {
		 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		 * throw new ScanSeeWebSqlException(e); } try { simpleJdbcCall = new
		 * SimpleJdbcCall(jdbcTemplate);
		 * simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
		 * simpleJdbcCall.withProcedureName("usp_GalleryAllRebates");
		 * simpleJdbcCall.returningResultSet("RebateDetails", new
		 * BeanPropertyRowMapper<RebateDetail>(RebateDetail.class)); final
		 * MapSqlParameterSource fetchRebateParameters = new
		 * MapSqlParameterSource();
		 * fetchRebateParameters.addValue(ApplicationConstants.USERID,
		 * clrDetails.getUserId()); if (clrDetails.getLowerLimit() != null) {
		 * fetchRebateParameters.addValue(ApplicationConstants.LOWERLIMIT,
		 * clrDetails.getLowerLimit()); } else {
		 * fetchRebateParameters.addValue(ApplicationConstants.LOWERLIMIT, 0); }
		 * fetchRebateParameters.addValue(ApplicationConstants.SCREENNAME,
		 * ApplicationConstants.CLRSCREENNAME); final Map<String, Object>
		 * resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);
		 * if (null !=
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
		 * Integer errorNum = (Integer)
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
		 * String errorMsg = (String)
		 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
		 * "Error Occured in usp_GalleryAllRebates  ..errorNum.{} errorMsg {} ",
		 * errorNum, errorMsg); throw new ScanSeeWebSqlException(errorMsg); }
		 * else { rebateDetailList = (List<RebateDetail>)
		 * resultFromProcedure.get("RebateDetails"); rebateNextPage = (Boolean)
		 * resultFromProcedure.get("NxtPageFlag"); } } catch
		 * (DataAccessException exception) {
		 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
		 * exception); throw new ScanSeeWebSqlException(exception); }
		 */

		if (couponDetailList != null || !couponDetailList.isEmpty() || rebateDetailList != null || !rebateDetailList.isEmpty()
				|| loyaltyDetailList != null || !loyaltyDetailList.isEmpty())
		{
			clrDetatilsObj = new CLRDetails();
			if (couponNextPage != null)
			{
				if (couponNextPage)
				{
					clrDetatilsObj.setClrC(1);
				}
				else
				{
					clrDetatilsObj.setClrC(0);
				}
			}
			if (loyaltyNextPage != null)
			{
				if (loyaltyNextPage)
				{
					clrDetatilsObj.setClrL(1);
				}
				else
				{
					clrDetatilsObj.setClrL(0);
				}
			}
			if (rebateNextPage != null)
			{
				if (rebateNextPage)
				{
					clrDetatilsObj.setClrR(1);
				}
				else
				{
					clrDetatilsObj.setClrR(0);
				}
			}

			if (couponNextPage || loyaltyNextPage || rebateNextPage)
			{
				clrDetatilsObj.setNextPage(1);
			}
			else
			{
				clrDetatilsObj.setNextPage(0);
			}
			if (couponDetailList != null && !couponDetailList.isEmpty())
			{
				clrDetatilsObj.setIsCouponthere(true);
				clrDetatilsObj.setCouponDetails(couponDetailList);
			}
			if (rebateDetailList != null && !rebateDetailList.isEmpty())
			{
				clrDetatilsObj.setIsRebatethere(true);
				clrDetatilsObj.setRebateDetails(rebateDetailList);
			}
			if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
			{
				clrDetatilsObj.setIsLoyaltythere(true);
				clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
			}

		}
		clrDetatilsObj.setTotalSize(totalSize);
		log.info(ApplicationConstants.METHODEND + methodName);
		return clrDetatilsObj;

	}

	/**
	 * This DAOImpl method is used to fetch my gallery of type Expired c,l,r
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSeeWebSqlException
	 *             Exception defined for the application will be thrown which is
	 *             caught in the Service layer
	 */
	public CLRDetails getMyGalleryExpiredTypeInfo(CLRDetails clrDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "getMyGalleryExpiredTypeInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;

		List<RebateDetail> rebateDetailList = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		List<CouponDetails> couponDetailList = null;

		Boolean couponNextPage = false;
		Boolean loyaltyNextPage = false;
		Boolean rebateNextPage = false;
		int totalSize = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGalleryExpiredCoupons");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			if (clrDetails.getLowerLimit() != null)
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			fetchCouponParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.CLRSCREENNAME);

			if (clrDetails.getSearchKey() == null)
			{
				clrDetails.setCategoryID(null);
				fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());
			}
			else
			{
				fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());

			}

			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}

			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			fetchCouponParameters.addValue("RecordCount", clrDetails.getRecordCount());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryExpiredCoupons method ..errorNum. {} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					couponNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					totalSize = (Integer) resultFromProcedure.get("MaxCnt");
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		/*
		 * try { simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		 * simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
		 * simpleJdbcCall.withProcedureName("usp_GalleryExpiredLoyaltyDeals");
		 * simpleJdbcCall.returningResultSet("LoyaltyDetails", new
		 * BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class)); final
		 * MapSqlParameterSource fetchLoyaltyParameters = new
		 * MapSqlParameterSource();
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.USERID,
		 * clrDetails.getUserId()); if (clrDetails.getLowerLimit() != null) {
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT,
		 * clrDetails.getLowerLimit()); } else {
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
		 * } fetchLoyaltyParameters.addValue(ApplicationConstants.SCREENNAME,
		 * ApplicationConstants.CLRSCREENNAME);
		 * fetchLoyaltyParameters.addValue("SearchKey", null); if
		 * (clrDetails.getCategoryID() == null) { clrDetails.setCategoryID(0);
		 * fetchLoyaltyParameters.addValue("CategoryIDs",
		 * clrDetails.getCategoryID()); } else {
		 * fetchLoyaltyParameters.addValue("CategoryIDs",
		 * clrDetails.getCategoryID()); } if (clrDetails.getRetailerId() ==
		 * null) { clrDetails.setRetailerId(0);
		 * fetchLoyaltyParameters.addValue("RetailerID",
		 * clrDetails.getRetailerId()); } else {
		 * fetchLoyaltyParameters.addValue("RetailerID",
		 * clrDetails.getRetailerId()); } final Map<String, Object>
		 * resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);
		 * if (null !=
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
		 * Integer errorNum = (Integer)
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
		 * String errorMsg = (String)
		 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
		 * "Error Occured in usp_GalleryExpiredLoyaltyDeals method ..errorNum..{} errorMsg {}"
		 * , errorNum, errorMsg); throw new ScanSeeWebSqlException(errorMsg); }
		 * else { loyaltyDetailList = (List<LoyaltyDetail>)
		 * resultFromProcedure.get("LoyaltyDetails"); loyaltyNextPage =
		 * (Boolean) resultFromProcedure.get("NxtPageFlag"); } } catch
		 * (DataAccessException e) {
		 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		 * throw new ScanSeeWebSqlException(e); } try { simpleJdbcCall = new
		 * SimpleJdbcCall(jdbcTemplate);
		 * simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
		 * simpleJdbcCall.withProcedureName("usp_GalleryExpiredRebates");
		 * simpleJdbcCall.returningResultSet("RebateDetails", new
		 * BeanPropertyRowMapper<RebateDetail>(RebateDetail.class)); final
		 * MapSqlParameterSource fetchRebateParameters = new
		 * MapSqlParameterSource();
		 * fetchRebateParameters.addValue(ApplicationConstants.USERID,
		 * clrDetails.getUserId()); if (clrDetails.getLowerLimit() != null) {
		 * fetchRebateParameters.addValue(ApplicationConstants.LOWERLIMIT,
		 * clrDetails.getLowerLimit()); } else {
		 * fetchRebateParameters.addValue(ApplicationConstants.LOWERLIMIT, 0); }
		 * fetchRebateParameters.addValue(ApplicationConstants.SCREENNAME,
		 * ApplicationConstants.CLRSCREENNAME); final Map<String, Object>
		 * resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);
		 * if (null !=
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
		 * Integer errorNum = (Integer)
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
		 * String errorMsg = (String)
		 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
		 * "Error Occured in usp_GalleryExpiredRebates  ..errorNum.{} errorMsg {}"
		 * , errorNum, errorMsg); throw new ScanSeeWebSqlException(errorMsg); }
		 * else { rebateDetailList = (List<RebateDetail>)
		 * resultFromProcedure.get("RebateDetails"); rebateNextPage = (Boolean)
		 * resultFromProcedure.get("NxtPageFlag"); } } catch
		 * (DataAccessException exception) {
		 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
		 * exception); throw new ScanSeeWebSqlException(exception); }
		 */
		if (couponDetailList != null || !couponDetailList.isEmpty() || rebateDetailList != null || !rebateDetailList.isEmpty()
				|| loyaltyDetailList != null || !loyaltyDetailList.isEmpty())
		{
			clrDetatilsObj = new CLRDetails();
			if (couponNextPage)
			{
				clrDetatilsObj.setClrC(1);
			}
			else
			{
				clrDetatilsObj.setClrC(0);
			}
			if (loyaltyNextPage)
			{
				clrDetatilsObj.setClrL(1);
			}
			else
			{
				clrDetatilsObj.setClrL(0);
			}
			if (rebateNextPage)
			{
				clrDetatilsObj.setClrR(1);
			}
			else
			{
				clrDetatilsObj.setClrR(0);
			}
			if (couponNextPage || loyaltyNextPage || rebateNextPage)
			{
				clrDetatilsObj.setNextPage(1);
			}
			else
			{
				clrDetatilsObj.setNextPage(0);
			}
			if (couponDetailList != null && !couponDetailList.isEmpty())
			{
				clrDetatilsObj.setIsCouponthere(true);
				clrDetatilsObj.setCouponDetails(couponDetailList);
			}
			if (rebateDetailList != null && !rebateDetailList.isEmpty())
			{
				clrDetatilsObj.setIsRebatethere(true);
				clrDetatilsObj.setRebateDetails(rebateDetailList);
			}
			if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
			{
				clrDetatilsObj.setIsLoyaltythere(true);
				clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
			}

		}
		clrDetatilsObj.setTotalSize(totalSize);
		log.info(ApplicationConstants.METHODEND + methodName);
		return clrDetatilsObj;

	}

	/**
	 * This DAOImpl method is used to fetch my gallery of type Used c,l,r
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the service
	 *             layer
	 */

	public CLRDetails getMyGalleryUsedTypeInfo(CLRDetails clrDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "getMyGalleryUsedTypeInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;

		List<RebateDetail> rebateDetailList = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		List<CouponDetails> couponDetailList = null;

		Boolean couponNextPage = false;
		Boolean loyaltyNextPage = false;
		Boolean rebateNextPage = false;
		int totalSize = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGalleryUsedCoupons");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			if (clrDetails.getLowerLimit() != null)
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			fetchCouponParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.CLRSCREENNAME);
			if (clrDetails.getSearchKey() == null)
			{
				clrDetails.setCategoryID(null);
				fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());
			}
			else
			{
				fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());

			}

			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}

			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			fetchCouponParameters.addValue("RecordCount", clrDetails.getRecordCount());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryUsedCoupons method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					couponNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					totalSize = (Integer) resultFromProcedure.get("MaxCnt");
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		/*
		 * try { simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		 * simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
		 * simpleJdbcCall.withProcedureName("usp_GalleryUsedLoyaltyDeals");
		 * simpleJdbcCall.returningResultSet("LoyaltyDetails", new
		 * BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class)); final
		 * MapSqlParameterSource fetchLoyaltyParameters = new
		 * MapSqlParameterSource();
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.USERID,
		 * clrDetails.getUserId()); if (clrDetails.getLowerLimit() != null) {
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT,
		 * clrDetails.getLowerLimit()); } else {
		 * fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
		 * } fetchLoyaltyParameters.addValue(ApplicationConstants.SCREENNAME,
		 * ApplicationConstants.CLRSCREENNAME);
		 * fetchLoyaltyParameters.addValue("SearchKey", null); if
		 * (clrDetails.getCategoryID() == null) { clrDetails.setCategoryID(0);
		 * fetchLoyaltyParameters.addValue("CategoryIDs",
		 * clrDetails.getCategoryID()); } else {
		 * fetchLoyaltyParameters.addValue("CategoryIDs",
		 * clrDetails.getCategoryID()); } if (clrDetails.getRetailerId() ==
		 * null) { clrDetails.setRetailerId(0);
		 * fetchLoyaltyParameters.addValue("RetailerID",
		 * clrDetails.getRetailerId()); } else {
		 * fetchLoyaltyParameters.addValue("RetailerID",
		 * clrDetails.getRetailerId()); } final Map<String, Object>
		 * resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);
		 * if (null !=
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
		 * Integer errorNum = (Integer)
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
		 * String errorMsg = (String)
		 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
		 * "Error Occured in usp_GalleryUsedLoyaltyDeals method ..errorNum..{} errorMsg {}"
		 * , errorNum, errorMsg); throw new ScanSeeWebSqlException(errorMsg); }
		 * else { loyaltyDetailList = (List<LoyaltyDetail>)
		 * resultFromProcedure.get("LoyaltyDetails"); loyaltyNextPage =
		 * (Boolean) resultFromProcedure.get("NxtPageFlag"); } } catch
		 * (DataAccessException e) {
		 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		 * throw new ScanSeeWebSqlException(e); } try { simpleJdbcCall = new
		 * SimpleJdbcCall(jdbcTemplate);
		 * simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
		 * simpleJdbcCall.withProcedureName("usp_GalleryUsedRebate");
		 * simpleJdbcCall.returningResultSet("RebateDetails", new
		 * BeanPropertyRowMapper<RebateDetail>(RebateDetail.class)); final
		 * MapSqlParameterSource fetchRebateParameters = new
		 * MapSqlParameterSource();
		 * fetchRebateParameters.addValue(ApplicationConstants.USERID,
		 * clrDetails.getUserId()); if (clrDetails.getLowerLimit() != null) {
		 * fetchRebateParameters.addValue(ApplicationConstants.LOWERLIMIT,
		 * clrDetails.getLowerLimit()); } else {
		 * fetchRebateParameters.addValue(ApplicationConstants.LOWERLIMIT, 0); }
		 * fetchRebateParameters.addValue(ApplicationConstants.SCREENNAME,
		 * ApplicationConstants.CLRSCREENNAME); final Map<String, Object>
		 * resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);
		 * if (null !=
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
		 * Integer errorNum = (Integer)
		 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
		 * String errorMsg = (String)
		 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
		 * "Error Occured in usp_GalleryUsedRebate  ..errorNum.{} errorMsg {}",
		 * errorNum, errorMsg); throw new ScanSeeWebSqlException(errorMsg); }
		 * else { rebateDetailList = (List<RebateDetail>)
		 * resultFromProcedure.get("RebateDetails"); rebateNextPage = (Boolean)
		 * resultFromProcedure.get("NxtPageFlag"); } } catch
		 * (DataAccessException exception) {
		 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
		 * exception); throw new ScanSeeWebSqlException(exception); }
		 */

		if (couponDetailList != null || !couponDetailList.isEmpty() || rebateDetailList != null || !rebateDetailList.isEmpty()
				|| loyaltyDetailList != null || !loyaltyDetailList.isEmpty())
		{
			clrDetatilsObj = new CLRDetails();
			if (couponNextPage)
			{
				clrDetatilsObj.setClrC(1);
			}
			else
			{
				clrDetatilsObj.setClrC(0);
			}
			if (loyaltyNextPage)
			{
				clrDetatilsObj.setClrL(1);
			}
			else
			{
				clrDetatilsObj.setClrL(0);
			}
			if (rebateNextPage)
			{
				clrDetatilsObj.setClrR(1);
			}
			else
			{
				clrDetatilsObj.setClrR(0);
			}
			if (couponNextPage || loyaltyNextPage || rebateNextPage)
			{
				clrDetatilsObj.setNextPage(1);
			}
			else
			{
				clrDetatilsObj.setNextPage(0);
			}
			if (couponDetailList != null && !couponDetailList.isEmpty())
			{
				clrDetatilsObj.setIsCouponthere(true);
				clrDetatilsObj.setCouponDetails(couponDetailList);
			}
			if (rebateDetailList != null && !rebateDetailList.isEmpty())
			{
				clrDetatilsObj.setIsRebatethere(true);
				clrDetatilsObj.setRebateDetails(rebateDetailList);
			}
			if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
			{
				clrDetatilsObj.setIsLoyaltythere(true);
				clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
			}

		}
		clrDetatilsObj.setTotalSize(totalSize);
		log.info(ApplicationConstants.METHODEND + methodName);
		return clrDetatilsObj;

	}

	/**
	 * This DAOImpl method is used to redeem coupon information based on userId.
	 * 
	 * @param couponDetailsObj
	 *            contains user id,coupon id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	/*
	 * @Override public String userRedeemCoupon(CouponDetails couponDetailsObj)
	 * throws ScanSeeException { final String methodName =
	 * "userRedeemCoupon in DAO layer";
	 * log.info(ApplicationConstants.METHODSTART + methodName); Integer fromProc
	 * = null; String responseFromProc = null; Integer usedFlag = null; try {
	 * simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
	 * simpleJdbcCall.withProcedureName("usp_GalleryRedeemCoupon"); final
	 * MapSqlParameterSource fetchCouponParameters = new
	 * MapSqlParameterSource();
	 * fetchCouponParameters.addValue(ApplicationConstants.USERID,
	 * couponDetailsObj.getUserId()); fetchCouponParameters.addValue("CouponID",
	 * couponDetailsObj.getCouponId());
	 * fetchCouponParameters.addValue("UserCouponClaimTypeID",
	 * couponDetailsObj.getUserCouponClaimTypeID());
	 * fetchCouponParameters.addValue("CouponPayoutMethod",
	 * couponDetailsObj.getCouponPayoutMethod());
	 * fetchCouponParameters.addValue("RetailLocationID",
	 * couponDetailsObj.getRetailLocationID());
	 * fetchCouponParameters.addValue("CouponClaimDate",
	 * couponDetailsObj.getCouponClaimDate()); final Map<String, Object>
	 * resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters); if
	 * (null != resultFromProcedure) { if (null !=
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
	 * Integer errorNum = (Integer)
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final String
	 * errorMsg = (String)
	 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
	 * "Error Occured in usp_MasterShoppingListCoupon method ..errorNum.{} errorMsg {}"
	 * , errorNum, errorMsg); throw new ScanSeeException(errorMsg); } else {
	 * fromProc = (Integer) resultFromProcedure.get("Status"); if (fromProc ==
	 * 0) { responseFromProc = ApplicationConstants.SUCCESS; usedFlag =
	 * (Integer) resultFromProcedure.get("UsedFlag"); if (usedFlag != null) { if
	 * (usedFlag == 1) { responseFromProc =
	 * ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT; } } } else {
	 * responseFromProc = ApplicationConstants.FAILURE; } } } } catch
	 * (DataAccessException exception) {
	 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
	 * exception); throw new ScanSeeException(exception); } return
	 * responseFromProc; }
	 *//**
	 * This DAOImpl method is used to redeem loyalty information based on
	 * userId.
	 * 
	 * @param loyaltyDetailObj
	 *            contains user id,loyalty deal id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	/*
	 * @Override public String userRedeemLoyalty(LoyaltyDetail loyaltyDetailObj)
	 * throws ScanSeeException { final String methodName =
	 * "userRedeemLoyalty in DAO layer";
	 * log.info(ApplicationConstants.METHODSTART + methodName); Integer fromProc
	 * = null; String responseFromProc = null; Integer usedFlag = null; try {
	 * simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
	 * simpleJdbcCall.withProcedureName("usp_GalleryRedeemLoyalty"); final
	 * MapSqlParameterSource fetchCouponParameters = new
	 * MapSqlParameterSource();
	 * fetchCouponParameters.addValue(ApplicationConstants.USERID,
	 * loyaltyDetailObj.getUserId());
	 * fetchCouponParameters.addValue("UserClaimTypeID",
	 * loyaltyDetailObj.getUserClaimTypeID());
	 * fetchCouponParameters.addValue("LoyaltyDealID",
	 * loyaltyDetailObj.getLoyaltyDealId());
	 * fetchCouponParameters.addValue("APIPartnerID",
	 * loyaltyDetailObj.getaPIPartnerID());
	 * fetchCouponParameters.addValue("LoyaltyDealRedemptionDate",
	 * loyaltyDetailObj.getLoyaltyDealRedemptionDate()); final Map<String,
	 * Object> resultFromProcedure =
	 * simpleJdbcCall.execute(fetchCouponParameters); if (null !=
	 * resultFromProcedure) { if (null !=
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
	 * Integer errorNum = (Integer)
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final String
	 * errorMsg = (String)
	 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
	 * "Error Occured in usp_GalleryRedeemLoyalty method ..errorNum.{} errorMsg {}"
	 * , errorNum, errorMsg); throw new ScanSeeException(errorMsg); } else {
	 * fromProc = (Integer) resultFromProcedure.get("Status"); if (fromProc ==
	 * 0) { responseFromProc = ApplicationConstants.SUCCESS; usedFlag =
	 * (Integer) resultFromProcedure.get("UsedFlag"); if (usedFlag != null) { if
	 * (usedFlag == 1) { responseFromProc =
	 * ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT; } } } else {
	 * responseFromProc = ApplicationConstants.FAILURE; } } } } catch
	 * (DataAccessException exception) {
	 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
	 * exception); throw new ScanSeeException(exception); } return
	 * responseFromProc; }
	 *//**
	 * This DAOImpl method is used to redeem rebate information based on
	 * userId.
	 * 
	 * @param rebateDetailObj
	 *            contains user id,rebate id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	/*
	 * @Override public String userRedeemRebate(RebateDetail rebateDetailObj)
	 * throws ScanSeeException { final String methodName =
	 * "userRedeemRebate in DAO layer";
	 * log.info(ApplicationConstants.METHODSTART + methodName); Integer fromProc
	 * = null; String responseFromProc = null; Integer usedFlag = null; try {
	 * simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
	 * simpleJdbcCall.withProcedureName("usp_GalleryRedeemRebate"); final
	 * MapSqlParameterSource fetchCouponParameters = new
	 * MapSqlParameterSource();
	 * fetchCouponParameters.addValue(ApplicationConstants.USERID,
	 * rebateDetailObj.getUserId()); fetchCouponParameters.addValue("RebateID",
	 * rebateDetailObj.getRebateId());
	 * fetchCouponParameters.addValue("RetailLocationID",
	 * rebateDetailObj.getRetailLocationID());
	 * fetchCouponParameters.addValue("RedemptionStatusID",
	 * rebateDetailObj.getRedemptionStatusID());
	 * fetchCouponParameters.addValue("PurchaseAmount",
	 * rebateDetailObj.getPurchaseAmount());
	 * fetchCouponParameters.addValue("PurchaseDate",
	 * rebateDetailObj.getPurchaseDate());
	 * fetchCouponParameters.addValue("ProductSerialNumber",
	 * rebateDetailObj.getProductSerialNumber());
	 * fetchCouponParameters.addValue("ScanImage",
	 * rebateDetailObj.getScanImage()); final Map<String, Object>
	 * resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters); if
	 * (null != resultFromProcedure) { if (null !=
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
	 * Integer errorNum = (Integer)
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final String
	 * errorMsg = (String)
	 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
	 * "Error Occured in usp_GalleryRedeemRebate method ..errorNum.{} errorMsg {}"
	 * , errorNum, errorMsg); throw new ScanSeeException(errorMsg); } else {
	 * fromProc = (Integer) resultFromProcedure.get("Status"); if (fromProc ==
	 * 0) { responseFromProc = ApplicationConstants.SUCCESS; usedFlag =
	 * (Integer) resultFromProcedure.get("UsedFlag"); if (usedFlag != null) { if
	 * (usedFlag == 1) { responseFromProc =
	 * ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT; } } } else {
	 * responseFromProc = ApplicationConstants.FAILURE; } } } } catch
	 * (DataAccessException exception) {
	 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
	 * exception); throw new ScanSeeException(exception); } return
	 * responseFromProc; }
	 *//**
	 * This DAOImpl method is used to delete used rebate information based on
	 * used coupon id.
	 * 
	 * @param couponDetailsObj
	 *            contains used coupon id.
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	/*
	 * @Override public String deleteUsedCoupon(CouponDetails couponDetailsObj)
	 * throws ScanSeeException { final String methodName =
	 * "userRedeemRebate in DAO layer";
	 * log.info(ApplicationConstants.METHODSTART + methodName); Integer fromProc
	 * = null; String responseFromProc = null; try { simpleJdbcCall = new
	 * SimpleJdbcCall(jdbcTemplate);
	 * simpleJdbcCall.withProcedureName("usp_GalleryDeleteUsedCoupons"); final
	 * MapSqlParameterSource fetchCouponParameters = new
	 * MapSqlParameterSource();
	 * fetchCouponParameters.addValue("UserCouponGalleryID",
	 * couponDetailsObj.getUserCouponGalleryID()); final Map<String, Object>
	 * resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters); if
	 * (null != resultFromProcedure) { if (null !=
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
	 * Integer errorNum = (Integer)
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final String
	 * errorMsg = (String)
	 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
	 * "Error Occured in usp_GalleryDeleteUsedCoupons method ..errorNum.{} errorMsg {}"
	 * , errorNum, errorMsg); throw new ScanSeeException(errorMsg); } else {
	 * fromProc = (Integer) resultFromProcedure.get("Status"); if (fromProc ==
	 * 0) { responseFromProc = ApplicationConstants.SUCCESS; } else {
	 * responseFromProc = ApplicationConstants.FAILURE; } } } } catch
	 * (DataAccessException exception) {
	 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
	 * exception); throw new ScanSeeException(exception); } return
	 * responseFromProc; }
	 */
	/**
	 * This DAOImpl method is used to delete used loyalty information based on
	 * used loyalty id.
	 * 
	 * @param loyaltyDetailObj
	 *            contains used loyalty id.
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	/*
	 * @Override public String deleteUsedLoyalty(LoyaltyDetail loyaltyDetailObj)
	 * throws ScanSeeException { final String methodName =
	 * "deleteUsedLoyalty in DAO layer";
	 * log.info(ApplicationConstants.METHODSTART + methodName); Integer fromProc
	 * = null; String responseFromProc = null; try { simpleJdbcCall = new
	 * SimpleJdbcCall(jdbcTemplate);
	 * simpleJdbcCall.withProcedureName("usp_GalleryDeleteUsedLoyaltyDeal");
	 * final MapSqlParameterSource fetchCouponParameters = new
	 * MapSqlParameterSource();
	 * fetchCouponParameters.addValue("UserLoyaltyGalleryID",
	 * loyaltyDetailObj.getUserLoyaltyGalleryID()); final Map<String, Object>
	 * resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters); if
	 * (null != resultFromProcedure) { if (null !=
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
	 * Integer errorNum = (Integer)
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final String
	 * errorMsg = (String)
	 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
	 * "Error Occured in usp_GalleryDeleteUsedLoyaltyDeal method ..errorNum.{} errorMsg {}"
	 * , errorNum, errorMsg); throw new ScanSeeException(errorMsg); } else {
	 * fromProc = (Integer) resultFromProcedure.get("Status"); if (fromProc ==
	 * 0) { responseFromProc = ApplicationConstants.SUCCESS; } else {
	 * responseFromProc = ApplicationConstants.FAILURE; } } } } catch
	 * (DataAccessException exception) {
	 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
	 * exception); throw new ScanSeeException(exception); } return
	 * responseFromProc; }
	 */
	/**
	 * This DAOImpl method is used to delete used rebate information based on
	 * used rebate id.
	 * 
	 * @param rebateDetailObj
	 *            contains used rebate id.
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	/*
	 * @Override public String deleteUsedRebate(RebateDetail rebateDetailObj)
	 * throws ScanSeeException { final String methodName =
	 * "deleteUsedRebate in DAO layer";
	 * log.info(ApplicationConstants.METHODSTART + methodName); Integer fromProc
	 * = null; String responseFromProc = null; try { simpleJdbcCall = new
	 * SimpleJdbcCall(jdbcTemplate);
	 * simpleJdbcCall.withProcedureName("usp_GalleryDeleteUsedRebate"); final
	 * MapSqlParameterSource fetchCouponParameters = new
	 * MapSqlParameterSource();
	 * fetchCouponParameters.addValue("UserRebateGalleryID",
	 * rebateDetailObj.getUserRebateGalleryID()); final Map<String, Object>
	 * resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters); if
	 * (null != resultFromProcedure) { if (null !=
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) { final
	 * Integer errorNum = (Integer)
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final String
	 * errorMsg = (String)
	 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); log.info(
	 * "Error Occured in usp_GalleryDeleteUsedRebate method ..errorNum.{} errorMsg {}"
	 * , errorNum, errorMsg); throw new ScanSeeException(errorMsg); } else {
	 * fromProc = (Integer) resultFromProcedure.get("Status"); if (fromProc ==
	 * 0) { responseFromProc = ApplicationConstants.SUCCESS; } else {
	 * responseFromProc = ApplicationConstants.FAILURE; } } } } catch
	 * (DataAccessException exception) {
	 * log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
	 * exception); throw new ScanSeeException(exception); } return
	 * responseFromProc; }
	 */

	/**
	 * this method is used to fetch the list of categories.
	 * 
	 * @return ArrayList<CategoryInfo> returns list of categories along with the
	 *         Category Information.
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<CategoryInfo> getCategories() throws ScanSeeWebSqlException
	{
		final String methodName = "getCategories method in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<CategoryInfo> categoryInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerRetrieveCategory");
			simpleJdbcCall.returningResultSet("retrivecategory", new BeanPropertyRowMapper<CategoryInfo>(CategoryInfo.class));
			final MapSqlParameterSource fetchCategoryInfo = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCategoryInfo);
			categoryInfoList = (ArrayList<CategoryInfo>) resultFromProcedure.get("retrivecategory");
			if (null != resultFromProcedure.get(ApplicationConstants.STATUS))
			{
				log.info("Error Occured in usp_RetrieveCategory method");
				throw new ScanSeeWebSqlException();
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return categoryInfoList;
	}

}

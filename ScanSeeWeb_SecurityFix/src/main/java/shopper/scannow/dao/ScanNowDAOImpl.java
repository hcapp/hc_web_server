package shopper.scannow.dao;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;

public class ScanNowDAOImpl implements ScanNowDAO
{
	/**
	 * Getting the Logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ScanNowDAOImpl.class.getName());

	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */

	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;

	/**
	 * To call stored procedue.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * This method for to get data source from xml.
	 * 
	 * @param dataSource
	 *            datasource name
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This DAOImpl method is used to fetch product information based on search
	 * key.
	 * 
	 * @param searchDetail
	 *            contains search key,user id.
	 * @return productDetails product information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */

	public ProductDetails smartSearchProducts(SearchForm searchDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "scanForProductName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;

		try
		{
			if (null == searchDetail.getLastVisitedNo())
			{
				searchDetail.setLastVisitedNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ProductSmartSearch");
			simpleJdbcCall.returningResultSet("smartsearchproducts", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			// scanQueryParams.addValue("UserId", searchDetail.getUserId());
			scanQueryParams.addValue("ProdSearch", searchDetail.getSearchKey());
			/*
			 * scanQueryParams.addValue("LowerLimit",
			 * searchDetail.getLastVistedProductNo());
			 * scanQueryParams.addValue("ScreenName",
			 * "General - Search Add  Product Name");
			 */

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("smartsearchproducts");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						LOG.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);

						/*
						 * final Boolean nextpage = (Boolean)
						 * resultFromProcedure.get("NxtPageFlag"); // This code
						 * for Pagination. Set the NextPage flag to // first row
						 * of list if(nextpage !=null) { if (nextpage) {
						 * productDetails.setNextPage(1); } else {
						 * productDetails.setNextPage(0); } }
						 */
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;

	}
}

package shopper.scannow.dao;

import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.shopper.ProductDetails;

public interface ScanNowDAO
{

	/**
	 * The DAO method fetches the product information based on search key.
	 * 
	 * @param seacrhDetail
	 *            Instance of ProductDetail.
	 * @return ProductDetails.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails smartSearchProducts(SearchForm seacrhDetail) throws ScanSeeWebSqlException;
}

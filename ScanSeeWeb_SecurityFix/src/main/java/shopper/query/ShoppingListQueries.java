package shopper.query;

/**
 * This class contains queries used for the Shopping List module.
 * @author malathi_lr
 *
 */
public class ShoppingListQueries {

	/**
	 * This query used to fetch the product information.
	 */
	public static final String PRODUCTINOQUERY = "SELECT p.ProductID, p.ProductName"
			+ " ,p.ManufacturerID, p.ModelNumber, p.ProductShortDescription,p.ProductLongDescription,"
			+ " p.ProductExpirationDate productExpDate, p.ProductImagePath imagePath, p.SuggestedRetailPrice,p.SKUNumber skuCode "
			+ ", p.Weight productWeight,p.ScanCode barCode, p.WeightUnits,M.ManufName manufacturersName FROM Product p LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID WHERE ProductID =?";

	/**
	 * This query used to insert external api call url.
	 */
	public static final String INSERTEXTERNALAPICALURL = "insert into FindNearByLog values(?,?,?)";
	
	/**
	 * 
	 */
	public static final String RETAILERDETAIL = "Select * from Retailer WHERE RetailID=?";
	
	
	
	
	public static final String ONLINESTOREPRODUCTINFOQUERY = "Select Productid" + ",ManufacturerID" + ",ScanCode" + ",ScanTypeID" + ",Service" + ",ProductName"
	+ ",ProductShortDescription" + ",ProductLongDescription" + ",SKUNumber" + ",ModelNumber" + ",ProductImagePath"
	+ ",SuggestedRetailPrice" + ",Weight" + ",WeightUnits" + ",ProductExpirationDate" + ",ProductAddDate" + ",ProductModifyDate"
	+ " FROM Product " + "WHERE Productid=? AND (ProductExpirationDate IS  NULL OR ProductExpirationDate  > GETDATE())";
	
	
	/**
	 * This query used to fetch the Shopping User notes information.
	 */
	public static final String SHOPPINGUSERNOTESQUERY = "SELECT UserID userId,Note userNotes,UserShoppingCartNoteID userNoteId FROM [UserShoppingCartNote] WHERE UserID=?";
	
	
	/**
	 * This query used to fetch the User email id.
	 */
	public static final String FETCHUSEREMAILID = "select Email from Users where UserID = ?";
	
	/**
	 * This query used to fetch the User email id.
	 */
	public static final String FETCHUNIVERSITYNAME = "select * from University where UniversityID = ?";

}

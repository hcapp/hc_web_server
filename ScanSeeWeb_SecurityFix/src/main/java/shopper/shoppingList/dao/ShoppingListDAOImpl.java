package shopper.shoppingList.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.query.ShoppingListQueries;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.AddSLRequest;
import common.pojo.shopper.Category;
import common.pojo.shopper.MainSLRetailerCategories;
import common.pojo.shopper.MainSLRetailerCategory;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.ShoppingBasketProducts;
import common.pojo.shopper.ShoppingCartProducts;
import common.pojo.shopper.ShoppingListRequest;
import common.pojo.shopper.UserNotesDetails;
import common.util.Utility;

/**
 * This class for fetching shopping list details.
 * 
 * @author emmanuel_b
 */

public class ShoppingListDAOImpl implements ShoppingListDAO
{

	/**
	 * Logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(ShoppingListDAOImpl.class.getName());
	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;
	/**
	 * To set ParameterizedBeanPropertyRowMapper to map POJO.
	 */

	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * This method for to get data source Spring DI.
	 * 
	 * @param dataSource
	 *            for DB operations.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * For displaying shopping basket product based on userId.
	 * 
	 * @param userId
	 *            requested user id.
	 * @return AddRemoveSBProducts list of cart products.
	 * @throws ScanSeeException
	 *             for exception
	 */

	@SuppressWarnings("unchecked")
	public AddRemoveSBProducts getShopBasketProducts(Long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "getShopBasketProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		AddRemoveSBProducts addRemoveSBProducts = new AddRemoveSBProducts();
		List<ProductDetail> productDetailList = null;
		ShoppingCartProducts cartProducts = null;
		ShoppingBasketProducts basketProducts = null;
		MapSqlParameterSource scanQueryParams = null;
		Map<String, Object> resultFromProcedure = null;
		try
		{
			log.info("getShopBasketProducts for user Id {}", userId);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_TodayShoppingListCategoryProduct");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", userId);
			resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");

					final HashMap<String, ArrayList<ProductDetail>> productsCategorisedByRetailer = new HashMap<String, ArrayList<ProductDetail>>();

					// these products should be group by Category and set the
					// products to category and set category list to
					// AddRemoveSBProducts
					// Key is parent category Name and value is respective
					// products
					// details

					if (null != productDetailList && !productDetailList.isEmpty())
					{
						for (ProductDetail productDetail : productDetailList)
						{

							if (productsCategorisedByRetailer.containsKey(productDetail.getParentCategoryName()))

							{
								final ArrayList<ProductDetail> prodDetails = productsCategorisedByRetailer.get(productDetail.getParentCategoryName());
								prodDetails.add(productDetail);
							}
							else
							{
								final ArrayList<ProductDetail> productDetailsList = new ArrayList<ProductDetail>();
								productDetailsList.add(productDetail);

								productsCategorisedByRetailer.put(productDetail.getParentCategoryName(), productDetailsList);

							}
						}

						cartProducts = new ShoppingCartProducts();

						final ArrayList<Category> categorylist = new ArrayList<Category>();
						Category categoryDetails = null;

						ProductDetail obj = null;
						for (Map.Entry e : productsCategorisedByRetailer.entrySet())
						{
							categoryDetails = new Category();
							categoryDetails.setParentCategoryName(e.getKey().toString());

							categoryDetails.setProductDetails((ArrayList<ProductDetail>) e.getValue());
							obj = ((ArrayList<ProductDetail>) e.getValue()).get(0);
							categoryDetails.setCategoryId(obj.getCategoryID());
							categorylist.add(categoryDetails);
						}

						cartProducts.setCategoryDetails(categorylist);

					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_ShoppingCartTodayListDisplay Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ShoppingCartCategoryProduct");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", userId);
			resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
					final HashMap<String, ArrayList<ProductDetail>> productsCategorisedByRetailer = new HashMap<String, ArrayList<ProductDetail>>();
					if (null != productDetailList && !productDetailList.isEmpty())
					{

						for (ProductDetail productDetail : productDetailList)
						{

							if (productsCategorisedByRetailer.containsKey(productDetail.getParentCategoryName()))

							{
								final ArrayList<ProductDetail> prodDetails = productsCategorisedByRetailer.get(productDetail.getParentCategoryName());
								prodDetails.add(productDetail);
							}
							else
							{
								final ArrayList<ProductDetail> productDetailsList = new ArrayList<ProductDetail>();
								productDetailsList.add(productDetail);

								productsCategorisedByRetailer.put(productDetail.getParentCategoryName(), productDetailsList);

							}
						}
						final ArrayList<Category> categorylist = new ArrayList<Category>();
						Category categoryDetails = null;
						ProductDetail obj = null;
						for (Map.Entry e : productsCategorisedByRetailer.entrySet())
						{
							categoryDetails = new Category();
							categoryDetails.setParentCategoryName(e.getKey().toString());
							obj = ((ArrayList<ProductDetail>) e.getValue()).get(0);

							categoryDetails.setCategoryId(obj.getCategoryID());
							categoryDetails.setProductDetails((ArrayList<ProductDetail>) e.getValue());
							categorylist.add(categoryDetails);

						}
						basketProducts = new ShoppingBasketProducts();
						basketProducts.setCategoryDetails(categorylist);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					log.error("Error occurred in usp_ShoppingCartDispaly Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

			if (null != basketProducts)
			{
				addRemoveSBProducts = new AddRemoveSBProducts();
				addRemoveSBProducts.setBasketProducts(basketProducts);
			}

			if (null != cartProducts)
			{
				if (null == addRemoveSBProducts)
				{
					addRemoveSBProducts = new AddRemoveSBProducts();
				}
				addRemoveSBProducts.setCartProducts(cartProducts);
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return addRemoveSBProducts;
	}

	/**
	 * This DAOImpl method is used to fetch product information based on search
	 * key.
	 * 
	 * @param searchDetail
	 *            contains search key,user id.
	 * @return productDetails product information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@SuppressWarnings({ "unchecked" })
	public ProductDetails scanForProductName(ShoppingListRequest shoppingListRequest, Integer lowerLimit) throws ScanSeeWebSqlException
	{

		final String methodName = "scanForProductName";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;
		Integer rowcount = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_SearchProductPagination");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", shoppingListRequest.getUserId());
			scanQueryParams.addValue("ProdSearch", shoppingListRequest.getSearchValue());
			scanQueryParams.addValue("LowerLimit", lowerLimit);
			scanQueryParams.addValue("ScreenName", ApplicationConstants.SCANNOWSEARCHPRODPAGINATION);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						log.info("Products found for the search");
						productDetails = new ProductDetails();

						if (productDetailList != null && !productDetailList.isEmpty())
						{
							rowcount = (Integer) resultFromProcedure.get("MaxCnt");
							productDetails.setProductDetail(productDetailList);
							productDetails.setTotalSize(rowcount);
						}
						else
						{
							final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
							final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
							log.info("Inside RetailerDAOImpl : getRetailerByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
							rowcount = (Integer) resultFromProcedure.get("MaxCnt");
							productDetails.setTotalSize(rowcount);
						}
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;

	}

	/**
	 * The DAO method for Adding the product to Main Shopping List.
	 * 
	 * @param
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	@SuppressWarnings("unchecked")
	public ProductDetails addProductMainToShopList(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException
	{

		Long userId = null;
		final String methodName = "addProductMainToShopList in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		MapSqlParameterSource scanQueryParams = null;
		ArrayList<ProductDetail> shoppingListResultSet = null;
		ProductDetails productList = null;
		Integer productExits = null;

		try
		{
			userId = shoppingListRequest.getUserId();

			if (shoppingListRequest.isIsWishlst())
			{
				final String userProductIds = "";// Utility.getCommaSepartedUserProdIdValues(productDetails);
				log.info("adding products from wish list {} to Master Shopping List for UserId {}", userProductIds, userId);
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
				simpleJdbcCall.withProcedureName("usp_WishListAddToMSL");
				simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

				scanQueryParams = new MapSqlParameterSource();
				scanQueryParams.addValue("UserID", userId);
				scanQueryParams.addValue("UserProductID", userProductIds);
				scanQueryParams.addValue("MasterListAddDate", Utility.getFormattedDate());
				productList = new ProductDetails();
			}
			else
			{

				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
				simpleJdbcCall.withProcedureName("usp_MasterShoppingListSearchAddProduct");
				simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
				scanQueryParams = new MapSqlParameterSource();
				scanQueryParams.addValue("UserID", userId);
				scanQueryParams.addValue("ProductID", shoppingListRequest.getSelectedProductId());

				scanQueryParams.addValue("MasterListAddDate", Utility.getFormattedDate());
				productList = new ProductDetails();
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{

				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					shoppingListResultSet = (ArrayList<ProductDetail>) resultFromProcedure.get("userProdId");

					if (null != shoppingListResultSet && !shoppingListResultSet.isEmpty())
					{
						productList = new ProductDetails();
						productList.setProductDetail(shoppingListResultSet);
						productExits = (Integer) resultFromProcedure.get("ProductExists");
						if (productExits != null)
						{
							if (productExits == 1)
							{
								productList.setProductIsThere(1);
							}
							else
							{
								productList.setProductIsThere(0);
							}
						}

					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error(
							"Error occurred in usp_WishListAddToMSL or usp_MasterShoppingListSearchAddProduct Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);

				}

			}

		}
		catch (ParseException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		catch (DataAccessException exception)
		{
			log.error("error occuredd in addProductToShopList", exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productList;
	}

	/**
	 * This method is for adding today's SL product for the given product id and
	 * user id.
	 * 
	 * @param addRemObj
	 *            -AS AddSLRequest object
	 * @return ProductDetail contains sale price ,categoryid etc.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	@SuppressWarnings({ "null", "unchecked" })
	public ProductDetail addTodaySLProductsBySearch(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException
	{
		final String methodName = "addRemoveTodaySLProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList = null;
		ProductDetail prodObj = null;
		Integer productExits = null;
		try
		{

			if (null != shoppingListRequest.getSelectedProductId() && 0 != shoppingListRequest.getSelectedProductId())
			{
				log.info("Executing addRemoveTodaySLProducts for Products ID {} for User ID", shoppingListRequest.getSelectedProductId(),
						shoppingListRequest.getUserId());
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
				simpleJdbcCall.withProcedureName("usp_TodayShoppingListSearchAddProduct");
				simpleJdbcCall.returningResultSet("userprodId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

				final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
				scanQueryParams.addValue("UserID", shoppingListRequest.getUserId());
				scanQueryParams.addValue("ProductId", shoppingListRequest.getSelectedProductId());
				scanQueryParams.addValue("TodayListAddDate", Utility.getFormattedDate());

				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

				if (null != resultFromProcedure)
				{
					if (null == resultFromProcedure.get("ErrorNumber"))
					{

						productDetailList = (List<ProductDetail>) resultFromProcedure.get("userprodId");
						if (null != productDetailList && !productDetailList.isEmpty())
						{
							// Store procedure will return generated
							// UserProductID for product added to TSL
							// This Id will be returned to Client for further
							// Operation

							productExits = (Integer) resultFromProcedure.get("ProductExists");
							if (productExits != null)
							{
								if (productExits == 1)
								{
									productDetailList.get(0).setProductIsThere(1);
								}
								else
								{
									productDetailList.get(0).setProductIsThere(0);
								}
							}

							prodObj = productDetailList.get(0);

						}
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_MasterShoppingListProductList Store Procedure error number: {} " + errorNum
							+ " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return prodObj;
	}

	/**
	 * The method for adding to or removing from SL.
	 * 
	 * @param addSLRequest
	 *            The XML with request for Add to SL(or remove from SL).
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	public String addRemoveTodaySLProducts(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException
	{

		final String methodName = "addRemoveTodaySLProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer productExits = null;
		Integer exists = null;
		String response = null;

		try
		{
			if (shoppingListRequest.isIsaddToTSL())
			{
				final String idString = Utility.getCommaSepartedUserProdIdValues(shoppingListRequest.getProductDetails());
				log.info("Executing addRemoveTodaySLProducts for Products ID {} for UserID", idString, shoppingListRequest.getUserId());
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
				simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddTSL");

				final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
				scanQueryParams.addValue("UserID", shoppingListRequest.getUserId());
				scanQueryParams.addValue("UserProductID", idString);
				scanQueryParams.addValue("TodayListAddDate", Utility.getFormattedDate());
				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
				productExits = (Integer) resultFromProcedure.get("ProductExists");
				exists = (Integer) resultFromProcedure.get("Exists");
				if (productExits == null && exists == null)
				{
					response = ApplicationConstants.SUCCESS;
				}
				if (productExits != null)
				{
					if (productExits == 1)
					{
						response = ApplicationConstants.SLADDPRODTLEXISTS;
					}
					else
					{
						response = ApplicationConstants.SUCCESS;
					}
				}
				if (exists != null)
				{
					if (exists == 1)
					{
						response = ApplicationConstants.FAILURE;
					}
					else
					{
						response = ApplicationConstants.SUCCESS;
					}
				}

			}
			else
			{
				log.info("Executing addRemoveTodaySLProducts for Products ID {} for User ID", shoppingListRequest.getSelectedProductId(),
						shoppingListRequest.getUserId());
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
				simpleJdbcCall.withProcedureName("usp_TodayListAddDelete");
				final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
				scanQueryParams.addValue("UserID", shoppingListRequest.getUserId());
				scanQueryParams.addValue("UserProductId", shoppingListRequest.getUserProductID());
				@SuppressWarnings("unused")
				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
				response = ApplicationConstants.SUCCESS;
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			return ApplicationConstants.FAILURE;
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method for removing rebate from gallery.
	 * 
	 * @param addRemObj
	 *            xml with Add product to Shopping Basked(or removing)
	 * @return response xml with success or failure error code.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	public String addRemoveSBProducts(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException
	{

		final String methodName = "addRemoveSBProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = 0;
		String response = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ShoppingCartAddDelete");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", shoppingListRequest.getUserId());
			if (null == shoppingListRequest.getUserProductID())
			{
				scanQueryParams.addValue("TodayListUserProductID", " ");
			}
			else
			{
				scanQueryParams.addValue("TodayListUserProductID", shoppingListRequest.getUserProductID());
			}
			if (null == shoppingListRequest.getCartUserProductID())
			{
				scanQueryParams.addValue("CartUserProductID", " ");
			}
			else
			{
				scanQueryParams.addValue("CartUserProductID", shoppingListRequest.getCartUserProductID());
			}
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_ShoppingCartAddDelete Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for adding Shopping list User Notes.
	 * 
	 * @param userID
	 *            as the request parameter
	 * @return Xml as the response.
	 * @throws ScanSeeException
	 *             of type SQL Exception.
	 */
	public UserNotesDetails getUserNotesDetails(Long userID) throws ScanSeeWebSqlException
	{

		final String methodName = "getUserNotesDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		UserNotesDetails userNotesDetails = null;
		try
		{
			log.info("Executing  UserNotesDetails UserID {} ", userID);
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			userNotesDetails = simpleJdbcTemplate.queryForObject(ShoppingListQueries.SHOPPINGUSERNOTESQUERY,
					new BeanPropertyRowMapper<UserNotesDetails>(UserNotesDetails.class), userID);
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return userNotesDetails;
	}

	/**
	 * The method for adding Shopping list User Notes.
	 * 
	 * @param userNotesDetails
	 *            as the request parameter
	 * @return Xml as the response.
	 * @throws ScanSeeException
	 *             of type SQL Exception.
	 */
	public String addUserNotesDetails(Long userId, String userNotes) throws ScanSeeWebSqlException
	{

		final String methodName = "addUserNotesDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer fromProc = null;

		try
		{
			log.info("Inside addUserNotesDetails method...");
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_TodayListNote");
			simpleJdbcCall.returningResultSet("addNotesDetails", new BeanPropertyRowMapper<UserNotesDetails>(UserNotesDetails.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", userId);
			scanQueryParams.addValue("Note", userNotes);
			scanQueryParams.addValue("NoteAddDate", Utility.getFormattedDate());
			resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_TodayListNote  ..errorNum...{} errorMsg", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (ParseException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method clears out the Shopping Basket.
	 * 
	 * @param userID
	 *            The userID entered by User.
	 * @param cartLatitude
	 *            The Latitude value(required for entry in
	 *            UserShoppingCartHistoryProduct table).
	 * @param cartLongitude
	 *            The Longitude value (required for entry in
	 *            UserShoppingCartHistoryProduct table).
	 * @return String with success or error code based on success or failure of
	 *         operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 */
	public String checkOutFromBasket(Long userID, float cartLatitude, float cartLongitude) throws ScanSeeWebSqlException
	{
		final String methodName = "checkOutFromBasket in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String responseFromProc = null;
		Integer fromProc = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ShoppingCartClearAll");

			final MapSqlParameterSource checkoutParams = new MapSqlParameterSource();
			checkoutParams.addValue("UserID", userID).addValue("CartLatitude", cartLatitude).addValue("CartLongitude", cartLongitude);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(checkoutParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_ShoppingCartClearAll  ..errorNum.{} errorMsg{}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * This method get the Master Shopping list products from the database.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @return list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<ProductDetail> getMSLCategoryProcuctDetails(Long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "getMSLCategoryProcuctDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ProductDetail> productDetailsList = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListCategoryProduct");
			simpleJdbcCall.returningResultSet("MSLProductsList", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					productDetailsList = (ArrayList<ProductDetail>) resultFromProcedure.get("MSLProductsList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListProductList Store Procedure with error number: {} " + errorNum
						+ " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailsList;
	}

	/**
	 * This is a DAO Implementation Method for fetching shopping list history
	 * products for the given userId.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the Shopping
	 *            list history products .
	 * @return XML containing shopping list history products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public MainSLRetailerCategories fetchSLHistoryProducts(Long userId, Integer lowerlimit) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchSLHistoryProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer rowcount = null;
		ArrayList<MainSLRetailerCategory> masterSLCategorylst = null;
		MainSLRetailerCategories mainSLRetailerCategories = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ShoppingListHistoryDisplay");
			simpleJdbcCall.returningResultSet("MSLProductsList", new BeanPropertyRowMapper<MainSLRetailerCategory>(MainSLRetailerCategory.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("UserID", userId);
			productQueryParams.addValue("LowerLimit", lowerlimit);
			productQueryParams.addValue("ScreenName", ApplicationConstants.SLHISTORYSCREENNAME);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					masterSLCategorylst = (ArrayList<MainSLRetailerCategory>) resultFromProcedure.get("MSLProductsList");

					if (!masterSLCategorylst.isEmpty() && masterSLCategorylst != null)
					{
						mainSLRetailerCategories = new MainSLRetailerCategories();

						if (masterSLCategorylst != null && !masterSLCategorylst.isEmpty())
						{
							rowcount = (Integer) resultFromProcedure.get("MaxCnt");
							mainSLRetailerCategories.setMainSLRetailerCategory(masterSLCategorylst);
							mainSLRetailerCategories.setTotalSize(rowcount);
						}
						else
						{
							final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
							final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
							log.info("Inside RetailerDAOImpl : getRetailerByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
							rowcount = (Integer) resultFromProcedure.get("MaxCnt");
							mainSLRetailerCategories.setTotalSize(rowcount);
						}
					}
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_ShoppingListHistoryDisplay Store Procedure error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return mainSLRetailerCategories;
	}

	/**
	 * The method for Adding to Shopping List.
	 * 
	 * @param addSLRequest
	 *            The XML with delete from Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	public String deleteProductMainShopListFav(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException
	{

		final String methodName = "dleteProductMainShopList";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = 0;
		String response = null;
		try
		{

			if (null != shoppingListRequest.getUserProductID())
			{
				// final String idString =
				// Utility.getCommaSepartedUserProdIdValues(productDetails);
				log.info("Removing products {} from Main Shopping List for User Id {}", shoppingListRequest.getUserProductID(),
						shoppingListRequest.getUserId());

				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
				simpleJdbcCall.withProcedureName("usp_MasterShoppingListSearchDeleteProduct");

				final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
				scanQueryParams.addValue("UserID", shoppingListRequest.getUserId());
				scanQueryParams.addValue("UserProductID", shoppingListRequest.getUserProductID());
				scanQueryParams.addValue("MasterListRemoveDate", Utility.getFormattedDate());
				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

				fromProc = (Integer) resultFromProcedure.get("Status");
				if (fromProc == 0)
				{
					response = ApplicationConstants.SUCCESS;
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_MasterShoppingListSearchDeleteProduct Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);

					response = ApplicationConstants.FAILURE;
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (ParseException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		catch (DataAccessException exception)
		{
			log.error("error occuredd in dleteProductToShopList", exception);
			throw new ScanSeeWebSqlException(exception);

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a DAO Implementation Method for adding shopping list history
	 * product to List ,favorites and both for the given userId.
	 * 
	 * @param addSLRequestObj
	 *            containing product information needed for adding to
	 *            list,favorites and both.
	 * @return XML containing message saying it added to list or favorites or
	 *         both.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ProductDetails addSLHistoryProductInfo(AddSLRequest addSLRequestObj) throws ScanSeeWebSqlException
	{
		final String methodName = "addSLHistoryProductInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		ProductDetails productDetailsObj = null;

		try
		{
			final List<ProductDetail> productDetails = addSLRequestObj.getProductDetails();
			if (null != productDetails && !productDetails.isEmpty())
			{
				final String userProductIdString = Utility.getCommaSepartedUserProdIdValues(productDetails);
				final String productIdString = Utility.getCommaSepartedValues(productDetails);
				log.info("Executing addSLHistoryProductInfo for Products ID {} for User ID", userProductIdString, addSLRequestObj.getUserId());
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
				simpleJdbcCall.withProcedureName("usp_ShoppingHistoryMoveItems");

				final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
				// scanQueryParams.addValue("UserID",
				// addSLRequestObj.getUserId());
				scanQueryParams.addValue("UserProductID", userProductIdString);
				scanQueryParams.addValue("ProductID", productIdString);
				scanQueryParams.addValue("AddTo", addSLRequestObj.getAddTo());

				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
				fromProc = (Integer) resultFromProcedure.get("Status");
				if (fromProc == 0)
				{
					productDetailsObj = new ProductDetails();

					/**
					 * Created list and favorites flag to fetch output
					 * parameters ListFlag and FavoritesFlag. These flag
					 * indicates whether product added to list or favorites.
					 */
					Integer favoritesFlag = null;
					Integer listFlag = null;
					listFlag = (Integer) resultFromProcedure.get("ListFlag");
					if (listFlag != null)
					{
						if (listFlag == 1)
						{
							productDetailsObj.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINLIST);
						}
						else if (listFlag == 0)
						{
							productDetailsObj.setResponseFlag(ApplicationConstants.ADDINGUNASSINGEDPRODUCT);
						}
					}
					favoritesFlag = (Integer) resultFromProcedure.get("FavoritesFlag");

					if (favoritesFlag != null)
					{
						if (favoritesFlag == 1)
						{
							productDetailsObj.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINFAVORITES);
						}
						else if (favoritesFlag == 0)
						{
							productDetailsObj.setResponseFlag(ApplicationConstants.ADDPRODUCTMAINTOSHOPPINGLIST);
						}
						if (listFlag != null && favoritesFlag != null)
						{
							if (listFlag == 1 && favoritesFlag == 1)
							{
								productDetailsObj.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINLISTANDFAVORITES);
							}
							else if (listFlag == 0 && favoritesFlag == 0)
							{
								productDetailsObj.setResponseFlag(ApplicationConstants.SLHISTORYPRODADDEDTOLISTFAVORITESTEXT);
							}
							else if (listFlag == 0 && favoritesFlag == 1)
							{
								productDetailsObj.setResponseFlag(ApplicationConstants.SLHSTADDEDTOLISTNOTFAVORITES);
							}
							else
							{
								productDetailsObj.setResponseFlag(ApplicationConstants.SLHSTADDEDTOFAVORITESNOTLIST);
							}
						}
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_ShoppingHistoryMoveItems Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
				}

			}
		}
		catch (DataAccessException exception)

		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailsObj;
	}

	/**
	 * The method used for adding UnassignedProduct.
	 * 
	 * @param productDetailsRequest
	 *            The ProductDetailsRequest object.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	public ProductDetails addUnassignedPro(ProductDetailsRequest productDetailsRequest) throws ScanSeeWebSqlException
	{
		final String methodName = "addUnassignedPro in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ProductDetail> productDetailsResultSet = null;
		ProductDetails productList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListUnassignedProducts").returningResultSet("addUnassigned",
					ParameterizedBeanPropertyRowMapper.newInstance(ProductDetail.class));
			log.info("Adding unassignd product {} and Description {} ", productDetailsRequest.getProductName(),
					productDetailsRequest.getProductDescription());
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", productDetailsRequest.getUserId());
			scanQueryParams.addValue("UnassignedProductName", productDetailsRequest.getProductName());
			scanQueryParams.addValue("UnassignedProductDescription", productDetailsRequest.getProductDescription());
			scanQueryParams.addValue("Date", Utility.getFormattedDate());
			scanQueryParams.addValue("ScreenFlag", productDetailsRequest.getAddedTo());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailsResultSet = (ArrayList<ProductDetail>) resultFromProcedure.get("addUnassigned");
					if (null != productDetailsResultSet && !productDetailsResultSet.isEmpty())
					{
						productList = new ProductDetails();
						productList.setProductDetail(productDetailsResultSet);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					log.error("Error occurred in usp_MasterShoppingListUnassignedProducts Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}

		catch (ParseException e)
		{
			log.info("EmptyResultDataAccessException" + e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return productList;

	}

	/**
	 * The DAO method for Adding the product to Main Shopping List.
	 * 
	 * @param addSLRequest
	 *            The XML with Add to Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	public ProductDetails addProductMainToShopList(Long userId, Integer productId) throws ScanSeeWebSqlException
	{

		final String methodName = "addProductMainToShopList in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		MapSqlParameterSource scanQueryParams = null;
		ArrayList<ProductDetail> shoppingListResultSet = null;
		ProductDetails productList = null;
		Integer productExits = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListSearchAddProduct");
			simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", userId);
			scanQueryParams.addValue("ProductID", productId);
			scanQueryParams.addValue("MasterListAddDate", Utility.getFormattedDate());
			productList = new ProductDetails();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					shoppingListResultSet = (ArrayList<ProductDetail>) resultFromProcedure.get("userProdId");

					if (null != shoppingListResultSet && !shoppingListResultSet.isEmpty())
					{
						productList = new ProductDetails();
						productList.setProductDetail(shoppingListResultSet);
						productExits = (Integer) resultFromProcedure.get("ProductExists");
						if (productExits != null)
						{
							if (productExits == 1)
							{
								productList.setProductIsThere(1);
							}
							else
							{
								productList.setProductIsThere(0);
							}
						}
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error(
							"Error occurred in usp_WishListAddToMSL or usp_MasterShoppingListSearchAddProduct Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}
		catch (ParseException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		catch (DataAccessException exception)
		{
			log.error("error occuredd in addProductToShopList", exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productList;
	}

	public ArrayList<ProductDetails> categorySmartSearch(String prodSearch) throws ScanSeeWebSqlException
	{
		final String methodName = "getMSLCategoryProcuctDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ProductDetails> productDetailsList = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ProductSmartSearch");
			simpleJdbcCall.returningResultSet("MSLProductsList", new BeanPropertyRowMapper<ProductDetails>(ProductDetails.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ProdSearch", prodSearch);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					productDetailsList = (ArrayList<ProductDetails>) resultFromProcedure.get("MSLProductsList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListProductList Store Procedure with error number: {} " + errorNum
						+ " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailsList;
	}

	public ArrayList<ProductDetail> productSmartSearch(String prodSearch, String parentCategoryID) throws ScanSeeWebSqlException
	{
		final String methodName = "getMSLCategoryProcuctDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ProductDetail> productDetailsList = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ProductSmartSearchResults");
			simpleJdbcCall.returningResultSet("MSLProductsList", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ProdSearch", prodSearch);
			productQueryParams.addValue("ParentCategoryID", parentCategoryID);
			productQueryParams.addValue("LowerLimit", 0);
			productQueryParams.addValue("ScreenName", "Shopping List - Product List");

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					productDetailsList = (ArrayList<ProductDetail>) resultFromProcedure.get("MSLProductsList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListProductList Store Procedure with error number: {} " + errorNum
						+ " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailsList;
	}
}
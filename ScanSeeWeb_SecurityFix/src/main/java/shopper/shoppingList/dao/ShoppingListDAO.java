package shopper.shoppingList.dao;

import java.util.ArrayList;

import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.AddSLRequest;
import common.pojo.shopper.MainSLRetailerCategories;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.ShoppingListRequest;
import common.pojo.shopper.UserNotesDetails;

/**
 * The Interface for ShoppingListDAO. ImplementedBy {@link ShoppingListDAOImpl}
 * 
 * @author emmanuel_b
 */
public interface ShoppingListDAO
{
	/**
	 * For displaying shopping basket product based on userId.
	 * 
	 * @param userId
	 *            requested user id.
	 * @return AddRemoveSBProducts list of cart products.
	 * @throws ScanSeeException
	 *             for exception
	 */
	AddRemoveSBProducts getShopBasketProducts(Long userId) throws ScanSeeWebSqlException;

	/**
	 * The DAO method fetches the product information based on search key.
	 * 
	 * @param seacrhDetail
	 *            Instance of ProductDetail.
	 * @return ProductDetails.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails scanForProductName(ShoppingListRequest shoppingListRequest, Integer lowerLimit) throws ScanSeeWebSqlException;

	/**
	 * The DAO method updates database for requested products to shopping list.
	 * 
	 * @param addSLRequest
	 *            contain products need to be added to Shopping list.
	 * @return ProductDetails with User product id's for the added product.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ProductDetails addProductMainToShopList(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException;

	/**
	 * This method is for adding today's SL product for the given product id and
	 * user id.
	 * 
	 * @param addRemObj
	 *            -AS AddSLRequest object
	 * @return ProductDetail contains sale price ,categoryid etc.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ProductDetail addTodaySLProductsBySearch(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException;

	/**
	 * method update database to add/remove the products to/from Today Shopping
	 * List.
	 * 
	 * @param shoppingListRequest
	 *            contains products information.
	 * @return String with success or failure.
	 * @throws ScanSeeWebSqlException
	 *             throws if exception occurs.
	 */
	String addRemoveTodaySLProducts(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException;

	/**
	 * method updates the database to add/Remove product to/from cart.
	 * 
	 * @param shoppingListRequest
	 *            products to be added/removed.
	 * @return String with success or failure.
	 * @throws ScanSeeWebSqlException
	 *             throws if exception occurs.
	 */
	String addRemoveSBProducts(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException;

	/**
	 * The method for adding Shopping list User Notes.
	 * 
	 * @param userID
	 *            as the request parameter
	 * @return Xml as the response.
	 * @throws ScanSeeException
	 *             of type SQL Exception.
	 */
	public UserNotesDetails getUserNotesDetails(Long userID) throws ScanSeeWebSqlException;

	/**
	 * The method for adding Shopping list User Notes.
	 * 
	 * @param userNotesDetails
	 *            as the request parameter
	 * @return String as the response.
	 * @throws ScanSeeException
	 *             of type SQL Exception.
	 */
	public String addUserNotesDetails(Long userId, String userNotes) throws ScanSeeWebSqlException;

	/**
	 * method to removes all the products from Cart.
	 * 
	 * @param userID
	 *            requested user.
	 * @param cartLatitude
	 *            location of user.
	 * @param cartLongitude
	 *            location of user.
	 * @return String with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String checkOutFromBasket(Long userID, float cartLatitude, float cartLongitude) throws ScanSeeWebSqlException;

	/**
	 * This method get the favorite list products from the database.
	 * 
	 * @param userId
	 *            as request
	 * @return list of favorite List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ArrayList<ProductDetail> getMSLCategoryProcuctDetails(Long userId) throws ScanSeeWebSqlException;

	/**
	 * This is a DAO Implementation Method for fetching shopping list history
	 * products for the given userId.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the Shopping
	 *            list history products .
	 * @return XML containing shopping list history products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public MainSLRetailerCategories fetchSLHistoryProducts(Long userId, Integer lowerlimit) throws ScanSeeWebSqlException;

	/**
	 * this methods updates database for requested products which need to be
	 * removed from shopping list.
	 * 
	 * @param addSLRequest
	 *            products need to be deleted from Shopping list.
	 * @return String with Success or failure information.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String deleteProductMainShopListFav(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException;

	/**
	 * This is a DAO Implementation Method for adding shopping list history
	 * product to List ,favorites and both for the given userId.
	 * 
	 * @param addSLRequestObj
	 *            containing product information needed for adding to
	 *            list,favorites and both.
	 * @return XML containing message saying it added to list or favorites or
	 *         both.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ProductDetails addSLHistoryProductInfo(AddSLRequest addSLRequestObj) throws ScanSeeWebSqlException;

	/**
	 * The method used for adding UnassignedProduct.
	 * 
	 * @param productDetailsRequest
	 *            The ProductDetailsRequest object.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	public ProductDetails addUnassignedPro(ProductDetailsRequest productDetailsRequest) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for Adding the product to Main Shopping List.
	 * 
	 * @param addSLRequest
	 *            The XML with Add to Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	public ProductDetails addProductMainToShopList(Long userId, Integer productId) throws ScanSeeWebSqlException;

	/**
	 * 
	 */
	public ArrayList<ProductDetails> categorySmartSearch(String prodSearch) throws ScanSeeWebSqlException;

	/**
	 * 
	 */
	public ArrayList<ProductDetail> productSmartSearch(String prodSearch, String parentCategoryID) throws ScanSeeWebSqlException;
}

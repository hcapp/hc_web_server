/**
 * 
 */
package shopper.shoppingList.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.Category;
import common.pojo.shopper.MainSLRetailerCategory;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.SLCategory;
import common.pojo.shopper.ShoppingListDetails;
import common.pojo.shopper.ShoppingListRetailerCategoryList;

/**
 * This class contains Utility methods for converting Shopping List objects.
 * 
 * @author emmanuel_b
 */
public class ShoppingListHelper
{

	public static final String STARTQUOTATION = "\"";
	public static final String ENDQUOTATION = "\"";

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ShoppingListHelper.class);

	/**
	 * for ShoppingListHelper constructor.
	 */
	public ShoppingListHelper()
	{
		LOG.info("Inside ShoppingListHelper class");
	}

	/**
	 * .
	 * 
	 * @param .
	 * @return
	 */

	public String shoppinglistDivBuilding(AddRemoveSBProducts addRemoveSBProducts, Long userId)
	{
		StringBuffer innerhtml = new StringBuffer();

		int cartCount = 0;
		int listCount = 0;
		if (null != addRemoveSBProducts.getCartProducts())
		{
			for (Category category : addRemoveSBProducts.getCartProducts().getCategoryDetails())
			{
				cartCount = cartCount + category.getProductDetails().size();
			}
		}
		if (null != addRemoveSBProducts.getBasketProducts())
		{
			for (Category category : addRemoveSBProducts.getBasketProducts().getCategoryDetails())
			{
				listCount = listCount + category.getProductDetails().size();
			}
		}

		innerhtml.append("<div class=" + STARTQUOTATION + "splitDsply floatR" + STARTQUOTATION + " id = " + STARTQUOTATION + "shoppingView"
				+ STARTQUOTATION + ">");
		innerhtml.append("<table width=" + STARTQUOTATION + "100%" + STARTQUOTATION + " border=" + STARTQUOTATION + "0" + STARTQUOTATION
				+ " cellspacing=" + STARTQUOTATION + "0" + STARTQUOTATION + " cellpadding=" + STARTQUOTATION + "0" + STARTQUOTATION + " class="
				+ STARTQUOTATION + "brdrLsTbl topSubBar" + STARTQUOTATION + "><tr><td width=" + STARTQUOTATION + "22%" + STARTQUOTATION
				+ "><input type=" + STARTQUOTATION + "hidden" + STARTQUOTATION + " name=" + STARTQUOTATION + "userId" + STARTQUOTATION + " id="
				+ STARTQUOTATION + "userId" + STARTQUOTATION + " value=" + STARTQUOTATION + "" + userId + "" + STARTQUOTATION + " ></input>");
		innerhtml.append("<input type=" + STARTQUOTATION + "text" + STARTQUOTATION + " name = " + STARTQUOTATION + "searchValue" + STARTQUOTATION
				+ " id = " + STARTQUOTATION + "searchValue" + STARTQUOTATION + " class=" + STARTQUOTATION + "textboxSmall focustxt" + STARTQUOTATION
				+ " value=" + STARTQUOTATION + " " + STARTQUOTATION + " onkeypress=" + STARTQUOTATION + "return searchShoppingList(event)"
				+ STARTQUOTATION + "/></td>");
		innerhtml.append("<td width=" + STARTQUOTATION + "24%" + STARTQUOTATION + "><input type=" + STARTQUOTATION + "button" + STARTQUOTATION
				+ " class=" + STARTQUOTATION + "mobBtn" + STARTQUOTATION + " value=" + STARTQUOTATION + "Search" + STARTQUOTATION + " onclick="
				+ STARTQUOTATION + "return searchShoppingList('')" + STARTQUOTATION + "  /> </td><td width=" + STARTQUOTATION + "4%" + STARTQUOTATION
				+ "><img src=" + STARTQUOTATION + "../images/cart.png" + STARTQUOTATION + " title=" + STARTQUOTATION + "List Items" + STARTQUOTATION
				+ " alt=" + STARTQUOTATION + "cart list" + STARTQUOTATION + " width=" + STARTQUOTATION + "16" + STARTQUOTATION + " height="
				+ STARTQUOTATION + "16" + STARTQUOTATION + " />");
		innerhtml.append("</td><td width=" + STARTQUOTATION + "15%" + STARTQUOTATION + "><span class=" + STARTQUOTATION + "cntItems" + STARTQUOTATION
				+ "> ");
		innerhtml.append("" + cartCount + "</span> Items in List</td><td width=" + STARTQUOTATION + "5%" + STARTQUOTATION + ">");
		innerhtml.append("</td><td width=" + STARTQUOTATION + "5%" + STARTQUOTATION + "><img src=" + STARTQUOTATION + "../images/cartAdd.png"
				+ STARTQUOTATION + " title=" + STARTQUOTATION + "Cart Items" + STARTQUOTATION + " alt=" + STARTQUOTATION + "cart added"
				+ STARTQUOTATION + " width=" + STARTQUOTATION + "16" + STARTQUOTATION + " height=" + STARTQUOTATION + "16" + STARTQUOTATION
				+ " /></td>");
		innerhtml.append("<td width=" + STARTQUOTATION + "17%" + STARTQUOTATION + "><span class=" + STARTQUOTATION + "cntCart" + STARTQUOTATION + ">"
				+ listCount + " </span>Items in Cart</td></tr></table>");
		innerhtml.append("<div class=" + STARTQUOTATION + "clear" + STARTQUOTATION + "></div><div class=" + STARTQUOTATION + "fluidView"
				+ STARTQUOTATION + "><div style=" + STARTQUOTATION + "height: 412px; overflow-x: hidden;" + STARTQUOTATION + "><table width="
				+ STARTQUOTATION + "100%" + STARTQUOTATION + " border=" + STARTQUOTATION + "0" + STARTQUOTATION + " cellspacing=" + STARTQUOTATION
				+ "0" + STARTQUOTATION + " cellpadding=" + STARTQUOTATION + "0" + STARTQUOTATION + " class=" + STARTQUOTATION + "gnrlGrd mobGrd htlt"
				+ STARTQUOTATION + " id=" + STARTQUOTATION + "slctItems" + STARTQUOTATION + ">");
		innerhtml.append(" <colgroup> <col width=" + STARTQUOTATION + "13%" + STARTQUOTATION + "/><col width=" + STARTQUOTATION + "49%"
				+ STARTQUOTATION + "/><col width=" + STARTQUOTATION + "17%" + STARTQUOTATION + "/><col width=" + STARTQUOTATION + "7%"
				+ STARTQUOTATION + "/><col width=" + STARTQUOTATION + "14%" + STARTQUOTATION + "/> </colgroup>");
		innerhtml.append("<input type=" + STARTQUOTATION + "hidden" + STARTQUOTATION + " name=" + STARTQUOTATION + "productId" + STARTQUOTATION
				+ " id=" + STARTQUOTATION + "productId" + STARTQUOTATION + " value=" + STARTQUOTATION + "" + STARTQUOTATION + " />");
		if (null != addRemoveSBProducts.getCartProducts())
		{
			for (Category category : addRemoveSBProducts.getCartProducts().getCategoryDetails())
			{

				innerhtml.append("<tr name=" + STARTQUOTATION + "" + category.getParentCategoryName() + "" + STARTQUOTATION + " class="
						+ STARTQUOTATION + "noHvr" + STARTQUOTATION + " >");
				innerhtml.append("<td colspan=" + STARTQUOTATION + "5" + STARTQUOTATION + " class=" + STARTQUOTATION + "mobSubTitle" + STARTQUOTATION
						+ " >" + category.getParentCategoryName() + "</td> </tr>");

				for (ProductDetail productDetail : category.getProductDetails())
				{
					innerhtml.append("<tr style=" + STARTQUOTATION + "cursor: pointer;" + STARTQUOTATION + " name=" + STARTQUOTATION + ""
							+ category.getParentCategoryName() + "" + STARTQUOTATION + ">");
					innerhtml.append("<td width=" + STARTQUOTATION + "13%" + STARTQUOTATION + " ><img src=" + STARTQUOTATION
							+ "../images/cartAdd.png" + STARTQUOTATION + " alt=" + STARTQUOTATION + "add" + STARTQUOTATION + " width="
							+ STARTQUOTATION + "34" + STARTQUOTATION + " height=" + STARTQUOTATION + "25" + STARTQUOTATION + " name="
							+ STARTQUOTATION + "tglImgs" + STARTQUOTATION + " " + "onclick=" + STARTQUOTATION + "addRemoveSBProducts('' ,"
							+ productDetail.getUserProductId() + "," + userId + ")" + STARTQUOTATION + "/></td>");
					innerhtml.append("<td width=" + STARTQUOTATION + "49%" + STARTQUOTATION + " class=" + STARTQUOTATION + "wrpWord" + STARTQUOTATION
							+ " onclick=" + STARTQUOTATION + "callproductSummarySC(" + productDetail.getProductId() + ")" + STARTQUOTATION + ">"
							+ productDetail.getProductName() + "</td>");
					innerhtml.append("<td width=" + STARTQUOTATION + "17%" + STARTQUOTATION + "><img src=" + STARTQUOTATION + ""
							+ productDetail.getProductImagePath() + "" + STARTQUOTATION + " width=" + STARTQUOTATION + "50" + STARTQUOTATION
							+ " height=" + STARTQUOTATION + "50" + STARTQUOTATION + " /></td>");
					innerhtml.append("<td width=" + STARTQUOTATION + "7%" + STARTQUOTATION + "><ul class=" + STARTQUOTATION + "clr" + STARTQUOTATION
							+ ">");
					innerhtml.append("<li><img src=" + STARTQUOTATION + "../images/C-" + productDetail.getCoupon_Status() + ".png" + STARTQUOTATION
							+ " alt=" + STARTQUOTATION + "c" + STARTQUOTATION + " width=" + STARTQUOTATION + "14" + STARTQUOTATION + " height="
							+ STARTQUOTATION + "14" + STARTQUOTATION + " /></li>");
					innerhtml.append("<li><img src=" + STARTQUOTATION + "../images/L-" + productDetail.getLoyalty_Status() + ".png" + STARTQUOTATION
							+ " alt=" + STARTQUOTATION + "c" + STARTQUOTATION + " width=" + STARTQUOTATION + "14" + STARTQUOTATION + " height="
							+ STARTQUOTATION + "14" + STARTQUOTATION + " /></li>");
					innerhtml.append("<li><img src=" + STARTQUOTATION + "../images/R-" + productDetail.getRebate_Status() + ".png" + STARTQUOTATION
							+ " alt=" + STARTQUOTATION + "c" + STARTQUOTATION + " width=" + STARTQUOTATION + "14" + STARTQUOTATION + " height="
							+ STARTQUOTATION + "14" + STARTQUOTATION + " /></li>");
					innerhtml.append("</ul></td><td width=" + STARTQUOTATION + "14%" + STARTQUOTATION + ">");
					innerhtml.append("<img src=" + STARTQUOTATION + "../images/deleteProdRow.png" + STARTQUOTATION + " alt=" + STARTQUOTATION
							+ "delete" + STARTQUOTATION + " name=" + STARTQUOTATION + "DeleteRow" + STARTQUOTATION + " width=" + STARTQUOTATION
							+ "29" + STARTQUOTATION + " height=" + STARTQUOTATION + "29" + STARTQUOTATION + " title=" + STARTQUOTATION + "delete"
							+ STARTQUOTATION + " style=" + STARTQUOTATION + "display: none;" + STARTQUOTATION + "  onclick= " + STARTQUOTATION
							+ "addRemoveTodaySLProducts(" + productDetail.getUserProductId() + " , " + userId + ")" + STARTQUOTATION + "/></td>");
					innerhtml.append("</tr>");
				}
			}
		}
		innerhtml.append("</table></div><div style=" + STARTQUOTATION + "height: 412px; overflow-x: hidden;" + STARTQUOTATION + ">");
		innerhtml.append("<table width=" + STARTQUOTATION + "100%" + STARTQUOTATION + " border=" + STARTQUOTATION + "0" + STARTQUOTATION
				+ " cellspacing=" + STARTQUOTATION + "0" + STARTQUOTATION + " cellpadding=" + STARTQUOTATION + "0" + STARTQUOTATION + " class="
				+ STARTQUOTATION + "gnrlGrd mobGrd htlt" + STARTQUOTATION + " id=" + STARTQUOTATION + "cartItems" + STARTQUOTATION + ">");
		innerhtml.append("<colgroup><col width=" + STARTQUOTATION + "13%" + STARTQUOTATION + "/><col width=" + STARTQUOTATION + "49%"
				+ STARTQUOTATION + "/><col width=" + STARTQUOTATION + "17%" + STARTQUOTATION + "/><col width=" + STARTQUOTATION + "7%"
				+ STARTQUOTATION + "/><col width=" + STARTQUOTATION + "14%" + STARTQUOTATION + "/> </colgroup>");
		if (null != addRemoveSBProducts.getBasketProducts())
		{
			for (Category category : addRemoveSBProducts.getBasketProducts().getCategoryDetails())
			{
				innerhtml.append("<tr name=" + STARTQUOTATION + "" + category.getParentCategoryName() + "" + STARTQUOTATION + " class="
						+ STARTQUOTATION + "noHvr" + STARTQUOTATION + " >");
				innerhtml.append("<td colspan=" + STARTQUOTATION + "5" + STARTQUOTATION + " class=" + STARTQUOTATION + "mobSubTitle" + STARTQUOTATION
						+ ">" + category.getParentCategoryName() + "</td> </tr>");

				for (ProductDetail productDetail : category.getProductDetails())
				{
					innerhtml.append("<tr style=" + STARTQUOTATION + "cursor: pointer;" + STARTQUOTATION + "name=" + STARTQUOTATION + ""
							+ category.getParentCategoryName() + "" + STARTQUOTATION + ">");
					innerhtml.append("<td width=" + STARTQUOTATION + "13%" + STARTQUOTATION + "><img src=" + STARTQUOTATION
							+ "../images/cartDelete.png" + STARTQUOTATION + " alt=" + STARTQUOTATION + "add" + STARTQUOTATION + " width="
							+ STARTQUOTATION + "34" + STARTQUOTATION + " height=" + STARTQUOTATION + "25" + STARTQUOTATION + " name="
							+ STARTQUOTATION + "tglImgs" + STARTQUOTATION + " " + "onclick=" + STARTQUOTATION + "addRemoveSBProducts("
							+ productDetail.getUserProductId() + ", '' ," + userId + ")" + STARTQUOTATION + "/></td>");
					innerhtml.append("<td width=" + STARTQUOTATION + "49%" + STARTQUOTATION + " class=" + STARTQUOTATION + "wrpWord" + STARTQUOTATION
							+ " onclick=" + STARTQUOTATION + "callproductSummarySC(" + productDetail.getProductId() + ")" + STARTQUOTATION + " >"
							+ productDetail.getProductName() + "</td>");
					innerhtml.append("<td width=" + STARTQUOTATION + "17%" + STARTQUOTATION + "><img src=" + STARTQUOTATION + ""
							+ productDetail.getProductImagePath() + "" + STARTQUOTATION + " width=" + STARTQUOTATION + "50" + STARTQUOTATION
							+ " height=" + STARTQUOTATION + "50" + STARTQUOTATION + " /></td>");
					innerhtml.append("<td width=" + STARTQUOTATION + "7%" + STARTQUOTATION + "><ul class=" + STARTQUOTATION + "clr" + STARTQUOTATION
							+ ">");
					innerhtml.append("<li><img src=" + STARTQUOTATION + "../images/C-" + productDetail.getCoupon_Status() + ".png" + STARTQUOTATION
							+ " alt=" + STARTQUOTATION + "c" + STARTQUOTATION + " width=" + STARTQUOTATION + "14" + STARTQUOTATION + " height="
							+ STARTQUOTATION + "14" + STARTQUOTATION + " /></li>");
					innerhtml.append("<li><img src=" + STARTQUOTATION + "../images/L-" + productDetail.getLoyalty_Status() + ".png" + STARTQUOTATION
							+ " alt=" + STARTQUOTATION + "c" + STARTQUOTATION + " width=" + STARTQUOTATION + "14" + STARTQUOTATION + " height="
							+ STARTQUOTATION + "14" + STARTQUOTATION + " /></li>");
					innerhtml.append("<li><img src=" + STARTQUOTATION + "../images/R-" + productDetail.getRebate_Status() + ".png" + STARTQUOTATION
							+ " alt=" + STARTQUOTATION + "c" + STARTQUOTATION + " width=" + STARTQUOTATION + "14" + STARTQUOTATION + " height="
							+ STARTQUOTATION + "14" + STARTQUOTATION + " /></li>");
					innerhtml.append("</ul></td><td width=" + STARTQUOTATION + "14%" + STARTQUOTATION + ">&nbsp</td>");
				}
				innerhtml.append("</tr>");
			}

			innerhtml.append("</table>");
			innerhtml.append("<ul id=" + STARTQUOTATION + "btnBar" + STARTQUOTATION + "><li>");
			innerhtml.append("<input type=" + STARTQUOTATION + "button" + STARTQUOTATION + " class=" + STARTQUOTATION + "mobBtn" + STARTQUOTATION
					+ " value=" + STARTQUOTATION + "Check Out" + STARTQUOTATION + " onclick=" + STARTQUOTATION + "moveToHstry()" + STARTQUOTATION
					+ " title=" + STARTQUOTATION + "Remove Items from Cart" + STARTQUOTATION + " /> <img src=" + STARTQUOTATION
					+ "../images/cart-remove.png" + STARTQUOTATION + " alt=" + STARTQUOTATION + "Remove Cart Items" + STARTQUOTATION + "/>");
			innerhtml.append("</li></ul></div>");
		}
		innerhtml.append("</div>");
		LOG.info("Response \n" + innerhtml);
		LOG.info(innerhtml.toString());
		return innerhtml.toString();
	}

	/**
	 * .
	 * 
	 * @param .
	 * @return
	 */

	public String favDeleteshoppinglistDivBuilding(ShoppingListDetails shoppingListDetails, Long userId)
	{
		StringBuffer innerhtml = new StringBuffer();

		innerhtml
				.append(" <div class=" + STARTQUOTATION + "fluidView" + STARTQUOTATION + " id = " + STARTQUOTATION + "favDiv" + STARTQUOTATION + ">");
		innerhtml.append("<div style=" + STARTQUOTATION + "height: 408px; overflow-x: hidden;" + STARTQUOTATION + " class=" + STARTQUOTATION
				+ "stretch" + STARTQUOTATION + "> <table class=" + STARTQUOTATION + "gnrlGrd mobGrd htlt" + STARTQUOTATION + " id=" + STARTQUOTATION
				+ "hstryItems" + STARTQUOTATION + " border=" + STARTQUOTATION + "0" + STARTQUOTATION + " cellpadding=" + STARTQUOTATION + "0"
				+ STARTQUOTATION + " cellspacing=" + STARTQUOTATION + "0" + STARTQUOTATION + " width=" + STARTQUOTATION + "100%" + STARTQUOTATION
				+ ">");
		innerhtml.append("<tbody> <input type=" + STARTQUOTATION + "hidden" + STARTQUOTATION + " name=" + STARTQUOTATION + "productId"
				+ STARTQUOTATION + " id=" + STARTQUOTATION + "productId" + STARTQUOTATION + " value=" + STARTQUOTATION + "" + STARTQUOTATION + " />");
		if (null != shoppingListDetails.getCategoryInfolst())
		{
			for (Category category : shoppingListDetails.getCategoryInfolst())
			{
				innerhtml.append("<tr style=" + STARTQUOTATION + "cursor: pointer;" + STARTQUOTATION + " class=" + STARTQUOTATION + ""
						+ STARTQUOTATION + "> <td style=" + STARTQUOTATION + "cursor: default;" + STARTQUOTATION + " colspan=" + STARTQUOTATION + "4"
						+ STARTQUOTATION + " class=" + STARTQUOTATION + "mobSubTitle" + STARTQUOTATION + ">" + category.getParentCategoryName()
						+ "</td> </tr>");
				for (ProductDetail productDetail : category.getProductDetails())
				{
					innerhtml.append("<tr style=" + STARTQUOTATION + "cursor: pointer;" + STARTQUOTATION + " class=" + STARTQUOTATION + ""
							+ STARTQUOTATION + "> <td style=" + STARTQUOTATION + "border-right: 1px solid rgb(218, 218, 218);" + STARTQUOTATION
							+ " width=" + STARTQUOTATION + "5%" + STARTQUOTATION + " align=" + STARTQUOTATION + "center" + STARTQUOTATION
							+ "> <input type=" + STARTQUOTATION + "checkbox" + STARTQUOTATION + " value=" + STARTQUOTATION + productDetail.getUserProductId()+ STARTQUOTATION + "id=" + STARTQUOTATION + "favCheck"+ STARTQUOTATION +"name=" + STARTQUOTATION +"favCheck"+ STARTQUOTATION +"></input></td>");
					innerhtml.append("<td style=" + STARTQUOTATION + "border-right: 1px solid rgb(218, 218, 218);" + STARTQUOTATION + " class="
							+ STARTQUOTATION + "wrpWord" + STARTQUOTATION + " onclick=" + STARTQUOTATION + "callproductSummarySC("
							+ productDetail.getProductId() + ")" + STARTQUOTATION + ">");
					innerhtml.append(productDetail.getProductName());
					innerhtml.append("</td>");
					innerhtml.append("<td style=" + STARTQUOTATION + "border-right: 1px solid rgb(218, 218, 218);" + STARTQUOTATION + " width="
							+ STARTQUOTATION + "15%" + STARTQUOTATION + ">&nbsp;</td>");
					innerhtml.append(" <td width=" + STARTQUOTATION + "6%" + STARTQUOTATION + " >");

					innerhtml.append("<img style=" + STARTQUOTATION + "display: none;" + STARTQUOTATION + " src=" + STARTQUOTATION
							+ "../images/deleteProdRow.png" + STARTQUOTATION + " alt=" + STARTQUOTATION + "delete" + STARTQUOTATION + " name="
							+ STARTQUOTATION + "DeleteRow" + STARTQUOTATION + " title=" + STARTQUOTATION + "delete" + STARTQUOTATION + " height="
							+ STARTQUOTATION + "29" + STARTQUOTATION + " width=" + STARTQUOTATION + "29" + STARTQUOTATION + " onclick= "
							+ STARTQUOTATION + "deleteProductMainShopListFav(" + productDetail.getUserProductId() + "," + userId + ")"
							+ STARTQUOTATION + "></td> </tr>");
				}
			}

		}
		innerhtml.append("</tbody> </table> </div> </div> ");

		return innerhtml.toString();
	}

	/**
	 * This method for Categorizing master shopping list products. It includes
	 * product details and clr information grouped by category id.
	 * 
	 * @param shoppingListResultSet
	 *            as request parameter.
	 * @return ShoppingListRetailerCategoryList as response
	 */
	public static ShoppingListDetails getShoppingListCategoryList(ArrayList<ProductDetail> shoppingListResultSet)
	{

		final ShoppingListDetails shoppingListCategoryList = new ShoppingListDetails();

		final HashMap<Integer, Category> categoryMap = new HashMap<Integer, Category>();

		ArrayList<ProductDetail> productDetaillst = null;
		Category categoryInfo = null;
		Integer key = null;
		for (ProductDetail listResultSet : shoppingListResultSet)
		{
			key = listResultSet.getParentCategoryID();

			if (categoryMap.containsKey(key))
			{

				categoryInfo = categoryMap.get(key);
				productDetaillst = categoryInfo.getProductDetails();
				if (null != productDetaillst)

				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductId());
					productDetail.setCLRFlag(listResultSet.getCLRFlag());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setCoupon_Status(listResultSet.getCoupon_Status());
					productDetail.setRebate_Status(listResultSet.getRebate_Status());
					productDetail.setLoyalty_Status(listResultSet.getLoyalty_Status());
					productDetail.setButtonFlag(listResultSet.getButtonFlag());
					productDetail.setSubCategoryID(listResultSet.getSubCategoryID());
					productDetail.setSubCategoryName(listResultSet.getSubCategoryName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getPrice());
					productDetaillst.add(productDetail);
					categoryInfo.setProductDetails(productDetaillst);
				}
			}
			else
			{
				categoryInfo = new Category();
				categoryInfo.setCategoryId(listResultSet.getCategoryID());
				categoryInfo.setParentCategoryId(listResultSet.getParentCategoryID());
				categoryInfo.setParentCategoryName(listResultSet.getParentCategoryName());
				productDetaillst = new ArrayList<ProductDetail>();
				final ProductDetail productDetail = new ProductDetail();
				if (null != listResultSet.getProductId())
				{
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductId());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setCLRFlag(listResultSet.getCLRFlag());
					productDetail.setCoupon_Status(listResultSet.getCoupon_Status());
					productDetail.setRebate_Status(listResultSet.getRebate_Status());
					productDetail.setLoyalty_Status(listResultSet.getLoyalty_Status());
					productDetail.setButtonFlag(listResultSet.getButtonFlag());
					productDetail.setSubCategoryID(listResultSet.getSubCategoryID());
					productDetail.setSubCategoryName(listResultSet.getSubCategoryName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getPrice());
					productDetaillst.add(productDetail);
					categoryInfo.setProductDetails(productDetaillst);
				}
				categoryMap.put(key, categoryInfo);
			}
		}

		final Set<Map.Entry<Integer, Category>> set = categoryMap.entrySet();

		final ArrayList<Category> categoryInfolst = new ArrayList<Category>();

		for (Map.Entry<Integer, Category> entry : set)
		{
			categoryInfolst.add(entry.getValue());
		}

		shoppingListCategoryList.setCategoryInfolst(categoryInfolst);
		return shoppingListCategoryList;
	}

	/**
	 * This method for displaying shopping list history grouped by parent
	 * category name.
	 * 
	 * @param shoppingListResultSet
	 *            as request parameter.
	 * @return ShoppingListRetailerCategoryList as response
	 */
	public ShoppingListRetailerCategoryList fetchSLHistoryCategoryDisplay(ArrayList<MainSLRetailerCategory> shoppingListResultSet)
	{

		final ShoppingListRetailerCategoryList shoppingListCategoryList = new ShoppingListRetailerCategoryList();

		final HashMap<String, SLCategory> categoryMap = new HashMap<String, SLCategory>();

		ArrayList<ProductDetail> productDetaillst = null;
		SLCategory categoryInfo = null;
		String key = null;
		for (MainSLRetailerCategory listResultSet : shoppingListResultSet)
		{
			key = listResultSet.getParentCategoryName();

			if (categoryMap.containsKey(key))
			{

				categoryInfo = categoryMap.get(key);
				productDetaillst = categoryInfo.getProductDetails();
				if (null != productDetaillst)

				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductID());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setStarFlag(listResultSet.getStarFlag());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getPrice());
					productDetail.setRow_Num(listResultSet.getRowNum());
					productDetaillst.add(productDetail);
					categoryInfo.setProductDetails(productDetaillst);
				}
			}
			else
			{
				categoryInfo = new SLCategory();
				categoryInfo.setParentCategoryId(listResultSet.getParentCategoryID());
				categoryInfo.setParentCategoryName(listResultSet.getParentCategoryName());
				productDetaillst = new ArrayList<ProductDetail>();
				final ProductDetail productDetail = new ProductDetail();
				if (null != listResultSet.getProductId())
				{
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductID());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setStarFlag(listResultSet.getStarFlag());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getPrice());
					productDetail.setRow_Num(listResultSet.getRowNum());
					productDetaillst.add(productDetail);
					categoryInfo.setProductDetails(productDetaillst);
				}
				categoryMap.put(key, categoryInfo);
			}
		}

		final Set<Map.Entry<String, SLCategory>> set = categoryMap.entrySet();

		final ArrayList<SLCategory> categoryInfolst = new ArrayList<SLCategory>();

		for (Map.Entry<String, SLCategory> entry : set)
		{
			categoryInfolst.add(entry.getValue());
		}

		shoppingListCategoryList.setCategoryInfolst(categoryInfolst);
		return shoppingListCategoryList;
	}
}

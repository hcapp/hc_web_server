package shopper.shoppingList.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import shopper.shoppingList.helper.ShoppingListHelper;
import shopper.shoppingList.service.ShoppingListService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Users;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.AddSLRequest;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.ShoppingListDetails;
import common.pojo.shopper.ShoppingListRequest;
import common.pojo.shopper.ShoppingListRetailerCategoryList;
import common.pojo.shopper.UserNotesDetails;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is a controller class for this location page
 * 
 * @author emmanuel_b
 */
@Controller
public class ShoppingListController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ShoppingListController.class);

	/**
	 * This is a Method for fetching the products from shopping basket for the
	 * given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which the products from shopping basket need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return String response as a string contains all shopping basket
	 *         products. if no products found No records found code will be
	 *         returned.
	 */
	@RequestMapping(value = "/shoppingList.htm", method = RequestMethod.GET)
	public String getShoppingBasketProd(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getShoppingBasketProd of ShoppingListController";
		AddRemoveSBProducts addRemoveSBProducts = null;
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute("retailerNameProductSum");
		session.removeAttribute("salepriceProductSum");
		ShoppingListRequest ShoppingList = new ShoppingListRequest();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		Users loginUser = (Users) session.getAttribute("loginuser");
		ShoppingList.setUserId(loginUser.getUserID());
		model.put("ShoppingListRequest", ShoppingList);

		session.removeAttribute("favourite");
		try
		{
			addRemoveSBProducts = shoppingListService.getShopBasketProducts(ShoppingList.getUserId());

			request.setAttribute("addRemoveSBProducts", addRemoveSBProducts);
			session.setAttribute("fromSL", "SLMain");
			session.removeAttribute("FromSLSearch");
			session.removeAttribute("fromSLFavSearch");
			session.removeAttribute("FromWLSearch");
			session.removeAttribute("prodSearch");
			session.removeAttribute("fromFind");
			session.removeAttribute("fromscannow");
			session.removeAttribute("fromSLFAV");
			session.removeAttribute("fromSLHistory");
			session.removeAttribute("fromWL");
			session.removeAttribute("fromWLHistory");
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "shoppingList";
	}

	/**
	*
	*
	*
	*
	* 
	*/
	@RequestMapping(value = "/searchShoppingList.htm", method = RequestMethod.POST)
	public String scanSearchProductForName(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		final String methodName = "scanSearchProductForName";
		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		Pagination objPage = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ProductDetails productDetails = null;

		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		String addedTo = (String) request.getParameter("addedTo");
		try
		{
			if (null != pageFlag && "true".equalsIgnoreCase(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}
			}
			if (ShoppingList.getSearchValue() == null)
			{
				ShoppingList = (ShoppingListRequest) session.getAttribute("ShoppingListReq");
			}
			productDetails = shoppingListService.scanSearchProductForName(ShoppingList, lowerLimit);
			String strFlag = (String) session.getAttribute("moduleName");
			String favourite = (String) request.getParameter("fav");
			if ("SLFavSearch".equals(strFlag))
			{
				session.setAttribute("moduleName", "SLFavSearch");
			}
			else
			{
				if ("true".equals(favourite))
				{
					session.setAttribute("fromSLFavSearch", "true");
					session.setAttribute("moduleName", "SLFavSearch");
					session.removeAttribute("FromSLSearch");
					session.removeAttribute("FromWLSearch");
					session.removeAttribute("prodSearch");
					session.removeAttribute("fromFind");
					session.removeAttribute("fromscannow");
					session.removeAttribute("fromSL");
					session.removeAttribute("fromSLFAV");
					session.removeAttribute("fromSLHistory");
					session.removeAttribute("fromWL");
					session.removeAttribute("fromWLHistory");
				}
				else
				{
					session.setAttribute("FromSLSearch", "true");
					session.setAttribute("moduleName", "SLSearch");
					session.removeAttribute("fromSLFavSearch");
					session.removeAttribute("FromWLSearch");
					session.removeAttribute("prodSearch");
					session.removeAttribute("fromFind");
					session.removeAttribute("fromscannow");
					session.removeAttribute("fromSL");
					session.removeAttribute("fromSLFAV");
					session.removeAttribute("fromSLHistory");
					session.removeAttribute("fromWL");
					session.removeAttribute("fromWLHistory");
				}
			}
			request.setAttribute("favourite", favourite);
			if (null != productDetails)
			{
				objPage = Utility.getPagination(productDetails.getTotalSize(), currentPage, "searchShoppingList.htm");

			}

			if (ShoppingList.isIsWishlst() == true)
			{
				session.setAttribute("FromWishlist", ShoppingList.isIsWishlst());
				session.setAttribute("FromWLSearch", "true");
				session.setAttribute("moduleName", "WLSearch");
				session.removeAttribute("FromSLSearch");
				session.removeAttribute("fromSLFavSearch");
				session.removeAttribute("FromWLSearch");
				session.removeAttribute("prodSearch");
				session.removeAttribute("fromFind");
				session.removeAttribute("fromscannow");
				session.removeAttribute("fromSL");
				session.removeAttribute("fromSLFAV");
				session.removeAttribute("fromSLHistory");
				session.removeAttribute("fromWL");
				session.removeAttribute("fromWLHistory");
			}
			else
			{
				session.setAttribute("FromWishlist", false);
			}
			Boolean strBack = ShoppingList.isBack();
			if (strBack == true)
				session.setAttribute("fromback", true);
			else
				session.setAttribute("fromback", false);
			request.setAttribute("productDetails", productDetails);
			request.setAttribute("addedTo", addedTo);
			session.setAttribute("ShoppingListReq", ShoppingList);
			session.setAttribute("pagination", objPage);
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		catch (ScanSeeWebSqlException e)
		{

		}
		return "searchShoppingList";
	}

	/**
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/addToMainShoppingList.htm", method = RequestMethod.POST)
	public String addToMainShoppingList(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		final String methodName = "addToMainShoppingList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		AddRemoveSBProducts addRemoveSBProducts = null;
		try
		{
			ProductDetails userProdIdList = shoppingListService.addProductMainToShopList(ShoppingList);

			if (userProdIdList.getProductDetail().size() > 0)
			{
				addRemoveSBProducts = shoppingListService.getShopBasketProducts(ShoppingList.getUserId());
			}
			request.setAttribute("addRemoveSBProducts", addRemoveSBProducts);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "shoppingList";

	}

	/**
	 * This Method for adding today's SL product for the given product id and
	 * user id.
	 * 
	 * @param .
	 * @return containing List of SL products.
	 */

	@RequestMapping(value = "/addTodaySLProductsBySearch.htm", method = RequestMethod.POST)
	public String addTodaySLProductsBySearch(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		final String methodName = "addTodaySLProductsBySearch of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		Pagination objPage = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		ProductDetails productDetails = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		AddRemoveSBProducts addRemoveSBProducts = null;
		String view = null;
		try
		{
			ProductDetail userProdId = shoppingListService.addTodaySLProductsBySearch(ShoppingList);

			if (userProdId.getProductIsThere() == 1)
			{
				if (null != pageFlag && "true".equalsIgnoreCase(pageFlag))
				{
					pageNumber = request.getParameter("pageNumber");
					Pagination pageSess = (Pagination) session.getAttribute("pagination");
					if (Integer.valueOf(pageNumber) != 0)
					{
						currentPage = Integer.valueOf(pageNumber);
						int number = Integer.valueOf(currentPage) - 1;
						int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					}
					else
					{
						lowerLimit = 0;
					}

				}
				productDetails = shoppingListService.scanSearchProductForName(ShoppingList, lowerLimit);
				if (null != productDetails)
				{
					objPage = Utility.getPagination(productDetails.getProductDetail().size(), currentPage, "searchShoppingList.htm");

				}
				if (ShoppingList.isIsWishlst() == true)
				{
					request.setAttribute("duplicateprodtxt", ApplicationConstants.DUPLICATESLPRODUCTTEXT);
				}
				else
				{
					request.setAttribute("duplicateprodtxt", ApplicationConstants.DUPLICATESLPRODUCTTEXT);
				}

				request.setAttribute("productDetails", productDetails);
				LOG.info("Product Exists Already.");
				view = "searchShoppingList";
			}
			else
			{
				addRemoveSBProducts = shoppingListService.getShopBasketProducts(ShoppingList.getUserId());
				request.setAttribute("addRemoveSBProducts", addRemoveSBProducts);
				view = "shoppingList";
			}
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return view;
	}

	/**
	 * This is a Method adds/removes the products to/from Today's shopping
	 * list.Method Type:GET.
	 * 
	 * @param contains
	 *            information about the products.
	 * @return String response as Success or failure.
	 */
	@RequestMapping(value = "/addRemoveTodaySLProducts.htm", method = RequestMethod.GET)
	public @ResponseBody
	String addRemoveTodaySLProducts(@RequestParam(value = "userProductId", required = true) Integer userProductId,
			@RequestParam(value = "userId", required = true) Long userId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{
		final String methodName = "addRemoveTodaySLProducts of ShoppingListController";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		AddRemoveSBProducts addRemoveSBProducts = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		String responce = null;
		ShoppingListRequest ShoppingList = new ShoppingListRequest();
		ShoppingListHelper shoppingListHelper = new ShoppingListHelper();
		ShoppingList.setUserProductID(userProductId);
		ShoppingList.setUserId(userId);
		try
		{
			responce = shoppingListService.addRemoveTodaySLProducts(ShoppingList);
			if (ApplicationConstants.SUCCESS.equals(responce))
			{
				addRemoveSBProducts = shoppingListService.getShopBasketProducts(ShoppingList.getUserId());
				responce = shoppingListHelper.shoppinglistDivBuilding(addRemoveSBProducts, userId);
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responce;
	}

	/**
	 * This is a Method for adds/removes products to/from shopping basket.
	 * Method Type:GET.
	 * 
	 * @param input
	 *            information need to be adds/removes products to/from shopping
	 *            basket.
	 * @return response: return the whether updated is successful or not.
	 */

	@RequestMapping(value = "/addRemoveSBProducts.htm", method = RequestMethod.GET)
	public @ResponseBody
	String addRemoveSBProducts(@RequestParam(value = "userProductId", required = true) Integer userProductId,
			@RequestParam(value = "userId", required = true) Long userId,
			@RequestParam(value = "cartUserProductId", required = true) Integer cartUserProductId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "addRemoveSBProducts of ShoppingListController";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responce = null;
		AddRemoveSBProducts addRemoveSBProducts = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		ShoppingListHelper shoppingListHelper = new ShoppingListHelper();
		ShoppingListRequest ShoppingList = new ShoppingListRequest();
		ShoppingList.setUserProductID(userProductId);
		ShoppingList.setCartUserProductID(cartUserProductId);
		ShoppingList.setUserId(userId);

		try
		{
			responce = shoppingListService.addRemoveSBProducts(ShoppingList);
			if (ApplicationConstants.SUCCESS.equals(responce))
			{
				addRemoveSBProducts = shoppingListService.getShopBasketProducts(ShoppingList.getUserId());
				responce = shoppingListHelper.shoppinglistDivBuilding(addRemoveSBProducts, userId);
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responce;
	}

	/**
	 * This is a RestEasy WebService Method for retrieve User notes for shopping
	 * list. Method Type:GET.
	 * 
	 * @param userId
	 *            user id id for fetching the notes.
	 * @return String response contains user notes.
	 */
	@RequestMapping(value = "/retrieveUserNotes.htm", method = RequestMethod.GET)
	public String retrieveUserNotes(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		final String methodName = "retrieveUserNotes of Shoppinglist Controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		UserNotesDetails userNotesDetails = new UserNotesDetails();
		try
		{
			userNotesDetails = shoppingListService.getUserNotesDetails(userId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		request.setAttribute("userNotesDetails", userNotesDetails);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "shopperNotes";
	}

	/**
	 * This is a RestEasy WebService Method for add User notes for shopping
	 * list. Method Type:POST.
	 * 
	 * @param @RequestParam,userNotes,HttpServletRequest,HttpServletResponse,
	 *        HttpSession.
	 * @return String response as Success or failure.
	 */
	@RequestMapping(value = "/saveUserNotes.htm", method = RequestMethod.GET)
	public @ResponseBody
	String addUserNotes(@RequestParam(value = "userNotes", required = true) String userNotes, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "addUserNotes of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String result = null;
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		try
		{
			result = shoppingListService.addUserNotes(userId, userNotes);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return result;
	}

	/**
	 * This is a RestEasy WebService Method for removing all items from shopping
	 * cart list as CheckOut functionality.Method Type:GET.
	 * 
	 * @param ShoppingListRequest
	 *            ,HttpServletRequest,HttpSession,ModelMap.
	 * @return String view.
	 */
	@RequestMapping(value = "/checkOut.htm", method = RequestMethod.POST)
	public String checkOut(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{
		final String methodName = "checkOut";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		AddRemoveSBProducts addRemoveSBProducts = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		try
		{
			response = shoppingListService.checkOutFromBasket(ShoppingList.getUserId(), 0, 0);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				addRemoveSBProducts = shoppingListService.getShopBasketProducts(ShoppingList.getUserId());
				request.setAttribute("addRemoveSBProducts", addRemoveSBProducts);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return "shoppingList";
	}

	/**
	 * This Method is for fetching master shopping list products based on
	 * categories for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which master shopping list categories and products need to
	 *            be fetched.If userId is null then it is invalid request.
	 * @return XML containing master shopping list items in the response.
	 */
	@RequestMapping(value = "/favCategoryProducts.htm", method = RequestMethod.GET)
	public String getMSLCategoryProducts(

	HttpServletRequest request, HttpSession session, ModelMap model)
	{

		final String methodName = "getMSLCategoryProducts of ShoppingListController";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ShoppingListDetails shoppingListDetails = new ShoppingListDetails();
		ShoppingListRequest ShoppingList = new ShoppingListRequest();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		ShoppingList.setUserId(userId);
		model.put("ShoppingListRequest", ShoppingList);
		session.setAttribute("favourite", "true");
		try
		{
			shoppingListDetails = shoppingListService.getMSLCategoryProducts(ShoppingList.getUserId());

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);

		}
		request.setAttribute("shoppingListDetails", shoppingListDetails);
		LOG.info(ApplicationConstants.METHODEND + methodName);

		session.setAttribute("fromSLFAV", "SLFAV");
		session.removeAttribute("FromSLSearch");
		session.removeAttribute("fromSLFavSearch");
		session.removeAttribute("FromWLSearch");
		session.removeAttribute("prodSearch");
		session.removeAttribute("fromFind");
		session.removeAttribute("fromscannow");
		session.removeAttribute("fromSL");
		session.removeAttribute("fromSLHistory");
		session.removeAttribute("fromWL");
		session.removeAttribute("fromWLHistory");
		return "favCategoryProducts";
	}

	/**
	 * This is a RestEasy WebService Method for fetching shopping list History
	 * products based on given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which shopping list history products need to be fetched.If
	 *            userId is null then it is invalid request.
	 * @return XML containing shopping list history items in the response.
	 */
	@RequestMapping(value = "/slHistory.htm", method = RequestMethod.POST)
	public String fetchSLHistory(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "fetchSLHistory of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		Integer lowerlimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		Pagination objPage = null;
		String pageFlag = (String) request.getParameter("pageFlag");

		if (null != pageFlag && pageFlag.equals("true"))
		{
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerlimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerlimit = 0;
			}

		}
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		ShoppingListRetailerCategoryList shoppingListCategoryInfo = new ShoppingListRetailerCategoryList();

		try
		{
			shoppingListCategoryInfo = shoppingListService.fetchSLHistoryItems(userId, lowerlimit);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		request.setAttribute("shoppingListCategoryInfo", shoppingListCategoryInfo);
		if (null != shoppingListCategoryInfo)
		{
			objPage = Utility.getPagination(shoppingListCategoryInfo.getTotalSize(), currentPage, "retailerinfo.htm");

		}
		String fromSL = ShoppingList.getFromSL();
		if ("true".equals(fromSL))
		{
			request.setAttribute("fromSL", fromSL);
		}
		String fromSLFAV = ShoppingList.getFromSLFAV();
		if ("true".equals(fromSLFAV))
		{
			request.setAttribute("fromSLFAV", fromSLFAV);
		}
		session.setAttribute("pagination", objPage);
		session.removeAttribute("FromSLSearch");
		session.removeAttribute("FromWLSearch");
		session.removeAttribute("fromSLFavSearch");
		session.removeAttribute("FromWLSearch");
		session.removeAttribute("prodSearch");
		session.removeAttribute("fromFind");
		session.removeAttribute("fromscannow");
		session.removeAttribute("fromSL");
		session.removeAttribute("fromSLFAV");
		session.setAttribute("fromSLHistory", "SLHistory");
		session.removeAttribute("fromWL");
		session.removeAttribute("fromWLHistory");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "shoppinglisthistory";

	}

	/**
	 * This is a deleting product from SL fav. Method Type:POST.
	 * 
	 * @param userProductId
	 *            ,userId ,request ,request,session containing input information
	 *            need to be deleted product from SL.
	 * @return response: return the whether updated is successful or not.
	 */
	@RequestMapping(value = "/deleteProductMainShopListFav.htm", method = RequestMethod.GET)
	public @ResponseBody
	String deleteProductMainShopListFav(@RequestParam(value = "userProductId", required = true) Integer userProductId,
			@RequestParam(value = "userId", required = true) Long userId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{
		final String methodName = "deleteProductMainShopListFav ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String wresponse = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		ShoppingListRequest ShoppingList = new ShoppingListRequest();
		ShoppingListDetails shoppingListDetails = new ShoppingListDetails();
		ShoppingListHelper shoppingListHelper = new ShoppingListHelper();
		ShoppingList.setUserProductID(userProductId);
		Users loginUser = (Users) session.getAttribute("loginuser");
		ShoppingList.setUserId(loginUser.getUserID());
		try
		{
			wresponse = shoppingListService.deleteProductMainShopListFav(ShoppingList);
			if (ApplicationConstants.SUCCESS.equals(wresponse))
			{
				shoppingListDetails = shoppingListService.getMSLCategoryProducts(ShoppingList.getUserId());
				if (null != shoppingListDetails)
				{
					wresponse = shoppingListHelper.favDeleteshoppinglistDivBuilding(shoppingListDetails, userId);
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return wresponse;
	}

	/**
	 * This is a RestEasy WebService Method for adding product from shopping
	 * list history to list or favorites or both. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information needed to be add product form
	 *            shopping list history to list or favorites or both products .
	 * @return response: return the whether updated is successful or not.
	 */
	@RequestMapping(value = "/addItemstoSL.htm", method = RequestMethod.GET)
	public @ResponseBody
	String addProuctFromHistoryToSL(@RequestParam(value = "strSel", required = true) String strSel,
			@RequestParam(value = "curVal", required = true) String curVal, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{
		final String methodName = "addProuctFromHistoryToSL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String result = null;
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		ArrayList<ProductDetail> productDetails = new ArrayList<ProductDetail>();
		StringTokenizer st1 = new StringTokenizer(strSel, "\n");
		List<String> temp = new ArrayList<String>();
		while (st1.hasMoreElements())
		{
			String token1 = st1.nextToken();
			temp.add(token1);
		}
		for (int i = 0; i < temp.size(); i++)
		{
			StringTokenizer st2 = new StringTokenizer(temp.get(i), ",");
			List<String> temp1 = new ArrayList<String>();
			while (st2.hasMoreElements())
			{
				String token1 = st2.nextToken();
				temp1.add(token1);
			}
			ProductDetail productDetail = new ProductDetail();
			productDetail.setProductId(Integer.parseInt(temp1.get(0)));
			productDetail.setUserProductId(Integer.parseInt(temp1.get(1)));
			productDetails.add(productDetail);

		}
		AddSLRequest addSLRequest = new AddSLRequest();
		addSLRequest.setAddTo(curVal);
		addSLRequest.setProductDetails(productDetails);
		ProductDetails objProductDetail = new ProductDetails();
		try
		{
			objProductDetail = shoppingListService.addSLHistoryProdut(addSLRequest);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		result = objProductDetail.getResponseFlag();
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return result;
	}

	/**
	 * This is a Method adds/removes the products to/from Today's shopping
	 * list.Method Type:GET.
	 * 
	 * @param contains
	 *            information about the products.
	 * @return String response as Success or failure.
	 */
	@RequestMapping(value = "/addRemoveFavSLProducts.htm", method = RequestMethod.POST)
	public String addRemoveFavSLProducts(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "addRemoveTodaySLProducts of ShoppingListController";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		String responce = null;
		String view = null;
		String[] userProductId = request.getParameterValues("favCheck");
		ArrayList<ProductDetail> productDetails = new ArrayList<ProductDetail>();
		AddRemoveSBProducts addRemoveSBProducts = null;
		ShoppingListDetails shoppingListDetails = new ShoppingListDetails();
		for (String string : userProductId)
		{
			ProductDetail productDetail = new ProductDetail();
			productDetail.setUserProductId(Integer.parseInt(string));
			productDetails.add(productDetail);
		}
		ShoppingList.setProductDetails(productDetails);
		ShoppingList.setIsaddToTSL(true);
		try
		{
			responce = shoppingListService.addRemoveTodaySLProducts(ShoppingList);
			if (ApplicationConstants.SUCCESS.equals(responce))
			{
				addRemoveSBProducts = shoppingListService.getShopBasketProducts(ShoppingList.getUserId());
				request.setAttribute("addRemoveSBProducts", addRemoveSBProducts);
				view = "shoppingList";
			}
			else
			{
				shoppingListDetails = shoppingListService.getMSLCategoryProducts(ShoppingList.getUserId());
				request.setAttribute("shoppingListDetails", shoppingListDetails);
				request.setAttribute("sladdprodtlexists", ApplicationConstants.SLADDPRODTLEXISTS);
				view = "favCategoryProducts";
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return view;
	}

	/**
	 * This is a Method used to display the product data Method Type:GET.
	 * 
	 * @param contains
	 *            information about the products.
	 * @return String response as Success or failure.
	 */
	@RequestMapping(value = "/getProductPage.htm", method = RequestMethod.POST)
	public String getProductPage(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map)
	{
		final String methodName = "getProductPage of ShoppingListController";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String searchValue = (String) request.getParameter("searchValue");
		request.setAttribute("searchValue", searchValue);
		String addedTo = (String) request.getParameter("addedTo");
		request.setAttribute("addedTo", addedTo);
		ShoppingListRequest ShoppingList = new ShoppingListRequest();
		map.put("ShoppingList", ShoppingList);
		return "productPage";
	}

	/**
	 * This is a RestEasy WebService Method for adding unassigned product to SL.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be add unassigned product
	 *            to SL.
	 * @return view
	 */
	@RequestMapping(value = "/addUnassignedProduct.htm", method = RequestMethod.POST)
	public String addUnassignedProd(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "addUnassignedProd of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		String view = "";
		ProductDetailsRequest productDetailsRequest = new ProductDetailsRequest();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		ProductDetails productDetails = new ProductDetails();
		productDetailsRequest.setProductName(ShoppingList.getProductName());
		productDetailsRequest.setProductDescription(ShoppingList.getProductDescription());
		productDetailsRequest.setUserId(userId);
		AddRemoveSBProducts addRemoveSBProducts = null;
		ShoppingListDetails shoppingListDetails = null;
		String addedTo = (String) request.getParameter("addedTo");
		productDetailsRequest.setAddedTo(addedTo);
		try
		{
			productDetails = shoppingListService.addUnassigedProd(productDetailsRequest);
			if ("L".equals(addedTo))
			{
				addRemoveSBProducts = shoppingListService.getShopBasketProducts(userId);
			}
			if ("F".equals(addedTo))
			{
				shoppingListDetails = shoppingListService.getMSLCategoryProducts(userId);
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (addRemoveSBProducts != null)
		{
			request.setAttribute("addRemoveSBProducts", addRemoveSBProducts);
			view = "shoppingList";
		}
		if (shoppingListDetails != null)
		{
			request.setAttribute("shoppingListDetails", shoppingListDetails);
			view = "favCategoryProducts";
		}
		return view;

	}

	/**
	 * This is a RestEasy WebService Method for adding product to MSL. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be add product to MSL
	 *            products .
	 * @return response: return the whether updated is successful or not.
	 */
	@RequestMapping(value = "/addProductToSLFav.htm", method = RequestMethod.POST)
	public String addToMainShoppingList(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "addToMainShoppingList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		String view = "";
		int lowerLimit = 0;
		Pagination objPage = null;
		int currentPage = 1;
		String pageNumber = "0";
		String pageFlag = (String) request.getParameter("pageFlag");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
		Integer productId = ShoppingList.getProductId();
		ProductDetails productDetails = new ProductDetails();
		ShoppingListDetails shoppingListDetails = null;
		try
		{
			productDetails = shoppingListService.addProductMainToShopList(userId, productId);

			if (productDetails.getProductIsThere() == 1)
			{
				if (null != pageFlag && "true".equalsIgnoreCase(pageFlag))
				{
					pageNumber = request.getParameter("pageNumber");
					Pagination pageSess = (Pagination) session.getAttribute("pagination");
					if (Integer.valueOf(pageNumber) != 0)
					{
						currentPage = Integer.valueOf(pageNumber);
						int number = Integer.valueOf(currentPage) - 1;
						int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					}
					else
					{
						lowerLimit = 0;
					}

				}
				productDetails = shoppingListService.scanSearchProductForName(ShoppingList, lowerLimit);
				if (null != productDetails)
				{
					objPage = Utility.getPagination(productDetails.getProductDetail().size(), currentPage, "searchShoppingList.htm");

				}
				request.setAttribute("duplicateprodtxt", ApplicationConstants.DUPLICATEFAVPRODUCTTEXT);
				request.setAttribute("productDetails", productDetails);
				LOG.info("Product Exists Already.");
				view = "searchShoppingList";
			}
			else
			{
				shoppingListDetails = shoppingListService.getMSLCategoryProducts(userId);
				request.setAttribute("shoppingListDetails", shoppingListDetails);
				view = "favCategoryProducts";
			}
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return view;

	}

	/**
	 * This method is used to get the details of shoppinglist details Calls
	 * method in service layer. accepts a GET request in mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/addSLProduct", method = RequestMethod.GET)
	public @ResponseBody
	String addSLProduct(@RequestParam(value = "productId", required = true) Integer productId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{

		final String methodName = "addWLProduct of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		String view = null;
		String sResponse = null;
		ProductDetail productDetail = new ProductDetail();
		ShoppingListRequest shoppinglistRequest = new ShoppingListRequest();
		shoppinglistRequest.setSelectedProductId(productId);
		shoppinglistRequest.setUserId(userId);
		try
		{
			productDetail = shoppingListService.addTodaySLProductsBySearch(shoppinglistRequest);

			if (productDetail.getProductIsThere() == 1)
			{
				sResponse = ApplicationConstants.DUPLICATESLPRODUCTTEXT;
			}
			else
			{
				sResponse = ApplicationConstants.PRODUCTADDEDSUCCESSTEXT;
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return sResponse;
	}

	@RequestMapping(value = "/smartsearch", method = RequestMethod.GET)
	public @ResponseBody
	String categorySmartSearch(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException,
			ParseException
	{
		LOG.info("Inside RetailerProfileController : getCities ");
		ArrayList<ProductDetails> products = null;
		String searchStr = request.getParameter("term");
		JSONObject object = new JSONObject();
		JSONObject valueObj = null;
		JSONArray array = new JSONArray();
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
			products = shoppingListService.categorySmartSearch(searchStr);
			response.setContentType("application/json");
			session.setAttribute("smartSearchKey", searchStr);
			if (null != products && !products.isEmpty())
			{
				for (ProductDetails productDetails : products)
				{
					String name = productDetails.getCategs().replace("|~|", " ");
					valueObj = new JSONObject();
					valueObj.put("lable", productDetails.getParCatId());
					valueObj.put("value", name);
					array.put(valueObj);
				}
			}
			object.put("Product", array);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Inside RetailerProfileController : getCities : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return object.get("Product").toString();
	}

	@RequestMapping(value = "/productsmartsearch", method = RequestMethod.POST)
	public @ResponseBody
	String productSmartSearch(@RequestParam(value = "prodId", required = true) String categoryId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException, ParseException
	{
		LOG.info("Inside RetailerProfileController : getCities ");
		ArrayList<ProductDetail> products = null;
		JSONObject object = new JSONObject();
		JSONObject valueObj = null;
		JSONArray array = new JSONArray();
		String searchStr = null;
		try
		{
			searchStr = (String) session.getAttribute("smartSearchKey");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ShoppingListService shoppingListService = (ShoppingListService) appContext.getBean("shoppingList");
			products = shoppingListService.productSmartSearch(searchStr, categoryId);
			response.setContentType("application/json");

			if (null != products && !products.isEmpty())
			{
				for (int i = 0; i < products.size(); i++)
				{
					String productName = products.get(i).getProductName();
					Integer productId = products.get(i).getProductId();
					String imagePath = products.get(i).getProductImagePath();
					// String longDesc =
					// products.get(i).getProductLongDescription();
					valueObj = new JSONObject();
					valueObj.put("productName", productName);
					valueObj.put("productId", productId);
					valueObj.put("imagePath", imagePath);
					// valueObj.put("longDesc", longDesc);
					array.put(valueObj);
				}
			}
			session.removeAttribute("smartSearchKey");
			object.put("Product", array);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Inside RetailerProfileController : getCities : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return object.toString();
	}
}
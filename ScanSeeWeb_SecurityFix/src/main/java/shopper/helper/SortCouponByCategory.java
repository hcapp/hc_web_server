package shopper.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import shopper.service.MygalleryCouponSort;

import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.RetailerDetail;

public class SortCouponByCategory
{

	/**
	 * For displaying coupons by category wise.
	 * 
	 * @param cLRDetailsObj
	 * @return CLRDetails
	 */

	public static CLRDetails getCouponByCategory(CLRDetails cLRDetailsObj)

	{
		int totalSize = cLRDetailsObj.getTotalSize();
		CLRDetails clrDetails = new CLRDetails();
		List<CouponDetails> couponDetailList = cLRDetailsObj.getCouponDetails();

		if (couponDetailList != null && !couponDetailList.isEmpty())
		{
			clrDetails.setNextPage(cLRDetailsObj.getNextPage());
			// clrDetails.setCoupDetails(cLRDetailsObj.getCoupDetails());
		}
		RetailerDetail clrDetailsObj = null;
		final HashMap<String, RetailerDetail> cLRDetailsMap = new HashMap<String, RetailerDetail>();
		List<CouponDetails> coupongrouplst = null;

		String cateName = null;
		for (CouponDetails couponDetail : couponDetailList)
		{
			cateName = couponDetail.getCateName();

			if (cLRDetailsMap.containsKey(cateName))

			{
				clrDetailsObj = cLRDetailsMap.get(cateName);
				coupongrouplst = clrDetailsObj.getCouponDetails();
				if (coupongrouplst != null)
				{
					CouponDetails couponDetailsObj = new CouponDetails();

					couponDetailsObj.setRowNum(couponDetail.getRowNum());
					couponDetailsObj.setCouponId(couponDetail.getCouponId());
					couponDetailsObj.setUserCouponGalleryID(couponDetail.getUserCouponGalleryID());
					couponDetailsObj.setCouponName(couponDetail.getCouponName());
					couponDetailsObj.setCouponDiscountType(couponDetail.getCouponDiscountType());
					couponDetailsObj.setCouponDiscountAmount(couponDetail.getCouponDiscountAmount());
					couponDetailsObj.setCouponDiscountPct(couponDetail.getCouponDiscountPct());
					couponDetailsObj.setCoupDesptn(couponDetail.getCoupDesptn());
					couponDetailsObj.setCouponDateAdded(couponDetail.getCoupDateAdded());
					couponDetailsObj.setCouponStartDate(couponDetail.getCoupStartDate());
					couponDetailsObj.setCouponExpireDate(couponDetail.getCoupExpireDate());
					couponDetailsObj.setCouponURL(couponDetail.getCouponURL());
					couponDetailsObj.setCouponImagePath(couponDetail.getCouponImagePath());
					couponDetailsObj.setFavFlag(couponDetail.getFavFlag());
					couponDetailsObj.setViewableOnWeb(couponDetail.isViewableOnWeb());

					coupongrouplst.add(couponDetailsObj);

					clrDetailsObj.setCouponDetails(coupongrouplst);

				}
			}

			else
			{
				clrDetailsObj = new RetailerDetail();
				clrDetailsObj.setCateId(couponDetail.getCateId());
				clrDetailsObj.setCateName(couponDetail.getCateName());

				coupongrouplst = new ArrayList<CouponDetails>();

				CouponDetails couponDetailsObj = new CouponDetails();
				couponDetailsObj.setCateId(couponDetail.getCateId());
				couponDetailsObj.setRowNum(couponDetail.getRowNum());
				couponDetailsObj.setCouponId(couponDetail.getCouponId());
				couponDetailsObj.setUserCouponGalleryID(couponDetail.getUserCouponGalleryID());
				couponDetailsObj.setCouponName(couponDetail.getCouponName());
				couponDetailsObj.setCouponDiscountType(couponDetail.getCouponDiscountType());
				couponDetailsObj.setCouponDiscountAmount(couponDetail.getCouponDiscountAmount());
				couponDetailsObj.setCouponDiscountPct(couponDetail.getCouponDiscountPct());
				couponDetailsObj.setCoupDesptn(couponDetail.getCoupDesptn());
				couponDetailsObj.setCouponDateAdded(couponDetail.getCoupDateAdded());
				couponDetailsObj.setCouponStartDate(couponDetail.getCoupStartDate());
				couponDetailsObj.setCouponExpireDate(couponDetail.getCoupExpireDate());
				couponDetailsObj.setCouponURL(couponDetail.getCouponURL());
				couponDetailsObj.setCouponImagePath(couponDetail.getCouponImagePath());
				couponDetailsObj.setFavFlag(couponDetail.getFavFlag());
				couponDetailsObj.setViewableOnWeb(couponDetail.isViewableOnWeb());

				coupongrouplst.add(couponDetailsObj);
				clrDetailsObj.setCouponDetails(coupongrouplst);

			}
			cLRDetailsMap.put(cateName, clrDetailsObj);
		}

		final Set<Map.Entry<String, RetailerDetail>> set = cLRDetailsMap.entrySet();

		final ArrayList<RetailerDetail> cLRDetailslist = new ArrayList<RetailerDetail>();

		for (Map.Entry<String, RetailerDetail> entry : set)
		{
			cLRDetailslist.add(entry.getValue());
		}
		// Implemented logic to sort coupon categories alphabetically.
		final MygalleryCouponSort sortCouponCatgories = new MygalleryCouponSort();
		Collections.sort(cLRDetailslist, sortCouponCatgories);
		clrDetails.setLoygrpbyRetlst(cLRDetailslist);
		clrDetails.setTotalSize(totalSize);
		return clrDetails;

	}

}

/**
 * @ (#) PreferencesController.java 23-Dec-2011
 * Project       :ScanSeeWeb
 * File          : PreferencesController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 23-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package shopper.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import shopper.service.ShopperService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Users;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.MainCategoryDetail;
import common.pojo.shopper.SubCategoryDetail;

/**
 * This class is a controller class for shopper preferences.
 * @author Dileep
 *
 */
@Controller
public class PreferencesController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PreferencesController.class);

	/**
	 * This method returns shopper preference categories.
	 * @param categoryDetail
	 * @param request
	 * @param session
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/preferences.htm", method = RequestMethod.GET)
	public String showPage(@ModelAttribute("preferencesform") CategoryDetail categoryDetail, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{
		final String methodName = "showPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		final Users user = (Users) session.getAttribute("loginuser");
		final Long shopperId = Long.valueOf(user.getUserID());
		final Integer lowerlimit = null;		
		Integer mainCtgyCount = 0;
		Integer mainCtgySelCount = 0;
		Integer subCtgyCount = 0;
		Integer subCtgySelCount = 0;
		String catName = null;
		String subCatName = null;
		StringBuffer buffer = null;
		StringBuffer stringBuffer = null;
		StringBuffer finalBuffer = null;
		Integer categoryId;
		Integer favCatId = 0;
		StringBuffer selectedCat = null;
		
		session.setAttribute("ShopperRegMenuTitle", "Preferences");
		
		try
		{
			finalBuffer = new StringBuffer();
			buffer = new StringBuffer();
			selectedCat = new StringBuffer();
			
			if (null != shopperId)
			{
				categoryDetail = shopperService.getUserFavCategories(shopperId, lowerlimit);
				
				for (MainCategoryDetail mainCategoryDetail : categoryDetail.getMainCategoryDetaillst())
				{
					subCtgySelCount = 0;
					subCtgyCount = 0;
					mainCtgyCount++;
					stringBuffer = new StringBuffer();
					catName = mainCategoryDetail.getMainCategoryName();
					
					for (SubCategoryDetail subCategoryDetail : mainCategoryDetail.getSubCategoryDetailLst())
					{
						subCtgyCount++;
						favCatId++;
						subCatName = subCategoryDetail.getSubCategoryName();
						categoryId = subCategoryDetail.getCategoryId();
						stringBuffer.append("<tr name="
										+ catName
										+ " onclick='toggleConfirm();'><td align='center' width='4%' class='wrpWord'>&nbsp;</td><td width='4%' class='wrpWord'>");
						
						if (subCategoryDetail.getDisplayed() == 0)
						{
							stringBuffer.append("<input type='checkbox' id='favCatId" + favCatId + "' value='" + categoryId + "' name='favCatId'/>");
						}
						else
						{
							stringBuffer.append("<input type='checkbox' id='favCatId" + favCatId + "' checked='checked' value='" + categoryId + "' name='favCatId' />");
							subCtgySelCount++;
						}
						
						stringBuffer.append("</td><td width='92%' class='wrpWord'>" + subCatName + "</td>");
					}
					
					if(subCtgyCount.equals(subCtgySelCount))
					{
						buffer.append("<tr name=" + catName + " class='mainCtgry'>" +
								"<td width='4%' align='center' class='wrpWord'>" +
								"<input type='checkbox' name='favCatId' checked='checked'/></td>"+
								"<td colspan='2' width='96%'  class='wrpWord'>" + catName + "</td></tr>");
						mainCtgySelCount++;
						String[] name = {catName} ;
						name = name[0].split("[&|,| ]");
						selectedCat.append(name[0] + ",");
					}
					else
					{
						buffer.append("<tr name=" + catName + " class='mainCtgry'>" +
								"<td width='4%' align='center' class='wrpWord'>" +
								"<input type='checkbox' name='favCatId'/></td>"+
								"<td colspan='2' width='96%'  class='wrpWord'>" + catName + "</td></tr>");
						if(!subCtgySelCount.equals(0))
						{
							String[] name = {catName} ;
							name = name[0].split("[&|,| ]");
							selectedCat.append(name[0] + ",");
						}
					}
					
					buffer.append(stringBuffer);

				}
				
				finalBuffer.append("<thead><tr class='subHdr'><th align='center'>");
				
				if(mainCtgyCount.equals(mainCtgySelCount))
				{
					finalBuffer.append("<input type='checkbox' id='checkAll' name='checkAll' checked='checked'>");					
				}
				else
				{
					finalBuffer.append("<input type='checkbox' id='checkAll' name='checkAll'>");
				}
				
				finalBuffer.append("<label for='checkbox' name='ctgry'></label></th><th align='left' colspan='2'>" +
									"<img width='14' height='13' alt='Preferences' src='../images/prefIconSml.png'/>" +
									"Select Your Preferences</th></tr></thead><tbody>");
				finalBuffer.append(buffer);
				finalBuffer.append("</tbody>");
				selectedCat.append("end");
				session.setAttribute("expandCategory", selectedCat);
				session.setAttribute("preferenceCategories", finalBuffer);				
				session.setAttribute("categoryDetailresponse", categoryDetail);
			}
			
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED  + e);
			e.printStackTrace();
		}
		
		model.addAttribute("preferencesform", categoryDetail);
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "conspreferences";
	}

	/**
	 * This method saves shopper preferred categories.
	 * @param categoryDetail
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 */
	@RequestMapping(value = "/preferences.htm", method = RequestMethod.POST)
	public ModelAndView preferencesSubmit(@ModelAttribute("preferencesform") CategoryDetail categoryDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
	{
		final String methodName = "preferencesSubmit";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		String favId;
		try
		{
			final Users loginUser = (Users) session.getAttribute("loginuser");
			final Long userId = loginUser.getUserID();
			favId = categoryDetail.getFavCatId();
			if (null != favId)
			{
				favId = favId.replace("on,", "");
			}
			shopperService.setUserFavCategories(favId, userId, categoryDetail.isCellPhone(), categoryDetail.isEmail());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			e.printStackTrace();
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(new RedirectView("selectschool.htm"));
	}
}

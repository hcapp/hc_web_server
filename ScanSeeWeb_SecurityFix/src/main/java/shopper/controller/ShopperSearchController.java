package shopper.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.Offer;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;

import shopper.service.ShopperSearchService;
import supplier.controller.AddRebatesController;
import supplier.service.SearchService;

import common.exception.ScanSeeServiceException;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductReview;
import common.tags.Pagination;
import common.util.Utility;



@Controller
public class ShopperSearchController 
{
	ShopperSearchService shopperSearchService;

	@Autowired
	public void setShopperSearchService(ShopperSearchService shopperSearchService)
	{
		this.shopperSearchService = shopperSearchService;
	}

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(AddRebatesController.class);
	/**
	 * @param searchService
	 *            the searchService to set
	 */

	@RequestMapping(value = "/header.htm", method = RequestMethod.POST)
	public ModelAndView searchProduct(@ModelAttribute("shopperProductForm") ProductVO productInfo,BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException
	{

		ServletContext servletContext = request.getSession()
		.getServletContext();
        WebApplicationContext appContext = WebApplicationContextUtils
		.getWebApplicationContext(servletContext);
        ShopperSearchService shopperSearchService = (ShopperSearchService) appContext
		.getBean("shopperSearchService");
		ArrayList<ProductVO> productList = null;
		SearchForm objForm = null;
		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";
		String view = null;
		/*
		 * String zipCode = "91303"; String productName =
		 * "Coolpix S4000 Compact Camera";
		 */

		/*
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 */
		if (null != pageFlag && pageFlag.equals("true"))
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			searchType = objForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}

		if (objForm == null)
		{
			zipCode = (String) request.getParameter("zipCode");
			searhKey = (String) request.getParameter("searchKey");
			searchType = (String) request.getParameter("searchType");
			//LOG.info("UserType****" + loginUser.getUserType());
			if (loginUser != null)
			{
				if (loginUser.getUserType() == 1 || loginUser.getUserType() == 2)
				{
					searchType = "products";
				}

			}
           
			LOG.info("zipCode****" + zipCode);
			LOG.info("searhKey****" + searhKey);
			LOG.info("searchType****" + searchType);
			objForm = new SearchForm(zipCode, searhKey, searchType);
			if (null != searhKey)
			{
				objForm.setSearchKey(searhKey.trim());
			}
			else
			{
				objForm.setSearchKey(searhKey);
			}
			session.setAttribute("searchForm", objForm);
		}

		/*
		 * Pagination check
		 */

		try
		{
			SearchResultInfo resultInfo = shopperSearchService.searchProducts(loginUser, objForm, lowerLimit);
			if (searchType.equals("products"))
			{
				if(resultInfo.getProductList().isEmpty()|| resultInfo.getProductList() == null)
				{
					result.reject("product.error");
					view = "shopperProductSearch";
				}else{
				resultInfo.setSearchType("products");
				view = "shopperProductSearch";
				}
			}
			else if (searchType.equals("deals"))
			{
				if(resultInfo.getProductList().isEmpty()|| resultInfo.getProductList() == null)
				{
					result.reject("product.error");
					view = "shopperProductSearch";
				}else{
				resultInfo.setSearchType("deals");
				view = "shopperProductSearch";
				}
			}
			else if (searchType.equals("producthotdeal"))
			{
				if(resultInfo.getProductList().isEmpty()|| resultInfo.getProductList() == null)
				{
					result.reject("product.error");
					view = "searchProductwithdeals";
				}else{
				resultInfo.setSearchType("producthotdeal");
				view = "searchProductwithdeals";
				}

			}
			session.setAttribute("seacrhList", resultInfo);

			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "header.htm");

			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}

		return new ModelAndView(view);
	}

	
	
	
	@RequestMapping(value = "/productDetail.htm", method = RequestMethod.POST)
	public ModelAndView productInfo(@ModelAttribute("productinfoform")ProductVO productinfo,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws IOException ,ScanSeeServiceException{
		
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		SearchService searchService = (SearchService) appContext
				.getBean("searchService");
		FindNearByDetails findNearByDetails = null;
		ProductVO productInfoDetail = null;
		SearchForm objForm = null;
		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		ExternalAPIInformation apiInfo = null;
		ProductDetail productDet = null;
		OnlineStoresRequest onlineStores = null;
		OnlineStoresService onlineStoresService = null;
		OnlineStores onlineStoresinfo = null;
		List<Offer> onlineStoresList = null;
		String review = null;
		ArrayList<ProductReview> productReviewslist = null;
	try {
			Integer productID = productinfo.getProductID();
			Integer retailLocationID = productinfo.getRetailLocationID();
			LOG.info("productID*************"+productID);
			LOG.info("retailLocationID++++++++++++++++++"+retailLocationID);
			productInfoDetail = searchService.getAllProductInfo(retailLocationID, productID);
			productReviewslist = searchService.getProductReviews(productID);
			
			Users loginUser = (Users) session.getAttribute("loginuser");
			objForm = (SearchForm) session.getAttribute("searchForm");
			objAreaInfoVO.setProductId(productID);
			if(loginUser != null){
		   		
				objAreaInfoVO.setZipCode(loginUser.getPostalCode());
				objAreaInfoVO.setUserID(loginUser.getUserID());
			}else{
			
			objAreaInfoVO.setZipCode(objForm.getZipCode());
			}
			
			//find near by stores
			findNearByDetails = searchService.findNearBy(objAreaInfoVO);
			
			if (findNearByDetails != null)
			{
				request.setAttribute("findNearByDetails", findNearByDetails.getFindNearByDetail().get(0));
			}
			// online stores
		  if(loginUser != null){
			apiInfo = searchService.externalApiInfo("FindOnlineStores");
            productDet = searchService.fetchProductDetails(productID.toString());
			onlineStoresService = new OnlineStoresServiceImpl();
			onlineStores = new OnlineStoresRequest();
			onlineStores.setPageStart("0");
			onlineStores.setUpcCode(productDet.getScanCode());
		    onlineStores.setUserId(loginUser.getUserID().toString());
			onlineStores.setValues();
			onlineStoresinfo = onlineStoresService.onlineStoresInfoEmailTemplate(apiInfo, onlineStores);
			
			if (onlineStoresinfo != null)
			{
				if (!onlineStoresinfo.getOffers().isEmpty())
				{
					onlineStoresList = onlineStoresinfo.getOffers();
					
					request.setAttribute("OnlineStoresInfo", onlineStoresList.get(0));

				
				}

			  }
		   }
			
			
				
			
		} catch (ScanSeeServiceException e) {
			
			LOG.error("Exception occurred in productInfo::::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}
		
		if(productReviewslist !=null && !productReviewslist.isEmpty()){
	    		
			
			ProductReview productReview =  productReviewslist.get(0);
			
			
			request.setAttribute("productReviewslist",productReview);
			
     	}
		session.setAttribute("productinfo", productInfoDetail);
		request.setAttribute("searchForm", objForm);
		
		return new ModelAndView("productdetails");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*@RequestMapping(value = "/searchproductwithdeal.htm", method = RequestMethod.POST)
	public ModelAndView searchProductWithDeals(@ModelAttribute("shopperProductForm") ProductVO productInfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException
	{


		ServletContext servletContext = request.getSession()
		.getServletContext();
        WebApplicationContext appContext = WebApplicationContextUtils
		.getWebApplicationContext(servletContext);
        ShopperSearchService shopperSearchService = (ShopperSearchService) appContext
		.getBean("shopperSearchService");

		SearchForm objForm = null;
		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";
		String view = null;
		
		 * String zipCode = "91303"; String productName =
		 * "Coolpix S4000 Compact Camera";
		 

		
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 
		if (null != pageFlag && pageFlag.equals("true"))
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			searchType = objForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}
		
		

		if (objForm == null)
		{
			 objForm = (SearchForm) session.getAttribute("searchForm");
			
			zipCode = objForm.getZipCode();
			searhKey = objForm.getSearchKey();
			searchType = objForm.getSearchType();
			if (loginUser != null)
			{
				if (loginUser.getUserType() == 1 || loginUser.getUserType() == 2)
				{
					searchType = "products";
				}

			}
			
			

			objForm = new SearchForm(zipCode, searhKey, searchType);
			session.setAttribute("searchForm", objForm);
		}

		
		 * Pagination check
		 

		try
		{
			SearchResultInfo resultInfo = shopperSearchService.searchProducts(loginUser, objForm, lowerLimit);

			resultInfo.setSearchType("producthotdeal");
			view = "searchProductwithdeals";

			session.setAttribute("seacrhList", resultInfo);

			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "searchproductwithdeal.htm");

			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}

		return new ModelAndView(view);
	}

	
	
	@RequestMapping(value = "/searchdeal.htm", method = RequestMethod.POST)
	public ModelAndView searchDeals(@ModelAttribute("shopperProductForm") ProductVO productInfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException
	{


		ServletContext servletContext = request.getSession()
		.getServletContext();
        WebApplicationContext appContext = WebApplicationContextUtils
		.getWebApplicationContext(servletContext);
        ShopperSearchService searchService = (ShopperSearchService) appContext
		.getBean("shopperSearchService");

		SearchForm objForm = null;
		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";
		String view = null;
		
		 * String zipCode = "91303"; String productName =
		 * "Coolpix S4000 Compact Camera";
		 

		
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 
		if (null != pageFlag && pageFlag.equals("true"))
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			searchType = objForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}

		if (objForm == null)
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			zipCode = objForm.getZipCode();
			searhKey = objForm.getSearchKey();
			searchType = objForm.getSearchType();
			if (loginUser != null)
			{
				if (loginUser.getUserType() == 1 || loginUser.getUserType() == 2)
				{
					searchType = "products";
				}

			}
			LOG.info("UserType****" + loginUser.getUserType());
			LOG.info("zipCode****" + zipCode);
			LOG.info("searhKey****" + searhKey);
			LOG.info("searchType****" + searchType);
			
			objForm = new SearchForm(zipCode, searhKey, searchType);
			session.setAttribute("searchForm", objForm);
		}

		
		 * Pagination check
		 

		try
		{
			SearchResultInfo resultInfo = searchService.searchDeals(loginUser, objForm, lowerLimit);

			resultInfo.setSearchType("hotdeal");
			view = "searchProductwithdeals";

			session.setAttribute("seacrhList", resultInfo);

			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "searchdeal.htm");

			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}

		return new ModelAndView(view);
	}
*/
}

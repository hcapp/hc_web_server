package shopper.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import shopper.service.CommonService;
import shopper.service.ShopperService;

import common.constatns.ApplicationConstants;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.UserTrackingData;

@Controller
public class TwitterController
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(TwitterController.class);

	private String TINYAPIURL = "http://tinyurl.com/api-create.php?url=";

	@RequestMapping(value = "/twitterShareProductInfo.htm", method = RequestMethod.GET)
	@ResponseBody
	public String twitterShareProductInfo(@RequestParam("productId") Integer productId, @RequestParam("productListId") Integer productListId,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception
	{
		String shareURL = null;
		String prodName = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		List<UserTrackingData> lstUserTrackingDatas = null;
		ProductDetail productDetailObj = null;

		if (productId != null)
		{
			productDetailObj = new ProductDetail();
			productDetailObj.setProductId(productId);
		}

		lstUserTrackingDatas = commonServiceObj.getConsumerShareTypes();

		for (UserTrackingData userTrackingData : lstUserTrackingDatas)
		{
			if (userTrackingData.getShrTypNam().equals(ApplicationConstants.PRODTWITTERSHARETYPETEXT))
			{
				productDetailObj.setShareTypeID(userTrackingData.getShrTypID());
			}

		}
		commonServiceObj.saveConsShareProdDetails(productDetailObj);
		ProductDetail prdDetail = commonServiceObj.fetchConsumerProductDetails(productDetailObj);

		shareURL = commonServiceObj.getConsShareEmailConfiguration();
		session.removeAttribute("tweetterShareURL");
		session.removeAttribute("twtProdName");
		/*
		 * Users loginUser = (Users) session.getAttribute("loginuser"); int
		 * userId = 0; if (loginUser != null) { if (loginUser.getUserID() !=
		 * null) { userId = loginUser.getUserID().intValue(); decryptedUserId =
		 * Long.toString(userId); } } else { decryptedUserId =
		 * Long.toString(userId); }
		 */
		// String key = decryptedUserId + "/" + productId;
		// EncryptDecryptPwd enryptDecryptpwd = null;
		// enryptDecryptpwd = new EncryptDecryptPwd();
		// productKey = enryptDecryptpwd.encrypt(key);
		shareURL = shareURL + "key1=" + productId + "&key2=" + productListId + "&share=yes";
		if (prdDetail != null)
		{
			prodName = prdDetail.getProductName();
			session.setAttribute("twtProdName", prodName);
		}
		session.setAttribute("tweetterShareURL", shareURL);

		// Using tinyurl to create shortten product share link
		String tinyUrl = TINYAPIURL;
		String tinyUrlLookup = TINYAPIURL + shareURL;
		BufferedReader reader;
		String strShorturl = null;
		try
		{
			reader = new BufferedReader(new InputStreamReader(new URL(tinyUrlLookup).openStream()));
			tinyUrl = reader.readLine();
		}
		catch (MalformedURLException e)
		{
			LOG.error("Error occureed in ConsFindHome", e.getMessage());
		}
		catch (IOException e)
		{
			LOG.error("Error occureed in ConsFindHome", e.getMessage());
		}
		strShorturl = tinyUrl + ";" + prodName;
		return strShorturl;
	}
}

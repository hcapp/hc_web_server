package shopper.controller;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.find.service.FindService;
import shopper.service.CommonService;
import shopper.service.MyGalleryService;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.State;
import common.pojo.Users;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CategoryInfo;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.ShareProductInfo;
import common.tags.Pagination;
import common.util.Utility;
/**
 * This class is a controller class for my gallery.
 * 
 * @author manjunatha_gh
 * 
 */
@Controller
public class ConsumerGalleryController
{
	/**
	 * getting instance logger.
	 */
	public static final Logger LOG = LoggerFactory.getLogger(ConsumerGalleryController.class);

	/**
	 * getCouponGallery is a method for displaying user's coupons based on the
	 * category .
	 * 
	 * @param clrForm
	 *            contains retailerId,categoryID and searchKey.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String that contains view name to be displayed.
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/consmygallery.htm", method = RequestMethod.GET)
	public String getCouponGallery(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getCouponGallery";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ConsumerRequestDetails consumerRequestDetails = null;
		ArrayList<CategoryInfo> categories = null;
		final String pageFlag = (String) request.getParameter("pageFlag");
		int recordCount = 15;
		int currentPage = 1;
		String pageNumber = "0";
		Pagination objPage = null;
		int lowerLimit = 0;
		String consumerType = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			Integer limit = (Integer) session.getAttribute("limit");

			if (null != loginUser)
			{
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}
			consumerRequestDetails = new ConsumerRequestDetails(ApplicationConstants.GALLERYMODULE, consumerType);
			session.setAttribute("consumerReqObj", consumerRequestDetails);

			if (null != clrForm.getRecordCount() && !"".equals(clrForm.getRecordCount()) && !"undefined".equals(clrForm.getRecordCount()))
			{
				recordCount = clrForm.getRecordCount();
			}

			if (null != clrForm.getLowerLimit())
			{
				lowerLimit = clrForm.getLowerLimit();
			}
			if(null != limit)
			{
				lowerLimit = limit;
			}
			if (null == loginUser)
			{
				if (null != pageFlag && "true".equals(pageFlag))
				{
					pageNumber = request.getParameter("pageNumber");
					final Pagination pageSess = (Pagination) session.getAttribute("pagination");
					if (Integer.valueOf(pageNumber) != 0)
					{
						currentPage = Integer.valueOf(pageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						final int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					}
				}
				else
				{
					currentPage = (lowerLimit + recordCount)/recordCount;
				}
				final CLRDetails clrDetails = new CLRDetails();
				clrDetails.setRetailerId(clrForm.getRetailerId());
				clrDetails.setCategoryID(clrForm.getCategoryID());
				clrDetails.setSearchKey(clrForm.getSearchKey());
				clrDetails.setLowerLimit(lowerLimit);
				clrDetails.setMainMenuId(null);
				clrDetails.setRecordCount(recordCount);
				final CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "ALL");
				if (null != clrResult)
				{
					if (null != clrResult.getLoygrpbyRetlst())
					{
						for (RetailerDetail details : clrResult.getLoygrpbyRetlst())
						{
							for (CouponDetails couponDetails : details.getCouponDetails())
							{
								couponDetails.setTitleText(couponDetails.getCouponName());
								couponDetails.setCouponName(couponDetails.getCouponName().replace("'", "\\'"));
							}
						}
					}
					objPage = Utility.getPagination(clrResult.getTotalSize(), currentPage, "consmygallery.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					clrResult.setLowerLimit(lowerLimit);
					request.setAttribute("allGalleryResult", clrResult);
					session.removeAttribute("limit");
				}
				model.put("clrForm", clrForm);
				LOG.info(ApplicationConstants.METHODEND + methodName);
				return "consgalleryallcoupons";
			}
			else
			{
				if (null != pageFlag && "true".equals(pageFlag))
				{
					pageNumber = request.getParameter("pageNumber");
					final Pagination pageSess = (Pagination) session.getAttribute("pagination");
					if (Integer.valueOf(pageNumber) != 0)
					{
						currentPage = Integer.valueOf(pageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						final int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					}
				}
				else
				{
					currentPage = (lowerLimit + 15)/15;
				}
				final CLRDetails clrDetails = new CLRDetails();
				clrDetails.setRetailerId(clrForm.getRetailerId());
				clrDetails.setCategoryID(clrForm.getCategoryID());
				clrDetails.setSearchKey(clrForm.getSearchKey());
				clrDetails.setLowerLimit(lowerLimit);
				clrDetails.setUserId(Utility.longToInteger(loginUser.getUserID()));
				clrDetails.setRecordCount(recordCount);
				final CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "MYGALLERY");
				if (null != clrResult)
				{
					if (null != clrResult.getLoygrpbyRetlst())
					{
						for (RetailerDetail details : clrResult.getLoygrpbyRetlst())
						{
							for (CouponDetails couponDetails : details.getCouponDetails())
							{
								couponDetails.setTitleText(couponDetails.getCouponName());
								couponDetails.setCouponName(couponDetails.getCouponName().replace("'", "\\'"));
							}
						}
					}
					objPage = Utility.getPagination(clrResult.getTotalSize(), currentPage, "consmygallery.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					categories = galleryService.getCategories();
					clrResult.setLowerLimit(lowerLimit);
					request.setAttribute("galleryResult", clrResult);
					request.setAttribute("", clrResult.getLoygrpbyRetlst());
					session.setAttribute("categories", categories);
					session.removeAttribute("limit");
				}
				model.put("clrForm", clrForm);
			}
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "consmygallery";

	}

	/**
	 * getGalleryUsed is a method for displaying Used coupons based on the
	 * category.
	 * 
	 * @param clrForm
	 *            contains retailerId,categoryID and searchKey.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String that contains view name to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/consgalleryused.htm", method = RequestMethod.GET)
	public String getGalleryUsed(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getGalleryUsed";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final String pageFlag = (String) request.getParameter("pageFlag");
		int currentPage = 1;
		String pageNumber = "0";
		Pagination objPage = null;
		int lowerLimit = 0;
		ConsumerRequestDetails consumerRequestDetails = null;
		String consumerType = null;
		int recordCount = 15;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			Integer limit = (Integer) session.getAttribute("limit");

			if (null != loginUser)
			{
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}
			consumerRequestDetails = new ConsumerRequestDetails(ApplicationConstants.GALLERYMODULE, consumerType);
			session.setAttribute("consumerReqObj", consumerRequestDetails);

			if (null != clrForm.getRecordCount() && !"".equals(clrForm.getRecordCount()) && !"undefined".equals(clrForm.getRecordCount()))
			{
				recordCount = clrForm.getRecordCount();
			}

			if (null != clrForm.getLowerLimit())
			{
				lowerLimit = clrForm.getLowerLimit();
			}
			if(null != limit)
			{
				lowerLimit = limit;
			}
			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}
			else
			{
				currentPage = (lowerLimit + recordCount)/recordCount;
			}
			final CLRDetails clrDetails = new CLRDetails();
			clrDetails.setRetailerId(clrForm.getRetailerId());
			clrDetails.setCategoryID(clrForm.getCategoryID());
			clrDetails.setSearchKey(clrForm.getSearchKey());
			clrDetails.setLowerLimit(lowerLimit);
			clrDetails.setUserId(Utility.longToInteger(loginUser.getUserID()));
			clrDetails.setRecordCount(recordCount);
			final CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "USED");
			if (null != clrResult)
			{
				if (null != clrResult.getLoygrpbyRetlst())
				{
					for (RetailerDetail details : clrResult.getLoygrpbyRetlst())
					{
						for (CouponDetails couponDetails : details.getCouponDetails())
						{
							couponDetails.setTitleText(couponDetails.getCouponName());
							couponDetails.setCouponName(couponDetails.getCouponName().replace("'", "\\'"));
						}
					}
				}
				objPage = Utility.getPagination(clrResult.getTotalSize(), currentPage, "consgalleryused.htm", recordCount);
				session.setAttribute("pagination", objPage);
				clrResult.setLowerLimit(lowerLimit);
				request.setAttribute("usedGalleryResult", clrResult);
				session.removeAttribute("limit");
			}
			model.put("clrForm", clrForm);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "consgalleryused";

	}

	/**
	 * getGalleryExpired is a method for displaying Expired coupons based on the
	 * category.
	 * 
	 * @param clrForm
	 *            contains retailerId,categoryID and searchKey.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String that contains view name to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/consgalleryexp.htm", method = RequestMethod.GET)
	public String getGalleryExpired(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getGalleryExpired";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final String pageFlag = (String) request.getParameter("pageFlag");
		int currentPage = 1;
		int lowerLimit = 0;
		String pageNumber = "0";
		Pagination objPage = null;
		ConsumerRequestDetails consumerRequestDetails = null;
		String consumerType = null;
		int recordCount = 15;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");

			if (null != loginUser)
			{
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}
			consumerRequestDetails = new ConsumerRequestDetails(ApplicationConstants.GALLERYMODULE, consumerType);
			session.setAttribute("consumerReqObj", consumerRequestDetails);

			if (null != clrForm.getRecordCount() && !"".equals(clrForm.getRecordCount()) && !"undefined".equals(clrForm.getRecordCount()))
			{
				recordCount = clrForm.getRecordCount();
			}
			
			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}
			else
			{
				currentPage = (lowerLimit + recordCount)/recordCount;
			}
			final CLRDetails clrDetails = new CLRDetails();
			clrDetails.setRetailerId(clrForm.getRetailerId());
			clrDetails.setCategoryID(clrForm.getCategoryID());
			clrDetails.setSearchKey(clrForm.getSearchKey());
			clrDetails.setLowerLimit(lowerLimit);
			clrDetails.setUserId(Utility.longToInteger(loginUser.getUserID()));
			clrDetails.setRecordCount(recordCount);
			final CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "EXPIRED");
			if (null != clrResult)
			{
				if (null != clrResult.getLoygrpbyRetlst())
				{
					for (RetailerDetail details : clrResult.getLoygrpbyRetlst())
					{
						for (CouponDetails couponDetails : details.getCouponDetails())
						{
							couponDetails.setTitleText(couponDetails.getCouponName());
							couponDetails.setCouponName(couponDetails.getCouponName().replace("'", "\\'"));
						}
					}
				}
				objPage = Utility.getPagination(clrResult.getTotalSize(), currentPage, "consgalleryexp.htm", recordCount);
				session.setAttribute("pagination", objPage);
				clrResult.setLowerLimit(0);
				request.setAttribute("expGalleryResult", clrResult);
			}
			model.put("clrForm", clrForm);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "consgalleryexpired";

	}

	/**
	 * getGalleryAll is a method for displaying All coupons based on the
	 * category.
	 * 
	 * @param clrForm
	 *            contains retailerId,categoryID and searchKey.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String that contains view name to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/consgalleryall.htm", method = RequestMethod.GET)
	public String getGalleryAll(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request, HttpSession session,
			ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getGalleryAll";
		final String pageFlag = (String) request.getParameter("pageFlag");
		int currentPage = 1;
		String pageNumber = "0";
		Pagination objPage = null;
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer lowerLimit = 0;
		final String latitute = null;
		final String longitude = null;
		ConsumerRequestDetails consumerRequestDetails = null;
		String consumerType = null;
		int recordCount = 15;
		int mainMenuId = 0;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final FindService findService = (FindService) appContext.getBean("findService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			Integer limit = (Integer) session.getAttribute("limit");
			if (null != loginUser)
			{
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}

			if (null != clrForm.getRecordCount() && !"".equals(clrForm.getRecordCount()) && !"undefined".equals(clrForm.getRecordCount()))
			{
				recordCount = clrForm.getRecordCount();
			}

			if (null != clrForm.getLowerLimit())
			{
				lowerLimit = clrForm.getLowerLimit();
			}
			if(null != limit)
			{
				lowerLimit = limit;
			}
			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			}
			else
			{
				currentPage = (lowerLimit + recordCount)/recordCount;
			}
			final CLRDetails clrDetails = new CLRDetails();
			clrDetails.setRetailerId(clrForm.getRetailerId());
			clrDetails.setCategoryID(clrForm.getCategoryID());
			clrDetails.setSearchKey(clrForm.getSearchKey());
			clrDetails.setLowerLimit(lowerLimit);
			clrDetails.setRecordCount(recordCount);
			clrDetails.setUserId(Utility.longToInteger(loginUser.getUserID()));
			clrDetails.setRecordCount(recordCount);

			consumerRequestDetails = new ConsumerRequestDetails(loginUser.getUserID(), latitute, longitude, loginUser.getPostalCode(),
					ApplicationConstants.COUPONMODULE);
			mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			consumerRequestDetails = new ConsumerRequestDetails(ApplicationConstants.GALLERYMODULE, consumerType);
			session.setAttribute("consumerReqObj", consumerRequestDetails);
			clrDetails.setMainMenuId(mainMenuId);
			final CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "ALL");
			if (null != clrResult)
			{
				if (null != clrResult.getLoygrpbyRetlst())
				{
					for (RetailerDetail details : clrResult.getLoygrpbyRetlst())
					{
						for (CouponDetails couponDetails : details.getCouponDetails())
						{
							couponDetails.setTitleText(couponDetails.getCouponName());
							couponDetails.setCouponName(couponDetails.getCouponName().replace("'", "\\'"));
						}
					}
				}
				objPage = Utility.getPagination(clrResult.getTotalSize(), currentPage, "consgalleryall.htm", recordCount);
				session.setAttribute("pagination", objPage);
				clrResult.setLowerLimit(lowerLimit);
				request.setAttribute("allGalleryResult", clrResult);
				session.removeAttribute("limit");
			}
			model.put("clrForm", clrForm);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "consgalleryall";

	}

	/**
	 * getCouponDetails is a method for displaying coupon details.
	 * 
	 * @param clrForm
	 *            contains clrType,requestType.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String that contains view name to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/conscoupondetails.htm", method = RequestMethod.POST)
	public String getCouponDetails(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "getCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		Integer lowerLimit = clrForm.getLowerLimit();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			if (null == clrForm.getCouponId())
			{
				clrForm = (CLRDetails) session.getAttribute("CouponDetails");
			}
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			clrForm.setUserId(Utility.longToInteger(loginUser.getUserID()));
			final String clrType = clrForm.getClrType();
			final String requestType = clrForm.getRequestType();
			final String addedFlag = request.getParameter("added");
			final String imagePath = clrForm.getClrImagePath();
			final boolean viewOnWeb = clrForm.isViewableOnWeb();
			ArrayList<State> states = null;
			String stateName = null;
			if (null != clrType)
			{
				final CLRDetails clrDetails = galleryService.getCLRInfoInfo(clrForm, "C");
				if (null != clrDetails.getCoupDetails().getCouponInfo().getState()
						&& !clrDetails.getCoupDetails().getCouponInfo().getState().isEmpty())
				{
					states = supplierService.getState(clrDetails.getCoupDetails().getCouponInfo().getState());
					for (State state : states)
					{
						stateName = state.getStateName();
					}
					clrDetails.getCoupDetails().getCouponInfo().setState(stateName);
				}
				LOG.info("details of the coupon");
				final CouponDetails coupDetails = clrDetails.getCoupDetails().getCouponInfo();
				coupDetails.setViewableOnWeb(viewOnWeb);
				coupDetails.setCouponImagePath(imagePath);
				request.setAttribute("couponType", clrDetails.getClrType());
				request.setAttribute("coupondetails", clrDetails.getCoupDetails());
				viewName = "conscoupondetails";
				request.setAttribute("requestType", requestType);
				request.setAttribute("added", addedFlag);
				request.setAttribute("lowerLimit", lowerLimit);
			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}


	/**
	 * getCouponDetails is a method for displaying coupon details.
	 * 
	 * @param clrForm
	 *            contains clrType,requestType.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String that contains view name to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/conscoupondetails.htm", method = RequestMethod.GET)
	public String getfbCouponDetails(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "getCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		Integer lowerLimit = clrForm.getLowerLimit();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			if (null == clrForm.getCouponId())
			{
				clrForm = (CLRDetails) session.getAttribute("CouponDetails");
			}
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			clrForm.setUserId(Utility.longToInteger(loginUser.getUserID()));
			final String clrType = clrForm.getClrType();
			final String requestType = clrForm.getRequestType();
			final String addedFlag = request.getParameter("added");
			final String imagePath = clrForm.getClrImagePath();
			final boolean viewOnWeb = clrForm.isViewableOnWeb();
			ArrayList<State> states = null;
			String stateName = null;
			if (null != clrType)
			{
				final CLRDetails clrDetails = galleryService.getCLRInfoInfo(clrForm, "C");
				if (null != clrDetails.getCoupDetails().getCouponInfo().getState()
						&& !clrDetails.getCoupDetails().getCouponInfo().getState().isEmpty())
				{
					states = supplierService.getState(clrDetails.getCoupDetails().getCouponInfo().getState());
					for (State state : states)
					{
						stateName = state.getStateName();
					}
					clrDetails.getCoupDetails().getCouponInfo().setState(stateName);
				}
				LOG.info("details of the coupon");
				final CouponDetails coupDetails = clrDetails.getCoupDetails().getCouponInfo();
				coupDetails.setViewableOnWeb(viewOnWeb);
				coupDetails.setCouponImagePath(imagePath);
				request.setAttribute("couponType", clrDetails.getClrType());
				request.setAttribute("coupondetails", clrDetails.getCoupDetails());
				viewName = "conscoupondetails";
				request.setAttribute("requestType", requestType);
				request.setAttribute("added", addedFlag);
				request.setAttribute("lowerLimit", lowerLimit);
			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}
	/**
	 * addCoupon is a method for adding the coupon from allCoupons to myCoupons.
	 * It adds the coupon only if it is not used.
	 * 
	 * @param couponId
	 *            contains the id of the coupon to be added.
	 * @param usage
	 *            contains the information whether the coupon is used or not.
	 * @param from
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/consaddcoupon.htm", method = RequestMethod.GET)
	public @ResponseBody
	String addCoupon(@RequestParam(value = "couponIds", required = true) StringBuffer couponId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "addCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String daoResponse = null;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");

			final Users loginUser = (Users) session.getAttribute("loginuser");

			final Long userId = loginUser.getUserID();

			daoResponse = galleryService.couponAdd(userId, couponId);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.ERROROCCURRED + methodName);
			throw e;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return daoResponse;
	}

	/**
	 * userRedeemCLR is a method for marking CLR as used(redeeming CLR). It is
	 * done after clipping the coupon.
	 * 
	 * @param clrForm
	 *            contains clrType,couponId,rebateId and layaltyId.
	 * @param result
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	@ResponseBody
	@RequestMapping(value = "/consredeemcoupon.htm", method = RequestMethod.GET)
	public String userRedeemCoupon(@RequestParam(value = "couponIds", required = true) StringBuffer ids, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "userRedeemCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String daoResponse = null;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");

			final Long userId = loginUser.getUserID();

			if (userId != null)
			{
				daoResponse = galleryService.userRedeemCoupon(userId, ids);
			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return daoResponse;
	}

	/**
	 * deleteCLR is a method for deleting CLR from myCoupon and allCoupons.
	 * 
	 * @param ids
	 *            contains coupon id to be deleted.
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/consdeletecoupons.htm", method = RequestMethod.GET)
	public @ResponseBody
	String deleteCoupons(@RequestParam(value = "couponIds", required = true) StringBuffer ids, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "deleteCLR";
		String daoResponse = null;
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final Users loginUser = (Users) session.getAttribute("loginuser");
			final Long userId = loginUser.getUserID();

			if (userId != null && ids != null)
			{
				daoResponse = galleryService.couponRemove(userId, ids);
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.ERROROCCURRED + methodName);
			throw e;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

	/**
	 * sendMail is a method for sending CLR via email.
	 * 
	 * @param id
	 *            contains couponId or layaltyId or rebateId.
	 * @param clrType
	 *            contain the CLR type
	 * @param toEmailId
	 *            contains receipt email id.
	 * @param fromEmailId
	 *            contains sender email id.
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws
	 */

	@RequestMapping(value = "/conssendmail", method = RequestMethod.GET)
	public @ResponseBody
	String sendMail(@RequestParam(value = "clrId", required = true) Integer id, @RequestParam(value = "clrType", required = true) String clrType,
			@RequestParam(value = "toEmailId", required = true) String toEmailId,
			@RequestParam(value = "fromEmailId", required = true) String fromEmailId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException

	{
		final String methodName = "sendMail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String daoResponse = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			final ShareProductInfo shareProductInfoObj = new ShareProductInfo();
			if (null != loginUser)
			{
				shareProductInfoObj.setUserId(loginUser.getUserID().intValue());
			}
			shareProductInfoObj.setToEmail(toEmailId);
			shareProductInfoObj.setFromEmail(fromEmailId);

			if (id != null)
			{

				if (clrType.equalsIgnoreCase("coupon"))

				{

					shareProductInfoObj.setCouponId(id);

					daoResponse = galleryService.shareCLRbyEmail(shareProductInfoObj);

				}
				else if (clrType.equalsIgnoreCase("loyality"))
				{
					shareProductInfoObj.setLoyaltyId(id);
					daoResponse = galleryService.shareCLRbyEmail(shareProductInfoObj);

				}
				else if (clrType.equalsIgnoreCase("rebate"))
				{

					shareProductInfoObj.setRebateId(id);
					daoResponse = galleryService.shareCLRbyEmail(shareProductInfoObj);

				}

			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error("Error occureed in sendMail", exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

	/**
	 * getCouponDetails is a method for displaying coupon details.
	 * 
	 * @param clrForm
	 *            contains clrType,requestType.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String returns viewName to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/consgallerycoupondetails.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public String getGalleryCouponDetails(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "getGalleryCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		Integer lowerLimit = clrForm.getLowerLimit();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			if (null == clrForm.getCouponId())
			{
				clrForm = (CLRDetails) session.getAttribute("CouponDetails");
			}
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			if (null != loginUser)
			{
				clrForm.setUserId(Utility.longToInteger(loginUser.getUserID()));
			}
			final String clrType = clrForm.getClrType();
			final String requestType = clrForm.getRequestType();
			final String addedFlag = request.getParameter("added");
			final String imagePath = clrForm.getClrImagePath();
			final boolean viewOnWeb = clrForm.isViewableOnWeb();
			ArrayList<State> states = null;
			String stateName = null;
			if (null != clrType)
			{
				final CLRDetails clrDetails = galleryService.getCLRInfoInfo(clrForm, "C");
				if (null != clrDetails.getCoupDetails().getCouponInfo().getState()
						&& !clrDetails.getCoupDetails().getCouponInfo().getState().isEmpty())
				{
					states = supplierService.getState(clrDetails.getCoupDetails().getCouponInfo().getState());
					for (State state : states)
					{
						stateName = state.getStateName();
					}
					clrDetails.getCoupDetails().getCouponInfo().setState(stateName);
				}
				LOG.info("details of the coupon");
				final CouponDetails coupDetails = clrDetails.getCoupDetails().getCouponInfo();
				coupDetails.setViewableOnWeb(viewOnWeb);
				coupDetails.setCouponImagePath(imagePath);
				request.setAttribute("couponType", clrDetails.getClrType());
				request.setAttribute("coupondetails", clrDetails.getCoupDetails());
				viewName = "consgallerycoupondetails";
				request.setAttribute("requestType", requestType);
				request.setAttribute("added", addedFlag);
				request.setAttribute("lowerLimit", lowerLimit);
			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}
	
	
	@RequestMapping(value = "/conscoupprod.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView getConsCoupAssocitedProds(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String strMethodName = "getConsCoupAssocitedProds";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String viewName = "conscoupprod";
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
			
			
			final ProductDetails productDetailsObj= commonServiceObj.getConsCoupAssocitedProds(clrForm);
			
			if(null != productDetailsObj)
			{
				session.setAttribute("conscoupprod", productDetailsObj);
				request.setAttribute("couponType", clrForm.getClrType());
				request.setAttribute("couponName", clrForm.getCouponName());
			}
			
		}
		
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + strMethodName);
			
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(viewName);
	}
}

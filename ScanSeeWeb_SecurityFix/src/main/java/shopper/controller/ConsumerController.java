package shopper.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.Users;
import common.pojo.shopper.GoogleCategory;

import shopper.controller.ShopperDashboardController;
import shopper.find.service.FindService;

/**
 * This class is used for consumer home page controller.
 * 
 * @author shyamsundara_hm
 */
@Controller
public class ConsumerController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ShopperDashboardController.class);

	/**
	 * This method is used to navigate to consumer home page.
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 */

	@RequestMapping(value = "/conshome.htm", method = RequestMethod.GET)
	public String showConsumerHome(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Entered in showConsumerHome Get Method....");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		FindService findService = (FindService) appContext.getBean("findService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		ConsumerRequestDetails consumerRequestDetails = new ConsumerRequestDetails();
		if (null == loginUser)
		{

			consumerRequestDetails.setConsumerType(ApplicationConstants.GUESTUSER);
		}
		else
		{

			consumerRequestDetails.setConsumerType(ApplicationConstants.LOGINUSER);
		}

		consumerRequestDetails.setModuleName(ApplicationConstants.HOMEMODULE);
		session.setAttribute("consumerReqObj", consumerRequestDetails);
		List<GoogleCategory> categoryInfo = null;
		Long userId = (long) 0;
		categoryInfo = findService.getCategoryInfo(userId);
		session.setAttribute("categoryInfo", categoryInfo);
		return "conshome";
	}

	@RequestMapping(value = "/activatesession.htm", method = RequestMethod.GET)
	@ResponseBody
	public final void activateSession() throws ScanSeeServiceException
	{
		LOG.info("Inside activateSession");

		LOG.info("Exit activateSession");
	}
}

/**
 * @ (#) SelectSchoolController.java 23-Dec-2011
 * Project       :ScanSeeWeb
 * File          : SelectSchoolController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 23-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package shopper.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import shopper.service.ShopperService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.NonProfitPartnerInfo;
import common.pojo.SchoolInfo;
import common.pojo.Users;

/**
 * This class is a controller class for shopper school selection.
 * @author Dileep
 *
 */
@Controller
public class SelectSchoolController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SelectSchoolController.class);

	/**
	 * This method returns shopper school select page.
	 * @param schoolInfo
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 */
	@RequestMapping(value = "/selectschool.htm", method = RequestMethod.GET)
	public ModelAndView showPage(@ModelAttribute("selectschoolform") SchoolInfo schoolInfo, HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1)
	{
		final String methodName = "showPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
			final Users user = (Users) session.getAttribute("loginuser");
			final Long shopperId = Long.valueOf(user.getUserID());
			ArrayList<SchoolInfo> states = null;
			List<NonProfitPartnerInfo> nonProfitList = null;
			
			session.setAttribute("ShopperRegMenuTitle", "Select School");
			
			schoolInfo = shopperService.displaySchoolInfo(shopperId);		
			
			if (null != schoolInfo)
			{
				if (null != schoolInfo.getUniversityID().toString())
				{
					schoolInfo.setUniversityHidden(schoolInfo.getUniversityID().toString());
				}

				if (null != schoolInfo.getState())
				{
					schoolInfo.setStateHidden(schoolInfo.getState());
				}
				if (null != schoolInfo.getUniversityName())
				{
					schoolInfo.setCollege(schoolInfo.getUniversityName());
				}
			}
			else
			{
				schoolInfo = new SchoolInfo();
				schoolInfo.setUserId(shopperId);
			}
			
			states = shopperService.getAllStates();
			session.setAttribute("statesList", states);
			nonProfitList = shopperService.getNonProfitPartners(); 
			
			model.put("selectschoolform", schoolInfo);
			session.setAttribute("universityId", schoolInfo.getUniversityID());
			model.put("nonProfitList", nonProfitList);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			e.printStackTrace();
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("consselectschool");
	}

	/**
	 * This method saves shopper selected college.
	 * @param schoolInfo
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/selectschool.htm", method = RequestMethod.POST)
	public ModelAndView selectSchoolOnsubmit(@ModelAttribute("selectschoolform") SchoolInfo schoolInfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException
	{
		final String methodName = "selectSchoolOnsubmit";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		String view = null;
		
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
			final Users user = (Users) session.getAttribute("loginuser");
			final Long shopperId = Long.valueOf(user.getUserID());
			schoolInfo.setUserId(shopperId);
			schoolInfo.setStateHidden(schoolInfo.getState());
			final String collegeId = schoolInfo.getCollege();
			
			if (null == collegeId || "".equals(collegeId))
			{
				schoolInfo.setUniversityID(null);
			}
			else
			{
				schoolInfo.setUniversityID(Integer.parseInt(collegeId));
				schoolInfo.setUniversityHidden(collegeId);
			}
			
			shopperService.saveSchoolInfo(schoolInfo);

			view = "doublecheck.htm";

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			e.printStackTrace();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(new RedirectView(view));

	}

	/**
	 * This method is to populate the university/college dropdown for the given state code.
	 * @param stateCode
	 * @param college
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/shopperfetccollege", method = RequestMethod.GET)
	public @ResponseBody
	String getColleges(@RequestParam(value = "statecode", required = true) String stateCode,
			@RequestParam(value = "college", required = true) String college, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{
		final String methodName = "getColleges";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		ArrayList<SchoolInfo> universitesList = null;
		final StringBuffer innerHtml = new StringBuffer();
		Integer universityId = null;

		final String finalHtml = "<select name='college' id='College' tabindex='2'class='textboxBig'><option value=''>--Select--</option>";

		innerHtml.append(finalHtml);

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
			
			universitesList = shopperService.getAllColleges(stateCode);
			
			session.setAttribute("universiteslist", universitesList);

			response.setContentType("text/xml");

			for (int i = 0; i < universitesList.size(); i++)
			{

				if ("setFlag".equalsIgnoreCase(college))
				{
					universityId = (Integer) session.getAttribute("universityId");
					if (universityId.equals(universitesList.get(i).getUniversityID()))
					{
						innerHtml.append("<option selected=true value=" + universitesList.get(i).getUniversityID() + ">"
								+ universitesList.get(i).getCollege() + " </option>");
						session.setAttribute("collegeName", universitesList.get(i).getCollege());

					}
					else
					{
						innerHtml.append("<option value=" + universitesList.get(i).getUniversityID() + ">" + universitesList.get(i).getCollege()
								+ " </option>");
					}
				}
				else

				{

					innerHtml.append("<option value=" + universitesList.get(i).getUniversityID() + ">" + universitesList.get(i).getCollege()
							+ " </option>");
				}

			}

			innerHtml.append("</select>");

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			e.printStackTrace();
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}
}

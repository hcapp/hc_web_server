/**
 * @ (#) DoubleCheckController.java 23-Dec-2011
 * Project       :ScanSeeWeb
 * File          : DoubleCheckController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 23-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package shopper.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import shopper.service.ShopperService;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.DoubleCheckInfo;
import common.pojo.PreferencesInfo;
import common.pojo.SchoolInfo;
import common.pojo.State;
import common.pojo.UserPreference;
import common.pojo.UserPreferenceDisplay;
import common.pojo.Users;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.DoubleCheckData;
import common.pojo.shopper.MainCategoryDetail;
import common.pojo.shopper.SubCategoryDetail;
import common.util.Utility;
/**
 * This class is a controller class for shopper double check.
 * @author Dileep
 *
 */
@Controller
@RequestMapping("/doublecheck.htm")
public class DoubleCheckController
{

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DoubleCheckController.class);

	/**
	 * This method returns shopper double check page.
	 * @param categoryDetail
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/doublecheck.htm", method = RequestMethod.GET)
	public ModelAndView showPage(@ModelAttribute("doublecheckinfoform") CategoryDetail categoryDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "showPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		
		String view = null;
		final DoubleCheckData doubleCheckData = new DoubleCheckData();
		ArrayList<State> preferenceStates = null;
		String prefernceStateName = null;
		final SchoolInfo schoolInfo = new SchoolInfo();
		ArrayList<SchoolInfo> states = null;
		Integer mainCtgyCount = 0;
		Integer mainCtgySelCount = 0;
		Integer subCtgyCount = 0;
		Integer subCtgySelCount = 0;
		String catName = null;
		String subCatName = null;
		StringBuffer buffer = null;
		StringBuffer stringBuffer = null;
		StringBuffer finalBuffer = null;
		Integer categoryId;
		Integer favCatId = 0;
		StringBuffer selectedCat = null;
		session.setAttribute("ShopperRegMenuTitle", "Double Check");
		
		try
		{
			final Users user = (Users) session.getAttribute("loginuser");

			if (user != null)
			{
				finalBuffer = new StringBuffer();
				buffer = new StringBuffer();
				selectedCat = new StringBuffer();
				final Long shopperId = Long.valueOf(user.getUserID());
				final UserPreferenceDisplay doublecheckinfoform = shopperService.getDoubleCheckDetails(shopperId);
				final List<UserPreference> userPreferenceInfoList = doublecheckinfoform.getUserPreferenceList();
				UserPreference userPreference = new UserPreference();
				userPreference = userPreferenceInfoList.get(0);
				
				final CategoryDetail categoryDetailresponse = doublecheckinfoform.getCategoryDetailresponse();
				
				for (MainCategoryDetail mainCategoryDetail : categoryDetailresponse.getMainCategoryDetaillst())
				{
					subCtgySelCount = 0;
					subCtgyCount = 0;
					mainCtgyCount++;
					stringBuffer = new StringBuffer();
					catName = mainCategoryDetail.getMainCategoryName();
					
					for (SubCategoryDetail subCategoryDetail : mainCategoryDetail.getSubCategoryDetailLst())
					{
						subCtgyCount++;
						favCatId++;
						subCatName = subCategoryDetail.getSubCategoryName();
						categoryId = subCategoryDetail.getCategoryId();
						stringBuffer.append("<tr name="
										+ catName
										+ " onclick='toggleConfirm();'><td align='center' width='4%' class='wrpWord'>&nbsp;</td><td width='4%' class='wrpWord'>");
						
						if (subCategoryDetail.getDisplayed() == 0)
						{
							stringBuffer.append("<input type='checkbox' id='categoryDetailresponse.favCatId" + favCatId + "' value='" + categoryId + "' name='categoryDetailresponse.favCatId'/>");
						}
						else
						{
							stringBuffer.append("<input type='checkbox' id='categoryDetailresponse.favCatId" + favCatId + "' checked='checked' value='" + categoryId + "' name='categoryDetailresponse.favCatId' />");
							subCtgySelCount++;
						}
						
						stringBuffer.append("</td><td width='92%' class='wrpWord'>" + subCatName + "</td>");
					}
					
					if(subCtgyCount.equals(subCtgySelCount))
					{
						buffer.append("<tr name=" + catName + " class='mainCtgry'>" +
								"<td width='4%' align='center' class='wrpWord'>" +
								"<input type='checkbox' name='categoryDetailresponse.favCatId' checked='checked'/></td>"+
								"<td colspan='2' width='96%'  class='wrpWord'>" + catName + "</td></tr>");
						mainCtgySelCount++;
						String[] name = {catName} ;
						name = name[0].split("[&|,| ]");
						selectedCat.append(name[0] + ",");
					}
					else
					{
						buffer.append("<tr name=" + catName + " class='mainCtgry'>" +
								"<td width='4%' align='center' class='wrpWord'>" +
								"<input type='checkbox' name='categoryDetailresponse.favCatId'/></td>"+
								"<td colspan='2' width='96%'  class='wrpWord'>" + catName + "</td></tr>");
						if(!subCtgySelCount.equals(0))
						{
							String[] name = {catName} ;
							name = name[0].split("[&|,| ]");
							selectedCat.append(name[0] + ",");
						}
					}
					
					buffer.append(stringBuffer);

				}
				
				finalBuffer.append("<thead><tr class='subHdr'><th align='center'>");
				
				if(mainCtgyCount.equals(mainCtgySelCount))
				{
					finalBuffer.append("<input type='checkbox' id='checkAll' name='checkAll' checked='checked'>");					
				}
				else
				{
					finalBuffer.append("<input type='checkbox' id='checkAll' name='checkAll'>");
				}
				
				finalBuffer.append("<label for='checkbox' name='ctgry'></label></th><th align='left' colspan='2'>" +
									"<img width='14' height='13' alt='Preferences' src='../images/prefIconSml.png'/>" +
									"Select Your Preferences</th></tr></thead><tbody>");
				finalBuffer.append(buffer);
				finalBuffer.append("</tbody>");
				selectedCat.append("end");
				session.setAttribute("expandCategory", selectedCat);
				session.setAttribute("preferenceCategories", finalBuffer);
				
				session.setAttribute("categoryDetailresponse", categoryDetailresponse);
				final PreferencesInfo alertpreferencesInfo = new PreferencesInfo();
				final Boolean cellPhone = categoryDetailresponse.isCellPhone();
				final Boolean email = categoryDetailresponse.isEmail();
				
				if (null != cellPhone)
				{
					if (cellPhone)
					{
						alertpreferencesInfo.setCellPhone(true);
					}
				}
				
				if (null != email)
				{
					if (email)
					{
						alertpreferencesInfo.setEmail(true);
					}
				}
				
				final List<SchoolInfo> schoolInfos = doublecheckinfoform.getSchollList();
				
				if (null != schoolInfos)
				{
					for (SchoolInfo schoolDetails : schoolInfos)
					{
						if (null != schoolDetails)
						{
							schoolInfo.setUniversityID(schoolDetails.getUniversityID());
							schoolInfo.setState(schoolDetails.getState());
							
							if (null != schoolDetails.getUniversityID().toString())
							{
								schoolInfo.setUniversityHidden(schoolDetails.getUniversityID().toString());
							}

							if (null != schoolDetails.getState())
							{
								schoolInfo.setStateHidden(schoolDetails.getState());
							}
							
							if (null != schoolDetails.getUniversityName())
							{
								schoolInfo.setCollege(schoolDetails.getUniversityName());
							}
						}
					}
				}
				
				states = shopperService.getAllStates();
				session.setAttribute("statesList", states);
				preferenceStates = supplierService.getState(userPreference.getState());
				userPreference.setStateCodeHidden(userPreference.getState());
				
				for (State state : preferenceStates)
				{
					prefernceStateName = state.getStateName();
				}
				
				userPreference.setStateHidden(prefernceStateName);
				userPreference.setMobilePhone(Utility.getPhoneFormate(userPreference.getMobilePhone()));
				doubleCheckData.setUserPreferenceInfoList(userPreference);
				doubleCheckData.setAlertpreferencesInfo(alertpreferencesInfo);
				doubleCheckData.setCategoryDetailresponse(categoryDetailresponse);
				doubleCheckData.setStates(schoolInfo);
				session.setAttribute("universityId", schoolInfo.getUniversityID());
				
				model.put("doublecheckinfoform", doubleCheckData);
				view = "consdoublecheck";
			}
			else
			{
				LOG.info("UserID value is null in DoubleCheckController Post Method... ");
				return new ModelAndView(new RedirectView("createShopperProfile.htm"));
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			e.printStackTrace();
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(view);
	}

	/**
	 * This method redirects the control to shopperdashboard.htm.
	 * @param doubleCheckInfo
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return ModelAndView
	 * @throws IOException
	 */
	@RequestMapping(value = "/doublecheck.htm", method = RequestMethod.POST)
	public ModelAndView showHomePage(@ModelAttribute("doublecheckinfoform") DoubleCheckInfo doubleCheckInfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException
	{
		final String methodName = "showHomePage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(new RedirectView("shopperdashboard.htm"));
	}

}

package shopper.controller;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import shopper.service.ShopperService;
import shopper.validator.ShopperProfileValidator;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.City;
import common.pojo.State;
import common.pojo.Users;
import common.util.Utility;

/**
 * This class is a controller class for shopper profile creation.
 * 
 * @author manjunath_gh
 */
@Controller
public class ShopperProfileController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ShopperProfileController.class);

	/**
	 * Variable which contain url to be return once the login is success.
	 */
	private final String LOGIN_URL = "/ScanSeeWeb/shopper/preferences.htm";

	/**
	 * Reference variable for ShopperProfileValidator class.
	 */
	private ShopperProfileValidator shopperProfileValidator;

	@Autowired
	public void setShopperProfileValidator(ShopperProfileValidator shopperProfileValidator)
	{
		this.shopperProfileValidator = shopperProfileValidator;
	}

	/**
	 * This method returns shopper registration form.
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return String
	 */
	@RequestMapping(value = "/createShopperProfile.htm", method = RequestMethod.GET)
	public String showPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		final String methodName = "showPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Users shopperRegistrationInfo = new Users();
		ArrayList<State> states = null;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			states = retailerService.getAllStates();
			
			session.setAttribute("states", states);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + e.getMessage());
			throw new ScanSeeServiceException();
		}
		model.put("createshopperprofileform", shopperRegistrationInfo);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "consprofilecreation";
	}

	/**
	 * This method validates and saves shopper registration information.
	 * 
	 * @param shopperRegistrationInfo
	 * @param result
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/createShopperProfile.htm", method = RequestMethod.POST)
	public ModelAndView createShopperProfile(@ModelAttribute("createshopperprofileform") Users shopperRegistrationInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "createShopperProfile";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		final String strLogoImage = shopperService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
		
		Users userInfo = null;
		String view = "preferences.htm";
		String emailId = null;
		boolean isValidEmail = true;
		String password = null;
		String retypePassword = null;
		String city = null;
		boolean isValidContactNum = true;
		final String s = "1";
		try
		{
			shopperProfileValidator.validate(shopperRegistrationInfo, result);
			city = shopperRegistrationInfo.getCity();
			session.setAttribute("shopperCity", city);

			if (result.hasErrors())
			{
				shopperRegistrationInfo.setPassword(null);
				shopperRegistrationInfo.setRePassword(null);
				return new ModelAndView("consprofilecreation");
			}
			
			emailId = shopperRegistrationInfo.getEmail();
			isValidEmail = Utility.validateEmailId(emailId);
			isValidContactNum = Utility.validatePhoneNum(shopperRegistrationInfo.getMobilePhone());
			password = shopperRegistrationInfo.getPassword();
			retypePassword = shopperRegistrationInfo.getRePassword();
			
			if (!isValidContactNum)
			{
				shopperProfileValidator.validate(shopperRegistrationInfo, result, ApplicationConstants.INVALIDCONTPHONE);
				
				if (result.hasErrors())
				{
					shopperRegistrationInfo.setPassword(null);
					shopperRegistrationInfo.setRePassword(null);
					return new ModelAndView("consprofilecreation");
				}
			}

			if (null != password && !"".equals(password))
			{
				if (password.length() < 8)
				{
					shopperProfileValidator.validate(shopperRegistrationInfo, result, ApplicationConstants.PASSWORDLength);

				}
				else if (!(Utility.validatePassword(password)))
				{
					shopperProfileValidator.validate(shopperRegistrationInfo, result, ApplicationConstants.PASSWORDMATCH);
				}
				else if (!(password.equals(retypePassword)))
				{
					shopperProfileValidator.validate(shopperRegistrationInfo, result, ApplicationConstants.PASSWORD);
				}
				if (result.hasErrors())
				{
					shopperRegistrationInfo.setPassword(null);
					shopperRegistrationInfo.setRePassword(null);
					return new ModelAndView("consprofilecreation");
				}
			}
			
			if (!isValidEmail)
			{
				shopperProfileValidator.validate(shopperRegistrationInfo, result, ApplicationConstants.INVALIDEMAIL);
				if (result.hasErrors())
				{
					shopperRegistrationInfo.setPassword(null);
					shopperRegistrationInfo.setRePassword(null);
					return new ModelAndView("consprofilecreation");
				}
			}
			
			
			final Users user = shopperService.processShopperProfileDetails(shopperRegistrationInfo, strLogoImage);
			
			if (user.getUserID() != null)
			{
				LOG.info("user  id :::::::::" + user.getUserID());
				session.removeAttribute("supplierCity");
				final Users loginUser = new Users();
				loginUser.setFirstName(shopperRegistrationInfo.getFirstName());
				loginUser.setUserType(1);
				session.setAttribute("registration", s);
				user.setUserType(1);
				session.setAttribute("loginuser", user);
				final String uname = user.getUserName();
				final String fname = user.getFirstName();
				emailId = user.getEmail();
				session.setAttribute("truncUsername", Utility.truncate(uname, 15));
				session.setAttribute("truncFirstName", Utility.truncate(fname, 15));
				session.setAttribute("emailId", emailId);
				view = LOGIN_URL;
			}
			else
			{
				if (user.getIsProfileCreated().equalsIgnoreCase(ApplicationConstants.DUPLICATE_USER))
				{
					shopperProfileValidator.validate(shopperRegistrationInfo, result, ApplicationConstants.DUPLICATE_USER);

					if (result.hasErrors())
					{
						shopperRegistrationInfo.setPassword(null);
						shopperRegistrationInfo.setRePassword(null);
						LOG.info("in Shopper Contoller Duplicate user exit, if error there...");
						view = "consprofilecreation";
					}

				}
			}

			if (result.hasErrors())
			{
				shopperRegistrationInfo.setPassword(null);
				shopperRegistrationInfo.setRePassword(null);
				return new ModelAndView("consprofilecreation");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + e);
			e.printStackTrace();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		if (view.equals(LOGIN_URL))
		{
			userInfo = new Users();
			userInfo.setUserName(shopperRegistrationInfo.getUserName());

			return new ModelAndView(new RedirectView(view));
		}
		else
		{
			return new ModelAndView(view);
		}

	}

	/**
	 * This method populate the city dropdown based on the given state code.
	 * 
	 * @param stateCode
	 * @param city
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "/shopperfetchcity", method = RequestMethod.GET)
	public @ResponseBody
	String getCities(@RequestParam(value = "statecode", required = true) String stateCode,
			@RequestParam(value = "city", required = true) String city, HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		final String methodName = "getCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<City> cities = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='city' id='City'><option value='0'>--Select--</option>";

		innerHtml.append(finalHtml);

		try
		{

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			
			cities = supplierService.getAllCities(stateCode);
			response.setContentType("text/xml");

			String selCity = (String) session.getAttribute("shopperCity");
			if (null == selCity || "".equals(selCity))
			{
				selCity = city;
			}
			for (int i = 0; i < cities.size(); i++)
			{
				final String cityName = cities.get(i).getCityName();
				if (null != selCity && cityName.equals(selCity))
				{
					innerHtml.append("<option selected=true >" + cities.get(i).getCityName() + "</option>");
				}
				else
				{
					innerHtml.append("<option>" + cities.get(i).getCityName() + "</option>");
				}
			}
			innerHtml.append("</select>");

		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + e);
			e.printStackTrace();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}
}

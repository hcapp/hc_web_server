/**
 * @ (#) ShopController.java 23-Dec-2011
 * Project       :ScanSeeWeb
 * File          : ShopController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 23-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package shopper.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/shop.htm")
public class ShopController {
	
	
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory
			.getLogger(DoubleCheckController.class);
	
	
	@RequestMapping(value = "/shop.htm", method = RequestMethod.GET)
	public String showPage(HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1) {

		LOG.info("Entered in ShopController Get Method.....");	
		
		return "shop";
	}

}

package shopper.wishlist.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.wishlist.helper.SortProductByFlag;
import shopper.wishlist.helper.SortWishListHistoryByDate;

import common.pojo.shopper.AlertedProducts;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.WishListHistoryDetails;
import common.pojo.shopper.WishListProducts;
import common.pojo.shopper.WishListResultSet;

/**
 * This class contains methods for grouping wish list history products based on
 * wish list deleted date.
 * 
 * @author shyamsundara_hm
 */

public class WishListHelper
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WishListHelper.class);

	/**
	 * for WishListHelper constructor.
	 */
	protected WishListHelper()
	{
		LOG.info("Inside WishListHelper class");
	}

	
	/**
	 * This method for grouping based on wish list add date and sorting by
	 * Current date. It also groups products into alerted products and other
	 * products based on sale price.
	 * 
	 * @param wishlistHistorylst
	 *            As parameter
	 * @return list containing wish list history.
	 */

	public static WishListProducts getWishListDetails(ArrayList<ProductDetail> wishlistHistorylst)
	{

		final WishListProducts wishListProd = new WishListProducts();

		final HashMap<String, WishListResultSet> wishListResultMap = new HashMap<String, WishListResultSet>();

		ArrayList<ProductDetail> productDetaillst = null;

		final ArrayList<ProductDetail> alertProdList = new ArrayList<ProductDetail>();

		final AlertedProducts objAlert = new AlertedProducts();
		WishListResultSet wishListResultSetInfo = null;
		String key = null;
		for (ProductDetail listResultSet : wishlistHistorylst)
		{
			key = listResultSet.getWishListAddDate();

			if (wishListResultMap.containsKey(key))
			{

				wishListResultSetInfo = wishListResultMap.get(key);
				productDetaillst = (ArrayList<ProductDetail>) wishListResultSetInfo.getProductDetail();
				if (null != productDetaillst)
				{
					if ((listResultSet.getRetailFlag()  != null && listResultSet.getRetailFlag() >0) 
							|| (listResultSet.getCouponSaleFlag() != null && listResultSet.getCouponSaleFlag() > 0)
							|| (listResultSet.getHotDealFlag() != null && listResultSet.getHotDealFlag() > 0))
					{
						final ProductDetail objProduct = new ProductDetail();
						objProduct.setProductId(listResultSet.getProductId());
						objProduct.setProductName(listResultSet.getProductName());
						objProduct.setSalePrice(listResultSet.getSalePrice());
						objProduct.setUserProductId(listResultSet.getUserProductId());
						objProduct.setProductPrice(listResultSet.getProductPrice());
						objProduct.setProductImagePath(listResultSet.getProductImagePath());
						objProduct.setRetailFlag(listResultSet.getRetailFlag());
						objProduct.setCouponSaleFlag(Integer.valueOf(listResultSet.getCouponSaleFlag()));
						objProduct.setHotDealFlag(Integer.valueOf(listResultSet.getHotDealFlag()));
						objProduct.setPushNotifyFlag(listResultSet.getPushNotifyFlag());
						alertProdList.add(objProduct);
					}
					else
					{

						final ProductDetail productDetail = new ProductDetail();
						productDetail.setProductId(listResultSet.getProductId());
						productDetail.setUserProductId(listResultSet.getUserProductId());
						productDetail.setProductName(listResultSet.getProductName());
						productDetail.setProductImagePath(listResultSet.getProductImagePath());
						productDetail.setProductPrice(listResultSet.getProductPrice());
						productDetail.setSalePrice(listResultSet.getSalePrice());
						productDetail.setCouponSaleFlag(Integer.valueOf(listResultSet.getCouponSaleFlag()));
						productDetail.setHotDealFlag(Integer.valueOf(listResultSet.getHotDealFlag()));
						productDetail.setRetailFlag(listResultSet.getRetailFlag());
						productDetail.setPushNotifyFlag(listResultSet.getPushNotifyFlag());
						productDetaillst.add(productDetail);
					}
					wishListResultSetInfo.setProductDetail(productDetaillst);
				}
			}
			else
			{
				wishListResultSetInfo = new WishListResultSet();
				wishListResultSetInfo.setWishListAddDate(listResultSet.getWishListAddDate());
				productDetaillst = new ArrayList<ProductDetail>();

				if ((listResultSet.getRetailFlag()  != null && listResultSet.getRetailFlag() >0)
						|| (listResultSet.getCouponSaleFlag() != null && listResultSet.getCouponSaleFlag() > 0)
						|| (listResultSet.getHotDealFlag() != null && listResultSet.getHotDealFlag() > 0))
				{
					final ProductDetail objProduct = new ProductDetail();
					objProduct.setProductId(listResultSet.getProductId());
					objProduct.setUserProductId(listResultSet.getUserProductId());
					objProduct.setProductName(listResultSet.getProductName());
					objProduct.setSalePrice(listResultSet.getSalePrice());
					objProduct.setProductPrice(listResultSet.getProductPrice());
					objProduct.setProductImagePath(listResultSet.getProductImagePath());
					objProduct.setCouponSaleFlag(Integer.valueOf(listResultSet.getCouponSaleFlag()));
					objProduct.setHotDealFlag(Integer.valueOf(listResultSet.getHotDealFlag()));
					objProduct.setRetailFlag(listResultSet.getRetailFlag());
					objProduct.setPushNotifyFlag(listResultSet.getPushNotifyFlag());
					alertProdList.add(objProduct);
				}
				else
				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductId());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getProductPrice());
					productDetail.setSalePrice(listResultSet.getSalePrice());
					productDetail.setCouponSaleFlag(Integer.valueOf(listResultSet.getCouponSaleFlag()));
					productDetail.setHotDealFlag(Integer.valueOf(listResultSet.getHotDealFlag()));
					productDetail.setRetailFlag(listResultSet.getRetailFlag());
					productDetail.setPushNotifyFlag(listResultSet.getPushNotifyFlag());
					productDetaillst.add(productDetail);
					wishListResultSetInfo.setProductDetail(productDetaillst);
					wishListResultMap.put(key, wishListResultSetInfo);
				}
			}
		}

		final Set<Map.Entry<String, WishListResultSet>> set = wishListResultMap.entrySet();

		final ArrayList<WishListResultSet> wishListResultSetlst = new ArrayList<WishListResultSet>();

		for (Map.Entry<String, WishListResultSet> entry : set)
		{
			wishListResultSetlst.add(entry.getValue());
		}
		final Comparator<ProductDetail> comp = Collections.reverseOrder(new SortProductByFlag());

		if (alertProdList != null && !alertProdList.isEmpty())
		{
			Collections.sort(alertProdList, comp);
			objAlert.setAlertProductDetails(alertProdList);
			wishListProd.setAlertProducts(objAlert);
		}

		/**
		 * Calling Comparator interface for sorting wishListAddDate. It sorts
		 * and displays according to current date.
		 */

		if (wishListResultSetlst != null && !wishListResultSetlst.isEmpty())
		{

			final SortWishListHistoryByDate sortHistoryByDateObj = new SortWishListHistoryByDate();
			Collections.sort(wishListResultSetlst, sortHistoryByDateObj);
			Collections.reverse(wishListResultSetlst);
			wishListProd.setProductInfo(wishListResultSetlst);
		}

		return wishListProd;
	}
	
	/**
	 * This method for grouping based on wish list add date and sorting by
	 * current date.
	 * 
	 * @param wishListHistory
	 *            AS parameter
	 * @return list containing wish list history.
	 */

	public static WishListHistoryDetails getWishListHistory(List<ProductDetail> wishListHistory)
	{

		final WishListHistoryDetails wishListHistoryDetails = new WishListHistoryDetails();

		final HashMap<String, WishListResultSet> wishListResultMap = new HashMap<String, WishListResultSet>();

		ArrayList<ProductDetail> productDetaillst = null;

		WishListResultSet wishListResultSetInfo = new WishListResultSet();

		String key = null;
		for (ProductDetail listResultSet : wishListHistory)
		{
			key = listResultSet.getWishListAddDate();

			if (wishListResultMap.containsKey(key))
			{

				wishListResultSetInfo = wishListResultMap.get(key);
				productDetaillst = (ArrayList<ProductDetail>) wishListResultSetInfo.getProductDetail();
				if (null != productDetaillst)

				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductId());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getProductPrice());
					productDetail.setSalePrice(listResultSet.getSalePrice());
					productDetail.setRow_Num(listResultSet.getRow_Num());
					productDetaillst.add(productDetail);
					wishListResultSetInfo.setProductDetail(productDetaillst);
				}
			}
			else
			{
				wishListResultSetInfo = new WishListResultSet();

				wishListResultSetInfo.setWishListAddDate(listResultSet.getWishListAddDate());
				productDetaillst = new ArrayList<ProductDetail>();
				final ProductDetail productDetail = new ProductDetail();
				if (null != listResultSet.getProductId())
				{
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductId());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getProductPrice());
					productDetail.setSalePrice(listResultSet.getSalePrice());
					productDetail.setRow_Num(listResultSet.getRow_Num());
					productDetaillst.add(productDetail);
					wishListResultSetInfo.setProductDetail(productDetaillst);
				}
				wishListResultMap.put(key, wishListResultSetInfo);
			}
		}

		final Set<Map.Entry<String, WishListResultSet>> set = wishListResultMap.entrySet();

		final ArrayList<WishListResultSet> wishListResultSetlst = new ArrayList<WishListResultSet>();

		for (Map.Entry<String, WishListResultSet> entry : set)
		{
			wishListResultSetlst.add(entry.getValue());
		}

		/**
		 * Calling Comparator interface for sorting wishListAddDate. It sorts
		 * and displays according to current date.
		 */
		final SortWishListHistoryByDate sortHistoryByDateObj = new SortWishListHistoryByDate();
		Collections.sort(wishListResultSetlst, sortHistoryByDateObj);
		Collections.reverse(wishListResultSetlst);

		wishListHistoryDetails.setProductHistoryInfo(wishListResultSetlst);
		return wishListHistoryDetails;

	}
}

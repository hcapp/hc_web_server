package shopper.wishlist.service;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.wishlist.dao.WishListDAO;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import common.constatns.ApplicationConstants;
import common.email.EmailComponent;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.WishListHistoryDetails;
import common.pojo.shopper.WishListProducts;

/**
 * This class is used to display user wish list items depending on retailers.
 * 
 * @author sowjanya_d
 */

public class WishListServiceImpl implements WishListService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WishListServiceImpl.class);
	/**
	 * Instance variable for Wish list DAO instance.
	 */
	/**
	 * Instance variable for Wish List DAO instance.
	 */
	private WishListDAO wishListDao;

	/**
	 * sets the Wish List DAO.
	 * 
	 * @param wishListDao
	 *            The instance for WishListDAO
	 */

	public void setWishListDao(WishListDAO wishListDao)
	{
		this.wishListDao = wishListDao;
	}

	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to WishListDAO method
	 * 
	 * @param userId
	 *            as the request parameter.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("static-access")
	public WishListProducts getWishListItems(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "getWishListItems";
		LOG.info("In Service..." + methodName);

		ArrayList<ProductDetail> productDetaillst = null;

		productDetaillst = wishListDao.getWishListItems(areaInfoVO);

		WishListProducts wishLisProduct = null;
		if (null != productDetaillst && !productDetaillst.isEmpty())
		{
		//	final WishListHelper wishlisthelperObj = new WishListHelper();
		//	wishLisProduct = wishlisthelperObj.getWishListDetails(productDetaillst);

		}

		LOG.info(methodName);
		return wishLisProduct;
	}

	/**
	 * This method for fetching wish list history details based on user.
	 * 
	 * @param userId
	 *            The userId in the request.
	 * @param lowerlimit
	 *            as input request parameter
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("static-access")
	public WishListHistoryDetails fetchWishListHistoryDetails(Long userId, Integer lowerlimit) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchWishListHistoryDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ProductDetails productDetails = new ProductDetails();
		WishListHistoryDetails wishListHistoryDetails = null;

		if (lowerlimit == null)
		{
			lowerlimit = 0;
		}
		productDetails = wishListDao.getWishListHistoryDetails(userId, lowerlimit);

		/*if (null != productDetails && !productDetails.getProductDetail().isEmpty())
		{
			final WishListHelper wishlisthelperObj = new WishListHelper();
			wishListHistoryDetails = wishlisthelperObj.getWishListHistory(productDetails.getProductDetail());
			wishListHistoryDetails.setTotalSize(productDetails.getTotalSize());

		}
*/
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return wishListHistoryDetails;
	}

	/**
	 * To get hot deals information for the product.
	 * 
	 * @param userId
	 *            as request parameter
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return response as success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public HotDealsDetails getHotDealInfoForProduct(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "getWishListProductItems";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		HotDealsDetails hotDealsDetails = null;

		List<HotDealsDetails> hotDealsDetailslst = null;

		hotDealsDetailslst = wishListDao.getHotDealInfoForProduct(areaInfoVO);

		if (null != hotDealsDetailslst && !hotDealsDetailslst.isEmpty())
		{

			hotDealsDetails = hotDealsDetailslst.get(0);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsDetails;
	}

	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to Wish List DAO method.
	 * 
	 * @param xml
	 *            the Input XML.
	 * @return returns response depending on SUCCESS or ERROR.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public String deleteWishListItem(Long userId, Integer userProductId) throws ScanSeeWebSqlException
	{
		final String methodName = "deleteWishListItem";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		response = wishListDao.deleteWishListItem(userId, userProductId);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * To fetch the coupon details from the database.
	 * 
	 * @param userId
	 *            as request parameter
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ArrayList<CouponDetails> fetchCouponDetails(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "getWishListItems";
		LOG.info("In Service..." + ApplicationConstants.METHODSTART + methodName);

		ArrayList<CouponDetails> couponDetailslst = null;

		couponDetailslst = wishListDao.fetchCouponDetails(areaInfoVO);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return couponDetailslst;
	}

	/**
	 * the method fetches all the WishList store information.
	 * 
	 * @param userId
	 *            as the request.
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public RetailersDetails getAllWishListStoreDetails(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "getAllStoreDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final RetailersDetails retailersDetails = wishListDao.getStoreDetail(areaInfoVO);

		LOG.info(methodName);
		return retailersDetails;

	}
	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to WishListDAO method
	 * 
	 * @param xml
	 *            as the request.
	 * @return returns searched wishList product added SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public String addWishListProd(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{

		final String methodName = "addWishListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;

		

			response = wishListDao.addWishListProd(areaInfoVO);
			
		

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}
	
	
	
	public ExternalAPIInformation externalApiInfo(String moduleName) throws ScanSeeServiceException
	{

		ExternalAPIInformation externalAPIInformation = null;

		try
		{

			externalAPIInformation = wishListDao.getExternalApiInfo(moduleName);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return externalAPIInformation;

	}
	
	
	
	
	public ProductDetail fetchProductDetails(String productId) throws ScanSeeServiceException
	{

		ProductDetail prdDetail = null;

		try
		{

			prdDetail = wishListDao.getProductDetails(productId);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return prdDetail;

	}


	public String shareWishListHotDealbyEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeServiceException
	{
		final String methodName = " sharWishListHotDealbyEmail  ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String dealInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = null;
		String msgBody = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;

		try
		{

			if (null != shareProductInfoObj)
			{

				dealInfo = wishListDao.getWishListHotDealShareThruEmail(shareProductInfoObj);

				subject = ApplicationConstants.SHAREHOTDEALINFO;

				userEmailId = wishListDao.getUserInfo(shareProductInfoObj);
				if (userEmailId == null)
				{
					LOG.info("Validation failed::::To Email is not available");

					response = ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE;
					return response;
				}
				toAddress = shareProductInfoObj.getToEmail();
				emailConf = wishListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				msgBody = dealInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				LOG.info("Mail delivered to:" + toAddress);
				// response =
				// Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
				// ApplicationConstants.SHAREPRODUCTINFO);
				response = ApplicationConstants.SHAREPRODUCTINFO;
			}
		}
		catch (MessagingException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = ApplicationConstants.INVALIDEMAILADDRESSTEXT;
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Inside HotDealServiceImpl : setUserFavCategories : " + e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
}

package shopper.wishlist.service;



import java.util.ArrayList;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.WishListHistoryDetails;
import common.pojo.shopper.WishListProducts;

/**
 * The WishListService Interface. {@link ImplementedBy}
 * {@link WishListServiceImpl}
 * 
 * @author dileepa_cc
 */
public interface WishListService
{

	/**
	 * For displaying Wish List Items based on userId.
	 * 
	 * @param userId
	 *            as request
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return XML
	 * @throws ScanSeeException
	 *             for exception
	 */
	WishListProducts getWishListItems(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException;

	/**
	 * This method for fetching wish list history details based on user.
	 * 
	 * @param userId
	 *            The userId in the request.
	 * @param lowerlimit
	 *            as input request parameter
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public WishListHistoryDetails fetchWishListHistoryDetails(Long userId, Integer lowerlimit) throws ScanSeeWebSqlException;


	/**
	 * For displaying Wish List Items based on userId.
	 * 
	 * @param userId
	 *            as request
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return XML
	 * @throws ScanSeeException
	 *             for exception
	 */
	ArrayList<CouponDetails> fetchCouponDetails(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException;

	/**
	 * To get hotdeals information for the product.
	 * 
	 * @param userId
	 *            As request parameter
	 * @param productId
	 *            As request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	HotDealsDetails getHotDealInfoForProduct(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException;
	/**
	 * the method fetches all the WishList store information.
	 * 
	 * @param userId
	 *            as the request.
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	RetailersDetails getAllWishListStoreDetails(AreaInfoVO areaInfoVO)
			throws ScanSeeWebSqlException;

	
	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to Wish List DAO method.
	 * 
	 * @param xml
	 *            the Input XML.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public String deleteWishListItem(Long userId, Integer userProductId) throws ScanSeeWebSqlException;

	
	public ExternalAPIInformation externalApiInfo(String moduleName) throws ScanSeeServiceException;
	public ProductDetail fetchProductDetails(String productId) throws ScanSeeServiceException;

	
	/**
	 * To add searched product to Wish List.
	 * 
	 * @param xml
	 *            as request
	 * @return added success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	String addWishListProd(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException;
	String shareWishListHotDealbyEmail(ShareProductInfo shareProductInfo) throws ScanSeeServiceException;


}

package shopper.wishlist.helper;

import java.util.Comparator;

import common.pojo.shopper.ProductDetail;



/**
 * This class for sorting retailer list based on productprice.
 * @author team
 */
public class SortProductByFlag implements Comparator<ProductDetail>
{
	/**
	 * This method is to compare two products.
	 * @param product1
	 *       -As parameter
	 * @param product2
	 *       -As parameter      
	 * @return flagComp
	 *       -As parameter
	 */
	public int compare(ProductDetail product1, ProductDetail product2)
	{

		final int flagComp = product1.getPushNotifyFlag().compareTo(product2.getPushNotifyFlag());
		return flagComp;
	}
}

package shopper.wishlist.dao;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.query.ShoppingListQueries;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.common.pojos.ExternalAPISearchParameters;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.HotDealInfo;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.WishListResultSet;
import common.util.Utility;

/**
 * This dao implementation class for wish list screen.
 * 
 * @author shyamsundara_hm
 */
public class WishListDAOImpl implements WishListDAO
{

	/**
	 * Getting the Logger Instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(WishListDAOImpl.class.getName());

	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */

	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;

	/**
	 * To call stored procedue.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * This method for to get data source from xml.
	 * 
	 * @param dataSource
	 *            datasource name
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * The method fetches WishList Products.
	 * 
	 * @param areaInfoVO contains input parameters
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeWebSqlException
	 *             The exception of type SQL Exception.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<ProductDetail> getWishListItems(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "getWishListItems";
		log.info("Method name : " + methodName);

		WishListResultSet wishListProds = null;
		ArrayList<ProductDetail> productDetails = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WishListDisplay");
			simpleJdbcCall.returningResultSet("wistListsItems", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource wishlistdisplayparams = new MapSqlParameterSource();
			wishlistdisplayparams.addValue(ApplicationConstants.USERID, areaInfoVO.getUserID());
			wishlistdisplayparams.addValue("ZipCode", areaInfoVO.getZipCode());
			wishlistdisplayparams.addValue("Latitude", areaInfoVO.getLattitude());
			wishlistdisplayparams.addValue("Longitude", areaInfoVO.getLongitude());
			wishlistdisplayparams.addValue("Radius", areaInfoVO.getRadius());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(wishlistdisplayparams);
			productDetails = (ArrayList<ProductDetail>) resultFromProcedure.get("wistListsItems");

			if (null != productDetails && !productDetails.isEmpty())
			{
				wishListProds = new WishListResultSet();
				wishListProds.setProductDetail(productDetails);
			}
		}
		catch (DataAccessException exception)
		{
			log.error(" methodName" + exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		log.info(methodName);
		return productDetails;
	}

	/**
	 * The method fetches WishList history Products.
	 * 
	 * @param userId
	 *            as the request parameter
	 * @param lowerlimit
	 *            as the request parameter
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	@SuppressWarnings("unchecked")
	public ProductDetails getWishListHistoryDetails(Long userId, Integer lowerlimit) throws ScanSeeWebSqlException
	{
		final String methodName = "getWishListHistoryDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ProductDetails productDetails = null;
		List<ProductDetail> productDetailLst = null;
		Integer rowcount = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WishListHistoryDisplay");
			simpleJdbcCall.returningResultSet("wistListItems", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource wishlisthistoryparams = new MapSqlParameterSource();
			wishlisthistoryparams.addValue(ApplicationConstants.USERID, userId);
			wishlisthistoryparams.addValue("LowerLimit", lowerlimit);
			wishlisthistoryparams.addValue("ScreenName", ApplicationConstants.WLHISTORYSCREENNAME);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(wishlisthistoryparams);
			productDetailLst = (List<ProductDetail>) resultFromProcedure.get("wistListItems");

			if (!productDetailLst.isEmpty() && productDetailLst != null)
			{
				productDetails = new ProductDetails();
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				productDetails.setProductDetail(productDetailLst);
				productDetails.setTotalSize(rowcount);
			}
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in addWishListProd method ..errorNum ..." + errorNum + "errorMsg is" + errorMsg);
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				productDetails.setTotalSize(rowcount);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * This method for deleting wish list item.
	 * 
	 * @param productDetail
	 *            request parameter
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public String deleteWishListItem(Long userId, Integer userProductId) throws ScanSeeWebSqlException
	{

		final String methodName = "deleteWishListItem";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Integer result = 1;

		simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
		simpleJdbcCall.withProcedureName("usp_WishListDelete");
		SqlParameterSource wishListDeleteParameters;
		try
		{
			wishListDeleteParameters = new MapSqlParameterSource().addValue("UserProductID", userProductId)
					.addValue(ApplicationConstants.USERID, userId).addValue("WishListRemoveDate", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(wishListDeleteParameters);
			result = (Integer) resultFromProcedure.get(ApplicationConstants.DBSTATUS);
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsgis = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListDelete" + errorNum + "errorMsgis.." + errorMsgis);
				throw new ScanSeeWebSqlException(errorMsgis);
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}


	/**
	 * The method fetches WishList Product Coupon Details.
	 * 
	 * @param areaInfoVO contains input parameters
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeWebSqlException
	 *             The exception of type SQL Exception.
	 */

	@SuppressWarnings("unchecked")
	public ArrayList<CouponDetails> fetchCouponDetails(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchCouponDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<CouponDetails> couponDetailslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WishListCouponDisplay");
			simpleJdbcCall.returningResultSet("wistListCoupons", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", areaInfoVO.getUserID());
			scanQueryParams.addValue("ProductId", areaInfoVO.getProductId());
			scanQueryParams.addValue("ZipCode", areaInfoVO.getZipCode());
			scanQueryParams.addValue("Latitude", areaInfoVO.getLattitude());
			scanQueryParams.addValue("Longitude", areaInfoVO.getLongitude());
			scanQueryParams.addValue("Radius", areaInfoVO.getRadius());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			couponDetailslst = (ArrayList<CouponDetails>) resultFromProcedure.get("wistListCoupons");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in addWishListProd method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return couponDetailslst;
	}
	/**
	 * The method fetches WishList Product Local store Details.
	 * 
	 * @param areaInfoVO contains input parameters
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeWebSqlException
	 *             The exception of type SQL Exception.
	 */
	public RetailersDetails getStoreDetail(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "getStoreDetail in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		RetailersDetails retailersDetailsObj = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WishListRetailerInfo");
			simpleJdbcCall.returningResultSet("StoreDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource storetParameters = new MapSqlParameterSource();
			storetParameters.addValue("UserID", areaInfoVO.getUserID());
			storetParameters.addValue("ProductID", areaInfoVO.getProductId());
			storetParameters.addValue("Latitude", areaInfoVO.getLattitude());
			storetParameters.addValue("Longitude", areaInfoVO.getLongitude());
			storetParameters.addValue("ZipCode", areaInfoVO.getZipCode());
			storetParameters.addValue("Radius", areaInfoVO.getRadius());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(storetParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					@SuppressWarnings("unchecked")
					final List<RetailerDetail> storeDetaillst = (List<RetailerDetail>) resultFromProcedure.get("StoreDetails");

					if (null != storeDetaillst && !storeDetaillst.isEmpty())
					{
						retailersDetailsObj = new RetailersDetails();
						retailersDetailsObj.setRetailerDetail(storeDetaillst);
					}

				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					log.error("Error occurred in getProductDetail Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getProductDetail", exception);
			return null;
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return retailersDetailsObj;
	}
	/**
	 * The method fetches WishList Product hotdeal  Details.
	 * 
	 * @param areaInfoVO contains input parameters
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeWebSqlException
	 *             The exception of type SQL Exception.
	 */
	public List<HotDealsDetails> getHotDealInfoForProduct(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "getHotDealInfoForProduct";
		log.info(ApplicationConstants.METHODSTART + methodName);
		HotDealsDetails objHotDeal = null;
		List<HotDealsDetails> hotDetailList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WishListProductHotDealInformation");
			simpleJdbcCall.returningResultSet("hotDeals", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", areaInfoVO.getUserID());
			scanQueryParams.addValue("ProductId", areaInfoVO.getProductId());
			scanQueryParams.addValue("ZipCode", areaInfoVO.getZipCode());
			scanQueryParams.addValue("Latitude", areaInfoVO.getLattitude());
			scanQueryParams.addValue("Longitude", areaInfoVO.getLongitude());

			scanQueryParams.addValue("Radius", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDetailList = (List<HotDealsDetails>) resultFromProcedure.get("hotDeals");

				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListProductHotDealInformation" + errorNum + "errorMsg is " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDetailList;
	}
	/**
	 * The method to add searched product to WishList.
	 * 
	 * @param productDetailsRequest
	 *            is the request parameter
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	public String addWishListProd(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException
	{
		final String methodName = "addWishListProd";
		log.info(ApplicationConstants.METHODSTART + methodName);

		MapSqlParameterSource scanQueryParams = null;

		String responseFromProc = null;
		Integer fromProc = null;
		Integer productExits = null;

		try
		{				
			//		final String idString = Utility.getCommaSepartedValues(productDetails);

					simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
					simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
									simpleJdbcCall.withProcedureName("usp_WishListSearchAdd");
					simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
					scanQueryParams = new MapSqlParameterSource();
					scanQueryParams.addValue(ApplicationConstants.USERID, areaInfoVO.getUserID());
					scanQueryParams.addValue("ProductID", areaInfoVO.getProductId());
					scanQueryParams.addValue("WishListAddDate", Utility.getFormattedDate());
			

				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

				fromProc = (Integer) resultFromProcedure.get("Status");

				if (fromProc == 0)
				{
					responseFromProc = ApplicationConstants.SUCCESS;
					productExits = (Integer) resultFromProcedure.get("ProductExists");
					if (productExits != null)
					{
						if (productExits == 1)
						{
							responseFromProc = ApplicationConstants.PRODUCTEXISTS;
						}
						else
						{
							responseFromProc = ApplicationConstants.SUCCESS;
						}
					}
				}
				else
				{
					responseFromProc = ApplicationConstants.FAILURE;
				}
				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in addWishListProd method ..errorNum is..." + errorNum + "errorMsg..is " + errorMsg);
				}
			
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	public ExternalAPIInformation getExternalApiInfo(String moduleName) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;
		
		ExternalAPIInformation exteApi = null;
		
		
		
		List<ExternalAPIVendor> vendorList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIListOnline");
			simpleJdbcCall.returningResultSet("vendorList", new BeanPropertyRowMapper<ExternalAPIVendor>(ExternalAPIVendor.class));
			
			final MapSqlParameterSource vendorParameter = new MapSqlParameterSource()
			.addValue("prSubModule", moduleName);
			resultFromProcedure = simpleJdbcCall.execute(vendorParameter);
			vendorList = (ArrayList) resultFromProcedure.get("vendorList");
			
			ArrayList<ExternalAPISearchParameters> externalAPIInputParameters = null;
			
			final HashMap<Integer, ArrayList<ExternalAPISearchParameters>> apiParamMap = new HashMap<Integer, ArrayList<ExternalAPISearchParameters>>();
			
			exteApi = new ExternalAPIInformation();
			exteApi.setVendorList((ArrayList<ExternalAPIVendor>) vendorList);
			if ("FindOnlineStores".equals(moduleName))
			{
				for (ExternalAPIVendor obj : vendorList)
				{
					externalAPIInputParameters = getApiparams(Integer.valueOf(obj.getApiUsageID()), "FindOnlineStores");
					apiParamMap.put(Integer.valueOf(obj.getApiUsageID()), externalAPIInputParameters);
				}
			
			}
			
			exteApi.setSerchParameters(apiParamMap);
			
			
			
			//log.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" + arUniversityList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllUniversity" + exception);
		}
		return exteApi;
	}

	
	
	private ArrayList<ExternalAPISearchParameters> getApiparams(int id, String moduleName) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;

		ArrayList<ExternalAPISearchParameters> paramList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIInputParameters");
			simpleJdbcCall.returningResultSet("aipInputList", new BeanPropertyRowMapper<ExternalAPISearchParameters>(
					ExternalAPISearchParameters.class));

			final MapSqlParameterSource APISearchParameters = new MapSqlParameterSource().addValue("prAPIUsageID", id).addValue("PrAPISubModuleName", moduleName);
			resultFromProcedure = simpleJdbcCall.execute(APISearchParameters);
			paramList = (ArrayList) resultFromProcedure.get("aipInputList");

			log.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" + paramList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllUniversity" + exception);
		}
		return paramList;
	}
	
	public ProductDetail getProductDetails(String productId) throws ScanSeeWebSqlException
	{

		ProductDetail prdDetail = null;

		simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
		prdDetail = simpleJdbcTemplate.queryForObject(ShoppingListQueries.ONLINESTOREPRODUCTINFOQUERY, new BeanPropertyRowMapper<ProductDetail>(
				ProductDetail.class), productId);
		if (prdDetail != null)
		{
			return prdDetail;
		}

		return prdDetail;
	}

	
	public String getWishListHotDealShareThruEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeWebSqlException
	{

		final String methodName = "getWishListHotDealShareThruEmail";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> hotDealListlst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_HotDealDetails");
			simpleJdbcCall.returningResultSet("shareCouponInfo", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

					final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("HotdealID", shareProductInfoObj.getHotdealId()).addValue("UserID", shareProductInfoObj.getUserId());

			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					hotDealListlst = (List<HotDealsDetails>) resultFromProcedure.get("shareCouponInfo");
					if (!hotDealListlst.isEmpty() && hotDealListlst != null)
					{

						response = Utility.formHotDealInfoHTML(hotDealListlst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_HotDealShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}
		catch (InvalidKeyException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException
	{

		final String methodName = "getStockInfo";

		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_GetScreenContent Store Procedure with error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e);
		}

		return appConfigurationList;
	}

	


	public String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "getUserInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			response = this.jdbcTemplate.queryForObject(ShoppingListQueries.FETCHUSEREMAILID, new Object[] { shareProductInfo.getUserId() },
					String.class);

		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	}





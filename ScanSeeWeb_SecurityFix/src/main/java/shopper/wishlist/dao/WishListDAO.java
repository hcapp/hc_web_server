package shopper.wishlist.dao;

import java.util.ArrayList;
import java.util.List;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;

import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;

/**
 * The Interface for WishListDAO. ImplementedBy {@link WishListDAOImpl}
 * 
 * @author sowjanya_d
 */
public interface WishListDAO
{


	/**
	 * The method for getting WishList products.
	 * 
	 * @param userId
	 *            request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ArrayList<ProductDetail> getWishListItems(AreaInfoVO areaInfoVO )
			throws ScanSeeWebSqlException;
	

	List<HotDealsDetails> getHotDealInfoForProduct(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException;
	
	  ArrayList<CouponDetails> fetchCouponDetails(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException;
	  RetailersDetails getStoreDetail(AreaInfoVO areaInfoVO)
		throws ScanSeeWebSqlException;
	/**
	 * The method for deleting wish list item.
	 * 
	 * @param productDetail
	 *            request parameter
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String deleteWishListItem(Long userId, Integer userProductId) throws ScanSeeWebSqlException;
	
	
	
	/**
	 * The method for adding searched WishList product.
	 * 
	 * @param productDetailsRequest
	 *            is the request parameter.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *
	 */
	
	
	String addWishListProd(AreaInfoVO areaInfoVO) throws ScanSeeWebSqlException;

/*
 * 
	*//**
	 * The method for getting WishList searched products.
	 * 
	 * @param userId
	 *            of user
	 * @param prodSearchKey
	 *            are the request parameters.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *//*
	WishListResultSet getWishListProductItems(int userId, String prodSearchKey) throws ScanSeeWebSqlException;

	*//**
	 * The method for adding unassigned product to WishList.
	 * 
	 * @param productDetailsRequest
	 *            is the request parameter.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *//*
	String addUnassignedPro(ProductDetailsRequest productDetailsRequest) throws ScanSeeWebSqlException;

	*//**
	 * The method for adding searched WishList product.
	 * 
	 * @param productDetailsRequest
	 *            is the request parameter.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *//*
	// ProductDetails addWishListProd(ProductDetailsRequest
	// productDetailsRequest)throws ScanSeeException;
	String addWishListProd(ProductDetailsRequest productDetailsRequest) throws ScanSeeWebSqlException;

	*//**
	 * The method for getting hot deal information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *//*



	*//**
	 * The method for getting all wishList store information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *//*
	RetailersDetails getStoreDetail(Integer userId, Integer productId, String zipcode, String latitude, String longitude, Integer radius)
			throws ScanSeeWebSqlException;

	*//**
	 * The method for getting WishList history details.
	 * 
	 * @param userId
	 *            request parameter
	 * @param lowerlimit
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails getWishListHistoryDetails(Long userId, Integer lowerlimit) throws ScanSeeWebSqlException;

	/**
	 * The method for getting Product Attribute information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *//*
	ProductDetail fetchProductAttributeDetails(Integer userId, Integer productId) throws ScanSeeWebSqlException;

	*//**
	 * The method for getting all wishList store information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *//*
	
	
	 

	*//**
	 * The method for getting user zipcode information.
	 * 
	 * @param userId
	 *            of user
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 *//*
	WishListResultSet checkUserZipcode(Integer userId) throws ScanSeeWebSqlException;*/
	
	
    public ExternalAPIInformation getExternalApiInfo(String moduleName) throws ScanSeeWebSqlException;
	
	public ProductDetail getProductDetails(String productId) throws ScanSeeWebSqlException;
	
	public String getWishListHotDealShareThruEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeWebSqlException;
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException;
	public String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException;
}

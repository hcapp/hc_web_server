package shopper.preferences.service;

import java.util.ArrayList;

import common.exception.ScanSeeWebSqlException;
import common.pojo.City;
import common.pojo.UserPreference;
import common.pojo.shopper.CountryDetails;
import common.pojo.shopper.CreateOrUpdateUserPreference;
import common.pojo.shopper.UserInfo;
import common.pojo.shopper.UserRegistrationInfo;

public interface PreferencesService
{

	/**
	 * The service method for fetching user setting preferrences information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	CreateOrUpdateUserPreference fetchUserPreferences(Long userId) throws ScanSeeWebSqlException;

	/**
	 * The Service method for Saving User setting preffereces information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param inputXml
	 *            Contains user proffered categories information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String saveUserPreference(UserPreference userPreference) throws ScanSeeWebSqlException;

	/**
	 * The service method for fetching user information. This method validates
	 * the userId, if it is valid it will call the DAO method.
	 * 
	 * @param userID
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	UserInfo fetchUserInfo(Long userID) throws ScanSeeWebSqlException;

	/**
	 * The Service method for Saving User Information. This method checks for
	 * mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            containing user information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String insertUserInfo(UserRegistrationInfo userRegistrationInfoObj) throws ScanSeeWebSqlException;

	/**
	 * The DAO method used get All States.
	 * 
	 * @return List of states.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	CountryDetails getAllStates() throws ScanSeeWebSqlException;

	/**
	 * The service method for all the cities for the given state name.
	 * 
	 * @param stateName
	 *            -For which cities to be fetched
	 * @param searchKeyword
	 *            -For which cities to be fetched
	 * @return the XML containing cities information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	ArrayList<City> fetchAllCities(String stateName) throws ScanSeeWebSqlException;

	/**
	 * The service Method takes the user new password and encrypt it. Calls the
	 * dao method with encrypted password and userID.
	 * 
	 * @param inputXML
	 *            in the User Request.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String changePassword(Long userId, String password) throws ScanSeeWebSqlException;

}

package shopper.preferences.dao;

import java.util.ArrayList;
import java.util.List;

import common.exception.ScanSeeWebSqlException;
import common.pojo.City;
import common.pojo.CountryCode;
import common.pojo.UserPreference;
import common.pojo.shopper.CreateOrUpdateUserPreference;
import common.pojo.shopper.EducationalLevelInfo;
import common.pojo.shopper.IncomeRange;
import common.pojo.shopper.MaritalStatusInfo;
import common.pojo.shopper.UpdateUserInfo;
import common.pojo.shopper.UserRegistrationInfo;

public interface PreferencesDAO
{

	/**
	 * The DAO method returns the Settings information from the Database.
	 * 
	 * @param userID
	 *            - requested user.
	 * @return UserSettingPrefferenec object
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	CreateOrUpdateUserPreference fetchUserPreference(Long userID) throws ScanSeeWebSqlException;

	/**
	 * the DAO method for storing user settings preference Info.
	 * 
	 * @param userPreference
	 *            the userPreference in the request.
	 * @return the XML in response.
	 * @author shyamsundara_hm
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String saveUserPreferenceDetails(UserPreference userPreference) throws ScanSeeWebSqlException;

	/**
	 * The method for fetching Income ranges.
	 * 
	 * @return The IncomeRange as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<IncomeRange> getIncomeRanges() throws ScanSeeWebSqlException;

	/**
	 * The method for fetching country codes .
	 * 
	 * @return The CountryCode as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<CountryCode> getCountryCodes() throws ScanSeeWebSqlException;

	/**
	 * The method for getting education level informations.
	 * 
	 * @return EducationalLevelInfo as List type
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<EducationalLevelInfo> getEducationalLevel() throws ScanSeeWebSqlException;

	/**
	 * The method for getting marital status information.
	 * 
	 * @return MaritalStatusInfo as list type
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<MaritalStatusInfo> getMaritalStatusInfo() throws ScanSeeWebSqlException;

	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            - for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */
	UpdateUserInfo fetchUserInfo(Long userID) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for saving user information in the database.
	 * 
	 * @param userRegistrationInfo
	 *            - of user information
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String insertUserInfo(UserRegistrationInfo userRegistrationInfo) throws ScanSeeWebSqlException;

	/**
	 * The DAO method used get All States.
	 * 
	 * @return List of states.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	List<CountryCode> getAllStates() throws ScanSeeWebSqlException;

	/**
	 * this method used get All cities.
	 * 
	 * @param stateName
	 *            -for which cities to be fetched.
	 * @param searchKeyword
	 *            - for which cities to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	ArrayList<City> getAllCities(String stateName) throws ScanSeeWebSqlException;

	/**
	 * The DAO method to saves user password.
	 * 
	 * @param userID
	 *            -For which password needs to be updated
	 * @param password
	 *            -user new password
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeWebSqlException will be thrown.
	 */
	String changePassword(Long userID, String password) throws ScanSeeWebSqlException;
}

package shopper.preferences.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.preferences.query.PreferencesQueries;
import shopper.wishlist.dao.WishListDAOImpl;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.City;
import common.pojo.CountryCode;
import common.pojo.UserPreference;
import common.pojo.shopper.CreateOrUpdateUserPreference;
import common.pojo.shopper.EducationalLevelInfo;
import common.pojo.shopper.IncomeRange;
import common.pojo.shopper.MaritalStatusInfo;
import common.pojo.shopper.UpdateUserInfo;
import common.pojo.shopper.UserRegistrationInfo;
import common.util.Utility;

public class PreferencesDAOImpl implements PreferencesDAO

{

	/**
	 * Getting the Logger Instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(WishListDAOImpl.class.getName());
	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;

	/**
	 * To call stored procedue.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */

	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * This method for to get data source from xml.
	 * 
	 * @param dataSource
	 *            datasource name
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * The DAO method returns the Settings information from the Database.
	 * 
	 * @param userId
	 *            - requested user.
	 * @return UserSettingPrefferenec object
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	public final CreateOrUpdateUserPreference fetchUserPreference(Long userId) throws ScanSeeWebSqlException
	{
		
		final String methodName = "fetchUserPreference";

		log.info(ApplicationConstants.METHODSTART + methodName);

		CreateOrUpdateUserPreference createOrUpdateUserPreference = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			createOrUpdateUserPreference = simpleJdbcTemplate.queryForObject(PreferencesQueries.FETCHUSERPREFERENCEQUERY,
					new BeanPropertyRowMapper<CreateOrUpdateUserPreference>(CreateOrUpdateUserPreference.class), userId);
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return createOrUpdateUserPreference;

	}

	/**
	 * the DAO method for storing user settings preference Info.
	 * 
	 * @param userPreference
	 *            the userPreference in the request.
	 * @return the XML in response.
	 * @author shyamsundara_hm
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	public final String saveUserPreferenceDetails(UserPreference userPreference) throws ScanSeeWebSqlException
	{
		final String methodName = "saveUserPreferenceDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = 0;
		Map<String, Object> resultFromProcedure = null;
		String response = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerUserPreferences");
			final MapSqlParameterSource userPreferenceParameters = new MapSqlParameterSource();

			userPreferenceParameters.addValue(ApplicationConstants.USERID, userPreference.getUserID());
			userPreferenceParameters.addValue("LocaleRadius", userPreference.getLocaleRadius());
			userPreferenceParameters.addValue("ScannerSilent", userPreference.getScannerSilent());
			userPreferenceParameters.addValue("DisplayCoupons", userPreference.getDisplayCoupons());
			userPreferenceParameters.addValue("DisplayRebates", userPreference.getDisplayRebates());
			userPreferenceParameters.addValue("DisplayLoyaltyRewards", userPreference.getDisplayLoyaltyRewards());
			userPreferenceParameters.addValue("SavingsActivated", userPreference.getSavingsActivated());
			userPreferenceParameters.addValue("SleepStatus", userPreference.getSleepStatus());
			userPreferenceParameters.addValue("DateModified", Utility.getFormattedDate());
			resultFromProcedure = simpleJdbcCall.execute(userPreferenceParameters);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in saveUserPreferenceDetails method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in saveUserPreferenceDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (ParseException exception)
		{
			log.error("ParseException occurred in saveUserPreferenceDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		return response;
	}

	/**
	 * The method for fetching income ranges.
	 * 
	 * @return The IncomeRange as List.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public final List<IncomeRange> getIncomeRanges() throws ScanSeeWebSqlException
	{
		final String methodName = "getIncomeRanges";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<IncomeRange> incomeRanges = null;
		try
		{
			incomeRanges = this.jdbcTemplate.query(PreferencesQueries.FETCHINCOMERANGESQUERY,
					ParameterizedBeanPropertyRowMapper.newInstance(IncomeRange.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return incomeRanges;
	}

	/**
	 * The method for fetching country codes .
	 * 
	 * @return The CountryCode as List type.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public final List<CountryCode> getCountryCodes() throws ScanSeeWebSqlException
	{
		final String methodName = "getCountryCodes";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> countryCodes = null;
		try
		{
			countryCodes = this.jdbcTemplate.query(PreferencesQueries.FETCHCOUNTRYCODESQUERY,
					ParameterizedBeanPropertyRowMapper.newInstance(CountryCode.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return countryCodes;
	}

	/**
	 * The method for fetching EducationalLevelInfo..
	 * 
	 * @return educationalLevelInfo List.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public final List<EducationalLevelInfo> getEducationalLevel() throws ScanSeeWebSqlException
	{
		final String methodName = "getEducationalLevel";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<EducationalLevelInfo> eduLevelInfoList = null;
		try
		{

			eduLevelInfoList = this.jdbcTemplate.query(PreferencesQueries.FETCHEDUCATIONALLEVELQUERY,
					ParameterizedBeanPropertyRowMapper.newInstance(EducationalLevelInfo.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return eduLevelInfoList;
	}

	/**
	 * The method for fetching MaritalStatus inforamtions.
	 * 
	 * @return List of MaritalStatus objects.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public final List<MaritalStatusInfo> getMaritalStatusInfo() throws ScanSeeWebSqlException
	{
		final String methodName = "getMaritalStatusInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<MaritalStatusInfo> maritalStatusList = null;
		try
		{

			maritalStatusList = this.jdbcTemplate.query(PreferencesQueries.FETCHMARITALSTATUSQUERY,
					ParameterizedBeanPropertyRowMapper.newInstance(MaritalStatusInfo.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return maritalStatusList;
	}

	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            for which user information need to be fetched.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */

	@SuppressWarnings("unchecked")
	public final UpdateUserInfo fetchUserInfo(Long userID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchUserInfo";
		log.info("In DAO...." + ApplicationConstants.METHODSTART + methodName);
		log.info("userID...." + userID);
		UpdateUserInfo updateUserInfo = null;
		List<UpdateUserInfo> updateUserInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerUserInfoDisplay");
			simpleJdbcCall.returningResultSet("UserInfo", new BeanPropertyRowMapper<UpdateUserInfo>(UpdateUserInfo.class));

			final MapSqlParameterSource fetchUserInfoParameters = new MapSqlParameterSource();
			fetchUserInfoParameters.addValue("UserId", userID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchUserInfoParameters);
			updateUserInfoList = (ArrayList<UpdateUserInfo>) resultFromProcedure.get("UserInfo");
			if (null != updateUserInfoList && !updateUserInfoList.isEmpty())
			{
				updateUserInfo = new UpdateUserInfo();
				updateUserInfo = updateUserInfoList.get(0);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e);
		}
		if (null != updateUserInfo)
		{
			return updateUserInfo;
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return updateUserInfo;
	}

	/**
	 * The DAO method for saving user information in the database.
	 * 
	 * @param userRegistrationInfo
	 *            - of user information
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public String insertUserInfo(UserRegistrationInfo userRegistrationInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "insertUserInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer result = 1;
		String response = null;
		Timestamp modifiedDate = null;

		if (userRegistrationInfo.getChildren() == null)
		{
			userRegistrationInfo.setChildren(0);
		}
		if (userRegistrationInfo.getEducationLevelId() == 0)
		{
			userRegistrationInfo.setEducationLevelId(null);
		}

		if ((null != userRegistrationInfo.getCountryID() && userRegistrationInfo.getCountryID() == 0) || null == userRegistrationInfo.getCountryID())
		{
			userRegistrationInfo.setCountryID(1);
		}
		if (userRegistrationInfo.getIncomeRangeID() == 0)
		{
			userRegistrationInfo.setIncomeRangeID(null);
		}
		if (userRegistrationInfo.getMaritalStatus().equals("0"))
		{
			userRegistrationInfo.setMaritalStatus(null);
		}
		try
		{
			modifiedDate = Utility.getFormattedDate();
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			response = ApplicationConstants.FAILURE;
		}

		log.info("userID...." + userRegistrationInfo.getUserId());
		simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
		simpleJdbcCall.withProcedureName("usp_WebConsumerUserInfo");

		final MapSqlParameterSource registrationParameters = new MapSqlParameterSource();
		registrationParameters.addValue(ApplicationConstants.USERID, userRegistrationInfo.getUserId());
		registrationParameters.addValue("FirstName", userRegistrationInfo.getFirstName());
		registrationParameters.addValue("Lastname", userRegistrationInfo.getLastName());
		registrationParameters.addValue("Address1", userRegistrationInfo.getAddress1());
		registrationParameters.addValue("Address2", userRegistrationInfo.getAddress2());
		registrationParameters.addValue("City", userRegistrationInfo.getCity());
		registrationParameters.addValue("State", userRegistrationInfo.getState());
		registrationParameters.addValue("PostalCode", userRegistrationInfo.getPostalCode());
		registrationParameters.addValue("CountryID", userRegistrationInfo.getCountryID());
		registrationParameters.addValue("Gender", userRegistrationInfo.getGender());
		registrationParameters.addValue("DOB", userRegistrationInfo.getDob());
		registrationParameters.addValue("IncomeRangeID", userRegistrationInfo.getIncomeRangeID());
		registrationParameters.addValue("EducationLevelID", userRegistrationInfo.getEducationLevelId());
		registrationParameters.addValue("HomeOwner", userRegistrationInfo.getHomeOwner());
		registrationParameters.addValue("Children", userRegistrationInfo.getChildren());
		registrationParameters.addValue("MaritalStatus", userRegistrationInfo.getMaritalStatus());
		registrationParameters.addValue("DateModified", modifiedDate);
		registrationParameters.addValue("MobilePhone", Utility.removePhoneFormate(userRegistrationInfo.getMobileNumber()));

		try
		{
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(registrationParameters);
			result = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + errorNum + "errorMsg is" + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The DAO method used get All States.
	 * 
	 * @return List of states.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public final List<CountryCode> getAllStates() throws ScanSeeWebSqlException
	{
		final String methodName = "getAllStates";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> stateList = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			stateList = this.jdbcTemplate
					.query(PreferencesQueries.FETCHSTATEQUERY, ParameterizedBeanPropertyRowMapper.newInstance(CountryCode.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return stateList;
	}

	/**
	 * this method used get All cities.
	 * 
	 * @param stateName
	 *            -For which cities to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public final ArrayList<City> getAllCities(String stateName) throws ScanSeeWebSqlException
	{
		final String methodName = "getAllCities";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<City> cityList = null;
		try
		{
			cityList = this.jdbcTemplate.query("SELECT Distinct City FROM GeoPosition WHERE [State] =? order by City", new Object[] { stateName },
					new RowMapper<City>() {
						public City mapRow(ResultSet rs, int rowNum) throws SQLException
						{
							final City city = new City();

							city.setCityName(rs.getString("City"));

							return city;
						}

					});
		}
		catch (EmptyResultDataAccessException exception)
		{

			throw new ScanSeeWebSqlException(exception);

		}
		return (ArrayList<City>) cityList;
	}

	/**
	 * The DAO method to saves user password.
	 * 
	 * @param userID
	 *            For which password needs to be updated
	 * @param password
	 *            user new password
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeWebSqlException will be
	 *             thrown.
	 */
	public final String changePassword(Long userID, String password) throws ScanSeeWebSqlException
	{
		final String methodName = "changePassword";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc;
		Map<String, Object> resultFromProcedure = null;
		String response = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerChangePassword");
			final MapSqlParameterSource changePasswordParameters = new MapSqlParameterSource();
			changePasswordParameters.addValue("userid", userID);
			changePasswordParameters.addValue("password", password);
			resultFromProcedure = simpleJdbcCall.execute(changePasswordParameters);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in saveUserPreferenceDetails method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in changePassword", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}

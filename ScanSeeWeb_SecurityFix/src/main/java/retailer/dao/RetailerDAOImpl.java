package retailer.dao;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AccountType;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.Contact;
import common.pojo.ContactDetail;
import common.pojo.ContactType;
import common.pojo.Coupon;
import common.pojo.DropDown;
import common.pojo.Event;
import common.pojo.HotDealInfo;
import common.pojo.LocationResult;
import common.pojo.PlanInfo;
import common.pojo.Product;
import common.pojo.ProductHotDeal;
import common.pojo.ProductVO;
import common.pojo.Rebates;
import common.pojo.RetailProduct;
import common.pojo.Retailer;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerImages;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.RetailerLocationProduct;
import common.pojo.RetailerLocationProductJson;
import common.pojo.RetailerProfile;
import common.pojo.RetailerRegistration;
import common.pojo.SearchProductRetailer;
import common.pojo.SearchResultInfo;
import common.pojo.State;
import common.pojo.StoreInfoVO;
import common.pojo.TimeZones;
import common.util.EncryptDecryptPwd;
import common.util.Utility;

/**
 * RetailerDAOImpl implements RetailerDAOImpl methods.
 * 
 * @author Created by SPAN.
 */
@SuppressWarnings({ "rawtypes", "unchecked", "null" })
public class RetailerDAOImpl implements RetailerDAO {
	/**
	 * Variable YEARLY declared as String.
	 */
	private static final String YEARLY = "yearly";

	/**
	 * Variable MONTHLY declared as String.
	 */
	private static final String MONTHLY = "monthly";

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RetailerDAOImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * Variable platformTxManager declared as instance of
	 * PlatformTransactionManager.
	 */
	private PlatformTransactionManager platformTxManager;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * To set platformTransactionManager.
	 * 
	 * @param platformTxManager
	 *            to set.
	 */
	public void setPlatformTransactionManager(PlatformTransactionManager platformTxManager) {
		this.platformTxManager = platformTxManager;
	}

	/**
	 * This DAOImpl method save Retailer register details and success or failure
	 * depending upon the result of operation to service method .
	 * 
	 * @param objRetailReg
	 *            instance of RetailerRegistration.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public final String addRetailerRegistration(RetailerRegistration objRetailReg) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : addRetailerRegistration ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		EncryptDecryptPwd enryptDecryptpwd;
		String enryptPassword = null;
		boolean isDuplicateUserEmailID;
		String autogenPassword = null;
		// boolean isDuplicateRetailerName;
		try {
			// retailerName=objRetailReg.getRetailerName();
			// retailerName=retailerName.replaceAll("^\\s+", "");
			// retailerName=retailerName.replaceAll("\\s+$", "");
			// LOG.info("RETAILER NAME ========"+retailerName);
			enryptDecryptpwd = new EncryptDecryptPwd();
			autogenPassword = Utility.randomString(5);
			enryptPassword = enryptDecryptpwd.encrypt(autogenPassword);
			objRetailReg.setPassword(autogenPassword);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailRegisterParam = new MapSqlParameterSource();

			objRetailRegisterParam.addValue("RetailerName", objRetailReg.getRetailerName());
			objRetailRegisterParam.addValue("Address1", objRetailReg.getAddress1());
			objRetailRegisterParam.addValue("Address2", objRetailReg.getAddress2());
			objRetailRegisterParam.addValue("State", objRetailReg.getState());
			objRetailRegisterParam.addValue("City", objRetailReg.getCity());
			objRetailRegisterParam.addValue("PostalCode", objRetailReg.getPostalCode());
			objRetailRegisterParam.addValue("CorporatePhoneNo", Utility.removePhoneFormate(objRetailReg.getContactPhone()));
			/*
			 * objRetailRegisterParameter.addValue("LegalAuthorityFirstName",
			 * objRetailReg.getLegalAuthorityFName());
			 * objRetailRegisterParameter.addValue("LegalAuthorityLastName",
			 * objRetailReg.getLegalAuthorityLName());
			 */
			objRetailRegisterParam.addValue("ContactFirstName", objRetailReg.getContactFName());
			objRetailRegisterParam.addValue("ContactLastName", objRetailReg.getContactLName());
			objRetailRegisterParam.addValue("ContactPhone", Utility.removePhoneFormate(objRetailReg.getContactPhoneNo()));
			objRetailRegisterParam.addValue("ContactEmail", objRetailReg.getContactEmail());
			objRetailRegisterParam.addValue("Password", enryptPassword);
			objRetailRegisterParam.addValue("AdminFlag", objRetailReg.getAdminFlag());
			objRetailRegisterParam.addValue("CorporateAndSore", objRetailReg.isIslocation());
			objRetailRegisterParam.addValue("FlagRetailerLocationIDs", objRetailReg.getFlagDuplicateRet());

			if (objRetailReg.isIslocation()) {
				objRetailRegisterParam.addValue("StoreIdentification", objRetailReg.getStoreNum());
			} else {
				objRetailRegisterParam.addValue("StoreIdentification", null);
			}
			objRetailRegisterParam.addValue("BusinessCategory", objRetailReg.getbCategory());
			objRetailRegisterParam.addValue("NonProfitStatus", objRetailReg.isNonProfit());
			objRetailRegisterParam.addValue("UserName", objRetailReg.getUserName());
			objRetailRegisterParam.addValue("RetailerURL", objRetailReg.getWebUrl());
			objRetailRegisterParam.addValue("NumberOfLocations", objRetailReg.getNumOfStores());
			objRetailRegisterParam.addValue("RetailerKeyword", objRetailReg.getKeyword());
			objRetailRegisterParam.addValue("RetailerLatitude", objRetailReg.getRetailerLocationLatitude());
			objRetailRegisterParam.addValue("RetailerLongitude", objRetailReg.getRetailerLocationLongitude());
			objRetailRegisterParam.addValue("DuplicateRetailerID", objRetailReg.getDuplicateRetId());
			objRetailRegisterParam.addValue("DuplicateRetailerLocationID", objRetailReg.getDuplicateRetLocId());
			
			if(null != objRetailReg.getFilters() && !"".equals(objRetailReg.getFilters())) {
				objRetailRegisterParam.addValue("AdminFilterID", objRetailReg.getFilters());
			} else {
				objRetailRegisterParam.addValue("AdminFilterID", null);
			}
			if(null != objRetailReg.getFilterValues() && !"".equals(objRetailReg.getFilterValues())) {
				objRetailRegisterParam.addValue("AdminFilterValueID", objRetailReg.getFilterValues());
			} else {
				objRetailRegisterParam.addValue("AdminFilterValueID", null);
			}
			
			resultFromProcedure = simpleJdbcCall.execute(objRetailRegisterParam);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				isDuplicateUserEmailID = (Boolean) resultFromProcedure.get("DuplicateFlag");
				// isDuplicateRetailerName = (Boolean)
				// resultFromProcedure.get("DuplicateRetailerFlag");
				if (isDuplicateUserEmailID) {
					response = ApplicationConstants.DUPLICATE_USER;
					LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : DUPLICATE_USER : " + response);
				}
				/*
				 * else if (isDuplicateRetailerName) { response =
				 * ApplicationConstants.DUPLICATE_RETAILERNAME; LOG.info(
				 * "Inside RetailerDAOImpl : addRetailerRegistration : DUPLICATE_RETAILERNAME : "
				 * + response); }
				 */
				else {
					response = ApplicationConstants.SUCCESS;
					LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : SUCCESS : " + response);
				}
				LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : responseFromProc is : " + responseFromProc);
			} else {
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : addRetailerRegistration   : errorNumber  : " + errorNum + " errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (IllegalBlockSizeException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (InvalidKeyException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (InvalidAlgorithmParameterException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (InvalidKeySpecException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (NoSuchAlgorithmException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (NoSuchPaddingException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (BadPaddingException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	/**
	 * This method search the product description depending on the productName.
	 * 
	 * @param productName
	 * @return productName List based on parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public SearchResultInfo searchProductRetailer(String productName, int userId, int lowerLimit) throws ScanSeeWebSqlException {
		final String methodName = "searchProductRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		// log.info("Inside RetailerDAOImpl :searchProductRetailer ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<SearchProductRetailer> searchproductlist = null;

		SearchResultInfo searchresult = null;
		Integer rowcount = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("searchproductlist", new BeanPropertyRowMapper<SearchProductRetailer>(SearchProductRetailer.class));
			simpleJdbcCall.withProcedureName("usp_WebProductSearchRetailerOrSupplier");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchProduct = new MapSqlParameterSource();
			objSearchProduct.addValue("SearchParameter", productName);
			objSearchProduct.addValue("UserID", userId);
			objSearchProduct.addValue("LowerLimit", lowerLimit);
			resultFromProcedure = simpleJdbcCall.execute(objSearchProduct);
			searchproductlist = (ArrayList<SearchProductRetailer>) resultFromProcedure.get("searchproductlist");
			if (searchproductlist != null && !searchproductlist.isEmpty()) {
				searchresult = new SearchResultInfo();
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setRetailerproductList(searchproductlist);
				searchresult.setTotalSize(rowcount);
			}
		}

		catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :searchProductRetailer :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchresult;
	}

	/**
	 * This method will return the Rebate Name List based on parameter.
	 * 
	 * @param retailerId
	 *            ,
	 * @param rebateName
	 * @param lowerLimit
	 * @return RebateName List based on parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getRetailerByRebateName(Long retailerId, String rebateName, int lowerLimit) throws ScanSeeWebSqlException {
		final String methodName = "getRetailerByRebateName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		//Integer responseFromProc = null;
		List<Rebates> arRebateNameList = null;
		SearchResultInfo searchresult = null;
		Integer rowcount = null;
		searchresult = new SearchResultInfo();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRebateSearchRetailer");
			simpleJdbcCall.returningResultSet("searchRebateNameList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailerRebNameParameters = new MapSqlParameterSource();

			objRetailerRebNameParameters.addValue("RetailerID", retailerId);
			objRetailerRebNameParameters.addValue("RebateName", rebateName);
			objRetailerRebNameParameters.addValue("LowerLimit", lowerLimit);

			resultFromProcedure = simpleJdbcCall.execute(objRetailerRebNameParameters);
			if (null == resultFromProcedure.get("ErrorNumber")) {
				arRebateNameList = (ArrayList) resultFromProcedure.get("searchRebateNameList");
				if (arRebateNameList != null && !arRebateNameList.isEmpty()) {
					rowcount = (Integer) resultFromProcedure.get("RowCount");
					searchresult.setRebatesList(arRebateNameList);
					searchresult.setTotalSize(rowcount);
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside RetailerDAOImpl : getRetailerByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException("Inside RetailerDAOImpl : getRetailerByRebateName  : errorNumber  : " + errorNum + "errorMessage : "
						+ errorMsg);
			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getRetailerByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchresult;
	}

	/**
	 * This DAOImpl method return all the locations for that retailer based on
	 * input parameter and its locationList return to service method.
	 * 
	 * @param retailID
	 *            as request parameter.
	 * @return retaileLocationList, All the retailer locations.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final ArrayList<RetailerLocation> couponRetaileLocation(Long retailID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : couponRetaileLocation ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<RetailerLocation> retaileLocationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("retailelocationlist", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponRetailLocations");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailerLocation = new MapSqlParameterSource();
			objRetailerLocation.addValue("RetailID", retailID);
			resultFromProcedure = simpleJdbcCall.execute(objRetailerLocation);
			retaileLocationList = (ArrayList<RetailerLocation>) resultFromProcedure.get("retailelocationlist");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return retaileLocationList;
	}

	/**
	 * This method create New Coupon details.
	 * 
	 * @param objCoupon
	 *            instance of Coupon.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String addCoupon(Coupon objCoupon) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : addCoupon ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponInsertion");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objCouponParameter = new MapSqlParameterSource();
			objCouponParameter.addValue("RetailID", objCoupon.getRetailID());
			if (!"".equals(Utility.checkNull(objCoupon.getLocationID()))) {
				objCouponParameter.addValue("RetailLocationID", objCoupon.getLocationID());
			} else {
				objCouponParameter.addValue("RetailLocationID", null);
			}
			objCouponParameter.addValue("CouponName", objCoupon.getCouponName());
			objCouponParameter.addValue("NoOfCouponsToIssue", objCoupon.getNumOfCouponIssue());
			objCouponParameter.addValue("CouponShortDescription", objCoupon.getCouponShortDesc());
			objCouponParameter.addValue("CouponLongDescription", objCoupon.getCouponLongDesc());
			objCouponParameter.addValue("CouponTermsAndConditions", objCoupon.getCouponTermsCondt());

			if (!"".equals(Utility.checkNull(objCoupon.getCouponImgPath())) && !ApplicationConstants.BLANKIMAGE.equals(objCoupon.getCouponImgPath())) {
				objCouponParameter.addValue("CouponImagePath", objCoupon.getCouponImgPath());
			} else {
				objCouponParameter.addValue("CouponImagePath", null);
			}
			objCouponParameter.addValue("CouponStartDate", objCoupon.getCouponStartDate());
			objCouponParameter.addValue("CouponEndDate", objCoupon.getCouponExpireDate());
			objCouponParameter.addValue("CouponStartTime",
					String.valueOf(objCoupon.getCouponStartTimeHrs()) + ":" + String.valueOf(objCoupon.getCouponStartTimeMins()));
			objCouponParameter.addValue("CouponEndTime",
					String.valueOf(objCoupon.getCouponEndTimeHrs()) + ":" + String.valueOf(objCoupon.getCouponEndTimeMins()));

			if (objCoupon.getTimeZoneId() == 0) {
				objCouponParameter.addValue("CouponTimeZoneID", null);
			} else {
				objCouponParameter.addValue("CouponTimeZoneID", objCoupon.getTimeZoneId());
			}
			objCouponParameter.addValue("CouponFaceValue", objCoupon.getCouponDiscountAmt());
			if (!"".equals(Utility.checkNull(objCoupon.getProductID()))) {
				objCouponParameter.addValue("ProductIDs", objCoupon.getProductID());
			} else {
				objCoupon.setProductID(null);
				objCouponParameter.addValue("ProductIDs", objCoupon.getProductID());
			}

			resultFromProcedure = simpleJdbcCall.execute(objCouponParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				response = ApplicationConstants.SUCCESS;
				LOG.info("nside RetailerDAOImpl : addCoupon : Return response message : " + response);
			} else {
				response = ApplicationConstants.FAILURE;
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : addCoupon   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : addCoupon : " + exception.getMessage());
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	/**
	 * This method will return the Coupon Name List based on parameter.
	 * 
	 * @param strCouponName
	 * @return CouponName List based on parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<Product> getCouponByCouponName(String strCouponName, Long RetailerLocID, Long RetailID) throws ScanSeeWebSqlException {

		final String methodName = "getCouponByCouponName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		//Integer responseFromProc = null;

		ArrayList<Product> arProdNameList = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();
			if (RetailerLocID != null) {
				simpleJdbcCall.withProcedureName("usp_WebRetailerCouponProductSearch");
				simpleJdbcCall.returningResultSet("searchCouponNameList", new BeanPropertyRowMapper<Product>(Product.class));

				objCouponNameParameters.addValue("RetailID", RetailID);
				objCouponNameParameters.addValue("RetailLocationID", RetailerLocID);
				objCouponNameParameters.addValue("ProductSearch", strCouponName);
			} else {
				simpleJdbcCall.withProcedureName("usp_WebRetailerAddHotDealProductSearch");
				// simpleJdbcCall.returningResultSet("searchCouponNameList", new
				// BeanPropertyRowMapper<Product>(Product.class));

				simpleJdbcCall.returningResultSet("searchCouponNameList", new BeanPropertyRowMapper<Product>(Product.class));

				objCouponNameParameters.addValue("RetailID", RetailID);

				objCouponNameParameters.addValue("ProductSearch", strCouponName);
			}
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);
			arProdNameList = (ArrayList<Product>) resultFromProcedure.get("searchCouponNameList");

		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getCouponByCouponName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arProdNameList;
	}

	/**
	 * This method update the existing retailer profile
	 * 
	 * @param objRetProfile
	 *            instance of RetailerRegistraion.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public String updateProfile(RetailerRegistration objRetProfile) throws ScanSeeWebSqlException {
		final String methodName = "updateProfile";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			final MapSqlParameterSource objUpdateProfileParameter = new MapSqlParameterSource();
			objUpdateProfileParameter.addValue("RetailID", objRetProfile.getRetailID());
			objUpdateProfileParameter.addValue("RetailerName", objRetProfile.getRetailerName());
			objUpdateProfileParameter.addValue("Address1", objRetProfile.getAddress1());
			objUpdateProfileParameter.addValue("Address2", objRetProfile.getAddress2());
			objUpdateProfileParameter.addValue("City", objRetProfile.getCity());
			objUpdateProfileParameter.addValue("PostalCode", objRetProfile.getPostalCode());
			objUpdateProfileParameter.addValue("State", objRetProfile.getState());
			objUpdateProfileParameter.addValue("CorporatePhoneNo", Utility.removePhoneFormate(objRetProfile.getContactPhone()));
			objUpdateProfileParameter.addValue("LegalAuthorityFirstName", objRetProfile.getLegalAuthorityFName());
			objUpdateProfileParameter.addValue("LegalAuthorityLastName", objRetProfile.getLegalAuthorityLName());
			objUpdateProfileParameter.addValue("ContactFirstName", objRetProfile.getContactFName());
			objUpdateProfileParameter.addValue("ContactLastName", objRetProfile.getContactLName());
			objUpdateProfileParameter.addValue("ContactPhone", Utility.removePhoneFormate(objRetProfile.getContactPhone()));
			objUpdateProfileParameter.addValue("ContactEmail", objRetProfile.getContactEmail());

			objUpdateProfileParameter.addValue("AdminFlag", 1);
			objUpdateProfileParameter.addValue("BusinessCategoryIDs", objRetProfile.getbCategory());
			objUpdateProfileParameter.addValue("IsNonProfitOrganisation", objRetProfile.isNonProfit());
			objUpdateProfileParameter.addValue("RetailerURL", objRetProfile.getWebUrl());
			objUpdateProfileParameter.addValue("NumberOfLocations", objRetProfile.getNumOfStores());
			resultFromProcedure = simpleJdbcCall.execute(objUpdateProfileParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;

			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl : updateProfile  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}

		catch (DataAccessException exception) {
			LOG.error("Inside retaierDAOImpl : updateProfile  : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method update Existing Coupon details.
	 * 
	 * @param objCoupon
	 *            instance of Coupon.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	public String updateCouponInfo(Coupon objCoupon) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : updateCouponInfo ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objCouponParameter = new MapSqlParameterSource();
			objCouponParameter.addValue("RetailID", objCoupon.getRetailID());
			if (!"".equals(Utility.checkNull(objCoupon.getLocationID()))) {
				objCouponParameter.addValue("RetailLocationID", objCoupon.getLocationID());
			} else {
				objCouponParameter.addValue("RetailLocationID", null);
			}
			objCouponParameter.addValue("CouponName", objCoupon.getCouponName());
			objCouponParameter.addValue("NoOfCouponsToIssue", objCoupon.getNumOfCouponIssue());
			objCouponParameter.addValue("CouponID", objCoupon.getCouponID());
			objCouponParameter.addValue("CouponShortDescription", objCoupon.getCouponShortDesc());
			objCouponParameter.addValue("CouponLongDescription", objCoupon.getCouponLongDesc());
			objCouponParameter.addValue("CouponTermsAndConditions", objCoupon.getCouponTermsCondt());

			if (!"".equals(Utility.checkNull(objCoupon.getCouponImgPath())) && !ApplicationConstants.BLANKIMAGE.equals(objCoupon.getCouponImgPath())) {
				objCouponParameter.addValue("CouponImagePath", objCoupon.getCouponImgPath());
			} else {
				objCouponParameter.addValue("CouponImagePath", null);
			}
			objCouponParameter.addValue("CouponStartDate", objCoupon.getCouponStartDate());
			objCouponParameter.addValue("CouponEndDate", objCoupon.getCouponExpireDate());
			objCouponParameter.addValue("CouponStartTime",
					String.valueOf(objCoupon.getCouponStartTimeHrs()) + ":" + String.valueOf(objCoupon.getCouponStartTimeMins()));
			objCouponParameter.addValue("CouponEndTime",
					String.valueOf(objCoupon.getCouponEndTimeHrs()) + ":" + String.valueOf(objCoupon.getCouponEndTimeMins()));
			if (objCoupon.getTimeZoneId() == 0) {
				objCouponParameter.addValue("CouponTimeZoneID", null);
			} else {
				objCouponParameter.addValue("CouponTimeZoneID", objCoupon.getTimeZoneId());
			}
			objCouponParameter.addValue("CouponFaceValue", objCoupon.getCouponDiscountAmt());
			if (!"".equals(Utility.checkNull(objCoupon.getProductID()))) {
				objCouponParameter.addValue("ProductIDs", objCoupon.getProductID());
			} else {
				objCoupon.setProductID(null);
				objCouponParameter.addValue("ProductIDs", objCoupon.getProductID());
			}
			resultFromProcedure = simpleJdbcCall.execute(objCouponParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				response = ApplicationConstants.SUCCESS;
				LOG.info("Inside RetailerDAOImpl : updateCouponInfo : Return response message : " + response);
			} else {
				response = ApplicationConstants.FAILURE;
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : updateCouponInfo   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException exception) {
			response = ApplicationConstants.FAILURE;
			LOG.error("Inside RetailerDAOImpl : updateCouponInfo : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	/**
	 * This method will return the coupon details based on CouponID parameter.
	 * 
	 * @param CouponID
	 * @return CouponID List based on parameter.
	 * @throws Exception
	 */
	public ArrayList<Coupon> getCouponIDByCouponDetails(Long objCouponID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getCouponIDByCouponDetails ");
		Map<String, Object> resultFromProcedure = null;

		ArrayList<Coupon> arCoupnDetailsList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponDetails");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchCouponID", new BeanPropertyRowMapper<Coupon>(Coupon.class));

			final MapSqlParameterSource objCouponIDParameters = new MapSqlParameterSource();

			objCouponIDParameters.addValue("CouponID", objCouponID);
			resultFromProcedure = simpleJdbcCall.execute(objCouponIDParameters);

			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					arCoupnDetailsList = (ArrayList<Coupon>) resultFromProcedure.get("searchCouponID");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : getCouponIDByCouponDetails : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getCouponIDByCouponDetails : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return arCoupnDetailsList;
	}

	/**
	 * This method displays Coupon details by its couponID.
	 * 
	 * @param retailID
	 *            .
	 * @return List of Coupons based on retailID.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public SearchResultInfo retailerCouponDisplay(Long retailID, String searchKey, int lowerLimit, int iRecordCount) throws ScanSeeWebSqlException {
		final String methodName = "retailerCouponDisplay";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		ArrayList<Coupon> couponlist = null;
		SearchResultInfo searchresult = null;
		Integer rowcount = null;
		searchresult = new SearchResultInfo();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("couponlist", new BeanPropertyRowMapper<Coupon>(Coupon.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objCouponDisplay = new MapSqlParameterSource();
			objCouponDisplay.addValue("RetailID", retailID);
			objCouponDisplay.addValue("CouponSearch", searchKey);
			objCouponDisplay.addValue("LowerLimit", lowerLimit);
			objCouponDisplay.addValue("RecordCount", iRecordCount);
			resultFromProcedure = simpleJdbcCall.execute(objCouponDisplay);
			LOG.info("usp_WebRetailerCouponDisplay SP is excuted successfully");
			couponlist = (ArrayList<Coupon>) resultFromProcedure.get("couponlist");
			if (couponlist != null && !couponlist.isEmpty()) {
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setCouponsList(couponlist);
				searchresult.setTotalSize(rowcount);
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : retailerCouponDisplay  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setTotalSize(rowcount);

			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :retailerCouponDisplay :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchresult;

	}

	/**
	 * This method will return the ads that are created by a retailer based on
	 * strAdsName parameter.
	 * 
	 * @param objRetailID
	 *            logged in retailer id.
	 * @param strAdsName
	 *            contains search key.
	 * @param lowerLimit
	 *            used for pagination.
	 * @return AdsName List based on parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getRetailerAdsByAdsNames(Long objRetailID, String strAdsName, int lowerLimit, int recordCount)
			throws ScanSeeWebSqlException {
		final String methodName = "getRetailerAdsByAdsNames";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<RetailerLocationAdvertisement> arAdsNamesList = null;
		SearchResultInfo searchresult = null;
		Integer rowcount = null;
		/*
		 * Integer objDeleteFlag = null; Integer objExpireFlag = null;
		 */
		searchresult = new SearchResultInfo();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerWelcomePageDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchCouponID", new BeanPropertyRowMapper<RetailerLocationAdvertisement>(
					RetailerLocationAdvertisement.class));

			final MapSqlParameterSource objAdsNameParameters = new MapSqlParameterSource();

			objAdsNameParameters.addValue("RetailID", objRetailID);
			objAdsNameParameters.addValue("SearchParameter", strAdsName);
			objAdsNameParameters.addValue("LowerLimit", lowerLimit);
			objAdsNameParameters.addValue("RecordCount", recordCount);
			objAdsNameParameters.addValue("ShowAll", 1);

			resultFromProcedure = simpleJdbcCall.execute(objAdsNameParameters);
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
			arAdsNamesList = (ArrayList) resultFromProcedure.get("searchCouponID");

			if (arAdsNamesList != null && !arAdsNamesList.isEmpty()) {
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setAddsList(arAdsNamesList);
				searchresult.setTotalSize(rowcount);
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : getRetailerByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setTotalSize(rowcount);
			}
		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchresult;

	}

	/**
	 * This DAOImpl method create new banner page and its status return to
	 * service method.
	 * 
	 * @param retLocationAdvertisement
	 *            instance of RetailerLocationAdvertisement
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String retailerAddsInsertion(RetailerLocationAdvertisement retLocationAdvertisement) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : retailerAddsInsertion ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerWelcomePageCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailAddsInsertion = new MapSqlParameterSource();
			objRetailAddsInsertion.addValue("RetailID", retLocationAdvertisement.getRetailID());
			objRetailAddsInsertion.addValue("RetailLocationIds", retLocationAdvertisement.getRetailLocationIds());
			objRetailAddsInsertion.addValue("WelcomePageName", retLocationAdvertisement.getAdvertisementName());
			objRetailAddsInsertion.addValue("WelcomePageImagePath", retLocationAdvertisement.getStrBannerAdImagePath());
			objRetailAddsInsertion.addValue("StartDate", retLocationAdvertisement.getAdvertisementDate());
			if ("".equals(Utility.checkNull(retLocationAdvertisement.getAdvertisementEndDate()))) {
				retLocationAdvertisement.setAdvertisementEndDate(null);
			}
			objRetailAddsInsertion.addValue("EndDate", retLocationAdvertisement.getAdvertisementEndDate());

			resultFromProcedure = simpleJdbcCall.execute(objRetailAddsInsertion);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
				response = ApplicationConstants.SUCCESS;
				retLocationAdvertisement.setInvalidLocationIds((String) resultFromProcedure.get("InvalidRetailLocations"));
			} else
				if (null != responseFromProc && responseFromProc.intValue() == 1) {
					response = ApplicationConstants.FAILURE;
				}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	/**
	 * This DAOImpl method to update Retailer register details and its status
	 * will return to service method.
	 * 
	 * @param retailerProfile
	 *            instance of RetailerProfile.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String addRetailerUpdateProfile(RetailerProfile retailerProfile) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : addRetailerUpdateProfile ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		// boolean isDuplicateUserEmailID;
		boolean isDuplicateRetailerName;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebRetailerProfileUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objUpdateProfileParam = new MapSqlParameterSource();

			objUpdateProfileParam.addValue("RetailID", retailerProfile.getRetailID());
			objUpdateProfileParam.addValue("RetailerName", retailerProfile.getRetailerName());
			objUpdateProfileParam.addValue("Address1", retailerProfile.getAddress1());
			objUpdateProfileParam.addValue("Address2", retailerProfile.getAddress2());
			objUpdateProfileParam.addValue("State", retailerProfile.getState());
			objUpdateProfileParam.addValue("City", retailerProfile.getCity());
			objUpdateProfileParam.addValue("PostalCode", retailerProfile.getPostalCode());
			objUpdateProfileParam.addValue("CorporatePhoneNo", Utility.removePhoneFormate(retailerProfile.getContactPhone()));
			/*
			 * objUpdateProfileParameter.addValue("LegalAuthorityFirstName",
			 * objRetailer.getLegalAuthorityFName());
			 * objUpdateProfileParameter.addValue("LegalAuthorityLastName",
			 * objRetailer.getLegalAuthorityLName());
			 */
			objUpdateProfileParam.addValue("ContactFirstName", retailerProfile.getContactFName());
			objUpdateProfileParam.addValue("ContactLastName", retailerProfile.getContactLName());
			objUpdateProfileParam.addValue("ContactPhone", Utility.removePhoneFormate(retailerProfile.getContactPhoneNo()));
			objUpdateProfileParam.addValue("ContactEmail", retailerProfile.getContactEmail());
			objUpdateProfileParam.addValue("AdminFlag", 0);
			objUpdateProfileParam.addValue("CorporateAndStore", retailerProfile.isIslocation());
			objUpdateProfileParam.addValue("BusinessCategoryIDs", retailerProfile.getbCategory());
			objUpdateProfileParam.addValue("IsNonProfitOrganisation", retailerProfile.isNonProfit());
			objUpdateProfileParam.addValue("RetailerURL", retailerProfile.getWebUrl());
			objUpdateProfileParam.addValue("RetailerKeyword", retailerProfile.getKeyword());
			objUpdateProfileParam.addValue("RetailerLatitude", retailerProfile.getRetailerLocationLatitude());
			objUpdateProfileParam.addValue("RetailerLongitude", retailerProfile.getRetailerLocationLongitude());
			
			if(null != retailerProfile.getFilters() && !"".equals(retailerProfile.getFilters())) {
				objUpdateProfileParam.addValue("AdminFilterID", retailerProfile.getFilters());
			} else {
				objUpdateProfileParam.addValue("AdminFilterID", null);
			}
			if(null != retailerProfile.getFilterValues() && !"".equals(retailerProfile.getFilterValues())) {
				objUpdateProfileParam.addValue("AdminFilterValueID", retailerProfile.getFilterValues());
			} else {
				objUpdateProfileParam.addValue("AdminFilterValueID", null);
			}
			
			
			resultFromProcedure = simpleJdbcCall.execute(objUpdateProfileParam);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			// if (null != responseFromProc && responseFromProc.intValue() == 0)
			// {
			// response = ApplicationConstants.SUCCESS;
			// LOG.info("Inside RetailerDAOImpl : addRetailerUpdateProfile : SUCCESS : "
			// + response);
			// }
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				isDuplicateRetailerName = (Boolean) resultFromProcedure.get("DuplicateRetailer");
				if (isDuplicateRetailerName) {
					response = ApplicationConstants.DUPLICATE_RETAILERNAME;
					LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : DUPLICATE_RETAILERNAME : " + response);
				} else {
					response = ApplicationConstants.SUCCESS;
					LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : SUCCESS : " + response);
				}
				LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : responseFromProc is : " + responseFromProc);
			} else {
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : addRetailerUpdateProfile   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetailerProfileUpdation SP is executed successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	/**
	 * This DAOImpl method upload multiple locations with the help of CSV file
	 * templates and return status to service method.
	 * 
	 * @param retailerLocationList
	 *            instance of ArrayList<RetailerLocation>.
	 * @param userId
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String retailerLocationUpload(ArrayList<RetailerLocation> retailerLocationList, Long userId, int retailID)
			throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : retailerLocationUpload ");
		String response = null;
		Object[] values = null;
		final List<Object[]> batch = new ArrayList<Object[]>();
		try {
			final Calendar currentDate = Calendar.getInstance();
			final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MMM/dd HH:mm:ss");
			final String dateNow = formatter.format(currentDate.getTime());

			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			if (!retailerLocationList.isEmpty()) {
				for (RetailerLocation objMgProducts : retailerLocationList) {
					values = new Object[] { userId, objMgProducts.getStoreIdentification(), retailID, objMgProducts.getHeadquarters(),
							objMgProducts.getAddress1(), objMgProducts.getAddress2(), objMgProducts.getAddress3(), objMgProducts.getAddress4(),
							objMgProducts.getCity(), objMgProducts.getState(), objMgProducts.getPostalCode(), 1,
							objMgProducts.getRetailerLocationLatitude(), objMgProducts.getRetailerLocationLongitude(),
							objMgProducts.getRetailerLocationTimeZone(), dateNow, objMgProducts.getModifyDate(),
							objMgProducts.getRetailLocationStoreHours(), objMgProducts.getPhonenumber(), objMgProducts.getContactEmail(),
							objMgProducts.getContactLastName(), objMgProducts.getContactTitle(), objMgProducts.getContactFirstName(),
							objMgProducts.getContactMobilePhone(), objMgProducts.getRetailLocationUrl(), objMgProducts.getKeyword(),
							objMgProducts.getUploadImgName() };
					batch.add(values);
				}
				final int[] updateCounts = simpleJdbcTemplate.batchUpdate(
						"INSERT INTO UploadRetaillocations VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", batch);
				if (updateCounts.length != 0) {
					response = ApplicationConstants.SUCCESS;
				} else {
					response = ApplicationConstants.FAILURE;
				}
			} else {
				response = ApplicationConstants.EMPTY_LIST;
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	/**
	 * This DAOImpl method display Retailer register details and its status
	 * return to service layer.
	 * 
	 * @param iRetailID
	 *            as request parameter.
	 * @return RetailerProfile details as list.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final List<RetailerProfile> getRetailUpdateProfileByRetailID(int iRetailID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getRetailUpdateProfileByRetailID ");
		Map<String, Object> resultFromProcedure = null;
		List<RetailerProfile> arRetailInfoList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerProfileDisplay");
			simpleJdbcCall.returningResultSet("searchRetailerInfoList", new BeanPropertyRowMapper<RetailerProfile>(RetailerProfile.class));
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailInfoParameters = new MapSqlParameterSource();

			objRetailInfoParameters.addValue("RetailerID", iRetailID);
			resultFromProcedure = simpleJdbcCall.execute(objRetailInfoParameters);
			LOG.info("usp_WebRetailerProfileDisplay SP is excuted successfully");
			arRetailInfoList = (ArrayList) resultFromProcedure.get("searchRetailerInfoList");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arRetailInfoList;
	}

	/**
	 * This method ReRun the Retailer Profile
	 * 
	 * @param coupon
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	@SuppressWarnings("unused")
	public String retailerCouponReRun(Coupon objcoupon) throws ScanSeeWebSqlException {
		final String methodName = "retailerCouponReRun";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponReRun");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailCouponReRun = new MapSqlParameterSource();
			objRetailCouponReRun.addValue("RetailID", objcoupon.getRetailID());
			objRetailCouponReRun.addValue("ManufacturerID", objcoupon.getManufacturerID());
			objRetailCouponReRun.addValue("APIPartnerID", objcoupon.getApiPartnerID());
			objRetailCouponReRun.addValue("CouponName", objcoupon.getCouponName());
			objRetailCouponReRun.addValue("CouponDiscountType", objcoupon.getCouponDiscountType());
			objRetailCouponReRun.addValue("CouponAmount", objcoupon.getCouponDiscountAmt());
			objRetailCouponReRun.addValue("NoOfCouponsToIssue", objcoupon.getNumOfCouponIssue());
			objRetailCouponReRun.addValue("CouponShortDescription", objcoupon.getCouponShortDesc());
			objRetailCouponReRun.addValue("CouponLongDescription", objcoupon.getCouponLongDesc());
			objRetailCouponReRun.addValue("CouponTermsAndConditions", objcoupon.getCouponTermsCondt());
			objRetailCouponReRun.addValue("CouponDisplayExpirationDate", objcoupon.getCouponDisplayExpiratnDate());
			objRetailCouponReRun.addValue("CouponImagePath", objcoupon.getCouponImagePath());
			objRetailCouponReRun.addValue("CouponStartDate", objcoupon.getCouponStartDate());
			objRetailCouponReRun.addValue("CouponEndDate", objcoupon.getCouponExpireDate());
			objRetailCouponReRun.addValue("CouponStartTime", objcoupon.getCouponStartTimeHrs());
			objRetailCouponReRun.addValue("CouponEndTime", objcoupon.getCouponEndTimeHrs());
			objRetailCouponReRun.addValue("CouponURL", objcoupon.getCouponURL());
			objRetailCouponReRun.addValue("ExternalCoupon", objcoupon.getExternalCoupon());

			resultFromProcedure = simpleJdbcCall.execute(objRetailCouponReRun);

			if (null != responseFromProc && responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl :retailerCouponReRun  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetailerCouponReRun SP is excuted successfully");

		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :retailerCouponReRun :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method search the rebates of retailer based on retailerID and
	 * rebateName.
	 * 
	 * @param retailID
	 * @param rebateName
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<Rebates> rebateSearchRetailer(int retailID, String rebateName) throws ScanSeeWebSqlException {
		final String methodName = "rebateSearchRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		ArrayList<Rebates> rebateList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("rebateList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			simpleJdbcCall.withProcedureName("usp_WebRebateSearchRetailer");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchRebates = new MapSqlParameterSource();
			objSearchRebates.addValue("RetailerID", retailID);
			objSearchRebates.addValue("RebateName", rebateName);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebates);
			LOG.info("usp_WebRebateSearchRetailer SP is excuted successfully");
			rebateList = (ArrayList<Rebates>) resultFromProcedure.get("rebateList");

		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :rebateSearchRetailer:" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return rebateList;
	}

	/**
	 * This method insert the new rebates.
	 * 
	 * @param objRebates
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String retailerAddRebate(Rebates objRebates) throws ScanSeeWebSqlException {
		final String methodName = "retailerAddRebate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer responseFromProc = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerAddRebate");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objAddRetailerRebate = new MapSqlParameterSource();

			objAddRetailerRebate.addValue("UserID", objRebates.getUserID());
			objAddRetailerRebate.addValue("RetailerID", objRebates.getRetailerID());
			objAddRetailerRebate.addValue("RetailerLocationID", objRebates.getRetailerLocID());
			objAddRetailerRebate.addValue("RebateName", objRebates.getRebName());
			objAddRetailerRebate.addValue("RebateAmount", objRebates.getRebAmount());
			objAddRetailerRebate.addValue("RebateDescription", objRebates.getRebLongDescription());
			objAddRetailerRebate.addValue("RebateTermsAndCondition", objRebates.getRebTermCondtn());
			objAddRetailerRebate.addValue("RebateStartTime", objRebates.getRebStartTime());
			objAddRetailerRebate.addValue("RebateEndTime", objRebates.getRebEndTime());
			objAddRetailerRebate.addValue("RebateStartDate", objRebates.getRebStartDate());
			objAddRetailerRebate.addValue("RebateEndDate", objRebates.getRebEndDate());
			objAddRetailerRebate.addValue("NoOfRebatesIssued", objRebates.getNoOfrebatesIsued());
			objAddRetailerRebate.addValue("ProductID", objRebates.getProductID());
			if (objRebates.getRebateTimeZoneId() == 0) {
				objAddRetailerRebate.addValue("RebateTimeZoneID", null);
			} else {
				objAddRetailerRebate.addValue("RebateTimeZoneID", objRebates.getRebateTimeZoneId());
			}
			resultFromProcedure = simpleJdbcCall.execute(objAddRetailerRebate);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;

			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl :retailerAddRebate : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				// throw new ScanSeeWebSqlException(errorMsg);
			}

			LOG.info("usp_WebRetailerAddRebate SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :retailerAddRebate :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method displays the Rebates Information
	 * 
	 * @param retailID
	 * @param rebateSearch
	 * @return list of rebates based on retailId and rebateSearch
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<Rebates> retailerRebateDisplay(int retailID, String rebateSearch) throws ScanSeeWebSqlException {
		final String methodName = "retailerRebateDisplay";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<Rebates> rebateList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("rebateList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerRebateDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRebateDispaly = new MapSqlParameterSource();

			objRebateDispaly.addValue("RetailerID", retailID);
			objRebateDispaly.addValue("RebateSearch", rebateSearch);
			resultFromProcedure = simpleJdbcCall.execute(objRebateDispaly);
			rebateList = (ArrayList<Rebates>) resultFromProcedure.get("rebateList");
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
		}

		catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :retailerRebateDisplay:" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return rebateList;
	}

	/**
	 * This DAOImpl method will add one new HotDeals for retailer and return
	 * status to service method.
	 * 
	 * @param objHotDeal
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String retailerAddHotDeal(HotDealInfo objHotDeal) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : retailerAddHotDeal ");
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer responseFromProc = null;
		Boolean bPercentFlag = false;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("rebateList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerAddHotDeal");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objAddRetailerHoteDeal = new MapSqlParameterSource();
			objAddRetailerHoteDeal.addValue("RetailerID", objHotDeal.getRetailID());
			objAddRetailerHoteDeal.addValue("HotDealName", objHotDeal.getHotDealName());
			objAddRetailerHoteDeal.addValue("RegularPrice", objHotDeal.getPrice());
			objAddRetailerHoteDeal.addValue("SalePrice", objHotDeal.getSalePrice());
			objAddRetailerHoteDeal.addValue("HotDealDescription", objHotDeal.getHotDealShortDescription());
			objAddRetailerHoteDeal.addValue("HotDealLongDescription", objHotDeal.getHotDealLongDescription());
			objAddRetailerHoteDeal.addValue("HotDealTerms", objHotDeal.getHotDealTermsConditions());

			objAddRetailerHoteDeal.addValue("DealStartDate", objHotDeal.getDealStartDate());
			objAddRetailerHoteDeal.addValue("DealEndDate", objHotDeal.getDealEndDate());
			objAddRetailerHoteDeal.addValue("DealStartTime", objHotDeal.getDealStartTime());
			objAddRetailerHoteDeal.addValue("DealEndTime", objHotDeal.getDealEndTime());
			objAddRetailerHoteDeal.addValue("DealEndTime", objHotDeal.getDealEndTime());
			objAddRetailerHoteDeal.addValue("CategoryID", objHotDeal.getbCategory());
			/* Promotion Changes */
			// objAddRetailerHoteDeal.addValue("URL", objHotDeal.getUrl());
			objAddRetailerHoteDeal.addValue("CouponCode", objHotDeal.getCouponCode());
			objAddRetailerHoteDeal.addValue("Quantity", objHotDeal.getNumOfHotDeals());
			objAddRetailerHoteDeal.addValue("ExpirationDate", objHotDeal.getExpireDate());
			objAddRetailerHoteDeal.addValue("ExpirationTime", objHotDeal.getExpireTime());
			objAddRetailerHoteDeal.addValue("HotDealImagePath", objHotDeal.getProductImage());
			/*--Ends--*/
			/*
			 * if (objHotDeal.getCity() != null &&
			 * !objHotDeal.getCity().equals(""))
			 * objAddRetailerHoteDeal.addValue("PopulationCentreID",
			 * objHotDeal.getCity()); else
			 * objAddRetailerHoteDeal.addValue("PopulationCentreID", null);
			 */

			if (!"".equals(Utility.checkNull(objHotDeal.getProductId()))) {
				objAddRetailerHoteDeal.addValue("ProductID", objHotDeal.getProductId());
			} else {
				objAddRetailerHoteDeal.addValue("ProductID", null);
			}

			if ("City".equals(objHotDeal.getDealForCityLoc())) {
				objAddRetailerHoteDeal.addValue("PopulationCentreID", objHotDeal.getCity());
				objAddRetailerHoteDeal.addValue("RetailLocationID", null);
			} else
				if ("Location".equals(objHotDeal.getDealForCityLoc())) {
					objAddRetailerHoteDeal.addValue("PopulationCentreID", null);
					objAddRetailerHoteDeal.addValue("RetailLocationID", objHotDeal.getRetailerLocID());
				} else {
					objAddRetailerHoteDeal.addValue("PopulationCentreID", null);
					objAddRetailerHoteDeal.addValue("RetailLocationID", null);
				}

			/*
			 * if (objHotDeal.getRetailerLocID() == 0)
			 * objAddRetailerHoteDeal.addValue("RetailLocationID", null); else
			 * objAddRetailerHoteDeal.addValue("RetailLocationID",
			 * objHotDeal.getRetailerLocID());
			 */

			if (objHotDeal.getDealTimeZoneId() == 0) {
				objAddRetailerHoteDeal.addValue("HotDealTimeZoneID", null);
			} else {
				objAddRetailerHoteDeal.addValue("HotDealTimeZoneID", objHotDeal.getDealTimeZoneId());
			}
			resultFromProcedure = simpleJdbcCall.execute(objAddRetailerHoteDeal);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			bPercentFlag = (Boolean) resultFromProcedure.get(ApplicationConstants.PERCENTAGEFLAG);
			if (null != responseFromProc && responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS + ":" + bPercentFlag;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl :retailerAddHotDeal: errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetailerAddHotDeal SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	public SearchResultInfo retailerProductSetupProductSearch(String searchParameter, int lowerLimit, int rowCount) throws ScanSeeWebSqlException {
		final String methodName = "retailerProductSetupProductSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<ProductVO> searchProductList = new ArrayList<ProductVO>();

		SearchResultInfo searchresult = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("searchProductList", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerProductSetupProductSearch");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchProduct = new MapSqlParameterSource();

			objSearchProduct.addValue("SearchParameter", searchParameter);
			objSearchProduct.addValue("LowerLimit", lowerLimit);
			objSearchProduct.addValue("RecordCount", rowCount);
			resultFromProcedure = simpleJdbcCall.execute(objSearchProduct);
			searchProductList = (ArrayList<ProductVO>) resultFromProcedure.get("searchProductList");
			if (searchProductList != null && !searchProductList.isEmpty()) {
				searchresult = new SearchResultInfo();
				int totalSize = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setTotalSize(totalSize);
				searchresult.setProductList(searchProductList);
			}
			LOG.info("usp_WebRetailerProductSetupProductSearch SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :retailerProductSetupProductSearch :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchresult;
	}

	/**
	 * This method will return all the Retailer Location ID and Store
	 * Identification based on Retailer ID.
	 * 
	 * @param iRetailID
	 * @return Retailer Location ID and Store Identification List based on input
	 *         parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocation> getRetailProductSetupRetailLocationSearch(Long iRetailID) throws ScanSeeWebSqlException {
		final String methodName = "getRetailProductSetupRetailLocationSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		List<RetailerLocation> arLocIDStorIdentnList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("searchLocationIDList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerProductSetupRetailLocationSearch");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailIDParameters = new MapSqlParameterSource();

			objRetailIDParameters.addValue("RetailerID", iRetailID);
			resultFromProcedure = simpleJdbcCall.execute(objRetailIDParameters);
			arLocIDStorIdentnList = (ArrayList<RetailerLocation>) resultFromProcedure.get("searchLocationIDList");
			LOG.info("usp_WebRetailerProductSetupRetailLocationSearch SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :getRetailProductSetupRetailLocationSearch:" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arLocIDStorIdentnList;
	}

	/**
	 * This method Add Retail Product Setup Search Results Modification details.
	 * 
	 * @param objRetailLoctnProduct
	 *            instance of RetailerLocationProduct.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	public String associateRetailerProd(RetailerLocationProduct objRetailLoctnProduct) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : associateRetailerProd");
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebRetailerProductSetupSearchResultsModification");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objaddRetProductParameter = new MapSqlParameterSource();

			objaddRetProductParameter.addValue("RetailID", objRetailLoctnProduct.getRetailID());
			if ("".equals(Utility.checkNull(objRetailLoctnProduct.getRetailLocationIDs()))) {
				objRetailLoctnProduct.setLocID(null);
				objaddRetProductParameter.addValue("RetailLocationID", objRetailLoctnProduct.getLocID());
			} else {
				objaddRetProductParameter.addValue("RetailLocationID", objRetailLoctnProduct.getRetailLocationIDs());
			}
			objaddRetProductParameter.addValue("ProductID", objRetailLoctnProduct.getProductID());
			objaddRetProductParameter.addValue("Price", objRetailLoctnProduct.getPrice());
			objaddRetProductParameter.addValue("Description", objRetailLoctnProduct.getRetailLocationProductDesc());
			objaddRetProductParameter.addValue("SalePrice", objRetailLoctnProduct.getSalePrice());
			objaddRetProductParameter.addValue("ApplyAll", objRetailLoctnProduct.getApplyAll());

			if (!Utility.checkNull(objRetailLoctnProduct.getSaleStartDate()).equals("")) {
				objaddRetProductParameter.addValue("SaleStartDate", Utility.getFormattedDateTime(objRetailLoctnProduct.getSaleStartDate()));
			} else {
				objaddRetProductParameter.addValue("SaleStartDate", null);
			}
			if (!Utility.checkNull(objRetailLoctnProduct.getSaleEndDate()).equals("")) {
				objaddRetProductParameter.addValue("SaleEndDate", Utility.getFormattedDateTime(objRetailLoctnProduct.getSaleEndDate()));

			} else {
				objaddRetProductParameter.addValue("SaleEndDate", null);
			}

			resultFromProcedure = simpleJdbcCall.execute(objaddRetProductParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl : associateRetailerProd : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : associateRetailerProd :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		} catch (ParseException exception) {
			LOG.error("Inside RetailerDAOImpl : associateRetailerProd : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return response;
	}

	/**
	 * This DAOImpl method will return all list of Retailer Location/Store
	 * Identification by add new location, upload multiple locations with the
	 * help of CSV file templates and return status to service method.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final SearchResultInfo getLocationList(int retailerId, long userId, int lowerLimit) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getLocationList");
		Map<String, Object> resultFromProcedure = null;
		List<RetailerLocation> locationList = null;
		SearchResultInfo searchInfo = null;
		try {
			searchInfo = new SearchResultInfo();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("arLocationList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerUploadRetailerLocationsDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailIDParameters = new MapSqlParameterSource();

			objRetailIDParameters.addValue("RetailID", retailerId);
			objRetailIDParameters.addValue("UserID", userId);
			objRetailIDParameters.addValue("LowerLimit", lowerLimit);
			resultFromProcedure = simpleJdbcCall.execute(objRetailIDParameters);
			// locationList = (ArrayList<RetailerLocation>)
			// resultFromProcedure.get("LocationList");

			if (resultFromProcedure != null) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum) {
					locationList = (ArrayList<RetailerLocation>) resultFromProcedure.get("arLocationList");

					if (null != locationList && !locationList.isEmpty()) {
						final int totalSize = (Integer) resultFromProcedure.get("TotalRecords");
						searchInfo.setTotalSize(totalSize);
					}
					searchInfo.setLocationList(locationList);
				} else {
					LOG.info(ApplicationConstants.EXCEPTION_OCCURED + errorNum + "errorMsg -->" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebRetailerUploadRetailerLocationsDisplay SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return searchInfo;
	}

	/**
	 * This DAOImpl method updates location on changes and its status return to
	 * service method.
	 * 
	 * @param locationList
	 *            instance of List<RetailerLocation>
	 * @param userId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @param fileName
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String updateRetailerLocationSetUp(List<RetailerLocation> locationList, long userId, int retailerId, String fileName)
			throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : retailerLocationUpload ");
		String response = null;
		Object[] values = null;
		final List<Object[]> batch = new ArrayList<Object[]>();

		try {
			final Calendar currentDate = Calendar.getInstance();
			final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MMM/dd HH:mm:ss");
			final String dateNow = formatter.format(currentDate.getTime());

			final String updateQuery = "UPDATE UploadRetaillocations SET  UserID = ?," + " StoreIdentification = ?, " + "RetailID = ?,"
					+ " Headquarters = ?, " + "Address1 = ?," + " Address2 = ?, " + "Address3 = ?, " + "Address4 = ?," + " City = ?," + " State = ?,"
					+ " PostalCode =  ?," + " CountryID = ?," + " RetailLocationLatitude = ?," + " RetailLocationLongitude = ?, "
					+ "RetailLocationTimeZone = ?, " + "DateCreated = ?, " + "DateModified = ?, " + "RetailLocationStoreHours = ?, "
					+ "PhoneNumber = ?, " + "RetailLocationURL =?, " + "RetailKeyword =?," + "RetailLocationImagePath =?"
					+ "WHERE UploadRetaillocationsID = ?";

			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			for (RetailerLocation objRetLocation : locationList) {
				values = new Object[] { userId, objRetLocation.getStoreIdentification(), retailerId, objRetLocation.getHeadquarters(),
						objRetLocation.getAddress1(), objRetLocation.getAddress2(), objRetLocation.getAddress3(), objRetLocation.getAddress4(),
						objRetLocation.getCity(), objRetLocation.getState(), objRetLocation.getPostalCode(), 1,
						objRetLocation.getRetailerLocationLatitude(), objRetLocation.getRetailerLocationLongitude(),
						objRetLocation.getRetailerLocationTimeZone(), dateNow, objRetLocation.getModifyDate(),
						objRetLocation.getRetailLocationStoreHours(), objRetLocation.getPhonenumber().trim(), objRetLocation.getRetailLocationUrl(),
						objRetLocation.getKeyword(), objRetLocation.getGridImgLocationPath(), objRetLocation.getUploadRetaillocationsID() };
				LOG.info("userId : " + userId + "\nretailerId : " + retailerId + "\nUploadRetaillocationsID : "
						+ objRetLocation.getUploadRetaillocationsID());
				batch.add(values);
			}
			final int[] updateCounts = simpleJdbcTemplate.batchUpdate(updateQuery, batch);
			if (updateCounts.length != 0) {
				response = ApplicationConstants.SUCCESS;
				LOG.info("Data inserted into staging table status:::::::::::::::" + response);
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	/**
	 * This method upload a file containing product Image path.
	 * 
	 * @param retailID
	 *            , imagePath.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	public String UpdateRetailerUploadLogo(Long retailID, String strLogoPath) throws ScanSeeWebSqlException {
		final String methodName = "UpdateRetailerUploadLogo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerUploadLogo");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objaddUploadParameter = new MapSqlParameterSource();

			objaddUploadParameter.addValue("RetailerID", retailID);
			objaddUploadParameter.addValue("RetailerLogo", strLogoPath);
			LOG.info("retailID  " + retailID);
			resultFromProcedure = simpleJdbcCall.execute(objaddUploadParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null == responseFromProc || responseFromProc.intValue() == 0) {
				strResponse = ApplicationConstants.SUCCESS;

			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl :UpdateRetailerUploadLogo : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);

				throw new ScanSeeWebSqlException(errorMsg);

			}
			LOG.info("usp_WebRetailerUploadLogo SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : UpdateRetailerUploadLogo : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This method deletes the Product details that has been selected from list.
	 * 
	 * @param RetailID
	 *            ,ProductID
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteProductsFromLocHoldControl(Long retailID, Long productID) throws ScanSeeWebSqlException {
		final String methodName = "deleteProductsFromLocHoldControl";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerDeleteProduct");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailID);
			objDelProductsParameter.addValue("ProductID", productID);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
			LOG.info("usp_WebRetailerDeleteProduct SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : deleteProductsFromLocHoldControl : " + exception);
			if (null == responseFromProc || responseFromProc.intValue() != 0) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetaierDAOImpl :deleteProductsFromLocHoldControl : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			strResponse = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	public String addBatchUploadProductsToStage(ArrayList<RetailProduct> prodList, int retailID, long userId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetaierDAOImpl : addBatchUploadProductsToStage ");
		String response = null;

		Object[] values = null;
		List<Object[]> batch = new ArrayList<Object[]>();
		TransactionStatus objTxStatus = null;
		try {
			// Create a new transaction, suspending the current transaction if
			// exists!.
			DefaultTransactionDefinition objDefaultTxDefn = new DefaultTransactionDefinition();
			objTxStatus = platformTxManager.getTransaction(objDefaultTxDefn);
			SimpleJdbcTemplate simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			for (RetailProduct objMgProducts : prodList) {
				values = new Object[] { Utility.checkNull(objMgProducts.getStoreIdentificationID()), Utility.checkNull(objMgProducts.getScanCode()),
						objMgProducts.getLongDescription(), null, objMgProducts.getProductPrice(), objMgProducts.getProductSalePrice(),
						Utility.getFormattedDateTime(objMgProducts.getSaleStartDate()), Utility.getFormattedDateTime(objMgProducts.getSaleEndDate()),
						null, userId, retailID };
				batch.add(values);
			}
			int[] updateCounts = simpleJdbcTemplate.batchUpdate("INSERT INTO UploadRetailLocationProduct VALUES (?,?,?,?,?,?,?,?,?,?,?)", batch);
			if (updateCounts.length != 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				response = ApplicationConstants.FAILURE;
			}
			platformTxManager.commit(objTxStatus);
		} catch (DataAccessException exception) {
			platformTxManager.rollback(objTxStatus);
			LOG.error("Inside RetaierDAOImpl : addBatchUploadProductsToStage : " + exception.getMessage());
		} catch (Exception exception) {
			platformTxManager.rollback(objTxStatus);
			LOG.error("Inside RetaierDAOImpl : addBatchUploadProductsToStage : " + exception.getMessage());
		}
		return response;
	}

	public String locationListDelete(long userId, int retailId) throws ScanSeeWebSqlException {
		final String methodName = "locationListDelete";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebDeleteRetailLocationStageTable");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objaddRetProductParameter = new MapSqlParameterSource();

			objaddRetProductParameter.addValue("UserID", userId);
			objaddRetProductParameter.addValue("RetailerID", retailId);

			resultFromProcedure = simpleJdbcCall.execute(objaddRetProductParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl :addRetailProductSetupSearchResultsModification : errorNumber  : " + errorNum + "errorMessage : "
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :getRetailProductSetupRetailLocationSearch:" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * @param userId
	 * @param retailId
	 * @param fileName
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public LocationResult moveLocationListFrmStageToProductionTable(long userId, int retailId, String fileName) throws ScanSeeWebSqlException {
		final String methodName = "moveLocationListFrmStageToProductionTable";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer responseFromProc = null;
		Integer count = null;
		LocationResult locationResult = new LocationResult();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebUploadRetailerLocations");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objaddRetProductParameter = new MapSqlParameterSource();

			objaddRetProductParameter.addValue("RetailerID", retailId);
			objaddRetProductParameter.addValue("UserID", userId);
			objaddRetProductParameter.addValue("RetailLocationFileName", fileName);

			resultFromProcedure = simpleJdbcCall.execute(objaddRetProductParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			count = (Integer) resultFromProcedure.get("FailureCount");
			if (null != responseFromProc && responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl :addRetailProductSetupSearchResultsModification : errorNumber  : " + errorNum + "errorMessage : "
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebUploadRetailerLocations SP is excuted successfully");
			locationResult.setFailureCount(count);
			locationResult.setStatus(response);
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :getRetailProductSetupRetailLocationSearch:" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return locationResult;

	}

	public ArrayList<PlanInfo> fetchAllPlanList() throws ScanSeeWebSqlException {

		final String methodName = "fetchAllPlanList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<PlanInfo> planList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerBillingPlanDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("planList", new BeanPropertyRowMapper<PlanInfo>(PlanInfo.class));

			final SqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			planList = (ArrayList<PlanInfo>) resultFromProcedure.get("planList");
			LOG.info("usp_WebRetailerBillingPlanDisplay SP is excuted successfully");

		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :fetchAllPlanList:" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return planList;

	}

	/**
	 * The DAOImpl method for displaying all the states and it return to service
	 * method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return arStatesList,List of states.
	 */
	public ArrayList<State> getAllStates() throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllStates ");
		List<State> arStatesList = null;
		try {
			arStatesList = this.jdbcTemplate.query("SELECT StateName,Stateabbrevation FROM [State] order by StateName", new RowMapper<State>() {
				public State mapRow(ResultSet rs, int rowNum) throws SQLException {
					final State state = new State();
					state.setStateName(rs.getString("StateName"));
					state.setStateabbr(rs.getString("Stateabbrevation"));

					return state;
				}

			});
		} catch (EmptyResultDataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return (ArrayList<State>) arStatesList;
	}

	public ArrayList<Retailer> getRetailersForProducts(String productId) throws ScanSeeServiceException, ScanSeeWebSqlException {
		final String methodName = "getRetailersForProducts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		// Integer responseFromProc = null;
		ArrayList<Retailer> listPdtRetailer = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebHotDealRetrieveRetailer");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("listRetailerList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("ProductID", productId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			// responseFromProc = (Integer)
			// resultFromProcedure.get(ApplicationConstants.STATUS);
			listPdtRetailer = (ArrayList) resultFromProcedure.get("listRetailerList");
			LOG.info("usp_WebHotDealRetrieveRetailer SP is excuted successfully");
		} catch (Exception exception) {
			LOG.error("Inside RetailerDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return listPdtRetailer;
	}

	/**
	 * This DAOImpl method return product Hot deals info based on input
	 * parameter and its status return to service layer.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param pdtName
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return SearchResultInfo instance.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final SearchResultInfo getDealsForDisplay(Long userId, String pdtName, int lowerLimit, int recordCount) throws ScanSeeWebSqlException {
		LOG.info("Inside SuplierDaoImpl : getDealsForDisplay ");
		Map<String, Object> resultFromProcedure = null;
		List<HotDealInfo> listHotDeals = null;
		SearchResultInfo searchresult = null;
		Integer rowcount = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerHotDealSearch");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("dealsSearchList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailerID", userId);
			objSearchRebParameter.addValue("ProductHotDealID", pdtName);
			objSearchRebParameter.addValue("LowerLimit", lowerLimit);
			objSearchRebParameter.addValue("RecordCount", recordCount);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			listHotDeals = (ArrayList) resultFromProcedure.get("dealsSearchList");

			if (listHotDeals != null && !listHotDeals.isEmpty()) {
				searchresult = new SearchResultInfo();
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setDealList(listHotDeals);
				searchresult.setTotalSize(rowcount);
			} else {
				searchresult = new SearchResultInfo();
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setTotalSize(0);
				if (errorNum != null && errorMsg != null) {
					LOG.info("Inside RetailerDAOImpl : getDealsForDisplay  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				}
			}
			LOG.info("usp_WebRetailerHotDealSearch SP is excuted successfully");
		} catch (Exception exception) {
			LOG.error("Inside RetailerDAOImpl : getDealsForDisplay : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return searchresult;
	}

	/**
	 * This method get Uploaded RetailLocation Product details from Stage table
	 * to Production table. 'userID','retailID','strFileName'.
	 * 
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String getUploadRetailLocationProductsFromStage(Long userID, Long retailID, String strFileName) throws ScanSeeWebSqlException {
		final String methodName = "getUploadRetailLocationProductsFromStage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebUploadRetailerProducts");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetLocProductParameter = new MapSqlParameterSource();

			objRetLocProductParameter.addValue("UserID", userID);
			objRetLocProductParameter.addValue("RetailerID", retailID);
			objRetLocProductParameter.addValue("FileName", strFileName);

			resultFromProcedure = simpleJdbcCall.execute(objRetLocProductParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getUploadRetailLocationProductsFromStage : " + exception.getMessage());
			if (responseFromProc == null || responseFromProc.intValue() != 0) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				strResponse = ApplicationConstants.FAILURE;
				LOG.info("Inside retaierDAOImpl :getUploadRetailLocationProductsFromStage: errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This method delete Uploaded Retail location details from Stage table.
	 * 'userID','retailID','strFileName'.
	 * 
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public String deleteUploadRetailLocationProductsFromStage(Long userID, Long retailID, String strFileName) throws ScanSeeWebSqlException {
		final String methodName = "deleteUploadRetailLocationProductsFromStage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try { // PROPAGATION_REQUIRES_NEW - Create a new transaction, suspending
				// the current transaction if one exists.
			//final TransactionDefinition txDefn = new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
			// txStatus = txManager.getTransaction(txDefn);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebDeleteRetailerProductStageTable");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetLocProductParameter = new MapSqlParameterSource();

			objRetLocProductParameter.addValue("UserID", userID);
			objRetLocProductParameter.addValue("RetailerID", retailID);
			objRetLocProductParameter.addValue("RetailLocationFileName", strFileName);

			resultFromProcedure = simpleJdbcCall.execute(objRetLocProductParameter);
			// txManager.commit(txStatus);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : deleteUploadRetailLocationProductsFromStage : " + exception.getMessage());
			if (responseFromProc == null || responseFromProc.intValue() != 0) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				strResponse = ApplicationConstants.FAILURE;
				LOG.info("Inside retaierDAOImpl :deleteUploadRetailLocationProductsFromStage: errorNumber  : " + errorNum + "errorMessage : "
						+ errorMsg);
			}
			// txManager.rollback(txStatus);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	public ArrayList<Rebates> getRebateProductToAssociate(String strRebateName, Long retailID, int retailLocId) throws ScanSeeWebSqlException {

		final String methodName = "getRebateProductToAssociate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<Rebates> arProdNameList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponProductSearch");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchCouponNameList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();
			objCouponNameParameters.addValue("RetailID", retailID);
			objCouponNameParameters.addValue("RetailLocationID", retailLocId);
			objCouponNameParameters.addValue("ProductSearch", strRebateName);
			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);
			arProdNameList = (ArrayList<Rebates>) resultFromProcedure.get("searchCouponNameList");
			LOG.info("usp_WebRetailerCouponProductSearch SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getRebateProductToAssociate : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arProdNameList;
	}

	/**
	 * 
	 */
	public List<RetailerLocation> getRetailerLocations(long retailerID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getRetailerLocations ");
		
		Map<String, Object> resultFromProcedure = null;
		List<RetailerLocation> retailerLocList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponRetailLocations");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchRetLocList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			final MapSqlParameterSource objRetLocParameters = new MapSqlParameterSource();
			objRetLocParameters.addValue("RetailID", retailerID);
			resultFromProcedure = simpleJdbcCall.execute(objRetLocParameters);
			retailerLocList = (ArrayList<RetailerLocation>) resultFromProcedure.get("searchRetLocList");
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getRetailerLocations : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return retailerLocList;
	}

	public String savePlan(List<PlanInfo> pInfoList, int manufacturerID, long userId) throws ScanSeeWebSqlException {

		final String methodName = "savePlan";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String response = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerChoosePlan");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<PlanInfo>(PlanInfo.class));
			final MapSqlParameterSource objParameter = new MapSqlParameterSource();

			for (PlanInfo planInfo : pInfoList) {

				objParameter.addValue("RetailerID", manufacturerID);
				objParameter.addValue("UserID", userId);
				objParameter.addValue("BillingPlanRetailerIDs", planInfo.getProductId());
				if ((planInfo.getQuantity() == null) || (planInfo.getQuantity().equals(""))) {

					objParameter.addValue("Quantitys", null);
				} else {
					objParameter.addValue("Quantitys", planInfo.getQuantity());
				}
				objParameter.addValue("TotalPrice", planInfo.getGrandTotal());

				/*
				 * objParameter.addValue("ACHorBankInformationFlag", false);
				 * objParameter.addValue("AccountTypeID", null);
				 * objParameter.addValue("BankName", null);
				 * objParameter.addValue("RoutingNumber", null);
				 * objParameter.addValue("AccountNumber", null);
				 * objParameter.addValue("AccountHolderName", null);
				 * objParameter.addValue("BillingAddress", null);
				 * objParameter.addValue("City", null);
				 * objParameter.addValue("State", null);
				 * objParameter.addValue("Zip", null);
				 * objParameter.addValue("PhoneNumber", null);
				 * objParameter.addValue("CreditCardFlag", null);
				 * objParameter.addValue("CreditCardNumber", null);
				 * objParameter.addValue("ExpirationDate", null);
				 * objParameter.addValue("CVV", null);
				 * objParameter.addValue("CardholderName", null);
				 * objParameter.addValue("CreditCardBillingAddress", null);
				 */
				objParameter.addValue("DiscountPlanRetailerID", null);
				objParameter.addValue("RetailerProductFeeFlag", 1);

				resultFromProcedure = simpleJdbcCall.execute(objParameter);
				LOG.info("usp_WebRetailerChoosePlan SP is excuted successfully");
			}

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : getHotDealByID  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		} catch (Exception exception) {
			LOG.error("Inside SupplierDAOImpl : getHotDealByID : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	/**
	 * This DAOImpl method will update existing HotDeals for retailer product
	 * and its status return to service method.
	 * 
	 * @param objProdtHotDeal
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String updateRetailerProductHotDeal(HotDealInfo objProdtHotDeal) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : updateRetailerProductHotDeal ");
		String strResponse = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Boolean bPercentFlag = false;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerHotDealModification");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objUpdateHotDealParameter = new MapSqlParameterSource();
			objUpdateHotDealParameter.addValue("RetailerID", objProdtHotDeal.getRetailID());
			objUpdateHotDealParameter.addValue("HotDealID", objProdtHotDeal.getHotDealID());
			objUpdateHotDealParameter.addValue("HotDealName", objProdtHotDeal.getHotDealName());
			objUpdateHotDealParameter.addValue("SalePrice", objProdtHotDeal.getSalePrice());

			objUpdateHotDealParameter.addValue("RegularPrice", objProdtHotDeal.getPrice());

			objUpdateHotDealParameter.addValue("HotDealShortDescription", objProdtHotDeal.getHotDealShortDescription());
			objUpdateHotDealParameter.addValue("HotDealLongDescription", objProdtHotDeal.getHotDealLongDescription());
			objUpdateHotDealParameter.addValue("HotDealTerms", objProdtHotDeal.getHotDealTermsConditions());

			objUpdateHotDealParameter.addValue("DealStartDate", objProdtHotDeal.getDealStartDate());
			objUpdateHotDealParameter.addValue("DealStartTime", objProdtHotDeal.getDealStartTime());
			objUpdateHotDealParameter.addValue("DealEndDate", objProdtHotDeal.getDealEndDate());
			objUpdateHotDealParameter.addValue("DealEndTime", objProdtHotDeal.getDealEndTime());

			/* Promotion Changes */
			// objUpdateHotDealParameter.addValue("URL",
			// objProdtHotDeal.getUrl());
			objUpdateHotDealParameter.addValue("CouponCode", objProdtHotDeal.getCouponCode());
			objUpdateHotDealParameter.addValue("Quantity", objProdtHotDeal.getNumOfHotDeals());
			objUpdateHotDealParameter.addValue("ExpirationDate", objProdtHotDeal.getExpireDate());
			objUpdateHotDealParameter.addValue("ExpirationTime", objProdtHotDeal.getExpireTime());
			objUpdateHotDealParameter.addValue("HotDealImagePath", objProdtHotDeal.getProductImage());
			/* Promotion Changes End */
			/*
			 * if (objProdtHotDeal.getCity() != null)
			 * objUpdateHotDealParameter.addValue("PopulationCentreID",
			 * objProdtHotDeal.getCity()); else
			 * objUpdateHotDealParameter.addValue("PopulationCentreID", null);
			 */

			if (objProdtHotDeal.getProductId() != null) {
				objUpdateHotDealParameter.addValue("ProductIDs", objProdtHotDeal.getProductId());
			} else {
				objUpdateHotDealParameter.addValue("ProductIDs", null);
			}
			/*
			 * if (objProdtHotDeal.getRetailerLocID() == 0)
			 * objUpdateHotDealParameter.addValue("RetailerLocationID", null);
			 * else objUpdateHotDealParameter.addValue("RetailerLocationID",
			 * objProdtHotDeal.getRetailerLocID());
			 */

			if (objProdtHotDeal.getDealForCityLoc().equals("City")) {
				objUpdateHotDealParameter.addValue("PopulationCentreID", objProdtHotDeal.getCity());
				objUpdateHotDealParameter.addValue("RetailerLocationID", null);
			} else
				if (objProdtHotDeal.getDealForCityLoc().equals("Location")) {
					objUpdateHotDealParameter.addValue("PopulationCentreID", null);
					objUpdateHotDealParameter.addValue("RetailerLocationID", objProdtHotDeal.getRetailerLocID());
				} else {
					objUpdateHotDealParameter.addValue("PopulationCentreID", null);
					objUpdateHotDealParameter.addValue("RetailerLocationID", null);
				}
			if (objProdtHotDeal.getDealTimeZoneId() == 0) {
				objUpdateHotDealParameter.addValue("HotDealTimeZoneID", null);
			} else {
				objUpdateHotDealParameter.addValue("HotDealTimeZoneID", objProdtHotDeal.getDealTimeZoneId());
			}
			objUpdateHotDealParameter.addValue("CategoryID", objProdtHotDeal.getbCategory());
			resultFromProcedure = simpleJdbcCall.execute(objUpdateHotDealParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			bPercentFlag = (Boolean) resultFromProcedure.get(ApplicationConstants.PERCENTAGEFLAG);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				strResponse = ApplicationConstants.SUCCESS + ":" + bPercentFlag;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : updateRetailerProductHotDeal : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetailerHotDealModification SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return strResponse;
	}

	/**
	 * This DAOImpl method return product Hot deals info based on input
	 * parameter and its status return to service method.
	 * 
	 * @param hotDealID
	 *            as request parameter.
	 * @return Hot deals details.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final List<HotDealInfo> getProductHotDealByID(int hotDealID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getProductHotDealByID ");
		Map<String, Object> resultFromProcedure = null;
		List<HotDealInfo> arHotDealList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerDisplayHotDeal");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource objSearchDealParameter = new MapSqlParameterSource();
			objSearchDealParameter.addValue("HotDealID", hotDealID);

			resultFromProcedure = simpleJdbcCall.execute(objSearchDealParameter);
			arHotDealList = (ArrayList) resultFromProcedure.get("searchList");
			LOG.info("usp_WebDisplayHotDeal SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arHotDealList;
	}

	/**
	 * This method will return the Hot deal product details List based on input
	 * search parameter. 'RetailerID','strProductName'.
	 * 
	 * @return Hot deal product details list based on input parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<ProductHotDeal> getRetailerHotDealByRetailID(Long RetailerID, String strProductName) throws ScanSeeWebSqlException {
		final String methodName = "getRetailerHotDealByRetailID";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		List<ProductHotDeal> arProductNameList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerHotDealSearch");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchProductName", new BeanPropertyRowMapper<ProductHotDeal>(ProductHotDeal.class));
			final MapSqlParameterSource objHotDealProduct = new MapSqlParameterSource();
			objHotDealProduct.addValue("RetailerID", RetailerID);
			objHotDealProduct.addValue("SearchParameter", strProductName);
			resultFromProcedure = simpleJdbcCall.execute(objHotDealProduct);
			arProductNameList = (ArrayList) resultFromProcedure.get("searchProductName");
			LOG.info("usp_WebRetailerHotDealSearch SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getRetailerHotDealByRetailID : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arProductNameList;
	}

	/**
	 * This method Add the Rebates details into database.
	 * 
	 * @param objRebates
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public String addRetRerunRebates(Rebates objRebates) throws ScanSeeWebSqlException {

		final String methodName = "addRetRerunRebates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerReRunRebate");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objAddRebParameter = new MapSqlParameterSource();
			objAddRebParameter.addValue("RebateName", objRebates.getRebName());
			objAddRebParameter.addValue("RebateAmount", objRebates.getRebAmount());
			objAddRebParameter.addValue("RebateDescription", objRebates.getRebLongDescription());
			objAddRebParameter.addValue("RebateTermsAndCondition", objRebates.getRebTermCondtn());
			objAddRebParameter.addValue("RebateStartDate", objRebates.getRebStartDate());
			objAddRebParameter.addValue("RebateEndDate", objRebates.getRebEndDate());
			objAddRebParameter.addValue("RebateStartTime", objRebates.getRebStartTime());
			objAddRebParameter.addValue("RebateEndTime", objRebates.getRebEndTime());
			objAddRebParameter.addValue("RetailerID", objRebates.getRetailerID());
			objAddRebParameter.addValue("RetailerLocationID", objRebates.getRetailerLocID());
			objAddRebParameter.addValue("NoOfRebatesIssued", objRebates.getNoOfrebatesIsued());
			objAddRebParameter.addValue("ProductID", objRebates.getRebproductId());
			if (objRebates.getRebateTimeZoneId() == 0) {
				objAddRebParameter.addValue("RebateTimeZoneID", null);
			} else {
				objAddRebParameter.addValue("RebateTimeZoneID", objRebates.getRebateTimeZoneId());
			}
			resultFromProcedure = simpleJdbcCall.execute(objAddRebParameter);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				response = ApplicationConstants.SUCCESS;
				LOG.info("the responseFromProc is :" + responseFromProc);
			} else {
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : addRetRerunRebates  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebRetailerReRunRebate SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside SupplierDAOImpl : addRetRerunRebates : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * @param rebateId
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<Rebates> getRebatesForDisplay(int rebateId) throws ScanSeeWebSqlException {
		final String methodName = "getRebatesForDisplay";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<Rebates> listRebNames = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebDisplayRebate");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RebateID", rebateId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listRebNames = (ArrayList) resultFromProcedure.get("searchList");
			if (listRebNames != null) {

				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + listRebNames.size());
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info("Inside SupplierDAOImpl : getRebatesForDisplay  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebDisplayRebate SP is excuted successfully");
		} catch (Exception exception) {
			LOG.error("Inside SupplierDAOImpl : getRebatesForDisplay : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return listRebNames;
	}

	/**
	 * This method Add the Rebates details into database.
	 * 
	 * @param objRebates
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public String updateRetRebates(Rebates objRebates) throws ScanSeeWebSqlException {

		final String methodName = "updateRetRebates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerRebateUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objAddRebParameter = new MapSqlParameterSource();

			objAddRebParameter.addValue("RebateID", objRebates.getRebateID());
			objAddRebParameter.addValue("RebateName", objRebates.getRebName());
			objAddRebParameter.addValue("RebateAmount", objRebates.getRebAmount());
			objAddRebParameter.addValue("RebateLongDescription", objRebates.getRebLongDescription());
			objAddRebParameter.addValue("RebateShortDescription", objRebates.getRebShortDescription());
			objAddRebParameter.addValue("RebateTermsAndCondition", objRebates.getRebTermCondtn());
			objAddRebParameter.addValue("RebateStartDate", objRebates.getRebStartDate());
			objAddRebParameter.addValue("RebateEndDate", objRebates.getRebEndDate());
			objAddRebParameter.addValue("RebateStartTime", objRebates.getRebStartTime());
			objAddRebParameter.addValue("RebateEndTime", objRebates.getRebEndTime());
			objAddRebParameter.addValue("RetailerID", objRebates.getRetailerID());
			objAddRebParameter.addValue("RetailerLocationID", objRebates.getRetailerLocID());
			if (objRebates.getRebateTimeZoneId() == 0) {
				objAddRebParameter.addValue("RebateTimeZoneID", null);
			} else {
				objAddRebParameter.addValue("RebateTimeZoneID", objRebates.getRebateTimeZoneId());
			}
			resultFromProcedure = simpleJdbcCall.execute(objAddRebParameter);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				response = ApplicationConstants.SUCCESS;
				LOG.info("the responseFromProc is :" + responseFromProc);
			} else {
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : updateRetRebates  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException("Inside SupplierDAOImpl : updateRetRebates  : errorNumber  : " + errorNum + "errorMessage : "
						+ errorMsg);
			}
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside SupplierDAOImpl : updateRetRebates : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public PlanInfo getRetailerDiscount(String discountCode) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getDiscount");
		
		//Double discount = 0.0;
		PlanInfo planInfo = null;
		
		Map<String, Object> resultFromProcedure = null;

		List<PlanInfo> planinfolst = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerDiscountPlanDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("dicountList", new BeanPropertyRowMapper<PlanInfo>(PlanInfo.class));
			final MapSqlParameterSource objdiscountParameter = new MapSqlParameterSource();
			objdiscountParameter.addValue("DiscountCode", discountCode);

			resultFromProcedure = simpleJdbcCall.execute(objdiscountParameter);

			planinfolst = (ArrayList<PlanInfo>) resultFromProcedure.get("dicountList");
			if (!planinfolst.isEmpty() && planinfolst != null) {
				// discount=planinfolst.get(0).getDiscount();
				planInfo = planinfolst.get(0);
			}
			LOG.info("usp_WebRetailerDiscountPlanDisplay SP is excuted successfully");
		} catch (Exception exception) {
			LOG.error("Inside RetailerDAOImpl : getRetailerDiscount : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return planInfo;
	}

	/**
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException {
		final String methodName = "getAppConfig";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside RetailerDAOImpl : getAppConfig : Error occurred in  Store Procedure with error number: {} " + errorNum
						+ " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_GetScreenContent SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getAppConfig : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return appConfigurationList;
	}

	/**
	 * The DAOImpl method for displaying list of categories and its status
	 * return to service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown.
	 * @return categories,List of categories.
	 */
	public final ArrayList<Category> getAllBusinessCategory() throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllBusinessCategory ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<Category> arCategoryList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveBusinessCategory");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("categoryList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			arCategoryList = (ArrayList) resultFromProcedure.get("categoryList");
			LOG.info("usp_WebRetrieveBusinessCategory SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arCategoryList;
	}

	/**
	 * The DAOImpl method for displaying list of cities and its status return to
	 * service method.
	 * 
	 * @param lUserID
	 *            as request parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return cities,List of cities.
	 */
	public final ArrayList<City> getHdPopulationCenters(Long lUserID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getHdPopulationCenters ");
		ArrayList<City> hotDealsCategorylst = null;
		Map<String, Object> resultFromProcedure = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_HotDealPopulationCentres");
			simpleJdbcCall.returningResultSet("HotDealCategorylist", new BeanPropertyRowMapper<City>(City.class));
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource fetchHotDealCityParam = new MapSqlParameterSource();
			fetchHotDealCityParam.addValue("UserID", lUserID);
			resultFromProcedure = simpleJdbcCall.execute(fetchHotDealCityParam);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) {
					hotDealsCategorylst = (ArrayList<City>) resultFromProcedure.get("HotDealCategorylist");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_HotDealDetails Store Procedure error number: {} " + errorNum + " and error message: {}"
							+ errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_HotDealPopulationCentres SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return hotDealsCategorylst;
	}

	/**
	 * The DAOImpl method for displaying list of TimeZones (all standard time
	 * like (Atlantic,Central, Eastern, Mountain, Pacific Standard Time) and its
	 * status return to service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return timeZonelst,List of TimeZones.
	 */
	public final ArrayList<TimeZones> getAllTimeZones() throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllTimeZones ");
		ArrayList<TimeZones> timeZoneslst = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveTimeZone");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("TimeZoneslst", new BeanPropertyRowMapper<TimeZones>(TimeZones.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					timeZoneslst = (ArrayList<TimeZones>) resultFromProcedure.get("TimeZoneslst");
				}
			} else {

				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside RetailerDAOImpl : getAllTimeZones : Error occurred in usp_WebRetrieveTimeZone Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetrieveTimeZone SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return timeZoneslst;
	}

	public String batchUploadProductsFromStageToProd(Long userId, int retailId, String fileName, Integer logId) throws ScanSeeWebSqlException {
		final String methodName = "batchUploadProductsFromStageToProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String response = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebUploadRetailerProducts");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objParameter = new MapSqlParameterSource();
			objParameter.addValue("UserID", userId);
			objParameter.addValue("RetailerID", retailId);
			objParameter.addValue("FileName", fileName);
			objParameter.addValue("UploadRetailLocationProductLogID", logId);
			resultFromProcedure = simpleJdbcCall.execute(objParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside retaierDAOImpl :addRetailProductSetupSearchResultsModification : errorNumber  : " + errorNum + "errorMessage : "
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebUploadRetailerProducts SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getAllTimeZones : " + exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public List<Retailer> fetchUploadPreviewList(Long retailerID) throws ScanSeeWebSqlException {
		final String methodName = "fetchUploadPreviewList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		List<Retailer> retailerPreviewList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerProfileDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("RetailerList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource objSearchprodParameter = new MapSqlParameterSource();
			objSearchprodParameter.addValue("RetailerID", retailerID);
			resultFromProcedure = simpleJdbcCall.execute(objSearchprodParameter);

			// LOG.info(resultFromProcedure);
			//final Integer responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					retailerPreviewList = (List) resultFromProcedure.get("RetailerList");

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : fetchUploadPreviewList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebRetailerProfileDisplay SP is excuted successfully");

		} catch (Exception exception) {
			LOG.error("Inside RetailerDAOImpl : fetchUploadPreviewList : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerPreviewList;

	}

	/**
	 * The DAOImpl method for displaying account type (ACH Bank
	 * information/Credit Card) and it return to service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return arAccountTypeList,List of AccountType.
	 */
	public final ArrayList<AccountType> getAllAcountType() throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllAcountType ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<AccountType> arAccountTypeList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveAccountType");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("accountTypeList", new BeanPropertyRowMapper<AccountType>(AccountType.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			arAccountTypeList = (ArrayList) resultFromProcedure.get("accountTypeList");
			LOG.info("usp_WebRetrieveAccountType SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arAccountTypeList;
	}

	/**
	 * This DAOImpl method delete all the locations in stage table and its
	 * status return to service method.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param retailId
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String deleteRetailerLocationStageTable(int retailId, Long userId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : deleteRetailerLocationStageTable ");
		String strResponse = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebDeleteRetailerLocationsStageTable");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailerID", retailId);
			objDelProductsParameter.addValue("UserID", userId);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
			LOG.info("usp_WebDeleteRetailerLocationsStageTable SP is excuted successfully");
		} catch (DataAccessException e) {
			if (null == responseFromProc || responseFromProc.intValue() != 0) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.EXCEPTION_OCCURED + " errorNumber : " + errorNum + "errorMessage : " + errorMsg);
			}
			strResponse = ApplicationConstants.FAILURE;
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return strResponse;
	}

	public ArrayList<Rebates> getRebateProductImage(String productName, Long retailID) throws ScanSeeWebSqlException {

		final String methodName = "getRebateProductImage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;

		ArrayList<Rebates> arProdList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerRebateAddProductSearch");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("productImageList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();
			objCouponNameParameters.addValue("RetailerID", retailID);
			objCouponNameParameters.addValue("Search", productName);
			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);
			arProdList = (ArrayList<Rebates>) resultFromProcedure.get("productImageList");
			LOG.info("usp_WebRetailerRebateAddProductSearch SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getRebateProductImage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arProdList;
	}

	public ArrayList<Coupon> getCouponImagePath(Long couponID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getCouponImagePath ");
		Map<String, Object> resultFromProcedure = null;

		ArrayList<Coupon> coupnDetailsList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponDetails");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchCouponID", new BeanPropertyRowMapper<Coupon>(Coupon.class));

			final MapSqlParameterSource objCouponIDParameters = new MapSqlParameterSource();

			objCouponIDParameters.addValue("CouponID", couponID);
			resultFromProcedure = simpleJdbcCall.execute(objCouponIDParameters);

			coupnDetailsList = (ArrayList<Coupon>) resultFromProcedure.get("searchCouponID");
			LOG.info("usp_WebRetailerCouponDetails SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getCouponIDByCouponDetails : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return coupnDetailsList;
	}

	public List<Rebates> getProductInfo(int rebateId) throws ScanSeeWebSqlException {
		final String methodName = "getProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<Rebates> listRebProduct = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebDisplayRebate");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchProductList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RebateID", rebateId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listRebProduct = (ArrayList) resultFromProcedure.get("searchProductList");
			if (listRebProduct != null) {

				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + listRebProduct.size());
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info("Inside RetailerDAOImpl : getProductInfo  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebDisplayRebate SP is excuted successfully");
		} catch (Exception exception) {
			LOG.error("Inside RetailersDAOImpl : getProductInfo : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		return listRebProduct;
	}

	/**
	 * The DAOImpl method return if product name is empty then display product
	 * image path and its status return to service method.
	 * 
	 * @param prodName
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @return prodList,List of products.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final ArrayList<HotDealInfo> getProdDealDetails(String prodName, Long retailID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getProdDealDetails ");
		Map<String, Object> resultFromProcedure = null;

		ArrayList<HotDealInfo> arProdNameList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();
			simpleJdbcCall.withProcedureName("usp_WebRetailerAddHotDealProductSearch");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchProductNameList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			objCouponNameParameters.addValue("RetailID", retailID);
			objCouponNameParameters.addValue("ProductSearch", prodName);

			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);
			arProdNameList = (ArrayList<HotDealInfo>) resultFromProcedure.get("searchProductNameList");
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arProdNameList;
	}

	public ArrayList<Coupon> getProducts(String productName) throws ScanSeeWebSqlException {
		final String methodName = "getProducts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;

		ArrayList<Coupon> arProdNameList = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();

			simpleJdbcCall.withProcedureName("usp_WebPreviewProductDetails");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchCouponNameList", new BeanPropertyRowMapper<Coupon>(Coupon.class));
			objCouponNameParameters.addValue("Scancodes", productName);

			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);
			arProdNameList = (ArrayList<Coupon>) resultFromProcedure.get("searchCouponNameList");
			LOG.info("usp_WebPreviewProductDetails SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getProducts : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arProdNameList;

	}

	public SearchResultInfo getRetailLocProd(String searchParameter, int lowerLimit, int retailID, String retailLocationID, int recordCount)
			throws ScanSeeWebSqlException {
		final String methodName = "getRetailLocProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<ProductVO> searchProductList = new ArrayList<ProductVO>();

		SearchResultInfo searchresult = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("searchProductList", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));
			simpleJdbcCall.withProcedureName("usp_WebDisplayRetailLocationProducts");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchProduct = new MapSqlParameterSource();

			objSearchProduct.addValue("RetailerID", retailID);
			objSearchProduct.addValue("RetailLocationID", retailLocationID);
			objSearchProduct.addValue("SearchParameter", searchParameter);
			objSearchProduct.addValue("LowerLimit", lowerLimit);
			objSearchProduct.addValue("RecordCount", recordCount);
			resultFromProcedure = simpleJdbcCall.execute(objSearchProduct);
			searchProductList = (ArrayList<ProductVO>) resultFromProcedure.get("searchProductList");
			if (searchProductList != null && !searchProductList.isEmpty()) {
				searchresult = new SearchResultInfo();
				int totalSize = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setTotalSize(totalSize);
				searchresult.setProductList(searchProductList);
			}
			LOG.info("usp_WebRetailerWelcomePageDisplay SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :retailerProductSetupProductSearch :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchresult;
	}

	/**
	 * The DAOImpl method will add one location details and its status return to
	 * service method.
	 * 
	 * @param storeInfoVO
	 *            StoreInfoVO instance as request parameter
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String addLocation(StoreInfoVO storeInfoVO) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : addLocation ");
		String result = null;
		Integer responseFromProc = null;
		Map<String, Object> resultFromProcedure = null;
		Integer iStoreExists = null;
		Integer retLocId = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			final MapSqlParameterSource objAddLocationParameters = new MapSqlParameterSource();
			simpleJdbcCall.withProcedureName("usp_WebRetailerAddRetailerLocation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			objAddLocationParameters.addValue("RetailID", storeInfoVO.getRetailID());
			objAddLocationParameters.addValue("StoreIdentification", storeInfoVO.getStoreID());
			objAddLocationParameters.addValue("StoreAddress", storeInfoVO.getStoreAddress());
			objAddLocationParameters.addValue("State", storeInfoVO.getState());
			objAddLocationParameters.addValue("City", storeInfoVO.getCity());
			objAddLocationParameters.addValue("PostalCode", storeInfoVO.getPostalCode());
			objAddLocationParameters.addValue("Phone", Utility.removePhoneFormate(storeInfoVO.getPhoneNumber()));
			objAddLocationParameters.addValue("RetailLocationURL", storeInfoVO.getRetailUrl());
			objAddLocationParameters.addValue("RetailLocationLatitude", storeInfoVO.getRetailerLocationLatitude());
			objAddLocationParameters.addValue("RetailLocationLongitude", storeInfoVO.getRetailerLocationLongitude());
			objAddLocationParameters.addValue("RetailLocationKeyword", storeInfoVO.getKeyword());
			if (ApplicationConstants.BLANKIMAGE.equals(storeInfoVO.getLocationImgPath())) {
				objAddLocationParameters.addValue("RetailLocationImagePath", null);
			} else {
				objAddLocationParameters.addValue("RetailLocationImagePath", storeInfoVO.getLocationImgPath());
			}

			resultFromProcedure = simpleJdbcCall.execute(objAddLocationParameters);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			iStoreExists = (Integer) resultFromProcedure.get("StoreExists");

			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				if (iStoreExists == 1) {
					result = ApplicationConstants.DUPLICATE_STORE;
					LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : Return response message : " + result);
				} else {
					result = ApplicationConstants.SUCCESS;
					retLocId = (Integer) resultFromProcedure.get("ResponseRetailLocationID");
					result = String.valueOf(retLocId);
					LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : Return response message : " + result);
				}
				LOG.info("Inside RetailerDAOImpl : addRetailerRegistration : responseFromProc is : " + responseFromProc);
			} else {
				if (iStoreExists == 1) {
					result = ApplicationConstants.DUPLICATE_STORE;
					LOG.info("Inside RetailerDAOImpl : addLocation : Return response message : " + result);
				}
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : addLocation   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
			LOG.info("usp_WebRetailerAddRetailerLocation SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return result;
	}

	/**
	 * The DAOImpl method for displaying all the business categories and it
	 * return to service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @throws ScanSeeWebSqlException
	 *             as SQL exception.
	 * @return arCategoryList,List of categories.
	 */
	public final ArrayList<Category> getAllRetailerProfileBusinessCategory() throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllRetailerProfileBusinessCategory ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<Category> arCategoryList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveRetailerBusinessCategory");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("categoryList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			arCategoryList = (ArrayList) resultFromProcedure.get("categoryList");

			Collections.sort(arCategoryList, new Comparator<Category>() {
				public int compare(Category o1, Category o2) {
					return o1.getBusinessCategoryName().compareToIgnoreCase(o2.getBusinessCategoryName());
				}
			});
			LOG.info("usp_WebRetrieveRetailerBusinessCategory SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arCategoryList;
	}

	/**
	 * This DAOImpl method will return list of Retailer Locations/fetch the
	 * retailer location based on input parameter and its status return to
	 * service method.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @param storeIdentification
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final SearchResultInfo getBatchLocationList(int retailerId, long userId, int lowerLimit, String storeIdentification)
			throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getLocationList ");
		Map<String, Object> resultFromProcedure = null;
		List<RetailerLocation> locationList = null;
		SearchResultInfo searchInfo = null;
		try {
			searchInfo = new SearchResultInfo();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("LocationList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerDisplayRetailLocations");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objRetailIDParameters = new MapSqlParameterSource();
			objRetailIDParameters.addValue("RetailID", retailerId);
			// objRetailIDParameters.addValue("RetailID", 458);
			objRetailIDParameters.addValue("SearchKey", storeIdentification);
			objRetailIDParameters.addValue("LowerLimit", lowerLimit);
			resultFromProcedure = simpleJdbcCall.execute(objRetailIDParameters);
			// locationList = (ArrayList<RetailerLocation>)
			// resultFromProcedure.get("LocationList");

			if (resultFromProcedure != null) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum) {
					locationList = (ArrayList<RetailerLocation>) resultFromProcedure.get("LocationList");
					if (null != locationList && !locationList.isEmpty()) {
						final int totalSize = (Integer) resultFromProcedure.get("RowCount");
						searchInfo.setTotalSize(totalSize);
					}
					searchInfo.setLocationList(locationList);
				} else {
					LOG.info("Error Occured in registerUser method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebRetailerDisplayRetailLocations SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return searchInfo;
	}

	/**
	 * The DAOImpl method will update retailer multiple locations and its status
	 * return to service method.
	 * 
	 * @param locationList
	 *            instance of List<RetailerLocation>.
	 * @param userId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String saveLocationList(final List<RetailerLocation> locationList, long userId, final int retailerId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : saveLocationList ");
		String response = null;
		/*
		 * Object[] values = null; Object[] contactValues = null; String
		 * isDataInserted = null; List<Object[]> contactBatch = new
		 * ArrayList<Object[]>(); List<Object[]> locationBatch = new
		 * ArrayList<Object[]>();
		 */
		// final List<RetailerLocation> batchLocationList = locationList;
		TransactionStatus status = null;
		try {
			// PROPAGATION_REQUIRES_NEW - Create a new transaction, suspending
			// the current transaction if one exists.
			DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();

			status = platformTxManager.getTransaction(paramTransactionDefinition);

			// final TransactionDefinition txDefn = new
			// DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
			// txStatus = txManager.getTransaction(txDefn);
			final int[] arry = jdbcTemplate.batchUpdate("{call usp_WebRetailerUpdateRetailerLocations(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}",

			new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					final RetailerLocation objlocation = locationList.get(i);
					ps.setInt(1, retailerId);
					ps.setInt(2, Integer.valueOf(objlocation.getRetailLocationID()));

					ps.setString(3, objlocation.getStoreIdentification());

					ps.setString(4, objlocation.getAddress1());
					ps.setString(5, objlocation.getCity());
					ps.setString(6, objlocation.getState());
					ps.setString(7, objlocation.getPostalCode());
					ps.setString(8, objlocation.getPhonenumber());
					ps.setString(9, objlocation.getRetailLocationUrl());
					ps.setString(10, objlocation.getKeyword());
					if (null == objlocation.getRetailerLocationLatitude()) {
						ps.setDouble(11, 0.0);
					} else {
						ps.setDouble(11, objlocation.getRetailerLocationLatitude());
					}
					if (null == objlocation.getRetailerLocationLongitude()) {
						ps.setDouble(12, 0.0);
					} else {
						ps.setDouble(12, objlocation.getRetailerLocationLongitude());
					}
					ps.setString(13, objlocation.getGridImgLocationPath());
					ps.setNull(14, Types.BIT);
					ps.setNull(15, Types.BIT);
					ps.setNull(16, Types.INTEGER);
					ps.setNull(17, Types.VARCHAR);
				}

				public int getBatchSize() {
					return locationList.size();
				}
			});

			/*
			 * jdbcTemplate.batchUpdate("", new BatchPreparedStatementSetter() {
			 * public void setValues(PreparedStatement ps, int i) throws
			 * SQLException { ps.setInt(1, i + 1); ps.setString(2,
			 * firstNames.get(i)); ps.setString(3, lastNames.get(i));
			 * ps.setNull(4, Types.TIMESTAMP); ps.setNull(5, Types.CLOB); }
			 * public int getBatchSize() { return locationList.size(); } });
			 */

			// Connection
			// connection=jdbcTemplate.getDataSource().getConnection();
			/*
			 * String contactQuery = "UPDATE RetailLocation SET " +
			 * "StoreIdentification = ?," + "Address1 = ?," + "State = ?," +
			 * "City = ?," + "PostalCode = ?," +
			 * "RetailLocationLatitude = (SELECT Latitude FROM GeoPosition WHERE PostalCode = ?),"
			 * +
			 * "RetailLocationLongitude = (SELECT Longitude FROM GeoPosition WHERE PostalCode = ?),"
			 * + "RetailLocationUrl = ?" + " WHERE RetailLocationID = ?"; for
			 * (RetailerLocation objlocation : locationList) { contactValues =
			 * new Object[] { objlocation.getStoreIdentification(),
			 * objlocation.getAddress1(), objlocation.getState(),
			 * objlocation.getCity(), objlocation.getPostalCode(),
			 * objlocation.getPostalCode(), objlocation.getPostalCode(),
			 * objlocation
			 * .getRetailLocationUrl(),objlocation.getRetailLocationID() };
			 * contactBatch.add(contactValues); }
			 */

			/*
			 * for (RetailerLocation objlocation : locationList) { contactValues
			 * = new Object[] { objlocation.getRetailLocationID(),
			 * objlocation.getPhonenumber(), objlocation.getRetailLocationID(),
			 * objlocation.getStoreIdentification(), objlocation.getAddress1(),
			 * objlocation.getState(), objlocation.getCity(),
			 * objlocation.getPostalCode(), objlocation.getPostalCode(),
			 * objlocation.getPostalCode(), objlocation.getRetailLocationUrl()
			 * }; objlocation.getRetailLocationID(),
			 * objlocation.getStoreIdentification(), objlocation.getAddress1(),
			 * objlocation.getState(), objlocation.getCity(),
			 * objlocation.getPostalCode(), objlocation.getPostalCode(),
			 * objlocation.getPostalCode(), objlocation.getRetailLocationUrl(),
			 * objlocation.getRetailLocationID(), objlocation.getPhonenumber(),
			 * objlocation.getRetailLocationID()
			 * contactBatch.add(contactValues); }
			 */
			/*
			 * int[] updateContact =
			 * simpleJdbcTemplate.batchUpdate(ApplicationConstants
			 * .MANAGE_RET_LOCATION_QUERY, contactBatch); if
			 * (updateContact.length != 0) { response =
			 * ApplicationConstants.SUCCESS; } else { response =
			 * ApplicationConstants.FAILURE; }
			 */
			response = ApplicationConstants.SUCCESS;
			platformTxManager.commit(status);
			// platformTransactionManager.rollback(status);
			LOG.info("Data inserted into production table status:::::::::::::::" + response);
		} catch (DataAccessException e) {
			platformTxManager.rollback(status);
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (Exception e) {
			platformTxManager.rollback(status);
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	public String duplicateStoreIdentityCheck(int retailId, String storeIdentification) throws ScanSeeWebSqlException {
		final String methodName = "duplicateStoreIdentityCheck";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		String responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerStoreIdentificationDuplicatesCheck");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailId);
			objDelProductsParameter.addValue("StoreIdentification", storeIdentification);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (String) resultFromProcedure.get("DuplicateStores");
			LOG.info("usp_WebRetailerStoreIdentificationDuplicatesCheck SP is excuted successfully");

		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : deleteProductsFromLocHoldControl : " + exception);

			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * This DAOImpl method will delete one retailer location from manage
	 * Location grid and its status return to service method.
	 * 
	 * @param retailLocationId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String deleteBatchLocationList(String retailLocationId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : deleteBatchLocationList ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String strResponse = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerDeleteRetaillocations");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetaillocationID", retailLocationId);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
			LOG.info("usp_WebRetailerDeleteRetaillocations SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : deleteProductsFromLocHoldControl : " + e);
			if (null == responseFromProc || responseFromProc.intValue() != 0) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetaierDAOImpl :deleteProductsFromLocHoldControl : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			strResponse = ApplicationConstants.FAILURE;
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return strResponse;
	}

	public ArrayList<ContactType> getAllContactTypes() throws ScanSeeWebSqlException {
		final String methodName = "getAllContactTypes";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<ContactType> contactTypes = new ArrayList<ContactType>();

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveContactTitle");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("contactList", new BeanPropertyRowMapper<ContactType>(ContactType.class));

			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			contactTypes = (ArrayList<ContactType>) resultFromProcedure.get("contactList");
			LOG.info("usp_WebRetrieveContactTitle SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside SupplierDAOImpl : getProdImage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return contactTypes;

	}

	/**
	 * This method is used to get the rejected records in the location upload
	 * file
	 * 
	 * @param retailerId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<RetailerLocation> getRejectedRecords(int retailerId, Long userId) throws ScanSeeWebSqlException {
		final String methodName = "getRejectedRecords";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<RetailerLocation> objRetailerLocation = new ArrayList<RetailerLocation>();
		Map<String, Object> resultFromProcedure = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerFetchDiscardedRetailLocations");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("rejectedLocationList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));

			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailerID", retailerId);
			objSearchRebParameter.addValue("UserID", userId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			objRetailerLocation = (ArrayList<RetailerLocation>) resultFromProcedure.get("rejectedLocationList");
			LOG.info("usp_WebRetailerFetchDiscardedRetailLocations SP is excuted successfully");
		} catch (EmptyResultDataAccessException exception) {
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objRetailerLocation;

	}

	/**
	 * This DAOImpl method return retailer email Id based on input parameter by
	 * call DAO layer.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @return result,retailer email Id.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String getUserMailId(int retailerId, Long userId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getUserMailId ");
		String result = null;
		ArrayList<ContactDetail> objRetailerLocation = new ArrayList<ContactDetail>();
		Map<String, Object> resultFromProcedure = null;
		ContactDetail objContact = new ContactDetail();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveRetailerContactEmail");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("rejectedLocationList", new BeanPropertyRowMapper<ContactDetail>(ContactDetail.class));

			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailerID", retailerId);
			objSearchRebParameter.addValue("UserID", userId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			objRetailerLocation = (ArrayList<ContactDetail>) resultFromProcedure.get("rejectedLocationList");
			objContact = objRetailerLocation.get(0);
			result = objContact.getContactEmail();
			LOG.info("usp_WebRetrieveRetailerContactEmail SP is excuted successfully");
		} catch (EmptyResultDataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return result;
	}

	public int insertRetailerCretaedPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException {
		LOG.info("Inside the method insertRetailerCretaedPageInfo");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc;
		int pageID = 0;
		String startDateandTime = null;
		String endDateandTime = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCreatedPageCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailerCustomPage.getRetailerId());

			if (retailerCustomPage.getLandigPageType().equals("Landing"))

			{
				objDelProductsParameter.addValue("RetailLocationID", retailerCustomPage.getRetCreatedPageLocId());
				objDelProductsParameter.addValue("PageTitle", retailerCustomPage.getRetCreatedPageTitle());

				if (null != retailerCustomPage.getRetCreatedPageStartDate() && !retailerCustomPage.getRetCreatedPageStartDate().equals("")) {
					startDateandTime = Utility.getFormattedDate(retailerCustomPage.getRetCreatedPageStartDate()) + " "
							+ retailerCustomPage.getLandingPageStartTimeHrs() + ":" + retailerCustomPage.getLandingPageStartTimeMin() + ":" + "00";
					objDelProductsParameter.addValue("StartDate", startDateandTime);
				} else {
					objDelProductsParameter.addValue("StartDate", null);
				}
				if (null != retailerCustomPage.getRetCreatedPageEndDate() && !retailerCustomPage.getRetCreatedPageEndDate().equals("")) {
					endDateandTime = Utility.getFormattedDate(retailerCustomPage.getRetCreatedPageEndDate()) + " "
							+ retailerCustomPage.getLandingPageEndTimeHrs() + ":" + retailerCustomPage.getLandingPageEndTimeMin() + ":" + "00";
					objDelProductsParameter.addValue("EndDate", endDateandTime);
				} else {
					objDelProductsParameter.addValue("EndDate", null);
				}

				objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
				objDelProductsParameter.addValue("PageDescription", retailerCustomPage.getRetCreatedPageDescription());
				objDelProductsParameter.addValue("ShortDescription", retailerCustomPage.getRetCreatedPageShortDescription());
				objDelProductsParameter.addValue("LongDescription", retailerCustomPage.getRetCreatedPageLongDescription());
				objDelProductsParameter.addValue("MediaPath", retailerCustomPage.getMediaPath());
				objDelProductsParameter.addValue("ExternalFlag", retailerCustomPage.getExternalFlag());

			} else
				if (retailerCustomPage.getLandigPageType().equals("AttachLink")) {
					objDelProductsParameter.addValue("RetailLocationID", retailerCustomPage.getRetCreatedPageAttachLinkLocId());
					objDelProductsParameter.addValue("PageTitle", retailerCustomPage.getRetCreatedPageAttachLinkTitle());
					objDelProductsParameter.addValue("StartDate", null);
					objDelProductsParameter.addValue("EndDate", null);
					objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
					objDelProductsParameter.addValue("PageDescription", null);
					objDelProductsParameter.addValue("ShortDescription", null);
					objDelProductsParameter.addValue("LongDescription", null);
					objDelProductsParameter.addValue("MediaPath", retailerCustomPage.getMediaPath());
					objDelProductsParameter.addValue("ExternalFlag", retailerCustomPage.getExternalFlag());
				}

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				pageID = (Integer) resultFromProcedure.get("PageID");
			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : insertRetailerCretaedPageInfo : " + exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		} catch (ParseException e) {
			LOG.error("Inside RetailerDAOImpl : insertRetailerCretaedPageInfo : " + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		LOG.info("Exit method insertRetailerCretaedPageInfo");
		return pageID;
	}

	/**
	 * 
	 */
	public SearchResultInfo getCustomPageList(int retailerId, String searchKey, int currentPage, int recordCount) throws ScanSeeWebSqlException {
		final String methodName = "getCustomPageList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo customPageResp = null;
		Map<String, Object> resultFromProcedure = null;
		List<RetailerCustomPage> pageList = new ArrayList<RetailerCustomPage>();
		Integer rowcount = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerAnythingPageDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("pageList", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailID", retailerId);
			objSearchRebParameter.addValue("SearchParameter", searchKey);
			objSearchRebParameter.addValue("LowerLimit", currentPage);
			objSearchRebParameter.addValue("RecordCount", recordCount);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);

			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					customPageResp = new SearchResultInfo();

					pageList = (List<RetailerCustomPage>) resultFromProcedure.get("pageList");
					if (null != pageList && !pageList.isEmpty()) {
						rowcount = (Integer) resultFromProcedure.get("RowCount");
						customPageResp.setPageList(pageList);
						customPageResp.setTotalSize(rowcount);
					}

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : fetchUploadPreviewList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info(ApplicationConstants.METHODEND + methodName);
		} catch (Exception exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return customPageResp;
	}

	/*
	 * (non-Javadoc)
	 * @see retailer.dao.RetailerDAO#getQRcodeForPage(java.lang.Long, int)
	 */
	public RetailerCustomPage getQRcodeForPage(Long pageId, int retailerId) throws ScanSeeWebSqlException {

//		String result = null;
		//ArrayList<ContactDetail> objRetailerLocation = new ArrayList<ContactDetail>();
		Map<String, Object> resultFromProcedure = null;
//		ContactDetail objContact = new ContactDetail();
		final String methodName = "getAllStates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerCustomPage pageDetails = null;
		List<RetailerCustomPage> pageList = new ArrayList<RetailerCustomPage>();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebDisplayRetailerCustomPageURL");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("pageList", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailID", retailerId);
			objSearchRebParameter.addValue("PageID", pageId);
			objSearchRebParameter.addValue("UserID", pageId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					pageList = (List<RetailerCustomPage>) resultFromProcedure.get("pageList");
					if (null != pageList && !pageList.isEmpty()) {

						pageDetails = pageList.get(0);
					}

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : fetchUploadPreviewList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebDisplayRetailerCustomPageURL SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : insertRetailerPageInfo : " + exception);

			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageDetails;
	}

	public int insertRetailerPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException {

		final String methodName = "insertRetailerPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc;
		int pageID = 0;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.withProcedureName("usp_WebRetailerAnythingPageCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailerCustomPage.getRetailerId());
			objDelProductsParameter.addValue("RetailLocationID", retailerCustomPage.getRetCreatedPageLocId());
			objDelProductsParameter.addValue("AnythingPageTitle", retailerCustomPage.getRetPageTitle());
			objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
			objDelProductsParameter.addValue("WebLink", retailerCustomPage.getRetCreatedPageattachLink());
			if (retailerCustomPage.getImageIconID() > 0) {
				objDelProductsParameter.addValue("ImageIconID", retailerCustomPage.getImageIconID());
			} else {
				objDelProductsParameter.addValue("ImageIconID", null);
			}

			objDelProductsParameter.addValue("PageDescription", retailerCustomPage.getRetPageDescription());
			objDelProductsParameter.addValue("PageShortDescription", retailerCustomPage.getRetPageShortDescription());
			objDelProductsParameter.addValue("PageLongDescription", retailerCustomPage.getRetPageLongDescription());

			if (null == retailerCustomPage.getRetCreatedPageStartDate() || "".equals(retailerCustomPage.getRetCreatedPageStartDate())) {
				objDelProductsParameter.addValue("StartDate", null);
			} else {
				objDelProductsParameter.addValue("StartDate", retailerCustomPage.getRetCreatedPageStartDate());
			}

			if ("".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate()))) {
				retailerCustomPage.setRetCreatedPageEndDate(null);
			}
			objDelProductsParameter.addValue("EndDate", retailerCustomPage.getRetCreatedPageEndDate());

			objDelProductsParameter.addValue("StartTime", "00:00");
			objDelProductsParameter.addValue("EndTime", "00:00");
			objDelProductsParameter.addValue("UploadFileName", retailerCustomPage.getPdfFileName());

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				pageID = (Integer) resultFromProcedure.get("PageID");
			}
			LOG.info("usp_WebRetailerAnythingPageCreation SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : insertRetailerPageInfo : " + exception);

			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info("Exit method insertRetailerPageInfo");
		return pageID;

	}

	public int insertSplOfferPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException {
		LOG.info("Inside the method insertSplOfferPageInfo");
		Map<String, Object> resultFromProcedure = null;
//		Integer responseFromProc;
		int pageID = 0;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerSpecialOfferCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailerCustomPage.getRetailerId());
			objDelProductsParameter.addValue("RetailLocationID", retailerCustomPage.getHiddenLocId());
			objDelProductsParameter.addValue("SpecialOfferPageTitle", retailerCustomPage.getSplOfferTitle());
			objDelProductsParameter.addValue("WebLink", retailerCustomPage.getSplOfferattachLink());
			if (retailerCustomPage.getImageIconID() > 0) {
				objDelProductsParameter.addValue("Image", null);
				objDelProductsParameter.addValue("ImageIconID", retailerCustomPage.getImageIconID());
				objDelProductsParameter.addValue("StartTime", "00:00");
				objDelProductsParameter.addValue("EndTime", "00:00");
			} else {
				objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
				objDelProductsParameter.addValue("ImageIconID", null);
				objDelProductsParameter.addValue("StartTime",
						retailerCustomPage.getSplofferStartTimeHrs() + ":" + retailerCustomPage.getSplofferStartTimeMin());
				objDelProductsParameter.addValue("EndTime",
						retailerCustomPage.getSplOfferEndTimeHrs() + ":" + retailerCustomPage.getSplOfferEndTimeMin());
			}

			objDelProductsParameter.addValue("PageDescription", null);
			objDelProductsParameter.addValue("PageShortDescription", retailerCustomPage.getSplOfferShortDescription());
			objDelProductsParameter.addValue("PageLongDescription", retailerCustomPage.getSplOfferLongDescription());
			objDelProductsParameter.addValue("StartDate", retailerCustomPage.getSplOfferStartDate());
			if ("".equals(Utility.checkNull(retailerCustomPage.getSplOfferEndDate()))) {
				retailerCustomPage.setSplOfferEndDate(null);
			}
			objDelProductsParameter.addValue("EndDate", retailerCustomPage.getSplOfferEndDate());
			objDelProductsParameter.addValue("UploadFileName", null);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
//			final Integer responseFromProc = (Integer) resultFromProcedure.get("Status");
			if (resultFromProcedure != null) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					pageID = (Integer) resultFromProcedure.get("PageID");

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside RetailerDAOImpl : usp_WebRetailerSpecialOfferCreation  : errorNumber  : " + errorNum + "errorMessage : "
							+ errorMsg);
					throw new ScanSeeWebSqlException("Inside RetailerDAOImpl : usp_WebRetailerSpecialOfferCreation  : errorNumber  : " + errorNum
							+ "errorMessage : " + errorMsg);
				}

			}

			LOG.info("usp_WebRetailerSpecialOfferCreation SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : insertRetailerCretaedPageInfo : " + exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}

		LOG.info("Exit method insertRetailerCretaedPageInfo");
		return pageID;
	}

	public RetailerCustomPage getRetailerPageInfo(Long userId, Long retailerId, Long pageId) throws ScanSeeWebSqlException {
		final String methodName = "getRetailerPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<RetailerCustomPage> retailerCustomPagelist = null;

		RetailerCustomPage searchresult = null;
//		Integer rowcount = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("searchresult", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerAnythingPageDisplayDetails");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchProduct = new MapSqlParameterSource();

			objSearchProduct.addValue("RetailID", retailerId);
			objSearchProduct.addValue("PageID", pageId);

			resultFromProcedure = simpleJdbcCall.execute(objSearchProduct);

			retailerCustomPagelist = (ArrayList<RetailerCustomPage>) resultFromProcedure.get("searchresult");
			if (retailerCustomPagelist != null && !retailerCustomPagelist.isEmpty()) {

				searchresult = retailerCustomPagelist.get(0);

			}
			LOG.info("usp_WebRetailerAnythingPageDisplayDetails SP is excuted successfully");
		}

		catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :searchProductRetailer :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchresult;
	}

	public String updateRetailerCustomPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException {
		LOG.info("Inside the method insertSplOfferPageInfo");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc;
		String response = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.withProcedureName("usp_WebRetailerAnythingPageUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailerCustomPage.getRetailerId());
			objDelProductsParameter.addValue("RetailLocationID", retailerCustomPage.getHiddenLocId());
			objDelProductsParameter.addValue("AnythingPageTitle", retailerCustomPage.getRetPageTitle());
			objDelProductsParameter.addValue("PageID", retailerCustomPage.getPageId());
			objDelProductsParameter.addValue("WebLink", retailerCustomPage.getRetCreatedPageattachLink());
			objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
			if (retailerCustomPage.getImageIconID() > 0) {
				objDelProductsParameter.addValue("ImageIconID", retailerCustomPage.getImageIconID());
			} else {

				objDelProductsParameter.addValue("ImageIconID", null);
			}

			objDelProductsParameter.addValue("PageDescription", null);
			objDelProductsParameter.addValue("PageShortDescription", retailerCustomPage.getRetPageShortDescription());
			objDelProductsParameter.addValue("PageLongDescription", retailerCustomPage.getRetPageLongDescription());

			if (null == retailerCustomPage.getRetCreatedPageStartDate() || "".equals(retailerCustomPage.getRetCreatedPageStartDate())) {
				objDelProductsParameter.addValue("StartDate", null);
			} else {
				objDelProductsParameter.addValue("StartDate", retailerCustomPage.getRetCreatedPageStartDate());
			}
			if ("".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate()))) {
				retailerCustomPage.setRetCreatedPageEndDate(null);
			}
			objDelProductsParameter.addValue("EndDate", retailerCustomPage.getRetCreatedPageEndDate());

			objDelProductsParameter.addValue("StartTime", "00:00");
			objDelProductsParameter.addValue("EndTime", "00:00");
			objDelProductsParameter.addValue("UploadFileName", retailerCustomPage.getPdfFileName());

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : fetchUploadPreviewList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetailerAnythingPageUpdation SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :updateRetailerCustomPageInfo :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return response;
	}

	/**
	 * this method will return retailer name and retailer image
	 * 
	 * @param retailerId
	 * @return retailer name and image
	 * @throws ScanSeeServiceException
	 *             as SQL Exception will be thrown.
	 */
	public Retailer fetchRetaielrInfo(Long retailerId) throws ScanSeeServiceException {
		final String methodName = "fetchLatLong in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Retailer> retailerlst = null;
		Retailer retailerObj = null;
		// String RETAILINFOQUERY =
		// "select RetailName retailName,RetailerImagePath retailerImagePath from Retailer where RetailID=?";

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebRetailerImageDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("retailerStoreDetails", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("RetailID", retailerId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			retailerlst = (List<Retailer>) resultFromProcedure.get("retailerStoreDetails");

			if (null == resultFromProcedure.get("ErrorNumber")) {
				if (!retailerlst.isEmpty() && retailerlst != null) {
					retailerObj = new Retailer();
					retailerObj = retailerlst.get(0);
				}
			}
		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return retailerObj;
	}

	public String validateRetailerPage(Long retialerId, String retailerLocId, Long pageID, int qrType) throws ScanSeeServiceException {
		final String methodName = "validateRetailerPage in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer responseFromProc = null;
		Integer pageId = null;
		String response = ApplicationConstants.FAILURE;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebRetailerCheckRetailerPage");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			// simpleJdbcCall.returningResultSet("retailerStoreDetails", new
			// BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("RetailID", retialerId);
			queryParams.addValue("RetailLocationID", retailerLocId);
			queryParams.addValue("QRTypeID", qrType);
			if (pageID == 0) {
				queryParams.addValue("InputPageID", null);
			} else {

				queryParams.addValue("InputPageID", pageID);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			responseFromProc = (Integer) resultFromProcedure.get("PageExists");

			if (responseFromProc == 0) {
				response = ApplicationConstants.FAILURE;

			} else {

				pageId = (Integer) resultFromProcedure.get("PageID");
				response = String.valueOf(pageId);
			}
			LOG.info("usp_WebRetailerCheckRetailerPage SP is excuted successfully");
		} catch (DataAccessException exception) {

			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			throw new ScanSeeServiceException(exception.getMessage());

		}
		return response;
	}

	public String deleteRetailerPage(Long pageID) throws ScanSeeServiceException {
		final String methodName = "deleteRetailerPage in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer responseFromProc = null;
//		Integer pageId = null;
		String response = ApplicationConstants.FAILURE;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebDeleteRetailerPage");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("PageID", pageID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				LOG.error(ApplicationConstants.ERROROCCURRED);
				throw new ScanSeeServiceException();
			}
			LOG.info("usp_WebDeleteRetailerPage SP is excuted successfully");
		} catch (DataAccessException exception) {

			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			throw new ScanSeeServiceException(exception.getMessage());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to domain Name for Application configuration
	 * 
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<AppConfiguration> getAppConfigForGeneralImages(String configType) throws ScanSeeWebSqlException {
		final String methodName = "getAppConfigForGeneralImages";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_GeneralImagesGetScreenContent");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside RetailerDAOImpl : getAppConfigForGeneralImages : Error occurred in usp_GeneralImagesGetScreenContent Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_GeneralImagesGetScreenContent SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error("Inside SupplierDAOImpl : getAppConfig : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return appConfigurationList;
	}

	/**
	 * This DAOImpl method updates location latitude and longitude fields on
	 * changes in address, state, city and postal code and call DAO layer.
	 * 
	 * @param longitude
	 *            as request parameter.
	 * @param latitude
	 *            as request parameter.
	 * @param retailerName
	 *            as request parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final void addLocationCoordinates(String latitude, String longitude, String retailerName) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : addLocationCoordinates ");
		if (!Utility.isEmptyOrNullString(retailerName) && !Utility.isEmptyOrNullString(latitude) && !Utility.isEmptyOrNullString(longitude)) {
			final String selectQuery = "select RetailID from Retailer where RetailName = ?";
			final String updateQuery = "update RetailLocation set RetailLocationLatitude = ? , RetailLocationLongitude = ? where RetailID = ?";
			try {
				final long retailerId = jdbcTemplate.queryForLong(selectQuery, retailerName);
				LOG.info("Inside RetailerDAOImpl : addLocationCoordinates : Retailer Name : " + retailerId + " Latitude --> " + latitude
						+ " Longitude --> " + longitude);
				jdbcTemplate.update(updateQuery, latitude, longitude, retailerId);
			} catch (DataAccessException e) {
				LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
				throw new ScanSeeWebSqlException(e);
			}
		}
	}

	public void addLocationCoordinatesWithRetailId(String latitude, String longitude, Integer retailerId, String storeId)
			throws ScanSeeWebSqlException {

		final String methodName = "addLocationCoordinatesWithRetailId";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (!Utility.isEmptyOrNullString(latitude) && !Utility.isEmptyOrNullString(longitude) && !Utility.isEmptyOrNullString(storeId)
				&& retailerId != 0) {

			String updateQuery = "update RetailLocation set RetailLocationLatitude = ? , RetailLocationLongitude = ? where RetailID = ? and StoreIdentification = ?";
			try {
				LOG.info("RetailerDAOImpl addLocationCoordinates :: Retailer Name : " + retailerId + " Latitude : " + latitude + " Longitude : "
						+ longitude + "Store Id : " + storeId);
				jdbcTemplate.update(updateQuery, latitude, longitude, retailerId, storeId);
			} catch (DataAccessException dataAccessExp) {
				LOG.error("Inside RetailerDAOImpl : addLocationCoordinates : " + dataAccessExp);
				throw new ScanSeeWebSqlException(dataAccessExp);
			}
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
	}

	public void addCorpAndStoreLocationCoordinates(String latitude, String longitude, String retailerName) throws ScanSeeWebSqlException {
		final String methodName = "addCorpAndStoreLocationCoordinates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (!Utility.isEmptyOrNullString(retailerName) && !Utility.isEmptyOrNullString(latitude) && !Utility.isEmptyOrNullString(longitude)) {
			String selectQuery = "select RetailID from Retailer where RetailName = ?";
			String checkCorpAndStoreQuery = "select RetailLocationID from RetailLocation where CorporateAndStore = 1 and RetailID = ?";
			String updateQuery = "update RetailLocation set RetailLocationLatitude = ? , RetailLocationLongitude = ? where RetailLocationID = ?";
			try {
				long retailerId = jdbcTemplate.queryForLong(selectQuery, retailerName);

				Long retailLocationId = jdbcTemplate.queryForLong(checkCorpAndStoreQuery, retailerId);

				LOG.info("RetailerDAOImpl addLocationCoordinates :: Retailer Name : " + retailerId + " Latitude : " + latitude + " Longitude : "
						+ longitude + " retailLocationId : " + retailLocationId);
				if (null != retailLocationId && retailLocationId.longValue() >= 1) {
					jdbcTemplate.update(updateQuery, latitude, longitude, retailLocationId);
				}
			} catch (EmptyResultDataAccessException dataAccessExp) {

				LOG.error("Inside RetailerDAOImpl : addCorpAndStoreLocationCoordinates : " + dataAccessExp);
				throw new ScanSeeWebSqlException(dataAccessExp);

			} catch (DataAccessException dataAccessExp) {
				LOG.error("Inside RetailerDAOImpl : addCorpAndStoreLocationCoordinates : " + dataAccessExp);
				throw new ScanSeeWebSqlException(dataAccessExp);
			}
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
	}

	/**
	 * This method is used to delete custom page
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public String deleteCustomPage(Long pageId, int retailerId) throws ScanSeeWebSqlException {
		final String methodName = "deleteRetailerPage in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer responseFromProc = null;
		String response = ApplicationConstants.FAILURE;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebRetailerCustomPageDeletion");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("RetailID", retailerId);
			queryParams.addValue("PageID", pageId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			responseFromProc = (Integer) resultFromProcedure.get("Status");
			if (responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				LOG.error(ApplicationConstants.ERROROCCURRED);
			}
			LOG.info("usp_WebRetailerCustomPageDeletion SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This DAOImpl method will get one Welcome page Advertisement info from
	 * Database based on parameter(adsSplashID).
	 * 
	 * @param adsSplashID
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocationAdvertisement> getWelcomePageForDisplay(Long adsSplashID) throws ScanSeeWebSqlException {
		final String methodName = "getWelcomePageForDisplay";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerLocationAdvertisement> arAdsList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerWelcomePageDisplayDetails");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("WelcomePageList", new BeanPropertyRowMapper<RetailerLocationAdvertisement>(
					RetailerLocationAdvertisement.class));

			final MapSqlParameterSource objAdsParameter = new MapSqlParameterSource();
			objAdsParameter.addValue("AdvertisementSplashID", adsSplashID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objAdsParameter);
			arAdsList = (List<RetailerLocationAdvertisement>) resultFromProcedure.get("WelcomePageList");
			LOG.info("usp_WebRetailerWelcomePageDisplayDetails SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getWelcomePageForDisplay : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arAdsList;
	}

	public String batchUploadDiscardedProducts(ArrayList<RetailProduct> prodList, int retailID, long userId, Integer logId)
			throws ScanSeeWebSqlException {
		LOG.info("Inside SupplierDAOImpl : batchUploadDiscardedProducts ");
		String response = null;

		Object[] values = null;
		List<Object[]> batch = new ArrayList<Object[]>();

		try {
			SimpleJdbcTemplate simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			for (RetailProduct objMgProducts : prodList) {
				values = new Object[] { Utility.checkNull(objMgProducts.getStoreIdentificationID()), Utility.checkNull(objMgProducts.getScanCode()),
						objMgProducts.getLongDescription(), null, objMgProducts.getProductPrice(), objMgProducts.getProductSalePrice(),
						Utility.getFormattedDateTime(objMgProducts.getSaleStartDate()), Utility.getFormattedDateTime(objMgProducts.getSaleEndDate()),
						null, userId, retailID, objMgProducts.getErrorMessage(), logId };
				batch.add(values);
			}
			int[] updateCounts = simpleJdbcTemplate.batchUpdate(
					"INSERT INTO UploadRetailLocationDiscardedProducts VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", batch);
			if (updateCounts.length != 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (DataAccessException exception) {
			LOG.error("Inside SupplierDAOImpl : addBatchUploadProductsToStage : " + exception.getMessage());
		} catch (Exception exception) {
			LOG.error("Inside SupplierDAOImpl : addBatchUploadProductsToStage : " + exception.getMessage());
		}
		return response;
	}

	public Integer getStageTableLogId(int retailerId, Long userId, String fileName) throws ScanSeeWebSqlException {
		final String methodName = "getStageTableLogId";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer logId = null;
		Integer status = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebUploadRetailerProductsLogCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailerID", retailerId);
			objSearchRebParameter.addValue("UserID", userId);
			objSearchRebParameter.addValue("FileName", fileName);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);

			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				logId = (Integer) resultFromProcedure.get("UploadRetailLocationProductLogID");
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside RetailerDAOImpl : getStageTableLogId : Error occurred in usp_WebUploadRetailerProductsLogCreationLogCreation Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebUploadRetailerProductsLogCreation SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getAdsForDisplay : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return logId;
	}

	public ArrayList<RetailProduct> getRejectedRetProducts(int retailerId, Long userId, Integer logId) throws ScanSeeWebSqlException {

		ArrayList<RetailProduct> objRetailerprod = new ArrayList<RetailProduct>();
		Map<String, Object> resultFromProcedure = null;
		final String methodName = "getRejectedRetProducts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerFetchDiscardedRetailLocationProducts");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("rejectedprodList", new BeanPropertyRowMapper<RetailProduct>(RetailProduct.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailerID", retailerId);
			objSearchRebParameter.addValue("UserID", userId);
			objSearchRebParameter.addValue("UploadRetailLocationProductLogID", logId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			objRetailerprod = (ArrayList<RetailProduct>) resultFromProcedure.get("rejectedprodList");
			LOG.info("usp_WebRetailerFetchDiscardedRetailLocationProducts SP is excuted successfully");
		} catch (EmptyResultDataAccessException exception) {
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objRetailerprod;
	}

	public String deleteRetailerBatchUploadStageTable(int retailID, long userId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : deleteRetailerBatchUploadStageTable ");
		String response = null;
		Integer status = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerDeleteUploadRetailerProduct");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailID", retailID);
			objSearchRebParameter.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);

			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside RetailerDAOImpl : getStageTableLogId : Error occurred in usp_WebUploadRetailerProductsLogCreationLogCreation Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

			LOG.info("usp_WebRetailerDeleteUploadRetailerProduct SP is excuted successfully");

		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : deleteRetailerBatchUploadStageTable : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	/**
	 * This DAOImpl method will update Retailer Location Advertisement Info.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String updateWelcomePageInfo(RetailerLocationAdvertisement objRetLocAds) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : updateWelcomePageInfo ");
		String strStatus = null;
		Map<String, Object> resultFmProc = null;
		Integer responseFmProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerWelcomePageUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objUpdateParameter = new MapSqlParameterSource();

			objUpdateParameter.addValue("WelcomePageID", objRetLocAds.getRetailLocationAdvertisementID());
			objUpdateParameter.addValue("RetailID", objRetLocAds.getRetailID());
			objUpdateParameter.addValue("RetailLocationIds", objRetLocAds.getRetailLocationIds());
			objUpdateParameter.addValue("WelcomePageName", objRetLocAds.getAdvertisementName());
			objUpdateParameter.addValue("WelcomePageImagePath", objRetLocAds.getStrBannerAdImagePath());
			objUpdateParameter.addValue("StartDate", objRetLocAds.getAdvertisementDate());
			if ("".equals(Utility.checkNull(objRetLocAds.getAdvertisementEndDate()))) {
				objRetLocAds.setAdvertisementEndDate(null);
			}
			objUpdateParameter.addValue("EndDate", objRetLocAds.getAdvertisementEndDate());

			resultFmProc = simpleJdbcCall.execute(objUpdateParameter);
			responseFmProc = (Integer) resultFmProc.get(ApplicationConstants.STATUS);
			if (null != responseFmProc && responseFmProc.intValue() == 0) {
				strStatus = ApplicationConstants.SUCCESS;
				objRetLocAds.setInvalidLocationIds((String) resultFmProc.get("InvalidRetailLocations"));
			} else
				if (null != responseFmProc && responseFmProc.intValue() == 1) {
					strStatus = ApplicationConstants.FAILURE;
				}
			LOG.info("updateWelcomePageInfo SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : updateWelcomePageInfo : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}

		return strStatus;
	}

	/**
	 * This DAOImpl method will return list location info from Database based on
	 * parameter(adsSplashID).
	 * 
	 * @param adsSplashID
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocation> getLocationIDForWelcomePage(Long adsSplashID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getLocationIDForWelcomePage ");
		List<RetailerLocation> arAdsList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerWelcomePageRetailLocationDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("LocationIdPageList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));

			final MapSqlParameterSource objAdsParameter = new MapSqlParameterSource();
			objAdsParameter.addValue("AdvertisementSplashID", adsSplashID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objAdsParameter);
			arAdsList = (List<RetailerLocation>) resultFromProcedure.get("LocationIdPageList");
			LOG.info("usp_WebRetailerWelcomePageRetailLocationDisplay SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getLocationIDForWelcomePage : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arAdsList;
	}

	/**
	 * This DAOImpl method deletes the welcome page from list based on input (if
	 * expireFlag is 1 Stop campaign sp's is execute else deleteFlag is 1 delete
	 * sp's is execute).
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteWelcomePage(RetailerLocationAdvertisement objRetLocAds) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl :deleteWelcomePage ");
		String strResponse = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			if (objRetLocAds.getExpireFlag() == 0 && objRetLocAds.getDeleteFlag() == 1) {
				simpleJdbcCall.withProcedureName("usp_WebRetailerWelcomePageDeletion");
			} else
				if (objRetLocAds.getExpireFlag() == 1 && objRetLocAds.getDeleteFlag() == 0) {
					simpleJdbcCall.withProcedureName("usp_WebRetailerWelcomePageExpiration");

				}
			final MapSqlParameterSource objDelWelcomePageParam = new MapSqlParameterSource();

			objDelWelcomePageParam.addValue("RetailID", objRetLocAds.getRetailID());
			objDelWelcomePageParam.addValue("WelcomePageID", objRetLocAds.getRetailLocationAdvertisementID());

			resultFromProcedure = simpleJdbcCall.execute(objDelWelcomePageParam);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : deleteWelcomePage : " + exception);
			if (null == responseFromProc || responseFromProc.intValue() != 0) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetaierDAOImpl :deleteWelcomePage : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			strResponse = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return strResponse;
	}

	/**
	 * This DAOImpl method will return list of images to display
	 * 
	 * @return Retailer Image Icon List List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<RetailerImages> getRetailerImageIconsDisplay(String pageType) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getLocationIDForWelcomePage ");
		ArrayList<RetailerImages> imageList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerImageIconsDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("LocationIdPageList", new BeanPropertyRowMapper<RetailerImages>(RetailerImages.class));

			final MapSqlParameterSource objAdsParameter = new MapSqlParameterSource();

			objAdsParameter.addValue("PageType", pageType);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objAdsParameter);
			imageList = (ArrayList<RetailerImages>) resultFromProcedure.get("LocationIdPageList");
			LOG.info("usp_WebRetailerImageIconsDisplay SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getLocationIDForWelcomePage : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return imageList;
	}

	/**
	 * This method is used to display the list of special offer pages.
	 * 
	 * @param retailerId
	 * @param searchKey
	 * @param currentPage
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getspecialOfferList(int retailerId, RetailerCustomPage objCustomPage, int currentPage, int recordCount)
			throws ScanSeeWebSqlException {
		final String methodName = "getspecialOfferList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo customPageResp = null;
		Map<String, Object> resultFromProcedure = null;
		List<RetailerCustomPage> pageList = new ArrayList<RetailerCustomPage>();
		Integer rowcount = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerSpecialOfferDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("specialOfferPageList", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			final MapSqlParameterSource objSpecialOfferParam = new MapSqlParameterSource();
			objSpecialOfferParam.addValue("RetailID", retailerId);
			objSpecialOfferParam.addValue("SearchParameter", objCustomPage.getSearchKey());
			objSpecialOfferParam.addValue("LowerLimit", currentPage);
			objSpecialOfferParam.addValue("RecordCount", recordCount);
			objSpecialOfferParam.addValue("SpecialOfferPageID", objCustomPage.getPageIds());
			objSpecialOfferParam.addValue("SortOrder", objCustomPage.getSortIds());
			resultFromProcedure = simpleJdbcCall.execute(objSpecialOfferParam);

			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					customPageResp = new SearchResultInfo();

					pageList = (List<RetailerCustomPage>) resultFromProcedure.get("specialOfferPageList");
					if (null != pageList && !pageList.isEmpty()) {
						rowcount = (Integer) resultFromProcedure.get("RowCount");
						customPageResp.setPageList(pageList);
						customPageResp.setTotalSize(rowcount);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : getspecialOfferList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		} catch (Exception exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return customPageResp;
	}

	/**
	 * This DAOImpl method will create banner page.
	 * 
	 * @param retLocationAds
	 *            instance of RetailerLocationAdvertisement
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public String buildBannerAdInfo(RetailerLocationAdvertisement objAdvertisement) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl :buildBannerAdInfo ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerBannerAdCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objBuildBannerAds = new MapSqlParameterSource();
			objBuildBannerAds.addValue("RetailID", objAdvertisement.getRetailID());
			objBuildBannerAds.addValue("RetailLocationIds", objAdvertisement.getRetailLocationIds());
			objBuildBannerAds.addValue("BannerAdName", objAdvertisement.getAdvertisementName());
			objBuildBannerAds.addValue("BannerAdURL", objAdvertisement.getRibbonXAdURL());
			objBuildBannerAds.addValue("BannerAdPageImagePath", objAdvertisement.getStrBannerAdImagePath());
			objBuildBannerAds.addValue("StartDate", objAdvertisement.getAdvertisementDate());
			if ("".equals(Utility.checkNull(objAdvertisement.getAdvertisementEndDate()))) {
				objAdvertisement.setAdvertisementEndDate(null);
			}
			objBuildBannerAds.addValue("EndDate", objAdvertisement.getAdvertisementEndDate());
			resultFromProcedure = simpleJdbcCall.execute(objBuildBannerAds);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				response = ApplicationConstants.SUCCESS;
				objAdvertisement.setInvalidLocationIds((String) resultFromProcedure.get("InvalidRetailLocations"));
			} else
				if (null != responseFromProc && responseFromProc.intValue() == 1) {
					response = ApplicationConstants.FAILURE;
				}

			LOG.info("usp_WebRetailerBannerAdCreation SP is excuted successfully");
		} catch (DataAccessException exception) {

			LOG.error("Inside RetailerDAOImpl : buildBannerAdInfo : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return response;
	}

	/**
	 * This DAOImpl method will display retailer banner page details based on
	 * input parameter (bannerId) and its status return to service method..
	 * 
	 * @param adsSplashID
	 *            as request parameter.
	 * @return arAdsList,Retailer Location Advertisement details.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final List<RetailerLocation> getLocationIDForBannerPage(Long adsSplashID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getLocationIDForBannerPage ");
		List<RetailerLocation> arAdsList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerBannerAdsRetailLocationDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("BannerLocationList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			final MapSqlParameterSource objAdsParameter = new MapSqlParameterSource();
			objAdsParameter.addValue("AdvertisementBannerID", adsSplashID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objAdsParameter);
			arAdsList = (List<RetailerLocation>) resultFromProcedure.get("BannerLocationList");
			LOG.info("usp_WebRetailerBannerAdsRetailLocationDisplay SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arAdsList;
	}

	/**
	 * This DAOImpl method deletes the banner page from list based on input (if
	 * expireFlag is 1 Stop campaign sp's is execute else deleteFlag is 1 delete
	 * sp's is execute).
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @param adsIDs
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteBannerPage(RetailerLocationAdvertisement objBannerAds) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl :deleteBannerPage ");
		String strResponse = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			if (objBannerAds.getExpireFlag() == 0 && objBannerAds.getDeleteFlag() == 1) {
				simpleJdbcCall.withProcedureName("usp_WebRetailerBannerAdDeletion");
			} else
				if (objBannerAds.getDeleteFlag() == 0 && objBannerAds.getExpireFlag() == 1) {
					simpleJdbcCall.withProcedureName("usp_WebRetailerBannerAdExpiration");

				}
			final MapSqlParameterSource objDelBannerPageParam = new MapSqlParameterSource();
			objDelBannerPageParam.addValue("RetailID", objBannerAds.getRetailID());
			objDelBannerPageParam.addValue("BannerAdID", objBannerAds.getRetailLocationAdvertisementID());

			resultFromProcedure = simpleJdbcCall.execute(objDelBannerPageParam);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : deleteBannerPage : " + exception);
			if (null == responseFromProc || responseFromProc.intValue() != 0) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetaierDAOImpl :deleteBannerPage : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			strResponse = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return strResponse;
	}

	/**
	 * This DAOImpl method will update existing banner page details based return
	 * based on input parameter (bannerId)and its status return to service
	 * method.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String updateBuildBannerInfo(RetailerLocationAdvertisement objRetLocAds) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : updateBuildBannerInfo ");
		String strStatus = null;
		Map<String, Object> resultFmProc = null;
		Integer responseFmProc = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerBannerAdUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objUpdateParameter = new MapSqlParameterSource();

			objUpdateParameter.addValue("BannerAdID", objRetLocAds.getRetailLocationAdvertisementID());
			objUpdateParameter.addValue("RetailID", objRetLocAds.getRetailID());
			objUpdateParameter.addValue("RetailLocationIds", objRetLocAds.getRetailLocationIds());
			objUpdateParameter.addValue("BannerAdName", objRetLocAds.getAdvertisementName());
			objUpdateParameter.addValue("BannerAdURL", objRetLocAds.getRibbonXAdURL());
			objUpdateParameter.addValue("BannerAdImagePath", objRetLocAds.getStrBannerAdImagePath());
			objUpdateParameter.addValue("StartDate", objRetLocAds.getAdvertisementDate());
			if ("".equals(Utility.checkNull(objRetLocAds.getAdvertisementEndDate()))) {
				objRetLocAds.setAdvertisementEndDate(null);
			}
			objUpdateParameter.addValue("EndDate", objRetLocAds.getAdvertisementEndDate());

			resultFmProc = simpleJdbcCall.execute(objUpdateParameter);
			responseFmProc = (Integer) resultFmProc.get(ApplicationConstants.STATUS);
			if (null != responseFmProc && responseFmProc.intValue() == 0) {
				strStatus = ApplicationConstants.SUCCESS;
				objRetLocAds.setInvalidLocationIds((String) resultFmProc.get("InvalidRetailLocations"));
			} else
				if (null != responseFmProc && responseFmProc.intValue() == 1) {
					strStatus = ApplicationConstants.FAILURE;
				}
			LOG.info("usp_WebRetailerBannerAdUpdation SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return strStatus;
	}

	/**
	 * This DAOImpl method will display list of created Banner by the retailer
	 * or by searching Banner Names and and its status return to service method.
	 * 
	 * @param lRetailID
	 *            as request parameter.
	 * @param strAdsName
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final SearchResultInfo getBannerAdsByAdsNames(Long lRetailID, String strAdsName, int lowerLimit, int recordCount)
			throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getBannerAdsByAdsNames ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<RetailerLocationAdvertisement> arAdsNamesList = null;
		SearchResultInfo searchresult = null;
		Integer rowcount = null;
		searchresult = new SearchResultInfo();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerBannerAdsDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("searchBuildBanner", new BeanPropertyRowMapper<RetailerLocationAdvertisement>(
					RetailerLocationAdvertisement.class));

			final MapSqlParameterSource objAdsNameParameters = new MapSqlParameterSource();

			objAdsNameParameters.addValue("RetailID", lRetailID);
			objAdsNameParameters.addValue("SearchParameter", strAdsName);
			objAdsNameParameters.addValue("LowerLimit", lowerLimit);
			objAdsNameParameters.addValue("ShowAll", 1);
			objAdsNameParameters.addValue("RecordCount", recordCount);
			resultFromProcedure = simpleJdbcCall.execute(objAdsNameParameters);
			arAdsNamesList = (ArrayList) resultFromProcedure.get("searchBuildBanner");

			if (arAdsNamesList != null && !arAdsNamesList.isEmpty()) {
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setAddsList(arAdsNamesList);
				searchresult.setTotalSize(rowcount);
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : getBannerAdsByAdsNames  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setTotalSize(rowcount);
			}
			LOG.info("usp_WebRetailerBannerAdsDisplay SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return searchresult;
	}

	/**
	 * This DAOImpl method will get one Banner Advertisement info from Database
	 * based on input parameter(adsSplashID).
	 * 
	 * @param adsSplashID
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocationAdvertisement> getBannerPageForDisplay(Long adsSplashID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getBannerPageForDisplay ");
		List<RetailerLocationAdvertisement> arAdsList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerBannerAdsDisplayDetails");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("BannerPageList", new BeanPropertyRowMapper<RetailerLocationAdvertisement>(
					RetailerLocationAdvertisement.class));
			final MapSqlParameterSource objAdsParameter = new MapSqlParameterSource();
			objAdsParameter.addValue("AdvertisementBannerID", adsSplashID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objAdsParameter);
			arAdsList = (List<RetailerLocationAdvertisement>) resultFromProcedure.get("BannerPageList");
			LOG.info("usp_WebRetailerBannerAdsDisplayDetails SP is excuted successfully");
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getWelcomePageForDisplay : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arAdsList;
	}

	public RetailerCustomPage getRetailerSplOfrPageInfo(Long retailerId, Long pageId) throws ScanSeeWebSqlException {
		final String methodName = "getRetailerPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<RetailerCustomPage> retailerCustomPagelist = null;

		RetailerCustomPage searchresult = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("searchresult", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerSpecialOfferDisplayDetails");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchProduct = new MapSqlParameterSource();

			objSearchProduct.addValue("RetailID", retailerId);
			objSearchProduct.addValue("PageID", pageId);

			resultFromProcedure = simpleJdbcCall.execute(objSearchProduct);

			retailerCustomPagelist = (ArrayList<RetailerCustomPage>) resultFromProcedure.get("searchresult");
			if (retailerCustomPagelist != null && !retailerCustomPagelist.isEmpty()) {

				searchresult = retailerCustomPagelist.get(0);

			}
			LOG.info("usp_WebRetailerAnythingPageDisplayDetails SP is excuted successfully");
		}

		catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :searchProductRetailer :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchresult;
	}

	/**
	 * This DAOImpl method is used for updating special offer page.
	 * 
	 * @param retailerCustomPage
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String updateRetailerSplOfrPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException {
		LOG.info("Inside the method insertSplOfferPageInfo");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc;
		String response = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.withProcedureName("usp_WebRetailerSpecialOfferUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailerCustomPage.getRetailerId());
			objDelProductsParameter.addValue("PageID", retailerCustomPage.getPageId());
			objDelProductsParameter.addValue("RetailLocationID", retailerCustomPage.getSplOfferLocId());
			objDelProductsParameter.addValue("SpecialOfferPageTitle", retailerCustomPage.getSplOfferTitle());
			objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
			objDelProductsParameter.addValue("WebLink", retailerCustomPage.getSplOfferattachLink());
			if (retailerCustomPage.getImageIconID() > 0) {
				objDelProductsParameter.addValue("ImageIconID", retailerCustomPage.getImageIconID());
			} else {
				objDelProductsParameter.addValue("ImageIconID", null);
			}
			objDelProductsParameter.addValue("PageDescription", retailerCustomPage.getSplOfferDescription());
			objDelProductsParameter.addValue("PageShortDescription", retailerCustomPage.getSplOfferShortDescription());
			objDelProductsParameter.addValue("PageLongDescription", retailerCustomPage.getSplOfferLongDescription());
			objDelProductsParameter.addValue("StartDate", retailerCustomPage.getSplOfferStartDate());
			if ("".equals(Utility.checkNull(retailerCustomPage.getSplOfferEndDate()))) {
				retailerCustomPage.setSplOfferEndDate(null);
			}
			objDelProductsParameter.addValue("EndDate", retailerCustomPage.getSplOfferEndDate());
			if (null != retailerCustomPage.getSplofferStartTimeHrs()) {
				objDelProductsParameter.addValue("StartTime",
						retailerCustomPage.getSplofferStartTimeHrs() + ":" + retailerCustomPage.getSplofferStartTimeMin());
			} else {
				objDelProductsParameter.addValue("StartTime", "00:00");
			}
			if (null != retailerCustomPage.getSplofferStartTimeHrs()) {
				objDelProductsParameter.addValue("EndTime",
						retailerCustomPage.getSplOfferEndTimeHrs() + ":" + retailerCustomPage.getSplOfferEndTimeMin());
			} else {
				objDelProductsParameter.addValue("EndTime", "00:00");
			}

			objDelProductsParameter.addValue("UploadFileName", null);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : fetchUploadPreviewList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetailerAnythingPageUpdation SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :updateRetailerCustomPageInfo :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return response;
	}

	public RetailerCustomPage getRetLocQRcode(Long retLocId, int retailerId, Long userId) throws ScanSeeWebSqlException {

//		String result = null;
//		ArrayList<ContactDetail> objRetailerLocation = new ArrayList<ContactDetail>();
		Map<String, Object> resultFromProcedure = null;
//		ContactDetail objContact = new ContactDetail();
		final String methodName = "getAllStates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerCustomPage pageDetails = null;
		List<RetailerCustomPage> pageList = new ArrayList<RetailerCustomPage>();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebDisplayRetailLocationQRURL");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("pageList", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailID", retailerId);
			objSearchRebParameter.addValue("RetailLocationID", retLocId);
			objSearchRebParameter.addValue("UserID", userId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					pageList = (List<RetailerCustomPage>) resultFromProcedure.get("pageList");
					if (null != pageList && !pageList.isEmpty()) {

						pageDetails = pageList.get(0);
					}

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : fetchUploadPreviewList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebDisplayRetailerCustomPageURL SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : insertRetailerPageInfo : " + exception);

			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageDetails;
	}

	public Map<String, Map<String, String>> getRetailerDiscountPlans() throws ScanSeeWebSqlException {
		Map<String, Map<String, String>> discountPlanList = new HashMap<String, Map<String, String>>();
		Map<String, String> discountPrice = null;

		List<Map<String, Object>> discountDetailsList = this.jdbcTemplate
				.queryForList("SELECT RetailerDiscountPlanCode,DiscountPriceYearly,DiscountPriceMonthly from RetailerDiscountPlan where ActiveFlag = 1");
		Object monthlyDiscount = null;
		Object yearlyDiscount = null;
		Object discountCode = null;
		for (Map<String, Object> discountDetail : discountDetailsList) {

			discountPrice = new HashMap<String, String>();
			monthlyDiscount = discountDetail.get("DiscountPriceMonthly");
			if (null != monthlyDiscount) {
				discountPrice.put(MONTHLY, monthlyDiscount.toString());
			}
			yearlyDiscount = discountDetail.get("DiscountPriceYearly");
			if (null != yearlyDiscount) {
				discountPrice.put(YEARLY, yearlyDiscount.toString());
			}
			discountCode = discountDetail.get("RetailerDiscountPlanCode");
			if (null != discountCode) {
				discountPlanList.put(discountCode.toString().toUpperCase(), discountPrice);
			}
		}

		return discountPlanList;
	}

	/**
	 * This DAOImpl method will return the all RetailerLocation list based on
	 * input parameter.
	 * 
	 * @param searchKey
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocationProduct> getAllRetailerLocationList(Long retailerId, String searchKey) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllRetailerLocationList ");
		Map<String, Object> resultFromProcedure = null;
		List<RetailerLocationProduct> arRetLocatList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("StoreProcedureName");
			simpleJdbcCall.returningResultSet("retLocationList", new BeanPropertyRowMapper<RetailerLocationProduct>(RetailerLocationProduct.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			/*
			 * objSearchParameter.addValue("", );
			 * objSearchParameter.addValue("", );
			 * objSearchParameter.addValue("", );
			 */
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			final Integer responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			arRetLocatList = (List<RetailerLocationProduct>) resultFromProcedure.get("retLocationList");
		} catch (Exception exception) {
			LOG.error("Inside RetailerDAOImpl : getAllRetailerLocationList : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return arRetLocatList;
	}

	/**
	 * This DAOImpl method will return the Product details based on input
	 * parameter.
	 * 
	 * @param objRetLocationProduct
	 *            as request parameter.
	 * @return Product Details.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocationProduct> getAssocProdToLocationPop(RetailerLocationProduct objRetLocationProduct) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllRetailerLocationList ");
		Map<String, Object> resultFromProcedure = null;
		List<RetailerLocationProduct> arRetLocatList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerManageProductRetailLocationSearch");
			simpleJdbcCall.returningResultSet("retLocationList", new BeanPropertyRowMapper<RetailerLocationProduct>(RetailerLocationProduct.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("RetailerID", objRetLocationProduct.getRetailID());
			objSearchParameter.addValue("ProductID", objRetLocationProduct.getProductID());
			objSearchParameter.addValue("SearchKey", objRetLocationProduct.getSearchKey());
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			//final Integer responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			arRetLocatList = (List<RetailerLocationProduct>) resultFromProcedure.get("retLocationList");
			objRetLocationProduct.setProductName((String) resultFromProcedure.get("ProductName"));
			objRetLocationProduct.setProductImagePath((String) resultFromProcedure.get("ProductImagePath"));
			objRetLocationProduct.setProductID(Long.valueOf((Integer) resultFromProcedure.get("Product")));
		} catch (Exception exception) {
			LOG.error("Inside RetailerDAOImpl : getAllRetailerLocationList : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return arRetLocatList;
	}

	public String UpdateRetailLocationProduct(final ArrayList<RetailerLocationProductJson> locationJsonList, final Long retailerId)
			throws ScanSeeWebSqlException {
		final String methodName = "saveLocationList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = ApplicationConstants.SUCCESS;
		try {

			int[] arry = jdbcTemplate.batchUpdate("{call usp_WebRetailerProductSetupSearchResultsModification(?,?,?,?,?,?,?,?,?,?,?,?)}",

			new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					RetailerLocationProductJson objlocation = locationJsonList.get(i);
					ps.setLong(1, retailerId);
					ps.setInt(2, Integer.valueOf(objlocation.getLocation()));
					ps.setString(3, objlocation.getProductID());

					if (null != objlocation.getPrice() && !"".equals(objlocation.getPrice())) {
						ps.setString(4, objlocation.getPrice());

					} else {

						ps.setString(4, null);
					}
					if (null != objlocation.getSalePrice() && !"".equals(objlocation.getSalePrice())) {
						ps.setString(5, objlocation.getSalePrice());

					} else {

						ps.setString(5, null);
					}

					if (null != objlocation.getDescription() && !"".equals(objlocation.getDescription())) {
						ps.setString(6, objlocation.getDescription());
					} else {

						ps.setString(6, null);
					}
					if (null != objlocation.getSaleStartDate() && !"".equals(objlocation.getSaleStartDate())) {
						ps.setString(7, objlocation.getSaleStartDate());
					} else {

						ps.setString(7, null);
					}

					if (null != objlocation.getSaleEndDate() && !"".equals(objlocation.getSaleEndDate())) {
						ps.setString(8, objlocation.getSaleEndDate());
					} else {

						ps.setString(8, null);
					}

					ps.setString(9, "0");
					ps.setNull(10, Types.INTEGER);
					ps.setNull(11, Types.INTEGER);
					ps.setNull(12, Types.VARCHAR);
				}

				public int getBatchSize() {
					return locationJsonList.size();
				}
			});

		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : retailerLocationUpload : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return response;
	}

	/**
	 * This DAOImpl method to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param strSortOrder
	 *            as request parameter.
	 * @param strPageID
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String saveReorderList(String strSortOrder, String strPageID, int retailerId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : saveReorderList ");
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer status;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerAnythingPageSortOrderUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSaveOrderParam = new MapSqlParameterSource();

			objSaveOrderParam.addValue("RetailID", retailerId);
			objSaveOrderParam.addValue("AnyThingPageId", strPageID);
			objSaveOrderParam.addValue("SortOrder", strSortOrder);

			resultFromProcedure = simpleJdbcCall.execute(objSaveOrderParam);
			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				LOG.info("usp_WebRetailerAnythingPageSortOrderUpdation SP is excuted successfully");
				response = ApplicationConstants.SUCCESS;
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (DataAccessException exception) {
			LOG.info("usp_WebRetailerAnythingPageSortOrderUpdation SP is excuted successfully");
			LOG.error("Inside RetailerDAOImpl : saveReorderList : " + exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return response;
	}

	public RetailerRegistration saveFreeAppSiteRetailer(RetailerRegistration objRetailReg) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : saveFreeAppSiteRetailer ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
//		String enryptPassword = null;
//		boolean isDuplicateUserName;
		boolean isDuplicateRetailerName;
		boolean responseCount;
		Integer retailID = null;
//		Integer userId = null;
		RetailerRegistration objRetailRegResponse = new RetailerRegistration();

		List<Retailer> duplicateRetList = new ArrayList<Retailer>();
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebFreeAppListingRetailerSignUp");
			simpleJdbcCall.returningResultSet("duplicateRetList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			final MapSqlParameterSource objRetailRegisterParameter = new MapSqlParameterSource();
			objRetailRegisterParameter.addValue("RetailerName", objRetailReg.getRetailerName());
			objRetailRegisterParameter.addValue("Address1", objRetailReg.getAddress1());
			objRetailRegisterParameter.addValue("Address2", objRetailReg.getAddress2());
			objRetailRegisterParameter.addValue("State", objRetailReg.getState());
			objRetailRegisterParameter.addValue("City", objRetailReg.getCity());
			objRetailRegisterParameter.addValue("PostalCode", objRetailReg.getPostalCode());
			objRetailRegisterParameter.addValue("CorporatePhoneNo", Utility.removePhoneFormate(objRetailReg.getContactPhone()));
			objRetailRegisterParameter.addValue("RetailLocationLatitude", objRetailReg.getRetailerLocationLatitude());
			objRetailRegisterParameter.addValue("RetailLocationLongitude", objRetailReg.getRetailerLocationLongitude());
			// objRetailRegisterParameter.addValue("UserName",
			// objRetailReg.getUserName());
			// objRetailRegisterParameter.addValue("Password",
			// objRetailReg.getPassword());
			resultFromProcedure = simpleJdbcCall.execute(objRetailRegisterParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				// isDuplicateUserName = (Boolean)
				// resultFromProcedure.get("DuplicateFlag");
				isDuplicateRetailerName = (Boolean) resultFromProcedure.get("DuplicateRetailerFlag");
				/*
				 * if (isDuplicateUserName) { response =
				 * ApplicationConstants.DUPLICATE_USER;
				 * objRetailRegResponse.setResponse(response); LOG.info(
				 * "Inside RetailerDAOImpl : saveFreeAppSiteRetailer : Return response message : "
				 * + response); }
				 */
				if (isDuplicateRetailerName) {

					response = ApplicationConstants.DUPLICATE_BUSINESSNAME;
					objRetailRegResponse.setResponse(response);
					LOG.info("Inside RetailerDAOImpl : saveFreeAppSiteRetailer : Return response message : " + response);
				} else {
					retailID = (Integer) resultFromProcedure.get("ResponseRetailID");
					Integer locationId = (Integer) resultFromProcedure.get("ResponseRetailLocationID");
					// userId = (Integer)
					// resultFromProcedure.get("ResponseUserID");
					responseCount = (Boolean) resultFromProcedure.get("ResponseCount");
					duplicateRetList = (ArrayList) resultFromProcedure.get("duplicateRetList");
					if (responseCount) {
						duplicateRetList = (ArrayList) resultFromProcedure.get("duplicateRetList");

						objRetailRegResponse.setDuplicateRetList(duplicateRetList);

					}

					response = ApplicationConstants.SUCCESS;
					objRetailRegResponse.setResponse(response);
					objRetailRegResponse.setRetailID(retailID.longValue());
					objRetailRegResponse.setRetailLocId(locationId.longValue());
					// objRetailRegResponse.setUserId(userId);

					LOG.info("nside RetailerDAOImpl : saveFreeAppSiteRetailer : Return response message : " + response);
				}
				LOG.info("Inside RetailerDAOImpl : saveFreeAppSiteRetailer : responseFromProc is : " + responseFromProc);
			} else {
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : saveFreeAppSiteRetailer   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : saveFreeAppSiteRetailer :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("Inside RetailerDAOImpl : saveFreeAppSiteRetailer ");
		return objRetailRegResponse;
	}

	public String saveAppListLocationList(final List<RetailerLocation> locationList) throws ScanSeeWebSqlException {
		final String methodName = "saveAppListLocationList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
//		Integer responseFromProc = null;

		TransactionStatus status = null;
		try {

			DefaultTransactionDefinition paramTransactionDefinition = new DefaultTransactionDefinition();

			status = platformTxManager.getTransaction(paramTransactionDefinition);

			// final TransactionDefinition txDefn = new
			// DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
			// txStatus = txManager.getTransaction(txDefn);
			int[] arry = jdbcTemplate.batchUpdate("{call usp_WebAppListingRetailerAddRetailerLocation(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}",

			new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					RetailerLocation objlocation = locationList.get(i);
					ps.setInt(1, objlocation.getRetailerID().intValue());
					ps.setString(2, objlocation.getStoreIdentification());

					ps.setString(3, objlocation.getAddress1());
					ps.setString(4, objlocation.getState());
					ps.setString(5, objlocation.getCity());

					ps.setString(6, objlocation.getPostalCode());
					ps.setString(7, Utility.removePhoneFormate(objlocation.getPhonenumber()));
					ps.setString(8, objlocation.getRetailLocationUrl());

					if (null == objlocation.getRetailerLocationLatitude()) {
						ps.setDouble(9, 0.0);
					} else {

						ps.setDouble(9, objlocation.getRetailerLocationLatitude());
					}
					if (null == objlocation.getRetailerLocationLongitude()) {
						ps.setDouble(10, 0.0);
					} else {

						ps.setDouble(10, objlocation.getRetailerLocationLongitude());
					}
					ps.setString(11, objlocation.getKeyword());
					ps.setNull(12, Types.INTEGER);
					ps.setNull(13, Types.BIT);
					ps.setNull(14, Types.BIT);
					ps.setNull(15, Types.INTEGER);
					ps.setNull(16, Types.VARCHAR);
				}

				public int getBatchSize() {
					return locationList.size();
				}
			});

			response = ApplicationConstants.SUCCESS;
			platformTxManager.commit(status);

		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : saveAppListLocationList : " + exception.getMessage());
			platformTxManager.rollback(status);
		} catch (Exception exception) {
			LOG.error("Inside RetailerDAOImpl : saveAppListLocationList : " + exception.getMessage());
			platformTxManager.rollback(status);
		}
		LOG.info("isDataInserted++++++++++" + response);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public int insertAppListingAboutUsPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException {

		final String methodName = "insertAppListingAboutUsPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc;
		int pageID = 0;
		Integer errorNum = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.withProcedureName("usp_WebAppListingAboutUsPageCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailerCustomPage.getRetailerId());
			objDelProductsParameter.addValue("RetailLocationID", retailerCustomPage.getRetailLocationID());
			objDelProductsParameter.addValue("AnythingPageTitle", retailerCustomPage.getRetPageTitle());
			objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
			objDelProductsParameter.addValue("PageDescription", retailerCustomPage.getAboutUs());

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				pageID = (Integer) resultFromProcedure.get("PageID");
			} else {

				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : insertAppListingAboutUsPageInfo   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
			LOG.info("usp_WebAppListingAboutUsPageCreation SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : insertAppListingAboutUsPageInfo : " + exception);

			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info("Exit method insertAppListingAboutUsPageInfo");
		return pageID;
	}

	public List<RetailerLocation> retrievRetailLocation(int retailID) throws ScanSeeWebSqlException {
		final String methodName = "retrievRetailLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc;

		List<RetailerLocation> retailerLocations = new ArrayList<RetailerLocation>();
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.withProcedureName("usp_WebRetrieveRetailerLocation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("retailerLocation", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();
			objDelProductsParameter.addValue("RetailID", retailID);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				retailerLocations = (ArrayList<RetailerLocation>) resultFromProcedure.get("retailerLocation");
				LOG.info("usp_WebRetrieveRetailerLocation SP is excuted successfully");
			}

		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : retrievRetailLocation : " + exception);

			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info("Exit method retrievRetailLocation");
		return retailerLocations;
	}

	/**
	 * This DAOImpl method display the product details based on input
	 * parameter(productId) and its status return to service layer.
	 * 
	 * @param objRetLtnProduct
	 *            instance of RetailerLocationProduct.
	 * @return productList, one the products details.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final List<RetailerLocationProduct> getRetailerProductPreview(RetailerLocationProduct objRetLtnProduct) throws ScanSeeWebSqlException {
		LOG.info("Inside SupplierDAOImpl : getRetailerProductPreview ");
		Map<String, Object> resultFromProcedure = null;
		// Integer responseFromProc = null;
		List<RetailerLocationProduct> prodList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayRetailLocationProductInfo");
			simpleJdbcCall.returningResultSet("ProdList", new BeanPropertyRowMapper<RetailerLocationProduct>(RetailerLocationProduct.class));
			final MapSqlParameterSource objLtnProductParam = new MapSqlParameterSource();
			// objSearchprodParameter.addValue("ProductID", productId);
			objLtnProductParam.addValue("ProductID", objRetLtnProduct.getProductID());
			objLtnProductParam.addValue("RetailerID", objRetLtnProduct.getRetailID());
			objLtnProductParam.addValue("RetailLocationID", objRetLtnProduct.getRetailLocationID());
			resultFromProcedure = simpleJdbcCall.execute(objLtnProductParam);
			// responseFromProc = (Integer)
			// resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					prodList = (List) resultFromProcedure.get("ProdList");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : fetchProductPreviewList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebDisplayProductInfo is  executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return prodList;
	}

	public RetailerRegistration finalizeFreeAppSiteRetReg(RetailerRegistration objRetailReg) throws ScanSeeWebSqlException {

		LOG.info("Inside RetailerDAOImpl : finalizeFreeAppSiteRetReg ");

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		String response = null;
		boolean isDuplicateUserName;
		Integer userId = null;
		RetailerRegistration objRetailRegResponse = new RetailerRegistration();
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebFreeAppListingRetailerSignUpFinalization");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			final MapSqlParameterSource objRetailRegisterParameter = new MapSqlParameterSource();
			// objRetailRegisterParameter.addValue("UserID",
			// objRetailReg.getUserId());
			objRetailRegisterParameter.addValue("RetailID", objRetailReg.getRetailID());
			objRetailRegisterParameter.addValue("RetailLocationID", objRetailReg.getRetailLocId());
			objRetailRegisterParameter.addValue("BusinessCategory", objRetailReg.getbCategory());
			objRetailRegisterParameter.addValue("RetailerURL", objRetailReg.getWebUrl());
			objRetailRegisterParameter.addValue("RetailerKeyword", objRetailReg.getKeyword());
			objRetailRegisterParameter.addValue("ContactFirstName", objRetailReg.getContactFName());
			objRetailRegisterParameter.addValue("ContactLastName", objRetailReg.getContactLName());
			objRetailRegisterParameter.addValue("ContactPhone", Utility.removePhoneFormate(objRetailReg.getContactPhoneNo()));
			objRetailRegisterParameter.addValue("ContactEmail", objRetailReg.getContactEmail());
			objRetailRegisterParameter.addValue("CorporateAndSore", objRetailReg.isIslocation());
			objRetailRegisterParameter.addValue("ReferralName", objRetailReg.isIslocation());
			objRetailRegisterParameter.addValue("AssociateOrganizations", objRetailReg.getAssociateOrg());
			objRetailRegisterParameter.addValue("UserName", objRetailReg.getUserName());
			objRetailRegisterParameter.addValue("Password", objRetailReg.getPassword());
			if (objRetailReg.isIslocation() == false) {
				objRetailRegisterParameter.addValue("StoreIdentification", null);
			} else {
				objRetailRegisterParameter.addValue("StoreIdentification", objRetailReg.getStoreNum());
			}

			if(null != objRetailReg.getFilters() && !"".equals(objRetailReg.getFilters())) {
				objRetailRegisterParameter.addValue("AdminFilterID", objRetailReg.getFilters());
			} else {
				objRetailRegisterParameter.addValue("AdminFilterID", null);
			}
			if(null != objRetailReg.getFilterValues() && !"".equals(objRetailReg.getFilterValues())) {
				objRetailRegisterParameter.addValue("AdminFilterValueID", objRetailReg.getFilterValues());
			} else {
				objRetailRegisterParameter.addValue("AdminFilterValueID", null);
			}
			
			resultFromProcedure = simpleJdbcCall.execute(objRetailRegisterParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {

				isDuplicateUserName = (Boolean) resultFromProcedure.get("DuplicateFlag");

				if (isDuplicateUserName) {
					objRetailRegResponse.setResponse(ApplicationConstants.DUPLICATE_USER);

					LOG.info("Inside RetailerDAOImpl : finalizeFreeAppSiteRetReg : Return response message : " + response);
				} else {
					userId = (Integer) resultFromProcedure.get("ResponseUserID");
					objRetailRegResponse.setResponse(ApplicationConstants.SUCCESS);
					objRetailRegResponse.setUserId(userId);
				}

			} else {
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : finalizeFreeAppSiteRetReg   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : finalizeFreeAppSiteRetReg :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("Inside RetailerDAOImpl : finalizeFreeAppSiteRetReg ");
		return objRetailRegResponse;
	}

	public String validateDuplicateRetailer(Long retailId, int duplicateRetailId, int duplicateRetailLocId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : validateDuplicateRetailer ");

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		String response = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebFreeAppListingRetailerValidation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			final MapSqlParameterSource objRetailRegisterParameter = new MapSqlParameterSource();
			objRetailRegisterParameter.addValue("RetailerID", retailId);
			objRetailRegisterParameter.addValue("DuplicateRetailerID", duplicateRetailId);
			objRetailRegisterParameter.addValue("DuplicateRetailerLocationID", duplicateRetailLocId);

			resultFromProcedure = simpleJdbcCall.execute(objRetailRegisterParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {

				response = ApplicationConstants.SUCCESS;

			} else {
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : finalizeFreeAppSiteRetReg   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : finalizeFreeAppSiteRetReg :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("Inside RetailerDAOImpl : finalizeFreeAppSiteRetReg ");
		return response;
	}

	public String markAsDuplicateRetailer(Long retailId, Long retailLocId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : markAsDuplicateRetailer ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNumber;
		String errorMessage;
		String response = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebFreeAppListingRetailerFlagDuplicates");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			final MapSqlParameterSource objRetailRegisterParameter = new MapSqlParameterSource();
			objRetailRegisterParameter.addValue("RetailerID", retailId);
			objRetailRegisterParameter.addValue("DuplicateRetailerLocationID", retailLocId);
			// objRetailRegisterParameter.addValue("DuplicateRetailerLocationID",
			// duplicateRetailLocId);

			resultFromProcedure = simpleJdbcCall.execute(objRetailRegisterParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {

				response = ApplicationConstants.SUCCESS;

			} else {
				errorNumber = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				errorMessage = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : markAsDuplicateRetailer   : errorNumber  : " + errorNumber + "errorMessage : " + errorMessage);
				throw new ScanSeeWebSqlException(errorMessage);

			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : markAsDuplicateRetailer :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("Exit RetailerDAOImpl : markAsDuplicateRetailer ");
		return response;
	}

	/**
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<DropDown> displaySupplierRetailerDemographicsDD() throws ScanSeeWebSqlException {
		LOG.info("Inside SupplierDAOImpl : displaySupplierRetailerDemographicsDD ");
		ArrayList<DropDown> dropDownList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_SupplierRetailerDemographicsDropDownList");
			simpleJdbcCall.returningResultSet("DropDownList", new BeanPropertyRowMapper<DropDown>(DropDown.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure) {
				dropDownList = (ArrayList<DropDown>) resultFromProcedure.get("DropDownList");
			}
			LOG.info("usp_SupplierRetailerDemographicsDropDownList is  executed Successfully.");
		} catch (DataAccessException e) {
			LOG.error("Inside SupplierDAOImpl : displaySupplierRetailerDemographicsDD : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		return dropDownList;
	}

	/**
	 * This method is used to display the list of give away promotion pages.
	 * 
	 * @param retailerId
	 * @param searchKey
	 * @param currentPage
	 * @param recordCount
	 * @return SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getGiveAwayList(int retailerId, String searchKey, int currentPage, int recordCount) throws ScanSeeWebSqlException {
		final String methodName = "getGiveAwayList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo customPageResp = null;
		Map<String, Object> resultFromProcedure = null;
		List<RetailerCustomPage> pageList = new ArrayList<RetailerCustomPage>();
		Integer rowcount = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerGiveAwayPageDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("pageList", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailID", retailerId);
			objSearchRebParameter.addValue("SearchParameter", searchKey);
			objSearchRebParameter.addValue("LowerLimit", currentPage);
			objSearchRebParameter.addValue("RecordCount", recordCount);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);

			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					customPageResp = new SearchResultInfo();

					pageList = (List<RetailerCustomPage>) resultFromProcedure.get("pageList");
					if (null != pageList && !pageList.isEmpty()) {
						rowcount = (Integer) resultFromProcedure.get("RowCount");
						customPageResp.setPageList(pageList);
						customPageResp.setTotalSize(rowcount);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : getGiveAwayList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		} catch (Exception exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return customPageResp;
	}

	/**
	 * This method is used to delete Give away page
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return String
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public String deleteGiveAwayPage(Long pageId, int retailerId) throws ScanSeeWebSqlException {
		final String methodName = "deleteGiveAwayPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer responseFromProc = null;
		String response = ApplicationConstants.FAILURE;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("usp_WebRetailerGiveAwayPageDeletion");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("RetailID", retailerId);
			queryParams.addValue("PageID", pageId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);
			responseFromProc = (Integer) resultFromProcedure.get("Status");
			if (responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				LOG.error(ApplicationConstants.ERROROCCURRED);
			}
			LOG.info("usp_WebRetailerGiveAwayPageDeletion SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This DAOimpl method saves Giveaway page information.
	 * 
	 * @param retailerCustomPage
	 *            as a parameter.
	 * @return Generated PageID.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as DAO exception.
	 */
	public int saveGiveAwayPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException {
		final String methodName = "saveGiveAwayPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		int pageID = 0;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerGiveAwayPageCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailerCustomPage.getRetailerId());
			objDelProductsParameter.addValue("PageTitle", retailerCustomPage.getRetPageTitle());
			objDelProductsParameter.addValue("StartDate", retailerCustomPage.getRetCreatedPageStartDate());
			if ("".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate()))) {
				retailerCustomPage.setRetCreatedPageEndDate(null);
			}
			objDelProductsParameter.addValue("EndDate", retailerCustomPage.getRetCreatedPageEndDate());
			objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
			objDelProductsParameter.addValue("ShortDescription", retailerCustomPage.getShortDescription());
			objDelProductsParameter.addValue("LongDescription", retailerCustomPage.getLongDescription());
			objDelProductsParameter.addValue("Rules", retailerCustomPage.getRule());
			objDelProductsParameter.addValue("TermsandConditions", retailerCustomPage.getTermsandConditions());
			objDelProductsParameter.addValue("Quantity", retailerCustomPage.getQuantity());

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			//final Integer responseFromProc = (Integer) resultFromProcedure.get("Status");
			if (resultFromProcedure != null) {
				if (null == resultFromProcedure.get("ErrorNumber")) {
					pageID = (Integer) resultFromProcedure.get("PageID");

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside RetailerDAOImpl : usp_WebRetailerSpecialOfferCreation  : errorNumber  : " + errorNum + "errorMessage : "
							+ errorMsg);
					throw new ScanSeeWebSqlException("Inside RetailerDAOImpl : usp_WebRetailerGiveAwayPageCreation  : errorNumber  : " + errorNum
							+ "errorMessage : " + errorMsg);
				}

			}

			LOG.info("usp_WebRetailerGiveAwayPageCreation SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : saveGiveAwayPageInfo : " + exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageID;
	}

	/**
	 * Method to fetch giveaway page details.
	 * 
	 * @param retailId
	 *            as a parameter.
	 * @param pageId
	 *            as a parameter.
	 * @return RetailerCustomPage obj.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as DAO exception.
	 */
	public RetailerCustomPage getGiveAwayPageInfo(Long retailId, Long pageId) throws ScanSeeWebSqlException {

		final String methodName = "getGiveAwayPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<RetailerCustomPage> retailerCustomPagelist = null;

		RetailerCustomPage retailerCustomPage = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("retailerCustomPage", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerGiveAwayPageDetailsDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSearchProduct = new MapSqlParameterSource();

			objSearchProduct.addValue("RetailID", retailId);
			objSearchProduct.addValue("PageID", pageId);

			resultFromProcedure = simpleJdbcCall.execute(objSearchProduct);

			retailerCustomPagelist = (ArrayList<RetailerCustomPage>) resultFromProcedure.get("retailerCustomPage");
			if (retailerCustomPagelist != null && !retailerCustomPagelist.isEmpty()) {

				retailerCustomPage = retailerCustomPagelist.get(0);

			}
			LOG.info("usp_WebRetailerGiveAwayPageDetailsDisplay SP is excuted successfully");
		}

		catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :getGiveAwayPageInfo :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerCustomPage;
	}

	/**
	 * Method to update giveaway page details.
	 * 
	 * @param retailerCustomPage
	 *            as parameter
	 * @return Status, Success or Failure
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String updateGiveawayPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException {
		final String methodName = "updateGiveawayPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc;
		String response = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.withProcedureName("usp_WebRetailerGiveAwayPageUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();

			objDelProductsParameter.addValue("RetailID", retailerCustomPage.getRetailerId());
			objDelProductsParameter.addValue("QRRetailerCustomPageID", retailerCustomPage.getPageId());
			objDelProductsParameter.addValue("PageTitle", retailerCustomPage.getRetPageTitle());
			objDelProductsParameter.addValue("Image", retailerCustomPage.getRetailerImg());
			objDelProductsParameter.addValue("StartDate", retailerCustomPage.getRetCreatedPageStartDate());
			if ("".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate()))) {
				retailerCustomPage.setRetCreatedPageEndDate(null);
			}
			objDelProductsParameter.addValue("EndDate", retailerCustomPage.getRetCreatedPageEndDate());

			objDelProductsParameter.addValue("ShortDescription", retailerCustomPage.getShortDescription());
			objDelProductsParameter.addValue("LongDescription", retailerCustomPage.getLongDescription());
			objDelProductsParameter.addValue("Rules", retailerCustomPage.getRule());
			objDelProductsParameter.addValue("TermsandConditions", retailerCustomPage.getTermsandConditions());
			objDelProductsParameter.addValue("Quantity", retailerCustomPage.getQuantity());

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get("Status");

			if (responseFromProc == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : updateGiveawayPageInfo  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetailerGiveAwayPageUpdation SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl :updateGiveawayPageInfo :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is for displaying the QRCode for give away page.
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */

	public RetailerCustomPage getQRcodeForGAPage(Long pageId, int retailerId) throws ScanSeeWebSqlException {
		Map<String, Object> resultFromProcedure = null;
		final String methodName = "getQRcodeForGAPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerCustomPage pageDetails = null;
		List<RetailerCustomPage> pageList = new ArrayList<RetailerCustomPage>();
		try {
			LOG.info(ApplicationConstants.METHODSTART + methodName);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebDisplayGiveAwayQRPageURL");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("pageList", new BeanPropertyRowMapper<RetailerCustomPage>(RetailerCustomPage.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailID", retailerId);
			objSearchRebParameter.addValue("QRRetailerCustomPageID", pageId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					pageList = (List<RetailerCustomPage>) resultFromProcedure.get("pageList");
					if (null != pageList && !pageList.isEmpty()) {

						pageDetails = pageList.get(0);
					}

				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : getQRcodeForGAPage  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebDisplayGiveAwayQRPageURL SP is excuted successfully");
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : getQRcodeForGAPage : " + exception);

			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageDetails;
	}

	public List<Product> getPdtInfoForDealCoupon(String productIds, int retailerId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getPdtInfoForDealCoupon ");
		Map<String, Object> resultFromProcedure = null;
		// Integer responseFromProc = null;
		ArrayList<Product> listProduct = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerProductDetails");
			simpleJdbcCall.returningResultSet("listProductInfo", new BeanPropertyRowMapper<Product>(Product.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("RetailID", retailerId);
			objSearchParameter.addValue("ProductID", productIds);
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			// responseFromProc = (Integer)
			// resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					listProduct = (ArrayList) resultFromProcedure.get("listProductInfo");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside getPdtInfoForDealCoupon : getPdtInfoForDealRerun : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		} catch (Exception exception) {
			LOG.error("Inside getPdtInfoForDealCoupon : getPdtInfoForDealRerun : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebRetailerProductDetails is  executed Successfully.");
		return listProduct;
	}

	/**
	 * The DAOImpl method for displaying list of Hot deals by calling DAO.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return Hot deal Names,List of Hot deal Names.
	 */
	public final ArrayList<HotDealInfo> getAllHotDealsName(Long userId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllHotDealsName ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<HotDealInfo> arHotDealInfoList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerHotDealList");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("hotDealList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource objSearchHotDealParam = new MapSqlParameterSource();
			objSearchHotDealParam.addValue("RetailID", userId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchHotDealParam);
			if (null != resultFromProcedure) {
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg) {
					arHotDealInfoList = (ArrayList) resultFromProcedure.get("hotDealList");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside RetailerDAOImpl : getAllHotDealsName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arHotDealInfoList;
	}

	/**
	 * This DAOImpl method is used to get Hot dealView Report details.
	 * 
	 * @param hotDealId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final HotDealInfo getHotDealViewReport(int hotDealId, Long retailerId, int currentPage, int recordCount) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getHotDealViewReport ");
		Map<String, Object> resultFromProcedure = null;
		List<HotDealInfo> arHotDealInfoList = null;
		HotDealInfo objHotDealInfo = null;
		List<Contact> arContactList = null;
		try {
			objHotDealInfo = new HotDealInfo();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.returningResultSet("hotDealInfo", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			simpleJdbcCall.returningResultSet("userDetails", new BeanPropertyRowMapper<Contact>(Contact.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerHotDealReport");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objHDealViewReportParam = new MapSqlParameterSource();
			objHDealViewReportParam.addValue("RetailID", retailerId);
			objHDealViewReportParam.addValue("ProductHotDealID", hotDealId);
			objHDealViewReportParam.addValue("LowerLimit", currentPage);
			objHDealViewReportParam.addValue("RecordCount", recordCount);
			resultFromProcedure = simpleJdbcCall.execute(objHDealViewReportParam);
			if (resultFromProcedure != null) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum) {
					arHotDealInfoList = (ArrayList<HotDealInfo>) resultFromProcedure.get("hotDealInfo");
					arContactList = (List<Contact>) resultFromProcedure.get("userDetails");
					if (null != arHotDealInfoList && !arHotDealInfoList.isEmpty()) {
						objHotDealInfo = arHotDealInfoList.get(0);
					}
					if (null != arContactList && !arContactList.isEmpty()) {
						objHotDealInfo.setContact(arContactList);
					}
					objHotDealInfo.setTotalSize((Integer) resultFromProcedure.get("MaxCnt"));
				} else {
					LOG.info("Error Occured Inside RetailerDAOImpl : getHotDealViewReport: errorNum..." + errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return objHotDealInfo;
	}

	
	public RetailerRegistration findDuplicateRetailer(RetailerRegistration objRetailReg) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : findDuplicateRetailer ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		boolean responseCount;
		boolean duplicateRetFlag;
		// RetailerRegistration objRetailRegResponse = new
		// RetailerRegistration();
		List<Retailer> duplicateRetList = new ArrayList<Retailer>();
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebAppListingDuplicateRetailers");
			simpleJdbcCall.returningResultSet("duplicateRetList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			final MapSqlParameterSource objRetailRegisterParameter = new MapSqlParameterSource();
			objRetailRegisterParameter.addValue("RetailerName", objRetailReg.getRetailerName());
			objRetailRegisterParameter.addValue("Address1", objRetailReg.getAddress1());
			objRetailRegisterParameter.addValue("Address2", objRetailReg.getAddress2());
			objRetailRegisterParameter.addValue("State", objRetailReg.getState());
			objRetailRegisterParameter.addValue("City", objRetailReg.getCity());
			objRetailRegisterParameter.addValue("PostalCode", objRetailReg.getPostalCode());
			objRetailRegisterParameter.addValue("CorporatePhoneNo", Utility.removePhoneFormate(objRetailReg.getContactPhone()));
			resultFromProcedure = simpleJdbcCall.execute(objRetailRegisterParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0) {

				duplicateRetFlag = (Boolean) resultFromProcedure.get("DuplicateRetailerFlag");
				if (duplicateRetFlag) {
					objRetailReg.setResponse(ApplicationConstants.DUPLICATE_RETAILERNAME);
				} else {
					responseCount = (Boolean) resultFromProcedure.get("ResponseCount");
					if (responseCount) {
						duplicateRetList = (ArrayList<Retailer>) resultFromProcedure.get("duplicateRetList");
						if (null != objRetailReg.getDuplicateRetList() && !objRetailReg.getDuplicateRetList().isEmpty()) {

							objRetailReg.getDuplicateRetList().clear();
						}
						objRetailReg.setDuplicateRetList(duplicateRetList);
					}
					objRetailReg.setResponse(ApplicationConstants.SUCCESS);
				}

				LOG.info("Inside RetailerDAOImpl : findDuplicateRetailer : Return response message : " + response);
			} else {
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : findDuplicateRetailer   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
		} catch (DataAccessException exception) {
			LOG.error("Inside RetailerDAOImpl : findDuplicateRetailer :" + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("Exit RetailerDAOImpl : findDuplicateRetailer ");
		return objRetailReg;
	}

	/**
	 * This DAOImpl method will return list of locations from Database based on
	 * parameter(pageId & retailerId).
	 * 
	 * @param pageId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocation> getLocationIDForAnythingPage(Long pageId, int retailerId) throws ScanSeeWebSqlException {
		final String methodName = "getLocationIDForAnythingPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<RetailerLocation> pageLocationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCustomPageRetailLcoationDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("LocationIdPageList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));

			final MapSqlParameterSource objAdsParameter = new MapSqlParameterSource();
			objAdsParameter.addValue("RetailID", retailerId);
			objAdsParameter.addValue("PageID", pageId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objAdsParameter);
			pageLocationList = (List<RetailerLocation>) resultFromProcedure.get("LocationIdPageList");
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageLocationList;
	}

	/**
	 * This DAOImpl method to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param strSortOrder
	 *            as request parameter.
	 * @param strPageID
	 *            as request parameter.
	 * @param iRetailId
	 *            as request parameter.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String saveReorderSpecialOfferList(String strSortOrder, String strPageID, int iRetailId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : saveReorderSpecialOfferList ");
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer status;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebRetailerSpecialOfferPageSortOrderUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objSaveReorderParam = new MapSqlParameterSource();

			objSaveReorderParam.addValue("RetailID", iRetailId);
			objSaveReorderParam.addValue("SpecialOfferPageID", strPageID);
			objSaveReorderParam.addValue("SortOrder", strSortOrder);

			resultFromProcedure = simpleJdbcCall.execute(objSaveReorderParam);
			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				LOG.info("usp_WebRetailerAnythingPageSortOrderUpdation SP is excuted successfully");
				response = ApplicationConstants.SUCCESS;
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (DataAccessException exception) {
			LOG.info("usp_WebRetailerSpecialOfferPageSortOrderUpdation SP is excuted successfully");
			LOG.error("Inside RetailerDAOImpl : saveReorderSpecialOfferList : " + exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		return response;
	}

	/**
	 * The DAOImpl method for displaying all the Event categories and it return
	 * to service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return Categories,List of event categories.
	 */
	public final ArrayList<Category> getAllEventCategory() throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllEventCategory ");

		Map<String, Object> resultFromProcedure = null;
		ArrayList<Category> arEventCatList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.withProcedureName("usp_WebRetailerEventsCategoryDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			simpleJdbcCall.returningResultSet("eventCategoryList", new BeanPropertyRowMapper<Category>(Category.class));

			final MapSqlParameterSource objEventCatParam = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objEventCatParam);

			arEventCatList = (ArrayList) resultFromProcedure.get("eventCategoryList");

			Collections.sort(arEventCatList, new Comparator<Category>() {
				public int compare(Category o1, Category o2) {
					return o1.getCategoryName().compareToIgnoreCase(o2.getCategoryName());
				}
			});

		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getAllEventCategory : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		return arEventCatList;
	}

	/**
	 * This DAO method will return list of Retailer Locations.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @return list of Retailer Locations.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public ArrayList<RetailerLocation> getEventRetailerLocationList(int retailerId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getEventRetailerLocationList ");

		Map<String, Object> resultFromProcedure = null;
		ArrayList<RetailerLocation> arEventLocationList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.returningResultSet("eventLocationList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));
			simpleJdbcCall.withProcedureName("usp_WebRetailerEventRetailLocationsDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			final MapSqlParameterSource objRetailIDParameters = new MapSqlParameterSource();
			objRetailIDParameters.addValue("RetailID", retailerId);
			resultFromProcedure = simpleJdbcCall.execute(objRetailIDParameters);

			if (resultFromProcedure != null) {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum) {
					arEventLocationList = (ArrayList<RetailerLocation>) resultFromProcedure.get("eventLocationList");
				} else {
					LOG.info("Inside RetailerDAOImpl : getEventRetailerLocationList : errorNum : " + errorNum + "errorMsg : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		} catch (DataAccessException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arEventLocationList;
	}

	/**
	 * This DAOImpl method create/Update retailer Event detail.
	 * 
	 * @param event
	 *            instance of Event
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final String addUpdateEventDetail(Event objEvent) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : addUpdateEventDetail ");

		Map<String, Object> resultFromProcedure = null;
		String response = null;

		Integer status;

		String days = Arrays.toString(objEvent.getDays());
		if ("".equals(Utility.checkNull(days))) {
			days = null;
		}

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerEventsCreationAndUpdation");

			final MapSqlParameterSource addUpdateEventParam = new MapSqlParameterSource();

			/* Add Event, If Event Id is null. */
			if (null == objEvent.getEventID()) {
				addUpdateEventParam.addValue("EventID", null);
			} else {
				/* Update Event, If Event Id is not null. */
				addUpdateEventParam.addValue("EventID", objEvent.getEventID());
			}

			addUpdateEventParam.addValue("RetailID", objEvent.getRetailID());
			addUpdateEventParam.addValue("EventName", objEvent.getEventName());
			addUpdateEventParam.addValue("ShortDescription", objEvent.getShortDescription());

			if (!"".equals(Utility.checkNull(objEvent.getLongDescription()))) {
				addUpdateEventParam.addValue("LongDescription", objEvent.getLongDescription());
			} else {
				addUpdateEventParam.addValue("LongDescription", null);
			}

			addUpdateEventParam.addValue("ImagePath", objEvent.getEventImageName());
			addUpdateEventParam.addValue("EventCategoryID", objEvent.getEventCategory());

			if (!"".equals(Utility.checkNull(objEvent.getMoreInfoURL()))) {
				addUpdateEventParam.addValue("MoreInformationURL", objEvent.getMoreInfoURL());
			} else {
				addUpdateEventParam.addValue("MoreInformationURL", null);
			}

			if ("yes".equalsIgnoreCase(objEvent.getIsOngoing())) {
				addUpdateEventParam.addValue("StartDate", objEvent.getEventStartDate());
				addUpdateEventParam.addValue("StartTime", objEvent.getEventStartTime());
				addUpdateEventParam.addValue("OngoingEvent", true);
				addUpdateEventParam.addValue("EndTime", objEvent.getEventEndTime());
				addUpdateEventParam.addValue("EndDate", objEvent.getEventEndDate());
				addUpdateEventParam.addValue("RecurrencePatternID", objEvent.getRecurrencePatternID());
				addUpdateEventParam.addValue("RecurrenceInterval", objEvent.getRecurrenceInterval());
				addUpdateEventParam.addValue("EveryWeekday", objEvent.getIsWeekDay());
				addUpdateEventParam.addValue("Days", days);
				addUpdateEventParam.addValue("DayNumber", objEvent.getDayNumber());
				addUpdateEventParam.addValue("EndAfter", objEvent.getEndAfter());
			} else {
				addUpdateEventParam.addValue("StartDate", objEvent.getEventDate());
				addUpdateEventParam.addValue("StartTime", objEvent.getEventTime());
				addUpdateEventParam.addValue("OngoingEvent", false);
				addUpdateEventParam.addValue("EndTime", objEvent.getEventETime());
				addUpdateEventParam.addValue("EndDate", objEvent.getEventEDate());
				addUpdateEventParam.addValue("RecurrencePatternID", null);
				addUpdateEventParam.addValue("RecurrenceInterval", null);
				addUpdateEventParam.addValue("EveryWeekday", null);
				addUpdateEventParam.addValue("Days", null);
				addUpdateEventParam.addValue("EndAfter", null);
				addUpdateEventParam.addValue("DayNumber", null);
			}

			if (!"".equals(Utility.checkNull(objEvent.getBsnsLoc()))) {
				if ("yes".equals(objEvent.getBsnsLoc())) {
					addUpdateEventParam.addValue("BussinessEvent", true);
					addUpdateEventParam.addValue("RetailLocationID", objEvent.getRetailLocationIDs());
					addUpdateEventParam.addValue("Address", null);
					addUpdateEventParam.addValue("City", null);
					addUpdateEventParam.addValue("State", null);
					addUpdateEventParam.addValue("PostalCode", null);
					addUpdateEventParam.addValue("Latitude", null);
					addUpdateEventParam.addValue("Longitude", null);
					addUpdateEventParam.addValue("GeoErrorFlag", false);
				} else
					if ("no".equals(objEvent.getBsnsLoc())) {
						addUpdateEventParam.addValue("BussinessEvent", false);
						addUpdateEventParam.addValue("RetailLocationID", null);
						addUpdateEventParam.addValue("Address", objEvent.getAddress());
						addUpdateEventParam.addValue("City", objEvent.getCity());
						addUpdateEventParam.addValue("State", objEvent.getState());
						addUpdateEventParam.addValue("PostalCode", objEvent.getPostalCode());
						addUpdateEventParam.addValue("Latitude", objEvent.getLatitude());
						addUpdateEventParam.addValue("Longitude", objEvent.getLongitude());
						addUpdateEventParam.addValue("GeoErrorFlag", objEvent.isGeoError());
					}
			}

			resultFromProcedure = simpleJdbcCall.execute(addUpdateEventParam);

			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : addUpdateEventDetail : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	/**
	 * This serviceImpl method will create Event page.
	 * 
	 * @param event
	 *            instance of Event
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public Event getEventDetail(Integer eventId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getEventDetail ");

		Integer iStatus = null;
		List<Event> arEventList = null;
		Event eventDetail = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerEventDetails");

			simpleJdbcCall.returningResultSet("eventDetail", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource editEventParam = new MapSqlParameterSource();
			editEventParam.addValue("EventID", eventId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(editEventParam);

			if (null != resultFromProcedure) {
				iStatus = (Integer) resultFromProcedure.get("Status");

				if (null != iStatus && iStatus == 0) {
					arEventList = (ArrayList<Event>) resultFromProcedure.get("eventDetail");

					if (null != arEventList && !arEventList.isEmpty()) {
						eventDetail = arEventList.get(0);
					}
				}
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getEventDetail : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return eventDetail;
	}

	/**
	 * This DAOImpl method will return list of Retailer Events based on input
	 * parameter.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @param storeIdentification
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public final SearchResultInfo getRetailerEventList(int retailerId, int lowerLimit, String eventName, Integer isFundraising) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getRetailerEventList ");

		Integer iRowcount = null;
		List<Event> arEventList = null;
		SearchResultInfo objSearchResult = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);

			simpleJdbcCall.withProcedureName("usp_WebRetailerEventsDisplay");

			simpleJdbcCall.returningResultSet("eventList", new BeanPropertyRowMapper<Event>(Event.class));

			final MapSqlParameterSource eventDisplayParam = new MapSqlParameterSource();
			eventDisplayParam.addValue("RetailID", retailerId);
			eventDisplayParam.addValue("SearchKey", eventName);
			eventDisplayParam.addValue("LowerLimit", lowerLimit);
			eventDisplayParam.addValue("Fundraising", isFundraising);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(eventDisplayParam);

			if (null != resultFromProcedure) {
				objSearchResult = new SearchResultInfo();
				iRowcount = (Integer) resultFromProcedure.get("MaxCnt");
				arEventList = (ArrayList<Event>) resultFromProcedure.get("eventList");
				if (null != arEventList && !arEventList.isEmpty()) {
					objSearchResult.setEventList(arEventList);
					objSearchResult.setTotalSize(iRowcount);
				}
			}

		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getRetailerEventList : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return objSearchResult;
	}

	/**
	 * This DAOImpl method will return list of event patterns.
	 * 
	 * @return List of event patterns.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<Event> getEventPatterns() throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getEventPatterns ");

		List<Event> arEventList = null;
		Integer iStatus = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("patterns", new BeanPropertyRowMapper<Event>(Event.class));
			simpleJdbcCall.withProcedureName("usp_WebEventPatternDisplay");
			final MapSqlParameterSource param = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(param);
			if (null != resultFromProcedure) {
				iStatus = (Integer) resultFromProcedure.get("Status");

				if (null != iStatus && iStatus == 0) {
					arEventList = (ArrayList<Event>) resultFromProcedure.get("patterns");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Inside RetailerDAOImpl : getEventPatterns : errorNum : " + errorNum +" errorMsg : "+errorMsg);
				}
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getEventPatterns : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arEventList;
	}

	/**
	 * This DAOImpl method deletes the Event page from list.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteEvent(Integer eventId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : deleteEvent ");

		Map<String, Object> resultFromProcedure = null;
		Integer iStatus = null;
		String strResponse = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerEventDeletion");
			final MapSqlParameterSource deleteEventParams = new MapSqlParameterSource();

			deleteEventParams.addValue("EventID", eventId);

			resultFromProcedure = simpleJdbcCall.execute(deleteEventParams);

			iStatus = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (iStatus == 0) {
				strResponse = ApplicationConstants.SUCCESS;
			} else {
				strResponse = ApplicationConstants.FAILURE;
			}

		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : deleteEvent : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return strResponse;
	}

	/**
	 * This DAOImpl method will display all associated Retailer Location with
	 * that Event.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final List<RetailerLocation> getAllLocationEvent(Integer eventId, int iRetailId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getAllLocationEvent ");

		List<RetailerLocation> arEventLocationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebEventsAssociationRetailLocationsList");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			simpleJdbcCall.returningResultSet("eventLocationList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));

			final MapSqlParameterSource eventLocationParam = new MapSqlParameterSource();
			eventLocationParam.addValue("EventID", eventId);
			eventLocationParam.addValue("RetailID", iRetailId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(eventLocationParam);
			arEventLocationList = (List<RetailerLocation>) resultFromProcedure.get("eventLocationList");

		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getLocationIDForEventPage : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arEventLocationList;
	}

	public SearchResultInfo getFundraiserEventList(int retailerId, int lowerLimit, String eventName, Long userId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getFundraiserEventList() ");
		Integer iRowcount = null;
		List<Event> arEventList = null;
		SearchResultInfo objSearchResult = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerFundraisingEventsDisplay");
			simpleJdbcCall.returningResultSet("eventList", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource eventDisplayParam = new MapSqlParameterSource();
			eventDisplayParam.addValue("RetailID", retailerId);
			eventDisplayParam.addValue("SearchKey", eventName);
			eventDisplayParam.addValue("LowerLimit", lowerLimit);
			eventDisplayParam.addValue("UserID", userId);
//			eventDisplayParam.addValue("ScreenName", "clr screen");

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(eventDisplayParam);

			if (null != resultFromProcedure) {
				objSearchResult = new SearchResultInfo();
				iRowcount = (Integer) resultFromProcedure.get("MaxCnt");
				arEventList = (ArrayList<Event>) resultFromProcedure.get("eventList");
				if (null != arEventList && !arEventList.isEmpty()) {
					objSearchResult.setEventList(arEventList);
					objSearchResult.setTotalSize(iRowcount);
				}
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getRetailerEventList : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return objSearchResult;
	}

	public List<RetailerLocation> getFundraiserEventLocation(Integer eventId, int iRetailId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getFundraiserEventLocation() ");

		List<RetailerLocation> arEventLocationList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebFundraisingAssociatedRetailLocationsList");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);

			simpleJdbcCall.returningResultSet("eventLocationList", new BeanPropertyRowMapper<RetailerLocation>(RetailerLocation.class));

			final MapSqlParameterSource eventLocationParam = new MapSqlParameterSource();
			eventLocationParam.addValue("FundraisingID", eventId);
			eventLocationParam.addValue("RetailID", iRetailId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(eventLocationParam);
			arEventLocationList = (List<RetailerLocation>) resultFromProcedure.get("eventLocationList");

		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getLocationIDForEventPage : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return arEventLocationList;
	}

	public List<Category> getFundraiserDepartmentList(Integer retailId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getDepartmentList() ");
		List<Category> deptList = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebFundraisingDepartmentDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("deptList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource eventLocationParam = new MapSqlParameterSource();
			eventLocationParam.addValue("RetailID", retailId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(eventLocationParam);
			Integer status = (Integer) resultFromProcedure.get("Status");
			if(status == 0)	{
				deptList = (List<Category>) resultFromProcedure.get("deptList");
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getLocationIDForEventPage : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return deptList;
	}
	
	public final ArrayList<Category> getFundraiserEventCategory(Integer retailId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getFundraiserEventCategory() ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<Category> fundraiserCatList = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebFundraisingCategoryDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("fundraiserCategoryList", new BeanPropertyRowMapper<Category>(Category.class));

			final MapSqlParameterSource objEventCatParam = new MapSqlParameterSource();
			objEventCatParam.addValue("RetailID", retailId);
			resultFromProcedure = simpleJdbcCall.execute(objEventCatParam);
			Integer status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				fundraiserCatList = (ArrayList) resultFromProcedure.get("fundraiserCategoryList");
				if (null != fundraiserCatList && !fundraiserCatList.isEmpty()) {
					Collections.sort(fundraiserCatList, new Comparator<Category>() {
						public int compare(Category o1, Category o2) {
							return o1.getCategoryName().compareToIgnoreCase(o2.getCategoryName());
						}
					});
				}
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getFundraiserEventCategory : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		return fundraiserCatList;
	}

	public String saveFundraiserDept(Long userId, Integer retailId, String deptName) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : saveFundraiserDept() ");
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer deptId = null;
		Boolean duplicateFlag = null;
		
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_WebFundraisingDepartmentCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.returningResultSet("addDept", new BeanPropertyRowMapper<Category>(Category.class));

			final MapSqlParameterSource objEventCatParam = new MapSqlParameterSource();
			objEventCatParam.addValue("UserID", userId);
			objEventCatParam.addValue("RetailID", retailId);
			objEventCatParam.addValue("HcDepartmentName", deptName);
			resultFromProcedure = simpleJdbcCall.execute(objEventCatParam);
			Integer status = (Integer) resultFromProcedure.get("Status");
			if (status == 0) {
				duplicateFlag = (Boolean) resultFromProcedure.get("DuplicateFlag");
				if(duplicateFlag)	{
					response = ApplicationConstants.DUPLICATE_DEPARTMENT;
				} else	{
					deptId = (Integer) resultFromProcedure.get("HcFundraisingDepartmentID");
					response = deptId.toString();
				}
			} else	{
				Integer errNum = (Integer) resultFromProcedure.get("ErrorNumber");
				String errMsg = (String) resultFromProcedure.get("ErrorMessage");
				LOG.error("Inside RetailerDAOImpl : getFundraiserEventCategory : usp_WebFundraisingDepartmentCreation " + errNum + ", " +errMsg);
				throw new ScanSeeWebSqlException(errMsg);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : getFundraiserEventCategory : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		return response;
	}

	public String saveUpdateFundraiser(Long userId, Event objEvent) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : saveUpdateFundraiser() ");
		String response = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerFundraisingEventsCreationAndUpdation");
			simpleJdbcCall.returningResultSet("eventList", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource eventDisplayParam = new MapSqlParameterSource();
			eventDisplayParam.addValue("UserID", userId);
			eventDisplayParam.addValue("FundraisingID",  objEvent.getEventID());
			eventDisplayParam.addValue("FundraisingName", objEvent.getEventName());
			eventDisplayParam.addValue("FundraisingOrganizationImagePath", objEvent.getEventImageName());
			if("yes".equalsIgnoreCase(objEvent.getIsRetLoc()))	{
				eventDisplayParam.addValue("FundraisingRetailLocationFlag", 1);
				eventDisplayParam.addValue("RetailLocationID", objEvent.getRetLocId());
				eventDisplayParam.addValue("FundraisingOrganizationName", null);
			} else	{
				eventDisplayParam.addValue("FundraisingRetailLocationFlag", 0);
				eventDisplayParam.addValue("RetailLocationID", null);
				eventDisplayParam.addValue("FundraisingOrganizationName", objEvent.getOrganization());
			}
			eventDisplayParam.addValue("ShortDescription", objEvent.getShortDescription());
			eventDisplayParam.addValue("LongDescription", objEvent.getLongDescription());
			eventDisplayParam.addValue("HcFundraisingCategoryID", objEvent.getEventCategory());
			if(null != objEvent.getDepartment() && !"".equals(objEvent.getDepartment()))	{
				eventDisplayParam.addValue("HcFundraisingDepartmentID", objEvent.getDepartment());
			} else {
				eventDisplayParam.addValue("HcFundraisingDepartmentID", null);
			}
			eventDisplayParam.addValue("RetailID", objEvent.getRetailID());
			eventDisplayParam.addValue("StartDate", objEvent.getEventStartDate());
			if(null != objEvent.getEventEndDate() && !"".equals(objEvent.getEventEndDate()))	{
				eventDisplayParam.addValue("EndDate", objEvent.getEventEndDate());
			} else	{
				eventDisplayParam.addValue("EndDate", null);
			}
			if("yes".equalsIgnoreCase(objEvent.getIsEventTied()))	{
				eventDisplayParam.addValue("FundraisingEventFlag", 1);
				eventDisplayParam.addValue("EventID", objEvent.getEventAssociatedIds());
			} else	{
				eventDisplayParam.addValue("FundraisingEventFlag", 0);
				eventDisplayParam.addValue("EventID", null);
			}
			if(null != objEvent.getMoreInfo() && !"".equals(objEvent.getMoreInfo()))	{
				eventDisplayParam.addValue("MoreInformationURL", objEvent.getMoreInfo());
			} else	{
				eventDisplayParam.addValue("MoreInformationURL", null);
			}
			if(null != objEvent.getPurchaseProduct() && !"".equals(objEvent.getPurchaseProduct()))	{
				eventDisplayParam.addValue("PurchaseProductURL", objEvent.getPurchaseProduct());
			} else	{
				eventDisplayParam.addValue("PurchaseProductURL", null);
			}
			if(null != objEvent.getFundraisingGoal() && !"".equals(objEvent.getFundraisingGoal()))	{
				eventDisplayParam.addValue("FundraisingGoal", objEvent.getFundraisingGoal());
			} else	{
				eventDisplayParam.addValue("FundraisingGoal", null);
			}
			if(null != objEvent.getCurrentLevel() && !"".equals(objEvent.getCurrentLevel()))	{
				eventDisplayParam.addValue("CurrentLevel", objEvent.getCurrentLevel());
			} else	{
				eventDisplayParam.addValue("CurrentLevel", null);
			}
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(eventDisplayParam);
			Integer iStatus = (Integer) resultFromProcedure.get("Status");
			if (iStatus == 0) {
				response = ApplicationConstants.SUCCESS;
			} else	{
				Integer errNum = (Integer) resultFromProcedure.get("ErrorNumber");
				String errMsg = (String) resultFromProcedure.get("ErrorMessage");
				LOG.error("Inside RetailerDAOImpl : deleteFundraiser : usp_WebRetailerFundraisingEventsCreationAndUpdation " + errNum + ", " +errMsg);
				throw new ScanSeeWebSqlException(errMsg);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : saveUpdateFundraiser : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info("Exit RetailerDAOImpl : saveUpdateFundraiser() ");
		return response;
	}

	public String deleteFundraiser(Integer eventId) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : deleteFundraiser ");

		Map<String, Object> resultFromProcedure = null;
		Integer iStatus = null;
		String strResponse = null;
		
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerFundraisingEventDeletion");
			final MapSqlParameterSource deleteEventParams = new MapSqlParameterSource();
			deleteEventParams.addValue("FundraisingID", eventId);
			resultFromProcedure = simpleJdbcCall.execute(deleteEventParams);
			iStatus = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (iStatus == 0) {
				strResponse = ApplicationConstants.SUCCESS;
			} else {
				Integer errNum = (Integer) resultFromProcedure.get("ErrorNumber");
				String errMsg = (String) resultFromProcedure.get("ErrorMessage");
				LOG.error("Inside RetailerDAOImpl : deleteFundraiser : usp_WebRetailerFundraisingEventDeletion " + errNum + ", " +errMsg);
				throw new ScanSeeWebSqlException(errMsg);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : deleteFundraiser : usp_WebRetailerFundraisingEventDeletion" + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return strResponse;
	}

	public Event getFundEventDetails(Integer eventId, Integer retailID) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : getFundEventDetails ");

		Map<String, Object> resultFromProcedure = null;
		Integer iStatus = null;
		ArrayList<Event> eventList = null;
		Event objEvent = null;
		
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerFundraisingEventDetails");
			simpleJdbcCall.returningResultSet("eventList", new BeanPropertyRowMapper<Event>(Event.class));
			final MapSqlParameterSource eventDisplayParam = new MapSqlParameterSource();
			eventDisplayParam.addValue("HcFundraisingID", eventId);
			eventDisplayParam.addValue("RetailID", retailID);
			resultFromProcedure = simpleJdbcCall.execute(eventDisplayParam);
			iStatus = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (iStatus == 0) {
				eventList = (ArrayList<Event>) resultFromProcedure.get("eventList");
				if(null != eventList && !eventList.isEmpty())	{
					objEvent = new Event();
					objEvent = eventList.get(0);
					objEvent.setEventStartDate(Utility.formattedDate(objEvent.getEventDate()));
					if(null != objEvent.getEventEDate() && !"".equals(objEvent.getEventEDate()))	{
						objEvent.setEventEndDate(Utility.formattedDate(objEvent.getEventEDate()));
					}
				}
			} else {
				Integer errNum = (Integer) resultFromProcedure.get("ErrorNumber");
				String errMsg = (String) resultFromProcedure.get("ErrorMessage");
				LOG.error("Inside RetailerDAOImpl : deleteFundraiser : usp_WebRetailerFundraisingEventDetails " + errNum + ", " +errMsg);
				throw new ScanSeeWebSqlException(errMsg);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : deleteFundraiser : usp_WebRetailerFundraisingEventDetails" + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		} catch (ParseException e) {
			LOG.error("Inside RetailerDAOImpl : deleteFundraiser : Error parsing date" + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return objEvent;
	}
	
	public List<Category> fetchFilters(String filterIds) throws ScanSeeWebSqlException {
		LOG.info("Inside RetailerDAOImpl : fetchFilters ");

		Map<String, Object> resultFromProcedure = null;
		Integer iStatus = null;
		ArrayList<Category> filtersList = null;
		
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerFiltersDisplay");
			simpleJdbcCall.returningResultSet("filterList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource filterDisplayParam = new MapSqlParameterSource();
			filterDisplayParam.addValue("CategoryID", filterIds);
			resultFromProcedure = simpleJdbcCall.execute(filterDisplayParam);
			iStatus = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (iStatus == 0) {
				filtersList = (ArrayList<Category>) resultFromProcedure.get("filterList");
			} else {
				Integer errNum = (Integer) resultFromProcedure.get("ErrorNumber");
				String errMsg = (String) resultFromProcedure.get("ErrorMessage");
				LOG.error("Inside RetailerDAOImpl : fetchFilters : usp_WebRetailerFiltersDisplay " + errNum + ", " +errMsg);
				throw new ScanSeeWebSqlException(errMsg);
			}
		} catch (DataAccessException e) {
			LOG.error("Inside RetailerDAOImpl : fetchFilters : usp_WebRetailerFiltersDisplay" + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		
		return filtersList;
	}
	
}

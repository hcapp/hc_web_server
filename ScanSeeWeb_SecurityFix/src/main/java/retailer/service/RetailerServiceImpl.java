package retailer.service;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.util.UriUtils;

import retailer.dao.RetailerDAO;
import common.constatns.ApplicationConstants;
import common.email.EmailComponent;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AccountType;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.ContactType;
import common.pojo.Coupon;
import common.pojo.DropDown;
import common.pojo.Event;
import common.pojo.GAddress;
import common.pojo.HotDealInfo;
import common.pojo.LocationResult;
import common.pojo.PlanInfo;
import common.pojo.Product;
import common.pojo.QRCodeResponse;
import common.pojo.Rebates;
import common.pojo.RetailProduct;
import common.pojo.Retailer;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerImages;
import common.pojo.RetailerInfo;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.RetailerLocationProduct;
import common.pojo.RetailerLocationProductJson;
import common.pojo.RetailerProfile;
import common.pojo.RetailerRegistration;
import common.pojo.RetailerUploadLogoInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.State;
import common.pojo.StoreInfoVO;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.EncryptDecryptPwd;
import common.util.QRCodeGenerator;
import common.util.ScanSeeProperties;
import common.util.UtilCode;
import common.util.Utility;

/**
 * RetailerServiceImpl implements RetailerService methods.
 * 
 * @author Created by SPAN.
 */
public class RetailerServiceImpl implements RetailerService {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(RetailerServiceImpl.class);

	/**
	 * Variable retailerDAO declared as instance of RetailerDAO.
	 */
	private RetailerDAO retailerDAO;

	/**
	 * Variable commas declared as String.
	 */
	private final String commas = ",";

	/**
	 * To set setRetailerDAO.
	 * 
	 * @param retailerDAO
	 *            to set.
	 */
	public final void setRetailerDAO(RetailerDAO retailerDAO) {
		this.retailerDAO = retailerDAO;
	}

	/**
	 * This serviceImpl method save Retailer register details by call DAO layer.
	 * 
	 * @param objRetailerRegistration
	 *            instance of RetailerRegistration.
	 * @param strLogoImage
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String processRetailerProfileDetails(
			RetailerRegistration objRetailerRegistration, String strLogoImage)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : processRetailerProfileDetails ");
		String response = null;
		String smtpHost = null;
		String smtpPort = null;
//		final double latLng = 0.0;
		String strResponse = null;
		String strAdminEmailId = null;
//		String result = null;
		try {
			/*
			 * final String strAddress =
			 * Utility.checkNull(objRetailerRegistration.getAddress1().trim()) +
			 * commas + objRetailerRegistration.getState() + commas +
			 * objRetailerRegistration.getCity() + commas +
			 * objRetailerRegistration.getPostalCode();
			 */

			response = retailerDAO
					.addRetailerRegistration(objRetailerRegistration);
			if (response != null
					&& response.equals(ApplicationConstants.SUCCESS)) {
				final ArrayList<AppConfiguration> emailConf = retailerDAO
						.getAppConfig(ApplicationConstants.EMAILCONFIG);
				for (int j = 0; j < emailConf.size(); j++) {
					if (emailConf.get(j).getScreenName()
							.equals(ApplicationConstants.SMTPHOST)) {
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName()
							.equals(ApplicationConstants.SMTPPORT)) {
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}
				final ArrayList<AppConfiguration> adminEmailList = retailerDAO
						.getAppConfig(ApplicationConstants.WEBREGISTRATION);
				for (int j = 0; j < adminEmailList.size(); j++) {
					if (adminEmailList.get(j).getScreenName()
							.equals(ApplicationConstants.ADMINEMAILID)) {
						strAdminEmailId = adminEmailList.get(j)
								.getScreenContent();
					}
				}
				final ArrayList<AppConfiguration> list = retailerDAO
						.getAppConfig(ApplicationConstants.FACEBOOKCONFG);
				for (int j = 0; j < list.size(); j++) {
					if (list.get(j).getScreenName()
							.equals(ApplicationConstants.SCANSEEBASEURL)) {

						objRetailerRegistration.setScanSeeUrl(list.get(j)
								.getScreenContent());

					}
				}
				strResponse = Utility.sendMailRetailerLoginSuccess(
						objRetailerRegistration, smtpHost, smtpPort,
						strAdminEmailId, strLogoImage);
				if (strResponse != null
						&& strResponse.equals(ApplicationConstants.SUCCESS)) {
					response = ApplicationConstants.SUCCESS;
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	/**
	 * This serviceImpl method return all the locations for that retailer based
	 * on input parameter by calling DAO layer.
	 * 
	 * @param retailID
	 *            as request parameter.
	 * @return locationList, All the retailer locations.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final ArrayList<RetailerLocation> getRetailerLocations(Long retailID)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getRetailerLocations ");
		ArrayList<RetailerLocation> retailerLocations = null;
		try {
			retailerLocations = retailerDAO.couponRetaileLocation(retailID);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return retailerLocations;
	}

	public String insertCouponInfo(Coupon objCoupon)
			throws ScanSeeServiceException {
		String response = null;
		final String fileSeparator = System.getProperty("file.separator");
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					Integer.valueOf(String.valueOf(objCoupon.getRetailID())));
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			if (!"".equals(Utility.checkNull(objCoupon.getCouponImgPath()))) {
				if (!ApplicationConstants.BLANKIMAGE.equals(objCoupon
						.getCouponImgPath())) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ objCoupon.getCouponImgPath()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream, retMediaPath
								+ fileSeparator + objCoupon.getCouponImgPath());
					}
				}
			}
			response = retailerDAO.addCoupon(objCoupon);
		} catch (ScanSeeWebSqlException e) {
			response = ApplicationConstants.FAILURE;
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	public Coupon fetchCouponInfo(Long couponID) throws ScanSeeServiceException {
		Coupon couponInfo = null;
		ArrayList<Coupon> couponList = null;
		try {
			couponList = retailerDAO.getCouponIDByCouponDetails(couponID);

			if (!couponList.isEmpty() || couponList != null) {
				couponInfo = couponList.get(0);
			}
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return couponInfo;

	}

	public ArrayList<Product> getProdToAssociateCoupon(String searchKey,
			Long RetailerLocID, Long RetailID) throws ScanSeeServiceException {
		ArrayList<Product> prodList = null;
		try {
			prodList = retailerDAO.getCouponByCouponName(searchKey,
					RetailerLocID, RetailID);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return prodList;
	}

	public SearchResultInfo getCouponSearchResult(Long retailID,
			SearchForm objForm, int lowerLimit, int iRecordCount)
			throws ScanSeeServiceException {
		SearchResultInfo couponlist = null;
		try {
			couponlist = retailerDAO.retailerCouponDisplay(retailID,
					objForm.getSearchKey(), lowerLimit, iRecordCount);

		} catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e.getCause());
		}

		return couponlist;
	}

	public String updateCouponInfo(Coupon objCoupon)
			throws ScanSeeServiceException {
		String response = null;
		final String fileSeparator = System.getProperty("file.separator");
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					Integer.valueOf(String.valueOf(objCoupon.getRetailID())));
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			if (!"".equals(Utility.checkNull(objCoupon.getCouponImgPath()))) {
				if (!ApplicationConstants.BLANKIMAGE.equals(objCoupon
						.getCouponImgPath())) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ objCoupon.getCouponImgPath()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream, retMediaPath
								+ fileSeparator + objCoupon.getCouponImgPath());
					}
				}
			}
			response = retailerDAO.updateCouponInfo(objCoupon);
		} catch (ScanSeeWebSqlException e) {
			response = ApplicationConstants.FAILURE;
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	public String processRetailerUploadLogo(
			RetailerUploadLogoInfo retailerUploadLogoInfo, String realPath,
			Users loginUser) throws ScanSeeServiceException {
		ScanSeeProperties properties = new ScanSeeProperties();
		properties.readProperties();
		String response = ApplicationConstants.SUCCESS;
		String fileSeparator = System.getProperty("file.separator");
		/*
		 * String mediaPath =
		 * ScanSeeProperties.getPropertyValue("RetLogoPath "); String
		 * mediaPath="D:/Web/Retailer/Logo"; String logoDirPath =
		 * "D:/SVN/ScanSeeWeb/uploadImages/" +
		 * productInfoVO.getImageFile().getOriginalFilename(); String response =
		 * null; ScanSeeProperties properties = new ScanSeeProperties();
		 * properties.readProperties(); String mediaPath = ScanSeeProperties
		 * .getPropertyValue("couponImagePath");
		 */
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER, loginUser.getRetailerId());
			String suppMediaPath = mediaPathBuilder.toString();
			// String suppMediaPath = realPath + fileSeparator + "flow_old";
			LOG.info(" Images path ********************" + suppMediaPath);
			Utility.writeFileData(retailerUploadLogoInfo.getRetailerLogo(),
					suppMediaPath
							+ fileSeparator
							+ retailerUploadLogoInfo.getRetailerLogo()
									.getOriginalFilename());
			// Dileep- added
			String outputPath = retailerUploadLogoInfo.getRetailerLogo()
					.getOriginalFilename();
			String extension = FilenameUtils
					.getExtension(retailerUploadLogoInfo.getRetailerLogo()
							.getOriginalFilename());
			if (!Utility.isEmptyOrNullString(extension)) {
				if (!extension.equals("png")) {
					outputPath = FilenameUtils.removeExtension(outputPath);

					if (!Utility.isEmptyOrNullString(outputPath)) {
						outputPath = outputPath + ".png";
					}
				}
			}

			response = retailerDAO.UpdateRetailerUploadLogo(
					Long.valueOf(loginUser.getRetailerId()), outputPath);

		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		} catch (Exception exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return response;

	}

	public String processRetailerUploadLogoForMinDimension(
			RetailerUploadLogoInfo retailerUploadLogoInfo, String realPath,
			Users loginUser, BufferedImage img) throws ScanSeeServiceException {
		ScanSeeProperties properties = new ScanSeeProperties();
		properties.readProperties();
		String response = ApplicationConstants.SUCCESS;
		String fileSeparator = System.getProperty("file.separator");
		/*
		 * String mediaPath =
		 * ScanSeeProperties.getPropertyValue("RetLogoPath "); String
		 * mediaPath="D:/Web/Retailer/Logo"; String logoDirPath =
		 * "D:/SVN/ScanSeeWeb/uploadImages/" +
		 * productInfoVO.getImageFile().getOriginalFilename(); String response =
		 * null; ScanSeeProperties properties = new ScanSeeProperties();
		 * properties.readProperties(); String mediaPath = ScanSeeProperties
		 * .getPropertyValue("couponImagePath");
		 */
		try {
			StringBuilder mediaPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String suppMediaPath = mediaPathBuilder.toString();
			// String suppMediaPath = realPath + fileSeparator + "flow_old";
			LOG.info(" Images path ********************" + suppMediaPath);
			Utility.writeImage(img, suppMediaPath
					+ fileSeparator
					+ retailerUploadLogoInfo.getRetailerLogo()
							.getOriginalFilename());
			// Dileep- added
			String outputPath = retailerUploadLogoInfo.getRetailerLogo()
					.getOriginalFilename();
			String extension = FilenameUtils
					.getExtension(retailerUploadLogoInfo.getRetailerLogo()
							.getOriginalFilename());
			if (!Utility.isEmptyOrNullString(extension)) {
				if (!extension.equals("png")) {
					outputPath = FilenameUtils.removeExtension(outputPath);

					if (!Utility.isEmptyOrNullString(outputPath)) {
						outputPath = outputPath + ".png";
					}
				}
			}

			// response =
			// retailerDAO.UpdateRetailerUploadLogo(Long.valueOf(loginUser.getRetailerId()),
			// outputPath);
		} catch (ScanSeeServiceException exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		} catch (Exception exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return response;

	}

	public String processRetailerCroppedLogo(
			CommonsMultipartFile retailerUploadLogoInfo, String realPath,
			Users loginUser, int x, int y, int w, int h)
			throws ScanSeeServiceException {
		ScanSeeProperties properties = new ScanSeeProperties();
		properties.readProperties();
		String response = ApplicationConstants.SUCCESS;
		String fileSeparator = System.getProperty("file.separator");
		Random rand = new Random();
		int numNoRange = rand.nextInt(100000);
		String outputFileName;
		String sourceFileName;
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER, loginUser.getRetailerId());
			/*Retailer Location Image path. */
			StringBuilder mediaLocationPath = Utility.getMediaLocationPath(
					ApplicationConstants.RETAILER, loginUser.getRetailerId());
			String MediaLocationPath = mediaLocationPath.toString();
			String suppMediaPath = mediaPathBuilder.toString();
			StringBuilder tempMediaPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			// String suppMediaPath = realPath + fileSeparator + "flow_old";
			outputFileName = retailerUploadLogoInfo.getOriginalFilename();
			sourceFileName = retailerUploadLogoInfo.getOriginalFilename();

			if (!Utility.isEmptyOrNullString(outputFileName)) {
				outputFileName = FilenameUtils
						.removeExtension(retailerUploadLogoInfo
								.getOriginalFilename());
				outputFileName = outputFileName + "_"
						+ String.valueOf(numNoRange) + ".png";

				sourceFileName = FilenameUtils
						.removeExtension(retailerUploadLogoInfo
								.getOriginalFilename());
				// outputFileName = outputFileName + "_" +
				// String.valueOf(numNoRange) + ".png";
				sourceFileName = sourceFileName + ".png";
			}
			LOG.info("Images path ********************" + suppMediaPath
					+ fileSeparator + outputFileName);
			// Update local Utiliy Code and uncomment below line for cropping
			// feature
			Utility.writeCroppedFileData(retailerUploadLogoInfo,
					tempMediaPathBuilder + fileSeparator + sourceFileName,
					suppMediaPath + fileSeparator + outputFileName, x, y, w, h);
			/*Image is write to retailer location image path. */
			Utility.writeCroppedFileData(retailerUploadLogoInfo,
					tempMediaPathBuilder + fileSeparator + sourceFileName,
					MediaLocationPath + fileSeparator + outputFileName, x, y, w, h);

			response = retailerDAO.UpdateRetailerUploadLogo(
					Long.valueOf(loginUser.getRetailerId()), outputFileName);
			if (response != null && response.equalsIgnoreCase("SUCCESS")) {

				response = outputFileName;
			}

		} catch (Exception exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return response;

	}

	/**
	 * This serviceImpl method create new banner page by calling DAO method.
	 * 
	 * @param retLocationAdvertisement
	 *            instance of RetailerLocationAdvertisement
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String insertBannerAdInfo(
			RetailerLocationAdvertisement retLocationAdvertisement)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : insertBannerAdInfo ");
		String response = null;
		final String fileSeparator = System.getProperty("file.separator");
		try {

			final StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER, Integer.valueOf(String
							.valueOf(retLocationAdvertisement.getRetailID())));
			final StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			final String retMediaPath = mediaPathBuilder.toString();
			final String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);
			if (null != retLocationAdvertisement.getStrBannerAdImagePath()
					&& !"".equals(retLocationAdvertisement
							.getStrBannerAdImagePath())) {
				final InputStream inputStream = new BufferedInputStream(
						new FileInputStream(retTempMediaPath
								+ fileSeparator
								+ retLocationAdvertisement
										.getStrBannerAdImagePath()));
				if (null != inputStream) {
					Utility.writeFileData(
							inputStream,
							retMediaPath
									+ fileSeparator
									+ retLocationAdvertisement
											.getStrBannerAdImagePath());
				}
			}
			response = retailerDAO
					.retailerAddsInsertion(retLocationAdvertisement);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	public SearchResultInfo getRetailerAdsByAdsNames(Long objRetailID,
			SearchForm objForm, int lowerLimit, int recordCount)
			throws ScanSeeServiceException {
		final String methoName = "getRetailerAdsByAdsNames";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		SearchResultInfo bannerAdlist = null;
		try {
			bannerAdlist = retailerDAO.getRetailerAdsByAdsNames(objRetailID,
					objForm.getSearchKey(), lowerLimit, recordCount);

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED, exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return bannerAdlist;
	}

	/**
	 * This serviceImpl method upload multiple locations with the help of CSV
	 * file templates and call DAO layer.
	 * 
	 * @param retailerLocation
	 *            instance of RetailerLocation.
	 * @param userId
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	public final String saveBatchLocation(RetailerLocation retailerLocation,
			Long userId, int retailID) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : saveBatchLocation ");
		String status = null;
		ArrayList<RetailerLocation> retailerLocationList = null;
		final CommonsMultipartFile batchFile = retailerLocation
				.getLocationSetUpFile();
		try {
			retailerLocationList = Utility.getRetailLocationList(batchFile);
			status = retailerDAO.retailerLocationUpload(retailerLocationList,
					userId, retailID);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return status;
	}

	/**
	 * This serviceImpl method display Retailer register details by calling DAO
	 * method.
	 * 
	 * @param user
	 *            instance of Users.
	 * @return RetailerProfile instance.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final RetailerProfile getRetailerProfile(Users user)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getRetailerProfile ");
		RetailerProfile profileInfo = new RetailerProfile();
		try {
			final List<RetailerProfile> retailerList = retailerDAO
					.getRetailUpdateProfileByRetailID(user.getRetailerId());
			profileInfo = retailerList.get(0);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return profileInfo;
	}

	/**
	 * This serviceImpl method update Retailer register details by call DAO
	 * layer.
	 * 
	 * @param retailerProfile
	 *            instance of RetailerProfile.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String updateProfile(RetailerProfile retailerProfile)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : updateProfile ");
		String response = null;
		try {
			response = retailerDAO.addRetailerUpdateProfile(retailerProfile);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	/*
	 * public List<RetailerLocation> getRebatesAll(Long userId, String
	 * strRebName) throws ScanSeeWebSqlException { 
	 * stub final String methodName = "getRebatesAll"; List<Rebates>
	 * listOfRebates = null; try { listOfRebates =
	 * retailerDAO.getRetailerByRebateName(userId, strRebName); } catch
	 * (Exception e) { e.printStackTrace(); }
	 * LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
	 * return listOfRebates; }
	 */

	public SearchResultInfo searchProducts(SearchForm objForm, int lowerLimit,
			int rowCount) throws ScanSeeServiceException {

		SearchResultInfo resultInfo = null;
		try {
			if ("".equals(Utility.checkNull(objForm.getSearchKey()))) {
				objForm.setSearchKey(null);
			}

			resultInfo = retailerDAO.retailerProductSetupProductSearch(
					objForm.getSearchKey(), lowerLimit, rowCount);

		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return resultInfo;
	}

	public List<RetailerLocation> getRetailerLocation(Long retailID)
			throws ScanSeeServiceException {

		List<RetailerLocation> retLocList = null;
		try {
			retLocList = retailerDAO
					.getRetailProductSetupRetailLocationSearch(retailID);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return retLocList;
	}

	public String associateUpdateRetProd(
			RetailerLocationProduct objRetailLoctnProduct)
			throws ScanSeeServiceException {
		String response = null;
		try {
			response = retailerDAO.associateRetailerProd(objRetailLoctnProduct);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * This serviceImpl method will return all list of Retailer Location/Store
	 * Identification by add new location, upload multiple locations with the
	 * help of CSV file templates and call DAO layer.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @throws FileNotFoundException 
	 */
	public final SearchResultInfo getRetailerLocationList(int retailerId,
			Long userId, int lowerLimit) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getRetailerLocationList ");
		SearchResultInfo searchResult = null;

		try {
			searchResult = retailerDAO.getLocationList(retailerId, userId, lowerLimit);
			
			if (searchResult != null)
			{
				String strLocationImg  = null;
				
				if (null !=searchResult.getLocationList() && !searchResult.getLocationList().isEmpty()) {
					
				for (int i = 0; i < searchResult.getLocationList().size(); i++)
				{
					strLocationImg = searchResult.getLocationList().get(i).getGridImgLocationPath();
					
					if (!"".equals(Utility.checkNull(strLocationImg))) {
						final int dotIndex = strLocationImg.lastIndexOf('.');
						final int slashIndex = strLocationImg.lastIndexOf("/");
						
						if (dotIndex == -1) {
							searchResult.getLocationList().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
							searchResult.getLocationList().get(i).setGridImgLocationPath(null);
						} else {
							
							/*To check Retailer logo level image or Retailer Location level image.*/
							if (strLocationImg.contains(ApplicationConstants.IMAGES_SLASH_RETAILER)
									&& !strLocationImg.contains(ApplicationConstants.SLASH_LOCATIONLOGO_SLASH)) {
								searchResult.getLocationList().get(i).setImgLocationPath(strLocationImg);
								searchResult.getLocationList().get(i).setGridImgLocationPath(null);
							} else {
								
								if (!"".equals(Utility.checkNull(strLocationImg.substring(slashIndex + 1, strLocationImg.length())))) {
									searchResult.getLocationList().get(i).setImgLocationPath(strLocationImg);
									searchResult.getLocationList().get(i)
											.setGridImgLocationPath(strLocationImg.substring(slashIndex + 1, strLocationImg.length()));
								} else {
									searchResult.getLocationList().get(i).setGridImgLocationPath(null);
									searchResult.getLocationList().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
								}
							}
							/*If End */
						}
					} else {
							searchResult.getLocationList().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
					}
				}
				
			  }
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} 
		return searchResult;
	}

	/**
	 * This serviceImpl method updates location on changes and call DAO layer.
	 * 
	 * @param retailerLocation
	 *            instance of RetailerLocation
	 * @param userId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @param fileName
	 *            as request parameter.
	 * @param objUploadlocation
	 *            instance of SearchResultInfo
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String updateLocationSetUp(RetailerLocation retailerLocation,
			long userId, int retailerId, String fileName,
			SearchResultInfo objUploadlocation, HttpSession session)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : updateLocationSetUp ");
		String response = null;
		boolean bGAddress = false;
		String strAddress = null;
		final double dLatLng = 0.0;
		session.removeAttribute("RetlocationList");
		String strRetailLoc = null;
		try {
			final String prodJson = retailerLocation.getProdJson();
			final List<RetailerLocation> locationList = Utility
					.jsonToObjectList(prodJson);
			if (!objUploadlocation.getLocationList().isEmpty()
					&& !locationList.isEmpty()
					&& objUploadlocation.getLocationList() != null
					&& locationList != null) {
				RetailerLocation objRetailerLocation1 = null;
				RetailerLocation objRetailerLocation2 = null;
				for (int i = 0; i < locationList.size(); i++) {
					objRetailerLocation1 = locationList.get(i);
					for (int j = 0; j < objUploadlocation.getLocationList()
							.size(); j++) {
						objRetailerLocation2 = objUploadlocation
								.getLocationList().get(j);

						if ((objRetailerLocation1.getUploadRetaillocationsID())
								.equals(objRetailerLocation2
										.getUploadRetaillocationsID())) {
							if (!objRetailerLocation1
									.getAddress1()
									.trim()
									.equalsIgnoreCase(
											objRetailerLocation2.getAddress1()
													.trim())
									|| !objRetailerLocation1.getState()
											.equalsIgnoreCase(
													objRetailerLocation2
															.getState())
									|| !objRetailerLocation1.getCity()
											.equalsIgnoreCase(
													objRetailerLocation2
															.getCity())
									|| !objRetailerLocation1.getPostalCode()
											.equals(objRetailerLocation2
													.getPostalCode())
									|| ("".equals(Utility
											.checkNull(objRetailerLocation1
													.getRetailerLocID()))
											&& null == objRetailerLocation1
													.getRetailerLocationLatitude() && null == objRetailerLocation1
											.getRetailerLocationLatitude())) {
								bGAddress = true;
							} else {
								bGAddress = false;
								locationList
										.get(i)
										.setRetailerLocationLongitude(
												objRetailerLocation2
														.getRetailerLocationLongitude());
								locationList
										.get(i)
										.setRetailerLocationLatitude(
												objRetailerLocation2
														.getRetailerLocationLatitude());
							}
							if (bGAddress) {
								strAddress = objRetailerLocation1.getAddress1()
										.trim().toUpperCase()
										+ commas
										+ objRetailerLocation1.getState()
												.toUpperCase()
										+ commas
										+ objRetailerLocation1.getCity()
												.toUpperCase()
										+ commas
										+ objRetailerLocation1.getPostalCode();
								if (!"".equals(Utility.checkNull(strAddress))) {
									final GAddress objGAddress = UtilCode
											.getGeoDetails(strAddress);
									// If user provide valid address or zipcode.
									if (!"".equals(Utility
											.checkNull(objGAddress))) {

										if ("OK".equals(objGAddress.getStatus())
												&& "ROOFTOP".equals(objGAddress
														.getLocationType())) {
											locationList
													.get(i)
													.setRetailerLocationLongitude(
															objGAddress
																	.getLng());
											locationList
													.get(i)
													.setRetailerLocationLatitude(
															objGAddress
																	.getLat());
										} else {
											// If user provide invalid address
											// or zipcode for Geocode
											// Address (Lat/Lng) is null and
											// default value is initialized to
											// 0.0 .
											/*
											 * As per requirement user has to
											 * provide input data . We don't
											 * provide default value 0.0
											 */
											// locationList.get(i).setRetailerLocationLongitude(dLatLng);
											// locationList.get(i).setRetailerLocationLatitude(dLatLng);
											locationList
													.get(i)
													.setRetailerLocID(
															String.valueOf(locationList
																	.get(i)
																	.getUploadRetaillocationsID()));
										}

									} else {
										locationList.get(i)
												.setRetailerLocationLongitude(
														dLatLng);
										locationList.get(i)
												.setRetailerLocationLatitude(
														dLatLng);
										locationList
												.get(i)
												.setRetailerLocID(
														String.valueOf(locationList
																.get(i)
																.getUploadRetaillocationsID()));
									}

								}
							}
						}
					}
				}
			}

			for (int i = 0; i < locationList.size(); i++) {
				if (!"".equals(Utility.checkNull(locationList.get(i)
						.getRetailerLocID()))) {
					if (!"".equals(Utility.checkNull(strRetailLoc))) {
						strRetailLoc = strRetailLoc
								+ locationList.get(i).getRetailerLocID();
					} else {
						strRetailLoc = locationList.get(i).getRetailerLocID();
					}
					strRetailLoc = strRetailLoc + ApplicationConstants.COMMA;
					/*
					 * locationList.remove(i); i--;
					 */
					locationList.get(i).setRetailerLocationLongitude(null);
					locationList.get(i).setRetailerLocationLatitude(null);
				}
			}

			if ("".equals(Utility.checkNull(strRetailLoc))) {
				response = retailerDAO.updateRetailerLocationSetUp(
						locationList, userId, retailerId, fileName);
				
				/* Upload Location, Grid Images are upload to server if Upload image bit is true.*/
				if (null != locationList && !locationList.isEmpty() ) {
					for (int i = 0; i < locationList.size(); i++) {
							if (locationList.get(i).isUploadImage()) {
								if (!"".equals(Utility.checkNull(locationList.get(i).getGridImgLocationPath()))) {
									final String fileSeparator = System.getProperty("file.separator");
									StringBuilder mediaPathBuilder = Utility.getMediaLocationPath(ApplicationConstants.RETAILER,Integer.valueOf(retailerId));
									StringBuilder mediaTempPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
									String retMediaPath = mediaPathBuilder.toString();
									String retTempMediaPath = mediaTempPathBuilder.toString();
									InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator + locationList.get(i).getGridImgLocationPath()));
											if (null != inputStream) {
												Utility.writeFileData(inputStream, retMediaPath + fileSeparator + locationList.get(i).getGridImgLocationPath());
											}
										}
									}
								}
							}
				
			} else {
				SearchResultInfo objLocationList = new SearchResultInfo();
				List<RetailerLocation> arLocationList = locationList;
				objLocationList.setLocationList(arLocationList);
				session.setAttribute("RetlocationList", locationList);
				strRetailLoc = strRetailLoc.toString().substring(0,
						strRetailLoc.toString().length() - 1);
				response = ApplicationConstants.REJECTED;
				response = response + ":" + strRetailLoc;
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	public SearchResultInfo getRetailerByRebateName(Long retailerid,
			SearchForm objForm, int lowerLimit) throws ScanSeeServiceException {
		SearchResultInfo rebatesList = null;
		try {
			rebatesList = retailerDAO.getRetailerByRebateName(retailerid,
					objForm.getSearchKey(), lowerLimit);
		} catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e.getCause());
		}
		return rebatesList;
	}

	public String saveBatchFile(RetailProduct retailProduct, int retailID,
			long userId) throws ScanSeeServiceException {

		LOG.info("Inside RetailerServiceImpl : saveBatchFile ");
		String tableInsertFlg = ApplicationConstants.FAILURE;
		String isRecordsDiscarded = null;
		RetailerInfo produdtList = new RetailerInfo();
		ArrayList<RetailProduct> objRetailerProd = null;
		String emailBody = null;
		try {

			// Get valid and invalid uploaded products list.
			produdtList = Utility.getUploadedProductsList(retailProduct
					.getProductuploadFilePath());

			// Create an entry in the Log table that holds the summary about the
			// batch process
			Integer logId = retailerDAO.getStageTableLogId(retailID, userId,
					retailProduct.getProductuploadFilePath()
							.getOriginalFilename());

			if (produdtList.getValidProdlist().size() > 0) {

				// Move valid products to stage table
				tableInsertFlg = retailerDAO.addBatchUploadProductsToStage(
						produdtList.getValidProdlist(), retailID, userId);

				if (tableInsertFlg != null
						&& tableInsertFlg.equals(ApplicationConstants.SUCCESS)) {

					// Move data from stage table to production table
					tableInsertFlg = batchUploadProductsFromStageToProd(userId,
							retailID, retailProduct.getProductuploadFilePath()
									.getOriginalFilename(), logId);

				}
			}

			if ((tableInsertFlg != null && tableInsertFlg
					.equals(ApplicationConstants.SUCCESS))
					|| produdtList.getValidProdlist().isEmpty()) {

				if (produdtList.getInValidProdlist().size() > 0) {

					// Move invalid products to discarded records table
					retailerDAO.batchUploadDiscardedProducts(
							produdtList.getInValidProdlist(), retailID, userId,
							logId);

				}

				// Get Discarded records that needs to be notify to the user via
				// Email
				objRetailerProd = retailerDAO.getRejectedRetProducts(retailID,
						userId, logId);

				// Write discarded records into CSV file
				if (objRetailerProd != null && objRetailerProd.size() > 0) {
					isRecordsDiscarded = ApplicationConstants.RECORDSDISCARDED;
					Utility.writeRejectedRecordsToCSV(objRetailerProd,
							retailProduct.getDicardedProductsCSVFilePath());
				}

				// clear stage table
				retailerDAO.deleteRetailerBatchUploadStageTable(retailID,
						userId);

				// ScanSee Logo
				String strLogoImage = getDomainName()
						+ ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;

				// get User Email ID
				String mailId = getUserMailId(retailID, userId);
				emailBody = "<Html>Hi ,<br/><br/>"
						+ ApplicationConstants.SUBJECT_MSG_FOR_RETAILER_PRODUCT_MAILSEND
						+ "<br/><br/>Regards,<br/>ScanSee Team<br/>"
						+ "<img src= " + strLogoImage
						+ " alt=\"scansee logo\"  border=\"0\"></Html>";

				// Send discarded records mail to the user with CSV file
				// attached.
				boolean strFlag = sendMailWithAttachment(
						retailProduct.getDicardedProductsCSVFilePath(), mailId,
						emailBody);

				// Delete discarded records CSV file
				if (strFlag) {
					File f = new File(
							retailProduct.getDicardedProductsCSVFilePath());
					f.delete();
				}

				if (isRecordsDiscarded != null
						&& isRecordsDiscarded
								.equals(ApplicationConstants.RECORDSDISCARDED)) {
					tableInsertFlg = isRecordsDiscarded;
				}

			}

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : saveBatchFile "
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getMessage());
		}
		LOG.info("Exit RetailerServiceImpl : saveBatchFile ");
		return tableInsertFlg;
	}

	public String addRetailerRebates(Rebates rebatesVo)
			throws ScanSeeServiceException {
		String isDataInserted = null;
		try {
			isDataInserted = retailerDAO.retailerAddRebate(rebatesVo);
		} catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e.getCause());
		}
		return isDataInserted;

	}

	public ArrayList<Rebates> getRebateProductToAssociate(String strRebateName,
			Long RetailID, int retailerLocId) throws ScanSeeServiceException {
		ArrayList<Rebates> prodList = null;
		try {
			prodList = retailerDAO.getRebateProductToAssociate(strRebateName,
					RetailID, retailerLocId);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return prodList;
	}

	public List<RetailerLocation> getRebRetailerLocations(long retailerID)
			throws ScanSeeServiceException {
		List<RetailerLocation> prodList = null;
		try {
			prodList = retailerDAO.getRetailerLocations(retailerID);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return prodList;

	}

	public LocationResult moveDataFromstgTOProduction(
			RetailerLocation retailerLocation, long userId, int retailerId,
			String fileName) throws ScanSeeServiceException {
		LocationResult locationResult = new LocationResult();
		try {

			locationResult = retailerDAO
					.moveLocationListFrmStageToProductionTable(userId,
							retailerId, fileName);
		} catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e.getCause());
		}
		return locationResult;
	}

	public ArrayList<PlanInfo> getAllPlanList() throws ScanSeeServiceException {

		ArrayList<PlanInfo> planList = null;

		try {
			planList = retailerDAO.fetchAllPlanList();
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}

		return planList;

	}

	public String savePlanInfo(List<PlanInfo> pInfoList, int manufacturerID,
			long userId) throws ScanSeeServiceException {
		String response = null;

		try {
			response = retailerDAO.savePlan(pInfoList, manufacturerID, userId);
		} catch (ScanSeeWebSqlException e) {

			e.printStackTrace();
		}
		return response;
	}

	/**
	 * The service method for displaying all the states and it will call DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return states,List of states.
	 */
	public final ArrayList<State> getAllStates() throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getAllStates ");
		ArrayList<State> states = null;
		try {
			states = retailerDAO.getAllStates();
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return states;
	}

	public String addRetRerunRebates(Rebates objRebates)
			throws ScanSeeServiceException {
		String response = null;

		try {
			response = retailerDAO.addRetRerunRebates(objRebates);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return response;

	}

	/**
	 * This serviceImpl method will add one new HotDeals for retailer by calling
	 * DAO layer.
	 * 
	 * @param objHotDeal
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	public final String retailerAddHotDeal(HotDealInfo objHotDeal)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : retailerAddHotDeal ");
		String isDataInserted = null;
		final String fileSeparator = System.getProperty("file.separator");
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					Integer.valueOf(String.valueOf(objHotDeal.getRetailID())));
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			if (!"".equals(Utility.checkNull(objHotDeal.getDealImgPath()))) {
				if (!ApplicationConstants.BLANKIMAGE.equals(objHotDeal
						.getDealImgPath())) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ objHotDeal.getProductImage()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream, retMediaPath
								+ fileSeparator + objHotDeal.getProductImage());
					}
				}
			}
			isDataInserted = retailerDAO.retailerAddHotDeal(objHotDeal);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return isDataInserted;
	}

	public ArrayList<Retailer> getRetailersForProducts(String productId)
			throws ScanSeeServiceException {
		ArrayList<Retailer> retailers = null;
		try {
			retailers = retailerDAO.getRetailersForProducts(productId);
		} catch (ScanSeeWebSqlException e) {

			e.printStackTrace();
		}
		return retailers;
	}

	/**
	 * This service method return product Hot deals info based on input
	 * parameter by calling DAO method.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param pdtName
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return SearchResultInfo instance.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public SearchResultInfo getDealsForDisplay(Long userId, String pdtName,
			int lowerLimit, int recordCount) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getDealsForDisplay ");
		SearchResultInfo searchResultInfo = null;
		try {
			// userId = (long) 59;
			searchResultInfo = retailerDAO.getDealsForDisplay(userId, pdtName,
					lowerLimit, recordCount);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return searchResultInfo;
	}

	public List<Rebates> getRebatesForDisplay(int rebateId) {

		List<Rebates> rebatesList = null;
		try {
			rebatesList = retailerDAO.getRebatesForDisplay(rebateId);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}

		return rebatesList;

	}

	public String updateRetRebates(Rebates objRebates) {

		String response = null;
		try {
			response = retailerDAO.updateRetRebates(objRebates);
		} catch (ScanSeeWebSqlException e) {

			e.printStackTrace();
		}

		return response;

	}

	/**
	 * This service method return product Hot deals info based on input
	 * parameter by calling DAO layer.
	 * 
	 * @param dealID
	 *            as request parameter.
	 * @return Hot deals details.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final List<HotDealInfo> getHotDealByID(int dealID)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getHotDealByID ");
		List<HotDealInfo> hotDealList = null;
		try {
			hotDealList = retailerDAO.getProductHotDealByID(dealID);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return hotDealList;
	}

	/**
	 * This service method will display the HotDeals deals by passing HotDealId
	 * as input parameter by call service and DAO methods.
	 * 
	 * @param dealID
	 *            as request parameter.
	 * @return HotDealList, All the HotDeals.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<HotDealInfo> getProductHotDealByID(int dealID)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getProductHotDealByID ");
		List<HotDealInfo> isDataInserted = null;
		try {
			isDataInserted = retailerDAO.getProductHotDealByID(dealID);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return isDataInserted;
	}

	/**
	 * This serviceImpl method will update existing HotDeals for retailer
	 * product by calling DAO layer.
	 * 
	 * @param objHotDeal
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	public String updateRetailerProductHotDeal(HotDealInfo objHotDeal)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : updateRetailerProductHotDeal ");
		String isHotDealUpdated = null;
		final String fileSeparator = System.getProperty("file.separator");
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					Integer.valueOf(String.valueOf(objHotDeal.getRetailID())));
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			if (!"".equals(Utility.checkNull(objHotDeal.getDealImgPath()))) {
				if (!ApplicationConstants.BLANKIMAGE.equals(objHotDeal
						.getDealImgPath())) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ objHotDeal.getDealImgPath()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream, retMediaPath
								+ fileSeparator + objHotDeal.getDealImgPath());
					}
				}
			}
			isHotDealUpdated = retailerDAO
					.updateRetailerProductHotDeal(objHotDeal);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return isHotDealUpdated;
	}

	public PlanInfo getDiscountValue(String discountCode)
			throws ScanSeeServiceException {
//		Double discountValue = null;

		PlanInfo planInfo = null;

		try {
			planInfo = retailerDAO.getRetailerDiscount(discountCode);
		} catch (ScanSeeWebSqlException e) {

			e.printStackTrace();
		}

		return planInfo;
	}

	/**
	 * The serviceImpl method for displaying list of TimeZones (all standard
	 * time like (Atlantic,Central, Eastern, Mountain, Pacific Standard Time) by
	 * calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return timeZonelst,List of TimeZones.
	 */
	public final ArrayList<TimeZones> getAllTimeZones()
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  getAllTimeZones ");
		ArrayList<TimeZones> timeZonelst = null;
		try {
			timeZonelst = retailerDAO.getAllTimeZones();
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return timeZonelst;
	}

	/**
	 * The serviceImpl method for displaying list of categories by calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return categories,List of categories.
	 */
	public final ArrayList<Category> getAllBusinessCategory()
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  getAllBusinessCategory ");
		ArrayList<Category> arCategoriesList = null;
		try {
			arCategoriesList = retailerDAO.getAllBusinessCategory();
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arCategoriesList;
	}

	/**
	 * The service method for displaying list of cities by calling DAO.
	 * 
	 * @param lUserID
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return cities,List of cities.
	 */
	public final ArrayList<City> getHdPopulationCenters(Long lUserID)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  getHdPopulationCenters ");
		ArrayList<City> arPopulationCenters = null;
		try {
			arPopulationCenters = retailerDAO.getHdPopulationCenters(lUserID);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arPopulationCenters;
	}

	public String batchUploadProductsFromStageToProd(Long userId, int retailId,
			String fileName, Integer logId) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : batchUploadProductsFromStageToProd ");
		String isDataInserted = null;
		try {
			isDataInserted = retailerDAO.batchUploadProductsFromStageToProd(
					userId, retailId, fileName, logId);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : batchUploadProductsFromStageToProd "
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		}
		return isDataInserted;
	}

	/**
	 * This serviceImpl method deletes the Product Product associated with
	 * Retailer Location from list.
	 * 
	 * @param RetailID
	 *            ,ProductID
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteProductsFromLocHoldControl(Long retailID, Long productID)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : deleteProductsFromLocHoldControl ");
		String isDataInserted = null;
		try {
			isDataInserted = retailerDAO.deleteProductsFromLocHoldControl(
					retailID, productID);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : deleteProductsFromLocHoldControl "
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		}
		return isDataInserted;
	}

	/**
	 * The serviceImpl method for displaying account type (ACH Bank
	 * information/Credit Card) and it will call DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return arAccountTypeList,List of AccountType.
	 */
	public final ArrayList<AccountType> getAllAcountType()
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  getAllAcountType ");
		ArrayList<AccountType> arAccountTypeList = null;
		try {
			arAccountTypeList = retailerDAO.getAllAcountType();
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arAccountTypeList;
	}

	public List<Retailer> fetchRetailerPreviewList(Long retailerID)
			throws ScanSeeServiceException {
		List<Retailer> retailerList = null;
		try {
			retailerList = retailerDAO.fetchUploadPreviewList(retailerID);
			// LOG.info("product List:"+prodList.get(0).getModelNumber());
			// LOG.info("product List:"+prodList.get(0).getProductID());
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  fetchRetailerPreviewList : "
					+ e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}
		LOG.info("Exiting RetailerServiceImpl :  fetchRetailerPreviewList ");

		return retailerList;
	}

	/**
	 * This serviceImpl method delete all the locations in stage table by call
	 * DAO layer.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param retailId
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	public final String deleteRetailerLocationStageTable(int retailId,
			Long userId) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : deleteRetailerLocationStageTable ");
		String response = null;
		try {
			response = retailerDAO.deleteRetailerLocationStageTable(retailId,
					userId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	public ArrayList<Rebates> getRebateProductImage(String productName,
			Long retailID) throws ScanSeeServiceException {

		ArrayList<Rebates> prodList = null;
		try {
			prodList = retailerDAO.getRebateProductImage(productName, retailID);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return prodList;
	}

	public ArrayList<Coupon> fetchCouponImage(Long couponID)
			throws ScanSeeServiceException {

		ArrayList<Coupon> list = null;
		try {
			list = retailerDAO.getCouponImagePath(couponID);
		} catch (ScanSeeWebSqlException e) {

			e.printStackTrace();
		}
		return list;
	}

	public List<Rebates> fetchProductInf(int rebateId)
			throws ScanSeeServiceException {

		List<Rebates> rebatesprodList = null;
		try {
			rebatesprodList = retailerDAO.getProductInfo(rebateId);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}

		return rebatesprodList;
	}

	/**
	 * The serviceImpl method if product name is empty then display product
	 * image path by calling DAO.
	 * 
	 * @param prodName
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @return prodList,List of products.
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 */
	public final ArrayList<HotDealInfo> getProdDetails(String prodName,
			Long retailID) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getProdDetails ");
		ArrayList<HotDealInfo> prodList = null;
		try {
			prodList = retailerDAO.getProdDealDetails(prodName, retailID);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return prodList;
	}

	public ArrayList<Coupon> getProdCoupon(String productName)
			throws ScanSeeServiceException {

		ArrayList<Coupon> prodList = null;
		try {
			prodList = retailerDAO.getProducts(productName);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return prodList;
	}

	public SearchResultInfo getRetailLocProd(String searchParameter,
			int lowerLimit, int retailID, String retailLocationID,
			int recordCount) throws ScanSeeServiceException {
		SearchResultInfo resultInfo = null;
		try {

			resultInfo = retailerDAO.getRetailLocProd(searchParameter,
					lowerLimit, retailID, retailLocationID, recordCount);

		} catch (ScanSeeWebSqlException exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return resultInfo;
	}

	/**
	 * The service method will add one location details by call DAO methods.
	 * 
	 * @param storeInfoVO
	 *            StoreInfoVO instance as request parameter
	 * @param loginUser
	 *            Users instance as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String addLocation(StoreInfoVO storeInfoVO, Users loginUser)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : addLocation ");
		
		String result = null;
		String strAddress = null;
		RetailerCustomPage pageDetails = null;
		final double dLatLng = 0.0;
		
		try {
			if ("".equals(Utility.checkNull(storeInfoVO.getGeoError()))) {
				strAddress = storeInfoVO.getStoreAddress().trim() + commas
						+ storeInfoVO.getCity() + commas
						+ storeInfoVO.getState();
				// + storeInfoVO.getPostalCode();
				/* final GAddress objGAddress = Utility.geocode(strAddress); */
				
				final GAddress objGAddress = UtilCode.getGeoDetails(strAddress);
				
				// If user provide valid address or zipcode.
				if (!"".equals(Utility.checkNull(objGAddress))) {
					if ("OK".equals(objGAddress.getStatus())
							&& "ROOFTOP".equals(objGAddress.getLocationType())) {
						storeInfoVO.setRetailerLocationLongitude(objGAddress
								.getLng());
						storeInfoVO.setRetailerLocationLatitude(objGAddress
								.getLat());
					} else {
						result = "GEOERROR";
						return result;
					}
				} else {
					// If user provide invalid address or zipcode for Geocode
					// Address (Lat/Lng) is null and default value is
					// initialized to
					// 0.0 .
					storeInfoVO.setRetailerLocationLongitude(dLatLng);
					storeInfoVO.setRetailerLocationLatitude(dLatLng);
				}
			}
			
			
			
			result = retailerDAO.addLocation(storeInfoVO);
			
			if (result != null
					&& !result.equals(ApplicationConstants.DUPLICATE_STORE)) {
				
				pageDetails = getLocQrCodeDetails(Long.valueOf(result),
						loginUser.getRetailerId(), loginUser.getUserID());
				result = pageDetails.getQrImagePath();
				
				/* Add Location Images are upload to server.*/
				final String fileSeparator = System.getProperty("file.separator");
				StringBuilder mediaPathBuilder = Utility.getMediaLocationPath(
						ApplicationConstants.RETAILER,
						Integer.valueOf(String.valueOf(storeInfoVO.getRetailID())));
				
				StringBuilder mediaTempPathBuilder = Utility
						.getTempMediaPath(ApplicationConstants.TEMP);
				String retMediaPath = mediaPathBuilder.toString();
				String retTempMediaPath = mediaTempPathBuilder.toString();
				
				if (!"".equals(Utility.checkNull(storeInfoVO.getLocationImgPath()))) {
					if (!ApplicationConstants.BLANKIMAGE.equals(storeInfoVO.getLocationImgPath())) {
						InputStream inputStream = new BufferedInputStream(
								new FileInputStream(retTempMediaPath
										+ fileSeparator
										+ storeInfoVO.getLocationImgPath()));
						if (null != inputStream) {
							Utility.writeFileData(inputStream, retMediaPath
									+ fileSeparator + storeInfoVO.getLocationImgPath());
						}
					}
				}
				
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return result;
	}

	/**
	 * The serviceImpl method for displaying all the business categories and it
	 * will call DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return arCategoryList,List of categories.
	 */
	public final ArrayList<Category> getAllRetailerProfileBusinessCategory()
			throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  getAllRetailerProfileBusinessCategory ");
		ArrayList<Category> arCategoriesList = null;
		try {
			arCategoriesList = retailerDAO
					.getAllRetailerProfileBusinessCategory();
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arCategoriesList;
	}

	/**
	 * This serviceImpl method will return list of Retailer Locations/fetch the
	 * retailer location based on input parameter by calling DAO methods.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @param storeIdentification
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	public final SearchResultInfo getRetailerBatchLocationList(int retailerId,
			Long userId, int lowerLimit, String storeIdentification)
			throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  getRetailerBatchLocationList ");
		SearchResultInfo searchResult = null;
		try {
			searchResult = retailerDAO.getBatchLocationList(retailerId, userId,
					lowerLimit, storeIdentification);
			
			if (searchResult != null){
				String strLocationImg = null;
				if (null != searchResult.getLocationList() && !searchResult.getLocationList().isEmpty()) {
					for (int i = 0; i < searchResult.getLocationList().size(); i++) {
						strLocationImg = searchResult.getLocationList().get(i).getGridImgLocationPath();
						if (!"".equals(Utility.checkNull(strLocationImg))) {
							final int dotIndex = strLocationImg.lastIndexOf('.');
							final int slashIndex = strLocationImg.lastIndexOf("/");
							
							if (dotIndex == -1) {
								searchResult.getLocationList().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
								searchResult.getLocationList().get(i).setGridImgLocationPath(null);
							} else {
								/*To check Retailer logo level image or Retailer Location level image.*/
								if (strLocationImg.contains(ApplicationConstants.IMAGES_SLASH_RETAILER)
										&& !strLocationImg.contains(ApplicationConstants.SLASH_LOCATIONLOGO_SLASH)) {
									searchResult.getLocationList().get(i).setImgLocationPath(strLocationImg);
									searchResult.getLocationList().get(i).setGridImgLocationPath(null);
								} else {
									
									if (!"".equals(Utility.checkNull(strLocationImg.substring(slashIndex + 1, strLocationImg.length())))) {
										searchResult.getLocationList().get(i).setImgLocationPath(strLocationImg);
										searchResult.getLocationList().get(i)
												.setGridImgLocationPath(strLocationImg.substring(slashIndex + 1, strLocationImg.length()));
									} else {
										searchResult.getLocationList().get(i).setGridImgLocationPath(null);
										searchResult.getLocationList().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
									}
								}
								/*If End */
							}
						} else {
							searchResult.getLocationList().get(i).setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
						}
					}
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} 
		return searchResult;
	}

	/**
	 * The serviceImpl method will update retailer multiple locations by calling
	 * DAO methods.
	 * 
	 * @param retailerLocation
	 *            instance of RetailerLocation
	 * @param userId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @param objBeforelocation
	 *            instance of SearchResultInfo
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String saveLocationSetUpList(
			RetailerLocation retailerLocation, long userId, int retailerId,
			SearchResultInfo objBeforelocationList, HttpSession session)
			throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  saveLocationSetUpList  : "
				+ retailerId);
		String response = null;
		boolean bGAddress = false;
		String strAddress = null;
		String strRetailLoc = null;
		final double dLatLng = 0.0;
		session.removeAttribute("RetlocationList");
		try {
			final String prodJson = retailerLocation.getProdJson();
			final List<RetailerLocation> locationList = Utility
					.jsonToObjectList(prodJson);

			if (!objBeforelocationList.getLocationList().isEmpty()
					&& !locationList.isEmpty()
					&& objBeforelocationList.getLocationList() != null
					&& locationList != null) {
				RetailerLocation objRetailerLocation1 = null;
				RetailerLocation objRetailerLocation2 = null;
				for (int i = 0; i < locationList.size(); i++) {
					objRetailerLocation1 = locationList.get(i);
					for (int j = 0; j < objBeforelocationList.getLocationList()
							.size(); j++) {
						objRetailerLocation2 = objBeforelocationList
								.getLocationList().get(j);
						if ((objRetailerLocation1.getRetailLocationID())
								.equals(objRetailerLocation2
										.getRetailLocationID())) {

							if (!objRetailerLocation1
									.getAddress1()
									.trim()
									.equalsIgnoreCase(
											objRetailerLocation2.getAddress1()
													.trim())
									|| !objRetailerLocation1.getState()
											.equalsIgnoreCase(
													objRetailerLocation2
															.getState())
									|| !objRetailerLocation1.getCity()
											.equalsIgnoreCase(
													objRetailerLocation2
															.getCity())
									|| !objRetailerLocation1.getPostalCode()
											.equals(objRetailerLocation2
													.getPostalCode())) {
								bGAddress = true;
							} else {
								bGAddress = false;
								/*
								 * if (null ==
								 * objRetailerLocation2.getRetailerLocationLongitude
								 * ()) {
								 * objRetailerLocation2.setRetailerLocationLongitude
								 * (dLatLng); } if (null ==
								 * objRetailerLocation2.
								 * getRetailerLocationLatitude()) {
								 * objRetailerLocation2
								 * .setRetailerLocationLatitude(dLatLng); }
								 */
								// commented for latitude and longitude fix
								/*
								 * if
								 * ("".equals(Utility.checkNull(retailerLocation
								 * .getRetailerLocID()))) {
								 * locationList.get(i).setRetailerLocationLongitude
								 * (
								 * objRetailerLocation2.getRetailerLocationLongitude
								 * ());
								 * locationList.get(i).setRetailerLocationLatitude
								 * (
								 * objRetailerLocation2.getRetailerLocationLatitude
								 * ()); }
								 */
							}
							// if(locationList.get(i).getRetailerLocationLatitude()==
							// null ||
							// locationList.get(i).getRetailerLocationLongitude()
							// == null && bGAddress == true )
							// {
							if (bGAddress) {
								/*
								 * strAddress =
								 * objRetailerLocation1.getAddress1(
								 * ).trim().toUpperCase() + commas +
								 * objRetailerLocation1.getState().toUpperCase()
								 * + commas +
								 * objRetailerLocation1.getCity().toUpperCase()
								 * + commas +
								 * objRetailerLocation1.getPostalCode();
								 */

								strAddress = objRetailerLocation1.getAddress1()
										.trim()
										+ commas
										+ objRetailerLocation1.getCity()
										+ commas
										+ objRetailerLocation1.getState();

								if (!"".equals(Utility.checkNull(strAddress))) {
									/*
									 * final GAddress objGAddress =
									 * Utility.geocode(strAddress); // If user
									 * provide valid address or zipcode. if
									 * (!"".
									 * equals(Utility.checkNull(objGAddress))) {
									 * locationList
									 * .get(i).setRetailerLocationLongitude
									 * (objGAddress.getLng());
									 * locationList.get(i
									 * ).setRetailerLocationLatitude
									 * (objGAddress.getLat()); } // If user
									 * provide invalid address or // zipcode for
									 * Geocode Address (Lat/Lng) is // null and
									 * default value is initialized to // 0.0 .
									 * if
									 * ("".equals(Utility.checkNull(objGAddress
									 * ))) { locationList.get(i).
									 * setRetailerLocationLongitude(dLatLng);
									 * locationList
									 * .get(i).setRetailerLocationLatitude
									 * (dLatLng); }
									 */

									/*
									 * final GAddress objGAddress =
									 * Utility.geocode(strAddress);
									 */
									final GAddress objGAddress = UtilCode
											.getGeoDetails(strAddress);
									// If user provide valid address or zipcode.
									if (!"".equals(Utility
											.checkNull(objGAddress))) {
										if ("OK".equals(objGAddress.getStatus())
												&& "ROOFTOP".equals(objGAddress
														.getLocationType())) {
											locationList
													.get(i)
													.setRetailerLocationLongitude(
															objGAddress
																	.getLng());
											locationList
													.get(i)
													.setRetailerLocationLatitude(
															objGAddress
																	.getLat());
										} else {
											// If user provide invalid address
											// or zipcode for Geocode
											// Address (Lat/Lng) is null and
											// default value is initialized to
											// 0.0 .
											/*
											 * As per requirement user has to
											 * provide input data . We don't
											 * provide default value 0.0
											 */
											// locationList.get(i).setRetailerLocationLongitude(dLatLng);
											// locationList.get(i).setRetailerLocationLatitude(dLatLng);
											locationList
													.get(i)
													.setRetailerLocID(
															locationList
																	.get(i)
																	.getRetailLocationID());

										}
									} else {
										// If user provide invalid address or
										// zipcode for Geocode
										// Address (Lat/Lng) is null and default
										// value is initialized to
										// 0.0 .
										locationList.get(i)
												.setRetailerLocationLongitude(
														dLatLng);
										locationList.get(i)
												.setRetailerLocationLatitude(
														dLatLng);
										locationList.get(i).setRetailerLocID(
												locationList.get(i)
														.getRetailLocationID());
									}
								}
							}
						}

					}
				}
			}
			for (int i = 0; i < locationList.size(); i++) {
				if (!"".equals(Utility.checkNull(locationList.get(i)
						.getRetailerLocID()))) {
					if (!"".equals(Utility.checkNull(strRetailLoc))) {
						strRetailLoc = strRetailLoc
								+ locationList.get(i).getRetailerLocID();
					} else {
						strRetailLoc = locationList.get(i).getRetailerLocID();
					}
					strRetailLoc = strRetailLoc + ApplicationConstants.COMMA;
					/*
					 * locationList.remove(i); i--;
					 */
					locationList.get(i).setRetailerLocationLongitude(null);
					locationList.get(i).setRetailerLocationLatitude(null);
				}
			}
			if ("".equals(Utility.checkNull(strRetailLoc))) {
				response = retailerDAO.saveLocationList(locationList, userId,
						retailerId);
				
				/* Manage Location, Grid Images are upload to server if Upload image bit is true.*/
				if (null != locationList && !locationList.isEmpty() ) {
					for (int i = 0; i < locationList.size(); i++) {
							if (locationList.get(i).isUploadImage()) {
								if (!"".equals(Utility.checkNull(locationList.get(i).getGridImgLocationPath()))) {
									final String fileSeparator = System.getProperty("file.separator");
									StringBuilder mediaPathBuilder = Utility.getMediaLocationPath(ApplicationConstants.RETAILER,Integer.valueOf(retailerId));
									StringBuilder mediaTempPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
									String retMediaPath = mediaPathBuilder.toString();
									String retTempMediaPath = mediaTempPathBuilder.toString();
									InputStream inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator + locationList.get(i).getGridImgLocationPath()));
											if (null != inputStream) {
												Utility.writeFileData(inputStream, retMediaPath + fileSeparator + locationList.get(i).getGridImgLocationPath());
											}
										}
									}
								}
							}
				
			} else {
				SearchResultInfo objLocationList = new SearchResultInfo();
				List<RetailerLocation> arLocationList = locationList;
				objLocationList.setLocationList(arLocationList);
				session.setAttribute("RetlocationList", locationList);
				strRetailLoc = strRetailLoc.toString().substring(0,
						strRetailLoc.toString().length() - 1);
				response = ApplicationConstants.REJECTED;
				response = response + ":" + strRetailLoc;
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (Exception e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	public String duplicateStoreCheck(int retailId, String storeIdentification)
			throws ScanSeeServiceException {
		String response = null;
		try {

			response = retailerDAO.duplicateStoreIdentityCheck(retailId,
					storeIdentification);
		} catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e.getCause());
		}
		return response;
	}

	/**
	 * This serviceImpl method will delete one retailer location from manage
	 * Location grid by calling DAO methods.
	 * 
	 * @param retailLocationId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String deleteBatchLocation(String retailLocationId)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : deleteBatchLocation ");
		String response = null;
		try {
			response = retailerDAO.deleteBatchLocationList(retailLocationId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	public ArrayList<ContactType> getAllContactTypes()
			throws ScanSeeServiceException {

		ArrayList<ContactType> contactTypes = new ArrayList<ContactType>();
		try {
			contactTypes = retailerDAO.getAllContactTypes();
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}

		return contactTypes;

	}

	/**
	 * This serviceImpl method is used to get the rejected records in the
	 * location upload file
	 * 
	 * @param retailerId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<RetailerLocation> getRejectedRecords(int retailerId,
			Long userId) throws ScanSeeServiceException {

		ArrayList<RetailerLocation> objRetailerLocation = new ArrayList<RetailerLocation>();
		try {
			objRetailerLocation = retailerDAO.getRejectedRecords(retailerId,
					userId);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return objRetailerLocation;
	}

	/**
	 * This serviceImpl method is used sending the mail with an attachment.
	 * 
	 * @param fileName
	 *            attached file path.
	 * @param strToMailId
	 *            mail id of the user whom to send.
	 * @param strLocationContent
	 *            content to send.
	 * @return boolean success or failure that the mail had sent or not
	 */
	public final boolean sendMailWithAttachment(String fileName,
			String strToMailId, String strLocationContent) {
		String smtpHost = null;
		String smtpPort = null;
		boolean strFlag = false;
		ArrayList<AppConfiguration> emailConf = null;
		/*
		 * String toAddress = null; String[] toMailArray = new String[10];
		 */
		String fromAddress = null;
		final String subject = "Retailers Rejected Records shared from ScanSee";
		try {
			emailConf = retailerDAO
					.getAppConfig(ApplicationConstants.EMAILCONFIG);
			for (int j = 0; j < emailConf.size(); j++) {
				if (emailConf.get(j).getScreenName()
						.equals(ApplicationConstants.SMTPHOST)) {
					smtpHost = emailConf.get(j).getScreenContent();
				}
				if (emailConf.get(j).getScreenName()
						.equals(ApplicationConstants.SMTPPORT)) {
					smtpPort = emailConf.get(j).getScreenContent();
				}
			}
			fromAddress = "support@scansee.com";
			/*
			 * final String delimiter = ";"; toAddress = mailId; toMailArray =
			 * toAddress.split(delimiter);
			 */
			if (null != smtpHost || null != smtpPort) {
				strFlag = EmailComponent.sendMailWithAttachmentWithoutCC(
						strToMailId, fromAddress, subject, strLocationContent,
						fileName, smtpHost, smtpPort);
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			e.printStackTrace();
		}
		return strFlag;
	}

	/**
	 * This serviceImpl method return retailer email Id based on input parameter
	 * by call DAO layer.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String getUserMailId(int retailerId, Long userId)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getUserMailId ");
		String result = null;
		try {
			result = retailerDAO.getUserMailId(retailerId, userId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return result;
	}

	public String createRetailerCreatedPage(
			RetailerCustomPage retailerCustomPage)
			throws ScanSeeServiceException {
		LOG.info("Inside the method createRetailerCreatedPage");
		String response = ApplicationConstants.FAILURE;
		int pageID = 0;
		String retCretaedPageUrl = null;
//		String retLocID = null;
//		String mediaPath = "";
//		String externalFlag = "";
		String attachLInk = null;
		String fileSeparator = System.getProperty("file.separator");
		QRCodeResponse qrCodeResponse = null;
		try {

			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					retailerCustomPage.getRetailerId());
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);
			if (retailerCustomPage.getRetImage().getSize() > 0) {
				InputStream inputStream = new BufferedInputStream(
						new FileInputStream(retTempMediaPath
								+ fileSeparator
								+ retailerCustomPage.getRetImage()
										.getOriginalFilename()));
				if (null != inputStream) {
					Utility.writeFileData(inputStream, retMediaPath
							+ fileSeparator
							+ retailerCustomPage.getRetImage()
									.getOriginalFilename());
					retailerCustomPage.setRetailerImg(retailerCustomPage
							.getRetImage().getOriginalFilename());
				}

			}

			retailerCustomPage.setMediaPath(retailerCustomPage
					.getRetCreatedPageattachLink());
			retailerCustomPage.setExternalFlag("1");

			pageID = retailerDAO
					.insertRetailerCretaedPageInfo(retailerCustomPage);

			if (pageID != 0) {
				ArrayList<AppConfiguration> appConfiguration = retailerDAO
						.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);

				attachLInk = retailerCustomPage.getRetCreatedPageattachLink();

				for (int j = 0; j < appConfiguration.size(); j++) {

					retCretaedPageUrl = appConfiguration.get(j)
							.getScreenContent();

				}

				retCretaedPageUrl = attachLInk;
				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(pageID), retCretaedPageUrl);
			}
			if (qrCodeResponse.getResponse().equals(
					ApplicationConstants.SUCCESS)) {
				response = "/" + ApplicationConstants.IMAGES + "/"
						+ ApplicationConstants.QRPATH + "/"
						+ String.valueOf(pageID)
						+ ApplicationConstants.PNGIMAGEFORMAT;
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerCreatedPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerCreatedPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit method createRetailerCreatedPage");
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see retailer.service.RetailerService#getCusomtPageList(int,
	 * java.lang.Long)
	 */
	public SearchResultInfo getCusomtPageList(int retailerId, String searchKey,
			int currentPage, int recordCount) throws ScanSeeServiceException {
		final String methodName = "getCusomtPageList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo customPageList = null;
		try {
			customPageList = retailerDAO.getCustomPageList(retailerId,
					searchKey, currentPage, recordCount);

		} catch (ScanSeeWebSqlException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return customPageList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see retailer.service.RetailerService#getQrCodeDetails(java.lang.Long,
	 * int)
	 */
	public RetailerCustomPage getQrCodeDetails(Long pageId, int retailerId)
			throws ScanSeeServiceException {
		final String methodName = "getQrCodeDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerCustomPage pageDetails = null;
		QRCodeResponse qrCodeResponse = null;
		String qrCodeURL = null;
		try {

			pageDetails = retailerDAO.getQRcodeForPage(pageId, retailerId);
			if (pageDetails != null) {
				qrCodeURL = UriUtils.encodeUri(pageDetails.getQrUrl(), "UTF-8");
				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(pageId), qrCodeURL);
				if (qrCodeResponse.getResponse().equals(
						ApplicationConstants.SUCCESS)) {
					String tempQrPath = "/" + ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.QRPATH + "/"
							+ String.valueOf(pageId)
							+ ApplicationConstants.PNGIMAGEFORMAT;
					pageDetails.setQrImagePath(tempQrPath);
				} else {
					throw new ScanSeeServiceException(
							"::::Error occured while creating QR code::::");
				}
			} else {
				LOG.info("Inside RetailerServiceImpl");
				throw new ScanSeeServiceException(
						"Page details not found for the Page id");
			}

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerPage : " + e);
			throw new ScanSeeServiceException(e);
		} catch (UnsupportedEncodingException e) {

			LOG.info("Inside RetailerServiceImpl :  createRetailerPage : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageDetails;

	}

	public String createRetailerPage(RetailerCustomPage retailerCustomPage)
			throws ScanSeeServiceException {
		LOG.info("Inside the method createRetailerPage");
		String response = ApplicationConstants.FAILURE;
		int pageID = 0;
		String retCretaedPageUrl = null;
		String fileSeparator = System.getProperty("file.separator");
		QRCodeResponse qrCodeResponse = null;
		String retLocID = null;
		String filePath = null;
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					retailerCustomPage.getRetailerId());
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);

			if (retailerCustomPage.getImageUplType().equals("usrUpld")) {
				if (null != retailerCustomPage.getRetailerImg()
						&& !(retailerCustomPage.getRetailerImg().equals(""))) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ retailerCustomPage.getRetailerImg()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream,
								retMediaPath + fileSeparator
										+ retailerCustomPage.getRetailerImg());

					}

				}
				retailerCustomPage.setImageIconID(0);
			} else if (retailerCustomPage.getImageUplType().equals("slctdUpld")) {

				retailerCustomPage.setRetailerImg(null);

			}

			if ("UploadPdf".equals(retailerCustomPage.getLandigPageType())) {

				LOG.info(" PDF path ********************" + retMediaPath);
				if (retailerCustomPage.getRetFile().getSize() > 0) {
					filePath = mediaPathBuilder
							+ fileSeparator
							+ retailerCustomPage.getRetFile()
									.getOriginalFilename();
					Utility.writeFileData(retailerCustomPage.getRetFile(),
							filePath);
					String sourceExtn = FilenameUtils
							.getExtension(retailerCustomPage.getRetFile()
									.getOriginalFilename());
					if (!(sourceExtn.equals("pdf") || sourceExtn.equals("PDF"))) {
						String imageSource = FilenameUtils
								.removeExtension(retailerCustomPage
										.getRetFile().getOriginalFilename());
						imageSource = imageSource + ".png";

						retailerCustomPage.setPdfFileName(imageSource);
					} else {
						retailerCustomPage.setPdfFileName(retailerCustomPage
								.getRetFile().getOriginalFilename());
					}
				} else if (null != retailerCustomPage.getPdfFileName()
						&& !"".equalsIgnoreCase(retailerCustomPage
								.getPdfFileName())) {
					filePath = mediaPathBuilder + fileSeparator
							+ retailerCustomPage.getPdfFileName();

					InputStream inputStream = new FileInputStream(
							mediaTempPathBuilder + fileSeparator
									+ retailerCustomPage.getPdfFileName());

					Utility.writeFileData(inputStream, filePath);
					/*
					 * String sourceExtn =
					 * FilenameUtils.getExtension(retailerCustomPage
					 * .getPdfFileName()); if (!(sourceExtn.equals("pdf") ||
					 * sourceExtn.equals("PDF"))) { String imageSource =
					 * FilenameUtils
					 * .removeExtension(retailerCustomPage.getPdfFileName());
					 * imageSource = imageSource + ".png";
					 * retailerCustomPage.setPdfFileName(imageSource); }
					 */
				}

			}

			pageID = retailerDAO.insertRetailerPageInfo(retailerCustomPage);
			if (pageID != 0) {
				ArrayList<AppConfiguration> appConfiguration = retailerDAO
						.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);
				for (int j = 0; j < appConfiguration.size(); j++) {

					retCretaedPageUrl = appConfiguration.get(j)
							.getScreenContent();

				}

				if (retailerCustomPage.getHiddenLocId().contains(commas)) {
					retLocID = "0";
				} else {
					retLocID = retailerCustomPage.getHiddenLocId();
				}
				if (retailerCustomPage.getLandigPageType().equals("AttachLink")
						|| retailerCustomPage.getLandigPageType().equals(
								"UploadPdf")) {
					retCretaedPageUrl = retCretaedPageUrl
							+ ApplicationConstants.RETAILERCREATEDPAGEURL
							+ "key1=" + retailerCustomPage.getRetailerId()
							+ ApplicationConstants.AMPERSAND + "key2="
							+ retLocID + ApplicationConstants.AMPERSAND
							+ "key3=" + pageID + ApplicationConstants.AMPERSAND
							+ ApplicationConstants.EXTERNALLINK + true;

				} else if (retailerCustomPage.getLandigPageType().equals(
						"MakeOwnPage")) {

					retCretaedPageUrl = retCretaedPageUrl
							+ ApplicationConstants.RETAILERCREATEDPAGEURL
							+ "key1=" + retailerCustomPage.getRetailerId()
							+ ApplicationConstants.AMPERSAND + "key2="
							+ retLocID + ApplicationConstants.AMPERSAND
							+ "key3=" + pageID;
				}
				/*
				 * else if
				 * (retailerCustomPage.getLandigPageType().equals("UploadPdf"))
				 * { ArrayList<AppConfiguration> serverConfg =
				 * retailerDAO.getAppConfig
				 * (ApplicationConstants.RETAILERMEDIAPATH); for (int j = 0; j <
				 * serverConfg.size(); j++) { retCretaedPageUrl =
				 * serverConfg.get(j).getScreenContent(); } retCretaedPageUrl =
				 * retCretaedPageUrl + retailerCustomPage.getRetailerId() + "/"
				 * + retailerCustomPage.getPdfFileName(); }
				 */

				retCretaedPageUrl = UriUtils.encodeUri(retCretaedPageUrl,
						"UTF-8");
				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(pageID), retCretaedPageUrl);
				if (qrCodeResponse.getResponse().equals(
						ApplicationConstants.SUCCESS)) {
					response = "/" + ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.QRPATH + "/"
							+ String.valueOf(pageID)
							+ ApplicationConstants.PNGIMAGEFORMAT;
				} else {
					response = ApplicationConstants.FAILURE;
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Error occured in RetailerServiceImpl :  createRetailerPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error("Error occured in RetailerServiceImpl :  createRetailerPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (UnsupportedEncodingException e) {
			LOG.error("Error occured in RetailerServiceImpl :  createRetailerPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit method createRetailerCreatedPage");
		return response;
	}

	public String createSplOfferPage(RetailerCustomPage retailerCustomPage)
			throws ScanSeeServiceException {
		LOG.info("Inside the method createSplOfferPage");
		String response = ApplicationConstants.FAILURE;
		int pageID = 0;
		String retCretaedPageUrl = null;
		String retLocID = null;
//		String attachLinkUrl = null;
		String fileSeparator = System.getProperty("file.separator");
		QRCodeResponse qrCodeResponse = null;
		try {

			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					retailerCustomPage.getRetailerId());
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);

			if (retailerCustomPage.getImageUplType().equals("usrUpld")) {
				if (null != retailerCustomPage.getRetailerImg()
						&& !(retailerCustomPage.getRetailerImg().equals(""))) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ retailerCustomPage.getRetailerImg()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream,
								retMediaPath + fileSeparator
										+ retailerCustomPage.getRetailerImg());

					}

				}
				retailerCustomPage.setImageIconID(0);
			} else if (retailerCustomPage.getImageUplType().equals("slctdUpld")) {

				retailerCustomPage.setRetailerImg(null);

			}

			pageID = retailerDAO.insertSplOfferPageInfo(retailerCustomPage);

			if (pageID != 0) {
				ArrayList<AppConfiguration> appConfiguration = retailerDAO
						.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);
				for (int j = 0; j < appConfiguration.size(); j++) {

					retCretaedPageUrl = appConfiguration.get(j)
							.getScreenContent();

				}
				if (retailerCustomPage.getSplOfferLocId().contains(commas)) {
					retLocID = "0";
				} else {
					retLocID = retailerCustomPage.getSplOfferLocId();
				}
				if (retailerCustomPage.getSplOfferType()
						.equals("Special Offer")) {

					retCretaedPageUrl = retCretaedPageUrl
							+ ApplicationConstants.SPECIALOFFERPAGEURL
							+ "key1=" + retailerCustomPage.getRetailerId()
							+ ApplicationConstants.AMPERSAND + "key2="
							+ retLocID + ApplicationConstants.AMPERSAND
							+ "key3=" + pageID;
				} else if (retailerCustomPage.getSplOfferType().equals(
						"AttachLink")) {
					retCretaedPageUrl = retCretaedPageUrl
							+ ApplicationConstants.SPECIALOFFERPAGEURL
							+ "key1=" + retailerCustomPage.getRetailerId()
							+ ApplicationConstants.AMPERSAND + "key2="
							+ retLocID + ApplicationConstants.AMPERSAND
							+ "key3=" + pageID + ApplicationConstants.AMPERSAND
							+ ApplicationConstants.EXTERNALLINK + true;

				}
				retCretaedPageUrl = UriUtils.encodeUri(retCretaedPageUrl,
						"UTF-8");
				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(pageID), retCretaedPageUrl);
				if (qrCodeResponse.getResponse().equals(
						ApplicationConstants.SUCCESS)) {
					response = "/" + ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.QRPATH + "/"
							+ String.valueOf(pageID)
							+ ApplicationConstants.PNGIMAGEFORMAT;
				} else {
					response = ApplicationConstants.FAILURE;
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerCreatedPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerCreatedPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (UnsupportedEncodingException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerCreatedPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit method createRetailerCreatedPage");
		return response;
	}

	public RetailerCustomPage getRetailerPageInfo(Long userId, Long retailerId,
			Long pageId) throws ScanSeeServiceException {
		RetailerCustomPage retailerCustomPage = null;

		try {
			retailerCustomPage = retailerDAO.getRetailerPageInfo(userId,
					retailerId, pageId);

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerCreatedPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		}

		return retailerCustomPage;
	}

	public String updateRetailerCustomPageInfo(
			RetailerCustomPage retailerCustomPage)
			throws ScanSeeServiceException {

		LOG.info("Inside the method createRetailerPage");
		String response = ApplicationConstants.FAILURE;
		String fileSeparator = System.getProperty("file.separator");
		String mediaPath = retailerCustomPage.getMediaPath();
		String externalFlag = retailerCustomPage.getExternalFlag();
		String retCretaedPageUrl = null;
		QRCodeResponse qrCodeResponse = null;
		String retLocID = null;
		String filePath = null;
		String attachLInk = null;
		String attachLinkUrl = null;
		Date date = new Date();
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					retailerCustomPage.getRetailerId());
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);

			if (retailerCustomPage.getPageType().equals(
					ApplicationConstants.QRTYPEMAINMENUPAGE)) {

				if (retailerCustomPage.getLandigPageType()
						.equals("MakeOwnPage")) {
					if ((null != retailerCustomPage.getRetailerImg() && !(retailerCustomPage
							.getRetailerImg().equals("")))
							&& null != retailerCustomPage.getImageName()) {
						if (!(retailerCustomPage.getRetailerImg()
								.equals(retailerCustomPage.getImageName()))) {
							InputStream inputStream = new BufferedInputStream(
									new FileInputStream(retTempMediaPath
											+ fileSeparator
											+ retailerCustomPage
													.getRetailerImg()));
							if (null != inputStream) {
								Utility.writeFileData(
										inputStream,
										retMediaPath
												+ fileSeparator
												+ retailerCustomPage
														.getRetailerImg());

							}
						}
					}

				} else if (retailerCustomPage.getLandigPageType().equals(
						"AttachLink")
						|| retailerCustomPage.getLandigPageType().equals(
								"UploadPdf")) {
					if (retailerCustomPage.getImageUplType().equals("usrUpld")) {

						if ((null != retailerCustomPage.getRetailerImg() && !(retailerCustomPage
								.getRetailerImg().equals("")))
								&& null != retailerCustomPage.getImageName()) {
							if (!(retailerCustomPage.getRetailerImg()
									.equals(retailerCustomPage.getImageName()))) {

								InputStream inputStream = new BufferedInputStream(
										new FileInputStream(retTempMediaPath
												+ fileSeparator
												+ retailerCustomPage
														.getRetailerImg()));
								if (null != inputStream) {
									Utility.writeFileData(
											inputStream,
											retMediaPath
													+ fileSeparator
													+ retailerCustomPage
															.getRetailerImg());

								}
							}
						}

						else if (null != retailerCustomPage.getImageName()
								&& !retailerCustomPage.getImageName()
										.equals("")) {
							retailerCustomPage
									.setRetailerImg(retailerCustomPage
											.getImageName());
						}
						retailerCustomPage.setImageIconID(0);
					} else if (retailerCustomPage.getImageUplType().equals(
							"slctdUpld")) {
						retailerCustomPage.setRetailerImg(null);
					}
				}
				if (retailerCustomPage.getLandigPageType().equals("UploadPdf")) {
					LOG.info(" PDF path ********************" + retMediaPath);
					if (retailerCustomPage.getRetFile().getSize() > 0) {
						filePath = mediaPathBuilder
								+ fileSeparator
								+ retailerCustomPage.getRetFile()
										.getOriginalFilename();
						Utility.writeFileData(retailerCustomPage.getRetFile(),
								filePath);

						String sourceExtn = FilenameUtils
								.getExtension(retailerCustomPage.getRetFile()
										.getOriginalFilename());
						if (!(sourceExtn.equals("pdf") || sourceExtn
								.equals("PDF"))) {
							String imageSource = FilenameUtils
									.removeExtension(retailerCustomPage
											.getRetFile().getOriginalFilename());
							imageSource = imageSource + ".png";

							retailerCustomPage.setPdfFileName(imageSource);
						} else {
							retailerCustomPage
									.setPdfFileName(retailerCustomPage
											.getRetFile().getOriginalFilename());
						}

					} else if (!retailerCustomPage.getOldPdfFileName().equals(
							retailerCustomPage.getPdfFileName())) {
						filePath = mediaPathBuilder + fileSeparator
								+ retailerCustomPage.getPdfFileName();

						InputStream inputStream = new FileInputStream(
								mediaTempPathBuilder + fileSeparator
										+ retailerCustomPage.getPdfFileName());

						Utility.writeFileData(inputStream, filePath);

						/*
						 * String sourceExtn =
						 * FilenameUtils.getExtension(retailerCustomPage
						 * .getPdfFileName()); if (!(sourceExtn.equals("pdf") ||
						 * sourceExtn.equals("PDF"))) { String imageSource =
						 * FilenameUtils
						 * .removeExtension(retailerCustomPage.getPdfFileName
						 * ()); imageSource = imageSource + ".png";
						 * retailerCustomPage.setPdfFileName(imageSource); }
						 */
					}
				}

				response = retailerDAO
						.updateRetailerCustomPageInfo(retailerCustomPage);
				if (response != null
						&& !response.equals(ApplicationConstants.FAILURE)) {
					ArrayList<AppConfiguration> appConfiguration = retailerDAO
							.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);
					for (int j = 0; j < appConfiguration.size(); j++) {

						retCretaedPageUrl = appConfiguration.get(j)
								.getScreenContent();

					}

					if (retailerCustomPage.getHiddenLocId().contains(commas)) {
						retLocID = "0";
					} else {
						retLocID = retailerCustomPage.getHiddenLocId();
					}
					if (retailerCustomPage.getLandigPageType().equals(
							"AttachLink")
							|| retailerCustomPage.getLandigPageType().equals(
									"UploadPdf")) {
						retCretaedPageUrl = retCretaedPageUrl
								+ ApplicationConstants.RETAILERCREATEDPAGEURL
								+ "key1=" + retailerCustomPage.getRetailerId()
								+ ApplicationConstants.AMPERSAND + "key2="
								+ retLocID + ApplicationConstants.AMPERSAND
								+ "key3=" + retailerCustomPage.getPageId()
								+ ApplicationConstants.AMPERSAND
								+ ApplicationConstants.EXTERNALLINK + true;

					}

					else if (retailerCustomPage.getLandigPageType().equals(
							"MakeOwnPage")) {

						retCretaedPageUrl = retCretaedPageUrl
								+ ApplicationConstants.RETAILERCREATEDPAGEURL
								+ "key1=" + retailerCustomPage.getRetailerId()
								+ ApplicationConstants.AMPERSAND + "key2="
								+ retLocID + ApplicationConstants.AMPERSAND
								+ "key3=" + retailerCustomPage.getPageId();
					}

					/*
					 * else if
					 * (retailerCustomPage.getLandigPageType().equals("UploadPdf"
					 * )) { ArrayList<AppConfiguration> serverConfg =
					 * retailerDAO
					 * .getAppConfig(ApplicationConstants.RETAILERMEDIAPATH);
					 * for (int j = 0; j < serverConfg.size(); j++) {
					 * retCretaedPageUrl =
					 * serverConfg.get(j).getScreenContent(); }
					 * retCretaedPageUrl = retCretaedPageUrl +
					 * retailerCustomPage.getRetailerId() + "/" +
					 * retailerCustomPage.getPdfFileName(); }
					 */
					retCretaedPageUrl = UriUtils.encodeUri(retCretaedPageUrl,
							"UTF-8");
					qrCodeResponse = QRCodeGenerator.generateQRCode(
							String.valueOf(retailerCustomPage.getPageId()),
							retCretaedPageUrl);
					if (qrCodeResponse.getResponse().equals(
							ApplicationConstants.SUCCESS)) {
						response = "/"
								+ ApplicationConstants.IMAGES
								+ "/"
								+ ApplicationConstants.QRPATH
								+ "/"
								+ String.valueOf(retailerCustomPage.getPageId())
								+ ApplicationConstants.PNGIMAGEFORMAT + "?"
								+ date.getTime();

					} else {
						response = ApplicationConstants.FAILURE;
					}

				} else {
					response = ApplicationConstants.FAILURE;
				}

			} else if (retailerCustomPage.getPageType().equals(
					ApplicationConstants.QRTYPELANDINGPAGE)) {
				if (retailerCustomPage.getRetCreatedPageImage().getSize() > 0) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ retailerCustomPage
											.getRetCreatedPageImage()
											.getOriginalFilename()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream, retMediaPath
								+ fileSeparator
								+ retailerCustomPage.getRetCreatedPageImage()
										.getOriginalFilename());
						retailerCustomPage
								.setRetailerImg(retailerCustomPage
										.getRetCreatedPageImage()
										.getOriginalFilename());
					}

				} else if (null != retailerCustomPage.getImageName()
						|| !retailerCustomPage.getImageName().equals("")) {
					retailerCustomPage.setRetailerImg(retailerCustomPage
							.getImageName());
				}

				if (retailerCustomPage.getLandigPageType().equals("Landing")) {

					CommonsMultipartFile[] uploadFile = retailerCustomPage
							.getRetCreatedPageFile();

					for (int i = 0; i < uploadFile.length; i++) {

						if (uploadFile[i].getSize() > 0) {
							String path = retMediaPath + "/"
									+ uploadFile[i].getOriginalFilename();
							LOG.info("Saving File to Disk Path " + path);
							Utility.writeFileData(uploadFile[i], path);
							mediaPath = mediaPath
									+ uploadFile[i].getOriginalFilename()
									+ commas;

							// Flag to indicate that the uploading file source
							// is
							// from
							// ScanSee
							// media server
							externalFlag = externalFlag + "0" + commas;

						}
						retailerCustomPage.setMediaPath(mediaPath);
						retailerCustomPage.setExternalFlag(externalFlag);
					}
				} else if (retailerCustomPage.getLandigPageType().equals(
						"AttachLink")) {
					retailerCustomPage.setMediaPath(retailerCustomPage
							.getRetCreatedPageattachLink());
					retailerCustomPage.setExternalFlag("1");

				}

				response = retailerDAO
						.updateRetailerCustomPageInfo(retailerCustomPage);

				if (response != null
						&& !response.equals(ApplicationConstants.FAILURE)) {
					ArrayList<AppConfiguration> appConfiguration = retailerDAO
							.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);

					if (retailerCustomPage.getLandigPageType()
							.equals("Landing")) {
						if (retailerCustomPage.getRetCreatedPageLocId()
								.contains(commas)) {
							retLocID = "0";
						} else {
							retLocID = retailerCustomPage
									.getRetCreatedPageLocId();
						}
					} else if (retailerCustomPage.getLandigPageType().equals(
							"AttachLink")) {

						attachLInk = retailerCustomPage
								.getRetCreatedPageattachLink();

					}

					for (int j = 0; j < appConfiguration.size(); j++) {

						retCretaedPageUrl = appConfiguration.get(j)
								.getScreenContent();

					}
					if (retailerCustomPage.getLandigPageType()
							.equals("Landing")) {
						retCretaedPageUrl = retCretaedPageUrl
								+ ApplicationConstants.RETAILERCREATEDPAGEURL
								+ "key1=" + retailerCustomPage.getRetailerId()
								+ ApplicationConstants.AMPERSAND + "key2="
								+ retLocID + ApplicationConstants.AMPERSAND
								+ "key3=" + retailerCustomPage.getPageId();
						qrCodeResponse = QRCodeGenerator.generateQRCode(
								String.valueOf(retailerCustomPage.getPageId()),
								retCretaedPageUrl);

					} else if (retailerCustomPage.getLandigPageType().equals(
							"AttachLink")) {

						retCretaedPageUrl = attachLInk;
						qrCodeResponse = QRCodeGenerator.generateQRCode(
								String.valueOf(retailerCustomPage.getPageId()),
								retCretaedPageUrl);
					}

					if (qrCodeResponse.getResponse().equals(
							ApplicationConstants.SUCCESS)) {
						response = "/"
								+ ApplicationConstants.IMAGES
								+ "/"
								+ ApplicationConstants.QRPATH
								+ "/"
								+ String.valueOf(retailerCustomPage.getPageId())
								+ ApplicationConstants.PNGIMAGEFORMAT + "?"
								+ date.getTime();

					} else {
						response = ApplicationConstants.FAILURE;
					}

				} else {
					response = ApplicationConstants.FAILURE;
				}

			} else if (retailerCustomPage.getPageType().equals(
					ApplicationConstants.QRTYPESPLOFFERPAGE)) {
				if (retailerCustomPage.getSplOfferType()
						.equals("Special Offer")) {

					if (null != retailerCustomPage.getRetailerImg()
							&& !(retailerCustomPage.getRetailerImg().equals(""))) {
						InputStream inputStream = new BufferedInputStream(
								new FileInputStream(retTempMediaPath
										+ fileSeparator
										+ retailerCustomPage.getRetailerImg()));
						if (null != inputStream) {
							Utility.writeFileData(
									inputStream,
									retMediaPath
											+ fileSeparator
											+ retailerCustomPage
													.getRetailerImg());

						}

					} else if (null != retailerCustomPage.getImageName()
							&& !retailerCustomPage.getImageName().equals("")) {
						retailerCustomPage.setRetailerImg(retailerCustomPage
								.getImageName());
					}

					CommonsMultipartFile[] uploadFile = retailerCustomPage
							.getSplOfferFile();
					for (int i = 0; i < uploadFile.length; i++) {

						if (uploadFile[i].getSize() > 0) {
							String path = retMediaPath + "/"
									+ uploadFile[i].getOriginalFilename();
							LOG.info("Saving File to Disk Path " + path);
							Utility.writeFileData(uploadFile[i], path);
							mediaPath = mediaPath
									+ uploadFile[i].getOriginalFilename()
									+ commas;

							// Flag to indicate that the uploading file source
							// is
							// from
							// ScanSee
							// media server
							externalFlag = externalFlag + "0" + commas;

						}
						retailerCustomPage.setMediaPath(mediaPath);
						retailerCustomPage.setExternalFlag(externalFlag);
					}

				} else if (retailerCustomPage.getSplOfferType().equals(
						"AttachLink")) {

					if (retailerCustomPage.getImageUplType().equals("usrUpld")) {
						if (null != retailerCustomPage.getRetailerImg()
								&& !(retailerCustomPage.getRetailerImg()
										.equals(""))) {
							InputStream inputStream = new BufferedInputStream(
									new FileInputStream(retTempMediaPath
											+ fileSeparator
											+ retailerCustomPage
													.getRetailerImg()));
							if (null != inputStream) {
								Utility.writeFileData(
										inputStream,
										retMediaPath
												+ fileSeparator
												+ retailerCustomPage
														.getRetailerImg());

							}

						} else if (null != retailerCustomPage.getImageName()
								&& !retailerCustomPage.getImageName()
										.equals("")) {
							retailerCustomPage
									.setRetailerImg(retailerCustomPage
											.getImageName());
						}

						retailerCustomPage.setImageIconID(0);

					} else if (retailerCustomPage.getImageUplType().equals(
							"slctdUpld")) {

						retailerCustomPage.setRetailerImg(null);
					}

					retailerCustomPage.setMediaPath(retailerCustomPage
							.getSplOfferattachLink());
					retailerCustomPage.setExternalFlag("1");
					/*
					 * if (null != retailerCustomPage.getSplOfferattachLink() &&
					 * !retailerCustomPage.getSplOfferattachLink().equals("")) {
					 * String[] attachLink =
					 * retailerCustomPage.getSplOfferattachLink().split(commas);
					 * for (int i = 0; i < attachLink.length; i++) { mediaPath =
					 * mediaPath + attachLink[i] + commas; // Flag to indicate
					 * that the uploading file source // is // from // External
					 * externalFlag = externalFlag + "1" + commas; } }
					 */
				}

				response = retailerDAO
						.updateRetailerCustomPageInfo(retailerCustomPage);

				if (response != null
						&& !response.equals(ApplicationConstants.FAILURE)) {
					ArrayList<AppConfiguration> appConfiguration = retailerDAO
							.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);
					for (int j = 0; j < appConfiguration.size(); j++) {

						retCretaedPageUrl = appConfiguration.get(j)
								.getScreenContent();

					}
					if (retailerCustomPage.getSplOfferType().equals(
							"Special Offer")) {
						if (retailerCustomPage.getSplOfferLocId().contains(
								commas)) {
							retLocID = "0";
						} else {
							retLocID = retailerCustomPage.getSplOfferLocId();
						}
					} else if (retailerCustomPage.getSplOfferType().equals(
							"AttachLink")) {
						attachLinkUrl = retailerCustomPage
								.getSplOfferattachLink();
					}

					if (retailerCustomPage.getSplOfferType().equals(
							"Special Offer")) {

						retCretaedPageUrl = retCretaedPageUrl
								+ ApplicationConstants.SPECIALOFFERPAGEURL
								+ "key1=" + retailerCustomPage.getRetailerId()
								+ ApplicationConstants.AMPERSAND + "key2="
								+ retLocID + ApplicationConstants.AMPERSAND
								+ "key3=" + retailerCustomPage.getPageId();
						qrCodeResponse = QRCodeGenerator.generateQRCode(
								String.valueOf(retailerCustomPage.getPageId()),
								retCretaedPageUrl);
					} else if (retailerCustomPage.getSplOfferType().equals(
							"AttachLink")) {
						retCretaedPageUrl = attachLinkUrl;
						qrCodeResponse = QRCodeGenerator.generateQRCode(
								String.valueOf(retailerCustomPage.getPageId()),
								retCretaedPageUrl);
					}

					if (qrCodeResponse.getResponse().equals(
							ApplicationConstants.SUCCESS)) {
						response = "/"
								+ ApplicationConstants.IMAGES
								+ "/"
								+ ApplicationConstants.QRPATH
								+ "/"
								+ String.valueOf(retailerCustomPage.getPageId())
								+ ApplicationConstants.PNGIMAGEFORMAT + "?"
								+ date.getTime();

					} else {
						response = ApplicationConstants.FAILURE;
					}
				} else {
					response = ApplicationConstants.FAILURE;
				}
			}

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  updateRetailerCustomPageInfo : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.info("Inside RetailerServiceImpl :  updateRetailerCustomPageInfo : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (UnsupportedEncodingException e) {

			LOG.info("Inside RetailerServiceImpl :  updateRetailerCustomPageInfo : "
					+ e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit method createRetailerCreatedPage");
		return response;

	}

	/**
	 * This serviceImpl method will return retailer name and retailer image
	 * 
	 * @param retailerId
	 * @return retailer name and image
	 * @throws ScanSeeServiceException
	 */
	public Retailer fetchRetaielrInfo(Long retailerId)
			throws ScanSeeServiceException {
		final String methodName = "fetchRetaielrInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Retailer retailerObj = null;

		try {

			retailerObj = retailerDAO.fetchRetaielrInfo(retailerId);

		} catch (ScanSeeServiceException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerPage : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerObj;
	}

	public String validateRetailerPage(Long retialerId, String retailerLocId,
			Long pageID, int qrType) throws ScanSeeServiceException {
		final String methodName = "validateRetailerPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try {

			response = retailerDAO.validateRetailerPage(retialerId,
					retailerLocId, pageID, qrType);

		} catch (ScanSeeServiceException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerPage : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String deleteRetailerPage(Long pageID)
			throws ScanSeeServiceException {
		final String methodName = "deleteRetailerPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try {

			response = retailerDAO.deleteRetailerPage(pageID);

		} catch (ScanSeeServiceException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerPage : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This serviceImpl method is used to domain Name for Application
	 * configuration.
	 * 
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String getDomainName() throws ScanSeeServiceException {
		String strDomainName = null;
		try {
			ArrayList<AppConfiguration> domainNameList = retailerDAO
					.getAppConfigForGeneralImages(ApplicationConstants.SERVER_CONFIGURATION);
			for (int j = 0; j < domainNameList.size(); j++) {
				strDomainName = domainNameList.get(j).getScreenContent();
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside SupplierServiceImpl : getDomainName : " + e);
			throw new ScanSeeServiceException(e);
		}
		return strDomainName;
	}

	/**
	 * This serviceImpl method updates location latitude and longitude fields on
	 * changes in address, state, city and postalcode and call DAO layer.
	 * 
	 * @param locationCoordinates
	 *            as request parameter.
	 * @param retailerName
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String addLocationCoordinates(String locationCoordinates,
			String retailerName) throws ScanSeeServiceException {
		if (!Utility.isEmptyOrNullString(retailerName)
				&& !Utility.isEmptyOrNullString(locationCoordinates)) {
			final String[] arrLocCord = locationCoordinates.split(commas);
			final String latitude = arrLocCord[0].split("\\(")[1];
			final String longitude = arrLocCord[1].split("\\)")[0];
			if (!Utility.isEmptyOrNullString(latitude)
					&& !Utility.isEmptyOrNullString(longitude)) {
				try {
					retailerDAO.addLocationCoordinates(latitude.trim(),
							longitude.trim(), retailerName);
				} catch (ScanSeeWebSqlException e) {
					LOG.error(ApplicationConstants.EXCEPTION_OCCURED,
							e.getMessage());
					throw new ScanSeeServiceException(e);
				}
			}
		}
		return null;
	}

	/**
	 * This serviceImpl method is used for deleting custom page.
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String deleteCustomPage(Long pageId, int retailerId)
			throws ScanSeeServiceException {
		final String methodName = "deleteCustomPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
//		QRCodeResponse qrCodeResponse = null;
		try {

			response = retailerDAO.deleteCustomPage(pageId, retailerId);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerPage : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String addCorpAndStoreLocationCoordinates(
			String locationCoordinates, String retailerName)
			throws ScanSeeServiceException {
		if (!Utility.isEmptyOrNullString(retailerName)
				&& !Utility.isEmptyOrNullString(locationCoordinates)) {
			String[] arrLocCord = locationCoordinates.split(commas);
			String latitude = arrLocCord[0].split("\\(")[1];
			String longitude = arrLocCord[1].split("\\)")[0];
			if (!Utility.isEmptyOrNullString(latitude)
					&& !Utility.isEmptyOrNullString(longitude)) {
				try {
					retailerDAO.addCorpAndStoreLocationCoordinates(
							latitude.trim(), longitude.trim(), retailerName);
				} catch (ScanSeeWebSqlException exp) {
					LOG.info("Inside RetailerServiceImpl : addCorpAndStoreLocationCoordinates : "
							+ exp);
					throw new ScanSeeServiceException(exp);
				}
			}
		}
		return null;
	}

	/**
	 * This service method will return the Retailer Location Advertisement List
	 * based on input parameter.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public List<RetailerLocationAdvertisement> getWelcomePageForDisplay(
			Long adsIDs) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getWelcomePageForDisplay ");
		List<RetailerLocationAdvertisement> arAdsList = null;
		try {
			arAdsList = retailerDAO.getWelcomePageForDisplay(adsIDs);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : getWelcomePageForDisplay : "
					+ e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arAdsList;
	}

	/**
	 * This service method will update Retailer Location Advertisement Info.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public String updateWelcomePageInfo(
			RetailerLocationAdvertisement objRetLocAds)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : updateWelcomePageInfo ");
		String strStatus = null;

		String fileSeparator = System.getProperty("file.separator");
		try {

			StringBuilder mediaPathBuilder = Utility
					.getMediaPath(ApplicationConstants.RETAILER,
							Integer.valueOf(String.valueOf(objRetLocAds
									.getRetailID())));
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);
			if (null != objRetLocAds.getStrBannerAdImagePath()
					&& !(objRetLocAds.getStrBannerAdImagePath().equals(""))) {
				if ((null != objRetLocAds.getTempImageName() && !(objRetLocAds
						.getTempImageName().equals("")))
						&& !(objRetLocAds.getStrBannerAdImagePath()
								.equals(objRetLocAds.getTempImageName()))) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ objRetLocAds.getStrBannerAdImagePath()));
					if (null != inputStream) {
						Utility.writeFileData(
								inputStream,
								retMediaPath
										+ fileSeparator
										+ objRetLocAds
												.getStrBannerAdImagePath());

					}
				}

			}
			strStatus = retailerDAO.updateWelcomePageInfo(objRetLocAds);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Exception occured in:::insertBannerAdInfo::\n" + e);
			throw new ScanSeeServiceException(e.getMessage());
		} catch (FileNotFoundException e) {
			LOG.error("Exception occured in:::insertBannerAdInfo::\n" + e);
			throw new ScanSeeServiceException(e.getMessage());
		}

		return strStatus;
	}

	/**
	 * This service method will return the Retailer Location Advertisement List
	 * based on input parameter.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public List<RetailerLocation> getLocationIDForWelcomePage(Long adsIDs)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getLocationIDForWelcomePage ");
		List<RetailerLocation> arAdsList = null;
		try {
			arAdsList = retailerDAO.getLocationIDForWelcomePage(adsIDs);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : getLocationIDForWelcomePage : "
					+ e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arAdsList;
	}

	/**
	 * This service method deletes the welcome page from list.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteWelcomePage(RetailerLocationAdvertisement objRetLocAds)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : deleteWelcomePage ");
		String isDataDeleted = null;
		try {
			isDataDeleted = retailerDAO.deleteWelcomePage(objRetLocAds);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : deleteWelcomePage "
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		}
		return isDataDeleted;
	}

	/**
	 * This service method will create banner page.
	 * 
	 * @param retLocationAds
	 *            instance of RetailerLocationAdvertisement
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 */
	public String buildBannerAdInfo(RetailerLocationAdvertisement retLocationAds)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : buildBannerAdInfo ");
		String response = null;
		String fileSeparator = System.getProperty("file.separator");
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER, Integer.valueOf(String
							.valueOf(retLocationAds.getRetailID())));
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);

			if (null != retLocationAds.getStrBannerAdImagePath()
					&& !(retLocationAds.getStrBannerAdImagePath().equals(""))) {
				InputStream inputStream = new BufferedInputStream(
						new FileInputStream(retTempMediaPath + fileSeparator
								+ retLocationAds.getStrBannerAdImagePath()));
				if (null != inputStream) {
					Utility.writeFileData(
							inputStream,
							retMediaPath + fileSeparator
									+ retLocationAds.getStrBannerAdImagePath());

				}
			}

			response = retailerDAO.buildBannerAdInfo(retLocationAds);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : buildBannerAdInfo : "
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		} catch (FileNotFoundException e) {
			LOG.info("Inside RetailerServiceImpl : buildBannerAdInfo : "
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		}
		return response;
	}

	/**
	 * This service method will display retailer banner page details based on
	 * input parameter (bannerId) by calling DAO method.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return arAdsList,Retailer Location Advertisement details.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final List<RetailerLocation> getLocationIDForBannerPage(Long adsIDs)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getLocationIDForBannerPage ");
		List<RetailerLocation> arAdsList = null;
		try {
			arAdsList = retailerDAO.getLocationIDForBannerPage(adsIDs);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arAdsList;
	}

	/**
	 * This service method deletes the welcome page from list.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @param objBannerAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteBannerPage(RetailerLocationAdvertisement objBannerAds)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : deleteBannerPage ");
		String isDataDeleted = null;
		try {
			isDataDeleted = retailerDAO.deleteBannerPage(objBannerAds);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : deleteBannerPage "
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		}
		return isDataDeleted;
	}

	/**
	 * This serviceImpl method will update existing banner page details based
	 * return based on input parameter (bannerId) by calling DAO method.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String updateBuildBannerInfo(
			RetailerLocationAdvertisement objRetLocAds)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : updateBuildBannerInfo ");
		String strStatus = null;
		final String fileSeparator = System.getProperty("file.separator");
		try {
			final StringBuilder mediaPathBuilder = Utility
					.getMediaPath(ApplicationConstants.RETAILER,
							Integer.valueOf(String.valueOf(objRetLocAds
									.getRetailID())));
			final StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			final String retMediaPath = mediaPathBuilder.toString();
			final String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);
			if (null != objRetLocAds.getStrBannerAdImagePath()
					&& !(objRetLocAds.getStrBannerAdImagePath().equals(""))) {

				if ((null != objRetLocAds.getTempImageName() && !""
						.equals(objRetLocAds.getTempImageName()))
						&& !(objRetLocAds.getStrBannerAdImagePath()
								.equals(objRetLocAds.getTempImageName()))) {
					final InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ objRetLocAds.getStrBannerAdImagePath()));
					if (null != inputStream) {
						Utility.writeFileData(
								inputStream,
								retMediaPath
										+ fileSeparator
										+ objRetLocAds
												.getStrBannerAdImagePath());
					}
				}
			}
			strStatus = retailerDAO.updateBuildBannerInfo(objRetLocAds);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return strStatus;
	}

	/**
	 * This serviceImpl method will display list of created Banner by the
	 * retailer or by searching Banner Names.
	 * 
	 * @param lRetailID
	 *            as request parameter.
	 * @param objForm
	 *            as instance of SearchResultInfo.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public SearchResultInfo getBannerAdsByAdsNames(Long lRetailID,
			SearchForm objForm, int lowerLimit, int recordCount)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getBannerAdsByAdsNames ");
		SearchResultInfo buildBannerAdlist = null;
		try {
			buildBannerAdlist = retailerDAO.getBannerAdsByAdsNames(lRetailID,
					objForm.getSearchKey(), lowerLimit, recordCount);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return buildBannerAdlist;
	}

	/**
	 * This service method will return the Banner Advertisement info based on
	 * input parameter.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<RetailerLocationAdvertisement> getBannerPageForDisplay(
			Long adsIDs) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getBannerPageForDisplay ");
		List<RetailerLocationAdvertisement> arAdsList = null;
		try {
			arAdsList = retailerDAO.getBannerPageForDisplay(adsIDs);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : getBannerPageForDisplay : "
					+ e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arAdsList;
	}

	public String processRetailerCroppedImage(
			CommonsMultipartFile retailerUploadLogoInfo, String realPath,
			Users loginUser, int x, int y, int w, int h)
			throws ScanSeeServiceException {

		ScanSeeProperties properties = new ScanSeeProperties();
		properties.readProperties();
//		String response = ApplicationConstants.SUCCESS;
		String fileSeparator = System.getProperty("file.separator");
		Random rand = new Random();
		int numNoRange = rand.nextInt(100000);
		String outputFileName;
		String sourceFileName;
		try {
			StringBuilder mediaPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String suppMediaPath = mediaPathBuilder.toString();
			// String suppMediaPath = realPath + fileSeparator + "flow_old";
			outputFileName = retailerUploadLogoInfo.getOriginalFilename();
			sourceFileName = retailerUploadLogoInfo.getOriginalFilename();
			if (!Utility.isEmptyOrNullString(outputFileName)) {
				outputFileName = FilenameUtils
						.removeExtension(retailerUploadLogoInfo
								.getOriginalFilename());
				sourceFileName = FilenameUtils
						.removeExtension(retailerUploadLogoInfo
								.getOriginalFilename());
				outputFileName = outputFileName + "_"
						+ String.valueOf(numNoRange) + ".png";
				sourceFileName = sourceFileName + ".png";
			}
			LOG.info(" Images path ********************" + suppMediaPath
					+ fileSeparator + outputFileName);
			// Update local Utiliy Code and uncomment below line for cropping
			// feature
			Utility.writeCroppedFileData(retailerUploadLogoInfo, suppMediaPath
					+ fileSeparator + sourceFileName, suppMediaPath
					+ fileSeparator + outputFileName, x, y, w, h);

		} catch (Exception exception) {
			throw new ScanSeeServiceException(exception.getMessage());
		}
		return outputFileName;
	}

	/**
	 * This serviceImpl method is used for fetching the image icons to display
	 * in link page.
	 * 
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public ArrayList<RetailerImages> getRetailerImageIconsDisplay(
			String pageType) throws ScanSeeServiceException {

		ArrayList<RetailerImages> retailerLocations = null;
		try {
			retailerLocations = retailerDAO
					.getRetailerImageIconsDisplay(pageType);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return retailerLocations;
	}

	/**
	 * This serviceImpl method is used to display the list of special offer
	 * pages.
	 * 
	 * @param retailerId
	 * @param RetailerCustomPage objCustomPage
	 * @param currentPage
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public SearchResultInfo getspecialOfferList(int retailerId,
			RetailerCustomPage objCustomPage, int currentPage, int recordCount)
			throws ScanSeeServiceException {
		final String methodName = "getspecialOfferList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo customPageList = null;
		try {
			customPageList = retailerDAO.getspecialOfferList(retailerId,
					objCustomPage, currentPage, recordCount);
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return customPageList;
	}

	public void addLocationCoordinatesWithRetailID(String locationCoordinates,
			Integer retailID, String storeId) throws ScanSeeServiceException {
		if (retailID != 0 && !Utility.isEmptyOrNullString(locationCoordinates)
				&& !Utility.isEmptyOrNullString(storeId)) {
			String[] arrLocCord = locationCoordinates.split(commas);
			String latitude = arrLocCord[0].split("\\(")[1];
			String longitude = arrLocCord[1].split("\\)")[0];
			if (!Utility.isEmptyOrNullString(latitude)
					&& !Utility.isEmptyOrNullString(longitude)) {
				try {
					retailerDAO.addLocationCoordinatesWithRetailId(
							latitude.trim(), longitude.trim(), retailID,
							storeId);
				} catch (ScanSeeWebSqlException exp) {
					LOG.info("Inside RetailerServiceImpl : addLocationCoordinates : "
							+ exp);
					throw new ScanSeeServiceException(exp);
				}
			}
		}

	}

	/**
	 * This serviceImpl method is used for updating special offer page.
	 * 
	 * @param retailerCustomPage
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String updateRetailerSplOfrPageInfo(
			RetailerCustomPage retailerCustomPage)
			throws ScanSeeServiceException {

		LOG.info("Inside the method createRetailerPage");
		String response = ApplicationConstants.FAILURE;
		String fileSeparator = System.getProperty("file.separator");
		String retCretaedPageUrl = null;
		QRCodeResponse qrCodeResponse = null;
		Date date = new Date();
		String retLocID = null;
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					retailerCustomPage.getRetailerId());
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);

			if (retailerCustomPage.getSplOfferType().equals("Special Offer")) {
				if ((null != retailerCustomPage.getRetailerImg() && !(retailerCustomPage
						.getRetailerImg().equals("")))
						&& null != retailerCustomPage.getImageName()) {
					if (!(retailerCustomPage.getRetailerImg()
							.equals(retailerCustomPage.getImageName()))) {
						InputStream inputStream = new BufferedInputStream(
								new FileInputStream(retTempMediaPath
										+ fileSeparator
										+ retailerCustomPage.getRetailerImg()));
						if (null != inputStream) {
							Utility.writeFileData(
									inputStream,
									retMediaPath
											+ fileSeparator
											+ retailerCustomPage
													.getRetailerImg());

						}
					}
				}

			} else if (retailerCustomPage.getSplOfferType()
					.equals("AttachLink")) {
				if (retailerCustomPage.getImageUplType().equals("usrUpld")) {

					if ((null != retailerCustomPage.getRetailerImg() && !(retailerCustomPage
							.getRetailerImg().equals("")))
							&& null != retailerCustomPage.getImageName()) {
						if (!(retailerCustomPage.getRetailerImg()
								.equals(retailerCustomPage.getImageName()))) {
							InputStream inputStream = new BufferedInputStream(
									new FileInputStream(retTempMediaPath
											+ fileSeparator
											+ retailerCustomPage
													.getRetailerImg()));
							if (null != inputStream) {
								Utility.writeFileData(
										inputStream,
										retMediaPath
												+ fileSeparator
												+ retailerCustomPage
														.getRetailerImg());

							}

						}

					}

					else if (null != retailerCustomPage.getImageName()
							&& !retailerCustomPage.getImageName().equals("")) {
						retailerCustomPage.setRetailerImg(retailerCustomPage
								.getImageName());
					}
					retailerCustomPage.setImageIconID(0);
				} else if (retailerCustomPage.getImageUplType().equals(
						"slctdUpld")) {
					retailerCustomPage.setRetailerImg(null);
				}
			}

			response = retailerDAO
					.updateRetailerSplOfrPageInfo(retailerCustomPage);
			if (response != null
					&& !response.equals(ApplicationConstants.FAILURE)) {
				ArrayList<AppConfiguration> appConfiguration = retailerDAO
						.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);
				for (int j = 0; j < appConfiguration.size(); j++) {
					retCretaedPageUrl = appConfiguration.get(j)
							.getScreenContent();
				}
				if (retailerCustomPage.getSplOfferLocId().contains(commas)) {
					retLocID = "0";
				} else {
					retLocID = retailerCustomPage.getSplOfferLocId();
				}
				if (retailerCustomPage.getSplOfferType()
						.equals("Special Offer")) {
					retCretaedPageUrl = retCretaedPageUrl
							+ ApplicationConstants.SPECIALOFFERPAGEURL
							+ "key1=" + retailerCustomPage.getRetailerId()
							+ ApplicationConstants.AMPERSAND + "key2="
							+ retLocID + ApplicationConstants.AMPERSAND
							+ "key3=" + retailerCustomPage.getPageId();

				} else if (retailerCustomPage.getSplOfferType().equals(
						"AttachLink")) {
					retCretaedPageUrl = retCretaedPageUrl
							+ ApplicationConstants.SPECIALOFFERPAGEURL
							+ "key1=" + retailerCustomPage.getRetailerId()
							+ ApplicationConstants.AMPERSAND + "key2="
							+ retLocID + ApplicationConstants.AMPERSAND
							+ "key3=" + retailerCustomPage.getPageId()
							+ ApplicationConstants.AMPERSAND
							+ ApplicationConstants.EXTERNALLINK + true;
				}

				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(retailerCustomPage.getPageId()),
						retCretaedPageUrl);

				if (qrCodeResponse.getResponse().equals(
						ApplicationConstants.SUCCESS)) {
					response = "/" + ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.QRPATH + "/"
							+ String.valueOf(retailerCustomPage.getPageId())
							+ ApplicationConstants.PNGIMAGEFORMAT + "?"
							+ date.getTime();
				} else {
					response = ApplicationConstants.FAILURE;
				}
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  updateRetailerCustomPageInfo : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.info("Inside RetailerServiceImpl :  updateRetailerCustomPageInfo : "
					+ e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Exit method createRetailerCreatedPage");
		return response;
	}

	public RetailerCustomPage getRetailerSplOfrPageInfo(Long retailerId,
			Long pageId) throws ScanSeeServiceException {
		RetailerCustomPage retailerCustomPage = null;

		try {
			retailerCustomPage = retailerDAO.getRetailerSplOfrPageInfo(
					retailerId, pageId);

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  createRetailerCreatedPage : "
					+ e);
			throw new ScanSeeServiceException(e);
		}

		return retailerCustomPage;
	}

	/**
	 * The serviceImpl method will get Quick response code(QR code)for retailer
	 * Location based on input parameter by calling DAO methods.
	 * 
	 * @param retLocID
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @return RetailerCustomPage instance.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final RetailerCustomPage getLocQrCodeDetails(Long retLocID,
			int retailerId, Long userId) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getLocQrCodeDetails ");
		RetailerCustomPage pageDetails = null;
		QRCodeResponse qrCodeResponse = null;
		try {
			pageDetails = retailerDAO.getRetLocQRcode(retLocID, retailerId,
					userId);
			if (pageDetails != null) {
				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(retLocID), pageDetails.getQrUrl());
				if (qrCodeResponse.getResponse().equals(
						ApplicationConstants.SUCCESS)) {
					final String tempQrPath = "/" + ApplicationConstants.IMAGES
							+ "/" + ApplicationConstants.QRPATH + "/"
							+ String.valueOf(retLocID)
							+ ApplicationConstants.PNGIMAGEFORMAT;
					pageDetails.setQrImagePath(tempQrPath);
				} else {
					LOG.error(ApplicationConstants.EXCEPTION_OCCURED,
							"Error occured while creating QR code");
					throw new ScanSeeServiceException(
							"::::Error occured while creating QR code::::");
				}
			} else {
				LOG.error(ApplicationConstants.EXCEPTION_OCCURED,
						"Page details not found for the Page id");
				throw new ScanSeeServiceException(
						"Page details not found for the Page id");
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return pageDetails;
	}

	/**
	 * Retrieves discount plan list for the Retailer return discountPlanList;
	 */
	public Map<String, Map<String, String>> getRetailerDiscountPlans()
			throws ScanSeeServiceException {
		Map<String, Map<String, String>> discountPlanList = null;
		try {
			discountPlanList = retailerDAO.getRetailerDiscountPlans();
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  getRetailerDiscountPlans : "
					+ e);
			throw new ScanSeeServiceException(e);
		}

		return discountPlanList;
	}

	/**
	 * This service method will return the all RetailerLocation list based on
	 * input parameter.
	 * 
	 * @param searchKey
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<RetailerLocationProduct> getAllRetailerLocationList(
			Long retailerId, String searchKey) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  getAllRetailerLocationList ");
		List<RetailerLocationProduct> arRetLocationList = null;
		try {
			arRetLocationList = retailerDAO.getAllRetailerLocationList(
					retailerId, searchKey);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  getAllRetailerLocationList : "
					+ e);
			throw new ScanSeeServiceException(e);
		}
		return arRetLocationList;
	}

	/**
	 * This service method will return the Product details based on input
	 * parameter.
	 * 
	 * @param objRetLocationProduct
	 *            as request parameter.
	 * @return Product Details.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<RetailerLocationProduct> getAssocProdToLocationPop(
			RetailerLocationProduct objRetLocationProduct)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  getAssocProdToLocationPop ");
		List<RetailerLocationProduct> arRetLocationList = null;
		try {
			arRetLocationList = retailerDAO
					.getAssocProdToLocationPop(objRetLocationProduct);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  getAssocProdToLocationPop : "
					+ e);
			throw new ScanSeeServiceException(e);
		}
		return arRetLocationList;
	}

	public String updateRetailerProductList(
			RetailerLocationProduct objRetLocationProduct)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  updateRetailerProductList ");
		String response = null;
		String strProductJson = null;
		try {
			strProductJson = objRetLocationProduct
					.getStrRetailerLocationProductJson();
			ArrayList<RetailerLocationProductJson> locationJsonList = Utility
					.jsonToProdInfoList(strProductJson);
			response = retailerDAO.UpdateRetailLocationProduct(
					locationJsonList, objRetLocationProduct.getRetailID());

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  updateRetailerProductList : "
					+ e);
			throw new ScanSeeServiceException(e);
		}
		return response;

	}

	/**
	 * This serviceImpl method to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param strSortOrder
	 *            as request parameter.
	 * @param strPageID
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String saveReorderList(String strSortOrder, String strPageID,
			int retailerId) throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  saveReorderList ");
		String response = null;
		try {
			response = retailerDAO.saveReorderList(strSortOrder, strPageID,
					retailerId);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  saveReorderList : " + e);
			throw new ScanSeeServiceException(e.getCause());
		}
		return response;
	}

	public RetailerRegistration registerFreeAppListRetailer(
			RetailerRegistration objRetailerRegistration, String strLogoImage)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : registerFreeAppListRetailer ");

//		double latLng = 0.0;
		EncryptDecryptPwd enryptDecryptpwd;
		String enryptPassword = null;
		String autogenPassword = null;
		String autogenUserName = "";
		String[] userNameArr = null;
		RetailerRegistration objRetailRegResponse = null;
//		String response = null;
		try {
			/*
			 * String strAddress =
			 * Utility.checkNull(objRetailerRegistration.getAddress1().trim()) +
			 * ", " + objRetailerRegistration.getState() + ", " +
			 * objRetailerRegistration.getCity() + ", " +
			 * objRetailerRegistration.getPostalCode();
			 */

			String strAddress = Utility.checkNull(objRetailerRegistration
					.getAddress1().trim())
					+ commas
					+ objRetailerRegistration.getCity()
					+ commas
					+ objRetailerRegistration.getState();

			/*
			 * GAddress objGAddress = Utility.geocode(strAddress); // If user
			 * provide valid address or zipcode. if
			 * (!"".equals(Utility.checkNull(objGAddress))) {
			 * objRetailerRegistration
			 * .setRetailerLocationLatitude(objGAddress.getLat());
			 * objRetailerRegistration
			 * .setRetailerLocationLongitude(objGAddress.getLng()); } else { //
			 * If user provide invalid address or zipcode for Geocode // Address
			 * (Lat/Lng) is null and default value is initialized to // 0.0 .
			 * objRetailerRegistration.setRetailerLocationLatitude(latLng);
			 * objRetailerRegistration.setRetailerLocationLongitude(latLng); }
			 */

			final GAddress objGAddress = UtilCode.getGeoDetails(strAddress);

			if (!"".equals(Utility.checkNull(objGAddress))) {
				if ("OK".equals(objGAddress.getStatus())
						&& "ROOFTOP".equals(objGAddress.getLocationType())) {
					objRetailerRegistration
							.setRetailerLocationLongitude(objGAddress.getLng());
					objRetailerRegistration
							.setRetailerLocationLatitude(objGAddress.getLat());
					objRetailerRegistration.setGeoErr(false);

				} else if (!objRetailerRegistration.isGeoErr()) {
					objRetailRegResponse = new RetailerRegistration();
					objRetailRegResponse.setGeoErr(objRetailerRegistration
							.isGeoErr());
					objRetailRegResponse
							.setRetailerLocationLongitude(objRetailerRegistration
									.getRetailerLocationLatitude());
					objRetailRegResponse
							.setRetailerLocationLatitude(objRetailerRegistration
									.getRetailerLocationLongitude());
					objRetailRegResponse
							.setResponse(ApplicationConstants.GEOERROR);
					return objRetailRegResponse;
				} else if (objRetailerRegistration.isGeoErr()) {

					if (null == objRetailerRegistration
							.getRetailerLocationLatitude()
							&& null == objRetailerRegistration
									.getRetailerLocationLongitude()) {
						objRetailRegResponse = new RetailerRegistration();
						objRetailRegResponse.setGeoErr(objRetailerRegistration
								.isGeoErr());

						objRetailRegResponse
								.setRetailerLocationLongitude(objRetailerRegistration
										.getRetailerLocationLatitude());
						objRetailRegResponse
								.setRetailerLocationLatitude(objRetailerRegistration
										.getRetailerLocationLongitude());

						objRetailRegResponse
								.setResponse(ApplicationConstants.GEOERROR);
						return objRetailRegResponse;

					}

				}

			}

			/*
			 * // If user provide valid address or zipcode. if
			 * (!"".equals(Utility.checkNull(objGAddress))) { if
			 * ("OK".equals(objGAddress.getStatus()) &&
			 * "ROOFTOP".equals(objGAddress.getLocationType())) {
			 * objRetailerRegistration
			 * .setRetailerLocationLongitude(objGAddress.getLng());
			 * objRetailerRegistration
			 * .setRetailerLocationLatitude(objGAddress.getLat()); } else {
			 * LOG.error("Could not find Lat and Lang for the address:" +
			 * strAddress); objRetailRegResponse = new RetailerRegistration();
			 * objRetailRegResponse.setResponse(ApplicationConstants.GEOERROR);
			 * return objRetailRegResponse; } } else { // If user provide
			 * invalid address or zipcode for Geocode // Address (Lat/Lng) is
			 * null and default value is initialized to // 0.0 .
			 * objRetailerRegistration.setRetailerLocationLongitude(latLng);
			 * objRetailerRegistration.setRetailerLocationLatitude(latLng); }
			 */

			if (null != objRetailerRegistration.getRetailerName()) {

				userNameArr = objRetailerRegistration.getRetailerName().split(
						" ");

				if (userNameArr.length >= 3) {

					if (Character.isDigit(userNameArr[0].charAt(0))
							|| Character.isLetter(userNameArr[0].charAt(0))) {

						autogenUserName = Character.toString(
								userNameArr[0].charAt(0)).toLowerCase();

					}

					if (Character.isDigit(userNameArr[1].charAt(0))
							|| Character.isLetter(userNameArr[1].charAt(0))) {

						autogenUserName = autogenUserName
								+ Character.toString(userNameArr[1].charAt(0))
										.toLowerCase();

					}
					autogenUserName = autogenUserName
							+ userNameArr[2].toLowerCase().replaceAll(
									"[^a-z0-9]+", "");
					autogenUserName = autogenUserName
							+ Utility.randomString(3).toLowerCase();
				} else if (userNameArr.length == 2) {
					if (Character.isDigit(userNameArr[0].charAt(0))
							|| Character.isLetter(userNameArr[0].charAt(0))) {

						autogenUserName = Character.toString(
								userNameArr[0].charAt(0)).toLowerCase();

					}
					autogenUserName = autogenUserName
							+ userNameArr[1].toLowerCase().replaceAll(
									"[^a-z0-9]+", "");
					autogenUserName = autogenUserName
							+ Utility.randomString(3).toLowerCase();

				} else if (userNameArr.length == 1) {
					autogenUserName = userNameArr[0].toLowerCase().replaceAll(
							"[^a-z0-9]+", "");
					autogenUserName = autogenUserName
							+ Utility.randomString(3).toLowerCase();
				}

			}

			enryptDecryptpwd = new EncryptDecryptPwd();
			autogenPassword = Utility.randomString(5);
			enryptPassword = enryptDecryptpwd.encrypt(autogenPassword);
			objRetailerRegistration.setPassword(enryptPassword);
			objRetailerRegistration.setUserName(autogenUserName);
			objRetailRegResponse = retailerDAO
					.saveFreeAppSiteRetailer(objRetailerRegistration);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : registerFreeAppListRetailer :"
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getMessage());
		} catch (IllegalBlockSizeException exception) {
			LOG.error("Inside RetailerServiceImpl : registerFreeAppListRetailer :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		} catch (InvalidKeyException exception) {
			LOG.error("Inside RetailerServiceImpl : registerFreeAppListRetailer :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		} catch (InvalidAlgorithmParameterException exception) {
			LOG.error("Inside RetailerServiceImpl : registerFreeAppListRetailer :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		} catch (InvalidKeySpecException exception) {
			LOG.error("Inside RetailerServiceImpl : registerFreeAppListRetailer :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		} catch (NoSuchAlgorithmException exception) {
			LOG.error("Inside RetailerServiceImpl : registerFreeAppListRetailer :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		} catch (NoSuchPaddingException exception) {
			LOG.error("Inside RetailerServiceImpl : registerFreeAppListRetailer :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		} catch (BadPaddingException exception) {
			LOG.error("Inside RetailerServiceImpl : registerFreeAppListRetailer :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		} catch (Exception exception) {
			LOG.error("Inside RetailerDAOImpl : registerFreeAppListRetailer :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		}
		return objRetailRegResponse;
	}

	public List<RetailerLocation> saveAppListLocation(
			RetailerLocation retailerLocation) throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  saveAppListLocation  : ");
		
		String response = null;
		String strAddress = null;
		
		List<RetailerLocation> locationList = null;

		try {
			String prodJson = retailerLocation.getProdJson();
			locationList = Utility.jsonToObjectList(prodJson);

			for (int i = 0; i < locationList.size(); i++) {
				RetailerLocation objRetailerLocation1 = locationList.get(i);

				strAddress = Utility.checkNull(objRetailerLocation1
						.getAddress1().trim())
						+ commas
						+ objRetailerLocation1.getCity()
						+ commas
						+ objRetailerLocation1.getState();

				/*
				 * strAddress =
				 * objRetailerLocation1.getAddress1().trim().toUpperCase() +
				 * ", " + objRetailerLocation1.getState().toUpperCase() + ", " +
				 * objRetailerLocation1.getCity().toUpperCase() + ", " +
				 * objRetailerLocation1.getPostalCode();
				 */

				if (!"".equals(Utility.checkNull(strAddress))) {

					final GAddress objGAddress = UtilCode
							.getGeoDetails(strAddress);
					if (!"".equals(Utility.checkNull(objGAddress))) {
						if ("OK".equals(objGAddress.getStatus())
								&& "ROOFTOP".equals(objGAddress
										.getLocationType())) {
							locationList.get(i).setRetailerLocationLongitude(
									objGAddress.getLng());
							locationList.get(i).setRetailerLocationLatitude(
									objGAddress.getLat());
						} else {

							if (null == locationList.get(i)
									.getRetailerLocationLatitude()
									&& null == locationList.get(i)
											.getRetailerLocationLongitude()) {
								locationList.get(0).setGeoError(true);

								if ("".equals(Utility.checkNull(locationList
										.get(0).getHighliteRow()))) {

									locationList.get(0).setHighliteRow(
											String.valueOf(i));
								} else {

									locationList.get(0).setHighliteRow(
											locationList.get(0)
													.getHighliteRow()
													+ ","
													+ String.valueOf(i));

								}

							}

						}

					}

					/*
					 * GAddress objGAddress = Utility.geocode(strAddress); // If
					 * user provide valid address or zipcode. if
					 * (!"".equals(Utility.checkNull(objGAddress))) {
					 * locationList
					 * .get(i).setRetailerLocationLongitude(objGAddress
					 * .getLng());
					 * locationList.get(i).setRetailerLocationLatitude
					 * (objGAddress.getLat()); } // If user provide invalid
					 * address or // zipcode for Geocode Address (Lat/Lng) is //
					 * null and default value is initialized to // 0.0 . if
					 * ("".equals(Utility.checkNull(objGAddress))) {
					 * locationList
					 * .get(i).setRetailerLocationLongitude(dLatLng);
					 * locationList.get(i).setRetailerLocationLatitude(dLatLng);
					 * }
					 */
				}

			}

			if (locationList.get(0).isGeoError()) {

				return locationList;
			} else {
				response = retailerDAO.saveAppListLocationList(locationList);
				locationList.get(0).setGeoError(false);
			}

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  saveAppListLocation : " + e);
			e.printStackTrace();
			throw new ScanSeeServiceException(e.getCause());
		} catch (Exception e) {
			LOG.info("Inside RetailerServiceImpl :  saveAppListLocation : " + e);
			throw new ScanSeeServiceException(e.getCause());
		}
		LOG.info("Exit retailerServiceImpl :  saveAppListLocation  : ");
		return locationList;
	}

	public String createAppListingAboutUsPage(
			RetailerCustomPage retailerCustomPage)
			throws ScanSeeServiceException {
		LOG.info("Inside the method:createAppListingAboutUsPage");
		String response = ApplicationConstants.FAILURE;
		int pageID = 0;

		try {
			String fileSeparator = System.getProperty("file.separator");
			String aboutUsIconPath = Utility.getMediaPath()
					+ ApplicationConstants.RETAILER + fileSeparator
					+ ApplicationConstants.ABOUTUSICON;
			InputStream inputStream = new BufferedInputStream(
					new FileInputStream(aboutUsIconPath));
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER, Integer.valueOf(String
							.valueOf(retailerCustomPage.getRetailerId())));

			Utility.writeFileData(inputStream, mediaPathBuilder.toString()
					+ fileSeparator + ApplicationConstants.ABOUTUSICON);
			retailerCustomPage.setRetailerImg(ApplicationConstants.ABOUTUSICON);
			retailerCustomPage
					.setRetPageTitle(ApplicationConstants.ABOUTUSTEXT);
			pageID = retailerDAO
					.insertAppListingAboutUsPageInfo(retailerCustomPage);
			if (pageID != 0) {

				response = ApplicationConstants.SUCCESS;
			}

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  createAppListingAboutUsPage : "
					+ e);
			throw new ScanSeeServiceException(e.getCause());
		} catch (FileNotFoundException e) {
			LOG.info("Inside RetailerServiceImpl :  createAppListingAboutUsPage : "
					+ e);
			throw new ScanSeeServiceException(e.getCause());
		}
		LOG.info("Exit method:createAppListingAboutUsPage");
		return response;

	}

	public List<RetailerLocation> retrievRetailLocation(int retailID)
			throws ScanSeeServiceException {
		LOG.info("Inside the method:retrievRetailLocation");
		List<RetailerLocation> retailerLocations = null;
		try {
			retailerLocations = retailerDAO.retrievRetailLocation(retailID);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  retrievRetailLocation : "
					+ e);
			throw new ScanSeeServiceException(e.getCause());
		}

		LOG.info("Exit method:retrievRetailLocation");
		return retailerLocations;
	}

	/**
	 * This serviceImpl method display the product details based on input
	 * parameter(productId) by calling DAO method.
	 * 
	 * @param objRetLtnProduct
	 *            instance of RetailerLocationProduct.
	 * @return productList, one the products details.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final List<RetailerLocationProduct> getRetailerProductPreview(
			RetailerLocationProduct objRetLtnProduct)
			throws ScanSeeServiceException {
		LOG.info("Inside SupplierServiceImpl :  getRetailerProductPreview ");
		List<RetailerLocationProduct> prodList = null;
		try {
			prodList = retailerDAO.getRetailerProductPreview(objRetLtnProduct);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return prodList;
	}

	public RetailerRegistration finalizeFreeAppSiteRetReg(
			RetailerRegistration objRetailerRegistration, String strLogoImage)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : finalizeFreeAppSiteRetReg ");
		EncryptDecryptPwd enryptDecryptpwd;
		String enryptPassword = null;
		String autogenPassword = null;
		String autogenUserName = "";
		String[] userNameArr = null;
//		String response = null;
		String smtpHost = null;
		String smtpPort = null;
//		double latLng = 0.0;
		String strResponse = null;
		String strAdminEmailId = null;
		String appSiteEmailId = null;
		RetailerRegistration objRetailRegResponse = null;
		try {
			if (null != objRetailerRegistration.getRetailerName()) {

				userNameArr = objRetailerRegistration.getRetailerName().split(
						" ");

				if (userNameArr.length >= 3) {

					if (Character.isDigit(userNameArr[0].charAt(0))
							|| Character.isLetter(userNameArr[0].charAt(0))) {

						autogenUserName = Character.toString(
								userNameArr[0].charAt(0)).toLowerCase();

					}

					if (Character.isDigit(userNameArr[1].charAt(0))
							|| Character.isLetter(userNameArr[1].charAt(0))) {

						autogenUserName = autogenUserName
								+ Character.toString(userNameArr[1].charAt(0))
										.toLowerCase();

					}
					autogenUserName = autogenUserName
							+ userNameArr[2].toLowerCase().replaceAll(
									"[^a-z0-9]+", "");
					autogenUserName = autogenUserName
							+ Utility.randomString(3).toLowerCase();
				} else if (userNameArr.length == 2) {
					if (Character.isDigit(userNameArr[0].charAt(0))
							|| Character.isLetter(userNameArr[0].charAt(0))) {

						autogenUserName = Character.toString(
								userNameArr[0].charAt(0)).toLowerCase();

					}
					autogenUserName = autogenUserName
							+ userNameArr[1].toLowerCase().replaceAll(
									"[^a-z0-9]+", "");
					autogenUserName = autogenUserName
							+ Utility.randomString(3).toLowerCase();

				} else if (userNameArr.length == 1) {
					autogenUserName = userNameArr[0].toLowerCase().replaceAll(
							"[^a-z0-9]+", "");
					autogenUserName = autogenUserName
							+ Utility.randomString(3).toLowerCase();
				}

			}

			enryptDecryptpwd = new EncryptDecryptPwd();
			autogenPassword = Utility.randomString(5);
			enryptPassword = enryptDecryptpwd.encrypt(autogenPassword);
			objRetailerRegistration.setPassword(enryptPassword);
			objRetailerRegistration.setUserName(autogenUserName);
			objRetailRegResponse = retailerDAO
					.finalizeFreeAppSiteRetReg(objRetailerRegistration);

			if (objRetailRegResponse.getResponse() != null
					&& objRetailRegResponse.getResponse().equals(
							ApplicationConstants.SUCCESS)) {

				objRetailerRegistration.setPassword(autogenPassword);

				ArrayList<AppConfiguration> emailConf = retailerDAO
						.getAppConfig(ApplicationConstants.EMAILCONFIG);
				for (int j = 0; j < emailConf.size(); j++) {
					if (emailConf.get(j).getScreenName()
							.equals(ApplicationConstants.SMTPHOST)) {
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName()
							.equals(ApplicationConstants.SMTPPORT)) {
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}
				ArrayList<AppConfiguration> adminEmailList = retailerDAO
						.getAppConfig(ApplicationConstants.WEBREGISTRATION);
				for (int j = 0; j < adminEmailList.size(); j++) {
					if (adminEmailList.get(j).getScreenName()
							.equals(ApplicationConstants.ADMINEMAILID)) {
						strAdminEmailId = adminEmailList.get(j)
								.getScreenContent();
					}
				}

				ArrayList<AppConfiguration> appSiteEmailList = retailerDAO
						.getAppConfig(ApplicationConstants.APPSITEEMAILID);
				for (int j = 0; j < appSiteEmailList.size(); j++) {
					if (appSiteEmailList.get(j).getScreenName()
							.equals(ApplicationConstants.APPSITEEMAILID)) {
						appSiteEmailId = appSiteEmailList.get(j)
								.getScreenContent();
					}
				}

				ArrayList<AppConfiguration> list = retailerDAO
						.getAppConfig(ApplicationConstants.FACEBOOKCONFG);
				for (int j = 0; j < list.size(); j++) {
					if (list.get(j).getScreenName()
							.equals(ApplicationConstants.SCANSEEBASEURL)) {

						objRetailerRegistration.setScanSeeUrl(list.get(j)
								.getScreenContent());

					}
				}
				strResponse = Utility.sendFreeAppRetailerRegMail(
						objRetailerRegistration, smtpHost, smtpPort,
						strAdminEmailId, strLogoImage, appSiteEmailId);
				if (strResponse != null
						&& strResponse.equals(ApplicationConstants.SUCCESS)) {
					objRetailRegResponse
							.setResponse(ApplicationConstants.SUCCESS);
				} else {
					objRetailRegResponse
							.setResponse(ApplicationConstants.FAILURE);

				}

			}
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : finalizeFreeAppSiteRetReg :"
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getMessage());
		}

		catch (Exception exception) {
			LOG.error("Inside RetailerServiceImpl : finalizeFreeAppSiteRetReg :"
					+ exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(),
					exception.getCause());
		}
		return objRetailRegResponse;
	}

	public String validateDuplicateRetailer(Long retailId,
			int duplicateRetailId, int duplicateRetailLocId)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : validateDuplicateRetailer");
		String response = null;
		try {
			response = retailerDAO.validateDuplicateRetailer(retailId,
					duplicateRetailId, duplicateRetailLocId);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : finalizeFreeAppSiteRetReg :"
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info("Exit RetailerServiceImpl : validateDuplicateRetailer");
		return response;
	}

	public String markAsDuplicateRetailer(Long retailId, Long retailLocId)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : markAsDuplicateRetailer");
		String response = null;
		try {
			response = retailerDAO.markAsDuplicateRetailer(retailId,
					retailLocId);

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : markAsDuplicateRetailer :"
					+ e.getMessage());
			throw new ScanSeeServiceException(e.getMessage());
		}
		LOG.info("Exit RetailerServiceImpl : markAsDuplicateRetailer");
		return response;
	}

	public ArrayList<DropDown> displaySupplierRetailerDemographicsDD()
			throws ScanSeeServiceException {
		ArrayList<DropDown> dropDowns = null;
		try {
			dropDowns = (ArrayList<DropDown>) retailerDAO
					.displaySupplierRetailerDemographicsDD();
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside SupplierServiceImpl :  displayDropDown : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Existing SupplierServiceImpl : displayDropDown methos");
		return dropDowns;
	}

	/**
	 * This serviceImpl method save Giveaway page information.
	 * 
	 * @param retailerCustomPage
	 *            as a parameter.
	 * @return QR code image path.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String createGiveAwayPage(RetailerCustomPage retailerCustomPage)
			throws ScanSeeServiceException {
		final String methodName = "createGiveAwayPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = ApplicationConstants.FAILURE;
		int pageID = 0;
		String retCretaedPageUrl = null;
		String fileSeparator = System.getProperty("file.separator");
		QRCodeResponse qrCodeResponse = null;
		try {

			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					retailerCustomPage.getRetailerId());
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);

			if (null != retailerCustomPage.getRetailerImg()
					&& !(retailerCustomPage.getRetailerImg().equals(""))) {
				InputStream inputStream = new BufferedInputStream(
						new FileInputStream(retTempMediaPath + fileSeparator
								+ retailerCustomPage.getRetailerImg()));
				if (null != inputStream) {
					Utility.writeFileData(
							inputStream,
							retMediaPath + fileSeparator
									+ retailerCustomPage.getRetailerImg());

				}

			}

			pageID = retailerDAO.saveGiveAwayPageInfo(retailerCustomPage);

			if (pageID != 0) {
				ArrayList<AppConfiguration> appConfiguration = retailerDAO
						.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);
				for (int j = 0; j < appConfiguration.size(); j++) {

					retCretaedPageUrl = appConfiguration.get(j)
							.getScreenContent();

				}

				retCretaedPageUrl = retCretaedPageUrl
						+ ApplicationConstants.GIVEAWAYPAGEURL + "key1="
						+ retailerCustomPage.getRetailerId()
						+ ApplicationConstants.AMPERSAND + "key2=" + pageID;
				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(pageID), retCretaedPageUrl);

				if (qrCodeResponse.getResponse().equals(
						ApplicationConstants.SUCCESS)) {
					response = "/" + ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.QRPATH + "/"
							+ String.valueOf(pageID)
							+ ApplicationConstants.PNGIMAGEFORMAT;
				} else {
					response = ApplicationConstants.FAILURE;
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  createGiveAwayPage : " + e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.info("Inside RetailerServiceImpl :  createGiveAwayPage : " + e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This service method to get Giveaway page information.
	 * 
	 * @param retailerId
	 *            as a parameter.
	 * @param pageId
	 *            as a parameter.
	 * @return RetailerCustomPage obj.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public RetailerCustomPage getGiveAwayPageInfo(Long retailerId, Long pageId)
			throws ScanSeeServiceException {

		final String methodName = "getGiveAwayPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerCustomPage retailerCustomPage = null;

		try {
			retailerCustomPage = retailerDAO.getGiveAwayPageInfo(retailerId,
					pageId);

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  getGiveAwayPageInfo : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerCustomPage;
	}

	/**
	 * This service method to Update Giveaway page information.
	 * 
	 * @param retailerCustomPage
	 *            as a parameter.
	 * @return QR code image path.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String updateGiveawayPageInfo(RetailerCustomPage retailerCustomPage)
			throws ScanSeeServiceException {
		final String methodName = "updateGiveawayPageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = ApplicationConstants.FAILURE;
		String fileSeparator = System.getProperty("file.separator");
		String retCretaedPageUrl = null;
		QRCodeResponse qrCodeResponse = null;
		Date date = new Date();
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(
					ApplicationConstants.RETAILER,
					retailerCustomPage.getRetailerId());
			StringBuilder mediaTempPathBuilder = Utility
					.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			LOG.info(" Images path ********************" + retMediaPath);

			if ((null != retailerCustomPage.getRetailerImg() && !(retailerCustomPage
					.getRetailerImg().equals("")))
					&& null != retailerCustomPage.getImageName()) {
				if (!(retailerCustomPage.getRetailerImg()
						.equals(retailerCustomPage.getImageName()))) {
					InputStream inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ retailerCustomPage.getRetailerImg()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream,
								retMediaPath + fileSeparator
										+ retailerCustomPage.getRetailerImg());

					}
				}
			}

			response = retailerDAO.updateGiveawayPageInfo(retailerCustomPage);
			if (response != null
					&& !response.equals(ApplicationConstants.FAILURE)) {
				ArrayList<AppConfiguration> appConfiguration = retailerDAO
						.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);
				for (int j = 0; j < appConfiguration.size(); j++) {
					retCretaedPageUrl = appConfiguration.get(j)
							.getScreenContent();
				}

				retCretaedPageUrl = retCretaedPageUrl
						+ ApplicationConstants.GIVEAWAYPAGEURL + "key1="
						+ retailerCustomPage.getRetailerId()
						+ ApplicationConstants.AMPERSAND + "key2="
						+ retailerCustomPage.getPageId();

				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(retailerCustomPage.getPageId()),
						retCretaedPageUrl);

				if (qrCodeResponse.getResponse().equals(
						ApplicationConstants.SUCCESS)) {
					response = "/" + ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.QRPATH + "/"
							+ String.valueOf(retailerCustomPage.getPageId())
							+ ApplicationConstants.PNGIMAGEFORMAT + "?"
							+ date.getTime();
				} else {
					response = ApplicationConstants.FAILURE;
				}
			} else {
				response = ApplicationConstants.FAILURE;
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  updateGiveawayPageInfo : "
					+ e);
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.info("Inside RetailerServiceImpl :  updateGiveawayPageInfo : "
					+ e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This serviceImpl method is used to display the list of give away
	 * promotion pages.
	 * 
	 * @param retailerId
	 * @param searchKey
	 * @param currentPage
	 * @param recordCount
	 * @return SearchResultInfo
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public SearchResultInfo getGiveAwayList(int retailerId, String searchKey,
			int currentPage, int recordCount) throws ScanSeeServiceException {
		final String methodName = "getGiveAwayList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo customPageList = null;
		try {
			customPageList = retailerDAO.getGiveAwayList(retailerId, searchKey,
					currentPage, recordCount);
		} catch (ScanSeeWebSqlException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return customPageList;
	}

	/**
	 * This serviceImpl method is used for deleting give away page.
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String deleteGiveAwayPage(Long pageId, int retailerId)
			throws ScanSeeServiceException {
		final String methodName = "deleteGiveAwayPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try {

			response = retailerDAO.deleteGiveAwayPage(pageId, retailerId);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  deleteGiveAwayPage : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This service method is used for deleting Giveaway page.
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return String
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public RetailerCustomPage getGAQrCodeDetails(Long pageId, int retailerId)
			throws ScanSeeServiceException {
		final String methodName = "getGAQrCodeDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerCustomPage pageDetails = null;
		QRCodeResponse qrCodeResponse = null;
		try {

			pageDetails = retailerDAO.getQRcodeForGAPage(pageId, retailerId);
			if (pageDetails != null) {
				qrCodeResponse = QRCodeGenerator.generateQRCode(
						String.valueOf(pageId), pageDetails.getUrl());
				if (qrCodeResponse.getResponse().equals(
						ApplicationConstants.SUCCESS)) {
					final String tempQrPath = "/" + ApplicationConstants.IMAGES
							+ "/" + ApplicationConstants.QRPATH + "/"
							+ String.valueOf(pageId)
							+ ApplicationConstants.PNGIMAGEFORMAT;
					pageDetails.setQrImagePath(tempQrPath);
				} else {
					throw new ScanSeeServiceException(
							"::::Error occured while creating QR code::::");
				}
			} else {
				LOG.info("Inside RetailerServiceImpl");
				throw new ScanSeeServiceException(
						"Page details not found for the Page id");
			}

		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  getGAQrCodeDetails : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageDetails;

	}

	public List<Product> getPdtInfoForDealCoupon(String productIds,
			int retailerId) {
		List<Product> productList = null;
		try {
			productList = retailerDAO.getPdtInfoForDealCoupon(productIds,
					retailerId);
		} catch (ScanSeeWebSqlException e) {
			e.printStackTrace();
		}
		return productList;
	}

	/**
	 * The serviceImpl method for displaying list of Hot deals by calling DAO.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return Hot deal Names,List of Hot deal Names.
	 */
	public final ArrayList<HotDealInfo> getAllHotDealsName(Long userId)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  getAllHotDealsName ");
		ArrayList<HotDealInfo> arHotDealInfoList = null;
		try {
			arHotDealInfoList = retailerDAO.getAllHotDealsName(userId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arHotDealInfoList;
	}

	/**
	 * This service method is used to get Hot dealView Report details.
	 * 
	 * @param hotDealId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return objHotDeal instance of HotDealInfo.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final HotDealInfo getHotDealViewReport(int hotDealId,
			Long retailerId, int currentPage, int recordCount)
			throws ScanSeeServiceException {
		HotDealInfo objHotDealInfo = null;
		try {
			objHotDealInfo = retailerDAO.getHotDealViewReport(hotDealId,
					retailerId, currentPage, recordCount);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return objHotDealInfo;
	}

	public RetailerRegistration findDuplicateRetailer(
			RetailerRegistration objRetRegistration)
			throws ScanSeeServiceException {
		LOG.info("Inside findDuplicateRetailer method ");
		// RetailerRegistration objRetailRegResponse = null;
//		String result = null;
		final double latLng = 0.0;
		try {
			if ("".equals(Utility.checkNull(objRetRegistration.getGeoError()))) {
				String strAddress = Utility.checkNull(objRetRegistration
						.getAddress1().trim())
						+ commas
						+ objRetRegistration.getCity()
						+ commas
						+ objRetRegistration.getState();
				/*
				 * final GAddress objGAddress = Utility.geocode(strAddress); //
				 * If user provide valid address or zipcode. if
				 * (!"".equals(Utility.checkNull(objGAddress))) {
				 * objRetailerRegistration
				 * .setRetailerLocationLatitude(objGAddress.getLat());
				 * objRetailerRegistration
				 * .setRetailerLocationLongitude(objGAddress.getLng()); } else {
				 * // If user provide invalid address or zipcode for Geocode //
				 * Address (Lat/Lng) is null and default value is initialized to
				 * // 0.0 .
				 * objRetailerRegistration.setRetailerLocationLatitude(latLng);
				 * objRetailerRegistration.setRetailerLocationLongitude(latLng);
				 * }
				 */
				final GAddress objGAddress = UtilCode.getGeoDetails(strAddress);
				// If user provide valid address or zipcode.
				if (!"".equals(Utility.checkNull(objGAddress))) {
					if ("OK".equals(objGAddress.getStatus())
							&& "ROOFTOP".equals(objGAddress.getLocationType())) {
						objRetRegistration
								.setRetailerLocationLongitude(objGAddress
										.getLng());
						objRetRegistration
								.setRetailerLocationLatitude(objGAddress
										.getLat());
					} else {
						LOG.error("Could not find Lat and Lang for the address:"
								+ strAddress);
						objRetRegistration
								.setResponse(ApplicationConstants.GEOERROR);
						return objRetRegistration;
					}
				} else {
					// If user provide invalid address or zipcode for Geocode
					// Address (Lat/Lng) is null and default value is
					// initialized to
					// 0.0 .
					objRetRegistration.setRetailerLocationLongitude(latLng);
					objRetRegistration.setRetailerLocationLatitude(latLng);
				}
			}
			objRetRegistration = retailerDAO
					.findDuplicateRetailer(objRetRegistration);

		} catch (ScanSeeWebSqlException e) {
			LOG.error("Exception occured inside the method:findDuplicateRetailer"
					+ e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit findDuplicateRetailer method ");
		return objRetRegistration;
	}

	/**
	 * This service method will return the Retailer Location List based on input
	 * parameter.
	 * 
	 * @param pageId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public List<RetailerLocation> getLocationIDForAnythingPage(Long pageId,
			int retailerId) throws ScanSeeServiceException {
		String methodName = "getLocationIDForAnythingPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<RetailerLocation> pageLocationList = null;
		try {
			pageLocationList = retailerDAO.getLocationIDForAnythingPage(pageId,
					retailerId);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl : getLocationIDForWelcomePage : "
					+ e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		return pageLocationList;
	}
	
	
	/**
	 * This serviceImpl method to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param strSortOrder
	 *            as request parameter.
	 * @param strPageID
	 *            as request parameter.
	 * @param iRetailId
	 *            as request parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final String saveReorderSpecialOfferList(String strSortOrder, String strPageID, int iRetailId) throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  saveReorderSpecialOfferList ");
		String strResponse = null;
		try {
			strResponse = retailerDAO.saveReorderSpecialOfferList(strSortOrder, strPageID, iRetailId);
		} catch (ScanSeeWebSqlException e) {
			LOG.info("Inside RetailerServiceImpl :  saveReorderSpecialOfferList : " + e);
			throw new ScanSeeServiceException(e.getCause());
		}
		return strResponse;
	}
	
	
	
	/**
	 * The serviceImpl method for displaying all the Event categories and it will
	 * calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return categories,List of event categories.
	 */
	public final ArrayList<Category> getAllEventCategory() throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl : getAllEventCategory ");
		
		ArrayList<Category> arEventCatList = null;
		
		try {
			arEventCatList = retailerDAO.getAllEventCategory();
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		
		return arEventCatList;
	}
	
	/**
	 * This serviceImpl method will return list of Retailer Locations/fetch the
	 * retailer location based on input parameter by calling DAO methods.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @return list of Retailer Locations.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public ArrayList<RetailerLocation> getEventRetailerLocationList(int retailerId) throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl : getAllEventCategory ");

		ArrayList<RetailerLocation> arEventRetLocationList = null;

		try {
			arEventRetLocationList = retailerDAO.getEventRetailerLocationList(retailerId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arEventRetLocationList;
	}
	
	
	/**
	 * This serviceImpl method will create/Update Event page.
	 * 
	 * @param event
	 *            instance of Event
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String addUpdateEventDetail(Event objEvent) throws ScanSeeServiceException {

		LOG.info("Inside retailerServiceImpl : addEventDetail ");
		
		String response = null;
		InputStream inputStream = null;
		String strAddress = null;
		final String fileSeparator = System.getProperty("file.separator");
		
		try {
			
			// If user provide invalid address or zipcode.
			if (null != objEvent.getBsnsLoc() && "no".equals(objEvent.getBsnsLoc())) {
				strAddress = objEvent.getAddress().trim() + commas + objEvent.getCity() + commas + objEvent.getState();
				
				final GAddress objGAddress = UtilCode.getGeoDetails(strAddress);
				if (!"".equals(Utility.checkNull(objGAddress))) {
					if ("OK".equals(objGAddress.getStatus()) && "ROOFTOP".equals(objGAddress.getLocationType())) {
						objEvent.setLatitude(objGAddress.getLat());
						objEvent.setLongitude(objGAddress.getLng());
						objEvent.setGeoError(false);
					} else
						if (!objEvent.isGeoError()) {
							response = "GEOERROR";
							return response;
						} else
							if (objEvent.isGeoError() && null == objEvent.getLatitude() && null == objEvent.getLongitude()) {
								response = "GEOERROR";
								return response;

							}
				}
			}

			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.RETAILER, objEvent.getRetailID());
			StringBuilder mediaTempPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			
			if (!"".equals(Utility.checkNull(objEvent.getEventImageName()))) {
				/* Event Images are upload to server if Upload image bit is true. When user edit the images*/
				if (objEvent.isUploadedImage()) {
					inputStream = new BufferedInputStream(
							new FileInputStream(retTempMediaPath
									+ fileSeparator
									+ objEvent.getEventImageName()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream, retMediaPath
								+ fileSeparator + objEvent.getEventImageName());
					}
				}
			}
			
			response = retailerDAO.addUpdateEventDetail(objEvent);
			
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside retailerServiceImpl : addEventDetail : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error("Inside retailerServiceImpl : addEventDetail : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}
	
	
	/**
	 * This serviceImpl method to get Retailer event page information.
	 * 
	 * @param eventId as a parameter.
	 * @return Event detail.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @throws ParseException 
	 */
	public Event getEventDetail(Integer eventId) throws ScanSeeServiceException, ParseException {
		LOG.info("Inside retailerServiceImpl : getEventDetail ");
		
		Event objEventDetail = null;
		try {
			
			objEventDetail = retailerDAO.getEventDetail(eventId);
			
			if (null != objEventDetail) {

				objEventDetail.setHiddenCategory(objEventDetail.getEventCategory());

				if (null == objEventDetail.getIsOngoing() || "0".equalsIgnoreCase(objEventDetail.getIsOngoing())) {
					objEventDetail.setIsOngoing("no");

					Format formatter = new SimpleDateFormat("MM/dd/yyyy");
					Date date = new Date();

					if (null != objEventDetail.getEventStartDate() && !"".equals(objEventDetail.getEventStartDate())) {
						String strtDate = Utility.formattedDate(objEventDetail.getEventStartDate());
						objEventDetail.setEventDate(strtDate);
						date = new Date(strtDate);
						objEventDetail.setEventStartDate(strtDate);
					} else {
						objEventDetail.setEventStartDate(formatter.format(date));
					}

					if (null != objEventDetail.getEventStartTime() && !"".equals(objEventDetail.getEventStartTime())) {
						String eventTime = objEventDetail.getEventStartTime();
						String[] tempTime = eventTime.split(":");
						objEventDetail.setEventTimeHrs(tempTime[0]);
						objEventDetail.setEventTimeMins(tempTime[1]);
					}

					if (null != objEventDetail.getEventEndDate() && !"".equals(objEventDetail.getEventEndDate())) {
						String endDate = Utility.formattedDate(objEventDetail.getEventEndDate());
						objEventDetail.setEventEDate(endDate);
						objEventDetail.setEventEndDate(endDate);
					} else {
						objEventDetail.setEventEndDate(formatter.format(date));
					}
					
					if (null != objEventDetail.getEventEndTime() && !"".equals(objEventDetail.getEventEndTime())) {
						String eventTime = objEventDetail.getEventEndTime();
						String[] tempTime = eventTime.split(":");
						objEventDetail.setEventETimeHrs(tempTime[0]);
						objEventDetail.setEventETimeMins(tempTime[1]);
					}

					// Daily Recurrence
					objEventDetail.setIsOngoingDaily("days");
					objEventDetail.setEveryWeekDay(1);
					
					// Weekly Recurrence
					objEventDetail.setEveryWeek(1);
					objEventDetail.setIsOngoingMonthly("date");
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date);
					Integer day = calendar.get(Calendar.DAY_OF_WEEK);
					objEventDetail.setDays(new String[] { day.toString() });
					
					// Monthly Recurrence
					objEventDetail.setDateOfMonth(calendar.get(Calendar.DATE));
					objEventDetail.setEveryMonth(1);
					Calendar tempCalendar = Calendar.getInstance();
					tempCalendar.setTime(date);
					tempCalendar.set(Calendar.DATE, 1);
					if (calendar.get(Calendar.DAY_OF_WEEK) < tempCalendar.get(Calendar.DAY_OF_WEEK)) {
						objEventDetail.setDayNumber((calendar.get(Calendar.WEEK_OF_MONTH)) - 1);
					} else {
						objEventDetail.setDayNumber(calendar.get(Calendar.WEEK_OF_MONTH));
					}
					day = calendar.get(Calendar.DAY_OF_WEEK);
					objEventDetail.setEveryWeekDayMonth(new String[] { day.toString() });
					objEventDetail.setEveryDayMonth(1);
					objEventDetail.setDayOfMonth(day.toString());
					
					// Range Of Recurrence
					objEventDetail.setOccurenceType("noEndDate");
					objEventDetail.setEndAfter(1);

				} else {
					objEventDetail.setIsOngoing("yes");

					if (null != objEventDetail.getEventStartDate() && !"".equals(objEventDetail.getEventStartDate())) {
						objEventDetail.setEventStartDate(Utility.formattedDate(objEventDetail.getEventStartDate()));
					}

					if (null != objEventDetail.getEventEndDate() && !"".equals(objEventDetail.getEventEndDate())) {
						objEventDetail.setEventEndDate(Utility.formattedDate(objEventDetail.getEventEndDate()));
					}

					if ("Daily".equalsIgnoreCase(objEventDetail.getRecurrencePatternName())) {
						if (objEventDetail.getIsWeekDay() == false) {
							objEventDetail.setIsOngoingDaily("days");
							objEventDetail.setEveryWeekDay(objEventDetail.getRecurrenceInterval());
						} else {
							objEventDetail.setIsOngoingDaily("weekDays");
							objEventDetail.setEveryWeekDay(1);
						}
						
						// Weekly Recurrence
						objEventDetail.setEveryWeek(1);
						Date date = new Date(objEventDetail.getEventStartDate());
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(date);
						Integer day = calendar.get(Calendar.DAY_OF_WEEK);
						objEventDetail.setDays(new String[] { day.toString() });
						
						// Monthly Recurrence
						objEventDetail.setIsOngoingMonthly("date");
						objEventDetail.setDateOfMonth(calendar.get(Calendar.DATE));
						objEventDetail.setEveryMonth(1);
						Calendar tempCalendar = Calendar.getInstance();
						tempCalendar.setTime(date);
						tempCalendar.set(Calendar.DATE, 1);
						if (calendar.get(Calendar.DAY_OF_WEEK) < tempCalendar.get(Calendar.DAY_OF_WEEK)) {
							objEventDetail.setDayNumber((calendar.get(Calendar.WEEK_OF_MONTH)) - 1);
						} else {
							objEventDetail.setDayNumber(calendar.get(Calendar.WEEK_OF_MONTH));
						}
						day = calendar.get(Calendar.DAY_OF_WEEK);
						objEventDetail.setEveryWeekDayMonth(new String[] { day.toString() });
						objEventDetail.setEveryDayMonth(1);
						objEventDetail.setDayOfMonth(day.toString());
					} else
						if ("Weekly".equalsIgnoreCase(objEventDetail.getRecurrencePatternName())) {
							objEventDetail.setEveryWeek(objEventDetail.getRecurrenceInterval());
							String[] tempDays = objEventDetail.getDays();
							objEventDetail.setHiddenDays(tempDays[0]);

							// Daily Recurrence
							objEventDetail.setIsOngoingDaily("days");
							objEventDetail.setEveryWeekDay(1);

							// Monthly Recurrence
							Date date = new Date(objEventDetail.getEventStartDate());
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(date);
							Integer day = calendar.get(Calendar.DAY_OF_WEEK);
							objEventDetail.setIsOngoingMonthly("date");
							objEventDetail.setDateOfMonth(calendar.get(Calendar.DATE));
							objEventDetail.setEveryMonth(1);
							Calendar tempCalendar = Calendar.getInstance();
							tempCalendar.setTime(date);
							tempCalendar.set(Calendar.DATE, 1);
							if (calendar.get(Calendar.DAY_OF_WEEK) < tempCalendar.get(Calendar.DAY_OF_WEEK)) {
								objEventDetail.setDayNumber((calendar.get(Calendar.WEEK_OF_MONTH)) - 1);
							} else {
								objEventDetail.setDayNumber(calendar.get(Calendar.WEEK_OF_MONTH));
							}
							day = calendar.get(Calendar.DAY_OF_WEEK);
							objEventDetail.setEveryWeekDayMonth(new String[] { day.toString() });
							objEventDetail.setEveryDayMonth(1);
							objEventDetail.setDayOfMonth(day.toString());
						} else
							if ("Monthly".equalsIgnoreCase(objEventDetail.getRecurrencePatternName())) {
								Date date = new Date(objEventDetail.getEventStartDate());
								Calendar calendar = Calendar.getInstance();
								calendar.setTime(date);
								Integer day = calendar.get(Calendar.DAY_OF_WEEK);

								if (objEventDetail.getByDayNumber() == true) {
									objEventDetail.setIsOngoingMonthly("date");
									objEventDetail.setDateOfMonth(objEventDetail.getDayNumber());
									objEventDetail.setEveryMonth(objEventDetail.getRecurrenceInterval());

									Calendar tempCalendar = Calendar.getInstance();
									tempCalendar.setTime(date);
									tempCalendar.set(Calendar.DATE, 1);
									if (calendar.get(Calendar.DAY_OF_WEEK) < tempCalendar.get(Calendar.DAY_OF_WEEK)) {
										objEventDetail.setDayNumber((calendar.get(Calendar.WEEK_OF_MONTH)) - 1);
									} else {
										objEventDetail.setDayNumber(calendar.get(Calendar.WEEK_OF_MONTH));
									}
									day = calendar.get(Calendar.DAY_OF_WEEK);
									objEventDetail.setEveryWeekDayMonth(new String[] { day.toString() });
									objEventDetail.setEveryDayMonth(1);
								} else {
									objEventDetail.setIsOngoingMonthly("day");
									day = calendar.get(Calendar.DAY_OF_WEEK);
									String[] tempDays = objEventDetail.getDays();
									objEventDetail.setHiddenDays(tempDays[0]);
									objEventDetail.setEveryWeekDayMonth(tempDays);
									objEventDetail.setEveryDayMonth(objEventDetail.getRecurrenceInterval());

									objEventDetail.setDateOfMonth(calendar.get(Calendar.DATE));
									objEventDetail.setEveryMonth(1);
								}
								
								// Weekly Recurrence
								objEventDetail.setEveryWeek(1);
								// eventDetails.setDays(new String[] {
								// day.toString() });
								// Daily Recurrence
								objEventDetail.setIsOngoingDaily("days");
								objEventDetail.setEveryWeekDay(1);
							}

					if (null == objEventDetail.getEventEndDate() && null == objEventDetail.getEndAfter()) {
						Date date = new Date();
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(date);
						Format formatter = new SimpleDateFormat("MM/dd/yyyy");
						objEventDetail.setOccurenceType("noEndDate");
						objEventDetail.setEndAfter(1);
						objEventDetail.setEventEndDate(formatter.format(date));
					} else
						if (null != objEventDetail.getEventEndDate() && null != objEventDetail.getEndAfter()) {
							objEventDetail.setOccurenceType("endAfter");
						} else {
							objEventDetail.setOccurenceType("endBy");
							objEventDetail.setEndAfter(1);
						}

					if (null != objEventDetail.getEventStartTime() && !"".equals(objEventDetail.getEventStartTime())) {
						String eventTime = objEventDetail.getEventStartTime();
						String[] tempTime = eventTime.split(":");
						objEventDetail.setEventStartTimeHrs(tempTime[0]);
						objEventDetail.setEventStartTimeMins(tempTime[1]);
					}
					if (null != objEventDetail.getEventEndTime() && !"".equals(objEventDetail.getEventEndTime())) {
						String eventTime = objEventDetail.getEventEndTime();
						String[] tempTime = eventTime.split(":");
						objEventDetail.setEventEndTimeHrs(tempTime[0]);
						objEventDetail.setEventEndTimeMins(tempTime[1]);
					}
				}
				
				if (null == objEventDetail.getBsnsLoc() || "0".equals(objEventDetail.getBsnsLoc())) {
					objEventDetail.setBsnsLoc("no");
				} else {
					objEventDetail.setBsnsLoc("yes");
					if (!"".equals(Utility.checkNull(objEventDetail.getRetailLocationIDs()))) {
						objEventDetail.setHiddenLocationIDs(objEventDetail.getRetailLocationIDs());
					}
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside retailerServiceImpl : getEventDetail : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return objEventDetail;
	}
	
	
	/**
	 * This serviceImpl method will return list of Retailer Events based on input parameter by calling DAO methods.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @param storeIdentification
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	public final SearchResultInfo getRetailerEventList(int retailerId,
			int lowerLimit, String eventName, Integer isFundraising)
					throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  getRetailerEventList ");
		
		SearchResultInfo searchResult = null;
		
		try {
			searchResult = retailerDAO.getRetailerEventList(retailerId, lowerLimit, eventName, isFundraising);

			if (null != searchResult) {
				for (int i = 0; i < searchResult.getEventList().size(); i++)
				{
					if (!"".equals(Utility.checkNull(searchResult.getEventList().get(i).getEventStartDate()))) {
						searchResult.getEventList().get(i).setEventStartDate(Utility.formattedDate(searchResult.getEventList().get(i).getEventStartDate())) ;
					}
					if (!"".equals(Utility.checkNull(searchResult.getEventList().get(i).getEventEndDate()))) {
						searchResult.getEventList().get(i).setEventEndDate(Utility.formattedDate(searchResult.getEventList().get(i).getEventEndDate())) ;
					}
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside retailerServiceImpl : getRetailerEventList : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (ParseException e) {
			LOG.error("Inside retailerServiceImpl : getRetailerEventList : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return searchResult;
	}
	
	
	/**
	 * This serviceImpl method will return list of event patterns.
	 * 
	 * @return List of event patterns. 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<Event> getEventPatterns() throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  getEventPatterns ");

		List<Event> eventList = null;
		
		try {
			eventList = retailerDAO.getEventPatterns();
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside retailerServiceImpl : getRetailerEventList : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return eventList;
	}
	
	/**
	 * This serviceImpl method deletes the Event page from list.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteEvent(Integer eventId) throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  deleteEvent ");
		
		String strResponse = null;
		try {
			strResponse = retailerDAO.deleteEvent(eventId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside retailerServiceImpl : deleteEvent : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return strResponse;
	}
	
	/**
	 * This serviceImpl method will display all associated  Retailer Location with that Event.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public final List<RetailerLocation> getAllLocationEvent(Integer eventId, int iRetailId)
			throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getAllLocationEvent ");
		
		List<RetailerLocation> arAdsList = null;
		
		try {
			arAdsList = retailerDAO.getAllLocationEvent(eventId, iRetailId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arAdsList;
	}

	public SearchResultInfo getFundraiserEventList(int retailerId, int lowerLimit, String eventName, Long userId) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl :  getFundraiserEventList() ");
		SearchResultInfo searchResult = null;

		try {
			searchResult = retailerDAO.getFundraiserEventList(retailerId, lowerLimit, eventName, userId);
			if (null != searchResult) {
				for (int i = 0; i < searchResult.getEventList().size(); i++) {
					if (!"".equals(Utility.checkNull(searchResult.getEventList().get(i).getEventStartDate()))) {
						searchResult.getEventList().get(i)
								.setEventStartDate(Utility.formattedDate(searchResult.getEventList().get(i).getEventStartDate()));
					}
					if (!"".equals(Utility.checkNull(searchResult.getEventList().get(i).getEventEndDate()))) {
						searchResult.getEventList().get(i)
								.setEventEndDate(Utility.formattedDate(searchResult.getEventList().get(i).getEventEndDate()));
					}
				}
			}
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside RetailerServiceImpl : getFundraiserEventList() : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (ParseException e) {
			LOG.error("Inside RetailerServiceImpl : getFundraiserEventList() : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return searchResult;
	}

	public List<RetailerLocation> getFundraiserEventLocation(Integer eventId, int iRetailId) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getFundraiserEventLocation ");
		List<RetailerLocation> arAdsList = null;
		try {
			arAdsList = retailerDAO.getFundraiserEventLocation(eventId, iRetailId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return arAdsList;
	}

	public List<Category> getFundraiserDepartmentList(Integer retailId) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getDepartmentList() ");
		List<Category> deptList = null;
		try	{
			deptList = retailerDAO.getFundraiserDepartmentList(retailId);
		} catch (ScanSeeWebSqlException e)	{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return deptList;
	}
	
	public final ArrayList<Category> getFundraiserEventCategory(Integer retailId) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : getFundraiserEventCategory() ");
		
		ArrayList<Category> fundraiserCatList = null;
		
		try {
			fundraiserCatList = (ArrayList<Category>) retailerDAO.getFundraiserEventCategory(retailId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return fundraiserCatList;
	}

	public String saveFundraiserDept(Long userId, Integer retailId, String deptName) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : saveFundraiserDept() ");
		String response = null;
		try {
			response = retailerDAO.saveFundraiserDept(userId, retailId, deptName);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

	public String saveUpdateFundraiser(Long userId, Event objEvent) throws ScanSeeServiceException {
		LOG.info("Inside RetailerServiceImpl : saveUpdateFundraiser() ");
		String response = null;
		InputStream inputStream = null;
		final String fileSeparator = System.getProperty("file.separator");
		try {
			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.RETAILER, objEvent.getRetailID());
			StringBuilder mediaTempPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP);
			String retMediaPath = mediaPathBuilder.toString();
			String retTempMediaPath = mediaTempPathBuilder.toString();
			
			if (!"".equals(Utility.checkNull(objEvent.getEventImageName()))) {
				/* Event Images are upload to server if Upload image bit is true. When user edit the images*/
				if (objEvent.isUploadedImage()) {
					inputStream = new BufferedInputStream(new FileInputStream(retTempMediaPath + fileSeparator + objEvent.getEventImageName()));
					if (null != inputStream) {
						Utility.writeFileData(inputStream, retMediaPath + fileSeparator + objEvent.getEventImageName());
					}
				}
			}
			
			try {
				objEvent.setEventStartDate(Utility.getFormattedDate(objEvent.getEventStartDate()));
				if(!"".equals(Utility.checkNull(objEvent.getEventEndDate())))	{
					objEvent.setEventEndDate(Utility.getFormattedDate(objEvent.getEventEndDate()));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			response = retailerDAO.saveUpdateFundraiser(userId, objEvent);
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (FileNotFoundException e) {
			LOG.error("Inside retailerServiceImpl : addEventDetail : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return response;
	}
	
	/**
	 * This serviceImpl method deletes the Event page from list.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteFundraiser(Integer eventId) throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  deleteFundraiser ");
		String strResponse = null;
		try {
			strResponse = retailerDAO.deleteFundraiser(eventId);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside retailerServiceImpl : deleteEvent : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return strResponse;
	}

	public Event getFundEventDetails(Integer eventId, Integer retailID) throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  getFundEventDetails ");
		Event objEventResp = null;
		try {
			objEventResp = retailerDAO.getFundEventDetails(eventId, retailID);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside retailerServiceImpl : deleteEvent : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return objEventResp;
	}
	
	public List<Category> fetchFilters(String filterIds) throws ScanSeeServiceException {
		LOG.info("Inside retailerServiceImpl :  fetchFilters ");
		List<Category> filterList = null;
		try {
			filterList = retailerDAO.fetchFilters(filterIds);
		} catch (ScanSeeWebSqlException e) {
			LOG.error("Inside retailerServiceImpl : fetchFilters : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return filterList;
	}
}

package retailer.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retailer.dao.RetailerSearchDAO;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductReview;

/**
 * RetailerSearchServiceImpl implements RetailerSearchService methods.
 * 
 * @author Created by SPAN.
 */
public class RetailerSearchServiceImpl implements RetailerSearchService{
   
	private static final Logger LOG = LoggerFactory.getLogger(RetailerSearchServiceImpl.class);
	private RetailerSearchDAO retailerSearchDAO;

	public RetailerSearchDAO getRetailerSearchDAO() {
		return retailerSearchDAO;
	}

	public void setRetailerSearchDAO(RetailerSearchDAO retailerSearchDAO) {
		this.retailerSearchDAO = retailerSearchDAO;
	}

	public ArrayList<ProductVO> getAllProduct(String zipcode, String productName, int lowerLimit, String screenName)
	{

		ArrayList<ProductVO> productList = null;

		// productList = searchDAO.fetchAllProduct(zipcode,
		// productName,lowerLimit, screenName);

		return productList;

	}

	public ProductVO getAllProductInfo(Integer retailLocationID,
			Integer productID) throws ScanSeeServiceException {

		ProductVO productInfo = null;
		try {

			productInfo = retailerSearchDAO.fetchProductInfo(retailLocationID,
					productID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return productInfo;

	}

	public SearchResultInfo searchProducts(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException
	{
		SearchResultInfo resultInfo = null;
		try
		{
			if (objForm.getSearchType().equalsIgnoreCase("products"))
			{
				resultInfo = retailerSearchDAO.fetchAllProduct(objForm, loginUser, lowerLimit);
			}
			else
			{
				// HotDeals Search
				resultInfo = retailerSearchDAO.searchHotDeals(objForm, loginUser);
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}
		/*
		 * if(loginUser == null){ //Consumer search String searchType =
		 * objForm.getSearchType(); if(searchType.equals("products")){
		 * searchDAO.fetchAllProduct(objForm,loginUser); }else{ } }else {
		 * searchDAO.fetchAllProduct(objForm,loginUser); } return null;
		 */

		return resultInfo;
	}
	
	
	/**
	 * This is a RestEasy WebService Method for fetching product reviews and
	 * user ratings.Method Type:GET.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML Containing Product Reviews and User Ratings.
	 * @throws ScanSeeServiceException 
	 */

	public ArrayList<ProductReview> getProductReviews(Integer productId) throws ScanSeeServiceException
	{
		final String methodName = "getProductReviews";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		ArrayList<ProductReview> productReviewslist = null;
	
		try
		{
			if (null != productId)
			{
				productReviewslist = retailerSearchDAO.getProductReviews(productId);
			}
			

		}
		catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e.getMessage());
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productReviewslist;
	}
	
	
	/**
	 * This is Service method for retrieving find near by retailers.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to get the Retailers.
	 * 
	 * @param AreaInfoVO
	 *            area information dtoes in the instance.
	 * @return FindNearByDetails 
	 *  		  instance containing list of retailers.
	 *  
	 */

	public FindNearByDetails findNearBy(AreaInfoVO objAreaInfoVO)	{

		final String methodName = "findNearBy";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		FindNearByDetails findNearByDetails = new FindNearByDetails();
		try{
		findNearByDetails = retailerSearchDAO.fetchNearByInfo(objAreaInfoVO);
		}catch (Exception e) {
			LOG.error("", e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return findNearByDetails;
	}

	public ExternalAPIInformation externalApiInfo(String moduleName) throws ScanSeeServiceException
	{

		ExternalAPIInformation externalAPIInformation = null;

		try
		{

			externalAPIInformation = retailerSearchDAO.getExternalApiInfo(moduleName);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return externalAPIInformation;

	}
	
	
	public ProductDetail fetchProductDetails(String productId) throws ScanSeeServiceException
	{

		ProductDetail prdDetail = null;

		try
		{

			prdDetail = retailerSearchDAO.getProductDetails(productId);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return prdDetail;

	}
	
}

package retailer.query;

/**
 * This class is used for retailer related queries
 * 
 * @author malathi_lr
 *
 */
public class RetailerQuery {

	/**
	 * For fetching Lat and Long for the given zipcode.
	 */
	public static final  String FETCHREJECTEDRECORDS="SELECT * FROM UploadRetaillocationsDiscarded WHERE RetailID = ?";
	
	
	/**
	 * 
	 */
	public static final String FETCHMAILID ="select ContactEmail from Contact where CreateUserID =?";
}

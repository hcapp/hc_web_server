package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.RetailerCustomPage;

/**
 * validation used in Retailer custom page screen.
 * 
 * @author Created by SPAN.
 */
public class RetailerPageValidator implements Validator
{
	/**
	 * Getting the RetailerCustomPage Instance.
	 * 
	 * @param clazz as request parameter.
	 * @return retailerCustomPage instance of RetailerCustomPage.
	 */
	public final boolean supports(Class<?> clazz)
	{
		return RetailerCustomPage.class.isAssignableFrom(clazz);
	}

	/**
	 * This method validate Retailer Banner Screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors)
	{
		final RetailerCustomPage retCustomPage = (RetailerCustomPage) target;
		if ("AttachLink".equals(retCustomPage.getLandigPageType()))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retPageTitle", "pageTile.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retCreatedPageattachLink", "webUrl.required");
			if (null == retCustomPage.getHiddenLocId() || "".equals(retCustomPage.getHiddenLocId()))
			{
				errors.rejectValue("retCreatedPageLocId", "location.required");
			}
			if ("slctdUpld".equals(retCustomPage.getImageUplType()))
			{
				if (!(retCustomPage.getImageIconID() > 0))
				{
					errors.rejectValue("retImage", "imageFile");
				}
			}
			else if ("usrUpld".equals(retCustomPage.getImageUplType()))
			{
				if (null == retCustomPage.getRetailerImg() || "".equals(retCustomPage.getRetailerImg()))
				{
					errors.rejectValue("retImage", "imageFile");
				}
			}
		}
		else if ("UploadPdf".equals(retCustomPage.getLandigPageType()))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retPageTitle", "pageTile.required");
			if (null == retCustomPage.getHiddenLocId() || "".equals(retCustomPage.getHiddenLocId()))
			{
				errors.rejectValue("retCreatedPageLocId", "location.required");
			}

			if ("slctdUpld".equals(retCustomPage.getImageUplType()))
			{
				if (!(retCustomPage.getImageIconID() > 0))
				{
					errors.rejectValue("retImage", "imageFile");
				}
			}
			else if ("usrUpld".equals(retCustomPage.getImageUplType()))
			{
				if (null == retCustomPage.getRetailerImg() || "".equals(retCustomPage.getRetailerImg()))
				{
					errors.rejectValue("retImage", "imageFile");
				}
			}

			if (null != retCustomPage.getRetFile() && retCustomPage.getRetFile().getSize() <= 0)
			{
				if (null != retCustomPage.getPdfFileName() && "".equals(retCustomPage.getPdfFileName()))
				{
					errors.rejectValue("retFile", "pdfFile.required");
				}
			}
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retPageTitle", "pageTile.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retPageShortDescription", "pageShorDescription.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retPageLongDescription", "longDescription.required");
			if (null == retCustomPage.getHiddenLocId() || "".equals(retCustomPage.getHiddenLocId()))
			{
				errors.rejectValue("retCreatedPageLocId", "location.required");
			}

			if (null == retCustomPage.getRetailerImg() || "".equals(retCustomPage.getRetailerImg()))
			{
				errors.rejectValue("retImage", "imageFile");
			}
		}
	}

	/**
	 * This method validate Retailer custom page.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.INVALIDURL)) {
			errors.rejectValue("retCreatedPageattachLink", "invalid.retailurl");
		}
		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			errors.rejectValue("retCreatedPageStartDate", "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			errors.rejectValue("retCreatedPageEndDate", "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue("retCreatedPageEndDate", "datebefore");
		}
		if (status.equals(ApplicationConstants.ENDDATE)) {
			errors.rejectValue("retCreatedPageEndDate", "endDate.required");
		}
		if (status.equals(ApplicationConstants.STARTDATE)) {
			errors.rejectValue("retCreatedPageStartDate", "startDate.required");
		}
	}
	
	/**
	 * This method validate Retailer AppList Anything page.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validateAppListAnythgPage(Object target, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "aboutUs", "aboutus.required");	
	}
}

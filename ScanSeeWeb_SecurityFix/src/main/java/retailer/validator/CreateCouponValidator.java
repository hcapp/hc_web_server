/**
 * @ (#) CreateCouponValidator.java 26-Dec-2011
 * Project       :ScanSeeWeb
 * File          : CreateCouponValidator.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 26-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.Coupon;
import common.util.Utility;

/**
 * validation used in coupon page screen.
 * 
 * @author Created by SPAN.
 */
public class CreateCouponValidator implements Validator
{
	/**
	 * Variable couponStartDate declared as String.
	 */
	private final String couponStartDate = "couponStartDate";
	
	/**
	 * Variable couponExpireDate declared as String.
	 */
	private final String couponExpireDate = "couponExpireDate";
	/**
	 * Getting the Coupon Instance.
	 * 
	 * @param target as request parameter.
	 * @return coupon instance of Coupon.
	 */
	public final boolean supports(Class<?> target)
	{
		return Coupon.class.isAssignableFrom(target);
	}

	/**
	 * This method validate Retailer coupon screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			errors.rejectValue(couponStartDate, "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			errors.rejectValue(couponExpireDate, "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue(couponExpireDate, "datebefore");
		}
	}
	
	/**
	 * This method validate Retailer coupon screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors)
	{
		final Coupon couponinfo = (Coupon) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "couponName", "couponName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "couponDiscountAmt", "couponDiscountAmt.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numOfCouponIssue", "numOfCouponIssue.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, couponStartDate, "couponStartDate.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, couponExpireDate, "couponEndDate.required");
		if (couponinfo.getCouponStartDate() != null && !"".equals(couponinfo.getCouponStartDate()))
		{
			if (!Utility.isValidDate(couponinfo.getCouponStartDate()))
			{
				errors.rejectValue(couponStartDate, "rebStartDate.invalid");
			}
		}

		if (couponinfo.getCouponExpireDate() != null && !"".equals(couponinfo.getCouponExpireDate()))
		{
			if (!Utility.isValidDate(couponinfo.getCouponExpireDate()))
			{
				errors.rejectValue(couponExpireDate, "rebEndDate.invalid");
			}
		}
		if (couponinfo.getLocationID() == null)
		{
			errors.rejectValue("locationID", "location.required");
		}
	}
}

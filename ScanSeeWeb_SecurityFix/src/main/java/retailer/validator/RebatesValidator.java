package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.Rebates;
import common.util.Utility;

/**
 * validation used in retailer rebates page screen.
 * 
 * @author Created by SPAN.
 */
public class RebatesValidator implements Validator
{
	/**
	 * Variable couponStartDate declared as String.
	 */
	private final String rebStartDate = "rebStartDate";
	
	/**
	 * Variable couponExpireDate declared as String.
	 */
	private final String rebEndDate = "rebEndDate";
	/**
	 * Getting the Rebates Instance.
	 * 
	 * @param arg0 as request parameter.
	 * @return rebates instance of Rebates.
	 */
	public final boolean supports(Class<?> arg0)
	{
		return Rebates.class.isAssignableFrom(arg0);
	}
	
	/**
	 * This method validate Retailer rebates page screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT))
		{
			errors.rejectValue(rebStartDate, "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT))
		{
			errors.rejectValue(rebEndDate, "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER))
		{
			errors.rejectValue(rebEndDate, "datebefore");
		}
	}
	
	/**
	 * This method validate Retailer rebates page screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors)
	{
		final Rebates rebatesVo = (Rebates) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebName", "rebName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebAmount", "rebAmount");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebLongDescription", "rebShortDescription");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebTermCondtn", "rebTermCondtn");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, rebStartDate, rebStartDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, rebEndDate, rebEndDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retailerLocID", "retailerLocID.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productName", "productName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "noOfrebatesIsued", "noOfrebatesIsued.required");

		// Validation to check if rebate amount is valid amount.
		if (rebatesVo.getRebAmount() != null && !"".equals(rebatesVo.getRebAmount()))
		{
			if (!Utility.isNumber(rebatesVo.getRebAmount()))
			{
				errors.rejectValue("rebAmount", "rebAmount.invalid");
			}
		}

		if (rebatesVo.getRebStartDate() != null && !"".equals(rebatesVo.getRebStartDate()))
		{
			if (!Utility.isValidDate(rebatesVo.getRebStartDate()))
			{
				errors.rejectValue(rebStartDate, "rebStartDate.invalid");
			}
		}

		if (rebatesVo.getRebEndDate() != null && !"".equals(rebatesVo.getRebEndDate()))
		{
			if (!Utility.isValidDate(rebatesVo.getRebEndDate()))
			{
				errors.rejectValue(rebEndDate, "rebEndDate.invalid");
			}
		}

		if (rebatesVo.getRetailerLocID() == 0)
		{
			errors.rejectValue("retailerLocID", "retailerLocID.required");
		}
	}

	/**
	 * This method validate edit Retailer rebates page screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validateEdit(Object target, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebName", "rebName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebAmount", "rebAmount");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebLongDescription", "rebShortDescription");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebTermCondtn", "rebTermCondtn");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, rebStartDate, rebStartDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, rebEndDate, rebEndDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retailerLocID", "retailerLocID.required");
		final Rebates rebatesVo = (Rebates) target;
		// Validation to check if rebate amount is valid amount.

		if (rebatesVo.getRebAmount() != null && !"".equals(rebatesVo.getRebAmount()))
		{
			if (!Utility.isNumber(rebatesVo.getRebAmount()))
			{
				errors.rejectValue("rebAmount", "rebAmount.invalid");
			}
		}

		if (rebatesVo.getRebStartDate() != null && !"".equals(rebatesVo.getRebStartDate()))
		{
			if (!Utility.isValidDate(rebatesVo.getRebStartDate()))
			{
				errors.rejectValue(rebStartDate, "rebStartDate.invalid");
			}
		}

		if (rebatesVo.getRebEndDate() != null && !"".equals(rebatesVo.getRebEndDate()))
		{
			if (!Utility.isValidDate(rebatesVo.getRebEndDate()))
			{
				errors.rejectValue(rebEndDate, "rebEndDate.invalid");
			}
		}

		if (rebatesVo.getRetailerLocID() == 0)
		{
			errors.rejectValue("retailerLocID", "retailerLocID.required");
		}
	}
	
	/**
	 * This method validate rerun Retailer rebates page screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validateRerun(Object target, Errors errors)
	{
		final Rebates rebatesVo = (Rebates) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebName", "rebName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebAmount", "rebAmount");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebLongDescription", "rebShortDescription");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rebTermCondtn", "rebTermCondtn");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, rebStartDate, rebStartDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, rebEndDate, rebEndDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retailerLocID", "retailerLocID.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "noOfrebatesIsued", "noOfrebatesIsued.required");
		// Validation to check if rebate amount is valid amount.
		if (rebatesVo.getRebAmount() != null && !"".equals(rebatesVo.getRebAmount()))
		{
			if (!Utility.isNumber(rebatesVo.getRebAmount()))
			{
				errors.rejectValue("rebAmount", "rebAmount.invalid");
			}
		}
		if (rebatesVo.getRebStartDate() != null && !"".equals(rebatesVo.getRebStartDate()))
		{
			if (!Utility.isValidDate(rebatesVo.getRebStartDate()))
			{
				errors.rejectValue(rebStartDate, "rebStartDate.invalid");
			}
		}
		if (rebatesVo.getRebEndDate() != null && !"".equals(rebatesVo.getRebEndDate()))
		{
			if (!Utility.isValidDate(rebatesVo.getRebEndDate()))
			{
				errors.rejectValue(rebEndDate, "rebEndDate.invalid");
			}
		}
		if (rebatesVo.getRetailerLocID() == 0)
		{
			errors.rejectValue("retailerLocID", "retailerLocID.required");
		}
	}
}

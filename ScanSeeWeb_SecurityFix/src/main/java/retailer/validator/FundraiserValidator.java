package retailer.validator;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import retailer.controller.FundraiserController;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Event;
import common.util.Utility;

public class FundraiserValidator implements Validator {

	/**
	 * Getting the logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(FundraiserController.class);

	public boolean supports(Class<?> clazz) {
		return Event.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		Event eventValidation = (Event) target;
		Date objCurrentDate = new Date();

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eventName", "eventname.required");

		if ("".equals(Utility.checkNull(eventValidation.getEventImageName()))) {
			errors.rejectValue("eventImageFile", "logoImageName.required");
		}

		if (null != eventValidation.getIsRetLoc() && "yes".equalsIgnoreCase(eventValidation.getIsRetLoc())) {
			if (null == eventValidation.getRetLocId()) {
				errors.rejectValue("retLocId", "retLocId.required");
			}
		} else {
			if ("".equals(Utility.checkNull(eventValidation.getOrganization()))) {
				errors.rejectValue("organization", "organization.required");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortDescription", "shortDescription.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "longDescription", "longDescription.required");

		if ("".equals(Utility.checkNull(eventValidation.getEventCategory()))) {
			errors.rejectValue("eventCategory", "eventCategory.required");
		}

		if ("".equals(Utility.checkNull(eventValidation.getEventStartDate()))) {
			errors.rejectValue("eventStartDate", "startDate.required");
		} else if (!Utility.isValidDate(eventValidation.getEventStartDate())) {
				errors.rejectValue("eventStartDate", "validstartdate");
		} else if(null == eventValidation.getEventID() || "".equals(eventValidation.getEventID()))	{
			try	{
				String compDate = Utility.compareCurrentDate(eventValidation.getEventStartDate(), objCurrentDate);
				if (null != compDate) {
					errors.rejectValue("eventStartDate", "datestartcurrent");
				}
			} catch(ScanSeeServiceException e)	{
				LOG.error("Error in class: FundraiserValidator: " + e.getMessage());
			}
		} /*else {
				String compDate = null;
				try {
					
					 * Start date is compared when new fundraiser is created or
					 * when changes is made for existing fundraiser
					 
					if (null == eventValidation.getEventID() || "".equals(eventValidation.getEventID())) {
						compDate = Utility.compareCurrentDate(eventValidation.getEventStartDate(), objCurrentDate);
						if (null != compDate) {
							errors.rejectValue("eventStartDate", "datestartcurrent");
						}
					} /else if (!eventValidation.getEventStartDate().equals(eventValidation.getEventDate())) {
						compDate = Utility.compareCurrentDate(eventValidation.getEventStartDate(), objCurrentDate);
						if (null != compDate) {
							errors.rejectValue("eventStartDate", "datestartcurrent");
						}
					}
				} catch (ScanSeeServiceException e) {
					LOG.error("Error in class: FundraiserValidator: " + e.getMessage());
				}
		}*/

		if (!"".equals(Utility.checkNull(eventValidation.getEventEndDate()))) {
			if (!Utility.isValidDate(eventValidation.getEventEndDate())) {
				errors.rejectValue("eventEndDate", "validenddate");
			} else {
				String startEndCompare = null;
				try {
					startEndCompare = Utility.compareDate(eventValidation.getEventStartDate(), eventValidation.getEventEndDate());
				} catch (ScanSeeServiceException e) {
					LOG.error("Error in class: FundraiserValidator in Comparing Event End Date comparision.");
				}
				if (null != startEndCompare) {
					errors.rejectValue("eventEndDate", "datebefore");
				}
			}
		}

		if (!"".equals(Utility.checkNull(eventValidation.getMoreInfo()))) {
			if (!Utility.validateURL(eventValidation.getMoreInfo())) {
				errors.rejectValue("moreInfo", "invalid.url");
			}
		}

		if (!"".equals(Utility.checkNull(eventValidation.getPurchaseProduct()))) {
			if (!Utility.validateURL(eventValidation.getMoreInfo())) {
				errors.rejectValue("purchaseProduct", "invalid.url");
			}
		}
		
		/*
		 * Checking for valid price
		 */
		if(!"".equals(Utility.checkNull(eventValidation.getFundraisingGoal())))	{
			if(!Utility.isValidPrice(eventValidation.getFundraisingGoal()))	{
				errors.rejectValue("fundraisingGoal", "price.invalid");
			}
		}
		
		/*
		 * Checking for valid price
		 */
		if(!"".equals(Utility.checkNull(eventValidation.getCurrentLevel())))	{
			if(!Utility.isValidPrice(eventValidation.getCurrentLevel()))	{
				errors.rejectValue("currentLevel", "price.invalid");
			}
		}

		if ("yes".equals(eventValidation.getIsEventTied())) {
			if (null == eventValidation.getEventAssociatedIds() || "".equals(eventValidation.getEventAssociatedIds())) {
				errors.rejectValue("eventAssociatedIds", "events.required");
			}
		}
	}

}

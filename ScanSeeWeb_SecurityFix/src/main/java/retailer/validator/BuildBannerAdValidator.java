/**
 * @ (#) BuildBannerAdValidator.java 08-Aug-2011
 * Project       :ScanSeeWeb
 * File          : BuildBannerAdValidator.java
 * Author        : Kumar D
 * Company       : Span Systems Corporation
 * Date Created  : 08-Aug-2011
 *
 * @author       : Kumar D
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import common.constatns.ApplicationConstants;

import common.pojo.RetailerLocationAdvertisement;
import common.util.Utility;

/**
 * validation used in Retailer Banner screen.
 * 
 * @author Created by SPAN.
 */
public class BuildBannerAdValidator implements Validator
{
	/**
	 * Variable advertisementDate declared as String.
	 */
	private final String advertisementDate = "advertisementDate";
	
	/**
	 * Variable advertisementEndDate declared as String.
	 */
	private final String advertisementEndDate = "advertisementEndDate";
	
	/**
	 * Variable ribbonXAdURL declared as String.
	 */
	private final String ribbonXAdURL = "ribbonXAdURL";
	/**
	 * Getting the RetailerLocationAdvertisement Instance.
	 * 
	 * @param target as request parameter.
	 * @return retailerLocationAdvertisement instance of RetailerLocationAdvertisement.
	 */
	public final boolean supports(Class<?> target)
	{
		return RetailerLocationAdvertisement.class.isAssignableFrom(target);
	}

	/**
	 * This method validate Retailer Banner screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			errors.rejectValue(advertisementDate, "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			errors.rejectValue(advertisementEndDate, "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue(advertisementEndDate, "datebefore");
		}
		if (status.equals(ApplicationConstants.WEBURL)) {
			errors.rejectValue(ribbonXAdURL, "invalid.url");
		}
	}

	/**
	 * This method validate Retailer Banner screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors)
	{
		final RetailerLocationAdvertisement retLocationAdvertisement = (RetailerLocationAdvertisement) target;
		if (retLocationAdvertisement.getRetailLocationIds() == null)
		{
			errors.rejectValue("retailLocationIds", "location.required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "advertisementName", "adName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, advertisementDate, "adStartDate.required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, advertisementEndDate, "adEndDate.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bannerAdImagePath", "bannerAdImagePath.required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ribbonAdImagePath", "ribbonAdImagePath.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, ribbonXAdURL, "ribbonXAdURL.required");
		if (retLocationAdvertisement.getAdvertisementDate() != null && !"".equals(retLocationAdvertisement.getAdvertisementDate()))
		{
			if (!Utility.isValidDate(retLocationAdvertisement.getAdvertisementDate())) {
				errors.rejectValue(advertisementDate, "rebStartDate.invalid");
			}
		}

		if (retLocationAdvertisement.getAdvertisementEndDate() != null && !"".equals(retLocationAdvertisement.getAdvertisementEndDate()))
		{
			if (!Utility.isValidDate(retLocationAdvertisement.getAdvertisementEndDate())) {
				errors.rejectValue(advertisementEndDate, "rebEndDate.invalid");
			}
		}
	}
	
	/**
	 * This method validate Retailer edit Banner screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void editValidate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue(advertisementEndDate, "datebefore");
		}
		if (status.equals(ApplicationConstants.WEBURL)) {
			errors.rejectValue(ribbonXAdURL, "invalid.url");
		}
	}
	
	/**
	 * This method validate Retailer Banner screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void editValidate(Object target, Errors errors)
	{
		final RetailerLocationAdvertisement retLocationAdvertisement = (RetailerLocationAdvertisement) target;
		if (retLocationAdvertisement.getRetailLocationIds() == null)
		{
			errors.rejectValue("retailLocationIds", "location.required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "advertisementName", "adName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, advertisementDate, "adStartDate.required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, advertisementEndDate, "adEndDate.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, ribbonXAdURL, "ribbonXAdURL.required");
		if (retLocationAdvertisement.getAdvertisementDate() != null && !"".equals(retLocationAdvertisement.getAdvertisementDate()))
		{
			if (!Utility.isValidDate(retLocationAdvertisement.getAdvertisementDate()))
			{
				errors.rejectValue(advertisementDate, "rebStartDate.invalid");
			}
		}
		if (retLocationAdvertisement.getAdvertisementEndDate() != null && !"".equals(retLocationAdvertisement.getAdvertisementEndDate()))
		{
			if (!Utility.isValidDate(retLocationAdvertisement.getAdvertisementEndDate()))
			{
				errors.rejectValue(advertisementEndDate, "rebEndDate.invalid");
			}
		}
	}
}

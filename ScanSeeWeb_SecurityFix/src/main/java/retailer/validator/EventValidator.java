package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.pojo.Event;
import common.util.Utility;
import common.constatns.ApplicationConstants;

/**
 * validation used in Event screen.
 * 
 * @author Kumar D.
 */
public class EventValidator implements Validator {

	public boolean supports(Class<?> target) {
		return Event.class.isAssignableFrom(target);
	}

	public void validate(Object target, Errors errors) {
		Event eventValidation = (Event) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eventName", "eventname.required");
		
		if ("".equals(Utility.checkNull(eventValidation.getEventImageName()))) {
			errors.rejectValue("eventImageFile", "logoImageName.required");
		}

		if ("".equals(Utility.checkNull(eventValidation.getEventCategory()))) {
			errors.rejectValue("eventCategory", "eventCategory.required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortDescription", "shortDescription.required");

		if (null != eventValidation.getIsOngoing() && "yes".equals(eventValidation.getIsOngoing())) {
			if (null != eventValidation.getRecurrencePatternName()) {
				if ("Daily".equalsIgnoreCase(eventValidation.getRecurrencePatternName())
						&& "days".equalsIgnoreCase(eventValidation.getIsOngoingDaily())) {
					if (null == eventValidation.getEveryWeekDay() || "".equals(eventValidation.getEveryWeekDay())) {
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "everyWeekDay", "everyWeekDay.required");
					} else
						if (eventValidation.getEveryWeekDay() <= 0) {
							errors.rejectValue("everyWeekDay", "everyWeekDay.greaterZero");
						}
				} else
					if ("Weekly".equalsIgnoreCase(eventValidation.getRecurrencePatternName())) {
						if (null == eventValidation.getEveryWeek() || "".equals(eventValidation.getEveryWeek())) {
							ValidationUtils.rejectIfEmptyOrWhitespace(errors, "everyWeek", "everyWeek.required");
						} else
							if (eventValidation.getEveryWeek() <= 0) {
								errors.rejectValue("everyWeek", "everyWeek.greaterZero");
							} else
								if (eventValidation.getDays().length <= 0) {
									errors.rejectValue("days", "days.required");
								}
					} else
						if ("Monthly".equalsIgnoreCase(eventValidation.getRecurrencePatternName())) {
							if ("date".equalsIgnoreCase(eventValidation.getIsOngoingMonthly())) {
								if (null == eventValidation.getDateOfMonth() || "".equals(eventValidation.getDateOfMonth())) {
									ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateOfMonth", "dateOfMonth.required");
								} else
									if (eventValidation.getDateOfMonth() <= 0 || eventValidation.getDateOfMonth() > 31) {
										errors.rejectValue("dateOfMonth", "dayOfMonth.greaterZero");
									} else {
										if (null == eventValidation.getEveryMonth() || "".equals(eventValidation.getEveryMonth())) {
											ValidationUtils.rejectIfEmptyOrWhitespace(errors, "everyMonth", "everyMonth.required");
										} else
											if (eventValidation.getEveryMonth() <= 0) {
												errors.rejectValue("everyMonth", "everyMonth.greaterZero");
											}
									}
							} else {
								if (null == eventValidation.getEveryDayMonth() || "".equals(eventValidation.getEveryDayMonth())) {
									ValidationUtils.rejectIfEmptyOrWhitespace(errors, "everyDayMonth", "everyMonth.required");
								} else
									if (eventValidation.getEveryDayMonth() <= 0) {
										errors.rejectValue("everyDayMonth", "everyMonth.greaterZero");
									}
							}
						}

				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eventStartDate", "startDate.required");
				if ("endBy".equalsIgnoreCase(eventValidation.getOccurenceType())) {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eventEndDate", "endDate.required");
				} else
					if ("endAfter".equalsIgnoreCase(eventValidation.getOccurenceType())) {
						if (null == eventValidation.getEndAfter() || "".equals(eventValidation.getEndAfter())) {
							ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endAfter", "endAfter.required");
						} else
							if (eventValidation.getEndAfter() <= 0) {
								errors.rejectValue("endAfter", "occurence.greaterZero");
							}

					}
			}
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eventDate", "startDate.required");
		}

		if (null != eventValidation.getBsnsLoc() && "yes".equals(eventValidation.getBsnsLoc())) {
			if ("".equals(Utility.checkNull(eventValidation.getRetailLocationIDs()))) {
				errors.rejectValue("retailLocationIDs", "select.retailerloc");
			}
		} else
			if (null != eventValidation.getBsnsLoc() && "no".equals(eventValidation.getBsnsLoc())) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "address.required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "city.required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "state", "state.required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "postalCode.required");
			}

	}

	/**
	 * This method validate Event Retailer Location.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors.
	 * @param status
	 *            as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.GEOERROR)) {
			errors.rejectValue("address", "invalid.geomessge");
		}
		if (status.equals(ApplicationConstants.LATITUDE)) {
			errors.rejectValue("latitude", "latitude.required");
		}
		if (status.equals(ApplicationConstants.LONGITUDE)) {
			errors.rejectValue("logitude", "longitude.required");
		}
		if (status.equals(ApplicationConstants.INVALIDEVENTURL)) {
			errors.rejectValue("moreInfoURL", "invalid.retailurl");
		}
	}

	public final void validateDates(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.VALIDSTARTDATE)) {
			errors.rejectValue("eventStartDate", "validstartdate");
		}
		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			errors.rejectValue("eventStartDate", "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.VALIDENDDATE)) {
			errors.rejectValue("eventEndDate", "validenddate");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			errors.rejectValue("eventEndDate", "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue("eventEndDate", "datebefore");
		}
		if (status.equals(ApplicationConstants.VALIDDATE)) {
			errors.rejectValue("eventDate", "validstartdate");
		}
		if (status.equals(ApplicationConstants.VALIDEDATE)) {
			errors.rejectValue("eventEDate", "validenddate");
		}
		if (status.equals(ApplicationConstants.DATENOGSTARTCURRENT)) {
			errors.rejectValue("eventDate", "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATENOGENDCURRENT)) {
			errors.rejectValue("eventEDate", "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATENOGAFTER)) {
			errors.rejectValue("eventEDate", "datebefore");
		}
	}
}

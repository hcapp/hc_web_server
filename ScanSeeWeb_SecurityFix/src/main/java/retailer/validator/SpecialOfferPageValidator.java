package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.RetailerCustomPage;
import common.util.Utility;

/**
 * validation used in Retailer Special Offer page screen.
 * 
 * @author Created by SPAN.
 */
public class SpecialOfferPageValidator implements Validator
{
	/**
	 * Getting the RetailerCustomPage Instance.
	 * 
	 * @param clazz as request parameter.
	 * @return retailerCustomPage instance of RetailerCustomPage.
	 */
	public final boolean supports(Class<?> clazz)
	{
		return RetailerCustomPage.class.isAssignableFrom(clazz);
	}

	/**
	 * This method validate Retailer Special Offer page screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT))
		{
			errors.rejectValue("splOfferStartDate", "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT))
		{
			errors.rejectValue("splOfferEndDate", "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER))
		{
			errors.rejectValue("splOfferEndDate", "datebefore");
		}
	}

	/**
	 * This method validate Retailer Special Offer page screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors)
	{
		final RetailerCustomPage retailerCustomPage = (RetailerCustomPage) target;
		if ("Special Offer".equals(retailerCustomPage.getSplOfferType()))
		{

			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "splOfferTitle", "pageTile.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "splOfferLongDescription", "longDescription.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "splOfferShortDescription", "pageShorDescription.required");

			if (retailerCustomPage.getRetailerImg() == null || "".equals(retailerCustomPage.getRetailerImg()))
			{
				errors.rejectValue("retImage", "imageFile");
			}
			if (retailerCustomPage.getHiddenLocId() == null || "".equals(retailerCustomPage.getHiddenLocId()))
			{
				errors.rejectValue("splOfferLocId", "location.required");
			}
			if (retailerCustomPage.getSplOfferStartDate() != null && !"".equals(retailerCustomPage.getSplOfferStartDate()))
			{
				if (!Utility.isValidDate(retailerCustomPage.getSplOfferStartDate()))
				{
					errors.rejectValue("splOfferStartDate", "rebStartDate.invalid");
				}
			} else {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "splOfferStartDate", "startDate.required");
			}

			if (retailerCustomPage.getSplOfferEndDate() != null && !"".equals(retailerCustomPage.getSplOfferEndDate()))
			{
				if (!Utility.isValidDate(retailerCustomPage.getSplOfferEndDate()))
				{
					errors.rejectValue("splOfferEndDate", "rebEndDate.invalid");
				}
			} /*else {
				//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "splOfferEndDate", "endDate.required");
			}*/
		}
		else if ("AttachLink".equals(retailerCustomPage.getSplOfferType()))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "splOfferTitle", "pageTile.required");
			if (retailerCustomPage.getHiddenLocId() == null || "".equals(retailerCustomPage.getHiddenLocId()))
			{
				errors.rejectValue("splOfferLocId", "location.required");
			}
			if (retailerCustomPage.getSplOfferattachLink() != null && !"".equals(retailerCustomPage.getSplOfferattachLink()))
			{
				if (!Utility.validateURL(retailerCustomPage.getSplOfferattachLink()))
				{
					errors.rejectValue("splOfferattachLink", "url.valid");
				}
			} else {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "splOfferattachLink", "link.required");
			}

			if ("slctdUpld".equals(retailerCustomPage.getImageUplType()))
			{
				if (!(retailerCustomPage.getImageIconID() > 0))
				{
					errors.rejectValue("retImage", "imageFile");
				}
			}
			else if ("usrUpld".equals(retailerCustomPage.getImageUplType()))
			{
				if (null == retailerCustomPage.getRetailerImg() || "".equals(retailerCustomPage.getRetailerImg()))
				{
					errors.rejectValue("retImage", "imageFile");
				}
			}
		}
	}
}

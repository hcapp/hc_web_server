/**
 * @ (#) BuildBannerAdController.java 08-Aug-2011
 * Project       :ScanSeeWeb
 * File          : BuildBannerAdController.java
 * Author        : Kumar D
 * Company       : Span Systems Corporation
 * Date Created  : 08-Aug-2011
 *
 * @author       : Kumar D
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.BuildBannerAdValidator;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.RetailerProfile;
import common.pojo.Users;
import common.util.Utility;

/**
 * BuildBannerAdController is a controller class for Build Banner Page screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class BuildBannerAdController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BuildBannerAdController.class);

	/**
	 * Variable RETURN_VIEW declared as constant string.
	 */
	private final String returnView = "buildBannerAd";

	/**
	 * buildBannerAdValidator as instance of BuildBannerAdValidator.
	 */
	private BuildBannerAdValidator buildBannerAdValidator;

	/**
	 * Getting the BuildBannerAdValidator Instance.
	 * 
	 * @param bannerAdValidator
	 *            is instance of CreateBannerAdValidator. buildBannerAdValidator
	 *            the BuildBannerAdValidator to set.
	 */
	@Autowired
	public void setBuildBannerAdValidator(BuildBannerAdValidator buildBannerAdValidator)
	{
		this.buildBannerAdValidator = buildBannerAdValidator;
	}

	/**
	 * The showCreateBannerAdPage method to show Build Banner Page screen.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return RETURN_VIEW addHotDeals view.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/buildbannerad.htm", method = RequestMethod.GET)
	public String createBannerAdPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside BuildBannerAdController : createBannerAdPage ");
		try
		{
			final RetailerLocationAdvertisement retailerLocationAds = new RetailerLocationAdvertisement();
			model.put("buildbanneradform", retailerLocationAds);
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			ArrayList<RetailerLocation> retailerLocations = null;
			session.setAttribute("imageCropPage", "CreateBannerAdPage");
			session.setAttribute("minCropWd", 320);
			session.setAttribute("minCropHt", 50);
			Long retailID = null;
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			retailID = Long.valueOf(loginUser.getRetailerId());

			retailerLocations = retailerService.getRetailerLocations(retailID);

			if (retailerLocations != null)
			{
				session.setAttribute("retLocationList", retailerLocations);
			}
			else
			{
				session.setAttribute("retLocationList", "");
			}
			session.setAttribute("bannerImagePath", ApplicationConstants.UPLOAD_IMAGE_PATH);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside BuildBannerAdController : createBannerAdPage : Exception : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return returnView;
	}

	/**
	 * This controller method will insert Build Banner Page info by calling
	 * service layer.
	 * 
	 * @param retailerLocationAds
	 *            instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/buildbannerad.htm", method = RequestMethod.POST)
	public ModelAndView onSubmitBannerInfo(@ModelAttribute("buildbanneradform") RetailerLocationAdvertisement retailerLocationAds,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside BuildBannerAdController : onSubmitBannerInfo");
		String view = returnView;
		String serviceResponse = null;
		String compDate = null;
		final Date currentDate = new Date();
		boolean isValidUrl = true;
		final MultipartFile fileBannerAd = retailerLocationAds.getBannerAdImagePath();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

			Long retailID = null;
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			retailID = Long.valueOf(loginUser.getRetailerId());
			retailerLocationAds.setRetailID(retailID);
			retailerLocationAds.setRetailLocationIDHidden(retailerLocationAds.getRetailLocationIds());
			buildBannerAdValidator.validate(retailerLocationAds, result);
			// For Indefinite Duration of Ads
			//Utility.setAdStartAndEndDate(retailerLocationAds);
			final FieldError fieldStartDate = result.getFieldError("advertisementDate");
			if (fieldStartDate == null)
			{
				compDate = Utility.compareCurrentDate(retailerLocationAds.getAdvertisementDate(), currentDate);
				if (null != compDate)
				{
					buildBannerAdValidator.validate(retailerLocationAds, result, ApplicationConstants.DATESTARTCURRENT);
				}
			}

			if (!"".equals(Utility.checkNull(retailerLocationAds.getRibbonXAdURL())))
			{
				isValidUrl = Utility.validateURL(retailerLocationAds.getRibbonXAdURL());
			}
			if (! "".equals(Utility.checkNull(retailerLocationAds.getAdvertisementEndDate()))) {
				final FieldError fieldEndDate = result.getFieldError("advertisementEndDate");
				if (fieldEndDate == null)
				{
					compDate = Utility.compareCurrentDate(retailerLocationAds.getAdvertisementEndDate(), currentDate);
					if (null != compDate)
					{
						buildBannerAdValidator.validate(retailerLocationAds, result, ApplicationConstants.DATEENDCURRENT);
					}
					else
					{
						compDate = Utility.compareDate(retailerLocationAds.getAdvertisementDate(), retailerLocationAds.getAdvertisementEndDate());
						if (null != compDate)
						{
							buildBannerAdValidator.validate(retailerLocationAds, result, ApplicationConstants.DATEAFTER);
						}
					}
				}
			}

			if (!isValidUrl)
			{
				buildBannerAdValidator.validate(retailerLocationAds, result, ApplicationConstants.WEBURL);
			}

			if ("".equals(Utility.checkNull(retailerLocationAds.getStrBannerAdImagePath())))
			{
				result.rejectValue("bannerAdImagePath", "Please Upload Banner Image", "Please Upload Banner Image");
			}
			
			if (result.hasErrors())
			{

				view = returnView;
			}
			else
			{
				serviceResponse = retailerService.buildBannerAdInfo(retailerLocationAds);
				if (serviceResponse.equals(ApplicationConstants.SUCCESS))
				{
					if (!"".equals(Utility.checkNull(retailerLocationAds.getInvalidLocationIds())))
					{
						retailerLocationAds.setRetailLocationIDHidden(null);
						retailerLocationAds.setRetailLocationIDHidden(retailerLocationAds.getInvalidLocationIds());
						request.setAttribute("message", "There is already banner page in given date range for selected location.");
						view = "buildBannerAd";
					}
					else
					{
						view = "/ScanSeeWeb/retailer/managebannerads.htm";
					}
				}
				else if (serviceResponse.equals(ApplicationConstants.FAILURE))
				{
					view = "buildBannerAd";
					result.reject("bannerAdDateCheck.error");
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside BuildBannerAdController : showCreateBannerAdPage : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		if (view.equals(returnView))
		{
			return new ModelAndView(view);
		}
		else
		{
			return new ModelAndView(new RedirectView(view));
		}
	}

	/**
	 * This controller method will display Banner Page preview screen.
	 * 
	 * @param objretLocAd
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param map
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/previewBannerAds.htm", method = RequestMethod.POST)
	public ModelAndView previewDisplayImage(@ModelAttribute("previewRetailerAdsform") RetailerLocationAdvertisement objretLocAd,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map)
			throws ScanSeeServiceException, IOException
	{
		LOG.info("Inside BuildBannerAdController : previewDisplayImage ");

		String strBannerName = null;
		InputStream isBannerAd = null;
		OutputStream osBannerAd = null;
		RetailerProfile objRetName = new RetailerProfile();
		try
		{
			final RetailerLocationAdvertisement retailerLocationAds = new RetailerLocationAdvertisement();
			map.put("previewBuildBannerform", retailerLocationAds);
			request.getSession().removeAttribute("message");
			request.getSession().removeAttribute("retailerName");
			request.getSession().removeAttribute("fileRibbonAd");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			objRetName = retailerService.getRetailerProfile(loginUser);
			final String strRetailName = objRetName.getRetailerName();
			request.getSession().setAttribute("retailerName", strRetailName);
			final MultipartFile fileBannerAd = objretLocAd.getBannerAdImagePath();

			if (fileBannerAd.getSize() > 0)
			{
				isBannerAd = fileBannerAd.getInputStream();
				strBannerName = request.getRealPath("") + "/images/" + fileBannerAd.getOriginalFilename();
				osBannerAd = new FileOutputStream(strBannerName);

				int readBytes = 0;
				final byte[] bufferBanner = new byte[100000];
				while ((readBytes = isBannerAd.read(bufferBanner, 0, 100000)) != -1)
				{
					osBannerAd.write(bufferBanner, 0, readBytes);
				}
				isBannerAd.close();
				osBannerAd.close();
			}
			session.setAttribute("fileRibbonAd", "../images/" + fileBannerAd.getOriginalFilename());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside BuildBannerAdController : previewDisplayImage : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		catch (IOException e)
		{
			LOG.info("Inside BuildBannerAdController : previewDisplayImage : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView("previewBuildBannerAd");
	}
}

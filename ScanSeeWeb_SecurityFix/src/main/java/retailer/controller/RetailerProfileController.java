package retailer.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.RetailerProfileValidator;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.RetailerRegistration;
import common.pojo.SearchZipCode;
import common.pojo.State;
import common.util.Utility;

/**
 * RetailerProfileController is a controller class for create Retailer
 * registration screen.
 * 
 * @author Created by SPAN.
 */
@Controller
@SessionAttributes("createretailerprofileform")
public class RetailerProfileController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RetailerProfileController.class);

	/**
	 * Variable retailerProfileValidator declared as instance of
	 * RetailerProfileValidator.
	 */
	private RetailerProfileValidator retailerProfileValidator;

	/**
	 * Variable LOGIN_URL declared as String.
	 */
	private final String loginUrl = "/ScanSeeWeb/login.htm";

	/**
	 * Variable city declared as String.
	 */
	private final String city = "City";

	/**
	 * Variable city declared as String.
	 */
	private final String supplierCity = "supplierCity";
	/**
	 * Variable DUPLICATE_USER_VIEW declared as String.
	 */
	private final String duplicateUserView = "createRetailerProfile";

	/**
	 * To set retailerProfileValidator.
	 * 
	 * @param retailerProfileValidator
	 *            to set.
	 */
	@Autowired
	public final void setRetailerProfileValidator(RetailerProfileValidator retailerProfileValidator)
	{
		this.retailerProfileValidator = retailerProfileValidator;
	}

	/**
	 * This controller method will display Retailer Registration screen.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param session1
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return redirect to create profile screen
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/registerretailer")
	public final ModelAndView showRetailerForm(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : showRetailerForm ");
		final RetailerRegistration retailerRegistrationInfo = new RetailerRegistration();

		model.put("createretailerprofileform", retailerRegistrationInfo);
		return new ModelAndView(new RedirectView("createRetailerProfile.htm"));
	}

	/**
	 * This controller method will display Retailer Registration screen.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param session1
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/createRetailerProfile.htm", method = RequestMethod.GET)
	public final String showRetailerForm(@ModelAttribute("createretailerprofileform") RetailerRegistration retailerRegistrationInfo,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : showRetailerForm ");
		
		ArrayList<State> states = null;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			states = retailerService.getAllStates();
			
			session.setAttribute("states", states);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + e.getMessage());
			throw new ScanSeeServiceException();
		}
		// final RetailerRegistration retailerRegistrationInfo = new
		// RetailerRegistration();
		model.put("createretailerprofileform", retailerRegistrationInfo);
		/*
		 * // ArrayList<State> states = null; //ArrayList<Category>
		 * arBCategoryList = null; // String reCaptchaPrivateKey = null; //
		 * String reCaptchaPublicKey = null; try { final ServletContext
		 * servletContext = request.getSession().getServletContext(); final
		 * WebApplicationContext appContext =
		 * WebApplicationContextUtils.getWebApplicationContext(servletContext);
		 * // final SupplierService supplierService = (SupplierService) //
		 * appContext.getBean(ApplicationConstants.SUPPLIERSERVICE); final
		 * RetailerService retailerService = (RetailerService)
		 * appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		 * reCaptchaPrivateKey = (String)
		 * session.getAttribute("reCaptchaPrivateKey"); reCaptchaPublicKey =
		 * (String) session.getAttribute("reCaptchaPublicKey"); if (null ==
		 * reCaptchaPrivateKey || null == reCaptchaPublicKey) { appConfig =
		 * supplierService.getAppConfig(ApplicationConstants.RECAPTCHA); if
		 * (null != appConfig) { for (int j = 0; j < appConfig.size(); j++) { if
		 * (appConfig.get(j).getScreenName().equals(ApplicationConstants
		 * .RECAPTCHAPRIVATEKEY)) { reCaptchaPrivateKey =
		 * appConfig.get(j).getScreenContent(); } if
		 * (appConfig.get(j).getScreenName
		 * ().equals(ApplicationConstants.RECAPTCHAPUBLICKEY)) {
		 * reCaptchaPublicKey = appConfig.get(j).getScreenContent(); } } }
		 * session.setAttribute("reCaptchaPrivateKey", reCaptchaPrivateKey);
		 * session.setAttribute("reCaptchaPublicKey", reCaptchaPublicKey); } //
		 * states = supplierService.getAllStates(); //arBCategoryList =
		 * retailerService.getAllRetailerProfileBusinessCategory(); //
		 * session.setAttribute("statesListCreat", states);
		 * //session.setAttribute("categoryList", arBCategoryList); } catch
		 * (ScanSeeServiceException e) {
		 * LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
		 * throw e; }
		 */
		return "createRetailerProfile";
	}

	/**
	 * This controller method will return the City List based on input
	 * parameter.
	 * 
	 * @param stateCode
	 *            as request parameter.
	 * @param selCity
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return City List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */

	@RequestMapping(value = "/retailerfetchcity", method = RequestMethod.GET)
	@ResponseBody
	public final String getCities(@RequestParam(value = "statecode", required = true) String stateCode,
			@RequestParam(value = "city", required = true) String selCity, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : getCities ");
		ArrayList<City> cities = null;
		final StringBuffer citiesStr = new StringBuffer();
		final JSONObject jsonObject = new JSONObject();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			if (null != selCity && !"".equals(selCity))
			{
				final String cityList = (String) request.getSession().getAttribute(supplierCity);
				return cityList;
			}
			else
			{
				cities = supplierService.getAllCities(stateCode);
				response.setContentType("application/json");
				if (null != cities && !cities.isEmpty())
				{
					for (int i = 1; i < cities.size(); i++)
					{
						citiesStr.append(cities.get(i).getCityName());
						citiesStr.append(",");
					}
					jsonObject.put(city, citiesStr.toString().substring(0, citiesStr.toString().length() - 1));
				}
				else
				{
					jsonObject.put(city, "");
				}
				session.setAttribute(supplierCity, jsonObject.toString());
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;

		}
		return jsonObject.toString();
	}

	/**
	 * This controller method is used to validate the corporate information
	 * entered by retailer
	 * 
	 * @param objRetRegistration
	 *            instance of RetailerRegistration.
	 * @param result
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/createRetailerProfile", method = RequestMethod.POST)
	public final ModelAndView createRetailerProfile(@ModelAttribute("createretailerprofileform") RetailerRegistration objRetRegistration,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : createRetailerProfile ");
		// String locationCoordinates =
		// request.getParameter("locationCoordinates");
		// String state = null;
		// String strCity = null;
		// String emailId = null;
		// String retypeEmailId = null;

		boolean isValidCorpPhoneNum = true;
		RetailerRegistration objRetailRegResponse = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		String view = null;

		// String captchResponse = null;
		try
		{
			// emailId = retailerRegistrationInfo.getContactEmail();
			/*
			 * retypeEmailId = retailerRegistrationInfo.getRetypeEmail(); String
			 * remoteAddr = request.getRemoteAddr(); String challengeField =
			 * request.getParameter("recaptcha_challenge_field"); String
			 * responseField = request.getParameter("recaptcha_response_field");
			 * retailerRegistrationInfo.setCaptch(responseField); String
			 * reCaptchaPrivateKey = (String)
			 * session.getAttribute("reCaptchaPrivateKey"); captchResponse =
			 * Utility.validateCaptcha(challengeField, responseField,
			 * remoteAddr,reCaptchaPrivateKey);
			 */
			// state = retailerRegistrationInfo.getStateHidden();
			// strCity = retailerRegistrationInfo.getCity();
			// session.setAttribute(supplierCity,
			// retailerRegistrationInfo.getCity());

			objRetRegistration.setbCategoryHidden(objRetRegistration.getbCategory());
			isValidCorpPhoneNum = Utility.validatePhoneNum(objRetRegistration.getContactPhone());
			retailerProfileValidator.validate(objRetRegistration, result);

			if (result.hasErrors()) {
				view = duplicateUserView;
				/*
				 * FieldError fieldcaptch = result.getFieldError("captch"); if
				 * (fieldcaptch == null) { if (captchResponse ==
				 * ApplicationConstants.FAILURE) {
				 * retailerProfileValidator.validate(retailerRegistrationInfo,
				 * result, ApplicationConstants.INVALID_CAPTCHA); } }
				 */
				return new ModelAndView(view);
			}
			// till here only for mandatiory validation done
			// from here checking the valida email id or not and pass word
			// double check
			if (!isValidCorpPhoneNum) {
				retailerProfileValidator.validate(objRetRegistration, result, ApplicationConstants.INVALIDCORPPHONE);
			}
			if (!"".equals(Utility.checkNull(objRetRegistration.getGeoError()))) {
				if (null == objRetRegistration.getRetailerLocationLatitude()) {
					retailerProfileValidator.validate(objRetRegistration, result, ApplicationConstants.LATITUDE);
				}
				if (null == objRetRegistration.getRetailerLocationLongitude()) {
					retailerProfileValidator.validate(objRetRegistration, result, ApplicationConstants.LONGITUDE);
				}
			}
			if (result.hasErrors())
			{
				view = duplicateUserView;
				return new ModelAndView(view);
			}
			
			objRetailRegResponse = retailerService.findDuplicateRetailer(objRetRegistration);
			if (null != objRetailRegResponse && objRetailRegResponse.getResponse() != null)
			{
				if (objRetailRegResponse.getResponse().equals(ApplicationConstants.GEOERROR))
				{
					/*Could not find Latitude/Longitude for the address.*/
					request.setAttribute("GEOERROR", ApplicationConstants.GEOERROR);
					retailerProfileValidator.validate(objRetRegistration, result, ApplicationConstants.GEOERROR);
					if (result.hasErrors())
					{
						view = duplicateUserView;
						return new ModelAndView(view);
					}
				} else if (objRetailRegResponse.getResponse().equals(ApplicationConstants.DUPLICATE_RETAILERNAME)) {
					retailerProfileValidator.validate(objRetRegistration, result, ApplicationConstants.DUPLICATE_RETAILERNAME);
					if (result.hasErrors())
					{
						view = duplicateUserView;
						return new ModelAndView(view);
					}
				} else if (objRetailRegResponse.getResponse().equals(ApplicationConstants.SUCCESS)) {
					objRetRegistration.setRetailerLocationLatitude(objRetailRegResponse.getRetailerLocationLatitude());
					objRetRegistration.setRetailerLocationLongitude(objRetailRegResponse.getRetailerLocationLongitude());
					if (null != objRetailRegResponse.getDuplicateRetList() && !objRetailRegResponse.getDuplicateRetList().isEmpty())
					{
						session.setAttribute("duplicateRegRetList", objRetailRegResponse.getDuplicateRetList());
						return new ModelAndView(new RedirectView("duplicateretailer.htm"));

					} else {
						return new ModelAndView(new RedirectView("finalizeregistration.htm"));
					}
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(new RedirectView("finalizeregistration.htm"));
	}

	/**
	 * This controller method will update the City List based on input
	 * parameter.
	 * 
	 * @param stateCode
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return City List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/displayselectedcity", method = RequestMethod.GET)
	@ResponseBody
	public final String getCitiesEdit(@RequestParam(value = "statecode", required = true) String stateCode, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : getCities ");
		ArrayList<City> cities = null;
		final StringBuffer citiesStr = new StringBuffer();
		final JSONObject jsonObject = new JSONObject();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

			cities = supplierService.getAllCities(stateCode);
			response.setContentType("application/json");
			if (null != cities && !cities.isEmpty())
			{
				for (int i = 1; i < cities.size(); i++)
				{
					citiesStr.append(cities.get(i).getCityName());
					citiesStr.append(",");
				}
				jsonObject.put(city, citiesStr.toString().substring(0, citiesStr.toString().length() - 1));
			}
			else
			{
				jsonObject.put(city, "");
			}
			session.setAttribute(supplierCity, jsonObject.toString());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return jsonObject.toString();
	}

	/**
	 * This controller method will update the City List based on input
	 * parameter.
	 * 
	 * @param stateCode
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return City List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/displayZipStateCity", method = RequestMethod.GET)
	@ResponseBody
	public final String getZip(@RequestParam(value = "statecode", required = true) String stateCode, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : getCities ");
		ArrayList<City> cities = null;
		final StringBuffer citiesStr = new StringBuffer();
		final JSONObject jsonObject = new JSONObject();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

			cities = supplierService.getAllCities(stateCode);
			response.setContentType("application/json");
			if (null != cities && !cities.isEmpty())
			{
				for (int i = 1; i < cities.size(); i++)
				{
					citiesStr.append(cities.get(i).getCityName());
					citiesStr.append(",");
				}
				jsonObject.put(city, citiesStr.toString().substring(0, citiesStr.toString().length() - 1));
			}
			else
			{
				jsonObject.put(city, "");
			}
			session.setAttribute(supplierCity, jsonObject.toString());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return jsonObject.toString();
	}

	/**
	 * This controller method will return zipcode, state and the City List based
	 * on input parameter.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return zipcode, state and the City List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/displayZipCodeStateCity", method = RequestMethod.GET)
	@ResponseBody
	public final String getZipStateCity(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : getZipStateCity ");
		String zipCode = request.getParameter("term");
		ArrayList<SearchZipCode> zipCodes = null;
		JSONObject object = new JSONObject();
		JSONObject valueObj = null;
		JSONArray array = new JSONArray();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

			zipCodes = supplierService.getZipStateCity(zipCode);
			response.setContentType("application/json");
			if (null != zipCodes && !zipCodes.isEmpty())
			{
				for (SearchZipCode code : zipCodes)
				{
					String value = code.getCity() + ", " + code.getStateName() + " " + code.getZipCode();
					valueObj = new JSONObject();
					valueObj.put("lable", code.getZipCode());
					valueObj.put("value", value);
					valueObj.put("city", code.getCity());
					valueObj.put("statecode", code.getStateCode());
					valueObj.put("state", code.getStateName());
					valueObj.put("zip", code.getZipCode());
					array.put(valueObj);
				}
			}
			else
			{
				valueObj = new JSONObject();
				valueObj.put("lable", "No Records Found");
				valueObj.put("value", "No Records Found");
				array.put(valueObj);
			}
			object.put("zipcodes", array);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return object.get("zipcodes").toString();
	}

	/**
	 * This controller method will return zipcode, state and the City List based
	 * on input parameter.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return zipcode, state and the City List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/displayCityStateZipCode", method = RequestMethod.GET)
	@ResponseBody
	public final String getCityStateZip(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : getZipStateCity ");
		String zipCode = request.getParameter("term");
		ArrayList<SearchZipCode> cities = null;
		JSONObject object = new JSONObject();
		JSONObject valueObj = null;
		JSONArray array = new JSONArray();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

			cities = supplierService.getCityStateZip(zipCode);
			response.setContentType("application/json");
			if (null != cities && !cities.isEmpty())
			{
				for (SearchZipCode city : cities)
				{
					String value = city.getCity() + ", " + city.getStateName() + " " + city.getZipCode();
					valueObj = new JSONObject();
					valueObj.put("lable", city.getZipCode());
					valueObj.put("value", value);
					valueObj.put("city", city.getCity());
					valueObj.put("statecode", city.getStateCode());
					valueObj.put("state", city.getStateName());
					valueObj.put("zip", city.getZipCode());
					array.put(valueObj);
				}
			}
			else
			{
				valueObj = new JSONObject();
				valueObj.put("lable", "No Records Found");
				valueObj.put("value", "No Records Found");
				array.put(valueObj);
			}
			object.put("cities", array);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return object.get("cities").toString();
	}

	/**
	 * This controller method is used to display the screen to enter retailer
	 * contact and other details
	 * 
	 * @param retailerRegistrationInfo
	 *            instance of RetailerRegistration.
	 * @param result
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */

	@RequestMapping(value = "/finalizeregistration", method = RequestMethod.GET)
	public String finalizeRegistration(@ModelAttribute("createretailerprofileform") RetailerRegistration retailerRegistrationInfo,
			BindingResult result, HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : finalizeRegistration ");
		// RetailerRegistration retailerRegistrationInfo = new
		// RetailerRegistration();
		model.put("createretailerprofileform", retailerRegistrationInfo);
		ArrayList<Category> arBCategoryList = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			arBCategoryList = retailerService.getAllRetailerProfileBusinessCategory();
			session.setAttribute("categoryList", arBCategoryList);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside RetailerProfileController : finalizeRegistration: " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Inside RetailerProfileController : finalizeRegistration");
		return "createRetailerProfilestep2";
	}

	/**
	 * This controller method is used to validate duplicate retailer location
	 * 
	 * @param retailerRegistrationInfo
	 *            instance of RetailerRegistration.
	 * @param result
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/duplicateretailer", method = RequestMethod.POST)
	public ModelAndView validateDuplicateRet(@ModelAttribute("createretailerprofileform") RetailerRegistration retailerRegistrationInfo,
			BindingResult result, HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : finalizeRegistration ");
		// RetailerRegistration retailerRegistrationInfo = new
		// RetailerRegistration();
		model.put("createretailerprofileform", retailerRegistrationInfo);
		LOG.info("Inside RetailerProfileController : finalizeRegistration");
		return new ModelAndView(new RedirectView("finalizeregistration.htm"));
	}

	/**
	 * This controller method is used to display duplicate retail locations
	 * 
	 * @param retailerRegistrationInfo
	 *            instance of RetailerRegistration.
	 * @param result
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/duplicateretailer", method = RequestMethod.GET)
	public String showDuplicateRet(@ModelAttribute("createretailerprofileform") RetailerRegistration retailerRegistrationInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : finalizeRegistration ");
		// RetailerRegistration retailerRegistrationInfo = new
		// RetailerRegistration();
		model.put("createretailerprofileform", retailerRegistrationInfo);
		LOG.info("Inside RetailerProfileController : finalizeRegistration");
		return "duplicateretailersregistered";
	}

	/**
	 * This controller method is used to save retailer information and redirect
	 * to login screen after successful registration
	 * 
	 * @param retailerRegistrationInfo
	 *            instance of RetailerRegistration.
	 * @param result
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/finalizeregistration", method = RequestMethod.POST)
	public ModelAndView finalizeRegistration(@ModelAttribute("createretailerprofileform") RetailerRegistration retailerRegistrationInfo,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session,
			SessionStatus sessionStatus) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : finalizeRegistration ");
		String emailId = null;
		boolean acceptTerm = false;
		boolean isValidEmail = true;
		boolean isValidContactNum = true;
		String strCategory = null;
		boolean isValidUrl = true;
		String isProfileCreated = ApplicationConstants.FAILURE;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		String returnView = null;
		String view = "createRetailerProfilestep2";
		String strLogoImage = retailerService.getDomainName() + ApplicationConstants.HUBCITI_LOGO_FOR_MAILSENDING;
		try
		{
			emailId = retailerRegistrationInfo.getContactEmail();
			acceptTerm = retailerRegistrationInfo.isTerms();
			strCategory = retailerRegistrationInfo.getbCategory();
			retailerRegistrationInfo.setbCategoryHidden(retailerRegistrationInfo.getbCategory());

			retailerProfileValidator.validateRegistration(retailerRegistrationInfo, result);
			if (null == strCategory || "".equals(strCategory))
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.BUSINESSCATEGORY);
			}
			if (!acceptTerm)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.TERM);
			}

			if (result.hasErrors())
			{
				return new ModelAndView(view);
			}

			if (!"".equals(retailerRegistrationInfo.getWebUrl()))
			{
				isValidUrl = Utility.validateURL(retailerRegistrationInfo.getWebUrl());
			}

			if (!isValidUrl)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.WEBURL);
			}

			isValidContactNum = Utility.validatePhoneNum(retailerRegistrationInfo.getContactPhoneNo());
			if (!isValidContactNum)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.INVALIDCONTPHONE);
			}

			isValidEmail = Utility.validateEmailId(emailId);
			if (!isValidEmail)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.INVALIDEMAIL);

			}

			if (result.hasErrors())
			{

				return new ModelAndView(view);
			}

			isProfileCreated = retailerService.processRetailerProfileDetails(retailerRegistrationInfo, strLogoImage);

			if (null != isProfileCreated && isProfileCreated.equals(ApplicationConstants.DUPLICATE_USER))
			{

				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.DUPLICATE_USER);
				if (result.hasErrors())
				{
					returnView = view;
				}
			}

			else if (null != isProfileCreated && isProfileCreated.equals(ApplicationConstants.SUCCESS))
			{

				returnView = loginUrl;
				final String s = "1";
				String retailerLogo = "UploadLogo";
				session.setAttribute("registration", s);
				session.setAttribute("retailerLogo", retailerLogo);
				session.removeAttribute(supplierCity);

			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside RetailerProfileController : finalizeRegistration : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Inside RetailerProfileController : finalizeRegistration ");
		if (null != returnView && returnView.equals(loginUrl))
		{
			session.removeAttribute("supplierCity");
			sessionStatus.setComplete();
			return new ModelAndView(new RedirectView(loginUrl));
		}
		else
		{
			return new ModelAndView(view);
		}

	}
}

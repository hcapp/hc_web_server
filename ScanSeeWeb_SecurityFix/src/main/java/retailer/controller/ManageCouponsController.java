/**
 * @ (#) ManageCouponsController.java 26-Dec-2011
 * Project       :ScanSeeWeb
 * File          : ManageCouponsController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 26-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Coupon;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * ManageCouponsController is a controller class for Manage coupon Page screen.
 * 
 * @author Created by SPAN.
 */

@Controller
@RequestMapping("/managecoupons.htm")
public class ManageCouponsController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageCouponsController.class);
	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();
	/**
	 * Variable viewName declared as String.
	 */
	private final String viewName = "managecoupons";
	/**
	 * This controller method displays Coupon details by its couponID.
	 * 
	 * @param couponFormObj instance as request parameter.
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param model as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException will be thrown. 
	 */
	@RequestMapping(method = RequestMethod.GET)
	public final ModelAndView showCreateCouponPage(@ModelAttribute("managecouponform") Coupon couponFormObj, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageCouponsController : showCreateCouponPage");
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		String sDate = null;
		String eDate = null;
		int iRecordCount = 20;
		try
		{
			request.getSession().removeAttribute("message");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);

			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != couponFormObj.getRecordCount() && !"".equals(couponFormObj.getRecordCount()) && !"undefined".equals(couponFormObj.getRecordCount()))
			{
				iRecordCount = Integer.valueOf(couponFormObj.getRecordCount());
			}
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
			}

			if (objForm == null) {
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchform", objForm);
			}
			final Long retailID = Long.valueOf(loginUser.getRetailerId());
			final SearchResultInfo couponList = retailerService.getCouponSearchResult(retailID, objForm, lowerLimit, iRecordCount);
			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("Promotions", "Coupons", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);
			/*If user has not created any coupon for the first time.It will redirect to Create Coupon page*/
			if (couponList != null  && couponList.getCouponsList().size() <= 0) {
					return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/createcoupon.htm"));
			}
			
			if (couponList != null)
			{
			String couponDiscountamt = null;
				for (int i = 0; i < couponList.getCouponsList().size(); i++)
				{
					 couponDiscountamt = couponList.getCouponsList().get(i).getCouponDiscountAmt();
					if (null != couponDiscountamt)
					{
						if (!couponDiscountamt.contains(".")) {
							couponDiscountamt = couponDiscountamt + ".00";
							couponList.getCouponsList().get(i).setCouponDiscountAmt(couponDiscountamt);
						}
					}
					sDate = couponList.getCouponsList().get(i).getCouponStartDate();
					sDate = Utility.getUsDateFormat(sDate);
					couponList.getCouponsList().get(i).setCouponStartDate(sDate);

					eDate = couponList.getCouponsList().get(i).getCouponExpireDate();
					eDate = Utility.getUsDateFormat(eDate);
					couponList.getCouponsList().get(i).setCouponExpireDate(eDate);
				}
				session.setAttribute("couponList", couponList);
				final Pagination objPage = Utility.getPagination(couponList.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/managecoupons.htm", iRecordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				request.getSession().setAttribute("message", "No Coupons to display");
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside ManageCouponsController : showCreateCouponPage : Exception " + e.getMessage());
			throw e;
		} catch (NullPointerException exception) {
			LOG.error("Inside ManageCouponsController : showCreateCouponPage : Null pointer Exception : ", exception.getMessage());
		} catch (ParseException exception) {
			LOG.error("Inside ManageCouponsController : showCreateCouponPage : ParseException : ", exception.getMessage());
		}
		return new ModelAndView(viewName);
	}

	/**
	 * This controller method displays Coupon details by searching couponName.
	 * 
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param result as request parameter.
	 * @param couponFormObj instance of Coupon.
	 * @param response as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException will be thrown.
	 * @throws IOException will be thrown.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public final ModelAndView onSubmitCouponInfo(@ModelAttribute("managecouponform") Coupon couponFormObj, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside ManageCouponsController : onSubmitCouponInfo ");
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		String sDate = null;
		String eDate = null;
		int iRecordCount = 20;
		String strCouponDiscountAmt = null;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);

			final String pageFlag = (String) request.getParameter("pageFlag");
			if (!"".equals(Utility.checkNull(couponFormObj.getRecordCount())))
			{
				iRecordCount = Integer.valueOf(couponFormObj.getRecordCount());
			}
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
			}

			if (objForm == null) {
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchform", objForm);
			}
			if (ISDUBUGENABLED) {
				LOG.debug("Retailer Id*********" + loginUser.getRetailerId());
				LOG.debug("searchKey*********" + searhKey);
			}
			final Long retailID = Long.valueOf(loginUser.getRetailerId());
			if (couponFormObj.getCouponName() != null) {
				final String counName = couponFormObj.getCouponName().trim();
				objForm.setSearchKey(counName);
			}

			final SearchResultInfo couponList = retailerService.getCouponSearchResult(retailID, objForm, lowerLimit, iRecordCount);
			if (couponList.getCouponsList().size() != 0)
			{
				if (couponList != null)
				{
					for (int i = 0; i < couponList.getCouponsList().size(); i++)
					{
						strCouponDiscountAmt = couponList.getCouponsList().get(i).getCouponDiscountAmt();
						if (!"".equals(Utility.checkNull(strCouponDiscountAmt))) {
							if (!strCouponDiscountAmt.contains(".")) {
								strCouponDiscountAmt = strCouponDiscountAmt + ".00";
								couponList.getCouponsList().get(i).setCouponDiscountAmt(strCouponDiscountAmt);
							}
						}
						sDate = couponList.getCouponsList().get(i).getCouponStartDate();
						sDate = Utility.getUsDateFormat(sDate);
						couponList.getCouponsList().get(i).setCouponStartDate(sDate);

						eDate = couponList.getCouponsList().get(i).getCouponExpireDate();
						eDate = Utility.getUsDateFormat(eDate);
						couponList.getCouponsList().get(i).setCouponExpireDate(eDate);
					}
					session.setAttribute("couponList", couponList);
					final Pagination objPage = Utility.getPagination(couponList.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/managecoupons.htm", iRecordCount);
					session.setAttribute("pagination", objPage);
				}
			} else {
				result.reject("searchCoupons.status");
				request.setAttribute("manageCpnsFont", "font-weight:regular;color:red;");
				session.setAttribute("couponList", couponList);
				session.removeAttribute("pagination");
				//request.getSession().setAttribute("message", "No Coupons to display");
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside ManageCouponsController : onSubmitCouponInfo : Exception " + e.getMessage());
			throw e;
		} catch (ParseException exception) {
			LOG.error("Inside ManageCouponsController : onSubmitCouponInfo : ParseException", exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}
		return new ModelAndView(viewName);
	}
}

package retailer.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.RebatesValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Rebates;
import common.pojo.RetailerLocation;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class RerunRetailerRebatesController {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory
			.getLogger(RerunRetailerRebatesController.class);

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "retailerrerunrebates";

	/**
	 * Variable VIEW_NAME declared as String.
	 */
	String view = VIEW_NAME;

	/**
	 * Variable rebatesValidator declared as instance of ReabtesValidator.
	 */
	RebatesValidator rebatesValidator;

	/**
	 * To set rebatesValidator.
	 * 
	 * @param rebatesValidator
	 *            To set
	 */
	@Autowired
	public void setRebatesValidator(RebatesValidator rebatesValidator) {
		this.rebatesValidator = rebatesValidator;
	}

	/**
	 * this method returns data for rerun rebate.
	 * @param rebatesVo
	 *                rebates instance as parameter.
	 * @param request
	 *                HttpServletRequest instance as parameter.
	 * @param session
	 *                HttpSession instance as parameter.
	 * @param model
	 *                ModelMap instance as parameter.
	 * @param session1
	 *                HttpSession instance as parameter.
	 * @return VIEW_NAME rerunRebates view.
	 * @throws ParseException.
	 * @throws ScanSeeServiceException. 
	 */
	@RequestMapping(value = "/rerunretrebate.htm", method = RequestMethod.GET)
	public String showPage(
			@ModelAttribute("reRunretRebatesForm") Rebates rebatesVo,
			HttpServletRequest request, HttpSession session, ModelMap model,
			HttpSession session1) throws ParseException,
			ScanSeeServiceException {
		LOG.info("Controller Layer:: Inside Get Method");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		request.getSession().removeAttribute("pdtInfoList");
		String rebateStartTime = null;
		String rebateEndTime = null;
		String[] tempStartTimeHrsMin = null;
		String[] tempEndTimeHrsMin = null;
		List<RetailerLocation> retailerLocations = null;
		ArrayList<TimeZones> timeZonelst = null;
		List<Rebates> rebatesList = (List<Rebates>) retailerService
				.getRebatesForDisplay(rebatesVo.getRebateID());
		if (rebatesList != null && rebatesList.size() > 0) {
			for (int i = 0; i < rebatesList.size(); i++) {
				rebatesVo.setRebName(rebatesList.get(i).getRebName());
				String ammount = rebatesList.get(i).getRebAmount();
				ammount = Utility.formatDecimalValue(ammount);
				rebatesVo.setRebAmount(ammount);
				if(rebatesList.get(i).getRebLongDescription() != null)
				{
				rebatesVo.setRebLongDescription(rebatesList.get(i)
						.getRebLongDescription());
				}else{
					rebatesVo.setRebLongDescription(rebatesList.get(i)
						.getRebShortDescription());
				}
				String sDate = rebatesList.get(i).getRebStartDate();
				if (sDate != null) {
					sDate = Utility.formattedDate(sDate);
					rebatesVo.setRebStartDate(sDate);
				}
				String eDate = rebatesList.get(i).getRebEndDate();
				if (eDate != null) {
					eDate = Utility.formattedDate(eDate);
					rebatesVo.setRebEndDate(eDate);
				}
				rebateStartTime = rebatesList.get(i).getRebStartTime();
				tempStartTimeHrsMin = rebateStartTime.split(":");
				rebatesVo.setRebStartHrs(tempStartTimeHrsMin[0]);
				rebatesVo.setRebStartMins(tempStartTimeHrsMin[1]);
				rebateEndTime = rebatesList.get(i).getRebEndTime();
				tempEndTimeHrsMin = rebateEndTime.split(":");
				rebatesVo.setRebEndhrs(tempEndTimeHrsMin[0]);
				rebatesVo.setRebEndMins(tempEndTimeHrsMin[1]);
				rebatesVo.setRebTermCondtn(rebatesList.get(i)
						.getRebTermCondtn());
				rebatesVo.setRebproductId(rebatesList.get(i)
						.getProductID());
				rebatesVo.setRebateTimeZoneId(rebatesList.get(i).getRebateTimeZoneId());
				rebatesVo.setRetailID(Long.valueOf(rebatesList.get(i)
						.getRetailerID()));
				rebatesVo.setRetailerLocationID(rebatesList.get(i)
						.getRetailerLocID());
				rebatesVo.setRetailerLocID(rebatesList.get(i)
						.getRetailerLocID());
				if(rebatesList.get(i).getNoOfrebatesIsued()!= null)
				{
					rebatesVo.setNoOfrebatesIsued(rebatesList.get(i).getNoOfrebatesIsued());
					
				}
				/*if(rebatesList.get(i).getNoOfRebatesUsed()!= null)
				{
					rebatesVo.setNoOfRebatesUsed(rebatesList.get(i).getNoOfRebatesUsed());
				}*/
				
				if(rebatesList.get(i).getRebateTimeZoneId() !=null)
				{
					rebatesVo.setRebateTimeZoneId(rebatesList.get(i).getRebateTimeZoneId());
				}
			
			}
			Users loginUser = (Users) request.getSession().getAttribute(
					"loginuser");
			Long retailID = Long.valueOf(loginUser.getRetailerId());
			retailerLocations = retailerService
					.getRebRetailerLocations(retailID);
			session.setAttribute("retailerLocList", retailerLocations);
			timeZonelst = retailerService.getAllTimeZones();
			session.setAttribute("rebateTimeZoneslst", timeZonelst);
			model.put("reRunretRebatesForm", rebatesVo);
		} else {
			model.put("reRunretRebatesForm", rebatesVo);
		}
		LOG.info("Controller Layer:: Exit Get Method");
		return VIEW_NAME;
	}

	
	/**
	 * this method returns map of hours.
	 * @return sortedMap.
	 * @throws ScanSeeServiceException.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("RebateStartHrs")
	public Map<String, String> populateRebateStartHrs() throws ScanSeeServiceException{
		HashMap<String, String> mapRebateStartHrs = new HashMap<String, String>();

		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapRebateStartHrs.put("0" + i, "0" + i);
			} else {
				mapRebateStartHrs.put(String.valueOf(i), String.valueOf(i));
			}

		}
		Iterator iterator = mapRebateStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapRebateStartHrs);
		return sortedMap;
	}

	/**
	 * this method returns sorted of entry set.
	 * @param unsortMap
	 *        Map instance as parameter.
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException{

		List list = new LinkedList(unsortMap.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue())
						.compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		// put sorted list into map again
		Map sortedMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * this method returns map of minutes.
	 * @return sortedMap.
	 * @throws ScanSeeServiceException.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("RebateStartMins")
	public Map<String, String> populatemapRebateStartMins() throws ScanSeeServiceException{
		HashMap<String, String> mapRebateStartHrs = new HashMap<String, String>();

		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapRebateStartHrs.put("0" + i, "0" + i);
				i = i + 4;
			} else {
				mapRebateStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}

		}
		@SuppressWarnings("unused")
		Iterator iterator = mapRebateStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapRebateStartHrs);
		return sortedMap;

	}

	
	/**
	 * this method returns edited data.
	 * @param rebatesVo
	 *               Rebates instance as parameter.
	 * @param result
	 *              BindingResult instance as parameter.
	 * @param request
	 *              HttpServletRequest instance as parameter.
	 * @param response
	 *              HttpServletResponse instance as parameter.
	 * @param session
	 *              HttpSession instance as parameter.
	 * @return View_NAME rerunRebates view.
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/rerunretrebate.htm", method = RequestMethod.POST)
	public ModelAndView editRebates(
			@ModelAttribute("reRunretRebatesForm") Rebates rebatesVo,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("editRebates:: inside POST Method");

		try {
			String compDate = null;
			Date currentDate = new Date();

			String isDataInserted = null;
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			
			Users user = (Users) request.getSession().getAttribute("loginuser");

			String productID = rebatesVo.getRebproductId();
			LOG.info("ProductId**********" + productID);

			

			String sTime = String.valueOf(rebatesVo.getRebStartHrs()) + ":"
					+ String.valueOf(rebatesVo.getRebStartMins());
			String eTime = String.valueOf(rebatesVo.getRebEndhrs()) + ":"
					+ String.valueOf(rebatesVo.getRebEndMins());

			rebatesVo.setRebStartTime(sTime);
			rebatesVo.setRebEndTime(eTime);

			rebatesVo.setUserID(user.getUserID());
			rebatesVo.setRetailerID(user.getRetailerId());
			

			rebatesValidator.validateRerun(rebatesVo, result);
			
			FieldError fieldStartDate = result.getFieldError("rebStartDate");
			
			
			FieldError fieldEndDate = result.getFieldError("rebEndDate");
			if(fieldEndDate == null){
				compDate = Utility.compareCurrentDate(rebatesVo.getRebEndDate(), currentDate);
				if (null != compDate)
				{

					rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEENDCURRENT);
				}
				else
				{

					compDate = Utility.compareDate(rebatesVo.getRebStartDate(), rebatesVo.getRebEndDate());

					if (null != compDate)
					{

						rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEAFTER);
					}
				}
			}
			

			if (result.hasErrors()) 
			{
				view = VIEW_NAME;

			} else 
			{
				isDataInserted = retailerService.addRetRerunRebates(rebatesVo);
				if (isDataInserted
						.equalsIgnoreCase(ApplicationConstants.FAILURE)
						|| isDataInserted == null) 
				{

					view = VIEW_NAME;
					result.reject("rerunRebate.error");

				} else 
				{
					view = "rebatesretailMfg.htm";

				}
			}
		} catch (ScanSeeServiceException e) 
		{
			LOG.error("Exception occured in editRebates service method");
			result.reject("rerunRebate.error");
			return new ModelAndView(view);
		}
		catch (NullPointerException e) 
		{
			LOG.error("null pointer Exception occured in editRebates service method");
			result.reject("rerunRebate.error");
			return new ModelAndView(view);
		}
		LOG.info("editRebates:: Exiting POST Method");
		if (view.equals(VIEW_NAME)) 
		{
			return new ModelAndView(view);
			
		} else 
		{
			return new ModelAndView(new RedirectView(view));
		}

	}
	
	/**
	 * this method returns preview of rerunRebates.
	 * @param rebatesVo
	 *                Rebates instance as parameter.
	 * @param result
	 *              BindingResult instance as parameter.
	 * @param request
	 *              HttpServletRequest instance as parameter.
	 * @param response
	 *              HttpServletResponse instance as parameter.
	 * @param model
	 *              ModelMap instance as parameter.
	 * @param session
	 *              HttpSession instance as parameter.
	 * @return VIEW_NAME rerunRebates view.
	 * @throws ScanSeeServiceException.
	 */
	
	@RequestMapping(value = "/previewretailerrerunrebate.htm", method = RequestMethod.POST)
	public ModelAndView previewPage(@ModelAttribute("previewrerunretailerrebform") Rebates rebatesVo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{

		LOG.info("previewPage in ReRunRetailerRebatesContrller in Retailer:: Inside POST Method");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		List<Rebates> prodList =null; 
		String rebName = request.getParameter("rebName");
		String rebLongDescription = request.getParameter("rebLongDescription");
		session.removeAttribute("rerunProductNames");
		session.removeAttribute("rerunProductImagePath");

		
		request.getSession().setAttribute("reRunRebateName", rebName);
		request.getSession().setAttribute("reRunRebateLongDescription", rebLongDescription);
		
		rebatesVo.setRebName(rebName);
		rebatesVo.setRebLongDescription(rebLongDescription);
		try{
			prodList=retailerService.fetchProductInf(rebatesVo.getRebateID());
			if(prodList!=null && !prodList.isEmpty()){
				session.setAttribute("rerunProductNames", prodList.get(0).getProductNames());
				session.setAttribute("rerunProductImagePath", prodList.get(0).getImagePath());
		       }
		}
		catch(ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in  previewPage:::::"+ e.getMessage());
            throw new ScanSeeServiceException(e);
		}
		
		model.put("previewrerunretailerrebform", rebatesVo);
		LOG.info("previewPage in ReRunRetailerRebatesContrller :: Exiting POST Method");
		return new ModelAndView("rerunretailerrebpreview");

	}	
	
	
	
}

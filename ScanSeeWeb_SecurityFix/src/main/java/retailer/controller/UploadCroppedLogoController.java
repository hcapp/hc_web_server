package retailer.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.pojo.Event;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Coupon;
import common.pojo.HotDealInfo;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.RetailerUploadLogoInfo;
import common.pojo.SearchResultInfo;
import common.pojo.StoreInfoVO;
import common.pojo.Users;
import common.util.Utility;

/**
 * UploadCroppedLogoController is a controller class for cropping Image screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class UploadCroppedLogoController {
	private static final String UPLOAD_RETAILER_LOGO = "uploadRetailerLogo";

	private static final String UPLOAD_RETAILER_LOGO_DASHBOARD = "uploadRetailerLogoDash";
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(UploadCroppedLogoController.class);

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadCroppedLogo.htm", method = RequestMethod.POST)
	public String uploadCroppedLogo(@ModelAttribute("retaileruploadlogoinfoform") RetailerUploadLogoInfo logoInfo, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside UploadCroppedLogoController : uploadCroppedLogo");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		String view = null;
		String status = "FALSE";
		String strImageStatus = null;
		String imageName = null;
		CommonsMultipartFile retailerUploadLogoInfo = null;

		// Span-Dileep added
		if (logoInfo.getNavigation().equals("registration")) {
			view = UPLOAD_RETAILER_LOGO;

		} else
			if (logoInfo.getNavigation().equals("fromDashBoard")) {

				view = UPLOAD_RETAILER_LOGO_DASHBOARD;
			}

		/*
		 * strImageStatus = (String) session.getAttribute("croppedlogosrc"); if
		 * (Utility.checkNull(strImageStatus).equals("")) {
		 * session.setAttribute("croppedlogosrc",
		 * ApplicationConstants.UPLOADIMAGEPATH); }
		 */
		double parseX = Double.parseDouble(request.getParameter("x"));
		double parseY = Double.parseDouble(request.getParameter("y"));
		double parseW = Double.parseDouble(request.getParameter("w"));
		double parseH = Double.parseDouble(request.getParameter("h"));

		int xPixel = (int) parseX;
		int yPixel = (int) parseY;
		int wPixel = (int) parseW;
		int hPixel = (int) parseH;

		Map<String, CommonsMultipartFile> uploadedLogoInfo = (Map<String, CommonsMultipartFile>) session.getAttribute("uploadedLogoInfo");
		if (uploadedLogoInfo.size() == 1) {
			retailerUploadLogoInfo = uploadedLogoInfo.get(String.valueOf(loginUser.getRetailerId()));
		}

		try {

			String filePathImages = request.getRealPath("images");
			status = retailerService.processRetailerCroppedLogo(retailerUploadLogoInfo, filePathImages, loginUser, xPixel, yPixel, wPixel, hPixel);

			if (null != status) {
				/*
				 * session.setAttribute("logosrc", "/" +
				 * ApplicationConstants.IMAGES + "/" +
				 * ApplicationConstants.RETAILER + "/" +
				 * loginUser.getRetailerId() + "/" +
				 * retailerUploadLogoInfo.getRetailerLogo
				 * ().getOriginalFilename());
				 */

				// uploadedLogoInfo = new HashMap<String,
				// RetailerUploadLogoInfo>();
				// uploadedLogoInfo.put(String.valueOf(retailerUploadLogoInfo.getRetailerID()),
				// retailerUploadLogoInfo);
				imageName = status;
				session.setAttribute("uploadedLogoInfo", uploadedLogoInfo);
				LOG.info("Retailer Logo Successfully  uploaded....");
				Long retailerId = Long.valueOf(loginUser.getRetailerId());
				RetailerUploadLogoInfo retailerUploadLogo = new RetailerUploadLogoInfo();
				retailerUploadLogo.setRetailerID(retailerId);

				/*
				 * strImageStatus = (String) session.getAttribute("logosrc"); if
				 * (Utility.checkNull(strImageStatus).equals("")) {
				 * session.setAttribute("logosrc",
				 * ApplicationConstants.UPLOADIMAGEPATH); } else {
				 */

				// Span-Dileep Added. Passing timestamp in the image URL to load
				// changed image- IE browser fix
				Date date = new Date();

				strImageStatus = "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.RETAILER + "/" + loginUser.getRetailerId() + "/"
						+ imageName + "?" + date.getTime();
				// strImageStatus = strImageStatus + "?" + date.getTime();
				session.setAttribute("logosrc", strImageStatus);
				// }
				session.removeAttribute("croppedlogosrc");
				model.put("retaileruploadlogoinfoform", retailerUploadLogo);
			} else {
				session.setAttribute("logosrc", ApplicationConstants.UPLOADIMAGEPATH);
				LOG.info("Problem Occured when Retailer Logo upload....");
				throw new ScanSeeServiceException("Problem Occured when Retailer Logo upload");
			}

		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in  UploadCroppedLogoController:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("UploadCroppedLogoController:: Exit uploadCroppedLogo Method");
		return view;

	}

	@RequestMapping(value = "cropImage.htm", method = RequestMethod.GET)
	public String showRetailerLogoPopup(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException {

		LOG.info("showRetailerLogoPopup:: Inside Get Method");
		Users user = (Users) request.getSession().getAttribute("loginuser");
		Long retailerId = Long.valueOf(user.getRetailerId());
		RetailerUploadLogoInfo retailerUploadLogoInfo = new RetailerUploadLogoInfo();
		retailerUploadLogoInfo.setRetailerID(retailerId);
		LOG.info("showRetailerLogoPopup:: Inside Exit Get Method");
		return "imgCropPopup";

	}

	@RequestMapping(value = "cropImageGeneral.htm", method = RequestMethod.GET)
	public String cropImage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException {

		LOG.info("cropImage:: Inside Get Method");
		Users user = (Users) request.getSession().getAttribute("loginuser");
		Long retailerId = Long.valueOf(user.getRetailerId());
		RetailerUploadLogoInfo retailerUploadLogoInfo = new RetailerUploadLogoInfo();
		retailerUploadLogoInfo.setRetailerID(retailerId);
		LOG.info("cropImage: Inside Exit Get Method");
		return "imageCropGenearlPopup";

	}

	public String saveCroppedImage(HttpServletRequest request, HttpServletResponse response, HttpSession session, CommonsMultipartFile imageFile)
			throws ScanSeeServiceException {
		LOG.info("Inside saveCroppedImage");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");

		String status = "FALSE";
		double parseX = Double.parseDouble(request.getParameter("x"));
		double parseY = Double.parseDouble(request.getParameter("y"));
		double parseW = Double.parseDouble(request.getParameter("w"));
		double parseH = Double.parseDouble(request.getParameter("h"));
		int xPixel = (int) parseX;
		int yPixel = (int) parseY;
		int wPixel = (int) parseW;
		int hPixel = (int) parseH;

		try {

			String filePathImages = request.getRealPath("images");
			status = retailerService.processRetailerCroppedImage(imageFile, filePathImages, loginUser, xPixel, yPixel, wPixel, hPixel);

		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in  UploadCroppedLogoController:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return status;
	}

	@RequestMapping(value = "/uploadCroppedBannerAd.htm", method = RequestMethod.POST)
	public String uploadCroppedImage(@ModelAttribute("createbanneradform") RetailerLocationAdvertisement retailerLocationAds,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		String status = "FALSE";
		retailerLocationAds.setRetailLocationIDHidden(retailerLocationAds.getRetailLocationIds());
		String imageSource = null;
		retailerLocationAds.setStrBannerAdImagePath(retailerLocationAds.getBannerAdImagePath().getOriginalFilename());

		status = saveCroppedImage(request, response, session, retailerLocationAds.getBannerAdImagePath());
		String imageName = null;

		Date date = new Date();
		if (null != status) {
			String strImageStatus = (String) session.getAttribute("cropImageSource");

			imageSource = status;
			if (null != imageSource) {

				// imageSource = FilenameUtils.removeExtension(imageSource);
				// imageSource = imageSource + ".png";
				retailerLocationAds.setStrBannerAdImagePath(imageSource);

			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("welcomePageCreateImgPath", ApplicationConstants.UPLOADIMAGEPATH);
					session.setAttribute("welcomePageBtnVal", "WelcomePageAds");

				} else {

					imageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + imageSource + "?" + date.getTime();
					session.setAttribute("welcomePageCreateImgPath", imageName);
					session.setAttribute("welcomePageBtnVal", "WelcomePageAds");

				}

			}

			model.put("createbanneradform", retailerLocationAds);
		} else {
			retailerLocationAds.setStrBannerAdImagePath(null);
			session.setAttribute("logosrc", ApplicationConstants.UPLOADIMAGEPATH);
			LOG.info("Problem Occured when Retailer Logo upload....");
		}
		return "createBannerAdd";
	}

	@RequestMapping(value = "/editBannerAdImage.htm", method = RequestMethod.POST)
	public String uploadCroppedImageEditPage(@ModelAttribute("editManageAdsForm") RetailerLocationAdvertisement retailerLocationAds,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		String status = "FALSE";
		retailerLocationAds.setRetailLocationIDHidden(retailerLocationAds.getRetailLocationIds());
		String imageSource = null;
		String imageName = null;
		Date date = new Date();
		status = saveCroppedImage(request, response, session, retailerLocationAds.getBannerAdImagePath());

		if (null != status) {
			imageSource = status;
			if (null != imageSource) {

				// imageSource = FilenameUtils.removeExtension(imageSource);
				// imageSource = imageSource + ".png";
				retailerLocationAds.setStrBannerAdImagePath(imageSource);

			}
			String strImageStatus = (String) session.getAttribute("cropImageSource");

			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("welcomePageCreateImgPath", ApplicationConstants.UPLOADIMAGEPATH);
					session.setAttribute("welcomePageBtnVal", "WelcomePageAds");
				} else {
					imageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + imageSource + "?" + date.getTime();
					session.setAttribute("welcomePageCreateImgPath", imageName);
					session.setAttribute("welcomePageBtnVal", "WelcomePageAds");

				}

			}

			model.put("editManageAdsForm", retailerLocationAds);
		} else {
			retailerLocationAds.setStrBannerAdImagePath(null);
			session.setAttribute("welcomePageCreateImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			LOG.info("Problem Occured when Retailer Logo upload....");
		}
		return "editWelcomePage";
	}

	@RequestMapping(value = "/uploadCroppedImage.htm", method = RequestMethod.POST)
	public String uploadCroppedAnythinbgPageImage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		String status = "FALSE";
		String imageName = null;
		Date date = new Date();
		String imageSource = null;
		retailerCustomPage.setHiddenLocId(retailerCustomPage.getRetCreatedPageLocId());
		status = saveCroppedImage(request, response, session, retailerCustomPage.getRetImage());
		if (null != status) {
			String strImageStatus = (String) session.getAttribute("cropImageSource");

			imageSource = status;
			if (null != imageSource) {

				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png";
				retailerCustomPage.setRetailerImg(imageSource);

			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);

				} else {
					imageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + imageSource + "?" + date.getTime();
					session.setAttribute("customPageRetImgPath", imageName);

				}

			}

			model.put("createCustomPage", retailerCustomPage);
		} else {
			retailerCustomPage.setRetailerImg(null);
			session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			LOG.info("Problem Occured when Retailer Logo upload....");
		}
		return retailerCustomPage.getViewName();
	}

	@RequestMapping(value = "/editAnythinbgPageImage.htm", method = RequestMethod.POST)
	public String uploadEditCroppedAnythinbgPageImage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		String status = "FALSE";
		String imageName = null;
		Date date = new Date();
		String imageSource = null;
		retailerCustomPage.setHiddenLocId(retailerCustomPage.getRetCreatedPageLocId());
		status = saveCroppedImage(request, response, session, retailerCustomPage.getRetImage());
		if (null != status) {
			String strImageStatus = (String) session.getAttribute("cropImageSource");

			imageSource = status;
			if (null != imageSource) {

				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png";
				retailerCustomPage.setRetailerImg(imageSource);

			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);

				} else {
					imageName = "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource + "?" + date.getTime();
					session.setAttribute("customPageRetImgPath", imageName);

				}

			}

			model.put("createCustomPage", retailerCustomPage);
		} else {
			retailerCustomPage.setRetailerImg(null);
			session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			LOG.info("Problem Occured when Retailer Logo upload....");
		}
		return retailerCustomPage.getViewName();
	}

	@RequestMapping(value = "/uploadCroppedBannderAdImage.htm", method = RequestMethod.POST)
	public String uploadCroppeBannedAdPageImage(@ModelAttribute("buildbanneradform") RetailerLocationAdvertisement objRetLocAds,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		String status = "FALSE";
		String imageName = null;
		Date date = new Date();
		String imageSource = null;
		objRetLocAds.setRetailLocationIDHidden(objRetLocAds.getRetailLocationIds());
		session.setAttribute("ChangeImageDim", "true");
		status = saveCroppedImage(request, response, session, objRetLocAds.getBannerAdImagePath());
		if (null != status) {
			String strImageStatus = (String) session.getAttribute("cropImageSource");

			imageSource = status;
			if (null != imageSource) {

				// imageSource = FilenameUtils.removeExtension(imageSource);
				// imageSource = imageSource + ".png";
				objRetLocAds.setStrBannerAdImagePath(imageSource);

			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("bannerImagePath", ApplicationConstants.UPLOADIMAGEPATH);

				} else {
					imageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + imageSource + "?" + date.getTime();
					session.setAttribute("bannerImagePath", imageName);

				}

			}

			model.put("buildbanneradform", objRetLocAds);
		} else {
			objRetLocAds.setStrBannerAdImagePath(null);
			session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			LOG.info("Problem Occured when Retailer Logo upload....");
		}
		return objRetLocAds.getViewName();
	}

	@RequestMapping(value = "/uploadEditCroppedBannderAdImage.htm", method = RequestMethod.POST)
	public String uploadEditCroppeBannedAdPageImage(@ModelAttribute("editBannerAdsForm") RetailerLocationAdvertisement objRetLocAds,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		String status = "FALSE";
		String imageName = null;
		Date date = new Date();
		String imageSource = null;
		objRetLocAds.setRetailLocationIDHidden(objRetLocAds.getRetailLocationIds());
		status = saveCroppedImage(request, response, session, objRetLocAds.getBannerAdImagePath());
		if (null != status) {
			String strImageStatus = (String) session.getAttribute("cropImageSource");

			imageSource = status;
			if (null != imageSource) {

				// imageSource = FilenameUtils.removeExtension(imageSource);
				// imageSource = imageSource + ".png";
				objRetLocAds.setStrBannerAdImagePath(imageSource);

			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("bannerADImage", ApplicationConstants.UPLOADIMAGEPATH);

				} else {
					imageName = "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource + "?" + date.getTime();
					session.setAttribute("bannerADImage", imageName);

				}

			}

			model.put("editBannerAdsForm", objRetLocAds);
		} else {
			objRetLocAds.setStrBannerAdImagePath(null);
			session.setAttribute("bannerADImage", ApplicationConstants.UPLOADIMAGEPATH);
			LOG.info("Problem Occured when Retailer Logo upload....");
		}
		return objRetLocAds.getViewName();
	}

	@RequestMapping(value = "/uploadCroppedSplOffImage.htm", method = RequestMethod.POST)
	public String uploadCroppedSplPageImage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		String status = "FALSE";
		String imageName = null;
		Date date = new Date();
		String imageSource = null;
		retailerCustomPage.setHiddenLocId(retailerCustomPage.getSplOfferLocId());
		status = saveCroppedImage(request, response, session, retailerCustomPage.getRetImage());
		if (null != status) {
			String strImageStatus = (String) session.getAttribute("cropImageSource");

			imageSource = status;
			if (null != imageSource) {

				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png";
				retailerCustomPage.setRetailerImg(imageSource);

			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);

				} else {
					imageName = "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource + "?" + date.getTime();
					session.setAttribute("customPageRetImgPath", imageName);

				}

			}

			model.put("createCustomPage", retailerCustomPage);
		} else {
			retailerCustomPage.setRetailerImg(null);
			session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			LOG.info("Problem Occured when Retailer Logo upload....");
		}
		return retailerCustomPage.getViewName();
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("DealStartHours")
	public Map<String, String> populateDealStartHrs() throws ScanSeeServiceException {
		HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapDealStartHrs.put("0" + i, "0" + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	@SuppressWarnings("rawtypes")
	@ModelAttribute("DealStartMinutes")
	public Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException {
		HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapDealStartHrs.put("0" + i, "0" + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException {
		List list = new LinkedList(unsortMap.entrySet());
		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
			}
		});
		// put sorted list into map again
		Map sortedMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * This controller method will crop and upload Add Hot deal image.
	 * 
	 * @param objHotDealAdd
	 *            HotDealInfo instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppedaddhotdealimg.htm", method = RequestMethod.POST)
	public String uploadCropAddHotDealImage(@ModelAttribute("addhotdealretailerform") HotDealInfo objHotDealAdd, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside UploadCroppedLogoController : uploadCropAddHotDealImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		session.removeAttribute("hotdeallocation");
		session.removeAttribute("hotdealcity");
		if ("City".equals(objHotDealAdd.getDealForCityLoc())) {
			request.getSession().setAttribute("hotdealcity", objHotDealAdd.getCity());
			session.removeAttribute("hotdeallocation");
		} else
			if ("Location".equals(objHotDealAdd.getDealForCityLoc())) {
				session.removeAttribute("hotdealcity");
				session.setAttribute("hotdeallocation", String.valueOf(objHotDealAdd.getRetailerLocID()));
			}
		objHotDealAdd.setDealImgPath(objHotDealAdd.getImageFile().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objHotDealAdd.getImageFile());
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objHotDealAdd.setDealImgPath(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("addHotDealImagePath", strImageName);
				}
			}
		} else {
			objHotDealAdd.setDealImgPath(null);
			session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		model.put("addhotdealretailerform", objHotDealAdd);
		return objHotDealAdd.getViewName();
	}

	/**
	 * This controller method will crop and upload edit Hot deal image.
	 * 
	 * @param objHotDealEdit
	 *            HotDealInfo instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppededithotdealimg.htm", method = RequestMethod.POST)
	public String uploadCropEditHotDealImage(@ModelAttribute("edithotdealretailerform") HotDealInfo objHotDealEdit, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside UploadCroppedLogoController : uploadCropEditHotDealImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		session.removeAttribute("hotdeallocation");
		session.removeAttribute("hotdealcity");
		if ("City".equals(objHotDealEdit.getDealForCityLoc())) {
			request.getSession().setAttribute("hotdealcity", objHotDealEdit.getCity());
			session.removeAttribute("hotdeallocation");
		} else
			if ("Location".equals(objHotDealEdit.getDealForCityLoc())) {
				session.removeAttribute("hotdealcity");
				session.setAttribute("hotdeallocation", String.valueOf(objHotDealEdit.getRetailerLocID()));
			}
		objHotDealEdit.setDealImgPath(objHotDealEdit.getImageFile().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objHotDealEdit.getImageFile());
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objHotDealEdit.setDealImgPath(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("addHotDealImagePath", strImageName);
				}
			}
		} else {
			objHotDealEdit.setDealImgPath(null);
			session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		model.put("edithotdealretailerform", objHotDealEdit);
		return objHotDealEdit.getViewName();
	}

	/**
	 * This controller method will crop and upload rerun Hot deal image.
	 * 
	 * @param objHotDealRerun
	 *            HotDealInfo instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppedrerunhotdealimg.htm", method = RequestMethod.POST)
	public String uploadCropReRunHotDealImage(@ModelAttribute("retailerHotDealsReRunForm") HotDealInfo objHotDealRerun, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside UploadCroppedLogoController : uploadCropReRunHotDealImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		session.removeAttribute("hotdeallocation");
		session.removeAttribute("hotdealcity");
		if ("City".equals(objHotDealRerun.getDealForCityLoc())) {
			request.getSession().setAttribute("hotdealcity", objHotDealRerun.getCity());
			session.removeAttribute("hotdeallocation");
		} else
			if ("Location".equals(objHotDealRerun.getDealForCityLoc())) {
				session.removeAttribute("hotdealcity");
				session.setAttribute("hotdeallocation", String.valueOf(objHotDealRerun.getRetailerLocID()));
			}
		objHotDealRerun.setDealImgPath(objHotDealRerun.getImageFile().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objHotDealRerun.getImageFile());
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objHotDealRerun.setDealImgPath(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("addHotDealImagePath", strImageName);
				}
			}
		} else {
			objHotDealRerun.setDealImgPath(null);
			session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		model.put("retailerHotDealsReRunForm", objHotDealRerun);
		return objHotDealRerun.getViewName();
	}

	/**
	 * This controller method will crop and upload add Coupon image.
	 * 
	 * @param objCouponAdd
	 *            Coupon instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppedaddcouponimg.htm", method = RequestMethod.POST)
	public String uploadCropAddCouponImage(@ModelAttribute("createcouponform") Coupon objCouponAdd, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside UploadCroppedLogoController : uploadCropAddCouponImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		if (!"".equals(Utility.checkNull(objCouponAdd.getLocationID()))) {
			objCouponAdd.setRetLocationIDs(objCouponAdd.getLocationID());
		}
		objCouponAdd.setCouponImgPath(objCouponAdd.getImageFile().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objCouponAdd.getImageFile());
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objCouponAdd.setCouponImgPath(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("couponImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("couponImagePath", strImageName);
				}
			}
		} else {
			objCouponAdd.setCouponImgPath(null);
			session.setAttribute("couponImagePath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		model.put("createcouponform", objCouponAdd);
		return objCouponAdd.getViewName();
	}

	/**
	 * This controller method will crop and upload edit Coupon image.
	 * 
	 * @param objCouponEdit
	 *            Coupon instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppededitcouponimg.htm", method = RequestMethod.POST)
	public String uploadCropEditCouponImage(@ModelAttribute("editcouponform") Coupon objCouponEdit, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside UploadCroppedLogoController : uploadCropEditCouponImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		if (!"".equals(Utility.checkNull(objCouponEdit.getLocationID()))) {
			objCouponEdit.setRetLocationIDs(objCouponEdit.getLocationID());
		}
		objCouponEdit.setCouponImgPath(objCouponEdit.getImageFile().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objCouponEdit.getImageFile());
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objCouponEdit.setCouponImgPath(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("couponImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("couponImagePath", strImageName);
				}
			}
		} else {
			objCouponEdit.setCouponImgPath(null);
			session.setAttribute("couponImagePath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		model.put("editcouponform", objCouponEdit);
		return objCouponEdit.getViewName();
	}

	/**
	 * This controller method will crop and upload rerun Coupon image.
	 * 
	 * @param objCouponRerun
	 *            Coupon instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppedreruncouponimg.htm", method = RequestMethod.POST)
	public String uploadCropReRunCouponImage(@ModelAttribute("reruncouponform") Coupon objCouponRerun, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside UploadCroppedLogoController : uploadCropReRunCouponImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		session.setAttribute("ChangeImageDim", "true");
		if (!"".equals(Utility.checkNull(objCouponRerun.getLocationID()))) {
			objCouponRerun.setRetLocationIDs(objCouponRerun.getLocationID());
		}
		objCouponRerun.setCouponImgPath(objCouponRerun.getImageFile().getOriginalFilename());
		status = saveCroppedImage(request, response, session, objCouponRerun.getImageFile());
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objCouponRerun.setCouponImgPath(strImageSource);
			}
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("couponImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("couponImagePath", strImageName);
				}
			}
		} else {
			objCouponRerun.setCouponImgPath(null);
			session.setAttribute("couponImagePath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		model.put("reruncouponform", objCouponRerun);
		return objCouponRerun.getViewName();
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("CouponStartHrs")
	public Map<String, String> populateCouponStartHrs() throws ScanSeeServiceException {
		HashMap<String, String> mapCouponStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapCouponStartHrs.put("0" + i, "0" + i);
			} else {
				mapCouponStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		Iterator iterator = mapCouponStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapCouponStartHrs);
		return sortedMap;
	}

	@SuppressWarnings("rawtypes")
	@ModelAttribute("CouponStartMin")
	public Map<String, String> populateCouponStartMin() throws ScanSeeServiceException {
		HashMap<String, String> mapCouponStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapCouponStartHrs.put("0" + i, "0" + i);
				i = i + 4;
			} else {
				mapCouponStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		Iterator iterator = mapCouponStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapCouponStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method will crop and AddLocation image.
	 * 
	 * @param objStoreInfoVO
	 *            StoreInfoVO instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppedaddlocationimg.htm", method = RequestMethod.POST)
	public String uploadCropAddLocationImage(@ModelAttribute("addlocationform") StoreInfoVO objStoreInfoVO, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		LOG.info("Inside UploadCroppedLogoController : uploadCropAddLocationImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		
		session.setAttribute("ChangeImageDim", "true");

		objStoreInfoVO.setLocationImgPath(objStoreInfoVO.getImageFile().getOriginalFilename());
		
		status = saveCroppedImage(request, response, session, objStoreInfoVO.getImageFile());
		
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			
			if (null != strImageSource) {
				objStoreInfoVO.setLocationImgPath(strImageSource);
			}
			
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("locationImgPath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("locationImgPath", strImageName);
				}
			}
			
		} else {
			objStoreInfoVO.setLocationImgPath(null);
			session.setAttribute("locationImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		
		model.put("addlocationform", objStoreInfoVO);
		return objStoreInfoVO.getViewName();
	}

	/**
	 * This controller method will crop and Upload Location image.
	 * 
	 * @param objRetLocation
	 *            RetailerLocation instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppedlocationimg.htm", method = RequestMethod.POST)
	public String uploadCropLocationImage(@ModelAttribute("locationsetupform") RetailerLocation objRetLocation, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		LOG.info("Inside UploadCroppedLogoController : uploadCropLocationImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		RetailerLocation objRetLocation1 = null;
		
		session.setAttribute("ChangeImageDim", "true");

		int rowIndex = objRetLocation.getRowIndex();
		final String locationJson = objRetLocation.getProdJson();

		List<RetailerLocation> arLocationList1 = Utility.jsonToObjectList(locationJson);
		objRetLocation1 = (RetailerLocation) arLocationList1.get(rowIndex);
		
		status = saveCroppedImage(request, response, session, objRetLocation.getImageFile()[rowIndex]);

		if (!"".equals(Utility.checkNull(status))) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			
			if (null != strImageSource) {
				objRetLocation1.setGridImgLocationPath(strImageSource);
			}
			
			if (!"".equals(Utility.checkNull(strImageStatus))) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					objRetLocation1.setGridImgLocationPath(null);
					objRetLocation1.setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					objRetLocation1.setImgLocationPath(strImageName);
					objRetLocation1.setUploadImage(true);
				}
			}
			
		} else {
			objRetLocation1.setGridImgLocationPath(null);
			objRetLocation1.setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
		}

		/*
		 * if (null != arLocationList2 && !arLocationList2.isEmpty()) { for (int
		 * i = 0; i < arLocationList2.size(); i++) { if
		 * (arLocationList2.get(i).getUploadRetaillocationsID
		 * ().equals(objRetLocation1.getUploadRetaillocationsID())) {
		 * arLocationList2.set(i, objRetLocation1); } } }
		 */

		arLocationList1.remove(rowIndex);
		arLocationList1.add(rowIndex, objRetLocation1);
		
		SearchResultInfo locResult = new SearchResultInfo();
		locResult.setLocationList(arLocationList1);
		session.setAttribute("locationlist", locResult);
		final RetailerLocation retailLocation = new RetailerLocation();
		model.put("locationsetupform", retailLocation);
		
		return objRetLocation.getViewName();
	}
	
	
	
	
	/**
	 * This controller method will crop and Manage Location image.
	 * 
	 * @param objRetLocation
	 *            RetailerLocation instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/managecroppedlocationimg.htm", method = RequestMethod.POST)
	public String manageCropLocationImage(@ModelAttribute("locationform") RetailerLocation objRetLocation, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		LOG.info("Inside UploadCroppedLogoController : manageCropLocationImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		RetailerLocation objRetLocation1 = null;
		
		session.setAttribute("ChangeImageDim", "true");
		
		int rowIndex = objRetLocation.getRowIndex();
		final String locationJson = objRetLocation.getProdJson();
		List<RetailerLocation> arLocationList1 = Utility.jsonToObjectList(locationJson);
		objRetLocation1 = (RetailerLocation) arLocationList1.get(rowIndex);
		
		status = saveCroppedImage(request, response, session, objRetLocation.getImageFile()[rowIndex]);

		if (!"".equals(Utility.checkNull(status))) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			if (null != strImageSource) {
				objRetLocation1.setGridImgLocationPath(strImageSource);
			}
			
			if (!"".equals(Utility.checkNull(strImageStatus))) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					objRetLocation1.setGridImgLocationPath(null);
					objRetLocation1.setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					objRetLocation1.setImgLocationPath(strImageName);
					objRetLocation1.setUploadImage(true);
				}
			}
			
		} else {
			objRetLocation1.setGridImgLocationPath(null);
			objRetLocation1.setImgLocationPath(ApplicationConstants.UPLOADGRIDIMAGE);
		}

		arLocationList1.remove(rowIndex);
		arLocationList1.add(rowIndex, objRetLocation1);
		
		SearchResultInfo locResult = new SearchResultInfo();
		locResult.setLocationList(arLocationList1);
		request.setAttribute("locResult", locResult);
		
		final RetailerLocation retailLocation = new RetailerLocation();
		model.put("locationform", retailLocation);
		
		return objRetLocation.getViewName();
	}
	
	
	
	
	/**
	 * This ModelAttribute sort Event start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("StartMinutes")
	public Map<String, String> populateEventStartMins() throws ScanSeeServiceException {
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}

		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort Event start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("StartHours")
	public Map<String, String> populateEventStartHrs() throws ScanSeeServiceException {
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}
	
	
	/**
	 * This controller method will crop for Add Event image.
	 * 
	 * @param objAddEditEvent
	 *            Event instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppedaddeditevent.htm", method = RequestMethod.POST)
	public String uploadCropAddEditEventImage(@ModelAttribute("addediteventform")Event objAddEditEvent, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		LOG.info("Inside UploadCroppedLogoController : uploadCropAddEditEventImage");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		
		session.setAttribute("ChangeImageDim", "true");

		objAddEditEvent.setEventImageName(objAddEditEvent.getEventImageFile().getOriginalFilename());
		
		status = saveCroppedImage(request, response, session, objAddEditEvent.getEventImageFile());
		
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			
			if (null != strImageSource) {
				objAddEditEvent.setEventImageName(strImageSource);
			}
			
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("eventImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("eventImagePath", strImageName);
					objAddEditEvent.setUploadedImage(true);
				}
			}
			
		} else {
			objAddEditEvent.setEventImageName(null);
			session.setAttribute("eventImagePath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		
		model.put("addediteventform", objAddEditEvent);
		return objAddEditEvent.getViewName();
	}
	
	/**
	 * This controller method will crop for Add Event image.
	 * 
	 * @param objAddEditEvent
	 *            Event instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return String given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcroppedaddeditfund.htm", method = RequestMethod.POST)
	public String uploadCropAddEditFundraiserImage(@ModelAttribute("addeditfundraiserform")Event objAddEditEvent, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		
		LOG.info("Inside UploadCroppedLogoController : uploadCropAddEditFundraiserImage() ");
		String status = "FALSE";
		String strImageName = null;
		Date date = new Date();
		String strImageSource = null;
		String strImageStatus = null;
		
		session.setAttribute("ChangeImageDim", "true");
		objAddEditEvent.setEventImageName(objAddEditEvent.getEventImageFile().getOriginalFilename());
		objAddEditEvent.setHidLongDescription(objAddEditEvent.getLongDescription());
		status = saveCroppedImage(request, response, session, objAddEditEvent.getEventImageFile());
		if (null != status) {
			strImageStatus = (String) session.getAttribute("cropImageSource");
			strImageSource = status;
			
			if (null != strImageSource) {
				objAddEditEvent.setEventImageName(strImageSource);
			}
			
			if (null != strImageStatus) {
				int dotIndex = strImageStatus.lastIndexOf('.');
				if (dotIndex == -1) {
					session.setAttribute("fundraiserImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					strImageName = strImageStatus.substring(0, strImageStatus.lastIndexOf("/") + 1) + strImageSource + "?" + date.getTime();
					session.setAttribute("fundraiserImagePath", strImageName);
					objAddEditEvent.setUploadedImage(true);
				}
			}
		} else {
			objAddEditEvent.setEventImageName(null);
			session.setAttribute("fundraiserImagePath", ApplicationConstants.UPLOADIMAGEPATH);
		}
		
		model.put("addediteventform", objAddEditEvent);
		return objAddEditEvent.getViewName();
	}
}
package retailer.controller;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.authorize.aim.Transaction;
import net.authorize.data.Customer;
import net.authorize.xml.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.AccountType;
import common.pojo.PlanInfo;
import common.pojo.RetailerProfile;
import common.pojo.State;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class ChoosePlanRetailerController
{
	private static final String	PAYMENT_TYPE	= "paymentType";

	private static final String	YEARLY_PAYMENT_PRICE_VALUE	= "300.00";

	private static final String	YEARLY_PAYMENT_PRICE	= "yearlyPaymentPrice";

	private static final String	MONTHLY_PAYMENT_PRICE	= "monthlyPaymentPrice";

	private static final String	MONTHLY_PAYMENT_PRICE_VALUE	= "30.00";

	private static final String	YEARLY	= "yearly";

	private static final String	MONTHLY	= "monthly";

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ChoosePlanRetailerController.class);
	
	private static final String	UNITED_STATES	= "United States";

	@RequestMapping(value = "/retailerchoosePlan.htm", method = RequestMethod.GET)
	public ModelAndView retailerChoosePlan(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside ChoosePlanRetailerController : retailerChoosePlan ");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		ArrayList<PlanInfo> PlanInfoList = null;
		model.addAttribute("myform", new PlanInfo());

		ArrayList<State> statesList = null;
		ArrayList<AccountType> accountTypeList = null;

		PlanInfo plnInfo = new PlanInfo();
		plnInfo.setSelectedTab(ApplicationConstants.ACHBNKTAB);
		session.setAttribute("selectedTab", ApplicationConstants.ACHBNKTAB);
		model.addAttribute("myform", plnInfo);
		Number priceNumber = null;
		NumberFormat df = DecimalFormat.getInstance();
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(2);
		try
		{
			PlanInfoList = retailerService.getAllPlanList();
			for (PlanInfo planInfo : PlanInfoList) 
			{
				if(!Utility.isEmptyOrNullString(planInfo.getPrice())) 
				{
					priceNumber = df.parse(planInfo.getPrice());
					planInfo.setPrice((df.format(priceNumber)));
				}
			}
			statesList = retailerService.getAllStates();
			accountTypeList = retailerService.getAllAcountType();
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in retailerChoosePlan:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (ParseException e) {
			LOG.error("Exception occurred in retailerChoosePlan:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		session.setAttribute("statesList", statesList);
		session.setAttribute("accountTypes", accountTypeList);
		session.setAttribute("planinfolist", PlanInfoList);
		session.setAttribute(MONTHLY_PAYMENT_PRICE, MONTHLY_PAYMENT_PRICE_VALUE);
		session.setAttribute(YEARLY_PAYMENT_PRICE, YEARLY_PAYMENT_PRICE_VALUE);

		return new ModelAndView("retailerchoosePlan");
	}

	@RequestMapping(value = "/retailerplanInfo.htm", method = RequestMethod.POST)
	public ModelAndView retailerPlanInfo(@ModelAttribute("retchooseplanform") PlanInfo planInfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside ChoosePlanRetailerController : retailerPlanInfo ");
		ArrayList<PlanInfo> PlanInfoList = (ArrayList<PlanInfo>) session.getAttribute("planinfolist");
		String totalPrice = planInfo.getTotal();
		String[] arraytotalPrice = totalPrice.split(",");
		List<String> totalPriceList = Arrays.asList(arraytotalPrice);
		List<PlanInfo> pInfoList = Utility.jsonToPlanObject(planInfo.getPlanJson());
		String selTab = planInfo.getSelectedTab();
		if (pInfoList != null)
		{
			for (Iterator iterator = PlanInfoList.iterator(); iterator.hasNext();)
			{
				PlanInfo planInfo2 = (PlanInfo) iterator.next();

				for (PlanInfo obj : pInfoList)
				{
					if (obj.getProductID().equals(planInfo2.getProductId()))
					{
						planInfo2.setQuantity(obj.getQuantity());
						planInfo2.setSubTotal(obj.getSubTotal());
						planInfo2.setPrice(obj.getPrice());
					}
				}
			}
			// session.setAttribute("quantitylist", quantityList);
			String paymentType = request.getParameter("paymentType");
			session.setAttribute(PAYMENT_TYPE, paymentType);
			
			// Calculate Tax 
			if(!Utility.isEmptyOrNullString(planInfo.getGrandTotal()))
			{
				final String taxAmount = Utility.calculateTotalWithTax(planInfo.getGrandTotal());
				session.setAttribute("taxAmount",taxAmount);
				double grandTotalWithTax = Double.parseDouble(planInfo.getGrandTotal()) + Double.parseDouble(taxAmount);
				planInfo.setGrandTotal(String.valueOf(grandTotalWithTax));
			}
			else
			{
				session.setAttribute("taxAmount","");
			}
			
			session.setAttribute("totalPricelist", planInfo.getGrandTotal());
			session.setAttribute("planInfolist", PlanInfoList);
			if (null != selTab && selTab.equals(ApplicationConstants.CREDITCARDPAY))
			{
				session.setAttribute("selectedTab", ApplicationConstants.CREDITCARDPAY);
			}
			else if (null != selTab && selTab.equals(ApplicationConstants.ACHBNKTAB))
			{
				session.setAttribute("selectedTab", ApplicationConstants.ACHBNKTAB);
			}
			else
			{
				session.setAttribute("selectedTab", ApplicationConstants.ACHBNKTAB);
			}
			PlanInfo plnInfo = new PlanInfo();
			model.addAttribute("myform", plnInfo);
			return new ModelAndView("retailerPlanConfirmation");
		}
		return new ModelAndView("retailerPlanConfirmation");
	}

	@RequestMapping(value = "/fetchretailerdiscount", method = RequestMethod.GET)
	public @ResponseBody
	String getDiscount(@RequestParam(value = "discountcode", required = true) String discountCode, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside ChoosePlanRetailerController : getDiscount ");
		Double discountValue = 0.00;
		StringBuilder innerHtml = new StringBuilder();
		PlanInfo planInfo = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			planInfo = retailerService.getDiscountValue(discountCode);
			discountValue = planInfo.getDiscount();
			Integer discountPlanRetailerID = planInfo.getDiscountPlanRetailerID();
			response.setContentType("text/xml");

			innerHtml.append("<input readonly='readonly' name='discount' class='textbox' value='" + discountValue + "'/>");
			session.setAttribute("discountPlanRetailerID", discountPlanRetailerID);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in getDiscount:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		innerHtml.append("&" + discountValue);
		return innerHtml.toString();
	}

	@RequestMapping(value = "/retailerpaymentconfirm.htm", method = RequestMethod.POST)
	public ModelAndView confirmPayment(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside ChoosePlanRetailerController : confirmPayment ");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

		return new ModelAndView("retailerpaymentconfirm");
	}

	@RequestMapping(value = "/saveretailerplanInfo.htm", method = RequestMethod.POST)
	public ModelAndView savePlanInfo(@ModelAttribute("myForm") PlanInfo planInfo, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside ChoosePlanRetailerController : savePlanInfo ");
		ArrayList<PlanInfo> PlanInfoList = (ArrayList<PlanInfo>) session.getAttribute("planinfolist");

		String saveResponse = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		// Integer discountplanManufacturerid = (Integer)
		// session.getAttribute("discountplanManufacturerid");

		int retailerId = loginUser.getRetailerId();
		long userId = loginUser.getUserID();
		try
		{
			saveResponse = retailerService.savePlanInfo(PlanInfoList, retailerId, userId);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in savePlanInfo:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(new RedirectView("dashboard.htm"));
	}

	@RequestMapping(value = "/retloadTab.htm", method = RequestMethod.POST)
	public ModelAndView loadPaymentsTab(@ModelAttribute("retchooseplanform") PlanInfo planInfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside ChoosePlanRetailerController : loadPaymentsTab ");
		ArrayList<PlanInfo> PlanInfoList = (ArrayList<PlanInfo>) session.getAttribute("planinfolist");
		String selTab = planInfo.getSelectedTab();

		if (null != selTab && selTab.equals(ApplicationConstants.ACHBNKTAB))
		{
			session.setAttribute("selectedTab", ApplicationConstants.ACHBNKTAB);
		}
		else if (null != selTab && selTab.equals(ApplicationConstants.CREDITCARDPAY))
		{
			session.setAttribute("selectedTab", ApplicationConstants.CREDITCARDPAY);
		}
		else
		{
			session.setAttribute("selectedTab", ApplicationConstants.ALTERNATEPAYMENT);
		}
		return new ModelAndView("retailerPlanConfirmation");
	}
	
	/*
	 *  Meyy related code
	 */
	@RequestMapping(value = "/processRetailerCC", method = RequestMethod.POST)
	public @ResponseBody
	String processRetailerCreditCard(
			@RequestParam(value = "totalString", required = true) String totalString,
			HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException {

		LOG.info("Inside ChoosePlanController : processCreditCard ");

		String totalAmount = (String) session.getAttribute("totalPricelist");

		StringBuilder innerHtml = new StringBuilder();

		if (!Utility.isEmptyOrNullString(totalAmount)) 
		{
			if(!Utility.isEmptyOrNullString(totalString))
			{			
				String[] strCardArr = totalString.split(":");
	
				Users userInfo = (Users) session.getAttribute("loginuser");
				
				RetailerProfile retailerProfile = (RetailerProfile) session.getAttribute("retailerProfile");
				Customer authCustomer = Customer.createCustomer();
						
				
				authCustomer.setAddress(strCardArr[6]);
				authCustomer.setState(strCardArr[7]);
				authCustomer.setCity(strCardArr[8]);
				authCustomer.setZipPostalCode(strCardArr[9]);
				authCustomer.setPhone(strCardArr[10]);
				authCustomer.setCountry(UNITED_STATES);
				
				String paymentType = (String)session.getAttribute(PAYMENT_TYPE);				
								
				if(!"".equals(Utility.checkNull(strCardArr[4]))) {
					authCustomer.setFirstName(Utility.removeSpecialChars(strCardArr[4]));
				}				
				
				if(!"".equals(Utility.checkNull(strCardArr[5]))) {
					authCustomer.setLastName(Utility.removeSpecialChars(strCardArr[5]));
				}	
				
				if(null != retailerProfile)
				{
					if(!"".equals(Utility.checkNull(retailerProfile.getRetailerName()))) {
						authCustomer.setCompany(Utility.removeSpecialChars(retailerProfile.getRetailerName()));
					}
					
						
					if(!Utility.isEmptyOrNullString(retailerProfile.getContactEmail()))
						authCustomer.setEmail(retailerProfile.getContactEmail());
				}					
				
				if(null != userInfo)
				{
					if(null != userInfo.getCountryCode() && !Utility.isEmptyOrNullString(userInfo.getCountryCode().getCountryName()))
						authCustomer.setCountry(userInfo.getCountryCode().getCountryName());
					if(userInfo.getUserID() > 0)
						authCustomer.setCustomerId(userInfo.getUserID().toString());					
				}
				
			//	Result<Transaction> result = Utility.authenticateCreditCard(strCardArr,	totalAmount,authCustomer);      
				net.authorize.arb.Result<Transaction> result = Utility.authenticateARBCreditCard(strCardArr,totalAmount,authCustomer,paymentType);
	
			//	if (null != result && result.isApproved()) 
				if (null != result && result.isOk())
				{
					innerHtml.append("<br /><big><bold>Success!</bold></big></h2><br /><br />Payment Processed.<br /><br />Press 'Continue' to go to your Dashboard<br/>");
				}
				//else if (null != result && result.isDeclined()) {
				else if (null != result && result.isError()) {
					innerHtml.append("Credit Card Processing Declined!! \n");
					innerHtml.append("Please verify credit card details");
					//LOG.info("Error while processing credit card :"+ result.getResponseText());
					LOG.info("Error while processing credit card :"+ result.getResultCode());
					final ArrayList<Message> messages = result.getMessages();
					if(null != messages)
					{
						for (Message message : messages) {
							if(null != message)
							{
								LOG.info("Credit Card Processing Result Message Code:" + message.getCode());
								LOG.info("Credit Card Processing Result Message Text:" + message.getText());
							}
						}
					}
					
				}
				else
				{
					innerHtml.append("Error while processing credit card!!");	
					if(null != result)
					//	LOG.info("Error while processing credit card :"+ result.getResponseText());
						LOG.info("Error while processing credit card :"+ result.getResultCode());
				}
			}
		}
		else
		{
			innerHtml.append("Total amount to be processed is invalid!!");		
		}
		return innerHtml.toString();
	}

	@RequestMapping(value = "/fetchretailerdiscountMonYearly", method = RequestMethod.GET)
	public @ResponseBody
	String getRetailerDiscount(@RequestParam(value = "discountcode", required = true) String discountCode, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside ChoosePlanRetailerController : getRetailerDiscount for monthly and yearly plans ");
		Double discountValue = 0.00;
		StringBuilder innerHtml = new StringBuilder();
		
		try
		{
			if(!Utility.isEmptyOrNullString(discountCode))
			{
				ServletContext servletContext = request.getSession().getServletContext();
				WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
				RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
				final Map<String, Map<String, String>> discountPlans = retailerService.getRetailerDiscountPlans();
				//final Map<String, Map<String, String>> discountPlans = getDiscountPlans();
				String defaultMonthlyPrice = "";
				String defaultYearlyPrice = "";
				ArrayList<PlanInfo> PlanInfoList = (ArrayList<PlanInfo>) session.getAttribute("planinfolist");
				if(null != PlanInfoList && PlanInfoList.size() > 0)
				{
					PlanInfo planInfo = PlanInfoList.get(0);
					if(null != planInfo)
					{
						defaultMonthlyPrice = planInfo.getRetailerBillingMonthly();
						defaultYearlyPrice = planInfo.getRetailerBillingYearly();
					}
				}
			
				
				if(null != discountPlans && discountPlans.size() > 0)
				{
					if(discountPlans.containsKey(discountCode.toUpperCase()))
					{
						final Map<String, String> obj = discountPlans.get(discountCode.toUpperCase());
						final String yearlyPrice = obj.get(YEARLY);
						final String monthlyPrice = obj.get(MONTHLY);						
						if (Utility.isNumber(yearlyPrice) && Utility.isNumber(monthlyPrice)) 
						{							
							//final int discountedMonthlyPrice = Integer.parseInt(defaultMonthlyPrice) - Integer.parseInt(monthlyPrice);
							//final int discountedYearlyPrice = Integer.parseInt(defaultYearlyPrice) - Integer.parseInt(yearlyPrice);
							//if(discountedMonthlyPrice >= 0 && discountedYearlyPrice >= 0)
							//{
								innerHtml.append(defaultMonthlyPrice);
								innerHtml.append("&");
								innerHtml.append(defaultYearlyPrice);
								innerHtml.append("&");								
								innerHtml.append(monthlyPrice);
								innerHtml.append("&");								
								innerHtml.append(yearlyPrice);
							/*}else
							{
								throw new Exception("Discount Code is invalid!");
							}*/
						}
						else
						{
							innerHtml.append("skippayment");
						}
					}
					else
					{
						throw new Exception("Discount Code is invalid!");
					}
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Exception occurred in getDiscount:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("innerHtml.toString() : "+innerHtml.toString());
		return innerHtml.toString();
	}
	
	
	private Map<String, Map<String, String>> getDiscountPlans()
	{
		Map<String,Map<String,String>> discountPlanList = new HashMap<String, Map<String,String>>();

		// Discount 1
		Map<String, String> discountPlan = new HashMap<String, String>();
		discountPlan.put(MONTHLY, "20.00");
		discountPlan.put(YEARLY, "200.00");
		discountPlanList.put("SB00B", discountPlan);
		
		// Discount 2
		discountPlan = new HashMap<String, String>();
		discountPlan.put(MONTHLY,"10.00");
		discountPlan.put(YEARLY, "100.00");
		discountPlanList.put("SB00C", discountPlan);
		
		return discountPlanList;
		
	}
	
}

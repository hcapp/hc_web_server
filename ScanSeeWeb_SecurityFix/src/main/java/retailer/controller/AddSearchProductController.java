/**
 * @ (#) AddSearchProductController.java 02-Jan-2012
 * Project       :ScanSeeWeb
 * File          : AddSearchProductController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 02-Jan-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ProductVO;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationProduct;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.Utility;

/**
 * AddSearchProductController is a controller class for add and manage retailer
 * products screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class AddSearchProductController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AddSearchProductController.class);

	/**
	 * Variable returnView declared as constant string.
	 */
	private final String returnView = "addsearchprod";

	/**
	 * This controller method will display adding and searching Retailer Manage
	 * product screen by call service and DAO methods.
	 * 
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param model
	 *            ModelMap instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "addseachprod.htm", method = RequestMethod.GET)
	public final ModelAndView showProductSetupPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside AddSearchProductController : showProductSetupPage ");
		try
		{
			request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
			session.removeAttribute(ApplicationConstants.PAGINATION);
			session.removeAttribute("retLocList");
			session.removeAttribute("seacrhList");
			session.removeAttribute("searchFormProduct");
			session.removeAttribute("manageAddProd");
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final Long retailID = Long.valueOf(loginUser.getRetailerId());
			List<RetailerLocation> listRetLoc = null;

			final RetailerLocationProduct retailerLocation = new RetailerLocationProduct();
			model.put("retprodsetupform", retailerLocation);

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			// prodInfoList = retailerService.getAllProdInfo(null);
			listRetLoc = retailerService.getRetailerLocation(retailID);
			session.setAttribute("retLocList", listRetLoc);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(returnView);
	}

	/**
	 * This controller method will display all the products by searching in
	 * Retailer Manage product screen by call service and DAO methods.
	 * 
	 * @param objRetLctnProduct
	 *            RetailerLocationProduct instance as request parameter
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param result
	 *            BindingResult instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "addseachprod.htm", method = RequestMethod.POST)
	public final ModelAndView productSetupPage(@ModelAttribute("retprodsetupform") RetailerLocationProduct objRetLctnProduct, BindingResult result,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside AddSearchProductController : showProductSetupPage ");
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		String productName = null;
		int recordCount = 20;
		if (null != objRetLctnProduct.getRecordCount() && !"".equals(objRetLctnProduct.getRecordCount())
				&& !"undefined".equals(objRetLctnProduct.getRecordCount()))
		{
			recordCount = Integer.valueOf(objRetLctnProduct.getRecordCount());
		}

		final List<ProductVO> arProductVOList = new ArrayList<ProductVO>();
		try
		{
			request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
			session.removeAttribute("manageAddProd");
			session.setAttribute("manageAddProd", "addProduct");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			// prodInfoList = retailerService.getAllProdInfo(null);
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null == pageFlag)
			{
				productName = objRetLctnProduct.getProductName();
			}

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchFormProduct");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);

				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);

				}
				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));

			}
			if (objForm == null)
			{
				objForm = new SearchForm();
				if (null != productName)
				{
					objForm.setSearchKey(productName.trim());
				}
				else
				{
					objForm.setSearchKey(productName);
				}
				session.setAttribute("searchFormProduct", objForm);
			}
			final SearchResultInfo resultInfo = retailerService.searchProducts(objForm, lowerLimit, recordCount);
			if (resultInfo != null)
			{
				ProductVO productVO = null;
				for (int i = 0; i < resultInfo.getProductList().size(); i++)
				{
					productVO = resultInfo.getProductList().get(i);
					productVO.setRowNumber(i);
					arProductVOList.add(productVO);
				}
				resultInfo.setProductList(arProductVOList);
				session.setAttribute("seacrhList", resultInfo);
				final Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/addseachprod.htm",
						recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
			else
			{
				session.removeAttribute(ApplicationConstants.PAGINATION);
				session.removeAttribute("seacrhList");
				result.reject("addseachprod.error");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(returnView);
	}

	/**
	 * This controller method will display all the products associated with
	 * Retailer location by call service and DAO methods.
	 * 
	 * @param objRetLctnProduct
	 *            RetailerLocationProduct instance as request parameter
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param result
	 *            BindingResult instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "manageRetLocProd.htm", method = RequestMethod.POST)
	public final ModelAndView getRetailLocProdSearch(@ModelAttribute("retprodsetupform") RetailerLocationProduct objRetLctnProduct,
			BindingResult result, HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside  AddSearchProductController  : getRetailLocProdSearch ");
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		String productName = null;
		final List<ProductVO> productList = new ArrayList<ProductVO>();
		int recordCount = 20;
		try
		{
			session.removeAttribute("seacrhList");
			session.removeAttribute("manageAddProd");
			session.removeAttribute("searchFormProduct");
			request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final String pageFlag = (String) request.getParameter("pageFlag");
			session.setAttribute("manageAddProd", "manageProd");
			if (null != objRetLctnProduct.getLocID())
			{
				objRetLctnProduct.setRetailLocIDHidden(objRetLctnProduct.getLocID());
				session.setAttribute("retaileLocProd", objRetLctnProduct.getLocID());
			}
			if (null != (String) session.getAttribute("retaileLocProd"))
			{
				objRetLctnProduct.setLocID((String) session.getAttribute("retaileLocProd"));
			}
			if (null == pageFlag)
			{
				productName = objRetLctnProduct.getProductName();
			}
			if (null != objRetLctnProduct.getRecordCount() && !"".equals(objRetLctnProduct.getRecordCount())
					&& !"undefined".equals(objRetLctnProduct.getRecordCount()))
			{
				recordCount = Integer.valueOf(objRetLctnProduct.getRecordCount());
			}
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchFormProduct");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}
			}

			if (objForm == null)
			{
				objForm = new SearchForm();
				if (null != productName)
				{
					objForm.setSearchKey(productName.trim());
				}
				else
				{
					objForm.setSearchKey(productName);
				}
				session.setAttribute("searchFormProduct", objForm);
			}
			final SearchResultInfo resultInfo = retailerService.getRetailLocProd(objForm.getSearchKey(), lowerLimit, loginUser.getRetailerId(),
					objRetLctnProduct.getLocID(), recordCount);
			if (resultInfo != null)
			{
				ProductVO productVO = null;
				for (int i = 0; i < resultInfo.getProductList().size(); i++)
				{
					productVO = resultInfo.getProductList().get(i);
					if (!"".equals(Utility.checkNull(productVO.getSaleStartDate())))
					{
						productVO.setSaleStartDate(Utility.formattedDateWithTime(productVO.getSaleStartDate()));
					}
					if (!"".equals(Utility.checkNull(productVO.getSaleEndDate())))
					{
						productVO.setSaleEndDate(Utility.formattedDateWithTime(productVO.getSaleEndDate()));
					}
					productVO.setRowNumber(i);
					productList.add(productVO);
				}
				resultInfo.setProductList(productList);
				session.setAttribute("seacrhList", resultInfo);
				final Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/manageRetLocProd.htm",
						recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
			else
			{
				session.removeAttribute(ApplicationConstants.PAGINATION);
				session.removeAttribute("seacrhList");
				result.reject("managesearchprod.error");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(returnView);
	}

	/**
	 * This controller method will display product manage details in iphone
	 * screen formate.
	 * 
	 * @param objRetLtnProduct
	 *            instance of RetailerLocationProduct.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView addHotDeals view.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/previewretailprod.htm", method = RequestMethod.GET)
	public final ModelAndView previewManageProduct(@ModelAttribute("retprodsetupform") RetailerLocationProduct objRetLtnProduct,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside AddSearchProductController : previewManageProduct");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		List<RetailerLocationProduct> arRetLtnProductList = null;
		objRetLtnProduct.setRetailID((long) loginUser.getRetailerId());
		try
		{
			arRetLtnProductList = retailerService.getRetailerProductPreview(objRetLtnProduct);
			if (arRetLtnProductList != null && !arRetLtnProductList.isEmpty())
			{
				request.setAttribute("previewlist", arRetLtnProductList.get(0));
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView("previewretailproduct");
	}
}

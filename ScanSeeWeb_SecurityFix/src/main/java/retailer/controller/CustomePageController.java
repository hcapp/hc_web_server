/**
 * 
 */
package retailer.controller;

import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;
import retailer.validator.RetailerCreatedPageValidator;
import retailer.validator.RetailerPageValidator;
import retailer.validator.SpecialOfferPageValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Contact;
import common.pojo.Retailer;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerImages;
import common.pojo.RetailerLocation;
import common.pojo.RetailerProfile;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * @author manjunatha_gh
 */
@Controller
public class CustomePageController {

	RetailerCreatedPageValidator createdPageValidator;

	RetailerPageValidator retailerPageValidator;

	SpecialOfferPageValidator specialOfferPageValidator;

	@Autowired
	public void setCreatedPageValidator(RetailerCreatedPageValidator createdPageValidator) {
		this.createdPageValidator = createdPageValidator;
	}

	@Autowired
	public void setRetailerPageValidator(RetailerPageValidator retailerPageValidator) {
		this.retailerPageValidator = retailerPageValidator;
	}

	@Autowired
	public void setSpecialOfferPageValidator(SpecialOfferPageValidator specialOfferPageValidator) {
		this.specialOfferPageValidator = specialOfferPageValidator;
	}

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CustomePageController.class);

	@RequestMapping(value = "/createcustompage", method = RequestMethod.GET)
	public String showCreatCustomPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		LOG.info("Inside the Methodn showCreatCustomPage ");
		String viewName = "createRetailerCustomPage";
		session.removeAttribute("pageType");
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		session.removeAttribute("customPageRetImgPath");
		session.removeAttribute("customLandingPageImgPath");
		session.removeAttribute("customSplOfferPageImgPath");
		session.removeAttribute("customPageBtnVal");
		session.removeAttribute("SpclOffrType");
		session.removeAttribute("landingPageType");
		session.removeAttribute("UploadedFile");
		session.removeAttribute("UploadedFileLength");
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		model.put("createCustomPage", retailerCustomPage);
		Long retailID = null;
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());
		Retailer retailerObj = retailerService.fetchRetaielrInfo(retailID);

		if (retailerObj != null) {
			session.setAttribute("retailStoreName", retailerObj.getRetailerName());
			session.setAttribute("retailStoreImage", retailerObj.getRetailerImagePath());
			session.setAttribute("retailStoreAddress", retailerObj.getAddress1());
		}

		RetailerProfile profileInfo = null;
		Users user = (Users) request.getSession().getAttribute("loginuser");
		profileInfo = retailerService.getRetailerProfile(user);
		String retailContactNumber = "(xxx)xxx-xxxx";
		if (null != profileInfo && !Utility.isEmptyOrNullString(profileInfo.getContactPhoneNo())) {
			retailContactNumber = Utility.getPhoneFormate(profileInfo.getContactPhoneNo());
		}
		session.setAttribute("retailContactNumber", retailContactNumber);

		ArrayList<RetailerLocation> retailerLocations = null;
		retailerLocations = retailerService.getRetailerLocations(retailID);
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		session.setAttribute("customLandingPageImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		session.setAttribute("customSplOfferPageImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		session.setAttribute("customPageBtnVal", "Upload Photo");
		if (retailerLocations != null) {
			session.setAttribute("retailerLocList", retailerLocations);
		} else {
			session.setAttribute("retailerLocList", "");
		}
		LOG.info("Exit Methodn showCreatCustomPage ");
		return viewName;
	}

	@RequestMapping(value = "/retailercreatedpage.htm", method = RequestMethod.POST)
	public ModelAndView creatCustomPage(@ModelAttribute("linktoExstngPg") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside the Method creatCustomPage ");
		String response = null;
		String viewName = "createRetailerCustomPage";
		String showQRCode = "showqrcode";
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");

		try {
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			int retailID = loginUser.getRetailerId();
			retailerCustomPage.setRetailerId(retailID);
			retailerCustomPage.setHiddenLocId(retailerCustomPage.getRetCreatedPageAttachLinkLocId());
			retailerCustomPage.setRetCreatedPageAttachLinkLocId("" + retailerCustomPage.getRetLocId());
			createdPageValidator.validate(retailerCustomPage, result);
			retailerCustomPage.setHiddenLocId("" + retailerCustomPage.getRetLocId());

			session.setAttribute("qrCodeImageTitle", retailerCustomPage.getRetCreatedPageAttachLinkTitle());

			if (result.hasErrors()) {
				return new ModelAndView(viewName);
			} else {

				response = retailerService.createRetailerCreatedPage(retailerCustomPage);

				if (null != response && !response.equals(ApplicationConstants.FAILURE)) {

					session.setAttribute("qrCodeImagePath", response);
					return new ModelAndView(showQRCode);
				}

			}
		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}

		LOG.info("Exit Methodn creatCustomPage ");
		return new ModelAndView(viewName);
	}

	@RequestMapping(value = "/showpagelist", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView showCustomPagesList(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		final String methodName = "showCustomPagesList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		model.put("customPageForm", new RetailerCustomPage());
		String pageFlag = (String) request.getParameter("pageFlag");
		String pageNumber = "0";
		int lowerLimit = 0;
		int currentPage = 1;
		String searchKey = null;
		int recordCount = 20;
		try {

			if (null != pageFlag && pageFlag.equals("true")) {
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");

				recordCount = pageSess.getPageRange();

				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}

			}
			SearchResultInfo result = retailerService.getCusomtPageList(loginUser.getRetailerId(), searchKey, lowerLimit, recordCount);
			request.setAttribute("customPageList", result);
			Pagination objPage = Utility.getPagination(result.getTotalSize(), currentPage, "showpagelist.htm");
			session.setAttribute("pagination", objPage);
		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("pageList");
	}

	@RequestMapping(value = "/getpageqr", method = RequestMethod.GET)
	public ModelAndView showQrCodeForPage(@ModelAttribute("customPageForm") RetailerCustomPage customPageForm, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		final String methodName = "showQrCodeForPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		try {

			RetailerCustomPage pageDetails = retailerService.getQrCodeDetails(customPageForm.getPageId(), loginUser.getRetailerId());
			request.setAttribute("pageDetails", pageDetails);

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("showQrpage");
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/createretailerpage.htm", method = RequestMethod.POST)
	public ModelAndView creatRetailerPage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		LOG.info("Inside the Method creatRetailerPage ");
		String response = null;
		String viewName = "cstmMainPg";
		String showQRCode = "showqrcode";
		String pageType = null;
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		session.removeAttribute("UploadedPDFFile");
		session.removeAttribute("oldUploadedPDFFile");
		session.removeAttribute("pageTitle");
		ArrayList<RetailerImages> imageList = null;
		boolean isValidURL = true;
		String compStartDate = null;
		String compEndDate = null;
		String compDate = null;
		Date currentDate = new Date();
		String filePath = null;
		try {
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			pageType = request.getParameter("radio");
			session.setAttribute("pageType", pageType);
			session.setAttribute("pageTitle", retailerCustomPage.getRetPageTitle());
			isValidURL = Utility.validateURL(retailerCustomPage.getRetCreatedPageattachLink());
			int retailID = loginUser.getRetailerId();
			retailerCustomPage.setRetailerId(retailID);
			retailerCustomPage.setHiddenLocId(retailerCustomPage.getRetCreatedPageLocId());

			// Utility.setCustomPageStartAndEndDate(retailerCustomPage);

			retailerPageValidator.validate(retailerCustomPage, result);

			if (!isValidURL) {
				retailerPageValidator.validate(retailerCustomPage, result, ApplicationConstants.INVALIDURL);
			}

			if (!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageStartDate()))) {
				compStartDate = Utility.compareCurrentDate(retailerCustomPage.getRetCreatedPageStartDate(), currentDate);
				if (null != compStartDate) {
					retailerPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATESTARTCURRENT);
				}
			}
			if (!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate()))) {
				compEndDate = Utility.compareCurrentDate(retailerCustomPage.getRetCreatedPageEndDate(), currentDate);
				if (null != compEndDate) {
					retailerPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEENDCURRENT);
				}
			}

			if ((!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageStartDate())))
					&& (!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate())))
					&& (null == compStartDate && null == compEndDate)) {
				compDate = Utility.compareDate(retailerCustomPage.getRetCreatedPageStartDate(), retailerCustomPage.getRetCreatedPageEndDate());
				if (null != compDate) {
					createdPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEAFTER);
				}
			} else
				if (("".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageStartDate())))
						&& (!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate())))) {
					retailerPageValidator.validate(retailerCustomPage, result, ApplicationConstants.STARTDATE);
				}
			if (result.hasErrors()) {
				/*
				 * if
				 * ("AttachLink".equals(retailerCustomPage.getLandigPageType()))
				 * { return new ModelAndView("linktoExstngPg"); }
				 */

				if ("UploadPdf".equals(retailerCustomPage.getLandigPageType())) {

					if (retailerCustomPage.getRetFile().getSize() > 0) {
						String mediaPathBuilder = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
						String fileSeparator = System.getProperty("file.separator");
						filePath = mediaPathBuilder + fileSeparator + retailerCustomPage.getRetFile().getOriginalFilename();
						Utility.writeFileData(retailerCustomPage.getRetFile(), filePath);

						String sourceExtn = FilenameUtils.getExtension(retailerCustomPage.getRetFile().getOriginalFilename());
						if (!(sourceExtn.equals("pdf") || sourceExtn.equals("PDF"))) {
							String imageSource = FilenameUtils.removeExtension(retailerCustomPage.getRetFile().getOriginalFilename());
							imageSource = imageSource + ".png";

							retailerCustomPage.setPdfFileName(imageSource);
							session.setAttribute("UploadedPDFFile", imageSource);
						} else {
							retailerCustomPage.setPdfFileName(retailerCustomPage.getRetFile().getOriginalFilename());
							session.setAttribute("UploadedPDFFile", retailerCustomPage.getRetFile().getOriginalFilename());
						}

					} else
						if (null != retailerCustomPage.getPdfFileName() && !"".equalsIgnoreCase(retailerCustomPage.getPdfFileName())) {
							retailerCustomPage.setPdfFileName(retailerCustomPage.getPdfFileName());
							session.setAttribute("UploadedPDFFile", retailerCustomPage.getPdfFileName());
						}
					/*
					 * StringBuilder mediaPathBuilder =
					 * Utility.getMediaPath(ApplicationConstants.RETAILER,
					 * retailerCustomPage.getRetailerId()); String fileSeparator
					 * = System.getProperty("file.separator"); filePath =
					 * mediaPathBuilder + fileSeparator +
					 * retailerCustomPage.getPdfFileName();
					 * Utility.writeFileData(retailerCustomPage.getRetFile(),
					 * filePath); session.setAttribute("UploadedPDFFile",
					 * retailerCustomPage.getPdfFileName()); String sourceExtn =
					 * FilenameUtils
					 * .getExtension(retailerCustomPage.getPdfFileName()); if
					 * (!(sourceExtn.equals("pdf") || sourceExtn.equals("PDF")))
					 * { String imageSource =
					 * FilenameUtils.removeExtension(retailerCustomPage
					 * .getPdfFileName()); imageSource = imageSource + ".png";
					 * retailerCustomPage.setPdfFileName(imageSource); } }
					 */
				}

				return new ModelAndView(retailerCustomPage.getViewName());
			}
			response = retailerService.createRetailerPage(retailerCustomPage);
			if (null != response && !response.equals(ApplicationConstants.FAILURE)) {
				session.setAttribute("qrCodeImageTitle", retailerCustomPage.getRetPageTitle());
				session.setAttribute("qrCodeImagePath", response);
				return new ModelAndView(showQRCode);
			}

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}
		return new ModelAndView(retailerCustomPage.getViewName());
	}

	@RequestMapping(value = "/createsploffrpage.htm", method = RequestMethod.POST)
	public ModelAndView creatSpecialOfferPage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside the Method creatSpecialOfferPage ");
		String response = null;
		String showQRCode = "showqrcode";
		String compDate = null;
		Date currentDate = new Date();
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int retailID = loginUser.getRetailerId();
		retailerCustomPage.setRetailerId(retailID);

		/*
		 * //For Indefinite Check
		 * if(retailerCustomPage.isIndefiniteAdDurationFlag()) { // DateFormat
		 * dateFormat = new SimpleDateFormat("MM/dd/yyyy"); // Date date = new
		 * Date(); //
		 * retailerCustomPage.setSplOfferStartDate(dateFormat.format(date));
		 * retailerCustomPage
		 * .setSplOfferEndDate(ApplicationConstants.AD_END_DATE);
		 * retailerCustomPage
		 * .setSplOfferEndTimeHrs(ApplicationConstants.AD_END_TIME);
		 * retailerCustomPage
		 * .setSplOfferEndTimeMin(ApplicationConstants.AD_END_TIME); LOG.info(
		 * "Inside CustomePageController :  setAdStartAndEndDate Method : creatSpecialOfferPage Start date : "
		 * + retailerCustomPage.getSplOfferStartDate()); LOG.info(
		 * "Inside CustomePageController :  setAdStartAndEndDate Method : creatSpecialOfferPage End date : "
		 * + retailerCustomPage.getSplOfferEndDate()); }
		 */

		specialOfferPageValidator.validate(retailerCustomPage, result);
		FieldError fieldStartDate = result.getFieldError("splOfferStartDate");
		FieldError fieldEndDate = result.getFieldError("splOfferEndDate");

		if (retailerCustomPage.getSplOfferType().equals("Special Offer")) {
			session.setAttribute("qrCodeImageTitle", retailerCustomPage.getSplOfferTitle());
			// retailerCustomPage.setHiddenLocId(retailerCustomPage.getSplOfferLocId());
			if (null == fieldStartDate) {
				if (!"".equals(Utility.checkNull(retailerCustomPage.getSplOfferStartDate()))) {
					compDate = Utility.compareCurrentDate(retailerCustomPage.getSplOfferStartDate(), currentDate);
					if (null != compDate) {
						specialOfferPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATESTARTCURRENT);
					}
				}
			}
			if (!"".equals(Utility.checkNull(retailerCustomPage.getSplOfferEndDate()))) {
				if (null == fieldEndDate) {
					compDate = Utility.compareCurrentDate(retailerCustomPage.getSplOfferEndDate(), currentDate);
					if (null != compDate) {
						specialOfferPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEENDCURRENT);
					}

					else
						if (null == fieldStartDate) {
							compDate = Utility.compareDate(retailerCustomPage.getSplOfferStartDate(), retailerCustomPage.getSplOfferEndDate());
							if (null != compDate) {
								specialOfferPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEAFTER);
							}
						}
				}
			}
		} else
			if (retailerCustomPage.getSplOfferType().equals("AttachLink")) {
				session.setAttribute("qrCodeImageTitle", retailerCustomPage.getSplOfferAttatchLinkTitle());
				// retailerCustomPage.setHiddenLocId(retailerCustomPage.getSplOfferAttatchLinkLocId());
			}

		if (result.hasErrors()) {
			if (retailerCustomPage.getSplOfferType().equals("Special Offer")) {
				return new ModelAndView("splOfferMain");
			} else
				if (retailerCustomPage.getSplOfferType().equals("AttachLink")) {

					return new ModelAndView("splOfferLinktoExstngPage");
				}

		} else {
			response = retailerService.createSplOfferPage(retailerCustomPage);
			if (null != response && !response.equals(ApplicationConstants.FAILURE)) {
				session.setAttribute("qrCodeImagePath", response);
				request.setAttribute("splOffer", "splOffer");
				return new ModelAndView(showQRCode);
			}
		}
		if (retailerCustomPage.getSplOfferType().equals("Special Offer")) {
			return new ModelAndView("splOfferMain");
		} else {

			return new ModelAndView("splOfferLinktoExstngPage");
		}
	}

	@RequestMapping(value = "/getretailerpageinfo.htm", method = RequestMethod.GET)
	public String editRetailerPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside the Methodn editRetailerPage ");
		String viewName = "editRetailerPage";
		session.removeAttribute("pageType");
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		session.removeAttribute("customPageImgPath");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		session.setAttribute("imageCropPage", "EditAnythingPage");
		session.removeAttribute("UploadedPDFpath");
		session.removeAttribute("UploadedPDFFile");
		session.removeAttribute("oldUploadedPDFFile");
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		RetailerCustomPage retailerCustomPageInfo = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Long retailID = null;
		String pageId = null;
		pageId = request.getParameter("pageId");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());
		ArrayList<RetailerLocation> retailerLocations = null;
		String pageType = null;
		try {
			retailerLocations = retailerService.getRetailerLocations(retailID);
			if (retailerLocations != null) {
				session.setAttribute("retailerLocList", retailerLocations);
			} else {
				session.setAttribute("retailerLocList", "");
			}
			retailerCustomPageInfo = retailerService.getRetailerPageInfo(loginUser.getUserID(), retailID, Long.valueOf(pageId));
			if (null != retailerCustomPageInfo) {
				retailerCustomPage.setRetailLocations(retailerCustomPageInfo.getRetailLocations());
				retailerCustomPage.setHiddenLocId(retailerCustomPageInfo.getRetailLocations());
				retailerCustomPage.setRetPageTitle(retailerCustomPageInfo.getPageTitle());
				retailerCustomPage.setRetPageLongDescription(retailerCustomPageInfo.getLongDescription());
				retailerCustomPage.setRetPageShortDescription(retailerCustomPageInfo.getShortDescription());
				retailerCustomPage.setPageId(Long.parseLong(pageId));
				retailerCustomPage.setRetCreatedPageEndDate(Utility.getCalenderFormattedDate(retailerCustomPageInfo.getEndDate()));
				retailerCustomPage.setRetCreatedPageStartDate(Utility.getCalenderFormattedDate(retailerCustomPageInfo.getStartDate()));

				retailerCustomPage.setRetCreatedPageattachLink(retailerCustomPageInfo.getUrl());
				if (retailerCustomPageInfo.getUrl() != null && !"".equals(retailerCustomPageInfo.getUrl())) {
					viewName = "editRetailerlinkPage";
					pageType = "AttachLink";
				}
				if (null != retailerCustomPageInfo.getPdfFileName() && !"".equals(retailerCustomPageInfo.getPdfFileName())) {

					viewName = "editUploadPDFPage";
					pageType = "UploadPDF";
					session.setAttribute("oldUploadedPDFFile", retailerCustomPageInfo.getPdfFileName());
					session.setAttribute("UploadedPDFFile", retailerCustomPageInfo.getPdfFileName());
					session.setAttribute("UploadedPDFpath", retailerCustomPageInfo.getFileName());
					retailerCustomPage.setPdfFileName(retailerCustomPageInfo.getPdfFileName());
				}
				if (retailerCustomPageInfo.getImageIconID() > 0) {
					retailerCustomPage.setImageUplType("slctdUpld");
					retailerCustomPage.setImageIconID(retailerCustomPageInfo.getImageIconID());
					session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					retailerCustomPage.setImageUplType("usrUpld");
					if (null != retailerCustomPageInfo.getImageName() && !retailerCustomPageInfo.getImageName().equals("")) {

						retailerCustomPage.setImageName(retailerCustomPageInfo.getImageName());
						retailerCustomPage.setRetailerImg(retailerCustomPageInfo.getImageName());
					}
					if (null != retailerCustomPageInfo.getImagePath()) {

						session.setAttribute("customPageBtnVal", "Change Photo");
						session.setAttribute("customPageRetImgPath", retailerCustomPageInfo.getImagePath());
					} else {
						session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
						session.setAttribute("customPageBtnVal", "Upload Photo");
					}
				}

			}
			ArrayList<RetailerImages> imageList = new ArrayList<RetailerImages>();
			imageList = retailerService.getRetailerImageIconsDisplay(pageType);
			session.setAttribute("imageList", imageList);
			model.put("createCustomPage", retailerCustomPage);

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);

		} catch (ParseException e) {
			LOG.error(e.getMessage(), e);
			throw new ScanSeeServiceException(e);

		}

		return viewName;
	}

	@RequestMapping(value = "/updateretailerpage.htm", method = RequestMethod.POST)
	public ModelAndView editRetailerPage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		LOG.info("Inside the Method creatRetailerPage ");
		String response = null;
		String viewName = retailerCustomPage.getViewName();
		String pageType = null;
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		session.removeAttribute("pageTitle");
		session.removeAttribute("UploadedPDFpath");
		String showQRCode = "showqrcode";
		String compDate = null;
		String filePath = null;
		try {
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			pageType = request.getParameter("radio");
			session.setAttribute("pageType", pageType);
			session.setAttribute("pageTitle", retailerCustomPage.getRetPageTitle());
			int retailID = loginUser.getRetailerId();
			retailerCustomPage.setRetailerId(retailID);

			retailerCustomPage.setPageType(ApplicationConstants.QRTYPEMAINMENUPAGE);
			/*
			 * if (null != retailerCustomPage.getUrl() &&
			 * !"".equals(retailerCustomPage.getUrl())) {
			 * retailerCustomPage.setLandigPageType("AttachLink");
			 * retailerCustomPage
			 * .setRetCreatedPageattachLink(retailerCustomPage.getUrl()); }
			 */

			Utility.setCustomPageStartAndEndDate(retailerCustomPage);

			retailerPageValidator.validate(retailerCustomPage, result);
			if ((retailerCustomPage.getRetCreatedPageStartDate() != null && !retailerCustomPage.getRetCreatedPageStartDate().equals(""))
					&& (retailerCustomPage.getRetCreatedPageEndDate() != null && !retailerCustomPage.getRetCreatedPageEndDate().equals(""))) {
				compDate = Utility.compareDate(retailerCustomPage.getRetCreatedPageStartDate(), retailerCustomPage.getRetCreatedPageEndDate());
				if (null != compDate) {
					retailerPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEAFTER);
				}
			} else
				if (("".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageStartDate())))
						&& (!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate())))) {
					retailerPageValidator.validate(retailerCustomPage, result, ApplicationConstants.STARTDATE);
				}

			if (result.hasErrors()) {
				if ("UploadPdf".equals(retailerCustomPage.getLandigPageType())) {

					if (retailerCustomPage.getRetFile().getSize() > 0) {
						StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.RETAILER, retailerCustomPage.getRetailerId());
						String fileSeparator = System.getProperty("file.separator");
						filePath = mediaPathBuilder + fileSeparator + retailerCustomPage.getRetFile().getOriginalFilename();
						Utility.writeFileData(retailerCustomPage.getRetFile(), filePath);
						session.setAttribute("UploadedPDFFile", retailerCustomPage.getRetFile().getOriginalFilename());
						session.setAttribute("oldUploadedPDFFile", retailerCustomPage.getRetFile().getOriginalFilename());

						String sourceExtn = FilenameUtils.getExtension(retailerCustomPage.getRetFile().getOriginalFilename());
						if (!(sourceExtn.equals("pdf") || sourceExtn.equals("PDF"))) {
							String imageSource = FilenameUtils.removeExtension(retailerCustomPage.getRetFile().getOriginalFilename());
							imageSource = imageSource + ".png";

							retailerCustomPage.setPdfFileName(imageSource);
						} else {
							retailerCustomPage.setPdfFileName(retailerCustomPage.getRetFile().getOriginalFilename());
						}
					}
					/*
					 * else if (retailerCustomPage.getOldPdfFileName() !=
					 * retailerCustomPage.getPdfFileName()) { StringBuilder
					 * mediaPathBuilder =
					 * Utility.getMediaPath(ApplicationConstants.RETAILER,
					 * retailerCustomPage.getRetailerId()); String fileSeparator
					 * = System.getProperty("file.separator"); filePath =
					 * mediaPathBuilder + fileSeparator +
					 * retailerCustomPage.getPdfFileName();
					 * Utility.writeFileData(retailerCustomPage.getRetFile(),
					 * filePath); session.setAttribute("UploadedPDFFile",
					 * retailerCustomPage.getPdfFileName());
					 * session.setAttribute("oldUploadedPDFFile",
					 * retailerCustomPage.getPdfFileName()); String sourceExtn =
					 * FilenameUtils
					 * .getExtension(retailerCustomPage.getPdfFileName()); if
					 * (!(sourceExtn.equals("pdf") || sourceExtn.equals("PDF")))
					 * { String imageSource =
					 * FilenameUtils.removeExtension(retailerCustomPage
					 * .getPdfFileName()); imageSource = imageSource + ".png";
					 * retailerCustomPage.setPdfFileName(imageSource); } }
					 */
				}

				return new ModelAndView(retailerCustomPage.getViewName());
			} else {

				response = retailerService.updateRetailerCustomPageInfo(retailerCustomPage);
				if (null != response && !response.equals(ApplicationConstants.FAILURE)) {

					session.setAttribute("qrCodeImageTitle", retailerCustomPage.getRetPageTitle());
					session.setAttribute("qrCodeImagePath", response);
					return new ModelAndView(showQRCode);

				}
			}

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}

		session.removeAttribute("UploadedPDFFile");
		return new ModelAndView(viewName);
	}

	@RequestMapping(value = "/uploadtempretimg.htm", method = RequestMethod.POST)
	public String onSubmitImage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception, ScanSeeServiceException {

		String fileSeparator = System.getProperty("file.separator");
		String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
		String btnValue = request.getParameter("uploadBtn");
		session.removeAttribute("customPageImgPath");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		Date date = new Date();
		session.removeAttribute("cropImageSource");
		StringBuffer strResponse = new StringBuffer();
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");
		String fileTempPath = null;
		String imagetempSource = null;
		if (null != retailerCustomPage.getRetFile() && retailerCustomPage.getRetFile().getSize() > 0) {

			fileTempPath = tempImgPath + fileSeparator + retailerCustomPage.getRetFile().getOriginalFilename();
			Utility.writeFileData(retailerCustomPage.getRetFile(), fileTempPath);
			String sourceExtn = FilenameUtils.getExtension(retailerCustomPage.getRetFile().getOriginalFilename());
			if (!(sourceExtn.equals("pdf") || sourceExtn.equals("PDF"))) {
				imagetempSource = FilenameUtils.removeExtension(retailerCustomPage.getRetFile().getOriginalFilename());
				imageSource = imagetempSource + ".png";

				retailerCustomPage.setPdfFileName(imageSource);
				session.setAttribute("UploadedPDFFile", imageSource);
				session.setAttribute("UploadedPDFpath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			} else {
				retailerCustomPage.setPdfFileName(retailerCustomPage.getRetFile().getOriginalFilename());
				session.setAttribute("UploadedPDFFile", retailerCustomPage.getRetFile().getOriginalFilename());
				session.setAttribute("UploadedPDFpath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
						+ retailerCustomPage.getRetFile().getOriginalFilename());

			}

		}
		if (btnValue.equals("trgrUpldBtn")) {
			// imageValidSizeValFlg = Utility.validImageDimension(70, 70,
			// retailerCustomPage.getRetImage().getInputStream());

			imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, retailerCustomPage
					.getRetImage().getInputStream());
			if (imageSizeValFlg) {
				session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
				response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
				return null;

				// Validate for Maximum size- Span/Dileep
				// imageSizeValFlg = Utility.validDimension(1024, 950,
				// retailerCustomPage.getRetImage().getInputStream());

				// commented code for fixing the crop image issue validation.
				/*
				 * if (imageSizeValFlg) { BufferedImage img =
				 * ImageIO.read(retailerCustomPage
				 * .getRetImage().getInputStream()); w = img.getWidth(null); h =
				 * img.getHeight(null); session.setAttribute("imageHt", h);
				 * session.setAttribute("imageWd", w); imageSource =
				 * retailerCustomPage.getRetImage().getOriginalFilename();
				 * imageSource = FilenameUtils.removeExtension(imageSource);
				 * imageSource = imageSource + ".png" + "?" + date.getTime();
				 * String filePath = tempImgPath + fileSeparator +
				 * retailerCustomPage.getRetImage().getOriginalFilename();
				 * LOG.info("file Path" + filePath);
				 * Utility.writeFileData(retailerCustomPage.getRetImage(),
				 * filePath); if (imageValidSizeValFlg) {
				 * strResponse.append("ValidImageDimention");
				 * strResponse.append("|" +
				 * FilenameUtils.removeExtension(retailerCustomPage
				 * .getRetImage().getOriginalFilename()) + ".png");
				 * session.setAttribute("customPageRetImgPath", "/" +
				 * ApplicationConstants.IMAGES + "/" +
				 * ApplicationConstants.TEMPFOLDER + "/" + imageSource); }
				 * strResponse.append("|" + "/" + ApplicationConstants.IMAGES +
				 * "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				 * response.getWriter().write("<imageScr>" +
				 * strResponse.toString() + "</imageScr>");
				 * session.setAttribute("customPageRetImgPath", "/" +
				 * ApplicationConstants.IMAGES + "/" +
				 * ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				 * session.setAttribute("cropImageSource", "/" +
				 * ApplicationConstants.IMAGES + "/" +
				 * ApplicationConstants.TEMPFOLDER + "/" + imageSource); } else
				 * { session.setAttribute("customPageRetImgPath",
				 * ApplicationConstants.UPLOADIMAGEPATH);
				 * response.getWriter().write("<imageScr>" + "UploadLogoMaxSize"
				 * + "</imageScr>"); return null; }
				 */

			} else {
				// session.setAttribute("customPageRetImgPath",
				// ApplicationConstants.UPLOADIMAGEPATH);
				// response.getWriter().write("<imageScr>" + "UploadLogoMinSize"
				// + "</imageScr>");

				// Meyy Related Code
				// BufferedImage img =
				// ImageIO.read(retailerCustomPage.getRetImage().getInputStream());
				BufferedImage img = Utility.getBufferedImageForMinDimension(retailerCustomPage.getRetImage().getInputStream(),
						request.getRealPath("images"), retailerCustomPage.getRetImage().getOriginalFilename(), "Anything");
				w = img.getWidth(null);
				h = img.getHeight(null);
				session.setAttribute("imageHt", h);
				session.setAttribute("imageWd", w);
				imageSource = retailerCustomPage.getRetImage().getOriginalFilename();
				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png" + "?" + date.getTime();
				String filePath = tempImgPath + fileSeparator + retailerCustomPage.getRetImage().getOriginalFilename();
				LOG.info("file Path" + filePath);
				// Utility.writeFileData(retailerCustomPage.getRetImage(),
				// filePath);
				Utility.writeImage(img, filePath);
				if (imageValidSizeValFlg) {
					strResponse.append("ValidImageDimention");
					strResponse.append("|" + retailerCustomPage.getRetImage().getOriginalFilename());

					session.setAttribute("customPageRetImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
							+ imageSource);

				}
				strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");

				/*
				 * session.setAttribute("customPageRetImgPath", "/" +
				 * ApplicationConstants.IMAGES + "/" +
				 * ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				 */
				session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);

			}

		} else
			if (btnValue.equals("trgrUpldCrtdPageBtn")) {
				String filePath = tempImgPath + fileSeparator + retailerCustomPage.getRetCreatedPageImage().getOriginalFilename();

				LOG.info("file Path" + filePath);
				Utility.writeFileData(retailerCustomPage.getRetCreatedPageImage(), filePath);
				response.setHeader("Cache-Control", "no-cache");
				response.setContentType("text/xml");
				response.getWriter().write(
						"<imageScr>" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
								+ retailerCustomPage.getRetCreatedPageImage().getOriginalFilename() + "</imageScr>");
				session.setAttribute("customLandingPageImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
						+ retailerCustomPage.getRetCreatedPageImage().getOriginalFilename());
			} else
				if (btnValue.equals("trgrUpldSplPageBtn")) {

					String filePath = tempImgPath + fileSeparator + retailerCustomPage.getSplOfferImage().getOriginalFilename();
					LOG.info("file Path" + filePath);
					Utility.writeFileData(retailerCustomPage.getSplOfferImage(), filePath);
					response.setHeader("Cache-Control", "no-cache");
					response.setContentType("text/xml");
					response.getWriter().write(
							"<imageScr>" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
									+ retailerCustomPage.getSplOfferImage().getOriginalFilename() + "</imageScr>");
					session.setAttribute("customSplOfferPageImgPath", ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
							+ retailerCustomPage.getSplOfferImage().getOriginalFilename());
				}

		return null;

	}

	@RequestMapping(value = "/getretailercrtdpageinfo.htm", method = RequestMethod.GET)
	public String editRetailerCreatedPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside the Methodn editRetailerCreatedPage ");
		String viewName = "editRetailerCreatedPage";
		session.removeAttribute("pageType");
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		session.removeAttribute("customPageImgPath");
		session.removeAttribute("UploadedFile");
		session.removeAttribute("UploadedFileLength");
		String landingPageType = null;
		String[] timeHrsMins = null;
		String[] uploadedFile = null;
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		RetailerCustomPage retailerCustomPageInfo = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Long retailID = null;
		String pageId = null;
		pageId = request.getParameter("pageId");
		landingPageType = request.getParameter("editLndgPgSlct");
		session.setAttribute("landingPageType", landingPageType);
		retailerCustomPage.setLandigPageType(landingPageType);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());
		ArrayList<RetailerLocation> retailerLocations = null;
		try {
			retailerLocations = retailerService.getRetailerLocations(retailID);
			if (retailerLocations != null) {
				session.setAttribute("retailerLocList", retailerLocations);
			} else {
				session.setAttribute("retailerLocList", "");
			}
			retailerCustomPageInfo = retailerService.getRetailerPageInfo(loginUser.getUserID(), retailID, Long.valueOf(pageId));
			if (null != retailerCustomPageInfo) {

				if (retailerCustomPageInfo.getExternalFlag().contains("0")) {
					session.setAttribute("landingPageType", "Landing");
					retailerCustomPage.setRetCreatedPageLocId(retailerCustomPageInfo.getRetailLocationID());
					retailerCustomPage.setRetCreatedPageTitle(retailerCustomPageInfo.getPageTitle());
					retailerCustomPage.setHiddenLocId(retailerCustomPageInfo.getRetailLocationID());
					retailerCustomPage.setRetCreatedPageDescription(retailerCustomPageInfo.getPageDescription());
					retailerCustomPage.setRetCreatedPageLongDescription(retailerCustomPageInfo.getLongDescription());
					retailerCustomPage.setRetCreatedPageShortDescription(retailerCustomPageInfo.getShortDescription());
					if (null != retailerCustomPageInfo.getStartDate() && !retailerCustomPageInfo.getStartDate().equals("")) {
						retailerCustomPage.setRetCreatedPageStartDate(Utility.getCalenderFormattedDate(retailerCustomPageInfo.getStartDate()));
					}
					if (null != retailerCustomPageInfo.getEndDate() && !retailerCustomPageInfo.getEndDate().equals("")) {
						retailerCustomPage.setRetCreatedPageEndDate(Utility.getCalenderFormattedDate(retailerCustomPageInfo.getEndDate()));
					}
					if (null != retailerCustomPageInfo.getImageName() && !retailerCustomPageInfo.getImageName().equals("")) {

						retailerCustomPage.setImageName(retailerCustomPageInfo.getImageName());
					}

					if (null != retailerCustomPageInfo.getStartTime()) {
						timeHrsMins = retailerCustomPageInfo.getStartTime().split(":");
						retailerCustomPage.setLandingPageStartTimeHrs(timeHrsMins[0]);
						retailerCustomPage.setLandingPageStartTimeMin(timeHrsMins[1]);

					}
					if (null != retailerCustomPageInfo.getEndTime()) {
						timeHrsMins = retailerCustomPageInfo.getEndTime().split(":");
						retailerCustomPage.setLandingPageEndTimeHrs(timeHrsMins[0]);
						retailerCustomPage.setLandingPageEndTimeMin(timeHrsMins[1]);

					}

					if (null != retailerCustomPageInfo.getFileName() && !retailerCustomPageInfo.getFileName().equals("")) {

						uploadedFile = retailerCustomPageInfo.getFileName().split(",");
						session.setAttribute("UploadedFile", uploadedFile);
						session.setAttribute("UploadedFileLength", uploadedFile.length);

					} else {
						session.setAttribute("UploadedFileLength", "0");

					}
				} else
					if (retailerCustomPageInfo.getExternalFlag().contains("1")) {
						session.setAttribute("landingPageType", "AttachLink");
						retailerCustomPage.setRetCreatedPageAttachLinkTitle(retailerCustomPageInfo.getPageTitle());
						retailerCustomPage.setRetCreatedPageAttachLinkLocId(retailerCustomPageInfo.getRetailLocationID());
						retailerCustomPage.setHiddenLocId(retailerCustomPageInfo.getRetailLocationID());
						retailerCustomPage.setRetCreatedPageattachLink(retailerCustomPageInfo.getMediaPath());

					}

				retailerCustomPage.setPageId(retailerCustomPageInfo.getPageId());
				if (null != retailerCustomPageInfo.getImagePath()) {

					// TODO:For Testing purpose. Remove Later

					// String img =
					// retailerCustomPageInfo.getImagePath().replace("122.181.128.152:8080/Images",
					// "localhost:8080/images");
					// session.setAttribute("customLandingPageImgPath", img);

					session.setAttribute("customPageBtnVal", "Change Photo");
					session.setAttribute("customLandingPageImgPath", retailerCustomPageInfo.getImagePath());
				} else {

					session.setAttribute("customLandingPageImgPath", ApplicationConstants.UPLOADIMAGEPATH);
					session.setAttribute("customPageBtnVal", "Upload Photo");
				}

			}
			model.put("createCustomPage", retailerCustomPage);

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);

		} catch (ParseException e) {
			LOG.error(e.getMessage(), e);
			throw new ScanSeeServiceException(e);
		}

		return viewName;
	}

	@RequestMapping(value = "/updateretailercrtdpage.htm", method = RequestMethod.POST)
	public ModelAndView updateRetCrtdPageInfo(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside the Method updateRetCrtdPageInfo ");
		String compDate = null;
		String response = null;
		String viewName = "editRetailerCreatedPage";
		String landingPageType = null;
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		session.removeAttribute("pageTitle");
		String uploadedFiles = "";
		String externalFlag = "";
		String showQRCode = "showqrcode";
		try {
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			int retailID = loginUser.getRetailerId();
			retailerCustomPage.setRetailerId(retailID);
			landingPageType = request.getParameter("editLndgPgSlct");
			session.setAttribute("landingPageType", landingPageType);
			retailerCustomPage.setLandigPageType(landingPageType);
			retailerCustomPage.setPageType(ApplicationConstants.QRTYPELANDINGPAGE);
			createdPageValidator.validate(retailerCustomPage, result);
			retailerCustomPage.setHiddenLocId(retailerCustomPage.getRetCreatedPageLocId());
			FieldError fieldStartDate = result.getFieldError("retCreatedPageStartDate");
			FieldError fieldEndDate = result.getFieldError("retCreatedPageEndDate");
			session.setAttribute("pageTitle", retailerCustomPage.getRetPageTitle());
			if (retailerCustomPage.getLandigPageType().equals("Landing")) {
				session.setAttribute("qrCodeImageTitle", retailerCustomPage.getRetCreatedPageTitle());
				retailerCustomPage.setHiddenLocId(retailerCustomPage.getRetCreatedPageLocId());
				if (null == fieldStartDate && null == fieldEndDate) {
					if ((retailerCustomPage.getRetCreatedPageStartDate() != null && !retailerCustomPage.getRetCreatedPageStartDate().equals(""))
							&& (retailerCustomPage.getRetCreatedPageEndDate() != null && !retailerCustomPage.getRetCreatedPageEndDate().equals(""))) {

						compDate = Utility
								.compareDate(retailerCustomPage.getRetCreatedPageStartDate(), retailerCustomPage.getRetCreatedPageEndDate());

						if (null != compDate) {

							createdPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEAFTER);
						}

					}
				}

			} else
				if (retailerCustomPage.getLandigPageType().equals("AttachLink")) {
					session.setAttribute("qrCodeImageTitle", retailerCustomPage.getRetCreatedPageAttachLinkTitle());
					retailerCustomPage.setHiddenLocId(retailerCustomPage.getRetCreatedPageLocId());
				}

			if (result.hasErrors()) {
				return new ModelAndView(viewName);
			} else {

				String[] uploadedFile = (String[]) session.getAttribute("UploadedFile");

				if (null != uploadedFile) {

					if (uploadedFile.length != 0) {
						for (int i = 0; i < uploadedFile.length; i++) {
							uploadedFiles = uploadedFiles + uploadedFile[i] + ",";
							externalFlag = externalFlag + "0" + ",";
						}
						retailerCustomPage.setMediaPath(uploadedFiles);
						retailerCustomPage.setExternalFlag(externalFlag);
					} else {

						retailerCustomPage.setMediaPath("");
						retailerCustomPage.setExternalFlag("");
					}
				} else {
					retailerCustomPage.setMediaPath("");
					retailerCustomPage.setExternalFlag("");

				}

				response = retailerService.updateRetailerCustomPageInfo(retailerCustomPage);

				if (null != response && !response.equals(ApplicationConstants.FAILURE)) {

					session.setAttribute("qrCodeImagePath", response);
					return new ModelAndView(showQRCode);

				}

			}
		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}

		LOG.info("Exit Methodn updateRetCrtdPageInfo ");
		return new ModelAndView(viewName);

	}

	@RequestMapping(value = "/getsplofferpageinfo.htm", method = RequestMethod.GET)
	public String editSpecialOfferPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside the Methodn editRetailerCreatedPage ");
		String viewName = "editSpecialOfferPage";
		session.removeAttribute("pageType");
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		session.removeAttribute("customPageImgPath");
		session.removeAttribute("UploadedFile");
		session.removeAttribute("UploadedFileLength");
		session.setAttribute("imageCropPage", "SpecialOfferPage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		String[] timeHrsMins = null;
		String spclOffrType = null;
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		RetailerCustomPage retailerCustomPageInfo = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Long retailID = null;
		String pageId = null;
		pageId = request.getParameter("pageId");
		spclOffrType = request.getParameter("editSlctSpclOffr");
		retailerCustomPage.setSplOfferType(spclOffrType);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());
		ArrayList<RetailerLocation> retailerLocations = null;
		try {
			retailerLocations = retailerService.getRetailerLocations(retailID);
			if (retailerLocations != null) {
				session.setAttribute("retailerLocList", retailerLocations);
			} else {
				session.setAttribute("retailerLocList", "");
			}
			retailerCustomPageInfo = retailerService.getRetailerSplOfrPageInfo(retailID, Long.valueOf(pageId));
			if (null != retailerCustomPageInfo) {

				if (retailerCustomPageInfo.getImageIconID() > 0) {
					retailerCustomPage.setImageUplType("slctdUpld");
					retailerCustomPage.setImageIconID(retailerCustomPageInfo.getImageIconID());
					session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
				} else {
					retailerCustomPage.setImageUplType("usrUpld");
					if (null != retailerCustomPageInfo.getImageName() && !retailerCustomPageInfo.getImageName().equals("")) {

						retailerCustomPage.setImageName(retailerCustomPageInfo.getImageName());
						retailerCustomPage.setRetailerImg(retailerCustomPageInfo.getImageName());
					}
					if (null != retailerCustomPageInfo.getImagePath()) {

						session.setAttribute("customPageBtnVal", "Change Photo");
						session.setAttribute("customPageRetImgPath", retailerCustomPageInfo.getImagePath());
					} else {

						session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
						session.setAttribute("customPageBtnVal", "Upload Photo");
					}

				}

				if (null == retailerCustomPageInfo.getUrl()) {
					session.setAttribute("SpclOffrType", "Special Offer");
					retailerCustomPage.setSplOfferTitle(retailerCustomPageInfo.getPageTitle());
					if (null != retailerCustomPageInfo.getStartDate() && !retailerCustomPageInfo.getStartDate().equals("")) {
						retailerCustomPage.setSplOfferStartDate(Utility.getCalenderFormattedDate(retailerCustomPageInfo.getStartDate()));
					}
					if (null != retailerCustomPageInfo.getEndDate() && !retailerCustomPageInfo.getEndDate().equals("")) {
						retailerCustomPage.setSplOfferEndDate(Utility.getCalenderFormattedDate(retailerCustomPageInfo.getEndDate()));
					}

					if (null != retailerCustomPageInfo.getStartTime()) {
						timeHrsMins = retailerCustomPageInfo.getStartTime().split(":");
						retailerCustomPage.setSplofferStartTimeHrs(timeHrsMins[0]);
						retailerCustomPage.setSplofferStartTimeMin(timeHrsMins[1]);
					}
					if (null != retailerCustomPageInfo.getEndTime()) {
						timeHrsMins = retailerCustomPageInfo.getEndTime().split(":");
						retailerCustomPage.setSplOfferEndTimeHrs(timeHrsMins[0]);
						retailerCustomPage.setSplOfferEndTimeMin(timeHrsMins[1]);
					}
				} else {
					session.setAttribute("SpclOffrType", "AttachLink");
					retailerCustomPage.setSplOfferTitle(retailerCustomPageInfo.getPageTitle());

					retailerCustomPage.setSplOfferattachLink(retailerCustomPageInfo.getUrl());
				}
				retailerCustomPage.setPageId(Long.parseLong(pageId));
				retailerCustomPage.setSplOfferLongDescription(retailerCustomPageInfo.getLongDescription());
				retailerCustomPage.setSplOfferShortDescription(retailerCustomPageInfo.getShortDescription());
				retailerCustomPage.setSplOfferLocId(retailerCustomPageInfo.getRetailLocations());
				retailerCustomPage.setHiddenLocId(retailerCustomPageInfo.getRetailLocations());
				/*
				 * if (null != retailerCustomPageInfo.getImagePath()) {
				 * session.setAttribute("customPageBtnVal", "Change Photo");
				 * session.setAttribute("customPageRetImgPath",
				 * retailerCustomPageInfo.getImagePath()); } else if (null !=
				 * retailerCustomPageInfo.getDefaultImageIcon()) {
				 * session.setAttribute("customPageBtnVal", "Change Photo");
				 * session.setAttribute("customPageRetImgPath",
				 * retailerCustomPageInfo.getDefaultImageIcon()); } else {
				 * session.setAttribute("customPageRetImgPath",
				 * ApplicationConstants.UPLOADIMAGEPATH);
				 * session.setAttribute("customPageBtnVal", "Upload Photo"); }
				 */

			}
			model.put("createCustomPage", retailerCustomPage);
			ArrayList<RetailerImages> imageList = new ArrayList<RetailerImages>();
			imageList = retailerService.getRetailerImageIconsDisplay("AttachLink");
			session.setAttribute("imageList", imageList);

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);

		} catch (ParseException e) {
			LOG.error(e.getMessage(), e);
			throw new ScanSeeServiceException(e);
		}
		if (null == retailerCustomPageInfo.getUrl()) {
			return viewName;
		} else {
			return "editSplOfrLinkPage";
		}

	}

	@RequestMapping(value = "/updatesploffrpage.htm", method = RequestMethod.POST)
	public ModelAndView updateSpecialOfferPage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside the Method updateSpecialOfferPage ");
		String response = null;
		String viewName = "editSpecialOfferPage";
		String showQRCode = "showqrcode";
		String compDate = null;
		ArrayList<RetailerImages> imageList = null;
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int retailID = loginUser.getRetailerId();
		retailerCustomPage.setRetailerId(retailID);
		String spclOffrType = request.getParameter("editSlctSpclOffr");
		retailerCustomPage.setSplOfferType(spclOffrType);
		retailerCustomPage.setHiddenLocId(retailerCustomPage.getSplOfferLocId());

		/*
		 * //For Indefinite Check
		 * if(retailerCustomPage.isIndefiniteAdDurationFlag()) { // DateFormat
		 * dateFormat = new SimpleDateFormat("MM/dd/yyyy"); // Date date = new
		 * Date(); //
		 * retailerCustomPage.setSplOfferStartDate(dateFormat.format(date));
		 * retailerCustomPage
		 * .setSplOfferEndDate(ApplicationConstants.AD_END_DATE); LOG.info(
		 * "Inside CustomePageController :  setAdStartAndEndDate Method : creatSpecialOfferPage Start date : "
		 * + retailerCustomPage.getSplOfferStartDate()); LOG.info(
		 * "Inside CustomePageController :  setAdStartAndEndDate Method : creatSpecialOfferPage End date : "
		 * + retailerCustomPage.getSplOfferEndDate()); }
		 */

		specialOfferPageValidator.validate(retailerCustomPage, result);
		FieldError fieldStartDate = result.getFieldError("splOfferStartDate");
		FieldError fieldEndDate = result.getFieldError("splOfferEndDate");
		retailerCustomPage.setPageType(ApplicationConstants.QRTYPESPLOFFERPAGE);
		if (retailerCustomPage.getSplOfferType().equals("Special Offer")) {
			session.setAttribute("qrCodeImageTitle", retailerCustomPage.getSplOfferTitle());
			// retailerCustomPage.setHiddenLocId(retailerCustomPage.getSplOfferLocId());
			if (!"".equals(Utility.checkNull(retailerCustomPage.getSplOfferEndDate()))) {
				if (null == fieldStartDate && null == fieldEndDate) {
					compDate = Utility.compareDate(retailerCustomPage.getSplOfferStartDate(), retailerCustomPage.getSplOfferEndDate());
					if (null != compDate) {
						specialOfferPageValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEAFTER);
					}
				}
			}
		} else
			if (retailerCustomPage.getSplOfferType().equals("AttachLink")) {
				viewName = "editSplOfrLinkPage";
				session.setAttribute("qrCodeImageTitle", retailerCustomPage.getSplOfferTitle());
				// retailerCustomPage.setHiddenLocId(retailerCustomPage.getSplOfferAttatchLinkLocId());
			}

		if (result.hasErrors()) {
			return new ModelAndView(viewName);
		} else {

			response = retailerService.updateRetailerSplOfrPageInfo(retailerCustomPage);
			if (null != response && !response.equals(ApplicationConstants.FAILURE)) {
				request.setAttribute("splOffer", "splOffer");
				session.setAttribute("qrCodeImagePath", response);
				return new ModelAndView(showQRCode);
			}
		}

		return new ModelAndView(viewName);
	}

	@RequestMapping(value = "/previewretailerpage.htm", method = RequestMethod.POST)
	public String previewRetailerPage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		LOG.info("Inside the Method creatRetailerPage ");
		String pageType = null;

		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		pageType = request.getParameter("radio");
		session.setAttribute("pageType", pageType);
		int retailID = loginUser.getRetailerId();
		retailerCustomPage.setRetailerId(retailID);
		session.removeAttribute("PreviewPhoto");
		session.removeAttribute("PreviewPageTitle");
		session.removeAttribute("PreviewPageDescription");
		session.removeAttribute("PreviewShortDescription");
		session.removeAttribute("PreviewLongDescription");

		if (retailerCustomPage.getRetImage().getSize() > 0) {

			session.setAttribute("PreviewPhoto", "/" + ApplicationConstants.ROOTIMAGEFOLDER + "/" + ApplicationConstants.TEMPFOLDER + "/"
					+ retailerCustomPage.getRetImage().getOriginalFilename());
		}

		if (null != retailerCustomPage.getRetPageTitle() && !retailerCustomPage.getRetPageTitle().equals("")) {
			session.setAttribute("PreviewPageTitle", retailerCustomPage.getRetPageTitle());
		}
		if (null != retailerCustomPage.getRetPageDescription() && !retailerCustomPage.getRetPageDescription().equals("")) {
			session.setAttribute("PreviewPageDescription", retailerCustomPage.getRetPageDescription());
		}
		if (null != retailerCustomPage.getRetPageShortDescription() && !retailerCustomPage.getRetPageShortDescription().equals("")) {
			session.setAttribute("PreviewShortDescription", retailerCustomPage.getRetPageShortDescription());
		}
		if (null != retailerCustomPage.getRetPageLongDescription() && !retailerCustomPage.getRetPageLongDescription().equals("")) {
			session.setAttribute("PreviewLongDescription", retailerCustomPage.getRetPageLongDescription());
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("DealStartHours")
	public Map<String, String> populateDealStartHrs() throws ScanSeeServiceException {

		HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();

		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapDealStartHrs.put("0" + i, "0" + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}

		}
		Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	@SuppressWarnings("rawtypes")
	@ModelAttribute("DealStartMinutes")
	public Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException {
		HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();

		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapDealStartHrs.put("0" + i, "0" + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}

		}
		@SuppressWarnings("unused")
		Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException {

		List list = new LinkedList(unsortMap.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		// put sorted list into map again
		Map sortedMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	@RequestMapping(value = "/deleteimage", method = RequestMethod.GET)
	public @ResponseBody
	String deleteImage(@RequestParam(value = "imageName", required = true) String imageName, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {

		String[] uploadedFile = (String[]) session.getAttribute("UploadedFile");

		String[] newList = new String[uploadedFile.length - 1];
		int j = 0;
		for (int i = 0; i < uploadedFile.length; i++) {
			if (!uploadedFile[i].equals(imageName)) {

				newList[j++] = uploadedFile[i];

			}

		}

		session.setAttribute("UploadedFile", newList);
		// session.setAttribute("uploadMaxLength", uploadedFile.length - 3);
		return String.valueOf(newList.length);
	}

	@RequestMapping(value = "/validateretpage", method = RequestMethod.GET)
	public @ResponseBody
	String validateRetailerPage(@RequestParam(value = "retailerLocId", required = true) String retailerLocId,
			@RequestParam(value = "pageID", required = true) Long pageID, @RequestParam(value = "qrType", required = true) int qrType,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		String responseStr = null;
		try {

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Long retailID = null;
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			retailID = Long.valueOf(loginUser.getRetailerId());
			responseStr = retailerService.validateRetailerPage(retailID, retailerLocId, pageID, qrType);
		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);

		}
		return responseStr;
	}

	@RequestMapping(value = "/updatemainmenupage.htm", method = RequestMethod.POST)
	public ModelAndView updateRetailerPage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException {

		LOG.info("Inside the Method creatRetailerPage ");
		String response = null;
		String delResponse = null;
		String viewName = null;
		String pageType = null;
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		String showQRCode = "showqrcode";

		try {
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			pageType = request.getParameter("radio");
			session.setAttribute("pageType", pageType);
			int retailID = loginUser.getRetailerId();
			retailerCustomPage.setRetailerId(retailID);
			retailerCustomPage.setPageType(ApplicationConstants.QRTYPEMAINMENUPAGE);
			retailerCustomPage.setHiddenLocId(retailerCustomPage.getRetCreatedPageLocId());
			retailerPageValidator.validate(retailerCustomPage, result);

			if (!"".equals(retailerCustomPage.getUrl())) {

				viewName = retailerCustomPage.getViewName();
				// viewName = "linktoExstngPg";
			} else {
				viewName = retailerCustomPage.getViewName();

			}
			if (result.hasErrors()) {

				return new ModelAndView(viewName);
			} else {

				response = retailerService.updateRetailerCustomPageInfo(retailerCustomPage);
				if (null != response && !response.equals(ApplicationConstants.FAILURE)) {

					if (null != response && !response.equals(ApplicationConstants.FAILURE)) {
						delResponse = retailerService.deleteRetailerPage(retailerCustomPage.getExistingPageId());

						if (null != delResponse && !delResponse.equals(ApplicationConstants.FAILURE)) {

							session.setAttribute("qrCodeImageTitle", retailerCustomPage.getRetPageTitle());
							session.setAttribute("qrCodeImagePath", response);
							return new ModelAndView(showQRCode);
						} else {
							throw new ScanSeeServiceException();

						}

					} else {
						throw new ScanSeeServiceException();

					}

				}
			}

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used for deleting custom page.
	 * 
	 * @param customPageForm
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/deleteCustomPage", method = RequestMethod.GET)
	public ModelAndView deleteCustomPage(@ModelAttribute("customPageForm") RetailerCustomPage customPageForm, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		final String methodName = "deleteCustomPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		String pageType = (String) request.getParameter("pageType");
		SearchResultInfo resultLst = null;
		Pagination pageSess = (Pagination) session.getAttribute("pagination");
		String pageNumber = "0";
		pageNumber = request.getParameter("pageNumber");
		final String strSortIds = request.getParameter("sortIds");
		if (!"".equals(Utility.checkNull(strSortIds))) {
			customPageForm.setPageIds(strSortIds);
		}
		final String strPageIds = request.getParameter("pageIds");
		if (!"".equals(Utility.checkNull(strPageIds))) {
			customPageForm.setPageIds(strPageIds);
		}
		int recordCount = 20;
		try {
			String status = retailerService.deleteCustomPage(customPageForm.getPageId(), loginUser.getRetailerId());

			if (null != customPageForm.getRecordCount() && !"".equals(customPageForm.getRecordCount())
					&& !"undefined".equals(customPageForm.getRecordCount())) {
				recordCount = Integer.valueOf(customPageForm.getRecordCount());
			}

			if (ApplicationConstants.SUCCESS.equals(status)) {
				request.setAttribute("message", "Deleted Successfully.");
				/*
				 * Application should navigate to the previous page , If user
				 * delete 21 record
				 */
				Long lSize = pageSess.getTotalSize();
				if (lSize == 21) {
					pageNumber = "1";
					pageSess.setTotalSize(20);
					session.setAttribute("pageNumber", pageNumber);
				}
				/* End */
			}
			String pageFlag = (String) request.getParameter("pageFlag");

			int lowerLimit = 0;
			int currentPage = 1;
			String searchKey = null;
			int pageSize = 0;
			if (null != pageFlag && pageFlag.equals("true")) {
				if (Integer.valueOf(pageNumber) != 0) {
					/*
					 * Application should navigate to the previous page , If
					 * user delete 21 record
					 */
					if (pageSess.getTotalSize() > 20) {
						currentPage = Integer.valueOf(pageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					} else {
						lowerLimit = 0;
					}
					/* End */
				} else {
					lowerLimit = 0;
				}
			}
			searchKey = customPageForm.getSearchKey();
			if (pageType != null && "Special Offer".equals(pageType)) {
				resultLst = retailerService.getspecialOfferList(loginUser.getRetailerId(), customPageForm, lowerLimit, recordCount);
			} else {
				resultLst = retailerService.getCusomtPageList(loginUser.getRetailerId(), searchKey, lowerLimit, recordCount);
			}
			request.setAttribute("customPageList", resultLst);
			Pagination objPage = Utility.getPagination(resultLst.getTotalSize(), currentPage, "showpagelist.htm", recordCount);
			session.setAttribute("pagination", objPage);
			model.put("createsplofferform", customPageForm);
		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (pageType != null && "Special Offer".equals(pageType)) {
			return new ModelAndView("specialOfferList");
		} else {
			return new ModelAndView("pageList");
		}
	}

	/**
	 * This method is used for build anything page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/buildAnythingPage", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView buildAnythingPage(RetailerCustomPage objCustomPage, ModelMap model, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException {
		final String methodName = "buildAnythingPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		String pageFlag = (String) request.getParameter("pageFlag");
		String flag = (String) session.getAttribute("page");
		String pageNumber = "0";
		int lowerLimit = 0;
		int currentPage = 1;
		String searchKey = null;
		SearchResultInfo result = new SearchResultInfo();
		int recordCount = 20;
		try {
			/*
			 * if ("true".equals(flag)) { pageFlag = flag; pageNumber =
			 * request.getParameter("pageNumber"); session.setAttribute("page",
			 * ""); session.setAttribute("pageNumber", ""); Pagination pageSess
			 * = (Pagination) session.getAttribute("pagination"); if
			 * (Integer.valueOf(pageNumber) != 0) { currentPage =
			 * Integer.valueOf(pageNumber); int number =
			 * Integer.valueOf(currentPage) - 1; int pageSize =
			 * pageSess.getPageRange(); lowerLimit = pageSize *
			 * Integer.valueOf(number); } pageFlag = "false"; }
			 */

			if (null != objCustomPage.getRecordCount() && !"".equals(objCustomPage.getRecordCount())
					&& !"undefined".equals(objCustomPage.getRecordCount())) {
				recordCount = Integer.valueOf(objCustomPage.getRecordCount());
			}
			if (null != pageFlag && pageFlag.equals("true")) {
				pageNumber = request.getParameter("pageNumber");
				objCustomPage.setPageNumber(pageNumber);
				session.setAttribute("pageNumber", pageNumber);
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
				}
				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));

			} else {
				objCustomPage.setPageNumber(String.valueOf(currentPage));
			}
			searchKey = request.getParameter("searchKey");
			result = retailerService.getCusomtPageList(loginUser.getRetailerId(), searchKey, lowerLimit, recordCount);
			if (searchKey == null) {
				searchKey = "";
			}

			request.setAttribute("customPageList", result);
			Pagination objPage = Utility.getPagination(result.getTotalSize(), currentPage, "buildAnythingPage.htm", recordCount);
			if (objPage.getTotalSize() == 20) {
				objPage.setPageRange(20);
			}
			session.setAttribute("pagination", objPage);
			model.put("customPageForm", objCustomPage);

			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("MyAppsite", "AnythingPage", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (result.getTotalSize() > 0 || !"".equals(searchKey)) {
			if (result.getTotalSize() == 0) {
				request.setAttribute("message", "No records found!");
			}
			return new ModelAndView("pageList");
		}

		return new ModelAndView("buildAnythingPage");
	}

	/**
	 * This method is used for build anything page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/cstmMainPg", method = RequestMethod.GET)
	public ModelAndView cstmMainPg(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "cstmMainPg";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		Long retailID = null;
		retailID = Long.valueOf(loginUser.getRetailerId());
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		ArrayList<RetailerLocation> retailerLocations = null;
		retailerLocations = retailerService.getRetailerLocations(retailID);
		if (retailerLocations != null) {
			session.setAttribute("retailerLocList", retailerLocations);
		} else {
			session.setAttribute("retailerLocList", "");
		}
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		model.put("createCustomPage", retailerCustomPage);
		session.setAttribute("imageCropPage", "CreatePage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("cstmMainPg");
	}

	/**
	 * This method is used for build anything page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/linktoExstngPg", method = RequestMethod.GET)
	public ModelAndView linktoExstngPg(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "linktoExstngPg";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		Long retailID = null;
		ArrayList<RetailerImages> imageList = new ArrayList<RetailerImages>();
		retailID = Long.valueOf(loginUser.getRetailerId());
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		ArrayList<RetailerLocation> retailerLocations = null;
		retailerLocations = retailerService.getRetailerLocations(retailID);
		imageList = retailerService.getRetailerImageIconsDisplay("AttachLink");
		retailerCustomPage.setImageUplType("slctdUpld");
		if (retailerLocations != null) {
			session.setAttribute("retailerLocList", retailerLocations);
		} else {
			session.setAttribute("retailerLocList", "");
		}
		session.setAttribute("imageList", imageList);
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		model.put("createCustomPage", retailerCustomPage);
		session.setAttribute("imageCropPage", "CreatePage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("linktoExstngPg");
	}

	/**
	 * This method is used for build anything page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/retailercreatedpage.htm", method = RequestMethod.GET)
	public ModelAndView buildAnythingPageNew(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "buildAnythingPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		return new ModelAndView("buildAnythingPage");
	}

	/**
	 * This method is used for special offer page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException.
	 */
	@RequestMapping(value = "/specialOffer.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView specialOfferPage(RetailerCustomPage objCustomPage, BindingResult bResult, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		final String methodName = "specialOfferPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
//		String pageFlag = (String) request.getParameter("pageFlag");
		//String flag = (String) session.getAttribute("page");
		String pageNumber = "0";
		int lowerLimit = 0;
		int currentPage = 1;
		//String searchKey = null;
		SearchResultInfo result = new SearchResultInfo();
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		
		final String pageFlag = objCustomPage.getPageFlag();
		pageNumber = objCustomPage.getPageNumber();
		int recordCount = 20;
		try {

			/*
			 * if ("true".equals(flag)) { pageFlag = flag; pageNumber =
			 * request.getParameter("pageNumber"); session.setAttribute("page",
			 * ""); session.setAttribute("pageNumber", ""); Pagination pageSess
			 * = (Pagination) session.getAttribute("pagination"); if
			 * (Integer.valueOf(pageNumber) != 0) { currentPage =
			 * Integer.valueOf(pageNumber); int number =
			 * Integer.valueOf(currentPage) - 1; int pageSize =
			 * pageSess.getPageRange(); lowerLimit = pageSize *
			 * Integer.valueOf(number); } pageFlag = "false"; }
			 */

			if (null != objCustomPage.getRecordCount() && !"".equals(objCustomPage.getRecordCount())
					&& !"undefined".equals(objCustomPage.getRecordCount())) {
				recordCount = Integer.valueOf(objCustomPage.getRecordCount());
			}

			if (null != pageFlag && pageFlag.equals("true")) {
//				pageNumber = request.getParameter("pageNumber");
				objCustomPage.setPageNumber(pageNumber);
				retailerCustomPage.setPageNumber(pageNumber);
				session.setAttribute("page", pageFlag);
				session.setAttribute("pageNumber", pageNumber);
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					if (pageSess.getTotalSize() > 20) {
						currentPage = Integer.valueOf(pageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						final int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					} else {
						lowerLimit = 0;
					}
				} else {
					lowerLimit = 0;
				}
			} else {
				retailerCustomPage.setPageNumber(String.valueOf(currentPage));
			}
			
			
			result = retailerService.getspecialOfferList(loginUser.getRetailerId(), objCustomPage, lowerLimit, recordCount);
			request.setAttribute("customPageList", result);

			
			Pagination objPage = Utility.getPagination(result.getTotalSize(), currentPage, "specialOffer.htm", recordCount);

			session.setAttribute("pagination", objPage);
			model.put("createsplofferform", retailerCustomPage);

			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("Promotions", "SplOfferPage", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);

		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (result.getTotalSize() > 0 || !"".equals(Utility.checkNull(objCustomPage.getSearchKey()))) {
			if (result.getTotalSize() == 0) {
				request.setAttribute("message", "No records found!");
			}
			return new ModelAndView("specialOfferList");
		}
		return new ModelAndView("specialOfferPage");
	}

	/**
	 * This method is used for special offer main page page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/cstmMainSplPg.htm", method = RequestMethod.GET)
	public ModelAndView cstmMainSplPg(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "cstmMainSplPg";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		Long retailID = null;
		retailID = Long.valueOf(loginUser.getRetailerId());
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		ArrayList<RetailerLocation> retailerLocations = null;
		retailerLocations = retailerService.getRetailerLocations(retailID);
		if (retailerLocations != null) {
			session.setAttribute("retailerLocList", retailerLocations);
		} else {
			session.setAttribute("retailerLocList", "");
		}
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		model.put("createCustomPage", retailerCustomPage);
		session.setAttribute("imageCropPage", "SpecialOfferPage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("splOfferMain");
	}

	/**
	 * This method is used for special offer link page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/linktoExstngSplPg.htm", method = RequestMethod.GET)
	public ModelAndView linktoExstngSplPg(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "linktoExstngSplPg";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		Long retailID = null;
		ArrayList<RetailerImages> imageList = new ArrayList<RetailerImages>();
		retailID = Long.valueOf(loginUser.getRetailerId());
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		ArrayList<RetailerLocation> retailerLocations = null;
		retailerLocations = retailerService.getRetailerLocations(retailID);
		imageList = retailerService.getRetailerImageIconsDisplay("AttachLink");
		if (retailerLocations != null) {
			session.setAttribute("retailerLocList", retailerLocations);
		} else {
			session.setAttribute("retailerLocList", "");
		}
		session.setAttribute("imageList", imageList);
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		model.put("createCustomPage", retailerCustomPage);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("linktoExstngPg");
	}

	/**
	 * This method is used for build anything page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/retailersplofferpage.htm", method = RequestMethod.GET)
	public ModelAndView retailersplofferpage(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "retailersplofferpage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		return new ModelAndView("specialOfferPage");
	}

	@RequestMapping(value = "/splofferlinktoExstngPg", method = RequestMethod.GET)
	public ModelAndView SpecialOfferlinktoExstngPg(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "linktoExstngPg";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		Long retailID = null;
		ArrayList<RetailerImages> imageList = new ArrayList<RetailerImages>();
		retailID = Long.valueOf(loginUser.getRetailerId());
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		retailerCustomPage.setImageUplType("slctdUpld");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		ArrayList<RetailerLocation> retailerLocations = null;
		retailerLocations = retailerService.getRetailerLocations(retailID);
		imageList = retailerService.getRetailerImageIconsDisplay("AttachLink");
		if (retailerLocations != null) {
			session.setAttribute("retailerLocList", retailerLocations);
		} else {
			session.setAttribute("retailerLocList", "");
		}
		session.setAttribute("imageList", imageList);
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		model.put("createCustomPage", retailerCustomPage);
		session.setAttribute("imageCropPage", "SpecialOfferPage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("splOfferLinktoExstngPage");
	}

	@RequestMapping(value = "/uploadpdf", method = RequestMethod.GET)
	public ModelAndView uploadPDFAnythingPage(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException {
		final String methodName = "uploadPDFAnythingPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		Long retailID = null;
		ArrayList<RetailerImages> imageList = new ArrayList<RetailerImages>();
		retailID = Long.valueOf(loginUser.getRetailerId());
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		ArrayList<RetailerLocation> retailerLocations = null;
		retailerLocations = retailerService.getRetailerLocations(retailID);
		imageList = retailerService.getRetailerImageIconsDisplay("UploadPDF");
		retailerCustomPage.setImageUplType("slctdUpld");
		session.removeAttribute("UploadedPDFFile");
		session.removeAttribute("oldUploadedPDFFile");
		if (retailerLocations != null) {
			session.setAttribute("retailerLocList", retailerLocations);
		} else {
			session.setAttribute("retailerLocList", "");
		}
		session.setAttribute("imageList", imageList);
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		model.put("createCustomPage", retailerCustomPage);
		session.setAttribute("imageCropPage", "CreatePage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("uploadpdfanythingpage");
	}

	/**
	 * This controller method is to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param objCustomPage
	 *            RetailerCustomPage instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return VIEW_NAME editRebates view.
	 */
	@RequestMapping(value = "/saveReorderlist.htm", method = RequestMethod.POST)
	public final ModelAndView saveReorderList(@ModelAttribute("customPageForm") RetailerCustomPage objCustomPage, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside CustomePageController : saveReorderList");
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			SearchResultInfo searchResult = new SearchResultInfo();
			String isDataInserted = null;
			String strNoMsg = null;
			String searchKey = null;
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			final int retailerId = loginUser.getRetailerId();
			final String pageFlag = (String) request.getParameter("pageFlag");
			String pageNumber = "0";
			int lowerLimit = 0;
			int currentPage = 1;
			final String strSortOrder = request.getParameter("sortOrderID");
			final String strPageID = request.getParameter("primaryKeys");
			
			searchKey = request.getParameter("searchKey");
			strNoMsg = request.getParameter("noMsg");
			int recordCount = 20;
			if (null != objCustomPage.getRecordCount() && !"".equals(objCustomPage.getRecordCount())
					&& !"undefined".equals(objCustomPage.getRecordCount())) {
				recordCount = Integer.valueOf(objCustomPage.getRecordCount());
			}
			if (pageFlag != null && "false".equals(pageFlag)) {

				isDataInserted = retailerService.saveReorderList(strSortOrder, strPageID, retailerId);
				if (isDataInserted.equalsIgnoreCase("SUCCESS")) {
					if (ApplicationConstants.NO_MSG_DISPLAY.equals(strNoMsg)) {
						request.setAttribute("successMSG", "Sort order is saved successfully.");
						request.setAttribute("manageAnyThing", "font-weight:bold;color:#00559c;");
					}
				}
			} else {
				pageNumber = request.getParameter("pageNumber");
				session.setAttribute("page", pageFlag);
				session.setAttribute("pageNumber", pageNumber);
				objCustomPage.setPageNumber(pageNumber);
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");

				if (Integer.valueOf(pageNumber) != 0) {
					/*
					 * Click on �Save Order� is pointing to empty �Anything
					 * PagesTM� screen, when you delete all anything pages in
					 * second page.
					 */
					if (pageSess.getTotalSize() > 20) {
						currentPage = Integer.valueOf(pageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						final int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					} else {
						lowerLimit = 0;
					}
				} else {
					lowerLimit = 0;
				}
				isDataInserted = retailerService.saveReorderList(strSortOrder, strPageID, retailerId);
				if (isDataInserted.equalsIgnoreCase("SUCCESS")) {
					if (ApplicationConstants.NO_MSG_DISPLAY.equals(strNoMsg)) {
						request.setAttribute("successMSG", "Sort order is saved successfully.");
						request.setAttribute("manageAnyThing", "font-weight:bold;color:#00559c;");
					}
				}
			}
			searchResult = retailerService.getCusomtPageList(loginUser.getRetailerId(), searchKey, lowerLimit, recordCount);
			request.setAttribute("customPageList", searchResult);
			final Pagination objPage = Utility.getPagination(searchResult.getTotalSize(), currentPage, "buildAnythingPage.htm", recordCount);
			session.setAttribute("pagination", objPage);
			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("MyAppsite", "AnythingPage", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);
			model.put("customPageForm", objCustomPage);
		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred inside CustomePageController : saveReorderList :" + e.getMessage());
			throw new ScanSeeServiceException(e.getMessage());
		}
		return new ModelAndView("pageList");
	}

	@RequestMapping(value = "/createcontactuspage.htm", method = RequestMethod.GET)
	public ModelAndView showRetailerContactUsPage(@ModelAttribute("contact") Contact contact, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		String methodName = "showRetailerContactUsPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		model.put("contact", contact);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("createcontactuspage");

	}

	/**
	 * This controller method will return locations associated with anything
	 * page.
	 * 
	 * @param request
	 *            http request object
	 * @param session
	 *            http session object.
	 * @param model
	 *            object to store form etc.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/showAnythingPageLocation.htm", method = RequestMethod.GET)
	public final ModelAndView showAnythingPageLocation(@ModelAttribute("customPageForm") RetailerCustomPage objCustomPage, BindingResult result,
			ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		String methodName = "showAnythingPageLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		List<RetailerLocation> apLocationList = null;

		try {
			apLocationList = (List<RetailerLocation>) retailerService.getLocationIDForAnythingPage(objCustomPage.getQRRetailerCustomPageID(),
					loginUser.getRetailerId());
		} catch (ScanSeeServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw exception;
		}

		model.addAttribute("customPageForm", objCustomPage);
		session.setAttribute("locationList", apLocationList);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("showLocation");
	}

	/**
	 * This controller method is used for reorder special offer page.
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException.
	 */
	@RequestMapping(value = "/savereorderspeciallist.htm", method = RequestMethod.POST)
	public ModelAndView reorderSpecialOfferPage(RetailerCustomPage objCustomPage, BindingResult bResult, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside CustomePageController : reorderSpecialOfferPage");

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		final int iRetailId = loginUser.getRetailerId();

		String pageFlag = (String) request.getParameter("pageFlag");

		String strPageNumber = "0";
		int lowerLimit = 0;
		int currentPage = 1;
		//String searchKey = null;
		String strResponseMsg = null;
		String strNoMsg = null;

		SearchResultInfo searchResult = new SearchResultInfo();
		RetailerCustomPage retCustomPage = new RetailerCustomPage();
		//searchKey = request.getParameter("searchKey");
		strNoMsg = request.getParameter("noMsg");

		int recordCount = 20;

		final String strSortOrder = request.getParameter("sortOrderID");
		final String strPageID = request.getParameter("primaryKeys");
		
		try {
			if (null != objCustomPage.getRecordCount() && !"".equals(objCustomPage.getRecordCount())
					&& !"undefined".equals(objCustomPage.getRecordCount())) {
				recordCount = Integer.valueOf(objCustomPage.getRecordCount());
			}

			if (null != pageFlag && pageFlag.equals("true")) {
				strPageNumber = request.getParameter("pageNumber");
				if ("".equals(Utility.checkNull(strPageNumber))) {
					strPageNumber = "0";
				}
				session.setAttribute("page", pageFlag);
				session.setAttribute("pageNumber", strPageNumber);
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(strPageNumber) != 0) {
					if (pageSess.getTotalSize() > 20) {
						currentPage = Integer.valueOf(strPageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						final int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					} else {
						lowerLimit = 0;
					}
				} else {
					lowerLimit = 0;
				}
			}

			strResponseMsg = retailerService.saveReorderSpecialOfferList(strSortOrder, strPageID, iRetailId);
			if (strResponseMsg.equalsIgnoreCase("SUCCESS")) {
				if (ApplicationConstants.NO_MSG_DISPLAY.equals(strNoMsg)) {
					request.setAttribute("successMSG", "Sort order is saved successfully.");
					request.setAttribute("manageSpecialOffer", "font-weight:bold;color:#00559c;");
				}
			}
			objCustomPage.setPageIds(strPageID);
			objCustomPage.setSortIds(strSortOrder);
			searchResult = retailerService.getspecialOfferList(loginUser.getRetailerId(), objCustomPage, lowerLimit, recordCount);
			request.setAttribute("customPageList", searchResult);

		/*	if ("".equals(Utility.checkNull(searchKey))) {
				searchKey = "";
			} else {
				retCustomPage.setSearchKey(searchKey);
			}*/
			Pagination objPage = Utility.getPagination(searchResult.getTotalSize(), currentPage, "specialOffer.htm", recordCount);

			session.setAttribute("pagination", objPage);
			model.put("createsplofferform", retCustomPage);

			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("Promotions", "SplOfferPage", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);

		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred inside CustomePageController : reorderSpecialOfferPage :" + e.getMessage());
			throw new ScanSeeServiceException(e.getMessage());
		}

		if (searchResult.getTotalSize() > 0 || !"".equals(Utility.checkNull(objCustomPage.getSearchKey()))) {
			if (searchResult.getTotalSize() == 0) {
				request.setAttribute("message", "No records found!");
			}
			return new ModelAndView("specialOfferList");
		}
		return new ModelAndView("specialOfferPage");
	}
}

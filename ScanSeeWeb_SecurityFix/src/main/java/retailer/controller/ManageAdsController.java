/**
 * @ (#) ManageAds.java 29-Dec-2011
 * Project       :ScanSeeWeb
 * File          : ManageAds.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 29-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import static common.constatns.ApplicationConstants.RETAILERSERVICE;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * This controller class is used to handle Welcome page module requests.
 * displaying all welcome pages in grid,Deleting welcome pages.
 * 
 * @author manjunatha_gh
 */
@Controller
public class ManageAdsController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageAdsController.class);

	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();

	/**
	 * variable for welcome page grid screen.
	 */
	public final String RETURNVIEW = "managebannerads";

	/**
	 * This method checks if retailer has already created welcome page then
	 * redirects to grid page, otherwise it will redirect to create welcome
	 * page.
	 * 
	 * @param addsVo
	 *            this variable contains request parameter from form if any.
	 * @param request
	 *            http request object.
	 * @param session
	 *            http session object.
	 * @param model
	 *            model object to store form.
	 * @return model and view object with view name.
	 * @throws ScanSeeServiceException
	 *             throws scansee exception.
	 */
	@RequestMapping(value = "/manageads.htm", method = RequestMethod.GET)
	public final ModelAndView showCreateCouponPage(@ModelAttribute("manageadform") RetailerLocationAdvertisement addsVo, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		LOG.info("showCreateCouponPage:: Inside Get Method");

		SearchResultInfo adList = null;
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		int recordCount = 20;
		try
		{
			// removing if any pagination object is active.
			session.removeAttribute("pagination");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(RETAILERSERVICE);
			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != addsVo.getRecordCount() && !"".equals(addsVo.getRecordCount()) && !"undefined".equals(addsVo.getRecordCount()))
			{
				recordCount = Integer.valueOf(addsVo.getRecordCount());
			}

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
				}

				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));
			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}

			final Users loginUser = (Users) session.getAttribute("loginuser");

			if (ISDUBUGENABLED)
			{
				LOG.debug("Retailer Id*********" + loginUser.getRetailerId());
				LOG.debug("searchKey*********" + searhKey);
			}

			final Long retailID = Long.valueOf(loginUser.getRetailerId());
			addsVo.setAdvertisementName(null);
			// adList = retailerService.getRetailerAdsByAdsNames(retailID,
			// objForm, lowerLimit);
			adList = getAdvertiseMentList(retailerService, retailID, objForm, lowerLimit, recordCount);
			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("MyAppsite", "WelcomePage", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);

			if (adList != null && adList.getAddsList().size() <= 0)
			{
				LOG.info("redirected to Create Banner page");
				return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/createbannerad.htm"));
			}
			else if (adList != null)
			{
				/*
				 * String sDate = null; String eDate = null; for
				 * (RetailerLocationAdvertisement ad : adList.getAddsList()) {
				 * sDate = ad.getAdvertisementDate(); if
				 * (!"".equals(Utility.checkNull(sDate))) { sDate =
				 * Utility.convertDBdate(sDate); ad.setAdvertisementDate(sDate);
				 * } eDate = ad.getAdvertisementEndDate(); if
				 * (!"".equals(Utility.checkNull(eDate))) { eDate =
				 * Utility.convertDBdate(eDate);
				 * ad.setAdvertisementEndDate(eDate); } }
				 */
				session.setAttribute("adsList", adList);
				final Pagination objPage = Utility.getPagination(adList.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/manageads.htm",
						recordCount);
				session.setAttribute("pagination", objPage);
			}
			else
			{
				request.setAttribute("message", "No Banner Ads to display");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e.getMessage());
			throw e;
		}
		LOG.info("showCreateCouponPage::  Exit Get Method");
		return new ModelAndView(RETURNVIEW);
	}

	/**
	 * this method handles the request for saving sort order.
	 * 
	 * @param retailerLocationAdvertisement
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @return model and view object with view name.
	 * @throws IOException
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/manageads.htm", method = RequestMethod.POST)
	public final ModelAndView onSubmitSearch(@ModelAttribute("manageadform") RetailerLocationAdvertisement retailerLocationAdvertisement,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside ManageAdsController : onSubmitSearch ");
		SearchResultInfo adList = null;
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		int recordCount = 20;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(RETAILERSERVICE);
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != retailerLocationAdvertisement.getRecordCount() && !"".equals(retailerLocationAdvertisement.getRecordCount())
					&& !"undefined".equals(retailerLocationAdvertisement.getRecordCount()))
			{
				recordCount = Integer.valueOf(retailerLocationAdvertisement.getRecordCount());
			}

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
				}

				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));
			}

			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}

			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (ISDUBUGENABLED)
			{
				LOG.debug("Retailer Id*********" + loginUser.getRetailerId());
				LOG.debug("searchKey*********" + searhKey);
			}
			final Long retailID = Long.valueOf(loginUser.getRetailerId());
			if (!"".equals(Utility.checkNull(retailerLocationAdvertisement.getAdvertisementName())))
			{
				final String adsName = Utility.checkNull(retailerLocationAdvertisement.getAdvertisementName());
				objForm.setSearchKey(adsName);
			}
			adList = getAdvertiseMentList(retailerService, retailID, objForm, lowerLimit, recordCount);
			// adList = retailerService.getRetailerAdsByAdsNames(retailID,
			// objForm, lowerLimit);
			if (!adList.getAddsList().isEmpty())
			{
				// if (adList != null)
				// {
				/*
				 * for (int i = 0; i < adList.getAddsList().size(); i++) { sDate
				 * = adList.getAddsList().get(i).getAdvertisementDate(); sDate =
				 * Utility.convertDBdate(sDate);
				 * adList.getAddsList().get(i).setAdvertisementDate(sDate); if
				 * (!
				 * "".equals(adList.getAddsList().get(i).getAdvertisementEndDate
				 * ())) { eDate =
				 * adList.getAddsList().get(i).getAdvertisementEndDate();
				 * retailerLocationAdvertisement
				 * .setDbAdvertisementEndDate(eDate);
				 * adList.getAddsList().get(i)
				 * .setAdvertisementEndDate(retailerLocationAdvertisement
				 * .getDbAdvertisementEndDate()); } }
				 */
				session.setAttribute("adsList", adList);
				final Pagination objPage = Utility.getPagination(adList.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/manageads.htm",
						recordCount);
				session.setAttribute("pagination", objPage);
				// }
			}
			else
			{
				session.setAttribute("adsList", adList);
				session.removeAttribute("pagination");
				request.setAttribute("message", "No records found");
			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw exception;
		}
		catch (NullPointerException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw exception;
		}
		return new ModelAndView(RETURNVIEW);
	}

	/**
	 * This method will delete the requested welcome pages.
	 * 
	 * @param objretLocAd
	 *            this object contains id and name of requested welcome page.
	 * @param request
	 *            http request object.
	 * @param session
	 *            http session object.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/deleteWelcomePage.htm", method = RequestMethod.POST)
	public final ModelAndView deleteWelcomePage(@ModelAttribute("manageadform") RetailerLocationAdvertisement objretLocAd,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageAdsController : deleteWelcomePage ");
		String strResponseStatus = null;
		final int lowerLimit = 0;
		final SearchForm objSearchForm = new SearchForm();
		SearchResultInfo adList = null;
		final int iCurrentPage = 1;
		int recordCount = 20;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(RETAILERSERVICE);
			final Users loginUser = (Users) session.getAttribute("loginuser");
			objretLocAd.setRetailID(Long.valueOf(loginUser.getRetailerId()));
			strResponseStatus = retailerService.deleteWelcomePage(objretLocAd);

			if (strResponseStatus.equals(ApplicationConstants.SUCCESS))
			{
				if (null != objretLocAd.getRecordCount() && !"".equals(objretLocAd.getRecordCount())
						&& !"undefined".equals(objretLocAd.getRecordCount()))
				{
					recordCount = Integer.valueOf(objretLocAd.getRecordCount());
				}

				adList = getAdvertiseMentList(retailerService, objretLocAd.getRetailID(), objSearchForm, lowerLimit, recordCount);
				// adList =
				// retailerService.getRetailerAdsByAdsNames(objretLocAd.getRetailID(),
				// objSearchForm, lowerLimit);

				if (adList != null && adList.getAddsList().size() <= 0)
				{
					return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/createbannerad.htm"));
				}
				else
				{
					/*
					 * for (int i = 0; i < adList.getAddsList().size(); i++) {
					 * sDate =
					 * adList.getAddsList().get(i).getAdvertisementDate(); sDate
					 * = Utility.convertDBdate(sDate);
					 * adList.getAddsList().get(i).setAdvertisementDate(sDate);
					 * if (!"".equals(adList.getAddsList().get(i).
					 * getAdvertisementEndDate())) { eDate =
					 * adList.getAddsList().get(i).getAdvertisementEndDate();
					 * objretLocAd.setDbAdvertisementEndDate(eDate);
					 * adList.getAddsList
					 * ().get(i).setAdvertisementEndDate(objretLocAd
					 * .getDbAdvertisementEndDate()); } }
					 */
					session.setAttribute("adsList", adList);
					final Pagination objPage = Utility.getPagination(adList.getTotalSize(), iCurrentPage, "/ScanSeeWeb/retailer/manageads.htm",
							recordCount);
					session.setAttribute("pagination", objPage);
				}
				if (objretLocAd.getExpireFlag() == 0 && objretLocAd.getDeleteFlag() == 1)
				{
					request.setAttribute("message", "Welcome Page is deleted Successfully!");
				}
				else if (objretLocAd.getDeleteFlag() == 0 && objretLocAd.getExpireFlag() == 1)
				{
					request.setAttribute("message", "Welcome page campaign has been stopped successfully!");
				}
			}
			else
			{
				if (objretLocAd.getExpireFlag() == 0 && objretLocAd.getDeleteFlag() == 1)
				{
					request.setAttribute("message", "Error occurred while deleting welcome page!");
				}
				else if (objretLocAd.getDeleteFlag() == 0 && objretLocAd.getExpireFlag() == 1)
				{
					request.setAttribute("message", "Error occurred while stopping the compaign of the welcome page!");
				}
			}
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw exception;
		}
		return new ModelAndView(RETURNVIEW);
	}

	/**
	 * This controller method will return location details info associated with
	 * welcome page from Database based on parameter(adsIDs)..
	 * 
	 * @param request
	 *            http request object
	 * @param session
	 *            http session object.
	 * @param model
	 *            object to store form etc.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/showLocation.htm", method = RequestMethod.GET)
	public final ModelAndView showLocation(@ModelAttribute("showLocationForm") RetailerLocationAdvertisement objretLocAd, BindingResult result,
			Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside ManageAdsController : showLocation ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		List<RetailerLocation> arLocationList = null;
		try
		{
			arLocationList = (List<RetailerLocation>) retailerService.getLocationIDForWelcomePage(objretLocAd.getRetailLocationAdvertisementID());
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw exception;
		}
		model.addAttribute("showLocationForm", objretLocAd);
		session.setAttribute("locationList", arLocationList);
		return new ModelAndView("showLocation");
	}

	/**
	 * This method will return the welcome pages created by the logged in
	 * retailer.
	 * 
	 * @param retailerService
	 *            object to call service layer.
	 * @param retailerId
	 *            logged in retailer id.
	 * @param objSearchForm
	 *            object contains search keywords.
	 * @param lowerLimit
	 *            values for pagination.
	 * @return info objects contains search result.
	 * @throws ScanSeeServiceException
	 *             throws exception if any.
	 */
	public SearchResultInfo getAdvertiseMentList(RetailerService retailerService, Long retailerId, SearchForm objSearchForm, int lowerLimit,
			int recordCount) throws ScanSeeServiceException
	{
		final String methoName = "getRetailerAdsByAdsNames";
		LOG.info(ApplicationConstants.METHODSTART + methoName);

		final SearchResultInfo info = retailerService.getRetailerAdsByAdsNames(retailerId, objSearchForm, lowerLimit, recordCount);
		String sDate = null;
		String eDate = null;
		if (!info.getAddsList().isEmpty())
		{
			for (RetailerLocationAdvertisement ad : info.getAddsList())
			{
				sDate = ad.getAdvertisementDate();
				if (!"".equals(Utility.checkNull(sDate)))
				{
					sDate = Utility.convertDBdate(sDate);
					ad.setAdvertisementDate(sDate);
				}
				eDate = ad.getAdvertisementEndDate();
				if (!"".equals(Utility.checkNull(eDate)))
				{
					eDate = Utility.convertDBdate(eDate);
					ad.setAdvertisementEndDate(eDate);
				}

			}
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return info;
	}

}

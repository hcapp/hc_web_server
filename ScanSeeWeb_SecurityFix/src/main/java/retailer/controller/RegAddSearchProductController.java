/**
o * @ (#) RegAddSearchProductController.java 06-Jan-2012
 * Project       :ScanSeeWeb
 * File          : RegAddSearchProductController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 06-Jan-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.exception.ScanSeeServiceException;
import common.pojo.ProductVO;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationProduct;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.Utility;

@Controller
public class RegAddSearchProductController
{
	private static final Logger LOG = LoggerFactory.getLogger(RegAddSearchProductController.class);
	final public String RETURN_VIEW = "regaddsearchprod";

	@RequestMapping(value = "regaddseachprod.htm", method = RequestMethod.GET)
	public ModelAndView showProductSetupPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside  RegAddSearchProductController  : showProductSetupPage ");
		try
		{
			request.getSession().removeAttribute("message");
			session.removeAttribute("pagination");
			session.removeAttribute("retLocList");
			session.removeAttribute("seacrhList");
			session.removeAttribute("searchproduct");
			session.removeAttribute("manageAddProd");
			Long retailID = null;
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			retailID = Long.valueOf(loginUser.getRetailerId());

			List<RetailerLocation> listRetLoc = null;

			RetailerLocationProduct retailerLocation = new RetailerLocationProduct();
			model.put("retprodsetupform", retailerLocation);

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			// prodInfoList = retailerService.getAllProdInfo(null);

			listRetLoc = retailerService.getRetailerLocation(retailID);
			session.setAttribute("retLocList", listRetLoc);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside  RegAddSearchProductController  : showProductSetupPage " + e.getMessage());
			LOG.error("Inside  RegAddSearchProductController  : showProductSetupPage " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(RETURN_VIEW);
	}

	@RequestMapping(value = "regaddseachprod.htm", method = RequestMethod.POST)
	public ModelAndView productSetupPage(@ModelAttribute("retprodsetupform") RetailerLocationProduct retailerLocation, BindingResult result,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside  RegAddSearchProductController  : productSetupPage ");
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		String productName = null;
		List<ProductVO> productList = new ArrayList<ProductVO>();

		try
		{
			session.removeAttribute("manageAddProd");
			session.setAttribute("manageAddProd", "addProduct");

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			// prodInfoList = retailerService.getAllProdInfo(null);
			String pageFlag = (String) request.getParameter("pageFlag");

			if (null == pageFlag)
			{
				productName = retailerLocation.getProductName();
			}
			if (null != pageFlag && pageFlag.equals("true"))
			{
				objForm = (SearchForm) session.getAttribute("searchproduct");
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}
			}
			if (objForm == null)
			{
				// searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				if (null != productName)
				{
					objForm.setSearchKey(productName.trim());
				}
				else
				{
					objForm.setSearchKey(productName);
				}

				session.setAttribute("searchproduct", objForm);
			}

			SearchResultInfo resultInfo = retailerService.searchProducts(objForm, lowerLimit, 20);

			if (resultInfo != null)
			{
				for (int i = 0; i < resultInfo.getProductList().size(); i++)
				{
					ProductVO productVO = resultInfo.getProductList().get(i);

					productVO.setRowNumber(i);
					productList.add(productVO);
				}
				resultInfo.setProductList(productList);
				session.setAttribute("seacrhList", resultInfo);
				Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/regaddseachprod.htm");
				session.setAttribute("pagination", objPage);
			}
			else
			{
				session.removeAttribute("pagination");
				session.removeAttribute("seacrhList");
				result.reject("regaddsearchprod.error");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside  RegAddSearchProductController  : productSetupPage " + e.getMessage());
			LOG.error("Inside  RegAddSearchProductController  : productSetupPage " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(RETURN_VIEW);
	}

	@RequestMapping(value = "regRetLocProd.htm", method = RequestMethod.POST)
	public ModelAndView getRegRetailLocProdSearch(@ModelAttribute("retprodsetupform") RetailerLocationProduct retailerLocation, BindingResult result,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside  RegAddSearchProductController  : getRegRetailLocProdSearch ");
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		String productName = null;
		List<ProductVO> productList = new ArrayList<ProductVO>();
		try
		{
			session.removeAttribute("seacrhList");
			session.removeAttribute("manageAddProd");
			request.getSession().removeAttribute("message");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			String pageFlag = (String) request.getParameter("pageFlag");
			session.setAttribute("manageAddProd", "manageProd");
			if (null != retailerLocation.getLocID())
			{
				retailerLocation.setRetailLocIDHidden(retailerLocation.getLocID());
				session.setAttribute("retaileLocProd", retailerLocation.getLocID());
			}
			if (null != (String) session.getAttribute("retaileLocProd"))
			{
				retailerLocation.setLocID((String) session.getAttribute("retaileLocProd"));
			}
			if (null == pageFlag)
			{
				productName = retailerLocation.getProductName();
			}
			if (null != pageFlag && pageFlag.equals("true"))
			{
				objForm = (SearchForm) session.getAttribute("searchFormProduct");
				pageNumber = request.getParameter("pageNumber");
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}
			}
			if (objForm == null)
			{
				// searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				if (null != productName)
				{
					objForm.setSearchKey(productName.trim());
				}
				else
				{
					objForm.setSearchKey(productName);
				}

				session.setAttribute("searchFormProduct", objForm);
			}

			SearchResultInfo resultInfo = retailerService.getRetailLocProd(objForm.getSearchKey(), lowerLimit, loginUser.getRetailerId(),
					retailerLocation.getLocID(), 20);
			session.setAttribute("seacrhList", resultInfo);

			if (resultInfo != null)
			{
				for (int i = 0; i < resultInfo.getProductList().size(); i++)
				{
					ProductVO productVO = resultInfo.getProductList().get(i);

					if (!Utility.checkNull(productVO.getSaleStartDate()).equals(""))
					{
						productVO.setSaleStartDate(Utility.formattedDateWithTime(productVO.getSaleStartDate()));
					}
					if (!Utility.checkNull(productVO.getSaleEndDate()).equals(""))
					{
						productVO.setSaleEndDate(Utility.formattedDateWithTime(productVO.getSaleEndDate()));
					}
					productVO.setRowNumber(i);
					productList.add(productVO);
				}
				resultInfo.setProductList(productList);
				session.setAttribute("seacrhList", resultInfo);
				Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/regRetLocProd.htm");
				session.setAttribute("pagination", objPage);
			}
			else
			{
				session.removeAttribute("pagination");
				session.removeAttribute("seacrhList");
				result.reject("regmanagesearchprod.error");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside  RegAddSearchProductController  : getRegRetailLocProdSearch " + e.getMessage());

			throw new ScanSeeServiceException(e);
		}
		catch (ParseException e)
		{
			LOG.info("Inside  RegAddSearchProductController  : getRegRetailLocProdSearch " + e.getMessage());

			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(RETURN_VIEW);
	}
}

/**
 * @ (#) CreateBannerAdController.java 28-Dec-2011
 * Project       :ScanSeeWeb
 * File          : CreateBannerAdController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 28-Dec-2011
 *
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.CreateBannerAdValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.RetailerProfile;
import common.pojo.Users;
import common.util.Utility;

/**
 * CreateBannerAdController is a controller class for Build Retailer Banner Page screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class CreateBannerAdController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CreateBannerAdController.class);

	/**
	 * Variable RETURN_VIEW declared as constant string.
	 */
	private final String returnView = "createBannerAdd";

	/**
	 * createBannerAdValidator as instance of CreateBannerAdValidator.
	 */
	private CreateBannerAdValidator createBannerAdValidator;

	/**
	 * Getting the CreateBannerAdValidator Instance.
	 * 
	 * @param createBannerAdValidator
	 *            is instance of CreateBannerAdValidator.
	 *            createBannerAdValidator the createBannerAdValidator to set.
	 */
	@Autowired
	public void setCreateBannerAdValidator(CreateBannerAdValidator createBannerAdValidator)
	{
		this.createBannerAdValidator = createBannerAdValidator;
	}

	/**
	 * This controller method will display new banner Page to user.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return RETURN_VIEW addHotDeals view.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/createbannerad.htm", method = RequestMethod.GET)
	public final String createBannerAdPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside CreateBannerAdController : createBannerAdPage ");
		try
		{
			final RetailerLocationAdvertisement retailerLocationAds = new RetailerLocationAdvertisement();
			model.put("createbanneradform", retailerLocationAds);
			session.removeAttribute("imageCropSubmitURL");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			ArrayList<RetailerLocation> retailerLocations = null;

			Long retailID = null;
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			retailID = Long.valueOf(loginUser.getRetailerId());

			retailerLocations = retailerService.getRetailerLocations(retailID);

			if (retailerLocations != null)
			{
				session.setAttribute("retailerLocList", retailerLocations);
			} else {
				session.setAttribute("retailerLocList", "");
			}
			session.setAttribute("welcomePageCreateImgPath", ApplicationConstants.UPLOAD_IMAGE_PATH);
			session.setAttribute("minCropWd", 320);
			session.setAttribute("minCropHt", 460);
			session.setAttribute("imageCropPage", "CreateWelcomePage");
			session.setAttribute("welcomePageBtnVal", "WelcomePageAds");
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return returnView;
	}

	/**
	 * This controller method will create new banner page info by calling
	 * service layer.
	 * 
	 * @param retailerLocationAds instance of RetailerLocationAdvertisement.
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException  will be thrown.
	 * @throws IOException will be thrown.
	 */
	@RequestMapping(value = "/createbannerad.htm", method = RequestMethod.POST)
	public final ModelAndView onSubmitWelcomeInfo(@ModelAttribute("createbanneradform") RetailerLocationAdvertisement retailerLocationAds,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside CreateBannerAdController : onSubmitWelcomeInfo");
		String view = returnView;
		String serviceResponse = null;
		String compDate = null;
		final Date currentDate = new Date();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			Long retailID = null;
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			retailID = Long.valueOf(loginUser.getRetailerId());
			retailerLocationAds.setRetailID(retailID);
			retailerLocationAds.setRetailLocationIDHidden(retailerLocationAds.getRetailLocationIds());
			createBannerAdValidator.validate(retailerLocationAds, result);
			// For Indefinite Duration of Ads
			//Utility.setAdStartAndEndDate(retailerLocationAds);
			final FieldError fieldStartDate = result.getFieldError("advertisementDate");
			if (fieldStartDate == null)
			{
				compDate = Utility.compareCurrentDate(retailerLocationAds.getAdvertisementDate(), currentDate);
				if (null != compDate)
				{
					createBannerAdValidator.validate(retailerLocationAds, result, ApplicationConstants.DATESTARTCURRENT);
				}
			}
			if (!"".equals(Utility.checkNull(retailerLocationAds.getAdvertisementEndDate())))
			{
				final FieldError fieldEndDate = result.getFieldError("advertisementEndDate");
				if (fieldEndDate == null)
				{
					compDate = Utility.compareCurrentDate(retailerLocationAds.getAdvertisementEndDate(), currentDate);
					if (null != compDate)
					{
						createBannerAdValidator.validate(retailerLocationAds, result, ApplicationConstants.DATEENDCURRENT);
					}
					else
					{
						compDate = Utility.compareDate(retailerLocationAds.getAdvertisementDate(), retailerLocationAds.getAdvertisementEndDate());
						if (null != compDate)
						{
							createBannerAdValidator.validate(retailerLocationAds, result, ApplicationConstants.DATEAFTER);
						}
					}
				}
			}
			if ("".equals(Utility.checkNull(retailerLocationAds.getStrBannerAdImagePath())))
			{
				result.rejectValue("bannerAdImagePath", "Please Upload Welcome Page Image", "Please Upload Welcome Page Image");
			}
			
			if (result.hasErrors())
			{
				return new ModelAndView(returnView);
			}
			else
			{
				serviceResponse = retailerService.insertBannerAdInfo(retailerLocationAds);
				if (serviceResponse.equals(ApplicationConstants.SUCCESS))
				{
					if (!"".equals(Utility.checkNull(retailerLocationAds.getInvalidLocationIds())))
					{
						retailerLocationAds.setRetailLocationIDHidden(null);
						retailerLocationAds.setRetailLocationIDHidden(retailerLocationAds.getInvalidLocationIds());
						request.setAttribute("message", "There is already welcome page in given date range for selected location.");
						view = returnView;
					}
					else
					{
						view = "manageads.htm";
					}
				}
				else if (serviceResponse.equals(ApplicationConstants.FAILURE))
				{
					view = returnView;
					result.reject("bannerAdDateCheck.error");
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (view.equals(returnView))
		{
			return new ModelAndView(view);
		}
		else
		{
			return new ModelAndView(new RedirectView(view));
		}
	}

	/**
	 * This controller method will display banner page details in IPhone format screen .
	 * 
	 * @param objretLocAd as request parameter.
	 * @param result as request parameter.
	 * @param request as request parameter.
	 * @param response as request parameter.
	 * @param session as request parameter.
	 * @param map as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException will be thrown.
	 * @throws IOException will be thrown.
	 */
	@RequestMapping(value = "/previewRetailerAds.htm", method = RequestMethod.POST)
	public final ModelAndView previewDisplayImage(@ModelAttribute("previewRetailerAdsform") RetailerLocationAdvertisement objretLocAd,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map)
			throws ScanSeeServiceException, IOException
	{
		LOG.info("Inside CreateBannerAdController : previewDisplayImage ");
		String strBannerName = null;
		InputStream isBannerAd = null;
		OutputStream osBannerAd = null;
		RetailerProfile objRetName = new RetailerProfile();
		try
		{
			final RetailerLocationAdvertisement retailerLocationAds = new RetailerLocationAdvertisement();
			map.put("createbanneradform", retailerLocationAds);
			request.getSession().removeAttribute("message");
			request.getSession().removeAttribute("retailerName");
			request.getSession().removeAttribute("fileRibbonAd");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			objRetName = retailerService.getRetailerProfile(loginUser);
			final String strRetailName = objRetName.getRetailerName();
			request.getSession().setAttribute("retailerName", strRetailName);
			final MultipartFile fileBannerAd = objretLocAd.getBannerAdImagePath();

			if (fileBannerAd.getSize() > 0)
			{
				isBannerAd = fileBannerAd.getInputStream();
				strBannerName = request.getRealPath("") + "/images/" + fileBannerAd.getOriginalFilename();
				osBannerAd = new FileOutputStream(strBannerName);

				int readBytes = 0;
				final byte[] bufferBanner = new byte[100000];
				while ((readBytes = isBannerAd.read(bufferBanner, 0, 100000)) != -1)
				{
					osBannerAd.write(bufferBanner, 0, readBytes);
				}
				isBannerAd.close();
				osBannerAd.close();
			}
			session.setAttribute("fileBannerAd", "../images/" + fileBannerAd.getOriginalFilename());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (IOException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView("previewRetailerAd");
	}
}

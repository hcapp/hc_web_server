/**
 * @ (#) ManageBannerAdsController.java 08-Aug-2011
 * Project       : ScanSeeWeb
 * File          : ManageBannerAdsController.java
 * Author        : Kumar D
 * Company       : Span Systems Corporation
 * Date Created  : 08-Aug-2011
 *
 * @author       : Kumar D
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */
package retailer.controller;

import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.BuildBannerAdValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.Users;
import common.util.Utility;

/**
 * EditBannerAdController is a controller class for edit banner page screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class EditBannerAdController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditWelcomePageController.class);

	/**
	 * Variable objEditAdValidator declared as instance of
	 * CreateBannerAdValidator.
	 */
	private BuildBannerAdValidator buildBannerAdValidator;

	/**
	 * Variable VIEW_NAME declared as String.
	 */
	private final String viewName = "editBannerAd";
	
	/**
	 * Getting the CreateBannerAdValidator Instance.
	 * 
	 * @param buildBannerAdValidator
	 *            is instance of CreateBannerAdValidator. objEditAdValidator the
	 *            CreateBannerAdValidator to set.
	 */
	@Autowired
	public final void setBuildBannerAdValidator(BuildBannerAdValidator buildBannerAdValidator)
	{
		this.buildBannerAdValidator = buildBannerAdValidator;
	}


	/**
	 * This controller method will display retailer banner page based on input parameter (bannerId) by calling service method.
	 * @param objRetLocAds instance of RetailerLocationAdvertisement.
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param model as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/editBannerAd.htm", method = RequestMethod.POST)
	public final String editBannerAdsInfo(@ModelAttribute("editManageAdsForm") RetailerLocationAdvertisement objRetLocAds, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside EditWelcomePageController : editBannerAdsInfo ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		List<RetailerLocationAdvertisement> arAdsList = null;
		List<RetailerLocation> arLocationList = null;
		String strAdsStartDate = null;
		String strAdsEndtDate = null;
		ArrayList<RetailerLocation> arRetLocationList = null;
		Long lRetailID = null;
		RetailerLocationAdvertisement objRetLocationAds = null;
		String strBanADImg = null;
		final StringBuffer strLocationIds = new StringBuffer();
		session.setAttribute("imageCropPage", "EditBannerAdPage");
		session.setAttribute("minCropWd", 320);
		session.setAttribute("minCropHt", 50);
		try
		{
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			lRetailID = Long.valueOf(loginUser.getRetailerId());
			arRetLocationList = retailerService.getRetailerLocations(lRetailID);
			if (arRetLocationList != null)
			{
				session.setAttribute("retailerLocList", arRetLocationList);
			}
			else
			{
				session.setAttribute("retailerLocList", "");
			}
			arAdsList = (List<RetailerLocationAdvertisement>) retailerService
					.getBannerPageForDisplay(objRetLocAds.getRetailLocationAdvertisementID());
			if (!arAdsList.isEmpty() || arAdsList != null)
			{
				objRetLocationAds = arAdsList.get(0);
				strBanADImg = objRetLocationAds.getStrBannerAdImagePath();
				if (!"".equals(Utility.checkNull(strBanADImg)))
				{
					session.setAttribute("bannerADImage", strBanADImg);
				}

				if (!"".equals(Utility.checkNull(strBanADImg)))
				{
					final int slashIndex = strBanADImg.lastIndexOf("/");
					final int dotIndex = strBanADImg.lastIndexOf('.');

					if (dotIndex == -1)
					{
						objRetLocationAds.setStrBannerAdImagePath(null);
						objRetLocationAds.setTempImageName(null);
					}
					else
					{
						objRetLocationAds.setStrBannerAdImagePath(strBanADImg.substring(slashIndex + 1, strBanADImg.length()));
						objRetLocationAds.setTempImageName(strBanADImg.substring(slashIndex + 1, strBanADImg.length()));
					}
				}
				else
				{
					objRetLocationAds.setTempImageName(null);
					objRetLocationAds.setStrBannerAdImagePath(null);
				}
				strAdsStartDate = objRetLocationAds.getAdvertisementDate();
				if (!"".equals(Utility.checkNull(strAdsStartDate)))
				{
					strAdsStartDate = Utility.formattedDate(strAdsStartDate);
					objRetLocationAds.setAdvertisementDate(strAdsStartDate);
				}
				strAdsEndtDate = objRetLocationAds.getAdvertisementEndDate();
				if (!"".equals(Utility.checkNull(strAdsEndtDate)))
				{
					strAdsEndtDate = Utility.formattedDate(strAdsEndtDate);
					objRetLocationAds.setAdvertisementEndDate(strAdsEndtDate);
				}
				arLocationList = (List<RetailerLocation>) retailerService.getLocationIDForBannerPage(objRetLocAds.getRetailLocationAdvertisementID());
				if (arLocationList != null && !arLocationList.isEmpty())
				{
					for (int i = 0; i < arLocationList.size(); i++)
					{
						strLocationIds.append(arLocationList.get(i).getRetailLocationIds());
						strLocationIds.append(",");
					}
					objRetLocationAds.setRetailLocationIDHidden(strLocationIds.toString().substring(0, strLocationIds.toString().length() - 1));
				}

				model.put("editBannerAdsForm", objRetLocationAds);
			}
			model.put("editBannerAdsForm", objRetLocationAds);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return viewName;
	}

	/**
	 * This controller method will upload banner image .
	 * 
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param result as request parameter.
	 * @param objRetLocAds instance of RetailerLocationAdvertisement.
	 * @param response as request parameter.
	 * @return string given a view name.
	 * @throws Exception will be thrown.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/uploadBannerAdsImg.htm", method = RequestMethod.POST)
	public final String onSubmitImage(@ModelAttribute("buildbanneradform") RetailerLocationAdvertisement objRetLocAds, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, Exception 
	{
		LOG.info("Inside EditManageAdsController : onSubmitImage ");
		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

	//	imageValidSizeValFlg = Utility.validImageDimension(50, 320, objRetLocAds.getBannerAdImagePath().getInputStream());

		
		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, objRetLocAds.getBannerAdImagePath().getInputStream());
		if (imageSizeValFlg)
		{
			//commented code for fixing the crop image issue validation.
			 
			 
			//imageSizeValFlg = Utility.validDimension(1024, 950, objRetLocAds.getBannerAdImagePath().getInputStream());
		//	imageSizeValFlg = true;
			/*if (imageSizeValFlg)
			{
				final BufferedImage img = ImageIO.read(objRetLocAds.getBannerAdImagePath().getInputStream());
				w = img.getWidth(null);
				h = img.getHeight(null);
				session.setAttribute("imageHt", h);
				session.setAttribute("imageWd", w);

				final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
				// session.removeAttribute("welcomePageBtnVal");
				imageSource = objRetLocAds.getBannerAdImagePath().getOriginalFilename();
				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png" + "?" + date.getTime();
				final String filePath = tempImgPath + fileSeparator + objRetLocAds.getBannerAdImagePath().getOriginalFilename();
				Utility.writeFileData(objRetLocAds.getBannerAdImagePath(), filePath);
				if (imageValidSizeValFlg)
				{
					strResponse.append("ValidImageDimention");
					strResponse.append("|" + objRetLocAds.getBannerAdImagePath().getOriginalFilename());
					session.setAttribute("bannerImagePath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
							+ imageSource);

				}
				strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
				// session.setAttribute("bannerImagePath", "/" +
				// ApplicationConstants.IMAGES + "/" +
				// ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				// Used for image cropping popup
				session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			}
			else
			{
				session.setAttribute("bannerImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
				return null;
			}*/
			session.setAttribute("bannerImagePath", ApplicationConstants.UPLOADIMAGEPATH);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
		}
		else
		{
			//session.setAttribute("bannerImagePath", ApplicationConstants.UPLOADIMAGEPATH);
			// Span-Dileep added
			//response.getWriter().write("<imageScr>" + "UploadLogoMinSize" + "</imageScr>");

			//BufferedImage img = ImageIO.read(objRetLocAds.getBannerAdImagePath().getInputStream());
			final BufferedImage img = Utility.getBufferedImageForMinDimension(objRetLocAds.getBannerAdImagePath().getInputStream(), request.getRealPath("images"), 
					objRetLocAds.getBannerAdImagePath().getOriginalFilename(), "Banner");
			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
			// session.removeAttribute("welcomePageBtnVal");
			imageSource = objRetLocAds.getBannerAdImagePath().getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();
			final String filePath = tempImgPath + fileSeparator + objRetLocAds.getBannerAdImagePath().getOriginalFilename();
			//Utility.writeFileData(objRetLocAds.getBannerAdImagePath(), filePath);
			Utility.writeImage(img, filePath);
			if (imageValidSizeValFlg)
			{
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objRetLocAds.getBannerAdImagePath().getOriginalFilename());
				session.setAttribute("bannerImagePath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
						+ imageSource);
			}
			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			// session.setAttribute("bannerImagePath", "/" +
			// ApplicationConstants.IMAGES + "/" +
			// ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			// Used for image cropping popup
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
		}
		return null;
	}

	/**
	 * This controller method will update existing banner page details based return based on input parameter (bannerId) by calling service method.
	 * 
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param result as request parameter.
	 * @param model as request parameter.
	 * @param objRetLocAds instance of RetailerLocationAdvertisement.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/updateBannerAd.htm", method = RequestMethod.POST)
	public final ModelAndView updateBannerPage(@ModelAttribute("editBannerAdsForm") RetailerLocationAdvertisement objRetLocAds, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside EditManageAdsController : updateBannerPage ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		boolean isValidUrl = true;
		String strStatus = null;
		String compDate = null;
		try
		{
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			objRetLocAds.setRetailID(Long.valueOf(loginUser.getRetailerId()));
			objRetLocAds.setRetailLocationIDHidden(objRetLocAds.getRetailLocationIds());
			if (!"".equals(Utility.checkNull(objRetLocAds.getRibbonXAdURL())))
			{
				isValidUrl = Utility.validateURL(objRetLocAds.getRibbonXAdURL());
			}
			if (!isValidUrl)
			{
				buildBannerAdValidator.editValidate(objRetLocAds, result, ApplicationConstants.WEBURL);
			}
			buildBannerAdValidator.editValidate(objRetLocAds, result);
			// For Indefinite Duration of Ads				
			//Utility.setAdStartAndEndDate(objRetLocAds);
			
			if (!"".equals(Utility.checkNull(objRetLocAds.getAdvertisementDate()))
					&& !"".equals(Utility.checkNull(objRetLocAds.getAdvertisementEndDate())))
			{
				compDate = Utility.compareDate(objRetLocAds.getAdvertisementDate(), objRetLocAds.getAdvertisementEndDate());
				if (null != compDate)
				{
					buildBannerAdValidator.editValidate(objRetLocAds, result, ApplicationConstants.DATEAFTER);
				}
			}
			if ("".equals(Utility.checkNull(objRetLocAds.getStrBannerAdImagePath())))
			{
				result.rejectValue("bannerAdImagePath", "Please Upload Banner Image", "Please Upload Banner Image");
			}
			if (result.hasErrors())
			{
				return new ModelAndView(viewName);
			}
			else
			{
				strStatus = retailerService.updateBuildBannerInfo(objRetLocAds);
				if (strStatus.equals(ApplicationConstants.SUCCESS))
				{
					if (!"".equals(Utility.checkNull(objRetLocAds.getInvalidLocationIds())))
					{
						objRetLocAds.setRetailLocationIDHidden(null);
						objRetLocAds.setRetailLocationIDHidden(objRetLocAds.getInvalidLocationIds());
						request.setAttribute("message", "Banner Page is already available for the highlighted Location Multi select box.Please verify it and re-submit it.");
						return new ModelAndView(viewName);
					}
					else
					{
						return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/managebannerads.htm"));
					}
				}
				else
				{
					return new ModelAndView(viewName);
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
	}
}

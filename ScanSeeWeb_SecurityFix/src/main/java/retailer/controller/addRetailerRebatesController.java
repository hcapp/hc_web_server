package retailer.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.RebatesValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Rebates;
import common.pojo.RetailerLocation;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

/**
 * AddRebatesController is a controller class for add rebates screen.
 * 
 * @author nanda_b
 */
@Controller
public class addRetailerRebatesController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(addRetailerRebatesController.class);

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "addRetRebates";

	/**
	 * Variable VIEW_NAME declared as String.
	 */
	String view = VIEW_NAME;

	/**
	 * Variable rebatesValidator declared as instance of ReabtesValidator.
	 */
	RebatesValidator rebatesValidator;

	/**
	 * To set rebatesValidator.
	 * 
	 * @param rebatesValidator
	 *            To set
	 */
	@Autowired
	public void setRebatesValidator(RebatesValidator rebatesValidator)
	{
		this.rebatesValidator = rebatesValidator;
	}

	/**
	 * The getRetailerLocations method to show addRebates view.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return VIEW_NAME addRebates view.
	 */

	@RequestMapping(value = "/addretrebates.htm", method = RequestMethod.GET)
	public String getRetailerLocations(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		LOG.info("Inside addRetailerRebatesController : getRetailerLocations ");
		try
		{
			request.getSession().removeAttribute("message");
			request.getSession().removeAttribute("pdtInfoList");
			Rebates rebateVo = new Rebates();
			model.put("addretrebatesForm", rebateVo);
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			List<RetailerLocation> retailerLocations = null;
			ArrayList<TimeZones> timeZonelst = null;
			Long retailID = null;
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			retailID = Long.valueOf(loginUser.getRetailerId());

			retailerLocations = retailerService.getRebRetailerLocations(retailID);
			timeZonelst = retailerService.getAllTimeZones();
			session.setAttribute("rebateTimeZoneslst", timeZonelst);

			if (retailerLocations != null)
			{
				session.setAttribute("retailerLocList", retailerLocations);
			}
			else
			{
				session.setAttribute("retailerLocList", "");
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in getRetailerLocations:::::"+ e.getMessage());
            throw new ScanSeeServiceException(e);
		}

		LOG.info("getRetailerLocations::  Exit Get Method");
		return VIEW_NAME;
	}

	/**
	 * The addRebates controller method is to call service and dao methods.
	 * 
	 * @param rebatesVo
	 *            Rebates instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return VIEW_NAME addRebates view.
	 */
	@RequestMapping(value = "/addretrebates.htm", method = RequestMethod.POST)
	public ModelAndView addRebates(@ModelAttribute("addretrebatesForm") Rebates rebatesVo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside addRetailerRebatesController : addRebates ");
		String compDate = null;
		Date currentDate = new Date();
		try
		{
			String isDataInserted = null;
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

			Users user = (Users) request.getSession().getAttribute("loginuser");
			rebatesVo.setUserID(user.getUserID());
			rebatesVo.setRetailerID(user.getRetailerId());

			String sTime = String.valueOf(rebatesVo.getRebStartHrs()) + ":" + String.valueOf(rebatesVo.getRebStartMins());
			String endTime = String.valueOf(rebatesVo.getRebEndhrs()) + ":" + String.valueOf(rebatesVo.getRebEndMins());
			rebatesVo.setRebStartTime(sTime);
			rebatesVo.setRebEndTime(endTime);
			
			rebatesValidator.validate(rebatesVo, result);
			if (!result.hasErrors())
			{
				compDate = Utility.compareCurrentDate(rebatesVo.getRebStartDate(), currentDate);
				if (null != compDate)
				{

					rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATESTARTCURRENT);
				}

				compDate = Utility.compareCurrentDate(rebatesVo.getRebEndDate(), currentDate);
				if (null != compDate)
				{

					rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEENDCURRENT);
				}
				else
				{

					compDate = Utility.compareDate(rebatesVo.getRebStartDate(), rebatesVo.getRebEndDate());

					if (null != compDate)
					{

						rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEAFTER);
					}
				}
			}
			if (result.hasErrors())
			{
				return new ModelAndView("addRetRebates");
			}
			else
			{
				isDataInserted = retailerService.addRetailerRebates(rebatesVo);

				if (isDataInserted == ApplicationConstants.FAILURE || isDataInserted == null)
				{

					view = VIEW_NAME;
					result.reject("addRebate.error");

				}
				else
				{
					view = "rebatesretailMfg.htm";
					
				}
			}
		}
		
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside addRetailerRebatesController : addRebates " + e.getMessage());
			result.reject("addRebate.error");
			return new ModelAndView(view);
		}
		catch (NullPointerException e)
		{
			LOG.error("Inside addRetailerRebatesController : addRebates " + e.getMessage());
			result.reject("addRebate.error");
			return new ModelAndView(view);
		}
		if (view.equals(VIEW_NAME))
		{
			return new ModelAndView(view);
		}
		else
		{
			return new ModelAndView(new RedirectView(view));
		}
	}
	/**
	 * this method returns map of hours.
	 * @return sortedMap.
	 * @throws ScanSeeServiceException.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("RebateStartHrs")
	public Map<String, String> populateDealStartHrs() throws ScanSeeServiceException
	{
		LOG.info(ApplicationConstants.METHODSTART + "populateDealStartHrs");
		HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();

		for (int i = 0; i < 24; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put("0" + i, "0" + i);
			}
			else
			{
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}

		}
		Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		LOG.info(ApplicationConstants.METHODEND + "populateDealStartHrs");
		return sortedMap;
	}

	/**
	 * this method sort the entry set.
	 * @param unsortMap 
	 *                Map instance as request parameter.
	 * @return sortedMap.
	 * @throws ScanSeeServiceException.
	 */
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException
	{
		LOG.info(ApplicationConstants.METHODSTART + "sortByComparator");
		List list = new LinkedList(unsortMap.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2)
			{
				return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		// put sorted list into map again
		Map sortedMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();)
		{
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		LOG.info(ApplicationConstants.METHODEND + "populateDealStartHrs");
		return sortedMap;
	}

	
	/**
	 * this method returns map of minutes.
	 * @return  sortedMap.
	 * @throws ScanSeeServiceException.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("RebateStartMins")
	public Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException
	{
		LOG.info(ApplicationConstants.METHODSTART + "populatemapDealStartMins");
		HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();

		for (int i = 0; i <= 55; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put("0" + i, "0" + i);
				i = i + 4;
			}
			else
			{
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}

		}
		@SuppressWarnings("unused")
		Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		LOG.info(ApplicationConstants.METHODEND + "populatemapDealStartMins");
		return sortedMap;

	}

	/**
	 * this method gives the view page of rebates.
	
	 * @param rebatesVo
	 *            Rebates instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return VIEW_NAME addRebates view.
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/previewretailerrebate.htm", method = RequestMethod.POST)
	public ModelAndView previewPage(@ModelAttribute("previewretailerrebform")Rebates rebatesVo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
    
		LOG.info("previewPage in addRetailerRebatesContrller in Retailer:: Inside POST Method");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
        
		request.getSession().removeAttribute("rebateprodlist");
		List<Rebates> rebateprodlist = null;
		String rebName = request.getParameter("rebName");
		String rebLongDescription = request.getParameter("rebLongDescription");
		Long retailID = null;
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());

		request.getSession().setAttribute("rebateName", rebName);
		request.getSession().setAttribute("rebateLongDescription", rebLongDescription);
	    
		rebatesVo.setRebName(rebName);
		rebatesVo.setRebLongDescription(rebLongDescription);
	
	
		
		String prdname = rebatesVo.getProductName();
	    String str[] = prdname.split(",");
	    String productName=str[0].trim();
	    rebatesVo.setProductName(productName);

       
		try {
			if(!productName.equalsIgnoreCase(null) && !productName.equalsIgnoreCase("")){
			rebateprodlist = retailerService.getRebateProductImage(productName,retailID);
		
			if(rebateprodlist!=null && !rebateprodlist.isEmpty()){
				session.setAttribute("rebateprodlist", rebateprodlist.get(0));
			}
		  }
		}
		catch(ScanSeeServiceException e){
			LOG.error("Exception occurred in previewPage:::::"+ e.getMessage());
            throw new ScanSeeServiceException(e);
		}
		
		model.put("previewretailerrebform", rebatesVo);
	    
		LOG.info("previewPage in addRetailerRebatesContrller :: Exiting POST Method");
		return new ModelAndView("addretailerrebpreview");

	}

}

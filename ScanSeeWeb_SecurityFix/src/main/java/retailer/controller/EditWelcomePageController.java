/**
 * 
 */
package retailer.controller;

import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.CreateBannerAdValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.Users;
import common.util.Utility;

/**
 * EditWelcomePageController is a controller class for edit welcome page screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class EditWelcomePageController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(EditWelcomePageController.class);

	/**
	 * Variable objEditAdValidator declared as instance of
	 * CreateBannerAdValidator.
	 */
	CreateBannerAdValidator createBannerAdValidator;

	/**
	 * Getting the CreateBannerAdValidator Instance.
	 * 
	 * @param objEditAdValidator
	 *            is instance of CreateBannerAdValidator. objEditAdValidator the
	 *            CreateBannerAdValidator to set.
	 */
	@Autowired
	public void setCreateBannerAdValidator(CreateBannerAdValidator createBannerAdValidator)
	{
		this.createBannerAdValidator = createBannerAdValidator;
	}

	/**
	 * Variable VIEW_NAME declared as String.
	 */
	public final String VIEW_NAME = "editWelcomePage";

	/**
	 * This controller method will return the retail location advertisement
	 * details.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/editWelcomePage.htm", method = RequestMethod.POST)
	public String editManageAdsInfo(@ModelAttribute("editManageAdsForm") RetailerLocationAdvertisement objRetLocAds, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	/*
	 * public String editManageAdsInfo(HttpServletRequest request, HttpSession
	 * session, ModelMap model) throws ScanSeeServiceException
	 */
	{
		log.info("Inside EditWelcomePageController : editManageAdsInfo ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		List<RetailerLocationAdvertisement> arAdsList = null;
		List<RetailerLocation> arLocationList = null;
		String strAdsStartDate = null;
		String strAdsEndtDate = null;
		ArrayList<RetailerLocation> arRetLocationList = null;
		Long lRetailID = null;
		RetailerLocationAdvertisement objRetLocationAds = null;
		Long lRetLocAdsID = new Long(0);
		String strBanADImg = null;
		StringBuffer strLocationIds = new StringBuffer();
		session.removeAttribute("welcomePageCreateImgPath");
		session.setAttribute("imageCropPage", "EditWelcomePage");
		session.setAttribute("minCropWd", 320);
		session.setAttribute("minCropHt", 460);
		try
		{
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			lRetailID = Long.valueOf(loginUser.getRetailerId());
			arRetLocationList = retailerService.getRetailerLocations(lRetailID);
			lRetLocAdsID = Long.valueOf(request.getParameter("retailLocationAdvertisementID"));
			if (arRetLocationList != null)
			{
				session.setAttribute("retailerLocList", arRetLocationList);
			}
			else
			{
				session.setAttribute("retailerLocList", "");
			}
			arAdsList = (List<RetailerLocationAdvertisement>) retailerService.getWelcomePageForDisplay(objRetLocAds
					.getRetailLocationAdvertisementID());
			if (!arAdsList.isEmpty() || arAdsList != null)
			{
				objRetLocationAds = arAdsList.get(0);
				strBanADImg = objRetLocationAds.getStrBannerAdImagePath();

				if ("".equals(Utility.checkNull(strBanADImg)))
				{
					session.setAttribute("welcomePageCreateImgPath", ApplicationConstants.UPLOADIMAGEPATH);
				}
				else
				{
					session.setAttribute("welcomePageCreateImgPath", strBanADImg);
				}

				if (!"".equals(Utility.checkNull(strBanADImg)))
				{
					int slashIndex = strBanADImg.lastIndexOf("/");
					int dotIndex = strBanADImg.lastIndexOf('.');

					if (dotIndex == -1)
					{
						objRetLocationAds.setStrBannerAdImagePath(null);
						objRetLocationAds.setTempImageName(null);
					}
					else
					{
						objRetLocationAds.setStrBannerAdImagePath(strBanADImg.substring(slashIndex + 1, strBanADImg.length()));
						objRetLocationAds.setTempImageName(strBanADImg.substring(slashIndex + 1, strBanADImg.length()));
					}
				}
				else
				{

					objRetLocationAds.setStrBannerAdImagePath(null);
					objRetLocationAds.setTempImageName(null);
				}

				strAdsStartDate = objRetLocationAds.getAdvertisementDate();
				if (!"".equals(Utility.checkNull(strAdsStartDate)))
				{
					strAdsStartDate = Utility.formattedDate(strAdsStartDate);
					objRetLocationAds.setAdvertisementDate(strAdsStartDate);
				}
				strAdsEndtDate = objRetLocationAds.getAdvertisementEndDate();
				if (!"".equals(Utility.checkNull(strAdsEndtDate)))
				{
					strAdsEndtDate = Utility.formattedDate(strAdsEndtDate);
					objRetLocationAds.setAdvertisementEndDate(strAdsEndtDate);
				}
				arLocationList = (List<RetailerLocation>) retailerService
						.getLocationIDForWelcomePage(objRetLocAds.getRetailLocationAdvertisementID());
				if (arLocationList != null && !arLocationList.isEmpty())
				{
					for (int i = 0; i < arLocationList.size(); i++)
					{
						strLocationIds.append(arLocationList.get(i).getRetailLocationIds());
						strLocationIds.append(",");
					}
					objRetLocationAds.setRetailLocationIDHidden(strLocationIds.toString().substring(0, strLocationIds.toString().length() - 1));
				}

				model.put("editManageAdsForm", objRetLocationAds);
			}
			model.put("editManageAdsForm", objRetLocationAds);
		}
		catch (ScanSeeServiceException e)
		{
			log.error("Inside EditWelcomePageController : editManageAdsInfo : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		catch (ParseException e)
		{
			log.error("Inside EditWelcomePageController : editManageAdsInfo : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return VIEW_NAME;
	}

	/**
	 * This controller method will upload banner and ribbon image .
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadAdsImg.htm", method = RequestMethod.POST)
	public String onSubmitImage(@ModelAttribute("createCustomPage") RetailerLocationAdvertisement objRetLocAds, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception, ScanSeeServiceException
	{
		log.info("Inside EditManageAdsController : onSubmitImage ");
		String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		StringBuffer strResponse = new StringBuffer();
		Date date = new Date();
		session.removeAttribute("cropImageSource");
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

	//	imageValidSizeValFlg = Utility.validImageDimension(460, 320, objRetLocAds.getBannerAdImagePath().getInputStream());

		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, objRetLocAds.getBannerAdImagePath().getInputStream());
		if (imageSizeValFlg)
		{
			
			session.setAttribute("welcomePageCreateImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
			
			//commented code for fixing the crop image issue validation(changes.ppt).
			
			//imageSizeValFlg = Utility.validDimension(1024, 950, objRetLocAds.getBannerAdImagePath().getInputStream());
			//imageSizeValFlg = true;
			/*if (imageSizeValFlg)
			{
				BufferedImage img = ImageIO.read(objRetLocAds.getBannerAdImagePath().getInputStream());
				w = img.getWidth(null);
				h = img.getHeight(null);
				session.setAttribute("imageHt", h);
				session.setAttribute("imageWd", w);

				String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
				// session.removeAttribute("welcomePageBtnVal");
				imageSource = objRetLocAds.getBannerAdImagePath().getOriginalFilename();
				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png" + "?" + date.getTime();
				String filePath = tempImgPath + fileSeparator + objRetLocAds.getBannerAdImagePath().getOriginalFilename();
				Utility.writeFileData(objRetLocAds.getBannerAdImagePath(), filePath);
				if (imageValidSizeValFlg)
				{
					strResponse.append("ValidImageDimention");
					strResponse.append("|" + objRetLocAds.getBannerAdImagePath().getOriginalFilename());

					session.setAttribute("welcomePageCreateImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
							+ imageSource);

				}
				strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
				
				 * session.setAttribute("welcomePageCreateImgPath", "/" +
				 * ApplicationConstants.IMAGES + "/" +
				 * ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				 
				// Used for image cropping popup
				session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			}
			else
			{

				session.setAttribute("welcomePageCreateImgPath", ApplicationConstants.UPLOADIMAGEPATH);
				response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
				return null;

			}
*/
		}
		else
		{

			//session.setAttribute("welcomePageCreateImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			// Span-Dileep added
			//response.getWriter().write("<imageScr>" + "UploadLogoMinSize" + "</imageScr>");
			
		//	BufferedImage img = ImageIO.read(objRetLocAds.getBannerAdImagePath().getInputStream());
			BufferedImage img = Utility.getBufferedImageForMinDimension(objRetLocAds.getBannerAdImagePath().getInputStream(),request.getRealPath("images"),
					objRetLocAds.getBannerAdImagePath().getOriginalFilename(),"WelcomePage");
			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
			// session.removeAttribute("welcomePageBtnVal");
			imageSource = objRetLocAds.getBannerAdImagePath().getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();
			String filePath = tempImgPath + fileSeparator + objRetLocAds.getBannerAdImagePath().getOriginalFilename();
			//Utility.writeFileData(objRetLocAds.getBannerAdImagePath(), filePath);
			Utility.writeImage(img, filePath);
			if (imageValidSizeValFlg)
			{
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objRetLocAds.getBannerAdImagePath().getOriginalFilename());

				session.setAttribute("welcomePageCreateImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
						+ imageSource);

			}
			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			/*
			 * session.setAttribute("welcomePageCreateImgPath", "/" +
			 * ApplicationConstants.IMAGES + "/" +
			 * ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			 */
			// Used for image cropping popup
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			
			
			

		}

		return null;
	}

	/**
	 * This controller method will return the retail location advertisement
	 * details.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/updateWelcomePage.htm", method = RequestMethod.POST)
	public ModelAndView updateWelcomePage(@ModelAttribute("editManageAdsForm") RetailerLocationAdvertisement objRetLocAds, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		log.info("Inside EditManageAdsController : updateWelcomePage ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		String strStatus = null;
		Long lRetailID = new Long(0);
		String compDate = null;
		final Date currentDate = new Date();
		// final MultipartFile fileBannerAd =
		// objRetLocAds.getBannerAdImagePath();
		try
		{
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			objRetLocAds.setRetailID(Long.valueOf(loginUser.getRetailerId()));
			objRetLocAds.setRetailLocationIDHidden(objRetLocAds.getRetailLocationIds());
			createBannerAdValidator.editValidate(objRetLocAds, result);
			// For Indefinite Duration of Ads				
			//Utility.setAdStartAndEndDate(objRetLocAds);
			
			if (!"".equals(Utility.checkNull(objRetLocAds.getAdvertisementDate()))
					&& !"".equals(Utility.checkNull(objRetLocAds.getAdvertisementEndDate())))
			{
				compDate = Utility.compareDate(objRetLocAds.getAdvertisementDate(), objRetLocAds.getAdvertisementEndDate());
				if (null != compDate)
				{
					createBannerAdValidator.editValidate(objRetLocAds, result, ApplicationConstants.DATEAFTER);
				}
			}

			if ("".equals(Utility.checkNull(objRetLocAds.getStrBannerAdImagePath())))
			{
				result.rejectValue("bannerAdImagePath", "Please Upload Welcome Page Image", "Please Upload Welcome Page Image");
			}
			
			if (result.hasErrors())
			{

				return new ModelAndView(VIEW_NAME);
			}
			else
			{
				strStatus = retailerService.updateWelcomePageInfo(objRetLocAds);
				if (strStatus.equals(ApplicationConstants.SUCCESS))
				{
					if (!"".equals(Utility.checkNull(objRetLocAds.getInvalidLocationIds())))
					{
						objRetLocAds.setRetailLocationIDHidden(null);
						objRetLocAds.setRetailLocationIDHidden(objRetLocAds.getInvalidLocationIds());
						request.setAttribute("message", "There is already welcome page in given date range for selected location.");
						return new ModelAndView(VIEW_NAME);
					}
					else
					{
						return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/manageads.htm"));
					}
				}
				else
				{
					return new ModelAndView(VIEW_NAME);
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			log.error("Inside EditManageAdsController : updateWelcomePage : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
	}
}

package retailer.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerSearchService;
import supplier.controller.LoginController;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.Offer;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;
import common.exception.ScanSeeServiceException;
import common.pojo.LoginVO;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductReview;

@Controller
@RequestMapping("/productInfo.htm")
public class RetailerProductInfoController {
	private static final Logger LOG = LoggerFactory.getLogger(RetailerProductInfoController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String showPage(HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1) throws ScanSeeServiceException {

		LoginVO loginVO = new LoginVO();
		model.put("loginform", loginVO);
		return "retailerproductInfo";
	}
	
	@RequestMapping(value = "/productInfo.htm", method = RequestMethod.POST)
	public ModelAndView productInfo(@ModelAttribute("retailerproductinfoform")ProductVO productinfo,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws IOException,ScanSeeServiceException {
		
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		RetailerSearchService retailerSearchService = (RetailerSearchService) appContext
				.getBean("retailerSearchService");
		
		ProductVO productInfoDetail = null;
		Integer productID = productinfo.getProductID();
		
		Integer retailLocationID = productinfo.getRetailLocationID();
		
		LOG.info("productID*************"+productID);
		LOG.info("retailLocationID++++++++++++++++++"+retailLocationID);
		SearchForm objForm = null;
		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		ExternalAPIInformation apiInfo = null;
		ProductDetail productDet = null;
		OnlineStoresRequest onlineStores = null;
		OnlineStoresService onlineStoresService = null;
		OnlineStores onlineStoresinfo = null;
		List<Offer> onlineStoresList = null;
		String review = null;
		ArrayList<ProductReview> productReviewslist = null;
		FindNearByDetails findNearByDetails = null;
		
		
		
		try {
		    	
			 productInfoDetail = retailerSearchService.getAllProductInfo(retailLocationID, productID);
             productReviewslist = retailerSearchService.getProductReviews(productID);
			
			Users loginUser = (Users) session.getAttribute("loginuser");
			objForm = (SearchForm) session.getAttribute("searchForm");
			objAreaInfoVO.setProductId(productID);
			if(loginUser != null){
		   		
				objAreaInfoVO.setZipCode(loginUser.getPostalCode());
				objAreaInfoVO.setUserID(loginUser.getUserID());
			}else{
			
			objAreaInfoVO.setZipCode(objForm.getZipCode());
			}
			
			//find near by stores
			findNearByDetails = retailerSearchService.findNearBy(objAreaInfoVO);
			
			if (findNearByDetails != null)
			{
				request.setAttribute("findNearByDetails", findNearByDetails.getFindNearByDetail().get(0));
			}
			// online stores
		  if(loginUser != null){
			apiInfo = retailerSearchService.externalApiInfo("FindOnlineStores");
            productDet = retailerSearchService.fetchProductDetails(productID.toString());
			onlineStoresService = new OnlineStoresServiceImpl();
			onlineStores = new OnlineStoresRequest();
			onlineStores.setPageStart("0");
			onlineStores.setUpcCode(productDet.getScanCode());
		    onlineStores.setUserId(loginUser.getUserID().toString());
			onlineStores.setValues();
			onlineStoresinfo = onlineStoresService.onlineStoresInfoEmailTemplate(apiInfo, onlineStores);
			
			if (onlineStoresinfo != null)
			{
				if (!onlineStoresinfo.getOffers().isEmpty())
				{
					onlineStoresList = onlineStoresinfo.getOffers();
					
					request.setAttribute("OnlineStoresInfo", onlineStoresList.get(0));

				
				}

			  }
		   }
			
			
				
			
		} catch (ScanSeeServiceException e) {
			
			LOG.error("Exception occurred in productInfo::::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}
		
		if(productReviewslist !=null && !productReviewslist.isEmpty()){
	    		
			
			ProductReview productReview =  productReviewslist.get(0);
			
			
			request.setAttribute("productReviewslist",productReview);
			
     	}
		session.setAttribute("retailerproductinfo", productInfoDetail);
		request.setAttribute("searchForm", objForm);
		return new ModelAndView("retaielrProductdetails");
	}
		
}

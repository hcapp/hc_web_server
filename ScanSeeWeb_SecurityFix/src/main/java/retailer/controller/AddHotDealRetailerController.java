package retailer.controller;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.HotDealRetailerDetailsValidator;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.HotDealInfo;
import common.pojo.Product;
import common.pojo.Retailer;
import common.pojo.SearchResultInfo;
import common.pojo.State;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * AddHotDealRetailerController is a controller class for adding retailer product hot deals
 * screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class AddHotDealRetailerController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AddHotDealRetailerController.class);

	/**
	 * Variable retailerDealsValidator declared as instance of
	 * HotDealRetailerDetailsValidator.
	 */
	private HotDealRetailerDetailsValidator retailerDealsValidator;

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private final String viewName = "addRetailerHotDeals";
	
	/**
	 * Variable view declared as String.
	 */
	private String view = viewName;
	
	/**
	 * Variable hotDealLocation declared as constant string.
	 */
	private final String hotDealLocation = "hotdeallocation";
	
	/**
	 * Variable hotDealCity declared as constant string.
	 */
	private final String hotDealCity = "hotdealcity";
	/**
	 * To hotdealRetailerValidator to set.
	 *    
	 * @param retailerDealsValidator to set.
	 */
	@Autowired
	public final void setHotdealRetailerDetailsValidator(HotDealRetailerDetailsValidator retailerDealsValidator)
	{
		this.retailerDealsValidator = retailerDealsValidator;
	}
	
	/**
	 * To view to set.
	 * @param view to set.
	 */
	public final void setView(String view)
	{
		this.view = view;
	}

	/**
	 * This controller method will display the new HotDeals screen.
	 * 
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param response as request parameter.
	 * @param model as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/hotDealRetailorShowPage.htm", method = RequestMethod.GET)
	public final ModelAndView showPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealRetailerController : showPage ");
		ArrayList<State> states = null;
		ArrayList<TimeZones> timeZonelst = null;
		ArrayList<Category> arBCategoryList = null;
		SearchResultInfo objHotdealList = null;
		try
		{
			request.getSession().removeAttribute(hotDealLocation);
			request.getSession().removeAttribute(hotDealCity);
			request.getSession().removeAttribute("hotDealProductImage");
			session.removeAttribute("pdtInfoList");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			request.getSession().removeAttribute("message");
			states = retailerService.getAllStates();
			arBCategoryList = retailerService.getAllBusinessCategory();
			session.setAttribute("statesListCreat", states);
			session.setAttribute("categoryList", arBCategoryList);
			timeZonelst = retailerService.getAllTimeZones();
			session.setAttribute("retailerTimeZoneslst", timeZonelst);
			session.setAttribute("imageCropPage", "AddHotDealRetailer");
			session.setAttribute("minCropWd", 70);
			session.setAttribute("minCropHt", 70);
			session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOAD_IMAGE_PATH);
			final HotDealInfo hotdealInfo = new HotDealInfo();
			hotdealInfo.setRetailID(user.getRetailerId());
			objHotdealList  =  (SearchResultInfo) session.getAttribute("listOfDeals");
			if (null == objHotdealList) {
				hotdealInfo.setBackButton("no");
			}  
			model.put("addhotdealretailerform", hotdealInfo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(viewName);
	}


	/**
	 * This controller method will add one new HotDeals details screen by call service and DAO methods.
	 * 
	 * @param objHotDealsInfo HotDealInfo instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/hotDealRetailoradd.htm", method = RequestMethod.POST)
	public final ModelAndView addHotDeals(@ModelAttribute("addhotdealretailerform") HotDealInfo objHotDealsInfo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealRetailerController : addHotDeals ");
		String compDate = null;
		final Date currentDate = new Date();
		String strCategory = null;
		String strLocRCity = null;
		String strpCity = null;
		String strProductImg = null;
		String strRetailerLogo = null;
		boolean bPercentageFlag =  false;
		SearchResultInfo objHotdealList = null;
		final MultipartFile fileHotDealImage = objHotDealsInfo.getImageFile();
		int strRetLoc = 0;
		try
		{
			request.getSession().removeAttribute("pdtInfoList");
			request.removeAttribute("productId");
			String isDataInserted = null;
			String strExpireTime = null;
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
			final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			
			final StringBuffer allCities = new StringBuffer();
			strLocRCity = request.getParameter("slctOpt");
			objHotDealsInfo.setCityHiddenChecked(Utility.checkNull(strLocRCity));
			String sTime = String.valueOf(objHotDealsInfo.getDealStartHrs()) + ":" + String.valueOf(objHotDealsInfo.getDealStartMins());
			String endTime = String.valueOf(objHotDealsInfo.getDealEndhrs()) + ":" + String.valueOf(objHotDealsInfo.getDealEndMins());
			strExpireTime = String.valueOf(objHotDealsInfo.getExpireHrs()) + ":" + String.valueOf(objHotDealsInfo.getExpireMins());
			strCategory = objHotDealsInfo.getbCategory();
			objHotDealsInfo.setbCategoryHidden(objHotDealsInfo.getbCategory());
			objHotDealsInfo.setCityHidden(objHotDealsInfo.getCity());
			objHotDealsInfo.setRetailID(user.getRetailerId());
			
			strpCity = objHotDealsInfo.getCity();
			if (",".equals(objHotDealsInfo.getProductImage())) {
				objHotDealsInfo.setProductImage(null);
			}
			strProductImg = objHotDealsInfo.getProductImage();
			objHotdealList  =  (SearchResultInfo) session.getAttribute("listOfDeals");
			if (null == objHotdealList) {
				objHotDealsInfo.setBackButton("no");
			}
			if (null == objHotDealsInfo.getRetailerLocID()) {
				strRetLoc = 0;
			} else {
				strRetLoc = objHotDealsInfo.getRetailerLocID();
			}
			objHotDealsInfo.setLocationHidden(String.valueOf(objHotDealsInfo.getRetailerLocID()));
			//objHotDealsInfo.setProductNameHidden(objHotDealsInfo.getProductNameHidden());
			
			/*Third priority is for the image uploaded by the retailer logo.*/
				strRetailerLogo = (String) session.getAttribute("logosrc");
				if (!"".equals(Utility.checkNull(strRetailerLogo))) {
						if (!ApplicationConstants.UPLOADIMAGEPATH.equals(strRetailerLogo)) {
							user.setRetailerLogoImage(strRetailerLogo);
						}
				}
		
			/* First priority is for the image uploaded by the user. */
			if (!"".equals(Utility.checkNull(objHotDealsInfo.getDealImgPath()))
					&& !ApplicationConstants.BLANKIMAGE.equals(objHotDealsInfo.getDealImgPath())) {
				objHotDealsInfo.setProductImage(objHotDealsInfo.getDealImgPath());
			/* Second priority is for the image  from Products Associated. */
			} else if (!"".equals(Utility.checkNull(objHotDealsInfo.getProductImage()))) {
				objHotDealsInfo.setProductImage(null);
				/*Third priority is for the image uploaded by the retailer logo user.*/
			} else if (!"".equals(Utility.checkNull(user.getRetailerLogoImage()))) {
				objHotDealsInfo.setProductImage(user.getRetailerLogoImage().substring(user.getRetailerLogoImage().lastIndexOf("/") + 1));
			} else {
				objHotDealsInfo.setDealImgPath(null);
				objHotDealsInfo.setProductImage(null);
				result.rejectValue("imageFile", "Please Upload Hot Deal Image since there is no image for Product or Retailer logo", "Please Upload Hot Deal Image since there is no image for Product or Retailer logo");
			}
			
			/*if (sTime != null && !"".equals(sTime)) {
				sTime = sTime.replace(',', ':');
			} else {
				sTime = "00:00";
			}*/
			objHotDealsInfo.setDealStartTime(sTime);
			/*if (endTime != null && !"".equals(endTime)) {
				endTime = endTime.replace(',', ':');
			} else {
				endTime = "00:00";
			}*/
			objHotDealsInfo.setDealEndTime(endTime);
			/*if (strExpireTime != null && !"".equals(strExpireTime)) {
				strExpireTime = strExpireTime.replace(',', ':');
			} else {
				strExpireTime = "00:00";
			}*/
			objHotDealsInfo.setExpireTime(strExpireTime);
			if (strCategory.equalsIgnoreCase(ApplicationConstants.ZERO))
			{
				retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.BUSINESSCATEGORY);
			}
			if ("City".equals(objHotDealsInfo.getDealForCityLoc()))
			{
				if ("".equals(Utility.checkNull(strpCity)))
				{
					retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.POPULATIONCENTER);
				}
			}

			if ("Location".equals(objHotDealsInfo.getDealForCityLoc())) {
				if (strRetLoc == 0) {
					retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.DEALRETAILERLOC);
				}
			}

			if (objHotDealsInfo != null)
			{
				retailerDealsValidator.validate(objHotDealsInfo, result);
				if (!result.hasErrors()) {
					compDate = Utility.compareCurrentDate(objHotDealsInfo.getDealStartDate(), currentDate);
					if (null != compDate) {
						retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.DATESTARTCURRENT);
					}
					
					if (!"".equals(Utility.checkNull(objHotDealsInfo.getDealEndDate()))) {
						compDate = Utility.compareCurrentDate(objHotDealsInfo.getDealEndDate(), currentDate);
						if (null != compDate) {
							retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.DATEENDCURRENT);
						} else {
							compDate = Utility.compareDate(objHotDealsInfo.getDealStartDate(), objHotDealsInfo.getDealEndDate());
							if (null != compDate) {
								retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.DATEAFTER);
							}
						}
					}
					
					compDate = Utility.compareCurrentDate(objHotDealsInfo.getExpireDate(), currentDate);
					if (null != compDate) {
						retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.EXPIREDDATE);
					}  else if (!"".equals(Utility.checkNull(objHotDealsInfo.getDealEndDate())) 
							&& !"".equals(Utility.checkNull(objHotDealsInfo.getExpireDate()))) {
						compDate = Utility.compareDate(objHotDealsInfo.getDealEndDate(), objHotDealsInfo.getExpireDate());
						if (null != compDate) {
							retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.EXPIREDDATEBEFORE);
						}
					} else if (!"".equals(Utility.checkNull(objHotDealsInfo.getDealStartDate())) 
							&& !"".equals(Utility.checkNull(objHotDealsInfo.getExpireDate()))) {
						compDate = Utility.compareDate(objHotDealsInfo.getDealStartDate(), objHotDealsInfo.getExpireDate());
						if (null != compDate) {
							retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.EXPIREDDATE_GRTN_STARTDATE);
						}
					}
				}
			}
			if (result.hasErrors()) {
				view = viewName;
			} else {
				if (null != objHotDealsInfo.getCity()) {
					if ("All".equals(objHotDealsInfo.getCity())) {
						final ArrayList<City> cities = (ArrayList<City>) request.getSession().getAttribute("hotdealpopcenters");
						for (int i = 0; i < cities.size(); i++) {
							allCities.append(cities.get(i).getPopulationCenterID());
							allCities.append(",");
						}
						objHotDealsInfo.setCity(allCities.toString().substring(0, allCities.toString().length() - 1));
					}
				}
				objHotDealsInfo.setRetailID(user.getRetailerId());
				isDataInserted = retailerService.retailerAddHotDeal(objHotDealsInfo);
				final String[] strArray = isDataInserted.split(":");
			    bPercentageFlag =  Boolean.parseBoolean(strArray[1]);
			     
				if (isDataInserted == null 
						|| isDataInserted == ApplicationConstants.FAILURE) {
					//request.setAttribute(ApplicationConstants.MESSAGE, "Error While Adding HotDeal");
					view = viewName;
				} else {
					view = "/ScanSeeWeb/retailer/hotDealRetailer.htm";
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		/*PercentageFlag returns true, then the discount less than 50%. */
		if (bPercentageFlag) {
	    	 view = viewName;
	    	 request.setAttribute(ApplicationConstants.MESSAGE, ApplicationConstants.PERCENTAGEDISCOUNTFLAG);
	     } 
		
		if (view.equals(viewName))
		{
			
			if ("City".equals(objHotDealsInfo.getDealForCityLoc())) {
				request.getSession().setAttribute(hotDealCity, objHotDealsInfo.getCity());
				session.removeAttribute(hotDealLocation);
			} else if ("Location".equals(objHotDealsInfo.getDealForCityLoc())) {
				session.removeAttribute(hotDealCity);
				session.setAttribute(hotDealLocation, String.valueOf(objHotDealsInfo.getRetailerLocID()));
			}
			if (!"".equals(Utility.checkNull(strProductImg))) {
				objHotDealsInfo.setProductImage(strProductImg);
				model.put("addhotdealretailerform", objHotDealsInfo);
			}
			return new ModelAndView(view);
		} else {
			return new ModelAndView(new RedirectView(view));
		}
	}

	
	/**
	 * This ModelAttribute sort Deal start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("DealStartHours")
	public Map<String, String> populateDealStartHrs() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}


	/**
	 * This ModelAttribute sort Deal start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("DealStartMinutes")
	public Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method return all the location for that retailer by call service and DAO methods.
	 * 
	 * @param productId as request parameter
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param strTabIndexLoc as request parameter.
	 * @return String with address, city, state, postal code.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/fetchretailerlocation.htm", method = RequestMethod.GET)
	 @ResponseBody
	 public final String getRetailerLocation(@RequestParam(value = "productId", required = true) String productId,
			 @RequestParam(value = "tabIndexLoc", required = true) String strTabIndexLoc, HttpServletRequest request,
			HttpServletResponse response) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealRetailerController : getRetailerLocation ");
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='retailerLocID' id='retailerLocID'  onchange='getLocationTrigger(this);' tabindex=" + strTabIndexLoc + "><option value='0'>Please Select Location</option>";

		innerHtml.append(finalHtml);
		// int selRetailerLoc = 0;
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		// String strSelRetailerLoc = (String)
		// request.getSession().getAttribute("selRetailerLoc");

		final String strSelRetailerLoc = (String) request.getSession().getAttribute(hotDealLocation);
		int selRetailerLoc = 0;
		if ("".equals(Utility.checkNull(productId))) {
			productId = null;
		} else if ("0".equals(productId)) {
			productId = null;
		}

		if (strSelRetailerLoc != null) {
			selRetailerLoc = Integer.valueOf(strSelRetailerLoc);
		}
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

			ArrayList<Retailer> retailers = null;
			retailers = supplierService.getRetailersLocForProducts(productId, user.getRetailerId());
			response.setContentType("text/xml");
			StringBuffer value = null;
			for (int i = 0; i < retailers.size(); i++)
			{
				value = new StringBuffer();
				if (retailers.get(i).getAddress1() != null)
				{
					value.append(retailers.get(i).getAddress1());
					value.append(ApplicationConstants.COMMA);
					if (retailers.get(i).getCity() != null)
					{
						value.append(retailers.get(i).getCity());
						value.append(ApplicationConstants.COMMA);
						if (retailers.get(i).getState() != null)
						{
							value.append(retailers.get(i).getState());
							value.append(ApplicationConstants.COMMA);
							if (retailers.get(i).getPostalCode() != null)
							{
								value.append(retailers.get(i).getPostalCode());
								value.append(ApplicationConstants.COMMA);
							}
						}
					}
				}

				if (selRetailerLoc == retailers.get(i).getRetailLocationID())
				{
					innerHtml.append("<option selected=true value=" + retailers.get(i).getRetailLocationID() + ">" + value.toString() + "</option>");
				} else {
					innerHtml.append("<option value=" + retailers.get(i).getRetailLocationID() + ">" + value.toString() + "</option>");
				}
			}
			innerHtml.append("</select>");
		}
		catch (ScanSeeServiceException e)
		{
			 LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			 throw e;
		}
		return innerHtml.toString();
	}

	/**
	 * This controller  method return all the cities names for that retailer by call service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param strTabIndexCity as request parameter.
	 * @return VIEW_NAME addHotDeals view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/fetchhotdealpopcenters.htm", method = RequestMethod.GET)
	@ResponseBody
	public final String getHdPopulationCenters(@RequestParam(value = "tabIndexCity", required = true) String strTabIndexCity,
			HttpServletRequest request, HttpServletResponse response) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealRetailerController : getHdPopulationCenters ");
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='city' id='City' onchange='getCityTrigger(this);' tabindex=" + strTabIndexCity + "><option value=''>Please Select Population Center</option>";
		Long lUserID = new Long(0);
		innerHtml.append(finalHtml);
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			ArrayList<City> cities = null;
			final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			lUserID = user.getUserID();
			cities = retailerService.getHdPopulationCenters(lUserID);
			response.setContentType("text/xml");
			request.getSession().setAttribute("hotdealpopcenters", cities);
			String strSelCity = (String) request.getSession().getAttribute(hotDealCity);
			if ("".equals(Utility.checkNull(strSelCity))) {
				strSelCity = ApplicationConstants.ZERO;
			}
			int selCity = 0;
			if (strSelCity != null)
			{
				if ("All".equals(strSelCity))
				{
					innerHtml.append("<option selected=true value='All'>All Cities</option>");
				} else {
					selCity = Integer.valueOf(strSelCity);
					innerHtml.append("<option value='All'>All Cities</option>");
				}
			} else {
				innerHtml.append("<option value='All'>All Cities</option>");
			}

			for (int i = 0; i < cities.size(); i++)
			{
				if (cities.get(i).getdMAName() != null)
				{
					if (selCity == cities.get(i).getPopulationCenterID()) {
						innerHtml.append("<option selected=true value=" + cities.get(i).getPopulationCenterID() + ">" + cities.get(i).getdMAName()
								+ "</option>");
					} else {
						innerHtml.append("<option value=" + cities.get(i).getPopulationCenterID() + ">" + cities.get(i).getdMAName() + "</option>");
					}
				}
			}
			innerHtml.append("</select>");
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return innerHtml.toString();
	}

	/**
	 * This controller method will display HotDeals details in iphone screen formate.
	 * 
	 * @param objHotdealInfo HotDealInfo instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @param model ModelMap instance as parameter.
	 * @return VIEW_NAME addHotDeals view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/previewHotDealRetailor.htm", method = RequestMethod.POST)
	public final String previewPage(HotDealInfo objHotdealInfo, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealRetailerController : previewPage ");
		final String viewPreview = "previewRetailerHotDeals";
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);

		String strSalePrice = null;
		String strRegularPrice = null;
		String strStartDT = null;
		String longDescription = null;
		String hotDealName = null;
		String strHotDealImage = null;
		Long retailID = null;
		ArrayList<HotDealInfo> productList = null;

		request.getSession().removeAttribute("saleprice");
		request.getSession().removeAttribute("regularprice");
		request.getSession().removeAttribute("startDT");
		session.removeAttribute(hotDealLocation);
		session.removeAttribute(hotDealCity);
		request.getSession().removeAttribute("longDescription");
		request.getSession().removeAttribute("hotDealName");
		/*
		 * request.getSession().removeAttribute("cityHidden");
		 * request.getSession().removeAttribute("retailerLocHidden");
		 */

		final ServletContext servletContext = request.getSession().getServletContext();
		final  WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		retailID = Long.valueOf(loginUser.getRetailerId());

		if ("City".equals(objHotdealInfo.getDealForCityLoc()))
		{
			request.getSession().setAttribute(hotDealCity, objHotdealInfo.getCity());
			session.removeAttribute(hotDealLocation);
		} else if ("Location".equals(objHotdealInfo.getDealForCityLoc())) {
			session.removeAttribute(hotDealCity);
			session.setAttribute(hotDealLocation, String.valueOf(objHotdealInfo.getRetailerLocID()));
		}
		/*
		 * if (!Utility.checkNull(objHotdealInfo.getCity()).equals("")) {
		 * objHotdealInfo.setCityHidden(objHotdealInfo.getCity());
		 * session.setAttribute("cityHidden", objHotdealInfo.getCityHidden()); }
		 * if
		 * (!Utility.checkNull(String.valueOf(objHotdealInfo.getRetailerLocID(
		 * ))).equals("")) {
		 * objHotdealInfo.setLocationHidden(String.valueOf(objHotdealInfo
		 * .getRetailerLocID())); session.setAttribute("retailerLocHidden",
		 * objHotdealInfo.getLocationHidden()); }
		 */
		strSalePrice = objHotdealInfo.getSalePrice();
		strRegularPrice = objHotdealInfo.getPrice();
		strStartDT = objHotdealInfo.getDealStartDate();
		longDescription = objHotdealInfo.getHotDealLongDescription();
		hotDealName = objHotdealInfo.getHotDealName();
		final String prdname = objHotdealInfo.getProductName();
		// LOG.info("productName*************" + productId);
		final String []str = prdname.split(",");
		final String productName = str[0].trim();
		objHotdealInfo.setProductName(productName);
		objHotdealInfo.setRetailID(user.getRetailerId());
		session.setAttribute("saleprice", strSalePrice);
		session.setAttribute("regularprice", strRegularPrice);
		session.setAttribute("startDT", strStartDT);
		session.setAttribute("longDescription", longDescription);
		session.setAttribute("hotDealName", hotDealName);

		try
		{
			strHotDealImage = (String) session.getAttribute("addHotDealImagePath");
			if (!"".equals(Utility.checkNull(strHotDealImage)) 
					&& !ApplicationConstants.UPLOAD_IMAGE_PATH.equals(strHotDealImage)) {
				session.setAttribute("hotDealProductImage", strHotDealImage);
			} else if (productName != null && !"".equals(productName)) {
				productList = retailerService.getProdDetails(productName, retailID);
				if (productList != null && !productList.isEmpty()) {
					session.setAttribute("hotDealProductImage", productList.get(0).getProductImagePath());
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("previewhotdealretailerform", objHotdealInfo);
		return viewPreview;
	}
	
	
	/**
	 * This controller method will display the promotion screen.
	 * 
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param response  as request parameter.
	 * @param model as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/promotionsdashboard.htm", method = RequestMethod.GET)
	public final ModelAndView showPromotionSetUpPage(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealRetailerController : showPromotionSetUpPage");
		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		leftNav = UtilCode.setRetailerModulesStyle("Promotions", null, leftNav);
		session.setAttribute("retlrLeftNav", leftNav);
		return new ModelAndView("promotionmainpage");
	}
	
	
	
	/**
	 * This controller method will upload Hot deal image .
	 * 
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param result as request parameter.
	 * @param objHotDealsInfo instance of HotDealInfo.
	 * @param response as request parameter.
	 * @return string given a view name.
	 * @throws Exception will be thrown.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/uploadhotdealsimg.htm", method = RequestMethod.POST)
	public final String onSubmitImage(@ModelAttribute("addhotdealretailerform") HotDealInfo objHotDealsInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws  Exception, ScanSeeServiceException 
			{
		LOG.info("Inside AddHotDealRetailerController : onSubmitImage ");
		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

		//imageValidSizeValFlg = Utility.validImageDimension(70, 70, objHotDealsInfo.getImageFile().getInputStream());

		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, objHotDealsInfo.getImageFile().getInputStream());
		if (imageSizeValFlg )
		{
			/*commented code for fixing the crop image issue validation.
			 
			 
			/*imageSizeValFlg = true;
			if (imageSizeValFlg)
			{
				final BufferedImage img = ImageIO.read(objHotDealsInfo.getImageFile().getInputStream());
				w = img.getWidth(null);
				h = img.getHeight(null);
				session.setAttribute("imageHt", h);
				session.setAttribute("imageWd", w);

				final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
				// session.removeAttribute("welcomePageBtnVal");
				imageSource = objHotDealsInfo.getImageFile().getOriginalFilename();
				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png" + "?" + date.getTime();
				final String filePath = tempImgPath + fileSeparator + objHotDealsInfo.getImageFile().getOriginalFilename();
				Utility.writeFileData(objHotDealsInfo.getImageFile(), filePath);
				if (imageValidSizeValFlg) {
					strResponse.append("ValidImageDimention");
					strResponse.append("|" + objHotDealsInfo.getImageFile().getOriginalFilename());
					session.setAttribute("addHotDealImagePath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				}
				strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
				session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			} else {
				session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
				return null;
			}*/
			session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
		} else {
			final BufferedImage img = Utility.getBufferedImageForMinDimension(objHotDealsInfo.getImageFile().getInputStream(), request.getRealPath("images"), 
					objHotDealsInfo.getImageFile().getOriginalFilename(), "HotDeal");
			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
			imageSource = objHotDealsInfo.getImageFile().getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();
			final String filePath = tempImgPath + fileSeparator + objHotDealsInfo.getImageFile().getOriginalFilename();
			Utility.writeImage(img, filePath);
			if (imageValidSizeValFlg) {
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objHotDealsInfo.getImageFile().getOriginalFilename());
				session.setAttribute("addHotDealImagePath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
						+ imageSource);
			}
			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
		}
		return null;
	}
	
	
	/**
	 * This controller method will check products are associated with supplier add hot deals screen.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param productId request as parameter.
	 * @param retailId request as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/checkRetAssociatedProd", method = RequestMethod.GET)
	@ResponseBody
	public final String checkAssociatedProd(@RequestParam(value = "productId", required = true) String productId, 
			@RequestParam(value = "retailID", required = true) int retailId, HttpServletRequest request, HttpSession session,
			HttpServletResponse response) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealController : checkAssociatedProd ");
		session.removeAttribute("pdtInfoList");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final ArrayList<Product> arPdtInfoList = (ArrayList<Product>) retailerService.getPdtInfoForDealCoupon(productId, retailId);
		if (arPdtInfoList != null && !arPdtInfoList.isEmpty())
		{
			for (int i = 0; i < arPdtInfoList.size(); i++)
			{
				if ("".equals(Utility.checkNull(arPdtInfoList.get(i).getPrice()))) {
					arPdtInfoList.get(i).setPrice("0");
				}
				if ("".equals(Utility.checkNull(arPdtInfoList.get(i).getImagePath()))) {
					arPdtInfoList.get(i).setImagePath(ApplicationConstants.UPLOAD_IMAGE_PATH);
				}
			}
			request.getSession().setAttribute("pdtInfoList", arPdtInfoList);
		}
		return null;
	}
}

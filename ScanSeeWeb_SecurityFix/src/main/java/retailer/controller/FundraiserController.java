package retailer.controller;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.FundraiserValidator;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.Event;
import common.pojo.RetailerLocation;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * This class is used to handle Fundraiser related functionalities.
 * 
 * @author dhruvanath_mm
 *
 */
@Controller
public class FundraiserController {

	/**
	 * Getting the logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(FundraiserController.class);
	
	FundraiserValidator objFundraiserValidator;
	
	/**
	 * To set fundraiser validator
	 * 
	 * @param objFundraiserValidator
	 */
	@Autowired
	public void setObjFundraiserValidator(FundraiserValidator objFundraiserValidator) {
		this.objFundraiserValidator = objFundraiserValidator;
	}
	
	/**
	 * This controller method will display the Fundraiser to manage.
	 * 
	 * @param objEvent
	 * @param result
	 * @param model
	 * @param request
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/managefundraiser.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public final ModelAndView manageFundraiserEvents(@ModelAttribute("managefundraiserform") Event objEvent, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException	{
		LOG.info("Inside class: FundraiserController, method: manageFundraiserEvents()");
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		String view = "managefundraiser";
		int currentPage = 1;
		String pageNumber = "0";
		int lowerLimit = 0;
		int recordCount = 20;
//		Event allEvents = new Event(); 
		SearchResultInfo objSearchResult = null;
		
		final Users loginUser = (Users) session.getAttribute("loginuser");
		final int retailID = loginUser.getRetailerId();
		
		session.removeAttribute("imageCropPage");
		session.removeAttribute("hideBackButton");
		String dispMsgFlag = (String)session.getAttribute("msgFlag");
		if(null == dispMsgFlag)	{
			session.removeAttribute("fundMessageFont");
			session.removeAttribute("responseFundMsg");
		} else if(!"display".equals(dispMsgFlag))	{
			session.removeAttribute("msgFlag");
		} else	{
			session.removeAttribute("msgFlag");
		}
		
		// for pagination.
		final String pageFlag = request.getParameter("pageFlag");

		try {

			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {
				currentPage = (lowerLimit + recordCount) / recordCount;
			}
			
			objSearchResult = retailerService.getFundraiserEventList(retailID, lowerLimit, objEvent.getFundSearchKey(), loginUser.getUserID());
			
			RetailerLeftNavigation retailerLeftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			retailerLeftNav = UtilCode.setRetailerModulesStyle("FundraiserEvent", null, retailerLeftNav);
			session.setAttribute("retlrLeftNav", retailerLeftNav);
			
			if (null != objSearchResult && objSearchResult.getEventList().size() <= 0 && "".equals(Utility.checkNull(objEvent.getFundSearchKey()))) {
				//Back button to hide in add event screen if manage event list is empty.
				session.setAttribute("hideBackButton", "yes");
				return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/addFundraiser.htm"));
			} else if (objSearchResult != null) {
				session.setAttribute("fundraiserEventList", objSearchResult);
				final Pagination objPage = Utility.getPagination(objSearchResult.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/managefundraiser.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				
				if(null == objSearchResult.getEventList() || objSearchResult.getEventList().isEmpty())	{
					session.setAttribute("responseFundMsg", "No Fundraisers Found");
					session.setAttribute("fundMessageFont", "font-weight:bold;color:red;");
				}
			} else	{
				session.setAttribute("responseFundMsg", "No Fundraisers Found");
				session.setAttribute("fundMessageFont", "font-weight:bold;color:red;");
			}
//			model.put("manageeventform", allEvents);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : manageAllEvents : " + e.getMessage());
			throw e;
		}
		LOG.info("Exit class: FundraiserController, method: manageFundraiserEvents()");
		return new ModelAndView(view);
	}
	
	/**
	 * Method to add new fundraiser.
	 * 
	 * @param objEvent
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @param map
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/addFundraiser.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView addFundraiser(@ModelAttribute("addeditfundraiserform") Event objEvent, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) throws ScanSeeServiceException {
		LOG.info("Inside class: FundraiserController, method: addFundraiser()");
		String view = "addfundraiser";
		
		final Users loginUser = (Users) session.getAttribute("loginuser");
		final int retailID = loginUser.getRetailerId();
		ArrayList<RetailerLocation> retLocList = null;
		List<Category> deptList = null;
		List<Category> fundraiserCatList = null;
		SearchResultInfo objSearchResultInfo = null;
		List<Event> eventList = null;
		Integer isFundraising = 1;
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		session.setAttribute("imageCropPage", "Fundraiser");
		// Minimum crop height and width
		session.setAttribute("minCropWd", 250);
		session.setAttribute("minCropHt", 250);
//		session.removeAttribute("addedDeptId");
		if(null == objEvent.getEventImageName() || "".equals(objEvent.getEventImageName())) {
			session.setAttribute("fundraiserImagePath", ApplicationConstants.UPLOAD_IMAGE_PATH);
		}
		
		try {
			retLocList = retailerService.getEventRetailerLocationList(retailID);
			deptList = retailerService.getFundraiserDepartmentList(retailID);
			objSearchResultInfo = retailerService.getRetailerEventList(retailID, 0, objEvent.getEventSearchKey(), isFundraising);
			eventList = objSearchResultInfo.getEventList();
			fundraiserCatList = retailerService.getFundraiserEventCategory(retailID);

			session.setAttribute("departmentsList", deptList);
			session.setAttribute("retLocList", retLocList);
			session.setAttribute("associatedEventList", eventList);
			session.setAttribute("fundraiserCatList", fundraiserCatList);		
			
			if(null == eventList || eventList.isEmpty())	{
				session.setAttribute("responseFundMsg", "No Event Found");
				session.setAttribute("fundMessageFont", "font-weight:bold;color:red;");
			} else	{
				session.removeAttribute("responseFundMsg");
				session.removeAttribute("fundMessageFont");
			}
			
			if(null == objEvent.getIsRetLoc())	{
				objEvent.setIsRetLoc("no");
			}
			
			if(null == objEvent.getIsEventTied())	{
				objEvent.setIsEventTied("no");
			}
			
			if(null != objEvent && null != objEvent.getEventSearchKey())	{
				objEvent.setHidLongDescription(objEvent.getLongDescription());
			}
			
			map.put("addeditfundraiserform", objEvent);	
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : addEvent : " + e.getMessage());
			throw e;
		}
		
		LOG.info("Exit class: FundraiserController, method: addFundraiser()");
		return new ModelAndView(view);
	}
	
	/**
	 * Method to edit existing fundraiser.
	 * 
	 * @param objEvent
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @param map
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/editFundraiser.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView editFundraiser(@ModelAttribute("addeditfundraiserform") Event objEvent, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap map)	throws ScanSeeServiceException{
		LOG.info("Inside class: FundraiserController, method: addFundraiser()");
		String view = "addfundraiser";
		
		final Users loginUser = (Users) session.getAttribute("loginuser");
		final int retailID = loginUser.getRetailerId();
		ArrayList<RetailerLocation> retLocList = null;
		List<Category> deptList = null;
		List<Category> fundraiserCatList = null;
		SearchResultInfo objSearchResultInfo = null;
		List<Event> eventList = null;
		Integer eventId = null;
		Integer lowerLimit = 0;
		Integer isFundraising = 1;
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		session.setAttribute("imageCropPage", "Fundraiser");
		// Minimum crop height and width
		session.setAttribute("minCropWd", 250);
		session.setAttribute("minCropHt", 250);
		if(null == objEvent.getEventImageName() || "".equals(objEvent.getEventImageName())) {
			session.setAttribute("fundraiserImagePath", ApplicationConstants.UPLOAD_IMAGE_PATH);
		}
		
		try {
			retLocList = retailerService.getEventRetailerLocationList(retailID);
			deptList = retailerService.getFundraiserDepartmentList(retailID);
			objSearchResultInfo = retailerService.getRetailerEventList(retailID, lowerLimit, objEvent.getEventSearchKey(), isFundraising);
			eventList = objSearchResultInfo.getEventList();
			fundraiserCatList = retailerService.getFundraiserEventCategory(retailID);
			eventId = objEvent.getEventID();
			
			objEvent = retailerService.getFundEventDetails(eventId, retailID);
			objEvent.setEventID(eventId);
			objEvent.setHidLongDescription(objEvent.getLongDescription());
			objEvent.setEventDate(objEvent.getEventStartDate());

			session.setAttribute("departmentsList", deptList);
			session.setAttribute("retLocList", retLocList);
			session.setAttribute("associatedEventList", eventList);
			session.setAttribute("fundraiserCatList", fundraiserCatList);
			request.setAttribute("fmEditScreen", "yes");
			
			if(null == eventList || eventList.isEmpty())	{
				session.setAttribute("responseFundMsg", "No Event Found");
				session.setAttribute("fundMessageFont", "font-weight:bold;color:#red;");
			} else	{
				session.removeAttribute("responseFundMsg");
				session.removeAttribute("fundMessageFont");
			}
			
			if(null != objEvent.getEventImagePath() || !"".equals(objEvent.getEventImagePath()))	{
				session.setAttribute("fundraiserImagePath", objEvent.getEventImagePath());
			}
			
			if(null == objEvent.getIsRetLoc())	{
				objEvent.setIsRetLoc("no");
			} else	{
				if("1".equals(objEvent.getIsRetLoc()))	{
					objEvent.setIsRetLoc("yes");
				} else	{
					objEvent.setIsRetLoc("no");
				}
			}
			
			if(null == objEvent.getIsEventTied())	{
				objEvent.setIsEventTied("no");
			} else	{
				if("1".equals(objEvent.getIsEventTied()))	{
					objEvent.setIsEventTied("yes");
					objEvent.setHidEventAssociatedIds(objEvent.getEventAssociatedIds());
				} else	{
					objEvent.setIsEventTied("no");
				}
			}
			
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : addEvent : " + e.getMessage());
			throw e;
		}
		
		map.put("addeditfundraiserform", objEvent);
		LOG.info("Exit class: FundraiserController, method: addFundraiser()");
		return new ModelAndView(view);
	}
	
	/**
	 * This controller method will return location details info associated with
	 * fundraiser Event page from Database based on parameter(eventId).
	 * 
	 * @param objEvent
	 *            instance of Event.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/showfundraisereventlocation.htm", method = RequestMethod.GET)
	public final ModelAndView getFundraiserEventLocation(@ModelAttribute("eventlocationform") Event objEvent, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside FundraiserController : showFundraiserEventLocation ");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		final Users loginUser = (Users) session.getAttribute("loginuser");
		final int iRetailID = loginUser.getRetailerId();
		
		session.removeAttribute("responseEventMsg");
		session.removeAttribute("eventMessageFont");
		request.setAttribute("fmFundraiser", "yes");

		List<RetailerLocation> arEventLocationList = null;

		try {
			arEventLocationList = retailerService.getFundraiserEventLocation(objEvent.getEventID(), iRetailID);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : showEventLocation : " + e.getMessage());
			throw e;
		}

		model.addAttribute("eventlocationform", objEvent);
		session.setAttribute("eventAssocLocationList", arEventLocationList);
		LOG.info("Exit class: FundraiserController, method: getFundraiserEventLocation()");
		return new ModelAndView("fundraiserlocation");
	}
	
	/**
	 * Controller to add new department for fundraiser.
	 * 
	 * @param objCategory
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/addFundraiserDept.htm", method = RequestMethod.GET)
	public final ModelAndView addFundraiserDepartment(@ModelAttribute("addFundraiserDeptForm") Category objCategory, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session)	{
		LOG.info("Inside FundraiserController : addFundraiserDepartment() ");
		
		return new ModelAndView("addFundraiserDept");
	}
	
	/**
	 * Method to save fundraiser department.
	 * 
	 * @param deptName
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/saveFundraiserDept", method = RequestMethod.POST)
	@ResponseBody
	public final String saveFundraiserDepartment(@RequestParam(value = "deptName", required = true) String deptName, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside FundraiserController : saveFundraiserDepartment() ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		String strResponse = null;
		final Users loginUser = (Users) session.getAttribute("loginuser");
		final Long userId = loginUser.getUserID();
		final Integer retailId = loginUser.getRetailerId();
		List<Category> deptList = null;
		
		try	{
			strResponse = retailerService.saveFundraiserDept(userId, retailId, deptName);
			if(strResponse != null && !ApplicationConstants.DUPLICATE_DEPARTMENT.equals(strResponse))	{
				deptList = (List<Category>) session.getAttribute("departmentsList");
				Category category = new Category();
				category.setDeptID(Integer.parseInt(strResponse));
				category.setDeptName(deptName);
				if(null != deptList) {
					deptList.add(category);
				} else {
					deptList = new ArrayList<Category>();
					deptList.add(category);
				}
				session.setAttribute("departmentsList", deptList);
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : saveFundraiserDepartment : " + e.getMessage());
		}
		return strResponse;
	}
	
	/**
	 * This controller method will upload Fundraiser image.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param objEvent
	 *            instance of Event.
	 * @param response
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws Exception
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadfundraiserimg.htm", method = RequestMethod.POST)
	public final String uploadFundraiserImage(@ModelAttribute("addeditfundraiserform") Event objEvent, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception, ScanSeeServiceException {
		LOG.info("Inside FundraiserController : uploadFundraiserImage() ");

		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");

		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, objEvent.getEventImageFile().getInputStream());

		if (imageSizeValFlg) {
			session.setAttribute("eventImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
		} else {
			final BufferedImage img = Utility.getBufferedImageForMinDimension(objEvent.getEventImageFile().getInputStream(), request.getRealPath("images"), objEvent.getEventImageFile().getOriginalFilename(), "Location");

			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();

			imageSource = objEvent.getEventImageFile().getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();

			final String filePath = tempImgPath + fileSeparator + objEvent.getEventImageFile().getOriginalFilename();

			Utility.writeImage(img, filePath);

			if (imageValidSizeValFlg) {
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objEvent.getEventImageFile().getOriginalFilename());
				session.setAttribute("eventImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			}

			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
		}
		LOG.info("Exit FundraiserController : uploadFundraiserImage() ");
		return null;
	}
	
	/**
	 * Method to save or update fundraiser.
	 * 
	 * @param objEvent
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/saveFundraiser.htm", method = RequestMethod.POST)
	public final ModelAndView saveUpdateFundraiser(@ModelAttribute("addeditfundraiserform") Event objEvent, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map) {
		LOG.info("Inside FundraiserController : saveUpdateFundraiser() ");
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
//		session.removeAttribute("addedDeptId");
		final Users loginUser = (Users) session.getAttribute("loginuser");
		final Long userId = loginUser.getUserID();
		final Integer retailId = loginUser.getRetailerId();
		String strResponse = null;
		
		try	{
			objEvent.setRetailID(retailId);
			objEvent.setHidEventAssociatedIds(objEvent.getEventAssociatedIds());
			objEvent.setHidLongDescription(objEvent.getLongDescription());
			objFundraiserValidator.validate(objEvent, result);
			if(result.hasErrors())	{
				return new ModelAndView(objEvent.getViewName());
			}
			strResponse = retailerService.saveUpdateFundraiser(userId, objEvent);
			
			if(ApplicationConstants.SUCCESS.equals(strResponse))	{
				if(null == objEvent.getEventID())	{
					session.setAttribute("fundMessageFont", "font-weight:bold;color:#00559c;");
					session.setAttribute("responseFundMsg", "Fundraiser Event Created Succesfully");
				} else	{
					session.setAttribute("fundMessageFont", "font-weight:bold;color:#00559c;");
					session.setAttribute("responseFundMsg", "Fundraiser Event Updated Succesfully");
				}
				session.setAttribute("msgFlag", "display");
			}
			
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : saveUpdateFundraiser : " + e.getMessage());
		}
		return new ModelAndView(new RedirectView("managefundraiser.htm"));
	}
	
	/**
	 * This controller method will delete fundraising events by call service layer.
	 * 
	 * @param event
	 *            instance of Event.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/deletefundraiser", method = RequestMethod.POST)
	public ModelAndView deleteFundraiser(@ModelAttribute("managefundraiserform") Event event, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside EventController : deleteFundraiser");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		SearchResultInfo objSearchResult = null;
		
		session.removeAttribute("fundMessageFont");
		session.removeAttribute("responseFundMsg");

		int recordCount = 20;
		int lowerLimit = 0;
		int currentPage = 1;
		try {
			final String status = retailerService.deleteFundraiser(event.getEventID());

			if (ApplicationConstants.SUCCESS.equals(status)) {
				session.setAttribute("fundMessageFont", "font-weight:bold;color:#00559c;");
				session.setAttribute("responseFundMsg", "Fundraiser Event Deleted Succesfully");
				session.setAttribute("msgFlag", "display");
			}

/*			objSearchResult = retailerService.getFundraiserEventList(loginUser.getRetailerId(), lowerLimit, event.getEventSearchKey());
			if (objSearchResult != null) {
				session.setAttribute("eventManageList", objSearchResult);
				final Pagination objPage = Utility.getPagination(objSearchResult.getTotalSize(), currentPage,
						"/ScanSeeWeb/retailer/managefundraiser.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
			model.put("managefundraiserform", event);*/
		} catch (ScanSeeServiceException exception) {
			LOG.error("Inside EventController : deleteEvent : " + exception.getMessage());
			throw exception;
		}
		return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/managefundraiser.htm"));
//		return new ModelAndView("managefundraiser");
	}
	
	/**
	 * Mehtod to clear fundraiser details while adding new fundraiser.
	 * 
	 * @param objEvent
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @param map
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/clearFundraiser.htm", method = { RequestMethod.POST})
	public ModelAndView clearFundraiser(@ModelAttribute("addeditfundraiserform") Event objEvent, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) throws ScanSeeServiceException {
		LOG.info("Inside class: FundraiserController, method: clearFundraiser()");
		String view = "addfundraiser";
		
		final Users loginUser = (Users) session.getAttribute("loginuser");
		final int retailID = loginUser.getRetailerId();
		/*ArrayList<RetailerLocation> retLocList = null;
		List<Category> deptList = null;
		List<Category> fundraiserCatList = null;
		SearchResultInfo objSearchResultInfo = null;
		List<Event> eventList = null;
		Integer isFundraising = 1;
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);*/
		Event eventObj = new Event();
		
		eventObj.setRetailID(retailID);
		eventObj.setIsRetLoc("no");
		eventObj.setIsEventTied("no");
		
		map.put("addeditfundraiserform", eventObj);
		LOG.info("Exit class: FundraiserController, method: addFundraiser()");
		return new ModelAndView(new RedirectView("addFundraiser.htm"));
	}
}

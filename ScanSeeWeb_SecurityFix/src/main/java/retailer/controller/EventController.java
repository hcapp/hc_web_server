package retailer.controller;

import java.awt.image.BufferedImage;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.EventValidator;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.Event;
import common.pojo.RetailerLocation;
import common.pojo.SearchResultInfo;
import common.pojo.State;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * This class is used to handle Event Create related functionalities.
 * 
 * @author Kumar D
 */

@Controller
public class EventController {
	/**
	 * Getting the logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(EventController.class);

	/**
	 * Variable eventValidator declared as instance of EventValidator.
	 */
	EventValidator eventValidator;

	/**
	 * To set eventValidator.
	 * 
	 * @param eventValidator
	 *            to set.
	 */
	@Autowired
	public void setEventValidator(EventValidator eventValidator) {
		this.eventValidator = eventValidator;
	}

	/**
	 * This ModelAttribute sort Event start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("StartMinutes")
	public Map<String, String> populateEventStartMins() throws ScanSeeServiceException {
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}

		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort Event start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("StartHours")
	public Map<String, String> populateEventStartHrs() throws ScanSeeServiceException {
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method will upload Event image in add Event.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param objEvent
	 *            instance of Event.
	 * @param response
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws Exception
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadeventimg.htm", method = RequestMethod.POST)
	public final String uploadEventImage(@ModelAttribute("addediteventform") Event objEvent, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception, ScanSeeServiceException {
		LOG.info("Inside EventController : uploadEventImage ");

		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");

		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, objEvent
				.getEventImageFile().getInputStream());

		if (imageSizeValFlg) {
			session.setAttribute("eventImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
		} else {
			final BufferedImage img = Utility.getBufferedImageForMinDimension(objEvent.getEventImageFile().getInputStream(),
					request.getRealPath("images"), objEvent.getEventImageFile().getOriginalFilename(), "Location");

			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();

			imageSource = objEvent.getEventImageFile().getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();

			final String filePath = tempImgPath + fileSeparator + objEvent.getEventImageFile().getOriginalFilename();

			Utility.writeImage(img, filePath);

			if (imageValidSizeValFlg) {
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objEvent.getEventImageFile().getOriginalFilename());
				session.setAttribute("eventImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			}

			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
		}
		return null;
	}

	/**
	 * This controller method will display the manage All Events by call service
	 * layer.
	 * 
	 * @param objEvent
	 *            instance of Event.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */

	@RequestMapping(value = "/manageevents.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView manageAllEvents(@ModelAttribute("manageeventform") Event objEvent, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside EventController : manageAllEvents");

		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		SearchResultInfo objSearchResult = null;
		int currentPage = 1;
		String pageNumber = "0";
		int lowerLimit = 0;
		int recordCount = 20;
		Integer isFundraissing = null;

		session.removeAttribute("eventManageList");
	
		Event event = new Event();

		final Users loginUser = (Users) session.getAttribute("loginuser");
		final int retailID = loginUser.getRetailerId();

		session.removeAttribute("imageCropPage");
		// for pagination.
		final String pageFlag = request.getParameter("pageFlag");

		try {

			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
			} else {
				currentPage = (lowerLimit + recordCount) / recordCount;
			}

			objSearchResult = retailerService.getRetailerEventList(retailID, lowerLimit, objEvent.getEventSearchKey(), isFundraissing);

			RetailerLeftNavigation retailerLeftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			retailerLeftNav = UtilCode.setRetailerModulesStyle("Event", null, retailerLeftNav);
			session.setAttribute("retlrLeftNav", retailerLeftNav);
			
			
			if (null != objSearchResult && objSearchResult.getEventList().size() <= 0 && "".equals(Utility.checkNull(objEvent.getEventSearchKey()))) {
				/*  Back button to hide in add event screen if manage event list is empty. */
				session.setAttribute("backButton", "no");
				
				return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/addevent.htm"));
			} else
				if (objSearchResult != null) {
					
					session.setAttribute("eventManageList", objSearchResult);
					final Pagination objPage = Utility.getPagination(objSearchResult.getTotalSize(), currentPage,
							"/ScanSeeWeb/retailer/manageevents.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					
					// for Removing "no records found" message.
					final String strNoRecords = (String)session.getAttribute("responseEventMsg");
					if (!"".equals(Utility.checkNull(strNoRecords)) && strNoRecords.equals("No Event found!")){
						session.removeAttribute("responseEventMsg");
						session.removeAttribute("eventMessageFont");
					}
				}


			if (objSearchResult.getTotalSize() > 0 || !"".equals(Utility.checkNull(objEvent.getEventSearchKey()))) {
				if (objSearchResult.getTotalSize() == 0) {
					session.setAttribute("eventMessageFont", "font-weight:bold;color:red;");
					session.setAttribute("responseEventMsg", "No Event found!");
				}
			}
			
			session.removeAttribute("backButton");
			model.put("manageeventform", event);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : manageAllEvents : " + e.getMessage());
			throw e;
		}
		return new ModelAndView("manageevents");
	}

	/**
	 * This controller method will delete Events by call service layer.
	 * 
	 * @param event
	 *            instance of Event.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/deleteevent", method = RequestMethod.POST)
	public ModelAndView deleteEvent(@ModelAttribute("manageeventform") Event event, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside EventController : deleteEvent");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		SearchResultInfo objSearchResult = null;
		
		int recordCount = 20;
		int lowerLimit = 0;
		int currentPage = 1;
		try {
			final String status = retailerService.deleteEvent(event.getEventID());

			if (ApplicationConstants.SUCCESS.equals(status)) {
				session.setAttribute("eventMessageFont", "font-weight:bold;color:#00559c;");
				session.setAttribute("responseEventMsg", "Event Deleted Successfully.");
			}

			objSearchResult = retailerService.getRetailerEventList(loginUser.getRetailerId(), lowerLimit, event.getEventSearchKey(), null);
				if (objSearchResult != null) {
					session.setAttribute("eventManageList", objSearchResult);
					final Pagination objPage = Utility.getPagination(objSearchResult.getTotalSize(), currentPage,
							"/ScanSeeWeb/retailer/manageevents.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				}
				
				model.put("manageeventform", event);
		} catch (ScanSeeServiceException exception) {
			LOG.error("Inside EventController : deleteEvent : " + exception.getMessage());
			throw exception;
		}
		return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/manageevents.htm"));
	}

	/**
	 * This controller method will return location details info associated with
	 * Event page from Database based on parameter(eventId).
	 * 
	 * @param objEvent
	 *            instance of Event.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/showeventlocation.htm", method = RequestMethod.GET)
	public final ModelAndView showEventLocation(@ModelAttribute("eventlocationform") Event objEvent, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside EventController : showEventLocation ");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		final Users loginUser = (Users) session.getAttribute("loginuser");
		final int iRetailID = loginUser.getRetailerId();
		
		session.removeAttribute("responseEventMsg");
		session.removeAttribute("eventMessageFont");

		List<RetailerLocation> arEventLocationList = null;

		try {
			arEventLocationList = retailerService.getAllLocationEvent(objEvent.getEventID(), iRetailID);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : showEventLocation : " + e.getMessage());
			throw e;
		}

		model.addAttribute("eventlocationform", objEvent);
		session.setAttribute("eventAssocLocationList", arEventLocationList);

		return new ModelAndView("eventlocation");
	}

	/**
	 * This controller method will display add event screen.
	 * 
	 * @param event
	 *            instance of Event.
	 * @param request
	 *            HttpServletRequest instance.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return RETURN_VIEW add Event screen.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/addevent.htm", method = { RequestMethod.GET, RequestMethod.POST })
	String addEvent(@ModelAttribute("addediteventform") Event objEvent, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap map) throws ScanSeeServiceException {
		LOG.info("Inside EventController : addEvent ");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		
		final Users loginUser = (Users) session.getAttribute("loginuser");
		
		session.removeAttribute("responseEventMsg");
		session.removeAttribute("eventMessageFont");

		ArrayList<State> arStateList = null;
		ArrayList<Category> arEventCategoryList = null;
		ArrayList<RetailerLocation> arEventLocationList = null;

		List<Event> arEventPatternList = null;

		session.setAttribute("imageCropPage", "Event");
		// Minimum crop height and width
		session.setAttribute("minCropWd", 300);
		session.setAttribute("minCropHt", 150);
		session.setAttribute("eventImagePath", ApplicationConstants.UPLOAD_IMAGE_PATH);

		try {

			arStateList = supplierService.getAllStates();
			session.setAttribute("states", arStateList);

			arEventCategoryList = retailerService.getAllEventCategory();
			session.setAttribute("eventCategoryList", arEventCategoryList);

			arEventLocationList = retailerService.getEventRetailerLocationList(loginUser.getRetailerId());
			session.setAttribute("eventLocationList", arEventLocationList);

			arEventPatternList = retailerService.getEventPatterns();
			session.setAttribute("eventPatterns", arEventPatternList);

			objEvent.setIsOngoing("no");
			objEvent.setRecurrencePatternID(arEventPatternList.get(0).getRecurrencePatternID());
			// Daily Recurrence
			objEvent.setIsOngoingDaily("days");
			objEvent.setEveryWeekDay(1);
			// Weekly Recurrence
			objEvent.setEveryWeek(1);
			Date date = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			Integer day = calendar.get(Calendar.DAY_OF_WEEK);
			objEvent.setDays(new String[] { day.toString() });
			objEvent.setHiddenDay(day.toString());
			// Monthly Recurrence
			objEvent.setIsOngoingMonthly("date");
			objEvent.setDateOfMonth(calendar.get(Calendar.DATE));
			objEvent.setHiddenDate(calendar.get(Calendar.DATE));
			objEvent.setEveryMonth(1);
			// Month Day Recurrence
			Calendar tempCalendar = Calendar.getInstance();
			tempCalendar.setTime(date);
			tempCalendar.set(Calendar.DATE, 1);
			if (calendar.get(Calendar.DAY_OF_WEEK) < tempCalendar.get(Calendar.DAY_OF_WEEK)) {
				objEvent.setDayNumber((calendar.get(Calendar.WEEK_OF_MONTH)) - 1);
				objEvent.setHiddenWeek((calendar.get(Calendar.WEEK_OF_MONTH)) - 1);
			} else {
				objEvent.setDayNumber(calendar.get(Calendar.WEEK_OF_MONTH));
				objEvent.setHiddenWeek(calendar.get(Calendar.WEEK_OF_MONTH));
			}
			day = calendar.get(Calendar.DAY_OF_WEEK);
			objEvent.setEveryWeekDayMonth(new String[] { day.toString() });
			objEvent.setHiddenWeekDay(day.toString());
			objEvent.setEveryDayMonth(1);
			objEvent.setDayOfMonth(day.toString());
			// Range Of Recurrence
			Format formatter = new SimpleDateFormat("MM/dd/yyyy");
			objEvent.setEventStartDate(formatter.format(date));
			objEvent.setOccurenceType("noEndDate");
			objEvent.setEndAfter(1);
			objEvent.setEventEndDate(formatter.format(date));
			objEvent.setBsnsLoc("no");
			objEvent.setIsOngoing("no");
			map.put("addediteventform", objEvent);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : addEvent : " + e.getMessage());
			throw e;
		}
		return "addevent";
	}

	/**
	 * This controller method will display edit event screen.
	 * 
	 * @param event
	 *            instance of Event.
	 * @param request
	 *            HttpServletRequest instance.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return RETURN_VIEW add Event screen.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/editevent.htm", method = RequestMethod.POST)
	String editEvent(@ModelAttribute("manageeventform") Event objEvent, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) throws ScanSeeServiceException, ParseException {
		LOG.info("Inside EventController : editEvent ");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		
		final Users loginUser = (Users) session.getAttribute("loginuser");

		session.removeAttribute("responseEventMsg");
		session.removeAttribute("eventMessageFont");

		session.setAttribute("imageCropPage", "Event");
		// Minimum crop height and width
		session.setAttribute("minCropWd", 300);
		session.setAttribute("minCropHt", 150);

		ArrayList<State> arStateList = null;
		ArrayList<Category> arEventCategoryList = null;
		List<RetailerLocation> arEventLocationList = null;

		Event event = null;
		List<Event> arEventPatternList = null;

		try {

			arStateList = supplierService.getAllStates();
			session.setAttribute("states", arStateList);

			arEventCategoryList = retailerService.getAllEventCategory();
			session.setAttribute("eventCategoryList", arEventCategoryList);

			arEventLocationList = retailerService.getEventRetailerLocationList(loginUser.getRetailerId());
			session.setAttribute("eventLocationList", arEventLocationList);

			arEventPatternList = retailerService.getEventPatterns();
			session.setAttribute("eventPatterns", arEventPatternList);

			event = retailerService.getEventDetail(objEvent.getEventID());
			
			event.setEventID(Integer.valueOf(objEvent.getEventID()));
			event.setHiddenWeek(event.getDayNumber());
			String[] tempDays = event.getEveryWeekDayMonth();
			event.setHiddenWeekDay(tempDays[0]);
			event.setHiddenDate(event.getDateOfMonth());
	
			StringBuffer strDays = new StringBuffer();
			if (!"".equals(Utility.checkNull(event.getDays()))) {
				for (String s :event.getDays()) {
					strDays.append(s);
				}
				event.setHiddenDay(strDays.toString());
			}
			
			
			if ("".equals(Utility.checkNull(event.getEventImagePath()))) {
				session.setAttribute("eventImagePath", ApplicationConstants.UPLOADIMAGEPATH);
			} else {
				session.setAttribute("eventImagePath", event.getEventImagePath());
				int iDotIndex = event.getEventImagePath().lastIndexOf('.');
				if (iDotIndex == -1) {
					event.setEventImageName(null);
					session.setAttribute("eventImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				}
			}
			
			if (null == event.getIsOngoing() || "no".equalsIgnoreCase(event.getIsOngoing())) {
				event.setRecurrencePatternID(arEventPatternList.get(0).getRecurrencePatternID());
			}
			map.put("addediteventform", event);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : editEvent : " + e.getMessage());
			throw e;
		}
		return "editevent";
	}
	

	
	/**
	 * This  method will return event recurrence details .
	 * @param objEvent instance of Event.
	 * @return Event details.
	 * @throws ScanSeeServiceException
	 */
	public Event eventRecurrenceDetails(Event objEvent) throws ScanSeeServiceException {
		String eventStartTime = null;
		String eventEndTime = null;

		if ("yes".equalsIgnoreCase(objEvent.getIsOngoing())) {
			eventStartTime = objEvent.getEventStartTimeHrs() + ":" + objEvent.getEventStartTimeMins();
			objEvent.setEventStartTime(eventStartTime);
			eventEndTime = objEvent.getEventEndTimeHrs() + ":" + objEvent.getEventEndTimeMins();
			objEvent.setEventEndTime(eventEndTime);

			if ("Daily".equalsIgnoreCase(objEvent.getRecurrencePatternName())) {
				if ("days".equalsIgnoreCase(objEvent.getIsOngoingDaily())) {
					objEvent.setRecurrenceInterval(objEvent.getEveryWeekDay());
					objEvent.setIsWeekDay(false);
					objEvent.setDays(null);
					objEvent.setDayNumber(null);
				} else {
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					Date startDate = new Date(objEvent.getEventStartDate());
					Calendar c = Calendar.getInstance();
					c.setTime(startDate);
					Integer day = c.get(Calendar.DAY_OF_WEEK);
					Integer date = c.get(Calendar.DATE);

					if (day == 7) {
						date = date + 2;
					} else
						if (day == 1) {
							date = date + 1;
						}

					c.set(Calendar.DATE, date);
					objEvent.setEventStartDate(sdf.format(c.getTime()));
					objEvent.setRecurrenceInterval(null);
					objEvent.setIsWeekDay(true);
					objEvent.setDays(null);
					objEvent.setDayNumber(null);
				}
			} else
				if ("Weekly".equalsIgnoreCase(objEvent.getRecurrencePatternName())) {
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					Date startDate = new Date(objEvent.getEventStartDate());
					Calendar c = Calendar.getInstance();
					c.setTime(startDate);
					Integer day = c.get(Calendar.DAY_OF_WEEK);
					Boolean dayContain = false;
					Integer occurenceWeek = objEvent.getEveryWeek() - 1;
					for (String days : objEvent.getDays()) {
						if (day.equals(Integer.parseInt(days))) {
							dayContain = true;
						}
					}
					if (!dayContain) {
						Integer selectedDay = 0;
						for (String days : objEvent.getDays()) {
							Integer tempDay = Integer.parseInt(days);
							if ((selectedDay == 0 && day < tempDay) || (day < tempDay && selectedDay > tempDay)) {
								selectedDay = tempDay;
							}
						}
						if (selectedDay > day) {
							Integer temp = selectedDay - day;
							c.set(Calendar.DATE, c.get(Calendar.DATE) + temp);
							objEvent.setEventStartDate(sdf.format(c.getTime()));
						} else {
							String[] selectedDays = objEvent.getDays();
							selectedDay = Integer.parseInt(selectedDays[0]);

							Integer temp = day - selectedDay;
							Integer finalDay = 7 - temp;
							if (occurenceWeek > 0) {
								finalDay = finalDay + (occurenceWeek * 7);
							}
							c.set(Calendar.DATE, c.get(Calendar.DATE) + finalDay);
							objEvent.setEventStartDate(sdf.format(c.getTime()));
						}
					}
					objEvent.setRecurrenceInterval(objEvent.getEveryWeek());
					objEvent.setIsWeekDay(false);
					objEvent.setDayNumber(null);
					LOG.info("EVENT START DATE :" + objEvent.getEventStartDate());
				} else
					if ("Monthly".equalsIgnoreCase(objEvent.getRecurrencePatternName())) {
						if ("date".equalsIgnoreCase(objEvent.getIsOngoingMonthly())) {
							SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
							Date startDate = new Date(objEvent.getEventStartDate());
							Calendar c = Calendar.getInstance();
							c.setTime(startDate);
							Integer strtdate = c.get(Calendar.DATE);
							Integer month = c.get(Calendar.MONTH);
							Integer lastDayOfMonth = c.getActualMaximum(Calendar.DATE);

							Integer comp = strtdate.compareTo(objEvent.getDateOfMonth());

							if (comp != 0) {
								if (comp > 0) {
									Integer tempMonth = month + objEvent.getEveryMonth();
									c.set(Calendar.MONTH, tempMonth);
								}
								if (objEvent.getDateOfMonth() < lastDayOfMonth) {
									c.set(Calendar.DATE, objEvent.getDateOfMonth());
								} else {
									c.set(Calendar.DATE, lastDayOfMonth);
								}
							}
							objEvent.setEventStartDate(sdf.format(c.getTime()));
							objEvent.setRecurrenceInterval(objEvent.getEveryMonth());
							objEvent.setIsWeekDay(false);
							objEvent.setDays(null);
							objEvent.setDayNumber(objEvent.getDateOfMonth());
							LOG.info("EVENT START DATE :" + objEvent.getEventStartDate());
						} else {
							SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
							Integer everyDay = objEvent.getDayNumber();
							String[] everyDayOfMonth = objEvent.getEveryWeekDayMonth();
							Date startDate = new Date(objEvent.getEventStartDate());
							Integer tempWeek = 0;

							Calendar c = Calendar.getInstance();
							c.setTime(startDate);	
							Integer tempDay = c.get(Calendar.DAY_OF_WEEK);
							//Integer tempDay1 = c.get(Calendar.DAY_OF_WEEK);
							Integer date = c.get(Calendar.DATE);
							c.set(Calendar.DATE, 1);
							Integer day = c.get(Calendar.DAY_OF_WEEK);
							//Integer tempDay = day;
							Boolean dayContain = false;
							for (String days : everyDayOfMonth) {
								if (tempDay.equals(Integer.parseInt(days))) {
									dayContain = true;
									break;
								}
							}
							if (!dayContain) {
								Integer length = everyDayOfMonth.length;
								Integer dayLoop = Integer.parseInt(everyDayOfMonth[length - 1]) - tempDay;
								for (int i = 0; i < dayLoop; i++) {
									tempDay = tempDay + 1;
									for (String days : everyDayOfMonth) {
										if (tempDay.equals(Integer.parseInt(days))) {
											dayContain = true;
											break;
										}
									}
									if (dayContain) {
										break;
									}
								}
							}
							if (!dayContain) {								
								tempDay = Integer.parseInt(everyDayOfMonth[0]);
								int numOfWeeksInMonth = c.getActualMaximum(Calendar.WEEK_OF_MONTH);

								if (tempWeek > numOfWeeksInMonth) {
									tempWeek = numOfWeeksInMonth;
								}
								c.set(Calendar.DAY_OF_WEEK, tempDay);
								c.set(Calendar.WEEK_OF_MONTH, everyDay);
							} else {
								if (tempDay < day){
									tempWeek = everyDay + 1;
								} else {
									tempWeek = everyDay;
								}
								int numOfWeeksInMonth = c.getActualMaximum(Calendar.WEEK_OF_MONTH);

								if(tempWeek > numOfWeeksInMonth)
								{
									tempWeek = numOfWeeksInMonth;
								}
								c.set(Calendar.DAY_OF_WEEK, tempDay);
								c.set(Calendar.WEEK_OF_MONTH, tempWeek);
							}

							if (date <= c.get(Calendar.DATE)) {
								objEvent.setEventStartDate(sdf.format(c.getTime()));
							} else {
								tempWeek = everyDay;
								Integer tempMonth = c.get(Calendar.MONTH) + objEvent.getEveryDayMonth();
								c.set(Calendar.MONTH, tempMonth);
								/*c.setTime(startDate);
								c.set(Calendar.MONTH, tempMonth);*/
								//tempDay = c.get(Calendar.DAY_OF_WEEK);
								c.set(Calendar.DATE, 1);
								day = c.get(Calendar.DAY_OF_WEEK);
								tempDay = day;

								for (String days : everyDayOfMonth) {
									if (tempDay.equals(Integer.parseInt(days))) {
										dayContain = true;
										break;
									}
								}

								if (!dayContain) {
									Integer length = everyDayOfMonth.length;
									Integer dayLoop = Integer.parseInt(everyDayOfMonth[length - 1]) - tempDay;
									for (int i = 0; i < dayLoop; i++) {
										tempDay = tempDay + 1;
										for (String days : everyDayOfMonth) {
											if (tempDay.equals(Integer.parseInt(days))) {
												dayContain = true;
												break;
											}
										}
										if (dayContain) {
											break;
										}
									}
								}
								if (!dayContain) {
									tempDay = Integer.parseInt(everyDayOfMonth[0]);
									int numOfWeeksInMonth = c.getActualMaximum(Calendar.WEEK_OF_MONTH);

									if (tempWeek > numOfWeeksInMonth) {
										tempWeek = numOfWeeksInMonth;
									}
									c.set(Calendar.DAY_OF_WEEK, tempDay);
									c.set(Calendar.WEEK_OF_MONTH, everyDay);
								} else {
									if (tempDay < day){
										tempWeek = everyDay + 1;
									} else {
										tempWeek = everyDay;
									}
									int numOfWeeksInMonth = c.getActualMaximum(Calendar.WEEK_OF_MONTH);

									if(tempWeek > numOfWeeksInMonth)
									{
										tempWeek = numOfWeeksInMonth;
									}
									c.set(Calendar.DAY_OF_WEEK, tempDay);
									c.set(Calendar.WEEK_OF_MONTH, tempWeek);
								}

								objEvent.setEventStartDate(sdf.format(c.getTime()));
							}
							objEvent.setRecurrenceInterval(objEvent.getEveryDayMonth());
							objEvent.setIsWeekDay(false);
							objEvent.setDays(objEvent.getEveryWeekDayMonth());
							LOG.info("EVENT START DATE :" + objEvent.getEventStartDate());
						}
					}
			if ("noEndDate".equalsIgnoreCase(objEvent.getOccurenceType())) {
				objEvent.setEventEndDate(null);
				objEvent.setEndAfter(null);
			} else
				if ("endBy".equalsIgnoreCase(objEvent.getOccurenceType())) {
					objEvent.setEndAfter(null);
				} else
					if ("endAfter".equalsIgnoreCase(objEvent.getOccurenceType())) {
						Integer endAfter = objEvent.getEndAfter();
						if ("Daily".equalsIgnoreCase(objEvent.getRecurrencePatternName())) {
							SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
							Date startDate = new Date(objEvent.getEventStartDate());

							Calendar c = Calendar.getInstance();
							c.setTime(startDate);
							if ("days".equalsIgnoreCase(objEvent.getIsOngoingDaily())) {
								Integer finalDay = c.get(Calendar.DATE)
										+ ((endAfter * objEvent.getEveryWeekDay()) - objEvent.getEveryWeekDay());
								c.set(Calendar.DATE, finalDay);
								objEvent.setEventEndDate(sdf.format(c.getTime()));
							} else {
								Integer finalDay = c.get(Calendar.DATE);
								Integer day = c.get(Calendar.DAY_OF_WEEK);

								Integer tempDay = 6 - day;

								if (endAfter > 1) {
									finalDay = finalDay + tempDay + ((endAfter - 1) * 7);
								} else {
									finalDay = finalDay + tempDay;
								}
								c.set(Calendar.DATE, finalDay);
								objEvent.setEventEndDate(sdf.format(c.getTime()));
							}
						} else
							if ("Weekly".equalsIgnoreCase(objEvent.getRecurrencePatternName())) {
								SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
								Date startDate = new Date(objEvent.getEventStartDate());

								Calendar c = Calendar.getInstance();
								c.setTime(startDate);
								Integer finalDay = c.get(Calendar.DATE) + ((endAfter - 1) * (objEvent.getEveryWeek() * 7));
								Integer day = c.get(Calendar.DAY_OF_WEEK);
								Integer i = objEvent.getDays().length;
								String days = objEvent.getDays()[i - 1];
								if (Integer.parseInt(days) > day) {
									int temp = Integer.parseInt(days) - day;
									finalDay = finalDay + temp;
								}
								c.set(Calendar.DATE, finalDay);
								objEvent.setEventEndDate(sdf.format(c.getTime()));
							} else
								if ("Monthly".equalsIgnoreCase(objEvent.getRecurrencePatternName())) {
									SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
									Date startDate = new Date(objEvent.getEventStartDate());

									Calendar c = Calendar.getInstance();
									c.setTime(startDate);
									if ("date".equalsIgnoreCase(objEvent.getIsOngoingMonthly())) {
										c.set(Calendar.MONTH, c.get(Calendar.MONTH) + (endAfter - 1) * objEvent.getEveryMonth());

										Integer lastDayOfMonth = c.getActualMaximum(Calendar.DATE);

										if (objEvent.getDateOfMonth() < lastDayOfMonth) {
											c.set(Calendar.DATE, objEvent.getDateOfMonth());
										} else {
											c.set(Calendar.DATE, lastDayOfMonth);
										}
									} else {
										Integer everyDay = objEvent.getDayNumber();
										String[] everyDayOfMonth = objEvent.getEveryWeekDayMonth();

										c.set(Calendar.MONTH, c.get(Calendar.MONTH) + (endAfter - 1) * objEvent.getEveryDayMonth());
										c.set(Calendar.DATE, 1);
										Integer day = c.get(Calendar.DAY_OF_WEEK);
										Integer weeksLength = everyDayOfMonth.length;
										if (day > Integer.parseInt(everyDayOfMonth[weeksLength - 1])) {
											everyDay = everyDay + 1;
										}
										c.set(Calendar.DAY_OF_WEEK, Integer.parseInt(everyDayOfMonth[weeksLength - 1]));
										Integer lastWeekOfMonth = c.getActualMaximum(Calendar.WEEK_OF_MONTH);
										if (everyDay > lastWeekOfMonth) {
											everyDay = everyDay - 1;
										}
										c.set(Calendar.WEEK_OF_MONTH, everyDay);
									}
									objEvent.setEventEndDate(sdf.format(c.getTime()));
								}
					}
			
				if(null != objEvent.getEventEndDate() && ApplicationConstants.DATEAFTER.equals(Utility.compareDate(objEvent.getEventStartDate(), objEvent.getEventEndDate()))) {
					objEvent.setEventEndDate(objEvent.getEventStartDate());
				}
		} else {
			eventStartTime = objEvent.getEventTimeHrs() + ":" + objEvent.getEventTimeMins();
			objEvent.setEventTime(eventStartTime);
			if(null != objEvent.getEventEDate() && !"".equals(objEvent.getEventEDate()))
			{
				eventEndTime = objEvent.getEventETimeHrs() + ":" + objEvent.getEventETimeMins();
				objEvent.setEventETime(eventEndTime);
			}
			else
			{
				objEvent.setEventETime(null);
				objEvent.setEventEDate(null);
			}
			
		}
		return objEvent;
	}
	
	
	
	/**
	 * This controller method will insert Build/Update Event info by calling
	 * service layer.
	 * 
	 * @param objEvent
	 *            instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.

	 */
	@RequestMapping(value = "/saveevent.htm", method = RequestMethod.POST)
	ModelAndView saveUpdateEvent(@ModelAttribute("addediteventform") Event objEvent, BindingResult result, HttpServletRequest request,
			HttpServletResponse httpresponse, HttpSession session, ModelMap map) throws ScanSeeServiceException {
		LOG.error("Inside EventController : saveUpdateEvent ");
	
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		String response = null;
		String compDate = null;
		Boolean bStartDate = false;
		Boolean bEndDate = false;
		
		session.removeAttribute("responseEventMsg");
		session.removeAttribute("eventMessageFont");
		
		objEvent.setHiddenCategory(objEvent.getEventCategory());
		objEvent.setHiddenState(objEvent.getState());
		objEvent.setHiddenLocationIDs(objEvent.getRetailLocationIDs());
		
		if ("".equals(Utility.checkNull(objEvent.getDays()))) {
			objEvent.setHiddenDay(null);
			objEvent.setHiddenDays(null);
		} else {
			StringBuffer strDays = new StringBuffer();
			if (!"".equals(Utility.checkNull(objEvent.getDays()))) {
				for (String s : objEvent.getDays()) {
					strDays.append(s);
				}
				objEvent.setHiddenDay(strDays.toString());
				objEvent.setHiddenDays(strDays.toString());
			}
		}
		
		
		Date objCurrentDate = new Date();
		//final Users loginUser = (Users) session.getAttribute("loginuser");
		
		final Users loginUser = (Users) session.getAttribute("loginuser");
		objEvent.setRetailID(loginUser.getRetailerId());

		try {

			eventValidator.validate(objEvent, result);

			if (result.hasErrors()) {
				//map.put("addediteventform", objEvent);
				return new ModelAndView(objEvent.getViewName());
			} else {
				if ("yes".equalsIgnoreCase(objEvent.getIsOngoing())) {

					if (!"".equals(Utility.checkNull(objEvent.getEventStartDate()))) {
						bStartDate = Utility.isValidDate(objEvent.getEventStartDate());

						if (bStartDate && (null == objEvent.getEventID() || "".equals(objEvent.getEventID()))) {
								compDate = Utility.compareCurrentDate(objEvent.getEventStartDate(), objCurrentDate);
								if (null != compDate) {
									eventValidator.validateDates(objEvent, result, ApplicationConstants.DATESTARTCURRENT);
								}
						} else if (!bStartDate) {
								eventValidator.validateDates(objEvent, result, ApplicationConstants.VALIDSTARTDATE);
						}
					}
					if (!"".equals(Utility.checkNull(objEvent.getEventEndDate()))) {
						
						bEndDate = Utility.isValidDate(objEvent.getEventEndDate());
						bStartDate = Utility.isValidDate(objEvent.getEventStartDate());

						if ("endBy".equalsIgnoreCase(objEvent.getOccurenceType())) {
							if (bEndDate) {
								if (bStartDate) {
									compDate = Utility.compareDate(objEvent.getEventStartDate(), objEvent.getEventEndDate());
									if (null != compDate) {
										eventValidator.validateDates(objEvent, result, ApplicationConstants.DATEAFTER);
									}
								} else {
									compDate = Utility.compareCurrentDate(objEvent.getEventEndDate(), objCurrentDate);
									if (null != compDate) {
										eventValidator.validateDates(objEvent, result, ApplicationConstants.DATEENDCURRENT);
									}
								}
							} else {
								eventValidator.validateDates(objEvent, result, ApplicationConstants.VALIDENDDATE);
							}
						}
					}
				} else {
					
					bStartDate = Utility.isValidDate(objEvent.getEventDate());
					bEndDate = Utility.isValidDate(objEvent.getEventEDate());
					
					if (!"".equals(Utility.checkNull(objEvent.getEventDate()))) {
						if (bStartDate && null == objEvent.getEventID() || "".equals(objEvent.getEventID())) {
							compDate = Utility.compareCurrentDate(objEvent.getEventDate(), objCurrentDate);
							if (null != compDate) {
								eventValidator.validateDates(objEvent, result, ApplicationConstants.DATENOGSTARTCURRENT);
							}
						} else
							if (!bStartDate) {
								eventValidator.validateDates(objEvent, result, ApplicationConstants.VALIDDATE);
							}
					}
					if (bEndDate) {
						if (bStartDate) {
							compDate = Utility.compareDate(objEvent.getEventDate(), objEvent.getEventEDate());
							if (null != compDate) {
								eventValidator.validateDates(objEvent, result, ApplicationConstants.DATENOGAFTER);
							}
						} else {
							compDate = Utility.compareCurrentDate(objEvent.getEventEDate(), objCurrentDate);
							if (null != compDate) {
								eventValidator.validateDates(objEvent, result, ApplicationConstants.DATENOGENDCURRENT);
							}
						}

					} else
						if (!"".equals(objEvent.getEventEDate())) {
							eventValidator.validateDates(objEvent, result, ApplicationConstants.VALIDEDATE);
						}
				}

				if (!"".equals(Utility.checkNull(objEvent.getMoreInfoURL()))) {
					if (!Utility.validateURL(objEvent.getMoreInfoURL())) {
						eventValidator.validate(objEvent, result, ApplicationConstants.INVALIDEVENTURL);
					}
				}

				if (result.hasErrors()) {
					map.put("addediteventform", objEvent);
					return new ModelAndView(objEvent.getViewName());
				}

				objEvent = eventRecurrenceDetails(objEvent);				
				
				response = retailerService.addUpdateEventDetail(objEvent);

				if (null != response && !"".equals(response)) {
					if (response.equals(ApplicationConstants.GEOERROR)) {
						if (objEvent.isGeoError()) {
							if (null == objEvent.getLatitude()) {
								eventValidator.validate(objEvent, result, ApplicationConstants.LATITUDE);
							}

							if (null == objEvent.getLongitude()) {
								eventValidator.validate(objEvent, result, ApplicationConstants.LONGITUDE);
							}
						} else {
							eventValidator.validate(objEvent, result, ApplicationConstants.GEOERROR);
							request.setAttribute("GEOERROR", ApplicationConstants.GEOERROR);
							objEvent.setLatitude(null);
							objEvent.setLongitude(null);
							objEvent.setGeoError(true);
						}

						if (result.hasErrors()) {
							map.put("addediteventform", objEvent);
							return new ModelAndView(objEvent.getViewName());
						}
					} else {
						if (response.equals(ApplicationConstants.SUCCESS)) {
							if (null == objEvent.getEventID() || "".equals(objEvent.getEventID())) {
								session.setAttribute("eventMessageFont", "font-weight:bold;color:#00559c;");
								session.setAttribute("responseEventMsg", "Event Created Succesfully");
							} else {
								session.setAttribute("eventMessageFont", "font-weight:bold;color:#00559c;");
								session.setAttribute("responseEventMsg", "Event Updated Succesfully");
							}
							return new ModelAndView(new RedirectView("manageevents.htm"));
						}
					}
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside EventController : saveUpdateEvent : " + e.getMessage());
			throw e;
		}
		return new ModelAndView(objEvent.getViewName());
	}
}

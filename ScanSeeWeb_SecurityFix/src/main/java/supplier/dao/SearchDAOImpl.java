package supplier.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.query.RateReviewQueries;
import shopper.query.ShoppingListQueries;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.common.pojos.ExternalAPISearchParameters;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.HotDealInfo;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetail;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductReview;
import common.util.Utility;

public class SearchDAOImpl implements SearchDAO
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SearchDAOImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public ArrayList<ProductVO> fetchAllProduct(String zipcode, String productName, int lowerLimit, String screenName)
	{
		final String methodName = "fetchAllProduct";
		// log.info(ApplicationConstants.METHODSTART + methodName);

		// ProductVO product = null;
		ArrayList<ProductVO> productList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSearchProductName");
			simpleJdbcCall.returningResultSet("productLists", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("ZIPCode", zipcode);
			fetchProductDetailsParameters.addValue("ProductName", productName);
			fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
			fetchProductDetailsParameters.addValue("ScreenName", screenName);
			fetchProductDetailsParameters.addValue("Radius", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			productList = (ArrayList<ProductVO>) resultFromProcedure.get("productLists");

		}
		catch (DataAccessException e)
		{
			// log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
			// e);
			// throw new ScanSeeException(e.getMessage());
			e.printStackTrace();
		}
		// log.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("usp_WebSearchProductName is  executed Successfully.");
		return productList;
	}

	public ProductVO fetchProductInfo(Integer retailLocationID, Integer productID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductInfo";

		LOG.info("In DAO...." + methodName);
		LOG.info("retailLocationID...." + retailLocationID);
		LOG.info("retailLocationID...." + productID);

		ProductVO productInfo = null;
		List<ProductVO> productInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductDetails");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("ProductID", productID);
			fetchProductInfoParameters.addValue("RetailLocationID", retailLocationID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);

			productInfoList = (ArrayList<ProductVO>) resultFromProcedure.get("ProductInfo");
			if (null != productInfoList && !productInfoList.isEmpty())
			{
				productInfo = new ProductVO();
				productInfo = productInfoList.get(0);
			}
			LOG.info("usp_WebDisplayProductDetails is  executed Successfully.");
		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}
		if (null != productInfo)
		{
			return productInfo;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return productInfo;
	}

	public SearchResultInfo fetchAllProduct(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllProduct";
		// log.info(ApplicationConstants.METHODSTART + methodName);

		// ProductVO product = null;
		SearchResultInfo searchInfo = null;
		int userType = 0;
		MapSqlParameterSource fetchProductDetailsParameters = null;
		try
		{
			searchInfo = new SearchResultInfo();
			if (loginUser != null)
			{
				userType = loginUser.getUserType();
			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			if (userType == 0)
			{
				simpleJdbcCall.withProcedureName("usp_WebSearchProductName");
				fetchProductDetailsParameters = new MapSqlParameterSource();

				if (objForm.getZipCode().equals(""))
				{
					fetchProductDetailsParameters.addValue("ZIPCode", null);

				}
				else
				{

					fetchProductDetailsParameters.addValue("ZIPCode", objForm.getZipCode());
				}

				fetchProductDetailsParameters.addValue("ProductName", objForm.getSearchKey());
				fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
				fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
				fetchProductDetailsParameters.addValue("Radius", objForm.getRadius());
			}
			else
			{
				simpleJdbcCall.withProcedureName("usp_WebProductSearchRetailerOrSupplier");
				fetchProductDetailsParameters = new MapSqlParameterSource();
				// fetchProductDetailsParameters.addValue("UserID",
				// loginUser.getUserID());
				fetchProductDetailsParameters.addValue("UserID", loginUser.getUserID());
				fetchProductDetailsParameters.addValue("SearchParameter", objForm.getSearchKey());
				fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
				fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
			}
			simpleJdbcCall.returningResultSet("productLists", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			/*
			 * if (resultFromProcedure != null) { final Integer errorNum =
			 * (Integer)
			 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final
			 * String errorMsg = (String)
			 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); if
			 * (null == errorNum) {
			 */
			List<ProductVO> prodList = (List<ProductVO>) resultFromProcedure.get("productLists");
			/*
			 * if (null != prodList && !prodList.isEmpty()) {
			 */
			int totalSize = (Integer) resultFromProcedure.get("RowCount");
			searchInfo.setTotalSize(totalSize);
			/* } */
			searchInfo.setProductList(prodList);
			/*
			 * } else {
			 * LOG.info("Error Occured in registerUser method ..errorNum..." +
			 * errorNum + "errorMsg  .." + errorMsg); throw new
			 * ScanSeeWebSqlException(errorMsg); } }
			 */

		}
		catch (DataAccessException exception)
		{
			LOG.error(exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}
		LOG.info("usp_WebProductSearchRetailerOrSupplier/usp_WebSearchProductName are  executed Successfully.");
		return searchInfo;
	}

	public SearchResultInfo searchHotDeals(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		SearchResultInfo searchInfo = null;
		int userType = 0;
		MapSqlParameterSource fetchProductDetailsParameters = null;
		try
		{
			searchInfo = new SearchResultInfo();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHotDealSearch");
			fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("ZIPCode", objForm.getZipCode());
			fetchProductDetailsParameters.addValue("HotDealName", objForm.getSearchKey());
			fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
			fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
			fetchProductDetailsParameters.addValue("Radius", null);
			simpleJdbcCall.returningResultSet("dealList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (resultFromProcedure != null)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum)
				{
					List<HotDealInfo> dealList = (List<HotDealInfo>) resultFromProcedure.get("dealList");
					if (null != dealList && !dealList.isEmpty())
					{
						int totalSize = (Integer) resultFromProcedure.get("RowCount");
						searchInfo.setTotalSize(totalSize);
					}
					searchInfo.setDealList(dealList);

				}
				else
				{
					LOG.info("Error Occured in registerUser method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebHotDealSearch is  executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error(exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}

		return searchInfo;
	}

	public SearchResultInfo getProductWithDeal(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException
	{
		final String methodName = "getProductWithDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo searchInfo = null;
		int userType = 0;
		MapSqlParameterSource fetchProductDetailsParameters = null;

		if (Utility.checkNull(objForm.getZipCode()).equals(""))
		{
			objForm.setZipCode(null);
		}
		try
		{
			searchInfo = new SearchResultInfo();
			if (loginUser != null)
			{
				userType = loginUser.getUserType();
			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			if (userType == 0)
			{
				simpleJdbcCall.withProcedureName("usp_WebSearchProductWithDeal");
				fetchProductDetailsParameters = new MapSqlParameterSource();
				fetchProductDetailsParameters.addValue("ZIPCode", objForm.getZipCode());
				fetchProductDetailsParameters.addValue("ProductName", objForm.getSearchKey());
				fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
				fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
				fetchProductDetailsParameters.addValue("Radius", null);
			}
			else
			{
				simpleJdbcCall.withProcedureName("usp_WebProductSearchRetailerOrSupplier");
				fetchProductDetailsParameters = new MapSqlParameterSource();
				fetchProductDetailsParameters.addValue("UserID", loginUser.getUserID());
				fetchProductDetailsParameters.addValue("SearchParameter", objForm.getSearchKey());
				fetchProductDetailsParameters.addValue("LowerLimit", 0);
				fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
			}
			simpleJdbcCall.returningResultSet("productLists", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (resultFromProcedure != null)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum)
				{
					List<ProductVO> prodList = (List<ProductVO>) resultFromProcedure.get("productLists");
					if (null != prodList && !prodList.isEmpty())
					{
						int totalSize = (Integer) resultFromProcedure.get("RowCount");
						searchInfo.setTotalSize(totalSize);
					}
					searchInfo.setProductList(prodList);

				}
				else
				{
					LOG.info("Error Occured in registerUser method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebSearchProductWithDeal/usp_WebProductSearchRetailerOrSupplier are  executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error(exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}

		return searchInfo;
	}

	public ProductVO fetchProductInfoWithDeals(Integer productID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductInfo";

		LOG.info("In DAO...." + methodName);

		LOG.info("retailLocationID...." + productID);

		ProductVO productInfo = null;
		List<ProductVO> productInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductInfo");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("ProductID", productID);
			// fetchProductInfoParameters.addValue("RetailLocationID",
			// retailLocationID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);

			productInfoList = (ArrayList<ProductVO>) resultFromProcedure.get("ProductInfo");
			if (null != productInfoList && !productInfoList.isEmpty())
			{
				productInfo = new ProductVO();
				productInfo = productInfoList.get(0);
			}
			LOG.info("usp_WebDisplayProductInfo is  executed Successfully.");
		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}
		if (null != productInfo)
		{
			return productInfo;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return productInfo;
	}

	public HotDealInfo fetchDealsInfo(int hotDealID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductInfo";

		LOG.info("In DAO...." + methodName);

		LOG.info("hotDealID...." + hotDealID);

		HotDealInfo hotDealInfo = null;
		List<HotDealInfo> productInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayHotDealDetails");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("HotDealID", hotDealID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);
			productInfoList = (ArrayList<HotDealInfo>) resultFromProcedure.get("ProductInfo");
			if (null != productInfoList && !productInfoList.isEmpty())
			{
				hotDealInfo = new HotDealInfo();
				hotDealInfo = productInfoList.get(0);
			}
			LOG.info("usp_WebDisplayHotDealDetails is  executed Successfully.");
		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}
		if (null != hotDealInfo)
		{
			return hotDealInfo;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return hotDealInfo;
	}

	public List<ProductVO> fetchProductAssociatedWithDeals(int productID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllProduct";

		List<ProductVO> prodAssociatedHotDealList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductAssociatedHotDeals");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("ProductID", productID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);

			prodAssociatedHotDealList = (ArrayList<ProductVO>) resultFromProcedure.get("ProductInfo");

		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("usp_WebDisplayProductAssociatedHotDeals is  executed Successfully.");
		return prodAssociatedHotDealList;

	}

	public List<ProductVO> fetchDealWithProductAssociated(Integer hotDealID) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchDealWithProductAssociated";

		List<ProductVO> prodAssociatedHotDealList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayHotDealProducts");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("ProductHotDealID", hotDealID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);

			prodAssociatedHotDealList = (ArrayList<ProductVO>) resultFromProcedure.get("ProductInfo");

		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}
		LOG.info("usp_WebDisplayHotDealProducts is  executed Successfully.");
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return prodAssociatedHotDealList;

	}

	/**
	 * method for fetching FindNearBy Info.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param latitude
	 *            The latitude in request.
	 * @param productId
	 *            The product Id in request.
	 * @param longitude
	 *            The longitude in request.
	 * @param postalCode
	 *            The postal code for which lowest price information is to be
	 * @param radius
	 *            The radius in request
	 * @return List of type FindNearByDetail objects. The method is called from
	 *         the service layer.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 */

	@SuppressWarnings("unchecked")
	public FindNearByDetails fetchNearByInfo(AreaInfoVO objAreaInfoVO) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchNearByInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		// Temperory fix, after integrating we will remove this code
		Long userID = objAreaInfoVO.getUserID();
		Integer productId = objAreaInfoVO.getProductId();
		Integer radius = objAreaInfoVO.getRadius();
		Double latitude = objAreaInfoVO.getLattitude();
		Double longitude = objAreaInfoVO.getLongitude();
		String postalcode = objAreaInfoVO.getZipCode();
		FindNearByDetails findNearByDetails = null;
		List<FindNearByDetail> findNearByDetaillst = null;
		Integer dbresult = 0;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_FindNearByRetailer");
			simpleJdbcCall.returningResultSet("SLfetchNearByInfo", new BeanPropertyRowMapper<FindNearByDetail>(FindNearByDetail.class));

			final SqlParameterSource fetchNearByParameters = new MapSqlParameterSource().addValue("UserID", userID).addValue("ProductId", productId)
					.addValue("Latitude", latitude).addValue("Longitude", longitude).addValue("PostalCode", postalcode).addValue("Radius", radius);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchNearByParameters);

			if (null != resultFromProcedure)
			{
				dbresult = (Integer) resultFromProcedure.get("Result");
				if (dbresult != null)
				{
					if (dbresult == -1)
					{
						findNearByDetails = new FindNearByDetails();
						findNearByDetails.setFindNearBy(true);
						return findNearByDetails;
					}
				}
				findNearByDetaillst = (List<FindNearByDetail>) resultFromProcedure.get("SLfetchNearByInfo");
				if (findNearByDetaillst != null && !findNearByDetaillst.isEmpty())
				{
					findNearByDetails = new FindNearByDetails();
					findNearByDetails.setFindNearByDetail(findNearByDetaillst);
				}
			}
			LOG.info("usp_FindNearByRetailer is  executed Successfully.");
		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return findNearByDetails;
	}

	public ExternalAPIInformation getExternalApiInfo(String moduleName) throws ScanSeeWebSqlException
	{
		LOG.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;

		ExternalAPIInformation exteApi = null;

		List<ExternalAPIVendor> vendorList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIListOnline");
			simpleJdbcCall.returningResultSet("vendorList", new BeanPropertyRowMapper<ExternalAPIVendor>(ExternalAPIVendor.class));

			final MapSqlParameterSource vendorParameter = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			resultFromProcedure = simpleJdbcCall.execute(vendorParameter);
			vendorList = (ArrayList) resultFromProcedure.get("vendorList");

			ArrayList<ExternalAPISearchParameters> externalAPIInputParameters = null;

			final HashMap<Integer, ArrayList<ExternalAPISearchParameters>> apiParamMap = new HashMap<Integer, ArrayList<ExternalAPISearchParameters>>();

			exteApi = new ExternalAPIInformation();
			exteApi.setVendorList((ArrayList<ExternalAPIVendor>) vendorList);
			if ("FindOnlineStores".equals(moduleName))
			{
				for (ExternalAPIVendor obj : vendorList)
				{
					externalAPIInputParameters = getApiparams(Integer.valueOf(obj.getApiUsageID()), "FindOnlineStores");
					apiParamMap.put(Integer.valueOf(obj.getApiUsageID()), externalAPIInputParameters);
				}

			}

			exteApi.setSerchParameters(apiParamMap);

			// log.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" +
			// arUniversityList.size());
			LOG.info("usp_GetAPIListOnline is  executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.info("Exception occurred in getAllUniversity" + exception);
		}
		return exteApi;
	}

	private ArrayList<ExternalAPISearchParameters> getApiparams(int id, String moduleName) throws ScanSeeWebSqlException
	{
		LOG.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;

		ArrayList<ExternalAPISearchParameters> paramList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIInputParameters");
			simpleJdbcCall.returningResultSet("aipInputList", new BeanPropertyRowMapper<ExternalAPISearchParameters>(
					ExternalAPISearchParameters.class));

			final MapSqlParameterSource APISearchParameters = new MapSqlParameterSource().addValue("prAPIUsageID", id).addValue("PrAPISubModuleName",
					moduleName);
			resultFromProcedure = simpleJdbcCall.execute(APISearchParameters);
			paramList = (ArrayList) resultFromProcedure.get("aipInputList");

			LOG.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" + paramList.size());
		}
		catch (DataAccessException exception)
		{
			LOG.info("Exception occurred in getAllUniversity" + exception);
		}
		LOG.info("usp_GetAPIInputParameters is  executed Successfully.");
		return paramList;
	}

	public ProductDetail getProductDetails(String productId) throws ScanSeeWebSqlException
	{

		ProductDetail prdDetail = null;

		simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
		prdDetail = simpleJdbcTemplate.queryForObject(ShoppingListQueries.ONLINESTOREPRODUCTINFOQUERY, new BeanPropertyRowMapper<ProductDetail>(
				ProductDetail.class), productId);
		if (prdDetail != null)
		{
			return prdDetail;
		}

		return prdDetail;
	}

	/**
	 * The DAO method for fetching product reviews.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return ProductReview list.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ArrayList<ProductReview> getProductReviews(Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "getProductReviews in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductReview> productReviewslist = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			productReviewslist = (ArrayList<ProductReview>) simpleJdbcTemplate.query(RateReviewQueries.PRODUCTREVIEWS,
					new BeanPropertyRowMapper<ProductReview>(ProductReview.class), productId);

		}

		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productReviewslist;
	}

}
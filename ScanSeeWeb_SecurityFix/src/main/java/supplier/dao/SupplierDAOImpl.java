package supplier.dao;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AccountType;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.ContactType;
import common.pojo.DropDown;
import common.pojo.HotDealInfo;
import common.pojo.ManageProducts;
import common.pojo.PlanInfo;
import common.pojo.Product;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfoVO;
import common.pojo.Rebates;
import common.pojo.Retailer;
import common.pojo.RetailerInfo;
import common.pojo.SearchResultInfo;
import common.pojo.SearchZipCode;
import common.pojo.State;
import common.pojo.SupplierProfile;
import common.pojo.SupplierQRCodeData;
import common.pojo.SupplierQRCodeResponse;
import common.pojo.SupplierRegistrationInfo;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.EncryptDecryptPwd;
import common.util.Utility;

/**
 * SupplierDAOImpl implements supplierDao methods.
 * 
 * @author Created by SPAN.
 */
public class SupplierDAOImpl implements SupplierDAO
{
	
	/**
	 * Variable YEARLY declared as String.
	 */
	private static final String YEARLY = "yearly";

	/**
	 * Variable MONTHLY declared as String.
	 */
	private static final String MONTHLY = "monthly";

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(SupplierDAOImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public final void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


	public Users loginAuthentication(String userName, String password) throws ScanSeeWebSqlException
	{
		final String methodName = "loginAuthentication";
		LOG.info("In DAO...." + ApplicationConstants.METHODSTART + methodName);
		EncryptDecryptPwd enryptDecryptpwd;
		String encryptedpwd = null;
		Users userInfo = null;
		List<Users> userInfoLst = null;
		try
		{
			enryptDecryptpwd = new EncryptDecryptPwd();
			encryptedpwd = enryptDecryptpwd.encrypt(password);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebLogin");

			simpleJdbcCall.returningResultSet("userInfo", new BeanPropertyRowMapper<Users>(Users.class));
			final MapSqlParameterSource loginCredParams = new MapSqlParameterSource();
			loginCredParams.addValue("UserName", userName);
			loginCredParams.addValue("Password", encryptedpwd);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(loginCredParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("Status"))
				{

					LOG.info("Login UnSuccessfull");
					userInfo = new Users();
					userInfo.setLoginSuccess(false);

				}
				else
				{
					LOG.info("Login Successfull");
					userInfo = new Users();
					userInfoLst = (ArrayList<Users>) resultFromProcedure.get("userInfo");
					if (!userInfoLst.isEmpty() || userInfoLst != null)
					{
						userInfo = userInfoLst.get(0);
						userInfo.setLoginSuccess(true);
						int userType = (Integer) resultFromProcedure.get("UserType");
						String pageForwardValue = (String) resultFromProcedure.get("LandingPage");
						Boolean resetPassword = (Boolean) resultFromProcedure.get("ResetPassword");
						Boolean bEventActive = (Boolean) resultFromProcedure.get("EventActiveFlag");
						userInfo.setEventActive(bEventActive);
						// Check if reset password is null then set the reset
						// password to true to login (previously reset password
						// has not set)
						if (null == resetPassword)
						{
							userInfo.setResetPassword(true);
						}
						else
						{
							userInfo.setResetPassword(resetPassword);
						}
						userInfo.setUserType(userType);
						userInfo.setPageForwardValue(pageForwardValue);
					}

				}
			}
			LOG.info("usp_WebLogin executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("", exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error("", e);
			throw new ScanSeeWebSqlException(e.getMessage());

		}
		catch (NoSuchPaddingException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidKeyException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidAlgorithmParameterException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidKeySpecException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (IllegalBlockSizeException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (BadPaddingException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		LOG.info("In DAO...." + ApplicationConstants.METHODEND + methodName);
		return userInfo;
	}

	/**
	 * This method insert the supplier registration details.
	 * 
	 * @param supplierRegistrationInfoObj
	 *            instance of supplierRegistration.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String supplierRegistration(SupplierRegistrationInfo supplierRegistrationInfoObj) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : supplierRegistration ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		EncryptDecryptPwd enryptDecryptpwd;
		boolean isDuplicateUser;
		String enryptPassword = null;
		boolean isDuplicateSupplierName;
		String autogenPassword = null;
		try
		{
			enryptDecryptpwd = new EncryptDecryptPwd();
			// getting auto generator password and converting into encrypt
			autogenPassword = Utility.randomString(5);
			enryptPassword = enryptDecryptpwd.encrypt(autogenPassword);
			supplierRegistrationInfoObj.setPassword(autogenPassword);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierCreation");
			final MapSqlParameterSource supplierRegistrationParameter = new MapSqlParameterSource();

			supplierRegistrationParameter.addValue("AdminFlag", 1);
			supplierRegistrationParameter.addValue("SupplierName", supplierRegistrationInfoObj.getSupplierName());
			supplierRegistrationParameter.addValue("CorporateAddress", supplierRegistrationInfoObj.getCorpAdderess());
			supplierRegistrationParameter.addValue("Address2", supplierRegistrationInfoObj.getAddress2());
			supplierRegistrationParameter.addValue("CityID", supplierRegistrationInfoObj.getCity());
			supplierRegistrationParameter.addValue("StateAbbreviation", supplierRegistrationInfoObj.getState());
			supplierRegistrationParameter.addValue("PostalCode", supplierRegistrationInfoObj.getPostalCode());
			supplierRegistrationParameter.addValue("CorporatePhoneNumber", Utility.removePhoneFormate(supplierRegistrationInfoObj.getCorpPhoneNo()));
			supplierRegistrationParameter.addValue("LegalAuthorityFirstName", supplierRegistrationInfoObj.getLegalAuthorityFName());
			supplierRegistrationParameter.addValue("LegalAuthorityLastName", supplierRegistrationInfoObj.getLegalAuthorityLName());
			supplierRegistrationParameter.addValue("ContactFirstName", supplierRegistrationInfoObj.getContactFName());
			supplierRegistrationParameter.addValue("ContactLastName", supplierRegistrationInfoObj.getContactLName());
			supplierRegistrationParameter.addValue("ContactPhoneNumber", Utility.removePhoneFormate(supplierRegistrationInfoObj.getContactPhoneNo()));
			supplierRegistrationParameter.addValue("ContactEmail", supplierRegistrationInfoObj.getContactEmail());
			// sending the encrypted autogen password to save in database
			supplierRegistrationParameter.addValue("Password", enryptPassword);

			supplierRegistrationParameter.addValue("BusinessCategory", supplierRegistrationInfoObj.getbCategory());
			supplierRegistrationParameter.addValue("NonProfitStatus", supplierRegistrationInfoObj.isNonProfit());
			supplierRegistrationParameter.addValue("UserName", supplierRegistrationInfoObj.getUserName());

			resultFromProcedure = simpleJdbcCall.execute(supplierRegistrationParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				isDuplicateUser = (Boolean) resultFromProcedure.get("DuplicateFlag");
				isDuplicateSupplierName = (Boolean) resultFromProcedure.get("DuplicateSupplierFlag");
				if (isDuplicateUser)
				{
					LOG.info("Inside SupplierDAOImpl : supplierRegistration : isDuplicateUser :" + isDuplicateUser);
					response = ApplicationConstants.DUPLICATE_USER;
				}
				else if (isDuplicateSupplierName)
				{

					response = ApplicationConstants.DUPLICATE_SUPPLIERNAME;
					LOG.info("Inside SupplierDAOImpl : addRetailerRegistration : Return response message : " + response);
				}
				else
				{
					response = ApplicationConstants.SUCCESS;
				}
			}
			else
			{
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside SupplierDAOImpl : supplierRegistration : Error occurred in  Store Procedure with error number: " + errorNum
						+ " and error message: " + errorMsg);
			}
			LOG.info("usp_WebSupplierCreation executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : supplierRegistration :" + exception);
			throw new ScanSeeWebSqlException();
		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error("Inside SupplierDAOImpl : supplierRegistration :" + e);
			e.printStackTrace();
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error("Inside SupplierDAOImpl : supplierRegistration :" + e);
			e.printStackTrace();
		}
		catch (InvalidKeyException e)
		{
			LOG.error("Inside SupplierDAOImpl : supplierRegistration :" + e);
			e.printStackTrace();
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error("Inside SupplierDAOImpl : supplierRegistration :" + e);
			e.printStackTrace();
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error("Inside SupplierDAOImpl : supplierRegistration :" + e);
			e.printStackTrace();
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error("Inside SupplierDAOImpl : supplierRegistration :" + e);
			e.printStackTrace();
		}
		catch (BadPaddingException e)
		{
			LOG.error("Inside SupplierDAOImpl : supplierRegistration :" + e);
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * This method Add the Rebates details into database.
	 * 
	 * @param objRebates
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeWebSqlException
	 */

	public String addRebates(Rebates objRebates) throws ScanSeeWebSqlException
	{

		final String methodName = "addRebates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierAddRebate");
			final MapSqlParameterSource objAddRebParameter = new MapSqlParameterSource();

			objAddRebParameter.addValue("SupplierID", objRebates.getSupplierId());
			objAddRebParameter.addValue("UserID", objRebates.getUserID());
			objAddRebParameter.addValue("RebateName", objRebates.getRebName());
			objAddRebParameter.addValue("RebateAmount", objRebates.getRebAmount());
			objAddRebParameter.addValue("RebateDescription", objRebates.getRebDescription());
			objAddRebParameter.addValue("RebateTermsAndCondition", objRebates.getRebTermCondtn());
			objAddRebParameter.addValue("RebateStartDate", objRebates.getRebStartDate());
			objAddRebParameter.addValue("RebateEndDate", objRebates.getRebEndDate());
			objAddRebParameter.addValue("RebateStartTime", objRebates.getRebStartTime());
			objAddRebParameter.addValue("RebateEndTime", objRebates.getRebEndTime());
			objAddRebParameter.addValue("RetailerID", objRebates.getRetailID());
			objAddRebParameter.addValue("RetailerLocationID", objRebates.getRetailerLocID());
			objAddRebParameter.addValue("NoOfRebatesIssued", objRebates.getNoOfrebatesIsued());
			objAddRebParameter.addValue("ProductID", objRebates.getProductID());

			if (objRebates.getRebateTimeZoneId() == 0)
			{
				objAddRebParameter.addValue("RebateTimeZoneID", null);
			}
			else
			{
				objAddRebParameter.addValue("RebateTimeZoneID", objRebates.getRebateTimeZoneId());
			}
			resultFromProcedure = simpleJdbcCall.execute(objAddRebParameter);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{

				response = ApplicationConstants.SUCCESS;
				LOG.info("the responseFromProc is :" + responseFromProc);

			}
			else
			{

				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : addRebates  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebSupplierAddRebate executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : addRebates : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method Updates the Rebates details into database.
	 * 
	 * @param objRebates
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeWebSqlException
	 */
	public String updateRebates(Rebates objRebates) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : updateRebates ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierRebateUpdation");
			final MapSqlParameterSource objUpdateRebParameter = new MapSqlParameterSource();

			objUpdateRebParameter.addValue("RebateID", objRebates.getRebateID());
			// System.out.println( objRebates.getRebateID());
			objUpdateRebParameter.addValue("RebateName", objRebates.getRebName());
			objUpdateRebParameter.addValue("Description", objRebates.getRebDescription());
			objUpdateRebParameter.addValue("TermsAndConditions", objRebates.getRebTermCondtn());
			objUpdateRebParameter.addValue("RebateAmount", objRebates.getRebAmount());
			objUpdateRebParameter.addValue("RebateStartDate", objRebates.getRebStartDate());
			objUpdateRebParameter.addValue("RebateEndDate", objRebates.getRebEndDate());
			objUpdateRebParameter.addValue("RebateStartTime",
					String.valueOf(objRebates.getRebStartHrs()) + ":" + String.valueOf(objRebates.getRebStartMins()));
			objUpdateRebParameter.addValue("RebateEndTime",
					String.valueOf(objRebates.getRebEndhrs()) + ":" + String.valueOf(objRebates.getRebEndMins()));
			// objUpdateRebParameter.addValue("RebateStartTime",objRebates.getRebStartTime());
			// objUpdateRebParameter.addValue("RebateEndTime",objRebates.getRebEndTime());
			objUpdateRebParameter.addValue("RetailerID", objRebates.getRetailID());

			objUpdateRebParameter.addValue("RetailerLocationID", objRebates.getRetailerLocID());
			// System.out.println("time hrs"+String.valueOf(objRebates.getRebStartHrs()));

			if (objRebates.getRebateTimeZoneId() == 0)
			{
				objUpdateRebParameter.addValue("RebateTimeZoneID", null);
			}
			else
			{
				objUpdateRebParameter.addValue("RebateTimeZoneID", objRebates.getRebateTimeZoneId());
			}

			resultFromProcedure = simpleJdbcCall.execute(objUpdateRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
				LOG.info("the responseFromProc is :" + responseFromProc);
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : updateRebates  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebSupplierRebateUpdation executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : updateRebates : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	/**
	 * This method will return the RebatesNames List based on parameter. This
	 * method will return the product Names List based on parameter not yet
	 * implement
	 * 
	 * @param strRebateName
	 * @return Rebates Names or Product Names List based on parameter.
	 * @throws ScanSeeWebSqlException
	 */
	public List<Rebates> getRebatesByRebateName(String strRebName) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getRebatesByRebateName ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<Rebates> listRebNames = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRebateSearchSupplier");
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RebateName", strRebName);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listRebNames = (ArrayList) resultFromProcedure.get("searchList");
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + listRebNames.size());
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : getRebatesByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebRebateSearchSupplier executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		return listRebNames;
	}

	/**
	 * This method will return the RebatesNames List based on parameter. This
	 * method will return the product Names List based on parameter not yet
	 * implement
	 * 
	 * @param strRebateName
	 * @return Rebates Names or Product Names List based on parameter.
	 * @throws ScanSeeWebSqlException
	 */
	public SearchResultInfo getRebatesAll(Long userId, String strRebName, int lowerLimit) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getRebatesAll ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<Rebates> listRebNames = null;
		SearchResultInfo searchresult = null;
		Integer rowcount = null;
		searchresult = new SearchResultInfo();
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRebateSearchSupplier");
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();

			objSearchRebParameter.addValue("ManufacturerID", userId);
			objSearchRebParameter.addValue("RebateName", strRebName);
			objSearchRebParameter.addValue("LowerLimit", lowerLimit);

			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listRebNames = (ArrayList) resultFromProcedure.get("searchList");
			if (listRebNames != null && !listRebNames.isEmpty())
			{
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setRebatesList(listRebNames);
				searchresult.setTotalSize(rowcount);
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetailerDAOImpl : getRetailerByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				rowcount = (Integer) resultFromProcedure.get("RowCount");
				searchresult.setTotalSize(0);
			}
			LOG.info("usp_WebRebateSearchSupplier executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return searchresult;
	}

	/**
	 * This method Add UpdateProfile details into database.
	 * 
	 * @param objSupProfile
	 *            instance of SupplierProfile.
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String addUpdateProfile(SupplierProfile objSupProfile) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : addUpdateProfile ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Boolean isDuplicateSupplierName;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierProfileUpdation");
			final MapSqlParameterSource objUpdateProfileParameter = new MapSqlParameterSource();
			objUpdateProfileParameter.addValue("ManufacturerID", objSupProfile.getSupProfileID());
			objUpdateProfileParameter.addValue("UserID", objSupProfile.getUserID());
			objUpdateProfileParameter.addValue("CompanyName", objSupProfile.getCompanyName());
			objUpdateProfileParameter.addValue("CorporateAddress", objSupProfile.getCorporateAddress());
			objUpdateProfileParameter.addValue("Address2", objSupProfile.getAddress());
			objUpdateProfileParameter.addValue("City", objSupProfile.getCity());
			objUpdateProfileParameter.addValue("PostalCode", objSupProfile.getPostalCode());
			objUpdateProfileParameter.addValue("State", objSupProfile.getState());
			objUpdateProfileParameter.addValue("PhoneNumber", Utility.removePhoneFormate(objSupProfile.getPhone()));
			/*
			 * objUpdateProfileParameter.addValue("LegalAuthorityFirstName",
			 * objSupProfile.getFirstName());
			 * objUpdateProfileParameter.addValue("LegalAuthorityLastName",
			 * objSupProfile.getLastName());
			 */
			objUpdateProfileParameter.addValue("ContactFirstName", objSupProfile.getContactFName());
			objUpdateProfileParameter.addValue("ContactLastName", objSupProfile.getContactLName());
			objUpdateProfileParameter.addValue("ContactPhoneNumber", Utility.removePhoneFormate(objSupProfile.getContactPhone()));
			objUpdateProfileParameter.addValue("ContactEmail", objSupProfile.getEmail());
			objUpdateProfileParameter.addValue("IsNonProfitOrganisation", objSupProfile.isNonProfit());
			objUpdateProfileParameter.addValue("BusinessCategoryIDs", objSupProfile.getbCategory());
			resultFromProcedure = simpleJdbcCall.execute(objUpdateProfileParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				isDuplicateSupplierName = (Boolean) resultFromProcedure.get("DuplicateSupplier");
				if (isDuplicateSupplierName)
				{
					response = ApplicationConstants.DUPLICATE_SUPPLIERNAME;
					LOG.info("Inside EditUserProfileController : addUpdateProfile : DUPLICATE_SUPPLIERNAME : " + response);
				}
				else{
					response = ApplicationConstants.SUCCESS;
					LOG.info("the responseFromProc is :" + responseFromProc);
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : addUpdateProfile  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebSupplierProfileUpdation executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : addUpdateProfile : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	/**
	 * This method insert hot deal information.
	 * 
	 * @param hotDealObj
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 */
	public String addHotDeal(int supplierID, HotDealInfo hotDealObj) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : addHotDeal : ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = 1;
		String response = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierAddHotDeal");
			final MapSqlParameterSource hotDealAddParamater = new MapSqlParameterSource();
			hotDealAddParamater.addValue("ManufacturerID", supplierID);
			hotDealAddParamater.addValue("HotDealName", hotDealObj.getHotDealName());
			hotDealAddParamater.addValue("RegularPrice", hotDealObj.getPrice());
			hotDealAddParamater.addValue("SalePrice", hotDealObj.getSalePrice());
			hotDealAddParamater.addValue("HotDealDescription", hotDealObj.getHotDealShortDescription());
			hotDealAddParamater.addValue("HotDealLongDescription", hotDealObj.getHotDealLongDescription());
			hotDealAddParamater.addValue("HotDealTerms", hotDealObj.getHotDealTermsConditions());
			hotDealAddParamater.addValue("URL", hotDealObj.getUrl());
			hotDealAddParamater.addValue("DealStartDate", hotDealObj.getDealStartDate());
			hotDealAddParamater.addValue("DealStartTime", hotDealObj.getDealStartTime());
			hotDealAddParamater.addValue("DealEndDate", hotDealObj.getDealEndDate());
			hotDealAddParamater.addValue("DealEndTime", hotDealObj.getDealEndTime());

			if (hotDealObj.getDealTimeZoneId() == 0)
			{
				hotDealAddParamater.addValue("HotDealTimeZoneID", null);
			}
			else
			{
				hotDealAddParamater.addValue("HotDealTimeZoneID", hotDealObj.getDealTimeZoneId());
			}

			if (hotDealObj.getDealForCityLoc().equals("City"))
			{
				hotDealAddParamater.addValue("PopulationCentreID", hotDealObj.getCity());
				hotDealAddParamater.addValue("RetailID", null);
				hotDealAddParamater.addValue("RetailLocationID", null);
			}
			else if (hotDealObj.getDealForCityLoc().equals("Location"))
			{
				hotDealAddParamater.addValue("PopulationCentreID", null);
				hotDealAddParamater.addValue("RetailID", hotDealObj.getRetailID());
				hotDealAddParamater.addValue("RetailLocationID", hotDealObj.getRetailerLocID());
			}
			else
			{
				hotDealAddParamater.addValue("PopulationCentreID", null);
				hotDealAddParamater.addValue("RetailID", null);
				hotDealAddParamater.addValue("RetailLocationID", null);
			}
			/*
			 * if (hotDealObj.getCity() != null &&
			 * !"0".equals(hotDealObj.getCity()))
			 * hotDealAddParamater.addValue("PopulationCentreID",
			 * hotDealObj.getCity()); else
			 * hotDealAddParamater.addValue("PopulationCentreID", null);
			 * if(hotDealObj.getState() != null && !
			 * "0".equals(hotDealObj.getState()))
			 * hotDealAddParamater.addValue("State", hotDealObj.getState());
			 * else hotDealAddParamater.addValue("State",null); if
			 * (hotDealObj.getRetailID() == 0)
			 * hotDealAddParamater.addValue("RetailID", null); else
			 * hotDealAddParamater.addValue("RetailID",
			 * hotDealObj.getRetailID()); if (hotDealObj.getRetailerLocID() ==
			 * 0) hotDealAddParamater.addValue("RetailLocationID", null); else
			 * hotDealAddParamater.addValue("RetailLocationID",
			 * hotDealObj.getRetailerLocID());
			 */

			if (hotDealObj.getProductId() != null || "".equals(hotDealObj.getProductId()))
				hotDealAddParamater.addValue("ProductID", hotDealObj.getProductId());
			else
				hotDealAddParamater.addValue("ProductID", null);

			hotDealAddParamater.addValue("CategoryID", hotDealObj.getbCategory());
			resultFromProcedure = simpleJdbcCall.execute(hotDealAddParamater);
			/**
			 * For getting response from stored procedure ,sp return Status as
			 * response success as 0 failure as 1
			 */
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
				LOG.info("Inside SupplierDAOImpl : addHotDeal : Success");
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);

				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				// respMessage.setErrorCode(errorNum);
				LOG.info("Error Occured in addHotDeal method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
			LOG.info("usp_WebSupplierAddHotDeal executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : addHotDeal : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	/**
	 * This method update hot deal information.
	 * 
	 * @param hotDealObj
	 *            As instance of HotDealInfo
	 * @return Success or failure of method execution.
	 * @throws ScanSeeWebSqlException
	 */
	public String hotDealUpdate(HotDealInfo hotDealObj) throws ScanSeeWebSqlException
	{
		final String methodName = "hotDealUpdate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		try
		{
			// jdbcTemplate = dbConnection.getConnection();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHotDealModification");
			final MapSqlParameterSource hotDealEditParamater = new MapSqlParameterSource();
			hotDealEditParamater.addValue("HotDealID", hotDealObj.getHotDealID());
			hotDealEditParamater.addValue("HotDealName", hotDealObj.getHotDealName());
			hotDealEditParamater.addValue("SalePrice", hotDealObj.getSalePrice());
			hotDealEditParamater.addValue("DealStartDate", hotDealObj.getDealStartDate());
			hotDealEditParamater.addValue("DealStartTime", hotDealObj.getDealStartTime());
			hotDealEditParamater.addValue("DealEndDate", hotDealObj.getDealEndDate());
			hotDealEditParamater.addValue("DealEndTime", hotDealObj.getDealEndTime());
			hotDealEditParamater.addValue("CategoryID", hotDealObj.getbCategory());
			resultFromProcedure = simpleJdbcCall.execute(hotDealEditParamater);
			/**
			 * For getting response from stored procedure ,sp return Status as
			 * response success as 0 failure as 1
			 */
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				response = ApplicationConstants.FAILURE;
				LOG.info("Error Occured in hotDealUpdate method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
			}
			LOG.info("usp_WebHotDealModification executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : hotDealUpdate : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method get hot deals city state and location information based on
	 * search parameter.
	 * 
	 * @return retailerList
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<RetailerInfo> getHotdealCityStateRetailerList(String search) throws ScanSeeWebSqlException
	{
		final String methodName = "getHotdealCityStateRetailerList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<RetailerInfo> retailerList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHotdealCityStateRetailerList");
			simpleJdbcCall.returningResultSet("retailerList", new BeanPropertyRowMapper<RetailerInfo>(RetailerInfo.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("Search", search);
			LOG.info("Before :" + resultFromProcedure);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			LOG.info("After :" + resultFromProcedure);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			retailerList = (ArrayList) resultFromProcedure.get("retailerList");
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + retailerList.size());
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info("Error Occured in getHotdealCityStateRetailerList method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
			}
			LOG.info("usp_WebHotdealCityStateRetailerList executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in getHotdealCityStateRetailerList");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerList;
	}

	/**
	 * This method will return the Product UPC or Product Name List based on
	 * parameter.
	 * @param strProductUpcOrName.
	 * @return strProductUpcOrName List based on parameter.
	 * @throws ScanSeeWebSqlException.
	 */
	public SearchResultInfo getProductsByProductUpcOrName(String strProductUpcOrName, int manufacturerID, int lowerLimit)
			throws ScanSeeWebSqlException
	{
		final String methodName = "getProductsByProductUpcOrName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<ManageProducts> arProductList = null;
		SearchResultInfo result = new SearchResultInfo();
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierManageProductSearch");
			simpleJdbcCall.returningResultSet("searchProductList", new BeanPropertyRowMapper<ManageProducts>(ManageProducts.class));

			final MapSqlParameterSource objMgProductParameters = new MapSqlParameterSource().addValue("ManufacturerID", manufacturerID).addValue(
					"SearchParameter", strProductUpcOrName);
			objMgProductParameters.addValue("LowerLimit", lowerLimit);

			resultFromProcedure = simpleJdbcCall.execute(objMgProductParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					arProductList = (ArrayList) resultFromProcedure.get("searchProductList");
					result.setManageProductList(arProductList);
					if (null != arProductList && !arProductList.isEmpty())
					{
						int totalSize = (Integer) resultFromProcedure.get("RowCount");
						result.setTotalSize(totalSize);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : getProductsByProductUpcOrName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebSupplierManageProductSearch executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Error Occurred in getProductsByProductUpcOrName ", exception);
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return result;
	}

	/*
	 * public static void main(String[] args) { try { new
	 * SupplierDAOImpl().getProductsByProductUpcOrName
	 * ("NetVanta 6310 VoIP Gateway",364); } catch (ScanSeeWebSqlException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } }
	 */

	/**
	 * this method will get one user UpdateProfile record from Database based on
	 * parameter(userID)
	 * 
	 * @param userId
	 *            .
	 * @return Update profile list based on userID.
	 * @throws ScanSeeWebSqlException
	 */

	public List<SupplierProfile> getUpdateProfile(Long userId) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getUpdateProfile ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<SupplierProfile> arGetProfile = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierProfileDisplay");
			simpleJdbcCall.returningResultSet("arGetProfileList", new BeanPropertyRowMapper<SupplierProfile>(SupplierProfile.class));

			final MapSqlParameterSource objMgProductParameters = new MapSqlParameterSource();
			objMgProductParameters.addValue("ManufacturerID", userId);
			resultFromProcedure = simpleJdbcCall.execute(objMgProductParameters);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			arGetProfile = (ArrayList) resultFromProcedure.get("arGetProfileList");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : getUpdateProfile : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebSupplierProfileDisplay executed Successfully.");
		return arGetProfile;
	}

	/**
	 * This method insert the Manage Products details into database.
	 * 
	 * @param objManageProduct
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeWebSqlException
	 */
	public String addManageProducts(ProductInfoVO objManageProduct, long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "addProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Boolean duplicateprodUpc;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierAddProduct");
			final MapSqlParameterSource objAddMgProdParameter = new MapSqlParameterSource();
			
			if(Utility.isEmptyOrNullString(objManageProduct.getSuggestedRetailPrice()))
			{
				objManageProduct.setSuggestedRetailPrice("0");
			}

			objAddMgProdParameter.addValue("UserID", userId);
			objAddMgProdParameter.addValue("ProductUPC", objManageProduct.getProductUPC());
			objAddMgProdParameter.addValue("ProductName", objManageProduct.getProductName());
			objAddMgProdParameter.addValue("ModelNumber", objManageProduct.getModelNumber());
			objAddMgProdParameter.addValue("SuggestedRetailPrice", objManageProduct.getSuggestedRetailPrice());
			objAddMgProdParameter.addValue("ProductimagePath", objManageProduct.getDefaultImagePath());
			objAddMgProdParameter.addValue("LongDescription", objManageProduct.getLongDescription());
			objAddMgProdParameter.addValue("ShortDescription", objManageProduct.getShortDescription());
			objAddMgProdParameter.addValue("Warranty", objManageProduct.getWarrantyORService());
			objAddMgProdParameter.addValue("ImagePath", objManageProduct.getImagePaths());
			objAddMgProdParameter.addValue("AudioMediapath", objManageProduct.getAudioFilePath());
			objAddMgProdParameter.addValue("VideoMediaPath", objManageProduct.getVideoFilePath());
			if ("".equals(objManageProduct.getPrdAttributes()))
			{
				objManageProduct.setPrdAttributes(null);
			}
			objAddMgProdParameter.addValue("AttributeID", objManageProduct.getPrdAttributes());
			if ("".equals(objManageProduct.getValues()))
			{
				objManageProduct.setValues(null);
			}
			objAddMgProdParameter.addValue("DisplayValue", objManageProduct.getValues());
			if (objManageProduct.getProductCategory() != null && "0".equals(objManageProduct.getProductCategory()))
			{
				objAddMgProdParameter.addValue("CategoryID", null);
			}
			else
			{
				objAddMgProdParameter.addValue("CategoryID", objManageProduct.getProductCategory());
			}

			resultFromProcedure = simpleJdbcCall.execute(objAddMgProdParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != resultFromProcedure)
			{
				duplicateprodUpc = (Boolean) resultFromProcedure.get("DuplicateProduct");

				if (duplicateprodUpc != null)
				{
					if (duplicateprodUpc == true)
					{
						response = ApplicationConstants.PRODUCTEXISTS;
						return response;
					}

				}
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					response = ApplicationConstants.SUCCESS;
					LOG.info("the responseFromProc is :" + responseFromProc);
				}
				else
				{
					response = ApplicationConstants.FAILURE;
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : addManageProducts  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebSupplierAddProduct executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : addManageProducts : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<PlanInfo> fetchAllPlanList()
	{

		final String methodName = "fetchAllPlanList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<PlanInfo> planList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierBillingPlanDisplay");
			simpleJdbcCall.returningResultSet("planList", new BeanPropertyRowMapper<PlanInfo>(PlanInfo.class));

			final SqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			planList = (ArrayList<PlanInfo>) resultFromProcedure.get("planList");

		}
		catch (DataAccessException e)
		{
			// LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
			// e);
			// throw new ScanSeeException(e.getMessage());
			e.printStackTrace();
		}
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("usp_WebSupplierBillingPlanDisplay executed Successfully.");
		return planList;

	}

	/**
	 * This method update's Manage Products information .
	 * 
	 * @param objManageProducts
	 * @return objManageProducts instance of ManageProducts.
	 * @throws Exception
	 */
	public String updateManageProducts(ManageProducts objManageProduct) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : updateManageProducts ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_webRebateUpdation");
			final MapSqlParameterSource objupdateMgProdParameter = new MapSqlParameterSource();

			objupdateMgProdParameter.addValue("ProductID", objManageProduct.getProductID());
			objupdateMgProdParameter.addValue("ProductUPC", objManageProduct.getScanCode());
			objupdateMgProdParameter.addValue("ProductName", objManageProduct.getProductName());
			objupdateMgProdParameter.addValue("CategoryName", objManageProduct.getCategory());
			objupdateMgProdParameter.addValue("ModelNumber", objManageProduct.getModelNumber());
			objupdateMgProdParameter.addValue("SuggestedRetailPrice", objManageProduct.getSuggestedRetailPrice());
			objupdateMgProdParameter.addValue("ProductimagePath", objManageProduct.getImagePath());
			objupdateMgProdParameter.addValue("LongDescription", objManageProduct.getLongDescription());
			objupdateMgProdParameter.addValue("ShortDescription", objManageProduct.getShortDescription());
			objupdateMgProdParameter.addValue("Warranty", objManageProduct.getWarranty());
			objupdateMgProdParameter.addValue("ProductMediatypeID", objManageProduct.getProductMediatypeID());
			objupdateMgProdParameter.addValue("AudioMediapath", objManageProduct.getAudioMediaPath());
			objupdateMgProdParameter.addValue("VideoMediaPath", objManageProduct.getVideoMediaPath());
			objupdateMgProdParameter.addValue("AttributeName", objManageProduct.getProdAttributeName());
			objupdateMgProdParameter.addValue("DisplayValue", objManageProduct.getProdDisplayValue());

			resultFromProcedure = simpleJdbcCall.execute(objupdateMgProdParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
				LOG.info("the responseFromProc is :" + responseFromProc);
			}
			else
			{
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : updateRebates  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_webRebateUpdation executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : updateManageProducts : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	/**
	 * The DAOImpl method for displaying all the states and it return to service layer.
	 * @throws ScanSeeWebSqlException as SQL exception.
	 * @return states,List of states.
	 */
	public final ArrayList<State> getAllStates() throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getAllStates ");
		List<State> arStateList = null;
		try
		{
			arStateList = this.jdbcTemplate.query("SELECT StateName,Stateabbrevation FROM [State] order by StateName", new RowMapper<State>() {
				public State mapRow(ResultSet rs, int rowNum) throws SQLException
				{
					final State state = new State();
					state.setStateName(rs.getString("StateName"));
					state.setStateabbr(rs.getString("Stateabbrevation"));
					return state;
				}
			});
		}
		catch (EmptyResultDataAccessException e)
		{

			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return (ArrayList<State>) arStateList;
	}
	/**
	 * The DAOImpl method for displaying all the states and it return to service layer.
	 * @throws ScanSeeWebSqlException as SQL exception.
	 * @return states,List of states.
	 */
	public final ArrayList<State> getState(String state) throws ScanSeeWebSqlException
	{
		final String methodName = "getState";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		List<State> arStateList = null;
		
		try
		{
			arStateList = this.jdbcTemplate.query("SELECT StateName FROM [State] Where Stateabbrevation like '"+state+"'", new RowMapper<State>() {
				public State mapRow(ResultSet rs, int rowNum) throws SQLException
				{
					final State state = new State();
					state.setStateName(rs.getString("StateName"));
					//state.setStateabbr(rs.getString("Stateabbrevation"));
					return state;
				}
			});
		}
		catch (EmptyResultDataAccessException e)
		{

			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return (ArrayList<State>) arStateList;
	}
	
	public List<Rebates> getRebatesForDisplay(int rebateId) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getRebatesForDisplay ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<Rebates> listRebNames = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayRebate");
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RebateID", rebateId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listRebNames = (ArrayList) resultFromProcedure.get("searchList");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesForDisplay : " + exception.getMessage());
			if (responseFromProc.intValue() != 0)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : getRebatesForDisplay  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebDisplayRebate executed Successfully.");
		return listRebNames;
	}

	/**
	 * The DAO method for displaying all the cities and it return to service layer.
	 * @param stateCode as request parameter.
	 * @throws ScanSeeWebSqlException as SQL exception.
	 * @return cities,List of cities.
	 */
	public final ArrayList<City> getAllCities(String stateCode) throws ScanSeeWebSqlException
	{
		final String methodName = "getAllCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		List<City> cityList = null;
		try {
			cityList = this.jdbcTemplate.query("SELECT Distinct City FROM GeoPosition WHERE [State] =? order by City", new Object[] { stateCode },
					new RowMapper<City>() {
						public City mapRow(ResultSet rs, int rowNum) throws SQLException
						{
							final City city = new City();
							city.setCityName(rs.getString("City"));
							return city;
						}
					});
		}
		catch (EmptyResultDataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return (ArrayList<City>) cityList;
	}

	/**
	 * This method will return the deals details based on dealId.
	 * 
	 * @param dealId
	 *            as request parameter
	 * @return deals List based on parameter.
	 * @throws ScanSeeWebSqlException
	 *             Sql exception.
	 */
	public List<HotDealInfo> getHotDealsDetails(int dealId) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getHotDealsDetails ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<HotDealInfo> listOfDeals = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierHotDealSearch");
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource objSearchDealParameter = new MapSqlParameterSource();
			objSearchDealParameter.addValue("ProductHotDealID", dealId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchDealParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listOfDeals = (ArrayList) resultFromProcedure.get("searchList");
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{

				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + listOfDeals.size());
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info("Inside SupplierDAOImpl : getHotDealsDetails  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebSupplierHotDealSearch executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getHotDealsDetails : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return listOfDeals;

	}

	/**
	 * This method will return the HotDeal List based on parameter.
	 * 
	 * @param strPdtName
	 * @return Rebates Names or Product Names List based on parameter.
	 * @throws ScanSeeWebSqlException
	 */
	public SearchResultInfo getDealsForDisplay(Long userId, String strPdtName, int lowerLimit) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getDealsForDisplay ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		SearchResultInfo searchresult = null;
		Integer rowcount = null;
		List<HotDealInfo> listHotDeals = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierHotDealSearch");
			simpleJdbcCall.returningResultSet("dealsSearchList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource objSearchDealsParameter = new MapSqlParameterSource();
			objSearchDealsParameter.addValue("ManufacturerID", userId);
			objSearchDealsParameter.addValue("SearchParameter", strPdtName);
			objSearchDealsParameter.addValue("LowerLimit", lowerLimit);

			resultFromProcedure = simpleJdbcCall.execute(objSearchDealsParameter);
			/*
			 * responseFromProc = (Integer) resultFromProcedure
			 * .get(ApplicationConstants.STATUS);
			 */
			listHotDeals = (ArrayList) resultFromProcedure.get("dealsSearchList");
			/*
			 * if (listHotDeals != null && !listHotDeals.isEmpty()) {
			 */

			searchresult = new SearchResultInfo();
			rowcount = (Integer) resultFromProcedure.get("RowCount");
			searchresult.setDealList(listHotDeals);
			searchresult.setTotalSize(rowcount);
			/* } */
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebSupplierHotDealSearch executed Successfully.");
		return searchresult;

	}

	public ArrayList<Retailer> getRetailers() throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getDealsForDisplay ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<Retailer> listRetailer = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveRetailer");
			simpleJdbcCall.returningResultSet("listRetailerList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listRetailer = (ArrayList) resultFromProcedure.get("listRetailerList");

		}
		
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		LOG.info("usp_WebRetrieveRetailer executed Successfully.");
		return listRetailer;
	}

	public ArrayList<Retailer> getRetailerLoc(int retailerId) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetailerLoc";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<Retailer> listRetailer = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveRetailerLocation");
			simpleJdbcCall.returningResultSet("listRetailerLocList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RetailID", retailerId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					listRetailer = (ArrayList) resultFromProcedure.get("listRetailerLocList");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : getRetailerLoc  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				}
			}
			LOG.info("usp_WebRetrieveRetailerLocation executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return listRetailer;
	}

	/**
	 * This method will return the all Category List .
	 * 
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<Category> getAllCategory() throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getAllCategory ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<Category> arCategoryList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveCategory");
			simpleJdbcCall.returningResultSet("categoryList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			arCategoryList = (ArrayList) resultFromProcedure.get("categoryList");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getAllCategory : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebRetrieveCategory executed Successfully.");
		return arCategoryList;
	}

	/**
	 * This method upload a file containing all of your product
	 * information,product images, audio, and video files.
	 * 
	 * @param listManageProducts
	 * @return listManageProducts list of ManageProducts.
	 * @throws ScanSeeWebSqlException
	 */

	public String addBatchUploadProductsToStage(ArrayList<ManageProducts> prodList, int manufacturerID, long userId) throws ScanSeeWebSqlException
	{

		LOG.info("Inside SupplierDAOImpl : addBatchUploadProductsToStage ");
		String response = null;

		Object[] values = null;
		List<Object[]> batch = new ArrayList<Object[]>();

		try
		{
			SimpleJdbcTemplate simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			Calendar currentDate = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MMM/dd HH:mm:ss");
			String dateNow = formatter.format(currentDate.getTime());
			String other = null;

			for (ManageProducts objMgProducts : prodList)
			{
				values = new Object[] { objMgProducts.getProductName(), objMgProducts.getScanCode(), objMgProducts.getScanType(),
						objMgProducts.getProdExpirationDate(), objMgProducts.getImagePath(), objMgProducts.getCategory(),
						objMgProducts.getShortDescription(), objMgProducts.getLongDescription(), objMgProducts.getModelNumber(),
						objMgProducts.getSuggestedRetPrice(), objMgProducts.getProductWeight(), objMgProducts.getWeightUnits(),
						objMgProducts.getManufProuductURL(), objMgProducts.getAudioMediaPath(), objMgProducts.getVideoMediaPath(),
						objMgProducts.getProductImagePath(), manufacturerID, userId, dateNow, other };
				batch.add(values);
			}
			int[] updateCounts = simpleJdbcTemplate.batchUpdate(
					"INSERT INTO UploadManufacturerProducts VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", batch);
			if (updateCounts.length != 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : addBatchUploadProductsToStage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : addBatchUploadProductsToStage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}
		return response;
	}

	public List<ProductAttributes> getMasterProdAttrList() throws ScanSeeWebSqlException
	{
		final String methodName = "getMasterProdAttrList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<ProductAttributes> attrList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebAttributeDisplay");
			simpleJdbcCall.returningResultSet("MasterAttList", new BeanPropertyRowMapper<ProductAttributes>(ProductAttributes.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			LOG.info(" executing usp_WebAttributeDisplay.");
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != resultFromProcedure)
			{
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg)
				{
					attrList = (List) resultFromProcedure.get("MasterAttList");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : getRebatesByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebAttributeDisplay executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return attrList;
	}

	public String uploadAttributeToStage(ArrayList<ManageProducts> attributeList, int manufacturerID, long userId) throws ScanSeeWebSqlException
	{

		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MMM/dd HH:mm:ss");
		String dateNow = formatter.format(currentDate.getTime());
		LOG.info("Inside SupplierDAOImpl : addBatchUploadProductsToStage ");
		String response = null;

		Object[] values = null;
		List<Object[]> batch = new ArrayList<Object[]>();

		try
		{
			SimpleJdbcTemplate simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			for (ManageProducts objMgProducts : attributeList)
			{
				values = new Object[] { objMgProducts.getScanCode(), objMgProducts.getAttributeName(), objMgProducts.getAttributeValue(), userId,
						manufacturerID, dateNow };
				batch.add(values);
			}
			int[] updateCounts = simpleJdbcTemplate.batchUpdate("INSERT INTO UploadManufacturerProductAttributes VALUES (?,?,?,?,?,?)", batch);
			if (updateCounts.length != 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : uploadAttributeToStage : " + exception.getMessage());
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : uploadAttributeToStage : " + exception.getMessage());
		}
		return response;
	}

	public List<Product> getProductAssociatedto(String pdtName, Long supplierId) throws ScanSeeWebSqlException
	{
		LOG.info("Inside RetailerDAOImpl : getCouponByCouponName ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		ArrayList<Product> arProdNameList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetailerCouponProductSearch");
			simpleJdbcCall.returningResultSet("searchCouponNameList", new BeanPropertyRowMapper<Product>(Product.class));

			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();
			objCouponNameParameters.addValue("UserID", supplierId);

			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);

			arProdNameList = (ArrayList<Product>) resultFromProcedure.get("searchCouponNameList");

		}
		catch (Exception exception)
		{
			LOG.error("Inside RetailerDAOImpl : getCouponByCouponName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebRetailerCouponProductSearch executed Successfully.");
		return arProdNameList;
	}

	public String moveProductDataStagingToProductionTable(ManageProducts objMgProducts, int manufacturerID, long userId, String productFileName,
			String attributeFileName) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : moveProductDataStagingToProductionTable ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUploadManufacturerProducts");
			final MapSqlParameterSource objupdateMgProdParameter = new MapSqlParameterSource();

			objupdateMgProdParameter.addValue("ManufacturerID", manufacturerID);
			objupdateMgProdParameter.addValue("UserID", userId);
			objupdateMgProdParameter.addValue("ProductFileName", productFileName);
			objupdateMgProdParameter.addValue("ManufacturerLogID", objMgProducts.getLogId());
			resultFromProcedure = simpleJdbcCall.execute(objupdateMgProdParameter);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				objMgProducts.setSuccess((Integer) resultFromProcedure.get("SuccessCount"));
				objMgProducts.setTotal((Integer) resultFromProcedure.get("TotalRows"));
				objMgProducts.setFailure((Integer) resultFromProcedure.get("FailureCount"));
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : moveProductDataStagingToProductionTable  : errorNumber  : " + errorNum + "errorMessage : "
						+ errorMsg);
				response = ApplicationConstants.FAILURE;

			}
			LOG.info("usp_WebUploadManufacturerProducts executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : moveProductDataStagingToProductionTable : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;

	}

	public String moveAttributesDataStagingToProductionTable(ManageProducts objMgProducts, int manufacturerID, long userId, String productFileName,
			String attributeFileName) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : moveAttributesDataStagingToProductionTable ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUploadManufacturerProductAttributes");
			final MapSqlParameterSource objupdateMgProdParameter = new MapSqlParameterSource();

			objupdateMgProdParameter.addValue("ManufacturerID", manufacturerID);
			objupdateMgProdParameter.addValue("UserID", userId);
			objupdateMgProdParameter.addValue("ProductAttributeFileName", attributeFileName);
			objupdateMgProdParameter.addValue("ManufacturerLogID", objMgProducts.getLogId());
			resultFromProcedure = simpleJdbcCall.execute(objupdateMgProdParameter);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				objMgProducts.setSuccess((Integer) resultFromProcedure.get("SuccessCount"));
				objMgProducts.setTotal((Integer) resultFromProcedure.get("TotalRows"));
				objMgProducts.setFailure((Integer) resultFromProcedure.get("FailureCount"));
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : moveAttributesDataStagingToProductionTable  : errorNumber  : " + errorNum + "errorMessage : "
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebUploadManufacturerProductAttributes executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : moveAttributesDataStagingToProductionTable : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	public String saveGridProducts(List<ManageProducts> prodLis, Users user) throws ScanSeeWebSqlException
	{

		LOG.info("Inside SupplierDAOImpl : addBatchUploadProductsToStage ");
		String response = null;

		Object[] values = null;
		Object[] catValues = null;
		Object[] deleteCatValues = null;
		List<Object[]> batch = new ArrayList<Object[]>();
		List<Object[]> catbatch = new ArrayList<Object[]>();
		List<Object[]> deleteCatbatch = new ArrayList<Object[]>();

		try
		{
			String updateQuery = "UPDATE Product SET ScanCode = ? ,ProductName = ? ,ProductShortDescription = ? ,"
					+ "ProductLongDescription = ? ,ModelNumber = ? ,SuggestedRetailPrice = ? ,"
					+ "WarrantyServiceInformation =? WHERE ProductID = ? and ManufacturerID = ?";

			String updateCatQuery = " IF EXISTS(SELECT 1 FROM ProductCategory WHERE ProductID = ?) "
					+ " BEGIN UPDATE ProductCategory SET CategoryID = ? WHERE ProductID = ? " + " END ELSE BEGIN "
					+ " INSERT INTO ProductCategory(ProductID,CategoryID,DateAdded) VALUES (?,?,'10-11-2011') END";

			String deleteCatQuery = "DELETE FROM ProductCategory WHERE ProductID = ?";

			SimpleJdbcTemplate simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			for (ManageProducts objMgProducts : prodLis)
			{
				values = new Object[] { objMgProducts.getScanCode(), objMgProducts.getProductName(), objMgProducts.getShortDescription(),
						objMgProducts.getLongDescription(), objMgProducts.getModelNumber(), objMgProducts.getSuggestedRetailPrice(),
						objMgProducts.getWarranty(), objMgProducts.getProductID(), user.getSupplierId() };
				if (objMgProducts.getCategory() != null && "0".equals(objMgProducts.getCategory()))
				{
					deleteCatValues = new Object[] { objMgProducts.getProductID() };
					deleteCatbatch.add(deleteCatValues);
				}
				else
				{
					catValues = new Object[] { objMgProducts.getProductID(), objMgProducts.getCategory(), objMgProducts.getProductID(),
							objMgProducts.getProductID(), objMgProducts.getCategory() };
					catbatch.add(catValues);
				}

				batch.add(values);
			}

			int[] updateCounts = simpleJdbcTemplate.batchUpdate(updateQuery, batch);

			int[] updateCatCounts = simpleJdbcTemplate.batchUpdate(updateCatQuery, catbatch);

			int[] deleteCatCounts = simpleJdbcTemplate.batchUpdate(deleteCatQuery, deleteCatbatch);

			if (updateCounts.length != 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : uploadAttributeToStage : " + exception.getMessage());

			throw new ScanSeeWebSqlException(exception.getCause());
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : uploadAttributeToStage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getCause());
		}
		return response;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public ArrayList<Rebates> getProductListForRebates(String strProductName, int supllierId, long retailerID, int retailerLocId)
			throws ScanSeeWebSqlException
	{

		LOG.info("Inside SuplierDaoImpl : getProductListForRebates ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		ArrayList<Rebates> arProdNameList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierRebateAddProductSearch");
			simpleJdbcCall.returningResultSet("searchProdcutNameList", new BeanPropertyRowMapper<Rebates>(Rebates.class));

			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();
			objCouponNameParameters.addValue("ManufacturerID", supllierId);
			objCouponNameParameters.addValue("RetailerID", retailerID);
			objCouponNameParameters.addValue("RetailerLocationID", retailerLocId);
			objCouponNameParameters.addValue("Search", strProductName);

			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);
			/*
			 * responseFromProc = (Integer) resultFromProcedure
			 * .get(ApplicationConstants.STATUS);
			 */

			arProdNameList = (ArrayList) resultFromProcedure.get("searchProdcutNameList");
			/*
			 * if (null != responseFromProc && responseFromProc.intValue() == 0)
			 * { //response = ApplicationConstants.SUCCESS;
			 * LOG.info("the responseFromProc is :" + responseFromProc); } else
			 * { int errorNum = (Integer) resultFromProcedure
			 * .get(ApplicationConstants.ERRORNUMBER); final String errorMsg =
			 * (String) resultFromProcedure
			 * .get(ApplicationConstants.ERRORMESSAGE);
			 * LOG.info("Inside SupplierDAOImpl : updateRebates  : errorNumber  : "
			 * + errorNum + "errorMessage : " + errorMsg); }
			 */

		}
		catch (Exception exception)
		{
			LOG.error("Inside SuplierDaoImpl : getProductListForRebates : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebSupplierRebateAddProductSearch executed Successfully.");
		return arProdNameList;
	}

	public List<ProductAttributes> fetchProductAttributes(int productId, int supplierId, String type) throws ScanSeeWebSqlException
	{
		final String methoName = "fetchProductAttributes";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<ProductAttributes> attrList = null;
		try
		{
			LOG.info("Fetching Product attributes for product ID{} and Type {}", productId, type);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			if (type != null && type.equals("productattributes"))
			{
				simpleJdbcCall.withProcedureName("usp_WebSupplierAttributeDisplay");
			}
			else
			{
				simpleJdbcCall.withProcedureName("usp_WebSupplierAttributeDisplayList");
			}

			simpleJdbcCall.returningResultSet("ProdAttList", new BeanPropertyRowMapper<ProductAttributes>(ProductAttributes.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("Productid", productId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != resultFromProcedure)
			{
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg)
				{
					attrList = (List) resultFromProcedure.get("ProdAttList");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : getRebatesByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebSupplierAttributeDisplay/usp_WebSupplierAttributeDisplayList are executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return attrList;
	}

	/**
	 * This method Add the Rebates details into database.
	 * 
	 * @param objRebates
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeWebSqlException
	 */

	public String addRerunRebates(Rebates objRebates) throws ScanSeeWebSqlException
	{

		LOG.info("Inside SupplierDAOImpl : addRerunRebates ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierAddRebate");
			final MapSqlParameterSource objAddRebParameter = new MapSqlParameterSource();

			objAddRebParameter.addValue("SupplierID", objRebates.getSupplierId());
			objAddRebParameter.addValue("UserID", objRebates.getUserID());
			objAddRebParameter.addValue("RebateName", objRebates.getRebName());
			objAddRebParameter.addValue("RebateAmount", objRebates.getRebAmount());
			objAddRebParameter.addValue("RebateDescription", objRebates.getRebDescription());
			objAddRebParameter.addValue("RebateTermsAndCondition", objRebates.getRebTermCondtn());
			objAddRebParameter.addValue("RebateStartDate", objRebates.getRebStartDate());
			objAddRebParameter.addValue("RebateEndDate", objRebates.getRebEndDate());
			objAddRebParameter.addValue("RebateStartTime", objRebates.getRebStartTime());
			objAddRebParameter.addValue("RebateEndTime", objRebates.getRebEndTime());
			objAddRebParameter.addValue("RetailerID", objRebates.getRetailID());
			objAddRebParameter.addValue("RetailerLocationID", objRebates.getRetailerLocID());
			objAddRebParameter.addValue("NoOfRebatesIssued", objRebates.getNoOfrebatesIsued());
			objAddRebParameter.addValue("ProductID", objRebates.getRebproductId());
			if (objRebates.getRebateTimeZoneId() == 0)
			{
				objAddRebParameter.addValue("RebateTimeZoneID", null);
			}
			else
			{
				objAddRebParameter.addValue("RebateTimeZoneID", objRebates.getRebateTimeZoneId());
			}

			resultFromProcedure = simpleJdbcCall.execute(objAddRebParameter);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
				LOG.info("the responseFromProc is :" + responseFromProc);
			}
			else
			{
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : addRerunRebates  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebSupplierAddRebate is executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : addRerunRebates : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	public ArrayList<HotDealInfo> getProductListForHotDeal(String strProductName, int supllierId) throws ScanSeeWebSqlException
	{

		LOG.info("Inside SuplierDaoImpl : getProductListForRebates ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		ArrayList<HotDealInfo> arProdNameList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierHotDealSearch");
			simpleJdbcCall.returningResultSet("searchProdcutNameList", new BeanPropertyRowMapper<Rebates>(Rebates.class));

			final MapSqlParameterSource objSearchDealParameter = new MapSqlParameterSource();
			objSearchDealParameter.addValue("ManufacturerID", supllierId);
			objSearchDealParameter.addValue("SearchParameter", strProductName);
			resultFromProcedure = simpleJdbcCall.execute(objSearchDealParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			arProdNameList = (ArrayList) resultFromProcedure.get("searchProdcutNameList");
			if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				LOG.info("the responseFromProc is :" + responseFromProc);
			}
			else
			{
				LOG.info("Error Occured in getProductListForHotDeal() method ..errorNum..." + ApplicationConstants.ERRORNUMBER + "errorMsg  .."
						+ ApplicationConstants.ERRORMESSAGE);
			}
			LOG.info("usp_WebSupplierHotDealSearch is executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SuplierDaoImpl : getProductListForHotDeal : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return arProdNameList;
	}

	/**
	 * This method Updates the Rebates details into database.
	 * 
	 * @param objRebates
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeWebSqlException
	 */
	public String updateHotDeal(HotDealInfo hotDealInfo, Long supplierID, String flag) throws ScanSeeWebSqlException
	{
		final String methodName = "updateHotDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objHotDealsParameter = new MapSqlParameterSource();
			if (flag.equalsIgnoreCase("editdeal"))
			{
				simpleJdbcCall.withProcedureName("usp_WebSupplierHotDealUpdation");
				objHotDealsParameter.addValue("ManufacturerID", supplierID);
				objHotDealsParameter.addValue("HotDealID", hotDealInfo.getHotDealID());
			}
			else if (flag.equalsIgnoreCase("rerundeal"))
			{
				simpleJdbcCall.withProcedureName("usp_WebSupplierHotDealReRun");
				objHotDealsParameter.addValue("ManufacturerID", supplierID);

			}
			objHotDealsParameter.addValue("HotDealName", hotDealInfo.getHotDealName());
			objHotDealsParameter.addValue("RegularPrice", hotDealInfo.getPrice());
			objHotDealsParameter.addValue("SalePrice", hotDealInfo.getSalePrice());
			objHotDealsParameter.addValue("HotDealDescription", hotDealInfo.getHotDealShortDescription());
			objHotDealsParameter.addValue("HotDealLongDescription", hotDealInfo.getHotDealLongDescription());
			objHotDealsParameter.addValue("HotDealTerms", hotDealInfo.getHotDealTermsConditions());
			objHotDealsParameter.addValue("URL", hotDealInfo.getUrl());
			objHotDealsParameter.addValue("DealStartDate", hotDealInfo.getDealStartDate());
			objHotDealsParameter.addValue("DealEndDate", hotDealInfo.getDealEndDate());
			objHotDealsParameter.addValue("DealStartTime", hotDealInfo.getDealStartTime());
			objHotDealsParameter.addValue("DealEndTime", hotDealInfo.getDealEndTime());

			objHotDealsParameter.addValue("CategoryID", hotDealInfo.getbCategory());

			if (hotDealInfo.getDealTimeZoneId() == 0)

			{
				objHotDealsParameter.addValue("HotDealTimeZoneID", null);
			}
			else
			{
				objHotDealsParameter.addValue("HotDealTimeZoneID", hotDealInfo.getDealTimeZoneId());
			}

			if (hotDealInfo.getDealForCityLoc().equals("City"))
			{
				objHotDealsParameter.addValue("PopulationCentreID", hotDealInfo.getCity());
				objHotDealsParameter.addValue("RetailID", null);
				objHotDealsParameter.addValue("RetailLocationID", null);
			}
			else if (hotDealInfo.getDealForCityLoc().equals("Location"))
			{
				objHotDealsParameter.addValue("PopulationCentreID", null);
				objHotDealsParameter.addValue("RetailID", hotDealInfo.getRetailID());
				objHotDealsParameter.addValue("RetailLocationID", hotDealInfo.getRetailerLocID());
			}
			else
			{
				objHotDealsParameter.addValue("PopulationCentreID", null);
				objHotDealsParameter.addValue("RetailID", null);
				objHotDealsParameter.addValue("RetailLocationID", null);
			}

			/*
			 * if (hotDealInfo.getCity() != null &&
			 * !"0".equals(hotDealInfo.getCity()))
			 * objHotDealsParameter.addValue("PopulationCentreID",
			 * hotDealInfo.getCity()); else
			 * objHotDealsParameter.addValue("PopulationCentreID", null); if
			 * (hotDealInfo.getRetailID() == 0)
			 * objHotDealsParameter.addValue("RetailID", null); else
			 * objHotDealsParameter.addValue("RetailID",
			 * hotDealInfo.getRetailID()); if (hotDealInfo.getRetailerLocID() ==
			 * 0) objHotDealsParameter.addValue("RetailLocationID", null); else
			 * objHotDealsParameter.addValue("RetailLocationID",
			 * hotDealInfo.getRetailerLocID());
			 */

			if (hotDealInfo.getProductId() != null || "".equals(hotDealInfo.getProductId()))
				objHotDealsParameter.addValue("ProductID", hotDealInfo.getProductId());
			else
				objHotDealsParameter.addValue("ProductID", null);

			resultFromProcedure = simpleJdbcCall.execute(objHotDealsParameter);
			/**
			 * For getting response from stored procedure ,sp return Status as
			 * response success as 0 failure as 1
			 */
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
				LOG.info("the responseFromProc is :" + responseFromProc);
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);

				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				// respMessage.setErrorCode(errorNum);
				LOG.info("Error Occured in addHotDeal method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
			LOG.info("usp_WebSupplierHotDealUpdation  is executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : addHotDeal : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public List<HotDealInfo> getHotDealByID(int dealID) throws ScanSeeWebSqlException
	{

		LOG.info("Inside SupplierDAOImpl : getRebatesForDisplay ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<HotDealInfo> listHotDealNames = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayHotDeal");
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource objSearchDealParameter = new MapSqlParameterSource();
			objSearchDealParameter.addValue("HotDealID", dealID);
			resultFromProcedure = simpleJdbcCall.execute(objSearchDealParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listHotDealNames = (ArrayList) resultFromProcedure.get("searchList");
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + listHotDealNames.size());
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info("Inside SupplierDAOImpl : getHotDealByID  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebDisplayHotDeal  is executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getHotDealByID : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return listHotDealNames;

	}

	public String saveAttributes(ManageProducts manageProducts, Long userId, int supplierId) throws ScanSeeWebSqlException
	{
		final String methoName = "saveAttributes";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String response = null;
		try
		{
			LOG.info("Saving attributes for Product ID {}", manageProducts.getProductID());
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierManageProductAddAttributes");
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource objSearchDealParameter = new MapSqlParameterSource();
			objSearchDealParameter.addValue("UserID", userId);
			objSearchDealParameter.addValue("ManufacturerID", supplierId);
			objSearchDealParameter.addValue("ProductID", manageProducts.getProductID());
			objSearchDealParameter.addValue("AttributeID", manageProducts.getProdAttributeName());
			objSearchDealParameter.addValue("AttributeContent", manageProducts.getProdDisplayValue());
			resultFromProcedure = simpleJdbcCall.execute(objSearchDealParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : getHotDealByID  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebSupplierManageProductAddAttributes is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getHotDealByID : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return response;
	}

	public ArrayList<Retailer> getRetailersForProducts(String productId) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getRetailersForProducts ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<Retailer> listPdtRetailer = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHotDealRetrieveRetailer");

			simpleJdbcCall.returningResultSet("listRetailerList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("ProductID", productId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listPdtRetailer = (ArrayList) resultFromProcedure.get("listRetailerList");

		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		LOG.info("usp_WebHotDealRetrieveRetailer is  executed Successfully.");
		return listPdtRetailer;

	}

	/**
	 * The DAO method for displaying all the retailer location for products and its status return to service layer.
	 * @param productIDs as request parameter.
	 * @param retailID as request parameter.
	 * @throws ScanSeeServiceException as service exception.
	 * @return location,List of location.
	 */
	public final ArrayList<Retailer> getRetailerLocForProducts(String productId, int retLocID) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getRetailersForProducts ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<Retailer> listPdtRetailer = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHotDealRetrieveRetailerLocation");
			simpleJdbcCall.returningResultSet("listRetailerList", new BeanPropertyRowMapper<Retailer>(Retailer.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("RetailID", retLocID);
			if ("".equals(Utility.checkNull(productId))) {
				objSearchParameter.addValue("ProductID", null);
			} else {
				objSearchParameter.addValue("ProductID", productId);
			}
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listPdtRetailer = (ArrayList) resultFromProcedure.get("listRetailerList");
		}
		catch (Exception e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info("usp_WebHotDealRetrieveRetailerLocation is  executed Successfully.");
		return listPdtRetailer;
	}

	public List<Product> getProductUPCList(Long supplierId, String productName) throws ScanSeeWebSqlException
	{
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<Product> listProduct = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierProductSearch");
			simpleJdbcCall.returningResultSet("listProduct", new BeanPropertyRowMapper<Product>(Product.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("ManufacturerID", supplierId);
			objSearchParameter.addValue("ProductName", productName);
			objSearchParameter.addValue("LowerLimit", 0);

			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listProduct = (ArrayList) resultFromProcedure.get("listProduct");

		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getRebatesByRebateName : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		LOG.info("usp_WebSupplierProductSearch is  executed Successfully.");
		return listProduct;
	}

	public PlanInfo getDiscount(String discountCode) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getDiscount");
		PlanInfo planInfo = null;
		Map<String, Object> resultFromProcedure = null;
		List<PlanInfo> planInfoList = new ArrayList<PlanInfo>();
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierDiscountPlanDisplay");
			simpleJdbcCall.returningResultSet("dicountList", new BeanPropertyRowMapper<PlanInfo>(PlanInfo.class));
			final MapSqlParameterSource objdiscountParameter = new MapSqlParameterSource();
			objdiscountParameter.addValue("DiscountCode", discountCode);

			resultFromProcedure = simpleJdbcCall.execute(objdiscountParameter);

			planInfoList = (ArrayList<PlanInfo>) resultFromProcedure.get("dicountList");
			if (!planInfoList.isEmpty() && planInfoList != null)
			{
				// discount=planinfolst.get(0).getDiscount();
				planInfo = planInfoList.get(0);
			}
			LOG.info("usp_WebSupplierDiscountPlanDisplay is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getHotDealByID : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return planInfo;
	}

	public String savePlan(List<PlanInfo> pInfoList, int manufacturerID, long userId, Integer discountPlanManufacturerID)
			throws ScanSeeWebSqlException
	{

		LOG.info("Inside SupplierDAOImpl : savePlan ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierChoosePlan");
			simpleJdbcCall.returningResultSet("searchList", new BeanPropertyRowMapper<PlanInfo>(PlanInfo.class));
			final MapSqlParameterSource objParameter = new MapSqlParameterSource();

			for (PlanInfo planInfo : pInfoList)
			{

				objParameter.addValue("ManufacturerID", manufacturerID);
				objParameter.addValue("UserID", userId);
				objParameter.addValue("BillingPlanManufacturerIDs", planInfo.getProductId());

				if ((planInfo.getQuantity().isEmpty()) || (planInfo.getQuantity().equals("")))
				{

					objParameter.addValue("Quantitys", null);
				}
				else
				{
					objParameter.addValue("Quantitys", planInfo.getQuantity());
				}

				// objParameter.addValue("Quantity", planInfo.getQuantity());
				objParameter.addValue("TotalPrice", planInfo.getGrandTotal());
				objParameter.addValue("BillingPlanManufacturerIDs", planInfo.getProductId());
				/*
				 * objParameter.addValue("ACHorBankInformationFlag", false);
				 * objParameter.addValue("AccountTypeID", null);
				 * objParameter.addValue("BankName", null);
				 * objParameter.addValue("RoutingNumber", null);
				 * objParameter.addValue("AccountNumber", null);
				 * objParameter.addValue("AccountHolderName", null);
				 * objParameter.addValue("BillingAddress", null);
				 * objParameter.addValue("City", null);
				 * objParameter.addValue("State", null);
				 * objParameter.addValue("Zip", null);
				 * objParameter.addValue("PhoneNumber", null);
				 * objParameter.addValue("CreditCardFlag", null);
				 * objParameter.addValue("CreditCardNumber", null);
				 * objParameter.addValue("ExpirationDate", null);
				 * objParameter.addValue("CVV", null);
				 * objParameter.addValue("CardholderName", null);
				 * objParameter.addValue("CreditCardBillingAddress", null);
				 */objParameter.addValue("ManufacturerProductFeeFlag", 1);
				objParameter.addValue("DiscountPlanManufacturerID", discountPlanManufacturerID);

				resultFromProcedure = simpleJdbcCall.execute(objParameter);
			}

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : getHotDealByID  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebSupplierChoosePlan is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getHotDealByID : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}

		LOG.info("Choose plan saved::::::::::" + response);
		return response;
	}

	public List<Product> getPdtInfoForDealRerun(String productIds) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getPdtInfoForDealRerun ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<Product> listProduct = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierReRunHotDealDisplayProductInfo");
			simpleJdbcCall.returningResultSet("listProductInfo", new BeanPropertyRowMapper<Product>(Product.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("ProductID", productIds);
			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listProduct = (ArrayList) resultFromProcedure.get("listProductInfo");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getPdtInfoForDealRerun : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebSupplierReRunHotDealDisplayProductInfo is  executed Successfully.");
		return listProduct;
	}

	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getAppConfig ");
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside SupplierDAOImpl : getAppConfig : Error occurred in usp_GetScreenContent Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_GetScreenContent is  executed Successfully.");
		}
		catch (DataAccessException e)
		{
			LOG.error("Inside SupplierDAOImpl : getAppConfig : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}

	/**
	 * This method will return the time zones for add hot deal screen.
	 * 
	 * @return time zones List.
	 * @throws ScanSeeWebSqlException
	 *             Sql exception.
	 */

	@SuppressWarnings("unchecked")
	public ArrayList<TimeZones> getAllTimeZones() throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getAllTimeZones ");
		ArrayList<TimeZones> timeZoneslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveTimeZone");
			simpleJdbcCall.returningResultSet("TimeZoneslst", new BeanPropertyRowMapper<TimeZones>(TimeZones.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					timeZoneslst = (ArrayList<TimeZones>) resultFromProcedure.get("TimeZoneslst");
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside SupplierDAOImpl : getAppConfig : Error occurred in usp_GetScreenContent Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebRetrieveTimeZone is  executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : getAppConfig : " + exception);
			throw new ScanSeeWebSqlException(exception);
		}
		// TODO Auto-generated method stub
		return timeZoneslst;
	}

	/**
	 * This method will return the all Business Category List .
	 * 
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<Category> getAllBusinessCategory() throws ScanSeeWebSqlException
	{
		final String methodName = "getAllBusinessCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<Category> arCategoryList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveBusinessCategory");
			simpleJdbcCall.returningResultSet("categoryList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			LOG.info("Executing Store Procedure usp_WebRetrieveBusinessCategory");
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			arCategoryList = (ArrayList) resultFromProcedure.get("categoryList");
			Collections.sort(arCategoryList, new Comparator<Category>() {
				public int compare(Category o1, Category o2)
				{
					return o1.getParentSubCategory().compareToIgnoreCase(o2.getParentSubCategory());
				}
			});
			LOG.info("usp_WebRetrieveBusinessCategory is  executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : getAllBusinessCategory : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arCategoryList;
	}

	public ArrayList<PlanInfo> fetchAllFeeList()
	{
		final String methodName = "getAllFeeDetailsList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<PlanInfo> planList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierProductFeeDisplay");// usp_WebSupplierProductFeeDisplay
			simpleJdbcCall.returningResultSet("planFeeList", new BeanPropertyRowMapper<PlanInfo>(PlanInfo.class));

			final SqlParameterSource fetchFeeDetailsParameters = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchFeeDetailsParameters);
			planList = (ArrayList<PlanInfo>) resultFromProcedure.get("planFeeList");

			if (null != resultFromProcedure)
			{
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg)
				{
					planList = (ArrayList<PlanInfo>) resultFromProcedure.get("planFeeList");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : FetchAllPlanlist : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					// throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebSupplierProductFeeDisplay is  executed Successfully.");
		}
		catch (DataAccessException e)
		{
			// LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
			// e);
			// throw new ScanSeeException(e.getMessage());
			e.printStackTrace();
		}
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return planList;
	}

	public List<Product> fetchProductMedia(int productId) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : fetchProductMedia ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<Product> mediaList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductMedia");
			simpleJdbcCall.returningResultSet("ProdMediaList", new BeanPropertyRowMapper<Product>(Product.class));
			final MapSqlParameterSource objSearchmediaParameter = new MapSqlParameterSource();
			objSearchmediaParameter.addValue("ProductID", productId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchmediaParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != resultFromProcedure)
			{
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg)
				{
					mediaList = (List) resultFromProcedure.get("ProdMediaList");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : getRebatesByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebDisplayProductMedia is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : fetchProductMedia : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		return mediaList;
	}

	public List<ManageProducts> fetchProductPreviewList(Long productId) throws ScanSeeWebSqlException
	{

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<ManageProducts> prodList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductInfo");
			simpleJdbcCall.returningResultSet("ProdList", new BeanPropertyRowMapper<ManageProducts>(ManageProducts.class));
			final MapSqlParameterSource objSearchprodParameter = new MapSqlParameterSource();
			objSearchprodParameter.addValue("ProductID", productId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchprodParameter);


			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != resultFromProcedure)
			{
				String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				if (null == errorMsg)
				{
					prodList = (List) resultFromProcedure.get("ProdList");

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Inside SupplierDAOImpl : fetchProductPreviewList  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
			LOG.info("usp_WebDisplayProductInfo is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : fetchProductMedia : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		return prodList;

	}

	/**
	 * This method will return the all AccountType List .
	 * 
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<AccountType> getAllAcountType() throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getAllAcountType ");
		Map<String, Object> resultFromProcedure = null;
		ArrayList<AccountType> arAccountTypeList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveAccountType");
			simpleJdbcCall.returningResultSet("accountTypeList", new BeanPropertyRowMapper<AccountType>(AccountType.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			arAccountTypeList = (ArrayList) resultFromProcedure.get("accountTypeList");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : getAllAcountType : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebRetrieveAccountType is  executed Successfully.");
		return arAccountTypeList;
	}

	/**
	 * This method save Manufacturer Bank Information details.
	 * 
	 * @param manufacturerID
	 * @param userId
	 * @param objPlanInfo
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String ManufBankInfo(Long manufacturerID, Long userId, String isPaymentType, PlanInfo objPlanInfo) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : ManufBankInfo ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierPaymentInformation");
			final MapSqlParameterSource objBankPaymentInfo = new MapSqlParameterSource();
			// -- ACH Bank Information.
			if ("".equals(isPaymentType))
			{
				objBankPaymentInfo.addValue("ACHorBankInformationFlag", objPlanInfo.isAchOrBankInfoFlag());
				objBankPaymentInfo.addValue("BankName", objPlanInfo.getBankName());
				objBankPaymentInfo.addValue("AccountTypeID", objPlanInfo.getAccountType());
				objBankPaymentInfo.addValue("AccountNumber", objPlanInfo.getAccountNumber());
				objBankPaymentInfo.addValue("AccountHolderName", objPlanInfo.getAccountHolderName());
				// -- Bank General Information.
				objBankPaymentInfo.addValue("ManufacturerID", manufacturerID);
				objBankPaymentInfo.addValue("UserID", userId);
				objBankPaymentInfo.addValue("BillingAddress", objPlanInfo.getBillingAddress());
				objBankPaymentInfo.addValue("City", objPlanInfo.getCity());
				objBankPaymentInfo.addValue("State", objPlanInfo.getState());
				objBankPaymentInfo.addValue("Zip", objPlanInfo.getZip());
				objBankPaymentInfo.addValue("PhoneNumber", objPlanInfo.getPhoneNumber());
				resultFromProcedure = simpleJdbcCall.execute(objBankPaymentInfo);
			}
			else if ("".equals(isPaymentType))
			{
				objBankPaymentInfo.addValue("CreditCardFlag ", objPlanInfo.isCreditCardFlag());
				objBankPaymentInfo.addValue("CreditCardNumber", objPlanInfo.getCreditCardNumber());
				objBankPaymentInfo.addValue("ExpirationDate", String.valueOf(objPlanInfo.getYear()) + "-" + String.valueOf(objPlanInfo.getMonth())
						+ "-" + String.valueOf("01"));
				objBankPaymentInfo.addValue("CVV", objPlanInfo.getcVV());
				objBankPaymentInfo.addValue("CardholderName", objPlanInfo.getCardholderName());
				// -- Bank General Information.
				objBankPaymentInfo.addValue("ManufacturerID", manufacturerID);
				objBankPaymentInfo.addValue("UserID", userId);
				objBankPaymentInfo.addValue("BillingAddress", objPlanInfo.getBillingAddress());
				objBankPaymentInfo.addValue("City", objPlanInfo.getCity());
				objBankPaymentInfo.addValue("State", objPlanInfo.getState());
				objBankPaymentInfo.addValue("Zip", objPlanInfo.getZip());
				objBankPaymentInfo.addValue("PhoneNumber", objPlanInfo.getPhoneNumber());
				resultFromProcedure = simpleJdbcCall.execute(objBankPaymentInfo);
			}
			else if ("".equals(isPaymentType))
			{
				objBankPaymentInfo.addValue("AlternatePaymentFlag ", objPlanInfo.isAlternatePaymentFlag());
				objBankPaymentInfo.addValue("AccessCode", objPlanInfo.getAccessCode());
				resultFromProcedure = simpleJdbcCall.execute(objBankPaymentInfo);
			}
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : ManufBankInfo : " + exception.getMessage());
			if (null == responseFromProc && responseFromProc.intValue() != 0)
			{
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : ManufBankInfo   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebSupplierPaymentInformation is  executed Successfully.");
		return response;
	}

	public String addProductMedia(ManageProducts productInfoVO, long userId, int supplierId, int productId) throws ScanSeeWebSqlException

	{
		final String methodName = "AddProductMedia";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierManageProductsAddMedia");
			final MapSqlParameterSource objAddProductMediaParameter = new MapSqlParameterSource();
			objAddProductMediaParameter.addValue("UserId", userId);
			objAddProductMediaParameter.addValue("ManufacturerID", supplierId);
			objAddProductMediaParameter.addValue("ProductID", productId);
			objAddProductMediaParameter.addValue("Images", productInfoVO.getImagePaths());
			objAddProductMediaParameter.addValue("Audio", productInfoVO.getAudioFilePath());
			objAddProductMediaParameter.addValue("Video", productInfoVO.getVideoFilePath());

			resultFromProcedure = simpleJdbcCall.execute(objAddProductMediaParameter);

			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
				LOG.info("the responseFromProc is :" + responseFromProc);

			}
			else
			{
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : AddProductMedia  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebSupplierManageProductsAddMedia is  executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : AddProductMedia : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public ArrayList<AppConfiguration>  fetchReportServerConfiguration() throws ScanSeeWebSqlException
	{
		final String methodName = "fetchReportServerConfiguration";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveReportServerConfiguration");
			simpleJdbcCall.returningResultSet("appConfiguration", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			LOG.info("Executing Store procedure {}","usp_WebRetrieveReportServerConfiguration");
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			appConfigurationList = (ArrayList) resultFromProcedure.get("appConfiguration");
			LOG.info("usp_WebRetrieveReportServerConfiguration is  executed Successfully.");
			
			
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : getAllAcountType : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return appConfigurationList;
	}

	public List<Rebates> getProductList(String productName, int supplierId) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SuplierDaoImpl : getProductListForRebates ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		ArrayList<Rebates> arProdList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierRebateProductSearch");
			simpleJdbcCall.returningResultSet("searchProdcutList", new BeanPropertyRowMapper<Rebates>(Rebates.class));

			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();
			objCouponNameParameters.addValue("ManufacturerID", supplierId);
			objCouponNameParameters.addValue("Search", productName);

			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);
			arProdList = (ArrayList) resultFromProcedure.get("searchProdcutList");

			if (arProdList != null)
			{

				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + arProdList.size());
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info("Inside SupplierDAOImpl : getProductInformation  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebSupplierRebateProductSearch is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SuplierDaoImpl : getProductList : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return arProdList;

	}

	public List<Rebates> getProductInformation(int rebateId) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getProductInformation ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		List<Rebates> listRebProduct = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayRebate");
			simpleJdbcCall.returningResultSet("searchProductInfo", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("RebateID", rebateId);
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listRebProduct = (ArrayList) resultFromProcedure.get("searchProductInfo");
			if (listRebProduct != null)
			{

				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + listRebProduct.size());
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info("Inside SupplierDAOImpl : getProductInformation  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebDisplayRebate is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getProductInformation : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		return listRebProduct;
	}

	public List<HotDealInfo> getProductInfoDetails(Long supplierId, String productName) throws ScanSeeWebSqlException
	{

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<HotDealInfo> listProduct = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierProductSearch");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource objSearchParameter = new MapSqlParameterSource();
			objSearchParameter.addValue("ManufacturerID", supplierId);
			objSearchParameter.addValue("ProductName", productName);
			objSearchParameter.addValue("LowerLimit", 0);

			resultFromProcedure = simpleJdbcCall.execute(objSearchParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			listProduct = (ArrayList) resultFromProcedure.get("productDetails");

			if (listProduct != null)
			{
				LOG.info("the responseFromProc is :" + responseFromProc + "\t size :" + listProduct.size());
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				LOG.info("Inside SupplierDAOImpl : getProductInfoDetails  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebSupplierProductSearch is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : getProductInfoDetails : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());

		}
		return listProduct;
	}

	public List<Rebates> getProdImage(String scancodes) throws ScanSeeWebSqlException
	{

		LOG.info("Inside SupplierDAOImpl : getProdImage ");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		ArrayList<Rebates> arProdNameList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();

			simpleJdbcCall.withProcedureName("usp_WebPreviewProductDetails");
			simpleJdbcCall.returningResultSet("searchProdList", new BeanPropertyRowMapper<Rebates>(Rebates.class));
			objCouponNameParameters.addValue("Scancodes", scancodes);

			resultFromProcedure = simpleJdbcCall.execute(objCouponNameParameters);
			arProdNameList = (ArrayList<Rebates>) resultFromProcedure.get("searchProdList");

		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : getProdImage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebPreviewProductDetails is  executed Successfully.");
		return arProdNameList;
	}

	public ArrayList<ContactType> getAllContactTypes() throws ScanSeeWebSqlException
	{

		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		ArrayList<ContactType> contactTypes = new ArrayList<ContactType>();

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			final MapSqlParameterSource objCouponNameParameters = new MapSqlParameterSource();

			simpleJdbcCall.withProcedureName("usp_WebRetrieveContactTitle");
			simpleJdbcCall.returningResultSet("contactList", new BeanPropertyRowMapper<ContactType>(ContactType.class));

			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);
			contactTypes = (ArrayList<ContactType>) resultFromProcedure.get("contactList");

		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : getProdImage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		LOG.info("usp_WebRetrieveContactTitle is  executed Successfully.");
		return contactTypes;

	}

	public Users changePassword(Long userId, String password) throws ScanSeeWebSqlException
	{

		EncryptDecryptPwd enryptDecryptpwd;
		String encryptedpwd = null;
		Users userInfo = null;
		List<Users> userInfoLst = null;
		try
		{
			enryptDecryptpwd = new EncryptDecryptPwd();
			// converting the password in encrypted
			encryptedpwd = enryptDecryptpwd.encrypt(password);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ChangePassword");

			simpleJdbcCall.returningResultSet("userInfo", new BeanPropertyRowMapper<Users>(Users.class));
			final MapSqlParameterSource loginCredParams = new MapSqlParameterSource();
			loginCredParams.addValue("UserID", userId);
			loginCredParams.addValue("Password", encryptedpwd);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(loginCredParams);
			userInfoLst = (ArrayList<Users>) resultFromProcedure.get("userInfo");

		}
		catch (DataAccessException exception)
		{
			LOG.error("", exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error("", e);
			throw new ScanSeeWebSqlException(e.getMessage());

		}
		catch (NoSuchPaddingException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidKeyException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidAlgorithmParameterException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidKeySpecException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (IllegalBlockSizeException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (BadPaddingException e)
		{
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		LOG.info("usp_ChangePassword is  executed Successfully.");
		return userInfo;
	}

	/**
	 * This method checks username is valid or not. If username is valid
	 * passowrd will be send to user EmailId .
	 * 
	 * @param strUsername
	 *            .
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 */
	public String forgotPwd(String strUsername, Users objUsers) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : forgotPwd ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		String strEnryptPwd = null;
		String strautogenPwd = null;
		EncryptDecryptPwd enryptDecryptpwd = null;
		Integer responseFromProc = null;
		boolean isSuccess;
		try
		{
			enryptDecryptpwd = new EncryptDecryptPwd();
			// getting auto generator password and converting into encrypt
			strautogenPwd = Utility.randomString(5);
			strEnryptPwd = enryptDecryptpwd.encrypt(strautogenPwd);
			objUsers.setPassword(strautogenPwd);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUpdateUserPassword");
			simpleJdbcCall.returningResultSet("userInfo", new BeanPropertyRowMapper<Users>(Users.class));

			final MapSqlParameterSource objCheckUsernameParameter = new MapSqlParameterSource();
			objCheckUsernameParameter.addValue("UserName", strUsername);
			objCheckUsernameParameter.addValue("Password", strEnryptPwd);
			resultFromProcedure = simpleJdbcCall.execute(objCheckUsernameParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				isSuccess = (Boolean) resultFromProcedure.get("Success");
				if (isSuccess)
				{
					LOG.info("Inside SupplierDAOImpl : forgotPwd  : response :" + responseFromProc);
					objUsers.setUserName((String) resultFromProcedure.get("LoginName"));
					objUsers.setEmail((String) resultFromProcedure.get("ContactEmail"));
					objUsers.setFirstName((String) resultFromProcedure.get("FirstName"));
					response = ApplicationConstants.SUCCESS;
				}
				else
				{
					response = ApplicationConstants.FAILURE;
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : forgotPwd  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebUpdateUserPassword is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : forgotPwd : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return response;
	}

	/**
	 * This method is used to get Rejected records from the Products Attributes
	 * upload File.
	 * 
	 * @param iLogId
	 * @param iManufactureId
	 * @param lUserId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public List<ManageProducts> getDiscardedRecordsForAttributes(long lUserId, int iManufacturerID, ManageProducts objMgProducts)
			throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getDiscardedRecordsForAttributes");
		List<ManageProducts> arMgProductsList = new ArrayList<ManageProducts>();
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProcedure = null;
		int iLogId = objMgProducts.getLogId();
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUploadManufacturerDiscardedProductAttributesDisplayDetails");
			simpleJdbcCall.returningResultSet("discardedAttributeList", new BeanPropertyRowMapper<ManageProducts>(ManageProducts.class));

			final MapSqlParameterSource objMapSqlParameterSource = new MapSqlParameterSource();
			objMapSqlParameterSource.addValue("UploadManufacturerLogID", iLogId);
			objMapSqlParameterSource.addValue("ManufacturerID", iManufacturerID);
			objMapSqlParameterSource.addValue("UserID", lUserId);
			resultFromProcedure = simpleJdbcCall.execute(objMapSqlParameterSource);
			responseFromProcedure = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			arMgProductsList = (ArrayList) resultFromProcedure.get("discardedAttributeList");
			if (responseFromProcedure != null && responseFromProcedure.intValue() == 0)
			{
				objMgProducts.setContantEmail((String) resultFromProcedure.get("ContactEmail"));
				objMgProducts.setFirstName((String) resultFromProcedure.get("FirstName"));
			}
			LOG.info("usp_WebUploadManufacturerDiscardedProductAttributesDisplayDetails is  executed Successfully.");
		}
		catch (EmptyResultDataAccessException e)
		{
			LOG.info("Inside SupplierDAOImpl : getDiscardedRecordsForAttributes :" + e);
			throw new ScanSeeWebSqlException(e);
		}
		return arMgProductsList;
	}

	/**
	 * This method is used to get Rejected records from the Products Attributes
	 * upload File.
	 * 
	 * @param iLogId
	 * @param iManufactureId
	 * @param lUserId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public List<ManageProducts> getDiscardedRecordsForProducts(long lUserId, int iManufacturerID, ManageProducts objMgProducts)
			throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getDiscardedRecordsForProducts");
		List<ManageProducts> arMgProductsList = new ArrayList<ManageProducts>();
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProcedure = null;
		int iLogId = objMgProducts.getLogId();
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUploadManufacturerDiscardedProductsDisplayDetails");
			simpleJdbcCall.returningResultSet("discardedProductsList", new BeanPropertyRowMapper<ManageProducts>(ManageProducts.class));
			final MapSqlParameterSource objMapSqlParameterSource = new MapSqlParameterSource();
			objMapSqlParameterSource.addValue("UploadManufacturerLogID", iLogId);
			objMapSqlParameterSource.addValue("ManufacturerID", iManufacturerID);
			objMapSqlParameterSource.addValue("UserID", lUserId);
			resultFromProcedure = simpleJdbcCall.execute(objMapSqlParameterSource);
			responseFromProcedure = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			arMgProductsList = (ArrayList) resultFromProcedure.get("discardedProductsList");
			if (responseFromProcedure != null && responseFromProcedure.intValue() == 0)
			{
				objMgProducts.setContantEmail((String) resultFromProcedure.get("ContactEmail"));
				objMgProducts.setFirstName((String) resultFromProcedure.get("FirstName"));
			}
			LOG.info("usp_WebUploadManufacturerDiscardedProductsDisplayDetails is  executed Successfully.");
		}
		catch (EmptyResultDataAccessException e)
		{
			LOG.info("Inside SupplierDAOImpl : getDiscardedRecordsForProducts :" + e);
			throw new ScanSeeWebSqlException(e);
		}
		return arMgProductsList;
	}

	/**
	 * The dao Method is used to save the Supplier QR code
	 * 
	 * @param SupplierQRCodeData
	 * @return SupplierQRCodeResponse
	 * @throws ScanSeeWebSqlException
	 */
	public SupplierQRCodeResponse supplierQRCodeGeneration(SupplierQRCodeData objSupplierQRCodeData) throws ScanSeeWebSqlException
	{
		SupplierQRCodeResponse objSupplierQRCodeResponse = new SupplierQRCodeResponse();
		LOG.info("Inside SupplierDAOImpl : supplierQRCodeGeneration ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		boolean isSuccess;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSupplierProductQRCodeCreation");
			simpleJdbcCall.returningResultSet("supplierQRCode", new BeanPropertyRowMapper<SupplierQRCodeResponse>(SupplierQRCodeResponse.class));

			final MapSqlParameterSource objCheckUsernameParameter = new MapSqlParameterSource();
			objCheckUsernameParameter.addValue("UserID", objSupplierQRCodeData.getUserId());
			objCheckUsernameParameter.addValue("ProductID", objSupplierQRCodeData.getProductId());
			objCheckUsernameParameter.addValue("AudioID", objSupplierQRCodeData.getAudioID());
			objCheckUsernameParameter.addValue("VideoID", objSupplierQRCodeData.getVideoID());
			objCheckUsernameParameter.addValue("FileLink", objSupplierQRCodeData.getFilePath());
			resultFromProcedure = simpleJdbcCall.execute(objCheckUsernameParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				objSupplierQRCodeResponse.setProductPageID((Integer) resultFromProcedure.get("ProductPageID"));
				if (objSupplierQRCodeData.getAudioID() != null)
				{
					objSupplierQRCodeResponse.setAudioPageID((Integer) resultFromProcedure.get("AudioPageID"));
				}
				if (objSupplierQRCodeData.getVideoID() != null)
				{
					objSupplierQRCodeResponse.setVideoPageID((Integer) resultFromProcedure.get("VideoPageID"));
				}
				if (objSupplierQRCodeData.getFilePath() != null)
				{
					objSupplierQRCodeResponse.setFileLinkPageID((Integer) resultFromProcedure.get("FileLinkPageID"));
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : supplierQRCodeGeneration  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			LOG.info("usp_WebSupplierProductQRCodeCreation is  executed Successfully.");
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : supplierQRCodeGeneration : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}
		return objSupplierQRCodeResponse;

	}

	/**
	 * This method is used to deleted all the Products Attributes records in the
	 * staging table.
	 * 
	 * @param iManufacture
	 * @param iUserID
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteAllFromProductAttributesStageTable(int iManufacture, long iUserID) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : deleteAllFromProductsStageTable");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String strResponse = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDeleteProductAttributesStagingTable");
			final MapSqlParameterSource objDelProductsAttrParameter = new MapSqlParameterSource();

			objDelProductsAttrParameter.addValue("ManufacturerID", iManufacture);
			objDelProductsAttrParameter.addValue("UserID", iUserID);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsAttrParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : deleteAllFromProductsStageTable : " + exception);
			if (null == responseFromProc || responseFromProc.intValue() != 0)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside SupplierDAOImpl : deleteAllFromProductsStageTable : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			strResponse = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info("usp_WebDeleteProductAttributesStagingTable is  executed Successfully.");
		return strResponse;
	}

	/**
	 * This method is used to deleted all the Products records in the staging
	 * table.
	 * 
	 * @param iManufacture
	 * @param iUserID
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String deleteAllFromProductsStageTable(int iManufacture, long iUserID) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : deleteAllFromProductsStageTable");
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String strResponse = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDeleteProductStagingTable");
			final MapSqlParameterSource objDelProductsParameter = new MapSqlParameterSource();
			objDelProductsParameter.addValue("ManufacturerID", iManufacture);
			objDelProductsParameter.addValue("UserID", iUserID);

			resultFromProcedure = simpleJdbcCall.execute(objDelProductsParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			strResponse = ApplicationConstants.SUCCESS;
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside RetailerDAOImpl : deleteProductsFromLocHoldControl : " + exception);
			if (null == responseFromProc || responseFromProc.intValue() != 0)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Inside RetaierDAOImpl :deleteProductsFromLocHoldControl : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
			}
			strResponse = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		LOG.info("usp_WebDeleteProductStagingTable is  executed Successfully.");
		return strResponse;
	}

	/**
	 * This method is used to domain Name for Application configuration
	 * 
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<AppConfiguration> getAppConfigForGeneralImages(String configType) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getAppConfigForGeneralImages ");
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GeneralImagesGetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside SupplierDAOImpl : getAppConfigForGeneralImages : Error occurred in usp_GeneralImagesGetScreenContent Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_GeneralImagesGetScreenContent is  executed Successfully.");
		}
		catch (DataAccessException e)
		{
			LOG.error("Inside SupplierDAOImpl : getAppConfig : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}

	public Integer getStageTableLogId(int manufacturerID, Long userId, String fileName) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getStageTableLogId ");
		Integer logId = null;
		Integer status = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUploadManufacturerProductsLogCreation");
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("ManufacturerID", manufacturerID);
			objSearchRebParameter.addValue("UserID", userId);
			objSearchRebParameter.addValue("FileName", fileName);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);

			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0)
			{
				logId = (Integer) resultFromProcedure.get("ManufacturerLogID");
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside RetailerDAOImpl : getStageTableLogId : Error occurred in usp_WebUploadRetailerProductsLogCreationLogCreation Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebUploadManufacturerProductsLogCreation is  executed Successfully.");
		}
		catch (DataAccessException e)
		{
			LOG.error("Inside SupplierDAOImpl : getStageTableLogId : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return logId;
	}

	public String insertDiscardedProductse(ArrayList<ManageProducts> prodList, int manufacturerID, long userId, Integer logId, String fileName)
			throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : addBatchUploadProductsToStage ");
		String response = null;

		Object[] values = null;
		List<Object[]> batch = new ArrayList<Object[]>();

		try
		{
			SimpleJdbcTemplate simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			Calendar currentDate = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MMM/dd HH:mm:ss");
			String dateNow = formatter.format(currentDate.getTime());
			String other = null;

			for (ManageProducts objMgProducts : prodList)
			{
				values = new Object[] { logId, objMgProducts.getProductName(), objMgProducts.getScanCode(), objMgProducts.getScanType(),
						objMgProducts.getProdExpirationDate(), objMgProducts.getImagePath(), objMgProducts.getCategory(),
						objMgProducts.getShortDescription(), objMgProducts.getLongDescription(), objMgProducts.getModelNumber(),
						objMgProducts.getSuggestedRetPrice(), objMgProducts.getProductWeight(), objMgProducts.getWeightUnits(),
						objMgProducts.getManufProuductURL(), objMgProducts.getAudioMediaPath(), objMgProducts.getVideoMediaPath(),
						objMgProducts.getProductImagePath(), manufacturerID, userId, dateNow, other, fileName, objMgProducts.getErrMsg() };
				batch.add(values);
			}
			int[] updateCounts = simpleJdbcTemplate.batchUpdate(
					"INSERT INTO UploadManufacturerDiscardedProducts VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", batch);
			if (updateCounts.length != 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : addBatchUploadProductsToStage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : addBatchUploadProductsToStage : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception);
		}
		return response;
	}

	public String insertDiscardedAttributes(ManageProducts manageProducts, int manufacturerID, long userId, Integer logID)
			throws ScanSeeWebSqlException
	{
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MMM/dd HH:mm:ss");
		String dateNow = formatter.format(currentDate.getTime());
		LOG.info("Inside SupplierDAOImpl : addBatchUploadProductsToStage ");
		String response = null;

		Object[] values = null;
		List<Object[]> batch = new ArrayList<Object[]>();

		try
		{
			SimpleJdbcTemplate simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			for (ManageProducts objMgProducts : manageProducts.getInvalidProductList())
			{
				values = new Object[] { logID, objMgProducts.getScanCode(), objMgProducts.getAttributeName(), objMgProducts.getAttributeValue(),
						manageProducts.getAttributeuploadFile().getOriginalFilename(), objMgProducts.getErrMsg() };
				batch.add(values);
			}
			int[] updateCounts = simpleJdbcTemplate.batchUpdate("INSERT INTO UploadManufacturerDiscardedProductAttributes VALUES (?,?,?,?,?,?)",
					batch);
			if (updateCounts.length != 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside SupplierDAOImpl : uploadAttributeToStage : " + exception.getMessage());
		}
		catch (Exception exception)
		{
			LOG.error("Inside SupplierDAOImpl : uploadAttributeToStage : " + exception.getMessage());
		}
		return response;
	}

	public Integer getAttributStageTableLogId(int manufacturerID, Long userId, String fileName) throws ScanSeeWebSqlException
	{
		LOG.info("Inside SupplierDAOImpl : getStageTableLogId ");
		Integer logId = null;
		Integer status = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUploadManufacturerProductAttributesLogCreation");
			final MapSqlParameterSource objSearchRebParameter = new MapSqlParameterSource();
			objSearchRebParameter.addValue("ManufacturerID", manufacturerID);
			objSearchRebParameter.addValue("UserID", userId);
			objSearchRebParameter.addValue("ProductAttributeFileName", fileName);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objSearchRebParameter);

			status = (Integer) resultFromProcedure.get("Status");
			if (status == 0)
			{
				logId = (Integer) resultFromProcedure.get("ManufacturerLogID");
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Inside RetailerDAOImpl : getStageTableLogId : Error occurred in usp_WebUploadRetailerProductsLogCreationLogCreation Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			LOG.info("usp_WebUploadManufacturerProductAttributesLogCreation is  executed Successfully.");
		}
		catch (DataAccessException e)
		{
			LOG.error("Inside SupplierDAOImpl : getStageTableLogId : " + e.getMessage());
			throw new ScanSeeWebSqlException(e);
		}
		return logId;
	}
	
	public Map<String, Map<String, String>> getSupplierDiscountPlans() throws ScanSeeWebSqlException
	{
		Map<String, Map<String, String>> discountPlanList = new HashMap<String, Map<String, String>>();
		Map<String, String> discountPrice = null;

		List<Map<String, Object>> discountDetailsList = this.jdbcTemplate
				.queryForList("select ManufacturerDiscountPlanCode,DiscountPriceYearly,DiscountPriceMonthly from ManufacturerDiscountPlan where ActiveFlag = 1");
		Object monthlyDiscount = null;
		Object yearlyDiscount = null;
		Object discountCode = null;
		for (Map<String, Object> discountDetail : discountDetailsList)
		{

			discountPrice = new HashMap<String, String>();
			monthlyDiscount = discountDetail.get("DiscountPriceMonthly");
			if (null != monthlyDiscount)
			{
				discountPrice.put(MONTHLY, monthlyDiscount.toString());
			}
			yearlyDiscount = discountDetail.get("DiscountPriceYearly");
			if (null != yearlyDiscount)
			{
				discountPrice.put(YEARLY, yearlyDiscount.toString());
			}
			discountCode = discountDetail.get("ManufacturerDiscountPlanCode");
			if (null != discountCode)
			{
				discountPlanList.put(discountCode.toString().toUpperCase(), discountPrice);
			}
		}

		return discountPlanList;
	}

	public List<SearchZipCode> getZipStateCity(String zipCode) throws ScanSeeWebSqlException
	{
		List<SearchZipCode> zipCodes = null;	
		LOG.info("Inside SupplierDAOImpl : getZipStateCity methos");
			zipCodes = this.jdbcTemplate.query("select City, State,StateName,PostalCode from GeoPosition G,State S where G.State = s.Stateabbrevation and g.PostalCode like '"+zipCode+"%'", new RowMapper<SearchZipCode>() {
				public SearchZipCode mapRow(ResultSet rs, int rowNum) throws SQLException
				{
					final SearchZipCode zipCodeData = new SearchZipCode();
					zipCodeData.setCity(rs.getString(1));
					zipCodeData.setStateCode(rs.getString(2));
					zipCodeData.setStateName(rs.getString(3));
					zipCodeData.setZipCode(rs.getString(4));
					return zipCodeData;
				}
			});
		LOG.info("Existing SupplierDAOImpl : getZipStateCity methos");
		return zipCodes;
	}
	public List<SearchZipCode> getCityStateZip(String city) throws ScanSeeWebSqlException
	{
		List<SearchZipCode> cities = null;	
		LOG.info("Inside SupplierDAOImpl : getCityStateZip methos");
		cities = this.jdbcTemplate.query("select City, State,StateName,PostalCode from GeoPosition G,State S where G.State = s.Stateabbrevation and g.City like '"+city+"%'", new RowMapper<SearchZipCode>() {
				public SearchZipCode mapRow(ResultSet rs, int rowNum) throws SQLException
				{
					final SearchZipCode cityData = new SearchZipCode();
					cityData.setCity(rs.getString(1));
					cityData.setStateCode(rs.getString(2));
					cityData.setStateName(rs.getString(3));
					cityData.setZipCode(rs.getString(4));
					return cityData;
				}
			});
		LOG.info("Existing SupplierDAOImpl : getZipStateCity methos");
		return cities;
	}
	/**
     * 
      * @return
     * @throws ScanSeeWebSqlException
     */
     public ArrayList<DropDown> displayDropDown() throws ScanSeeWebSqlException
     {
           LOG.info("Inside SupplierDAOImpl : displayDropDown ");
           ArrayList<DropDown> dropDownList = null;
           try
           {
                 simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
                 simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
                 simpleJdbcCall.withProcedureName("usp_SupplierDropDownList");
                 simpleJdbcCall.returningResultSet("DropDownList", new BeanPropertyRowMapper<DropDown>(DropDown.class));
                 final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
                 final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
                 if (null != resultFromProcedure)
                 {
                             dropDownList = (ArrayList<DropDown>) resultFromProcedure.get("DropDownList");
                 }
                 LOG.info("usp_SupplierDropDownList is  executed Successfully.");
           }
           catch (DataAccessException e)
           {
                 LOG.error("Inside SupplierDAOImpl : displayDropDown : " + e);
                 throw new ScanSeeWebSqlException(e);
           }
           return dropDownList;
     }
}
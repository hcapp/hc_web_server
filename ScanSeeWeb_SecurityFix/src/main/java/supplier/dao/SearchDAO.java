package supplier.dao;

import java.util.ArrayList;
import java.util.List;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;

import common.exception.ScanSeeWebSqlException;
import common.pojo.HotDealInfo;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductReview;

public interface SearchDAO
{

	/**
	 * The DAO method for fetch the product details from the database based on
	 * the zipcode and UserType
	 * 
	 * @param zipcode
	 *            as request parameter.
	 * @param UserType
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object.
	 */

	public SearchResultInfo fetchAllProduct(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException;

	public ProductVO fetchProductInfo(Integer retailLocationID, Integer productID)throws ScanSeeWebSqlException;

	public SearchResultInfo searchHotDeals(SearchForm objForm, Users loginUser,int lowerLimit) throws ScanSeeWebSqlException;
   
	public SearchResultInfo getProductWithDeal(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException;
   
	public ProductVO fetchProductInfoWithDeals(Integer productID)throws ScanSeeWebSqlException;

	public HotDealInfo fetchDealsInfo(int hotDealID)throws ScanSeeWebSqlException;
	
	public List<ProductVO> fetchProductAssociatedWithDeals(int productID) throws ScanSeeWebSqlException;
	
	public List<ProductVO> fetchDealWithProductAssociated(Integer hotDealID)throws ScanSeeWebSqlException;
	
	/**
	 * method to get the Retailers from the database based on query parameters.
	 * 
	 * @param AreaInfoVO
	 *            area information Stored in the instance.
	 * @return FindNearByDetails 
	 *  		  instance containing list of retailers.
	 * @throws ScanSeeWebSqlException
	 *             throws if exception occurs
	 */

	FindNearByDetails fetchNearByInfo(AreaInfoVO objAreaInfoVO) throws ScanSeeWebSqlException;
	
	 public ExternalAPIInformation getExternalApiInfo(String moduleName) throws ScanSeeWebSqlException;
	 
	 public ProductDetail getProductDetails(String productId) throws ScanSeeWebSqlException;

	 /**
		 * The DAO method for fetching product reviews.
		 * 
		 * @param userId
		 *            as a request parameter
		 * @param productId
		 *            as a request parameter
		 * @return ProductReview list.
		 * @throws ScanSeeException
		 *             The exceptions are caught and a ScanSee Exception defined for
		 *             the application is thrown which is caught in the Controller
		 *             layer.
		 */

		public ArrayList<ProductReview> getProductReviews(Integer productId) throws ScanSeeWebSqlException;
}

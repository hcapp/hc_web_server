package supplier.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ManageProducts;
import common.pojo.Users;
import common.util.Utility;

/**
 * BatchUploadController is a controller class for insert  Batch Upload like Products, Images, Video, Audio and Attributes.
 * 
 * @author Created by SPAN.
 */
@Controller
public class BatchUploadController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BatchUploadController.class);

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "batchupload";

	/**
	 * The batch Upload Page for method to show product, product images, audio, video files and attribute information.
	 * 
	 * @param request HttpServletRequest instance as parameter.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session as request parameter.
	 * @param model as request parameter.
	 * @return VIEW_NAME batchupload view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/batchupload.htm", method = RequestMethod.GET)
	public final String showbatchUploadPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadController : showbatchUploadPage ");
		final ManageProducts manageProducts = new ManageProducts();
		model.put("batchUploadform", manageProducts);
		return VIEW_NAME;
	}

	/**
	 * This controller will insert batch upload using .csv file for product, product images, audio, video files and attribute information and  call service and dao methods.
	 * 
	 * @param manageProducts ManageProducts instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as parameter.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session as request parameter.
	 * @return VIEW_NAME batchupload view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/savebatchupload.htm", method = RequestMethod.POST)
	public final ModelAndView saveBatchUpload(@ModelAttribute("batchUploadform") ManageProducts manageProducts, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadController : saveBatchUpload ");
		String isDataInserted = null;
		String strResponse = null;
		String autoGenKey = null;
		/* To avoid On F5 key pressed, ctrl + r, right click select reload, product, product images, audio, video files and attribute information upload will happen repeatedly. */
		final boolean bKeyStatus = Utility.isKeySame(session, manageProducts.getKey());
		if (bKeyStatus) 
		{
			String isAttributeInserted = null;
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			final int iManufacturerID = loginUser.getSupplierId();
			final long userId = loginUser.getUserID();
			LOG.info("\nManufacturerID -->" + iManufacturerID + "\nuserId -->" + userId);
			try
			{
				final CommonsMultipartFile productuploadFilePath = manageProducts.getProductuploadFilePath();
				final long productFileSize = productuploadFilePath.getSize();
				LOG.info("Product File Size -->" + productFileSize);
				if (productFileSize <= (1024 * 1024))
				{
					manageProducts.setDiscardedRecordsFIleName(request.getRealPath(iManufacturerID + "_Product.csv"));
					isDataInserted = supplierService.saveBatchFile(manageProducts, iManufacturerID, userId);
					LOG.info("isDataInserted -->" + isDataInserted);
					if (isDataInserted != null && isDataInserted.equals(ApplicationConstants.SUCCESS))
					{
						LOG.info("Inside BatchUploadController : Product BatchUpload : Uploaded Successfully");
						result.rejectValue("productuploadFilePath", "Product(s) Uploaded Successfully", "  Product(s) Uploaded Successfully");
						request.setAttribute("productuploadFile", "font-weight:bold;color:green;");
					}
					else if (isDataInserted != null && isDataInserted.equals(ApplicationConstants.RECORDSDISCARDED))
					{
						LOG.info("Product(s) information uploaded partially, please check your mail box for rejected Product(s)");
						result.rejectValue("productuploadFilePath",
								"  Product(s) information uploaded partially, please check your mail box for rejected Product(s).",
						"Product(s) information uploaded partially, please check your mail box for rejected Product(s).");
						request.setAttribute("productuploadFile", "font-weight:bold;color:green;");
					}
					else
					{
						LOG.error("Error occurred in uploadProductFile");
						result.rejectValue("productuploadFilePath", "Error Occurred while uploading Products",
						"  Error Occurred while uploading Products");
						request.setAttribute("productuploadFile", "font-weight:bold;color:red");
					}
				}
				else
				{
					result.rejectValue("productuploadFilePath", "Maximum size of uploaded file should be less than 1MB",
					"  Maximum size of uploaded file should be less than 1MB");
					request.setAttribute("productuploadFile", "font-weight:bold;color:red;");
				}
			}
			catch (ScanSeeServiceException exception)
			{
				LOG.error("Inside BatchUploadController : saveBatchUpload : " + exception.getMessage());
				LOG.info("Inside BatchUploadController : saveBatchUpload : " + exception.getMessage());
				result.rejectValue("productuploadFilePath", "Error Occurred while uploading Product(s)", "  Error Occurred while uploading Product(s)");
				request.setAttribute("productuploadFile", "font-weight:bold;color:red");
			}
			finally
			{
				try
				{
					final CommonsMultipartFile attributeuploadFile = manageProducts.getAttributeuploadFile();
					final long attributeFileSize = attributeuploadFile.getSize();
					LOG.info("Attribute File Size -->" + attributeFileSize);
					// Checking File Size,size should be less than 1MB
					if (attributeFileSize <= (1024 * 1024))
					{
						manageProducts.setDiscardedAttrRecordsFIleName(request.getRealPath(iManufacturerID + "_Attributes.csv"));
						isAttributeInserted = supplierService.uploadAttributeFile(manageProducts, iManufacturerID, userId);
						LOG.info("isAttributeInserted --> " + isAttributeInserted);
						if (isAttributeInserted != null && isAttributeInserted.equals(ApplicationConstants.SUCCESS))
						{
							LOG.info("Product(s) Uploaded Successfully");
							result.rejectValue("attributeuploadFile", "   Product(s) Attribute�s information Uploaded Successfully",
							"  Product(s) Attribute�s information Uploaded Successfully");
							request.setAttribute("attributeupload", "font-weight:bold;color:green;");
						}
						else if (isAttributeInserted != null && isAttributeInserted.equals(ApplicationConstants.RECORDSDISCARDED))
						{
							LOG.info("Product(s) Attribute�s information uploaded partially, please check your mail box for rejected Product(s)");
							result.rejectValue("attributeuploadFile",
									"  Product(s) Attribute�s information uploaded partially, please check your mail box for rejected Product(s).",
							"Product(s) Attribute�s information partially, please check your mail box for rejected Product(s).");
							request.setAttribute("attributeupload", "font-weight:bold;color:green;");
						}
						else
						{
							LOG.error("Error occurred in uploadProductFile");
							result.rejectValue("attributeuploadFile", "Error Occurred while uploading Product(s) Attributes",
							"  Error Occurred while uploading Products");
							request.setAttribute("attributeupload", "font-weight:bold;color:red");
						}
					}
					else
					{
						result.rejectValue("attributeuploadFile", "Maximum size of uploaded file should be less than 1MB",
						"  Maximum size of uploaded file should be less than 1MB");
						request.setAttribute("attributeupload", "font-weight:bold;color:red;");
					}
				}
				catch (ScanSeeServiceException exception)
				{
					LOG.error("Inside BatchUploadController : saveBatchUpload : finally : " + exception.getMessage());
					LOG.info("Inside BatchUploadController : saveBatchUpload : finally : " + exception.getMessage());
					result.rejectValue("attributeuploadFile", "Error Occurred while uploading Products Attributes",
					"  Error Occurred while uploading Products Attributes");
					request.setAttribute("attributeupload", "font-weight:bold;color:red");
				}
				try
				{
					strResponse = supplierService.uploadMediaFiles(manageProducts, loginUser.getSupplierId());
					if (strResponse.equals(ApplicationConstants.SUCCESS))
					{
						result.rejectValue("imageUploadFilePath", "         Media file(s) Uploaded Successfully", "         Media file(s) Uploaded Successfully");
						request.setAttribute("imageUploadFile", "font-weight:bold;color:green");
					}
					else if (strResponse.equals(ApplicationConstants.FAILURE))
					{
						result.rejectValue("imageUploadFilePath", "  Error Occurred while uploading media files",
						"  Error Occurred while uploading media files");
						request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
					}
					else
					{
						result.rejectValue("imageUploadFilePath", "   " + strResponse, "   " + strResponse);
						request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
					}
				}
				catch (ScanSeeServiceException e)
				{
					result.rejectValue("imageUploadFilePath", "  Error Occurred while uploading media files");
					request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
				}
				autoGenKey = Utility.randomString(5);
				session.setAttribute("autoGenKey", autoGenKey);
				return new ModelAndView(VIEW_NAME);
			}
		}
		/* End - To avoid On F5 key pressed, ctrl + r, right click select reload, product, product images, audio, video files and attribute information upload will happen repeatedly. */
		return new ModelAndView(VIEW_NAME);
	}

	/**
	 * This controller will insert batch upload using .csv file for product, product images, audio, video files and attribute information and  call service and dao methods.
	 * 
	 * @param manageProducts ManageProducts instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as parameter.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session as request parameter.
	 * @return VIEW_NAME batchupload view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/uploadproduct.htm", method = RequestMethod.POST)
	public final ModelAndView uploadProductFile(@ModelAttribute("batchUploadform") ManageProducts manageProducts, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadController : uploadProductFile");
		String isDataInserted = null;
		/* To avoid On F5 key pressed, ctrl + r, right click select reload, product information upload will happen repeatedly. */
		final boolean bKeyStatus = Utility.isKeySame(session, manageProducts.getKey());
		if (bKeyStatus) {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			final int iManufacturerID = loginUser.getSupplierId();
			final long userId = loginUser.getUserID();
			final String productFileName = manageProducts.getProductuploadFilePath().getOriginalFilename();
			final String attributeFileName = manageProducts.getAttributeuploadFile().getOriginalFilename();
			LOG.info("\nManufacturerID -->" + iManufacturerID + "\nUserID -->" + userId + "\nProductFileName -->" + productFileName
					+ "\nAttributeFileName -->" + attributeFileName);
			String autoGenKey = null;
			try
			{
				final CommonsMultipartFile productuploadFilePath = manageProducts.getProductuploadFilePath();
				final long productFileSize = productuploadFilePath.getSize();
				LOG.info("File Size -->" + productFileSize);
				// Checking File Size,size should be less than 1MB
				if (productFileSize <= (1024 * 1024))
				{
					manageProducts.setDiscardedRecordsFIleName(request.getRealPath(iManufacturerID + "_Product.csv"));
					isDataInserted = supplierService.saveBatchFile(manageProducts, iManufacturerID, userId);
					if (isDataInserted != null && isDataInserted.equals(ApplicationConstants.SUCCESS))
					{
						LOG.info("Product(s) Uploaded Successfully");
						result.rejectValue("productuploadFilePath", "Product(s) Uploaded Successfully", "  Product(s) Uploaded Successfully");
						request.setAttribute("productuploadFile", "font-weight:bold;color:green;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					}
					else if (isDataInserted != null && isDataInserted.equals(ApplicationConstants.RECORDSDISCARDED))
					{
						LOG.info("Product(s) information uploaded partially, please check your mail box for rejected Product(s)");
						result.rejectValue("productuploadFilePath",
								"  Product(s) information uploaded partially, please check your mail box for rejected Product(s).",
						"Product(s) information uploaded partially, please check your mail box for rejected Product(s).");
						request.setAttribute("productuploadFile", "font-weight:bold;color:green;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					}
					else
					{
						LOG.error("Error occurred in uploadProductFile");
						result.rejectValue("productuploadFilePath", "Error Occurred while uploading Products",
						"  Error Occurred while uploading Products");
						request.setAttribute("productuploadFile", "font-weight:bold;color:red");
					}
				}
				else
				{
					result.rejectValue("productuploadFilePath", "Maximum size of uploaded file should be less than 1MB",
					"  Maximum size of uploaded file should be less than 1MB");
					request.setAttribute("productuploadFile", "font-weight:bold;color:red;");
				}
			}
			catch (ScanSeeServiceException exception)
			{
				LOG.error("Error occurred in uploadProductFile", exception);
				result.rejectValue("productuploadFilePath", "Error Occurred while uploading Products", "  Error Occurred while uploading Products");
				request.setAttribute("productuploadFile", "font-weight:bold;color:red");
				return new ModelAndView(VIEW_NAME);
			}
		}
		/* To avoid On F5 key pressed, ctrl + r, right click select reload, product information upload will happen repeatedly. */
		return new ModelAndView(VIEW_NAME);
	}

	/**
	 * This controller will insert product images, audio, video files and attribute information and  call service and dao methods.
	 * 
	 * @param manageProducts ManageProducts instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as parameter.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session as request parameter.
	 * @return VIEW_NAME batchupload view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/uploadimage.htm", method = RequestMethod.POST)
	public final ModelAndView uploadImageFile(@ModelAttribute("batchUploadform") ManageProducts manageProducts, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadController : uploadImageFile");
		String strResponse = null;
		String autoGenKey = null;
		/* On F5 key pressed, ctrl + r, right click select reload, product images, audio, video files upload will happen repeatedly. */
		final boolean bKeyStatus = Utility.isKeySame(session, manageProducts.getKey());
		if (bKeyStatus) {
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			try
			{
				strResponse = supplierService.uploadMediaFiles(manageProducts, loginUser.getSupplierId());
				if (strResponse.equals(ApplicationConstants.SUCCESS))
				{
					result.rejectValue("imageUploadFilePath", "          Media file(s) Uploaded Successfully", "           Media file(s) Uploaded Successfully");
					request.setAttribute("imageUploadFile", "font-weight:bold;color:green");
					autoGenKey = Utility.randomString(5);
					session.setAttribute("autoGenKey", autoGenKey);
				}
				else if (strResponse.equals(ApplicationConstants.FAILURE))
				{
					result.rejectValue("imageUploadFilePath", "  Error Occurred while uploading media files",
					"  Error Occurred while uploading media files");
					request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
					autoGenKey = Utility.randomString(5);
					session.setAttribute("autoGenKey", autoGenKey);
				} else {
					result.rejectValue("imageUploadFilePath", "   " + strResponse, "   " + strResponse);
					request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
				}
			}
			catch (ScanSeeServiceException e)
			{
				result.rejectValue("imageUploadFilePath", "  Error Occurred while uploading media files");
				request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
			}
		}
		/* End : On F5 key pressed, ctrl + r, right click select reload, product images, audio, video files upload will happen repeatedly. */
		return new ModelAndView(VIEW_NAME);
	}

	/**
	 * This controller will insert batch upload using .csv file for attribute information and  call service and dao methods.
	 * 
	 * @param manageProducts ManageProducts instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as parameter.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session as request parameter.
	 * @return VIEW_NAME batchupload view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/uploadattribute.htm", method = RequestMethod.POST)
	public final ModelAndView uploadAttribute(@ModelAttribute("batchUploadform") ManageProducts manageProducts, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadController : uploadAttribute");
		String isAttributeInserted = null;
		String autoGenKey = null;
		/* To avoid On F5 key pressed, ctrl + r, right click select reload, attribute information upload will happen repeatedly. */
		final boolean bKeyStatus = Utility.isKeySame(session, manageProducts.getKey());
		if (bKeyStatus) 
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			final int iManufacturerID = loginUser.getSupplierId();
			final long userId = loginUser.getUserID();
			try
			{
				final String productFileName = manageProducts.getProductuploadFilePath().getOriginalFilename();
				final String attributeFileName = manageProducts.getAttributeuploadFile().getOriginalFilename();
				LOG.info("manufacturerID -->" + iManufacturerID + "\nuserId -->" + userId + "\nproductFileName -->" + productFileName
						+ "\nattributeFileName -->" + attributeFileName);
				final CommonsMultipartFile attributeuploadFile = manageProducts.getAttributeuploadFile();
				final long attributeFileSize = attributeuploadFile.getSize();
				LOG.info("File Size -->" + attributeFileSize);
				// Checking File Size,size should be less than 1MB
				if (attributeFileSize <= (1024 * 1024))
				{
					manageProducts.setDiscardedAttrRecordsFIleName(request.getRealPath(iManufacturerID + "_Attributes.csv"));
					isAttributeInserted = supplierService.uploadAttributeFile(manageProducts, iManufacturerID, userId);

					if (isAttributeInserted != null && isAttributeInserted.equals(ApplicationConstants.SUCCESS))
					{
						LOG.info("Product(s) Uploaded Successfully");
						result.rejectValue("attributeuploadFile", "   Product(s) Attribute�s information Uploaded Successfully",
						"  Product(s) Attribute�s information Uploaded Successfully");
						request.setAttribute("attributeupload", "font-weight:bold;color:green;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					}
					else if (isAttributeInserted != null && isAttributeInserted.equals(ApplicationConstants.RECORDSDISCARDED))
					{
						LOG.info("Product(s) Attribute�s information uploaded partially, please check your mail box for rejected Product(s)");
						result.rejectValue("attributeuploadFile",
								"  Product(s) Attribute�s information uploaded partially, please check your mail box for rejected Product(s).",
						"Product(s) Attribute�s information partially, please check your mail box for rejected Product(s).");
						request.setAttribute("attributeupload", "font-weight:bold;color:green;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					}
					else
					{
						LOG.error("Error occurred in uploadProductFile");
						result.rejectValue("attributeuploadFile", "Error Occurred while uploading Product(s) Attributes",
						"  Error Occurred while uploading Products");
						request.setAttribute("attributeupload", "font-weight:bold;color:red");
					}
				}
				else
				{
					result.rejectValue("attributeuploadFile", "Maximum size of uploaded file should be less than 1MB",
					"  Maximum size of uploaded file should be less than 1MB");
					request.setAttribute("attributeupload", "font-weight:bold;color:red;");
				}
			}
			catch (ScanSeeServiceException e)
			{
				result.rejectValue("attributeuploadFile", "Error Occurred while uploading Products Attributes",
				"  Error Occurred while uploading Products Attributes");
				request.setAttribute("attributeupload", "font-weight:bold;color:red");
			}
			supplierService.deleteAllFromProductAttributesStageTable(iManufacturerID, userId);
		}
		/* End : To avoid On F5 key pressed, ctrl + r, right click select reload, attribute information upload will happen repeatedly. */
		return new ModelAndView(VIEW_NAME);
	}
}

package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;
import supplier.validator.ManageProductValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfoVO;
import common.pojo.Users;
import common.util.Utility;

/**
 *  This Controller is used for serving the request for add product screen.
 * @author manjunatha_gh
 *
 */
@Controller
public class AddProductsController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AddProductsController.class);
	
	/**
	 * Variable for Add product page.
	 */
	final String ADDPRODUCTVIEW = "productInfo";
	
	/**
	 * Manage product screen validator.
	 */
	ManageProductValidator manageProductValidator;
	
	/**
	 * Autowired method for setting ManageProductValidator object.
	 * @param manageProductValidator.
	 */
	@Autowired
	public void setManageProductValidator(ManageProductValidator manageProductValidator)
	{
		this.manageProductValidator = manageProductValidator;
	}
	/**
	 * This Method redirects user to Add product page.
	 * @param request HttpServletRequest object.
	 * @param model  ModelMap object .
	 * @param session HttpSession
	 * @return View name for Add product page.
	 * @throws ScanSeeServiceException any exception while processing the request.
	 */
	@RequestMapping(value = "/upload.htm", method = RequestMethod.GET)
	public String showUploadForm(HttpServletRequest request, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		final String methodName = "showUploadForm";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strProductImage = null;
		try
		{
			session.removeAttribute("imageScr");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

			List<Category> categories = null;
			categories = supplierService.getAllBusinessCategory();
			session.setAttribute("categoryList", categories);
			strProductImage = (String) session.getAttribute("imageScr");
			if ("".equals(Utility.checkNull(strProductImage)))
			{
				session.setAttribute("imageScr", ApplicationConstants.ALTERNATEIMG);
			}
			// Loading set of attribute list to Attributes Drop down
			List<ProductAttributes> attrList = supplierService.getMasterAttributesList();
			session.setAttribute("attrList", attrList);
			model.put("productInfoVO", new ProductInfoVO());
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "upload";
	}
	
	/**
	 *  This method validates product information entered by the user if all info is valid then saves to db then redirect to add product page.
	 *  if validation fails then redirect to add product page with validation error messages.
	 * @param productInfoVO ProductInfoVO which contains product information.
	 * @param model ModelMap object.
	 * @param request HttpServletRequest object.
	 * @param response HttpServletResponse object.
	 * @param session HttpSession object.
	 * @return String xml value which contains Success or Failure response.
	 * @throws ScanSeeServiceException Generic  exception if any while processing request.
	 * @throws IOException may be thrown while getting writer from response.
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/upload.htm", method = RequestMethod.POST)
	public String addDashProduct(@ModelAttribute("productInfoVO") ProductInfoVO productInfoVO, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, IOException
	{
		final String methodName = "addDashProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		session.removeAttribute("imageScr");
		String realPath = request.getRealPath("");
		String status = "success";
		String strProductImage = null;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");
		try
		{
			strProductImage = (String) session.getAttribute("imageScr");
			if (Utility.checkNull(strProductImage).equals(""))
			{
				session.setAttribute("imageScr", ApplicationConstants.ALTERNATEIMG);
			}

			status = supplierService.addProduct(productInfoVO, realPath, loginUser);
			// below line used To show Status message.
			model.put("productInfoVO", new ProductInfoVO());
			session.setAttribute("imageScr", "");

			if (status.equals(ApplicationConstants.PRODUCTEXISTS))
			{
				response.getWriter().write("<responseText>" + "Product UPC code  Exists Already" + "</responseText>");
			}
			else
			{
				response.getWriter().write("<responseText>" + "Success" + "</responseText>");
			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error("Exception occurred in showUploadForm:::::" + exception.getMessage());
			response.getWriter().write("<responseText>" + "Error occurred while adding product." + "</responseText>");
			return null;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return null;

	}
	/**
	 * this  method saves the image to temporary path.
	 * @param productInfoVO ProductInfoVO which contains image file.
	 * @param request HttpServletRequest
	 * @param session HttpSession
	 * @return ModelAndView object containing View details.
	 * @throws ScanSeeServiceException any exception while processing the request.
	 */
	@RequestMapping(value = "/uploadDashboardtempimg.htm", method = RequestMethod.POST)
	public ModelAndView onSubmitImage(@ModelAttribute("productInfoVO") ProductInfoVO productInfoVO, HttpServletRequest request,
			 HttpSession session) throws ScanSeeServiceException
	{

		String filePathImages = request.getRealPath("") + "/images/" + productInfoVO.getImageFile()[0].getOriginalFilename();
		LOG.info("file Path" + filePathImages);
		Utility.writeFileData(productInfoVO.getImageFile()[0], filePathImages);

		session.setAttribute("imageScr", "images/" + productInfoVO.getImageFile()[0].getOriginalFilename());
		return new ModelAndView("upload");

	}
}

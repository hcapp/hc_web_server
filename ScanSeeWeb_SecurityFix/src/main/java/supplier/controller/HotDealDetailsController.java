package supplier.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.exception.ScanSeeServiceException;
import common.pojo.Rebates;
import common.pojo.Users;
import common.util.Utility;

@Controller
@RequestMapping("/dailyHotDeals.htm")
public class HotDealDetailsController {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory
			.getLogger(HotDealDetailsController.class);
	
	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "rerunRebate";
	
	/**
	 * Variable VIEW_NAME declared as String.
	 */
	String view = VIEW_NAME;
	

	@RequestMapping(method = RequestMethod.GET)
	public String showPage(HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1) throws ScanSeeServiceException {
		LOG.info("Controller Layer:: Inside Get Method");
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext
				.getBean("supplierService");
		Users user = (Users) request.getSession().getAttribute("loginuser");
		Long userId = user.getUserID();
		String rebateId = request.getParameter("rebateID");
		int rebId = Integer.parseInt(rebateId);
		Rebates rebatesVo = new Rebates();
		List<Rebates>  rebatesList  = (List<Rebates>)supplierService.getRebatesForDisplay(rebId);
		if(rebatesList != null && rebatesList.size() >0){
			for(int i=0;i<rebatesList.size();i++){
				rebatesVo.setRebName(rebatesList.get(i).getRebName());
				rebatesVo.setRebAmount(rebatesList.get(i).getRebAmount());
				rebatesVo.setRebLongDescription(rebatesList.get(i).getRebLongDescription());
				rebatesVo.setRebStartDate(rebatesList.get(i).getRebStartDate());
				rebatesVo.setRebEndDate(rebatesList.get(i).getRebEndDate());
				rebatesVo.setRebStartTime(rebatesList.get(i).getRebStartTime());
				
				rebatesVo.setRebEndTime(rebatesList.get(i).getRebEndTime());
				rebatesVo.setRebTermCondtn(rebatesList.get(i).getRebTermCondtn());
			}
			model.put("reRunRebatesForm", rebatesVo);
		}else{
			model.put("reRunRebatesForm", rebatesVo);		
		}
		LOG.info("Controller Layer:: Exit Get Method");
		return "rerunRebate";
	}

	@RequestMapping(value = "/dailyHotDeals.htm", method = RequestMethod.POST)
	public ModelAndView editRebates(@ModelAttribute("reRunRebatesForm") Rebates rebatesVo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException {

      try {
    	 
		String isDataInserted = null;
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext
				.getBean("supplierService");
		///int rebId = Integer.parseInt(rebateId);
		Users user = (Users) request.getSession().getAttribute("loginuser");
		
		String startDate = rebatesVo.getRebStartDate();
   	    String sDate = Utility.getFormattedDate(startDate);
        rebatesVo.setRebStartDate(sDate);
        
        
        String endDate = rebatesVo.getRebEndDate();
        String eDate =  Utility.getFormattedDate(endDate);
        rebatesVo.setRebEndDate(eDate);
        
        String stTime = rebatesVo.getRebStartTime();
        stTime = stTime.replace(',' , ':');
        rebatesVo.setRebStartTime(stTime);
        
        String endTime = rebatesVo.getRebEndTime();
        endTime = endTime.replace(',' , ':');
        rebatesVo.setRebEndTime(endTime);
  		/*String startDate = rebatesVo.getRebStartDate();
  		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = (Date)formatter .parse(startDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(convertedDate);
        cal.add(Calendar.HOUR, rebatesVo.getStartHours());
        cal.add(Calendar.MINUTE, rebatesVo.getStartMinutes());
        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        String sDate = formatter1.format(cal.getTime());
        rebatesVo.setRebStartDate(sDate);
        Date convertedDate1 = (Date)formatter .parse(startDate);
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(convertedDate1);
        cal1.add(Calendar.HOUR, rebatesVo.getEndHours());
        cal1.add(Calendar.MINUTE, rebatesVo.getEndMinutes());
        DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        String eDate = formatter2.format(cal.getTime());
        rebatesVo.setRebEndDate(eDate);*/
		
        rebatesVo.setUserID(user.getUserID());
        isDataInserted = supplierService.addRebates(rebatesVo);

		if (result.hasErrors()|| isDataInserted == null) 
		{
			request.getSession().setAttribute("message",
			"Error while re-run");
			view = VIEW_NAME;

		} else {
			view = VIEW_NAME;
			request.getSession().setAttribute("message",
			"re-run executed successfully");
		}
      }catch(ScanSeeServiceException e)
      {
    	  LOG.error("Exception occurred in  editRebates:::::"+ e.getMessage());
          throw new ScanSeeServiceException(e);
      } catch (ParseException e) {
    	  LOG.error("Exception occured while parsing date.");
		e.printStackTrace();
	}

		return new ModelAndView(view);
	}
}
	
	


package supplier.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;
import supplier.validator.ManageProductValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfoVO;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class RegistrationProdSetUp
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RegistrationProdSetUp.class);
	final String ADDPRODUCTVIEW = "productInfo";
	ManageProductValidator manageProductValidator;

	@Autowired
	public void setManageProductValidator(ManageProductValidator manageProductValidator)
	{
		this.manageProductValidator = manageProductValidator;
	}

	@RequestMapping(value = "/registprodsetup.htm", method = RequestMethod.GET)
	public String showUploadForm(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Controller Layer:: Inside Get Method");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ArrayList<Category> categories = null;
		String strProductImage = null;
		try
		{
			categories = supplierService.getAllBusinessCategory();
			session.setAttribute("categoryList", categories);
			strProductImage = (String) session.getAttribute("logosrc");
			if (Utility.checkNull(strProductImage).equals(""))
			{
				session.setAttribute("imageScr", ApplicationConstants.ALTERNATEIMG);
			}
			// Loading set of attribute list to Attributes Drop down
			List<ProductAttributes> attrList = supplierService.getMasterAttributesList();
			session.setAttribute("attrList", attrList);
			model.addAttribute(new ProductInfoVO());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showUploadForm:::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Controller Layer:: Inside Exit Get Method");
		return "registprodsetup";
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/registprodsetup.htm", method = RequestMethod.POST)
	public ModelAndView addProductRegiStrn(@ModelAttribute("productInfoVO") ProductInfoVO productInfoVO, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception, ScanSeeServiceException
	{
		LOG.info("Entering in to Upload Controller......");
		final String methodName = "addProductRegiStrn";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		String realPath = request.getRealPath("");
		String view = "registprodsetup";
		String status = ApplicationConstants.FAILURE;
		request.getSession().removeAttribute("imageScr");
		String strProductImage = null;
		LOG.info("Button IS" + productInfoVO.getBtnType());
		try
		{
			strProductImage = (String) session.getAttribute("logosrc");
			if (Utility.checkNull(strProductImage).equals(""))
			{
				session.setAttribute("imageScr", ApplicationConstants.ALTERNATEIMG);
			}
			manageProductValidator.validate(productInfoVO, result);
			if (result.hasErrors())
			{
				status = ApplicationConstants.FAILURE;
				view = "registprodsetup";
				productInfoVO.setBtnType("Save");
			}
			else
			{
				status = supplierService.addProduct(productInfoVO, realPath, loginUser);
				model.put("productInfoVO", new ProductInfoVO());
				if (status.equals(ApplicationConstants.PRODUCTEXISTS))
				{
					request.setAttribute("productexists", "Product UPC code  Exists Already.");
					request.setAttribute("fontStyle", "font-weight:bold;color:#00559c;");
				}
				else
				{
					request.setAttribute("produstatus", "Product added successfully.");
					request.setAttribute("fontStyle", "font-weight:bold;color:#00559c;");
				}

				// result.reject("addProduct.status");
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showUploadForm:::" + e.getMessage());

			request.setAttribute("produstatus", " Error occurred while adding product.");
			request.setAttribute("fontStyle", "font-weight:bold;color:red");
			return new ModelAndView(view);
		}
		if (productInfoVO.getBtnType().equalsIgnoreCase("Save"))
		{

			view = "registprodsetup";
		}/*
		 * else if(productInfoVO.getBtnType().equalsIgnoreCase("SaveContinue"))
		 * { view = "choosePlan"; }
		 */
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView(view);

	}

	@RequestMapping(value = "/uploadtempimg", method = RequestMethod.POST)
	public String onSubmitImage(@ModelAttribute("productInfoVO") ProductInfoVO productInfoVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception, ScanSeeServiceException
	{
		final String methodName = "onSubmitImage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String userAgnet = request.getHeader("User-Agent");
		CommonsMultipartFile file[] = productInfoVO.getImageFile();
		List<CommonsMultipartFile> tempList = new ArrayList<CommonsMultipartFile>();
		// System.out.println("uploaded image"+request.getSession().getAttribute("viewedImage"));
		LOG.info("User agent " + userAgnet);
		if (StringUtils.contains(userAgnet, "MSIE"))
		{
			// need to check size of array and remove file object whose size is
			// zero
			for (int i = 0; i < file.length; i++)
			{
				CommonsMultipartFile temp = file[i];
				if (temp.getSize() > 0)
				{
					tempList.add(temp);
				}
			}

			String filePathImages = Utility.getTempMediaPath(ApplicationConstants.TEMP) + "/" + tempList.get(0).getOriginalFilename();
			LOG.info("Product image file uploaded::" + tempList.get(0).getOriginalFilename());
			Utility.writeFileData(tempList.get(0), filePathImages);
			response.getWriter().write(
					"<imageScr>" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
							+ tempList.get(0).getOriginalFilename() + "</imageScr>");
			// session.setAttribute("viewedImage",
			// tempList.get(0).getOriginalFilename());
		}
		else
		{
			String filePathImages = Utility.getTempMediaPath(ApplicationConstants.TEMP) + "/" + file[0].getOriginalFilename();
			LOG.info("Product image file uploaded::" + file[0].getOriginalFilename());
			Utility.writeFileData(file[0], filePathImages);
			response.getWriter().write(
					"<imageScr>" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + file[0].getOriginalFilename()
							+ "</imageScr>");
			// request.getSession().setAttribute("viewedImage",
			// tempList.get(0).getOriginalFilename());
		}
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return null;

	}

}

/**
 * 
 */
package supplier.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import common.exception.ScanSeeServiceException;

/**
 * HotDealsDisplayController is a controller class for displaying supplier product hot deals.
 * 
 * @author Created by SPAN.
 */
@Controller
@RequestMapping("/displaysupdeals.htm")
public class HotDealsDisplayController
{
	/**
	 * This controller method will return to login screen .
	 * 
	 * @param request HttpServletRequest instance.
	 * @param model  ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public final String showPage(HttpServletRequest request, HttpSession session,
			ModelMap model) throws ScanSeeServiceException {
		return "login";
	}
}

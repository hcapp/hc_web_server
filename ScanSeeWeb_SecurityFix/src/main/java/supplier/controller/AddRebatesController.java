package supplier.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;
import supplier.validator.RebatesValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Rebates;
import common.pojo.Retailer;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

/**
 * AddRebatesController is a controller class for add rebates screen.
 * 
 * @author nanda_b
 */
@Controller
public class AddRebatesController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AddRebatesController.class);

	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();

	/**
	 * Variable viewName declared as constant string.
	 */
	private final String viewName = "addRebates";

	/**
	 * Variable view declared as String.
	 */
	private String view = viewName;

	/**
	 * Variable rebatesValidator declared as instance of
	 * RebatesValidator.
	 */
	private RebatesValidator rebatesValidator;

	/**
	 * To set rebatesValidator.
	 * 
	 * @param rebatesValidator To set
	 */
	@Autowired
	public final void setRebatesValidator(RebatesValidator rebatesValidator)
	{
		this.rebatesValidator = rebatesValidator;
	}

	/**
	 * This controller method will display the new rebate screen.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/rebatesaddmfg.htm", method = RequestMethod.GET)
	public final String showPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside AddRebatesController :  showPage ");
		session.removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		request.getSession().removeAttribute("pdtInfoList");
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			request.getSession().removeAttribute("productList");
			ArrayList<Retailer> retailers = null;
			retailers = supplierService.getRetailers();
			session.setAttribute("retailerList", retailers);
			ArrayList<TimeZones> timeZonelst = null;
			timeZonelst = supplierService.getAllTimeZones();
			session.setAttribute("rebateTimeZoneslst", timeZonelst);
			final Rebates rebatesVo = new Rebates();
			model.put("addRebatesform", rebatesVo);
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return viewName;
	}

	/**
	 * This controller method will display all the retailer address (Address1, City, State, Postal code) by calling service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param retailerId request as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/fetchretailerlocation.htm", method = RequestMethod.GET)
	@ResponseBody
	public final String getRetailerLocations(@RequestParam(value = "retailerId", required = true) int retailerId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside AddRebatesController :  getRetailerLocations ");
		ArrayList<Retailer> retailerLoc = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='retailerLocID' id='retailerLocID' onchange='getRetailerLocTrigger();'><option value='0'>--Select--</option>";
		final String strSelRetailerLoc = (String) request.getSession().getAttribute("retailerLocHidden");
		final String strSelRetailer = (String) request.getSession().getAttribute("retailerIdHidden");

			if (null != strSelRetailer) {
				retailerId = Integer.valueOf(strSelRetailer);
				session.removeAttribute("retailerIdHidden");
			}
		innerHtml.append(finalHtml);
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			if (ISDUBUGENABLED)
			{
				LOG.debug("fetching Retail locations for Retailerid {}" + retailerId);
			}
			retailerLoc = supplierService.getRetailerLoc(retailerId);
			final Rebates rebateVo = new Rebates();
			response.setContentType("text/xml");
			final StringBuffer value = new StringBuffer();
			for (int i = 0; i < retailerLoc.size(); i++)
			{
				if (null != strSelRetailerLoc && Integer.valueOf(strSelRetailerLoc) == retailerLoc.get(i).getRetailerLocID())
				{
					rebateVo.setRetailerLocationID(retailerLoc.get(i).getRetailerLocID());
					if (retailerLoc.get(i).getAddress1() != null)
					{
						value.append(retailerLoc.get(i).getAddress1());
						value.append(ApplicationConstants.COMMA);
						if (retailerLoc.get(i).getCity() != null)
						{
							value.append(retailerLoc.get(i).getCity());
							value.append(ApplicationConstants.COMMA);
							if (retailerLoc.get(i).getState() != null)
							{
								value.append(retailerLoc.get(i).getState());
								value.append(ApplicationConstants.COMMA);
								if (retailerLoc.get(i).getPostalCode() != null)
								{
									value.append(retailerLoc.get(i).getPostalCode());
									value.append(ApplicationConstants.COMMA);
								}
							}
						}
					}
					innerHtml.append("<option  selected=true value=" + retailerLoc.get(i).getRetailerLocID() + ">" + value.toString()
					+ "</option>");
					request.getSession().removeAttribute("retailerLocHidden");
				}
				else
				{
					rebateVo.setRetailerLocationID(retailerLoc.get(i).getRetailerLocID());
					if (retailerLoc.get(i).getAddress1() != null)
					{
						value.append(retailerLoc.get(i).getAddress1());
						value.append(ApplicationConstants.COMMA);
						if (retailerLoc.get(i).getCity() != null)
						{
							value.append(retailerLoc.get(i).getCity());
							value.append(ApplicationConstants.COMMA);
							if (retailerLoc.get(i).getState() != null)
							{
								value.append(retailerLoc.get(i).getState());
								value.append(ApplicationConstants.COMMA);
								if (retailerLoc.get(i).getPostalCode() != null)
								{
									value.append(retailerLoc.get(i).getPostalCode());
									value.append(ApplicationConstants.COMMA);
								}
							}
						}
					}
					innerHtml.append("<option value=" + retailerLoc.get(i).getRetailerLocID() + ">" + value.toString()

					+ "</option>");
				}
			}
			innerHtml.append("</select>");
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (NullPointerException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return innerHtml.toString();
	}

	/**
	 * The controller method add new rebate for Supplier by calling service and DAO methods.
	 * 
	 * @param rebatesVo Rebates instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/rebatesaddmfg.htm", method = RequestMethod.POST)
	public final ModelAndView addRebates(@ModelAttribute("addRebatesform") Rebates rebatesVo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside AddRebatesController : addRebates ");
		String compDate = null;
		final Date currentDate = new Date();
		session.removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		try
		{
			String isDataInserted = null;
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

			final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			rebatesVo.setUserID(user.getUserID());
			rebatesVo.setSupplierId(user.getSupplierId());

			/*
			 * String startDate = rebatesVo.getRebStartDate(); String sDate =
			 * Utility.getFormattedDate(startDate);
			 * rebatesVo.setRebStartDate(sDate); String endDate =
			 * rebatesVo.getRebEndDate(); String eDate =
			 * Utility.getFormattedDate(endDate);
			 * rebatesVo.setRebEndDate(eDate);
			 */

			final String sTime = String.valueOf(rebatesVo.getRebStartHrs()) + ":" + String.valueOf(rebatesVo.getRebStartMins());
			final String endTime = String.valueOf(rebatesVo.getRebEndhrs()) + ":" + String.valueOf(rebatesVo.getRebEndMins());
			rebatesVo.setRebStartTime(sTime);
			rebatesVo.setRebEndTime(endTime);
			/*
			 * String stTime = rebatesVo.getRebStartTime(); stTime =
			 * stTime.replace(',', ':'); rebatesVo.setRebStartTime(stTime);
			 * String endTime = rebatesVo.getRebEndTime(); endTime =
			 * endTime.replace(',', ':'); rebatesVo.setRebEndTime(endTime);
			 */
			rebatesValidator.validate(rebatesVo, result);
			if (!result.hasErrors())
			{
				compDate = Utility.compareCurrentDate(rebatesVo.getRebStartDate(), currentDate);
				if (null != compDate) {
					rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATESTARTCURRENT);
				}

				compDate = Utility.compareCurrentDate(rebatesVo.getRebEndDate(), currentDate);
				if (null != compDate) {
					rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEENDCURRENT);
				} else {
					compDate = Utility.compareDate(rebatesVo.getRebStartDate(), rebatesVo.getRebEndDate());
					if (null != compDate)
					{
						rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEAFTER);
					}
				}
			}
			if (result.hasErrors()) {
				if (!"".equals(Utility.checkNull(String.valueOf(rebatesVo.getRetailerLocID()))))
				{
					session.setAttribute("retailerLocHidden", String.valueOf(rebatesVo.getRetailerLocID()));
				}
				if (!"".equals(Utility.checkNull(String.valueOf(rebatesVo.getRetailerID()))))
				{
					session.setAttribute("retailerIdHidden", String.valueOf(rebatesVo.getRetailID()));
				}
				return new ModelAndView(viewName);
			}
			else
			{
				isDataInserted = supplierService.addRebates(rebatesVo);

				if (isDataInserted == ApplicationConstants.FAILURE || isDataInserted == null)
				{
					view = viewName;
					result.reject("addRebate.error");
				} else {
					view = "rebatesMfg.htm";
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			result.reject("addRebate.error");
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (view.equals(viewName))
		{
			return new ModelAndView(view);
		} else {
			return new ModelAndView(new RedirectView(view));
		}
	}

	/**
	 * This ModelAttribute sort rebate start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("RebateStartHrs")
	public Map<String, String> populateDealStartHrs() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method returns a negative integer, zero, or a positive 
	 *  integer as the first argument is less than, equal to, or greater than the second.
	 * 
	 * @param unsortMap as request parameter.
	 * @return sortedMap returns a integer value.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException
	{
		final List list = new LinkedList(unsortMap.entrySet());
		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2)
			{
				return ((Comparable) ((Map.Entry) o1).getValue()).compareTo(((Map.Entry) o2).getValue());
			}
		});
		// put sorted list into map again
		final Map sortedMap = new LinkedHashMap();
		Map.Entry entry = null;
		for (final Iterator it = list.iterator(); it.hasNext();)
		{
			entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort rebate start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("RebateStartMins")
	public Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method will display supplier rebates details in iphone screen formate.
	 * 
	 * @param rebatesVo Rebates instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @param model ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/previewsuppladdrebate.htm", method = RequestMethod.POST)
	public final ModelAndView previewPage(@ModelAttribute("previewsuppladdrebform") Rebates rebatesVo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("previewPage in addRebatesContrller in Supplier:: Inside POST Method");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

		request.getSession().removeAttribute("prdAddImage");
		request.getSession().removeAttribute("addProductName");
		request.getSession().removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		final String rebName = request.getParameter("rebName");
		final String rebDescription = request.getParameter("rebDescription");
		
		List<Rebates> prdList = null;
		//int supplierId = 0;
		//final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		//supplierId = loginUser.getSupplierId();

		if (!"".equals(Utility.checkNull(String.valueOf(rebatesVo.getRetailerLocID()))))
		{
			session.setAttribute("retailerLocHidden", String.valueOf(rebatesVo.getRetailerLocID()));
		}
		if (!"".equals(Utility.checkNull(String.valueOf(rebatesVo.getRetailerID()))))
		{
			session.setAttribute("retailerIdHidden", String.valueOf(rebatesVo.getRetailID()));
		}
		request.getSession().setAttribute("addRebateName", rebName);
		request.getSession().setAttribute("addRebateDescription", rebDescription);

		rebatesVo.setRebName(rebName);
		rebatesVo.setRebDescription(rebDescription);

		try
		{
			final String scancodes = rebatesVo.getProductName();
			 if (scancodes != null && !scancodes.isEmpty())
			 {
				 prdList = supplierService.getProdImage(scancodes);
				 if (prdList != null && !prdList.isEmpty()) {
						session.setAttribute("addProductName", prdList.get(0).getProductName());
						final String prod = prdList.get(0).getProductImagePath();
						final String []str = prod.split(",");
						final String image = str[0].trim();
						session.setAttribute("prdAddImage", image);
					}
			 }		
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("previewsuppladdrebform", rebatesVo);
		return new ModelAndView("addsupplrebpreview");
	}
}

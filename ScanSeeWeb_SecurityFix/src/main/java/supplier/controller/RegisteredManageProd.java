/**
 * @ (#) RegisteredManageProd.java 24-Dec-2011
 * Project       :ScanSeeWeb
 * File          : RegisteredManageProd.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 24-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package supplier.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;


import supplier.service.SupplierService;
import supplier.validator.ManageProductValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ManageProducts;

import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.Utility;

@Controller
public class RegisteredManageProd
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RegisteredManageProd.class);

	ManageProductValidator manageProductValidator;

	@Autowired
	public void setManageProductValidator(ManageProductValidator manageProductValidator)
	{
		this.manageProductValidator = manageProductValidator;
	}

	@RequestMapping(value = "/registprodmanageprod.htm", method = RequestMethod.GET)
	public ModelAndView manageProduct(@ModelAttribute("manageform") ManageProducts manageProducts, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException,ScanSeeServiceException
	{
		session.removeAttribute("seacrhList");
		session.removeAttribute("pagination");
		session.removeAttribute("previewlist");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		String productName = manageProducts.getProductName();
		LOG.info("productName*************" + productName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int lowerLimit = 0;
		int manufacturerID = loginUser.getSupplierId();
		LOG.info("manufacturerID*************" + manufacturerID);
		int currentPage = 1;
		try
		{
			
			SearchResultInfo resultInfo = supplierService.getAllManageProduct(productName, manufacturerID, lowerLimit);
			LOG.info("manageProduct search product name {} Page Start {}",productName,lowerLimit);
			session.setAttribute("seacrhList", resultInfo);
			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "savegridprod.htm");
			session.setAttribute("pagination", objPage);
			
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in manageProduct::::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}
		return new ModelAndView("registprodmanageprod");
	}

	@RequestMapping(value = "/registprodmanageprod.htm", method = RequestMethod.POST)
	public ModelAndView searchResult(@ModelAttribute("manageform") ManageProducts manageProducts, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException,ScanSeeServiceException
	{
		final String methoName = "saveBatchUpload";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		List<ManageProducts> manageProductList = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

		String productName = manageProducts.getProductName();
		LOG.info("productName*************" + productName);

		/*
		 * manageProductValidator.validateManageProduct(manageProducts, result);
		 * if (result.hasErrors()) { return new ModelAndView("manageProduct"); }
		 */

		Users loginUser = (Users) request.getSession().getAttribute("loginuser");

		// int manufacturerID = loginUser.getSupplierId();
		int manufacturerID = loginUser.getSupplierId();
		LOG.info("manufacturerID*************" + manufacturerID);
		int lowerLimit = 0;
		int currentPage = 1;
		try
		{
			SearchResultInfo resultInfo = supplierService.getAllManageProduct(productName, manufacturerID, lowerLimit);
			session.setAttribute("seacrhList", resultInfo);
			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "savegridprod.htm");
			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException e)
		{

			LOG.error("Exception occurred in searchResult::::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}

		session.setAttribute("regmanageproductlist", manageProductList);
		       
		return new ModelAndView("registprodmanageprod");
	}

	@RequestMapping(value = "/savereggridprod.htm", method = RequestMethod.POST)
	public ModelAndView saveGridProducts(@ModelAttribute("manageform") ManageProducts manageProducts, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException,ScanSeeServiceException
	{
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int manufacturerID = loginUser.getSupplierId();
		String isDataInserted = null;
		String pageFlag = manageProducts.getPageFlag();
		int lowerLimit = 0;
		int currentPage = 1;
		try
		{
			isDataInserted = supplierService.saveProductGridData(manageProducts, loginUser);
			if(isDataInserted.equalsIgnoreCase(ApplicationConstants.SUCCESS)&& manageProducts.getPageFlag().equalsIgnoreCase("false"))
			{
				result.reject("manageProduct.status");
				request.setAttribute("manageProductFont", "font-weight:bold;color:#00559c;");	
			}else if(manageProducts.getPageFlag().equalsIgnoreCase("false"))
			{
				result.reject("manageProduct.error");
				request.setAttribute("manageProductFont", "font-weight:regular;color:red;");
			}
			if (null != pageFlag && pageFlag.equals("true"))
			{
				String pageNumber = manageProducts.getPageNumber();
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}
			}
			String productName = manageProducts.getProductName();
			SearchResultInfo resultInfo = supplierService.getAllManageProduct(productName, manufacturerID, lowerLimit);
			session.setAttribute("seacrhList", resultInfo);
			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "savegridprod.htm");
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in saveGridProducts::::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}
		return new ModelAndView("registprodmanageprod");
	}
	@RequestMapping(value = "/previewregprod.htm", method = RequestMethod.GET)
	public ModelAndView preview(@ModelAttribute("manageform") ManageProducts manageProducts, BindingResult result,
			HttpServletRequest request, HttpServletResponse response,HttpSession session) throws IOException,ScanSeeServiceException
	{
		LOG.info("preview in RegisteredManageProd:: Inside GET Method");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
	
		List<ManageProducts> managePreviewList = null;
		Long productId = manageProducts.getProductID();	
//		LOG.info("productId value in controller"+productId);
		try{
			managePreviewList = supplierService.getProductPreviewList(productId);
			LOG.info("listsize:" + managePreviewList.size());
		    
		   if(managePreviewList!=null && !managePreviewList.isEmpty()){
			   session.setAttribute("previewlist", managePreviewList.get(0)); 
		   }
			
		}
		catch(ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in preview::::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}	
		LOG.info("preview in RegisteredManageProd:: Exiting GET Method");
		return new ModelAndView("registprodmanagepreview");
	}
}

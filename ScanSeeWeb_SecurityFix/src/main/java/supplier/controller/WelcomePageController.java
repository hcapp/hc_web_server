package supplier.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.LoginVO;
import common.pojo.Users;

@Controller
public class WelcomePageController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WelcomePageController.class);

	/**
	 * Variable isPaymentDone declared as constant string.
	 */
	private final String isPaymentDone = "isPaymentDone";

	final String viewChangePwd = "changePassword";

	@RequestMapping("/welcome.htm")
	public ModelAndView redirect(ModelMap model, HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{

		// mention the user role then header will display.
		String userRole = "welcome";
		Users userInfo = null;
		Boolean hasLoggedIn = (Boolean) session.getAttribute("hasLoggedIn");
		if (hasLoggedIn != null && hasLoggedIn)
		{

			userInfo = (Users) session.getAttribute("loginuser");

			if (null != userInfo)
			{

				LoginVO loginVO = new LoginVO();
				loginVO.setUserName(userInfo.getUserName());
				model.put("loginform", loginVO);
				return new ModelAndView(new RedirectView("/ScanSeeWeb/login.htm"));

			}

		}
		session.setAttribute("user", userRole);

		return new ModelAndView("welcome");
	}

	@RequestMapping("/homepage.htm")
	public String redirectHome(HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{

		session.invalidate();

		return "welcome";
	}
}

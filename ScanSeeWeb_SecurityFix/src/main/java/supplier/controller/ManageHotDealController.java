package supplier.controller;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.HotDealInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.Utility;

/**
 * ManageHotDealController is a controller class for managing all supplier product hot deals
 * screen.
 * 
 * @author Created by SPAN.
 */
@Controller
@RequestMapping("/hotDeal.htm")
public class ManageHotDealController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageHotDealController.class);

	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();

	/**
	 * This controller method will display all HotDeals or by passing HotDealName as input parameter by call service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param model  ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public final String showPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageHotDealController : showPage ");
		request.getSession().removeAttribute("message");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);

		final HotDealInfo dealInfo = new HotDealInfo();
		if (ISDUBUGENABLED)
		{
			LOG.debug("Hot Deal Name*********" + dealInfo.getHotDealName());
			LOG.debug("Supplier Id*********" + user.getSupplierId());
		}
		int lowerLimit = 0;
		dealInfo.setHotDealName(null);
		SearchResultInfo searchResultInfo = null;
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String sDate = null;
		String eDate = null;
		try
		{
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
			}
			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			searchResultInfo = supplierService.getDealsForDisplay(Long.valueOf(user.getSupplierId()), dealInfo.getHotDealName(), lowerLimit);
			session.setAttribute("listOfDeals", searchResultInfo);
			if (searchResultInfo != null)
			{
				String salePrice = null;
				for (int i = 0; i < searchResultInfo.getDealList().size(); i++)
				{
					salePrice = searchResultInfo.getDealList().get(i).getSalePrice();
					salePrice = Utility.formatDecimalValue(salePrice);
					searchResultInfo.getDealList().get(i).setSalePrice(salePrice);

					sDate = searchResultInfo.getDealList().get(i).getDealStartDate();
					sDate = Utility.getUsDateFormat(sDate);
					searchResultInfo.getDealList().get(i).setDealStartDate(sDate);

					eDate = searchResultInfo.getDealList().get(i).getDealEndDate();
					eDate = Utility.getUsDateFormat(eDate);
					searchResultInfo.getDealList().get(i).setDealEndDate(eDate);
				}
				final Pagination objPage = Utility.getPagination(searchResultInfo.getTotalSize(), currentPage, "hotDeal.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}

			/*
			 * if (listOfDeals != null && listOfDeals.size() > 0) {
			 * request.getSession().setAttribute("listOfDeals", listOfDeals); }
			 * else request.getSession().setAttribute("message",
			 * "No Deals found matching the criteria");
			 */
			/*
			 * listOfDeals = (List<HotDealInfo>)
			 * supplierService.getDealsForDisplay
			 * (Long.valueOf(user.getSupplierId()), dealInfo.getHotDealName(),
			 * lowerLimit); if (listOfDeals != null && listOfDeals.size() > 0) {
			 * request.getSession().setAttribute("listOfDeals", listOfDeals); }
			 * else request.getSession().setAttribute("message",
			 * "No more Deals to display");
			 */
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		/*
		 * catch (ScanSeeWebSqlException exception) { LOG.error(
		 * "ScanSeeWebSql error occured Inside ManageHotDealController : showPage : "
		 * + exception.getMessage()); }
		 */
		catch (ParseException exception)
		{
			LOG.error("Parse error occured Inside ManageHotDealController : showPage : " + exception.getMessage());
		}
		catch (NullPointerException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("displayDealsForm", dealInfo);
		return "hotDeal";
	}

	/**
	 * This controller method will display HotDeals deals by passing HotDealName as input parameter by call service and DAO methods.
	 * 
	 * @param dealInfo HotDealInfo instance as request parameter
	 * @param request HttpServletRequest instance.
	 * @param model  ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param result BindingResult instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws IOException will be thrown.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings("unused")
	@RequestMapping(method = RequestMethod.POST)
	public final ModelAndView getRebatesByName(@ModelAttribute("displayDealsForm") HotDealInfo dealInfo, BindingResult result, HttpServletRequest request,
			 ModelMap model, HttpSession session) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside ManageHotDealController : getRebatesByName ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		request.getSession().removeAttribute("message");
		request.getSession().removeAttribute("listOfDeals");
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		SearchResultInfo searchResultInfo = null;
		if (ISDUBUGENABLED)
		{
			LOG.debug("Hot Deal Name*********" + dealInfo.getHotDealName());
			LOG.debug("Supplier Id*********" + user.getSupplierId());
		}
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		Pagination objPage = null;
		String sDate = null;
		String eDate = null;
		int lowerLimit = 0;
		String hotDealName = null;
		try
		{
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}
			}
			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			if (dealInfo.getHotDealName() != null)
			{
				hotDealName = dealInfo.getHotDealName();
				if (null != hotDealName && !"".equals(hotDealName))
				{
					hotDealName = hotDealName.trim();
				}
				dealInfo.setHotDealName(hotDealName);
			}
			searchResultInfo = supplierService.getDealsForDisplay(Long.valueOf(user.getSupplierId()), dealInfo.getHotDealName(), lowerLimit);

			if (!searchResultInfo.getDealList().isEmpty())
			{
				if (searchResultInfo != null)
				{
					String salePrice = null;
					for (int i = 0; i < searchResultInfo.getDealList().size(); i++)
					{
						salePrice = searchResultInfo.getDealList().get(i).getSalePrice();
						salePrice = Utility.formatDecimalValue(salePrice);
						searchResultInfo.getDealList().get(i).setSalePrice(salePrice);

						sDate = searchResultInfo.getDealList().get(i).getDealStartDate();
						sDate = Utility.getUsDateFormat(sDate);
						searchResultInfo.getDealList().get(i).setDealStartDate(sDate);

						eDate = searchResultInfo.getDealList().get(i).getDealEndDate();
						eDate = Utility.getUsDateFormat(eDate);
						searchResultInfo.getDealList().get(i).setDealEndDate(eDate);
					}
					session.setAttribute("listOfDeals", searchResultInfo);
					objPage = Utility.getPagination(searchResultInfo.getTotalSize(), currentPage, "hotDeal.htm");
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				}
			} else {
				result.reject("Deals.status");
				request.setAttribute("manageDealsFont", "font-weight:regular;color:red;");
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}
			/*
			 * if (listOfDeals != null && listOfDeals.size() > 0) {
			 * request.getSession().setAttribute("listOfDeals", listOfDeals); }
			 * else request.getSession().setAttribute("message",
			 * "No Deals found matching the criteria");
			 */
		}
		catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (NullPointerException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (ParseException exception) {
			LOG.error("Parse error occured Inside ManageHotDealController : getRebatesByName : " + exception.getMessage());
		}
		return new ModelAndView("hotDeal");
	}
}

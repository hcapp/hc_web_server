package supplier.controller;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.authorize.aim.Transaction;
import net.authorize.data.Customer;
import net.authorize.xml.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.AccountType;
import common.pojo.PlanInfo;
import common.pojo.State;
import common.pojo.SupplierProfile;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class ChoosePlanController {

	private static final String	UNITED_STATES	= "United States";
	
	private static final String	YEARLY	= "yearly";

	private static final String	MONTHLY	= "monthly";

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(ChoosePlanController.class);

	@RequestMapping(value = "/choosePlan.htm", method = RequestMethod.GET)
	public ModelAndView choosePlan(HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException {
		LOG.info("Inside ChoosePlanController : choosePlan ");
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext
				.getBean("supplierService");
		ArrayList<PlanInfo> PlanInfoList = null;
		ArrayList<PlanInfo> PlanInfoFeeList = null;

		ArrayList<State> statesList = null;
		ArrayList<AccountType> accountTypeList = null;

		PlanInfo plnInfo = new PlanInfo();
		plnInfo.setSelectedTab(ApplicationConstants.ACHBNKTAB);
		session.setAttribute("selectedTab", ApplicationConstants.ACHBNKTAB);
		model.addAttribute("myform", plnInfo);
		Number priceNumber = null;
		NumberFormat df = DecimalFormat.getInstance();
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(2);
		try {
			PlanInfoList = supplierService.getAllPlanList();
			for (PlanInfo planInfo : PlanInfoList) 
			{
				if(!Utility.isEmptyOrNullString(planInfo.getPrice())) 
				{
					priceNumber = df.parse(planInfo.getPrice());
					planInfo.setPrice((df.format(priceNumber)));
				}
			}
			PlanInfoFeeList = supplierService.getAllFeeDetailsList();
			statesList = supplierService.getAllStates();
			accountTypeList = supplierService.getAllAcountType();
		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in choosePlan(:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		} catch (ParseException e) {
			LOG.error("Exception occurred in choosePlan:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		session.setAttribute("planinfolist", PlanInfoList);
		session.setAttribute("planinfofeelist", PlanInfoFeeList);
		session.setAttribute("statesList", statesList);
		session.setAttribute("accountTypes", accountTypeList);
		return new ModelAndView("choosePlan");
	}

	@RequestMapping(value = "/planInfo.htm", method = RequestMethod.POST)
	public ModelAndView planInfo(
			@ModelAttribute("chooseplanform") PlanInfo planInfo,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside ChoosePlanController : planInfo ");
		ArrayList<PlanInfo> PlanInfoList = (ArrayList<PlanInfo>) session
				.getAttribute("planinfolist");
		String totalPrice = planInfo.getTotal();
		
		
		// Calculate Tax 
		if(!Utility.isEmptyOrNullString(planInfo.getGrandTotal()))
		{
			final String taxAmount = Utility.calculateTotalWithTax(planInfo.getGrandTotal());
			session.setAttribute("taxAmount",taxAmount);
			double grandTotalWithTax = Double.parseDouble(planInfo.getGrandTotal()) + Double.parseDouble(taxAmount);
			planInfo.setGrandTotal(String.valueOf(grandTotalWithTax));
		}
		else
		{
			session.setAttribute("taxAmount","");
		}
		
		  
		String[] arraytotalPrice = totalPrice.split(",");
		List<String> totalPriceList = Arrays.asList(arraytotalPrice);
		List<PlanInfo> pInfoList = Utility.jsonToPlanObject(planInfo
				.getPlanJson());
		String selTab = planInfo.getSelectedTab();
		if (pInfoList != null) {
			for (Iterator<PlanInfo> iterator = PlanInfoList.iterator(); iterator
					.hasNext();) {
				PlanInfo planInfo2 = (PlanInfo) iterator.next();

				for (PlanInfo obj : pInfoList) {

					if (obj.getProductID().equals(planInfo2.getProductId())) {
						planInfo2.setQuantity(obj.getQuantity());
						planInfo2.setSubTotal(obj.getSubTotal());
						planInfo2.setPrice(obj.getPrice());
					}
				}
			}
			session.setAttribute("totalPricelist", planInfo.getGrandTotal());
			session.setAttribute("planInfolist", PlanInfoList);
			if (null != selTab && selTab.equals("CreditCard")) {
				session.setAttribute("selectedTab", "CreditCard");
			} else if (null != selTab
					&& selTab.equals(ApplicationConstants.ACHBNKTAB)) {
				session.setAttribute("selectedTab",
						ApplicationConstants.ACHBNKTAB);
			} else {
				session.setAttribute("selectedTab",
						ApplicationConstants.ACHBNKTAB);
			}
			PlanInfo plnInfo = new PlanInfo();
			model.addAttribute("myform", plnInfo);

		
			return new ModelAndView("planConfirmation");
		}
		return new ModelAndView("planConfirmation");

	}

	

	@RequestMapping(value = "/fetchdiscount", method = RequestMethod.GET)
	public @ResponseBody
	String getDiscount(
			@RequestParam(value = "discountcode", required = true) String discountCode,
			HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException {

		LOG.info("Inside ChoosePlanController : getDiscount ");
		Double discountValue = 0.00;
		StringBuilder innerHtml = new StringBuilder();
		PlanInfo planInfo = null;

		try {
			// session.removeAttribute("manufacturerDiscountPlanID");
			ServletContext servletContext = request.getSession()
					.getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			SupplierService supplierService = (SupplierService) appContext
					.getBean("supplierService");
			planInfo = supplierService.getDiscountValue(discountCode);
			discountValue = planInfo.getDiscount();
			Integer manufacturerDiscountPlanID = planInfo
					.getManufacturerDiscountPlanID();
			response.setContentType("text/xml");

			innerHtml
					.append("<input readonly='readonly' name='discount' class='textbox' value='"
							+ discountValue + "'/>");
			session.setAttribute("manufacturerDiscountPlanID",
					manufacturerDiscountPlanID);
		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in getDiscount:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		innerHtml.append("&" + discountValue);
		return innerHtml.toString();
	}

	@RequestMapping(value = "/paymentconfirm.htm", method = RequestMethod.POST)
	public ModelAndView confirmPayment(HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException {
		LOG.info("Inside ChoosePlanController : confirmPayment ");

		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext
				.getBean("supplierService");

		return new ModelAndView("paymentconfirm");
	}

	@RequestMapping(value = "/saveplanInfo.htm", method = RequestMethod.POST)
	public ModelAndView savePlanInfo(
			@ModelAttribute("myForm") PlanInfo planInfo,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside ChoosePlanController : savePlanInfo ");
		ArrayList<PlanInfo> PlanInfoList = (ArrayList<PlanInfo>) session
				.getAttribute("planinfolist");
		// List<PlanInfo> pInfoList =
		// Utility.jsonToPlanObject(planInfo.getPlanJson());

		String saveResponse = null;
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext
				.getBean("supplierService");
		Users loginUser = (Users) request.getSession()
				.getAttribute("loginuser");
		Integer discountPlanManufacturerID = (Integer) session
				.getAttribute("manufacturerDiscountPlanID");

		int manufacturerID = loginUser.getSupplierId();
		long userId = loginUser.getUserID();
		try {
			saveResponse = supplierService.savePlanInfo(PlanInfoList,
					manufacturerID, userId, discountPlanManufacturerID);
		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in savePlanInf:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(new RedirectView("dashboard.htm"));

	}

	@RequestMapping(value = "/loadTab.htm", method = RequestMethod.POST)
	public ModelAndView loadPaymentsTab(
			@ModelAttribute("chooseplanform") PlanInfo planInfo,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside ChoosePlanController : loadPaymentsTab ");
		ArrayList<PlanInfo> PlanInfoList = (ArrayList<PlanInfo>) session
				.getAttribute("planinfolist");
		String selTab = planInfo.getSelectedTab();

		if (null != selTab && selTab.equals(ApplicationConstants.ACHBNKTAB)) {
			session.setAttribute("selectedTab", ApplicationConstants.ACHBNKTAB);
		} else if (null != selTab
				&& selTab.equals(ApplicationConstants.CREDITCARDPAY)) {
			session.setAttribute("selectedTab",
					ApplicationConstants.CREDITCARDPAY);
		} else {
			session.setAttribute("selectedTab",
					ApplicationConstants.ALTERNATEPAYMENT);
		}
		return new ModelAndView("planConfirmation");

	}

	@RequestMapping(value = "/processCC", method = RequestMethod.POST)
	public @ResponseBody
	String processCreditCard(
			@RequestParam(value = "totalString", required = true) String totalString,
			HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException {

		LOG.info("Inside ChoosePlanController : processCreditCard ");

		String totalAmount = (String) session.getAttribute("totalPricelist");
		Users userInfo = (Users) session.getAttribute("loginuser");
		
		StringBuilder innerHtml = new StringBuilder();
		
		if (!Utility.isEmptyOrNullString(totalAmount) && !("0".equals(totalAmount) || "0.0".equals(totalAmount))) 
		{
			if(!Utility.isEmptyOrNullString(totalString))
			{				
				String[] strCardArr = totalString.split(":");
	
				List<SupplierProfile> supplierList = (List<SupplierProfile>) session.getAttribute("supplierProfileList");
				String companyName = null;
				String email = null;
				if(null != supplierList && supplierList.size() > 0)
				{
					final SupplierProfile supplierProfile = supplierList.get(0);
					if(!Utility.isEmptyOrNullString(supplierProfile.getCompanyName()))
						companyName = supplierProfile.getCompanyName();
					if(!Utility.isEmptyOrNullString(supplierProfile.getEmail()))
						email = supplierProfile.getEmail();					
				}
				
				
				Customer authCustomer = Customer.createCustomer();
				
				if(!Utility.isEmptyOrNullString(strCardArr[4]))
				{
					authCustomer.setFirstName(strCardArr[4]);
				}				
				
				if(!Utility.isEmptyOrNullString(strCardArr[5]))
				{
					authCustomer.setLastName(strCardArr[5]);
					
				}	
				
				
				
				/*if(!Utility.isEmptyOrNullString(strCardArr[4]))
				{
					String[] strNameArr = strCardArr[4].split(" ");
					if(null != strNameArr)
					{
						if(strNameArr.length == 2)
						{
							authCustomer.setFirstName(strNameArr[0]);
							authCustomer.setLastName(strNameArr[1]);
						}
						else
						{
							authCustomer.setFirstName(strNameArr[0]);
						}
					}
					
				}*/
				
				authCustomer.setAddress(strCardArr[6]);
				authCustomer.setState(strCardArr[7]);
				authCustomer.setCity(strCardArr[8]);
				authCustomer.setZipPostalCode(strCardArr[9]);
				authCustomer.setPhone(strCardArr[10]);
				authCustomer.setCountry(UNITED_STATES);
				
				authCustomer.setEmail(email);
				authCustomer.setCompany(companyName);
				if(null != userInfo)
				{
					if(null != userInfo.getCountryCode() && !Utility.isEmptyOrNullString(userInfo.getCountryCode().getCountryName()))
						authCustomer.setCountry(userInfo.getCountryCode().getCountryName());
					if(userInfo.getUserID() > 0)
						authCustomer.setCustomerId(userInfo.getUserID().toString());
					/*if(!Utility.isEmptyOrNullString(userInfo.getUserName()))
						authCustomer.setEmail(userInfo.getUserName());*/
				}
				
				
				//	Result<Transaction> result = Utility.authenticateCreditCard(strCardArr,	totalAmount,authCustomer);      
				net.authorize.arb.Result<Transaction> result = Utility.authenticateARBCreditCard(strCardArr,totalAmount,authCustomer,"monthly");
	
			//	if (null != result && result.isApproved()) 
				if (null != result && result.isOk())
				{
					innerHtml.append("<br /><big><bold>Success!</bold></big></h2><br /><br />Payment Processed.<br /><br />Press 'Continue' to go to your Dashboard<br/>");
				}
				//else if (null != result && result.isDeclined()) {
				else if (null != result && result.isError()) {
					innerHtml.append("Credit Card Processing Declined!! \n");
					innerHtml.append("Please verify credit card details");
					//LOG.info("Error while processing credit card :"+ result.getResponseText());
					LOG.info("Error while processing credit card :"+ result.getResultCode());
					final ArrayList<Message> messages = result.getMessages();
					if(null != messages)
					{
						for (Message message : messages) {
							if(null != message)
							{
								LOG.info("Credit Card Processing Result Message Code:" + message.getCode());
								LOG.info("Credit Card Processing Result Message Text:" + message.getText());
							}
						}
					}
					
				}
				else
				{
					innerHtml.append("Error while processing credit card!!");	
					if(null != result)
					//	LOG.info("Error while processing credit card :"+ result.getResponseText());
						LOG.info("Error while processing credit card :"+ result.getResultCode());
				}
				
				
				/*Result<Transaction> result = Utility.authenticateCreditCard(strCardArr,totalAmount,authCustomer);
	
				if (null != result && result.isApproved()) 
				{
					innerHtml.append("<br /> <big>  <bold> PAYMENT CONFIRMATION RECEIPT </bold></big> </h2> <br />    <br />    Your payment has been authorized. A receipt will be sent to the email provided. <br />    <br />    Please print the following information for your records: <br />    ");
					
					final Transaction target = result.getTarget();
					
					
					if(null != target)
					{				
						final Customer customer = target.getCustomer();
						
						if(null != target.getOrder())
						{					
							innerHtml.append(" <br /> <h3> <strong> ORDER INFORMATION </strong> </h3> ");
							if(!Utility.isEmptyOrNullString(target.getOrder().getInvoiceNumber()))
								innerHtml.append(" <br />  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your Invoice :" + target.getOrder().getInvoiceNumber());	
							if(!Utility.isEmptyOrNullString(totalAmount))
								innerHtml.append(" <br /> <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amount : $" + totalAmount);
							if(null != target.getCreditCard().getCardType() && !Utility.isEmptyOrNullString(target.getCreditCard().getCardType().toString()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment Method :" + target.getCreditCard().getCardType());
						}
						
						innerHtml.append("  <br /> <br /> <h3> <strong> RESULTS  </strong> </h3> ");
						if(!Utility.isEmptyOrNullString(result.getResponseText()))
							innerHtml.append(" <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Response :" + result.getResponseText());
						if(!Utility.isEmptyOrNullString(target.getAuthorizationCode()))
							innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authorization Code :" + target.getAuthorizationCode());
						
						if(null != target.getCreditCard())
						{
							innerHtml.append("  <br /> <br /> <h3> <strong> PAYMENT INFORMATION </strong> </h3>");
							if(!Utility.isEmptyOrNullString(target.getCreditCard().getCreditCardNumber()))
								innerHtml.append(" <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Card : " + target.getCreditCard().getCreditCardNumber());
						}
						
						if(null != customer)
						{
							innerHtml.append("  <br /> <br /> </h3> <strong> CUSTOMER BILLING INFORMATION </strong> </h3> ");
							if(!Utility.isEmptyOrNullString(customer.getCustomerId()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer ID : " + customer.getCustomerId());
							if(!Utility.isEmptyOrNullString(customer.getFirstName()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Card Holder First Name : " + customer.getFirstName());
							if(!Utility.isEmptyOrNullString(customer.getLastName()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Card Holder Last Name : " + customer.getLastName());
							if(!Utility.isEmptyOrNullString(customer.getCompany()))
								innerHtml.append(" <br /> <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company  : " + customer.getCompany());
							if(!Utility.isEmptyOrNullString(customer.getAddress()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Address  : " + customer.getAddress());
							if(!Utility.isEmptyOrNullString(customer.getCity()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City  : " + customer.getCity());
							if(!Utility.isEmptyOrNullString(customer.getState()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State/Province : " + customer.getState());
							if(!Utility.isEmptyOrNullString(customer.getZipPostalCode()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zip/Postal Code : " + customer.getZipPostalCode());
							if(!Utility.isEmptyOrNullString(customer.getPhone()))
								innerHtml.append(" <br /> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone  : " + customer.getPhone());
						}
						
						innerHtml.append("<br /> <br /><a href=\"#\" onclick=\"printPage();return false;\"><img src=\"images/print_icon.jpg\" width=\"20\" height=\"20\"> Print</a>");
					}
					
				}
				else if (null != result && result.isDeclined()) {
					innerHtml.append("Credit Card Processing Declined!! \n");
					innerHtml.append("Please verify credit card details");
					LOG.info("Error while processing credit card :"+ result.getResponseText());
				}
				else
				{
					innerHtml.append("Error while processing credit card!!");
					if(null != result)
						LOG.info("Error while processing credit card :"+ result.getResponseText());
				}*/
			}
		}
		else
		{
			innerHtml.append("Total amount to be processed is invalid!!");		
		}

		return innerHtml.toString();
	}

	
	@RequestMapping(value = "/fetchSupplierdiscountMonYearly", method = RequestMethod.GET)
	public @ResponseBody
	String getSupplierDiscount(@RequestParam(value = "discountcode", required = true) String discountCode, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside ChoosePlanController : getSupplierDiscount for monthly and yearly plans ");
		Double discountValue = 0.00;
		StringBuilder innerHtml = new StringBuilder();
		
		try
		{
			if(!Utility.isEmptyOrNullString(discountCode))
			{
				ServletContext servletContext = request.getSession().getServletContext();
				WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
				//RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
				SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
				final Map<String, Map<String, String>> discountPlans = supplierService.getSupplierDiscountPlans();
			//final Map<String, Map<String, String>> discountPlans = getDiscountPlans();
				String defaultMonthlyPrice = "";
				String defaultYearlyPrice = "";
				ArrayList<PlanInfo> PlanInfoList = (ArrayList<PlanInfo>) session.getAttribute("planinfolist");
				if(null != PlanInfoList && PlanInfoList.size() > 0)
				{
					PlanInfo planInfo = PlanInfoList.get(0);
					if(null != planInfo)
					{
						defaultMonthlyPrice = planInfo.getRetailerBillingMonthly();
						defaultYearlyPrice = planInfo.getRetailerBillingYearly();
					}
				}
			
				
				if(null != discountPlans && discountPlans.size() > 0)
				{
					if(discountPlans.containsKey(discountCode.toUpperCase()))
					{
						final Map<String, String> obj = discountPlans.get(discountCode.toUpperCase());
						final String yearlyPrice = obj.get(YEARLY);
						final String monthlyPrice = obj.get(MONTHLY);						
						if (Utility.isNumber(yearlyPrice) && Utility.isNumber(monthlyPrice)) 
						{							
							//final int discountedMonthlyPrice = Integer.parseInt(defaultMonthlyPrice) - Integer.parseInt(monthlyPrice);
							//final int discountedYearlyPrice = Integer.parseInt(defaultYearlyPrice) - Integer.parseInt(yearlyPrice);
							//if(discountedMonthlyPrice >= 0 && discountedYearlyPrice >= 0)
							//{
								innerHtml.append(defaultMonthlyPrice);
								innerHtml.append("&");
								innerHtml.append(defaultYearlyPrice);
								innerHtml.append("&");								
								innerHtml.append(monthlyPrice);
								innerHtml.append("&");								
								innerHtml.append(yearlyPrice);
							/*}else
							{
								throw new Exception("Discount Code is invalid!");
							}*/
						}
						else
						{
							innerHtml.append("skippayment");
						}
					}
					else
					{
						throw new Exception("Discount Code is invalid!");
					}
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Exception occurred in getDiscount:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("innerHtml.toString() : "+innerHtml.toString());
		return innerHtml.toString();
	}
	
	

}

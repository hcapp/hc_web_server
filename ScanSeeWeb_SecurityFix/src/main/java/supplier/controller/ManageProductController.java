package supplier.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;
import supplier.validator.ManageProductValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.ManageProducts;
import common.pojo.Product;
import common.pojo.QRDetails;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.Utility;

/**
 *  This Controller class serves the request for Displaying the Products,editing and saving the products.
 * @author manjunatha_gh
 *
 */
@Controller
public class ManageProductController
{

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageProductController.class);
	
	/**
	 * 
	 * Manage product screen validator.
	 */
	 
	private ManageProductValidator manageProductValidator;
	 
	 /**
		 * Autowired method for setting ManageProductValidator object.
		 * @param manageProductValidator ManageProductValidator.
		 */
	@Autowired
	public void setManageProductValidator(ManageProductValidator manageProductValidator)
	{
		this.manageProductValidator = manageProductValidator;
	}

	/**
	 *  This method  fetches the list of products uploaded by the user and redirets the user to manage product screen.
	 * @param manageProducts ManageProducts object contains request parametres.
	 * @param request HttpServletRequest object for storing dataobjects.
	 * @param session HttpSession object for storing dataobjects. 
	 * @return ModelAndView view name for manage product screen.
	 * @throws ScanSeeServiceException if any exception during the processing of the request.
	 */
	@RequestMapping(value = "/manageProduct.htm", method = RequestMethod.GET)
	public ModelAndView manageProduct(@ModelAttribute("manageform") ManageProducts manageProducts, HttpServletRequest request,
			 HttpSession session) throws  ScanSeeServiceException
	{
		final String methodName = "manageProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ArrayList<Category> categories = null;
		String productName = manageProducts.getProductName();
		LOG.info("productName*************" + productName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int lowerLimit = 0;
		int manufacturerID = loginUser.getSupplierId();
		int currentPage = 1;
		try
		{
			categories = supplierService.getAllBusinessCategory();
			session.removeAttribute("seacrhList");
			session.removeAttribute("pagination");

			SearchResultInfo resultInfo = supplierService.getAllManageProduct(productName, manufacturerID, lowerLimit);
			LOG.info("manageProduct search product name {} Page Start {}", productName, lowerLimit);
			session.setAttribute("seacrhList", resultInfo);
			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "savegridprod.htm");
			session.setAttribute("pagination", objPage);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in manageProduct::::" + e.getMessage());
			throw e;
		}
		session.setAttribute("categoryList", categories);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("manageProduct");
	}
	
	/**
	 * This method is used for searching the Products based on UPC/Name.
	 * @param manageProducts ManageProducts Contains product information.
	 * @param request HttpServletRequest object.
	 * @param session HttpSession object.
	 * @return ModelAndView contains view name of manage product screen.
	 * @throws ScanSeeServiceException if any exception while processing the request.
	 */
	@RequestMapping(value = "/manageProduct.htm", method = RequestMethod.POST)
	public ModelAndView searchProduct(@ModelAttribute("manageform") ManageProducts manageProducts,BindingResult result, HttpServletRequest request,
			 HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "searchProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

		String productName = manageProducts.getProductName();
		LOG.info("productName*************" + productName);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int lowerLimit = 0;
		int manufacturerID = loginUser.getSupplierId();
		LOG.info("manufacturerID*************" + manufacturerID);
		int currentPage = 1;
		try
		{
			final SearchResultInfo resultInfo = supplierService.getAllManageProduct(productName, manufacturerID, lowerLimit);
			LOG.info("getAllManageProduct search product name {} Page Start {}", productName, lowerLimit);
			session.setAttribute("seacrhList", resultInfo);
			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "savegridprod.htm");
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in searchProduct::::" + e.getMessage());
			throw  e;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("manageProduct");
	}
	
	/**
	 * This method saves products data from the Manage product Grid. This is also used forpagination.
	 * @param manageProducts ManageProducts Contains product information.
	 * @param result BindingResult object.
	 * @param request HttpServletRequest object.
	 * @param session HttpSession object.
	 * @return ModelAndView contains view name of manage product screen.
	 * @throws ScanSeeServiceException if any exception while processing the request.
	 */
	@RequestMapping(value = "/savegridprod.htm", method = RequestMethod.POST)
	public ModelAndView saveGridProducts(@ModelAttribute("manageform") ManageProducts manageProducts, BindingResult result,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "savegridprod.htm";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int manufacturerID = loginUser.getSupplierId();
		String pageFlag = manageProducts.getPageFlag();
		String isDataInserted = null;
		int lowerLimit = 0;
		int currentPage = 1;
		try
		{
			isDataInserted = supplierService.saveProductGridData(manageProducts, loginUser);
			if (isDataInserted.equalsIgnoreCase(ApplicationConstants.SUCCESS) && manageProducts.getPageFlag().equalsIgnoreCase("false"))
			{
				result.reject("manageProduct.status");
				request.setAttribute("manageProductFont", "font-weight:bold;color:#00559c;");
			}
			else if (manageProducts.getPageFlag().equalsIgnoreCase("false"))
			{
				result.reject("manageProduct.error");
				request.setAttribute("manageProductFont", "font-weight:regular;color:red;");
			}
			String productName = manageProducts.getProductName();
			if (null != pageFlag && pageFlag.equals("true"))
			{
				String pageNumber = manageProducts.getPageNumber();
				Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					int number = Integer.valueOf(currentPage) - 1;
					int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}
			}
			final SearchResultInfo resultInfo = supplierService.getAllManageProduct(productName, manufacturerID, lowerLimit);
			session.setAttribute("seacrhList", resultInfo);
			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "savegridprod.htm");
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in saveGridProducts::::" + e.getMessage());
			throw  e;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("manageProduct");
	}
	
	/**
	 *  This method lists all the media files uploaded by  the user for selected product.
	  * @param manageProducts ManageProducts Contains product information.
	 * @param result BindingResult object.
	 * @param request HttpServletRequest object.
	 * @param session HttpSession object.
	 * @return ModelAndView contains view name of manage product screen.
	 * @throws ScanSeeServiceException if any exception while processing the request.
	 */
	@RequestMapping(value = "/supplierQRCode.htm", method = RequestMethod.GET)
	public ModelAndView supplierQRCode(@ModelAttribute("manageform") ManageProducts manageProducts, BindingResult result, HttpServletRequest request,
			 HttpSession session) throws  ScanSeeServiceException
	{
		final String methodName = "supplierQRCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		request.removeAttribute("message");
		int productId = (int) manageProducts.getProductID();
		final String productName = manageProducts.getProductName();
		final String productNameEscapeSpcl = manageProducts.getProdNameEscape();
		List<Product> mediaList = null;
		int audioCount = 0;
		int videoCount = 0;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		try
		{
			mediaList = supplierService.fetchProductMedia(productId);
			for (int i = 0; i < mediaList.size(); i++)
			{

				if (null != mediaList.get(i).getMediaPath() && !"".equals(mediaList.get(i).getMediaPath()))
				{

					if (mediaList.get(i).getProductMediaType().equalsIgnoreCase("Audio Files"))
					{
						if ("null".equals(mediaList.get(i).getMediaPath()))
						{
							mediaList.get(i).setAudioPath(null);
							mediaList.get(i).setFullMediaPath(null);
							mediaList.get(i).setProductMediaID(null);
						}
						else
						{
							mediaList.get(i).setAudioPath((mediaList.get(i).getMediaPath()));
							mediaList.get(i).setFullMediaPath(mediaList.get(i).getFullMediaPath());
							mediaList.get(i).setProductMediaID(mediaList.get(i).getProductMediaID());
							audioCount = audioCount + 1;
						}
					}
					if (mediaList.get(i).getProductMediaType().equalsIgnoreCase("Video Files"))
					{
						if ("null".equals(mediaList.get(i).getMediaPath()))
						{
							mediaList.get(i).setVideoPath(null);
							mediaList.get(i).setFullMediaPath(null);
							mediaList.get(i).setProductMediaID(null);
						}
						else
						{
							mediaList.get(i).setVideoPath(((mediaList.get(i).getMediaPath())));
							mediaList.get(i).setFullMediaPath(mediaList.get(i).getFullMediaPath());
							mediaList.get(i).setProductMediaID(mediaList.get(i).getProductMediaID());
							videoCount = videoCount + 1;
						}
					}

				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in supplierQRCode::::" + e.getMessage());
			throw e;
		}
		request.setAttribute("productId", productId);
		request.setAttribute("productName", productName);
		request.setAttribute("productNameEscapeSpcl", productNameEscapeSpcl);
		session.setAttribute("ProductMediaList", mediaList);
		if (audioCount > 0)
		{
			session.setAttribute("audioSize", true);
		}
		else
		{
			session.setAttribute("audioSize", false);
		}
		if (videoCount > 0)
		{
			session.setAttribute("videoSize", true);
		}
		else
		{
			session.setAttribute("videoSize", false);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("supplierQRCode");
	}

	/**
	 * 
	 * @param manageProducts ManageProducts Contains product information.
	 * @param request HttpServletRequest object.
	 * @param session HttpSession object.
	 * @return ModelAndView contains view name of manage product screen.
	 * @throws ScanSeeServiceException  if any exception while processing the request.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/supplierQRCodeGen.htm", method = RequestMethod.POST)
	public ModelAndView supplierQRCodeGeneration(@ModelAttribute("qrcode") ManageProducts manageProducts,BindingResult result, HttpServletRequest request, HttpSession session) throws  ScanSeeServiceException
	{
		final String methodName = "supplierQRCodeGeneration";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int productId = (int) manageProducts.getProductID();
		final String productName = manageProducts.getProductName();
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int userId = loginUser.getSupplierId();
		request.removeAttribute("message");
		List<Product> mediaList = new ArrayList<Product>();
		mediaList = (ArrayList<Product>) session.getAttribute("ProductMediaList");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		QRDetails pageDetails = new QRDetails();
		try
		{
			pageDetails = supplierService.supplierQRCodeGeneration(manageProducts, userId, mediaList);
			request.setAttribute("productId", productId);
			request.setAttribute("productName", productName);
			request.setAttribute("QRCodeDetails", pageDetails);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in supplierQRCode::::" + e.getMessage());
			throw e;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (pageDetails.getMessage() == null)
		{
			return new ModelAndView("supplierQRCodeDisplay");
		}
		else
		{
			request.setAttribute("message", pageDetails.getMessage());
			return new ModelAndView("supplierQRCode");
		}
		
	}
}

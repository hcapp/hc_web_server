package supplier.controller;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Rebates;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.Utility;

/**
 * ManageHotDealController is a controller class for managing all supplier Rebates screen.
 * 
 * @author Created by SPAN.
 */
@Controller
@RequestMapping("/rebatesMfg.htm")
public class ManageRebatesController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageRebatesController.class);
	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "manageRebates";
	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();
	
	/**
	 * This controller method will display all Rebates or by passing Rebate Name as input parameter by call service and DAO methods.
	 * 
	 * @param rebatesVo Rebates instance.
	 * @param request HttpServletRequest instance.
	 * @param model  ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings("unused")
	@RequestMapping(method = RequestMethod.GET)
	public final ModelAndView showPage(@ModelAttribute("rebatessearchForm") Rebates rebatesVo, HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeWebSqlException, ScanSeeServiceException 
	{
		LOG.info("Inside ManageRebatesController : showPage");
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = new SearchForm();
		int lowerLimit = 0;
		String sDate = null;
		String eDate = null;
		
		request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
		request.getSession().removeAttribute("listOfRebates");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		//removing if any pagination object is active
		session.removeAttribute(ApplicationConstants.PAGINATION); 
		// session.invalidate();
		try {
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != pageFlag && "true".equals(pageFlag)) 
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) 
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
			}
			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchform", objForm);
			}
			final Users user = (Users) session.getAttribute(ApplicationConstants.LOGINUSER);
			if (ISDUBUGENABLED) {
				LOG.debug("Supplier ID****" + user.getSupplierId());
			}
			rebatesVo.setRebName(null);
			//objForm.setSearchKey(rebatesVo.getRebName().trim());

			final SearchResultInfo listOfRebates =  supplierService.getRebatesAll(Long.valueOf(user.getSupplierId()), objForm, lowerLimit);
			String strAmount = null;
			for (int i = 0; i < listOfRebates.getRebatesList().size(); i++)
			{
				strAmount = listOfRebates.getRebatesList().get(i).getRebAmount();
				strAmount = Utility.formatDecimalValue(strAmount);
				listOfRebates.getRebatesList().get(i).setRebAmount(strAmount);

				sDate = listOfRebates.getRebatesList().get(i).getRebStartDate();
				sDate = Utility.getUsDateFormat(sDate);
				listOfRebates.getRebatesList().get(i).setRebStartDate(sDate);

				eDate = listOfRebates.getRebatesList().get(i).getRebEndDate();
				eDate = Utility.getUsDateFormat(eDate);
				listOfRebates.getRebatesList().get(i).setRebEndDate(eDate);
			}
			request.getSession().setAttribute("listOfRebates", listOfRebates);

			if (listOfRebates != null)
			{
				final Pagination objPage = Utility.getPagination(
						listOfRebates.getTotalSize(), currentPage,
				"rebatesMfg.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				request.getSession().setAttribute(ApplicationConstants.MESSAGE, "No More Rebates To Display");
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}
			//view = VIEW_NAME;
		}  catch (ScanSeeServiceException e) {
			 LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			 throw e;
		} catch (NullPointerException e) {
			 LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			 throw e;
		} catch (ParseException e) {
			 LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			 throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(VIEW_NAME);
	}
	
	/**
	 * This controller method will display  Rebates details by passing Rebate Name as input parameter by call service and DAO methods.
	 * 
	 * @param rebatesVo Rebates instance.
	 * @param request HttpServletRequest instance.
	 * @param model  ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param result  BindingResult instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws IOException will be thrown.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public final ModelAndView getRebatesByName(@ModelAttribute("rebatessearchForm") Rebates rebatesVo, BindingResult result, HttpServletRequest request, ModelMap model, HttpSession session)throws IOException, ScanSeeServiceException 
	{
		LOG.info("Inside ManageRebatesController : getRebatesByName");
		request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		request.getSession().removeAttribute("listOfRebates");
		//session.removeAttribute(ApplicationConstants.PAGINATION);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		Pagination objPage = null;
		String sDate = null;
		String eDate = null;
		try 
		{
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != pageFlag && "true".equals(pageFlag)) 
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) 
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
			}
			if (objForm == null) {
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchform", objForm);
			}
			final Users user = (Users) session.getAttribute(ApplicationConstants.LOGINUSER);
			if (ISDUBUGENABLED) {
				LOG.debug("Supplier ID****" + user.getSupplierId());
				LOG.debug("searchKey***" + searhKey);
			}

			if (rebatesVo.getRebName() != null) {
				final String rebName = rebatesVo.getRebName().trim();
				objForm.setSearchKey(rebName);
			}
			final SearchResultInfo listOfRebates =  supplierService.getRebatesAll(Long.valueOf(user.getSupplierId()), objForm, lowerLimit);
			if (listOfRebates.getRebatesList() != null || !listOfRebates.getRebatesList().isEmpty()) 
			{
				String strAmount = null;
				for (int i = 0; i < listOfRebates.getRebatesList().size(); i++)
				{
					strAmount = listOfRebates.getRebatesList().get(i).getRebAmount();
					strAmount = Utility.formatDecimalValue(strAmount);
					listOfRebates.getRebatesList().get(i).setRebAmount(strAmount);

					sDate = listOfRebates.getRebatesList().get(i).getRebStartDate();
					sDate = Utility.getUsDateFormat(sDate);
					listOfRebates.getRebatesList().get(i).setRebStartDate(sDate);

					eDate = listOfRebates.getRebatesList().get(i).getRebEndDate();
					eDate = Utility.getUsDateFormat(eDate);
					listOfRebates.getRebatesList().get(i).setRebEndDate(eDate);
				}
				request.getSession().setAttribute("listOfRebates", listOfRebates);
				objPage = Utility.getPagination(listOfRebates.getTotalSize(), currentPage, "rebatesMfg.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				result.reject("Rebates.status");
				request.setAttribute("manageRbtssFont", "font-weight:regular;color:red;");
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (NullPointerException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (ParseException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(VIEW_NAME);
	}
}

package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SearchService;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.Offer;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;
import common.exception.ScanSeeServiceException;
import common.pojo.HotDealInfo;
import common.pojo.LoginVO;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductReview;

@Controller
public class ProductInfoController
{

	private static final Logger LOG = LoggerFactory.getLogger(ProductInfoController.class);

	@RequestMapping(value = "/productInfo.htm", method = RequestMethod.GET)
	public String showPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{

		LoginVO loginVO = new LoginVO();
		model.put("loginform", loginVO);
		return "productInfo";
	}

	@RequestMapping(value = "/productInfo.htm", method = RequestMethod.POST)
	public ModelAndView productInfo(@ModelAttribute("productinfoform") ProductVO productinfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException, ScanSeeServiceException
	{

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SearchService searchService = (SearchService) appContext.getBean("searchService");
		FindNearByDetails findNearByDetails = null;
		ProductVO productInfoDetail = null;
		SearchForm objForm = null;
		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		ExternalAPIInformation apiInfo = null;
		ProductDetail productDet = null;
		OnlineStoresRequest onlineStores = null;
		OnlineStoresService onlineStoresService = null;
		OnlineStores onlineStoresinfo = null;
		List<Offer> onlineStoresList = null;
		String review = null;
		ArrayList<ProductReview> productReviewslist = null;
		try
		{
			Integer productID = productinfo.getProductID();
			Integer retailLocationID = productinfo.getRetailLocationID();
			LOG.info("productID*************" + productID);
			LOG.info("retailLocationID++++++++++++++++++" + retailLocationID);
			productInfoDetail = searchService.getAllProductInfo(retailLocationID, productID);
			productReviewslist = searchService.getProductReviews(productID);

			Users loginUser = (Users) session.getAttribute("loginuser");
			objForm = (SearchForm) session.getAttribute("searchForm");
			objAreaInfoVO.setProductId(productID);
			if (loginUser != null)
			{

				objAreaInfoVO.setZipCode(loginUser.getPostalCode());
				objAreaInfoVO.setUserID(loginUser.getUserID());
			}
			else
			{
				if (null != objForm)
				{
					objAreaInfoVO.setZipCode(objForm.getZipCode());
				}

			}
			if (null!=objAreaInfoVO.getZipCode())
			{
				// find near by stores
				findNearByDetails = searchService.findNearBy(objAreaInfoVO);
			}

			if (findNearByDetails != null)
			{
				request.setAttribute("findNearByDetails", findNearByDetails.getFindNearByDetail().get(0));
			}
			// online stores
			if (loginUser != null)
			{
				apiInfo = searchService.externalApiInfo("FindOnlineStores");
				productDet = searchService.fetchProductDetails(productID.toString());
				onlineStoresService = new OnlineStoresServiceImpl();
				onlineStores = new OnlineStoresRequest();
				onlineStores.setPageStart("0");
				onlineStores.setUpcCode(productDet.getScanCode());
				onlineStores.setUserId(loginUser.getUserID().toString());
				onlineStores.setValues();
				onlineStoresinfo = onlineStoresService.onlineStoresInfoEmailTemplate(apiInfo, onlineStores);

				if (onlineStoresinfo != null)
				{
					if (!onlineStoresinfo.getOffers().isEmpty())
					{
						onlineStoresList = onlineStoresinfo.getOffers();

						request.setAttribute("OnlineStoresInfo", onlineStoresList.get(0));

					}

				}
			}

		}
		catch (ScanSeeServiceException e)
		{

			LOG.error("Exception occurred in productInfo::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		if (productReviewslist != null && !productReviewslist.isEmpty())
		{

			ProductReview productReview = productReviewslist.get(0);

			request.setAttribute("productReviewslist", productReview);

		}
		session.setAttribute("productinfo", productInfoDetail);
		request.setAttribute("searchForm", objForm);

		return new ModelAndView("productdetails");
	}

	@RequestMapping(value = "/productInfowithdeal.htm", method = RequestMethod.POST)
	public ModelAndView productInfoWithDeals(@ModelAttribute("productinfoform") ProductVO productinfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException, ScanSeeServiceException
	{

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SearchService searchService = (SearchService) appContext.getBean("searchService");
		ProductVO productInfoDetail = null;
		Integer productID = productinfo.getProductID();
		LOG.info("productID*************" + productID);
		List<ProductVO> prodAssociatedHotDealList = null;

		FindNearByDetails findNearByDetails = null;
		SearchForm objForm = null;
		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		ExternalAPIInformation apiInfo = null;
		ProductDetail productDet = null;
		OnlineStoresRequest onlineStores = null;
		OnlineStoresService onlineStoresService = null;
		OnlineStores onlineStoresinfo = null;
		List<Offer> onlineStoresList = null;
		ArrayList<ProductReview> productReviewslist = null;
		try
		{

			productInfoDetail = searchService.getAllProductInfoWithDeals(productID);
			productReviewslist = searchService.getProductReviews(productID);
			prodAssociatedHotDealList = searchService.getProductAssociatedWithDeals(productID);
			Users loginUser = (Users) session.getAttribute("loginuser");
			objForm = (SearchForm) session.getAttribute("searchForm");
			objAreaInfoVO.setProductId(productID);

			if (loginUser != null)
			{

				objAreaInfoVO.setZipCode(loginUser.getPostalCode());

				objAreaInfoVO.setUserID(loginUser.getUserID());
			}
			else
			{

				objAreaInfoVO.setZipCode(objForm.getZipCode());
			}

			// find near by stores
			findNearByDetails = searchService.findNearBy(objAreaInfoVO);

			if (findNearByDetails != null)
			{
				request.setAttribute("findNearByDetails", findNearByDetails.getFindNearByDetail().get(0));
			}

			// online stores

			if (loginUser != null)
			{

				apiInfo = searchService.externalApiInfo("FindOnlineStores");

				productDet = searchService.fetchProductDetails(productID.toString());
				onlineStoresService = new OnlineStoresServiceImpl();
				onlineStores = new OnlineStoresRequest();
				onlineStores.setPageStart("0");
				onlineStores.setUpcCode(productDet.getScanCode());
				onlineStores.setUserId(loginUser.getUserID().toString());
				onlineStores.setValues();
				onlineStoresinfo = onlineStoresService.onlineStoresInfoEmailTemplate(apiInfo, onlineStores);

				if (onlineStoresinfo != null)
				{
					if (!onlineStoresinfo.getOffers().isEmpty())
					{
						onlineStoresList = onlineStoresinfo.getOffers();

						request.setAttribute("OnlineStoresInfo", onlineStoresList.get(0));

					}

				}
			}

			if (productReviewslist != null && !productReviewslist.isEmpty())
			{

				ProductReview productReview = productReviewslist.get(0);

				request.setAttribute("productReviewslist", productReview);

			}

		}
		catch (ScanSeeServiceException e)
		{

			LOG.error("Exception occurred in productInfoWithDeals::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		session.setAttribute("productinfowithdeals", productInfoDetail);
		session.setAttribute("prodAssociatedhotdeallist", prodAssociatedHotDealList);

		request.setAttribute("searchForm", objForm);

		return new ModelAndView("productdetailswithdeals");
	}

	@RequestMapping(value = "/productDealInfo.htm", method = RequestMethod.POST)
	public ModelAndView productDealInfo(@ModelAttribute("productinfoform") HotDealInfo hotDealInfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException, ScanSeeServiceException
	{

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SearchService searchService = (SearchService) appContext.getBean("searchService");
		List<ProductVO> hotDealAssociatedProductList = null;
		HotDealInfo productInfoDetail = null;

		Integer hotDealID = hotDealInfo.getHotDealID();

		LOG.info("hotDealID*************" + hotDealID);

		try
		{

			productInfoDetail = searchService.getDealsInfo(hotDealID);
			hotDealAssociatedProductList = searchService.getDealWithProductAssociated(hotDealID);
			if (hotDealAssociatedProductList != null && !hotDealAssociatedProductList.isEmpty())
			{
				session.setAttribute("hotdealassociatedproductlist", hotDealAssociatedProductList);
			}
		}
		catch (ScanSeeServiceException e)
		{

			LOG.error("Exception occurred in  productDealInfo::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		session.setAttribute("productdealinfo", productInfoDetail);

		return new ModelAndView("productdealinfo");
	}

}

package supplier.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;
import supplier.validator.HotDealsDetailsValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.HotDealInfo;
import common.pojo.Product;
import common.pojo.Retailer;
import common.pojo.State;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

/**
 * SupplierHotDealReRunController is a controller class for rerun supplier product hot deals.
 * 
 * @author Created by SPAN.
 */
@Controller
// @RequestMapping("/rerunHotDeals.htm")
public class RerunDealsController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RerunDealsController.class);

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "hotDealRerun";

	/**
	 * Variable view declared as String.
	 */
	private String view = VIEW_NAME;
	/**
	 * Variable viewRedirect declared as String.
	 */
	private final String viewRedirect = "hotDeal.htm";

	/**
	 * Variable locationSetUpValidator declared as instance of
	 * HotDealRetailerDetailsValidator.
	 */
	private HotDealsDetailsValidator dealsDetailsValidator;

	/**
	 * To HotDealsDetailsValidator to set.
	 *    
	 * @param dealsDetailsValidator to set.
	 */
	@Autowired
	public final void setDealsDetailsValidator(HotDealsDetailsValidator dealsDetailsValidator)
	{
		this.dealsDetailsValidator = dealsDetailsValidator;
	}

	/**
	 * This controller method will display the HotDeals deals by passing HotDealId as input parameter by call service and DAO methods.
	 * 
	 * @param hotDealInfo HotDealInfo instance as request parameter
	 * @param request HttpServletRequest instance.
	 * @param model  ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 * @throws ParseException will be thrown.
	 */
	@RequestMapping(value = "/rerunHotDeals.htm", method = RequestMethod.GET)
	public final String showPage(@ModelAttribute("rerundealsform") HotDealInfo hotDealInfo, HttpServletRequest request, HttpSession session,
			ModelMap model) throws ParseException, ScanSeeServiceException
	{
		LOG.info("Inside RerunDealsController : showPage");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		request.getSession().removeAttribute("message");
		ArrayList<State> states = null;
		states = supplierService.getAllStates();
		session.setAttribute("statesList", states);
		String dealStartTime = null;
		String dealEndTime = null;
		request.getSession().removeAttribute("selRetailerLoc");
		request.getSession().removeAttribute("selRetailer");
		request.getSession().removeAttribute("hotdealcity");
		request.getSession().removeAttribute("rerunProdImage");
		String[] tempStartTimeHrsMin = null;
		String[] tempEndTimeHrsMin = null;
		ArrayList<Category> arBCategoryList = null;
		try
		{
			arBCategoryList = supplierService.getAllBusinessCategory();
			final List<HotDealInfo> dealsList = (List<HotDealInfo>) supplierService.getHotDealByID(hotDealInfo.getHotDealID());
			if (dealsList != null && !dealsList.isEmpty())
			{
				String salePrice = null;
				String regPrice = null;
				String sDate = null;
				String eDate = null;
				for (int i = 0; i < dealsList.size(); i++)
				{
					hotDealInfo.setHotDealName(dealsList.get(i).getHotDealName());
					hotDealInfo.setPrice(dealsList.get(i).getPrice());
					hotDealInfo.setSalePrice(dealsList.get(i).getSalePrice());
					salePrice = dealsList.get(i).getSalePrice();

					salePrice = Utility.formatDecimalValue(salePrice);
					hotDealInfo.setSalePrice(salePrice);
					regPrice = dealsList.get(i).getPrice();
					regPrice = Utility.formatDecimalValue(regPrice);
					hotDealInfo.setPrice(regPrice);

					hotDealInfo.setHotDealShortDescription(dealsList.get(i).getHotDealShortDescription());
					hotDealInfo.setHotDealLongDescription(dealsList.get(i).getHotDealLongDescription());
					hotDealInfo.setHotDealTermsConditions(dealsList.get(i).getHotDealTermsConditions());
					sDate = dealsList.get(i).getDealStartDate();
					eDate = dealsList.get(i).getDealEndDate();
					hotDealInfo.setbCategory(dealsList.get(i).getbCategory());
					hotDealInfo.setbCategoryHidden(dealsList.get(i).getbCategory());
					if (sDate != null) {
						sDate = Utility.formattedDate(sDate);
						hotDealInfo.setDealStartDate(sDate);
					}
					if (eDate != null) {
						eDate = Utility.formattedDate(eDate);
						hotDealInfo.setDealEndDate(eDate);
					}
					dealStartTime = dealsList.get(i).getDealStartTime();
					tempStartTimeHrsMin = dealStartTime.split(":");
					hotDealInfo.setDealStartHrs(tempStartTimeHrsMin[0]);
					hotDealInfo.setDealStartMins(tempStartTimeHrsMin[1]);
					dealEndTime = dealsList.get(i).getDealEndTime();
					tempEndTimeHrsMin = dealEndTime.split(":");
					hotDealInfo.setDealEndhrs(tempEndTimeHrsMin[0]);
					hotDealInfo.setDealEndMins(tempEndTimeHrsMin[1]);
					hotDealInfo.setDealTimeZoneId(dealsList.get(i).getDealTimeZoneId());
					hotDealInfo.setHotDealTermsConditions(dealsList.get(i).getHotDealTermsConditions());
					hotDealInfo.setHotDealID(dealsList.get(i).getHotDealID());
					hotDealInfo.setRetailerLocID(dealsList.get(i).getRetailerLocID());
					hotDealInfo.setScanCode(dealsList.get(i).getScanCode());
					hotDealInfo.setUrl(dealsList.get(i).getUrl());
					if (!"".equals(dealsList.get(i).getProductId()) && dealsList.get(i).getProductId() != null)
						hotDealInfo.setProductId(dealsList.get(i).getProductId());
					hotDealInfo.setExistingProductIds(dealsList.get(i).getProductId());
					if (dealsList.get(i).getCity() != null)
					{
						final StringTokenizer tokenizer = new StringTokenizer(dealsList.get(i).getCity(), ApplicationConstants.COMMA);
						if (tokenizer.countTokens() > 1)
						{
							hotDealInfo.setCity("All");
							hotDealInfo.setCityHidden("All");
							hotDealInfo.setDealForCityLoc("City");
							request.getSession().setAttribute("hotdealcity", "All");
						} else {
							hotDealInfo.setCity(dealsList.get(i).getCity());
							hotDealInfo.setCityHidden(dealsList.get(i).getCity());
							hotDealInfo.setDealForCityLoc("City");
							request.getSession().setAttribute("hotdealcity", dealsList.get(i).getCity());
						}

						request.getSession().removeAttribute("selRetailerLoc");
						request.getSession().removeAttribute("selRetailer");

					}
					else if (dealsList.get(i).getRetailID() != 0 && dealsList.get(i).getRetailerLocID() != 0)
					{
						session.setAttribute("selRetailer", String.valueOf(dealsList.get(i).getRetailID()));
						session.setAttribute("selRetailerLoc", String.valueOf(dealsList.get(i).getRetailerLocID()));
						hotDealInfo.setRetailID(dealsList.get(i).getRetailID());
						hotDealInfo.setRetailerLocID(dealsList.get(i).getRetailerLocID());
						hotDealInfo.setDealForCityLoc("Location");
						request.getSession().removeAttribute("hotdealcity");
					}
					else
					{
						hotDealInfo.setDealForCityLoc("");
					}
				}
				request.getSession().removeAttribute("pdtInfoList");
				final ArrayList<Product> pdtInfoList = (ArrayList<Product>) supplierService.getPdtInfoForDealRerun(hotDealInfo.getProductId());
				if (pdtInfoList != null && !pdtInfoList.isEmpty())
				{
					request.getSession().setAttribute("pdtInfoList", pdtInfoList);
				}
				ArrayList<Retailer> retailers = null;
				retailers = supplierService.getRetailers();
				session.setAttribute("retailerList", retailers);
				ArrayList<TimeZones> timeZonelst = null;
				timeZonelst = supplierService.getAllTimeZones();
				session.setAttribute("timeZoneslst", timeZonelst);
				session.setAttribute("categoryList", arBCategoryList);
			} 
			model.put("rerundealsform", hotDealInfo);
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (NullPointerException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return VIEW_NAME;
	}

	/**
	 * This controller method will add one new rerun HotDeals details screen by call service and DAO methods.
	 * 
	 * @param hotDealInfo HotDealInfo instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/rerunHotDeals.htm", method = RequestMethod.POST)
	public final ModelAndView editHotDeals(@ModelAttribute("rerundealsform") HotDealInfo hotDealInfo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside RerunDealsController : editHotDeals");
		String compDate = null;
		final Date currentDate = new Date();
		String strBCategory = null;
		String strpCity = null;
		int strRet = 0;
		int strRetLoc = 0;
		try
		{
			String isDataInserted = null;
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final StringBuffer allCities = new StringBuffer();
			// String startDate = hotDealInfo.getDealStartDate();
			// String endDate = hotDealInfo.getDealEndDate();
			/*
			 * if (startDate.contains("/")) { startDate =
			 * Utility.getFormattedDate(startDate);
			 * hotDealInfo.setDealStartDate(startDate); } else if
			 * (startDate.contains("-")) {
			 * hotDealInfo.setDealStartDate(startDate); } if
			 * (endDate.contains("/")) { endDate =
			 * Utility.getFormattedDate(endDate);
			 * hotDealInfo.setDealEndDate(endDate); } else if
			 * (endDate.contains("-")) { hotDealInfo.setDealEndDate(endDate); }
			 */
			final String sTime = String.valueOf(hotDealInfo.getDealStartHrs()) + ":" + String.valueOf(hotDealInfo.getDealStartMins());
			final String eTime = String.valueOf(hotDealInfo.getDealEndhrs()) + ":" + String.valueOf(hotDealInfo.getDealEndMins());
			hotDealInfo.setDealStartTime(sTime);
			hotDealInfo.setDealEndTime(eTime);
			strBCategory = hotDealInfo.getbCategory();
			hotDealInfo.setbCategoryHidden(hotDealInfo.getbCategory());
			strpCity = hotDealInfo.getCity();
			strRet = hotDealInfo.getRetailID();
			strRetLoc = hotDealInfo.getRetailerLocID();
			if (strBCategory.equalsIgnoreCase(ApplicationConstants.ZERO))
			{
				dealsDetailsValidator.validate(hotDealInfo, result, ApplicationConstants.BUSINESSCATEGORY);
			}
			if ("City".equals(hotDealInfo.getDealForCityLoc()))
			{
				if (strpCity.equalsIgnoreCase(""))
				{
					dealsDetailsValidator.validate(hotDealInfo, result, ApplicationConstants.POPULATIONCENTER);
				}
			}
			
			if ("Location".equals(hotDealInfo.getDealForCityLoc()))
			{
				if (strRet == 0) {
					dealsDetailsValidator.validate(hotDealInfo, result, ApplicationConstants.DEALRETAILER);
				}
				if (strRetLoc == 0) {
					dealsDetailsValidator.validate(hotDealInfo, result, ApplicationConstants.DEALRETAILERLOC);
				}
			}
			if (hotDealInfo != null)
			{
				dealsDetailsValidator.validate(hotDealInfo, result);
				if (!result.hasErrors())
				{
					compDate = Utility.compareCurrentDate(hotDealInfo.getDealEndDate(), currentDate);
					if (null != compDate)
					{
						dealsDetailsValidator.validate(hotDealInfo, result, ApplicationConstants.DATEENDCURRENT);
					} else {
						compDate = Utility.compareDate(hotDealInfo.getDealStartDate(), hotDealInfo.getDealEndDate());
						if (null != compDate)
						{
							dealsDetailsValidator.validate(hotDealInfo, result, ApplicationConstants.DATEAFTER);
						}
					}
				}
			}

			if (result.hasErrors())
			{
				if ("City".equals(hotDealInfo.getDealForCityLoc()))
				{
					request.getSession().setAttribute("hotdealcity", hotDealInfo.getCity());
					session.removeAttribute("selRetailer");
					session.removeAttribute("selRetailerLoc");
				}
				else if ("Location".equals(hotDealInfo.getDealForCityLoc()))
				{
					session.removeAttribute("hotdealcity");
					session.setAttribute("selRetailer", String.valueOf(hotDealInfo.getRetailID()));
					session.setAttribute("selRetailerLoc", String.valueOf(hotDealInfo.getRetailerLocID()));
				}
				view = VIEW_NAME;
			}
			else
			{
				if (null != hotDealInfo.getCity())
				{
					if ("All".equals(hotDealInfo.getCity()))
					{
						final ArrayList<City> cities = (ArrayList<City>) request.getSession().getAttribute("hotdealpopcenters");
						for (int i = 0; i < cities.size(); i++)
						{
							allCities.append(cities.get(i).getPopulationCenterID());
							allCities.append(ApplicationConstants.COMMA);
						}
						hotDealInfo.setCity(allCities.toString().substring(0, allCities.toString().length() - 1));
					}
				}
				isDataInserted = supplierService.updateHotDeal(hotDealInfo, Long.valueOf(user.getSupplierId()), "rerundeal");
				if (isDataInserted == null || isDataInserted == ApplicationConstants.FAILURE)
				{
					//request.getSession().setAttribute("message", "Error while re-run");
					view = VIEW_NAME;
				}
				else
				{
					view = viewRedirect;
					request.getSession().setAttribute("message", "re-run executed successfully");
				}
			}
		}
		catch (NullPointerException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (view.equals(viewRedirect))
		{
			return new ModelAndView(new RedirectView(view));
		} else {
			return new ModelAndView(view);
		}
	}

	/**
	 * This ModelAttribute sort Deal start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("DealStartHours")
	public Map<String, String> populateDealStartHrs() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method returns a negative integer, zero, or a positive 
	 *  integer as the first argument is less than, equal to, or greater than the second.
	 * 
	 * @param unsortMap as request parameter.
	 * @return sortedMap returns a integer value.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException
	{
		final List list = new LinkedList(unsortMap.entrySet());
		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2)
			{
				return ((Comparable) ((Map.Entry) o1).getValue()).compareTo(((Map.Entry) o2).getValue());
			}
		});
		// put sorted list into map again
		final Map sortedMap = new LinkedHashMap();
		Map.Entry entry = null;
		for (final Iterator it = list.iterator(); it.hasNext();)
		{
			entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort Deal start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("DealStartMinutes")
	public Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method will display supplier HotDeals details in iphone screen formate.
	 * 
	 * @param hotDealInfo HotDealInfo instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @param model ModelMap instance as parameter.
	 * @return VIEW_NAME addHotDeals view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/previewrerunsuppldeals.htm", method = RequestMethod.POST)
	public final ModelAndView previewPage(@ModelAttribute("previewrerunsuppldealsform") HotDealInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside RerunDealsController : previewPage");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		final String salePrice = request.getParameter("salePrice");
		final String price = request.getParameter("price");
		final String dealStartDate = request.getParameter("dealStartDate");
		final String hotDealLongDescription = request.getParameter("hotDealLongDescription");
		final String hotDealName = request.getParameter("hotDealName");
		List<HotDealInfo> dealProdList = null;
		final String prdname = hotDealInfo.getScanCode();
		final String []str = prdname.split(ApplicationConstants.COMMA);
		final String productName = str[0].trim();
		hotDealInfo.setProductName(productName);

		request.getSession().setAttribute("reRunSupplSalePrice", salePrice);
		request.getSession().setAttribute("reRunSupplPrice", price);
		request.getSession().setAttribute("reRunSupplDealStartDate", dealStartDate);
		request.getSession().setAttribute("reRunSupplHotDealLongDescription", hotDealLongDescription);
		request.getSession().setAttribute("reRunSupplHotDealName", hotDealName);
		request.getSession().removeAttribute("PreviewProductID");

		hotDealInfo.setHotDealName(hotDealName);
		hotDealInfo.setDealStartDate(dealStartDate);
		hotDealInfo.setPrice(price);
		hotDealInfo.setSalePrice(salePrice);
		hotDealInfo.setHotDealLongDescription(hotDealLongDescription);

		if (null != hotDealInfo.getProductId() || !"".equals(hotDealInfo.getProductId()))
		{
			session.setAttribute("PreviewProductID", hotDealInfo.getProductId());
		}
		if (null != hotDealInfo.getDealForCityLoc())
		{
			if ("City".equals(hotDealInfo.getDealForCityLoc()))
			{
				request.getSession().setAttribute("hotdealcity", hotDealInfo.getCity());
				request.getSession().removeAttribute("selRetailer");
				request.getSession().removeAttribute("selRetailerLoc");
			}
			else if ("Location".equals(hotDealInfo.getDealForCityLoc()))
			{
				session.setAttribute("selRetailer", String.valueOf(hotDealInfo.getRetailID()));
				session.setAttribute("selRetailerLoc", String.valueOf(hotDealInfo.getRetailerLocID()));
				request.getSession().removeAttribute("hotdealcity");
			}
		}
		try
		{
			if (productName != null && !"".equals(productName))
			{
				dealProdList = supplierService.getProdDetailsInfo(Long.valueOf(loginUser.getSupplierId()), productName);
				if (dealProdList != null && !dealProdList.isEmpty())
				{
					session.setAttribute("rerunProdImage", dealProdList.get(0).getProductImagePath());
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			 LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			 throw e;
		}
		model.put("previewrerunsuppldealsform", hotDealInfo);
		LOG.info("previewPage in RerunDealsCntroller:: Exiting POST Method");
		return new ModelAndView("rerunsuppldealspreview");
	}
}

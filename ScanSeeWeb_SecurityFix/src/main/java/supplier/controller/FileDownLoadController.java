/**
 * 
 */
package supplier.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * @author manjunatha_gh
 */
@Controller
@RequestMapping(value = "/fileController")
public class FileDownLoadController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(FileDownLoadController.class);

	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public void downLoadFile(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		final String methodName = "downLoadFile";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		// String fileName =
		// "D:/ScanSeebkp/ScanSeeDoc/SecurityDocuments/jboss-5.0.1.GA/server/default/deploy/ScanSeeWeb.war/uploadtemplates/Productupload.xlsx";
		String fileName = "/uploadtemplates/";
		FileInputStream fis = null;
		try
		{
			String templateType = request.getParameter("TType");
			String filepath = null;
			if (templateType != null)
			{
				if (templateType.equalsIgnoreCase("sproductupload"))
				{
					filepath = request.getRealPath("") + fileName + ApplicationConstants.SPRODUCTTEMPLATE;
				}
				else if (templateType.equalsIgnoreCase("sattributeupload"))
				{
					filepath = request.getRealPath("") + fileName + ApplicationConstants.SATTRIBUTETEMPLATE;
				}
				else if (templateType.equalsIgnoreCase("rproductpload"))
				{
					filepath = request.getRealPath("") + fileName + ApplicationConstants.RPRODUCTTEMPLATE;
				}
				else if (templateType.equalsIgnoreCase("rlocationupload"))
				{
					filepath = request.getRealPath("") + fileName + ApplicationConstants.RLOCATIONTEMPLATE;
				}
			}
			LOG.info("requested File for download " + templateType);
			File file = new File(filepath);
			fis = new FileInputStream(file);
			response.setContentType("application/xlsx");
			response.setHeader("Content-Disposition", "attachment; filename=uploadFormat.csv");
			IOUtils.copy(fis, response.getOutputStream());
			response.flushBuffer();
		}
		catch (FileNotFoundException exception)
		{
			LOG.error("Error occurred ", exception);
			throw exception;
		}
		// IOUtils.copy(input, output)
		catch (IOException exception)
		{
			LOG.error("Error occurred ", exception);
			throw exception;
		}
		finally
		{
			fis.close();
		}
	}

	@RequestMapping(value = "/downloadimg", method = RequestMethod.GET)
	public void downLoadFileImage(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		final String methodName = "downloadimg";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String fileSeparator = System.getProperty("file.separator");
		// String fileName =
		// "D:/ScanSeebkp/ScanSeeDoc/SecurityDocuments/jboss-5.0.1.GA/server/default/deploy/ScanSeeWeb.war/uploadtemplates/Productupload.xlsx";
		String fileName = Utility.getMediaPath().toString() + ApplicationConstants.QRPATH + fileSeparator;

		FileInputStream fis = null;
		try
		{
			String ImagName = request.getParameter("ImagName");
			String filepath = fileName + ImagName;

			LOG.info("requested File for download " + filepath);
			File file = new File(filepath);
			fis = new FileInputStream(file);
			response.setContentType("image/png");
			response.setHeader("Content-Disposition", "attachment; filename=" + ImagName);
			IOUtils.copy(fis, response.getOutputStream());
			response.flushBuffer();
		}
		catch (FileNotFoundException exception)
		{
			LOG.error("Error occurred ", exception);
			throw exception;
		}
		// IOUtils.copy(input, output)
		catch (IOException exception)
		{
			LOG.error("Error occurred ", exception);
			throw exception;
		}
		finally
		{
			fis.close();
		}
	}

	@RequestMapping(value = "/downloadexportfile", method = RequestMethod.GET)
	public void downLoadEcportFile(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		final String methodName = "downloadimg";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		FileInputStream fis = null;
		try
		{
			String filepath = request.getParameter("filePath");
			String fileName = request.getParameter("fileName");
			String fileFromat = request.getParameter("fileFromat");

			LOG.info("requested File for download " + filepath);
			File file = new File(filepath);
			fis = new FileInputStream(file);

			if (fileFromat != null && fileFromat.equals("PDF"))
			{
				response.setContentType("application/pdf");
			}
			else if (fileFromat != null && fileFromat.equals("EXCEL"))
			{
				response.setContentType("application/xlsx");
			}

			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			IOUtils.copy(fis, response.getOutputStream());
			response.flushBuffer();
		}
		catch (FileNotFoundException exception)
		{
			LOG.error("Error occurred ", exception);
			throw exception;
		}
		// IOUtils.copy(input, output)
		catch (IOException exception)
		{
			LOG.error("Error occurred ", exception);
			throw exception;
		}
		finally
		{
			fis.close();
		}
	}
}

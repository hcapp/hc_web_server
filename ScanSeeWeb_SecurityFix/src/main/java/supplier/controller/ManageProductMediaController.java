package supplier.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ManageProducts;
import common.pojo.Product;
import common.pojo.Users;

@Controller
public class ManageProductMediaController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(ManageProductMediaController.class);

	@RequestMapping(value = "/managemedia.htm", method = RequestMethod.GET)
	public ModelAndView manageProductMedia(
			@ModelAttribute("manageform") ManageProducts manageProducts,
			BindingResult result, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, ScanSeeServiceException {
		final String methodName = "manageProductMedia";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext
				.getBean("supplierService");
		List<Product> mediaList = null;
		List<Product> audiovideoList = null;
		int productId = (int) manageProducts.getProductID();
		manageProducts.setProductID(productId);
		session.setAttribute("productId", productId);
		int imagecount = 0;
		int audioCount = 0;
		int videoCount = 0;

		LOG.info("Product ID is*************" + productId);
		try {
			mediaList = supplierService.fetchProductMedia(productId);

			for (int i = 0; i < mediaList.size(); i++) {

				if (!mediaList.get(i).getMediaPath().equals(null)
						&& !"".equals(mediaList.get(i).getMediaPath())) {

					if (mediaList.get(i).getProductMediaType()
							.equalsIgnoreCase("Image Files")) {
						if ("null".equals(mediaList.get(i).getMediaPath())) {
							mediaList.get(i).setProductImagePath(null);
						} else {
							mediaList.get(i).setProductImagePath(
									mediaList.get(i).getMediaPath());
							imagecount = imagecount + 1;

						}

					}
					if (mediaList.get(i).getProductMediaType()
							.equalsIgnoreCase("Audio Files")) {
						if ("null".equals(mediaList.get(i).getMediaPath())) {
							mediaList.get(i).setAudioPath(null);
						} else {
							mediaList.get(i).setAudioPath(
									(mediaList.get(i).getMediaPath()));
							audioCount = audioCount + 1;
						}
					}
					if (mediaList.get(i).getProductMediaType()
							.equalsIgnoreCase("Video Files")) {
						if ("null".equals(mediaList.get(i).getMediaPath())) {
							mediaList.get(i).setVideoPath(null);
						} else {
							mediaList.get(i).setVideoPath(
									((mediaList.get(i).getMediaPath())));
							videoCount = videoCount + 1;
						}
					}
					audiovideoList = mediaList;

				}

			}
			manageProducts.setImageCount(imagecount);
			manageProducts.setAudioCount(audioCount);
			manageProducts.setVideoCount(videoCount);
			LOG.info("Media List from fetchProductMedia" + mediaList.size());
		}

		catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in manageProductMedia::::"
					+ e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		model.addAttribute("manageform", manageProducts);
		session.setAttribute("ProductMediaList", audiovideoList);
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView("productMediaList");
	}

	@RequestMapping(value = "/managemedia.htm", method = RequestMethod.POST)
	public ModelAndView addProductMedia(
			@ModelAttribute("manageform") ManageProducts productVo,
			BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws Exception, ScanSeeServiceException {
		LOG.info("Entering in to addProductMedia Controller......");
		final String methodName = "addProductMedia";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String status = null;
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext
				.getBean("supplierService");
		Users loginUser = (Users) request.getSession()
				.getAttribute("loginuser");
		String realPath = request.getRealPath("");
		List<Product> mediaList = null;
		String view = "productMediaList";
		int productId = (int) productVo.getProductID();
		LOG.info("Product ID is*************" + productId);
		List<Product> audiovideoList = null;
		int imagecount = 0;
		int audioCount = 0;
		int videoCount = 0;
		int imageSize = 0;
		int audioSize = 0;
		int videoSize = 0;
       String mediaType = productVo.getMediaType();
		try {
			
			
			//if image upload,checking whether user clicks on image upload button
		if(mediaType.equalsIgnoreCase("Image")){
				
				imagecount = productVo.getImageCount();
				CommonsMultipartFile[] imageFile = productVo.getImageFile();
				/*for(int i=0;i<imageFile.length; i++){
					CommonsMultipartFile obj= imageFile[i];
					if(obj.getSize()>0){
						if(obj.getOriginalFilename() != "" ){
						imageFile[i] = obj;
						
						}
					}
				}*/
				if (imageFile.length > 0  ) {
					imageSize = (imageFile.length)-1;
				}
			if (imageSize != 0) {
				if ((imagecount + imageSize) > 5) {

					result.rejectValue("imageFile",
							"Maximum 5 image files can be Uploaded",
							"Maximum 5 image files can be Uploaded");
					
				}

				else {
					
					status = supplierService.addProductMedia(productVo,
							realPath, loginUser, productId, "image");
				}
			}
		}
		//if Audio Upload,checking whether user clicks on Audio upload button
			if(mediaType.equalsIgnoreCase("Audio"))
		{
				audioCount = productVo.getAudioCount();
				CommonsMultipartFile[] audioFile = productVo.getAudioFile();
				if (audioFile.length > 0) {
					audioSize = (audioFile.length)-1;
				}
			if (audioSize != 0) {
				if ((audioSize + audioCount) > 5) {
				
					result.rejectValue("audioFile",
							"Maximum 5 Audio files can be Uploaded",
							"Maximum 5 Audio files can be Uploaded");
				}

				else {
					
					status = supplierService.addProductMedia(productVo,
							realPath, loginUser, productId, "audio");
				}
			}
		}
			//If Video Upload,checking whether user clicks on video upload button
			if(mediaType.equalsIgnoreCase("Video"))
			{
				videoCount = productVo.getVideoCount();
				CommonsMultipartFile[] videoFile = productVo.getVideoFile();
				if (videoFile.length > 0) {
					videoSize =( videoFile.length)-1;
				}
			if (videoSize != 0) {
				if ((videoSize + videoCount) > 5) {
					
					result.rejectValue("videoFile",
							"Maximum 5 video files can be Uploaded",
							"Maximum 5 video files can be Uploaded");
				}

				else {
					
					status = supplierService.addProductMedia(productVo,
							realPath, loginUser, productId, "video");
				}
			  }
			}

			videoCount = 0;
			audioCount = 0;
			imagecount = 0;
			mediaList = supplierService.fetchProductMedia(productId);

			for (int i = 0; i < mediaList.size(); i++) {

				
				if (null != mediaList.get(i).getMediaPath()
						&& !"".equals(mediaList.get(i).getMediaPath())) {

					if (mediaList.get(i).getProductMediaType()
							.equalsIgnoreCase("Image Files")) {
						if ("null".equals(mediaList.get(i).getMediaPath())) {
							mediaList.get(i).setProductImagePath(null);
						} else {
							mediaList.get(i).setProductImagePath(
									mediaList.get(i).getMediaPath());
							imagecount = imagecount + 1;
						}

					}
					if (mediaList.get(i).getProductMediaType()
							.equalsIgnoreCase("Audio Files")) {
						if ("null".equals(mediaList.get(i).getMediaPath())) {
							mediaList.get(i).setAudioPath(null);
						} else {
							mediaList.get(i).setAudioPath(
									(mediaList.get(i).getMediaPath()));
							audioCount = audioCount + 1;
						}
					}
					if (mediaList.get(i).getProductMediaType()
							.equalsIgnoreCase("Video Files")) {
						if ("null".equals(mediaList.get(i).getMediaPath())) {
							mediaList.get(i).setVideoPath(null);
						} else {
							mediaList.get(i).setVideoPath(
									((mediaList.get(i).getMediaPath())));
							videoCount = videoCount + 1;
						}
					}
					audiovideoList = mediaList;

				}
			}

			productVo.setImageCount(imagecount);
			productVo.setAudioCount(audioCount);
			productVo.setVideoCount(videoCount);

		}
		/*
		 * catch (Exception exception) {
		 * LOG.info("Exception in onSubmit() in addProductMedia",
		 * exception.getMessage()); result.reject("addProdcuMedia.error");
		 * return new ModelAndView(view); }
		 */
		catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in addProductMedia::::"
					+ e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		// model.addAttribute("manageform", productVo);
		session.setAttribute("ProductMediaList", audiovideoList);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(view);

	}

}

package supplier.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.HotDealInfo;

/**
 * validation used in supplier product hotdeals screen.
 * 
 * @author Created by SPAN.
 */
public class HotDealsDetailsValidator implements Validator {
	/**
	 * Variable dealStartDate declared as String.
	 */
	private final String dealStartDate = "dealStartDate";
	/**
	 * Variable dealEndDate declared as String.
	 */
	private final String dealEndDate = "dealEndDate";
	/**
	 * Getting the HotDealInfo Instance.
	 * 
	 * @param arg0 as request parameter.
	 * @return HotDealInfo instance.
	 */
	public final boolean supports(Class<?> arg0) {
		return HotDealInfo.class.isAssignableFrom(arg0);
	}

	/**
	 * This method validate supplier product hotdeals screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT))
		{
			errors.rejectValue(dealStartDate, "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT))
		{
			errors.rejectValue(dealEndDate, "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER))
		{
			errors.rejectValue(dealEndDate, "datebefore");
		}
		if (status.equals(ApplicationConstants.BUSINESSCATEGORY)) {
			errors.rejectValue("bCategory", "select.category");
		}
		if (status.equals(ApplicationConstants.POPULATIONCENTER)) {
			errors.rejectValue("city", "select.city");
		}
		if (status.equals(ApplicationConstants.DEALRETAILER)) {
			errors.rejectValue("retailID", "select.retailer");
		}
		if (status.equals(ApplicationConstants.DEALRETAILERLOC)) {
			errors.rejectValue("retailerLocID", "select.retailerloc");
		}
	}
	
	/**
	 * This method validate supplier product hotdeals screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Object.
	 */
	public final void validate(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hotDealName", "hotDealName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "price");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hotDealShortDescription", "hotDealShortDescription");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hotDealTermsConditions", "hotDealTermsConditions");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, dealStartDate, dealStartDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, dealEndDate, dealEndDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salePrice", "salePrice");
	}
}

package supplier.service;

import java.util.ArrayList;
import java.util.List;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;

import common.exception.ScanSeeServiceException;
import common.pojo.HotDealInfo;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductReview;

public interface SearchService
{

	public ArrayList<ProductVO> getAllProduct(String zipcode, String productName, int lowerLimit, String screenName);

	public ProductVO getAllProductInfo(Integer retailLocationID, Integer productID)throws ScanSeeServiceException;

	public SearchResultInfo searchProducts(Users loginUser, SearchForm objForm,int lowerLimit) throws ScanSeeServiceException;
    
	public SearchResultInfo searchDeals(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException;
   
	public ProductVO getAllProductInfoWithDeals(Integer productID) throws ScanSeeServiceException;

	public HotDealInfo getDealsInfo(int hotDealID) throws ScanSeeServiceException;
 
	//public ArrayList<ProductVO> getAssociatedHotDeals();
	
	public List<ProductVO> getProductAssociatedWithDeals(Integer productID) throws ScanSeeServiceException;
	
	public List<ProductVO> getDealWithProductAssociated(Integer hotDealID)throws ScanSeeServiceException;
	public FindNearByDetails findNearBy(AreaInfoVO objAreaInfoVO)throws ScanSeeServiceException;
	public ExternalAPIInformation externalApiInfo(String moduleName) throws ScanSeeServiceException;
	public ProductDetail fetchProductDetails(String productId) throws ScanSeeServiceException;
	public ArrayList<ProductReview> getProductReviews(Integer productId) throws ScanSeeServiceException;
	
	
}

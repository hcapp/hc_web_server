/**
 * ExecutionInfo2Holder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.scansee.report.holders;

public final class ExecutionInfo2Holder implements javax.xml.rpc.holders.Holder {
    public com.scansee.report.ExecutionInfo2 value;

    public ExecutionInfo2Holder() {
    }

    public ExecutionInfo2Holder(com.scansee.report.ExecutionInfo2 value) {
        this.value = value;
    }

}

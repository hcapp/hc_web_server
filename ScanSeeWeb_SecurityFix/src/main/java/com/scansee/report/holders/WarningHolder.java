/**
 * WarningHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.scansee.report.holders;

public final class WarningHolder implements javax.xml.rpc.holders.Holder {
    public com.scansee.report.Warning value;

    public WarningHolder() {
    }

    public WarningHolder(com.scansee.report.Warning value) {
        this.value = value;
    }

}

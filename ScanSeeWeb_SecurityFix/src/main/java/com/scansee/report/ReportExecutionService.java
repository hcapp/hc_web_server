/**
 * ReportExecutionService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.scansee.report;

public interface ReportExecutionService extends javax.xml.rpc.Service {

/**
 * The Reporting Services Execution Service enables report execution
 */
    public java.lang.String getReportExecutionServiceSoapAddress();

    public com.scansee.report.ReportExecutionServiceSoap getReportExecutionServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.scansee.report.ReportExecutionServiceSoap getReportExecutionServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getReportExecutionServiceSoap12Address();

    public com.scansee.report.ReportExecutionServiceSoap getReportExecutionServiceSoap12() throws javax.xml.rpc.ServiceException;

    public com.scansee.report.ReportExecutionServiceSoap getReportExecutionServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

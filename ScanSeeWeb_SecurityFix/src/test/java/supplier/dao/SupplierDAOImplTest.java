/*

import java.util.List;

import javax.naming.Context;
import javax.sql.DataSource;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import supplier.dao.SupplierDAO;
import supplier.dao.SupplierDAOImpl;
import supplier.pojos.HotDealInfo;
import supplier.pojos.Rebates;
import supplier.pojos.Response;
import supplier.pojos.RetailerInfo;
import supplier.pojos.SupplierProfile;
import supplier.pojos.SupplierRegistrationInfo;
import supplier.pojos.Users;

public class SupplierDAOImplTest {

	SupplierDAO supplierDao = null;
	Users userObj = null;
	HotDealInfo hotdealObj  = null;
	SupplierRegistrationInfo supplierRegObj = null;
	Rebates rebatesObj = null;
	SupplierProfile supplierprofObj = null;
	Response respMessage = null;
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	@Before
	public void setUp() throws Exception 
	{
		supplierDao =  new SupplierDAOImpl();
		userObj = new Users();
	    hotdealObj = new HotDealInfo();
	    supplierRegObj = new SupplierRegistrationInfo();
	    rebatesObj = new Rebates();
	    supplierprofObj = new SupplierProfile();
	    hotdealObj.setHotDealID(6463686);
	    hotdealObj.setHotDealName("Leather Jacket");
	    hotdealObj.setPrice("118");
	    hotdealObj.setSalePrice("96");
	    hotdealObj.setHotDealShortDescription("");
	    hotdealObj.setHotDealLongDescription("");
	    hotdealObj.setHotDealTermsConditions("");
	    hotdealObj.setUrl("http://www.descuentolibre.com/images/deals/1/132261039509e55e2e0df2189193e1.jpg");
	    hotdealObj.setDealStartDate("2011-12-08");
	    hotdealObj.setDealStartTime("22:00");
	    hotdealObj.setDealEndDate("2011-2-28");
	    hotdealObj.setDealEndTime("19:04");
	    
	    supplierRegObj.setAddress2("vanivilas");
	    supplierRegObj.setCity("Bangalore");
	    supplierRegObj.setContactEmail("nanda_b@spanservice.com");
	    supplierRegObj.setContactFName("Shyam");
	    supplierRegObj.setContactLName("hm");
	    supplierRegObj.setContactPhoneNo("1234567891");
	    supplierRegObj.setCorpAdderess("vanivilas road");
	    supplierRegObj.setCorpPhoneNo("1234567891");
	    supplierRegObj.setLegalAuthorityFName("jhon");
	    supplierRegObj.setLegalAuthorityLName("bell");
	    supplierRegObj.setPassword("span@1234");
	    supplierRegObj.setPostalCode("54378");
	    supplierRegObj.setState("karantaka");
	    supplierRegObj.setSupplierName("Nanda");
	    
	    rebatesObj.setRebAmount(23.00);
	    rebatesObj.setRebateID(2324l);
	    rebatesObj.setRebDescription("");
	    rebatesObj.setRebEndDate("24-12-2011");
	    rebatesObj.setRebEndTime("12:00");
	    rebatesObj.setRebLongDescription("");
	    rebatesObj.setRebName("Leather jacket");
	    rebatesObj.setRebShortDescription("");
	    rebatesObj.setRebStartDate("2011-12-28");
	    rebatesObj.setRebStartTime("23:00");
	    rebatesObj.setRebTermCondtn("");
	    rebatesObj.setUserID(12344l);
	    rebatesObj.setUsers(userObj);
	    
	    supplierprofObj.setAddress("vanivilas");
	    supplierprofObj.setCity("bangalore");
	    supplierprofObj.setCompanyName("spaninfotech");
	    supplierprofObj.setContactFName("Shyam");
	    supplierprofObj.setContactLName("HM");
	    supplierprofObj.setContactPhone("1234567891");
	    supplierprofObj.setCorporateAddress("vanivilas");
	    supplierprofObj.setEmail("nanda_b@spanservices.com");
	    supplierprofObj.setFirstName("Nanda");
	    supplierprofObj.setLastName("Bhat");
	    supplierprofObj.setPhone("1234567891");
	    supplierprofObj.setPostalCode("534678");
	    supplierprofObj.setRetProfileID(124152l);
	    supplierprofObj.setState("Karanatka");
	    supplierprofObj.setUserID(123456l);
	    supplierprofObj.setUsers(userObj);
	    
	    userObj.setLoginSuccess(true);
	    userObj.setUsersid(123456);
	    
	  //  System.setProperty(Context.INITIAL_CONTEXT_FACTORY, InitialContextFactoryForTest.class.getName());
		dataSource = getDataSource();
		
	//	InitialContextFactoryForTest.bind("SCANSEEDS", dataSource);
	    }

	private DataSource getDataSource()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
		dataSource.setUrl("jdbc:jtds:sqlserver://10.11.201.226:1433/ScanSee_22ndJuly2011_Dev;SelectMethod=cursor");
		dataSource.setUsername("scansee");
		dataSource.setPassword("span@1234");
		return dataSource;
	}
	@After
	public void tearDown() throws Exception 
	{
		
	}

	
	
	//@Test
	public void testLoginAuthentication()
	{
		jdbcTemplate = new JdbcTemplate(dataSource);
		userObj = supplierDao.loginAuthentication("supplier", "span@1234");
		if(userObj == null){
			Assert.assertNull(userObj);
			//LOG.info("loginAuthentication method failed");
			System.out.println("loginAuthentication method failed");
		}else {
			Assert.assertNotNull(userObj);
			//LOG.info("loginAuthentication method processed successfully");
			System.out.println("loginAuthentication method successfully");
		}
		
	}

	//@Test
	public void testSupplierRegistration() throws Exception 
	{
		String response = supplierDao.supplierRegistration(supplierRegObj);
		if(response.isEmpty() || response.contains("error") )
		{
			Assert.assertEquals("Not getting expected result", "SUCCESS", response);
			System.out.println("supplierRegistration method failed");
			//LOG.info("supplierRegistration method failed");
		}else {
			Assert.assertEquals("Getting expected result", "SUCCESS", response);
			//LOG.info("supplierRegistration method processed successfully");
			System.out.println("supplierRegistration method processed successfully");
		}
		
	}

	//@Test
	public void testAddRebates() throws Exception
	{
		String response = supplierDao.addRebates(rebatesObj);
		if(response.isEmpty() || response.contains("error") )
		{
			Assert.assertEquals("Not getting expected result", "SUCCESS", response);
			System.out.println("addRebates method failed");
			//LOG.info("addRebates method failed");
		}else {
			Assert.assertEquals("Getting expected result", "SUCCESS", response);
			//LOG.info("addRebates method processed successfully");
			System.out.println("addRebates method processed successfully");
		}
	}

	//@Test
	public void testUpdateRebates() throws Exception
	{
		String response = supplierDao.updateRebates(rebatesObj);
		if(response.isEmpty() || response.contains("error") )
		{
			Assert.assertEquals("Not getting expected result", "SUCCESS", response);
			System.out.println("updateRebates method failed");
			//LOG.info("updateRebates method failed");
		}else {
			Assert.assertEquals("Getting expected result", "SUCCESS", response);
			//LOG.info("updateRebates method processed successfully");
			System.out.println("updateRebates method processed successfully");
		}
	}

	//@Test
	public void testGetRebatesName() throws Exception 
	{
		List listRebNames = supplierDao.getRebatesName("Leather Jacket");
		if(listRebNames == null){
			System.out.println("getRebatesName method failed");
			//LOG.info("getRebatesName method failed");
		}else {
			//LOG.info("getRebatesName method processed successfully");
			System.out.println("getRebatesName method processed successfully");
		}
		
	}

	//@Test
	public void testUpdateProfile() throws Exception {
		String response = supplierDao.updateProfile(supplierprofObj);
		if(response.isEmpty() || response.contains("error") )
		{
			Assert.assertEquals("Not getting expected result", "SUCCESS", response);
			System.out.println("updateProfile method failed");
			//LOG.info("updateProfile method failed");
		}else {
			Assert.assertEquals("Getting expected result", "SUCCESS", response);
			//LOG.info("updateProfile method processed successfully");
			System.out.println("updateProfile method processed successfully");
		}
		
	}

	//@Test
	public void testHotDealSearch() {
	}

	//@Test
	public void testAddHotDeal() throws Exception
	{
		 respMessage = supplierDao.addHotDeal(hotdealObj,respMessage);
		if( respMessage.getErrorCode()!= null || respMessage == null || respMessage.getErrorMessage() != null){
			Assert.assertNull(respMessage);
			//LOG.info("AddHotDeal method failed");
			System.out.println("AddHotDeal method failed");
		}else {
			Assert.assertNotNull(respMessage);
			//LOG.info("AddHotDeal method processed successfully");
			System.out.println("success");
		}
	}
	
  @Test
	public void testHotDealUpdate() throws Exception
	{
	  jdbcTemplate = new JdbcTemplate(dataSource);
		String response = supplierDao.hotDealUpdate(hotdealObj);
			if(response.isEmpty() || response.contains("error") )
			{
				Assert.assertEquals("Not getting expected result", "SUCCESS", response);
				System.out.println("HotDealUpdate method failed");
				//LOG.info("HotDealUpdate method failed");
			}else {
				Assert.assertEquals("Getting expected result", "SUCCESS", response);
				//LOG.info("HotDealUpdate method processed successfully");
				System.out.println("success");
			}
	}
   
 // @Test
	public void testGetHotdealCityStateRetailerList() throws Exception
	{
		List<RetailerInfo> list = supplierDao.getHotdealCityStateRetailerList("City");
		if(list.listIterator() == null ){
			Assert.assertNull(list);
			//LOG.info("getHotdealCityStateRetailerList method failed");
			System.out.println("getHotdealCityStateRetailerList method failed");
		}else {
			Assert.assertNotNull(list);
			//LOG.info("getHotdealCityStateRetailerList method processed successfully");
			System.out.println("success");
		}
	}
	

}
*/
package com.rssfeed.controller;
/**
 * This Class is used to store application lever constants.
 * @author shyamsundara_hm
 *
 */
public  class CommonConstants {
	
	
	
	
	
	public static final String breaking="http://www.tylerpaper.com/rss/custom/type/allbreaking/hours/192";
	//public static final String breakingSilent="http://www.tylerpaper.com/rss/custom/type/breakingsilent/hours/192";
	public static final String sports="http://www.etfinalscore.com/rss/custom/type/sports/hours/90";
	public static final String food="http://www.tylerpaper.com/rss/custom/type/food/hours/192";
    public static final String business="http://www.tylerpaper.com/rss/custom/type/business/hours/192";
    public static final String top="http://www.tylerpaper.com/rss/custom/type/news/hours/90";
    public static final String editorial="http://www.tylerpaper.com/rss/custom/type/editorial/hours/192";
    public static final String health="http://www.tylerpaper.com/rss/custom/type/health/hours/192";
    public static final String real="http://www.tylerpaper.com/rss/custom/type/editorial/hours/192";
    public static final String life="http://www.tylerpaper.com/rss/custom/type/life/hours/192";
    public static final String sportsVideos="http://link.brightcove.com/services/mrss/2322069287001/tags/sports/hours/192";
    public static final String newsVideos="http://api.brightcove.com/services/library?command=search_videos&page_size=30&all=tag:news&video_fields=id,name,shortDescription,&sort_by=CREATION_DATE:DESC&get_item_count=true&token=x95LXczyNI5-G9kX0cjsHM9edPFzaKFTE4PANJ7L2rQfuF-swGUxJg..";
    
    
    public static final String employment="http://redadmin.thejobnetwork.com/API/api.asp?sp=API_Get_Active_PositionsWithLocation_RSS2&isRSS2=true&Parameters=1495,0";
    public static final String goguideEvents="http://www.tylerpaper.info/GoGuide/rss.php?days=90";

//SaveAMomBlog
    	
    	public static final String saveamomblog="http://kdhnews.com/search/?q=&t=article&l=100&d=&d1=&d2=&s=start_time&sd=desc&nsa=eedition&c[]=blogs/savealotmom,blogs/savealotmom/*&f=rss";
    
    //Rockwall static links
    
    public static final String Weather = "http://mobile.weather.gov/index.php?lat=32.93&lon=-96.43";
    	public static final String RealEstate = "http://www.zillow.com/rockwall-county-tx/";
    	public static final String Employment = "http://www.indeed.com/l-Rockwall-County,-TX-jobs.html";
    	public static final String GasPrices = "https://www.gasbuddy.com/GasPrices/Texas/Rockwall/2601";
    	public static final String Traffic = "http://www.localconditions.com/weather-rockwall-texas/75032/traffic.php";
    	public static final String MovieTimes = "http://www.americantowns.com/tx/rockwall-county-movies";
    	
    	
    	//Added on 8/3/2015
    	public static final String DEFAULTROCKWALLIMAGEPTH = "images/brn.png";
    	public static final String DEFAULTTYLERIMAGEPTH = "images/newsfeed_noimage.jpg";
    	public static final String DEFAULTAUSTINIMAGEPTH = "images/appiconupload.png";
    	public static final String DEFAULTKILLEENIMAGEPATH="images/kdh.JPG";

public static final String TylerHubCitiName="Marble Falls";
	    public static final String RockwallHubCitiName="spanqa.regionsapp";
	    public static final String AustinHubCitiName = "spanqa.hubciti6";
	    public static final String KillenHubCitiName="KILLEEN DAILY HERALD";
	    
    /**
     * METHODSTART declared as string for logger message.
     */
    public static final String METHODSTART = "In side method >>> ";
    /**
     * METHODEND declared as string for logger message.
     */
    public static final String METHODEND = "Exiting method >>> ";
    /**
     * EXCEPTIONOCCURED declared as string for logger message.
     * 
     * 
     */
    
    public static final String TylerHubCitiId="10";
    public static final String RockwallHubCitiId="2070";
    public static final String AustinHubCitiId = "8";
	
    
    
    public static final String EXCEPTIONOCCURED = "Exception Occurred in  >>> ";
    public static final String URL_PATTERN = "^(https://|http://|ftp://|file://|www.)[-a-zA-Z0-9+&@#/%?=~_|!:;]+[.][-a-zA-Z0-9+&@#/%?=~_|!:;.()]+";
    public static final String ERRORNUMBER = "ErrorNumber";

 public static final String weatherLink="http://kdhnews.com/news/weather";
	    public static final String businessLink="http://kdhnews.com/business";
	    public static final String crimeLink ="http://kdhnews.com/news/crime";
	    public static final String opinionLink="http://kdhnews.com/opinion";
public static final String topstoriesLink="http://m.kdhnews.com/news/local";
 public static final String topnewsLink="http://m.kdhnews.com/news/local";
    public static final String obitsLink="http://kdhnews.com/obituaries/";
    public static final String worldnewsLink="http://kdhnews.com/news/world/";

	/**
	 * for database Error Message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";
	public static String killeenHubcitiId="2146";
	
	
	
	
	
    
}



package com.rssfeed.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

	private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

	private JdbcCustomerDAO customerDAO;

	public void setCustomerDAO(JdbcCustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	/**
	 * This method is used to fetch rss feeds based on feedtype.
	 * 
	 * @param item
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return feed list details
	 */
	@RequestMapping(value = "/videos.htm", method = RequestMethod.GET)
	public ModelAndView getVideos(@ModelAttribute("frontpageform") Item item, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {

		final String strMethodName = "getVideos";

		LOG.info(CommonConstants.METHODSTART + strMethodName);
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");

		if (!Utility.isEmptyOrNullString(item.getFeedType())) {

			ApplicationContext app = new ClassPathXmlApplicationContext("database-config.xml");

			JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app.getBean("customerDAO");
			Category itemd;
			ArrayList<Item> items;
			Category objCategory = null;
			try {
				objCategory = customerDAO.fetchAllFeeds(item.getFeedType(), item.getHcHubCitiID());
				items = objCategory.getItemLst();

				if (null != items && !items.isEmpty()) {
					session.setAttribute("videosList", items);
					session.setAttribute("defaultImage", CommonConstants.DEFAULTTYLERIMAGEPTH);

					Item objItem = new Item();
					objItem.setItems(items);

				} else {

					LOG.info("Item list is empty :");
					session.setAttribute("videosList", null);

					request.setAttribute("noregfound", "Currently No:" + " " + item.getFeedType() + "News . Please Check back later .");
					return new ModelAndView("error");
				}
			}

			catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			LOG.info("Feed type is null or empty");
			request.setAttribute("feedmessage", "No feed type");
		}
		model.put("frontpageform", item);

		// addObject("lists", item.getItems());
		LOG.info(CommonConstants.METHODEND + strMethodName);
		return new ModelAndView("indextest");

	}

	@RequestMapping(value = "/getlist.htm", method = RequestMethod.GET)
	public ModelAndView getListSaveAMom(ModelMap map, HttpSession session, HttpServletRequest request) {
		LOG.info("Inside MainController : getListSaveAMom ");
		System.out.println("+++++++++++++++");
		List<Item> items = null;
		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		try {
			items = Utility.getSaveAlotBlogList();

			if (items != null) {

				session.setAttribute("newsList", items);
				session.setAttribute("defaultImage", CommonConstants.DEFAULTKILLEENIMAGEPATH);

			} else {
				session.setAttribute("newsList", null);
				request.setAttribute("noregfound", "No Records Found");
				return new ModelAndView("error");
			}
		} catch (Exception e) {
			LOG.info("Inside getListSaveAMom : " + e);
			e.printStackTrace();
		}

		return new ModelAndView("savealotmom");
	}

	@RequestMapping(value = "/newsdetail.htm", method = RequestMethod.POST)
	public ModelAndView getDetails(@ModelAttribute("newslistform") Item item, BindingResult result, ModelMap map, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) {
		LOG.info("Inside MainController : getDetails ");
		/*
		 * final String strMethodName ="getDetails";
		 * LOG.info(CommonConstants.METHODSTART +strMethodName );
		 */

		Item items = null;

		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		// session.removeAttribute("itemsList");
		if (item != null) {

			LOG.info("Inside MainController : getDetails  : image : " + item.getImage());
			LOG.info("Inside MainController : getDetails  : title : " + item.getTitle());
			LOG.info("Inside MainController : getDetails  : description : " + item.getDescription());
			LOG.info("Inside MainController : getDetails  : link : " + item.getLink());

			if (item.getImage() != null) {
				request.setAttribute("image", item.getImage());
			}

			if (item.getTitle() != null) {
				request.setAttribute("title", item.getTitle());
			}

			if (item.getDescription() != null) {
				request.setAttribute("description", item.getDescription());
			}

			if (item.getHcHubCitiID().equalsIgnoreCase(CommonConstants.RockwallHubCitiId)
					|| item.getHcHubCitiID().equalsIgnoreCase(CommonConstants.killeenHubcitiId)) {

				if (item.getLink() != null) {
					request.setAttribute("link", item.getLink());
				}
			}

			/*
			 * if (item.getShortDesc() != null) { request.setAttribute("link",
			 * item.getShortDesc()); }
			 */
		}

		LOG.info("Exit MainController : getDetails ");
		return new ModelAndView("newsdetail");
	}

	@RequestMapping(value = "/videosdetail.htm", method = RequestMethod.POST)
	public ModelAndView getVideoDetails(@ModelAttribute("videoform") Item item, BindingResult result, ModelMap map, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) {

		final String strMethodName = "getVideoDetails";
		Item items = null;
		LOG.info(CommonConstants.METHODSTART + strMethodName);
		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		if (item != null) {

			LOG.info("link" + item.getLink());

			if (item.getLink() != null) {
				request.setAttribute("link", item.getLink());
				// Log.info("link"+ item.getLink())
			}
		}

		LOG.info(CommonConstants.METHODEND + strMethodName);
		return new ModelAndView("videoss");

	}

	@RequestMapping(value = "/combineFeeds.htm", method = RequestMethod.GET)
	public ModelAndView getdataFeed(@ModelAttribute("frontpageform") Item item, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {

		final String strMethodName = "getdataFeed";
		final String strViewName = "indextest";
		String key = null;

		Map<String, List<Item>> citiesByCountrySortedDesc = null;

		LOG.info(CommonConstants.METHODSTART + strMethodName);
		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		if (!Utility.isEmptyOrNullString(item.getFeedType())) {
			List<Item> items = Utility.getFeedList(item.getFeedType());

			Map<String, List<Item>> citiesByCountrySorted = Utility.groupToList(items);

			// Iterator it=citiesByCountry.entrySet().iterator();

			for (Map.Entry<String, List<Item>> entry : citiesByCountrySorted.entrySet()) {

				key = entry.getKey();

				for (Item i : entry.getValue()) {

					LOG.info("Title" + i.getTitle());
					LOG.info("Image" + i.getImage());

				}

			}
			// ArrayList<Item> groupedList=Utility.groupToList(items);

			if (null != items && !items.isEmpty()) {
				Item objItem = new Item();
				objItem.setItems(items);

				session.setAttribute("itemsList", citiesByCountrySorted);

				session.setAttribute("defaultImage", CommonConstants.DEFAULTTYLERIMAGEPTH);

				for (Item i : items) {
					LOG.info("Title" + i.getTitle());
					LOG.info("Image" + i.getImage());
					LOG.info("Desc" + i.getDescription());
					LOG.info("link" + i.getLink());

				}
			} else {

				LOG.info("Item list is empty :");
				session.setAttribute("itemsList", null);
				request.setAttribute("noregfound", "Currently No" + " " + item.getFeedType() + " " + " News . Please Check back later .");
				return new ModelAndView("error");
			}
		} else {
			LOG.info("Feed type is null or empty");
			request.setAttribute("feedmessage", "No feed type");
		}
		model.put("frontpageform", item);

		LOG.info(CommonConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);

	}

	@RequestMapping(value = "/home.htm", method = RequestMethod.GET)
	public ModelAndView getdata(@ModelAttribute("frontpageform") Item item, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		LOG.info("Inside MainController : getdata");

		// final String strMethodName = "getdata" ;
		final String strViewName = "indextest";
		String key = null;
		String shortDesc = null;

		Map<String, List<Item>> citiesByCountrySortedDesc = null;

		// LOG.info(CommonConstants.METHODSTART +strMethodName );
		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		session.removeAttribute("itemsList");
		session.removeAttribute("topList");
		session.removeAttribute("weather");
		session.removeAttribute("crime");
		session.removeAttribute("opinion");
		session.removeAttribute("business");
		session.removeAttribute("topstories");
		session.removeAttribute("obits");
		session.removeAttribute("worldnews");
		session.removeAttribute("city");

		ApplicationContext app = new ClassPathXmlApplicationContext("database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app.getBean("customerDAO");

		Category itemd;
		Category objCategory = null;

		ArrayList<Item> feedsList;
		List<Item> topnewslst;

		try {

			objCategory = customerDAO.fetchAllFeeds(item.getFeedType(), item.getHcHubCitiID());

			/*
			 * SortRSSFeedByDate sortByDate = new SortRSSFeedByDate();
			 * Collections.sort(feedsList, sortByDate);
			 */

			feedsList = objCategory.getItemLst();
			topnewslst = objCategory.getTopNewsLst();
			if (null != topnewslst && !topnewslst.isEmpty()) {
				session.setAttribute("topList", topnewslst);

				session.setAttribute("defaultImage", CommonConstants.DEFAULTTYLERIMAGEPTH);
				session.setAttribute("HubCitiID", item.getHcHubCitiID());

			}

			if (null != feedsList && !feedsList.isEmpty()) {
				ArrayList<Category> ctLst = null;
				ctLst = Utility.sortHotdealsByAPINames(feedsList);

				session.setAttribute("itemsList", ctLst);

				if (item.getHcHubCitiID().equalsIgnoreCase(CommonConstants.RockwallHubCitiId)) {

					session.setAttribute("defaultImage", CommonConstants.DEFAULTROCKWALLIMAGEPTH);
					session.setAttribute("HubCitiID", item.getHcHubCitiID());
				} else if (item.getHcHubCitiID().equalsIgnoreCase(CommonConstants.AustinHubCitiId)) {

					session.setAttribute("defaultImage", CommonConstants.DEFAULTAUSTINIMAGEPTH);
					session.setAttribute("HubCitiID", item.getHcHubCitiID());
				}

				else if (item.getHcHubCitiID().equalsIgnoreCase(CommonConstants.killeenHubcitiId)) {

					session.setAttribute("defaultImage", CommonConstants.DEFAULTKILLEENIMAGEPATH);
					session.setAttribute("HubCitiID", item.getHcHubCitiID());
					session.setAttribute("city", "KDH");
					if (item.getFeedType().equalsIgnoreCase("weather")) {
						session.setAttribute("weather", CommonConstants.weatherLink);
					} else if (item.getFeedType().equalsIgnoreCase("crime")) {
						session.setAttribute("crime", CommonConstants.crimeLink);
					} else if (item.getFeedType().equalsIgnoreCase("opinion")) {
						session.setAttribute("opinion", CommonConstants.opinionLink);
					} else if (item.getFeedType().equalsIgnoreCase("business")) {
						session.setAttribute("business", CommonConstants.businessLink);
					} else if (item.getFeedType().equalsIgnoreCase("topstories")) {
						session.setAttribute("topstories", CommonConstants.topnewsLink);
					}

					else if (item.getFeedType().equalsIgnoreCase("obits")) {
						session.setAttribute("obits", CommonConstants.obitsLink);
					}

					else if (item.getFeedType().equalsIgnoreCase("worldnews")) {
						session.setAttribute("worldnews", CommonConstants.worldnewsLink);
					}
				}

				else {

					session.setAttribute("defaultImage", CommonConstants.DEFAULTTYLERIMAGEPTH);
					session.setAttribute("HubCitiID", item.getHcHubCitiID());
				}
			}

			else {

				LOG.info("Item list is empty :");
				session.setAttribute("feedsList", null);
				request.setAttribute("noregfound", "No News Found :" + " " + "feedsList");
				return new ModelAndView("error");
			}

		} catch (Exception e) {
			LOG.info("Inside MainController : getdata : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit MainController : getdata");
		return new ModelAndView(strViewName);

	}

	@RequestMapping(value = "/homepage.htm", method = RequestMethod.GET)
	public ModelAndView getHomeData(HttpServletRequest request, HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getHomeData");

		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");

		ApplicationContext app = new ClassPathXmlApplicationContext("database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app.getBean("customerDAO");
		Category items;
		String hubcitiId = null;
		ArrayList<Category> itemsList;
		try {
			itemsList = customerDAO.getHcHubCitiID();
			try {
				for (int i = 0; i < itemsList.size(); i++) {
					if (itemsList.get(i).getHubCitiName().equalsIgnoreCase(CommonConstants.RockwallHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCiti" + e.getMessage());
			}
			LOG.info("HubCitiId Rockwall" + hubcitiId);
			itemsList = customerDAO.fetchAllImages(hubcitiId);

			if (null != itemsList) {
				session.setAttribute("HcHubCitiID", hubcitiId);
				session.setAttribute("itemsList", itemsList);

				for (int i = 0; i < itemsList.size(); i++) {

					if ("good times".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("events", itemsList.get(i).getImagePath());
					} else if ("good cause".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("cause", itemsList.get(i).getImagePath());
					} else if ("good living".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("living", itemsList.get(i).getImagePath());
					} else if ("good culture".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("culture", itemsList.get(i).getImagePath());
					} else if ("good people".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("people", itemsList.get(i).getImagePath());
					} else if ("good pets".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("pets", itemsList.get(i).getImagePath());
					}

					else if ("good faith".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("faith", itemsList.get(i).getImagePath());
					} else if ("good thinking".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("thinking", itemsList.get(i).getImagePath());
					} else if ("good business".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("business", itemsList.get(i).getImagePath());
					} else if ("good sports".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("sports", itemsList.get(i).getImagePath());
					} else if ("good neighbours".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("health", itemsList.get(i).getImagePath());
					} else if ("columnists".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("columnists", itemsList.get(i).getImagePath());
					}
				}

			} else {
				session.setAttribute("imagesList", null);
				request.setAttribute("noregfound", "No News Found :" + " " + "imagesList");
				return new ModelAndView("error");
			}

		} catch (RssFeedWebSqlException e) {
			LOG.error("Inside MainController : getFrontData : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit MainController : getFrontData");
		return new ModelAndView("homepage");
	}

	@RequestMapping(value = "/frontpage.htm", method = RequestMethod.GET)
	public ModelAndView getFrontData(HttpServletRequest request, HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getFrontData");

		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");

		ApplicationContext app = new ClassPathXmlApplicationContext("database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app.getBean("customerDAO");
		Category items;
		String hubcitiId = null;
		ArrayList<Category> itemsList;

		try {
			itemsList = customerDAO.getHcHubCitiID();
			try {
				for (int i = 0; i < itemsList.size(); i++) {

					if (itemsList.get(i).getHubCitiName().equalsIgnoreCase(CommonConstants.TylerHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
				LOG.info("HubCitiId Tyler in try" + hubcitiId);
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCiti" + e.getMessage());
				LOG.info("HubCitiId Tyler in catch" + hubcitiId);

			}
			LOG.info("HubCitiId Tyler" + hubcitiId);
			itemsList = customerDAO.fetchAllImages(hubcitiId);

			if (null != itemsList) {
				session.setAttribute("HcHubCitiID", hubcitiId);
				session.setAttribute("itemsList", itemsList);

				for (int i = 0; i < itemsList.size(); i++) {

					if ("sports News".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("sports", itemsList.get(i).getImagePath());
					} else if ("health News".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("health", itemsList.get(i).getImagePath());
					} else if ("business News".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("business", itemsList.get(i).getImagePath());
					} else if ("top News".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("top", itemsList.get(i).getImagePath());
					} else if ("life News".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("life", itemsList.get(i).getImagePath());
					} else if ("opinion News".equals(itemsList.get(i).getCategoryName())) {
						session.setAttribute("opinion", itemsList.get(i).getImagePath());
					}

				}

			} else {
				session.setAttribute("imagesList", null);
				request.setAttribute("noregfound", "No News Found :" + " " + "imagesList");
				return new ModelAndView("error");
			}

		} catch (RssFeedWebSqlException e) {
			LOG.error("Inside MainController : getFrontData : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit MainController : getFrontData");
		return new ModelAndView("frontpage");
	}

	// Request mapping for Austin

	@RequestMapping(value = "/firstpage.htm", method = RequestMethod.GET)
	public ModelAndView getFirstPageData(HttpServletRequest request, HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getFirstPageData");

		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");
		Category items;
		String hubcitiId = null;
		ArrayList<Category> itemsList;
		ApplicationContext app = new ClassPathXmlApplicationContext("database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app.getBean("customerDAO");

		try {
			itemsList = customerDAO.getHcHubCitiID();

			try {
				for (int i = 0; i < itemsList.size(); i++) {
					if (itemsList.get(i).getHubCitiName().equalsIgnoreCase(CommonConstants.KillenHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCitiInner" + e.getMessage());
			}
			session.setAttribute("HcHubCitiID", hubcitiId);
			LOG.info("HubCitiId Austin" + hubcitiId);
		} catch (RssFeedWebSqlException e1) {
			LOG.info("Excpetion occured in Utility: getHubCitiOuter" + e1.getMessage());
		}

		LOG.info("Exit MainController : getFirstPageData");
		return new ModelAndView("firstpage");
	}

	// Request mapping for Austin

	@RequestMapping(value = "/mainpage.htm", method = RequestMethod.GET)
	public ModelAndView getMainPageData(HttpServletRequest request, HttpSession session, ModelMap model) {
		LOG.info("Inside MainController : getMainPageData");

		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("itemsList");
		session.removeAttribute("videosList");
		session.removeAttribute("HcHubCitiID");
		Category items;
		String hubcitiId = null;
		ArrayList<Category> itemsList;
		ApplicationContext app = new ClassPathXmlApplicationContext("database-config.xml");

		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app.getBean("customerDAO");

		try {
			itemsList = customerDAO.getHcHubCitiID();

			try {
				for (int i = 0; i < itemsList.size(); i++) {
					if (itemsList.get(i).getHubCitiName().equalsIgnoreCase(CommonConstants.AustinHubCitiName)) {
						hubcitiId = itemsList.get(i).getHcHubCitiID();
						break;
					}

				}
			} catch (Exception e) {
				LOG.info("Excpetion occured in Utility: getHubCitiInner" + e.getMessage());
			}
			session.setAttribute("HcHubCitiID", hubcitiId);
			LOG.info("HubCitiId Austin" + hubcitiId);
		} catch (RssFeedWebSqlException e1) {
			LOG.info("Excpetion occured in Utility: getHubCitiOuter" + e1.getMessage());
		}

		LOG.info("Exit MainController : getMainPageData");
		return new ModelAndView("mainpage");
	}

	/*
	 * @RequestMapping(value = "/employment.htm", method = RequestMethod.GET)
	 * public ModelAndView getGoGuideData(
	 * 
	 * @ModelAttribute("frontpageform") Item item, BindingResult result,
	 * ModelMap model, HttpServletRequest request, HttpServletResponse response,
	 * HttpSession session) {
	 * LOG.info("Inside MainController : getGoGuideData"); int count = 0; //
	 * final String strMethodName = "getemployment" ; final String strViewName =
	 * "indextest";
	 * 
	 * // LOG.info(CommonConstants.METHODSTART +strMethodName );
	 * session.removeAttribute("realestateList");
	 * session.removeAttribute("eventsList");
	 * session.removeAttribute("employmentList");
	 * session.removeAttribute("itemsList");
	 * session.removeAttribute("videosList");
	 * 
	 * ApplicationContext app = new ClassPathXmlApplicationContext(
	 * "database-config.xml");
	 * 
	 * JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
	 * .getBean("customerDAO");
	 * 
	 * Category itemd; ArrayList<Item> items; Category objCategory = null;
	 * 
	 * try { objCategory =
	 * customerDAO.fetchAllFeeds(item.getFeedType(),item.getHubCitiID()); items
	 * =objCategory.getItemLst(); //items =
	 * customerDAO.fetchAllFeeds(item.getFeedType());
	 * 
	 * for(Item i:items){ i.setId(count++); i.setTitle(items.get(0).getTitle());
	 * i.setDescription(items.get(0).getDescription());
	 * i.setLink(items.get(0).getLink()); i.setImage(items.get(0).getImage());
	 * i.setDate(items.get(0).getDate());
	 * i.setDate(items.get(0).getShortDesc()); items.add(i); }
	 * 
	 * 
	 * if (null != items && !items.isEmpty()) { Item objItem = new Item();
	 * objItem.setItems(items);
	 * 
	 * session.setAttribute("employmentList", items);
	 * 
	 * for (Item i : items) { LOG.info("Title" + i.getTitle()); LOG.info("Desc"
	 * + i.getDescription()); LOG.info("link" + i.getLink()); } } else {
	 * 
	 * LOG.info("Inside MainController : getGoGuideData : Empty");
	 * session.setAttribute("employmentList", null);
	 * request.setAttribute("noregfound", "No News Found :" + " " +
	 * "Employment"); return new ModelAndView("error"); }
	 * 
	 * } catch (RssFeedWebSqlException e) {
	 * LOG.error("Inside MainController : getGoGuideData : " + e);
	 * e.printStackTrace(); }
	 * 
	 * LOG.info("Exit MainController : getGoGuideData"); return new
	 * ModelAndView(strViewName); }
	 */

	/*
	 * @SuppressWarnings("resource")
	 * 
	 * @RequestMapping(value = "/realestate.htm", method = RequestMethod.GET)
	 * public ModelAndView getrealEstateData(ModelMap model, HttpServletRequest
	 * request, HttpServletResponse response, HttpSession session) {
	 * LOG.info("Inside MainController : getrealEstateData ");
	 * 
	 * // final String strMethodName = "getRealEstateData" ; final String
	 * strViewName = "indextest";
	 * 
	 * // LOG.info(CommonConstants.METHODSTART +strMethodName );
	 * session.removeAttribute("realestateList");
	 * session.removeAttribute("eventsList");
	 * session.removeAttribute("employmentList");
	 * session.removeAttribute("itemsList");
	 * session.removeAttribute("videosList");
	 * 
	 * ApplicationContext app = new ClassPathXmlApplicationContext(
	 * "database-config.xml");
	 * 
	 * JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app
	 * .getBean("customerDAO"); RealEstate items; try { items =
	 * customerDAO.fetchAllItems();
	 * 
	 * if (null != items) { session.setAttribute("realestateList",
	 * items.getRealestateList()); } else {
	 * 
	 * LOG.error("Inside MainController : getrealEstateData : Empty");
	 * session.setAttribute("realestateList", null);
	 * request.setAttribute("noregfound", "No News Found :" + " " +
	 * "realestateList"); return new ModelAndView("error"); }
	 * 
	 * } catch (RssFeedWebSqlException e) {
	 * LOG.error("Inside MainController : getrealEstateData : " + e);
	 * e.printStackTrace(); }
	 * 
	 * LOG.info("Inside MainController : getrealEstateData "); return new
	 * ModelAndView(strViewName); }
	 */

	/**
	 * 
	 * Push Notification Controller for handling Daily based news push
	 * notification. Based on specified FeedType get the article & display it.
	 * feedType = 9 -> for fetching today's top three articles from Tyler
	 * hubciti
	 * 
	 */
	@RequestMapping(value = "/pushhome.htm", method = RequestMethod.GET)
	public ModelAndView getpushhome(@ModelAttribute("frontpageform") Item item, BindingResult result, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {

		LOG.info("Inside MainController : getdata");

		// Displaying articles list page link
		String strViewName = "newslist";

		// remove the Previous articles list
		session.removeAttribute("itemsList");

		// Database Credentials & resources
		ApplicationContext app = new ClassPathXmlApplicationContext("database-config.xml");

		// Create DAO Instance for handling with database
		JdbcCustomerDAO customerDAO = (JdbcCustomerDAO) app.getBean("customerDAO");

		Category objCategory = null;

		// List for holding articles
		ArrayList<Item> feedsList;

		try {

			// Invoke DAO Layer for Interacting with database
			objCategory = customerDAO.fetchPushNotificationFeeds(item.getFeedType());

			/*
			 * SortRSSFeedByDate sortByDate = new SortRSSFeedByDate();
			 * Collections.sort(feedsList, sortByDate);
			 */

			// Get Articles List
			feedsList = objCategory.getItemLst();

			// Sorting articles based on dates
			if (null != feedsList && !feedsList.isEmpty()) {
				ArrayList<Category> ctLst = null;
				//ctLst = Utility.sortHotdealsByAPINames(feedsList);
				session.setAttribute("itemsList", feedsList);

				if (1 == feedsList.size()) {

					Item signleitem = feedsList.get(0);

					if (signleitem != null) {

						LOG.info("Inside MainController : getDetails  : image : " + signleitem.getImage());
						LOG.info("Inside MainController : getDetails  : title : " + signleitem.getTitle());
						LOG.info("Inside MainController : getDetails  : description : " + signleitem.getDescription());
						LOG.info("Inside MainController : getDetails  : link : " + signleitem.getLink());

						if (signleitem.getImagePath() != null) {
							request.setAttribute("image", signleitem.getImagePath());
						}

						if (signleitem.getTitle() != null) {
							request.setAttribute("title", signleitem.getTitle());
						}

						if (signleitem.getDescription() != null) {
							request.setAttribute("description", signleitem.getDescription());
						}
						if (item.getShortDesc() != null) {
							request.setAttribute("link", item.getShortDesc());
						}
						request.setAttribute("isSignleItem", "yes");
					}
					strViewName = "newsdetail";
				}

			} else {
				LOG.info("Item list is empty :");
				session.setAttribute("feedsList", null);
				request.setAttribute("noregfound", "You have no notifications at this time");
				return new ModelAndView("error");
			}

		} catch (Exception e) {
			LOG.info("Inside MainController : getdata : " + e);
			e.printStackTrace();
		}

		LOG.info("Exit MainController : getdata");
		return new ModelAndView(strViewName);

	}

	/*
	 * 
	 * Push Notification article details displaying
	 * 
	 */
	@RequestMapping(value = "/pushnewdetails.htm", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView getPushNewsDetails(@ModelAttribute("newslistform") Item item, BindingResult result, ModelMap map, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) {
		LOG.info("Inside MainController : getDetails ");
		/*
		 * final String strMethodName ="getDetails";
		 * LOG.info(CommonConstants.METHODSTART +strMethodName );
		 */

		Item items = null;

		session.removeAttribute("realestateList");
		session.removeAttribute("eventsList");
		session.removeAttribute("employmentList");
		session.removeAttribute("videosList");
		// session.removeAttribute("itemsList");
		if (item != null) {

			LOG.info("Inside MainController : getDetails  : image : " + item.getImage());
			LOG.info("Inside MainController : getDetails  : title : " + item.getTitle());
			LOG.info("Inside MainController : getDetails  : description : " + item.getDescription());
			LOG.info("Inside MainController : getDetails  : link : " + item.getLink());

			if (item.getImage() != null) {
				request.setAttribute("image", item.getImage());
			}

			if (item.getTitle() != null) {
				request.setAttribute("title", item.getTitle());
			}

			if (item.getDescription() != null) {
				request.setAttribute("description", item.getDescription());
			}

			/*
			 * if (item.getShortDesc() != null) { request.setAttribute("link",
			 * item.getShortDesc()); }
			 */
		}

		LOG.info("Exit MainController : getDetails ");
		return new ModelAndView("newsdetail");
	}
}

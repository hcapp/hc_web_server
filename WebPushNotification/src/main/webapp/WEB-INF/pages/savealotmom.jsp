<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style type="text/css">
table {
	width: 100%;
	min-height: 200px;
	padding: 0px;
	border-collapse: collapse;
}

html,body {
	margin: 0;
	padding: 0;
}
</style>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>



<script type="text/javascript">
	
	function testOrientation() {
		document.getElementById('block_land').style.display = (screen.width > screen.height) ? 'block'
				: 'block';
	}
</script>

<style type="text/css">
@media only screen and (min-device-width: 768px) and (max-device-width:
	1024px) and (orientation:portrait) {
	orienCss {
		-webkit-transform: rotate(90deg);
		width: 100%;
		height: 100%;
		overflow: hidden;
		position: absolute;
		top: 0;
		left: 0;
	}
}

#box1 {
	overflow-x: auto;
	overflow-y: auto;
	background-color: white;
	min-width: 75%;
	min-height: 75%;
	height: 100%;
	width: 100%;
}


#link {
	background: #3498db;
	background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
	background-image: -moz-linear-gradient(top, #3498db, #2980b9);
	background-image: -ms-linear-gradient(top, #3498db, #2980b9);
	background-image: -o-linear-gradient(top, #3498db, #2980b9);
	background-image: linear-gradient(to bottom, #3498db, #2980b9);
	-webkit-border-radius: 28;
	-moz-border-radius: 28;
	border-radius: 28px;
	-webkit-box-shadow: 0px 1px 3px #315fd4;
	-moz-box-shadow: 0px 1px 3px #315fd4;
	box-shadow: 0px 1px 3px #315fd4;
	font-family: Arial;
	color: #ffffff;
	font-size: 20px;
	padding: 10px 20px 10px 20px;
	text-decoration: none;
}

#link:hover {
	background: #3cb0fd;
	text-decoration: none;
}

html,body {
	margin: 0px;
	padding: 0px;
	overflow: auto;
}

#spanbox2 {
	width: auto;
	height: auto;
	min-width: 75%;
	min-height: 75%;
	text-align: justify;
	padding: 40px;
	font-size: 2.4em;
	font-weight: normal;
	font-family: arial;
	letter-spacing: normal;
	line-height: 128%;
}

.btn {
	padding: 15px;
	background: #3498db;
	background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
	background-image: -moz-linear-gradient(top, #3498db, #2980b9);
	background-image: -ms-linear-gradient(top, #3498db, #2980b9);
	background-image: -o-linear-gradient(top, #3498db, #2980b9);
	background-image: linear-gradient(to bottom, #3498db, #2980b9);
	-webkit-border-radius: 19;
	-moz-border-radius: 19;
	border-radius: 19px;
	font-family: Arial;
	text-align: center;
	color: #ffffff;
	width: 100%;
	float: right;
	font-size: 40px;
	text-decoration: none;
}

.btn:hover {
	background: #3cb0fd;
	text-decoration: none;
}

.close:hover {
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #378de5
		), color-stop(1, #79bbff));
	background: -moz-linear-gradient(center top, #378de5 5%, #79bbff 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5',
		endColorstr='#79bbff');
	background-color: #378de5;
}

.close:active {
	position: relative;
	top: 1px;
}

.highLightErr {
	color: #FF0000 !important;
	padding: 30px 30px 30px 30px;
}

#div_link {
	padding: 30px;
}

#longTitle {
	text-align: center;
	font-size: 5em;
	font-weight: bolder;
	font-family: arial;
}

#loading {
	width: 100%;
	height: 100%;
	top: 0px;
	left: 0px;
	position: fixed;
	display: block;
	z-index: 99
}

#loading-image {
	position: absolute;
	top: 40%;
	left: 45%;
	z-index: 100
}
</style>
</head>

<body>

<div id="box1">

<c:choose>
<c:when
				test="${sessionScope.newsList ne null && !empty sessionScope.newsList}">
				
				
				
				
					
					
			
				
				<table cellpadding="8px" style="height: auto; min-height: auto;">
				
							
					<c:forEach var="listValue" items="${sessionScope.newsList}">
							<c:choose>
								<c:when
									test="${listValue.imagePath ne null && !empty listValue.imagePath}">
									<tr onclick='location.href="${listValue.link}"'  id="${listValue.id}" style="margin: 0px;cursor:pointer; padding: 0px; margin-bottom: 4px; border-bottom: 1px solid gray;"
										>
										
										<td style="height: 250px; width: 250px"><img
											id="image${listValue.id}" style="display: block; height: 240px; width: 240px; margin: 4px; min-width: 12em; min-height: 12em;"
											src="${listValue.imagePath}" /></td>
											
											<td colspan="2"
											style="vertical-align: middle; text-align: justify; font-size: 2.0em; padding: 8px; font-family: arial; font-weight: bolder;"><span
											id="title${listValue.id}">${listValue.title}</span><br />
										<br />
										<span
											style="font-size: 0.9em; font-weight: normal; color: gray;">${listValue.description}</span></td>
										
										<%-- <td colspan="1" id="link${listValue.id}" ><a style="font-size: 2.0em;padding-right: 10px;padding-top: 15px;padding-left: 10px;" href="${listValue.link}">More</a> </td> --%>
										

									</tr>

								</c:when>

								<c:otherwise>
									<tr onclick='location.href="${listValue.link}"'  id="${listValue.id}"
										style="margin: 0px;cursor:pointer; padding: 0px; margin-bottom: 4px; border-bottom: 1px solid gray;"
										>



										


<c:if test="${sessionScope.defaultImage ne null && !empty sessionScope.defaultImage}">

										<td style="height: 250px; width: 250px"><img
											id="image${listValue.id}"
											style="display: block; height: 240px; width: 240px; margin: 4px; min-width: 12em; min-height: 12em;"
											src="${sessionScope.defaultImage}" /></td>
											</c:if>
											

										
										<td colspan="2"
											style="vertical-align: middle; text-align: justify; font-size: 2.0em; padding: 8px; font-family: arial; font-weight: bolder;"><span
											id="title${listValue.id}">${listValue.title}</span><br />
										<br />
										<span
											style="font-size: 0.9em; font-weight: normal; color: gray;">${listValue.description}</span></td>

										
										<%-- <td colspan="1" id="link${listValue.id}" ><a style="font-size: 2.0em;padding-right: 10px;padding-top: 15px;padding-left: 10px;" href="${listValue.link}">More</a> </td> --%>
									</tr>

								</c:otherwise>
							</c:choose>
						</c:forEach>
					








					


				</table>

			</c:when>


		</c:choose>



		<c:if
			test="${requestScope.feedmessage ne null && !empty requestScope.feedmessage}">
			<span class="highLightErr"><c:out
					value="${requestScope.feedmessage}"></c:out></span>

		</c:if>






		<div id="bottombar">
			<img
				style="float: right; width: 180px; height: 70px; margin-right: 20px; padding: 10px;"
				align="right" src="images/poweredby_1.jpg" alt="Powered By HubCiti">
		</div>
	</div>






	




</body>

</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<!-- <meta name="viewport" content="width=device-width, initial-scale=0, maximum-scale=0, user-scalable=no" /> -->
<%@page contentType="text/html;charset=UTF-8"%>

<script type="text/javascript" src="/RssFeedNew/scripts/jquery-1.10.2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<title>frontpage</title>



<script type="text/javascript">
	//This method is used to call feed list page.
	
	
	function reply_click(id) {
		document.getElementById("loading").style.display = 'inline' ;
		document.frontpageform.feedType.value = id;
		document.frontpageform.HcHubCitiID.value = document.getElementById("hubcitiId").innerHTML;
		document.frontpageform.method = "GET";
		document.frontpageform.action = "home.htm";
		document.frontpageform.submit();
	}
	
	
	function hideLoading(){
 document.getElementById("loading").style.display = 'none' ;
}
</script>

<style type="text/css">

/*loading {
  width: 500px;
  height: 250px;
  top: 40%;
  left: 40%;
 border:2px solid white;
  position: fixed;
  display: block;
  border-radius:20px;
  background-color: black;
  
  
}*/
#loading {
    background-color: black;
    border: 2px solid white;
    border-radius: 20px;
    display: block;
    height: 250px;
    left: 32%;
    position: fixed;
    top: 40%;
    width: 500px;
    z-index: 999;
}


#loading-image {
  position: absolute;
  top: 20%;
  left: 15%;
  width:100px;
  height:100px;
  z-index: 90;
}

body {
	margin: 0px;
	padding: 0px;
	overflow: auto;
	overflow-x:hidden;
	}

table {
	width: 100%;
	height: 100%;
	min-height: 567px;
	padding: 0px;
	border-collapse: collapse;
	background-color: white;
}

table tr td {
	padding-top: 0;
	padding-bottom: 0;
	padding-left: 0;
	padding-right: 0;
	width: 33.3%;
	
	position:relative;
	
}

table tr td div.tiles{
	width:100%;
}

img {
	display: block;
	height:400px; 
	width:100%;
	background-size:cover; 
}

body, html	{
	width: 100%;
	overflow-x: hidden!important;
}
.tiles {
    letter-spacing: normal !important;
    line-height: 2.8em;
    padding: 0 !important;
}
</style>
</head>

<body onload="hideLoading();">


	<center>
		<form name="frontpageform" commandName="frontpageform">




			<table>
				<input type="hidden" name="feedType" />
				<input type="hidden" name="HcHubCitiID" />
			<tr><td id="hubcitiId" style="display:none;">${sessionScope.HcHubCitiID}</td></tr>
				<tr>

				
				<c:choose>
						<c:when
							test="${sessionScope.events  ne null && !empty sessionScope.events && sessionScope.events ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="Good Times">
							
							
								<img src="${sessionScope.events}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 3px solid blue;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Times</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="Good Times"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 3px solid blue;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Times</div></td>
						</c:otherwise>
					</c:choose>
				
						
					
				
					<c:choose>
						<c:when
							test="${sessionScope.cause  ne null && !empty sessionScope.cause && sessionScope.cause ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good cause">
								<img src="${sessionScope.cause}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 3px solid blue;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Cause</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good cause"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 3px solid blue;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Cause</div></td>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${sessionScope.living  ne null && !empty sessionScope.living && sessionScope.living ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good living">
								<img src="${sessionScope.living}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 3px solid blue;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Living</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good living"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 3px solid blue;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good Living</div></td>
						</c:otherwise>
					</c:choose>
				</tr>

				<tr>
						
						<!-- <td align="center" onclick='location.href="http://www.tylerpaper.com/weather"' ><img src="images/weather.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: black;border-right: 1px solid white;color:white;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Weather</div></td> -->
						<c:choose>
						<c:when
							test="${sessionScope.business  ne null && !empty sessionScope.business && sessionScope.business ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good business">
								<img src="${sessionScope.business}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Business</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good business"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good Business</div></td>
						</c:otherwise>
					</c:choose>
					
				
					<c:choose>
						<c:when
							test="${sessionScope.people  ne null && !empty sessionScope.people && sessionScope.people ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good people">
								<img src="${sessionScope.people}"  />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good People</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good people"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good People</div></td>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${sessionScope.pets  ne null && !empty sessionScope.pets && sessionScope.pets ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good pets">
								<img src="${sessionScope.pets}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Pets</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good pets"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good People</div></td>
						</c:otherwise>
					</c:choose>
					
					
				</tr>

				<tr>
					<c:choose>
						<c:when
							test="${sessionScope.faith  ne null && !empty sessionScope.faith && sessionScope.faith ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good faith">
								<img src="${sessionScope.faith}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Faith</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good faith"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good Faith</div></td>
						</c:otherwise>
					</c:choose>
					
					
				

					<c:choose>
						<c:when
							test="${sessionScope.thinking  ne null && !empty sessionScope.thinking && sessionScope.thinking ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good thinking">
								<img src="${sessionScope.thinking}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Thinking</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good thinking"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good Thinking</div></td>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${sessionScope.health  ne null && !empty sessionScope.health && sessionScope.health ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good health">
								<img src="${sessionScope.health}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Health</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good health"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good Health</div></td>
						</c:otherwise>
					</c:choose>
						
				

				</tr>
				<tr>

						<c:choose>
						<c:when
							test="${sessionScope.culture  ne null && !empty sessionScope.culture && sessionScope.culture ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good culture">
								<img src="${sessionScope.culture}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Culture</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good culture"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;padding-top: 18px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good Culture</div></td>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${sessionScope.sports  ne null && !empty sessionScope.sports && sessionScope.sports ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="good sports">
								<img src="${sessionScope.sports}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Good Sports</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="good sports"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Good Sports</div></td>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${sessionScope.columnists  ne null && !empty sessionScope.columnists && sessionScope.columnists ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="columnists">
								<img src="${sessionScope.columnists}" />
								<div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;padding-top: 18px;">Columnists</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="columnists"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Columnists</div></td>
						</c:otherwise>
					</c:choose>
					</tr>
				<tr>
					
							<td align="center" onclick='location.href="http://mobile.weather.gov/index.php?lat=32.93&lon=-96.43"' id="weather"><img src="images/weathrr.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Weather</div></td>
					
				
				
					
							<td align="center" onclick='location.href="http://www.zillow.com/rockwall-county-tx/"' id="realestate"><img src="images/Real-Estate-market.jpeg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;padding-top: 18px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Real Estate</div></td>
					
					
						
						
							<td align="center" onclick='location.href="http://www.indeed.com/l-Rockwall-County,-TX-jobs.html"' id="employment"><img src="images/employment.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;padding-top: 18px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Employment</div></td>
					
				</tr>
				
				<tr>
				
							<td align="center" onclick='location.href="http://gasprices.mapquest.com/station/us/tx/rockwall"' id="gasprices"><img src="images/gas-prices.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;padding-top: 18px;font-weight: bold;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Gas Prices</div></td>
						
					
						
							<td align="center" onclick='location.href="http://www.localconditions.com/weather-rockwall-texas/75032/traffic.php"' id="traffic"><img src="images/traffic.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Traffic</div></td>
					
					
						
							<td align="center" onclick='location.href="http://www.americantowns.com/tx/rockwall-county-movies"' id="movie"><img src="images/movie.jpg" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: #0066CC;color:white;height: 100px;font-weight: bold;padding-top: 18px;border-right: 1px solid white;font-size: 2em;font-family: Arial;letter-spacing: 0.1em;">Movie Times</div></td>
						
				</tr>
				
				<tr>
					<td colspan="2"><div id="bottombar">
							<img
								style="float: right; width: 110px; height: 70px; margin-right: 20px; padding: 10px;"
								align="right" src="images/newlogobottom.png"
								alt="Powered By HubCiti">
						</div></td>
				</tr>

			</table>
			
			<div id="loading" style="background: 1px solid black;">
<img id="loading-image" src="images/ajax-loaders.gif" alt="Loading..." /><span style="color:white;font-size: 2.8em; position: absolute;font-weight:bolder;top: 30%;letter-spacing:0.1em;
  left: 30%;">Refreshing Data..</span> 
</div>  
		</form>
	</center>
	
	<script>
	$("table").find("td img").on('error',function(){
  $(this).removeAttr("src").attr("src","/RssFeedNew/images/brn.png");
});
	</script>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<!-- <meta name="viewport" content="width=device-width, initial-scale=0, maximum-scale=0, user-scalable=no" /> -->
<%@page contentType="text/html;charset=UTF-8"%>
<title>frontpage</title>


<script type="text/javascript">
	//This method is used to call feed list page.
	
	
	function reply_click(id) {
	document.getElementById("loading").style.display = 'inline' ;
		document.frontpageform.feedType.value = id;
		document.frontpageform.HcHubCitiID.value = document.getElementById("hubcitiId").innerHTML;
		document.frontpageform.method = "GET";
		document.frontpageform.action = "home.htm";
		document.frontpageform.submit();
	}
	

	
	
	
	
	function hideLoading(){
 document.getElementById("loading").style.display = 'none' ;
}
</script>

<style type="text/css">

/*loading {
  width: 500px;
  height: 250px;
  top: 40%;
  left: 40%;
 border:2px solid white;
  position: fixed;
  display: block;
  border-radius:20px;
  background-color: black;
  
  
}*/
#loading {
    background-color: black;
    border: 2px solid white;
    border-radius: 20px;
    display: block;
    height: 250px;
    left: 32%;
    position: fixed;
    top: 40%;
    width: 500px;
    z-index: 999;
}


#loading-image {
  position: absolute;
  top: 20%;
  left: 15%;
  width:100px;
  height:100px;
  z-index: 90;
}

body {
	margin: 0px;
	padding: 0px;
	overflow: auto;
	overflow-x:hidden;
	}

table {
	width: 100%;
	height: 100%;
	min-height: 567px;
	padding: 0px;
	border-collapse: collapse;
	background-color: white;
}

table tr td {
	padding-top: 0;
	padding-bottom: 0;
	padding-left: 0;
	padding-right: 0;
	width: 50%;
	position:relative;
	
}

table tr td div.tiles{
	width:100%;
}




img {
	display: block;
	height:500px; 
	width:100%;
	background-size:cover; 
}

body, html	{
	width: 100%;
	overflow-x: hidden!important;
}

</style>
</head>
<body onload="hideLoading();">


	<center>
		<form name="frontpageform" commandName="frontpageform">




			<table>
				<input type="hidden" name="feedType" />
		<input type="hidden" name="HcHubCitiID" />
			<tr><td id="hubcitiId" style="display:none;">${sessionScope.HcHubCitiID}</td></tr>
				<tr>

				
				
			<td align="center" onclick="reply_click(this.id)" id="citywide"><img src="images/citywide.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0px;background-color: black;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 3em;font-family: Arial;letter-spacing: 0.1em;">CityWide</div></td>
		    <td align="center" onclick="reply_click(this.id)" id="imagineaustin"><img src="images/imagineaustin.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;color: white;border-left: 1px solid white;background-color:black;height: 100px;font-weight: bold;font-size: 3em;font-family: Arial;letter-spacing: 0.1em;">Imagine Austin</div></td>
						
				</tr>

				<tr>
						
					<!-- <td align="center" onclick="reply_click(this.id)" id="abia"><img src="images/abiaairport.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;color: white;border-left: 1px solid white;background-color:black;height: 100px;font-weight: bold;font-size: 3em;font-family: Arial;letter-spacing: 0.1em;">ABIA</div></td> --> 
					
				
					
					<!--  	<td align="center" onclick="reply_click(this.id)" id="animalservices"><img src="images/animalservices.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;background-color: black;color:white;border-left: 1px solid white;height: 100px;font-weight: bold;font-size: 3em;font-family: Arial;letter-spacing: 0.1em;">Animal Services</div></td> -->	
					
				</tr>

				<tr>
					
							<td align="center" onclick="reply_click(this.id)" id="medicalservices"><img src="images/emergencymedical.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0;background-color: black;color:white;border-right: 1px solid white;height: 100px;font-weight: bold;font-size:3em;font-family: Arial;letter-spacing: 0.1em;">Medical Services</div></td>
						
				
<!--  <td align="center" onclick="reply_click(this.id)" id="fire"><img src="images/fire.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0;background-color: black;color:white;border-right: 1px solid white;height: 100px;font-weight: bold;font-size:3em;font-family: Arial;letter-spacing: 0.1em;">Fire</div></td>-->
					
						
						<td align="center" onclick="reply_click(this.id)" id="humanservices"><img src="images/healthandhuman.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;left:0;background-color: black;color:white;border-right: 1px solid white;height: 100px;font-weight: bold;font-size:3em;font-family: Arial;letter-spacing: 0.1em;">Health</div></td>
				

				</tr>
				<tr>

					
					
					
							<td align="center" onclick="reply_click(this.id)" id="sustainability"><img src="images/sustainability.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;background-color: black;color:white;height: 100px;font-weight: bold;border-left: 1px solid white;font-size: 3em;font-family: Arial;letter-spacing: 0.1em;">Sustainability</div>
							
							<td align="center" onclick="reply_click(this.id)" id="parks"><img src="images/parksandrec.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;background-color:100%;left:0px;background-color: black;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Parks</div>
						
					</tr>
				
					
						
						
				
				
					
							<!-- <tr><td align="center" onclick="reply_clickEmp(this.id)" id="employment"><img src="images/employment.png" /><div class="tiles" style="z-index:2;border-left: 1px solid white;position:absolute;bottom:0;background-color: black;color:white;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Employment</div></td> -->
						<!--  	<td align="center" onclick="reply_click(this.id)" id="police"><img src="images/police.png" /><div class="tiles" style="z-index:2;position:absolute;bottom:0;background-color:100%;left:0px;background-color: black;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 3em;font-family: Arial;letter-spacing: 0.1em;">Police</div></tr>-->
				
				
				<tr>
					<td colspan="2"><div id="bottombar">
							<img
								style="float: right; width: 110px; height: 70px; margin-right: 20px; padding: 10px;"
								align="right" src="images/newlogobottom.png"
								alt="Powered By HubCiti">
						</div></td>
				</tr>

			</table>
			
			<div id="loading" style="background: 1px solid black;">
<img id="loading-image" src="images/ajax-loaders.gif" alt="Loading..." /><span style="color:white;font-size: 2.8em; position: absolute;font-weight:bolder;top: 30%;letter-spacing:0.1em;
  left: 30%;">Refreshing Data..</span> 
</div>  
		</form>
	</center>
</body>
</html>
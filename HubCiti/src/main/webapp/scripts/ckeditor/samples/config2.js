/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.language = 'es';
	config.uiColor = '#f3f3f3';
	config.height = 400;
	config.toolbarCanCollapse = true;
	config.extraPlugins = 'wordcount';
	config.wordcount = {

		// Whether or not you want to show the Word Count
		showWordCount: true,

		// Whether or not you want to show the Char Count
		showCharCount: true,
		
		// Maximum allowed Word Count
		maxWordCount: 5,

		// Maximum allowed Char Count
		maxCharCount: 10
	};
};

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
	$(document).ready(function() {
	
	
	
	$("input[name='actions']").not(":checked").parents("tr").find(".errorDsply").remove();
	
	
	
		$(".hdrClone span:gt(1)").css("text-indent", "8px");
		$(".modal-close").on('click.popup', function() {
			$(".modal-popupWrp").hide();
			$(".modal-popup").slideUp();
			$(".modal-popup").find(".modal-bdy input").val("");
			$(".modal-popup i").removeClass("errDsply");
		});
		$(".alrtClose").click(function() {
			$('p.ctgryassoci').hide();
		});
		$(".actn-close").on("click", function() {
			var elemnt = $(this).parents('div');
			var alrtId = elemnt.attr("id");
			if (alrtId == 'evntClose') {
				$("#" + alrtId).hide();
			} else if (elemnt) {
				$(this).parent('div.alertBx').hide();
			} else {
			}
		});
		
		$(".eventDateclassjs").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/icon-calendar-active.png'
		});
		
		$(".eventEDateclassjs").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/icon-calendar-active.png'
		});
		
		$("input[name='eventDate']").each(function() {
		    var ed = $(this).attr("id");
		   // $("#" + ed).datepicker('option', 'minDate', $("#eventStartDate").val());		 
		  //  $("#" + ed).datepicker('option', 'maxDate', $("#eventEndDate").val());
		  
		    $("#" + ed).datepicker('option', 'minDate',  "setDate", new Date());
			$("#" + ed).datepicker('option', 'maxDate',  $("#eventEndDate").val());
		  
		}).change(function() {
		    var nxtDt = $(this).parents('td').next("td").find("input[name='eventEDate']").attr("id");
		    var prevDt = $(this).parents('td').find("input[name='eventDate']").attr("id");
		    $("#" + nxtDt).datepicker('option', 'minDate', $("#" + prevDt).val());
		});
		
		$("input[name='eventEDate']").each(function() {
			  var eed = $(this).attr("id");
  		// $("#"+eed).datepicker('option', 'minDate',  $("#eventStartDate").val());
		 $("#"+eed).datepicker('option', 'maxDate',  $("#eventEndDate").val());
			
			 $("#"+eed).datepicker('option', 'minDate',  "setDate", new Date());
			//$("#"+eed).datepicker('option', 'maxDate',  "setDate", new Date());
		});
	});

	function callNextPage(pagenumber, url) {
		document.bandForm.pageNumber.value = pagenumber;
		document.bandForm.pageFlag.value = "true";
		document.bandForm.action = url;
		document.bandForm.method = "get";
		document.bandForm.submit();
	}

	function associateBands(eventId) {
		document.bandForm.action = "associatebands.htm?eventid=" + eventId;
		document.bandForm.method = "get";
		document.bandForm.submit();
	}
	function saveAssociatedBands(){
		
		var isValid = false;
		var vBandStime='';
		var error = false;
		var vBandEtime='';
		var t = $("input[name='actions']:checked").parents('tr').find("input,select").not("input[name='_actions']").serializeArray();
		var stringObj="{\"associatedBands\":[{";
		$.each(t, function(i, field){
		   
			if(field.name =="actions")
		        {
				
		         stringObj+="\""+field.name+"\""+":"+"\""+$.trim(field.value)+"\"},{";
								
		        }else{
				
						
				 if(field.name == "startHH"  && field.value == "00")
				{
				vBandStime = field.value;
				}
			/*	 if(field.name == "startMM"  && field.value == "00")
				 {
				 vBandStime = vBandStime +field.value;
				 }*/
				 
				 
				 if(field.name == "endHH"  && field.value == "00")
				{
				vBandEtime = field.value;
				}
				/* if(field.name == "endMM"  && field.value == "00")
				 {
				 vBandEtime = vBandEtime +field.value;
				 }*/
				 
				 if(field.name == "eventDate" && field.value == "")
				{
				//alert("Please enter the start date");
				error = true;
				isValid=true;
				}else if (vBandStime == "00")
				 {
				// alert("Please select the start time");
				error = true;
				 	isValid=true;
				 }else if (vBandEtime == "00")
				 {
				// alert("Please select the end time");
				error = true;
				 	isValid=true;
				 }			 
				else{
		         stringObj+="\""+field.name+"\""+":"+"\""+field.value+"\""+",";
				 }	
				 
		        }
		});
		if(error == true){
			 alert("Please enter all the mandatory fields to associate band");
		 }
		
		
		if(!isValid)
		{
		var unassociated = $("input:checkbox:not(:checked)").parents('tr').find("input,select,checked").not("input[name='_actions']").serializeArray();
		var unAssociatedObj="{\"unAssociatedBands\":[{";
		$.each(unassociated, function(i, field){
		   
			if(field.name =="stage")
		        {
				unAssociatedObj+="\""+field.name+"\""+":"+"\""+field.value+"\"},{";
						
		        }else{
		        	unAssociatedObj+="\""+field.name+"\""+":"+"\""+field.value+"\""+",";
		        }
		});

		
		stringObj= stringObj.substring(0,stringObj.length-2)+"]}"; 
		unAssociatedObj= unAssociatedObj.substring(0,unAssociatedObj.length-2)+"]}";
	
		document.bandForm.action = "savebands.htm";
		document.bandForm.prodJson.value =stringObj;
		document.bandForm.unAssociatedJson.value = unAssociatedObj;
		document.bandForm.method = "POST";
		document.bandForm.submit();
		
		}
	}
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div class="wrpr-cont relative">
		<div id="slideBtn">
			<a href="javascript:void(0);" onclick="revealPanel(this);"
				title="Hide Menu"> <img src="images/slide_off.png" width="11"
				height="28" alt="btn_off" />
			</a>
		</div>
		<div id="bread-crumb">
			<ul>
				<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
				<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
					<li><a href="welcome.htm">Home</a></li>
				</sec:authorize>
				<li><a href="managebandevents.htm">Manage Band Events</a></li>
				<li class="last">Band Events</li>
			</ul>
		</div>
		<span class="blue-brdr"></span>
		<div class="content" id="login">
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>
			<div class="cont-pnl split" id="">
				<div class="cont-block stretch">
					<form:form name="bandForm" commandName="bandForm">
						<form:hidden path="lowerLimit" />
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<form:hidden path="hcEventID"/>
						<form:hidden path="prodJson"/>
						<form:hidden path="eventStartDate"/>
						<form:hidden path="eventEndDate"/>
						<form:hidden path="unAssociatedJson"/>
						<div class="title-bar">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-bandevents">&nbsp;</span>
								</li>
								<li>Associate Bands</li>
							</ul>
						</div>
						<div class="tabd-pnl">
							<ul class="nav-tabs">
								<li><a href="managebandevents.htm">Manage Band Events</a></li>
								<li><a id="mainMenu" href="managebandevents.htm"
									class="active rt-brdr">Associate Bands</a></li>
								<!--<li><a id="subMenu" href="displayeventcate.htm"
									class="rt-brdr">Manage Event Category</a></li> -->
							</ul>
							<div class="clear"></div>
						</div>
						<div class="cont-wrp">
							<table width="100%" border="0" cellspacing="0" cellpAdd ing="0"
								class="zerobrdrTbl">
								<tr>
									<c:choose>
										<c:when test="${requestScope.searchEvnt eq 'searchEvnt'}">
											<td width="6%"><label>Search</label></td>
											<td width="20%"><div class="cntrl-grp">
													<form:input type="text" path="eventSearchKey"
														class="inputTxtBig" onkeypress="searchEventBand(event)" />
												</div> <i class="emptysearh">Please Enter Search keyword</i></td>
											<td width="10%"><a href="javascript:void(0);"><img
													src="images/searchIcon.png" width="20" height="17"
													alt="search" title="Search event" maxlength="30"
													onclick="searchEventBand('')" /> </a></td>
										</c:when>
										<c:otherwise>
											<c:if
												test="${sessionScope.eventlst ne null && !empty sessionScope.eventlst  }">
												<td width="6%"><label>Search</label></td>
												<td width="20%"><div class="cntrl-grp">
														<form:input type="text" path="eventSearchKey"
															class="inputTxtBig" onkeypress="searchEventBand(event)" />
													</div> <i class="emptysearh">Please Enter Search keyword</i></td>
												<td width="10%"><a href="javascript:void(0);"><img
														src="images/searchIcon.png" width="20" height="17"
														alt="search" title="Search event" maxlength="30"
														onclick="searchEventBand('')" /> </a></td>
											</c:if>
										</c:otherwise>
									</c:choose>
								</tr>
							</table>
							<c:if
								test="${requestScope.responseBandEventstatus ne null && !empty requestScope.responseBandEventstatus}">
								<c:choose>
									<c:when
										test="${requestScope.responseBandEventstatus eq 'SUCCESS' }">
										<div class="alertBx success mrgnTop cntrAlgn">
											<span class="actn-close" title="close"></span>
											<p class="msgBx">
												<c:out value="${requestScope.responeBandEventMsg}" />
											</p>
										</div>
									</c:when>
								</c:choose>
							</c:if>
							<div class="relative mrgnTop">
								<div class="hdrClone"></div>
								<c:choose>
									<c:when
										test="${sessionScope.bandList ne null && sessionScope.bandList.eventLst ne null && !empty sessionScope.bandList.eventLst}">
										<div class="overflowxscroll">
											<table width="160%" cellspacing="0" cellpadding="0"
												border="0" id="alertTbl" class="grdTbl clone-hdr btmalgn fxdhtTbl">
												<thead>
													<tr class="tblHdr">
													
														<th width="20%">Band Name</th>
														<th width="20%"><label class="mand">Start date</label></th>
														<th width="16%">End Date</th>
														<th width="20%" colspan="2"><label class="mand" >Start time</label></th>
														<th width="20%" colspan="2"><label class="mand">End Time</label></th>
														<th width="46%">Stage</th>
														<th width="6%">Associate</th>
													</tr>
													
												</thead>
												<tbody class="scrollContent " id="eventLogisticsBtn">
													<c:forEach items="${sessionScope.bandList.eventLst}"
														var="band">
														<tr id="logisticsBtnListId-${band.bandId}">
															<td>${band.bandName}
															<form:hidden path="bandIds" value="${band.bandId}"></form:hidden></td>
															<td><form:errors cssClass="errorDsply" path="eventDate"></form:errors>
															<div class="cntrl-grp cntrl-dt floatL">
																<form:input path="eventDate" id="eventDate-${band.bandId}" type="text" 
																value="${band.eventDate}"	class="inputTxt eventDateclassjs"  readonly="readonly"/>
															</div></td>
															<td>
															<div class="cntrl-grp cntrl-dt floatL">
																<form:input path="eventEDate" id="eventEDate-${band.bandId}" type="text" value="${band.eventEDate}"
																	class="inputTxt eventEDateclassjs"  readonly="readonly"/>
															</div></td>
															<td><form:errors cssClass="errorDsply" path="startHH"></form:errors>
															<form:select path="startHH" class="slctSmall"
																	name="startHH">
																	<c:forEach items="${StartHours}" var="hours">
																		<c:choose>
																			<c:when test="${hours.value eq band.startHH}">
																				<form:option value="${hours.value}"
																					selected="selected" />
																			</c:when>
																			<c:otherwise>
																				<form:option value="${hours.value}" />
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</form:select> Hrs</td>
															<td>
															<form:errors cssClass="errorDsply" path="startMM"></form:errors>
															<form:select path="startMM" class="slctSmall"
																	name="startMM">
																	<c:forEach items="${StartMinutes}" var="minutes">
																		<c:choose>
																			<c:when test="${minutes.value eq band.startMM}">
																				<form:option value="${minutes.value}"
																					selected="selected" />
																			</c:when>
																			<c:otherwise>
																				<form:option value="${minutes.value}" />
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</form:select> Mins</td>
															<td><form:select path="endHH" class="slctSmall"
																	name="endHH">
																	<c:forEach items="${StartHours}" var="hours">
																		<c:choose>
																			<c:when test="${hours.value eq band.endHH}">
																				<form:option value="${hours.value}"
																					selected="selected" />
																			</c:when>
																			<c:otherwise>
																				<form:option value="${hours.value}" />
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</form:select> Hrs</td>
															<td><form:select path="endMM" class="slctSmall"
																	name="endMM">
																	<c:forEach items="${StartMinutes}" var="minutes">
																		<c:choose>
																			<c:when test="${minutes.value eq band.endMM}">
																				<form:option value="${minutes.value}"
																					selected="selected" />
																			</c:when>
																			<c:otherwise>
																				<form:option value="${minutes.value}" />
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</form:select> Mins</td>
															<td>
																<div class="cntrl-grp">
																	<form:input path="stage" type="text"
																		class="inputTxt stage" maxlength="40"
																		value="${band.stage}" />
																</div>
															</td>
															<td><c:choose>
																	<c:when
																		test="${null ne band.action && band.action == true }">
																		<form:checkbox path="actions" value="${band.bandId}"
																			class="action" checked="checked" />
																	</c:when>
																	<c:otherwise>
																		<form:checkbox path="actions" class="action"
																			value="${band.bandId}"/>
																	</c:otherwise>
															</c:choose></td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</c:when>
									<c:otherwise>
										<div class="alertBx warning mrgnTop cntrAlgn" id="evntClose">
											<span class="actn-close" title="close"></span>
											<p class="msgBx">No Bands found</p>
										</div>
									</c:otherwise>
								</c:choose>
							</div>
							<c:if
								test="${sessionScope.bandList ne null && !empty sessionScope.bandList}">
								<div class="pagination mrgnTop">
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" />
								</div>
							</c:if>
							<c:choose>
									<c:when test="${sessionScope.bandList ne null && sessionScope.bandList.eventLst ne null && !empty sessionScope.bandList.eventLst}">
									<div class="col" align="right">
									<input type="button" class="btn-blue" value="Save" title="Save"
										onclick="saveAssociatedBands()" />
									</div>
									</c:when>
							</c:choose>		
					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	configureMenu("setupbandevent");
	$( window ).load(function() {

	$(".hasDatepicker").attr("readonly","readonly");
	});
	
</script>

</html>
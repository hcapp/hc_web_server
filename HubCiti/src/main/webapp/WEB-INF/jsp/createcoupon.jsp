<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-min.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script src="/HubCiti/scripts/ckeditor/ckeditor.js"></script>
<script src="/HubCiti/scripts/ckeditor/config.js"></script>
<script type="text/javascript" src="scripts/web.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<title>Create Deal</title>
<head>
<script type="text/javascript">
	$(document)
			.ready(
					function() {

						CKEDITOR.editorConfig = function( config ) {
							//config.uiColor = '#AADC6E';
							config.language = 'es';
							config.uiColor = '#f3f3f3';
							config.height = 300;
							config.toolbarCanCollapse = true;
							config.extraPlugins = 'wordcount';
							config.wordcount = {

								// Whether or not you want to show the Word Count
								showWordCount: true,

								// Whether or not you want to show the Char Count
								showCharCount: true,
								
								// Maximum allowed Char Count
								maxCharCount: 500
							};
						};
						CKEDITOR.replace('coupDescription',{

							extraPlugins : 'onchange',
							width : "100%",
							toolbar : [
									/* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
									{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
									{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
									'/',
									{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
									{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
									{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
									{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
									'/',
									{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
									{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
									{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
									{
										name : 'basicstyles',
										items : [ 'Bold', 'Italic',
												'Underline' ]
									},
									{
										name : 'paragraph',
										items : [ 'JustifyLeft',
												'JustifyCenter',
												'JustifyRight',
												'JustifyBlock' ]
									},
									{
										name : 'colors',
										items : [ 'BGColor' ]
									},
									{
										name : 'paragraph',
										items : [ 'Outdent', 'Indent' ]
									},
									{
										name : 'links',
										items : [ 'Link', 'Unlink' ]
									},
									'/',
									{
										name : 'styles',
										items : [ 'Styles', 'Format' ]
									},
									{
										name : 'tools',
										items : [ 'Font', 'FontSize',
												'RemoveFormat' ]
									} ],
							removePlugins : 'resize'
						});
						CKEDITOR.on('instanceReady', function (evt) {
						    //editor
						    var editor = evt.editor;

						    //webkit not redraw iframe correctly when editor's width is < 310px (300px iframe + 10px paddings)
						    if (CKEDITOR.env.webkit && parseInt(editor.config.width) < 310) {
						    	if(editor.name == 'coupDescription')
						    	{
								   	var iframe = document.getElementById('cke_contents_' + editor.name).getElementsByTagName('iframe')[0];        
								    iframe.style.display = 'none';
								    iframe.style.display = 'block';
						    	}
						    }
						});
						
						$('#trgrUpldCoupImg')
								.bind(
										'change',
										function() {

											var imageType = document
													.getElementById("trgrUpldCoupImg").value;
											var uploadImageType = $(this).attr(
													'name');

											document.CreateCouponForm.uploadCoupImageType.value = uploadImageType;
											if (imageType != '') {
												var checkbannerimg = imageType
														.toLowerCase();
												if (!checkbannerimg
														.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
													alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
													return false;
												} else {
													$("#CreateCouponForm")
															.ajaxForm(
																	{
																		success : function(
																				response) {
																			var imgRes = response
																					.getElementsByTagName('imageScr')[0].firstChild.nodeValue;
																			var substr = imgRes
																					.split('|');
																			if (substr[0] == 'maxSizeImageError') {
																				$(
																						'#maxSizeDealLstImgError')
																						.text(
																								"Image Dimension should not exceed Width: 800px Height: 600px");
																			} else {
																				openIframePopup(
																						'ifrmPopup',
																						'ifrm',
																						'/HubCiti/cropImage.htm',
																						100,
																						99.5,
																						'Crop Image');
																			}
																		}
																	}).submit();
												}
											}

										});

						//for deal list image trgrUpldCoupImg
						$('#trgrUpldDetailImg')
								.bind(
										'change',
										function() {

											var imageType = document
													.getElementById("trgrUpldDetailImg").value;
											var uploadImageType = $(this).attr(
													'name');

											document.CreateCouponForm.uploadCoupImageType.value = uploadImageType;
											if (imageType != '') {
												var checkbannerimg = imageType
														.toLowerCase();
												if (!checkbannerimg
														.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
													alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
													return false;
												} else {
													$("#CreateCouponForm")
															.ajaxForm(
																	{
																		success : function(
																				response) {
																			var imgRes = response
																					.getElementsByTagName('imageScr')[0].firstChild.nodeValue;
																			var substr = imgRes
																					.split('|');
																			if (substr[0] == 'maxSizeImageError') {
																				$(
																						'#maxSizeDealImgError')
																						.text(
																								"Image Dimension should not exceed Width: 800px Height: 600px");
																			} else {
																				openIframePopup(
																						'ifrmPopup',
																						'ifrm',
																						'/HubCiti/cropImage.htm',
																						100,
																						99.5,
																						'Crop Image');
																			}
																		}
																	}).submit();
												}
											}

										});

						$("#coupStartDate").datepicker({
							showOn : 'both',
							buttonImageOnly : true,
							buttonText : 'Click to show the calendar',
							buttonImage : 'images/icon-calendar-active.png'
						});
						$("#coupEndDate").datepicker({
							showOn : 'both',
							buttonImageOnly : true,
							buttonText : 'Click to show the calendar',
							buttonImage : 'images/icon-calendar-active.png'
						});
						$("#coupExpiryDate").datepicker({
							showOn : 'both',
							buttonImageOnly : true,
							buttonText : 'Click to show the calendar',
							buttonImage : 'images/icon-calendar-active.png'
						});

						var existingretId = $('#retailerId').val();
						var selectedLocIds = $('#tempretids').val();
						if (existingretId != 'undefined') {
							if (selectedLocIds == null) {
								getLogRetailerLocs(existingretId, "");
							} else {
								getLogRetailerLocs(existingretId,
										selectedLocIds)
							}

						}

						/*	var prodCategory = '${requestScope.prodcategory}';
						if(null != prodCategory && "" != prodCategory) {
						var arr = prodCategory.split(',');	
						jQuery.each(arr, function(i, val) { 
							$("#prodCategory option[value='" + val + "']").prop('selected', 'selected');
						});
						var totOpt = $('#prodCategory option').length;
						var totOptSlctd = $('#prodCategory option:selected').length;
						if (totOpt == totOptSlctd) {
							$('#chkAllProdCat').prop('checked', 'checked');
						} else {
							$('#chkAllProdCat').removeAttr('checked');
						}
						} 
						
						$('#chkAllProdCat').click(function() {
						
						var sel = document.getElementById("prodCategory");
						if ($(this).is(':checked')){
							for ( var i = 0; i < sel.options.length; i++) {
								sel.options[i].selected = true;
							}
						}else{
						for ( var i = 0; i < sel.options.length; i++) {
								sel.options[i].selected = false;
							}
						}
						
						});
						
							$("#prodCategory").change(function() {
						var totOpt = $('#prodCategory option').length;
						var totOptSlctd = $('#prodCategory option:selected').length;
						if (totOpt == totOptSlctd) {
						$('#chkAllProdCat').prop('checked', 'checked');
						} else {
						$('#chkAllProdCat').removeAttr('checked');
						}
						});
						
						$('#prodCategory option').click(function() {
						var totOpt = $('#prodCategory option').length;
						var totOptSlctd = $('#prodCategory option:selected').length;
						if (totOpt == totOptSlctd) {
						$('#chkAllProdCat').prop('checked', 'checked');
						} else {
						$('#chkAllProdCat').removeAttr('checked');
						}
						}); */

						var locationId = '${requestScope.locationId}';
						if (null != locationId && "" != locationId) {
							var arr = locationId.split(',');

							jQuery.each(arr, function(i, val) {
								$("#locationId option[value='" + val + "']")
										.prop('selected', 'selected');
							});
							var totOpt = $('#locationId option').length;
							var totOptSlctd = $('#locationId option:selected').length;
							if (totOpt == totOptSlctd) {
								$('#chkAllLoc').prop('checked', 'checked');
							} else {
								$('#chkAllLoc').removeAttr('checked');
							}
						}

						$('#chkAllLoc').click(function() {

							var sel = document.getElementById("locationId");
							if ($(this).is(':checked')) {
								for (var i = 0; i < sel.options.length; i++) {
									sel.options[i].selected = true;
								}
							} else {
								for (var i = 0; i < sel.options.length; i++) {
									sel.options[i].selected = false;
								}
							}

						});

						$("#locationId")
								.change(
										function() {
											var totOpt = $('#locationId option').length;
											var totOptSlctd = $('#locationId option:selected').length;
											if (totOpt == totOptSlctd) {
												$('#chkAllLoc').prop('checked',
														'checked');
											} else {
												$('#chkAllLoc').removeAttr(
														'checked');
											}
										});

						$('#locationId option')
								.click(
										function() {
											var totOpt = $('#locationId option').length;
											var totOptSlctd = $('#locationId option:selected').length;
											if (totOpt == totOptSlctd) {
												$('#chkAllLoc').prop('checked',
														'checked');
											} else {
												$('#chkAllLoc').removeAttr(
														'checked');
											}
										});

						var coupid = $("#couponId").val();

						if (coupid != "") {
							document.title = "Update Deal";
							$("#coupTitle").text("Update Deal");

							$("#saveCpBtn").val("Update");

						}

					});

	function manageCoupons() {
		document.CreateCouponForm.action = "managecoupons.htm";
		document.CreateCouponForm.method = "POST";
		document.CreateCouponForm.submit();

	}

	function saveCoupon() {
		document.CreateCouponForm.action = "savecoupon.htm";
		document.CreateCouponForm.method = "POST";
		document.CreateCouponForm.submit();
	}

	function logRetNameAutocomplete(retaName) {

		$("#retailerName").autocomplete({
			minLength : 3,
			delay : 200,
			source : '/HubCiti/displaylogretnames.htm',
			select : function(event, ui) {

				if (ui.item.value == "No Records Found") {
					$("#retailerName").val("");
				} else {
					$("#retailerName").val(ui.item.rname);
					$("#retailerId").val(ui.item.retId);
					getLogRetailerLocs(ui.item.retId, "");
				}
				return false;
			}
		});
	}

	function getLogRetailerLocs(retId, selectedLocId) {

		$('#locationId').find('option').remove();
		$('#chkAllLoc').removeAttr('checked');

		$.ajaxSetup({
			cache : false
		});

		$
				.ajax({
					type : "GET",
					url : "displaylogretLoc.htm",
					data : {
						'retId' : retId

					},

					success : function(response) {

						var rLocations = response;

						var objs = JSON.parse(rLocations);

						slctbox = $('#locationId');
						$('#locationId').find('option:not(:first)').remove();
						if (objs != null && objs != 'undefined') {
							for (var i = 0; i < objs.length; i++) {
								slctbox.append(new Option(objs[i].address,
										objs[i].retLocId));
							}

							if ("" !== selectedLocId) {

								var arr = selectedLocId.split(',');
								jQuery.each(arr,
										function(i, val) {
											$(
													"#locationId option[value='"
															+ val + "']").prop(
													'selected', 'selected');
										});

								var totOpt = $('#locationId option').length;
								var totOptSlctd = $('#locationId option:selected').length;
								if (totOpt == totOptSlctd) {
									$('#chkAllLoc').prop('checked', 'checked');
								} else {
									$('#chkAllLoc').removeAttr('checked');
								}
							}

						}
					},
					error : function(e) {
						alert('Error occured while fetching retailer locations');
					}
				});

	}

	function clearForm() {
		var r = confirm("Do you really want to clear the form");
		if (r == true) {
			if (document.getElementById('coupTitle.errors') != null) {
				document.getElementById('coupTitle.errors').style.display = 'none';
			}
			if (document.getElementById('coupBanTitle.errors') != null) {
				document.getElementById('coupBanTitle.errors').style.display = 'none';
			}
			if (document.getElementById('coupDescription.errors') != null) {
				document.getElementById('coupDescription.errors').style.display = 'none';
			}
			if (document.getElementById('coupNum.errors') != null) {
				document.getElementById('coupNum.errors').style.display = 'none';
			}
			if (document.getElementById('coupStartDate.errors') != null) {
				document.getElementById('coupStartDate.errors').style.display = 'none';
			}
			if (document.getElementById('coupEndDate.errors') != null) {
				document.getElementById('coupEndDate.errors').style.display = 'none';
			}
			if (document.getElementById('coupExpiryDate.errors') != null) {
				document.getElementById('coupExpiryDate.errors').style.display = 'none';
			}
			if (document.getElementById('locationId.errors') != null) {
				document.getElementById('locationId.errors').style.display = 'none';
			}
			if (document.getElementById('retailerName.errors') != null) {
				document.getElementById('retailerName.errors').style.display = 'none';
			}
			if (document.getElementById('coupImg.errors') != null) {
				document.getElementById('coupImg.errors').style.display = 'none';
			}
			if (document.getElementById('dealLstImgName.errors') != null) {
				document.getElementById('dealLstImgName.errors').style.display = 'none';
			}

			document.CreateCouponForm.coupTitle.value = "";
			document.CreateCouponForm.coupBanTitle.value = "";
			document.CreateCouponForm.coupDescription.value = "";
			document.CreateCouponForm.coupKeywords.value = "";
			document.CreateCouponForm.coupNum.value = "";
			document.CreateCouponForm.coupTermsConds.value = "";
			document.CreateCouponForm.retailerName.value = "";
			document.CreateCouponForm.locationId.value = "";
			document.CreateCouponForm.coupStartDate.value = "";
			document.CreateCouponForm.coupEndDate.value = "";
			document.CreateCouponForm.coupExpiryDate.value = "";
			document.CreateCouponForm.retailerId.value = "";
			document.CreateCouponForm.coupSTimeHrs.value = "00";
			document.CreateCouponForm.coupExpTimeHrs.value = "00";
			document.CreateCouponForm.coupETimeHrs.value = "00";

			document.CreateCouponForm.coupSTimeMins.value = "00";
			document.CreateCouponForm.coupETimeMins.value = "00";
			document.CreateCouponForm.coupExpTimeMins.value = "00";
			document.CreateCouponForm.coupImg.value = null;
			document.CreateCouponForm.dealLstImgName.value = null;
			$('#locationId').html('');
			$('#chkAllLoc').prop("checked", false);
			CKEDITOR.instances['coupDescription'].setData('');

			$("#coupImg").prop('src', 'images/uploadIconSqr.png');
			$("#dealLstImgName").prop('src', 'images/uploadIconSqr.png');
		}
	}
</script>


</head>
<body>

	<!--Wrapper div Starts-->
	<div id="wrpr">
		<div class="clear"></div>
		<div class="wrpr-cont relative">
			<div id="slideBtn">
				<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
					src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
			</div>
			<!--Breadcrum div starts-->
			<div id="bread-crumb">
				<ul>
					<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
					<li><a href="welcome.htm">Home</a></li>
					<li class="last">Deals</li>
				</ul>
			</div>
			<!--Breadcrum div ends-->
			<span class="blue-brdr"></span>
			<!--Content div starts-->
			<div class="content" id="login">
				<!--Left Menu div starts-->
				<div id="menu-pnl" class="split">
					<jsp:include page="leftNavigation.jsp"></jsp:include>
				</div>
				<!--Left Menu div ends-->
				<!--Content panel div starts-->
				<div class="cont-pnl split" id="">
					<div class="cont-block stretch">
						<div class="title-bar">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-news">&nbsp;</span></li>
								<li>Deals</li>
							</ul>
						</div>
						<div class="tabd-pnl">
							<ul class="nav-tabs">
								<li><a class="active rt-brdr" id="coupTitle" href="#">Create
										Deal</a></li>
							</ul>
							<div class="clear"></div>
						</div>

						<form:form name="CreateCouponForm" id="CreateCouponForm"
							commandName="CreateCouponForm" enctype="multipart/form-data"
							action="cropcoupimage.htm">

							<form:hidden path="uploadCoupImageType" />
							<form:hidden path="retailerId" />
							<form:hidden path="tempretids" />
							<form:hidden path="couponId" id="couponId" />
							<div class="cont-wrp">
								<div class="relative">
									<div class="hdrClone"></div>
									<table class="brdrlsTbl" width="100%" cellspacing="0"
										cellpadding="0" border="0">
										<tbody>



											<tr>
												<td width="14%"><label class="mand">Title</label></td>
												<td width="26%"><form:errors cssClass="errorDsply"
														path="coupTitle"></form:errors>
													<div class="cntrl-grp">
														<form:input path="coupTitle" type="text" class="inputTxt"
															tabindex="1" maxlength="40" />
													</div></td>

												<td width="14%"><label class="mand">Banner
														Title </label></td>
												<td width="46%"><form:errors cssClass="errorDsply"
														path="coupBanTitle"></form:errors>
													<div class="cntrl-grp">

														<form:input path="coupBanTitle" type="text" class="inputTxt" maxlength="40"
															tabindex="2" />

													

													</div></td>
											</tr>
											<!--	<tr><td colspan="3"><div class="cntrl-grp zeroBrdr">
											<b>Note:Please select either product category or location to create coupon </b> </div>
											</td></tr>

											<tr>
											
												<td><label class="">Product Category</label></td>
												<td colspan="">
													<div>

														<input name="chkAllProdCat" id="chkAllProdCat"
															onclick="chkAllProdCat(this.checked);" tabindex="3"
															type="checkbox"> Select All Categories
													</div> 
													<div class="cntrl-grp zeroBrdr">
														<form:select path="prodCategory" id="prodCategory"
															multiple="multiple" cssClass="slctBx userMultiSlct"
															tabindex="4">
															<c:forEach items="${sessionScope.prodCategories}"
																var="prodcat">
																<form:option value="${prodcat.catId}">${prodcat.catName}</form:option>
															</c:forEach>
														</form:select>
														<label class="lblTxt" tabindex="5">Hold Ctrl to
															select more than one category</label>
													</div>
												</td>
												<!--	<td valign="top" align="left" class="Label" colspan="">
										<label>
											<input type="checkbox" id="chkfundraiserAllCat" tabindex="14">
												Select All Categories 
										</label>
									</td> -->
											<tr>


												<td align="left"><label class="mand"># of Deals
														to issue</label></td>
												<td><form:errors cssClass="error" path="coupNum"
														cssStyle="color:red" />

													<div class="cntrl-grp">
														<form:input path="coupNum" type="text" class="inputTxt"
															maxlength="4" tabindex="3"
															onkeypress="return isNumberKey(event)" />
													</div></td>
												<td align="left"><label class="">Terms &amp;
														Conditions</label></td>
												<td><span class="set-padding">
														<div class="cntrl-grp">
															<form:textarea name="coupTermsConds" tabindex="4"
																path="coupTermsConds" cols="25" rows="5"
																class="textareaTxt"></form:textarea>
														</div>
														<p class="mrgnBtm_small">&nbsp;</p></td>
											</tr>
											<!--	<tr><td colspan="3"><div class="cntrl-grp zeroBrdr">
											<form:errors cssStyle="color:red" path="prodCategory"></form:errors></div>
											</td></tr> -->

											<tr>

												<td align=""><label class="mand">Retailer Name</label></td>
												<td colspan=""><form:errors cssClass="error"
														path="retailerName" cssStyle="color:red" />
													<div class="cntrl-grp">


														<form:input path="retailerName" type="text"
															onkeypress='logRetNameAutocomplete(retailerName);'
															class="loadingInput" tabindex="5" maxlength="30" />
													</div></td>



												<td><label class="mand">Location(s)</label></td>
												<td>
													<div>

														<input name="chkAllLoc" id="chkAllLoc"
															onclick="chkAllLoc(this.checked);" tabindex="6"
															type="checkbox"> Select All Locations
													</div> <form:errors cssStyle="color:red" path="locationId"></form:errors>
													<div class="cntrl-grp">
														<form:select id="locationId" path="locationId"
															tabindex="7" class="slctBx textareaTxt"
															multiple="multiple">
														</form:select>
														<br />

													</div> <label class="lblTxt">Hold Ctrl to select more
														than one Location</label>
													</div>
												</td>



											</tr>
											<tr>


												<td><label class="mand">Description</label></td>
												<td colspan="3"><form:errors cssClass="error"
														path="coupDescription" cssStyle="color:red" />
													<div class="cntrl-grp">

														<form:textarea path="coupDescription" id="coupDescription"
															name="textarea" rows="5" cols="25"
															class="textareaTxt txtAreaLarge" maxlength="5000"
															tabindex="8" />
													</div></td>

											</tr>


											<tr>

												<td class="dealtxt"><label class="mand">Deal
														List Image</label></td>
												<td><form:errors cssClass="errorDsply" path="coupImg"></form:errors>
													<label> <img id="coupImg" width="57" height="57"
														alt="upload" src="${sessionScope.coupImagePreview}">
												</label> <span class="topPadding cmnRow"> <label
														for="trgrUpldCoupImg" class="coupimglbl"> <input
															type="button" value="Upload" id="trgrUpldBtnImg"
															class="btn trgrUpldCoupImg" title="Upload Image File"
															tabindex="9"> <form:hidden path="coupImg"
																id="coupImg" /> <span class="instTxt nonBlk"></span> <form:input
																type="file" class="textboxBig" id="trgrUpldCoupImg"
																path="imageFile" /> <!--	<li align="center">Suggested Size:<br>300px/600px<br>Maximum Size:800px/600px<br></li> -->
													</label>

												</span> <label id="maxSizeDealLstImgError" class="errorDsply"></label></td>


												<td><label class="mand">Deal Image</label></td>
												<td><form:errors cssClass="errorDsply"
														path="dealLstImgName"></form:errors> <label> <img
														id="dealLstImgName" width="57" height="57" alt="upload"
														src="${sessionScope.dealImgPreview}">
												</label> <span class="topPadding cmnRow"> <label
														for="trgrUpldDetailImg" class="coupimglbl"> <input
															type="button" value="Upload" id="trgrUpldBtnImg"
															class="btn trgrUpldDetailImg" title="Upload Image File"
															tabindex="10"> <form:hidden path="dealLstImgName"
																id="dealLstImgName" /> <span class="instTxt nonBlk"></span>
															<form:input type="file" class="textboxBig"
																id="trgrUpldDetailImg" path="dealimageFile" />
													</label>
												</span><label id="maxSizeDealImgError" class="errorDsply"></label></td>
											</tr>

											<tr>
												<td align="left"><label class="mand">Start Date</label></td>
												<td><form:errors cssClass="errorDsply"
														path="coupStartDate"></form:errors>
													<div class="cntrl-dt floatL">
														<form:input path="coupStartDate" id="coupStartDate"
															type="text" class="inputTxt" tabindex="11" />
													</div></td>
												<td align="left">Start Time</td>
												<td><form:select path="coupSTimeHrs" class="slctSmall"
														name="etHr" tabindex="12">
														<form:options items="${StartHours}" />
													</form:select> Hrs <form:select path="coupSTimeMins" class="slctSmall"
														name="stMin" tabindex="13">
														<form:options items="${StartMinutes}" />
													</form:select> Mins</td>

											</tr>
											<tr>

												<td align="left"><label class="">End Date</label></td>
												<td><form:errors cssClass="errorDsply"
														path="coupEndDate"></form:errors>
													<div class="cntrl-dt floatL">
														<form:input path="coupEndDate" id="coupEndDate"
															type="text" class="inputTxt" tabindex="14" />
													</div></td>
												<td>End Time</td>
												<td><form:select path="coupETimeHrs" class="slctSmall"
														name="etHr" tabindex="15">
														<form:options items="${StartHours}" />
													</form:select> Hrs <form:select path="coupETimeMins" class="slctSmall"
														name="stMin" tabindex="16">
														<form:options items="${StartMinutes}" />
													</form:select> Mins</td>


											</tr>
											<tr>

												<td align="left"><label>Expiration Date</label></td>
												<td><form:errors cssClass="errorDsply"
														path="coupExpiryDate"></form:errors>
													<div class="cntrl-dt floatL">
														<form:input path="coupExpiryDate" id="coupExpiryDate"
															type="text" class="inputTxt" tabindex="17" />
													</div></td>
												<td>Expiration Time</td>
												<td><form:select path="coupExpTimeHrs"
														class="slctSmall" name="etHr" tabindex="18">
														<form:options items="${StartHours}" />
													</form:select> Hrs <form:select path="coupExpTimeMins" class="slctSmall"
														name="stMin" tabindex="19">
														<form:options items="${StartMinutes}" />
													</form:select> Mins</td>

											</tr>

											<tr>


												<td align="left"><label class="">Keywords</label></td>
												<td><span class="set-padding">
														<div class="cntrl-grp">
															<form:textarea name="coupKeywords" path="coupKeywords"
																maxlength="50" tabindex="20" cols="25" rows="5"
																class="textareaTxt"></form:textarea>
														</div>
														<p class="mrgnBtm_small">&nbsp;</p></td>
											</tr>

										</tbody>
									</table>

								</div>
								<div class="cntrInput mrgnTop">
									<input value="Back" class="btn-blue" type="button"
										tabindex="21" onclick="manageCoupons()"> <input
										id="saveCpBtn" value="Save" class="btn-blue" type="button"
										tabindex="22" onclick="saveCoupon()"> <input
										type="button" value="Clear" onclick="clearForm();"
										class="btn-blue" tabindex="23" title="clear" />
								</div>
							</div>
						</form:form>
					</div>

					<!--Content panel div ends-->
				</div>
				<!--Content div ends-->
			</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
			<div class="headerIframe">
				<img src="/HubCiti/images/popupClose.png" class="closeIframe"
					alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
				allowtransparency="yes" width="100%" style="background-color: White">
			</iframe>
		</div>
</body>
<script type="text/javascript">
	configureMenu("setupdeals");
</script>
</html>
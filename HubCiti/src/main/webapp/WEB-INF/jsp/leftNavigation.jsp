<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">

	$(document)
			.ready(
					function() {
						$('#setuploginpage').click(function(){
							
							localStorage.setItem('curActive', "scrn-1");
							localStorage.setItem('curActiveScrnName', "1");
						})	
					});
	</script>
<ul id="icon-menu">
	<c:choose>
		<c:when test="${sessionScope.loginUserType eq 'RegionApp'}">
			<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
				<li><a href="welcome.htm" class="icon-adminStng"
					title="HubRegion Admin" id="hubCitiadministration"><span>HubRegion<sup>&#174;</sup>
							Admin
					</span></a></li>
			</sec:authorize>
		</c:when>
		<c:otherwise>
			<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
				<li><a href="welcome.htm" class="icon-adminStng"
					title="Hubciti Admin" id="hubCitiadministration"><span>HubCiti<sup>&#174;</sup>
							Administration
					</span></a></li>
			</sec:authorize>
		</c:otherwise>
	</c:choose>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displayusers.htm" class="icon-usersetup" title="Users"
			id="setupusers"><span>Users</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="setuploginscreen.htm" class="icon-login"
			title="Login Screen" id="setuploginpage"><span>
					Login Screen</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="generealsettings.htm" class="icon-setting"
			title="General Settings" id="generalsettings"><span>General
					Settings</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="setupsplashscreen.htm" class="icon-welcome"
			title="Welcome Page" id="setupsplashscreen"><span>
					Welcome Page</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="usersettings.htm" class="icon-usrstng"
			title="User Settings" id="setupusersettings"><span>
					User Settings</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="setupregscreen.htm" class="icon-registration"
			title="Registration" id="setupregistration"><span>
					Registration</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="setupprivacypolicyscreen.htm"
			class="icon-privacy-policy" title="Setup Privacy Policy"
			id="setupprivacypolicy"><span> Privacy Policy</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="setupaboutusscreen.htm" class="icon-about"
			title="About Us" id="setupaboutus"><span> About
					Us</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displaymainmenu.htm" class="icon-menu"
			title="Main Menu" id="setupmainmenu"><span>
					Main Menu</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displaysubmenu.htm" class="icon-submenu"
			title="Sub Menu" id="setupsubmenu"><span> Sub
					Menu</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displaysidemenu.htm" class="icon-sidemenu"
			title="Side Menu" id="setupsidemenu"><span> Side
					Menu</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="setuptabbar.htm" class="icon-tabbar"
			title="Tab Bar" id="setuptabbar"><span> Tab Bar</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displayanythingpages.htm" class="icon-about"
			title="AnyThing Page" id="setupanythingpage"><span>
					AnyThing<sup>TM</sup> Page
			</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displayalerts.htm" class="icon-alert"
			title="Alerts" id="setupalerts"><span> Alerts</span></a></li>
	</sec:authorize>
	<sec:authorize
		access="hasAnyRole('ROLE_ADMIN_VIEW','ROLE_EVENT_SUPER_USER_VIEW','ROLE_EVENT_USER_VIEW')">
		<li><a href="manageevents.htm" class="icon-event"
			title="Events" id="setupevents"><span> Events</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="managebandevents.htm" class="icon-bandevent"
			title="Band Event" id="setupbandevent"><span>
					Band Events</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displaycityexp.htm" class="icon-experience"
			title="Experience" id="setupexperience"><span>
					Experience</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="getassociretlocs.htm" class="icon-retlrLoctn"
			title="Retailer Location" id="setupretailerlocation"><span>
					Retailer Location</span></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displayfaq.htm" class="icon-faq" title="FAQ"
			id="setupfqa"><span>FAQ's</span></a></li>
	</sec:authorize>	
	<sec:authorize
		access="hasAnyRole('ROLE_ADMIN_VIEW','ROLE_FUNDRAISING_SUPER_USER_VIEW','ROLE_FUNDRAISING_USER_VIEW')">
		<li><a href="managefundraisers.htm" class="icon-fundraising"
			title="Fundraisers" id="setupfndrevt"><span>Fundraisers</span></a></li>
	</sec:authorize>
	<sec:authorize
		access="hasAnyRole('ROLE_ADMIN_VIEW','ROLE_DEAL_SUPER_USER_VIEW','ROLE_DEAL_USER_VIEW')">
		<li><a href="displaydeals.htm" class="icon-deal"
			title="Deal Of The Day" id="setupdealsday"><span>Deal Of The Day</span></a></li>
	</sec:authorize>

		<sec:authorize
		access="hasAnyRole('ROLE_ADMIN_VIEW','ROLE_DEAL_SUPER_USER_VIEW','ROLE_DEAL_USER_VIEW')">
		<li><a href="managecoupons.htm" class="icon-coupons"
			title="Deals" id="setupdeals"><span>Deals</span></a></li>
	</sec:authorize>
<!--
	<sec:authorize
		access="hasAnyRole('ROLE_ADMIN_VIEW')">
		<li><a href="displayspecialofferpages.htm" class="icon-specialoffer"
			title="Setup Special Offer" id="setupspecialoffer"><span>Setup Special Offer</span></a></li>
	</sec:authorize>
-->
	<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="displaylogistics.htm" class="icon-map"
			title="Interactive Map" id="setupLogisticsMap"><span>Interactive Map</span></a></li>
	</sec:authorize>
	
		<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
		<li><a href="newsgeneralsettings.htm" class="icon-news"
			title="News Settings" id="newssettings"><span>News Settings</span></a></li>
	</sec:authorize>
	
	
</ul>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>

<script type="text/javascript">


	function searchLogistics() {
		document.logistics.lowerLimit.value = 0;
		document.logistics.action = "displaylogistics.htm";
		document.logistics.method = "get";
		document.logistics.submit();
	
	}
	
	function searchLogisticsOnkeyPress(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if (keycode == '13') 
		{
			document.logistics.lowerLimit.value = 0;
			document.logistics.action = "displaylogistics.htm";
			document.logistics.method = "get";
			document.logistics.submit();
		}
	}
	
	function callNextPage(pagenumber, url) 
	{
		document.logistics.pageNumber.value = pagenumber;
		document.logistics.pageFlag.value = "true";
		document.logistics.action = url;
		document.logistics.method = "get";
		document.logistics.submit();
	}

	function addLogistics() {
		document.logistics.action = "addlogistics.htm";
		document.logistics.method = "get";
		document.logistics.submit();
	}
	
	function editLogistics(logisticsId) {
		document.logistics.logisticId.value = logisticsId;
		document.logistics.action = "editlogistics.htm";
		document.logistics.method = "get";
		document.logistics.submit();
	}
	
	function deleteLogistics(logisticsId) {
		r = confirm("Are you sure you want to delete this interactive map !");
		if(r) {
			document.logistics.logisticId.value = logisticsId;
			document.logistics.action = "deletelogistics.htm";
			document.logistics.method = "get";
			document.logistics.submit();
		}
	}
	
	function addMarkers(logisticsId,addMarker) {
		document.logistics.addMarker.value = addMarker;
		document.logistics.logisticId.value = logisticsId;
		document.logistics.action = "editlogistics.htm";
		document.logistics.method = "get";
		document.logistics.submit();
	}
	
</script>

<div id="wrpr"> 
	<span class="clear"></span>
  	<div class="wrpr-cont relative">
	    <div id="slideBtn">
	    	<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
	    </div>
		<div id="bread-crumb">
			<ul>
				<li class="scrn-icon"><span class="icon-home">&nbsp;</span>
				</li>
				<li><a href="welcome.htm">Home</a>
				</li>
				<li class="last">Interactive Map</li>
			</ul>
		</div>
		<!--Breadcrum div ends--> 
		<span class="blue-brdr"></span> 
		<!--Content div starts-->
		<div class="content" id="login"> 
			<!--Left Menu div starts-->
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>
			<!--Left Menu div ends--> 
			<!--Content panel div starts-->
			<div class="cont-pnl split" id="equalHt">
				<div class="cont-block rt-brdr stretch">
				  	<div class="title-bar">
					    <ul class="title-actn">
					      <li class="title-icon"><span class="icon-map">&nbsp;</span></li>
					      <li>Interactive Map</li>
					    </ul>
				  	</div>
    				<div class="cont-wrp">
    					<form:form name="logistics" id="logistics" commandName="logistics" enctype="multipart/form-data">
							<form:hidden path="logisticId" />
							<form:hidden path="lowerLimit"/>
							<input type="hidden" name="pageNumber" />
							<input type="hidden" name="addMarker" />
							<input type="hidden" name="pageFlag" />
					      	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="zerobrdrTbl">
						        <tr>
						          	<td width="15%"><label>Map Name</label></td>
						          	<td width="35%">
						          		<div class="cntrl-grp">
						              		<form:input path="searchLogisticName" cssClass="inputTxtBig" onkeypress="searchLogisticsOnkeyPress(event)" tabindex="1"/>
						            	</div></td>
						          	<td width="10%">
						          		<a href="#" onclick="searchLogistics();" title="Search Interactive Maps" tabindex="2">
						          			<img src="images/searchIcon.png" width="20" height="17" alt="search" /></a>
						          	</td>
						          	<td width="40%" align="right">
						          		<input type="button" title="Add Interactive Map" value="Add Map" class="btn-blue" onclick="addLogistics();" tabindex="3"/>
						          	</td>
						        </tr>
					      	</table>
						</form:form>
						<c:if test="${requestScope.responseStatus ne null && !empty requestScope.responseStatus}">
							<c:choose>
								<c:when test="${requestScope.responseStatus eq 'INFO' }">
									<div class="alertBx warning mrgnTop cntrAlgn">
										<span class="actn-close" title="close"></span>
										<p class="msgBx">
											<c:out value="${requestScope.responeMsg}" />
										</p>
									</div>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${requestScope.responseStatus eq 'SUCCESS' }">
											<div class="alertBx success mrgnTop cntrAlgn">
												<span class="actn-close" title="close"></span>
												<p class="msgBx">
													<c:out value="${requestScope.responeMsg}" />
												</p>
											</div>
										</c:when>
										<c:otherwise>
											<div class="alertBx failure mrgnTop cntrAlgn">
												<span class="actn-close" title="close"></span>
												<p class="msgBx">
													<c:out value="${requestScope.responeMsg}" />
												</p>
											</div>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:if>
				      	<div class="relative">
				      		<c:if test="${sessionScope.logisticList ne null && !empty sessionScope.logisticList}">
					        	<div class="hdrClone"></div>
					        	<div class="scrollTbl tblHt mrgnTop">
	          						<table width="100%" cellspacing="0" cellpadding="0" border="0" id="mngevntTbl" class="grdTbl clone-hdr fxdhtTbl markrDtls">
							            <thead>
							              	<tr class="tblHdr">
								                <th width="18%">Map Name</th>
								                <th width="20%">Location</th>
								                <th width="20%">Map Link</th>
								                <th width="12%">Start Date</th>
								                <th width="12%">End Date</th>
								                <th width="12%">Action</th>
							             	</tr>
							            </thead>
	            						<tbody class="scrollContent">
	            							<c:forEach items="${sessionScope.logisticList}" var="logistic">
								              	<tr>
									                <td>${logistic.logisticName}</td>
									                <td>${logistic.location}</td>
									                <td style="word-break:break-all;">${logistic.mapLink}</td>
									                <td>${logistic.startDate}</td>
									                <td>${logistic.endDate}</td>
									                <td class="logistics">
									                	<a title="edit" href="#" title="Edit Interactive Map">
									                		<img height="20" width="20" class="actn-icon" alt="edit" src="images/edit_icon.png" onclick="editLogistics('${logistic.logisticId}')"/></a> 
									                	<a title="delete" href="#" title="Delete Interactive Map">
									                		<img height="20" width="20" class="actn-icon" alt="delete" src="images/delete_icon.png" onclick="deleteLogistics('${logistic.logisticId}')"/></a>
									                	<a title="add" href="#" title="Add Marker Details">
									                		<img height="20" width="20" class="actn-icon" alt="add" src="images/marker_icon.png" onclick="addMarkers('${logistic.logisticId}', 'Yes')"/></a>

									                </td>
								              	</tr>
							              	</c:forEach>
							            </tbody>
							        </table>
								</div>
	        					<div class="pagination mrgnTop">
									<page:pageTag currentPage="${sessionScope.pagination.currentPage}" nextPage="4" totalSize="${sessionScope.pagination.totalSize}" pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" />
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		<!--Content panel div ends--> 
		</div>
		<!--Content div ends--> 
	</div>
</div>
<script type="text/javascript">
	configureMenu("setupLogisticsMap");
</script>



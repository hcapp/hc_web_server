<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script>
    $(document).ready(function() {
        var groupedTabWithImage = '${sessionScope.menuTemplteName}';
        if (groupedTabWithImage === 'Grouped Tab With Image') {
            $("#dynTab").addClass("grpdImg");
        }
       var iconic3x3='${sessionScope.menuTemplteName}'
       var square3x3='${sessionScope.menuTemplteName}'
    	   if ((iconic3x3 === 'Iconic 3X3')||(square3x3 =='Square Grid  3x3')) {
    		   $(".gridView").css("padding","15px");
    		   $(".tabs").css("paddingLeft","10px");
    		   $(".tabs").css("paddingRight","10px");
           }
       
    });
</script>
<!--[if IE]>
<script type="text/javascript" src="scripts/jquery.corner.js"></script>
<![endif]-->
<div class="wrpr-cont relative">
	<div id="slideBtn">
		<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img src="images/slide_off.png" width="11" height="28" alt="btn_off" /> </a>
	</div>
	<div id="bread-crumb">
		<ul>
			<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
			<li>Home</li>
			<li class="last">Main Menu</li>
		</ul>
	</div>
	<span class="blue-brdr"></span>
	<div class="content" id="login">
		<div id="menu-pnl" class="split">
			<jsp:include page="leftNavigation.jsp"></jsp:include>
		</div>
		<div class="cont-pnl split" id="equalHt">
			<div class="cont-block rt-brdr stretch">
				<div class="title-bar">
					<ul class="title-actn">
						<li class="title-icon"><span class="icon-main-menu">&nbsp;</span></li>
						<li>Main Menu [ <c:out value="${sessionScope.menuTemplteName }"></c:out> ]
						</li>
					</ul>
				</div>
				<c:choose>
					<c:when test="${sessionScope.mainMenuDetails ne null}">
						<div class="cont-wrp">
							<div id="iphone-preview" class="brdr">
								<!--Iphone Preview For Login screen starts here-->
								<div id="iphone-priPolicy-preview">
									<div class="iphone-status-bar"></div>
									<c:choose>
										<c:when test="${sessionScope.menuTemplteName eq 'Scrolling News Template' or sessionScope.menuTemplteName eq 'Combination News Template' or sessionScope.menuTemplteName eq 'News Tile Template'}">
											<div class="navBar iphone">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
													<tr>
														<!-- <td width="19%"><div class="icon-main-menu"></div></td> -->
														<td width="24%"><img width="35" height="25" alt="back" src="images/hamburger.png" class="hamburger"></td>
														<td width="54%" class="genTitle"><img src="${sessionScope.newsBannerImage}" width="86" height="35" alt="Banner"/></td>
														<td width="27%" align="center"></td>
													</tr>
													
												</table>
											</div>
										</c:when>
										<c:otherwise>
											<div class="navBar iphone">
												<table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
													<tr>
														<td width="19%"><img src="images/backBtn.png" alt="back" width="50" height="30" /></td>
														<td width="54%" class="genTitle">
															<!--<img src="images/small-logo.png" width="86" height="35" alt="Logo" />-->
														</td>
														<td width="27%" align="center"></td>
													</tr>
												</table>
											</div>

										</c:otherwise>
									</c:choose>

									<c:choose>
										<c:when test="${sessionScope.menuTemplteName eq 'Grouped Tab' || sessionScope.menuTemplteName eq 'Grouped Tab With Image'}">
													<%-- <div id="bnrImg">
														<img id="imgView" src="${sessionScope.grpTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
													</div> --%>
												<div class="previewAreaScroll actualView">		
													<c:choose>
													<c:when test="${sessionScope.isBannerOrTicker ne null && sessionScope.isBannerOrTicker ne '' && sessionScope.isBannerOrTicker == 1}">
														<div id="bnrImg">
															<img id="imgView" src="${sessionScope.grpTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
														</div>
													</c:when>
													<c:otherwise>
														<c:if test="${sessionScope.isBannerOrTicker ne null}">
														<div class="mrq-wrper theme-a">
  											 			 <div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
 														 </c:if>
													</c:otherwise>
													</c:choose>
													<ul class="singleTab grpdLst cstmBtn" id="dynTab">
														<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
															<c:choose>
																<c:when test="${item.functnName eq 'Text' }">
																	<li class="tabs grpHdr Group1"><c:out value="${item.btnName }"></c:out></li>
																</c:when>
																<c:otherwise>
																	<li class="tabs"><a> <img width="30" height="30" class="lstImg" alt="image" src="${item.btnImage}"> <span><c:out
																					value='${item.btnName }'></c:out> </span>
																	</a></li>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</ul>
												</div>	
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Single/Two Column Tab'}">
											<div class="previewAreaScroll actualView">
												<c:choose>
													<c:when test="${sessionScope.colBtnView eq 'singleCol'}">
														<ul id="dynTab" class="twinTab cstmBtn singleCol">
													</c:when>
													<c:otherwise>
														<ul id="dynTab" class="twinTab cstmBtn twoCol">
													</c:otherwise>
												</c:choose>
												<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
													<li class="tabs"><c:choose>
															<c:when test="${item.btnAction eq sessionScope.expFctnId}">
																<a class="experience"><img class="lstImg" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>"> <span><c:out
																			value="${item.btnName }"></c:out> </span> </a>
															</c:when>
															<c:otherwise>
																<a><img width="30" height="30" class="lstImg" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>">
																	<span><c:out value="${item.btnName }"></c:out> </span> </a>
															</c:otherwise>
														</c:choose></li>
												</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Two Column Tab with Banner Ad'}">
											<div class="previewAreaScroll actualView">
												<c:choose>
													<c:when test="${sessionScope.isBannerOrTicker eq '1'}">
														<div id="bnrImg">
															<img id="imgView" src="${sessionScope.bannerimage}" title="Banner Ad" width="320" height="50" />
														</div>
													</c:when>
														<c:otherwise>
														<div class="mrq-wrper theme-a">
  											 			 <div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
													</c:otherwise>
													</c:choose>
													
												<ul id="dynTab" class="twinTabBnr cstmBtn">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><c:choose>
																<c:when test="${item.btnAction eq sessionScope.expFctnId}">
																	<a class="experience"><img class="lstImg" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>"> <span><c:out
																				value="${item.btnName }"></c:out> </span> </a>
																</c:when>
																<c:otherwise>
																	<a><img width="30" height="30" class="lstImg" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>">
																		<span><c:out value="${item.btnName }"></c:out> </span> </a>
																</c:otherwise>
															</c:choose></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'List View'}">
											<div class="previewAreaScroll actualView">
												<ul class="listView" id="dynTab">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a><img width="30" height="30" class="lstImg" alt="image"
																src="<c:out
																value='${item.btnImage }'></c:out>"> <span><c:out value="${item.btnName }"></c:out> </span> </a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'List View with Banner Ad'}">
											<div class="previewAreaScroll actualView">
												<%-- <div id="bnrImg">
													<img id="imgView" src="${sessionScope.listTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
												</div> --%>
												<c:choose>
													<c:when test="${sessionScope.isBannerOrTicker eq '1'}">
														<div id="bnrImg">
															<img id="imgView" src="${sessionScope.listTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
														</div>
													</c:when>
														<c:otherwise>
														<div class="mrq-wrper theme-a">
  											 			 <div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
													</c:otherwise>
													</c:choose>
												<ul class="listView" id="dynTab">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a><img width="30" height="30" class="lstImg" alt="image"
																src="<c:out
																value='${item.btnImage }'></c:out>"> <span><c:out value="${item.btnName }"></c:out> </span> </a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Iconic Grid' || sessionScope.menuTemplteName eq 'Square Grid'}">
											<div class="previewAreaScroll actualView">
												<%-- <div id="bnrImg">
													<img id="imgView" src="${sessionScope.iconicTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
												</div> --%>
												<c:choose>
													<c:when test="${sessionScope.isBannerOrTicker eq '1'}">
														<div id="bnrImg">
															<img id="imgView" src="${sessionScope.iconicTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
														</div>
													</c:when>
														<c:otherwise>
														<div class="mrq-wrper theme-a">
  											 			 <div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
													</c:otherwise>
													</c:choose>
												<ul class="gridView" id="dynTab">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a>
																<div class="rnd">
																	<img width="60" height="60" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>" />
																</div> <span><c:out value="${item.btnName }"></c:out> </span>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Combo Template'}">
											<div class="previewAreaScroll actualView">
												<ul class="singleTab cstmBtn" id="dynTab">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<c:choose>
															<c:when test="${item.functnName eq 'Text' ||  item.functnName eq 'Label'}">
																<li class="tabs grpHdr ${item.functnName}"><c:out value="${item.btnName }"></c:out></li>
															</c:when>
															<c:otherwise>
																<li class="tabs ${item.comboBtnType}"><a class="${item.comboBtnType}"> <c:choose>
																			<c:when test="${item.comboBtnType ne 'Rectangle'}">
																				<c:choose>
																					<c:when test="${item.comboBtnType eq 'Circle'}">
																						<div class="rnd">
																							<c:choose>
																								<c:when test="${item.btnImage ne 'null' || !empty item.btnImage}">
																									<img width="60" height="60" alt="image" src="<c:out value='${item.btnImage }'></c:out>" />
																								</c:when>
																								<c:otherwise>
																									<img width="60" height="60" alt="image" src="images/uploadIcon.png" />
																								</c:otherwise>
																							</c:choose>
																						</div>
																						<span><c:out value='${item.btnName }'></c:out> </span>
																					</c:when>
																					<c:otherwise>
																						<div class="">
																							<c:choose>
																								<c:when test="${item.btnImage ne 'null' || !empty item.btnImage}">
																									<img width="30" height="30" alt="image" src="<c:out value='${item.btnImage }'></c:out>" />
																								</c:when>
																								<c:otherwise>
																									<img width="30" height="30" alt="image" src="images/dfltImg.png" />
																								</c:otherwise>
																							</c:choose>
																						</div>
																						<span><c:out value='${item.btnName }'></c:out> </span>
																					</c:otherwise>
																				</c:choose>
																			</c:when>
																			<c:otherwise>
																				<div class="">
																					<img width="50" height="50" class="lstImg" alt="image" src="<c:out value='${item.btnImage }'></c:out>">
																				</div>
																				<span><c:out value='${item.btnName }'></c:out> </span>
																			</c:otherwise>
																		</c:choose>
																</a></li>
															</c:otherwise>
														</c:choose>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Rectangular Grid'}">
											<div class="previewAreaScroll gnrlScrn">
												<c:choose>
												<c:when test="${sessionScope.isBannerOrTicker eq '1'}">
												<div id="bnrImg">
													<img id="imgView" src="${sessionScope.rectTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
												</div>
												</c:when>
														<c:otherwise>
														<div class="mrq-wrper theme-a">
  											 			 <div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
													</c:otherwise>
													</c:choose>
												<div id="preview_ie" title="Banner Ad"></div>
												<ul id="dynTab" class="gridView sortable rect">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a class="rectang">
																<div>
																	<img class="sqr" width="73" height="100" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>">
																</div>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq '4X4 Grid'}">
											<div class="previewAreaScroll gnrlScrn">
											<c:choose>
												<c:when test="${sessionScope.isBannerOrTicker eq '1'}">
												<div id="bnrImg">
													<img id="imgView" src="${sessionScope.rectTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
												</div>
												</c:when>
												<c:otherwise>
													<div class="mrq-wrper theme-a">
  											 			<div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
												</c:otherwise>
											</c:choose>
												<div id="preview_ie" title="Banner Ad"></div>
												<ul id="dynTab" class="gridView sortable rect">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a class="rectang rect4X4">
																<div>
																	<img class="sqr" width="77" height="77" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>">
																</div>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Two Image Template'}">
											<div class="previewAreaScroll gnrlScrn twoImagePnl">
											<c:choose>
												<c:when test="${sessionScope.isBannerOrTicker eq '1'}">
												<div id="bnrImg">
													<img id="imgView" src="${sessionScope.twoImgTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
												</div>
												</c:when>
												<c:otherwise>
													<div class="mrq-wrper theme-a">
  											 			<div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
												</c:otherwise>
											</c:choose>
												<div id="preview_ie" title="Banner Ad"></div>
												<ul id="dynTab" class="gridView sortable twoImage">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a class="rectang">
																<div>
																	<img class="sqr" width="125" height="128" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>">
																</div>
														</a><span class="displayLabel"><c:out value='${item.btnName }'></c:out> </span></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Square Image Template'}">
											<div class="previewAreaScroll gnrlScrn">
											<c:choose>
												<c:when test="${sessionScope.isBannerOrTicker eq '1'}">
												<div id="bnrImg">
													<img id="imgView" src="${sessionScope.twoImgTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
												</div>
												</c:when>
												<c:otherwise>
													<div class="mrq-wrper theme-a">
  											 			<div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
												</c:otherwise>
											</c:choose>
												<div id="preview_ie" title="Banner Ad"></div>
												<ul id="dynTab" class="gridView sortable">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs sqr4Plus"><a>
																<div>
																	<img width="77" height="77" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>">
																</div> <span class="displayLabel"><c:out value='${item.btnName }'></c:out> </span>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Four Tile Template'}">
											<div class="previewAreaScroll gnrlScrn">
												<ul id="dynTab" class="gridView sortable fourTile">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a>
																<div>
																	<img width="320" height="92" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>">
																</div> <span><label class="displayLabel"><c:out value='${item.btnName}'></c:out></label></span>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Two Tile Template'}">
											<div class="previewAreaScroll gnrlScrn">
												<ul id="dynTab" class="gridView sortable twoTile">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a>
																<div>
																	<img width="245" height="150" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>">
																</div> <span class="displayLabel"><c:out value='${item.btnName }'></c:out> </span>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Scrolling News Template'}">
											<div class="previewAreaScroll gnrlScrn scrolcls">
												<ul id="dynTab" class="sortable scrollingNews ui-sortable">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a> <span class="displayLabel"><c:out value='${item.btnName }'></c:out> </span>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'Combination News Template'}">
											<div class="previewAreaScroll gnrlScrn combcls">
												<ul id="dynTab" class="sortable scrollingNews ui-sortable">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a> <span class="displayLabel"><c:out value='${item.btnName }'></c:out> </span>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										<c:when test="${sessionScope.menuTemplteName eq 'News Tile Template'}">
											<div class="previewAreaScroll gnrlScrn tilecls">
												<ul id="dynTab" class="sortable scrollingNews ui-sortable">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li class="tabs"><a> <span class="displayLabel"><c:out value='${item.btnName }'></c:out> </span>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>
										
										<c:when test="${sessionScope.menuTemplteName eq 'Iconic 3X3' || sessionScope.menuTemplteName eq 'Square Grid  3x3'}">
											<div class="previewAreaScroll actualView">
												<%-- <div id="bnrImg">
													<img id="imgView" src="${sessionScope.iconicTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
												</div> --%>
												
												<c:choose>
													<c:when test="${sessionScope.isBannerOrTicker eq '1'}">
														<div id="bnrImg">
															<img id="imgView" src="${sessionScope.iconicTempbnrimgPreview}" title="Banner Ad" width="320" height="50" />
														</div>
													</c:when>
														<c:otherwise>
														<div class="mrq-wrper theme-a">
  											 			 <div class="mrq-title col">Latest News</div>
  												 			<marquee direction="${sessionScope.direction}" scrolldelay="100" class="col">
   																<div class="m-cont"><span>News Ticker Story 1</span><span>News Ticker Story 2</span><span>News Ticker Story 3</span><span>News Ticker Story 4</span><span>News Ticker Story 5</span></div>
   															</marquee>
 														 </div>
													</c:otherwise>
												</c:choose>
												<ul  class="gridView" id="dynTab">
													<c:forEach items="${sessionScope.mainMenuDetails.buttons }" var="item">
														<li  class="tabs"><a>
																<div class="rnd">
																	<img width="60" height="60" alt="image" src="<c:out
																value='${item.btnImage }'></c:out>" />
																</div> <span><c:out value="${item.btnName }"></c:out> </span>
														</a></li>
													</c:forEach>
												</ul>
											</div>
										</c:when>

									</c:choose>
								</div>
								<!--Iphone Preview For Login screen ends here-->
								<c:choose>
									<c:when test="${sessionScope.menuTemplteName eq 'Scrolling News Template' or sessionScope.menuTemplteName eq 'Combination News Template' or sessionScope.menuTemplteName eq 'News Tile Template'}">
									</c:when>
									<c:otherwise>
										<ul id="tab-main" class="tabbar">
											<c:forEach items="${sessionScope.menuBarButtonsList }" var="item">
												<li><img src="${item.imagePath }" width="80" height="50" alt="share" /></li>
											</c:forEach>
										</ul>
									</c:otherwise>
								</c:choose>
							</div>
						</div>

						<div class="title-bar" align="center">
							<a href="updatemainmenu.htm?menuType=mainmenu" class="btn-blue"><c:out value="Update Main Menu">
								</c:out> </a>
						</div>
					</c:when>
					<c:otherwise>
						<div class="title-bar" align="center">
							<a href="displaymenutemplate.htm?menuType=mainmenu" class="btn-blue"><c:out value="Add Main Menu">
								</c:out> </a>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
			<!-- 
			<c:if test="${sessionScope.mainMenuDetails ne null}">
				<div class="cont-block">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-submenu">&nbsp;</span>
							</li>
							<li>Sub Menu</li>
						</ul>
					</div>

					<div class="cont-wrp tmpltHt">
						<ul class="listCntrl relative">

							<c:forEach items="${sessionScope.subMenuList }" var="item">

								<li><a href="updatesubmenu.htm?subMenuId=${item.menuId }" title="Edit SubMenu"> <c:out value="${item.menuName }"></c:out>
								</a> <span class="actnIcon"> <a title="Edit SubMenu" class="actn-edit" href="updatesubmenu.htm?subMenuId=${item.menuId }"></a> <a title="Delete SubMenu" class="actn-del" href="#" onclick="deleteSubMenu(${item.menuId })"></a> </span></li>

							</c:forEach>

						</ul>
					</div>
					<div class="title-bar" align="center">

						<input type="submit" name="SubMnu" value="Add Sub Menu" class="btn-blue" id="addSubMnu" onclick="window.location.href='displaymenutemplate.htm?menuType=submenu'" />
					</div>
				</div>
			</c:if>
 -->
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    	
    	

        var mainMenuBGType = '${sessionScope.mainMenuBGType}';
        var mainMenuBG = '${sessionScope.mainMenuBG}';
        var mainMenuBtnClr = '${sessionScope.mainMenuBtnClr}';
        var mainMenuFntClr = '${sessionScope.mainMenuFntClr}';
        var menuTemplteName = '${sessionScope.menuTemplteName}';
        var mainMenuGrpClr = '${sessionScope.mainMenuGrpClr}';
        var mainMenuGrpFntClr = '${sessionScope.mainMenuGrpFntClr}';
        var mainMenuIconsFntClr = '${sessionScope.mainMenuIconsFntClr}';
        var tickerTextClr = "${sessionScope.newsFeedTxt}";
		var tickerBgrdClr = "${sessionScope.newsBackground}";
		
		
		if(tickerTextClr == ""){
			$(".m-cont span").css("color", "#808080");
		} else {
			$(".m-cont span").css("color", tickerTextClr);
		}
		
		if(tickerBgrdClr == ""){
			$(".theme-a").css("background", "#000000");
		} else {
			$(".theme-a").css("background", tickerBgrdClr);
		}

		var direction = $("marquee").attr("direction");
    	if(direction == "Up" || direction == "Down"){
   	 		$("marquee div").removeClass("m-cont");
    		$("marquee div").addClass("v-cont");
    	}
    	
        if (menuTemplteName == "Combo Template") {
            $("#dynTab").removeClass("singleTab cstmBtn").addClass("comboView");
            if ($('.actualView').hasScrollBar()) {
                $(".comboView li.tabs a.Square").removeClass("scrlOff").addClass("scrlOn");
            } else {
                $(".comboView li.tabs a.Square").removeClass("scrlOn").addClass("scrlOff");
            }

            $('.comboView li.tabs a.Circle span').each(function() {
                var val = $.trim($(this).text());
                if (/\s/.test(val)) {
                    $(this).css("word-break", "keeep-all");
                } else {
                    $(this).css("word-break", "break-all");
                }
            });
        }

        if (mainMenuBGType == 'Image') {
            $(".previewAreaScroll").css("background", "url('" + mainMenuBG + "') no-repeat scroll 0 0");

        } else if (mainMenuBGType == 'Color') {
            $("#iphone-priPolicy-preview").css("background", mainMenuBG);
        }
		
        if (menuTemplteName == 'Iconic Grid' || menuTemplteName == 'Square Grid' || menuTemplteName == 'Square Grid  3x3' || menuTemplteName == 'Iconic 3X3') {
		
            $(".gridView li.tabs a span").css("color", mainMenuIconsFntClr);

            $('.gridView li.tabs a span').each(function() {
                var val = $.trim($(this).text());
                if (/\s/.test(val)) {
                    $(this).css("word-break", "keeep-all");
                } else {
                    $(this).css("word-break", "break-all");
                }

                if ($('.actualView').hasScrollBar()) {
                    $(".gridView li.tabs a").removeClass("scrlOff");
                } else {
                    $(".gridView li.tabs a").addClass("scrlOff");
                }

                if (menuTemplteName == 'Square Grid' || menuTemplteName == 'Square Grid  3x3') {
                    $("#dynTab li").each(function() {
                        var chngDiv = $(this).find("div");
                        $(chngDiv).removeClass("rnd");
                        $(chngDiv).find("img").addClass("sqr");
                    });
                }

            });
        } else if (menuTemplteName == 'Grouped Tab' || menuTemplteName == 'Grouped Tab With Image') {

            $(".singleTab li.tabs a").css("color", mainMenuFntClr);
            $(".singleTab").css("background", mainMenuBtnClr);
            $("#dynTab li.grpHdr").css("background", mainMenuGrpClr);
            $("#dynTab li.grpHdr").css("color", mainMenuGrpFntClr);

        } else if (menuTemplteName == 'List View' || menuTemplteName == 'List View with Banner Ad') {

            $(".listView li.tabs a").css("color", mainMenuFntClr)
            $(".listView li.tabs").css("background", mainMenuBtnClr)
        } else if (menuTemplteName == 'Two Column Tab with Banner Ad' || menuTemplteName == 'Single/Two Column Tab') {

            if ($('.actualView').hasScrollBar()) {

                $(".twinTab li.tabs a,.twinTabBnr li.tabs a").css("width", "150px");
                $(".twinTab li.tabs a.experience,.twinTabBnr li.tabs a.experience").css("width", "301px");
                $(".previewAreaScroll").css("width", "319px");
                $("#iphone-preview").css("width", "319px");

            } else {
                $(".twinTab li.tabs a,.twinTabBnr li.tabs a").css("width", "159px");
                $(".twinTab li.tabs a.experience,.twinTabBnr li.tabs a.experience").css("width", "320px");
                $(".previewAreaScroll").css("width", "320px");
                $("#iphone-preview").css("width", "320px");
            }

            $(".twinTab li.tabs a, .twinTabBnr li.tabs a").css("color", mainMenuFntClr)
            $(".twinTab li.tabs a, .twinTabBnr li.tabs a").css("background", mainMenuBtnClr)
        } else if (menuTemplteName == "Combo Template") {

            $(".comboView li.tabs a").css("color", mainMenuFntClr);
            $(".comboView li.tabs.Circle a span").css("color", mainMenuIconsFntClr);
            $(".comboView li.tabs.Square a span").css("color", mainMenuIconsFntClr);
            $(".comboView li.tabs.Rectangle").css("background", mainMenuBtnClr);
            $("#dynTab li.grpHdr.Text").css("background", mainMenuGrpClr);
            $("#dynTab li.grpHdr").css("color", mainMenuGrpFntClr);

            if ($('.actualView').hasScrollBar()) {
                $(".comboView li.tabs a.Square").removeClass("scrlOff").addClass("scrlOn");
                $(".comboView li.tabs a.Circle").removeClass("scrlOff").addClass("scrlOn");
            } else {
                $(".comboView li.tabs a.Square").removeClass("scrlOn").addClass("scrlOff");
                $(".comboView li.tabs a.Circle").removeClass("scrlOn").addClass("scrlOff");
            }
        } else if (menuTemplteName == "Rectangular Grid") {
            var bgColor = '${sessionScope.tmplBckgrnColor}';
            $(".gnrlScrn").css("background", bgColor);
            if ($('.gnrlScrn').hasScrollBar()) {
                $(".gridView li.tabs a").removeClass("rectScrlOff");
            } else {
                $(".gridView li.tabs a").addClass("rectScrlOff");
            }
        } else if (menuTemplteName == "4X4 Grid") {
            var bgColor = '${sessionScope.tmplBckgrnColor}';
            $(".gnrlScrn").css("background", bgColor);
            if ($('.gnrlScrn').hasScrollBar()) {
                $(".gridView li.tabs a").removeClass("rect4X4ScrlOff");
            } else {
                $(".gridView li.tabs a").addClass("rect4X4ScrlOff");
            }
        } else if (menuTemplteName == "Two Image Template") {
            var bgImage = '${sessionScope.twoImgBckGrdPreview}';

            if (bgImage != "images/uploadIconSqr.png") {
                $(".twoImagePnl").css("background", "url('" + bgImage + "') no-repeat scroll 0 0");
            }

            var displayLabel = '${sessionScope.displayLabel}';

            if (displayLabel == "true") {
                $(".displayLabel").show();
                $(".displayLabel").css("color", mainMenuFntClr);
            } else {
                $(".displayLabel").hide();
            }
        } else if (menuTemplteName == "Square Image Template") {
            var bgImage = '${sessionScope.twoImgBckGrdPreview}';

            if (bgImage != "images/uploadIconSqr.png") {
                $(".gnrlScrn").css("background", "url('" + bgImage + "') no-repeat scroll 0 0");
            }

            var displayLabel = '${sessionScope.displayLabel}';

            if (displayLabel == "true") {
                $(".displayLabel").show();
                $(".displayLabel").css("color", mainMenuFntClr);
            } else {
                $(".displayLabel").hide();
            }
        } else if (menuTemplteName == "Four Tile Template") {

            var displayLabel = '${sessionScope.displayLabel}';
            if (displayLabel == "true") {
                $(".displayLabel").show();
                $(".displayLabel").css("color", mainMenuFntClr);
            } else {
                $(".displayLabel").hide();
            }

            var btnColor = '${sessionScope.labelBckGndColor}';
            var btnFontColor = '${sessionScope.labelFontColor}';
            $(".gridView.fourTile li a span").css("background", hex2rgba(btnColor));
            $(".gridView.fourTile li a span label").css("color", btnFontColor);
            $(".gnrlScrn").css("background", "");
        } else if (menuTemplteName == "Two Tile Template") {

            var displayLabel = '${sessionScope.displayLabel}';
            if (displayLabel == "true") {
                $(".displayLabel").show();
                $(".displayLabel").css("color", mainMenuFntClr);
            } else {
                $(".displayLabel").hide();
            }

            var btnFontColor = '${sessionScope.labelFontColor}';
            $(".gridView.twoTile li a span").css("color", btnFontColor);

            var twoTileBckGrdPreview = '${sessionScope.twoTileBckGrdPreview}';

            if (null != twoTileBckGrdPreview && '' != twoTileBckGrdPreview) {
                $(".gnrlScrn").css("background", "url('" + twoTileBckGrdPreview + "') no-repeat scroll 0 0");
            } else {
                if (mainMenuBGType == 'Image') {
                    $(".gnrlScrn").css("background", "url('" + mainMenuBG + "') no-repeat scroll 0 0");
                } else if (mainMenuBGType == 'Color') {
                    $(".gnrlScrn").css("background", mainMenuBG);
                }
            }
        }

    });

    $(window).load(function() {

        if ($("#dynTab li").length > 1) {
            $("#dynTab li a.experience").parents('li').insertBefore("#dynTab li.tabs:first");
        }
        //$(".rnd").corner("25px");

    });

    (function($) {
        $.fn.hasScrollBar = function() {
            return this.get(0).scrollHeight > this.height();
        }
    })(jQuery);

    (function($) {
        $.fn.hasScrollBar = function() {
            return this.get(0).scrollHeight > this.height();
        }
    })(jQuery);
</script>
<script type="text/javascript">
    configureMenu("setupmainmenu");
</script>

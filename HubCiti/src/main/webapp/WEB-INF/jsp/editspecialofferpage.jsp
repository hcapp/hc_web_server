<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-min.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script src="/HubCiti/scripts/ckeditor/ckeditor.js"></script>
<script src="/HubCiti/scripts/ckeditor/config.js"></script>
<script type="text/javascript" src="scripts/web.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />

<script type="text/javascript">
	$(document).ready(function() {

		$("input[name='iconSelect']:radio").change(function() {
			var slctOptn = $(this).attr("id");
			$(".cmnUpld").hide();
			$("." + slctOptn).slideDown();

			if (slctOptn == "exstngIcon") {
				$(".TglLbl").text("Select Icons");
			}
			if (slctOptn == "upldOwn") {
				$(".TglLbl").text("Upload Icon");
			}
		});

		var existingretId = $('#retailerId').val();
		var selectedLocIds = $('#tempretids').val();
		
		if (existingretId != 'undefined') {
			if (selectedLocIds == null) {
				getLogRetailerLocs(existingretId, "");
			} else {
				getLogRetailerLocs(existingretId, selectedLocIds)
			}
		}
		
		$(".exstngIcon").show();
		$(".TglLbl").text("Select Icons");

		/*To Set equal heights for the divs pass div class names as parameter*/
		setequalHeight($("#equalHt .cont-block"));
		setequalHeight($(".cntrl-grp input"));
		$("#menu-pnl").height($(".content").height());

		$("input:radio[name=iconSelect]").click(function() {
			var value = $(this).val();
			document.screenSettingsForm.iconSelectHid.value = value;
		});

		$('#retailerLocIds option').click(function() {
			var totOpt = $('#retailerLocIds option').length;
			var totOptSlctd = $('#retailerLocIds option:selected').length;
			if (totOpt == totOptSlctd) {
				$('#chkAllLoc').prop('checked', 'checked');
			} else {
				$('#chkAllLoc').removeAttr('checked');
			}
		});
		$("#retailerLocIds").change(function() {
			var totOpt = $('#retailerLocIds option').length;
			var totOptSlctd = $('#retailerLocIds option:selected').length;
			if (totOpt == totOptSlctd) {
				$('#chkAllLoc').prop('checked', 'checked');
			} else {
				$('#chkAllLoc').removeAttr('checked');
			}
		});
	});

	window.onload = function() {
		var vIcnType = $("input[name='iconSelectHid']").val();
		var iconSelect = document.screenSettingsForm.iconSelectHid.value;

		if (vIcnType.match(/(\icnSlctn)$/)) {
			$(".TglLbl").text("Upload Icon");
			$('#upldOwn').attr('checked', true);
			$(".cmnUpld").hide();
			$(".upldOwn").show();
		}

		if (iconSelect == "icnSlctn") {
			$('#upldOwn').attr("checked", true);
			$(".TglLbl").text("Upload Icon");
		} else {
			$('#exstngIcon').attr("checked", true);
			$(".TglLbl").text("Select Icons");
		}

	}

	function logRetNameAutocomplete(retaName) {
		$("#retailerName").autocomplete({
			minLength : 3,
			delay : 200,
			source : '/HubCiti/displaylogretnames.htm',
			select : function(event, ui) {

				if (ui.item.value == "No Records Found") {
					$("#retailerName").val("");
				} else {
					$("#retailerName").val(ui.item.rname);
					$("#retailerId").val(ui.item.retId);
					getLogRetailerLocs(ui.item.retId, "");
				}
				return false;
			}
		});
	}

	function getLogRetailerLocs(retId, selectedLocId) {

		$('#retailerLocIds').find('option').remove();

		$.ajaxSetup({
			cache : false
		});

		$.ajax({
			type : "GET",
			url : "displaylogretLoc.htm",
			data : {
				'retId' : retId

			},

			success : function(response) {

				var rLocations = response;

				var objs = JSON.parse(rLocations);

				slctbox = $('#retailerLocIds');
				$('#retailerLocIds').find('option:not(:first)').remove();
				if (objs != null && objs != 'undefined') {
					for (var i = 0; i < objs.length; i++) {
						slctbox.append(new Option(objs[i].address,
								objs[i].retLocId));
					}

					if ("" !== selectedLocId) {

						var arr = selectedLocId.split(',');
						jQuery.each(arr, function(i, val) {
							$("#retailerLocIds option[value='" + val + "']")
									.prop('selected', 'selected');
						});
						
						var totOpt = $('#retailerLocIds option').length;
						var totOptSlctd = $('#retailerLocIds option:selected').length;
						if (totOpt == totOptSlctd) {
							$('#chkAllLoc').prop('checked', 'checked');
						} else {
							$('#chkAllLoc').removeAttr('checked');
						}
					}

				}
			},
			error : function(e) {
				alert('Error occured while fetching retailer locations');
			}
		});

	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("retailerLocIds");
		if (checked == true) {
			for (var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for (var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}

	function setequalHeight(obj) {
		tallest = 0;
		obj.each(function() {
			curHeight = $(this).height();
			if (curHeight > tallest) {
				tallest = curHeight;
			}
		});
		obj.height(tallest);
	}

	function saveImage(imageIconID) {
		document.screenSettingsForm.imageIconID.value = imageIconID;
	}

	function updateSpecialOfferPage() {

		document.screenSettingsForm.pathName.value = document.screenSettingsForm.pathName.value;
		document.screenSettingsForm.action = "updatespecialofferscreen.htm";
		document.screenSettingsForm.method = "POST";
		document.screenSettingsForm.submit();
	}
</script>

<div id="wrpr">
	<div class="clear"></div>
	<div class="wrpr-cont relative">
		<div id="slideBtn">
			<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
				src="images/slide_off.png" width="11" height="28" alt="btn_off" />
			</a>
		</div>
		<div id="bread-crumb">
			<ul>
				<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
				<li><a href="welcome.htm">Home</a></li>
				<li><a href="displayspecialofferpages.htm">Special Offer
						Page</a></li>
				<li class="last">Edit Special Offer Page</li>
			</ul>
		</div>
		<span class="blue-brdr"></span>
		<div class="content" id="login">
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>
			<div class="cont-pnl split" id="equalHt">
				<div class="cont-block rt-brdr">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-welcome">&nbsp;</span></li>
							<li>Edit Special Offer Page</li>
						</ul>
					</div>
					<div class="cont-wrphidoverflow">
						<form:form name="screenSettingsForm" id="screenSettingsForm"
							commandName="screenSettingsForm" enctype="multipart/form-data"
							action="uploadimg.htm">
							<form:hidden path="viewName" value="editspecialofferpage" />
							<form:hidden path="iconSelectHid" />
							<form:hidden path="imageIconID" />
							<form:hidden path="retailerId" />
							<form:hidden path="tempretids" />
							<form:hidden path="hiddenLocationId" />
							<form:hidden path="oldImageName" />
							<form:hidden path="hiddenBtnLinkId"/>
							<form:hidden path="pageId"/>
							
							<form:hidden path="pathName" value="${sessionScope.uploadedFile}" />
							<table width="100%" border="0" cellpadding="0" cellspacing="0"
								class="cmnTbl">
								<tr>
									<td width="42%"><label class="mand">Page Title</label></td>
									<td width="60%"><form:errors cssClass="errorDsply"
											path="pageTitle" />
										<div class="cntrl-grp">
											<form:input path="pageTitle" type="text" id="pageTitle" tabindex="1"
												cssClass="inputTxtBig" maxlength="25" />
										</div></td>
								</tr>


								<tr>
									<td rowspan="2" align="left" valign="top">Icon Selection</td>
									<td><form:errors cssClass="errorDsply" path="logoImage" />
										<form:input path="iconSelect" id="exstngIcon"
											value="exstngIcon" type="radio" name="iconSelect" tabindex="2"/> <label
										for="exstngIcon">Existing Icon</label></td>
								</tr>
								<tr>
									<td><form:input path="iconSelect" id="upldOwn" tabindex="2"
											value="icnSlctn" type="radio" name="iconSelect" /> <label
										for="upldOwn">Upload Icon</label></td>
								</tr>

								<tr id="webLink" class="cmnOptn">
									<td width="42%"><label class="mand">Web Link</label></td>
									<td width="60%"><form:errors cssClass="errorDsply"
											path="pageAttachLink" />
										<div class="cntrl-grp">
											<form:input path="pageAttachLink" type="text" tabindex="4"
												id="pageAttachLink" class="inputTxtBig" />
										</div></td>
								</tr>
								<tr id="fileUpld">
								</tr>
								<tr>
									<td><label class="mand">Retailer Name</label></td>
									<td><form:errors cssClass="errorDsply" path="retailerName"></form:errors>
										<div class="cntrl-grp">
											<form:input path="retailerName" class="loadingInput" tabindex="5"
												onkeypress='logRetNameAutocomplete(retailerName);'
												maxlength="30" />
										</div></td>

								</tr>
								<tr>
									<td><label class="mand">Locations</label></td>
									<td><form:errors path="retailerLocIds"
											cssStyle="color:red">
										</form:errors>
										<div class="cntrl-grp">
											<form:select id="retailerLocIds" path="retailerLocIds" tabindex="6"
												 class="slctBx textareaTxt" multiple="multiple">
											</form:select>
											<br />

										</div> <form:label path="retailerLocIds">Hold Ctrl to select more than one location</form:label>
									</td>
								</tr>
								<tr>
									<td></td>
									<td width="50%" align="left" valign="top" class="Label"><label>
											<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
											tabindex="7"
											onclick="SelectAllLocation(this.checked);" /> Select All
											Locations
									</label></td>
								</tr>
								<tr>
									<td valign="top"><label>Keywords</label></td>
									<td><form:errors cssClass="errorDsply" path="keywords"></form:errors>
										<div class="cntrl-grp">

											<form:textarea path="keywords" class="textareaTxt" tabindex="8"
												></form:textarea>
										</div></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><input type="submit" name="button" id="button"
										value="Update" title="Update" class="btn-blue" tabindex="10"
										onclick="updateSpecialOfferPage();" /></td>
								</tr>
							</table>
					</div>

				</div>
				<div class="cont-block">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-iphone">&nbsp;</span></li>
							<li class="TglLbl">Select Icons</li>
						</ul>
					</div>
					<div class="cont-wrp grey-bg mnHt">
						<ul class="iconsPnl cmnUpld exstngIcon">
							<c:forEach items="${sessionScope.arHcImageList}" var="s">
								<c:choose>
									<c:when test="${sessionScope.ImageIcon eq s.customPageIconID}">
										<li class="active"><a href="#"> <img
												src="${s.imagePath}" alt="hcImages"
												id="${s.customPageIconID}" name="retPageImg"
												onclick="javascript:saveImage('${s.customPageIconID}');" />
										</a></li>
									</c:when>
									<c:otherwise>
										<li class=""><a href="#"> <img src="${s.imagePath}"
												alt="hcImages" id="${s.customPageIconID}" name="retPageImg"
												onclick="javascript:saveImage('${s.customPageIconID}');" />
										</a></li>
									</c:otherwise>
								</c:choose>

							</c:forEach>
						</ul>
						<span class="clear"></span>
						<div class="cmnUpld upldOwn">
							<table width="100%" border="0" cellpadding="0" cellspacing="0"
								class="cmnTbl">

								<tr id="fileUpld">
									<td width="33%"><label class="mand">Upload Icon</label></td>
									<td width="64%"><form:errors cssClass="errorDsply"
											path="logoImageName"></form:errors> <label> <img
											id="uploadImg" width="50" height="50" alt="upload"
											src="${sessionScope.specialofferScreenImage }">
									</label> <span class="topPadding cmnRow"> <label for="trgrUpld">
												<input type="button" value="Upload" id="trgrUpldBtn" 
												class="btn trgrUpld" title="Upload Image File"> <form:hidden
													path="logoImageName" id="logoImageName" /> <span
												class="instTxt nonBlk"></span> <form:input type="file" tabindex="3"
													class="textboxBig" id="trgrUpld" path="logoImage"
													onchange="checkBannerSize(this);" />
										</label>
									</span> <label id="maxSizeImageError" class="errorDsply maxSizeImageError"></label></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				</form:form>
			</div>
		</div>
	</div>
</div>


<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
	<div class="headerIframe">
		<img src="/HubCiti/images/popupClose.png" class="closeIframe"
			alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
			title="Click here to Close" align="middle" /> <span id="popupHeader"></span>
	</div>
	<iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%"
		allowtransparency="yes" width="100%" style="background-color: White">
	</iframe>
</div>

</div>

<script type="text/javascript">
	configureMenu("setupspecialoffer");
</script>
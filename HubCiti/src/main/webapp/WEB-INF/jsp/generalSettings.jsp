<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>
<script src="/HubCiti/scripts/ckeditor/ckeditor.js"></script>
<script src="/HubCiti/scripts/ckeditor/config.js"></script>
<script src="/HubCiti/scripts/global.js"></script>

<script type="text/javascript">
	function selectCategory(obj) {

		var imgSrc = $(obj).find('img').attr('src');
		var catId = $(obj).find('span').attr('catImgId');
		$("#catImgLogo").attr('src', imgSrc);
		$("#catDetails").val(catId);
		$(".errorDsply").remove();
		//$('#catDetails option[value="'+catId+'"]:selected');
		if (catId === '0') {
			$("#categoryImg").hide();
			$("#saveImgUpload").hide()
			$("#msgSelectCat").show();
		} else {
			$("#categoryImg").show();
			$("#saveImgUpload").show();
			$("#msgSelectCat").hide();
		}

		document.screenSettingsForm.catImgName.value = '';
		updateImgCropSize('clearSessionImg');
	}

	function claimTypeChange(type) {
		if (type == "image") {
			$('.claimImage-js').show();
			$('.claimText-js').hide();
		} else {
			$('.claimImage-js').hide();
			$('.claimText-js').show();
		}
	}
	function updateImgCropSize(type) {
		$.ajaxSetup({
			cache : false
		})
		$.ajax({
			type : "GET",
			url : "updateimgcropsize.htm",
			data : {
				"uploadtype" : type,
			},
			success : function(response) {
				/* if (response != null && response != "" && response != "CategoryExists") {					
					$(".modal-popupWrp").hide();
					alert("Category Updated Successfully");
					window.location.href = "/HubCiti/displayfaqcat.htm";
				} else {
					$('p.dupctgry').css("display", "block");
				} */
			},
			error : function(e) {
				alert("Error occured while updating.");
			}
		});
	}
</script>
<link rel="stylesheet" type="text/css"
	href="/HubCiti/styles/colorPicker.css" />

<div id="wrpr">
	<div class="clear"></div>

	<div class="wrpr-cont relative">
		<div id="slideBtn">
			<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
				src="images/slide_off.png" width="11" height="28" alt="btn_off" />
			</a>
		</div>
		<div id="bread-crumb">
			<ul>
				<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
				<li><a href="welcome.htm">Home</a></li>
				<li class="last">General Settings</li>
			</ul>
		</div>
		<span class="blue-brdr"></span>
		<div class="content" id="login">
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>

			</div>
			<div class="cont-pnl split" id="equalHt">
				<div class="cont-block rt-brdr">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-settings">&nbsp;</span></li>
							<li>General Settings</li>
						</ul>
					</div>
					<ul class="tabd-nav">
						<li><c:choose>
								<c:when test="${sessionScope.menuType eq 'MainMenu'}">
									<a href="#" class="active"
										onclick="displayGeneralSettings('MainMenu')">Main-Menu</a>
								</c:when>
								<c:otherwise>
									<a href="#" onclick="displayGeneralSettings('MainMenu')">Main-Menu</a>
								</c:otherwise>
							</c:choose></li>
						<li><c:choose>
								<c:when test="${sessionScope.menuType eq 'SubMenu'}">
									<a href="#" class="active"
										onclick="displayGeneralSettings('SubMenu')">Sub-Menu</a>
								</c:when>
								<c:otherwise>
									<a href="#" onclick="displayGeneralSettings('SubMenu')">Sub-Menu</a>
								</c:otherwise>
							</c:choose></li>
						<li><c:choose>
								<c:when test="${sessionScope.menuType eq 'News'}">
									<a href="#" class="active"
										onclick="displayGeneralSettings('News')">News</a>
								</c:when>
								<c:otherwise>
									<a href="#" onclick="displayGeneralSettings('News')">News</a>
								</c:otherwise>
							</c:choose></li>
						<li><c:choose>
								<c:when test="${sessionScope.menuType eq 'TabBarLogo'}">
									<a href="#" class="active"
										onclick="displayGeneralSettings('TabBarLogo')">Others</a>
								</c:when>
								<c:otherwise>
									<a href="#" onclick="displayGeneralSettings('TabBarLogo')">Others</a>
								</c:otherwise>
							</c:choose></li>
					</ul>
					<div class="cont-wrp">
						<c:choose>
							<c:when test="${requestScope.responseStatus eq 'SUCCESS' }">

								<div class="alertBx success">
									<span class="actn-close" title="close"></span>
									<p class="msgBx">
										<c:out value="${requestScope.responeMsg}" />
									</p>
								</div>

							</c:when>

							<c:when test="${requestScope.responseStatus eq 'FAILURE' }">

								<div class="alertBx failure">
									<span class="actn-close" title="close"></span>
									<p class="msgBx">
										<c:out value="${requestScope.responeMsg}" />
									</p>
								</div>

							</c:when>

						</c:choose>

						<form:form name="screenSettingsForm" id="screenSettingsForm"
							commandName="screenSettingsForm" enctype="multipart/form-data"
							action="uploadimg.htm">
							<form:hidden path="oldImageName" />
							<form:hidden path="pageType" />
							<form:hidden path="viewName" value="generalsettings" />
							<form:hidden path="uploadImageType" id="uploadImageType"
								value="logoImage" />
							<form:hidden path="oldAppIconImage" />
							<form:hidden path="oldHomeIconName" />
							<form:hidden path="oldBackButtonIconName" />
							<form:hidden path="btnPosition" />
							<form:hidden path="hiddenCategory" />
							<form:hidden path="oldCatImgName" />
							<form:hidden path="catImgUrl" />
							<form:hidden path="oldClaimImage" />
							<form:hidden id="titlebarchanged" path="titlebarchanged" />
							<form:hidden id="claimBannerTextFlag" path="claimBannerTextFlag" />

							<table width="100%" border="0" cellpadding="0" cellspacing="0"
								class="cmnTbl">
								<tr>
								<tr>
									<td width="40%">Image Upload</td>
									<td width="60%" class="actn-cntrl"><span class="">
											<form:radiobutton value="false" path="isCatImg"
												id="tglTabbarUpload" onclick="updateImgCropSize('logoImg')" />
											<label for="tglTabbarUpload">Tab Bar</label>
									</span> <span class="mrgn-left"> <form:radiobutton value="true"
												path="isCatImg" id="tglLogoUpload"
												onclick="updateImgCropSize('catImg')" /> <label
											for="tglLogoUpload">Find Category</label>
									</span></td>
								</tr>

								<tr class="tglLogoUpload">
									<!-- <td width="40%"></td> -->
									<td id="msgSelectCat" width="60%" style="color: #3178a1"
										colspan="4" align="center"><label>Please select
											category to upload image</label></td>
								</tr>

								<tr class="tglLogoUpload">
									<td width="40%"><label class="mand">Find Category</label></td>
									<td width="60%"><form:errors cssClass="errorDsply"
											path="category"></form:errors>
										<div class="cntrl-grp zeroBrdr">
											<form:select path="category" id="catDetails">
												<option value="0">Select Category</option>
												<c:forEach items="${sessionScope.findCategoryImg }"
													var="category">
													<option value="${category.catImgId}"
														imagePath="${category.catImgPath}">${category.catName}</option>
												</c:forEach>
											</form:select>
										</div></td>
								</tr>

								<tr class="tglLogoUpload" id="categoryImg">
									<td width="40%"><label class="mand">Image</label></td>
									<td width="60%"><label class="rnd"> <form:errors
												cssClass="errorDsply" path="catImgName"></form:errors> <img
											id="catImgLogo" width="46" height="46" alt="upload"
											src="${sessionScope.catImgPreview}">
									</label> <span class="topPadding cmnRow"> <label
											for="trgrUpldCatImg"> <input type="button"
												value="Upload" id="trgrUpldBtn" class="btn trgrUpld"
												title="Upload Image File" tabindex=""> <form:hidden
													path="catImgName" id="catImgpath" /> <span
												class="instTxt nonBlk"></span> <form:input type="file"
													class="textboxBig" id="trgrUpldCatImg" path="catImgFile" />
										</label>
									</span> <label id="maxSizeCatImage" class="errorDsply"></label></td>
								</tr>

								<tr class="tglLogoUpload">
									<td>&nbsp;</td>
									<td><input type="submit" name="button" id="saveImgUpload"
										value="Update Image" class="btn-blue"
										onclick="saveGeneralSettings('CatImage');" /></td>
								</tr>

								<tr class="bg-image tglSubMenuOptns">
									<td width="40%"><label class="mand">Title Bar Logo</label></td>

									<td width="60%"><form:errors cssClass="errorDsply"
											path="logoImageName"></form:errors> <label> <img
											id="loginScreenLogo" width="86" height="34" alt="upload"
											src="${sessionScope.titleBarLogo}">
									</label> <span class="topPadding cmnRow"> <label for="trgrUpld">
												<input type="button" value="Upload" id="trgrUpldBtn"
												class="btn trgrUpld" title="Upload Image File" tabindex="1">
												<form:hidden path="logoImageName" id="strBannerAdImagePath" />
												<span class="instTxt nonBlk"></span> <form:input type="file"
													class="textboxBig" id="trgrUpld" path="logoImage"
													onchange="checkBannerSize(this);" />
										</label>
									</span><label id="maxSizeImageError" class="errorDsply"></label></td>
								</tr>


								<tr id="" class="bg-image tglSubMenuOptns">

									<td width="40%"><label class="mand">App Icon</label></td>
									<td width="60%"><form:errors cssClass="errorDsply"
											path="bannerImageName"></form:errors> <label> <img
											id="twoColTmptImg" width="57" height="57" alt="upload"
											src="${sessionScope.appiconpreview}">
									</label> <span class="topPadding cmnRow"> <label
											for="trgrUpldImg"> <input type="button"
												value="Upload" id="trgrUpldBtnImg" class="btn trgrUpldImg"
												title="Upload Image File" tabindex="2"> <form:hidden
													path="bannerImageName" id="bannerImageName" /> <span
												class="instTxt nonBlk"></span> <form:input type="file"
													class="textboxBig" id="trgrUpldImg" path="imageFile" />
										</label>
									</span><label id="maxSizeAppIconError" class="errorDsply"></label></td>
								</tr>

								<!-- Icon Implementation -->
								<tr id="" class="bg-image tglSubMenuOptns">

									<td width="40%"><label class="mand">Home Icon</label></td>
									<td width="60%"><form:errors cssClass="errorDsply"
											path="homeIconName">
										</form:errors> <label> <img id="homebtnimg" width="57" height="57"
											alt="upload" src="${sessionScope.homeiconuploadpreview}">
									</label> <span class="topPadding cmnRow"> <label
											for="trgrUpldImghome"> <input type="button"
												value="Upload" id="trgrUpldBtnImghome"
												class="btn trgrUpldImghome" title="Upload Image File"
												tabindex="2"> <form:hidden path="homeIconName"
													id="homeIconName" /> <span class="instTxt nonBlk"></span>
												<form:input type="file" class="textboxBig"
													id="trgrUpldImghome" path="homeIconFile" />
										</label>
									</span><label id="maxSizeHomeError" class="errorDsply"></label></td>
								</tr>

								<tr id="" class="bg-image tglSubMenuOptns">

									<td width="40%"><label class="mand">Back Button
											Icon</label></td>
									<td width="60%"><form:errors cssClass="errorDsply"
											path="backButtonIconName">
										</form:errors> <label> <img id="backbtnimg" width="57" height="57"
											alt="upload"
											src="${sessionScope.backbuttoniconuploadpreview}">
									</label> <span class="topPadding cmnRow"> <label
											for="trgrUpldImgbackbtn"> <input type="button"
												value="Upload" id="trgrUpldImgBtnbackbtn"
												class="btn trgrUpldImgbackbtn" title="Upload Image File"
												tabindex="2"> <form:hidden path="backButtonIconName"
													id="backButtonIconName" /> <span class="instTxt nonBlk"></span>
												<form:input type="file" class="textboxBig"
													id="trgrUpldImgbackbtn" path="backButtonIconFile" />
										</label>
									</span><label id="maxSizeBackError" class="errorDsply"></label></td>
								</tr>


								<tr class="tglSubMenuOptns">
									<td valign="top"><span class="pick-color-label"><label
											class="mand">Background Color</label> </span></td>
									<td><form:errors cssClass="errorDsply"
											path="backGroundColor"></form:errors> <input
										class="nvgbgColorSpectrum" /> <form:hidden
											path="backGroundColor" class="inputTxt"
											name="backGroundColor" id="backGroundColor" /></td>
								</tr>

								<tr class="tglSubMenuOptns">
									<td valign="top"><span class="pick-color-label"><label
											class="mand">Title Color</label> </span></td>

									<td><form:errors cssClass="errorDsply" path="titleColor"></form:errors>
										<input class="titleColorSpectrum" /> <form:hidden
											path="titleColor" class="inputTxt" name="titleColor"
											id="titleColor" /></td>
								</tr>

								<tr class="bg-image tglSubMenuOptns">
									<td><label class="mand">Bottom Button Type</label></td>
									<td colspan="2"><form:errors cssClass="errorDsply"
											path="bottomBtnId"></form:errors> <c:choose>
											<c:when test="${sessionScope.buttomBtn eq 'Exist'}">
												<c:forEach items="${sessionScope.buttomBtnType}"
													var="buttomBtn">
													<form:radiobutton path="bottomBtnId"
														value="${buttomBtn.bottomBtnId}" disabled="true"
														tabindex="3" />${buttomBtn.bottomBtnName}&nbsp;&nbsp;&nbsp;									
										</c:forEach>
											</c:when>
											<c:otherwise>
												<c:forEach items="${sessionScope.buttomBtnType}"
													var="buttomBtn">
													<form:radiobutton path="bottomBtnId"
														value="${buttomBtn.bottomBtnId}" tabindex="4" />${buttomBtn.bottomBtnName}&nbsp;&nbsp;&nbsp;									
										</c:forEach>
											</c:otherwise>
										</c:choose></td>
								</tr>

								<tr class="tglSubMenuOptns">
									<td valign="center"><label class="mand">Claim URL</label>
									</td>
									<td><form:errors cssClass="errorDsply"
											path="hubCitiClaimURL"></form:errors>
										<div class="cntrl-grp">
											<form:input path="hubCitiClaimURL" class="inputTxtBig"
												value="" style="height: 22px;" type="text" />
										</div></td>
								</tr>
								<tr class="claim-js">
									<td width="40%"><span class="mand">Claim Type </span></td>
									<td width="60%" class="actn-cntrl"><span class="">
											<form:radiobutton value="true" path="claimType"
												id="claimimage" onclick="claimTypeChange('image');" /> <label
											for="claimtypeload">Image</label>
									</span> <span class="mrgn-left"> <form:radiobutton
												value="false" path="claimType" id="claimtext"
												onclick="claimTypeChange('text');" /> <label for="">
												Text</label>
									</span></td>
									<td><label id="maxSizeClaimError" class="errorDsply"></label></td>
								</tr>

								<tr id=""
									class="bg-image tglSubMenuOptns claimImage-js claim-js">

									<td width="40%"><label class="mand">Claim Image
											Icon</label></td>
									<td width="60%"><form:errors cssClass="errorDsply"
											path="claimImage">
										</form:errors> <label> <img id="claimbtnimg" width="150" height="50"
											alt="upload" src="${sessionScope.claimIconPreview}">
									</label> <span class="topPadding cmnRow"> <label
											for="trgrUpldImgclaimbtn"> <input type="button"
												value="Upload" id="trgrUpldImgBtncliambtn"
												class="btn trgrUpldImgclaimbtn" title="Upload Image File"
												tabindex="2"> <form:hidden path="claimImage"
													id="claimImage" /> <span class="instTxt nonBlk"></span> <form:input
													type="file" class="textboxBig" id="trgrUpldImgclaimbtn"
													path="claimImageFile" />
										</label>
									</span><label id="maxSizeclaimError" class="errorDsply"></label></td>
								</tr>

								<tr class="claimText-js claim-js">
									<td colspan="2"><label class="mand">Claim Your
											Business Text</label></td>

								</tr>
								<tr class="claimText-js claim-js">
									<td class="zeropaddingcss" colspan="2"><form:errors
											path="claimText" cssStyle="color:red">
										</form:errors> <form:textarea id="claimText" path="claimText"
											cssClass="textareaTxt cstmFix" /></td>
								</tr>
								
								
								<tr class="tglSubMenuOptns">
									<td colspan="2"><label><b>Claim Business Banner Text :</b></label></td>
								</tr>
								
								<tr class="tglSubMenuOptns">
									<td valign="center"><label class="mand">Line 1 : </label>
									</td>
									<td><form:errors cssClass="errorDsply"
											path="claimBannerText1"></form:errors>
										<div class="cntrl-grp">
											<form:input id="claimBannerText1" path="claimBannerText1" class="inputTxtBig"
											    style="height: 22px;" type="text"  maxlength="40" onpaste="return false" oncopy="return false"/>
										</div>
										<span class="lbl">Chars Left:</span><span
										class="lblcnt claimBannerText1">40</span></td>
								</tr>
								
								<tr class="tglSubMenuOptns">
									<td valign="center"><label>Line 2 : </label>
									</td>
									<td><form:errors cssClass="errorDsply"
											path="claimBannerText2"></form:errors>
										<div class="cntrl-grp">
											<form:input id="claimBannerText2" path="claimBannerText2" class="inputTxtBig"
											    style="height: 22px;" type="text" maxlength="40" onpaste="return false" oncopy="return false"/>
										</div><span class="lbl">Chars Left:</span><span
										class="lblcnt claimBannerText2">40</span></td>
								</tr>
								
								<tr class="tglSubMenuOptns">
									<td valign="center"><label>Line 3 : </label>
									</td>
									<td><form:errors cssClass="errorDsply"
											path="claimBannerText3"></form:errors>
										<div class="cntrl-grp">
											<form:input id="claimBannerText3" path="claimBannerText3" class="inputTxtBig"
											    style="height: 22px;" type="text" maxlength="40" onpaste="return false" oncopy="return false"/>
										</div><span class="lbl">Chars Left:</span><span
										class="lblcnt claimBannerText3">40</span></td>
								</tr>
								
								<tr class="tglSubMenuOptns">
									<td valign="center"><label>Line 4 : </label>
									</td>
									<td><form:errors cssClass="errorDsply"
											path="claimBannerText4"></form:errors>
										<div class="cntrl-grp">
											<form:input id="claimBannerText4" path="claimBannerText4" class="inputTxtBig"
											    style="height: 22px;" type="text" maxlength="40" onpaste="return false" oncopy="return false"/>
										</div><span class="lbl">Chars Left:</span><span
										class="lblcnt claimBannerText4">40</span></td>
								</tr >
								
								
								<tr class="bg-image tglSubMenuOptns">
									<td>&nbsp;</td>
									<td><input type="button" name="button" id="button"
										title="${sessionScope.btnname}"
										onclick="saveGeneralSettings('TabBarLogo');"
										value="${sessionScope.btnname}" class="btn-blue" tabindex="2" /></td>
								</tr>
							</table>
						</form:form>
					</div>
				</div>
				<div class="cont-block">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-iphone">&nbsp;</span></li>
							<li>Preview</li>
						</ul>
					</div>
					<div class="cont-wrp">
						<div id="iphone-preview">
							<!--Iphone Preview For Login screen starts here-->
							<div id="iphone-aboutus-preview">
								<div class="iphone-status-bar"></div>
								<div class="navBar iphone">
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="titleGrd">
										<tr>
											<td width="19%" align="left"><img id="backbtnpreview"
												width="30" height="30" alt="back"
												src="${sessionScope.backbuttoniconpreview}" alt="Back"></td>

											<td width="54%" class="genTitle">
												<div class="barlogo" style="display: block;">
													<img src="${sessionScope.titleBarLogoPreview }"
														onerror="this.src='images/small-logo.png';" width="86"
														height="35" alt="Logo" />
												</div>
												<div style="display: none; font-size: large;"
													class="titlebarcolor">TITLE</div>
											</td>

											<td width="27%" align="right"><img id="homebtnpreview"
												width="30" height="30" alt="home"
												src="${sessionScope.homeiconpreview }"></td>
										</tr>
									</table>
								</div>
								<div class="previewAreaScroll">
									<div class="iPhone-preview-container"></div>
								</div>
								<ul id="tab-main" class="tabbar"></ul>
							</div>

							<!-- For Category -->

							<div id="iphone-priPolicy-preview">
								<div class="iphone-status-bar"></div>
								<div class="navBar iphone">
									<table width="100%" cellspacing="0" cellpadding="0" border="0"
										class="titleGrd">
										<tbody>
											<tr>
												<td width="19%" align="left"><img id="backbtnpreview1"
													width="30" height="30" alt="back"
													src="${sessionScope.backbuttoniconpreview}" alt="Back"></td>
												<td width="50%" class="genTitle">
												<td width="24%" align="center"></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="previewAreaScroll gnrlScrn rnd">
									<ul class="sortable ui-sortable listView rnd" id="dynTab">
										<c:forEach items="${sessionScope.findCategoryImg }"
											var="category">
											<li class="tabs"><a id="temp-btn-${category.catImgId}"
												onclick="selectCategory(this)" href="javascript:void(0)">
													<img width="30" height="30" class="lstImg" alt="image"
													src="${category.catImgPath}"> <span class="trunc"
													catImgId="${category.catImgId}">${category.catName}</span>
											</a></li>
											<%-- <option value="${category.catImgId}" imagePath="${category.catImgPath}">${category.catName}</option> --%>
										</c:forEach>
									</ul>

								</div>
								<ul id="tab-main" class="tabbar"></ul>
							</div>

							<!-- END -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/HubCiti/images/popupClose.png" class="closeIframe"
				alt="close"
				onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span
				id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
			allowtransparency="yes" width="100%" style="background-color: White">
		</iframe>
	</div>
</div>
<style>
.menu-pnl {
	height: 1400px;
}

.cont-block {
	height: 1400px;
}
</style>
<script type="text/javascript">
	$(document)
			.ready(
					function() {

						CKEDITOR.config.uiColor = '#FFFFFF';
						CKEDITOR.replace('claimText',
								{
									extraPlugins : 'onchange',
									width : "100%",
									toolbar : [
											/* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
											{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
											{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
											'/',
											{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
											{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
											{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
											{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
											'/',
											{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
											{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
											{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
											{
												name : 'basicstyles',
												items : [ 'Bold', 'Italic',
														'Underline' ]
											},
											{
												name : 'paragraph',
												items : [ 'JustifyLeft',
														'JustifyCenter',
														'JustifyRight',
														'JustifyBlock' ]
											},
											{
												name : 'colors',
												items : [ 'BGColor' ]
											},
											{
												name : 'paragraph',
												items : [ 'Outdent', 'Indent' ]
											},
											{
												name : 'links',
												items : [ 'Link', 'Unlink' ]
											},
											'/',
											{
												name : 'styles',
												items : [ 'Styles', 'Format' ]
											},
											{
												name : 'tools',
												items : [ 'Font', 'FontSize',
														'RemoveFormat' ]
											} ],
									removePlugins : 'resize'
								});

						$('#trgrUpldImg')
								.bind(
										'change',
										function() {

											var imageType = document
													.getElementById("trgrUpldImg").value;
											var uploadImageType = $(this).attr(
													'name');

											document.screenSettingsForm.uploadImageType.value = uploadImageType;
											if (imageType != '') {
												var checkbannerimg = imageType
														.toLowerCase();
												if (!checkbannerimg
														.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
													alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
													return false;
												} else {
													$("#screenSettingsForm")
															.ajaxForm(
																	{
																		success : function(
																				response) {
																			var imgRes = response
																					.getElementsByTagName('imageScr')[0].firstChild.nodeValue
																			var substr = imgRes
																					.split('|');
																			if (substr[0] == 'maxSizeImageError') {
																				$(
																						'#maxSizeAppIconError')
																						.text(
																								"Image Dimension should not exceed Width: 800px Height: 600px");
																			} else {
																				openIframePopup(
																						'ifrmPopup',
																						'ifrm',
																						'/HubCiti/cropImage.htm',
																						100,
																						99.5,
																						'Crop Image');
																			}
																		}
																	}).submit();
												}
											}

										});

						$('#trgrUpldImghome')
								.bind(
										'change',
										function() {

											var imageType = document
													.getElementById("trgrUpldImghome").value;
											var uploadImageType = $(this).attr(
													'name');

											document.screenSettingsForm.uploadImageType.value = uploadImageType;
											if (imageType != '') {
												var checkbannerimg = imageType
														.toLowerCase();
												if (!checkbannerimg
														.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
													alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
													return false;
												} else {
													$("#screenSettingsForm")
															.ajaxForm(
																	{
																		success : function(
																				response) {
																			var imgRes = response
																					.getElementsByTagName('imageScr')[0].firstChild.nodeValue
																			var substr = imgRes
																					.split('|');
																			if (substr[0] == 'maxSizeImageError') {
																				$(
																						'#maxSizeHomeError')
																						.text(
																								"Image Dimension should not exceed Width: 800px Height: 600px");
																			} else {
																				openIframePopup(
																						'ifrmPopup',
																						'ifrm',
																						'/HubCiti/cropImage.htm',
																						100,
																						99.5,
																						'Crop Image');
																			}
																		}
																	}).submit();
												}
											}

										});
						$('#trgrUpldImgbackbtn')
								.bind(
										'change',
										function() {

											var imageType = document
													.getElementById("trgrUpldImgbackbtn").value;
											var uploadImageType = $(this).attr(
													'name');

											document.screenSettingsForm.uploadImageType.value = uploadImageType;
											if (imageType != '') {
												var checkbannerimg = imageType
														.toLowerCase();
												if (!checkbannerimg
														.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
													alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
													return false;
												} else {
													$("#screenSettingsForm")
															.ajaxForm(
																	{
																		success : function(
																				response) {
																			var imgRes = response
																					.getElementsByTagName('imageScr')[0].firstChild.nodeValue
																			var substr = imgRes
																					.split('|');
																			if (substr[0] == 'maxSizeImageError') {
																				$(
																						'#maxSizeBackError')
																						.text(
																								"Image Dimension should not exceed Width: 800px Height: 600px");
																			} else {
																				openIframePopup(
																						'ifrmPopup',
																						'ifrm',
																						'/HubCiti/cropImage.htm',
																						100,
																						99.5,
																						'Crop Image');
																			}
																		}
																	}).submit();
												}
											}

										});

						$('#trgrUpldImgclaimbtn')
								.bind(
										'change',
										function() {

											var imageType = document
													.getElementById("trgrUpldImgclaimbtn").value;
											var uploadImageType = $(this).attr(
													'name');

											document.screenSettingsForm.uploadImageType.value = uploadImageType;
											if (imageType != '') {
												var checkbannerimg = imageType
														.toLowerCase();
												if (!checkbannerimg
														.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
													alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
													return false;
												} else {
													$("#screenSettingsForm")
															.ajaxForm(
																	{
																		success : function(
																				response) {
																			var imgRes = response
																					.getElementsByTagName('imageScr')[0].firstChild.nodeValue
																			var substr = imgRes
																					.split('|');
																			if (substr[0] == 'maxSizeImageError') {
																				$(
																						'#maxSizeclaimError')
																						.text(
																								"Image Dimension should not exceed Width: 768px Height: 150px");
																			} else {
																				openIframePopup(
																						'ifrmPopup',
																						'ifrm',
																						'/HubCiti/cropImage.htm',
																						100,
																						100,
																						'Crop Image');
																			}
																		}
																	}).submit();
												}
											}

										});
						$('input:radio[name="isCatImg"]')
								.change(
										function() {
											var tagId = $(
													'input:radio[name="isCatImg"]:checked')
													.val();

											if (tagId === "true") {

												$(".tglLogoUpload").show();
												$(".tglSubMenuOptns").hide();
												$(".claim-js").hide();
												$("#iphone-aboutus-preview")
														.hide();
												$(".tabbaritems").hide();
												/* To update Image */
												var imgPath = $(
														"#catDetails option:selected")
														.attr("imagePath");
												if (null == imgPath
														|| imgPath === '') {
													$("#catImgLogo")
															.attr('src',
																	'/HubCiti/images/uploadIconSqr.png');
												} else {
													$("#catImgLogo").attr(
															'src', imgPath);
												}

												if ($(
														"#catDetails option:selected")
														.val() === '0') {
													$("#categoryImg").hide();
													$("#saveImgUpload").hide();
													$("#msgSelectCat").show();
												} else {
													$("#categoryImg").show();
													$("#saveImgUpload").show();
													$("#msgSelectCat").hide();
												}
											} else {

												$(".tglLogoUpload").hide();
												$(".tglSubMenuOptns").show();
												$(".claim-js").show();
												var isClaimType = $(
														'input:radio[name="claimType"]:checked')
														.val();

												if (isClaimType == "true") {
													$('.claimImage-js').show();
													$('.claimText-js').hide();
												} else {
													$('.claimImage-js').hide();
													$('.claimText-js').show();
												}
												//	$("#iphone-preview").show();
												$("#iphone-aboutus-preview")
														.show();
											}
										});
						//$("input[name='isCatImg']:checked").trigger('change');

						$("#catDetails")
								.change(
										function(event) {
											var imgPath = $(
													"#catDetails option:selected")
													.attr("imagePath");
											if (null == imgPath
													|| imgPath === '') {
												$("#catImgLogo")
														.attr('src',
																'/HubCiti/images/uploadIconSqr.png');
											} else {
												$("#catImgLogo").attr('src',
														imgPath);
											}
											if ($("#catDetails option:selected")
													.val() === '0') {
												$("#categoryImg").hide();
												$("#saveImgUpload").hide();
												$("#msgSelectCat").show();
											} else {
												$("#categoryImg").show();
												$("#saveImgUpload").show();
												$("#msgSelectCat").hide();
											}
											$(".errorDsply").remove();
											document.screenSettingsForm.catImgName.value = '';
											updateImgCropSize('clearSessionImg');
										});

						var catSelected = document.screenSettingsForm.hiddenCategory.value;
						$("#catDetails option[value='" + catSelected + "']")
								.attr("selected", "selected");

						//var isCatImg = document.screenSettingsForm.isCatImg.value;
						var isCatImg = $('input:radio[name="isCatImg"]:checked')
								.val();
						if (isCatImg == "true") {
							$(".tabbaritems").hide();
							$(".tglLogoUpload").show();
							$(".tglSubMenuOptns").hide();
							$("#iphone-aboutus-preview").hide();
							$('.claim-js').hide();

							if ($("#catDetails option:selected").val() === '0') {
								$("#categoryImg").hide();
								$("#saveImgUpload").hide();
								$("#msgSelectCat").show();
							} else {
								$("#categoryImg").show();
								$("#saveImgUpload").show();
								$("#msgSelectCat").hide();
							}

						} else {
							$(".tglLogoUpload").hide();
							$(".tglSubMenuOptns").show();
							$("#iphone-aboutus-preview").show();
							var isClaimType = $(
									'input:radio[name="claimType"]:checked')
									.val();

							if (isClaimType == "true") {
								$('.claimImage-js').show();
								$('.claimText-js').hide();
							} else {
								$('.claimImage-js').hide();
								$('.claimText-js').show();
							}
						}

						$('#trgrUpldCatImg')
								.bind(
										'change',
										function() {

											if ($("#catDetails option:selected")
													.val() === '0') {
												alert("Please select category to upload image");
												return false;
											}

											var imageType = document
													.getElementById("trgrUpldCatImg").value;
											var uploadImageType = $(this).attr(
													'name');
											document.screenSettingsForm.uploadImageType.value = uploadImageType;
											document.screenSettingsForm.hiddenCategory.value = $(
													"#catDetails option:selected")
													.val();

											if (imageType != '') {
												var checkbannerimg = imageType
														.toLowerCase();
												if (!checkbannerimg
														.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
													alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
													return false;
												} else {
													$("#screenSettingsForm")
															.ajaxForm(
																	{
																		success : function(
																				response) {
																			var imgRes = response
																					.getElementsByTagName('imageScr')[0].firstChild.nodeValue
																			var substr = imgRes
																					.split('|');
																			if (substr[0] == 'maxSizeImageError') {
																				$(
																						'#maxSizeCatImage')
																						.text(
																								"Image Dimension should not exceed Width: 800px Height: 600px");
																			} else {
																				openIframePopup(
																						'ifrmPopup',
																						'ifrm',
																						'/HubCiti/cropImage.htm',
																						100,
																						99.5,
																						'Crop Image');
																			}
																		}
																	}).submit();
												}
											}
										});

						var imgUrl = $("#catImgLogo").attr('src');
						if (imgUrl === ''
								|| imgUrl === '/HubCiti/images/uploadIconSqr.png') {
							$("#catImgLogo")
									.attr(
											'src',
											document.screenSettingsForm.catImgUrl.value);
						}

					});
	$(document).ready(function() {

		var backGroundColor = $('#backGroundColor').val();
		var titleColor = $('#titleColor').val();

		if (backGroundColor == "") {
			$(".nvgbgColorSpectrum").spectrum("set", "#000000");
			$('#backGroundColor').val("#000000");
			$(".navBar").css("background", $(".nvgbgColorSpectrum").val());
			$(".tabbar").css("background", $(".nvgbgColorSpectrum").val());

		} else {
			$(".nvgbgColorSpectrum").spectrum("set", backGroundColor);
			$(".navBar").css("background", $(".nvgbgColorSpectrum").val());
			$(".tabbar").css("background", $(".nvgbgColorSpectrum").val());
		}
		if (titleColor == "") {
			$(".titleColorSpectrum").spectrum("set", "#ffffff");
			$('#titleColor').val("#ffffff");
			$('.titlebarcolor').css("color", $(".titleColorSpectrum").val())
		} else {
			$(".titleColorSpectrum").spectrum("set", titleColor);
			$('.titlebarcolor').css("color", $(".titleColorSpectrum").val())
		}

		$(".titleColorSpectrum").change(function() {

			$(".genTitle .barlogo").hide();
			$(".genTitle .titlebarcolor").show();

		});

		var titlebarValue = $('#titlebarchanged').val();
		if (null != titlebarValue && titlebarValue != "") {
			$(".genTitle .barlogo").show();
			$(".genTitle .titlebarcolor").hide();
			$('#titlebarchanged').val(null);
		} else {

			$(".genTitle .barlogo").hide();
			$(".genTitle .titlebarcolor").show();
		}
		
		
		var vClaimBannerText1 = $('#claimBannerText1').val();
		var vClaimBannerText2 = $('#claimBannerText2').val();
		var vClaimBannerText3 = $('#claimBannerText3').val();
		var vClaimBannerText4 = $('#claimBannerText4').val();
		var vClaimBannerTextFlag = $('#claimBannerTextFlag').val();
		var vBtnName =  "${sessionScope.btnname}";
		if (vBtnName ==="Save Settings" &&  vClaimBannerTextFlag == "false") {
				$('#claimBannerText1').val("If this is your business, Claim it! - Free!");
				$('#claimBannerText2').val("Add/update your Website Address, Phone,");
				$('#claimBannerText3').val("Business Type, Key Words,");
				$('#claimBannerText4').val("About Us, See more options...");
		} else if (vBtnName ==="Update Settings") {
				
			if (vClaimBannerText1 == "" && vClaimBannerText2 == "" && vClaimBannerText3 == ""  && vClaimBannerText4 == "" && vClaimBannerTextFlag == "false") {
				$('#claimBannerText1').val("If this is your business, Claim it! - Free!");
				$('#claimBannerText2').val("Add/update your Website Address, Phone,");
				$('#claimBannerText3').val("Business Type, Key Words,");
				$('#claimBannerText4').val("About Us, See more options...");
			}
		}
		
		
		var vErrLength = $(".tglSubMenuOptns .errorDsply").length;
		
		if($(".tglSubMenuOptns .errorDsply").length > 0) {
			$(".cont-block").height(1500);
			$(".cont-block").height(1550);
		}
		
		$("#claimBannerText1").limiter(40, $(".claimBannerText1"));
		$("#claimBannerText2").limiter(40, $(".claimBannerText2"));
		$("#claimBannerText3").limiter(40, $(".claimBannerText3"));
		$("#claimBannerText4").limiter(40, $(".claimBannerText4"));
	});
</script>
<script type="text/javascript">
	configureMenu("generalsettings");
</script>
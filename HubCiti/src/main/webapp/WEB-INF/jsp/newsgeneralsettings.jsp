<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="/HubCiti/styles/colorPicker.css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>News Settings</title>

<script type="text/javascript">
$(document).ready(function(){

        $('#trgrUpldNewsImg').bind('change', function() {

            var imageType = document.getElementById("trgrUpldNewsImg").value;
            var uploadImageType = $(this).attr('name');

            document.addnewsform.uploadNewsImageType.value = uploadImageType;
            if (imageType != '') {
                var checkbannerimg = imageType.toLowerCase();
                if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
                    alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
                    return false;
                } else {
                    $("#addnewsform").ajaxForm({
                        success : function(response) {
                            var imgRes = response.getElementsByTagName('imageScr')[0].firstChild.nodeValue;
                            var substr = imgRes.split('|');
                            if (substr[0] == 'maxSizeImageError') {
                                $('#maxSizeAppIconError').text("Image Dimension should not exceed Width: 800px Height: 600px");
                            } else {
                                openIframePopup('ifrmPopup', 'ifrm', '/HubCiti/cropImage.htm', 100, 99.5, 'Crop Image');
                            }
                        }
                    }).submit();
                }
            }

        });
        
        $('#trgrUpldnewsDefImg').bind('change', function() {

            var imageType = document.getElementById("trgrUpldnewsDefImg").value;
            var uploadImageType = $(this).attr('name');

            document.addnewsform.uploadNewsImageType.value = uploadImageType;
            if (imageType != '') {
                var checkbannerimg = imageType.toLowerCase();
                if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
                    alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
                    return false;
                } else {
                    $("#addnewsform").ajaxForm({
                        success : function(response) {
                            var imgRes = response.getElementsByTagName('imageScr')[0].firstChild.nodeValue;
                            var substr = imgRes.split('|');
                            if (substr[0] == 'maxSizeImageError') {
                                $('#maxSizeAppIconError1').text("Image Dimension should not exceed Width: 800px Height: 600px");
                            } else {
                                openIframePopup('ifrmPopup', 'ifrm', '/HubCiti/cropImage.htm', 100, 99.5, 'Crop Image');
                            }
                        }
                    }).submit();
                }
            }

        });

	

});

function addnewssettings()
{
	
	document.addnewsform.action = "newsgeneralsettings.htm";
	document.addnewsform.method = "post";
	document.addnewsform.submit();

}

function clearNewsForm()
{
var r = confirm("Do you really want to clear the form");
		if (r == true) {
		
			document.addnewsform.bannerImg.value = "";
			document.addnewsform.newsImage.value = "";
			document.addnewsform.ThumnailPos.value = "";
			document.addnewsform.weatherURL.value = "";
			$("#bannerImg").removeAttr("src").prop('src','images/upload_image.png');
			$("#newsImage").removeAttr("src").prop('src','images/upload_image.png');
			
			$("#successMsg").hide();
				$("#failureMsg").hide();
			if (document.getElementById('bannerImg.errors') != null) {
				document.getElementById('bannerImg.errors').style.display = 'none';
			}
			if (document.getElementById('newsImage.errors') != null) {
				document.getElementById('newsImage.errors').style.display = 'none';
			}

			if (document.getElementById('weatherURL.errors') != null) {
				document.getElementById('weatherURL.errors').style.display = 'none';
			}
			if (document.getElementById('ThumnailPos.errors') != null) {
				document.getElementById('ThumnailPos.errors').style.display = 'none';
			}
			
			
			
			}
			}
</script>
</head>

<body>
	<!--Wrapper div Starts-->
	<div id="wrpr">
		<div class="clear"></div>
		<div class="wrpr-cont relative">
			<div id="slideBtn">
				<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
					src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
			</div>
			<!--Breadcrum div starts-->
			<div id="bread-crumb">
				<ul>
					<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
					<li><a href="welcome.htm">Home</a></li>
					<li class="last">News Settings</li>
				</ul>
			</div>
			<!--Breadcrum div ends-->
			<span class="blue-brdr"></span>
			<!--Content div starts-->
			<div class="content" id="login">
				<!--Left Menu div starts-->
				<div id="menu-pnl" class="split">
					<jsp:include page="leftNavigation.jsp"></jsp:include>
				</div>
				<!--Left Menu div ends-->
				<!--Content panel div starts-->
				<div class="cont-pnl split" id="">
					<div class="cont-block stretch">
						<div class="title-bar">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-news">&nbsp;</span></li>
								<li>News Settings</li>
							</ul>
						</div>
						<div class="tabd-pnl">
							<ul class="nav-tabs">


								<li><a class="brdr-rt active"
									href="newsgeneralsettings.htm" title="New General Settings">News
										General Settings</a></li>
								<li><a href="addcatsettings.htm" class="rt-brdr"
									title="News Categories">News Categories</a></li>

								<c:if
									test="${requestScope.dispmngcat ne null && !empty requestScope.dispmngcat}">
									<li><a href="managenewscats.htm" class="rt-brdr"
										title="Manage News Categories">Manage News Categories</a></li>

								</c:if>
							</ul>
							<div class="clear"></div>
						</div>
						<form:form name="addnewsform" id="addnewsform"
							commandName="addnewsform" enctype="multipart/form-data"
							action="uploadnewsimage.htm">


							<div class="cont-block rt-brdr">
								<form:hidden path="uploadNewsImageType" id="uploadNewsImageType"
									value="logoImage" />


								<div class="cont-wrp">
									<center>
										<c:if
											test="${requestScope.addsuccessMsg ne null && !empty requestScope.addsuccessMsg}">
											<span class="highLightTxt" id="successMsg"><c:out
													value="${requestScope.addsuccessMsg}" /> </span>
										</c:if>
										<c:if
											test="${requestScope.failureMsg ne null && !empty requestScope.failureMsg}">
											<span class="highLightErr" id="failureMsg"><c:out
													value="${requestScope.failureMsg}" /> </span>
										</c:if>
									</center>

									<table width="100%" border="0" cellpadding="0" cellspacing="0"
										class="cmnTbl padngTbl">


										<tr>

											<td><label class="mand">Banner Image</label></td>
											<td><form:errors cssClass="errorDsply" path="bannerImg"></form:errors>
												<label> <img id="bannerImg" width="57" height="57"
													alt="upload" src="${sessionScope.newsbannerimage}">
											</label> <span class="topPadding cmnRow"> <label
													for="trgrUpldNewsImg"> <input type="button"
														value="Upload" id="trgrUpldBtnImg"
														class="btn trgrUpldNewsImg" title="Upload Image File"
														tabindex="1"> <form:hidden path="bannerImg"
															id="bannerImg" /> <span class="instTxt nonBlk"></span> <form:input
															type="file" class="textboxBig" id="trgrUpldNewsImg"
															path="imageFile" />
												</label>
											</span><label id="maxSizeAppIconError" class="errorDsply"></label></td>
										</tr>


										<tr>

											<td><label class="mand">News Image</label></td>
											<td><form:errors cssClass="errorDsply" path="newsImage"></form:errors>
												<label> <img id="newsImage" width="57" height="57"
													alt="upload" src="${sessionScope.newsdefaultimage}">
											</label> <span class="topPadding cmnRow"> <label
													for="trgrUpldnewsDefImg"> <input type="button"
														value="Upload" id="trgrUpldnewsBtnImg" tabindex="2"
														class="btn trgrUpldnewsDefImg" title="Upload Image File">
														<form:hidden path="newsImage" id="newsImage" /> <span
														class="instTxt nonBlk"></span> <form:input type="file"
															class="textboxBig" id="trgrUpldnewsDefImg"
															path="imageNewsFile" />
												</label>
											</span><label id="maxSizeAppIconError1" class="errorDsply"></label></td>
										</tr>


										<tr class="js-scrolling">
											<td><label class="mand">Thumbnail Position</label></td>
											<td class="cmnTbl mainMenu subMenu"><span
												class="actn-cntrl"><span class=""> <form:radiobutton
															path="ThumnailPos" name="ThumnailPos" id="left"
															value="Left" class="" checked="checked" tabindex="3" /> <label
														for="left">Left</label>
												</span> <span class="mrgn-left"> <form:radiobutton
															path="ThumnailPos" name="ThumnailPos" id="Right"
															value="Right" class="" tabindex="4" /> <label
														for="right">Right</label>
												</span></span></td>

										</tr>

										<tr>
											<td><label class="mand">Weather URL</label></td>
											<td class="cmnTbl mainMenu subMenu"><form:errors
													cssClass="errorDsply" path="weatherURL"></form:errors>
												<div class="cntrl-grp">
													<form:input type='text' path="weatherURL"
														class="inputTxtBig" tabindex="5" />
												</div></td>
										</tr>

										<tr class="cntrInput">
											<td>&nbsp;</td>
											<td class="cmnTbl"><input type="submit" name="button2"
												id="button" value="Save Settings" title="Save Settings"
												class="btn-blue" tabindex="6" onclick="addnewssettings()" />
											</td>
										</tr>

									</table>





								</div>
							</div>
							<div class="cont-block">
								<div class="title-bar">
									<ul class="title-actn">
										<li class="title-icon"><span class="icon-iphone">&nbsp;</span></li>
										<li>Preview</li>
									</ul>
									<span class="clear"></span>
								</div>
								<div class="cont-wrp">
									<div id="iphone-preview" class="hideOverflow">
										<!--Iphone Preview For Login screen starts here-->
										<div id="iphone-welcome-preview">
											<div class="iphone-status-bar"></div>
											<div class="navBar iphone">
												<table class="titleGrd" border="0" cellpadding="0"
													cellspacing="0" width="100%">
													<tbody>
														<tr>
															<td width="19%"><img src="images/hamburger.png"
																alt="back" height="26" width="26"></td>
															<td class="genTitle" width="54%"><img id="bannerImg"
																width="50" height="47" alt="upload"
																src="${sessionScope.newsbannerimage}" /></td>
															<td align="center" width="27%"></td>
														</tr>
													</tbody>
												</table>
											</div>
											<div>
												<img src="images/scrollingNews.jpg" width="320" height="460"
													alt="News" class="tglImg" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</form:form>




					</div>
				</div>
				<!--Content panel div ends-->
			</div>
			<!--Content div ends-->
		</div>
	</div>
	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/HubCiti/images/popupClose.png" class="closeIframe"
				alt="close"
				onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span
				id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
			allowtransparency="yes" width="100%" style="background-color: White">
		</iframe>
	</div>
</body>
<script type="text/javascript">
	configureMenu("newssettings");
</script>
</html>

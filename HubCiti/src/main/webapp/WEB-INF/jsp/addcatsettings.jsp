<%@page import="com.hubciti.common.pojo.NewsCategory"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<link rel="stylesheet" type="text/css"
	href="/HubCiti/styles/colorPicker.css" />
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>

<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<script src="/HubCiti/scripts/jquery-ui.js"></script>
<script src="/HubCiti/scripts/global.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add News Categories</title>

<script>
	$(document)
			.ready(
					function() {
						
						if($('input:radio[name="isFeed"]:checked').val() == 'true'){
							$('.feed-css').show();
							$('.nonfeed-css').hide();
						}else{
							$('.nonfeed-css').show();
							$('.feed-css').hide();
						}
						$(".navBar").css("background",
								$('#categoryColor').val());
						var vHiddenNews = $('#hidIsNewsTicker').val();
						if (vHiddenNews === "newsyes") {

							$('.newsno').show();
							$('#newsyes').prop('checked', true);
							$('#newsno').prop('checked', false);

						} else {

							$('.newsno').hide();
							$('#newsno').prop('checked', true);
							$('#newsyes').prop('checked', false);
						}
						
						$('input:radio[name="isNewsTicker"]')
								.change(
										function() {
											var vIsNewsTicker = $(this).attr(
													"id");

											$("input[name='isNewsTicker']")
													.removeAttr("checked");
											if (vIsNewsTicker === "newsyes") {

												$('.newsno').show();
												$('#newsyes').prop('checked',
														true);
												$('#newsno').prop('checked',
														false);

												//document.getElementById('noStories.errors').style.display = 'none';

											} else {

												if (document
														.getElementById('noStories.errors') != null) {
													document
															.getElementById('noStories.errors').style.display = 'none';
												}
												$("#noStories").val("");
												$('.newsno').hide();
												$('#newsno').prop('checked',
														true);
												$('#newsyes').prop('checked',
														false);
											}
										});

						$('input[name="isNewsTicker"]:checked').trigger(
								"change");
					});

	$(function() {

		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};

		$("#newsCtgry tbody").sortable({
			'opacity' : 0.6,
			containment : '.scrollTbl',
			cursor : "move",
			tolerance : "pointer",
			helper : fixHelper /*fix for mozilla click trigger*/,
		});

		/* Close SubCategory popup */
		$(".modal-close,.btn-grey").on('click.popup', function() {
			$(".modal-popupWrp").hide();
			$(".modal-popup").slideUp();
			$(".modal-popup").find(".modal-bdy input").val("");
			$(".modal-popup i").removeClass("errDsply");
		});

		$("#displayTypId").find(
				"option[text='$('#hidDisplaytype').val()']:selected");
		$("#displayTypId option").filter(function() {
			return this.text == $("#hidDisplaytype").val();
		}).prop('selected', 'selected');

		$("#subPageId").find("option[text='$('#hidsubpage').val()']:selected");
		$("#subPageId option").filter(function() {
			return this.text == $("#hidsubpage").val();
		}).prop('selected', 'selected');

		$("#catId").find("option[text='$('#hidCatName').val()']:selected");
		$("#catId option").filter(function() {
			return this.text == $("#hidCatName").val();
		}).prop('selected', 'selected');

		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};
		$("#newsCtgry tbody").sortable({
			'opacity' : 0.6,
			containment : '.scrollTbl',
			cursor : "move",
			tolerance : "pointer",
			helper : fixHelper /*fix for mozilla click trigger*/,
		});
		/* Close SubCategory popup */
		$(".modal-close,.btn-grey").on('click.popup', function() {
			$(".modal-popupWrp").hide();
			$(".modal-popup").slideUp();
			$(".modal-popup").find(".modal-bdy input").val("");
			$(".modal-popup i").removeClass("errDsply");
		});
		$(".full").spectrum(
				{
					color : $("#getColor").val(),
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log(color.toHexString());
						var setColor = $("#getColor").attr("value",
								color.toHexString());
						console.log(setColor);
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});
		$(".white").spectrum(
				{
					color : $("#getColor").val(),
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log(color.toHexString());
						var setColor = $("#getColor").attr("value",
								color.toHexString());
						console.log(setColor);
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});

		$(".bBtnColor").spectrum(
				{
					color : $("#getColor").val(),
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log(color.toHexString());
						var setColor = $("#getColor").attr("value",
								color.toHexString());
						console.log(setColor);
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});
		$("#categoryColor").spectrum(
				{
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log("page" + color.toHexString());
						$(this).attr("value", color.toHexString());
						$(".navBar").css("background", color.toHexString());
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});
		$("#categoryFontColor").spectrum(
				{
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log("page" + color.toHexString());
						$(this).attr("value", color.toHexString());
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});

		$('#tmpltType').find(
				'option[value="' + localStorage.getItem('newTmpltType') + '"]')
				.prop("selected", true).trigger('change');

		$("select[name='tempName']").on('change.newsTab', function() {

			var slctd = $(this).find('option:selected').text();

			switch (slctd) {
			case "Scrolling News Template":

				$(".tglImg").attr("src", "images/scrollingNews.jpg");
				break;
			case "Combination News Template":
				$(".tglImg").attr("src", "images/Combinational.jpg");
				break;
			case "News Tile Template":
				$(".tglImg").attr("src", "images/newsTilesTemplate.png");

				break;
			}
		});

		$("select[name='catId']")
				.on(
						'change',
						function() {

							var obj = "select[name='catId']";
							var optn = $(obj).find('option:selected').text();
							var subPage = $("#subPageId");
							if (optn == "Photos" || optn == "Videos") {

								subPage.find('option').not(".js-block-view")
										.hide();
								subPage.find("option.js-block-view").prop(
										"selected", true);
								subPage.find("option.js-block-view").show();

							} else {
								var objSub = "select[name='subPageId']";
								var hsubpage = $(objSub)
										.find('option:selected').text();
								if (hsubpage == 'Block View') {
									subPage.find('option')
											.not(".js-block-view").show();
									subPage.find("option.js-block-view").hide();
									$("#subPageId").find("option:eq(0)").prop(
											"selected", true);
								} else {
									subPage.find('option')
											.not(".js-block-view").show();
									subPage.find("option.js-block-view").hide();
								}
							}
						});

		$("select[name='catId'] option:selected").trigger("change");
	});
	
	function nonfeed_click(){
		$('.errorDsply').remove();
		$('.feed-css').hide();
		$('.nonfeed-css').show();
	}
	
	function feed_click(){
		$('.errorDsply').remove();
		$('.feed-css').show();
		$('.nonfeed-css').hide();
	}
	
	function toggleBlockOption(obj) {

		var optn = $(obj).find('option:selected').text();
		var subPage = $("#subPageId");

		if (optn == "Photos" || optn == "Videos") {

			subPage.find('option').not(".js-block-view").hide();
			subPage.find("option.js-block-view").prop("selected", true);
			subPage.find("option.js-block-view").show();

		} else {
			subPage.find('option').removeProp("selected");
			var hsubpage = $("#hidsubpage").val();

			if (null != hsubpage && hsubpage != "") {
				$("#subPageId").find(
						"option[text='$('#hidsubpage').val()']:selected");
				$("#subPageId option").filter(function() {
					return this.text == $("#hidsubpage").val();
				}).prop('selected', 'selected');

			} else {

				subPage.find("option [text='Select Sub page']").prop(
						"selected", true);
				subPage.find('option').not(".js-block-view").show();
				subPage.find("option.js-block-view").hide();
			}

		}
	}

	function addNewsCategory(methodType) {

		document.addnewsform.methodType.value = methodType;

		var displaytypename = $("select[name='displayTypId'] option:selected")
				.text();
		document.addnewsform.displayType.value = displaytypename;

		var subpagename = $("select[name='subPageId'] option:selected").text();
		document.addnewsform.subPageName.value = subpagename;

		var CateName = $("select[name='catId'] option:selected").text();
		document.addnewsform.catName.value = CateName;

		if (methodType == "addsub") {

			//$('#subcatIdjs').text('');
			var isCurrectURls = true;
			var urlPattern = new RegExp('^(http|https|www)://.*$');

			var urls = $("tbody.addcat tr").find("td:eq(1) input").map(
					function(index, element) {
						if ($(this).val().length) {

							var isCurrect = urlPattern.test($(this).val());
							if (!isCurrect) {
								isCurrectURls = false;
								$(this).parent('div').prev().text(
										'Please enter correct url');
							} else {
								$(this).parent('div').prev().text('');
							}
							return ($(this).val());
						} else {
							$(this).parent('div').prev().text('');
						}
					}).toArray();

			if (isCurrectURls) {
				var subcats = $("tbody.addcat tr").find("td:eq(1) input").map(
						function(index, element) {
							if ($(this).val().length) {
								return ($(this).parents('tr').find(
										'td:eq(0) div').attr('id'));
							}

						}).toArray();

			}

			urls = $.trim(urls);
			//subcats = $.trim(subcats);
			document.addnewsform.subCatURL.value = urls;
			//document.addnewsform.subCatIds.value = subcats;
			//	document.addnewsform.hidsubcatid.value = subcats;
			document.addnewsform.hidsubcaturl.value = urls;

		} else {

			document.addnewsform.subCatIds.value = $('#hidsubcatid').val();
			document.addnewsform.subCatURL.value = $('#hidsubcaturl').val();
			isCurrectURls = true;
		}
		if (isCurrectURls) {

			document.addnewsform.action = "addcatsettings.htm";
			document.addnewsform.method = "post";
			document.addnewsform.submit();
		}

	}

	function clearNewsForm() {

		var r = confirm("Do you really want to clear the details");
		if (r == true) {
			document.addnewsform.catfeedURL.value = "";
			$(".full")
					.spectrum(
							{
								showInput : true,
								showInitial : true,
								showPalette : true,
								color : "#000000",
								palette : [
										[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
												"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
												"rgb(204, 204, 204)",
												"rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
												"rgb(255, 255, 255)" ],
										[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
												"rgb(255, 153, 0)",
												"rgb(255, 255, 0)",
												"rgb(0, 255, 0)",
												"rgb(0, 255, 255)",
												"rgb(74, 134, 232)",
												"rgb(0, 0, 255)",
												"rgb(153, 0, 255)",
												"rgb(255, 0, 255)" ],
										[ "rgb(230, 184, 175)",
												"rgb(244, 204, 204)",
												"rgb(252, 229, 205)",
												"rgb(255, 242, 204)",
												"rgb(217, 234, 211)",
												"rgb(208, 224, 227)",
												"rgb(201, 218, 248)",
												"rgb(207, 226, 243)",
												"rgb(217, 210, 233)",
												"rgb(234, 209, 220)",
												"rgb(221, 126, 107)",
												"rgb(234, 153, 153)",
												"rgb(249, 203, 156)",
												"rgb(255, 229, 153)",
												"rgb(182, 215, 168)",
												"rgb(162, 196, 201)",
												"rgb(164, 194, 244)",
												"rgb(159, 197, 232)",
												"rgb(180, 167, 214)",
												"rgb(213, 166, 189)",
												"rgb(204, 65, 37)",
												"rgb(224, 102, 102)",
												"rgb(246, 178, 107)",
												"rgb(255, 217, 102)",
												"rgb(147, 196, 125)",
												"rgb(118, 165, 175)",
												"rgb(109, 158, 235)",
												"rgb(111, 168, 220)",
												"rgb(142, 124, 195)",
												"rgb(194, 123, 160)",
												"rgb(166, 28, 0)",
												"rgb(204, 0, 0)",
												"rgb(230, 145, 56)",
												"rgb(241, 194, 50)",
												"rgb(106, 168, 79)",
												"rgb(69, 129, 142)",
												"rgb(60, 120, 216)",
												"rgb(61, 133, 198)",
												"rgb(103, 78, 167)",
												"rgb(166, 77, 121)",
												/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
												"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
												"rgb(91, 15, 0)",
												"rgb(102, 0, 0)",
												"rgb(120, 63, 4)",
												"rgb(127, 96, 0)",
												"rgb(39, 78, 19)",
												"rgb(12, 52, 61)",
												"rgb(28, 69, 135)",
												"rgb(7, 55, 99)",
												"rgb(32, 18, 77)",
												"rgb(76, 17, 48)" ] ]
							});

			$(".white")
					.spectrum(
							{
								showInput : true,
								showInitial : true,
								showPalette : true,
								color : "#ffffff",
								palette : [
										[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
												"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
												"rgb(204, 204, 204)",
												"rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
												"rgb(255, 255, 255)" ],
										[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
												"rgb(255, 153, 0)",
												"rgb(255, 255, 0)",
												"rgb(0, 255, 0)",
												"rgb(0, 255, 255)",
												"rgb(74, 134, 232)",
												"rgb(0, 0, 255)",
												"rgb(153, 0, 255)",
												"rgb(255, 0, 255)" ],
										[ "rgb(230, 184, 175)",
												"rgb(244, 204, 204)",
												"rgb(252, 229, 205)",
												"rgb(255, 242, 204)",
												"rgb(217, 234, 211)",
												"rgb(208, 224, 227)",
												"rgb(201, 218, 248)",
												"rgb(207, 226, 243)",
												"rgb(217, 210, 233)",
												"rgb(234, 209, 220)",
												"rgb(221, 126, 107)",
												"rgb(234, 153, 153)",
												"rgb(249, 203, 156)",
												"rgb(255, 229, 153)",
												"rgb(182, 215, 168)",
												"rgb(162, 196, 201)",
												"rgb(164, 194, 244)",
												"rgb(159, 197, 232)",
												"rgb(180, 167, 214)",
												"rgb(213, 166, 189)",
												"rgb(204, 65, 37)",
												"rgb(224, 102, 102)",
												"rgb(246, 178, 107)",
												"rgb(255, 217, 102)",
												"rgb(147, 196, 125)",
												"rgb(118, 165, 175)",
												"rgb(109, 158, 235)",
												"rgb(111, 168, 220)",
												"rgb(142, 124, 195)",
												"rgb(194, 123, 160)",
												"rgb(166, 28, 0)",
												"rgb(204, 0, 0)",
												"rgb(230, 145, 56)",
												"rgb(241, 194, 50)",
												"rgb(106, 168, 79)",
												"rgb(69, 129, 142)",
												"rgb(60, 120, 216)",
												"rgb(61, 133, 198)",
												"rgb(103, 78, 167)",
												"rgb(166, 77, 121)",
												/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
												"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
												"rgb(91, 15, 0)",
												"rgb(102, 0, 0)",
												"rgb(120, 63, 4)",
												"rgb(127, 96, 0)",
												"rgb(39, 78, 19)",
												"rgb(12, 52, 61)",
												"rgb(28, 69, 135)",
												"rgb(7, 55, 99)",
												"rgb(32, 18, 77)",
												"rgb(76, 17, 48)" ] ]
							});

			$(".bBtnColor")
					.spectrum(
							{
								showInput : true,
								showInitial : true,
								showPalette : true,
								color : "#ffffff",
								palette : [
										[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
												"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
												"rgb(204, 204, 204)",
												"rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
												"rgb(255, 255, 255)" ],
										[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
												"rgb(255, 153, 0)",
												"rgb(255, 255, 0)",
												"rgb(0, 255, 0)",
												"rgb(0, 255, 255)",
												"rgb(74, 134, 232)",
												"rgb(0, 0, 255)",
												"rgb(153, 0, 255)",
												"rgb(255, 0, 255)" ],
										[ "rgb(230, 184, 175)",
												"rgb(244, 204, 204)",
												"rgb(252, 229, 205)",
												"rgb(255, 242, 204)",
												"rgb(217, 234, 211)",
												"rgb(208, 224, 227)",
												"rgb(201, 218, 248)",
												"rgb(207, 226, 243)",
												"rgb(217, 210, 233)",
												"rgb(234, 209, 220)",
												"rgb(221, 126, 107)",
												"rgb(234, 153, 153)",
												"rgb(249, 203, 156)",
												"rgb(255, 229, 153)",
												"rgb(182, 215, 168)",
												"rgb(162, 196, 201)",
												"rgb(164, 194, 244)",
												"rgb(159, 197, 232)",
												"rgb(180, 167, 214)",
												"rgb(213, 166, 189)",
												"rgb(204, 65, 37)",
												"rgb(224, 102, 102)",
												"rgb(246, 178, 107)",
												"rgb(255, 217, 102)",
												"rgb(147, 196, 125)",
												"rgb(118, 165, 175)",
												"rgb(109, 158, 235)",
												"rgb(111, 168, 220)",
												"rgb(142, 124, 195)",
												"rgb(194, 123, 160)",
												"rgb(166, 28, 0)",
												"rgb(204, 0, 0)",
												"rgb(230, 145, 56)",
												"rgb(241, 194, 50)",
												"rgb(106, 168, 79)",
												"rgb(69, 129, 142)",
												"rgb(60, 120, 216)",
												"rgb(61, 133, 198)",
												"rgb(103, 78, 167)",
												"rgb(166, 77, 121)",
												/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
												"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
												"rgb(91, 15, 0)",
												"rgb(102, 0, 0)",
												"rgb(120, 63, 4)",
												"rgb(127, 96, 0)",
												"rgb(39, 78, 19)",
												"rgb(12, 52, 61)",
												"rgb(28, 69, 135)",
												"rgb(7, 55, 99)",
												"rgb(32, 18, 77)",
												"rgb(76, 17, 48)" ] ]
							});
			document.addnewsform.catId.value = "";
			document.addnewsform.isNewsTicker.value = "false";
			document.addnewsform.noStories.value = "";
			document.addnewsform.catColor.value = "#000000";
			document.addnewsform.catFontColor.value = "#ffffff";
			document.addnewsform.displayTypId.value = "";
			document.addnewsform.catfeedURL.value = "";
			document.addnewsform.subPageId.value = "";
			$('.newsno').hide();
			$('#newsno').prop('checked', true);
			$('#newsyes').prop('checked', false);
			$('#left').prop('checked', true);
			$('#right').prop('checked', false);

			$("#displayTypId option").removeAttr("selected");
			$("#displayTypId option").filter(function() {
				return this.text == "Select Display type";
			}).prop('selected', 'selected');

			$("#catId option").removeAttr("selected");
			$("#catId option").filter(function() {
				return this.text == "Select Category";
			}).prop('selected', 'selected');

			$("#subPageId option").removeAttr("selected");
			$("#subPageId option").filter(function() {
				return this.text == "Select Sub page";
			}).prop('selected', 'selected');

			$("#successMsg").hide();
			$("#failureMsg").hide();
			$("#catexistsMsg").hide();

			if (document.getElementById('catId.errors') != null) {
				document.getElementById('catId.errors').style.display = 'none';
			}
			if (document.getElementById('catColor.errors') != null) {
				document.getElementById('catColor.errors').style.display = 'none';
			}
			if (document.getElementById('catFontColor.errors') != null) {
				document.getElementById('catFontColor.errors').style.display = 'none';
			}
			if (document.getElementById('catfeedURL.errors') != null) {
				document.getElementById('catfeedURL.errors').style.display = 'none';
			}
			if (document.getElementById('displayTypId.errors') != null) {
				document.getElementById('displayTypId.errors').style.display = 'none';
			}

			if (document.getElementById('subPageId.errors') != null) {
				document.getElementById('subPageId.errors').style.display = 'none';
			}
			if (document.getElementById('noStories.errors') != null) {
				document.getElementById('noStories.errors').style.display = 'none';
			}

		}

	}
	function showModal() {

		var vCatId = $("select[name='catId'] option:selected").val();
		var vHidsubcatId = $('#hidsubcatid').val();

		var vHidsubcatURL = $('#hidsubcaturl').val();

		vHidsubcatURL = $.trim(vHidsubcatURL);
		$('#subcatIdjs').text('');
		$.ajaxSetup({
			cache : false
		})
		$
				.ajax({
					type : "GET",
					url : "fetchnewsmainsubcat.htm",
					data : {
						'newscatid' : vCatId,
						'hidsubcatid' : vHidsubcatId,
						'hidsubcaturl' : vHidsubcatURL
					},

					success : function(response) {
						$('.addcat').html(response);
						var bodyHt = $('body').height();
						var setHt = parseInt(bodyHt);
						$(".modal-popupWrp").show();
						$(".modal-popupWrp").height(setHt);
						$(".modal-popup").slideDown('fast');
						var arr1 = jQuery.makeArray($('#hidsubcatid').val()
								.split(','));
						var arr2 = jQuery.makeArray($('#hidsubcaturl').val()
								.split(','));
						for (i = 0; i <= arr1.length; i++) {
							$("#sub-ctgry-settings tr").find(
									"td div[id='" + arr1[i] + "']").parents(
									'tr').find('td .cntrl-grp input').val(
									arr2[i]);

						}
					},
					error : function(e) {

					}
				});
	}

	/* $("#subCategory").on('click', function() {

		showModal();
		clr();
		setTitle(this);
	}); */
	//subcatcheck();
	/* Close SubCategory popup */
	$(".modal-close,.btn-grey").on('click.popup', function() {
		$(".modal-popupWrp").hide();
		$(".modal-popup").slideUp();
		$(".modal-popup").find(".modal-bdy input").val("");
		$(".modal-popup i").removeClass("errDsply");
	});
</script>

</head>
<body>
	<!--Wrapper div Starts-->
	<div id="wrpr">
		<div class="clear"></div>
		<div class="wrpr-cont relative">
			<div id="slideBtn">
				<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
					src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
			</div>
			<!--Breadcrum div starts-->
			<div id="bread-crumb">
				<ul>
					<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
					<li><a href="welcome.htm">Home</a></li>
					<li class="last">News Settings</li>
				</ul>
			</div>
			<!--Breadcrum div ends-->



			<span class="blue-brdr"></span>
			<!--Content div starts-->
			<div class="content" id="login">
				<!--Left Menu div starts-->
				<div id="menu-pnl" class="split">
					<jsp:include page="leftNavigation.jsp"></jsp:include>
				</div>
				<!--Left Menu div ends-->
				<!--Content panel div starts-->
				<div class="cont-pnl split" id="">
					<div class="cont-block stretch">
						<div class="title-bar">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-news">&nbsp;</span></li>
								<li>News Settings</li>
							</ul>
						</div>
						<div class="tabd-pnl">
							<ul class="nav-tabs">
								<li><a class="brdr-rt " href="newsgeneralsettings.htm"
									title="New General Settings">News General Settings</a></li>
								<li><a href="addcatsettings.htm" class="rt-brdr active"
									title="News Categories">News Categories</a></li>

								<c:if
									test="${requestScope.dispmngcat ne null && !empty requestScope.dispmngcat}">
									<li><a href="managenewscats.htm" class="rt-brdr "
										title="Manage News Categories">Manage News Categories</a></li>
								</c:if>
							</ul>
							<div class="clear"></div>
						</div>
						<form:form name="addnewsform" id="addnewsform"
							commandName="addnewsform">
							<form:hidden path="hidCatName" value="${requestScope.hidCatName}" />
							<form:hidden path="hidDisplaytype"
								value="${requestScope.disptype}" />
							<form:hidden path="hidsubpage" value="${requestScope.hidsubpage}" />
							<form:hidden path="displayType" />
							<form:hidden path="subPageName" />
							<form:hidden path="catName" />
							<form:hidden path="methodType" />
							<form:hidden path="hidIsNewsTicker" id="hidIsNewsTicker" />

							<div class="cont-block rt-brdr">
								<div class="cont-wrp"></div>
								<span class="clear"></span>

								<div class="cont-wrp">

									<c:if
										test="${requestScope.addsuccessMsg ne null && !empty requestScope.addsuccessMsg}">
										<span class="highLightTxt" id="successMsg"> <c:out
												value="${requestScope.addsuccessMsg}" />
										</span>
									</c:if>

									<c:if
										test="${requestScope.catexistsMsg ne null && !empty requestScope.catexistsMsg}">
										<span class="highLightTxt" id="catexistsMsg"> <c:out
												value="${requestScope.catexistsMsg}" />
										</span>
									</c:if>

									<c:if
										test="${requestScope.addfailureMsg ne null && !empty requestScope.addfailureMsg}">
										<span class="highLightErr" id="failureMsg"><c:out
												value="${requestScope.addfailureMsg}" /> </span>
									</c:if>

									<c:if
										test="${requestScope.addcatsubmsg ne null && !empty requestScope.addcatsubmsg}">
										<span class="highLightTxt" id="successMsg"><c:out
												value="${requestScope.addcatsubmsg}" /> </span>
									</c:if>

									<table width="100%" border="0" cellpadding="0" cellspacing="0"
										class="cmnTbl padngTbl">

										<tr class="js-scrolling">
											<td><label class="mand">Type</label></td>
											<td class="cmnTbl mainMenu subMenu"><span
												class="actn-cntrl"><span class=""> <form:radiobutton
															path="isFeed" name="isFeed" id="left" value="true"
															class="isfeed-js" tabindex="6" onclick="feed_click();"
															checked="checked" /> <label for="left">Feed</label>
												</span> <span class="mrgn-left"> <form:radiobutton
															path="isFeed" name="isFeed" id="Right" value="false"
															onclick="nonfeed_click();"
															class="isfeed-js" tabindex="7" /> <label for="right">Non-Feed</label>
												</span></span></td>

										</tr>
										<tr class="nonfeed-css">
											<td><label class="mand"> Category Name</label></td>
											<td class="cmnTbl mainMenu subMenu"><form:errors
													cssClass="errorDsply" path="nonfeedCatName"></form:errors>
												<div class="cntrl-grp">
													<form:input path="nonfeedCatName" class="inputTxtBig"></form:input>
												</div></td>
										</tr>
										<tr class="feed-css">
											<td><label class="mand">Category</label></td>

											<td width="60%" class="cmnTbl"><form:errors
													cssClass="errorDsply" path="catId"></form:errors>
												<div class="cntrl-grp zeroBrdr">


													<form:select path="catId" class="slctBx" tabindex="1">
														<option selected="selected" value="0">Select
															Category</option>
														<c:forEach items="${sessionScope.newscatlst}" var="link">
															<c:choose>
																<c:when test="${null ne catId and catId eq link.catId }">
																	<option selected="selected" value="${link.catId}">${link.catName}</option>
																</c:when>
																<c:otherwise>
																	<option value="${link.catId}">${link.catName}</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</form:select>
												</div></td>
										</tr>


										<!--  <tr>
											<td align="left"><label> Sub Category </label></td>
											<td><input type="button" value="Association"
												class="btn-blue" id="subCategory" name="subCategory"
												disabled="disabled" onclick="showModal()" /></td>

											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>-->

										<tr>
											<td><label class="mand">Background Color</label></td>
											<td class="cmnTbl"><form:errors cssClass="errorDsply"
													path="catColor"></form:errors> <form:input class="full"
													id="categoryColor" path="catColor" tabindex="2" /></td>
										</tr>

										<tr>
											<td><label class="mand">Font Color</label></td>
											<td class="cmnTbl"><form:errors cssClass="errorDsply"
													path="catFontColor"></form:errors> <form:input
													class="white" id="categoryFontColor" path="catFontColor"
													tabindex="2" /></td>
										</tr>

										<tr>
											<td><label class="mand">Back Button Color</label></td>
											<td class="cmnTbl"><form:errors cssClass="errorDsply"
													path="BackButtonColor"></form:errors> <form:input
													class="bBtnColor" id="backBtntColor" path="BackButtonColor"
													tabindex="2" /></td>
										</tr>

										<tr class="js-combinational feed-css">

											<td><label class="mand">Category Display</label></td>

											<td class="cmnTbl mainMenu subMenu"><form:errors
													cssClass="errorDsply" path="displayTypId">
												</form:errors>
												<div class="cntrl-grp zeroBrdr">

													<form:select path="displayTypId" class="slctBx"
														id="displayTypId" tabindex="3">
														<option selected="selected" value="0">Select
															Display type</option>

														<c:forEach items="${sessionScope.newstypelst}" var="link">
															<option value="${link.displayTypId}">${link.displayType}</option>
														</c:forEach>
													</form:select>
												</div></td>

										</tr>


										<tr>
											<td><label class="mand">URL</label></td>
											<td class="cmnTbl mainMenu subMenu"><form:errors
													cssClass="errorDsply" path="catfeedURL"></form:errors>
												<div class="cntrl-grp">
													<form:input type='text' path="catfeedURL" id="catfeedURL"
														class="inputTxtBig" tabindex="4" />
												</div></td>
										</tr>
										<tr class="cntrInput feed-css">

											<td><label class="mand">Sub Page</label></td>
											<td class="cmnTbl"><form:errors cssClass="errorDsply"
													path="subPageId"></form:errors>
												<div class="cntrl-grp zeroBrdr">

													<form:select path="subPageId" class="slctBx" id="subPageId"
														tabindex="5">

														<option selected="selected" value="0">Select Sub
															page</option>
														<c:forEach items="${sessionScope.subpagelst}" var="slink">

															<c:choose>
																<c:when test="${slink.subPageName eq 'Block View'}">
																	<option value="${slink.subPageId}"
																		class="js-block-view">${slink.subPageName}</option>

																</c:when>
																<c:otherwise>

																	<option value="${slink.subPageId}">${slink.subPageName}</option>
																</c:otherwise>


															</c:choose>



														</c:forEach>
													</form:select>
												</div></td>
										</tr>

										<tr class="js-scrolling">
											<td><label class="mand">Default</label></td>
											<td class="cmnTbl mainMenu subMenu"><span
												class="actn-cntrl"><span class=""> <form:radiobutton
															path="isDefault" name="isDefault" id="left" value="true"
															class="" checked="checked" tabindex="6" /> <label
														for="left">Yes</label>
												</span> <span class="mrgn-left"> <form:radiobutton
															path="isDefault" name="isDefault" id="Right"
															value="false" class="" tabindex="7" /> <label
														for="right">No</label>
												</span></span></td>

										</tr>

										<tr class="js-scrolling feed-css">
											<td><label class="mand">News Ticker</label></td>
											<td class="cmnTbl mainMenu subMenu"><span
												class="actn-cntrl"><span class=""> <form:radiobutton
															path="isNewsTicker" name="isNewsTicker" id="newsyes"
															value="true" class="" tabindex="8" /> <label for="left">Yes</label>
												</span> <span class="mrgn-left"> <form:radiobutton
															path="isNewsTicker" name="isNewsTicker" id="newsno"
															value="false" class="" checked="checked" tabindex="9" />
														<label for="right">No</label>
												</span></span></td>

										</tr>

										<tr class="newsno feed-css">
											<td class="textv"><label class="mand noStories">Number
													of Stories</label></td>
											<td class="cmnTbl mainMenu subMenu noStories"><form:errors
													cssClass="errorDsply" path="noStories"></form:errors>
												<div class="cntrl-grp noStories">
													<form:input type='text' path="noStories" visible="false"
														class="inputTxtBig" tabindex="10"
														onkeypress="return isNumberKey(event)" maxlength="1" />
												</div></td>
										</tr>

										<tr class="cntrInput">
											<td>&nbsp;</td>
											<td class="cmnTbl"><input type="submit" name="button"
												id="button" value="Add Category" class="btn-blue"
												onclick="addNewsCategory('addmaincat')" tabindex="11" /> <input
												type="button" value="Reset" class="btn-grey"
												onclick="clearNewsForm()" tabindex="12" /></td>
										</tr>
									</table>
								</div>
							</div>


							<div class="cont-block">
								<div class="title-bar">
									<ul class="title-actn">
										<li class="title-icon"><span class="icon-iphone">&nbsp;</span></li>
										<li>Preview</li>
									</ul>
									<span class="clear"></span>
								</div>
								<div class="cont-wrp">
									<table width="100%" class="cmnTbl padngTbl">


										<tr>
											<td width="40%"><label>Template Type</label></td>


											<td width="60%" class="cmnTbl"><form:errors
													cssClass="errorDsply" path="tempName"></form:errors>
												<div class="cntrl-grp zeroBrdr">

													<form:select path="tempName" class="slctBx" id="tempName">

														<option selected="selected" value="0">Select
															Template</option>
														<c:forEach items="${sessionScope.newstemplateslst}"
															var="link">

															<option value="${link.newsTempleteID}">${link.tempName}</option>

														</c:forEach>
													</form:select>

												</div></td>

										</tr>

									</table>
									<div id="iphone-preview" class="hideOverflow">
										<!--Iphone Preview For Login screen starts here-->
										<div id="iphone-welcome-preview">
											<div class="iphone-status-bar"></div>
											<div class="navBar iphone">
												<table class="titleGrd" border="0" cellpadding="0"
													cellspacing="0" width="100%">
													<tbody>
														<tr>
															<td width="19%"><img src="images/hamburger.png"
																alt="back" height="26" width="26"></td>
															<td class="genTitle" width="54%"><img id="bannerImg"
																width="50" height="47" alt="upload"
																src="${sessionScope.newsbannerimage}" /></td>
															<td align="center" width="27%"></td>
														</tr>
													</tbody>
												</table>
											</div>
											<div>
												<img src="images/scrollingNews.jpg" width="320" height="460"
													alt="News" class="tglImg" />
											</div>
										</div>
									</div>
								</div>
							</div>


						</form:form>




					</div>
				</div>
				<!--Content panel div ends-->
			</div>
			<!--Content div ends-->
		</div>
	</div>
	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/HubCiti/images/popupClose.png" class="closeIframe"
				alt="close"
				onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span
				id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
			allowtransparency="yes" width="100%" style="background-color: White">
		</iframe>
	</div>

	<!-- News Sub Category Module Display  -->
	<div class="modal-popupWrp">
		<div class="modal-popup subCtgryModal">
			<div class="modal-hdr">
				<a title="close" class="modal-close">x</a>
				<h3>
					<span></span>Sub Category Settings
				</h3>
				<span class="highLightTxt" id="subcatIdjs"></span>
			</div>



			<div class="modal-bdy subcatmodal">

				<table cellpadding="0" cellspacing="0" width="100%"
					class="grdTbl  mrgnTop_small fullBrdr" id="sub-ctgry-settings">
					<thead>
						<tr class="tblHdr">
							<!-- <th width="10%"><input type="checkbox" id="chkAll" /></th> -->
							<th width="35%">Sub Category Name</th>
							<th width="55%">Feed URL</th>
						</tr>
					</thead>
					<tbody class="addcat">
					</tbody>
				</table>
			</div>
			<div class="modal-ftr">
				<p align="right">
					<input type="button" class="btn-blue subsave" value="Save"
						name="button" onclick="addNewsCategory('addsub')"> <input
						type="button" class="btn-grey" value="Cancel" id="" name="Cancel"
						onclick="cancel()">
				</p>
			</div>
		</div>
	</div>

</body>
<script type="text/javascript">
	configureMenu("newssettings");
</script>
</html>

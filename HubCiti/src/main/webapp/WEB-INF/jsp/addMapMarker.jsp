<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-min.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script src="/HubCiti/scripts/ckeditor/ckeditor.js"></script>
<script src="/HubCiti/scripts/ckeditor/config.js"></script>
<script type="text/javascript" src="scripts/web.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<script type="text/javascript">

	$(document).ready(function() {
		var logisticId = $("#logisticId").val();
		if(null !== logisticId && "" !== logisticId) {			
			document.title = "Add / Update Marker Details";
			$("#logisticsTitle").text("Add / Update Marker Details");
			$(".savelog").val("Update");
		}
		
		var retailerId = $("#retailerId").val();
		var locId = $("#hiddenLocationId").val();		
		
		if(null != retailerId && "" != retailerId) {
			getLogRetailerLocs(retailerId, locId);	
		} 
		
		
		/* $("#startDate").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/icon-calendar-active.png'
		});
		$('.ui-datepicker-trigger').css("padding-left", "5px");

		$("#endDate").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/icon-calendar-active.png'
		});
		$('.ui-datepicker-trigger').css("padding-left", "5px");
		
		$('#trgrUpldLogisticsImg').bind('change', function() {
			var imageType = document.getElementById("trgrUpldLogisticsImg").value;

			if (imageType != '') {
				var checkbannerimg = imageType.toLowerCase();
				if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
					alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
					return false;
				} else {
					$("#logisticDetails").ajaxForm({
						success : function(response) {

							var imgRes = response.getElementsByTagName('imageScr')[0].firstChild.nodeValue

							var substr = imgRes.split('|');
							if (substr[0] == 'maxSizeImageError') {
								$('#maxSizeImageError').text("Image Dimension should not exceed Width: 800px Height: 600px");
							} else {
								openIframePopup(
										'ifrmPopup',
										'ifrm',
										'/HubCiti/cropImage.htm',
										100,
										99.5,
										'Crop Image');
							}

						}
					}).submit();
				}
			}

		}); */
		
		$('#markerIcon').bind('change', function() {
			var imageType = document.getElementById("markerIcon").value;

			if (imageType != '') {
				var checkbannerimg = imageType.toLowerCase();
				if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
					alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
					document.getElementById("markerIcon").value = '';
					return false;
				} else {
					$("#editIcon").text('');
				}
			}
		});
	
	});
	
	function isLatLong(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
				|| charCode == 45 || charCode == 43)
			return true;
		return false;
	}
	
	function logRetNameAutocomplete(retaName) {
		$("#retailerName").autocomplete({
			minLength : 3,
			delay : 200,
			source : '/HubCiti/displaylogretnames.htm',
			select : function(event, ui) {
	
				if (ui.item.value == "No Records Found") {
					$("#retailerName").val("");
				} else {
					$("#retailerName").val(ui.item.rname);
					$("#retailerId").val(ui.item.retId);
					getLogRetailerLocs(ui.item.retId, "");
				}
				return false;
			}
		});
	}
	
	function getLogRetailerLocs(retId, selectedLocId) {
	
		$('#locationId').find('option:not(:first)').remove();

		$.ajaxSetup({
			cache : false
		});

		$.ajax({
			type : "GET",
			url : "displaylogretLoc.htm",
			data : {
				'retId' : retId

			},

			success : function(response) {

				var rLocations = response;

				var objs = JSON.parse(rLocations);

				slctbox = $('#locationId');
				$('#locationId').find('option:not(:first)').remove();
				if (objs != null && objs != 'undefined') {
					for ( var i = 0; i < objs.length; i++) {
						slctbox.append(new Option(objs[i].address, objs[i].retLocId));
					}
					
					if("" !== selectedLocId) {
						$("#locationId option[value='" + selectedLocId + "']").prop('selected', 'selected');
					}
					
				}
			},
			error : function(e) {
				alert('Error occured while fetching retailer locations');
			}
		});

	}
	
	function addMarker(functionality) {
		
		var submitFlag = true;
		var requiredInputs = "";
		var editIcon = $("#editIcon").text();
		
		if(document.logisticDetails.markerName.value == '') {
			submitFlag = false;
			requiredInputs += "Marker Name.<br>";
		}
		
		if(document.logisticDetails.markerImageFile.value == '' && editIcon == '') {
			submitFlag = false;
			if(requiredInputs == "") {
				requiredInputs += "Marker Icon.<br>";
			} else {
				requiredInputs += "Marker Icon.<br>";
			}
			
		}
		
		if(document.logisticDetails.markerLatitude.value == '') {
			submitFlag = false;
			if(requiredInputs == "") {
				requiredInputs += "Latitude.<br>";
			} else {
				requiredInputs +="Latitude.<br>";
			}
		}
		
		if(document.logisticDetails.markerLongitude.value == '') {
			submitFlag = false;
			if(requiredInputs == "") {
				requiredInputs += "Longitude.<br>";
			} else {
				requiredInputs +="Longitude.<br>";
			}
		} 
		
		if(submitFlag) {	
			document.logisticDetails.functionality.value = functionality;
			document.logisticDetails.action = "addmarker.htm";
			document.logisticDetails.method = "POST";
			document.logisticDetails.submit();
		} else {
			//alert("Please enter the required " + requiredInputs + " fields");
			custAlertShowModal("<b>Please enter the required fields:</b><br><br>" + requiredInputs,1);
		}
	}
	
	function editMarker(markerId, markerName, markerImage, lat, longi, anythingPg) {
		
		document.logisticDetails.markerId.value = markerId.replace(new RegExp("&quot", "g"), '"');
		$("#markerName").val(markerName.replace(new RegExp("&quot", "g"), '"'));
		$("#editIcon").text(markerImage);
		$("#markerLatitude").val(lat);
		$("#markerLongitude").val(longi);
		
		$("#markerLongitude").val(longi);
		$('#anythingPageId').val(anythingPg);
		
		$(".add").val("Update Marker");
		$(".add").attr("onclick", "addMarker('edit');");
		
	}
	
	function deleteMarker(markerId) {
		r = confirm("Are you sure you want to delete this marker !");
		if(r) {
			document.logisticDetails.markerId.value = markerId.replace(new RegExp("&quot", "g"), '"');
			document.logisticDetails.functionality.value = "delete";
			document.logisticDetails.action = "addmarker.htm";
			document.logisticDetails.method = "POST";
			document.logisticDetails.submit();
		}
	} 

	function saveLogistics() {
		document.logisticDetails.action = "savelogistics.htm";
		document.logisticDetails.method = "POST";
		document.logisticDetails.submit();
	}

	
	
	var vAnythingPageId = document.logisticDetails.hiddenAnythingPageId.value;
	if (null != vAnythingPageId && "" != vAnythingPageId) {
		$('#anythingPageId').val(vAnythingPageId).change();
	}

	function anythingPageChange() {
		document.logisticDetails.hiddenAnythingPageId.value = $('#anythingPageId').val();
	}
	
	function backButton() {
		document.logisticDetails.action = "displaylogistics.htm";
		document.logisticDetails.method = "GET";
		document.logisticDetails.submit();
	}
	
</script>

<div id="wrpr">

	<span class="clear"></span>
	
  	<div class="wrpr-cont relative">
  	
	    <div id="slideBtn">
	    	<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
	    </div>
	 	
	    <div id="bread-crumb">
			<ul>
				<li class="scrn-icon"><span class="icon-home">&nbsp;</span>
				</li>
				<li><a href="welcome.htm">Home</a>
				</li>
				<li><a href="displaylogistics.htm">Interactive Map</a>
				</li>
				<li class="last" id="logisticsTitle">Add Interactive Map Marker Details</li>
			</ul>
		</div>
		
    	<span class="blue-brdr"></span>

    	<div class="content" id="">

	      	<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>

      		<div class="cont-pnl split" id="equalHt">
      			
       			<form:form name="logisticDetails" id="logisticDetails" commandName="logisticDetails" enctype="multipart/form-data"
					action="uploadlogisticsimg.htm">
				<form:hidden path="logisticId" />
				<form:hidden path="oldLogisticImageName"/>
				<form:hidden path="hiddenLocationId" />
				<form:hidden path="retailerId" />
				<form:hidden path="markerId" />				
				<form:hidden path="markerImageName" />
				<form:hidden path="markerImagePath" />
				<form:hidden path="functionality" />
				<form:hidden path="lowerLimit" value="${requestScope.lowerLimit}"/>
				<form:hidden path="searchLogisticName" value="${requestScope.searchLogisticName}"/>
				<form:hidden path="hiddenAnythingPageId" />
      		
 				<div class="cont-block rt-brdr stretch">
 				
					<div class="title-bar">
            			<ul class="title-actn">
             	 			<li class="title-icon"><span class="icon-map">&nbsp;</span></li>
              				<li>Interactive Map Marker Details</li>
            			</ul>
          			</div>		
          				
          			<div class="cont-wrp">
							
							<table width="100%" cellspacing="0" cellpadding="0" border="0" class="brdrlsTbl">
			            		<tbody>
									
			                		
									<tr class="subHdr">
					                  	<td valign="bottom" align="left" colspan="4">Marker Details</td>
					                </tr>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrlsTbl">
				             			<tr>
						                  	<td width="20%"><label class="mand">Marker Name</label></td>
						                  	<td width="30%">
						                  		<div class="cntrl-grp">
						                      	<form:input path="markerName" style="height: 22px;" class="inputTxtBig" maxlength="25" tabindex="7"/>
						                    	</div></td>
						                  	<td width="20%" align="left"><label class="mand">Marker Icon</label></td>
						                  	<td width=30%>
						                		<div class="cntrl-grp">
													<form:input type="file" class="inputTxtBig"	path="markerImageFile" style="height: 22px;" tabindex="8" id="markerIcon"/>
												</div>												
												<span class="lbl-editIcon" id="editIcon"></span>
							                </td>
						                </tr>
						                <tr>
						                  	<td valign="top"><label class="mand">Latitude</label></td>
						                  	<td><div class="cntrl-grp">
						                    	<form:input path="markerLatitude" style="height: 22px;" class="inputTxtBig" maxlength="15" tabindex="9" onkeypress="return isLatLong(event)"/>
						                  		</div></td>
						                  	<td valign="top" align="left"><label class="mand">Longitude</label></td>
						                  	<td><div class="cntrl-grp">
						                    	<form:input path="markerLongitude" style="height: 22px;" class="inputTxtBig" maxlength="15" tabindex="10" onkeypress="return isLatLong(event)"/>
						                  		</div></td>
						                	</tr>




										<tr>
											<td align="left" valign="top"><label>Anything Page</label></td>
											<td colspan="2"><form:select path="anythingPageId" name="anythingPageId" id="anythingPageId"
													class="slctBx textareaTxt slctBx_Sm" tabindex="11"
													onchange="anythingPageChange();">
													
													<form:option value="">Select Anything Page</form:option>
													<c:forEach items="${sessionScope.anythingPageMarkerList}" var="item">
														<form:option value="${item.hcAnythingPageId }">${item.anythingPageTitle}</form:option>
													</c:forEach>
												</form:select>
												<a href="setupanythingscreen.htm"><img src="images/btn_add.png" width="24" height="24" alt="add" class="addImg" title="Add New AnyThing Page"/></a>
												<div class="info">Allowed Anything page Type (PDF, Image & WebSite) </div>
												</td>
										</tr>


									</table>
				           	 		
			            			<div class="cntrInput mrgnTop" align="right">
			                			<input type="button" class="btn-blue add" value="Add Marker" onclick="addMarker('add');" functionality="add" tabindex="12"/>
			              			</div>
			              			
			              			<div class="relative">
			              			
			                			<div class="hdrClone"></div>
			                			
			                			<div class="scrollTbl tblHt mrgnTop">
			                 				<table width="100%" cellspacing="0" cellpadding="0" border="0" id="evntTbl" class="grdTbl clone-hdr fxdhtTbl">
						                  		<thead>
						                    		<tr class="tblHdr">
						                      			<th width="40%">Marker Name</th>
						                      			<th width="19%">Latitude</th>
						                      			<th width="19%">Longitude</th>
						                      			<th width="10%">Icon</th>
						                      			<th width="12%">Action</th>
						                    		</tr>
						                  		</thead>
							                    <tbody class="scrollContent">
							                    	<c:forEach items="${sessionScope.markers}" var="marker">
								                    	<tr>
									                        <td><div class="cell-wrp">${marker.markerName} </div></td>
									                        <td>${marker.markerLatitude}</td>
									                        <td>${marker.markerLongitude}</td>
									                        <td><img src="${marker.markerImagePath}" width="26" height="26" nameImg="${marker.markerImageName}"/></td>
									                        <td>
									                        	<a title="edit" href="#">
									                        		 <img height="20" width="20" class="actn-icon" alt="edit" src="images/edit_icon.png" 
									                        		 onclick='editMarker("<c:out value="${fn:replace(marker.markerId, '\"', '&quot')}"/>","<c:out value="${fn:replace(marker.markerName, '\"', '&quot')}"/>","${marker.markerImageName}","${marker.markerLatitude}","${marker.markerLongitude}","${marker.anythingPageId}")'/></a> 
									                        		<!-- <img height="20" width="20" class="actn-icon" alt="edit" src="images/edit_icon.png" onclick="editMarker('${marker.markerId}','${marker.markerName}','${marker.markerImageName}','${marker.markerLatitude}','${marker.markerLongitude}','3196')"/></a> -->
									                        	<a title="delete" href="#">
									                        		<img height="20" width="20" class="actn-icon" alt="delete" src="images/delete_icon.png" onclick='deleteMarker("<c:out value="${fn:replace(marker.markerId, '\"', '&quot')}"/>")'/></a>
									                        </td>
								                      	</tr>
							                    	</c:forEach>							                      	
							                    </tbody>
											</table>
										</div>                
									</div>
					  				<!-- <div class="cntrInput mrgnTop" align="right">
		                				<input type="button" class="btn-blue savelog" value="Save" onclick="saveLogistics();" tabindex="13">
		              				</div> -->
		    					</tbody>
							</table>
							<div class="cntrInput mrgnTop" align="right">
			                			<input type="button" class="btn-blue" value="Back" onclick="backButton();" functionality="back" tabindex="12"/>
			              			</div>
          			</div>
        		</div>
        		</form:form>
      		</div>	
      	</div>
  	</div>
  	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/HubCiti/images/popupClose.png" class="closeIframe"
				alt="close"
				onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span
				id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
			height="100%" allowtransparency="yes" width="100%"
			style="background-color: White"> </iframe>
	</div>
</div>
<script type="text/javascript">
	configureMenu("setupLogisticsMap");	
</script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.js"></script>
<script type="text/javascript" src="scripts/jquery-ui-min.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script src="/HubCiti/scripts/ckeditor/ckeditor.js"></script>
<script src="/HubCiti/scripts/ckeditor/config.js"></script>

<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">

<script type="text/javascript">
	$(document)
			.ready(
					function() {

						var stHt = $(".cont-pnl").height();
						$("#menu-pnl").height(stHt);

						var existingretId = $('#retailerId').val();
						var selectedLocIds = $('#tempretids').val();
						if (existingretId != undefined) {
							if (selectedLocIds == null) {
								getLogRetailerLocs(existingretId, "");
							} else {
								getLogRetailerLocs(existingretId,
										selectedLocIds)
							}

						}

						$('#retailerLocIds option')
								.click(
										function() {
											var totOpt = $('#retailerLocIds option').length;
											var totOptSlctd = $('#retailerLocIds option:selected').length;
											if (totOpt == totOptSlctd) {
												$('#chkAllLoc').prop('checked', 'checked');
											} else {
												$('#chkAllLoc').removeAttr('checked');
											}
										});
						$("#retailerLocIds")
								.change(
										function() {
											var totOpt = $('#retailerLocIds option').length;
											var totOptSlctd = $('#retailerLocIds option:selected').length;
											if (totOpt == totOptSlctd) {
												$('#chkAllLoc').prop('checked', 'checked');
											} else {
												$('#chkAllLoc').removeAttr('checked');
											}
										});

						CKEDITOR.config.uiColor = '#FFFFFF';
						CKEDITOR.replace('shortDescription',
								{
									extraPlugins : 'onchange',
									width : "100%",
									toolbar : [
											/* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
											{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
											{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
											'/',
											{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
											{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
											{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
											{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
											'/',
											{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
											{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
											{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
											{
												name : 'basicstyles',
												items : [ 'Bold', 'Italic',
														'Underline' ]
											},
											{
												name : 'paragraph',
												items : [ 'JustifyLeft',
														'JustifyCenter',
														'JustifyRight',
														'JustifyBlock' ]
											},
											{
												name : 'colors',
												items : [ 'BGColor' ]
											},
											{
												name : 'paragraph',
												items : [ 'Outdent', 'Indent' ]
											},
											{
												name : 'links',
												items : [ 'Link', 'Unlink' ]
											},
											'/',
											{
												name : 'styles',
												items : [ 'Styles', 'Format' ]
											},
											{
												name : 'tools',
												items : [ 'Font', 'FontSize',
														'RemoveFormat' ]
											} ],
									removePlugins : 'resize'
								});

						CKEDITOR.config.uiColor = '#FFFFFF';
						CKEDITOR.replace('longDescription',
								{
									extraPlugins : 'onchange',
									width : "100%",
									toolbar : [
											/* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
											{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
											{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
											'/',
											{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
											{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
											{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
											{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
											'/',
											{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
											{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
											{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
											{
												name : 'basicstyles',
												items : [ 'Bold', 'Italic',
														'Underline' ]
											},
											{
												name : 'paragraph',
												items : [ 'JustifyLeft',
														'JustifyCenter',
														'JustifyRight',
														'JustifyBlock' ]
											},
											{
												name : 'colors',
												items : [ 'BGColor' ]
											},
											{
												name : 'paragraph',
												items : [ 'Outdent', 'Indent' ]
											},
											{
												name : 'links',
												items : [ 'Link', 'Unlink' ]
											},
											'/',
											{
												name : 'styles',
												items : [ 'Styles', 'Format' ]
											},
											{
												name : 'tools',
												items : [ 'Font', 'FontSize',
														'RemoveFormat' ]
											} ],
									removePlugins : 'resize'
								});

						$("#startDate").datepicker({
							showOn : 'both',
							buttonImageOnly : true,
							buttonText : 'Click to show the calendar',
							buttonImage : 'images/icon-calendar-active.png'
						});
						$('.ui-datepicker-trigger').css("padding-left", "5px");

						$("#endDate").datepicker({
							showOn : 'both',
							buttonImageOnly : true,
							buttonText : 'Click to show the calendar',
							buttonImage : 'images/icon-calendar-active.png'
						});
						$('.ui-datepicker-trigger').css("padding-left", "5px");
						
					});

	function logRetNameAutocomplete(retaName) {
		$("#retailerName").autocomplete({
			minLength : 3,
			delay : 200,
			source : '/HubCiti/displaylogretnames.htm',
			select : function(event, ui) {

				if (ui.item.value == "No Records Found") {
					$("#retailerName").val("");
				} else {
					$("#retailerName").val(ui.item.rname);
					$("#retailerId").val(ui.item.retId);
					getLogRetailerLocs(ui.item.retId, "");
				}
				return false;
			}
		});
	}

	function getLogRetailerLocs(retId, selectedLocId) {

		$('#retailerLocIds').find('option').remove();

		$.ajaxSetup({
			cache : false
		});

		$.ajax({
			type : "GET",
			url : "displaylogretLoc.htm",
			data : {
				'retId' : retId

			},

			success : function(response) {

				var rLocations = response;

				var objs = JSON.parse(rLocations);

				slctbox = $('#retailerLocIds');
				$('#retailerLocIds').find('option:not(:first)').remove();
				if (objs != null && objs != 'undefined') {
					for (var i = 0; i < objs.length; i++) {
						slctbox.append(new Option(objs[i].address,
								objs[i].retLocId));
					}

					if ("" !== selectedLocId) {

						var arr = selectedLocId.split(',');
						jQuery.each(arr, function(i, val) {
							$("#retailerLocIds option[value='" + val + "']")
									.prop('selected', 'selected');
						});
					}

				}
				var totOpt = $('#retailerLocIds option').length;
				var totOptSlctd = $('#retailerLocIds option:selected').length;
				if (totOpt == totOptSlctd) {
					$('#chkAllLoc').prop('checked', 'checked');
				} else {
					$('#chkAllLoc').removeAttr('checked');
				}
			},
			error : function(e) {
				alert('Error occured while fetching retailer locations');
			}
		});

	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("retailerLocIds");
		if (checked == true) {
			for (var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for (var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}

	function updateSpecialOfferPageScreen() {
		document.screenSettingsForm.action = "savemakeyourownspecialofferpage.htm";
		document.screenSettingsForm.method = "POST";
		document.screenSettingsForm.submit();
	}
</script>

<div id="wrpr">
	<div class="clear"></div>
	<div class="wrpr-cont relative">
		<div id="slideBtn">
			<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
				src="images/slide_off.png" width="11" height="28" alt="btn_off" />
			</a>
		</div>
		<div id="bread-crumb">
			<ul>
				<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
				<li><a href="welcome.htm">Home</a></li>
				<li><a href="displayspecialofferpages.htm">Special offer
						Page</a></li>
				<li class="last">Update Special Offer Page</li>
			</ul>
		</div>
		<span class="blue-brdr"></span>
		<div class="content" id="login">
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>
			<div class="cont-pnl split" id="">
				<div class="cont-wrp rt-brdr">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-aboutus">&nbsp;</span>
							</li>
							<li>Edit Special Offer Page</sup></li>
						</ul>
					</div>
					<div class="cont-wrphidoverflow">
						<form:form name="screenSettingsForm" id="screenSettingsForm"
							commandName="screenSettingsForm" enctype="multipart/form-data"
							action="uploadimg.htm">
							<form:hidden path="viewName" value="editmakeyourownspecailofferpage" />
							<form:hidden path="tempretids" />
							<form:hidden path="pageId"/>
							<form:hidden path="retailerId" />
							<table width="100%" border="0" cellpadding="0" cellspacing="0"
								class="cmnTbl">
								<tr>
									<td width="20%"><label class="mand">Page Title</label></td>
									<td width="30%"><form:errors cssClass="errorDsply"
											path="pageTitle" />
										<div class="cntrl-grp">
											<form:input path="pageTitle" type="text" id="pageTitle" tabindex="1"
												cssClass="inputTxtBig" maxlength="25" />
										</div></td>
									<td width="15%"></td>
									<td width="35%"></td>
								</tr>
								<tr>
									<td><label class="mand">Upload Image</label></td>
									<td><form:errors cssClass="errorDsply"
											path="logoImageName"></form:errors> <label> <img
											id="uploadImg" width="70" height="70" alt="upload"
											src="${sessionScope.makeSpecialOfferImage}">
									</label> <span class="topPadding cmnRow"> <label for="trgrUpld">
												<input type="button" value="Upload" id="trgrUpldBtn"
												class="btn trgrUpld" title="Upload Image File"> <form:hidden
													path="logoImageName" id="logoImageName" /> <span
												class="instTxt nonBlk"></span> <form:input type="file"
													class="textboxBig" id="trgrUpld" path="logoImage" tabindex="2"
													onchange="checkBannerSize(this);" />
										</label>
									</span><label id="maxSizeImageError" class="errorDsply maxSizeImageError"></label></td>
								</tr>
								<tr>
									<td><label class="mand">Retailer Name</label></td>
									<td><form:errors cssClass="errorDsply" path="retailerName"></form:errors>
										<div class="cntrl-grp">
											<form:input path="retailerName" class="loadingInput" tabindex="3"
												onkeypress='logRetNameAutocomplete(retailerName);'
												maxlength="30" />
										</div></td>

								</tr>
								<tr>
									<td><label class="mand">Locations</label></td>
									<td><form:errors path="retailerLocIds"
											cssStyle="color:red">
										</form:errors>
										<div class="cntrl-grp">

											<select id="retailerLocIds" name="retailerLocIds"
												tabindex="4" class="slctBx textareaTxt" multiple="multiple">
											</select> <br />
										</div> <form:label path="retailerLocIds">Hold Ctrl to select more than one location</form:label>

									</td>
									<td align="left" valign="top" class="Label" colspan="2"><label>
											<input type="checkbox" name="chkAllLoc" id="chkAllLoc" tabindex="5"
											onclick="SelectAllLocation(this.checked);" /> Select All
											Locations
									</label></td>
								</tr>
								<tr>
									<td valign="top"><label class="mand">Short
											Description</label></td>
									<td colspan="3"><form:errors cssClass="errorDsply"
											path="shortDescription"></form:errors>
										<div class="">
											<form:textarea path="shortDescription"
												class="textareaTxt cstmFix" cols="25" rows="3" tabindex="6"
												maxlength="2000"></form:textarea>
										</div></td>
								</tr>
								<tr>
									<td valign="top"><label class="mand">Long
											Description</label></td>
									<td colspan="3"><form:errors cssClass="errorDsply"
											path="longDescription"></form:errors>
										<div class="">
											<form:textarea path="longDescription"
												class="textareaTxt cstmFix" cols="25" rows="5" tabindex="7"
												maxlength="2000"></form:textarea>
										</div></td>

								</tr>
								<tr>
									<td><span class="mand">Start Date</span>`</td>
									<td><form:errors path="startDate" cssStyle="color:red"></form:errors>
										<div class="cntrl-grp cntrl-dt floatL">
											<form:input path="startDate" id="startDate" type="text"
												tabindex="8" />
										</div></td>
									<td>End Date</td>

									<td><form:errors path="endDate" cssStyle="color:red"></form:errors>
										<div class="cntrl-grp cntrl-dt floatL">
											<form:input path="endDate" id="endDate" type="text"
												tabindex="9" />
										</div></td>

								</tr>
								<tr>

									<td>Start Time</td>
									<td><form:select path="startHH" class="slctSmall"
											name="etHr" tabindex="10">
											<form:options items="${StartHours}" />
										</form:select> Hrs <form:select path="startMM" class="slctSmall"
											name="stMin" tabindex="11">
											<form:options items="${StartMinutes}" />
										</form:select> Mins</td>

									<td>End Time</td>
									<td><form:select path="endHH" class="slctSmall"
											name="etHr" tabindex="12">
											<form:options items="${StartHours}" />
										</form:select> Hrs <form:select path="endMM" class="slctSmall" name="stMin"
											tabindex="13">
											<form:options items="${StartMinutes}" />
										</form:select> Mins</td>
								</tr>
								<tr>
									<td></td>
									<td><input type="button" name="button" id="" value="Update" tabindex="14"
										class="btn-blue" onclick="updateSpecialOfferPageScreen();" /></td>
								</tr>
							</table>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/HubCiti/images/popupClose.png" class="closeIframe"
				alt="close"
				onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span
				id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
			allowtransparency="yes" width="100%" style="background-color: White">
		</iframe>
	</div>
</div>
<script type="text/javascript">
	configureMenu("setupspecialoffer");
</script>

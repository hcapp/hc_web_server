<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$(".hdrClone span:gt(1)").css("text-indent","8px");
	$(".modal-close").on('click.popup', function() {
		$(".modal-popupWrp").hide();
		$(".modal-popup").slideUp();
		$(".modal-popup").find(".modal-bdy input").val("");
		$(".modal-popup i").removeClass("errDsply");
	});
	$(".alrtClose").click(function() {
						$('p.ctgryassoci').hide();
						//$("ctgryassoci").hide();
					});
	/*$(".actn-close").click(function() {
		alert('event alert');
		var vEid = $(this).attr('div.alertBx.id').value;
		alert(vEid);
		$(this).parent('div.alertBx').remove();

	});*/

	$(".actn-close").on("click",function() { 
		var elemnt = $(this).parents('div'); 
		var alrtId = elemnt.attr("id");
		if(alrtId == 'evntClose'){
			$("#"+alrtId).hide();
			/*document.ManageEventForm.eventSearchKey.value=null;
			document.ManageEventForm.action = "manageevents.htm";
			document.ManageEventForm.method = "GET";
			document.ManageEventForm.submit();*/
		}
		else if(elemnt){
		$(this).parent('div.alertBx').hide();
		}else{
			}
	 });	
});

function callNextPage(pagenumber, url) 
{
	document.ManageEventForm.pageNumber.value = pagenumber;
	document.ManageEventForm.pageFlag.value = "true";
	document.ManageEventForm.action = url;
	document.ManageEventForm.method = "get";
	document.ManageEventForm.submit();
}

function associateBands(eventId) 
{
	document.ManageEventForm.hcEventID.value=eventId;
	document.ManageEventForm.eventSearchKey.value="";
	document.ManageEventForm.action = "associatebands.htm";
	document.ManageEventForm.method = "get";
	document.ManageEventForm.submit();
}
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div class="wrpr-cont relative">
		<div id="slideBtn">
			<a href="javascript:void(0);" onclick="revealPanel(this);"
				title="Hide Menu"> <img src="images/slide_off.png" width="11"
				height="28" alt="btn_off" />
			</a>
		</div>
		<div id="bread-crumb">
			<ul>
				<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
				<sec:authorize access="hasRole('ROLE_ADMIN_VIEW')">
					<li><a href="welcome.htm">Home</a></li>
				</sec:authorize>
				<li class="last">Manage Band Events</li>
			</ul>
		</div>
		<span class="blue-brdr"></span>
		<div class="content" id="login">
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>
			<div class="cont-pnl split" id="">
				<div class="cont-block stretch">
					<form:form name="ManageEventForm" commandName="ManageEventForm">
						<form:hidden path="lowerLimit" />
						<input type="hidden" name="pageNumber" />
						<input type="hidden" name="pageFlag" />
						<form:hidden path="hcEventID"/>
						<div class="title-bar">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-bandevents">&nbsp;</span>
								</li>
								<li>Band Events</li>
							</ul>
						</div>
						<div class="tabd-pnl">
							<ul class="nav-tabs">
								<li><a id="mainMenu" href="managebandevents.htm" class="active rt-brdr" >Manage Band
										Events</a></li>
								<!--<li><a id="subMenu" href="displayeventcate.htm"
									class="rt-brdr">Manage Event Category</a></li> -->
							</ul>
							<div class="clear"></div>
						</div>
						<div class="cont-wrp">
							<table width="100%" border="0" cellspacing="0" cellpAdd ing="0"
								class="zerobrdrTbl">
								<tr>
									<c:choose>
										<c:when test="${requestScope.searchEvnt eq 'searchEvnt'}">
											<td width="6%"><label>Search</label></td>
											<td width="20%"><div class="cntrl-grp">
													<form:input type="text" path="eventSearchKey"
														class="inputTxtBig" onkeypress="searchBandEvent(event)" />
												</div> <i class="emptysearh">Please Enter Search keyword</i></td>
											<td width="10%"><a href="javascript:void(0);"><img
													src="images/searchIcon.png" width="20" height="17"
													alt="search" title="Search event" maxlength="30"
													onclick="searchBandEvent('')" /> </a></td>
										</c:when>
										<c:otherwise>
											<c:if
												test="${sessionScope.eventlst ne null && !empty sessionScope.eventlst  }">
												<td width="6%"><label>Search</label></td>
												<td width="20%"><div class="cntrl-grp">
														<form:input type="text" path="eventSearchKey"
															class="inputTxtBig" onkeypress="searchBandEvent(event)" />
													</div> <i class="emptysearh">Please Enter Search keyword</i></td>
												<td width="10%"><a href="javascript:void(0);"><img
														src="images/searchIcon.png" width="20" height="17"
														alt="search" title="Search event" maxlength="30"
														onclick="searchBandEvent('')" /> </a></td>
											</c:if>
										</c:otherwise>
									</c:choose>
									<td width="40%" align="right"><input type="button"
										value="Add Band Event" title="Add Band event" class="btn-blue"
										onclick="window.location.href='addbandevent.htm'" /></td>
								</tr>
							</table>
							<c:if
								test="${sessionScope.responseEvntStatus ne null && !empty sessionScope.responseEvntStatus}">
								<c:choose>
									<c:when test="${sessionScope.responseEvntStatus eq 'SUCCESS' }">
										<div class="alertBx success mrgnTop cntrAlgn">
											<span class="actn-close" title="close"></span>
											<p class="msgBx">
												<c:out value="${sessionScope.responeEvntMsg}" />
											</p>
										</div>
									</c:when>
								</c:choose>
							</c:if>
							<div class="relative">
								<div class="hdrClone"></div>
								<p class="ctgryassoci">
									<a class="alrtClose" href="javascript:void(0);" title="close">x</a>Event
									has been associated to menu. Please deassociate and delete
								</p>
								<c:choose>
									<c:when
										test="${sessionScope.eventlst ne null && !empty sessionScope.eventlst}">
										<div class="scrollTbl tblHt mrgnTop">
											<table width="100%" cellspacing="0" cellpadding="0"
												border="0" id="alertTbl" class="grdTbl clone-hdr fxdhtTbl">
												<thead>
													<tr class="tblHdr">
														<th width="16%">Band Event Name</th>
														<th width="30%">Location</th>
														<th width="15%">Start Date</th>
														<th width="15%">End Date</th>
														<th width="24%">Action</th>
													</tr>
												</thead>
												<tbody class="scrollContent">
													<c:forEach items="${sessionScope.eventlst.eventLst}"
														var="eve">
														<tr>
															<td>${eve.hcEventName}</td>
												
															<td><div class="cell-wrp">${eve.address}, ${eve.city}, ${eve.state},
																${eve.postalCode}</div></td>
															<td>${eve.eventStartDate}</td>
															<td>${eve.eventEndDate}</td>

															<td><a title="edit" href="javascript:void(0);">
																	<img height="20" width="20" class="evt-actn-icon"
																	title="Edit band event" src="images/edit_icon.png"
																	onclick="window.location.href='editbandeventdetails.htm?eventId=${eve.hcEventID}'" />
															</a> <a title="Delete band event" href="javascript:void(0);">
																	<img height="20" width="20" class="evt-actn-icon"
																	alt="delete" src="images/delete_icon.png"
																	onclick="deleteBandEvent(${eve.hcEventID})" />
															</a>
															<a title="Associate Bands" href="javascript:void(0);">
																	<img height="20" width="20" class="evt-actn-icon"
																	alt="associate" src="images/bands_icon.png"
																	onclick="associateBands(${eve.hcEventID})" />
															</a>
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</c:when>
									<c:otherwise>
										<div class="alertBx warning mrgnTop cntrAlgn" id="evntClose">
											<span class="actn-close" title="close"></span>
											<p class="msgBx">No Band Events found</p>
										</div>
									</c:otherwise>
								</c:choose>
							</div>
					</form:form>
				</div>
				<c:if
								test="${sessionScope.eventlst ne null && !empty sessionScope.eventlst}">
								<div class="pagination mrgnTop">
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" />
								</div>
							</c:if>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	configureMenu("setupbandevent");
</script>

</html>
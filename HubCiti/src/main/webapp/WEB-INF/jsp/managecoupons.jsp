<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="/HubCiti/scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="/HubCiti/styles/colorPicker.css" />

<script type="text/javascript" src="/HubCiti/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<script src="/HubCiti/scripts/jquery-ui.js"></script>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Coupons</title>

<script type="text/javascript">
$(document).ready(function (){


	$(".modal-close").on('click.popup', function() {
		$(".modal-popupWrp").hide();
		$(".modal-popup").slideUp();
		$(".modal-popup").find(".modal-bdy input").val("");
		$(".modal-popup i").removeClass("errDsply");
	});
$('.boxx1').hide();
//$('.boxx2').hide();

});



function callNextPage(pagenumber, url) 
{
	document.ManageCouponForm.pageNumber.value = pagenumber;
	document.ManageCouponForm.pageFlag.value = "true";
	document.ManageCouponForm.action = url;
	document.ManageCouponForm.method = "get";
	document.ManageCouponForm.submit();

	}
	
	
function searchCoupon(event) {
	
	$('i.emptysearh').hide();

	var keycode = (event.keyCode ? event.keyCode : event.which);

	if (keycode == 13) {

		document.ManageCouponForm.action = "managecoupons.htm";
		document.ManageCouponForm.method = "GET";
		document.ManageCouponForm.submit();

	} else if (event == '') {

		document.ManageCouponForm.action = "managecoupons.htm";
		document.ManageCouponForm.method = "GET";
		document.ManageCouponForm.submit();

	} else {
		return true;
	}
}

function deleteCoupon(couponId)
{
	//r = confirm("Are you sure you want to delete this deal !");
	custAlertShowModal('Are you sure you want to delete this deal !',0);
	//if(r) {
	$("#cbbuttonX").on( "click", function() {
	custAlertCloseModal();
	document.ManageCouponForm.couponId.value = couponId ;
	document.ManageCouponForm.action = "deletecoupon.htm";
	document.ManageCouponForm.method = "post";
	document.ManageCouponForm.submit();
	});
//}

}

function editCoupon(couponId)
{

	document.ManageCouponForm.couponId.value=couponId;
	document.ManageCouponForm.action = "editcoupon.htm";
	document.ManageCouponForm.method = "POST";
	document.ManageCouponForm.submit();

}
	function featured(thiss,pageid) {
	
	$('.dealdeletemsgsucess').hide();
	$('.dealsaveupdatemsgsucess').hide();
	
		var isFeaturedValue;
		if($(thiss).is(":checked")){
				isFeaturedValue = 1;			
        }else if($(thiss).is(":not(:checked)")){
        	isFeaturedValue = 0;
        }
		
		$('.dealsucessmsg').hide();
		$('.boxcommon').hide();
		$('.msgBxsuccess').text('');
		
	
		$
		.ajax({
			type : "POST",
			url : "savefeaturedCoup.htm",
			data : {
				'isfeatured' : isFeaturedValue,
				'couponId': pageid,
				'text':''
			},
			success : function(response) {
			
					if( response == 'SUCCESS'){
						if( isFeaturedValue == 1){
						
							$('.msgBxsuccess').text('Deal saved as Featured');
						}else{
						
							$('.msgBxsuccess').text('Removed from featured list');
						}
						$('.boxx1').css({"display":"block"});
						//$('.boxx2').hide();
					}else if( response == 'featuredlimit'){
						
						$(thiss).prop('checked', false);
					
						$('.msgBxsuccess').text('Featured limit is reached');
						$('.boxx1').css({"display":"block"});
						//$('.boxx1').hide();
						//$('.boxx2').show();
						//thiss.attr('checked', false);
						
						
					}/*else{
						$('.msgBxfailure').text('failure');
						$('.boxx1').hide();
						$('.boxx2').show();
					}	*/								
			},
			error : function(e) {
				alert('Error occurred while saving Deal');
			//	$('.boxx1').hide();
				//$('.boxx2').show();
			}
		});
		
	}


</script>

</head>
<body>

	<!--Wrapper div Starts-->
	<div id="wrpr">
		<div class="clear"></div>
		<div class="wrpr-cont relative">
			<div id="slideBtn">
				<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
					src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
			</div>
			
			
			<!--Breadcrum div starts-->
			<div id="bread-crumb">
			
				<ul>
					<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
					<li><a href="welcome.htm">Home</a></li>
					<li class="last">Deals</li>
				</ul>
			</div>
			<!--Breadcrum div ends-->
			<span class="blue-brdr"></span>
			<!--Content div starts-->
			<div class="content" id="login">
				<!--Left Menu div starts-->
				<div id="menu-pnl" class="split">
					<jsp:include page="leftNavigation.jsp"></jsp:include>
				</div>
				<!--Left Menu div ends-->
				<!--Content panel div starts-->
				<div class="cont-pnl split" id="">
					<div class="cont-block stretch">
						<div class="title-bar">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-news">&nbsp;</span></li>
								<li>Deals</li>
							</ul>
						</div>
						<div class="tabd-pnl">
							<ul class="nav-tabs">
								<li><a class="active rt-brdr" href="#">Manage Deals</a></li>
							</ul>
							<div class="clear"></div>
						</div>
						<form:form name="ManageCouponForm" id="ManageCouponForm"
							commandName="ManageCouponForm" enctype="multipart/form-data">

							<form:hidden path="lowerLimit" />
							<input type="hidden" name="pageNumber" />
							<input type="hidden" name="pageFlag" />
							<input type="hidden" name="couponId" />
							<div class="cont-wrp">
							
									<c:choose>
									<c:when
										test="${sessionScope.couponslst ne null && !empty sessionScope.couponslst}">
										<table width="100%" border="0" cellspacing="0" cellpadding="0"	class="cmnTbl">
											<tr>
												<td width="19%"><label>Deal</label></td>
												<td width="31%"><div class="cntrl-grp">
														<form:input type="text" path="coupSearchKey" class="inputTxtBig"
															onkeypress="searchCoupon(event)" />
													</div></td>
												<td><a href="javascript:void(0);" class=""><img
														src="images/searchIcon.png" width="20" height="17"
														alt="search" title="Search Deal"
														onclick="searchCoupon('')"></a></td>
												<td width="40%" align="right"><input value="Add Deal"
											title="Create Deal" class="btn-blue"
											onclick="window.location.href='createcoupon.htm'"
											type="button"></td>
											</tr>


										</table> 
										
										<div class="alertBx success  mrgnTop cntrAlgn boxx1 dealdeletemsg">
							<span class="actn-closehide" title="close"></span>
							<p class="msgBxsuccess msgBx"></p>
						</div>
						<!-- <div class="alertBx failure mrgnTop cntrAlgn boxx2">
							<span class="actn-closehide" title="close"></span>
							<p class="msgBxfailure msgBx"></p>
						</div> -->
					<c:if
											test="${requestScope.Nocoupfound ne null && !empty requestScope.Nocoupfound}">		
							<div class="alertBx warning mrgnTop cntrAlgn">
										<span class="actn-close" title="close"></span>
										<p class="msgBx">
											${requestScope.Nocoupfound}
										</p>
									</div>
							</c:if>
							
							
							<c:if test="${requestScope.responseStatus ne null && !empty requestScope.responseStatus}">
							
										<c:choose>
										
										<c:when test="${requestScope.responseStatus eq 'Save'}">
										<div class="alertBx success mrgnTop cntrAlgn dealsaveupdatemsgsucess">
												<span class="actn-close" title="close"></span>
												<p class="msgBx">
													${requestScope.coupSuccessMsg}
												</p>
											</div>
										
										</c:when>
											<c:when test="${requestScope.responseStatus eq 'Delete'}">
										<div class="alertBx success mrgnTop cntrAlgn dealdeletemsgsucess">
												<span class="actn-close" title="close"></span>
												<p class="msgBx">
													${requestScope.coupSuccessMsg}
												</p>
											</div>
										
										</c:when>
										
										<c:otherwise>
											
									<!--	<c:if
											test="${requestScope.failureMsg ne null && !empty requestScope.failureMsg}">
											<span class="highLightErr" id="failureMsg"><c:out
													value="${requestScope.failureMsg}" /> </span>
										</c:if> -->
										</c:otherwise>
																		
							</c:choose>
							
							</c:if>
																
								<div class="relative">
									<div class="hdrClone"></div>
									<div class="scrollTbl tblHt mrgnTop">
										<table width="100%" cellspacing="0" cellpadding="0" border="0"
											id="mngevntTbl" class="grdTbl clone-hdr fxdhtTbl">
											<thead>
												<tr class="tblHdr">
													<th width="30%">Title</th>
													<th width="16%">Banner Title</th>
													<th width="14%">Start Date</th>
													<th width="14%">End Date</th>
													<th width="12%">Featured</th>
													<th width="14%" align="center">Actions</th>
												</tr>
											</thead>
											<tbody class="scrollContent">

												<c:forEach items="${sessionScope.couponslst.couponlst}"
													var="varCoupon">


													<tr class="dealgrd">
														<td>${varCoupon.coupTitle}</td>
														<td>${varCoupon.coupBanTitle}</td>
														<td>${varCoupon.coupStartDate}</td>
														<td>${varCoupon.coupEndDate}</td>
														
														
														
														
															<td class="centercss"><c:choose>
															
															
															
															<c:when
																test="${null ne varCoupon.coupfeature && varCoupon.coupfeature eq 1 }">
																<input type="checkbox" checked="checked"
																	name="coupfeature" id="coupfeature"
																	onchange="featured(this,${varCoupon.couponId});">
															</c:when>
															
															<c:otherwise>
															
																	
																<c:choose>

																		<c:when test="${ null != varCoupon.isEnded and varCoupon.isEnded eq 'true'}">

																			<input type="checkbox" name="coupfeature"
																	value="${varCoupon.coupfeature}" disabled="disabled"  />
																		</c:when>

																		<c:otherwise>
																			
																<input type="checkbox" name="coupfeature"
																	value="${varCoupon.coupfeature}"
																	onchange="featured(this,${varCoupon.couponId});">
																		</c:otherwise>


																	</c:choose>
																
															
															</c:otherwise>
														</c:choose></td>
													
														<td align="center"><a href="#" class="actn-icon"
															title="Edit Deal"><img src="images/edit_icon.png" onclick="editCoupon(${varCoupon.couponId})" /></a>
															<a href="#" class="actn-icon" title="Delete Deal"><img
																src="images/delete_icon.png" onclick="deleteCoupon(${varCoupon.couponId})"/></a></td>
													</tr>



												</c:forEach>




											</tbody>
										</table>
										
						<div>
									</div>



									<div class="pagination mrgnTop">

										<c:if
											test="${sessionScope.couponslst ne null && !empty sessionScope.couponslst}">
											<div class="pagination mrgnTop">
												<page:pageTag
													currentPage="${sessionScope.pagination.currentPage}"
													nextPage="4"
													totalSize="${sessionScope.pagination.totalSize}"
													pageRange="${sessionScope.pagination.pageRange}"
													url="${sessionScope.pagination.url}" />
											</div>

	

										</c:if>

									</div>
									
									</c:when>



									<c:otherwise>

										<table width="100%" border="0" cellspacing="0" cellpadding="0"
											class="cmnTbl">
											<tr>
												<td width="19%"><label>Deal</label></td>
												<td width="31%"><div class="cntrl-grp">
														<form:input type="text" path="coupSearchKey" class="inputTxtBig"
															onkeypress="searchCoupon(event)" />
													</div></td>
												<td><a href="javascript:void(0);" class=""><img
														src="images/searchIcon.png" width="20" height="17"
														alt="search" title="search Deal"
														onclick="searchCoupon('')"></a></td>
												<td width="40%" align="right"><input value="Add Deal"
											title="Create Deal" class="btn-blue"
											onclick="window.location.href='createcoupon.htm'"
											type="button"></td>
											</tr>


										</table> 
										<div class="alertBx warning mrgnTop cntrAlgn"
											id="evntCatClose">
											<span class="actn-close" title="close"></span>
											<p class="msgBx">No Deals found</p>
										</div>



									</c:otherwise>
									</c:choose>
								</form:form>
								
													
								
								</div>
								
								<c:if
											test="${sessionScope.couponslst ne null && !empty sessionScope.couponslst}">
								<div class="info-pnl mrgnTop_small">
								<label class="faqText"><b>Note:</b> If Deal End date is passed,can't be set it as Featured. If still want to make it as Featured, please reset the end date.</label> 
								
							</div>
							</c:if>
								
							</div>
						
					</div>
					<!--Content panel div ends-->
				</div>
				<!--Content div ends-->
			</div>
		</div>
</body>
<script type="text/javascript">
	configureMenu("setupdeals");
</script>
</html>
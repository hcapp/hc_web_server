<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="/HubCiti/styles/colorPicker.css" />
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript" src="/HubCiti/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<script src="/HubCiti/scripts/jquery-ui.js"></script>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>News Settings</title>

<script type="text/javascript">

$(document).ready(function (){
//$("#catUrlId").limiter(20, $(".catUrlId"));



        /* Close SubCategory popup */
        $(".modal-close,.btn-grey").on('click.popup', function() {
            $(".modal-popupWrp").hide();
            $(".modal-popup").slideUp();
            $(".modal-popup").find(".modal-bdy input").val("");
            $(".modal-popup i").removeClass("errDsply");
			 $('#subcatIdjs').text('');
        });
		
		/*$("#newsCtgry tr").click(function(){
			var		CatId=$(this).attr('id');
				$('#hidsubcatid').val=CatId;
			});*/
});


$(function() {
		var fixHelper = function(e, ui) {
		ui.children().each(function() {
		$(this).width($(this).width());
		});
		return ui;
	};
	$("#newsCtgry tbody").sortable({
		'opacity': 0.6,
		containment: '.scrollTbl',
		cursor: "move",
		tolerance: "pointer",
		helper: fixHelper /*fix for mozilla click trigger*/,
	});
	
	
});

function setColor(obj) {
	$(obj).parents('tr').hasClass("edited") && $("#getColor").val($(obj).attr("setcolor")) || $("#getColor").val("#00000");
    $(".full").spectrum({
			color: $("#getColor").val(),
		    showInitial: true,
    		showPalette: true,
			hide: function (color) {
				console.log(color.toHexString());
				var setColor = $("#getColor").attr("value", color.toHexString());
				$(obj).attr("setColor",color.toHexString());
			},
			palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
        "rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/ "rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
        /*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
        "rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ]
	});	
}

function callNextPage(pagenumber, url) 
{
	document.newssettings.pageNumber.value = pagenumber;
	document.newssettings.pageFlag.value = "true";
	document.newssettings.action = url;
	document.newssettings.method = "get";
	document.newssettings.submit();

	}
	
function deleteNewsCategories(newsSettingsID)
{
	r = confirm("Are you sure you want to delete this category !");
		if(r) {
	document.newssettings.newsSettingsID.value = newsSettingsID ;
	document.newssettings.action = "deletenewscat.htm";
	document.newssettings.method = "post";
	document.newssettings.submit();
}

}

function editCategory(newsettingsID)
{

	document.newssettings.newsSettingsID.value=newsettingsID;
	document.newssettings.action = "editnewscategory.htm";
	document.newssettings.method = "get";
	document.newssettings.submit();

}



function reOrderCats()
{
	var newsCatIds = [];
	$("#newsCtgry tbody").find("tr").each(function(){ newsCatIds.push(this.id); });	
	document.newssettings.newsCatIds.value=newsCatIds;
	document.newssettings.action = "reordernewscats.htm";
	document.newssettings.method = "post";
	document.newssettings.submit();

}

function showModal(catid) {


		
		$.ajaxSetup({
			cache : false
		})
		$
				.ajax({
					type : "GET",
					url : "fetchnewsmainsubcat.htm",
					data : {
						'newscatid' : catid,
						
					},

					success : function(response) {
				
						$('.addcat').html(response);
						var bodyHt = $('body').height();
						var setHt = parseInt(bodyHt);
						$(".modal-popupWrp").show();
						$(".modal-popupWrp").height(setHt);
						$(".modal-popup").slideDown('fast');
						var arr1 = jQuery.makeArray($('#hidsubcatid').val()
								.split(','));
						var arr2 = jQuery.makeArray($('#hidsubcaturl').val()
								.split(','));
						for (i = 0; i <= arr1.length; i++) {
							$("#sub-ctgry-settings tr").find(
									"td div[id='" + arr1[i] + "']").parents(
									'tr').find('td .cntrl-grp input').val(
									arr2[i]);

						}
					},
					error : function(e) {

					}
				});
	}
	
	 function savenewsSubCat() {
	 
	var CatId='${sessionScope.hCatId}';
	 
	 
			
        $('#subcatIdjs').text('');
        var isCurrectURls = true;
        var urlPattern = new RegExp('^(http|https|www).*$');

        var urls = $("tbody.addcat tr").find("td:eq(1) input").map(function(index, element) {
            if ($(this).val().length) {

			var vUrls = $(this).val();
			vUrls = $.trim(vUrls);
                var isCurrect = urlPattern.test(vUrls);
                if (!isCurrect) {
                    isCurrectURls = false;
                    $(this).parent('div').prev().text('Please enter correct url');
                } else {
                    $(this).parent('div').prev().text('');
                }
                return (vUrls);
            }else{
                $(this).parent('div').prev().text('');
            }
        }).toArray();

        if (isCurrectURls) {
            var subcats = $("tbody.addcat tr").find("td:eq(1) input").map(function(index, element) {
                if ($(this).val().length) {
                    return ($(this).parents('tr').find('td:eq(0) div').attr('id'));
                }

            }).toArray();

			urls = $.trim(urls);
            $.ajaxSetup({
                cache : false
            })
            $.ajax({
                type : "GET",
                url : "savenewssubcat.htm",
                data : {
                //    'newscatid' : '2',
                    'subcats' : subcats.toString(),
                    'urls' : urls.toString()

                },
                success : function(response) {
                    $('#subcatIdjs').text('Saved successfully');
                    $('.addcat').html(response);
                    var bodyHt = $('body').height();
                    var setHt = parseInt(bodyHt);
                    $(".modal-popupWrp").show();
                    $(".modal-popupWrp").height(setHt);
                    $(".modal-popup").slideDown('fast');
                },
                error : function(e) {

                }
            });
        }
    }
	
</script>
</head>
<body>
	<!--Wrapper div Starts-->
	<div id="wrpr">
		<div class="clear"></div>
		<div class="wrpr-cont relative">
			<div id="slideBtn">
				<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
					src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
			</div>
			<!--Breadcrum div starts-->
			<div id="bread-crumb">
				<ul>
					<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
					<li><a href="welcome.htm">Home</a></li>
					<li class="last">News Settings</li>
				</ul>
			</div>
			<!--Breadcrum div ends-->
			<span class="blue-brdr"></span>
			<!--Content div starts-->
			<div class="content" id="login">
				<!--Left Menu div starts-->
				<div id="menu-pnl" class="split">
					<jsp:include page="leftNavigation.jsp"></jsp:include>
				</div>
				<!--Left Menu div ends-->
				<!--Content panel div starts-->
				<div class="cont-pnl split" id="">
					<div class="cont-block stretch">
						<div class="title-bar">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-news">&nbsp;</span></li>
								<li>News Settings</li>
							</ul>
						</div>
						<div class="tabd-pnl">
							<ul class="nav-tabs">

								<li><a class="brdr-rt" href="newsgeneralsettings.htm">News
										General Settings</a></li>
								<li><a href="addcatsettings.htm" class="rt-brdr">
										News Categories</a></li>
								<li><a href="managenewscats.htm" class="rt-brdr active">Manage
										News Categories</a></li>


							</ul>
							<div class="clear"></div>
						</div>

						<div class="cont-wrp">
							<form:form name="newssettings" id="newssettings"
								commandName="newssettings">
								<form:hidden path="lowerLimit" />
								<input type="hidden" name="pageNumber" />
								<input type="hidden" name="pageFlag" />
								<form:hidden path="newsCatIds" />
								<form:hidden path="newsSettingsID" />

								<c:choose>
									<c:when
										test="${sessionScope.newssettingslst ne null && !empty sessionScope.newssettingslst}">
										<table width="100%" border="0" cellspacing="0" cellpadding="0"
											class="cmnTbl">
											<tr>
												<td width="19%"><label>Category</label></td>
												<td width="31%"><div class="cntrl-grp">
														<form:input type="text" path="catName" class="inputTxtBig"
															onkeypress="searchNewsCategory(event)" />
													</div></td>
												<td><a href="javascript:void(0);" class=""><img
														src="images/searchIcon.png" width="20" height="17"
														alt="search" title="search Category"
														onclick="searchNewsCategory('')"></a></td>
												<td align="right">&nbsp;</td>
											</tr>


										</table>
										<c:if
											test="${requestScope.successMsg ne null && !empty requestScope.successMsg}">
											<span class="highLightTxt" id="successMsg"><c:out
													value="${requestScope.successMsg}" /> </span>
										</c:if>
										<c:if
											test="${requestScope.failureMsg ne null && !empty requestScope.failureMsg}">
											<span class="highLightErr" id="failureMsg"><c:out
													value="${requestScope.failureMsg}" /> </span>
										</c:if>
										<c:if test="${null ne requestScope.responseStatus}">
											<c:choose>
												<c:when test="${requestScope.responseStatus eq 'SUCCESS' }">
													<div class="alertBx success mrgnTop cntrAlgn">
														<span class="actn-close" title="close"></span>
														<p class="msgBx">
															<c:out value="${requestScope.responeMsg}" />
														</p>
													</div>
												</c:when>
												<c:otherwise>
													<div class="alertBx failure mrgnTop cntrAlgn">
														<span class="actn-close" title="close"></span>
														<p class="msgBx">
															<c:out value="${requestScope.responeMsg}" />
														</p>
													</div>
												</c:otherwise>
											</c:choose>
										</c:if>
										<div class="relative" id="rtlrList">



											<div class="hdrClone"></div>
											<div class="newsCtgry scrollTbl tblHt mrgnTop">
												<input id="getIndex" type="hidden" /> <input id="getColor"
													type="hidden" />


												<table width="100%" cellspacing="0" cellpadding="0"
													border="0" id="newsCtgry" class="grdTbl clone-hdr fxdhtTbl">

													<thead>
														<tr class="tblHdr">
															<th width="18%">Name</th>
															<th width="25%">Feed URL</th>
															<th width="12%">Background Color</th>
															<th width="8%">Font Color</th>
															<th width="8%">Default</th>
															<th width="9%">News Ticker</th>
															<th width="20%">Action</th>
														</tr>
													</thead>
													<tbody class="scrollContent">
														<c:forEach
															items="${sessionScope.newssettingslst.newscatelst}"
															var="news">


															<tr id="${news.newsSettingsID}">


																<td> <div class="cell-wrp">${news.catName}</div></td>
																<td id="catUrlId">
																	<div class="cell-wrp">${news.catfeedURL}</div>
																</td>
																<td><span class="color"
																	style="background:${news.catColor}"></span></td>
																<td><span class="color"
																	style="background:${news.catFontColor}"></span></td>
																<td><c:choose>

																		<c:when test="${news.isDefault eq 'true'}">

																			<input type="checkbox" checked="checked"
																				disabled="disabled" />
																		</c:when>

																		<c:otherwise>
																			<input type="checkbox" disabled="disabled" />
																		</c:otherwise>


																	</c:choose></td>

																<td>
																<c:if test="${ null != news.isFeed and news.isFeed eq 'true'}"> 
																<c:choose>

																		<c:when test="${news.isNewsTicker eq 'true'}">

																			<input type="checkbox" checked="checked"
																				disabled="disabled" />
																		</c:when>

																		<c:otherwise>
																			<input type="checkbox" disabled="disabled" />
																		</c:otherwise>


																	</c:choose>
																	</c:if>
																	</td>

																<td><a href="javascript:void(0)"
																	setcolor="${news.catColor}" title="edit"
																	onclick="editCategory(${news.newsSettingsID})"><img
																		height="20" width="20" class="actn-icon" alt="edit"
																		src="images/edit_icon.png" /></a> <a href="#"
																	onclick="deleteNewsCategories(${news.newsSettingsID})">
																		<img height="20" width="20" class="actn-icon"
																		alt="delete" src="images/delete_icon.png"
																		title="Delete Category" />
																</a>
																<c:if test="${ null != news.isFeed and news.isFeed eq 'true'}"> 
																<c:choose>
																		
																		<c:when test="${ null != news.isSubCatFlag and news.isSubCatFlag eq 'true'}">
																			<a href="javascript:void(0)"
																				title="Associate Subcategories"
																				onclick="showModal(${news.catId})"><img
																				height="20" width="20" class="actn-icon"
																				alt="Associate" src="images/associate_icon.png" /></a>


																		</c:when>

																		<c:otherwise>

																			<a href="javascript:void(0)"
																				title="Associate Subcategories"><img height="20"
																				width="20" class="actn-icon" alt="Associate"
																				src="images/associate_icon_dsbld.png" /></a>

																		</c:otherwise>
																		

																	</c:choose>
																	</c:if></td>
															</tr>


														</c:forEach>
													</tbody>
												</table>
											</div>
											<%-- <div class="pagination mrgnTop">

												<c:if
													test="${sessionScope.newssettingslst ne null && !empty sessionScope.newssettingslst}">
													<div class="pagination mrgnTop">
														<page:pageTag
															currentPage="${sessionScope.pagination.currentPage}"
															nextPage="4"
															totalSize="${sessionScope.pagination.totalSize}"
															pageRange="${sessionScope.pagination.pageRange}"
															url="${sessionScope.pagination.url}" />
													</div>

												

												</c:if>

											</div> --%>
											<c:if test="${ null eq newssettings.catName or newssettings.catName eq '' }">	
											<div class="info-pnl mrgnTop_small">
												To change the order and to sort the categories, drag and
												drop the categories to the desired position. <input
													class="btn-blue floatR" value="Save Order" type="button"
													onclick="reOrderCats();">
											</div>
											</c:if>
										</div>
									</c:when>



									<c:otherwise>

										<table width="100%" border="0" cellspacing="0" cellpadding="0"
											class="cmnTbl">
											<tr>
												<td width="19%"><label>Category</label></td>
												<td width="31%"><div class="cntrl-grp">
														<form:input type="text" path="catName" class="inputTxtBig"
															onkeypress="searchNewsCategory(event)" />
													</div></td>
												<td><a href="javascript:void(0);" class=""><img
														src="images/searchIcon.png" width="20" height="17"
														alt="search" title="search Category"
														onclick="searchNewsCategory('')"></a></td>
												<td align="right">&nbsp;</td>
											</tr>


										</table>
										<div class="alertBx warning mrgnTop cntrAlgn"
											id="evntCatClose">
											<span class="actn-close" title="close"></span>
											<p class="msgBx">No Category details found</p>
										</div>



									</c:otherwise>



								</c:choose>
							</form:form>
						</div>



					</div>
					<!--Content panel div ends-->
				</div>
				<!--Content div ends-->
			</div>
		</div>
					<!-- News Sub Category Module Display  -->
	<div class="modal-popupWrp">
		<div class="modal-popup subCtgryModal">
			<div class="modal-hdr">
				<a title="close" class="modal-close">x</a>
				<h3>
					<span></span>Sub Category Settings
				</h3>
				<span class="highLightTxt" id="subcatIdjs"></span>
			</div>



			<div class="modal-bdy subcatmodal">

				<table cellpadding="0" cellspacing="0" width="100%"
					class="grdTbl  mrgnTop_small fullBrdr" id="sub-ctgry-settings">
					<thead>
						<tr class="tblHdr">
							<!-- <th width="10%"><input type="checkbox" id="chkAll" /></th> -->
							<th width="35%">Sub Category Name</th>
							<th width="55%">Feed URL</th>
						</tr>
					</thead>
					<tbody class="addcat">
					</tbody>
				</table>
			</div>
			<div class="modal-ftr">
				<p align="right">
					<input type="button" class="btn-blue subsave" value="Save"
						name="button" onclick="savenewsSubCat()"> <input
						type="button" class="btn-grey" value="Cancel" id="" name="Cancel"
						onclick="cancel()">
				</p>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	configureMenu("newssettings");
</script>
</body>

</html>
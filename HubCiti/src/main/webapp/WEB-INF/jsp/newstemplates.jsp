<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>
<script type="text/javascript">
    function displayListView() {
        document.menuDetails.menuTypeName.value = "List View";
        document.menuDetails.action = "setuplistmenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function displayGroupView() {
        document.menuDetails.menuTypeName.value = "Grouped Tab";
        document.menuDetails.action = "setupgroupmenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function comboNewsTemplate() {
        document.menuDetails.menuTypeName.value = "Combo Tab";
        document.menuDetails.action = "combonewsmenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }
	
    function newsTileTemplate() {
        document.menuDetails.menuTypeName.value = "news Tab";
        document.menuDetails.action = "newstilesmenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }
    
    function scrollingNewsTemplate() {
        document.menuDetails.menuTypeName.value = "Scrolling Tab";
        document.menuDetails.action = "scrollnewsmenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function createiconincTemplate() {
        document.menuDetails.action = "setupiconicmenu.htm";
        document.menuDetails.method = "POST";
        document.menuDetails.submit();
    }

    function createComboTemplate() {
        document.menuDetails.action = "setupcombomenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function createRectangularGrid() {
        document.menuDetails.action = "setuprectangularmenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function create4X4RectangularGrid() {
        document.menuDetails.action = "setup4X4menu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function createTwoImageTmpl() {
        document.menuDetails.action = "setuptwoimagemenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function createSquareImageTmpl() {
        document.menuDetails.action = "setupsquareimagemenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function displayTwoColView(template) {
        document.menuDetails.menuTypeName.value = template;
        document.menuDetails.action = "twocoltabview.htm";
        document.menuDetails.method = "GET";
        document.menuDetails.submit();
    }

    function createFourTileTmpl() {
        document.menuDetails.action = "setupfourtilemenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }

    function createTwoTileTmpl() {
        document.menuDetails.action = "setuptwotilemenu.htm";
        document.menuDetails.method = "get";
        document.menuDetails.submit();
    }
</script>
<link rel="stylesheet" type="text/css" href="/HubCiti/styles/colorPicker.css" />
<div class="wrpr-cont relative">
	<div id="slideBtn">
		<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
	</div>
	<div id="bread-crumb">
		<ul>
			<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
			<li>Home</li>
			<li class="last">Select Template</li>
		</ul>
	</div>
	<span class="blue-brdr"></span>
	<div class="content" id="login">
		<div id="menu-pnl" class="split">
			<jsp:include page="leftNavigation.jsp"></jsp:include>
		</div>
		<div class="cont-pnl split" id="equalHt">
			<div class="">
				<div class="title-bar">
					<ul class="title-actn">
						<li class="title-icon"><span class="icon-main-menu">&nbsp;</span></li>
						<li>Select Template</li>
					</ul>
				</div>
				<div class="tabd-pnl">
					<c:choose>
						<c:when test="${sessionScope.menuName eq 'Setup Sub Menu' }">
							<ul class="nav-tabs">
								<li class="news"><a href="displaymenutemplate.htm?menuType=submenu">HubCiti Templates</a></li>
								<li class="news"><a class="active" href="#" class="rt-brdr">News Templates</a></li>
							</ul>
						</c:when>
						<c:otherwise>
							<ul class="nav-tabs">
								<li class="news"><a href="displaymenutemplate.htm?menuType=mainmenu">HubCiti Templates</a></li>
								<li class="news"><a class="active" href="#" class="rt-brdr">News Templates</a></li>

							</ul>
						</c:otherwise>
					</c:choose>

					<div class="clear"></div>
				</div>
				<form:form name="menuDetails" commandName="menuDetails">
					<form:hidden path="menuTypeName" />
					<form:hidden path="level" />
					<form:hidden path="menuId" />
					<form:hidden path="menuName" />
					<input type="hidden" name="hidMenuType" id="hidMenuType" value="${requestScope.menuType}" />
					<div class="cont-wrp grey-bg tmplt-wrap">
						<p class="mrngBtm_small">Click on the templates to select</p>
						<ul class="grdLst">
							<li><span class="overlayInfo">Scrolling Template</span><img src="images/scrollingNewsTemplate.jpg" width="200" height="300" alt="template" />
								<input type="radio" name="tmpltOptn" id="singleTab" class="tmpltOptn" btnType="Grouped Tab Template" /> <span class="tmpltDsbl"></span> <a
								href="javascript:void(0);" onclick="scrollingNewsTemplate();">Next</a></li>
							<li><span class="overlayInfo">Combo Template </span><img src="images/combinationalTemplate.png" width="200" height="300" alt="template" />
								<input type="radio" name="tmpltOptn" id="twinTab" class="tmpltOptn" btnType="2 Column Tab Template" /> <span class="tmpltDsbl"></span> <a
								href="javascript:void(0);" onclick="comboNewsTemplate();">Next</a></li>
							<li><span class="overlayInfo">News Tile Template </span><img src="images/newsTilesTemplate.png" width="200" height="300" alt="template" />
								<input type="radio" name="tmpltOptn" id="twinTab" class="tmpltOptn" btnType="2 Column Tab Template" /> <span class="tmpltDsbl"></span> <a
								href="javascript:void(0);" onclick="newsTileTemplate();">Next</a></li>
						</ul>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    var menuType = '${requestScope.menuType}';
    if (menuType == 'mainmenu') {
        configureMenu("setupmainmenu");
    } else {
        configureMenu("setupsubmenu");
    }
</script>
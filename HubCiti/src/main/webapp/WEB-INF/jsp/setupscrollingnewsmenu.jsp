<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="/HubCiti/styles/colorPicker.css" />
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<script src="/HubCiti/scripts/jquery-ui.js"></script>
<script src="/HubCiti/scripts/global.js"></script>
<script>
	$(function() {
		$(".sortable").sortable({
			'opacity' : 0.6,
			helper : 'clone'
		});
		$(".sortable").disableSelection();
		$("#dataFnctn")
				.on(
						"change",
						function() {
							if ($(this).find("option:selected").attr("typeval") === "News") {
								$("#btnName").attr("readonly", "readonly");
								$("#btnName").val("");
								var updt = $("#News").find(
										"input[name='btnLinkId']:checked")
										.next("label").text();
								$("#btnName").attr("value", updt);
							} else {
								$("#btnName").attr("readonly", false);
							}
						});
		$("#News input[name='btnLinkId']").on('click', function() {
			updateValue();
		});

	});
	function updateValue() {

		$("#btnName").val(
				$("input[name='btnLinkId']:checked").next("label").text());
	}
</script>
<div id="wrpr">
	<div class="clear"></div>
	<div class="wrpr-cont relative">
		<div id="slideBtn">
			<a href="#" onclick="revealPanel(this);" title="Hide Menu"> <img
				src="images/slide_off.png" width="11" height="28" alt="btn_off" />
			</a>
		</div>
		<div id="bread-crumb">
			<c:choose>
				<c:when test="${sessionScope.menuName eq 'Setup Sub Menu' }">
					<ul>
						<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
						<li>Home</li>
						<li class=""><a
							href="displaymenutemplate.htm?menuType=submenu" id="slctTmpt">Change
								Template</a></li>
						<li class="last" id="menutitle-bread-crumb">Sub Menu [
							Scrolling News Template ]</li>
					</ul>
				</c:when>
				<c:otherwise>
					<ul>
						<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
						<li>Home</li>
						<li class=""><a
							href="displaymenutemplate.htm?menuType=mainmenu" id="slctTmpt">Change
								Template</a></li>
						<li class="last" id="menutitle-bread-crumb">Main Menu [
							Scrolling News Template ]</li>
					</ul>
				</c:otherwise>
			</c:choose>

		</div>
		<span class="blue-brdr"></span>
		<div class="content" id="login">
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>
			<div class="cont-pnl split">
				<div class="cont-block rt-brdr">
					<c:choose>
						<c:when test="${sessionScope.menuName eq 'Setup Sub Menu' }">
							<div class="title-bar" id="menutitleDiv">
								<ul class="title-actn">
									<li class="title-icon"><span class="icon-submenu">&nbsp;</span></li>
									<li id="menutitle">Sub Menu</li>
								</ul>
							</div>
						</c:when>

					</c:choose>

					<form:form name="screenSettingsForm" id="screenSettingsForm"
						commandName="screenSettingsForm" enctype="multipart/form-data"
						action="uploadimg.htm">
						<form:errors cssStyle="color:red"></form:errors>
						<form:hidden path="viewName" value="setupscrollingnewsmenu" />
						<form:hidden path="addDeleteBtn" />
						<form:hidden path="menuId" />
						<form:hidden path="menuLevel" />
						<form:hidden path="menuIconId" id="menuIconId" />
						<form:hidden path="hiddenmenuFnctn" />
						<form:hidden path="hiddenBtnLinkId" />
						<form:hidden path="templateName" />
						<form:hidden path="ismenuFilterTypeSelected" />
						<form:hidden path="editFilter" />
						<form:hidden path="hiddenFindCategory" />
						<input type="hidden" id="tmpltOptn" value="ListView" />
						<form:hidden path="btnPosition" />
						<form:hidden path="subCatIds" id="subCatIds" />
						<form:hidden path="hiddenSubCate" />
						<form:hidden path="chkSubCate" id="chkSubCate" />
						<form:hidden path="userType" value="${sessionScope.loginUserType}" />
						<form:hidden path="hiddenCitiId" />
						<form:hidden path="hiddenFundEvtId" />
						<!-- <form:hidden path="subMenuName" />-->
						<form:hidden path="uploadImageType" id="uploadImageType"
							value="logoImage" />

						<form:hidden path="bandSubCatIds" id="bandSubCatIds" />
						<form:hidden path="chkBandSubCate" id="chkBandSubCate" />
						<form:hidden path="hiddenBandSubCate" />

						<form:hidden path="newsSubCatIds" id="newsSubCatIds" />
						<form:hidden path="chkNewsSubCate" id="chkNewsSubCate" />
						<form:hidden path="hiddenNewsSubCate" />
						<c:choose>
							<c:when test="${sessionScope.menuName eq 'Setup Sub Menu' }">
								<div class="cont-wrp less-pdng" id="subMenuDetails">
									<table width="100%" border="0" cellpadding="0" cellspacing="0"
										class="cmnTbl" id="">
										<tr id="subMenuCont">
											<td width="36%"><label class="mand">SubMenu Name</label></td>
											<td width="64%" colspan="2"><label
												class="submenujs errorDsply"></label> <form:errors
													cssClass="submenujs errorDsply" path="subMenuName"></form:errors>
												<div class="cntrl-grp">
													<form:input path="subMenuName" type="text"
														id="subMenuInput" maxlength="35" class="inputTxtBig vldt"
														onkeypress="return specialCharCheck(event)" />
												</div> <span class="lbl">Chars Left:</span><span
												class="lblcnt subMenuInput">35</span></td>
										</tr>
									</table>
								</div>
							</c:when>
							<c:otherwise>
								<form:hidden path="subMenuName" id="subMenuInput" />
							</c:otherwise>
						</c:choose>
						<!-- <div class="title-bar top-brdr">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-main-menu">&nbsp;</span></li>
								<li id="menutitle">Button Details</li>
							</ul>
						</div> -->
						<div class="cont-wrp less-pdng newstempcss" id="dynData">
							<table width="100%" border="0" cellpadding="0" cellspacing="0"
								class="cmnTbl">

								<tr>
									<td width="36%"><label class="mand">Button Name</label></td>
									<td width="64%" colspan="2"><form:errors
											cssClass="errorDsply" path="menuBtnName"></form:errors>
										<div class="cntrl-grp">
											<form:input path="menuBtnName" type="text"
												class="inputTxtBig vldt " id="btnName" maxlength="35"
												onkeypress="return specialCharCheck(event)" />
										</div> <span class="lbl">Chars Left:</span><span
										class="lblcnt btnName">30</span>
								</tr>
								<tr>
									<td width="36%"><label class="mand">Functionality</label></td>
									<td width="64%" colspan="2"><form:errors
											cssClass="errorDsply" path="menuFucntionality"></form:errors>
										<div class="cntrl-grp zeroBrdr">
											<form:select path="menuFucntionality" class="slctBx"
												id="dataFnctn" onchange="updateBtnLink();">
												<option selected="selected" value="0">Select
													Functionality</option>
												<c:forEach items="${sessionScope.linkList}" var="link">
													<c:choose>
														<c:when
															test="${sessionScope.menuName eq 'Setup Sub Menu'}">
															<option value="${link.menuTypeId}"
																typeVal="${link.menuTypeVal}">${link.menuTypeName}</option>
														</c:when>
														<c:when
															test="${sessionScope.menuName eq 'Setup Main Menu' && link.menuTypeName != 'News'}">
															<option value="${link.menuTypeId}"
																typeVal="${link.menuTypeVal}">${link.menuTypeName}</option>
														</c:when>
													</c:choose>
												</c:forEach>
											</form:select>
										</div></td>
								</tr>
								<tr>
									<td colspan="3"><ul class="infoList cmnList " id="AppSite">
											<c:forEach items="${sessionScope.appsitelst}" var="appsite">
												<li><span class="cell"> <form:input type="radio"
															name="AppSite" path="btnLinkId"
															id="AS-${appsite.appSiteId}" value="${appsite.appSiteId}" />
														${appsite.appSiteName}
												</span> <span class="cell">${appsite.retName},${appsite.address}</span></li>
											</c:forEach>
											<li class="actn"><a href="javascript:void(0);"
												onclick="addAppsite(this)"><img src="images/btn_add.png"
													width="24" height="24" alt="add" class="addImg" /> Create
													New App Site</a></li>
										</ul>
										<ul class="infoList cmnList" id="AnythingPage">
											<c:forEach items="${sessionScope.anythingPageList}"
												var="item">
												<li><span class="cell zeroMrgn"> <form:input
															type="radio" value="${item.hcAnythingPageId}"
															id="AP-${item.hcAnythingPageId}" name="anythingPage"
															path="btnLinkId" /> <c:out
															value="${item.anythingPageTitle }" />
												</span> <span class="cell">${item.pageType}</span></li>
											</c:forEach>
											<li class="actn"><a href="buildanythingpage.htm"><img
													src="images/btn_add.png" width="24" height="24" alt="add"
													class="addImg" /> Add New AnyThing<sup>TM</sup> Page</a></li>
										</ul>
										<ul class="infoList cmnList menuLst" id="SubMenu">
											<c:forEach items="${sessionScope.subMenuList }" var="item">
													<c:if test="${!(item.isNewsTemp != null &&  item.isNewsTemp == true ) }">
														<li><span class="cell zeroMrgn"> <form:input
																	type="radio" value="${item.menuId}" name="subMenu"
																	id="SM-${item.menuId}" path="btnLinkId" /> <c:out
																	value="${item.menuName }"></c:out>
														</span>
														</li>
													</c:if>
											</c:forEach>
											<li class="actn"><a
												href="displaymenutemplate.htm?menuType=submenu"><img
													src="images/btn_add.png" width="24" height="24" alt="add"
													class="addImg" /> Add New SubMenu</a></li>
										</ul>
										<div class="input-actn">
											<input type="checkbox" name="subMenu" id="findchkAll" /> <label
												for="find1">Select All</label>
										</div>
										<ul class="infoList cmnList menuLst" id="Find">
											<c:forEach items="${sessionScope.businessCatList }"
												var="item">
												<li><span class="cell zeroMrgn"> <form:checkbox
															path="btnLinkId" id="FN-${item.catId}" class="main-ctrgy"
															value="${item.catId }-MC" /> <label for="find1">${item.catName
															}</label>
												</span> <!--For listout the subcategoirs of main category --> <c:if
														test="${item.subArrayList ne null && !empty item.subArrayList}">
														<ul class="sub-ctgry">
															<c:forEach items="${item.subArrayList}" var="subItem">
																<c:choose>
																	<c:when
																		test="${subItem.subCatId ne null && subItem.subCatId ne ''}">
																		<li><input type="checkbox" name="btnLinkId"
																			value="${subItem.subCatId}"
																			id="FNS-${subItem.subCatId}" /> <label>${subItem.subCatName}</label>
																		</li>
																	</c:when>
																	<c:otherwise>
																		<li><input type="checkbox" name="btnLinkId"
																			checked="checked" class="hidFindChk" value="NULL"
																			id="FNS-${subItem.subCatId}" /> <label>${subItem.subCatName}</label></li>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</ul>
													</c:if></li>
											</c:forEach>
										</ul>
										<div class="input-actn-evnt">
											<input type="checkbox" id="evntchkAll" /> <label
												for="evntchkAll">Select All</label>
										</div>
										<ul class="infoList cmnList menuLst" id="Events">
											<c:forEach items="${sessionScope.eventCatList }" var="item">
												<li><span class="cell zeroMrgn"> <form:checkbox
															path="btnLinkId" name="category" id="EVT-${item.catId}"
															value="${item.catId }" /> <label for="find1">${item.catName
															}</label>
												</span></li>
											</c:forEach>
										</ul> <!-- start : Adding code for fundraiser categories.. -->
										<div class="input-actn-fundraiser">
											<input type="checkbox" id="fundraChkAll" /> <label
												for="fundraChkAll">Select All</label>
										</div>
										<ul class="infoList cmnList menuLst" id="Fundraisers">
											<c:forEach items="${sessionScope.fundraiserCatList}"
												var="funEvt">
												<li><span class="cell zeroMrgn"> <form:checkbox
															path="btnLinkId" id="FUNDEVT-${funEvt.catId}"
															value="${funEvt.catId }" /> <label for="Fundraiser">${funEvt.catName
															}</label>
												</span></li>
											</c:forEach>
										</ul> <!-- end : Adding code for fundraiser categories.. --> <!-- start : Adding code for filters.. -->
										<div class="input-actn-filter">
											<input type="checkbox" id="filterChkAll" /> <label
												for="filterChkAll">Select All</label>
										</div>
										<ul class="infoList cmnList menuLst" id="Filters">
											<c:forEach items="${sessionScope.filterList}" var="filt">
												<li><span class="cell zeroMrgn"> <form:checkbox
															path="btnLinkId" id="FILT-${filt.filterID}"
															value="${filt.filterID }" /> <label for="Filter">${filt.filterName
															}</label>
												</span></li>
											</c:forEach>
										</ul> <!-- end : Adding code for filters.. --> <!--start: adding code for band categories -->

										<div class="input-actn-band">
											<input type="checkbox" name="bandsubMenu" id="bandchkAll" />
											<label for="find1">Select All</label>
										</div>
										<ul class="infoList cmnList menuLst" id="Band">
											<c:forEach items="${sessionScope.bandCatList}" var="item">
												<li><span class="cell zeroMrgn"> <form:checkbox
															path="btnLinkId" id="BC-${item.catId}" class="main-ctrgy"
															value="${item.catId }-MC" /> <label for="find1">${item.catName
															}</label>
												</span> <!--For list out the sub categories of main category --> <c:if
														test="${item.subArrayList ne null && !empty item.subArrayList}">
														<ul class="sub-ctgry">
															<c:forEach items="${item.subArrayList}" var="subItem">
																<c:choose>
																	<c:when
																		test="${subItem.subCatId ne null && subItem.subCatId ne ''}">
																		<li><input type="checkbox" name="btnLinkId"
																			value="${subItem.subCatId}"
																			id="BCS-${subItem.subCatId}" /> <label>${subItem.subCatName}</label>
																		</li>
																	</c:when>
																	<c:otherwise>
																		<li><input type="checkbox" name="btnLinkId"
																			value="NULL" id="BCS-${subItem.subCatId}"
																			checked="checked" class="hidFindChk" /> <label>${subItem.subCatName}</label></li>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</ul>
													</c:if></li>
											</c:forEach>
										</ul> <!-- End : adding code for band categories --> <!-- Start : adding code for news categories -->
										<div id="NewsSubMenu">
											<c:choose>
												<c:when test="${sessionScope.menuName eq 'Setup Sub Menu' }">
													
													<ul class="infoList cmnList menuLst" id="News">

														<c:forEach items="${sessionScope.newsCatList}" var="item">
															<li><span class="cell zeroMrgn"> <form:input type="radio"
																		path="btnLinkId" id="NC-${item.catId}"
																		class="main-ctrgy" value="${item.catId }-MC" /> <label
																	for="NC-${item.catId}">${item.catName }</label>
															</span> <!--For list out the sub categories of main category -->
																<c:if
																	test="${item.subArrayList ne null && !empty item.subArrayList}">
																	<ul class="sub-ctgry">
																		<c:forEach items="${item.subArrayList}" var="subItem">
																			<c:choose>
																				<c:when
																					test="${subItem.subCatId ne null && subItem.subCatId ne ''}">
																					<li><input type="radio" name="btnLinkId"
																						value="${subItem.subCatId}"
																						id="NCS-${subItem.subCatId}" /> <label>${subItem.subCatName}</label>
																					</li>
																				</c:when>
																				<c:otherwise>
																					<li><input type="radio" name="btnLinkId"
																						value="NULL" id="NCS-${subItem.subCatId}"
																						checked="checked" class="hidFindChk" /> <label>${subItem.subCatName}</label></li>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																	</ul>
																</c:if></li>
														</c:forEach>
													</ul>
												</c:when>
											</c:choose>
										</div> <!-- End : adding code for news categories -->



										<div>
											<div class="input-actn-city mrgnTop cityPnl prntChkbx">
												<input type="checkbox" id="citychkAll" class="trgrChk" /> <label
													for="citychkAll">Cities</label>
											</div>
											<ul class="infoList cmnList menuLst cityPnl" id="Cities">
												<c:forEach items="${sessionScope.citiesLst }" var="item">
													<li><span class="cell zeroMrgn"> <form:checkbox
																path="citiId" id="CITY-${item.cityExpId}"
																value="${item.cityExpId }" /> <label for="find1">${item.cityExpName
																}</label>
													</span></li>
												</c:forEach>
											</ul>
										</div></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><input type="button" name="button" value="Add Button"
										class="btn-blue" id="addBtn"
										onclick="creatDeleteMenuItem('AddButton');" /></td>
									<td align="right"><input type="button" name="delBtn"
										value="Delete Button" class="btn-red" id="delBtn"
										onclick="creatDeleteMenuItem('DeleteButton');" /></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</div>
					</form:form>
				</div>
				<div class="cont-block">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-iphone">&nbsp;</span></li>
							<li>App Preview</li>
						</ul>
					</div>
					<div class="prgrsStp">
						<ul>
							<li class="step1"><span>Tab Creation</span></li>
							<li class="step2"><span>Save Template</span></li>
							<li class="saveTmplt"><input type="button" name="button"
								value="Save" class="btn-blue" id="saveTmplt"
								onclick="saveTemplate()" /></li>
							<li class="clear"></li>
						</ul>
					</div>
					<div class="cont-wrp">
						<div id="iphone-preview" class="brdr">
							<div id="iphone-priPolicy-preview">
								<div class="iphone-status-bar"></div>
								<div class="navBar iphone">
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="titleGrd">
										<tr>
											<!-- <td width="19%"><div class="icon-main-menu"></div></td> -->
											<td width="24%"><img width="35" height="25" alt="back"
												src="images/hamburger.png" class="hamburger"></td>
											<td width="54%" class="genTitle"><img
												src="${sessionScope.newsBannerImage}" width="86" height="35"
												alt="Banner" /></td>
											<td width="27%" align="center"></td>
										</tr>
									</table>
								</div>
								<!-- <div class="previewAreaScroll gnrlScrn newspreview"> -->
								<div class="gnrlScrn newspreview">
									<ul id="dynTab" class="sortable scrollingNews ui-sortable">
										<c:forEach items="${sessionScope.previewMenuItems}" var="item">
											<li class="tabs"><a
												onclick="editiconicListTab(this,'List View')"
												datactn="<c:out value='${item.menuFucntionality }'></c:out>"
												iconid="${item.menuIconId }"
												iconimgname="${item.logoImageName }"
												linkId="${item.btnLinkId }" btnDept="${item.btnDept }"
												btnType="${item.btnType }" subCat="${item.chkSubCate}"
												citiId="${item.citiId}"
												chkBandSubCate="${item.chkBandSubCate}"
												chkNewsSubCate="${item.chkNewsSubCate}"
												href="javascript:void(0)"> <img width="30" height="30"
													class="lstImg" alt="image"
													src="<c:out value='${item.imagePath }'></c:out>"> <span><c:out
															value='${item.menuBtnName }'></c:out> </span>
											</a></li>
										</c:forEach>
									</ul>

								</div>
							</div>
							<!--Iphone Preview For Login screen ends here-->
							<%-- <ul id="tab-main" class="tabbar">
								<c:forEach items="${sessionScope.menuBarButtonsList }" var="item">
									<li><img src="${item.imagePath }" id="pre-${item.bottomBtnId}" btmbtnid="${item.bottomBtnId}" width="80" height="50" alt="share" /></li>
								</c:forEach>
							</ul> --%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/HubCiti/images/popupClose.png" class="closeIframe"
				alt="close"
				onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span
				id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
			allowtransparency="yes" width="100%" style="background-color: White">
		</iframe>
	</div>
</div>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('#dynData').hide();
						var licnt = $("#dynTab li").length;
						if (licnt) {
							$(".prgrsStp li:first").addClass("step1Actv");
						}
						//below code for find and band categories
						$(window)
								.load(
										function() {
											var mainChkd = $(
													'.main-ctrgy:visible')
													.prop('checked');

											if (mainChkd) {
												$(mainChkd)
														.each(
																function(index,
																		element) {
																	$(
																			"ul .sub-ctgry")
																			.removeAttr(
																					"style");
																	$(this)
																			.parent(
																					"span")
																			.next(
																					"ul .sub-ctgry")
																			.show();
																});
											} else {
												$('.sub-ctgry').hide();
											}

											if ($(".sub-ctgry input:checkbox[value='NULL']").length > 0) {
												$(
														".sub-ctgry input[value='NULL']")
														.parent('li').hide();
											}

										});

						/* menu and sub menu Implementation  starts */

						var menuLevel = document.screenSettingsForm.menuLevel.value;

						$("#btnName").limiter(30, $(".btnName"));

						if (menuLevel == 1) {
							$("#subMenuCont").addClass('btnNm');
							$("#grpingCont").addClass('btnNm');
							$("#grpingContTitle").addClass('btnNm');
							//$("#menutitleDiv").addClass('btnNm');
							//$("#subMenuDetails").addClass('btnNm');

							$("#slctTmpt")
									.attr('href',
											"displaymenutemplate.htm?menuType=mainmenu");
							var mainMenuBGType = '${sessionScope.mainMenuBGType}';
							var mainMenuBG = '${sessionScope.mainMenuBG}';
							var mainMenuBtnClr = '${sessionScope.mainMenuBtnClr}';
							var mainMenuFntClr = '${sessionScope.mainMenuFntClr}';

							$(".gnrlScrn").css(
									"background",
									"url('" + "images/list.png"
											+ "') no-repeat scroll 0 0");

							$(".listView li.tabs a").css("color",
									mainMenuFntClr);
							$(".listView li.tabs").css("background",
									mainMenuBtnClr);
						} else {
							var subMenuId = document.screenSettingsForm.menuId.value;
							var subMenuName = $("#subMenuInput").val();
							$("td.genTitle").text(subMenuName);

							$("#slctTmpt").attr(
									'href',
									"displaymenutemplate.htm?menuType=submenu&id="
											+ subMenuId + "&menuname="
											+ subMenuName);

							$("#menutitle").text("Sub Menu");
							$("#menutitle-bread-crumb").text(
									"Sub Menu [ Scrolling News Template ]");

							/* Code for displaying menu background */
							var subMenuBGType = '${sessionScope.subMenuBGType}';
							var subMenuBG = '${sessionScope.subMenuBG}';
							var subMenuBtnClr = '${sessionScope.subMenuBtnClr}';
							var subMenuFntClr = '${sessionScope.subMenuFntClr}';

							$(".gnrlScrn").css(
									"background",
									"url('" + "images/list.png"
											+ "') no-repeat scroll 0 0");

							$(".listView li.tabs a")
									.css("color", subMenuFntClr);
							$(".listView li.tabs").css("background",
									subMenuBtnClr);

							var ismenuFilterTypeSelected = document.screenSettingsForm.ismenuFilterTypeSelected.value;

							if (ismenuFilterTypeSelected == 'true') {
								$('#grpType,#grpDept').prop('disabled', true);

								$('#grpDept')
										.click(
												function(e) {

													if (!($(this)
															.is(':checked'))) {

														var rmvDept = confirm("Are you sure to remove Department association");
														if (rmvDept) {

															$(this).prop(
																	'checked',
																	false);
															tglGrping(this);
														} else {

															$(this).prop(
																	'checked',
																	true);
															tglGrping(this);
														}
													} else {
														$(this).prop('checked',
																true);
														tglGrping(this);
														alert("Please associate Department for all the buttons");
													}

												});

								$("#edittglGrping").removeClass("btnNm")
										.addClass('grpInputEdit');
							}
						}

						/* Menu and Sub Menu Implementation ends */

						var licnt = $("#dynTab li").length;
						if (licnt) {
							$(".prgrsStp li:first").addClass("step1Actv");
						}

						/* Displaying button length */
						$("#btnName").limiter(30, $(".btnName"));
						$("#subMenuInput").limiter(35, $(".subMenuInput"));

						var menuIconId = document.screenSettingsForm.menuIconId.value;
						if (null !== menuIconId && menuIconId != "") {
							$("#addBtn").attr("value", "Save Button");
							$("#delBtn").show();
						}

						onLoadslctFnctn();
						//displaySubMenuName();

						var selFuncVal = $("#dataFnctn option:selected").attr(
								'typeVal');
						$("#dataFnctn option:selected").trigger('change');
						$("#dataFnctn option[typeVal='" + selFuncVal + "']")
								.prop("selected", true);
						var hiddenBtnLinkId = document.screenSettingsForm.hiddenBtnLinkId.value;
						var hiddenSubCate = document.screenSettingsForm.hiddenSubCate.value;
						var hiddenBandSubCate = document.screenSettingsForm.hiddenBandSubCate.value;
						var hiddenNewsSubCate = document.screenSettingsForm.hiddenNewsSubCate.value;
						if (selFuncVal == "SubMenu") {

							$('#SM-' + hiddenBtnLinkId).prop('checked',
									'checked');
							$(".cmnList").hide();
							$("#" + selFuncVal).show();

						} else if (selFuncVal == "AnythingPage") {

							$('#AP-' + hiddenBtnLinkId).prop('checked',
									'checked');
							$(".cmnList").hide();
							$("#" + selFuncVal).show();

						} else if (selFuncVal == "AppSite") {

							$('#AS-' + hiddenBtnLinkId).prop('checked',
									'checked');
							$(".cmnList").hide();
							$("#" + selFuncVal).show();

						} else if (selFuncVal == "Find") {

							$('#Find input[name="btnLinkId"]').prop('checked',
									false);
							var arr = hiddenBtnLinkId.split(',');

							jQuery.each(arr,
									function(i, val) {
										if (jQuery(val).index("MC") === -1) {

											val = val.substring(0, val
													.lastIndexOf("-"));

											$('#FN-' + val).prop('checked',
													'checked');
											$('#FN-' + val).parent().next(
													'.sub-ctgry').show();
										}

									});
							//for subcategories 

							if (hiddenSubCate != null) {

								hiddenSubCate = hiddenSubCate.replace(
										/NULL!~~!/gi, "");
								hiddenSubCate = hiddenSubCate.replace(
										/!~~!NULL/gi, "");
								var arr2 = hiddenSubCate.split(',');
								jQuery.each(arr2,
										function(i, val) {
											$('#FNS-' + val).prop('checked',
													'checked');
										});
							}
							$(".input-actn").show();
							$(".cmnList").hide();
							$("#" + selFuncVal).show();

							var tolCnt = $('#Find input[name$="btnLinkId"]:checkbox:visible').length;
							var chkCnt = $('#Find input[name$="btnLinkId"]:checkbox:checked:visible').length;
							if (tolCnt == chkCnt) {
								$('#findchkAll').prop('checked', 'checked');
							} else {
								$('#findchkAll').removeAttr('checked');
							}

						} else if (selFuncVal == "Events") {
							$('#Events input[name="btnLinkId"]').prop(
									'checked', false);
							var arr = hiddenBtnLinkId.split(',');
							jQuery.each(arr, function(i, val) {
								$('#EVT-' + val).prop('checked', 'checked');
							});
							/* var busCatList = $("#hiddenFindCategory").val().split(",");
							for ( var k = 0; k < busCatList.length; k++) {
								$("#EVT-" + busCatList[k]).prop('checked', true);
							} */
							$(".input-actn-evnt").show();
							var tolCnt = $("#Events input[type=checkbox][name='btnLinkId']").length;
							var chkCnt = $('#Events input[name$="btnLinkId"]:checked').length;
							if (tolCnt == chkCnt) {
								$('#evntchkAll').prop('checked', 'checked');
							} else {
								$('#evntchkAll').removeAttr('checked');
							}
							$(".cmnList").hide();
							$("#" + selFuncVal).show();
						} else if (selFuncVal == "Fundraisers") {

							var arr = hiddenBtnLinkId.split(',');
							jQuery.each(arr,
									function(i, val) {
										$('#FUNDEVT-' + val).prop('checked',
												'checked');

									});

							var fundEvtCatIdLst = $("#hiddenFundEvtId").val()
									.split(",");
							for (var fnd = 0; fnd < fundEvtCatIdLst.length; fnd++) {
								$("#FUNDEVT-" + fundEvtCatIdLst[fnd]).prop(
										'checked', true);

							}

							$(".input-actn-fundraiser").show();
							var tolCnt = $("#Fundraisers input[type=checkbox][name='btnLinkId']").length;
							var chkCnt = $("#Fundraisers input[name$='btnLinkId']:checked").length;
							if (tolCnt == chkCnt) {
								$('#fundraChkAll').prop('checked', 'checked');
							} else {
								$('#fundraChkAll').removeAttr('checked');
							}
							$(".cmnList").hide();
							$("#" + selFuncVal).show();
						} else if (selFuncVal == "Filters") {
							var arr = hiddenBtnLinkId.split(',');
							jQuery.each(arr, function(i, val) {
								$('#FILT-' + val).prop('checked', 'checked');
							});
							var fundEvtCatIdLst = $("#hiddenFundEvtId").val()
									.split(",");
							for (var fnd = 0; fnd < fundEvtCatIdLst.length; fnd++) {
								$("#FILT-" + fundEvtCatIdLst[fnd]).prop(
										'checked', true);
							}

							$(".input-actn-filter").show();
							var tolCnt = $("#Filters input[type=checkbox][name='btnLinkId']").length;
							var chkCnt = $("#Filters input[name$='btnLinkId']:checked").length;
							if (tolCnt == chkCnt) {
								$('#filterChkAll').prop('checked', 'checked');
							} else {
								$('#filterChkAll').removeAttr('checked');
							}
							$(".cmnList").hide();
							$("#" + selFuncVal).show();
						} else if (selFuncVal == "Band") {

							$(".input-actn-band").show();
							$('#Band input[name="btnLinkId"]').prop('checked',
									false);
							var arr = hiddenBtnLinkId.split(',');

							jQuery.each(arr,
									function(i, val) {
										if (jQuery(val).index("MC") === -1) {

											val = val.substring(0, val
													.lastIndexOf("-"));

											$('#BC-' + val).prop('checked',
													'checked');
											$('#BCS-' + val).parent().next(
													'.sub-ctgry').show();
										}

									});
							//for subcategories 

							if (hiddenBandSubCate != null) {
								hiddenBandSubCate = hiddenBandSubCate.replace(
										/NULL!~~!/gi, "");
								hiddenBandSubCate = hiddenBandSubCate.replace(
										/!~~!NULL/gi, "");
								var arr2 = hiddenBandSubCate.split(',');
								jQuery.each(arr2,
										function(i, val) {
											$('#BCS-' + val).prop('checked',
													'checked');
										});
							}

							$(".cmnList").hide();
							$("#" + selFuncVal).show();

							var tolCnt = $('#Band input[name$="btnLinkId"]:checkbox:visible').length;
							var chkCnt = $('#Band input[name$="btnLinkId"]:checkbox:checked:visible').length;
							if (tolCnt == chkCnt) {
								$('#bandchkAll').prop('checked', 'checked');
							} else {
								$('#bandchkAll').removeAttr('checked');
							}
						} else if (selFuncVal == "News") {
							$(".input-actn-news").show();
							$('#News input[name="btnLinkId"]').prop('checked',
									true);
							var arr = hiddenBtnLinkId.split(',');

							jQuery.each(arr,
									function(i, val) {
										if (jQuery(val).index("MC") === -1) {

											val = val.substring(0, val
													.lastIndexOf("-"));

											$('#NC-' + val).prop('checked',
													'checked');
											$('#NCS-' + val).parent().next(
													'.sub-ctgry').show();
										}

									});
							//for subcategories 

							if (hiddenNewsSubCate != null) {
								hiddenNewsSubCate = hiddenNewsSubCate.replace(
										/NULL!~~!/gi, "");
								hiddenNewsSubCate = hiddenNewsSubCate.replace(
										/!~~!NULL/gi, "");
								var arr2 = hiddenNewsSubCate.split(',');
								jQuery.each(arr2,
										function(i, val) {
											$('#NCS-' + val).prop('checked',
													'checked');
										});
							}

							$(".cmnList").hide();
							$("#" + selFuncVal).show();

							var tolCnt = $('#News input[name$="btnLinkId"]:checkbox:visible').length;
							var chkCnt = $('#News input[name$="btnLinkId"]:checkbox:checked:visible').length;
							if (tolCnt == chkCnt) {
								$('#newschkAll').prop('checked', 'checked');
							} else {
								$('#newschkAll').removeAttr('checked');
							}
						}

						//Start: Changes related to Adding cities
						var loginUserType = document.screenSettingsForm.userType.value;

						if (loginUserType == "RegionApp"
								&& "undefined" != typeof (selFuncVal)) {

							$('#Cities input[name="citiId"]').prop('checked',
									false);
							var hiddenCitiId = document.screenSettingsForm.hiddenCitiId.value;
							var arr = hiddenCitiId.split(',');

							jQuery.each(arr, function(i, val) {
								$('#CITY-' + val).prop('checked', 'checked');
							});

							$(".cityPnl").show();
							var tolCnt = $("#Cities input[type=checkbox][name='citiId']").length;
							var chkCnt = $('#Cities input[name$="citiId"]:checked').length;
							if (tolCnt == chkCnt) {
								$('#citychkAll').prop('checked', 'checked');
							} else {
								$('#citychkAll').removeAttr('checked');
							}
						}
						//End: Changes related to Adding cities

					});

	function creatDeleteMenuItem(val) {

		//Below code for find -Subcategory implmenation 		
		var checkedSubCat = $.makeArray($('ul.sub-ctgry').find(
				'input[name="btnLinkId"]:checkbox:checked:visible')

		.map(function() {
			return $(this).val();
		}));

		var findbndchk = $('ul.sub-ctgry').parents(".infoList:visible").attr(
				'id');

		if (findbndchk == 'Find') {
			$('#chkSubCate').val(checkedSubCat);
		} else {
			$('#chkBandSubCate').val(checkedSubCat);
			$('#chkNewsSubCate').val(checkedSubCat);
		}

		var checkedMainCat = $.makeArray($('.main-ctrgy:checked').map(
				function() {
					return $(this).val();
				}));

		var subArr = [];
		$.each(checkedMainCat, function(i, val) {
			var arr = $.makeArray($(
					'input[name="btnLinkId"][value=' + val + ']')
					.parent('span').next('ul.sub-ctgry').find(
							'input[name="btnLinkId"]:checkbox:checked:visible')
					.map(function() {
						return $(this).val();
					}));
			if (arr.length === 0) {
				subArr.push('null');
				subArr.push('!~~!');
			} else {
				subArr.push(arr.toString());
				subArr.push('!~~!');
			}
		});
		var subCat = subArr.toString().replace(/,!~~!,/gi, "!~~!");
		if (subCat.match(/,!~~!$/)) {
			subCat = subCat.replace(/,!~~!/gi, "");
		}

		if (findbndchk == 'Find') {
			$('#subCatIds').val(subCat);
		} else if (findbndchk == 'Band') {
			$('#bandSubCatIds').val(subCat);

		} else if (findbndchk == 'News') {
			$('#newsSubCatIds').val(subCat);
		}

		var btnOrderList = "";
		if (!($("#grpDept").is(':checked'))) {

			$("#slctDept option[value='0']").prop("selected", "selected");
		}
		if (!($("#grpType").is(':checked'))) {

			$("#slctType option[value='0']").prop("selected", "selected");
		}

		$(".prgrsStp li:first").addClass("step1Actv");
		$("#dynTab li").find("a").each(function() {
			var btnId = $(this).attr("iconid");
			if (btnOrderList == "") {
				btnOrderList = btnId;
			} else {
				btnOrderList = btnOrderList + "~" + btnId;
			}

		});
		document.screenSettingsForm.btnPosition.value = btnOrderList;
		document.screenSettingsForm.addDeleteBtn.value = val;
		document.screenSettingsForm.action = "addbutton.htm";
		document.screenSettingsForm.method = "POST";
		document.screenSettingsForm.submit();
	}

	function saveTemplate() {
		$.ajaxSetup({
			cache : false
		});
		var licnt = $("#dynTab li").length;
		var bottmBtnId = "";
		var btnOrderList = "";
		if (licnt) {

			$("#dynTab li").find("a").each(function() {
				var btnId = $(this).attr("iconid");
				if (btnOrderList == "") {
					btnOrderList = btnId;
				} else {
					btnOrderList = btnOrderList + "~" + btnId;
				}

			});

			var menuId = document.screenSettingsForm.menuId.value;
			var menuLevel = document.screenSettingsForm.menuLevel.value;
			var subMenuName = document.screenSettingsForm.subMenuName.value;
			var bannerImg = "";
			var viewName = "Scrolling News Template";
			var errorFlag = false;

			var menuLevelvalue = document.screenSettingsForm.menuLevel.value;

			if (menuLevel != 1) {
				var sideName = $('#subMenuInput').val();
				if (sideName == '') {
					$('.submenujs').css('display', 'block');
					$('.submenujs').text('Please Enter Sub Menu Name');
				} else {
					$('.submenujs').text('');
					if (!errorFlag) {
						$
								.ajax({
									type : "POST",
									url : "savetemplate.htm",
									data : {
										'menuId' : menuId,
										'menuLevel' : menuLevel,
										'subMenuName' : subMenuName,
										'bottmBtnId' : bottmBtnId,
										'template' : viewName,
										'bannerImg' : bannerImg,
										'typeFilter' : false,
										'deptFilter' : false,
										'btnOrder' : btnOrderList
									},

									success : function(response) {

										if (response == 'SUCCESS') {
											alert("Template Saved");
											$(".prgrsStp li:eq(0)").addClass(
													"step1Actv");
											$(".prgrsStp li:eq(1)").addClass(
													"step2Actv");
										} else if (response == 'Associate City') {
											alert('Please associate City for all the bottons to continue saving');
										} else {
											window.location.href = "/HubCiti/sessionTimeout.htm";
										}
									},
									error : function(e) {
										alert('Error occurred while saving template');
									}
								});
					}
				}
			} else {
				if (!errorFlag) {
					$
							.ajax({
								type : "POST",
								url : "savetemplate.htm",
								data : {
									'menuId' : menuId,
									'menuLevel' : menuLevel,
									'subMenuName' : subMenuName,
									'bottmBtnId' : bottmBtnId,
									'template' : viewName,
									'bannerImg' : bannerImg,
									'typeFilter' : false,
									'deptFilter' : false,
									'btnOrder' : btnOrderList
								},

								success : function(response) {

									if (response == 'SUCCESS') {
										alert("Template Saved");
										$(".prgrsStp li:eq(0)").addClass(
												"step1Actv");
										$(".prgrsStp li:eq(1)").addClass(
												"step2Actv");
									} else if (response == 'Associate City') {
										alert('Please associate City for all the bottons to continue saving');
									} else {
										window.location.href = "/HubCiti/sessionTimeout.htm";
									}
								},
								error : function(e) {
									alert('Error occurred while saving template');
								}
							});
				}
			}
		} else {
			alert("Empty Template");
		}
	}

	function onLoadslctFnctn() {
		var functionalityId = document.screenSettingsForm.hiddenmenuFnctn.value;
		var functionality = document.getElementById("dataFnctn");
		if (functionalityId != "null" || functionalityId != "") {

			for (var i = 0; i < functionality.length; i++) {

				if (functionality.options[i].value == functionalityId) {
					functionality.options[i].selected = true;
					break;
				}
			}
		}
	}

	function updateBtnLink() {
		$('input[name="btnLinkId"]').prop('checked', false);
	}
	function hex2rgba(colour) {
		var r, g, b, a;
		if (colour.charAt(0) === '#') {
			colour = colour.substr(1);
		}

		r = colour.charAt(0) + colour.charAt(1);
		g = colour.charAt(2) + colour.charAt(3);
		b = colour.charAt(4) + colour.charAt(5);

		r = parseInt(r, 16);
		g = parseInt(g, 16);
		b = parseInt(b, 16);
		a = 0.5;
		return "rgb(" + r + "," + g + "," + b + "," + a + ")";
	}

	function showTab(obj) {
		if (typeof (obj) === "object") {
			$(".tmpltLst").hide();
			$("#" + $(obj).attr("value")).show();
			$("#tmptType").val($(obj).attr("value"));
		}
		if (typeof (obj) === "string") {
			$(".tmpltLst").hide();
			$("#" + obj).show();
			$("#tmptType").val(obj);
			$(".nav-tabs").find("a").removeClass("active");
			$('a[name="' + obj + '"]').addClass("active");
		}
	}

	//loadTab();
</script>
<script type="text/javascript">
	var menuLevel = document.screenSettingsForm.menuLevel.value;
	if (menuLevel == 1) {
		configureMenu("setupmainmenu");
	} else {

		configureMenu("setupsubmenu");
	}
</script>
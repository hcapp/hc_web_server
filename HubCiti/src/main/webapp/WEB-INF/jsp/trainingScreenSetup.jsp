<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="/HubCiti/styles/colorPicker.css" />
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<style>
.hidetr {
	display: none;
}
</style>
<script type="text/javascript">
	$(document)
			.ready(
					function() {

						var screenName = $('#screenName option:selected').val();

						var flag = true;

						var curVal = $('input[name=pgntn]:checked').attr(
								"value");

						var imgSrc = $("#" + curVal).find("img").attr("src");

						if (imgSrc == "" || typeof imgSrc == "undefined") {
							imgSrc = "images/upload_image.png";
							$("#addBtn").attr("value", "Add Slide");
						} else {
							$("#addBtn").attr("value", "Update Slide");

						}

						$('input:radio[name="btnLinkId"]')
								.change(
										function() {

											if ($('#addBtn').val() == "Add Slide") {

												$('#hiddenbtnLinkId')
														.val(
																$(
																		'input[name="btnLinkId"]:checked')
																		.val());

											}
										});

						if ($('#first').is(':hidden')) {

							$("tr").removeClass("hidetr");
							$("input[id='isUrl1']").trigger('click');
							$("input[id='isUrlOrPage1']").trigger('click');
							$('#AnythingPage').hide();

						} else if ($('#sec').is(':hidden')) {

							$("tr").removeClass("hidetr");
							$("input[id='isUrl1']").trigger('click');
							$("input[id='isUrlOrPage2']").trigger('click');
							$("#trUrl").hide();

						} else {
							if (screenName == "1" && flag == true) {
								var isurl1 = "${sessionScope.url1}";
								var isurlOrPage1 = "${sessionScope.isurlOrPage1}";
								var urlvalue1 = "${sessionScope.urlvalue1}";
								if ($('#strBannerAdImagePath').val() == null
										|| $('#strBannerAdImagePath').val() == "") {
									$('#strBannerAdImagePath').val(
											"${sessionScope.screenImageOne}"
													.split("p/").pop());
								}
								if (isurl1 == "1") {

									$("tr").removeClass("hidetr");
									$("input[id='isUrl1']").trigger('click');
									if (isurlOrPage1 == "1") {
										$("input[id='isUrlOrPage1']").trigger(
												'click');
										$('#firstUseURL').val(urlvalue1);
										$('#AnythingPage').hide();
									} else {
										$("input[id='isUrlOrPage2']").trigger(
												'click');
										$('#' + urlvalue1)
												.prop('checked', true);
										$('#trUrl').hide();
									}
								} else {
									$("input[id='isUrl2']").trigger('click');
									$("#trUrl,#trisUrlOrPage,#AnythingPage")
											.addClass("hidetr");
								}
								flag = false;
							}

							if (screenName == "2" && flag == true) {
								var isurl1 = "${sessionScope.url2}";
								var isurlOrPage1 = "${sessionScope.isurlOrPage2}";
								var urlvalue1 = "${sessionScope.urlvalue2}";
								if ($('#strBannerAdImagePath').val() == null
										|| $('#strBannerAdImagePath').val() == "") {
									$('#strBannerAdImagePath').val(
											"${sessionScope.screenImageTwo}"
													.split("p/").pop());
								}
								if (isurl1 == "1") {

									$("tr").removeClass("hidetr");
									$("input[id='isUrl1']").trigger('click');
									if (isurlOrPage1 == "1") {
										$("input[id='isUrlOrPage1']").trigger(
												'click');
										$('#firstUseURL').val(urlvalue1);
										$('#AnythingPage').hide();
									} else {
										$("input[id='isUrlOrPage2']").trigger(
												'click');
										$('#' + urlvalue1)
												.prop('checked', true);
										$('#trUrl').hide();
									}
								} else {
									$("input[id='isUrl2']").trigger('click');
									$("#trUrl,#trisUrlOrPage,#AnythingPage")
											.addClass("hidetr");
								}
								flag = false;
							}

							if (screenName == "3" && flag == true) {
								var isurl1 = "${sessionScope.url3}";
								var isurlOrPage1 = "${sessionScope.isurlOrPage3}";
								var urlvalue1 = "${sessionScope.urlvalue3}";
								if ($('#strBannerAdImagePath').val() == null
										|| $('#strBannerAdImagePath').val() == "") {
									$('#strBannerAdImagePath').val(
											"${sessionScope.screenImageThree}"
													.split("p/").pop());
								}
								if (isurl1 == "1") {

									$("tr").removeClass("hidetr");
									$("input[id='isUrl1']").trigger('click');
									if (isurlOrPage1 == "1") {
										$("input[id='isUrlOrPage1']").trigger(
												'click');
										$('#firstUseURL').val(urlvalue1);
										$('#AnythingPage').hide();
									} else {
										$("input[id='isUrlOrPage2']").trigger(
												'click');
										$('#' + urlvalue1)
												.prop('checked', true);
										$('#trUrl').hide();
									}
								} else {
									$("input[id='isUrl2']").trigger('click');
									$("#trUrl,#trisUrlOrPage,#AnythingPage")
											.addClass("hidetr");
								}
								flag = false;
							}

							if (screenName == "4" && flag == true) {
								var isurl1 = "${sessionScope.url4}";
								var isurlOrPage1 = "${sessionScope.isurlOrPage4}";
								var urlvalue1 = "${sessionScope.urlvalue4}";
								if ($('#strBannerAdImagePath').val() == null
										|| $('#strBannerAdImagePath').val() == "") {
									$('#strBannerAdImagePath').val(
											"${sessionScope.screenImageFour}"
													.split("p/").pop());
								}
								if (isurl1 == "1") {

									$("tr").removeClass("hidetr");
									$("input[id='isUrl1']").trigger('click');
									if (isurlOrPage1 == "1") {
										$("input[id='isUrlOrPage1']").trigger(
												'click');
										$('#firstUseURL').val(urlvalue1);
										$('#AnythingPage').hide();
									} else {
										$("input[id='isUrlOrPage2']").trigger(
												'click');
										$('#' + urlvalue1)
												.prop('checked', true);
										$('#trUrl').hide();
									}
								} else {
									$("input[id='isUrl2']").trigger('click');
									$("#trUrl,#trisUrlOrPage,#AnythingPage")
											.addClass("hidetr");
								}
								flag = false;
							}
							if ($('#addBtn').val() == "Add Slide") {
								if ($('#firstUseURL').val()) {

									$("input[id='isUrl1']").trigger('click');
									$('#trisUrlOrPage').show();
									$('#AnythingPage').hide();
									$('#trUrl').show();
									$("tr").removeClass("hidetr");
								}
								if ($('#hiddenbtnLinkId').val()) {

									$("input[id='isUrl1']").trigger('click');
									$('#trisUrlOrPage').show();
									$('#AnythingPage').show();
									$("tr").removeClass("hidetr");
									$('#trUrl').hide();
									$('#' + $('#hiddenbtnLinkId').val()).prop(
											'checked', true);

								}
							}

						}
						$('input:radio[name="isUrl"]')
								.change(
										function() {
											var isUrl = $(
													'input:radio[name="isUrl"]:checked')
													.val();
											var isUrlOrPage = $(
													'input:radio[name="isUrlOrPage"]:checked')
													.val();
											$("#first").css("display", "none");
											$("#sec").css("display", "none");
											$("#logoerror").css("display",
													"none");
											if (isUrl == 1) {
												$('#trisUrlOrPage').show();
												if (isUrlOrPage == 1)
													$('#trUrl').show();
												else {
													$('#AnythingPage').show();
													$('#trUrl').hide();
													$("tr").removeClass(
															"hidetr");
												}
											} else {
												$('#trisUrlOrPage').hide();
												$('#trUrl').hide();
												$('#AnythingPage').hide();
												$('#firstUseURL').val("");
												$(
														'input:radio[name="btnLinkId"]:checked')
														.removeAttr('checked');
											}

										});

						$('input:radio[name="isUrlOrPage"]')
								.change(
										function() {
											var isUrlOrPage = $(
													'input:radio[name="isUrlOrPage"]:checked')
													.val();
											$("#first").css("display", "none");
											$("#sec").css("display", "none");
											if (isUrlOrPage == 1) {
												$('#trUrl').show();
												$('#AnythingPage').hide();
												$(
														'input:radio[name="btnLinkId"]:checked')
														.removeAttr('checked');
											} else {
												$('#AnythingPage').show();
												$("tr").removeClass("hidetr");
												$('#trUrl').hide();
												$('#firstUseURL').val("");
											}

										});

						var getSlide = localStorage.getItem("curActive");
						$(".paginated-cntrl").find(
								"input[value$='" + getSlide + "']").prop(
								"checked", true);

						var scrnSelected = localStorage
								.getItem('curActiveScrnName');
						$("#screenName option[value='" + scrnSelected + "']")
								.attr("selected", true);

						var hasImg = $("input[name='pgntn']:checked").attr(
								"value");
						if ($("#" + hasImg).find("img").attr("src").length) {
							$("#logoerror").css("display", "none");
						} else {
							$("#logoerror").css("display", "block");
						}

						$("#screenName")
								.change(
										function() {

											var screenName = $(
													'#screenName option:selected')
													.val();
											var imgSrc1 = $("#scrn-1").find(
													"img").attr("src");
											var imgSrc2 = $("#scrn-2").find(
													"img").attr("src");
											var imgSrc3 = $("#scrn-3").find(
													"img").attr("src");

											if (screenName == "2") {
												if (imgSrc1 == "") {
													alert("Please setup previous slide information");

												}
											}
											if (screenName == "3") {
												if (imgSrc1 == ""
														|| imgSrc2 == "") {
													alert("Please setup previous slide information");

												}
											}
											if (screenName == "4") {
												if (imgSrc1 == ""
														|| imgSrc2 == ""
														|| imgSrc3 == "") {
													alert("Please setup previous slide information");

												}
											}
										});

						$("#screenName")
								.change(
										function() {
											$("#first").css("display", "none");
											$("#sec").css("display", "none");
											$(".alertBx").hide();
											if ($(this).val() == "") {
												$("input[id='isUrl2']")
														.trigger('click');

											}
											$('#hiddenbtnLinkId').val('');
											$('#firstUseURL').val('');
											var isurl1 = "${sessionScope.url1}";
											var isurl2 = "${sessionScope.url2}";
											var isurl3 = "${sessionScope.url3}";
											var isurl4 = "${sessionScope.url4}";

											var isurlOrPage1 = "${sessionScope.isurlOrPage1}";
											var isurlOrPage2 = "${sessionScope.isurlOrPage2}";
											var isurlOrPage3 = "${sessionScope.isurlOrPage3}";
											var isurlOrPage4 = "${sessionScope.isurlOrPage4}";

											var urlvalue1 = "${sessionScope.urlvalue1}";
											var urlvalue2 = "${sessionScope.urlvalue2}";
											var urlvalue3 = "${sessionScope.urlvalue3}";
											var urlvalue4 = "${sessionScope.urlvalue4}";

											var srnName = $(
													"#screenName option:selected")
													.attr("value");
											localStorage.setItem(
													'curActiveScrnName',
													srnName);
											if ($("#screenName").val() == 1) {

												$('#strBannerAdImagePath').val(
														"${sessionScope.screenImageOne}"
																.split("p/")
																.pop());

												$(".paginated-cntrl")
														.find(
																'input[value$="scrn-1"]:radio')
														.prop('checked', true);
												document
														.getElementById("scrn-1").style.display = 'list-item';
												$("input[value='scrn-1']")
														.attr("checked", true)
														.trigger('click');
												if (isurl1 == "1") {

													$("input[id='isUrl1']")
															.trigger('click');
													if (isurlOrPage1 == "1") {
														$(
																"input[id='isUrlOrPage1']")
																.trigger(
																		'click');
														$('#firstUseURL').val(
																urlvalue1);
													} else {
														$(
																"input[id='isUrlOrPage2']")
																.trigger(
																		'click');
														$('#' + urlvalue1)
																.prop(
																		'checked',
																		true);
													}
												} else {
													$("input[id='isUrl2']")
															.trigger('click');
													$(
															"input[id='isUrlOrPage1']")
															.trigger('click');
													$("#trUrl").hide();
												}

											} else if ($("#screenName").val() == 2) {

												$('#strBannerAdImagePath').val(
														"${sessionScope.screenImageTwo}"
																.split("p/")
																.pop());

												$(".paginated-cntrl")
														.find(
																'input[value$="scrn-2"]:radio')
														.prop('checked', true);
												document
														.getElementById("scrn-2").style.display = 'list-item';
												$("input[value='scrn-2']")
														.attr("checked", true)
														.trigger('click');
												if (isurl2 == "1") {
													$("input[id='isUrl1']")
															.trigger('click');
													if (isurlOrPage2 == "1") {
														$(
																"input[id='isUrlOrPage1']")
																.trigger(
																		'click');
														$('#firstUseURL').val(
																urlvalue2);
													} else {
														$(
																"input[id='isUrlOrPage2']")
																.trigger(
																		'click');
														$('#' + urlvalue2)
																.prop(
																		'checked',
																		true);
													}
												} else {
													$("input[id='isUrl2']")
															.trigger('click');
													$(
															"input[id='isUrlOrPage1']")
															.trigger('click');
													$("#trUrl").hide();
												}
											} else if ($("#screenName").val() == 3) {

												$('#strBannerAdImagePath').val(
														"${sessionScope.screenImageThree}"
																.split("p/")
																.pop());

												$(".paginated-cntrl")
														.find(
																'input[value$="scrn-3"]:radio')
														.prop('checked', true);
												document
														.getElementById("scrn-3").style.display = 'list-item';
												$("input[value='scrn-3']")
														.attr("checked", true)
														.trigger('click');
												if (isurl3 == "1") {
													$("input[id='isUrl1']")
															.trigger('click');
													if (isurlOrPage3 == "1") {
														$(
																"input[id='isUrlOrPage1']")
																.trigger(
																		'click');
														$('#firstUseURL').val(
																urlvalue3);
													} else {
														$(
																"input[id='isUrlOrPage2']")
																.trigger(
																		'click');
														$('#' + urlvalue3)
																.prop(
																		'checked',
																		true);
													}
												} else {
													$("input[id='isUrl2']")
															.trigger('click');
													$(
															"input[id='isUrlOrPage1']")
															.trigger('click');
													$("#trUrl").hide();
												}
											} else if ($("#screenName").val() == 4) {

												$('#strBannerAdImagePath').val(
														"${sessionScope.screenImageFour}"
																.split("p/")
																.pop());

												$(".paginated-cntrl")
														.find(
																'input[value$="scrn-4"]:radio')
														.prop('checked', true);
												document
														.getElementById("scrn-4").style.display = 'list-item';
												$("input[value='scrn-4']")
														.attr("checked", true)
														.trigger('click');
												if (isurl4 == "1") {
													$("input[id='isUrl1']")
															.trigger('click');
													if (isurlOrPage4 == "1") {
														$(
																"input[id='isUrlOrPage1']")
																.trigger(
																		'click');
														$('#firstUseURL').val(
																urlvalue4);
													} else {
														$(
																"input[id='isUrlOrPage2']")
																.trigger(
																		'click');
														$('#' + urlvalue4)
																.prop(
																		'checked',
																		true);
													}
												} else {
													$("input[id='isUrl2']")
															.trigger('click');
													$(
															"input[id='isUrlOrPage1']")
															.trigger('click');
													$("#trUrl").hide();
												}
											}
										});

						$('.uploadPdf').hide();
						$('input:radio[name="type"]').change(
								function() {
									var isPdf = $(
											'input:radio[name="type"]:checked')
											.val();
									if (isPdf == "false") {
										$(".uploadImage").hide();
										$(".uploadPdf").show();
									} else {
										$(".uploadPdf").hide();
										$(".uploadImage").show();
									}
								});

						var previewScreen = $(".paginated-cntrl").find(
								'input[name=pgntn]:checked').val();

						$('input:radio[name=pgntn]')
								.change(
										function() {
											var value = $(
													'input:radio[name="pgntn"]:checked')
													.attr("value").split("-")
													.pop();
											$('#screenName').val(value)
													.trigger('change');
										});

						$("input[name='pgntn']")
								.on(
										'click',
										function() {
											$("#maxSizeImageError").html('');
											$(".page").hide();
											$("#" + $(this).attr("value"))
													.show();

											var srnName = $(
													"#screenName option:selected")
													.attr("value");
											localStorage.setItem(
													'curActiveScrnName',
													srnName);

											$('input:radio[name=pgntn]')
													.click(
															function() {

																if (document
																		.getElementById('logoImageName.errors') != null) {
																	document
																			.getElementById('logoImageName.errors').style.display = 'none';
																}
																var slide = $(
																		'input[name=pgntn]:checked')
																		.val();
																localStorage
																		.setItem(
																				'curActive',
																				slide);

																var srnName = $(
																		"#screenName option:selected")
																		.attr(
																				"value");
																localStorage
																		.setItem(
																				'curActiveScrnName',
																				localStorage
																						.getItem(
																								'curActive')
																						.split(
																								"-")
																						.pop());

																var curVal = $(
																		this)
																		.attr(
																				"value");
																var curScrn = curVal
																		.split(
																				"-")
																		.pop();
																var imgSrc = $(
																		"#"
																				+ curVal)
																		.find(
																				"img")
																		.attr(
																				"src");

																if (imgSrc == "") {
																	imgSrc = "images/upload_image.png";
																	$("#addBtn")
																			.attr(
																					"value",
																					"Add Slide");
																} else {
																	$("#addBtn")
																			.attr(
																					"value",
																					"Update Slide");
																}
																$("#screenName")
																		.find(
																				"option")
																		.removeProp(
																				"selected");
																$("#screenName")
																		.find(
																				"option[value='"
																						+ curScrn
																						+ "']")
																		.prop(
																				"selected",
																				"selected");
															});
										});
						var response = "${requestScope.responeMsg}";
						if (response == "Please setup atleast 1 slide to save") {
							$(".msgBx").css("color", "red");
							$(".alertBx").addClass('failure');
						}
						$("input[name='pgntn']:checked").trigger('click');

					});

	function creatDeleteScreen(val) {
		var screenOne = "${sessionScope.screenImageOne}";
		var screenTwo = "${sessionScope.screenImageTwo}";
		var screenThree = "${sessionScope.screenImageThree}";
		var screenFour = "${sessionScope.screenImageFour}";

		
		
		
		if (val == "DeleteButton") {
			$("tr").removeClass("hidetr");
			$('#hiddenbtnLinkId').val('');
			$('#firstUseURL').val('');
			var imgSrc1 = $("#scrn-1").find("img").attr("src");
			var imgSrc2 = $("#scrn-2").find("img").attr("src");
			var imgSrc3 = $("#scrn-3").find("img").attr("src");
			var imgSrc4 = $("#scrn-4").find("img").attr("src");

			if (imgSrc1 == "" && imgSrc2 == "" && imgSrc3 == ""
					&& imgSrc4 == "") {
				var responseDel = false;
				var response = false;
			} else if (imgSrc1 == "" && imgSrc2 == "" && imgSrc3 == "") {
				var responseDel = true;
			} else if (imgSrc2 == "" && imgSrc3 == "" && imgSrc4 == "") {
				var responseDel = true;
			} else if (imgSrc1 == "" && imgSrc2 == "" && imgSrc4 == "") {
				var responseDel = true;
			} else if (imgSrc1 == "" && imgSrc3 == "" && imgSrc4 == "") {
				var responseDel = true;
			}

			var hasImg = $("input[name='pgntn']:checked").attr("value");
			//if($("#"+hasImg).find("img").length) {
			if ($("#" + hasImg).find("img").attr("src").length) {
				if (responseDel == true) {
					if (confirm("Atleast 1 slide is mandatorty, Are you sure you want to delete it?")) {
						$("#delete-button").attr("href",
								"query.php?ACTION=delete&ID='1'");
					} else {
						return false;
					}

				}
			}
			if ($('input:radio[name=pgntn]:checked').val() == "scrn-1") {
				document.screenSettingsForm.screenName.value = "1";
				document.screenSettingsForm.logoImageName.value = screenOne;

			} else if ($('input:radio[name=pgntn]:checked').val() == "scrn-2") {
				document.screenSettingsForm.screenName.value = "2";
				document.screenSettingsForm.logoImageName.value = screenTwo;

			} else if ($('input:radio[name=pgntn]:checked').val() == "scrn-3") {
				document.screenSettingsForm.screenName.value = "3";
				document.screenSettingsForm.logoImageName.value = screenThree;

			} else if ($('input:radio[name=pgntn]:checked').val() == "scrn-4") {
				document.screenSettingsForm.screenName.value = "4";
				document.screenSettingsForm.logoImageName.value = screenFour;

			}
		}
		if (val == "AddButton") {

			var screenName = $('#screenName option:selected').val();
			var imgSrc1 = $("#scrn-1").find("img").attr("src");
			var imgSrc2 = $("#scrn-2").find("img").attr("src");
			var imgSrc3 = $("#scrn-3").find("img").attr("src");
			var imgSrc4 = $("#scrn-4").find("img").attr("src");
			var response;

			if (screenName == "2") {
				if (imgSrc1 == "") {
					alert("Please setup previous slide information");
					$('#hiddenbtnLinkId').val('');
					$('#firstUseURL').val('');
					$("input[id='isUrl2']").trigger('click');
					$("#trisUrlOrPage").hide();
					$("#trUrl").hide();
					$("#AnythingPage").hide();
					response = false;

				}
			}
			if (screenName == "3") {
				if (imgSrc1 == "" || imgSrc2 == "") {
					alert("Please setup previous slide information");
					$('#hiddenbtnLinkId').val('');
					$('#firstUseURL').val('');
					$("input[id='isUrl2']").trigger('click');
					$("#trisUrlOrPage").hide();
					$("#trUrl").hide();
					$("#AnythingPage").hide();

					response = false;
				}
			}
			if (screenName == "4") {
				if (imgSrc1 == "" || imgSrc2 == "" || imgSrc3 == "") {
					alert("Please setup previous slide information");
					$('#hiddenbtnLinkId').val('');
					$('#firstUseURL').val('');
					$("input[id='isUrl2']").trigger('click');
					$("#trisUrlOrPage").hide();
					$("#trUrl").hide();
					$("#AnythingPage").hide();

					response = false;
				}
			}

			if (screenName == "") {
				alert("Please select a slide");
				response = false;
			}

		}

		if (response != false) {
			document.screenSettingsForm.action = "addTrainingScreen.htm";
			document.screenSettingsForm.method = "POST";
			document.screenSettingsForm.addDeleteScreen.value = val;
			document.screenSettingsForm.submit();
		}
	}

	function saveTemplate() {
		$('#hiddenbtnLinkId').val('');
		$('#firstUseURL').val('');

		localStorage.setItem('curActive', "scrn-1");
		localStorage.setItem('curActiveScrnName', "1");
		//$(".paginated-cntrl").find('input[value$="scrn-1"]:radio').prop('checked', true);
		document.screenSettingsForm.action = "saveTrainingScreen.htm";
		document.screenSettingsForm.method = "POST";
		document.screenSettingsForm.submit();

	}
</script>
<div id="wrpr">
	<div class="clear"></div>
	<div class="wrpr-cont relative">
		<div id="slideBtn">
			<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
				src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
		</div>
		<div id="bread-crumb">
			<ul>
				<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
				<li><a href="welcome.htm">Home</a></li>
				<li class="last">Login Screen</li>
			</ul>
		</div>
		<span class="blue-brdr"></span>
		<div class="content" id="login">
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>
			<div class="cont-pnl split" id="equalHt">
				<div class="cont-block rt-brdr">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-login">&nbsp;</span></li>
							<li>Login Details</li>
						</ul>
					</div>
					<div class="tabd-pnl">
						<ul class="nav-tabs">


							<li><a href="setuploginscreen.htm" title="Login General">Login
									General</a></li>
							<li><a class="brdr-rt active"
								href="setupTrainingScreen.htm" class="rt-brdr"
								title="Training Screen">Training Slides</a></li>

						</ul>
						<div class="clear"></div>
					</div>

					<div class="cont-wrp">
						<c:choose>
							<c:when test="${requestScope.responseStatus eq 'SUCCESS' }">

								<div class="alertBx success">
									<span class="actn-close" title="close"></span>
									<p class="msgBx">
										<c:out value="${requestScope.responeMsg}" />
									</p>
								</div>

							</c:when>

							<c:when test="${requestScope.responseStatus eq 'FAILURE' }">

								<div class="alertBx failure">
									<span class="actn-close" title="close"></span>
									<p class="msgBx">
										<c:out value="${requestScope.responeMsg}" />
									</p>
								</div>

							</c:when>

						</c:choose>



						<form:form name="screenSettingsForm" id="screenSettingsForm"
							commandName="screenSettingsForm" enctype="multipart/form-data"
							action="uploadimg.htm">

							<form:hidden path="hiddenbtnLinkId" />
							<form:hidden path="addDeleteScreen" />
							<form:hidden path="viewName" value="setuptrainingpage" />
							<table width="100%" border="0" cellpadding="0" cellspacing="0"
								class="cmnTbl">

								<tr>
									<td width="33%"><label class="mand">Slide Name</label></td>
									<td width="67%"><form:errors id="screenNameError"
											cssClass="errorDsply" path="screenName"></form:errors>
										<div class="cntrl-grp zeroBrdr">
											<form:select path="screenName" class="slctBx" id="screenName">
												<form:option value="">Select Slide</form:option>
												<form:option selected="selected" value="1">1</form:option>
												<form:option value="2">2</form:option>
												<form:option value="3">3</form:option>
												<form:option value="4">4</form:option>
											</form:select>
										</div></td>
								</tr>

								<!--  For image and PDF radio button -->
								<!-- 
								<tr>
									<td width="40%"><label class="mand">File Type</td>
									<td width="60%" class="actn-cntrl"><span class="">
											<form:radiobutton value="true" path="type" id="cstmImg"
												checked="checked" /> <label for="cstmImg">Image</label>
									</span> <span class="mrgn-left"> <form:radiobutton
												value="false" path="type" id=" cstmPdf" /> <label
											for="cstmPdf">PDF</label>
									</span></td>
								</tr>
								
								 -->

								<!-- For image // Temporary -->
								<tr>
									<td><form:input type="hidden" value="true" path="type" /></td>
								</tr>
								<tr class="uploadImage">
									<td width="36%"><label class="mand">Upload Slide</label></td>
									<td width="67%"><form:errors id="logoerror"
											cssClass="errorDsply" path="logoImageName"></form:errors> <span
										class="topPadding cmnRow"><label for="trgrUpld">
												<input type="button" value="Upload" id="trgrUpldBtn"
												class="btn trgrUpld" title="Upload Image File" tabindex="1">
												<form:hidden path="logoImageName" id="strBannerAdImagePath" />
												<span class="instTxt nonBlk"></span> <form:input type="file"
													class="textboxBig" id="trgrUpld" path="logoImage"
													onchange="checkBannerSize(this);" />
										</label> </span><label id="maxSizeImageError" class="errorDsply maxSizeSlideImageError"></label></td>

								</tr>
								<!-- 
								<tr class="uploadPdf">
									<td width="36%"><label class="mand">Upload PDF</label></td>
									<td width="64%"><div class="cntrl-grp">
											<input class="inputTxtBig" id="scrnContent" type="file">
										</div></td>
								</tr>
								 -->

								<tr>
									<td><label>Associate Page/URL</label></td>
									<td class="multi-inputs"><form:radiobutton path="isUrl"
											value="1" /> <label>Yes</label> <form:radiobutton
											path="isUrl" value="0" checked="checked" /> <label>No</label>
									</td>
								</tr>

								<tr id="trisUrlOrPage" class="hidetr">
									<td></td>
									<td class="multi-inputs"><form:radiobutton
											path="isUrlOrPage" value="1" checked="checked" /> <label>Web
											Link</label> <form:radiobutton path="isUrlOrPage" value="2" /> <label>Anything
											Page</label></td>
								</tr>

								<tr id="trUrl" class="hidetr">
									<td></td>
									<td><form:errors cssClass="errorDsply" path="firstUseURL"
											id="first"></form:errors>
										<div class="cntrl-grp">
											<form:input path="firstUseURL" class="inputTxtBig" value=""
												style="height: 22px;" type="text" />
										</div></td>
								</tr>

								<tr class="hidetr">

									<td colspan="3"><form:errors cssClass="errorDsply"
											path="btnLinkId" id="sec"></form:errors>
										<ul class="infoList" id="AnythingPage">
											<c:forEach items="${sessionScope.anythingPageList}"
												var="item">
												<li><span class="cell zeroMrgn"> <form:input
															type="radio" value="${item.hcAnythingPageId}"
															id="${item.hcAnythingPageId}" name="anythingPage"
															path="btnLinkId" /> <c:out
															value="${item.anythingPageTitle }" />
												</span> <span class="cell">${item.pageType}</span></li>
											</c:forEach>
											<li class="actn"><a href="buildanythingpage.htm"><img
													src="images/btn_add.png" width="24" height="24" alt="add"
													class="addImg" /> Add New AnyThing<sup>TM</sup> Page</a></li>
										</ul></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><input type="button" name="button" value="Add Slide"
										class="btn-blue" id="addBtn"
										onclick="creatDeleteScreen('AddButton');" /></td>
									<!-- 
									<td align="right"><input type="button" name="delBtn" value="Delete Button" class="btn-red" id="delBtn"
										onclick="creatDeleteScreen('DeleteButton');"
									/></td>
									 -->
								</tr>
							</table>
						</form:form>
					</div>
				</div>

				<div class="cont-block">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-iphone">&nbsp;</span></li>
							<li>App Preview</li>
						</ul>
					</div>
					<div class="prgrsLbl">
						<ul>

							<span>Training Slides for First Login</span>
							<li class="saveTmplt"><input type="button" name="button"
								value="Save Slides" class="btn-blue" id="saveTmplt"
								onclick="saveTemplate()" /></li>
							<li class="clear"></li>
						</ul>
					</div>
					<div class="cont-wrp">
						<div id="iphone-preview">
							<!--Iphone Preview For Login screen starts here-->
							<div id="iphone-login-preview">
								<div class="iphone-status-bar"></div>
								<!--  
								<div class="iPhone-preview-container">
									<div class="iphone-login-logo">
										<img src="${sessionScope.trainingScreenLogoPreview}"
											alt="Logo" width="280" height="400" />
									</div>
								</div>
								-->
								<div class="paginated-screen">
									<ul>
										<li id="scrn-1" class="page"><img
											src="${sessionScope.screenImageOne}" alt="" width="320"
											height="480"></li>
										<li id="scrn-2" class="page"><img
											src="${sessionScope.screenImageTwo}" alt="" width="320"
											height="480"></li>
										<li id="scrn-3" class="page"><img
											src="${sessionScope.screenImageThree}" alt="" width="320"
											height="480"></li>
										<li id="scrn-4" class="page"><img
											src="${sessionScope.screenImageFour}" alt="" width="320"
											height="480"></a></li>
									</ul>

								</div>

								<p class="paginated-cntrl">
									<input type="radio" name="pgntn" value="scrn-1"
										checked="checked" /><input type="radio" name="pgntn"
										value="scrn-2" /><input type="radio" name="pgntn"
										value="scrn-3" /><input type="radio" name="pgntn"
										value="scrn-4" />
								</p>
								<div class="screen-cntrl">
									<a href="javascript:void(0);" id="fl_update">Update</a> <a
										href="javascript:void(0);" id="fl_delete" class="icon-del"
										onclick="creatDeleteScreen('DeleteButton');"></a>
								</div>
							</div>
							<!--Iphone Preview For Login screen ends here-->
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/HubCiti/images/popupClose.png" class="closeIframe"
				alt="close"
				onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span
				id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%"
			allowtransparency="yes" width="100%" style="background-color: White">
		</iframe>
	</div>
</div>


<script type="text/javascript">
	configureMenu("setuploginpage");

	if ($("#screenName").length) {
		var srnName = $("#screenName option:selected").attr("value");
		//alert(t);
		var tmp = "scrn-" + srnName;
		$("input[value='" + tmp + "']").attr("checked", true).trigger('click');
	}

	$("input[name='pgntn']").on('click', function() {
		var radioChecked = $(this).attr("value");
		localStorage.setItem('curActive', radioChecked);
		$(".page").hide();
		$("#" + $(this).attr("value")).show();
	});
	var tst = localStorage.getItem('curACtive');
	$("input[value='" + tst + "']").trigger('click');

	$("#screenName").change(
			function() {
				var srnName = $("#screenName option:selected").attr("value");
				localStorage.setItem('curActiveScrnName', localStorage.getItem(
						'curActive').split("-").pop());
				$(".page").hide();
				$("#" + $(this).attr("value")).show();
			});
	var scrnSelected = localStorage.getItem('curActiveScrnName');
	$("#screenName option[value='" + scrnSelected + "']")
			.attr("selected", true);

	$("input[name='pgntn']").on(
			'click',
			function() {
				var srnName = $("#screenName option:selected").attr("value");
				localStorage.setItem('curActiveScrnName', localStorage.getItem(
						'curActive').split("-").pop());
				$(".page").hide();
				$("#" + $(this).attr("value")).show();
			});
	var scrnSelected = localStorage.getItem('curActiveScrnName');
	$("#screenName option[value='" + scrnSelected + "']")
			.attr("selected", true);
</script>
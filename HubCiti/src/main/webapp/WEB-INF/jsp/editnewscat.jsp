<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>
<script type="text/javascript"
	src="/HubCiti/scripts/colorPickDynamic.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/colorPicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="/HubCiti/styles/colorPicker.css" />
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript" src="/HubCiti/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/HubCiti/styles/jquery-ui.css" />
<script src="/HubCiti/scripts/jquery-ui.js"></script>
<script src="/HubCiti/scripts/global.js"></script>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>News Settings</title>

<script>
	$(document)
			.ready(
					function() {

						var vHiddenNews = $('#hidIsNewsTicker').val();
						if (vHiddenNews === "newsyes") {

							$('.newsno').show();
							$('#newsyes').prop('checked', true);
							$('#newsno').prop('checked', false);

						} else {

							$('.newsno').hide();
							$('#newsno').prop('checked', true);
							$('#newsyes').prop('checked', false);
						}

						$('input:radio[name="isNewsTicker"]')
								.change(
										function() {
											var vIsNewsTicker = $(this).attr(
													"id");

											$("input[name='isNewsTicker']")
													.removeAttr("checked");
											if (vIsNewsTicker === "newsyes") {

												$('.newsno').show();
												$('#newsyes').prop('checked',
														true);
												$('#newsno').prop('checked',
														false);

												//document.getElementById('noStories.errors').style.display = 'none';

											} else {

												if (document
														.getElementById('noStories.errors') != null) {
													document
															.getElementById('noStories.errors').style.display = 'none';
												}
												$("#noStories").val("");
												$('.newsno').hide();
												$('#newsno').prop('checked',
														true);
												$('#newsyes').prop('checked',
														false);

												//	document.getElementById('noStories.errors').style.display = 'none';

											}

										});
						$('input[name="isNewsTicker"]:checked').trigger(
								"change");
						//$('#'+hidIsNewsTicker).attr("checked",true).trigger("change");

					});
	$(function() {
		var fixHelper = function(e, ui) {
			ui.children().each(function() {
				$(this).width($(this).width());
			});
			return ui;
		};

		$("#newsCtgry tbody").sortable({
			'opacity' : 0.6,
			containment : '.scrollTbl',
			cursor : "move",
			tolerance : "pointer",
			helper : fixHelper /*fix for mozilla click trigger*/,
		});

		/* Close SubCategory popup */
		$(".modal-close,.btn-grey").on('click.popup', function() {
			$(".modal-popupWrp").hide();
			$(".modal-popup").slideUp();
			$(".modal-popup").find(".modal-bdy input").val("");
			$(".modal-popup i").removeClass("errDsply");
		});
		$(".full").spectrum(
				{
					color : $("#getColor").val(),
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log(color.toHexString());
						var setColor = $("#getColor").attr("value",
								color.toHexString());
						console.log(setColor);
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});

		$(".bBtnColor").spectrum(
				{
					color : $("#getColor").val(),
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log(color.toHexString());
						var setColor = $("#getColor").attr("value",
								color.toHexString());
						console.log(setColor);
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});

		$("#categoryColor").spectrum(
				{
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log("page" + color.toHexString());
						$(this).attr("value", color.toHexString());
						$(".navBar").css("background", color.toHexString());
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});

		$("#categoryFontColor").spectrum(
				{
					showInput : true,
					showInitial : true,
					showPalette : true,
					preferredFormat : "hex",
					hide : function(color) {
						console.log("page" + color.toHexString());
						$(this).attr("value", color.toHexString());
					},
					palette : [
							[ "rgb(0, 0, 0)", "rgb(67, 67, 67)",
									"rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
									"rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/
									"rgb(255, 255, 255)" ],
							[ "rgb(152, 0, 0)", "rgb(255, 0, 0)",
									"rgb(255, 153, 0)", "rgb(255, 255, 0)",
									"rgb(0, 255, 0)", "rgb(0, 255, 255)",
									"rgb(74, 134, 232)", "rgb(0, 0, 255)",
									"rgb(153, 0, 255)", "rgb(255, 0, 255)" ],
							[ "rgb(230, 184, 175)", "rgb(244, 204, 204)",
									"rgb(252, 229, 205)", "rgb(255, 242, 204)",
									"rgb(217, 234, 211)", "rgb(208, 224, 227)",
									"rgb(201, 218, 248)", "rgb(207, 226, 243)",
									"rgb(217, 210, 233)", "rgb(234, 209, 220)",
									"rgb(221, 126, 107)", "rgb(234, 153, 153)",
									"rgb(249, 203, 156)", "rgb(255, 229, 153)",
									"rgb(182, 215, 168)", "rgb(162, 196, 201)",
									"rgb(164, 194, 244)", "rgb(159, 197, 232)",
									"rgb(180, 167, 214)", "rgb(213, 166, 189)",
									"rgb(204, 65, 37)", "rgb(224, 102, 102)",
									"rgb(246, 178, 107)", "rgb(255, 217, 102)",
									"rgb(147, 196, 125)", "rgb(118, 165, 175)",
									"rgb(109, 158, 235)", "rgb(111, 168, 220)",
									"rgb(142, 124, 195)", "rgb(194, 123, 160)",
									"rgb(166, 28, 0)", "rgb(204, 0, 0)",
									"rgb(230, 145, 56)", "rgb(241, 194, 50)",
									"rgb(106, 168, 79)", "rgb(69, 129, 142)",
									"rgb(60, 120, 216)", "rgb(61, 133, 198)",
									"rgb(103, 78, 167)", "rgb(166, 77, 121)",
									/*"rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
									"rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",*/
									"rgb(91, 15, 0)", "rgb(102, 0, 0)",
									"rgb(120, 63, 4)", "rgb(127, 96, 0)",
									"rgb(39, 78, 19)", "rgb(12, 52, 61)",
									"rgb(28, 69, 135)", "rgb(7, 55, 99)",
									"rgb(32, 18, 77)", "rgb(76, 17, 48)" ] ]
				});
	});

	function updateCategory() {
		var vCatcolor = document.addnewsform.catColor.value;

		if (vCatcolor == null || vCatcolor == "") {
			document.addnewsform.catColor.value = "#000000";
		}

		var CateName = $("#catName").val();

		$("#hidCatName").val(CateName);
		document.addnewsform.action = "updatenewscategory.htm";
		document.addnewsform.method = "post";
		document.addnewsform.submit();

	}
	function backBtn() {
		document.addnewsform.catName.value = "";
		document.addnewsform.action = "managenewscats.htm";
		document.addnewsform.method = "post";
		document.addnewsform.submit();

	}

	function showModal(newscatId) {
		$('#subcatIdjs').text('');
		$.ajaxSetup({
			cache : false
		})
		$.ajax({
			type : "GET",
			url : "fetchnewssubcat.htm",
			data : {
				'newscatid' : newscatId
			},

			success : function(response) {
				$('.addcat').html(response);
				var bodyHt = $('body').height();
				var setHt = parseInt(bodyHt);
				$(".modal-popupWrp").show();
				$(".modal-popupWrp").height(setHt);
				$(".modal-popup").slideDown('fast');
			},
			error : function(e) {

			}
		});
	}
	function savenewsSubCat() {

		$('#subcatIdjs').text('');
		var isCurrectURls = true;
		var urlPattern = new RegExp('^(http|https|www)://.*$');

		var urls = $("tbody.addcat tr").find("td:eq(1) input").map(
				function(index, element) {
					if ($(this).val().length) {

						var vUrls = $(this).val();
						vUrls = $.trim(vUrls);
						var isCurrect = urlPattern.test(vUrls);
						if (!isCurrect) {
							isCurrectURls = false;
							$(this).parent('div').prev().text(
									'Please enter correct url');
						} else {
							$(this).parent('div').prev().text('');
						}
						return (vUrls);
					} else {
						$(this).parent('div').prev().text('');
					}
				}).toArray();

		if (isCurrectURls) {
			var subcats = $("tbody.addcat tr").find("td:eq(1) input").map(
					function(index, element) {
						if ($(this).val().length) {
							return ($(this).parents('tr').find('td:eq(0) div')
									.attr('id'));
						}

					}).toArray();

			urls = $.trim(urls);
			$.ajaxSetup({
				cache : false
			})
			$.ajax({
				type : "GET",
				url : "savenewssubcat.htm",
				data : {
					'newscatid' : document.addnewsform.catId.value,
					'subcats' : subcats.toString(),
					'urls' : urls.toString()

				},
				success : function(response) {
					$('#subcatIdjs').text('Saved successfully');
					$('.addcat').html(response);
					var bodyHt = $('body').height();
					var setHt = parseInt(bodyHt);
					$(".modal-popupWrp").show();
					$(".modal-popupWrp").height(setHt);
					$(".modal-popup").slideDown('fast');
				},
				error : function(e) {

				}
			});
		}
	}
</script>
</head>
<body>
	<!--Wrapper div Starts-->
	<div id="wrpr">
		<div class="clear"></div>
		<div class="wrpr-cont relative">
			<div id="slideBtn">
				<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img
					src="images/slide_off.png" width="11" height="28" alt="btn_off" /></a>
			</div>
			<!--Breadcrum div starts-->
			<div id="bread-crumb">
				<ul>
					<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
					<li><a href="welcome.htm">Home</a></li>
					<li class="last">News Settings</li>
				</ul>
			</div>
			<!--Breadcrum div ends-->
			<span class="blue-brdr"></span>
			<!--Content div starts-->
			<div class="content" id="login">
				<!--Left Menu div starts-->
				<div id="menu-pnl" class="split">
					<jsp:include page="leftNavigation.jsp"></jsp:include>
				</div>
				<!--Left Menu div ends-->
				<!--Content panel div starts-->
				<div class="cont-pnl split" id="">
					<div class="cont-block stretch">
						<div class="title-bar">
							<ul class="title-actn">
								<li class="title-icon"><span class="icon-news">&nbsp;</span></li>
								<li>News Settings</li>
							</ul>
						</div>
						<div class="tabd-pnl">
							<ul class="nav-tabs">

								<li><a href="newsgeneralsettings.htm"
									title="News General Settings">News General Settings</a></li>
								<li><a href="addcatsettings.htm" class="rt-brdr"
									title="News Categories">News Categories</a></li>
								<li><a href="#" class="rt-brdr active"
									title="Edit News Categories">Edit News Categories</a></li>


							</ul>
							<div class="clear"></div>
						</div>
						<div class="cont-wrp">
							<c:if
								test="${requestScope.catnameexistsmsg ne null && !empty requestScope.catnameexistsmsg}">
								<span class="highLightTxt_alert" id="catexistsMsg"> <c:out
										value="${requestScope.catnameexistsmsg}" />
								</span>
							</c:if>
							<form:form name="addnewsform" id="addnewsform"
								commandName="addnewsform">

								<form:hidden path="newsSettingsID" />
								<form:hidden path="catId" />
								<!--<form:hidden path="catName" />-->
								<form:hidden path="isSubCatFlag" />
								<form:hidden path="hidIsNewsTicker" id="hidIsNewsTicker" />
								<form:hidden path="hidCatName" id="hidCatName" />
								<form:hidden path="isFeed" />
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="cmnTbl">
									<c:if
										test="${ null ne addnewsform.isFeed && addnewsform.isFeed eq false }">
										<tr>
											<td><label class="mand"> Category Name</label></td>
											<td class="cmnTbl mainMenu subMenu"><form:errors
													cssClass="errorDsply" path="nonfeedCatName"></form:errors>
												<div class="cntrl-grp">
													<form:input path="nonfeedCatName" class="inputTxtBig"></form:input>
												</div></td>
												<form:hidden path="catName"/>
										</tr>
									</c:if>
									<c:if
										test="${ null ne addnewsform.isFeed && addnewsform.isFeed eq true }">
										<tr>
											<td width="20%"><label class="mand">Category </label></td>
											<td width="31%"><form:errors cssClass="errorDsply"
													path="catName"></form:errors>
												<div class="cntrl-grp">
													<form:input type='text' path="catName" id="catName"
														readonly="readonly" disabled="true" class="inputTxtBig" />
													<form:hidden path="nonfeedCatName" />
												</div></td>
											<td align="right">&nbsp;</td>
										</tr>
									</c:if>
									<tr>
										<td><label class="mand">Background Color</label></td>
										<td class="full"><form:errors cssClass="errorDsply"
												path="catColor"></form:errors> <form:input class="full"
												id="categoryColor" path="catColor" /></td>
									</tr>
									<tr>
										<td><label class="mand">Font Color</label></td>
										<td class="cmnTbl"><form:errors cssClass="errorDsply"
												path="catFontColor"></form:errors> <form:input class="white"
												id="categoryFontColor" path="catFontColor" tabindex="2" /></td>
									</tr>
									<tr>
									<tr>
										<td><label class="mand">Back Button Color</label></td>
										<td class="cmnTbl"><form:errors cssClass="errorDsply"
												path="BackButtonColor"></form:errors> <form:input
												class="bBtnColor" id="backBtntColor" path="BackButtonColor"
												tabindex="2" /></td>
									</tr>
									<c:if
										test="${ null ne addnewsform.isFeed && addnewsform.isFeed eq true }">
										<tr class="js-combinational">
											<td><label class="mand">Category Display</label></td>
											<td class="cmnTbl mainMenu subMenu"><form:errors
													cssClass="errorDsply" path="displayTypeName"></form:errors>
												<div class="cmnTbl mainMenu subMenu">
													<form:select path="displayTypeName" class="slctBx"
														id="alrtCtgry">
														<c:forEach items="${sessionScope.displayTypes}"
															var="dispName">
															<c:choose>
																<c:when
																	test="${dispName.displayType eq addnewsform.displayTypeName}">
																	<option value="${dispName.displayType}"
																		selected="selected">${dispName.displayType}</option>
																</c:when>
																<c:otherwise>
																	<option value="${dispName.displayType}">${dispName.displayType}</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</form:select>
												</div></td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</c:if>

									<!--
									<c:choose>
										<c:when test="${ null != addnewsform.isSubCatFlag && addnewsform.isSubCatFlag == true }">
											<tr>
												<td align="left"><label> Sub Category </label></td>
												<td><input type="button" value="Association" class="btn-blue" id="subCategory"
													onclick="showModal('<c:out value="${addnewsform.catId}"/>')" /></td>

												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
										</c:when>
									</c:choose> -->
									<tr>
										<td><label class="mand">URL</label></td>
										<td colspan="4"><form:errors cssClass="errorDsply"
												path="catfeedURL"></form:errors>
											<div class="cntrl-grp">
												<form:input type='text' path="catfeedURL"
													class="inputTxtBig" />
											</div></td>
									</tr>
									<c:if
										test="${ null ne addnewsform.isFeed && addnewsform.isFeed eq true }">
										<tr>
											<td><label class="mand">Sub Page</label></td>
											<td class="cmnTbl mainMenu subMenu"><form:errors
													cssClass="errorDsply" path="subPageName"></form:errors>
												<div class="cmnTbl mainMenu subMenu">
													<form:select path="subPageName" class="slctBx"
														id="alrtCtgry">
														<c:forEach items="${sessionScope.subPageTypes}"
															var="subpageName">
															<c:choose>
																<c:when test="${subpageName eq addnewsform.subPageName}">
																	<option value="${subpageName}" selected="selected">${subpageName}</option>
																</c:when>
																<c:otherwise>
																	<option value="${subpageName}">${subpageName}</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</form:select>
												</div></td>

											<td class="cmnTbl mainMenu subMenu">&nbsp;</td>
										</tr>
									</c:if>
									<tr>
										<td><label class="mand">Default</label></td>
										<td class="cmnTbl mainMenu subMenu"><span
											class="actn-cntrl"><span class=""> <form:radiobutton
														path="isDefault" name="isDefault" id="left" value="true"
														class="" checked="checked" tabindex="3" /> <label
													for="left">Yes</label>
											</span> <span class="mrgn-left"> <form:radiobutton
														path="isDefault" name="isDefault" id="Right" value="false"
														class="" tabindex="4" /> <label for="right">No</label>
											</span></span></td>

									</tr>
									<c:if
										test="${ null ne addnewsform.isFeed && addnewsform.isFeed eq true }">
										<tr class="js-scrolling">
											<td><label class="mand">News Ticker</label></td>
											<td class="cmnTbl mainMenu subMenu"><span
												class="actn-cntrl"><span class=""> <form:radiobutton
															path="isNewsTicker" name="isNewsTicker" id="newsyes"
															value="true" class="" tabindex="8" /> <label for="left">Yes</label>
												</span> <span class="mrgn-left"> <form:radiobutton
															path="isNewsTicker" name="isNewsTicker" id="newsno"
															value="false" class="" checked="checked" tabindex="9" />
														<label for="right">No</label>
												</span></span></td>

										</tr>
										<tr class="newsno">
											<td><label class="mand noStories">Number of
													Stories</label></td>
											<td class="cmnTbl mainMenu subMenu noStories"><form:errors
													cssClass="errorDsply" path="noStories"></form:errors>
												<div class="cntrl-grp noStories">
													<form:input type='text' path="noStories" visible="false"
														class="inputTxtBig" tabindex="10"
														onkeypress="return isNumberKey(event)" maxlength="1" />
												</div></td>
										</tr>
									</c:if>
								</table>
								<div align="right" class="col">
									<input type="button" onclick="backBtn()" value="Back"
										class="btn-blue mrgnRt"> <input type="button"
										onclick="updateCategory();" value="Save" class="btn-blue">
								</div>
							</form:form>
						</div>

					</div>
					<!--Content panel div ends-->
				</div>
				<!--Content div ends-->
			</div>
		</div>
	</div>

	<!-- News Sub Category Module Display  -->
	<div class="modal-popupWrp">
		<div class="modal-popup subCtgryModal">
			<div class="modal-hdr">
				<a class="modal-close" title="close">�</a>
				<h3>
					<span></span>Sub Category Settings
				</h3>
				<span id="subcatIdjs" class="highLightTxt"></span>
			</div>
			<div class="modal-bdy subcatmodal">

				<table cellpadding="0" cellspacing="0" width="100%"
					class="grdTbl  mrgnTop_small fullBrdr" id="sub-ctgry-settings">
					<thead>
						<tr class="tblHdr">
							<!-- <th width="10%"><input type="checkbox" id="chkAll" /></th> -->
							<th width="35%">Sub Category Name</th>
							<th width="55%">Feed URL</th>
						</tr>
					</thead>
					<tbody class="addcat">
					</tbody>
				</table>
			</div>
			<div class="modal-ftr">
				<p align="right">
					<input type="button" class="btn-blue" value="Save" name="button"
						onclick="savenewsSubCat()"> <input type="button"
						class="btn-grey" value="Cancel" id="" name="Cancel"
						onclick="cancel()">
				</p>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	configureMenu("newssettings");
</script>
</html>
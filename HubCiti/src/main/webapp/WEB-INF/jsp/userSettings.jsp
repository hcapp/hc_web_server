<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/hubcitileftmenu.tld"%>
<script type="text/javascript" src="scripts/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/HubCiti/scripts/jquery.form.js"></script>

<script type="text/javascript">
    $(document).ready(
            function() {

                var options = document.screenSettingsForm.userSettingsFields.value;
                var req = document.screenSettingsForm.requiredFields.value;
                var reqs = req.split(',');
                var myArray = options.split(',');
				
                for (var i = 0; i < myArray.length; i++) {
                    arrayId = myArray[i];
                    $('#userSettingTable tr.optnl').each(
                            function() {
                                var id = $(this).find('input').attr('class');

                                if (id == arrayId && arrayId != "optn-img") {

                                    if (id == "First_Name" || id == "Zip_Code") {
                                        $(this).find('input[name="dynForm"]').attr('checked', true).attr('disabled', true);
                                    } else {
                                        $(this).find('input[name="dynForm"]').attr('checked', true);
                                    }
                                    $(this).find('input[name="required"]').removeProp("disabled");

                                    if (reqs[i] == true) {
                                        $(this).find('input[name="required"]').attr('checked', true);
                                        var rowId = $(this).find('input[name="dynForm"]').attr("class");
                                        $(this).find('td > span.reqfield').show();
                                        $("#dynFormTbl tr#" + rowId).find('td > span.reqfield').show();

                                    }

                                    rowCnt = $("#dynFormTbl tbody > tr").length;
                                    firstRow = $("#dynFormTbl > tbody")
                                    lstRow = $("#dynFormTbl > tbody tr:last");
                                    cellLbl = $(this).find('td:eq(0)').html();
                                    cellData = $(this).find('td:eq(3)').html();
                                    var newRow = $("<tr id='" + arrayId + "'><td>" + cellLbl + "</td><td>" + cellData + "</td ></tr>");
                                    rowCnt == 0 ? firstRow.append(newRow).find('.cntrl-grp input,.cntrl-grp select').attr("disabled", "disabled") : newRow.insertAfter(lstRow).find(
                                            '.cntrl-grp input,.cntrl-grp select').prop("disabled", "disabled");

                                }
                                if (id == arrayId && arrayId == "optn-img") {

                                    $("#imgUpload").show();
                                    $(this).find('input[name="dynFormImg"]').attr('checked', true);
                                    $(this).find('input[name="required"]').removeProp("disabled");
                                    var imgSrc = $(this).find('td:eq(1) input').attr("imgsrc");

                                    if (imgSrc) {
                                        $(".usrImg").html("<img src='"+imgSrc+"'/>");
                                        $(this).find("div").css("display", "block");
                                    } else {
                                        $(".usrImg").html("");
                                        $(this).find("div").css("display", "block");
                                    }

                                    if (reqs[i] == true) {
                                        $(this).find('input[name="required"]').attr('checked', true);
                                        $(this).find('td > span.reqfield').show();
                                    }
                                }
                            });
                }

                $('#trgrUpldImg').bind('change', function() {

                    /* var selectedopt = ',';
                    $('#dynFormTbl tr').each( function() {
                    	selectedopt += $(this).attr('id') + ",";
                    });
                    selectedopt += "optn-img";
                    
                    document.screenSettingsForm.userSettingsFields.value = selectedopt;
                    alert(selectedopt) */

                    settingsFields();

                    var imageType = document.getElementById("trgrUpldImg").value;
                    if (imageType != '') {
                        var checkbannerimg = imageType.toLowerCase();
                        if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
                            alert("You must upload image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
                            return false;
                        } else {
                            $("#screenSettingsForm").ajaxForm({
                                success : function(response) {
                                    var imgRes = response.getElementsByTagName('imageScr')[0].firstChild.nodeValue;
                                    var substr = imgRes.split('|');
                                    if (substr[0] == 'maxSizeImageError') {
                                        $('#maxSizeImageError').text("Image Dimension should not exceed Width: 800px Height: 600px");
                                    } else {
                                        openIframePopup('ifrmPopup', 'ifrm', '/HubCiti/cropImage.htm', 100, 99.5, 'Crop Image');
                                    }

                                }
                            }).submit();
                        }
                    }

                });

                $("input[name='dynForm']").on(
                        'change.showOptns',
                        function() {
                            var curChkd = $(this).prop("checked");
                            frmOptn = $(this).attr("class");
                            lstRow = $("#dynFormTbl > tbody tr:last");
                            firstRow = $("#dynFormTbl > tbody");
                            prntRow = $(this).parents('tr');
                            cellLbl = $(prntRow).find('td:eq(0)').html();
                            cellData = $(prntRow).find('td:eq(3)').html();
                            reqCol = $(this).parent('td').next().find('input[type="checkbox"]');
                            rowCnt = $("#dynFormTbl tbody > tr").length;
                            var newRow = $("<tr id='" + frmOptn + "' pos='" + genDynId('row-') + "'>" + "<td>" + cellLbl + "</td><td><div class='cntrl-grp zeroBrdr'>" + cellData
                                    + "</div></td></tr>");
                            if (curChkd) {
                                reqCol.removeProp("disabled");
                                rowCnt == 0 ? firstRow.append(newRow).find('.cntrl-grp input,.cntrl-grp select').attr("disabled", "disabled") : newRow.insertAfter(lstRow).find(
                                        '.cntrl-grp input,.cntrl-grp select').prop("disabled", "disabled");
                            } else {
                                reqCol.prop("disabled", true).removeProp('checked');
                                $(this).parents('tr').find('td:eq(0) span:first-child').hide();
                                $('#' + frmOptn).remove();
                            }
                        });

                //$("input[name='dynForm']:checked").trigger('change.showOptns');
                $("input[name='required']").on('change', setRequired);

                $("input[name='dynFormImg']").on(
                        'change',
                        function() {
                            var curChkd = $(this).prop("checked");
                            reqCol = $(this).parent('td').next().find('input[type="checkbox"]');
                            curChkd === true ? $("#imgUpload").show() && $('#dynFormImgPreview').show() && reqCol.removeProp("disabled") : $("#imgUpload").hide() && $('#dynFormImgPreview').hide()
                                    && reqCol.prop("disabled", true).removeProp('checked');
                        });

                /* $("input[name='dynFormImg']").on('change',function() {
                
                	var imgSrc = $(this).attr("imgSrc");
                	var curChkd = $(this).prop("checked");
                		if ($(this).attr("class") == "optn-img" && curChkd && imgSrc) {
                			$(".usrImg").html("<img src='"+imgSrc+"'/>");
                			$(this).parents('tr.optnl').find("div").css("display","block");
                		} else if ($(this).attr("class") == "optn-img" && curChkd && !imgSrc) {
                			$(".usrImg").html("");
                			$(this).parents('tr.optnl').find("div").css("display","block");
                		} else {
                			$(".usrImg").html("");
                			$(this).parents('tr.optnl').find("div.cntrl-grp1").css("display", "none");
                			document.getElementById('logoImageName.errors').style.display = 'none';
                		}
                }); */

                function setRequired() {

                    var isChecked = $(this).prop('checked');
                    var rowId = $(this).parents('tr').find('input[name="dynForm"]').attr("class");
                    if (isChecked) {
                        $(this).parents('tr').find('td > span.reqfield').show();
                        $("#dynFormTbl tr#" + rowId).find('td > span.reqfield').show();
                    } else {
                        $(this).parents('tr').find('td > span.reqfield').hide();
                        $("#dynFormTbl tr#" + rowId).find('td > span.reqfield').hide();
                    }
                }
            });

    function settingsFields() {
        var selectedopt = '';
        var selectedreq = '';

        $('#dynFormTbl tr').each(function() {
            selectedopt += $(this).attr('id') + ',';
            mnd = $(this).attr('id');
            var isSame = false;
            var currow1 = $('#userSettingTable tr');
            var mnd1 = $(currow1).find('input[name="required"]:checked');
            $(mnd1).each(function() {
                mnd1 = $(this).parents('tr').find('td:eq(1) input[name="dynForm"]').attr("class");
                if (mnd == mnd1) {
                    isSame = true;
                }

            });

            if (isSame == true) {
                selectedreq += '1' + ',';
            } else {
                selectedreq += '0' + ',';
            }
        });

        $('#userSettingTable tr.optnl input:checked').each(function() {
            var displayProp = $(this).attr('class');
            if (displayProp == "optn-img") {
                selectedopt += displayProp + ",";
                var isSame = false;
                $('#userSettingTable tr.optnl input[name="required"]:checked').each(function() {
                    var requiredProp = $(this).parents('tr').find('td:eq(1) input[name="dynFormImg"]').attr("class");

                    if (requiredProp == displayProp) {
                        isSame = true;
                    }
                });
                if (isSame == true) {
                    selectedreq += '1' + ',';
                } else {
                    selectedreq += '0' + ',';
                }
            }
        });

        document.screenSettingsForm.userSettingsFields.value = selectedopt;
        document.screenSettingsForm.requiredFields.value = selectedreq;
    }
</script>
<script type="text/javascript">
    function saveUserSettings() {
        settingsFields();
        document.screenSettingsForm.action = "saveusersettings.htm";
        document.screenSettingsForm.method = "POST";
        document.screenSettingsForm.submit();

    }
</script>

<div id="wrpr">
	<div class="wrpr-cont relative">
		<div id="slideBtn">
			<a href="#" onclick="revealPanel(this);" title="Hide Menu"><img src="images/slide_off.png" width="11" height="28" alt="btn_off" /> </a>
		</div>
		<div id="bread-crumb">
			<ul>

				<li class="scrn-icon"><span class="icon-home">&nbsp;</span></li>
				<li><a href="welcome.htm">Home</a></li>
				<li class="last">User Settings</li>
			</ul>
		</div>
		<!--Breadcrum div starts-->
		<span class="blue-brdr"></span>
		<!--Content div starts-->
		<div class="content" id="">
			<!--Left Menu div starts-->
			<div id="menu-pnl" class="split">
				<jsp:include page="leftNavigation.jsp"></jsp:include>
			</div>
			<!--Left Menu div ends-->
			<!--Content panel div starts-->
			<div class="cont-pnl split" id="equalHt">
				<div class="cont-block rt-brdr">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-usrstng">&nbsp;</span></li>
							<li>User Settings</li>
						</ul>
					</div>
					<div class="cont-wrp">
						<form:form name="screenSettingsForm" id="screenSettingsForm" commandName="screenSettingsForm" enctype="multipart/form-data"
							action="uploadimg.htm">
							<form:hidden path="userSettingsFields" />
							<form:hidden path="requiredFields" />
							<form:hidden path="pageView" value="${sessionScope.pageView}" />
							<form:hidden path="viewName" value="userSettings" />
							<form:hidden path="oldImageName" />
							<c:if test="${requestScope.responseStatus ne null && !empty requestScope.responseStatus}">
								<c:choose>
									<c:when test="${requestScope.responseStatus eq 'INFO' }">
										<div class="alertBx warning zeroMrgn cntrAlgn">
											<span class="actn-close" title="close"></span>
											<p class="msgBx">
												<c:out value="${requestScope.responeMsg}" />
											</p>
										</div>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${requestScope.responseStatus eq 'SUCCESS' }">
												<div class="alertBx success zeroMrgn cntrAlgn">
													<span class="actn-close" title="close"></span>
													<p class="msgBx">
														<c:out value="${requestScope.responeMsg}" />
													</p>
												</div>
											</c:when>
											<c:otherwise>
												<div class="alertBx failure zeroMrgn cntrAlgn">
													<span class="actn-close" title="close"></span>
													<p class="msgBx">
														<c:out value="${requestScope.responeMsg}" />
													</p>
												</div>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</c:if>

							<table width="100%" border="0" cellpadding="0" cellspacing="0" class="cmnTbl formLbl" id="userSettingTable">
								<tr class="optnl">
									<th>&nbsp;</th>
									<th align="center">Display</th>
									<th align="center">Required</th>
								</tr>
								<tr class="optnl">
									<td width="46%">First Name <span class="reqfield">*</span></td>
									<td width="27%" align="center"><input type="checkbox" name="dynForm" class="First_Name" disabled="disabled" /></td>
									<td width="27%" align="center"><input type="checkbox" name="required" disabled="disabled" /></td>
									<td><div class="cntrl-grp">
											<input type="text" class="inputTxtBig solidd" />
										</div></td>
								</tr>
								<tr class="optnl">
									<td>Last Name <span class="reqfield">*</span></td>
									<td align="center"><input type="checkbox" name="dynForm" class="Last_Name" /></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td><div class="cntrl-grp">
											<input type="text" class="inputTxtBig solidd" />
										</div></td>
								</tr>
								<tr class="optnl">
									<td>Email <span class="reqfield">*</span></td>
									<td align="center"><input type="checkbox" name="dynForm" class="Email" /></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td>
										<div class="cntrl-grp zero-brdr" align="right">
											<input type="text" class="inputTxtBig solidd" />
											<div class="prmry-link link">Change Password</div>
										</div>
										
										
									</td>
								</tr>
								<tr class="optnl">
									<td>Zip Code <span class="reqfield">*</span></td>
									<td align="center"><input type="checkbox" name="dynForm" class="Zip_Code" disabled="disabled" /></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td><div class="cntrl-grp">
											<input type="text" class="inputTxtBig solidd" />
										</div></td>
								</tr>
								<tr class="optnl">
									<td>Mobile # <span class="reqfield">*</span></td>
									<td width="27%" align="center"><input type="checkbox" name="dynForm" class="optn-mob" /></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td><div class="cntrl-grp">
											<input type="text" class="inputTxtBig solidd" />
										</div></td>
								</tr>
								<tr class="optnl">
									<td>Gender <span class="reqfield">*</span></td>
									<td align="center"><input type="checkbox" name="dynForm" class="optn-gndr" /></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td><div class="cntrl-grp zero-brdr">
											<span class=""> <input type="radio" class="" value="radio" id="gndrMale" name="setGndr"> <label for="gndrMale">Male</label>
											</span> <span class="mrgn-left"> <input type="radio" value="radio" id="gndrFemale" name="setGndr"> <label for="gndrFemale">Female</label>
											</span>
										</div></td>
								</tr>
								<tr class="optnl">
									<td>Education <span class="reqfield">*</span></td>
									<td align="center"><input type="checkbox" name="dynForm" class="optn-eductn" />
										<div class="cntrl-grp zeroBrdr">
											<select id="eductn" class="slctBx">
												<option selected="selected">Select</option>
											</select>
										</div></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td><div class="cntrl-grp zeroBrdr">
											<select id="eductn" class="slctBx">
												<option selected="selected">Select</option>
											</select>
										</div></td>
								</tr>
								<tr class="optnl">
									<td>Marital Status <span class="reqfield">*</span></td>
									<td align="center"><input type="checkbox" name="dynForm" class="optn-mrtlSts" /></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td><div class="cntrl-grp zeroBrdr">
											<select id="mrtlSts" class="slctBx">
												<option selected="selected">Select</option>
											</select>
										</div></td>
								</tr>
								<tr class="optnl">
									<td>Income Range <span class="reqfield">*</span></td>
									<td align="center"><input type="checkbox" name="dynForm" class="optn-income" /></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td><div class="cntrl-grp zeroBrdr">
											<select id="income" class="slctBx">
												<option selected="selected">Select</option>
											</select>
										</div></td>
								</tr>
								<tr class="optnl">
									<td>Date Of Birth <span class="reqfield" >*</span></td>
									<td align="center"><input type="checkbox" name="dynForm" class="optn-dob" /></td>
									<td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td>
									<td><div class="cntrl-grp">
											<input type="text" class="inputTxtBig solid" />
										</div></td>
								</tr>

								<tr class="optnl">
									<td>Image <span class="reqfield" >*</span></td>
									<td align="center"><c:choose>
											<c:when test="${sessionScope.userSettingImg ne null && sessionScope.userSettingImg ne 'images/upload_image.png'}">
												<input type="checkbox" name="dynFormImg" class="optn-img" imgSrc="${sessionScope.userSettingImg}" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="dynFormImg" class="optn-img" />
											</c:otherwise>
										</c:choose></td>
									<!-- <td align="center"><input type="checkbox" name="required" class="req-chk" disabled="disabled" /></td> -->
								</tr>

								<tr id="imgUpload">
									<td valign="top"></td>
									<td colspan="2">
										<form:errors path="logoImageName" cssClass="errorDsply"></form:errors>
										<div class="upld-cntrl zero-brdr">
											<label> <img id="loginScreenLogo" width="80" height="40" alt="upload" src="${sessionScope.userSettingImg }">
											</label> <span class="topPadding cmnRow"> <label for="trgrUpldImg"> <input type="button" value="Upload" id="trgrUpldBtnImg"
													class="btn trgrUpldImg" title="Upload Image File" tabindex="1"> <form:hidden path="logoImageName" id="strBannerAdImagePath" /> <span
													class="instTxt nonBlk"></span> <form:input type="file" class="textboxBig" id="trgrUpldImg" path="logoImage" />
											</label>
											</span><label id="maxSizeImageError" class="errorDsply"></label>
										</div></td>
									<td></td>
								</tr>

								<tr>
									<td>&nbsp;</td>
									<td colspan="2"><input type="button" title="${sessionScope.btnname}" value="${sessionScope.btnname}" class="btn-blue"
										onclick="saveUserSettings();" /></td>
								</tr>
							</table>
						</form:form>
					</div>
				</div>
				<div class="cont-block">
					<div class="title-bar">
						<ul class="title-actn">
							<li class="title-icon"><span class="icon-iphone">&nbsp;</span></li>
							<li>Preview</li>
						</ul>
					</div>
					<div class="cont-wrp">
						<div id="iphone-preview">
							<!--Iphone Preview For Login screen starts here-->
							<div id="iphone-aboutus-preview">
								<div class="iphone-status-bar"></div>
								<div class="navBar iphone">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
										<tr>
											<td width="19%"><img src="images/backBtn.png" alt="back" width="50" height="30" /></td>
											<td width="54%" class="genTitle">Tell us about you</td>
											<td width="27%" align="center"><img src="images/mainMenuBtn.png" width="78" height="30" alt="mainmenu" /></td>
										</tr>
									</table>
								</div>
								<div class="previewAreaScroll gnrlScrn zeroBg">
									<div class="iPhone-preview-container">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobTbl mrgnTop" id="dynFormTbl">

											<tbody>

											</tbody>
										</table>
									</div>
									<div class="usrImg mrgnTop" id="dynFormImgPreview">
										<!--<img src="images/imgDntn.png" alt="dontaion">-->
									</div>
								</div>
							</div>
							<ul class="tabbar" id="tab-main">
								<table width="100%" height="51" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="33%" align="right"><img src="images/privacyplcyBtn_down.png" width="100" height="30" alt="privacypolicy" /></td>
										<td width="47%">&nbsp;</td>
										<td width="20%"><img src="images/saveBtn_down.png" width="58" height="30" alt="save" /></td>
									</tr>
								</table>
							</ul>
							<!--Iphone Preview For Login screen ends here-->
						</div>
					</div>
				</div>
			</div>
			<!--Content panel div ends-->
		</div>
		<!--Content div ends-->
	</div>
	<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="/HubCiti/images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
				title="Click here to Close" align="middle" /> <span id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color: White">
		</iframe>
	</div>
</div>

<script type="text/javascript">
    configureMenu("setupusersettings");
</script>

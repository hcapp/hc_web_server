package com.hubciti.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.exception.HubCitiServiceException;
import com.hubciti.common.pojo.HubCitiImages;
import com.hubciti.common.pojo.ScreenSettings;
import com.hubciti.common.pojo.SpecialOfferPages;
import com.hubciti.common.pojo.User;
import com.hubciti.common.tags.Pagination;
import com.hubciti.common.util.Utility;
import com.hubciti.service.HubCitiService;
import com.hubciti.validator.SpecialOfferValidation;

@Controller
public class SpecialOfferControllerImpl implements SpecialOfferController {

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SpecialOfferControllerImpl.class);;

	private SpecialOfferValidation specialofferValidation;

	@Autowired
	public void setSpecialofferValidation(SpecialOfferValidation specialofferValidation) {
		this.specialofferValidation = specialofferValidation;
	}

	/**
	 * This ModelAttribute sort Special Offer start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public Map<String, String> populatemapDealStartMins() throws HubCitiServiceException {
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort Special Offer start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public Map<String, String> populateDealStartHrs() throws HubCitiServiceException {
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}
	
	/**
	 * 
	 * Controller for landing into the special offer Page.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	public String setupSpecialOfferScreen(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferDetails,
			BindingResult result, HttpServletRequest request, HttpSession session, ModelMap model)
			throws HubCitiServiceException {
		final String methodName = "setupSpecialOfferScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");

		session.setAttribute("minCropHt", 50);
		session.setAttribute("minCropWd", 50);
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

		final List<HubCitiImages> arHcImageList = hubCitiService.getHubCitiImageIconsDisplay("AttachLink");

		session.setAttribute("arHcImageList", arHcImageList);
		session.setAttribute("specialofferScreenImage", ApplicationConstants.DEFAULTIMAGEPATH);
		request.setAttribute(ApplicationConstants.LOWERLIMIT, specialOfferDetails.getLowerLimit());
		model.put("screenSettingsForm", new ScreenSettings());

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupspecialofferpage";
	}

	/**
	 * 
	 * This method saves link to existing Special Offer page details.
	 * 
	 * @param specialOfferDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	public ModelAndView saveorUpdateSpecialOfferScreenDetails(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveSpecialOfferScreenDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		String viewName = null;
		
		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");
		request.setAttribute(ApplicationConstants.LOWERLIMIT, specialOfferPageDetails.getLowerLimit());

		boolean isValidURL = true;
		String strResponse = null;
		final String searchKey = null;
		final Integer lowerLimit = 0;
		final int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		SpecialOfferPages specialofferPages = null;
		
		if( null != specialOfferPageDetails.getPageId()){
			viewName= "setupspecialofferpage";
		}else{
			viewName = "editspecialofferpage";
		}
		specialOfferPageDetails.setTempretids(specialOfferPageDetails.getRetailerLocIds());
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);

		specialofferValidation.validate(specialOfferPageDetails, result);
		isValidURL = Utility.validateURL(specialOfferPageDetails.getPageAttachLink());
		if (!isValidURL) {
			specialofferValidation.validate(specialOfferPageDetails, result, ApplicationConstants.INVALIDURL);
		}

		if (result.hasErrors()) {
			session.setAttribute("ImageIcon", specialOfferPageDetails.getImageIconID());
			return new ModelAndView("setupspecialofferpage");
		} else {
			strResponse = hubCitiService.saveSpecialOfferScreen(specialOfferPageDetails, loginUser);

			if (null != strResponse && strResponse.equals(ApplicationConstants.SUCCESS)) {
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Special Offer Created Succesfully");
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);

				session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);

				specialofferPages = hubCitiService.displayspeciaOfferPage(loginUser, searchKey, lowerLimit);

				if (null != specialofferPages) {
					if (specialofferPages.getPageDetails().isEmpty() && (null == searchKey || "".equals(searchKey))) {
						model.put("screenSettingsForm", specialOfferPageDetails);

						LOG.info(ApplicationConstants.METHODEND + methodName);
						return new ModelAndView("buildspecialofferpage");
					} else if (specialofferPages.getPageDetails().isEmpty()) {
						request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "No Special Offer Pages Found");
						request.setAttribute(ApplicationConstants.RESPONSESTATUS, "INFO");

						session.removeAttribute("specialOfferPageList");
						session.removeAttribute(ApplicationConstants.PAGINATION);
					} else {
						session.setAttribute("specialOfferPageList", specialofferPages.getPageDetails());
						if (null != specialofferPages.getTotalSize()) {
							objPage = Utility.getPagination(specialofferPages.getTotalSize(), currentPage,
									"displayspecialofferpages.htm", recordCount);
							session.setAttribute(ApplicationConstants.PAGINATION, objPage);
						}
					}
				}

				request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
				session.setAttribute(ApplicationConstants.SEARCHKEY, searchKey);
				model.put("screenSettingsForm", new ScreenSettings());
				return new ModelAndView("displayspecialofferpage");
			} else {
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, strResponse);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(viewName);
	}

	/**
	 * This method will display user created Special offer Pages if exists
	 * otherwise will take you to build Special offer page screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */

	public final String displaySpecialOfferPages(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws HubCitiServiceException {
		final String methodName = "displaySpecialOfferPages";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute("ImageIcon");

		final String searchKey = specialOfferPageDetails.getSearchKey();
		final String pageFlag = request.getParameter("pageFlag");
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		SpecialOfferPages specialofferPages = null;

		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

		if (null != pageFlag && "true".equals(pageFlag)) {
			pageNumber = request.getParameter("pageNumber");
			final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
			if (Integer.valueOf(pageNumber) != 0) {
				currentPage = Integer.valueOf(pageNumber);
				final int number = Integer.valueOf(currentPage) - 1;
				final int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
		}

		specialofferPages = hubCitiService.displaySpecialOfferPage(loginUser, searchKey, lowerLimit);

		if (null != specialofferPages) {
			if (specialofferPages.getPageDetails().isEmpty() && (null == searchKey || "".equals(searchKey))) {
				model.put("screenSettingsForm", specialOfferPageDetails);

				LOG.info(ApplicationConstants.METHODEND + methodName);
				return "buildspecialofferpage";
			} else if (specialofferPages.getPageDetails().isEmpty()) {
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "No Special Offer Pages Found");
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, "INFO");

				session.removeAttribute("specialOfferPageList");
				session.removeAttribute(ApplicationConstants.PAGINATION);
			} else {
				session.setAttribute("specialOfferPageList", specialofferPages.getPageDetails());
				if (null != specialofferPages.getTotalSize()) {
					objPage = Utility.getPagination(specialofferPages.getTotalSize(), currentPage,
							"displayspecialofferpages.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				}
			}
		}

		request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
		session.setAttribute(ApplicationConstants.SEARCHKEY, searchKey);
		model.put("screenSettingsForm", new ScreenSettings());

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "displayspecialofferpage";
	}

	/**
	 * This method will return add make your own Special Offer page screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	public final String makeYourOwnSpecialOfferPage(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferDetails, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws HubCitiServiceException {
		final String methodName = "makeYourOwnSpecialOfferPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");
		session.setAttribute("minCropHt", 70);
		session.setAttribute("minCropWd", 70);
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);

		session.setAttribute("makeSpecialOfferImage", "images/upload_image.png");
		request.setAttribute(ApplicationConstants.LOWERLIMIT, specialOfferDetails.getLowerLimit());
		model.put("screenSettingsForm", new ScreenSettings());

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "makeyourownspecailofferpage";
	}

	public final ModelAndView buildSpecialOfferPage(
			@ModelAttribute("screenSettingsForm") ScreenSettings sepcialOfferPageDetails, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws HubCitiServiceException {
		final String methodName = "buildspecialofferpage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);
		request.setAttribute(ApplicationConstants.LOWERLIMIT, sepcialOfferPageDetails.getLowerLimit());
		model.put("screenSettingsForm", sepcialOfferPageDetails);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("buildspecialofferpage");
	}

	/**
	 * This method saves make yours own Special Offer page details.
	 * 
	 * @param specialOfferDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */

	public final ModelAndView saveorupdateMakeYourOwnSpecialOfferPage(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveorupdateMakeYourOwnSpecialOfferPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		String viewName = "makeyourownspecailofferpage";
		
		if( null != specialOfferPageDetails.getPageId()){
			viewName = "makeyourownspecailofferpage";
		}else{
			viewName = "editmakeyourownspecailofferpage";
		}
		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");
		request.setAttribute(ApplicationConstants.LOWERLIMIT, specialOfferPageDetails.getLowerLimit());

		String strResponse = null;
		final String searchKey = null;
		final Integer lowerLimit = 0;
		final int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		SpecialOfferPages specialofferPages = null;

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);

		specialOfferPageDetails.setTempretids(specialOfferPageDetails.getRetailerLocIds());
		specialofferValidation.validateMakeYourSpecialOfferPage(specialOfferPageDetails, result);

		result = validateDates(specialOfferPageDetails, result);

		if (result.hasErrors()) {
			if( null != specialOfferPageDetails.getPageId()){
				return new ModelAndView("editmakeyourownspecailofferpage");
			}else{
				return new ModelAndView("makeyourownspecailofferpage");
			}
			
		} else {
			strResponse = hubCitiService.saveSpecialOfferScreen(specialOfferPageDetails, loginUser);

			if (null != strResponse && strResponse.equals(ApplicationConstants.SUCCESS)) {
				if( null != specialOfferPageDetails.getPageId()){
					request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Special Offer Page updated Succesfully");
				}else{
					request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Special Offer Page Created Succesfully");
				}
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);

				specialofferPages = hubCitiService.displaySpecialOfferPage(loginUser, searchKey, lowerLimit);

				if (null != specialofferPages) {
					if (specialofferPages.getPageDetails().isEmpty() && (null == searchKey || "".equals(searchKey))) {
						model.put("screenSettingsForm", specialOfferPageDetails);

						LOG.info(ApplicationConstants.METHODEND + methodName);
						return new ModelAndView("buildspecialofferpage");
					} else if (specialofferPages.getPageDetails().isEmpty()) {
						request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "No Special Offer Pages Found");
						request.setAttribute(ApplicationConstants.RESPONSESTATUS, "INFO");

						session.removeAttribute("specialOfferPageList");
						session.removeAttribute(ApplicationConstants.PAGINATION);
					} else {
						session.setAttribute("specialOfferPageList", specialofferPages.getPageDetails());
						if (null != specialofferPages.getTotalSize()) {
							objPage = Utility.getPagination(specialofferPages.getTotalSize(), currentPage,
									"displayspecialofferpages.htm", recordCount);
							session.setAttribute(ApplicationConstants.PAGINATION, objPage);
						}
					}
				}

				request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
				session.setAttribute(ApplicationConstants.SEARCHKEY, searchKey);
				model.put("screenSettingsForm", new ScreenSettings());
				return new ModelAndView("displayspecialofferpage");
			} else {
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, strResponse);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(viewName);
	}

	/**
	 * This Method is to validate start and end date.
	 * 
	 * @param specialOfferDetails
	 * @param result
	 * @return
	 * @throws HubCitiServiceException
	 */
	public final BindingResult validateDates(ScreenSettings specialOfferDetails, BindingResult result)
			throws HubCitiServiceException {
		final String methodName = "validateDates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String compDate = null;
		final Date currentDate = new Date();
		Boolean validStartDate = false;
		Boolean validEndDate = false;

		if (!"".equals(Utility.checkNull(specialOfferDetails.getStartDate()))) {
			validStartDate = Utility.isValidDate(specialOfferDetails.getStartDate());

			if (validStartDate
					&& (null == specialOfferDetails.getHiddenBtnLinkId() || "".equals(specialOfferDetails
							.getHiddenBtnLinkId()))) {
				compDate = Utility.compareCurrentDate(specialOfferDetails.getStartDate(), currentDate);
				if (null != compDate) {
					specialofferValidation.validateDates(specialOfferDetails, result,
							ApplicationConstants.DATESTARTCURRENT);
				}
			} else if (!validStartDate) {
				specialofferValidation.validateDates(specialOfferDetails, result, ApplicationConstants.VALIDSTARTDATE);
			}
		}
		if (!"".equals(Utility.checkNull(specialOfferDetails.getEndDate()))) {
			validEndDate = Utility.isValidDate(specialOfferDetails.getEndDate());
			validStartDate = Utility.isValidDate(specialOfferDetails.getStartDate());

			if (validEndDate) {
				if (validStartDate) {
					compDate = Utility
							.compareDate(specialOfferDetails.getStartDate(), specialOfferDetails.getEndDate());
					if (null != compDate) {
						specialofferValidation.validateDates(specialOfferDetails, result,
								ApplicationConstants.DATEAFTER);
					}
				} else {
					compDate = Utility.compareCurrentDate(specialOfferDetails.getEndDate(), currentDate);
					if (null != compDate) {
						specialofferValidation.validateDates(specialOfferDetails, result,
								ApplicationConstants.DATEENDCURRENT);
					}
				}
			} else {
				specialofferValidation.validateDates(specialOfferDetails, result, ApplicationConstants.VALIDENDDATE);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return result;
	}

	/**
	 * This method deletes Special Offer page.
	 * 
	 * @param specialOfferDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	public final String deleteSpecialOfferPage(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "deleteSpecialOfferPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");
		final String searchKey = null;
		Integer lowerLimit = 0;
		int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);
		String strResponse = null;
		SpecialOfferPages specialofferPages = null;

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);
		session.removeAttribute("specialofferPageList");
		strResponse = hubCitiService.deleteSpecialOfferPage(specialOfferPageDetails.getPageId(), loginUser);

		if (null != strResponse && strResponse.equals(ApplicationConstants.SUCCESS)) {
			request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Special Offer Page Deleted Succesfully");
			request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);

			if (null != specialOfferPageDetails.getLowerLimit()) {
				lowerLimit = specialOfferPageDetails.getLowerLimit();
				currentPage = (lowerLimit + recordCount) / recordCount;
			}

			specialofferPages = hubCitiService.displaySpecialOfferPage(loginUser, searchKey, lowerLimit);

			if (null != specialofferPages) {
				if (specialofferPages.getPageDetails().isEmpty() && (null == searchKey || "".equals(searchKey))) {
					model.put("screenSettingsForm", specialOfferPageDetails);

					LOG.info(ApplicationConstants.METHODEND + methodName);
					return "buildspecialofferpage";
				} else if (specialofferPages.getPageDetails().isEmpty()) {
					request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "No Special Offer Pages Found");
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, "INFO");

					session.removeAttribute("specialOfferPageList");
					session.removeAttribute(ApplicationConstants.PAGINATION);
				} else {
					session.setAttribute("specialOfferPageList", specialofferPages.getPageDetails());
					if (null != specialofferPages.getTotalSize()) {
						objPage = Utility.getPagination(specialofferPages.getTotalSize(), currentPage,
								"displayspecialofferpages.htm", recordCount);
						session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					}
				}
			}

			request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
			session.setAttribute(ApplicationConstants.SEARCHKEY, searchKey);
			model.put("screenSettingsForm", new ScreenSettings());

		} else {
			request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);
			request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Failed to Delete Special Offer Page");
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "displayspecialofferpage";
	}

	/**
	 * This method will return edit link to existing Special offer page screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	public String editSpecialOfferScreen(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails,
			BindingResult result, HttpServletRequest request, HttpSession session, ModelMap model)
			throws HubCitiServiceException {
		final String methodName = "editSpecialOfferScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = "editspecialofferpage";
		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");

		session.setAttribute("minCropHt", 50);
		session.setAttribute("minCropWd", 50);
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);

		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

		final ScreenSettings specialOfferPage = hubCitiService.getSpecialOfferPage(specialOfferPageDetails, loginUser);
		final List<HubCitiImages> arHcImageList = hubCitiService.getHubCitiImageIconsDisplay("AttachLink");

		if (null != specialOfferPage.getImageIconID() && 0 != specialOfferPage.getImageIconID()) {
			session.setAttribute("iconSelection", "existing");
			session.setAttribute("ImageIcon", specialOfferPage.getImageIconID());
			specialOfferPage.setIconSelectHid("exstngIcon");
		} else {
			specialOfferPage.setIconSelectHid("icnSlctn");
			session.setAttribute("iconSelection", "Make Your Own");
		}

		if (null != specialOfferPage) {
			specialOfferPage.setTempretids(specialOfferPage.getRetailerLocIds());
			specialOfferPage.setPageId(specialOfferPageDetails.getPageId());
		}
		session.setAttribute("arHcImageList", arHcImageList);
		session.setAttribute("filePath", specialOfferPage.getMediaPath());
		session.setAttribute("fileName", specialOfferPage.getPathName());
		session.setAttribute("oldFileName", specialOfferPage.getPathName());
		if (null != specialOfferPage.getLogoImageName()) {
			session.setAttribute("specialofferScreenImage", specialOfferPage.getImagePath());
		} else {
			session.setAttribute("specialofferScreenImage", ApplicationConstants.DEFAULTIMAGEPATH);
		}

		request.setAttribute(ApplicationConstants.LOWERLIMIT, specialOfferPageDetails.getLowerLimit());
		session.setAttribute("specialofferpage", specialOfferPage);
		session.setAttribute("specialofferpageid", specialOfferPageDetails.getPageId());
		model.put("screenSettingsForm", specialOfferPage);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}

	/**
	 * This method saves link to existing Special Offer page details.
	 * 
	 * @param specialOfferDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	public ModelAndView updateSpecialOfferScreenDetails(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveSpecialOfferScreenDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");
		request.setAttribute(ApplicationConstants.LOWERLIMIT, specialOfferPageDetails.getLowerLimit());

		boolean isValidURL = true;
		String strResponse = null;
		final String searchKey = null;
		final Integer lowerLimit = 0;
		final int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;
		SpecialOfferPages specialofferPages = null;

		specialOfferPageDetails.setTempretids(specialOfferPageDetails.getRetailerLocIds());
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);

		specialofferValidation.validate(specialOfferPageDetails, result);
		isValidURL = Utility.validateURL(specialOfferPageDetails.getPageAttachLink());
		if (!isValidURL) {
			specialofferValidation.validate(specialOfferPageDetails, result, ApplicationConstants.INVALIDURL);
		}

		if (result.hasErrors()) {
			session.setAttribute("ImageIcon", specialOfferPageDetails.getImageIconID());
			return new ModelAndView("editspecialofferpage");
		} else {
			strResponse = hubCitiService.saveSpecialOfferScreen(specialOfferPageDetails, loginUser);

			if (null != strResponse && strResponse.equals(ApplicationConstants.SUCCESS)) {
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Special Offer Updated Succesfully");
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);

				session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);

				specialofferPages = hubCitiService.displayspeciaOfferPage(loginUser, searchKey, lowerLimit);

				if (null != specialofferPages) {
					if (specialofferPages.getPageDetails().isEmpty() && (null == searchKey || "".equals(searchKey))) {
						model.put("screenSettingsForm", specialOfferPageDetails);

						LOG.info(ApplicationConstants.METHODEND + methodName);
						return new ModelAndView("buildspecialofferpage");
					} else if (specialofferPages.getPageDetails().isEmpty()) {
						request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "No Special Offer Pages Found");
						request.setAttribute(ApplicationConstants.RESPONSESTATUS, "INFO");

						session.removeAttribute("specialOfferPageList");
						session.removeAttribute(ApplicationConstants.PAGINATION);
					} else {
						session.setAttribute("specialOfferPageList", specialofferPages.getPageDetails());
						if (null != specialofferPages.getTotalSize()) {
							objPage = Utility.getPagination(specialofferPages.getTotalSize(), currentPage,
									"displayspecialofferpages.htm", recordCount);
							session.setAttribute(ApplicationConstants.PAGINATION, objPage);
						}
					}
				}

				request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
				session.setAttribute(ApplicationConstants.SEARCHKEY, searchKey);
				model.put("screenSettingsForm", new ScreenSettings());
				return new ModelAndView("displayspecialofferpage");
			} else {
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, strResponse);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("editspecialofferpage");
	}

	/**
	 * This method saves make yours own Special Offer page details.
	 * 
	 * @param specialOfferDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */

	public final ModelAndView updateMakeYourOwnSpecialOfferPage(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "updateMakeYourOwnSpecialOfferPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");
		request.setAttribute(ApplicationConstants.LOWERLIMIT, specialOfferPageDetails.getLowerLimit());

		String strResponse = null;
		final String searchKey = null;
		final Integer lowerLimit = 0;
		final int currentPage = 1;
		Pagination objPage = null;
		final int recordCount = 20;

		SpecialOfferPages specialofferPages = null;

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);

		specialOfferPageDetails.setTempretids(specialOfferPageDetails.getRetailerLocIds());
		specialofferValidation.validateMakeYourSpecialOfferPage(specialOfferPageDetails, result);

		result = validateDates(specialOfferPageDetails, result);

		if (result.hasErrors()) {
			return new ModelAndView("makeyourownspecailofferpage");
		} else {
			strResponse = hubCitiService.saveSpecialOfferScreen(specialOfferPageDetails, loginUser);

			if (null != strResponse && strResponse.equals(ApplicationConstants.SUCCESS)) {
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Special Offer Page Created Succesfully");
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);

				specialofferPages = hubCitiService.displaySpecialOfferPage(loginUser, searchKey, lowerLimit);

				if (null != specialofferPages) {
					if (specialofferPages.getPageDetails().isEmpty() && (null == searchKey || "".equals(searchKey))) {
						model.put("screenSettingsForm", specialOfferPageDetails);

						LOG.info(ApplicationConstants.METHODEND + methodName);
						return new ModelAndView("buildspecialofferpage");
					} else if (specialofferPages.getPageDetails().isEmpty()) {
						request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "No Special Offer Pages Found");
						request.setAttribute(ApplicationConstants.RESPONSESTATUS, "INFO");

						session.removeAttribute("specialOfferPageList");
						session.removeAttribute(ApplicationConstants.PAGINATION);
					} else {
						session.setAttribute("specialOfferPageList", specialofferPages.getPageDetails());
						if (null != specialofferPages.getTotalSize()) {
							objPage = Utility.getPagination(specialofferPages.getTotalSize(), currentPage,
									"displayspecialofferpages.htm", recordCount);
							session.setAttribute(ApplicationConstants.PAGINATION, objPage);
						}
					}
				}

				request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
				session.setAttribute(ApplicationConstants.SEARCHKEY, searchKey);
				model.put("screenSettingsForm", new ScreenSettings());
				return new ModelAndView("displayspecialofferpage");
			} else {
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, strResponse);
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, strResponse);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView("editmakeyourownspecailofferpage");
	}

	/**
	 * This method will return edit link to existing Special offer page screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	public String editMakeYourOwnScreen(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails,
			BindingResult result, HttpServletRequest request, HttpSession session, ModelMap model)
			throws HubCitiServiceException {
		final String methodName = "editSpecialOfferScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final String viewName = "editmakeyourownspecailofferpage";
		session.removeAttribute("uploadedFile");
		session.removeAttribute("ImageIcon");

		session.setAttribute("minCropHt", 50);
		session.setAttribute("minCropWd", 50);
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPECIALOFFERPAGE);

		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

		final ScreenSettings specialOfferPage = hubCitiService.getSpecialOfferPage(specialOfferPageDetails, loginUser);

		if (null != specialOfferPage) {
			specialOfferPage.setTempretids(specialOfferPage.getRetailerLocIds());
			specialOfferPage.setPageId(specialOfferPageDetails.getPageId());
		}
		session.setAttribute("filePath", specialOfferPage.getMediaPath());
		session.setAttribute("fileName", specialOfferPage.getPathName());
		session.setAttribute("oldFileName", specialOfferPage.getPathName());
		if (null != specialOfferPage.getLogoImageName()) {
			session.setAttribute("makeSpecialOfferImage", specialOfferPage.getImagePath());
		} else {
			session.setAttribute("makeSpecialOfferImage", "images/upload_image.png");
		}

		request.setAttribute(ApplicationConstants.LOWERLIMIT, specialOfferPageDetails.getLowerLimit());
		session.setAttribute("specialofferpage", specialOfferPage);
		session.setAttribute("specialofferpageid", specialOfferPageDetails.getPageId());
		model.put("screenSettingsForm", specialOfferPage);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}
	
	/**
	 * 
	 * Make special offer as featured.
	 * 
	 */
	public String savefeatured(Integer isfeatured, Integer pageid, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws HubCitiServiceException {

		String txtResponse = null;

		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);

		int status = hubCitiService.saveAsFeautured(loginUser.getHubCitiID(), pageid, isfeatured);

		if (status == 1) {
			txtResponse = "SUCCESS";
		} else if (status == 2) {
			txtResponse = "LIMIT";
		} else {
			txtResponse = "FAILURE";
		}

		return txtResponse;
	}

}

package com.hubciti.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.exception.HubCitiServiceException;
import com.hubciti.common.pojo.Category;
import com.hubciti.common.pojo.Coupon;
import com.hubciti.common.pojo.NewsSettings;
import com.hubciti.common.pojo.User;
import com.hubciti.common.tags.Pagination;
import com.hubciti.common.util.Utility;
import com.hubciti.service.HubCitiService;
import com.hubciti.validator.CouponValidator;

/**
 * This class is used to handle "Setup Coupon" functionalities. It will have
 * coupon create,update and delete functionalities.
 * 
 * @author shyamsundara_hm
 * 
 */
@Controller
public class CouponSetupController {

	private static final Logger LOG = Logger.getLogger(CouponSetupController.class);

	CouponValidator couponValidator = null;

	@Autowired
	public void setCouponValidator(CouponValidator couponValidator) {
		this.couponValidator = couponValidator;
	}

	/**
	 * Below method is used to display coupons.
	 * 
	 * @param coupon
	 * @param result
	 * @param map
	 * @param request
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */

	@RequestMapping(value = "managecoupons.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView manageCoupons(@ModelAttribute("ManageCouponForm") Coupon coupon, BindingResult result, ModelMap map,
			HttpServletRequest request, HttpSession session) throws HubCitiServiceException

	{
		final String strMethodName = "manageCoupons";
		String strViewName = "managecoupons";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		NewsSettings objCoupon = null;
		// for pagination
		Integer iRecordCount = 20;
		Pagination objPagination;
		Integer lowerLimit = null;
		String pageNumber = "0";
		Integer iCurrentPage = 1;
		String pageFlag = null;
		String eDate = null;
		String sDate = null;

		try {

			pageFlag = request.getParameter("pageFlag");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			User user = (User) session.getAttribute("loginUser");

			String coupmsg = request.getParameter("cpmsg");

			if (null != coupmsg && !"".equals(coupmsg)) {

				if (coupmsg.equals("save")) {
					request.setAttribute("coupSuccessMsg", ApplicationConstants.CREATECOOUPONSUCCESSTEXT);
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, "Save");
				} else {
					request.setAttribute("coupSuccessMsg", ApplicationConstants.UPDATECOOUPONSUCCESSTEXT);
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, "Save");
				}

			}
			session.removeAttribute("couponslst");

			if (null == coupon.getLowerLimit() || coupon.getLowerLimit().equals("")) {
				coupon.setLowerLimit(0);
			}

			if ("".equals(coupon.getCoupSearchKey())) {
				coupon.setCoupSearchKey(null);
			}

			lowerLimit = coupon.getLowerLimit();

			if (null != pageFlag && "true".equals(pageFlag)) {
				pageNumber = (String) request.getParameter("pageNumber");
				session.setAttribute("pageNum", pageNumber);
				final Pagination pagination = (Pagination) session.getAttribute("pagination");

				if (Integer.valueOf(pageNumber) != 0) {
					iCurrentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(iCurrentPage) - 1;
					final int pageSize = pagination.getPageRange();
					lowerLimit = pageSize * number;
				}
			}

			else {

				iCurrentPage = (lowerLimit + iRecordCount) / iRecordCount;
			}

			coupon.setLowerLimit(lowerLimit);

			objCoupon = hubCitiService.displayCoupons(coupon, user);

			if (null != objCoupon) {
				if (null != objCoupon.getTotalSize() && objCoupon.getTotalSize() > 0) {

					objPagination = Utility.getPagination(objCoupon.getTotalSize(), iCurrentPage, "managecoupons.htm", iRecordCount);
					session.setAttribute("pagination", objPagination);

					if (null != objCoupon.getCouponlst() && !objCoupon.getCouponlst().isEmpty()) {

						for (int i = 0; i < objCoupon.getCouponlst().size(); i++) {

							sDate = objCoupon.getCouponlst().get(i).getCoupStartDate();
							sDate = Utility.getUsDateFormat(sDate);
							objCoupon.getCouponlst().get(i).setCoupStartDate(sDate);

							eDate = objCoupon.getCouponlst().get(i).getCoupEndDate();
							if(null != eDate && !eDate.isEmpty()){
							eDate = Utility.getUsDateFormat(eDate);
							objCoupon.getCouponlst().get(i).setCoupEndDate(eDate);
							}
						}

						session.setAttribute("couponslst", objCoupon);

					} else {
						session.removeAttribute("pagination");
					}
				}
			} else {

				if (null == coupon.getCoupSearchKey() || coupon.getCoupSearchKey().equals("")) {
					return new ModelAndView(new RedirectView("createcoupon.htm"));
				} else {

					request.setAttribute("Nocoupfound", ApplicationConstants.NOCOUPONFOUNDTEXT);

				}
			}

		} catch (HubCitiServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		} catch (ParseException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return new ModelAndView(strViewName);
	}

	/**
	 * below method is used to create coupon.
	 * 
	 * @param coupon
	 * @param result
	 * @param map
	 * @param request
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "createcoupon.htm", method = RequestMethod.GET)
	public String createCoupon(@ModelAttribute("CreateCouponForm") Coupon coupon, BindingResult result, ModelMap map, HttpServletRequest request,
			HttpSession session) throws HubCitiServiceException {

		final String strMethodName = "createCoupon";
		String strViewName = "createcoupon";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		List<Category> prodCategories = null;

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			User user = (User) session.getAttribute("loginUser");
		//	prodCategories = hubCitiService.getProductCategoires(user);
		//	session.setAttribute("prodCategories", prodCategories);
			if(null == user)
			{
				strViewName="login";
				
			}else{
			

			session.setAttribute("imageCropPage", "hubciticoupon");

			if (null == coupon.getCoupImg() || "".equals(coupon.getCoupImg())) {
				session.setAttribute("coupImagePreview", ApplicationConstants.DEFAULTIMAGESQR);
			}

			if (null == coupon.getDealLstImgName() || "".equals(coupon.getDealLstImgName())) {
				session.setAttribute("dealImgPreview", ApplicationConstants.DEFAULTIMAGESQR);
			}
			
			strViewName="createcoupon";
				
			}

			LOG.info(ApplicationConstants.METHODEND + strMethodName);

		} catch (Exception exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		}

		return strViewName;

	}

	@RequestMapping(value = "savecoupon.htm", method = RequestMethod.POST)
	public ModelAndView saveCoupon(@ModelAttribute("CreateCouponForm") Coupon coupon, BindingResult result, ModelMap map, HttpServletRequest request,
			HttpSession session) throws HubCitiServiceException {

		final String strMethodName = "saveCoupon";
		String strViewName = "createcoupon";
		Boolean isStDateVal = false;
		Boolean isEnDateVal = false;
		Boolean isExDateVal = false;
		String compDate = null;
		Date currentDate = new Date();
		String response = null;
		FieldError fieldStartDate = null;
		FieldError fieldEndDate = null;
		FieldError fieldExpDate = null;
		try {

			LOG.info(ApplicationConstants.METHODSTART + strMethodName);
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

			session.setAttribute("imageCropPage", "NewsSettings");
			request.setAttribute("prodcategory", coupon.getProdCategory());
			request.setAttribute("locationId", coupon.getLocationId());

			if (null == coupon.getCoupImg() || "".equals(coupon.getCoupImg())) {
				session.setAttribute("coupImagePreview", ApplicationConstants.DEFAULTIMAGESQR);
			}

			if (null == coupon.getDealLstImgName() || "".equals(coupon.getDealLstImgName())) {
				session.setAttribute("dealImgPreview", ApplicationConstants.DEFAULTIMAGESQR);
			}

			if (null != coupon.getLocationId() && !"".equals(coupon.getLocationId())) {
				coupon.setTempretids(coupon.getLocationId());
			}
			if (null == coupon.getRetailerId() || "".equals(coupon.getRetailerId())) {
				coupon.setRetailerId(null);
			}

			session.setAttribute("imageCropPage", "hubciticoupon");
			session.setAttribute("minCropWd", 70);
			session.setAttribute("minCropHt", 70);

			User user = (User) session.getAttribute("loginUser");
			couponValidator.validate(coupon, result);

			fieldStartDate = result.getFieldError("coupStartDate");
			if (fieldStartDate == null) {
				isStDateVal = Utility.isValidDate(coupon.getCoupStartDate());
				if (!isStDateVal) {
					couponValidator.validateDates(coupon, result, ApplicationConstants.VALIDSTARTDATE);

				}

			}

			fieldEndDate = result.getFieldError("coupEndDate");

			if (fieldEndDate == null) {

				if (null != coupon.getCoupEndDate() && !"".equals(coupon.getCoupEndDate())) {
					isEnDateVal = Utility.isValidDate(coupon.getCoupEndDate());
					if (!isEnDateVal) {
						couponValidator.validateDates(coupon, result, ApplicationConstants.VALIDENDDATE);

					}
				}

			}
			fieldStartDate = result.getFieldError("coupStartDate");

			if (fieldStartDate == null) {
				compDate = Utility.compareCurrentDate(coupon.getCoupStartDate(), currentDate);
				if (null != compDate) {
					couponValidator.validateDates(coupon, result, ApplicationConstants.DATESTARTCURRENT);

				}

			}

			fieldEndDate = result.getFieldError("coupEndDate");
			if (fieldEndDate == null) {
				if (null != coupon.getCoupEndDate() && !"".equals(coupon.getCoupEndDate())) {
					compDate = Utility.compareCurrentDate(coupon.getCoupEndDate(), currentDate);
					if (null != compDate) {
						couponValidator.validateDates(coupon, result, ApplicationConstants.DATEENDCURRENT);

					}
				}
			}
			fieldStartDate = result.getFieldError("coupStartDate");
			fieldEndDate = result.getFieldError("coupEndDate");

			if (fieldStartDate == null && fieldEndDate == null) {
				if (null != coupon.getCoupEndDate() && !"".equals(coupon.getCoupEndDate())) {
					compDate = Utility.compareDate(coupon.getCoupStartDate(), coupon.getCoupEndDate());
					if (null != compDate) {
						couponValidator.validateDates(coupon, result, ApplicationConstants.DATEENDAFTER);

					}
				}
			}

			if (null != coupon.getCoupExpiryDate() && !"".equals(coupon.getCoupExpiryDate())) {
				isExDateVal = Utility.isValidDate(coupon.getCoupExpiryDate());
				if (!isExDateVal) {
					couponValidator.validateDates(coupon, result, ApplicationConstants.VALIDEXPDATE);

				}

			}

			fieldExpDate = result.getFieldError("coupExpiryDate");

			if (fieldExpDate == null) {
				if (null != coupon.getCoupExpiryDate() && !"".equals(coupon.getCoupExpiryDate())) {
					compDate = Utility.compareCurrentDate(coupon.getCoupExpiryDate(), currentDate);
					if (null != compDate) {
						couponValidator.validateDates(coupon, result, ApplicationConstants.DATEEXPCURRENT);

					}
				}
			}

			fieldExpDate = result.getFieldError("coupExpiryDate");
			fieldEndDate = result.getFieldError("coupEndDate");

			if (fieldEndDate == null && fieldExpDate == null) {
				if (null != coupon.getCoupExpiryDate() && !"".equals(coupon.getCoupExpiryDate()) && null != coupon.getCoupEndDate()
						&& !"".equals(coupon.getCoupEndDate())) {

					compDate = Utility.compareDate(coupon.getCoupEndDate(), coupon.getCoupExpiryDate());
					if (null != compDate) {
						couponValidator.validateDates(coupon, result, ApplicationConstants.DATEAFTER);

					}
				}

			}

			if (null != coupon.getCoupExpTimeHrs() && !"00".equals(coupon.getCoupExpTimeHrs())
					|| (null != coupon.getCoupExpTimeMins() && !"00".equals(coupon.getCoupExpTimeMins()))) {

				if (null == coupon.getCoupExpiryDate() || "".equals(coupon.getCoupExpiryDate())) {

					couponValidator.validateDates(coupon, result, ApplicationConstants.DATEEXPENTER);
				}

			}

			if (null != coupon.getCoupETimeHrs() && !"00".equals(coupon.getCoupETimeHrs())
					|| (null != coupon.getCoupETimeMins() && !"00".equals(coupon.getCoupETimeMins()))) {

				if (null == coupon.getCoupEndDate() || "".equals(coupon.getCoupEndDate())) {

					couponValidator.validateDates(coupon, result, ApplicationConstants.DATEENDENTER);
				}

			}

			if (result.hasErrors()) {

				strViewName = "createcoupon";

			} else {

				if (null != coupon.getCoupExpiryDate() && "".equals(coupon.getCoupExpiryDate())) {
					coupon.setCoupExpiryDate(null);
				}

				response = hubCitiService.saveCoupon(coupon, user);

				if (null != response && response.equals(ApplicationConstants.SUCCESS)) {

					if (null != coupon.getCouponId() && !"".equals(coupon.getCouponId()))

					{
						return new ModelAndView(new RedirectView("managecoupons.htm?cpmsg=update"));
					} else {
						return new ModelAndView(new RedirectView("managecoupons.htm?cpmsg=save"));
					}

				} else {

					strViewName = "createcoupon";
				}

			}

		} catch (HubCitiServiceException exception) {

			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}

		map.put("CreateCouponForm", coupon);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return new ModelAndView(strViewName);

	}

	@RequestMapping(value = "editcoupon.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public String editCoupon(@ModelAttribute("ManageCouponForm") Coupon coupon, BindingResult result, ModelMap map, HttpServletRequest request,
			HttpSession session) throws HubCitiServiceException {

		final String strMethodName = "editCoupon";
		String strViewName = "createcoupon";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		List<Category> prodCategories = null;
		Coupon objCoupon = null;
		String sDate = null;
		String couponTime = null;
		String tempStartTimeHrsMin[];
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			User user = (User) session.getAttribute("loginUser");

			prodCategories = hubCitiService.getProductCategoires(user);

			session.setAttribute("prodCategories", prodCategories);
			objCoupon = hubCitiService.editCoupon(coupon);

			request.setAttribute("prodcategory", objCoupon.getProdCategory());
			request.setAttribute("locationId", objCoupon.getLocationId());

			if (null == objCoupon.getCoupImg() || "".equals(objCoupon.getCoupImg())) {
				session.setAttribute("coupImagePreview", ApplicationConstants.DEFAULTIMAGESQR);
			} else {

				session.setAttribute("coupImagePreview", objCoupon.getCoupImgName());
			}

			if (null == objCoupon.getDealLstImgName() || "".equals(objCoupon.getDealLstImgName())) {
				session.setAttribute("dealImgPreview", ApplicationConstants.DEFAULTIMAGESQR);
			} else {

				session.setAttribute("dealImgPreview", objCoupon.getDealLstImgPath());
			}

			if (null != objCoupon.getLocationId() && !"".equals(objCoupon.getLocationId())) {
				objCoupon.setTempretids(objCoupon.getLocationId());
			}

			// formating date.
			if (objCoupon != null) {
				sDate = objCoupon.getCoupStartDate();
				if (sDate != null) {
					sDate = Utility.formattedDate(sDate);
					objCoupon.setCoupStartDate(sDate);
				}
			}

			if (objCoupon != null) {
				sDate = objCoupon.getCoupEndDate();
				if (sDate != null) {
					sDate = Utility.formattedDate(sDate);
					objCoupon.setCoupEndDate(sDate);
				}
			}

			if (objCoupon != null) {
				sDate = objCoupon.getCoupExpiryDate();
				if (sDate != null && !"".equals(objCoupon.getCoupExpiryDate())) {
					sDate = Utility.formattedDate(sDate);
					objCoupon.setCoupExpiryDate(sDate);
				}
			}

			// formating time.

			couponTime = objCoupon.getCoupStaTime();

			if (!Utility.isEmptyOrNullString(couponTime)) {
				tempStartTimeHrsMin = couponTime.split(":");
				objCoupon.setCoupSTimeHrs(tempStartTimeHrsMin[0]);
				objCoupon.setCoupSTimeMins(tempStartTimeHrsMin[1]);
			}

			couponTime = objCoupon.getCoupEndTime();

			if (!Utility.isEmptyOrNullString(couponTime)) {
				tempStartTimeHrsMin = couponTime.split(":");
				objCoupon.setCoupETimeHrs(tempStartTimeHrsMin[0]);
				objCoupon.setCoupETimeMins(tempStartTimeHrsMin[1]);
			}

			couponTime = objCoupon.getCoupExpTime();

			if (!Utility.isEmptyOrNullString(couponTime)) {
				tempStartTimeHrsMin = couponTime.split(":");
				objCoupon.setCoupExpTimeHrs(tempStartTimeHrsMin[0]);
				objCoupon.setCoupExpTimeMins(tempStartTimeHrsMin[1]);
			}

			session.setAttribute("imageCropPage", "hubciticoupon");
			session.setAttribute("minCropWd", 300);
			session.setAttribute("minCropHt", 150);

			LOG.info(ApplicationConstants.METHODEND + strMethodName);
			map.put("CreateCouponForm", objCoupon);

		} catch (HubCitiServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		} catch (ParseException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}

		return strViewName;

	}

	@RequestMapping(value = "deletecoupon.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView deleteCoupon(@ModelAttribute("ManageCouponForm") Coupon coupon, BindingResult result, ModelMap map,
			HttpServletRequest request, HttpSession session) throws HubCitiServiceException {

		final String strMethodName = "deleteCoupon";
		String strViewName = "managecoupons";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String response = null;
		NewsSettings objCoupon;
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			User user = (User) session.getAttribute("loginUser");

			if (null != coupon) {

				coupon.setCoupSearchKey(null);
				coupon.setLowerLimit(0);
			}

			response = hubCitiService.deleteCoupon(coupon);

			objCoupon = hubCitiService.displayCoupons(coupon, user);
			if (null != objCoupon) {

				if (null != objCoupon.getCouponlst() && !objCoupon.getCouponlst().isEmpty() && response.equals(ApplicationConstants.SUCCESS)) {
					request.setAttribute("coupSuccessMsg", ApplicationConstants.DELETECOOUPONSUCCESSTEXT);
					session.setAttribute("couponslst", objCoupon);
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, "Delete");
				} else if (null != objCoupon.getCouponlst() && !objCoupon.getCouponlst().isEmpty()
						&& response.equals(ApplicationConstants.FAILURETEXT)) {

					request.setAttribute("failureMsg", ApplicationConstants.DELETECOOUPONFAILURETEXT);
					session.setAttribute("couponslst", objCoupon);

				}

			} else {

				return new ModelAndView(new RedirectView("createcoupon.htm"));

			}

			LOG.info(ApplicationConstants.METHODEND + strMethodName);

		} catch (HubCitiServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		}

		return new ModelAndView(strViewName);

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/savefeaturedCoup.htm", method = RequestMethod.POST)
	@ResponseBody
	public String savefeatured(@RequestParam(value = "isfeatured", required = true) Integer isfeatured,
			@RequestParam(value = "couponId", required = true) Integer couponId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws HubCitiServiceException {
		final String strMethodName = "deleteCoupon";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strResponse = null;

		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
		Coupon objCoupon = new Coupon();
		objCoupon.setCouponId(couponId);
		objCoupon.setCoupfeature(isfeatured);
		try {

			strResponse = hubCitiService.saveFeaturedCoupon(objCoupon, loginUser.getHubCitiID());

		} catch (HubCitiServiceException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		}
		return strResponse;

	}

	/**
	 * This ModelAttribute sort Deal start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("StartMinutes")
	public Map<String, String> populatemapDealStartMins() throws HubCitiServiceException {
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort Deal start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("StartHours")
	public Map<String, String> populateDealStartHrs() throws HubCitiServiceException {
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

}

package com.hubciti.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.exception.HubCitiServiceException;
import com.hubciti.common.pojo.AnythingPages;
import com.hubciti.common.pojo.Category;
import com.hubciti.common.pojo.ScreenSettings;
import com.hubciti.common.pojo.User;
import com.hubciti.common.util.Utility;
import com.hubciti.service.HubCitiService;
import com.hubciti.validator.AboutUsScreenSettingsValidation;
import com.hubciti.validator.LoginScreenSettingsValidator;
import com.hubciti.validator.PrivacyPolicyScreenSettingsValidation;
import com.hubciti.validator.RegistrationScreenSettingsValidator;
import com.hubciti.validator.SplashScreenSettingsValidator;

/**
 * This class is a controller class for Screen Settings.
 * 
 * @author dileep_cc
 */
@Controller
public class ScreenSettingsController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScreenSettingsController.class);

	private static final Object StringBuffer = null;

	/**
	 * variable of type LoginScreenSettingsValidator.
	 */
	LoginScreenSettingsValidator loginScreenDetailsValidator;
	/**
	 * variable of type RegistrationScreenSettingsValidator.
	 */
	RegistrationScreenSettingsValidator registrationScreenSettingsValidator;
	/**
	 * variable of type AboutUsScreenSettingsValidation.
	 */
	AboutUsScreenSettingsValidation aboutUsScreenSettingsValidation;
	/**
	 * variable of type PrivacyPolicyScreenSettingsValidation.
	 */
	PrivacyPolicyScreenSettingsValidation policyScreenSettingsValidation;
	/**
	 * variable of type SplashScreenSettingsValidator.
	 */
	SplashScreenSettingsValidator splashScreenSettingsValidator;
	

	/**
	 * @param splashScreenSettingsValidator
	 */
	@Autowired
	public void setSplashScreenSettingsValidator(SplashScreenSettingsValidator splashScreenSettingsValidator) {
		this.splashScreenSettingsValidator = splashScreenSettingsValidator;
	}

	/**
	 * @param loginScreenDetailsValidator
	 */
	@Autowired
	public void setLoginScreenDetailsValidator(LoginScreenSettingsValidator loginScreenDetailsValidator) {
		this.loginScreenDetailsValidator = loginScreenDetailsValidator;
	}

	/**
	 * @param registrationScreenSettingsValidator
	 */
	@Autowired
	public void setRegistrationScreenSettingsValidator(RegistrationScreenSettingsValidator registrationScreenSettingsValidator) {
		this.registrationScreenSettingsValidator = registrationScreenSettingsValidator;
	}

	/**
	 * @param aboutUsScreenSettingsValidation
	 */
	@Autowired
	public void setAboutUsScreenSettingsValidation(AboutUsScreenSettingsValidation aboutUsScreenSettingsValidation) {
		this.aboutUsScreenSettingsValidation = aboutUsScreenSettingsValidation;
	}

	/**
	 * @param policyScreenSettingsValidation
	 */
	@Autowired
	public void setPolicyScreenSettingsValidation(PrivacyPolicyScreenSettingsValidation policyScreenSettingsValidation) {
		this.policyScreenSettingsValidation = policyScreenSettingsValidation;
	}

	/**
	 * This method will return login setup screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setuploginscreen.htm", method = RequestMethod.GET)
	public String setupLoginScreen(HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "setupLoginScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		// Minimum crop height and width for login screen
		session.setAttribute("minCropHt", 140);
		session.setAttribute("minCropWd", 280);
		// Default images path
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.LOGINSCREEN);
		
		
		

		final User loginUser = (User) session.getAttribute("loginUser");
		ScreenSettings loginScreenDetails = null;
		loginUser.setPageType("Login Page");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		loginScreenDetails = hubCitiService.fetchScreenSettings(loginUser);
		
		AnythingPages anythingPageList = null;
		
		anythingPageList = hubCitiService.displayAnythingPage(loginUser, null, null);

		if (null != anythingPageList) {
			session.setAttribute("anythingPageList", anythingPageList.getPageDetails());
		} else {
			session.setAttribute("anythingPageList", null);
		}
		if (null == loginScreenDetails) {
			model.put("screenSettingsForm", new ScreenSettings());
			session.setAttribute("btnname", "Save Settings");
			session.setAttribute("loginScreenLogo", "images/upload_image.png");
			session.setAttribute("loginScreenLogoPreview", "images/dummy-logo.png");
			session.setAttribute("titleBarLogoPreview", "images/small-logo.png");
		} else {

			loginScreenDetails.setOldImageName(loginScreenDetails.getLogoImageName());
			model.put("screenSettingsForm", loginScreenDetails);
			session.setAttribute("btnname", "Update Settings");
			session.setAttribute("loginScreenLogo", loginScreenDetails.getLogoPath());
			session.setAttribute("loginScreenLogoPreview", loginScreenDetails.getLogoPath());
			session.setAttribute("titleBarLogoPreview", loginScreenDetails.getSmallLogoPath());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setuploginpage";
	}

	/**
	 * This method saves the login setup details.
	 * 
	 * @param loginScreenDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setuploginscreen.htm", method = RequestMethod.POST)
	public String saveLoginSetupScreenDetails(@ModelAttribute("screenSettingsForm") ScreenSettings loginScreenDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveLoginSetupScreenDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		loginScreenDetailsValidator.validate(loginScreenDetails, result);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");
		String status = null;
		String reponse = null;
		if (result.hasErrors()) {
			return "setuploginpage";
		} else {
			status = hubCitiService.saveLoginScreenSettings(loginScreenDetails, loginUser);

			if (null != status && status.equals(ApplicationConstants.SUCCESS)) {

				reponse = "Settings Saved Succesfully";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
				session.setAttribute("btnname", "Update Settings");
			} else {
				session.setAttribute("btnname", "Save Settings");
				reponse = "Error Occured While Saving Settings";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.FAILURE);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setuploginpage";
	}

	/**
	 * This method returns registration screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupregscreen.htm", method = RequestMethod.GET)
	public String setupRegistrationScreen(HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "setupRegistrationScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		// Default images path
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.REGISTRATION);

		final User loginUser = (User) session.getAttribute("loginUser");
		ScreenSettings registrationScreenDetails = null;
		loginUser.setPageType(ApplicationConstants.REGISTRATIONPAGE);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		registrationScreenDetails = hubCitiService.fetchScreenSettings(loginUser);
		if (null == registrationScreenDetails) {

			registrationScreenDetails = new ScreenSettings();
			registrationScreenDetails.setPageTitle(ApplicationConstants.REGISTRATIONSUCCESFULLTITLE);
			registrationScreenDetails.setPageContent(ApplicationConstants.REGISTRATIONSUCCESFULLTEXT);
			model.put("screenSettingsForm", registrationScreenDetails);
			session.setAttribute("btnname", "Save Settings");

		} else {

			registrationScreenDetails.setOldImageName(registrationScreenDetails.getLogoImageName());
			model.put("screenSettingsForm", registrationScreenDetails);
			session.setAttribute("btnname", "Update Settings");

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupregscreen";
	}

	/**
	 * This method saves the registration details.
	 * 
	 * @param regScreenSettings
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupregscreen.htm", method = RequestMethod.POST)
	public String saveRegScreenSettings(@ModelAttribute("screenSettingsForm") ScreenSettings regScreenSettings, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveRegScreenSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		registrationScreenSettingsValidator.validate(regScreenSettings, result);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");
		String status = null;
		String reponse = null;
		if (result.hasErrors()) {
			return "setupregscreen";
		} else {
			status = hubCitiService.saveRegScreenSettings(regScreenSettings, loginUser);

			if (null != status && status.equals(ApplicationConstants.SUCCESS)) {

				reponse = "Settings Saved Succesfully";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
				session.setAttribute("btnname", "Update Settings");
			} else {
				session.setAttribute("btnname", "Save Settings");
				reponse = "Error occured while saving Settings";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.FAILURE);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupregscreen";
	}

	/**
	 * This method will return about us screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupaboutusscreen.htm", method = RequestMethod.GET)
	public String setupAboutUsScreen(HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "setupAboutUsScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.setAttribute("minCropWd", "220");
		session.setAttribute("minCropHt", "50");

		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.ABOUTUS);

		final User loginUser = (User) session.getAttribute("loginUser");
		ScreenSettings aboutusScreenSettings = null;
		loginUser.setPageType(ApplicationConstants.ABOUTUSPAGE);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		aboutusScreenSettings = hubCitiService.fetchScreenSettings(loginUser);
		if (null != aboutusScreenSettings && aboutusScreenSettings.isScreenSettingsFlag()) {
			model.put("screenSettingsForm", new ScreenSettings());
			session.setAttribute("hubCitiAppVersion", aboutusScreenSettings.getHubCitiVersion());
			session.setAttribute("btnname", "Save Settings");
			// session.setAttribute("smallLogo", "images/upload_image.png");
			session.setAttribute("aboutusScreenImage", "images/upload_image.png");
			session.setAttribute("aboutusScreenImagePreview", "images/aboutus-image.png");
			session.setAttribute("smallLogoPreview", "images/small-logo.png");
		} else {

			aboutusScreenSettings.setOldImageName(aboutusScreenSettings.getLogoImageName());
			aboutusScreenSettings.setSmallOldImageName(aboutusScreenSettings.getSmallLogoImageName());
			session.setAttribute("hubCitiAppVersion", aboutusScreenSettings.getHubCitiVersion());
			model.put("screenSettingsForm", aboutusScreenSettings);
			session.setAttribute("btnname", "Update Settings");
			// session.setAttribute("smallLogo",
			// aboutusScreenSettings.getSmallLogoPath());
			session.setAttribute("aboutusScreenImage", aboutusScreenSettings.getLogoPath());
			session.setAttribute("aboutusScreenImagePreview", aboutusScreenSettings.getLogoPath());
			session.setAttribute("smallLogoPreview", aboutusScreenSettings.getSmallLogoPath());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupaboutusscreen";
	}

	/**
	 * This method saves the about us screen details.
	 * 
	 * @param screenSettings
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupaboutusscreen.htm", method = RequestMethod.POST)
	public String saveAboutUsScreenSettings(@ModelAttribute("screenSettingsForm") ScreenSettings screenSettings, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveAboutUsScreenSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		aboutUsScreenSettingsValidation.validate(screenSettings, result);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");
		String status = null;
		String reponse = null;
		if (result.hasErrors()) {
			return "setupaboutusscreen";
		} else {
			status = hubCitiService.saveAboutusScreenSettings(screenSettings, loginUser);

			if (null != status && status.equals(ApplicationConstants.SUCCESS)) {

				reponse = "Settings Saved Succesfully";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
				session.setAttribute("btnname", "Update Settings");
			} else {
				session.setAttribute("btnname", "Save Settings");
				reponse = "Error occured while saving Settings";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.FAILURE);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupaboutusscreen";
	}

	/**
	 * This method will return privacy policy setup screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupprivacypolicyscreen.htm", method = RequestMethod.GET)
	public String setupPrivacyPolicyScreen(HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "setupPrivacyPolicyScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		// Default images path
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.PRIVACYPOLICY);

		final User loginUser = (User) session.getAttribute("loginUser");
		ScreenSettings privacyPolicyScreenSettings = null;
		loginUser.setPageType(ApplicationConstants.PRIVACYPOLICYSCREEN);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		privacyPolicyScreenSettings = hubCitiService.fetchScreenSettings(loginUser);
		if (null == privacyPolicyScreenSettings) {
			model.put("screenSettingsForm", new ScreenSettings());
			session.setAttribute("btnname", "Save Settings");
			
		} else {

			model.put("screenSettingsForm", privacyPolicyScreenSettings);
			session.setAttribute("btnname", "Update Settings");
			session.setAttribute("titleBarLogoPreview", privacyPolicyScreenSettings.getSmallLogo());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupprivacypolicyscreen";
	}

	/**
	 * This method saves the privacy policy details.
	 * 
	 * @param screenSettings
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupprivacypolicyscreen.htm", method = RequestMethod.POST)
	public String savePrivacyPolicyScreenSettings(@ModelAttribute("screenSettingsForm") ScreenSettings screenSettings, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveRegScreenSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		policyScreenSettingsValidation.validate(screenSettings, result);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");

		String status = null;
		String reponse = null;
		if (result.hasErrors()) {
			return "setupprivacypolicyscreen";
		} else {
			status = hubCitiService.savePrivacyPolicyScreenSettings(screenSettings, loginUser);

			if (null != status && status.equals(ApplicationConstants.SUCCESS)) {

				reponse = "Settings Saved Succesfully";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
				session.setAttribute("btnname", "Update Settings");
			} else {
				session.setAttribute("btnname", "Save Settings");
				reponse = "Error occured while saving Settings";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.FAILURE);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupprivacypolicyscreen";
	}

	/**
	 * This method will return splash screen setup screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupsplashscreen.htm", method = RequestMethod.GET)
	public String setupSplashScreen(HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "setupSplashScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		// Minimum crop height and width for login screen
		session.setAttribute("minCropHt", 568);
		session.setAttribute("minCropWd", 320);
		// Default images path
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SPLASHSCREEN);

		final User loginUser = (User) session.getAttribute("loginUser");
		ScreenSettings splashScreenDetails = null;

		loginUser.setPageType(ApplicationConstants.SPLASHIMAGE);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		splashScreenDetails = hubCitiService.fetchScreenSettings(loginUser);
		if (null == splashScreenDetails) {
			model.put("screenSettingsForm", new ScreenSettings());
			session.setAttribute("btnname", "Save Settings");
			session.setAttribute("splashImage", "images/upload_image.png");
			session.setAttribute("splashImagePreview", "images/spash-screeen.png");

		} else {

			splashScreenDetails.setOldImageName(splashScreenDetails.getLogoImageName());
			model.put("screenSettingsForm", splashScreenDetails);
			session.setAttribute("btnname", "Update Settings");
			session.setAttribute("splashImage", splashScreenDetails.getLogoPath());
			session.setAttribute("splashImagePreview", splashScreenDetails.getLogoPath());

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupsplashscreen";
	}

	/**
	 * This method will save splash screen details.
	 * 
	 * @param screenSettings
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupsplashscreen.htm", method = RequestMethod.POST)
	public String saveSplashScreenSettings(@ModelAttribute("screenSettingsForm") ScreenSettings screenSettings, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveSplashScreenSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		splashScreenSettingsValidator.validate(screenSettings, result);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");
		String status = null;
		String reponse = null;
		if (result.hasErrors()) {
			return "setupsplashscreen";
		} else {
			status = hubCitiService.saveSplashScreenSettings(screenSettings, loginUser);

			if (null != status && status.equals(ApplicationConstants.SUCCESS)) {

				reponse = "Settings Saved Succesfully";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
				session.setAttribute("btnname", "Update Settings");
			} else {
				session.setAttribute("btnname", "Save Settings");
				reponse = "Error occured while saving Settings";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.FAILURE);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setupsplashscreen";
	}

	/**
	 * This method will return general setup screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/generealsettings.htm", method = RequestMethod.GET)
	public String showGeneralSettingsScreen(@ModelAttribute("screenSettingsForm") ScreenSettings screenSettings, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "showGeneralSettingsScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.GENERALSETTINGS);

		final User loginUser = (User) session.getAttribute("loginUser");
		ScreenSettings generalScreenDetails = null;
		String settingType = "MainMenu";
		String view = "menugeneralsettings";

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

		if (null != screenSettings.getPageType() || "".equalsIgnoreCase(screenSettings.getPageType())) {
			settingType = screenSettings.getPageType();
		}
		
		if ("TabBarLogo".equalsIgnoreCase(settingType)) {
			// Minimum crop height and width for login screen
			session.setAttribute("minCropHt", 34);
			session.setAttribute("minCropWd", 86);
			List<ScreenSettings> buttomBtnTyp = hubCitiService.displayButtomBtnType();
			session.setAttribute("buttomBtnType", buttomBtnTyp);
			List<Category> findCatList = hubCitiService.getCategoryImageDetails(loginUser.gethCAdminUserID(), loginUser.getHubCitiID());
			session.setAttribute("findCategoryImg", findCatList);
			session.setAttribute("catImgPreview", "/HubCiti/images/uploadIconSqr.png");
			if(null == screenSettings.getIsCatImg() || "".equalsIgnoreCase(screenSettings.getIsCatImg()))	{
				screenSettings.setIsCatImg("false");
			}
			view = "generalsettings";
			request.setAttribute("isCatImage", screenSettings.getIsCatImg());
		} else {
			// Minimum crop height and width for login screen
			session.setAttribute("minCropHt", 480);
			session.setAttribute("minCropWd", 320);
			view = "menugeneralsettings";
		}

		generalScreenDetails = hubCitiService.fetchGeneralSettings(loginUser.getHubCitiID(), settingType);

		if (null == generalScreenDetails) {
			ScreenSettings objScreenSettings = new ScreenSettings();
			objScreenSettings.setIsCatImg("false");
			model.put("screenSettingsForm", objScreenSettings);
			session.setAttribute("btnname", "Save Settings");
			session.setAttribute("titleBarLogo", "images/upload_image.png");
			session.setAttribute("titleBarLogoPreview", "small-logo.png");
			session.setAttribute("appiconpreview", "images/upload_image.png");
			session.setAttribute("homeiconuploadpreview", "images/upload_image.png");
			session.setAttribute("homeiconpreview", "images/mainMenuBtn.png");
			session.setAttribute("sidemnuiconuploadpreview", "images/upload_image.png");
			session.setAttribute("sidemenuiconpreview", "images/hamburger.png");
			session.setAttribute("backbuttoniconuploadpreview", "images/upload_image.png");
			session.setAttribute("backbuttoniconpreview", "images/backBtn.png");
			session.setAttribute("claimIconPreview", "images/bannerPlchldr.png");
		} else {

			if (null != generalScreenDetails.getBgColor() && !"".equalsIgnoreCase(generalScreenDetails.getBgColor())) {
				generalScreenDetails.setIconSelection("color");
				session.setAttribute("titleBarLogo", "images/upload_image.png");
				session.setAttribute("titleBarLogoPreview", "small-logo.png");

			}

			if (null != generalScreenDetails.getLogoImageName() && !"".equalsIgnoreCase(generalScreenDetails.getLogoImageName())) {
				generalScreenDetails.setIconSelection("image");
				generalScreenDetails.setOldImageName(generalScreenDetails.getLogoImageName());
				session.setAttribute("titleBarLogo", generalScreenDetails.getLogoPath());
				session.setAttribute("titleBarLogoPreview", generalScreenDetails.getLogoPath());

			} else {
				session.setAttribute("titleBarLogo", "images/upload_image.png");
				session.setAttribute("titleBarLogoPreview", "small-logo.png");
			}

			if (null != generalScreenDetails.getBannerImageName() && !"".equalsIgnoreCase(generalScreenDetails.getBannerImageName())) {
				generalScreenDetails.setIconSelection("image");
				generalScreenDetails.setOldAppIconImage(generalScreenDetails.getBannerImageName());
				session.setAttribute("appiconpreview", generalScreenDetails.getImagePath());

			} else {
				session.setAttribute("appiconpreview", "images/upload_image.png");
			}
			
			
			if (null != generalScreenDetails.getHomeIconName() && !"".equalsIgnoreCase(generalScreenDetails.getHomeIconName())) {
				generalScreenDetails.setIconSelection("image");
				generalScreenDetails.setOldHomeIconName(generalScreenDetails.getHomeIconName());
				session.setAttribute("homeiconpreview", generalScreenDetails.getHomeIconPath());
				session.setAttribute("homeiconuploadpreview", generalScreenDetails.getHomeIconPath());

			} else {
				session.setAttribute("homeiconpreview", "images/mainMenuBtn.png");
				session.setAttribute("homeiconuploadpreview", "images/upload_image.png");
			}
			
			if( null!= generalScreenDetails.getSidemenuImageName() && !generalScreenDetails.getSidemenuImageName().equalsIgnoreCase("")){
				generalScreenDetails.setIconSelection("image");
				generalScreenDetails.setOldsidemenuImageName(generalScreenDetails.getSidemenuImageName());
				session.setAttribute("sidemenuiconuploadpreview", generalScreenDetails.getSidemenuImage());
				session.setAttribute("sidemenuiconpreview", generalScreenDetails.getSidemenuImage());
			}else{
				session.setAttribute("sidemenuiconuploadpreview", "images/upload_image.png");
				session.setAttribute("sidemenuiconpreview", "images/hamburger.png");
			}
			
			if (null != generalScreenDetails.getBackButtonIconName() && !"".equalsIgnoreCase(generalScreenDetails.getBackButtonIconName())) {
				generalScreenDetails.setIconSelection("image");
				generalScreenDetails.setOldBackButtonIconName(generalScreenDetails.getBackButtonIconName());
				session.setAttribute("backbuttoniconpreview", generalScreenDetails.getBackButtonIconPath());
				session.setAttribute("backbuttoniconuploadpreview", generalScreenDetails.getBackButtonIconPath());

			} else {
				session.setAttribute("backbuttoniconpreview", "images/backBtn.png");
				session.setAttribute("backbuttoniconuploadpreview", "images/upload_image.png");
			}
			
			if (null != generalScreenDetails.getClaimImage() && !"".equalsIgnoreCase(generalScreenDetails.getClaimImage())) {
				generalScreenDetails.setIconSelection("image");
				generalScreenDetails.setClaimType(true);
				generalScreenDetails.setOldClaimImage(generalScreenDetails.getClaimImage());
				session.setAttribute("claimIconPreview", generalScreenDetails.getClaimImagePath());
				
			} else {
				generalScreenDetails.setClaimType(false);
				session.setAttribute("claimIconPreview", "images/bannerPlchldr.png");
			}
			
			if (null != generalScreenDetails.getBottomBtnId()) {
				session.setAttribute("buttomBtn", "Exist");
				session.setAttribute("bottomBtnId", generalScreenDetails.getBottomBtnId());
			}
			
			if( null == generalScreenDetails.getNewsFirstFlag()){
				generalScreenDetails.setNewsFirstFlag(false);
			}
			generalScreenDetails.setIsCatImg(screenSettings.getIsCatImg());
			
			model.put("screenSettingsForm", generalScreenDetails);
			session.setAttribute("btnname", "Update Settings");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		session.setAttribute("menuType", settingType);
		return view;
	}

	/**
	 * This method will save the general setup details.
	 * 
	 * @param screenSettings
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/generealsettings.htm", method = RequestMethod.POST)
	public String saveGeneralSettings(@ModelAttribute("screenSettingsForm") ScreenSettings screenSettings, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveGeneralSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String status = null;
		String reponse = null;
		String screenType = screenSettings.getPageType();
		session.setAttribute("menuType", screenType);
		Boolean isValidURL = null;
		screenSettings.setClaimBannerTextFlag(true);

		String view = "menugeneralsettings";
		if ("TabBarLogo".equalsIgnoreCase(screenType)) {
			String buttomBtn = (String) session.getAttribute("buttomBtn");
			if ("Exist".equalsIgnoreCase(buttomBtn)) {
				screenSettings.setBottomBtnId((Integer) session.getAttribute("bottomBtnId"));
			}

			view = "generalsettings";
		} else if("CatImage".equalsIgnoreCase(screenType))	{
			view = "generalsettings";
		}
		
		if( null != screenSettings && screenSettings.isClaimType() ==  true){
			screenSettings.setClaimText(null);
		}else{
			screenSettings.setClaimImage(null);
			screenSettings.setClaimImagePath(null);
			session.setAttribute("claimIconPreview", "images/bannerPlchldr.png");
		}
		
		splashScreenSettingsValidator.validateGeneralSettings(screenSettings, result);
		isValidURL = Utility.validateURL(screenSettings.getHubCitiClaimURL());
		if (!isValidURL) {
			splashScreenSettingsValidator.validate(screenSettings, result, ApplicationConstants.INVALIDURL, "claim");
		}
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");
		if (result.hasErrors()) {
			screenSettings.setHiddenCategory(screenSettings.getCategory());
			@SuppressWarnings("unchecked")
			List<Category> findCatList = (List<Category>) session.getAttribute("findCategoryImg");
			if(null != findCatList && !findCatList.isEmpty()) {
				for(Category objCat : findCatList)	{
					if(objCat.getCatImgId().equals(screenSettings.getCategory()))	{
						screenSettings.setCatImgUrl(objCat.getCatImgPath());
					}
				}
			}
			model.put("screenSettingsForm", screenSettings);
			return view;
		} else {
			if("CatImage".equalsIgnoreCase(screenType))	{
				status = hubCitiService.updateCategoryImage(screenSettings, loginUser);
				if(null != status && ApplicationConstants.SUCCESS.equals(status))	{
					List<Category> findCatList = hubCitiService.getCategoryImageDetails(loginUser.gethCAdminUserID(), loginUser.getHubCitiID());
					session.setAttribute("findCategoryImg", findCatList);
					screenSettings.setCatImgName(null);
					session.removeAttribute("catImgPreview");
					for(Category objCat : findCatList)	{
						if(objCat.getCatImgId().equals(screenSettings.getCategory()))	{
							screenSettings.setCatImgUrl(objCat.getCatImgPath());
						}
					}
				}
			} else	{
				status = hubCitiService.saveGeneralSettings(screenSettings, loginUser);
			}
			

			if ("TabBarLogo".equalsIgnoreCase(screenType)) {
				session.setAttribute("buttomBtn", "Exist");
				session.setAttribute("bottomBtnId", screenSettings.getBottomBtnId());
			}

			if (null != status && status.equals(ApplicationConstants.SUCCESS)) {

				reponse = "Settings Saved Succesfully";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
				session.setAttribute("btnname", "Update Settings");
			} else {
				session.setAttribute("btnname", "Save Settings");
				reponse = "Error occured while saving Settings";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.FAILURE);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return view;
	}

	/**
	 * This method will display User Settings Screen.
	 * 
	 * @param screenSettings
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return view
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/usersettings.htm", method = RequestMethod.GET)
	public String displayUserSettings(@ModelAttribute("screenSettingsForm") ScreenSettings screenSettings, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "displayUserSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ScreenSettings userSettings = null;
		// Minimum crop height and width for login screen
		session.setAttribute("minCropHt", 116);
		session.setAttribute("minCropWd", 300);

		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.USERSETTINGSSCREEN);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");

		userSettings = hubCitiService.fetchUserSettings(loginUser.getHubCitiID());

		if (null != userSettings) {
			session.setAttribute("btnname", "Update");
			if (null != userSettings.getLogoPath()) {
				session.setAttribute("userSettingImg", userSettings.getLogoPath());
				userSettings.setOldImageName(userSettings.getLogoImageName());
			} else {
				session.setAttribute("userSettingImg", "images/upload_image.png");
			}
			session.setAttribute("pageView", "edit");
			model.put("screenSettingsForm", userSettings);
		} else {
			session.setAttribute("btnname", "Save");
			session.setAttribute("userSettingImg", "images/upload_image.png");
			session.setAttribute("pageView", "add");
			model.put("screenSettingsForm", new ScreenSettings());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "userSettings";
	}

	/**
	 * This method will Save User Settings Details.
	 * 
	 * @param screenSettings
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return view
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/saveusersettings.htm", method = RequestMethod.POST)
	public String saveUserSettings(@ModelAttribute("screenSettingsForm") ScreenSettings screenSettings, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveUserSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.USERSETTINGSSCREEN);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");
		String status = null;
		String response = null;

		registrationScreenSettingsValidator.validateUserSeetings(screenSettings, result);

		if (result.hasErrors()) {
			return "userSettings";
		} else {
			status = hubCitiService.saveUserSettings(screenSettings, loginUser);
			if (null != status && status.equals(ApplicationConstants.SUCCESS)) {
				if ("add".equalsIgnoreCase(screenSettings.getPageView())) {
					response = "User Settings Saved Succesfully";
				} else {
					response = "User Settings Updated Succesfully";
				}

				request.setAttribute("responeMsg", response);
				request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
				session.setAttribute("btnname", "Update");
				session.setAttribute("pageView", "edit");

				if (!screenSettings.getUserSettingsFields().contains("optn-img")) {
					session.setAttribute("userSettingImg", "images/upload_image.png");
					screenSettings.setLogoImageName(null);
					screenSettings.setOldImageName(null);
				} else {
					screenSettings.setOldImageName(screenSettings.getLogoImageName());
				}

			} else {
				session.setAttribute("btnname", "Save");
				if ("add".equalsIgnoreCase(screenSettings.getPageView())) {
					response = "Error occured while saving User Settings";
					session.setAttribute("pageView", "edit");
				} else {
					response = "Error occured while updating User Settings";
				}
				request.setAttribute("responeMsg", response);
				request.setAttribute("responseStatus", ApplicationConstants.FAILURE);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "userSettings";
	}
	
	@RequestMapping(value = "/updateimgcropsize.htm", method = RequestMethod.GET)
	@ResponseBody
	public String updateCropImageSize(@RequestParam(value = "uploadtype", required = true) String uploadtype, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws HubCitiServiceException	{
		final String methodName = "updateCropImageSize";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		if("catImg".equals(uploadtype))	{
			session.setAttribute("oldMinCropHt", session.getAttribute("minCropHt"));
			session.setAttribute("oldMinCropWd", session.getAttribute("minCropWd"));
			session.setAttribute("minCropHt", 46);
			session.setAttribute("minCropWd", 46);
		} else if ("logoImg".equals(uploadtype)) {
			if(null != session.getAttribute("oldMinCropHt") && !"".equals(session.getAttribute("oldMinCropHt")))	{
				session.setAttribute("minCropHt", session.getAttribute("oldMinCropHt"));
				session.setAttribute("minCropWd", session.getAttribute("oldMinCropWd"));
			}
		} else if("clearSessionImg".equals(uploadtype))	{
			session.removeAttribute("catImgPreview");
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return null;
	}
	
	/**
	 * This method will return login setup screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupTrainingScreen.htm", method = { RequestMethod.POST, RequestMethod.GET })
	public String setupTrainingScreen(HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "setupTrainingScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		// Minimum crop height and width for training screen
		session.setAttribute("minCropHt", 480);
		session.setAttribute("minCropWd", 320);
		
		session.removeAttribute("screenImageOne");
		session.removeAttribute("screenImageTwo");
		session.removeAttribute("screenImageThree");
		session.removeAttribute("screenImageFour");
		
	 	
		session.removeAttribute("url1");
		session.removeAttribute("url2");
		session.removeAttribute("url3");
		session.removeAttribute("url4");

		session.removeAttribute("isurlOrPage1");
		session.removeAttribute("isurlOrPage2");
		session.removeAttribute("isurlOrPage3");
		session.removeAttribute("isurlOrPage4");
		
		session.removeAttribute("urlvalue1");
		session.removeAttribute("urlvalue2");
		session.removeAttribute("urlvalue3");
		session.removeAttribute("urlvalue4");
	
		String[] nameList = null;
		String[] imagePathList = null;
		String[] associatePageURL=null;
		String[] associatePageURLTypeID=null;
		String[] associatePageURLValue=null;
		int imageListSize = 0;
		
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.LOGINSCREEN);

		final User loginUser = (User) session.getAttribute("loginUser");
		ScreenSettings loginScreenDetails = null;
		loginUser.setPageType("Login Page");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		loginScreenDetails = hubCitiService.fetchTrainingScreenSettings(loginUser);
		

		if (null == loginScreenDetails) {
			model.put("screenSettingsForm", new ScreenSettings());
			session.setAttribute("btnname", "Save Settings");
			session.setAttribute("trainingScreenImage", "images/upload_image.png");
		} else {
			model.put("screenSettingsForm", loginScreenDetails);
			
			session.setAttribute("trainingScreenImage", "images/upload_image.png"); 
			if (loginScreenDetails.getScreenName() != null) {
				nameList = loginScreenDetails.getScreenName().split(",");
				imagePathList = loginScreenDetails.getFirstUseImage().split(",");
				associatePageURL=loginScreenDetails.getAssociatePageURL().split(",");
				associatePageURLTypeID=loginScreenDetails.getAssociatePageURLTypeID().split(",");
				associatePageURLValue=loginScreenDetails.getAssociatePageURLValue().split(",");
				imageListSize = imagePathList.length;
				for(int i=0;i<4;i++){
					if (imageListSize > i && imagePathList[i] != null && !imagePathList[i].isEmpty()) {
						if ("1".equals(nameList[i])) {
							session.setAttribute("screenImageOne", imagePathList[i]);
							session.setAttribute("url1",associatePageURL[i]);
							session.setAttribute("isurlOrPage1",associatePageURLTypeID[i]);
							session.setAttribute("urlvalue1",associatePageURLValue[i]);
						} else if ("2".equals(nameList[i])) {
							session.setAttribute("screenImageTwo", imagePathList[i]);
							session.setAttribute("url2",associatePageURL[i]);
							session.setAttribute("isurlOrPage2",associatePageURLTypeID[i]);
							session.setAttribute("urlvalue2",associatePageURLValue[i]);
						} else if ("3".equals(nameList[i])) {
							session.setAttribute("screenImageThree", imagePathList[i]);
							session.setAttribute("url3",associatePageURL[i]);
							session.setAttribute("isurlOrPage3",associatePageURLTypeID[i]);
							session.setAttribute("urlvalue3",associatePageURLValue[i]);
						} else if ("4".equals(nameList[i])) {
							session.setAttribute("screenImageFour", imagePathList[i]);
							session.setAttribute("url4",associatePageURL[i]);
							session.setAttribute("isurlOrPage4",associatePageURLTypeID[i]);
							session.setAttribute("urlvalue4",associatePageURLValue[i]);
						}
					}
				}
				
		}
		}
		session.setAttribute("trainingScreenPreview", loginScreenDetails);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setuptrainingpage";
		
	}

	/**
	 * This method will return training setup screen.
	 * 
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/addTrainingScreen.htm", method = { RequestMethod.POST, RequestMethod.GET })
	public String addTrainingScreen(@ModelAttribute("screenSettingsForm") ScreenSettings loginScreenDetails, BindingResult result, HttpServletRequest request,
			ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "setupTrainingScreen";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String[] imgList = null;
		String[] nameList = null;
		String[] urlList=null;
		String[] upList=null;
		String[] vList=null;
		String isurl=null;
		String urlOrPage=null;
		String urlorBntlnkValue=null;
		
		List<String> imageList = new ArrayList<String>();
		List<String> screenNameList = new ArrayList<String>();
		
		
		List<String> isUrlList=new ArrayList<String>();
		List<String> isUrlOrPageList=new ArrayList<String>();
		List<String> urlOrbtnLinkIdList=new ArrayList<String>();
		
		String addorDeleteScreen = loginScreenDetails.getAddDeleteScreen();
		final User loginUser = (User) session.getAttribute("loginUser");
		loginUser.setPageType("Login Page");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		loginScreenDetails.setSlideUpdated(true);
		
		ScreenSettings screenlenght = (ScreenSettings) session.getAttribute("trainingScreenPreview");
		String response=null;
		if (null == loginScreenDetails) {
			model.put("screenSettingsForm", new ScreenSettings());
			session.setAttribute("trainingScreenImage", "images/upload_image.png");
			
			
		} else {

			if (null != addorDeleteScreen && "AddButton".equals(addorDeleteScreen)) {
				session.setAttribute("trainingScreenImage", "images/upload_image.png");
				loginScreenDetailsValidator.validateTrainingScreen(loginScreenDetails, result,loginScreenDetails.getIsUrl(),loginScreenDetails.getIsUrlOrPage());
				boolean isValidURL = Utility.validateURL(loginScreenDetails.getFirstUseURL());
				if (!isValidURL) {
					loginScreenDetailsValidator.validate(loginScreenDetails, result, ApplicationConstants.INVALIDURL);
				}
				if (result.hasErrors()) {
					return "setuptrainingpage";
				} else {
					if (loginScreenDetails.getScreenName() != null) {
						if (loginScreenDetails.getLogoImageName() != null && !loginScreenDetails.getLogoImageName().isEmpty()) {

							isurl=loginScreenDetails.getIsUrl();
							
													
							if(isurl.equals("0")){
								urlOrPage=null;
								urlorBntlnkValue=null;
							}else{
								urlOrPage=loginScreenDetails.getIsUrlOrPage();
								if("1".equals(urlOrPage)){
									urlorBntlnkValue=loginScreenDetails.getFirstUseURL();
								}else if("2".equals(urlOrPage)){
									urlorBntlnkValue=loginScreenDetails.getBtnLinkId();
								}
							}
							
							
							if (null != screenlenght.getScreenName() && !screenlenght.getScreenName().isEmpty()) {

								imgList = screenlenght.getImages().split(",");
								nameList = screenlenght.getScreenName().split(",");
								

								urlList = screenlenght.getAssociatePageURL().split(",");
								upList = screenlenght.getAssociatePageURLTypeID().split(",");
								vList = screenlenght.getAssociatePageURLValue().split(",");
								
								imageList = new ArrayList<String>(Arrays.asList(imgList));
								screenNameList = new ArrayList<String>(Arrays.asList(nameList));
								
								isUrlList =new ArrayList<String>(Arrays.asList(urlList));
								isUrlOrPageList = new ArrayList<String>(Arrays.asList(upList));
								urlOrbtnLinkIdList =new ArrayList<String>(Arrays.asList(vList));

								
								int nameListSize = screenNameList.size();

															
								
								if (nameListSize > 0 && "1".equals(screenNameList.get(0)) && "1".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageOne", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									session.setAttribute("url1",isurl);
									session.setAttribute("isurlOrPage1",urlOrPage);
									session.setAttribute("urlvalue1",urlorBntlnkValue );
									imageList.set(0, loginScreenDetails.getLogoImageName());
									screenNameList.set(0, loginScreenDetails.getScreenName());
									
									isUrlList.set(0, isurl);
									isUrlOrPageList.set(0, urlOrPage);
									urlOrbtnLinkIdList.set(0,urlorBntlnkValue);
									
								} else if ("1".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageOne", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									session.setAttribute("url1",isurl );
									session.setAttribute("isurlOrPage1",urlOrPage);
									session.setAttribute("urlvalue1",urlorBntlnkValue );
									imageList.add(0, loginScreenDetails.getLogoImageName());
									screenNameList.add(0, loginScreenDetails.getScreenName());
									
									isUrlList.add(0, isurl);
									isUrlOrPageList.add(0, urlOrPage);
									urlOrbtnLinkIdList.add(0,urlorBntlnkValue);

								}

								if (nameListSize > 1 && "2".equals(screenNameList.get(1)) && "2".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageTwo", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									session.setAttribute("url2", isurl);
									session.setAttribute("isurlOrPage2", urlOrPage);
									session.setAttribute("urlvalue2", urlorBntlnkValue);
									imageList.set(1, loginScreenDetails.getLogoImageName());
									screenNameList.set(1, loginScreenDetails.getScreenName());
									
									isUrlList.set(1, isurl);
									isUrlOrPageList.set(1, urlOrPage);
									urlOrbtnLinkIdList.set(1,urlorBntlnkValue);

								} else if ("2".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageTwo", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									
									session.setAttribute("url2", isurl);
									session.setAttribute("isurlOrPage2", urlOrPage);
									session.setAttribute("urlvalue2", urlorBntlnkValue);
									imageList.add(1, loginScreenDetails.getLogoImageName());
									screenNameList.add(1, loginScreenDetails.getScreenName());
									
									isUrlList.add(1, isurl);
									isUrlOrPageList.add(1, urlOrPage);
									urlOrbtnLinkIdList.add(1,urlorBntlnkValue);
								}

								if (nameListSize > 2 && "3".equals(screenNameList.get(2)) && "3".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageThree", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									session.setAttribute("url3", isurl);
									session.setAttribute("isurlOrPage3", urlOrPage);
									session.setAttribute("urlvalue3", urlorBntlnkValue);
									imageList.set(2, loginScreenDetails.getLogoImageName());
									screenNameList.set(2, loginScreenDetails.getScreenName());
									
									isUrlList.set(2, isurl);
									isUrlOrPageList.set(2, urlOrPage);
									urlOrbtnLinkIdList.set(2,urlorBntlnkValue);

								} else if ("3".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageThree", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									session.setAttribute("url3", isurl);
									session.setAttribute("isurlOrPage3", urlOrPage);
									session.setAttribute("urlvalue3", urlorBntlnkValue);
									imageList.add(2, loginScreenDetails.getLogoImageName());
									screenNameList.add(2, loginScreenDetails.getScreenName());
									
									isUrlList.add(2, isurl);
									isUrlOrPageList.add(2, urlOrPage);
									urlOrbtnLinkIdList.add(2,urlorBntlnkValue);
								}

								if (nameListSize > 3 && "4".equals(screenNameList.get(3)) && "4".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageFour", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									session.setAttribute("url4", isurl);
									session.setAttribute("isurlOrPage4", urlOrPage);
									session.setAttribute("urlvalue4", urlorBntlnkValue);
									imageList.set(3, loginScreenDetails.getLogoImageName());
									screenNameList.set(3, loginScreenDetails.getScreenName());
									
									isUrlList.set(3, isurl);
									isUrlOrPageList.set(3, urlOrPage);
									urlOrbtnLinkIdList.set(3,urlorBntlnkValue);

								} else if ("4".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageFour", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									session.setAttribute("url4", isurl);
									session.setAttribute("isurlOrPage4", urlOrPage);
									session.setAttribute("urlvalue4", urlorBntlnkValue);
									imageList.add(3, loginScreenDetails.getLogoImageName());
									screenNameList.add(3, loginScreenDetails.getScreenName());
									
									isUrlList.add(3, isurl);
									isUrlOrPageList.add(3, urlOrPage);
									urlOrbtnLinkIdList.add(3,urlorBntlnkValue);
								}

							} else {
								
								if ("1".equals(loginScreenDetails.getScreenName())) {
									session.setAttribute("screenImageOne", ApplicationConstants.TRAININGIMAGEPATH + loginScreenDetails.getLogoImageName());
									session.setAttribute("url1",isurl);
									session.setAttribute("isurlOrPage1",urlOrPage);
									session.setAttribute("urlvalue1",urlorBntlnkValue);
									imageList.add(0, loginScreenDetails.getLogoImageName());
									screenNameList.add(0, loginScreenDetails.getScreenName());
									
									isUrlList.add(0, isurl);
									isUrlOrPageList.add(0, urlOrPage);
									urlOrbtnLinkIdList.add(0,urlorBntlnkValue);
								}
							}							
							request.setAttribute("responeMsg","Slide "+loginScreenDetails.getScreenName()+" Added Succesfully");
							request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
						}
						
					}
				}

			} else if (null != addorDeleteScreen && "DeleteButton".equals(addorDeleteScreen)) {
				if (null != screenlenght.getScreenName() && !screenlenght.getScreenName().isEmpty()) {
					if (null != screenlenght.getImages() && !screenlenght.getImages().isEmpty()) {

						imgList = screenlenght.getImages().split(",");
						nameList = screenlenght.getScreenName().split(",");
						

						imageList = new ArrayList<String>(Arrays.asList(imgList));
						screenNameList = new ArrayList<String>(Arrays.asList(nameList));
						
						
						urlList = screenlenght.getAssociatePageURL().split(",");
						upList = screenlenght.getAssociatePageURLTypeID().split(",");
						vList = screenlenght.getAssociatePageURLValue().split(",");
						
						isUrlList = new ArrayList<String>(Arrays.asList(urlList));
						isUrlOrPageList = new ArrayList<String>(Arrays.asList(upList));
						urlOrbtnLinkIdList = new ArrayList<String>(Arrays.asList(vList));
						
						for (int i = 0; i < screenNameList.size(); i++) {
							String name = screenNameList.get(i);
							if (loginScreenDetails.getScreenName().equals(name)) {
								screenNameList.remove(i);
								imageList.remove(i);
								
								isUrlList.remove(i);
								isUrlOrPageList.remove(i);
								urlOrbtnLinkIdList.remove(i);
								
								loginScreenDetails.setLogoImageName(null);
								if ("1".equals(loginScreenDetails.getScreenName())) {
									session.removeAttribute("screenImageOne");
									session.removeAttribute("url1");
									session.removeAttribute("isurlOrPage1");
									session.removeAttribute("urlvalue1");
								}
								if ("2".equals(loginScreenDetails.getScreenName())) {
									session.removeAttribute("screenImageTwo");
									session.removeAttribute("url2");
									session.removeAttribute("isurlOrPage2");
									session.removeAttribute("urlvalue2");
								}
								if ("3".equals(loginScreenDetails.getScreenName())) {
									session.removeAttribute("screenImageThree");
									session.removeAttribute("url3");
									session.removeAttribute("isurlOrPage3");
									session.removeAttribute("urlvalue3");
								}
								if ("4".equals(loginScreenDetails.getScreenName())) {
									session.removeAttribute("screenImageFour");
									session.removeAttribute("url4");
									session.removeAttribute("isurlOrPage4");
									session.removeAttribute("urlvalue4");
								}
							}
						}
					}
				}
			}
			StringBuilder image = new StringBuilder();
			StringBuilder name = new StringBuilder();
			StringBuilder type = new StringBuilder();
			StringBuilder  isurlb=new StringBuilder();
			StringBuilder  isurlorPageb=new StringBuilder();
			StringBuilder  urlPageValue=new StringBuilder();
						
						
			for (String img : imageList) {
				image.append(img);
				image.append(",");
			}
			for (String screenName : screenNameList) {
				name.append(screenName);
				name.append(",");
			}
			
			
			for(String burl:isUrlList){
				isurlb.append(burl);
				isurlb.append(",");
			}
			
			for(String isurlOrPage:isUrlOrPageList){
				isurlorPageb.append(isurlOrPage);
				isurlorPageb.append(",");
			}
			
			for(String Urlvalue:urlOrbtnLinkIdList){
				urlPageValue.append(Urlvalue);
				urlPageValue.append(",");
			}
			
			loginScreenDetails.setScreenName(name.toString());
			loginScreenDetails.setImages(image.toString());
			
			loginScreenDetails.setAssociatePageURL(isurlb.toString());
			loginScreenDetails.setAssociatePageURLTypeID(isurlorPageb.toString());
			loginScreenDetails.setAssociatePageURLValue(urlPageValue.toString());
		
			
			session.setAttribute("trainingScreenPreview", loginScreenDetails);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setuptrainingpage";
	}

	/**
	 * This method saves the training setup details.
	 * 
	 * @param loginScreenDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/saveTrainingScreen.htm", method = RequestMethod.POST)
	public String saveTrainingSetupScreenDetails(@ModelAttribute("screenSettingsForm") ScreenSettings loginScreenDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {
		final String methodName = "saveTrainingSetupScreenDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");
		String status = null;
		String reponse = null;

		if (result.hasErrors()) {
			return "setuptrainingpage";
		} else {
			ScreenSettings trainingscreen = (ScreenSettings) session.getAttribute("trainingScreenPreview");
			loginScreenDetails.setImages(loginScreenDetails.getLogoImageName());
			if (trainingscreen != null) {
				if(trainingscreen.getScreenName() == null || trainingscreen.getScreenName().isEmpty()){
					loginScreenDetails.setScreenName(null);
					loginScreenDetails.setLogoImageName(null);
					loginScreenDetails.setAssociatePageURL(null);
					loginScreenDetails.setAssociatePageURLTypeID(null);
					loginScreenDetails.setAssociatePageURLValue(null);
				}else {
					loginScreenDetails.setLogoImageName(trainingscreen.getImages());
					loginScreenDetails.setScreenName(trainingscreen.getScreenName());
					
					loginScreenDetails.setAssociatePageURL(trainingscreen.getAssociatePageURL());
					loginScreenDetails.setAssociatePageURLTypeID(trainingscreen.getAssociatePageURLTypeID());
					loginScreenDetails.setAssociatePageURLValue(trainingscreen.getAssociatePageURLValue());
				}
			}
			status = hubCitiService.saveTrainingScreenSettings(loginScreenDetails, loginUser);
			
			if (null != status && status.equals(ApplicationConstants.SUCCESS)) {
				if((loginScreenDetails.getLogoImageName() == null || loginScreenDetails.getLogoImageName().isEmpty()) && trainingscreen.isSlideUpdated() != true){
					reponse = "Please setup atleast 1 slide to save";
				} else {
					reponse = "Settings Saved Succesfully";
				}
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.SUCCESS);
			} else {
				reponse = "Error Occured While Saving Settings";
				request.setAttribute("responeMsg", reponse);
				request.setAttribute("responseStatus", ApplicationConstants.FAILURE);
			}
			session.setAttribute("trainingScreenImage", "images/upload_image.png");
		}
		loginScreenDetails.setLogoImageName(null);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "setuptrainingpage";
	}
}

/**
 * 
 */
package com.hubciti.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.exception.HubCitiServiceException;
import com.hubciti.common.pojo.AnythingPages;
import com.hubciti.common.pojo.AppSiteDetails;
import com.hubciti.common.pojo.LogisticDetails;
import com.hubciti.common.pojo.Logistics;
import com.hubciti.common.pojo.Marker;
import com.hubciti.common.pojo.User;
import com.hubciti.common.tags.Pagination;
import com.hubciti.common.util.Utility;
import com.hubciti.service.HubCitiService;
import com.hubciti.validator.LogisticsValidator;

/**
 * @author sangeetha.ts
 * 
 */
@Controller
public class LogisticsController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LogisticsController.class);

	LogisticsValidator logisticsValidator;

	@Autowired
	public void setLogisticsValidator(LogisticsValidator logisticsValidator) {
		this.logisticsValidator = logisticsValidator;
	}

	public String fetchLogistics(Logistics logistics, Integer currentPage, String from, HttpServletRequest request, HttpSession session)
			throws HubCitiServiceException {

		String returnView = null;
		LogisticDetails logisticDetails = null;
		Pagination objPage = null;
		final int recordCount = 20;
		String searchKey = logistics.getSearchLogisticName();
		Integer lowerLimit = logistics.getLowerLimit();

		if (null == lowerLimit) {
			lowerLimit = 0;
		}

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

		try {

			User loginUser = (User) session.getAttribute("loginUser");

			logisticDetails = hubCitiService.displaySearchLogistics(loginUser.getHubCitiID(), logistics.getSearchLogisticName(), lowerLimit);

			if (null == logisticDetails && lowerLimit > 0) {
				lowerLimit = lowerLimit - 20;
				currentPage = (lowerLimit + recordCount) / recordCount;
				logistics.setLowerLimit(lowerLimit);
				logisticDetails = hubCitiService.displaySearchLogistics(loginUser.getHubCitiID(), logistics.getSearchLogisticName(), lowerLimit);
			}

			if ((null == searchKey || "".equals(searchKey)) && logisticDetails.getLogistics().isEmpty()) {
				session.removeAttribute(ApplicationConstants.PAGINATION);
				session.removeAttribute("logisticList");
				if (null == searchKey || "".equals(searchKey)) {
					returnView = "add";
				}
			} else if (logisticDetails.getLogistics().isEmpty()) {
				session.removeAttribute(ApplicationConstants.PAGINATION);
				session.removeAttribute("logisticList");
				request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "No Maps Found");
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, "INFO");
			} else {
				session.setAttribute("logisticList", logisticDetails.getLogistics());
				objPage = Utility.getPagination(logisticDetails.getTotalSize(), currentPage, "displaylogistics.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);

				if (from.equals("update")) {
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, "SUCCESS");
					request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Interactive Map Updated Successfully");
				} else if (from.equals("save")) {
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, "SUCCESS");
					request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Interactive Map Created Successfully");
				} else if (from.equals("delete")) {
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, "SUCCESS");
					request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Interactive Map Deleted Successfully");
				}

			}

			request.setAttribute("lowerLimit", logistics.getLowerLimit());
			request.setAttribute("searchLogisticName", searchKey);

		} catch (HubCitiServiceException exception) {

			throw new HubCitiServiceException(exception);
		}

		session.removeAttribute("imageCropPage");
		session.removeAttribute("markers");
		return returnView;
	}

	/**
	 * This method is to display alerts which are created by the user.
	 * 
	 * @param anythingPageDetails
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/displaylogistics.htm", method = RequestMethod.GET)
	public final String displayLogistics(@ModelAttribute("logistics") Logistics logistics, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws HubCitiServiceException {

		final String methodName = "displayLogistics";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);
		
		AnythingPages anythingPageList = null;
		
		final String pageFlag = request.getParameter("pageFlag");
		Integer lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		final int recordCount = 20;

		if (null != logistics.getLowerLimit()) {
			lowerLimit = logistics.getLowerLimit();
		}
		if (null != pageFlag && "true".equals(pageFlag)) {
			pageNumber = request.getParameter("pageNumber");
			final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
			if (Integer.valueOf(pageNumber) != 0) {
				currentPage = Integer.valueOf(pageNumber);
				final int number = Integer.valueOf(currentPage) - 1;
				final int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
		} else {
			currentPage = (lowerLimit + recordCount) / recordCount;
		}

		logistics.setLowerLimit(lowerLimit);
		String view = fetchLogistics(logistics, currentPage, "manage", request, session);

		if (view == "add") {
			anythingPageList = hubCitiService.displayAnythingPageForMarker(loginUser);

			if (null != anythingPageList) {
				session.setAttribute("anythingPageMarkerList", anythingPageList.getPageDetails());
			} else {
				session.setAttribute("anythingPageMarkerList", null);
			}
			
			request.setAttribute(ApplicationConstants.LOWERLIMIT, 0);
			request.setAttribute("searchLogisticName", null);

			session.setAttribute("imageCropPage", "Logistics");
			session.setAttribute("minCropHt", 150);
			session.setAttribute("minCropWd", 300);
			session.setAttribute("markers", null);
			session.setAttribute("logisticImagePreview", ApplicationConstants.DEFAULTIMAGESQR);

			Logistics logisticsDetails = new Logistics();
			logisticsDetails.setLowerLimit(0);
			
			
			model.put("logisticDetails", logisticsDetails);
			LOG.info(ApplicationConstants.METHODEND + methodName);
			return "addLogistics";
		} else {
			model.put("logistics", logistics);
			LOG.info(ApplicationConstants.METHODEND + methodName);
			return "displayLogistics";
		}

	}

	/**
	 * This method will return add alerts screen.
	 * 
	 * @param alerts
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/addlogistics.htm", method = RequestMethod.GET)
	public final String addLogistics(@ModelAttribute("logistics") Logistics logistics,BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws HubCitiServiceException {

		final String methodName = "addLogistics";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean(ApplicationConstants.HUBCITISERVICE);
		final User loginUser = (User) session.getAttribute(ApplicationConstants.LOGINUSER);
		
		AnythingPages anythingPageList = null;
		request.setAttribute(ApplicationConstants.LOWERLIMIT, 0);
		request.setAttribute("searchLogisticName", null);

		session.setAttribute("imageCropPage", "Logistics");
		session.setAttribute("minCropHt", 150);
		session.setAttribute("minCropWd", 300);
		session.setAttribute("markers", null);
		session.setAttribute("logisticImagePreview", ApplicationConstants.DEFAULTIMAGESQR);
		Logistics logisticsDetails = new Logistics();
		logisticsDetails.setLowerLimit(0);
		
		anythingPageList = hubCitiService.displayAnythingPageForMarker(loginUser);

		if (null != anythingPageList) {
			session.setAttribute("anythingPageMarkerList", anythingPageList.getPageDetails());
		} else {
			session.setAttribute("anythingPageMarkerList", null);
		}
		
		
		model.put("logisticDetails", logisticsDetails);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if(null != logistics.getAddMarker() && logistics.getAddMarker().equals("Yes")){
			return  "addMapMarker";
		} else {
			return "addLogistics";
		}
	}

	@RequestMapping(value = "/editlogistics.htm", method = RequestMethod.GET)
	public final String editLogistics(@ModelAttribute("logistics") Logistics logistics, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws HubCitiServiceException {

		final String methodName = "editLogistics";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		AnythingPages anythingPageList = null;
		Logistics logisticsDetails = null;
		ArrayList<Marker> markers = null;
		Integer logisticId = logistics.getLogisticId();
		Integer hubCitiID = null;
		request.setAttribute(ApplicationConstants.LOWERLIMIT, logistics.getLowerLimit());
		request.setAttribute("searchLogisticName", logistics.getSearchLogisticName());
		session.setAttribute("imageCropPage", "Logistics");
		session.setAttribute("minCropHt", 150);
		session.setAttribute("minCropWd", 300);

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			User user = (User) session.getAttribute("loginUser");
			hubCitiID = user.getHubCitiID();

			logisticsDetails = hubCitiService.fetchLogisticsDetails(logisticId, hubCitiID);

			markers = hubCitiService.fetchLogisticsMarkerDetails(logisticId, hubCitiID);

			if (null == logisticsDetails.getLogisticImageName() || "".equals(logisticsDetails.getLogisticImageName())) {
				session.setAttribute("logisticImagePreview", ApplicationConstants.DEFAULTIMAGESQR);
			} else {
				session.setAttribute("logisticImagePreview", logisticsDetails.getLogisticImagePath());
			}

			logisticsDetails.setOldLogisticImageName(logisticsDetails.getLogisticImageName());
			logisticsDetails.setHiddenLocationId(logisticsDetails.getLocationId());

			anythingPageList = hubCitiService.displayAnythingPageForMarker(user);

			if (null != anythingPageList) {
				session.setAttribute("anythingPageMarkerList", anythingPageList.getPageDetails());
			} else {
				session.setAttribute("anythingPageMarkerList", null);
			}
			
			session.setAttribute("markers", markers);
			model.put("logisticDetails", logisticsDetails);

		} catch (HubCitiServiceException e) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e.getStackTrace());
			throw new HubCitiServiceException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if(null != logistics.getAddMarker() && logistics.getAddMarker().equals("Yes")){
			return  "addMapMarker";
		} else {
			return "addLogistics";
		}
	}

	@RequestMapping(value = "/displaylogretnames", method = RequestMethod.GET)
	@ResponseBody
	public String getLogisticsRetailer(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws HubCitiServiceException {

		String strMethodName = "getLogisticsRetailer";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		String strRetName = request.getParameter("term");
		List<AppSiteDetails> retailerLst = null;
		JSONObject object = new JSONObject();
		JSONObject valueObj = null;
		JSONArray array = new JSONArray();
		AppSiteDetails objAppSiteDetails = new AppSiteDetails();
		String value;

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

		try {

			response.setContentType("application/json");
			User user = (User) session.getAttribute("loginUser");

			objAppSiteDetails.setHubCityId(user.getHubCitiID());
			objAppSiteDetails.setSearchKey(strRetName);
			retailerLst = hubCitiService.getLogisticsRetailer(objAppSiteDetails);

			if (null != retailerLst && !retailerLst.isEmpty()) {
				for (AppSiteDetails ret : retailerLst) {
					value = ret.getRetName();
					valueObj = new JSONObject();
					valueObj.put("retId", ret.getRetailId());
					valueObj.put("retname", ret.getRetName());
					valueObj.put("rname", value);
					valueObj.put("value", value);
					array.put(valueObj);
				}
			} else {
				valueObj = new JSONObject();
				valueObj.put("lable", "No Records Found");
				valueObj.put("value", "No Records Found");
				array.put(valueObj);
			}
			object.put("retnamelst", array);

		} catch (HubCitiServiceException e) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + e.getStackTrace());
			throw new HubCitiServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return object.get("retnamelst").toString();

	}

	@RequestMapping(value = "/displaylogretLoc", method = RequestMethod.GET)
	@ResponseBody
	public String displayLogRetailLocations(@RequestParam(value = "retId", required = true) String retId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws HubCitiServiceException {

		final String strMethodName = "displayLogRetailLocations";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		List<AppSiteDetails> retLocationLst = null;
		AppSiteDetails objAppSiteDetails = new AppSiteDetails();
		int iHubCityId = 0;
		JSONObject object = new JSONObject();
		JSONObject valueObj = null;
		JSONArray array = new JSONArray();
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		String value = null;

		try {
			User user = (User) session.getAttribute("loginUser");

			if (null != user && !"".equals(user)) {
				iHubCityId = user.getHubCitiID();
				objAppSiteDetails.setHubCityId(iHubCityId);
			}

			if (null != retId && !"".equals(retId)) {
				objAppSiteDetails.setRetailId(Integer.valueOf(retId));
			}

			retLocationLst = hubCitiService.displayLogisticsRetailLocations(objAppSiteDetails);

			if (null != retLocationLst && !retLocationLst.isEmpty()) {
				for (AppSiteDetails ret : retLocationLst) {
					value = ret.getAddress();
					valueObj = new JSONObject();
					valueObj.put("retLocId", ret.getRetLocId());
					valueObj.put("address", value);
					array.put(valueObj);
				}
			} else {
				valueObj = new JSONObject();
				valueObj.put("lable", "No Records Found");
				valueObj.put("value", "No Records Found");
				array.put(valueObj);
			}
			object.put("retloclst", array);

		} catch (HubCitiServiceException e) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + e.getStackTrace());
			throw new HubCitiServiceException(e);
		}

		return object.get("retloclst").toString();

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/addmarker.htm", method = RequestMethod.POST)
	public String addMarker(@ModelAttribute("logisticDetails") Logistics logistics, BindingResult result, HttpServletRequest request, ModelMap model,
			HttpSession session) throws HubCitiServiceException, IOException {

		final String methodName = "addMarker";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		request.setAttribute(ApplicationConstants.LOWERLIMIT, logistics.getLowerLimit());
		request.setAttribute("searchLogisticName", logistics.getSearchLogisticName());

		ArrayList<Marker> markers = (ArrayList<Marker>) session.getAttribute("markers");
		final String fileSeparator = System.getProperty("file.separator");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		final User loginUser = (User) session.getAttribute("loginUser");
		String markerId = logistics.getMarkerId();
		Logistics newLogistics;
		int index = 0;
		String response = null;
		Logistics logisticsDetails = null;
		Integer hubCitiID = null;
		hubCitiID = loginUser.getHubCitiID();
		

		if ("add".equalsIgnoreCase(logistics.getFunctionality())) {

			markerId = logistics.getMarkerName().trim() + logistics.getMarkerLatitude() + logistics.getMarkerLongitude();

			String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
			MultipartFile file = logistics.getMarkerImageFile();
			InputStream inputStream = file.getInputStream();

			String fileName = file.getOriginalFilename();
			fileName = Utility.getImagewithDateTime(fileName);
			Utility.writeFileData(inputStream, tempImgPath + fileSeparator + fileName);

			Marker marker = new Marker();
			marker.setMarkerId(markerId);
			marker.setMarkerName(logistics.getMarkerName());
			marker.setMarkerLatitude(logistics.getMarkerLatitude());
			marker.setMarkerLongitude(logistics.getMarkerLongitude());
			marker.setMarkerImageName(fileName);
			marker.setMarkerImagePath(ApplicationConstants.TEMPIMGPATH + fileName);
			if (!"".equals(Utility.checkNull(logistics.getAnythingPageId()))) {
				marker.setAnythingPageId(logistics.getAnythingPageId());
			} else {
				marker.setAnythingPageId(null);
			}

			if (null == markers) {
				markers = new ArrayList<Marker>();
			}

			markers.add(marker);

		} else if ("edit".equalsIgnoreCase(logistics.getFunctionality())) {
			for (Marker marker : markers) {
				if (markerId.equals(marker.getMarkerId())) {
					markerId = logistics.getMarkerName().trim() + logistics.getMarkerLatitude() + logistics.getMarkerLongitude();

					if (null != logistics.getMarkerImageFile() && !"".equals(marker.getMarkerImageFile())) {
						MultipartFile file = logistics.getMarkerImageFile();
						String fileName = file.getOriginalFilename();
						if (null != fileName && !"".equals(fileName)) {
							String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
							InputStream inputStream = file.getInputStream();
							fileName = Utility.getImagewithDateTime(fileName);
							Utility.writeFileData(inputStream, tempImgPath + fileSeparator + fileName);

							marker.setMarkerImageName(fileName);
							marker.setMarkerImagePath(ApplicationConstants.TEMPIMGPATH + fileName);
						}
					}

					marker.setMarkerId(markerId);
					marker.setMarkerName(logistics.getMarkerName());
					marker.setMarkerLatitude(logistics.getMarkerLatitude());
					marker.setMarkerLongitude(logistics.getMarkerLongitude());
					marker.setMarkerLongitude(logistics.getMarkerLongitude());
					
					if (!"".equals(Utility.checkNull(logistics.getAnythingPageId()))) {
						marker.setAnythingPageId(logistics.getAnythingPageId());
					} else {
						marker.setAnythingPageId(null);
					}
				}
			}

		} else if ("delete".equalsIgnoreCase(logistics.getFunctionality())) {
			for (Marker marker : markers) {
				if (markerId.equals(marker.getMarkerId())) {
					break;
				}
				index++;
			}

			markers.remove(index);
		}
		
		logisticsDetails = hubCitiService.fetchLogisticsDetails(logistics.getLogisticId(), hubCitiID);

		newLogistics = new Logistics();
		newLogistics.setLogisticId(logistics.getLogisticId());
		newLogistics.setLogisticName(logisticsDetails.getLogisticName());
		newLogistics.setLogisticImageName(logisticsDetails.getLogisticImageName());
		newLogistics.setOldLogisticImageName(logisticsDetails.getOldLogisticImageName());
		newLogistics.setRetailerId(logisticsDetails.getRetailerId());
		newLogistics.setRetailerName(logisticsDetails.getRetailerName());
		newLogistics.setLocationId(logisticsDetails.getLocationId());
		newLogistics.setHiddenLocationId(logisticsDetails.getLocationId());
		newLogistics.setStartDate(logisticsDetails.getStartDate());
		newLogistics.setEndDate(logisticsDetails.getEndDate());
		newLogistics.setIsPortrtOrLandscp(logisticsDetails.getIsPortrtOrLandscp());
		
		response = hubCitiService.saveUpdateMarkers(newLogistics, loginUser, markers);

		if (ApplicationConstants.SUCCESS.equals(response)) {

			if (null != logistics.getLogisticId() && !"".equals(logistics.getLogisticId())) {
				//fetchLogistics(newLogistics, currentPage, "update", request, session);
				model.put("logistics", newLogistics);
			} else {
				fetchLogistics(logistics, 1, "save", request, session);
				Logistics logisticDtls = new Logistics();
				logisticDtls.setLowerLimit(0);
				model.put("logistics", logisticDtls);
			}
		}

		session.setAttribute("markers", markers);
		model.put("logisticDetails", newLogistics);

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "addMapMarker";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/savelogistics.htm", method = RequestMethod.POST)
	public final ModelAndView saveUpdateLogistics(@ModelAttribute("logisticDetails") Logistics logistics, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException {

		final String methodName = "saveUpdateLogistics";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Integer lowerLimit = logistics.getLowerLimit();
		Integer recordCount = 20;
		Integer currentPage = (lowerLimit + recordCount) / recordCount;

		request.setAttribute(ApplicationConstants.LOWERLIMIT, lowerLimit);
		request.setAttribute("searchLogisticName", logistics.getSearchLogisticName());
		logistics.setHiddenLocationId(logistics.getLocationId());

		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			final User loginUser = (User) session.getAttribute("loginUser");
			final ArrayList<Marker> markers = (ArrayList<Marker>) session.getAttribute("markers");
			String response = null;

			logisticsValidator.validate(logistics, result);

			result = validateDates(logistics, result);

			if (result.hasErrors()) {

				return new ModelAndView("addLogistics");

			} else {

				response = hubCitiService.saveUpdateLogistics(logistics, loginUser, markers);

				if (ApplicationConstants.SUCCESS.equals(response)) {

					if (null != logistics.getLogisticId() && !"".equals(logistics.getLogisticId())) {
						fetchLogistics(logistics, currentPage, "update", request, session);
						model.put("logistics", logistics);
					} else {
						fetchLogistics(logistics, 1, "save", request, session);
						Logistics logisticsDetails = new Logistics();
						logisticsDetails.setLowerLimit(0);
						model.put("logistics", logisticsDetails);
					}
				}
			}

		} catch (HubCitiServiceException e) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e.getStackTrace());
			throw new HubCitiServiceException(e);
		}

		return new ModelAndView("displayLogistics");
	}

	/**
	 * This Method is to validate start and end date.
	 * 
	 * @param alerts
	 * @param result
	 * @return
	 * @throws HubCitiServiceException
	 */
	public final BindingResult validateDates(Logistics logistics, BindingResult result) throws HubCitiServiceException {
		String compDate = null;
		final Date currentDate = new Date();
		Boolean validStartDate = false;
		Boolean validEndDate = false;

		if (!"".equals(Utility.checkNull(logistics.getStartDate()))) {
			validStartDate = Utility.isValidDate(logistics.getStartDate());

			if (validStartDate && (null == logistics.getLogisticId() || "".equals(logistics.getLogisticId()))) {
				compDate = Utility.compareCurrentDate(logistics.getStartDate(), currentDate);
				if (null != compDate) {
					logisticsValidator.validateDates(logistics, result, ApplicationConstants.DATESTARTCURRENT);
				}
			} else if (!validStartDate) {
				logisticsValidator.validateDates(logistics, result, ApplicationConstants.VALIDSTARTDATE);
			}
		}
		if (!"".equals(Utility.checkNull(logistics.getEndDate()))) {
			validEndDate = Utility.isValidDate(logistics.getEndDate());
			validStartDate = Utility.isValidDate(logistics.getStartDate());

			if (validEndDate) {
				if (validStartDate) {
					compDate = Utility.compareDate(logistics.getStartDate(), logistics.getEndDate());
					if (null != compDate) {
						logisticsValidator.validateDates(logistics, result, ApplicationConstants.DATEAFTER);
					}
				} else {
					compDate = Utility.compareCurrentDate(logistics.getEndDate(), currentDate);
					if (null != compDate) {
						logisticsValidator.validateDates(logistics, result, ApplicationConstants.DATEENDCURRENT);
					}
				}
			} else {
				logisticsValidator.validateDates(logistics, result, ApplicationConstants.VALIDENDDATE);
			}
		}
		return result;
	}

	@RequestMapping(value = "/deletelogistics.htm", method = RequestMethod.GET)
	public final ModelAndView deleteLogistics(@ModelAttribute("logistics") Logistics logistics, BindingResult result, HttpServletRequest request,
			ModelMap model, HttpSession session) throws HubCitiServiceException {

		final String methodName = "deleteLogistics";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String view = "manage";

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			final User loginUser = (User) session.getAttribute("loginUser");

			String response = null;
			Integer lowerLimit = logistics.getLowerLimit();
			Integer recordCount = 20;
			Integer currentPage = (lowerLimit + recordCount) / recordCount;

			response = hubCitiService.deleteLogistics(logistics.getLogisticId(), loginUser.getHubCitiID());

			if (ApplicationConstants.SUCCESS.equals(response)) {
				view = fetchLogistics(logistics, currentPage, "delete", request, session);
			}

			if (view == "add") {
				request.setAttribute(ApplicationConstants.LOWERLIMIT, 0);
				request.setAttribute("searchLogisticName", null);

				session.setAttribute("imageCropPage", "Logistics");
				session.setAttribute("minCropHt", 150);
				session.setAttribute("minCropWd", 300);
				session.setAttribute("markers", null);
				session.setAttribute("logisticImagePreview", ApplicationConstants.DEFAULTIMAGESQR);

				Logistics logisticsDetails = new Logistics();
				logisticsDetails.setLowerLimit(0);
				model.put("logisticDetails", logisticsDetails);
				LOG.info(ApplicationConstants.METHODEND + methodName);
				return new ModelAndView("addLogistics");
			} else {
				LOG.info(ApplicationConstants.METHODEND + methodName);
				model.put("logistics", logistics);
				return new ModelAndView("displayLogistics");
			}
		} catch (HubCitiServiceException e) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e.getStackTrace());
			throw new HubCitiServiceException(e);
		}
	}

}

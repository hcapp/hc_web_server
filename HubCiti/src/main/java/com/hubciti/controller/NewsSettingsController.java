package com.hubciti.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.exception.HubCitiServiceException;
import com.hubciti.common.pojo.NewsCategory;
import com.hubciti.common.pojo.NewsSettings;
import com.hubciti.common.pojo.User;
import com.hubciti.common.tags.Pagination;
import com.hubciti.common.util.Utility;
import com.hubciti.service.HubCitiService;
import com.hubciti.validator.NewsSettingsValidator;

/**
 * This class is used to setup news settings and Manage news categories.
 * 
 * @author shyamsundara_hm
 * 
 */
@Controller
public class NewsSettingsController {

	/**
	 * Getting logger instance
	 */

	private static final Logger log = LoggerFactory.getLogger(NewsSettingsController.class);

	private NewsSettingsValidator newsSettingsValidator;

	@Autowired
	public void setNewsSettingsValidator(NewsSettingsValidator newsSettingsValidator) {
		this.newsSettingsValidator = newsSettingsValidator;
	}

	/**
	 * Below method is used to display News settings screen.
	 * 
	 * @param request
	 * @param response
	 * @param mode
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/newsgeneralsettings.htm", method = RequestMethod.GET)
	public String displayNewsSettings(@ModelAttribute("addnewsform") NewsCategory newsCategory,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws HubCitiServiceException {

		final String strViewName = "newsgeneralsettings";
		final String strMethodName = "displayNewsSettings";
		NewsCategory newsImage = null;
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		NewsSettings newssettings = null;
		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			User user = (User) session.getAttribute("loginUser");

			// fetching banner and news images.
			newsImage = hubCitiService.fetchNewsImages(user, newsCategory);

			session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.SETUPNEWSSETTINGS);

			if (null != newsImage.getBannerImgPath()) {
				session.setAttribute("newsbannerimage", newsImage.getBannerImgPath());
			} else {
				session.setAttribute("newsbannerimage", "images/upload_image.png");
			}

			if (null != newsImage.getNewsImgPath()) {
				session.setAttribute("newsdefaultimage", newsImage.getNewsImgPath());
			} else {
				session.setAttribute("newsdefaultimage", "images/upload_image.png");
			}

			if (null != newsCategory) {

				newsCategory.setLowerLimit(0);
				newsCategory.setCatName(null);
			}

			newssettings = hubCitiService.getNewsCategories(newsCategory, user);

			if (null != newssettings) {
				if (null != newssettings.getNewscatelst() && !newssettings.getNewscatelst().isEmpty()) {
					request.setAttribute("dispmngcat", "yes");
				}

			}

			session.setAttribute("generalsettings", newsImage);

			session.setAttribute("imageCropPage", "NewsSettings");

			if (null != newsImage) {
				model.put("addnewsform", newsImage);

			} else {

				model.put("addnewsform", newsCategory);
			}

		} catch (HubCitiServiceException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}
		log.info(ApplicationConstants.METHODEND + strMethodName);

		return strViewName;
	}

	/**
	 * Belo method is used to Save news settings to database.
	 * 
	 * @param newsCategory
	 * @param result
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */

	@RequestMapping(value = "/newsgeneralsettings.htm", method = { RequestMethod.POST })
	public String SaveNewsSettings(@ModelAttribute("addnewsform") NewsCategory newsCategory, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws HubCitiServiceException {

		String strViewName = "newsgeneralsettings";
		final String strMethodName = "displayNewsSettings";
		NewsCategory newsLst = null;
		NewsCategory newsImage = null;
		NewsCategory objNewsCategory = null;
		Boolean isValidURL = null;
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		NewsSettings newssettings = null;
		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			User user = (User) session.getAttribute("loginUser");

			newsSettingsValidator.validate(null, newsCategory, result);

			isValidURL = Utility.validateURL(newsCategory.getWeatherURL());
			if (!isValidURL) {
				newsSettingsValidator.validate(newsCategory, result, ApplicationConstants.INVALIDURL, "weather");
			}
			if (result.hasErrors()) {
				model.put("addnewsform", newsCategory);
				strViewName = "newsgeneralsettings";
			} else {

				// save banner and news images.
				newsImage = hubCitiService.fetchNewsImages(user, newsCategory);

				// fetch banner and news images.

				if (null != newsImage && newsImage.getResponse().equals(ApplicationConstants.SUCCESS)) {
					request.setAttribute("addsuccessMsg", ApplicationConstants.ADDNEWSCATEGORYSUCCESSTEXT);
					objNewsCategory = new NewsCategory();

					newsLst = hubCitiService.fetchNewsImages(user, objNewsCategory);

					if (null != newsLst) {

						session.setAttribute("newsbannerimage", newsLst.getBannerImgPath());
						session.setAttribute("newsdefaultimage", newsLst.getNewsImgPath());

					} else {

						session.setAttribute("newsbannerimage", "images/upload_image.png");
						session.setAttribute("newsdefaultimage", "images/upload_image.png");

					}

				} else {
					request.setAttribute("failureMsg", ApplicationConstants.DELETENEWSCATEGORYFAILURETEXT);

				}
			}

			if (null != newsCategory) {
				newsCategory.setLowerLimit(0);
				newsCategory.setCatName(null);

			}

			newssettings = hubCitiService.getNewsCategories(newsCategory, user);

			if (null != newssettings) {

				if (null != newssettings.getNewscatelst() && !newssettings.getNewscatelst().isEmpty()) {
					request.setAttribute("dispmngcat", "yes");
				}

			}
			session.setAttribute("generalsettings", newsImage);

			session.setAttribute("imageCropPage", "NewsSettings");

			model.put("addnewsform", newsCategory);
		} catch (HubCitiServiceException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}
		log.info(ApplicationConstants.METHODEND + strMethodName);

		return strViewName;
	}

	@RequestMapping(value = "/reordernewscats.htm", method = { RequestMethod.POST })
	public ModelAndView reorderCategories(@ModelAttribute("newssettings") NewsCategory newsCategory,
			HttpServletRequest request, ModelMap model, HttpServletResponse res, HttpSession session)
			throws HubCitiServiceException {
		final String strMethodName = "ReOrderCategories";

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
		session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.NEWSSETTINGS);
		User user = (User) session.getAttribute("loginUser");

		String status = hubCitiService.reOrderNewscategories(newsCategory.getNewsCatIds(), user.getHubCitiID(),
				user.gethCAdminUserID());
		return new ModelAndView(new RedirectView("managenewscats.htm?message=" + status));
	}

	/**
	 * This method is used to fetch news categories...
	 * 
	 * @param newsCategory
	 * @param request
	 * @param model
	 * @param res
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/managenewscats.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView manageNewsCategories(@RequestParam(value = "message", required = false) String message,
			@ModelAttribute("newssettings") NewsCategory newsCategory, HttpServletRequest request, ModelMap model,
			HttpServletResponse res, HttpSession session) throws HubCitiServiceException {
		final String strMethodName = "manageNewsCategories";
		final String strViewName = "managecategory";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		ArrayList<NewsCategory> newsCategorylst = null;
		NewsSettings newssettings = null;
		try {

			session.removeAttribute("newssettingslst");

			// if lower limit is null then assign zero to lower limit

			/*
			 * if (null == newsCategory.getLowerLimit() ||
			 * newsCategory.getLowerLimit().equals("")) {
			 * newsCategory.setLowerLimit(0); }
			 */

			if (null == newsCategory.getCatName() || newsCategory.getCatName().equals("")) {
				newsCategory.setCatName(null);

			}

			// for pagination
			// final String pageFlag = (String)
			// request.getParameter("pageFlag");
			/*
			 * String pageNumber = "0"; Integer currentPage = 1; Pagination
			 * objPagination = null; final Integer iRecordCount = 10;
			 */

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.NEWSSETTINGS);
			User user = (User) session.getAttribute("loginUser");

			// Integer lowerLimit = newsCategory.getLowerLimit();

			/*
			 * if (null != pageFlag && "true".equals(pageFlag)) { pageNumber =
			 * (String) request.getParameter("pageNumber");
			 * session.setAttribute("pageNum", pageNumber); final Pagination
			 * pagination = (Pagination) session.getAttribute("pagination");
			 * 
			 * if (Integer.valueOf(pageNumber) != 0) { currentPage =
			 * Integer.valueOf(pageNumber); final int number =
			 * Integer.valueOf(currentPage) - 1; final int pageSize =
			 * pagination.getPageRange(); lowerLimit = pageSize * number; } }
			 * 
			 * else {
			 * 
			 * currentPage = (lowerLimit + iRecordCount) / iRecordCount; }
			 */

			// newsCategory.setLowerLimit(lowerLimit);

			newssettings = hubCitiService.getNewsCategories(newsCategory, user);

			if (null != newssettings) {

				/*
				 * if (null != newssettings.getTotalSize() &&
				 * newssettings.getTotalSize() > 0) { objPagination =
				 * Utility.getPagination(newssettings.getTotalSize(),
				 * currentPage, "managenewscats.htm", iRecordCount);
				 * session.setAttribute("pagination", objPagination);
				 */
				session.setAttribute("newssettingslst", newssettings);
				/*
				 * } else { session.removeAttribute("pagination"); }
				 */
			} else {

				if (null == newsCategory.getCatName() || newsCategory.getCatName().equals("")) {
					return new ModelAndView(new RedirectView("addcatsettings.htm?fromdel=yes"));
				}
			}

			request.setAttribute("catsearchkey", newsCategory.getCatName());

			if (!Utility.isEmptyOrNullString(message)) {
				if (message.equals(ApplicationConstants.SUCCESS)) {
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, ApplicationConstants.SUCCESS);
					request.setAttribute(ApplicationConstants.RESPONSEMESSAGE,
							"Category sort order is saved successfully.");
				} else {
					request.setAttribute(ApplicationConstants.RESPONSESTATUS, ApplicationConstants.FAILURE);
					request.setAttribute(ApplicationConstants.RESPONSEMESSAGE, "Error occured while saving sort order.");
				}
			} else {
				request.setAttribute(ApplicationConstants.RESPONSESTATUS, null);
			}

			model.put("newssettings", newsCategory);

		} catch (HubCitiServiceException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}

		log.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);

	}

	/**
	 * This method is used to delete news category.
	 * 
	 * @param newsCategory
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */

	@RequestMapping(value = "/deletenewscat.htm", method = RequestMethod.POST)
	ModelAndView DeleteNewsCategory(@ModelAttribute("newssettings") NewsCategory newsCategory,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws HubCitiServiceException {

		final String strMethodName = "manageNewsCategories";
		final String strViewName = "managecategory";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		String strResponse = null;
		NewsSettings newssettings = null;
		try {
			session.removeAttribute("newssettingslst");

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.NEWSSETTINGS);
			User user = (User) session.getAttribute("loginUser");

			strResponse = hubCitiService.deleteNewsCategories(newsCategory, user);

			if (null != strResponse && !"".equals(strResponse)) {

				if (null != newsCategory) {
					newsCategory.setCatName(null);
					newsCategory.setNewsSettingsID(null);
				}

				newssettings = hubCitiService.getNewsCategories(newsCategory, user);

				if (null != newssettings) {

					if (null != newssettings.getNewscatelst() && !newssettings.getNewscatelst().isEmpty()
							&& strResponse.equalsIgnoreCase("SUCCESS")) {

						request.setAttribute("successMsg", ApplicationConstants.DELETENEWSCATEGORYSUCCESSTEXT);
						session.setAttribute("newssettingslst", newssettings);

					} else if (null != newssettings.getNewscatelst() && !newssettings.getNewscatelst().isEmpty()
							&& strResponse.equalsIgnoreCase(ApplicationConstants.FAILURETEXT + "_FOREIGNKEY")) {
						request.setAttribute("failureMsg", ApplicationConstants.CATASSMESSAGE);
						session.setAttribute("newssettingslst", newssettings);
					} else {
						request.setAttribute("failureMsg", ApplicationConstants.CATERRORMESSAGE);
						session.setAttribute("newssettingslst", newssettings);
					}
				} else {
					return new ModelAndView(new RedirectView("addcatsettings.htm"));
				}

			} else {

				request.setAttribute("failureMsg", ApplicationConstants.DELETENEWSCATEGORYFAILURETEXT);
			}

		} catch (HubCitiServiceException exception) {

			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		}

		log.info(ApplicationConstants.METHODEND + strMethodName);

		return new ModelAndView(strViewName);
	}

	/**
	 * Below method is used to adding news categories.
	 * 
	 * @param newsCategory
	 * @param result
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/addcatsettings.htm", method = RequestMethod.GET)
	String addNewsCategory(@ModelAttribute("addnewsform") NewsCategory newsCategory, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws HubCitiServiceException {

		final String strMethodName = "manageNewsCategories";
		final String strViewName = "addcatsettings";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		List<NewsCategory> newsCategoryLst = null;
		List<NewsCategory> newsTypesLst = null;
		List<NewsCategory> newsTemplatesLst = null;
		List<NewsCategory> subpagelst = null;
		NewsSettings newssettings = null;
		try {
			session.removeAttribute("newssettingslst");

			request.getParameter("");
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.NEWSSETTINGS);
			User user = (User) session.getAttribute("loginUser");

			newsCategory.setHidIsNewsTicker("newsno");
			// fetching news categories list.
			newsCategoryLst = hubCitiService.fetchNewsCategoryList(user);
			session.setAttribute("newscatlst", newsCategoryLst);

			// fetching news category display type list...
			newsTypesLst = hubCitiService.fetchNewsTypesList(user);
			session.setAttribute("newstypelst", newsTypesLst);

			// fetching news templates.. newsTemplatesLst =
			newsTemplatesLst = hubCitiService.fetchNewsTemplates(user);
			session.setAttribute("newstemplateslst", newsTemplatesLst);

			// fetching subpage list...
			subpagelst = hubCitiService.fetchNewsSubpageslst(user);
			session.setAttribute("subpagelst", subpagelst);

			if (null != newsCategory) {

				// newsCategory.setLowerLimit(0);
				newsCategory.setCatName(null);
				newsCategory.setCatColor("#000000");
				newsCategory.setCatFontColor("#ffffff");
				newsCategory.setBackButtonColor("#ffffff");
				newsCategory.setIsFeed(true);
			}

			newssettings = hubCitiService.getNewsCategories(newsCategory, user);

			if (null != newssettings) {
				if (null != newssettings && null != newssettings.getNewscatelst()
						&& !newssettings.getNewscatelst().isEmpty()) {
					request.setAttribute("dispmngcat", "yes");
				}
			}

		} catch (HubCitiServiceException exception) {

			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		}

		model.put("addnewsform", newsCategory);
		log.info(ApplicationConstants.METHODEND + strMethodName);

		return strViewName;
	}

	/**
	 * Below method is used to add category.
	 * 
	 * @param newsCategory
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/addcatsettings.htm", method = { RequestMethod.POST })
	String saveNewsCategory(@ModelAttribute("addnewsform") NewsCategory newsCategory, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws HubCitiServiceException {

		final String strMethodName = "saveNewsCategory";
		String strViewName = "addcatsettings";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		String strResponse = null;
		Boolean isValidURL = null;
		Boolean isDefaultcat = null;
		Boolean isNewsTicker = null;
		NewsSettings newssettings = null;

		try {
			session.removeAttribute("newssettingslst");

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.NEWSSETTINGS);
			User user = (User) session.getAttribute("loginUser");

			request.setAttribute("disptype", newsCategory.getDisplayType());
			request.setAttribute("hidsubpage", newsCategory.getSubPageName());
			request.setAttribute("hidCatName", newsCategory.getCatName());

			isDefaultcat = newsCategory.getIsDefault();
			isNewsTicker = newsCategory.getIsNewsTicker();
			if (null != isNewsTicker && isNewsTicker == true) {
				newsCategory.setHidIsNewsTicker("newsyes");
			} else {

				newsCategory.setHidIsNewsTicker("newsno");
				newsCategory.setNoStories(null);

			}

			/*
			 * if (null != newsCategory.getCatfeedURL() &&
			 * newsCategory.getMethodType().equals("addsub")) {
			 * 
			 * request.setAttribute("addcatsubmsg",
			 * ApplicationConstants.SAVESUBCATEGORYSUCCESSTEXT);
			 * //request.setAttribute("hidsubcatid",
			 * newsCategory.getSubCatIds()); //
			 * request.setAttribute("hidsubcaturl",
			 * newsCategory.getSubCatURL());
			 * 
			 * } else {
			 */

			newsSettingsValidator.validate(newsCategory, result);

			isValidURL = Utility.validateNewsURL(newsCategory.getCatfeedURL());

			if (!isValidURL) {
				newsSettingsValidator.validate(newsCategory, result, ApplicationConstants.INVALIDURL, null);
			}
			if (result.hasErrors()) {
				// request.setAttribute("hidsubcatid",
				// newsCategory.getSubCatIds());
				// request.setAttribute("hidsubcaturl",
				// newsCategory.getSubCatURL());
				model.put("addnewsform", newsCategory);
				strViewName = "addcatsettings";
			} else {

				strResponse = hubCitiService.addNewsCategories(newsCategory, user);

				if (null != strResponse && !"".equals(strResponse))

				{
					if (strResponse.equals(ApplicationConstants.SUCCESS)) {
						newsCategory.setCatfeedURL(null);
						// newsCategory.setSubCatURL(null);
						// newsCategory.setSubCatIds(null);
						newsCategory.setIsDefault(true);
						newsCategory.setCatName(null);
						newsCategory.setCatId(null);
						request.removeAttribute("hidCatName");
						request.removeAttribute("disptype");
						request.removeAttribute("hidsubpage");
						newsCategory.setCatColor("#000000");
						newsCategory.setCatFontColor("#ffffff");
						newsCategory.setBackButtonColor("#ffffff");
						newsCategory.setIsNewsTicker(false);
						newsCategory.setNoStories(null);
						newsCategory.setNonfeedCatName(null);
						newsCategory.setHidIsNewsTicker("newsno");
						request.setAttribute("addsuccessMsg", ApplicationConstants.SAVECATEGORYSUCCESSTEXT);
						// request.setAttribute("hidsubcatid", null);
						// request.setAttribute("hidsubcaturl", null);

					} else if (strResponse.equals("Exists")) {
						newsCategory.setIsDefault(isDefaultcat);
						newsCategory.setIsNewsTicker(isNewsTicker);
						request.setAttribute("catexistsMsg", ApplicationConstants.NEWSCATEEXISTS);
						// newsCategory.setSubCatURL(null);
						// newsCategory.setSubCatIds(null);
						// request.setAttribute("hidsubcatid", null);
						// request.setAttribute("hidsubcaturl", null);
					}
				} else {

					request.setAttribute("addfailureMsg", ApplicationConstants.ADDNEWSCATEGORYFAILURETEXT);
				}

			}

			// }
			if (null != newsCategory) {

				newsCategory.setLowerLimit(0);
				newsCategory.setCatName(null);
				// newsCategory.setIsDefault(isDefaultcat);
			}

			newssettings = hubCitiService.getNewsCategories(newsCategory, user);

			if (null != newssettings) {
				if (null != newssettings.getNewscatelst() && !newssettings.getNewscatelst().isEmpty()) {
					request.setAttribute("dispmngcat", "yes");
				}
			}

		} catch (HubCitiServiceException exception) {

			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		}

		model.put("addnewsform", newsCategory);
		log.info(ApplicationConstants.METHODEND + strMethodName);

		return strViewName;
	}

	/**
	 * Below method is used to fetch news categories.
	 * 
	 * @param newsCategory
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/fetchnewscats.htm", method = RequestMethod.POST)
	String fetchNewsCategories(@ModelAttribute("addnewsform") NewsCategory newsCategory, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws HubCitiServiceException {

		final String strMethodName = "fetchNewsCategories";
		final String strViewName = "newssettings";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		List<NewsCategory> newsCategoryLst = null;
		try {
			session.removeAttribute("newssettingslst");

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");
			session.setAttribute(ApplicationConstants.MENUNAME, ApplicationConstants.NEWSSETTINGS);
			User user = (User) session.getAttribute("loginUser");

			newsCategoryLst = hubCitiService.fetchNewsCategoryList(user);

			session.setAttribute("newscatlst", newsCategoryLst);

		} catch (HubCitiServiceException exception) {

			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		}

		log.info(ApplicationConstants.METHODEND + strMethodName);

		return strViewName;
	}

	/**
	 * Below Controller used to Category Information
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/editnewscategory.htm", method = { RequestMethod.GET, RequestMethod.POST })
	String editNewsCategory(@RequestParam("newsSettingsID") Integer newsSettingsId,
			@ModelAttribute("newssettings") NewsCategory newsCategoryreq, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws HubCitiServiceException {

		final String strMethodName = "editNewsCategory";
		final String strViewName = "editnewscat";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		NewsCategory newsCategory = null;
		List<NewsCategory> newsTypesLst = null;
		Boolean isNewsTicker = null;

		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

			User user = (User) session.getAttribute("loginUser");

			newsCategory = hubCitiService.fetchCategoryDetails(user.getHubCitiID(), newsSettingsId);

			if (null != newsCategory.getIsFeed() && newsCategory.getIsFeed() == false) {
				newsCategory.setNonfeedCatName(newsCategory.getCatName());
			}
			isNewsTicker = newsCategory.getIsNewsTicker();
			if (null != isNewsTicker && isNewsTicker == true) {
				newsCategory.setHidIsNewsTicker("newsyes");
			} else {

				newsCategory.setHidIsNewsTicker("newsno");
				newsCategory.setNoStories(null);

			}

			// fetching news category display type list...
			newsTypesLst = hubCitiService.fetchNewsTypesList(user);
			session.setAttribute("newstypelst", newsTypesLst);

			List<String> subPageTypes = new ArrayList<String>();
			if (null != newsCategory) {
				if ((null != newsCategory.getCatName() && newsCategory.getCatName().equalsIgnoreCase("photos"))
						|| (null != newsCategory.getCatName() && newsCategory.getCatName().equalsIgnoreCase("videos"))) {
					subPageTypes.add("Block View");
				} else {
					subPageTypes.add("Big Banner");
					subPageTypes.add("List View");
				}
			}
			session.setAttribute("subPageTypes", subPageTypes);
			session.setAttribute("displayTypes", newsTypesLst);
			session.setAttribute("newsCategory", newsCategory);

		} catch (HubCitiServiceException exception) {

			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}
		model.put("addnewsform", newsCategory);
		log.info(ApplicationConstants.METHODEND + strMethodName);
		return strViewName;
	}

	/**
	 * Below Controller used to update Category Information
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/updatenewscategory.htm", method = RequestMethod.POST)
	ModelAndView updateNewsCategory(@ModelAttribute("addnewsform") NewsCategory newsCategory, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws HubCitiServiceException {

		final String strMethodName = "editNewsCategory";
		final String strViewName = "managenewscats.htm";
		Boolean isValidURL = null;
		String resposeMessage = null;
		Boolean isNewsTicker = null;
		String isResponse = null;
		log.info(ApplicationConstants.METHODSTART + strMethodName);

		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

			
			if( null == newsCategory.getIsFeed() || newsCategory.getIsFeed() == true){
				isNewsTicker = newsCategory.getIsNewsTicker();
				if (null != isNewsTicker && isNewsTicker == true) {
					newsCategory.setHidIsNewsTicker("newsyes");
				} else {
					newsCategory.setHidIsNewsTicker("newsno");
					newsCategory.setNoStories(null);
				}
			}
			

			if (null != newsCategory.getHidCatName() && !"".equals(newsCategory.getHidCatName())) {

				newsCategory.setCatName(newsCategory.getHidCatName());
			}

			newsSettingsValidator.validateNewsCatUpdate(newsCategory, result);

			isValidURL = Utility.validateNewsURL(newsCategory.getCatfeedURL());
			if (!isValidURL) {
				newsSettingsValidator.validate(newsCategory, result, ApplicationConstants.INVALIDURL, "catfeedURL");
			}

			if (result.hasErrors()) {
				model.put("addnewsform", newsCategory);
				return new ModelAndView("editnewscat");
			}

			User user = (User) session.getAttribute("loginUser");
			isResponse = hubCitiService.updateNewsCategory(newsCategory, user);

			if (null != isResponse && ApplicationConstants.SUCCESS.equals(isResponse)) {
				resposeMessage = "News Category Updated Successfully";
			}else if (isResponse.equals("Exists")) {
					request.setAttribute("catnameexistsmsg", ApplicationConstants.NEWSNAMEEXISTS);
					model.put("addnewsform", newsCategory);
					return new ModelAndView("editnewscat");
			}
		} catch (HubCitiServiceException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);

		}
		model.put("addnewsform", newsCategory);
		log.info(ApplicationConstants.METHODEND + strMethodName);
		
		return new ModelAndView(new RedirectView(strViewName));
	}

	/**
	 * Below Controller used to News Sub Category Information
	 * 
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/fetchnewssubcat.htm", method = RequestMethod.GET)
	public @ResponseBody String fetchNewsSubCat(@RequestParam("newscatid") Integer newscatId,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws HubCitiServiceException {

		final String strMethodName = "fetchNewsSubCat";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		List<NewsCategory> newsTemplatesLst = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "";

		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

			session.removeAttribute("hCatId");

			session.setAttribute("hCatId", newscatId);
			User user = (User) session.getAttribute("loginUser");

			newsTemplatesLst = hubCitiService.fetchNewsSubCat(user.getHubCitiID(), newscatId);

			innerHtml.append(finalHtml);
			response.setContentType("text/xml");

			if (null != newsTemplatesLst && !newsTemplatesLst.isEmpty()) {
				for (NewsCategory category : newsTemplatesLst) {

					innerHtml.append("<tr class=\"\">");
					innerHtml.append("<td><div class=\"cell-wrp\" id=\"");
					innerHtml.append(category.getSubCatIds());
					innerHtml.append("\">");
					innerHtml.append(category.getSubCatName());
					innerHtml.append("</td>");

					innerHtml.append("<td>");
					innerHtml.append("<span class=\"errorDsply subcaterror\"></span>");
					innerHtml.append("<div class=\"cntrl-grp\">");
					innerHtml
							.append("<input style=\"height: 22px;\" class=\"inputTxtBig\" id=\"bannerImage\" type=\"text\"");
					if (null != category.getSubCatURL() && !category.getSubCatURL().equalsIgnoreCase("null")) {
						innerHtml.append(" value=\"");
						innerHtml.append(category.getSubCatURL());
						innerHtml.append("\"");
					}
					innerHtml.append("/>");
					innerHtml.append("</div></td>");
					innerHtml.append("</tr>");
				}
			} else {
				innerHtml.append("<tr class=\"\">");
				innerHtml.append("<td><div class=\"cell-wrp\">");
				innerHtml.append("Sub Categories are not exist");
				innerHtml.append("</td>");
				innerHtml.append("</tr>");
			}
		} catch (HubCitiServiceException exception) {

			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}
		log.info(ApplicationConstants.METHODEND + strMethodName);
		return innerHtml.toString();
	}

	/**
	 * Below method is used to save news sub categories to database.
	 * 
	 * @param newscatId
	 * @param subCatIds
	 * @param subCatUrls
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/savenewssubcat.htm", method = RequestMethod.GET)
	public @ResponseBody String saveNewsSubCat(@RequestParam("subcats") String subCatIds,
			@RequestParam("urls") String subCatUrls, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws HubCitiServiceException {

		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "";
		List<NewsCategory> newsTemplatesLst = null;
		Integer newsCatId = null;
		final String strMethodName = "saveNewsSubCat";
		log.info(ApplicationConstants.METHODSTART + strMethodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

		User user = (User) session.getAttribute("loginUser");

		newsCatId = (Integer) session.getAttribute("hCatId");

		Boolean isInserted = hubCitiService.saveNewsSubCat(user.getHubCitiID(), newsCatId, subCatIds, subCatUrls);

		newsTemplatesLst = hubCitiService.fetchNewsSubCat(user.getHubCitiID(), newsCatId);

		innerHtml.append(finalHtml);
		response.setContentType("text/xml");

		if (null != newsTemplatesLst && !newsTemplatesLst.isEmpty()) {
			for (NewsCategory category : newsTemplatesLst) {

				innerHtml.append("<tr class=\"\">");
				innerHtml.append("<td><div class=\"cell-wrp\" id=\"");
				innerHtml.append(category.getSubCatIds());
				innerHtml.append("\">");
				innerHtml.append(category.getSubCatName());
				innerHtml.append("</td>");

				innerHtml.append("<td>");
				innerHtml.append("<span class=\"errorDsply subcaterror\"></span>");
				innerHtml.append("<div class=\"cntrl-grp\">");
				innerHtml
						.append("<input style=\"height: 22px;\" class=\"inputTxtBig\" id=\"bannerImage\" type=\"text\"");
				if (null != category.getSubCatURL() && !category.getSubCatURL().equalsIgnoreCase("null")) {
					innerHtml.append(" value=\"");
					innerHtml.append(category.getSubCatURL());
					innerHtml.append("\"");
				}
				innerHtml.append("/>");
				innerHtml.append("</div></td>");
				innerHtml.append("</tr>");
			}
		} else {
			innerHtml.append("<tr class=\"\">");
			innerHtml.append("<td><div class=\"cell-wrp\">");
			innerHtml.append("Sub Categories are not exist");
			innerHtml.append("</td>");
			innerHtml.append("</tr>");
		}
		return innerHtml.toString();
	}

	/**
	 * This method is used to fetch sub categories.
	 * 
	 * @param newscatId
	 * @param subcatId
	 * @param subCaturl
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fetchnewsmainsubcat.htm", method = RequestMethod.GET)
	public @ResponseBody String fetchNewsMainSubCat(@RequestParam("newscatid") Integer newscatId,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws HubCitiServiceException {

		final String strMethodName = "fetchNewsSubCat";
		log.info(ApplicationConstants.METHODSTART + strMethodName);
		List<NewsCategory> newsTemplatesLst = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "";

		try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final HubCitiService hubCitiService = (HubCitiService) appContext.getBean("hubCitiService");

			User user = (User) session.getAttribute("loginUser");

			session.removeAttribute("hCatId");

			session.setAttribute("hCatId", newscatId);
			newsTemplatesLst = hubCitiService.fetchNewsSubCat(user.getHubCitiID(), newscatId);
			session.setAttribute("subcatlst", newsTemplatesLst);

			innerHtml.append(finalHtml);
			response.setContentType("text/xml");

			if (null != newsTemplatesLst && !newsTemplatesLst.isEmpty()) {
				for (NewsCategory category : newsTemplatesLst) {

					innerHtml.append("<tr class=\"\">");
					innerHtml.append("<td><div class=\"cell-wrp\" id=\"");
					innerHtml.append(category.getSubCatIds());
					innerHtml.append("\">");
					innerHtml.append(category.getSubCatName());
					innerHtml.append("</td>");

					innerHtml.append("<td>");
					innerHtml.append("<span class=\"errorDsply subcaterror\"></span>");
					innerHtml.append("<div class=\"cntrl-grp\">");
					innerHtml
							.append("<input style=\"height: 22px;\" class=\"inputTxtBig\" id=\"bannerImage\" type=\"text\"");

					if (null != category.getSubCatURL() && !category.getSubCatURL().equalsIgnoreCase("null")) {
						innerHtml.append(" value=\"");
						innerHtml.append(category.getSubCatURL());
						innerHtml.append("\"");

					}
					innerHtml.append("/>");
					innerHtml.append("</div></td>");
					innerHtml.append("</tr>");
				}
			} else {
				innerHtml.append("<tr class=\"\">");
				innerHtml.append("<td><div class=\"cell-wrp\">");
				innerHtml.append("Sub Categories are not exist");
				innerHtml.append("</td>");
				innerHtml.append("</tr>");

			}
		} catch (HubCitiServiceException exception) {

			log.error(ApplicationConstants.EXCEPTIONOCCURRED + exception.getMessage());
			throw new HubCitiServiceException(exception);
		}
		log.info(ApplicationConstants.METHODEND + strMethodName);
		return innerHtml.toString();
	}
}

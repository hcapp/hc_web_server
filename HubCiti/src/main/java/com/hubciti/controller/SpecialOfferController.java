package com.hubciti.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hubciti.common.exception.HubCitiServiceException;
import com.hubciti.common.pojo.ScreenSettings;

/**
 * Special Offer Build and Manage in HubCiti carried implementation from ScanSeeWeb.
 * 
 * @author kirankumar.garaddi
 *
 */
public interface SpecialOfferController {
	
	/**
	 * This controller used to fetch start Minutes 0-59
	 * 
	 * @return StartMinutes 
	 * @throws HubCitiServiceException
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("StartMinutes")
	public Map<String, String> populatemapDealStartMins() throws HubCitiServiceException;

	/**
	 * 
	 * This Controller used to fetch the hours in 24 format 0-23
	 * @return startHours
	 * @throws HubCitiServiceException
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("StartHours")
	public Map<String, String> populateDealStartHrs() throws HubCitiServiceException;
	
	/**
	 * 
	 * Controller for landing into the special offer Page.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/setupspecialofferscreen.htm", method = RequestMethod.GET)
	public String setupSpecialOfferScreen(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails,
			BindingResult result, HttpServletRequest request, HttpSession session, ModelMap model)
			throws HubCitiServiceException;
	
	/**
	 * 
	 * Controller to save Link to an existing special offer page Save.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/savespecialofferscreen.htm", method = RequestMethod.POST)
	public ModelAndView saveorUpdateSpecialOfferScreenDetails(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException;
	
	/**
	 * Display all special offers both Link to an existing Special offer Page and Make your Own Page.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/displayspecialofferpages.htm", method = RequestMethod.GET)
	public String displaySpecialOfferPages(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails,
			BindingResult result, HttpServletRequest request, HttpSession session, ModelMap model)
			throws HubCitiServiceException;

	/**
	 * 
	 * Make your own landing page.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/makeyourownspecialofferpage.htm", method = RequestMethod.GET)
	public String makeYourOwnSpecialOfferPage(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails,
			BindingResult result, HttpServletRequest request, HttpSession session, ModelMap model)
			throws HubCitiServiceException;

	/**
	 * 
	 * Build your Special offer Page.
	 * 
	 * @param sepcialOfferPageDetails
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/buildspecialofferpage.htm", method = RequestMethod.GET)
	public ModelAndView buildSpecialOfferPage(
			@ModelAttribute("screenSettingsForm") ScreenSettings sepcialOfferPageDetails, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws HubCitiServiceException;
	
	/**
	 * 
	 * Save or update Make your own special offer Page.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/savemakeyourownspecialofferpage.htm", method = RequestMethod.POST)
	public ModelAndView saveorupdateMakeYourOwnSpecialOfferPage(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException;

	/**
	 * 
	 * Delete Special Offer Page
	 * 
	 * @param specialOfferDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/deletespecialoffer.htm", method = RequestMethod.POST)
	public String deleteSpecialOfferPage(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferDetails,
			BindingResult result, HttpServletRequest request, ModelMap model, HttpSession session)
			throws HubCitiServiceException;
	
	/**
	 * 
	 * Edit Special Offer Page Screen link to an existing page. 
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/editspecialofferscreen.htm", method = RequestMethod.POST)
	public String editSpecialOfferScreen(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws HubCitiServiceException;
	
	/**
	 * 
	 * Landing into Edit Screen make your own page.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/editmakeyourownspecailofferpage.htm", method = RequestMethod.POST)
	public String editMakeYourOwnScreen(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws HubCitiServiceException;
	
	/**
	 * 
	 * UpDate Special Offer Link to an existing Page.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/updatespecialofferscreen.htm", method = RequestMethod.POST)
	public ModelAndView updateSpecialOfferScreenDetails(
			@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException;
			
	/**
	 * 
	 *  Update Make your own page.
	 * 
	 * @param specialOfferPageDetails
	 * @param result
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@RequestMapping(value = "/updatemakeyourownspecialofferpage.htm", method = RequestMethod.POST)
	public ModelAndView updateMakeYourOwnSpecialOfferPage(@ModelAttribute("screenSettingsForm") ScreenSettings specialOfferPageDetails, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws HubCitiServiceException;
	
	/**
	 * 
	 * Making Special offer as Featured.  
	 * 
	 * @param isfeatured
	 * @param pageid
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws HubCitiServiceException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/savefeatured.htm", method = RequestMethod.POST)
	@ResponseBody
	public String savefeatured(@RequestParam(value = "isfeatured", required = true) Integer isfeatured,@RequestParam(value = "pageid", required = true) Integer pageid,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws HubCitiServiceException;
	
}

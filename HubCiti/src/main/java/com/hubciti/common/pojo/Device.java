package com.hubciti.common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class Device {

	private String deviceId;
	
	private String platform;
	
	private String hcName;
	
	private Integer badgeCount;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getHcName() {
		return hcName;
	}

	public void setHcName(String hcName) {
		this.hcName = hcName;
	}

	/**
	 * @return the badgeCount
	 */
	public Integer getBadgeCount() {
		return badgeCount;
	}

	/**
	 * @param badgeCount the badgeCount to set
	 */
	public void setBadgeCount(Integer badgeCount) {
		this.badgeCount = badgeCount;
	}
	
}

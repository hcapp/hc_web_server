/**
 * 
 */
package com.hubciti.common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author sangeetha.ts
 *
 */
public class Logistics {
	
	private Integer logisticId;
	
	private String logisticName;
	
	private String logisticImageName;
	
	private String oldLogisticImageName;
	
	private CommonsMultipartFile logisticImageFile;
	
	private String logisticImagePath;
	
	private String retailerName;
	
	private String retailerId;
	
	private String location;
	
	private String locationId;
	
	private String hiddenLocationId;
	
	private String startDate;
	
	private String endDate;
	
	private String searchLogisticName;
	
	private Integer lowerLimit;
	
	private String markerId;
	
	private String markerName;
	
	private String markerImageName;
	
	private String markerImagePath;
	
	private CommonsMultipartFile markerImageFile;
	
	private Double markerLatitude;
	
	private Double markerLongitude;
	
	private String functionality;
	
	private String latitude;
	
	private String longitude;
	
	private String mapLink;
	
	private String anythingPageId;
	
	private String hiddenAnythingPageId;
	
	private String isPortrtOrLandscp;
	
	private String addMarker;
	
	/**
	 * @return the logisticId
	 */
	public Integer getLogisticId() {
		return logisticId;
	}

	/**
	 * @param logisticId the logisticId to set
	 */
	public void setLogisticId(Integer logisticId) {
		this.logisticId = logisticId;
	}

	/**
	 * @return the logisticName
	 */
	public String getLogisticName() {
		return logisticName;
	}

	/**
	 * @param logisticName the logisticName to set
	 */
	public void setLogisticName(String logisticName) {
		this.logisticName = logisticName;
	}

	/**
	 * @return the logisticImageName
	 */
	public String getLogisticImageName() {
		return logisticImageName;
	}

	/**
	 * @param logisticImageName the logisticImageName to set
	 */
	public void setLogisticImageName(String logisticImageName) {
		this.logisticImageName = logisticImageName;
	}

	/**
	 * @return the logisticImagePath
	 */
	public String getLogisticImagePath() {
		return logisticImagePath;
	}

	/**
	 * @param logisticImagePath the logisticImagePath to set
	 */
	public void setLogisticImagePath(String logisticImagePath) {
		this.logisticImagePath = logisticImagePath;
	}

	/**
	 * @return the retailerName
	 */
	public String getRetailerName() {
		return retailerName;
	}

	/**
	 * @param retailerName the retailerName to set
	 */
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	/**
	 * @return the retailerId
	 */
	public String getRetailerId() {
		return retailerId;
	}

	/**
	 * @param retailerId the retailerId to set
	 */
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the logisticImageFile
	 */
	public CommonsMultipartFile getLogisticImageFile() {
		return logisticImageFile;
	}

	/**
	 * @param logisticImageFile the logisticImageFile to set
	 */
	public void setLogisticImageFile(CommonsMultipartFile logisticImageFile) {
		this.logisticImageFile = logisticImageFile;
	}

	/**
	 * @return the searchLogisticName
	 */
	public String getSearchLogisticName() {
		return searchLogisticName;
	}

	/**
	 * @param searchLogisticName the searchLogisticName to set
	 */
	public void setSearchLogisticName(String searchLogisticName) {
		this.searchLogisticName = searchLogisticName;
	}

	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit() {
		return lowerLimit;
	}

	/**
	 * @param lowerLimit the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @return the markerId
	 */
	public String getMarkerId() {
		return markerId;
	}

	/**
	 * @param markerId the markerId to set
	 */
	public void setMarkerId(String markerId) {
		this.markerId = markerId;
	}

	/**
	 * @return the markerName
	 */
	public String getMarkerName() {
		return markerName;
	}

	/**
	 * @param markerName the markerName to set
	 */
	public void setMarkerName(String markerName) {
		this.markerName = markerName;
	}

	/**
	 * @return the markerImageName
	 */
	public String getMarkerImageName() {
		return markerImageName;
	}

	/**
	 * @param markerImageName the markerImageName to set
	 */
	public void setMarkerImageName(String markerImageName) {
		this.markerImageName = markerImageName;
	}

	/**
	 * @return the markerImagePath
	 */
	public String getMarkerImagePath() {
		return markerImagePath;
	}

	/**
	 * @param markerImagePath the markerImagePath to set
	 */
	public void setMarkerImagePath(String markerImagePath) {
		this.markerImagePath = markerImagePath;
	}

	/**
	 * @return the markerImageFile
	 */
	public CommonsMultipartFile getMarkerImageFile() {
		return markerImageFile;
	}

	/**
	 * @param markerImageFile the markerImageFile to set
	 */
	public void setMarkerImageFile(CommonsMultipartFile markerImageFile) {
		this.markerImageFile = markerImageFile;
	}

	/**
	 * @return the markerLatitude
	 */
	public Double getMarkerLatitude() {
		return markerLatitude;
	}

	/**
	 * @param markerLatitude the markerLatitude to set
	 */
	public void setMarkerLatitude(Double markerLatitude) {
		this.markerLatitude = markerLatitude;
	}

	/**
	 * @return the markerLongitude
	 */
	public Double getMarkerLongitude() {
		return markerLongitude;
	}

	/**
	 * @param markerLongitude the markerLongitude to set
	 */
	public void setMarkerLongitude(Double markerLongitude) {
		this.markerLongitude = markerLongitude;
	}

	/**
	 * @return the functionality
	 */
	public String getFunctionality() {
		return functionality;
	}

	/**
	 * @param functionality the functionality to set
	 */
	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}

	/**
	 * @return the hiddenLocationId
	 */
	public String getHiddenLocationId() {
		return hiddenLocationId;
	}

	/**
	 * @param hiddenLocationId the hiddenLocationId to set
	 */
	public void setHiddenLocationId(String hiddenLocationId) {
		this.hiddenLocationId = hiddenLocationId;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the oldLogisticImageName
	 */
	public String getOldLogisticImageName() {
		return oldLogisticImageName;
	}

	/**
	 * @param oldLogisticImageName the oldLogisticImageName to set
	 */
	public void setOldLogisticImageName(String oldLogisticImageName) {
		this.oldLogisticImageName = oldLogisticImageName;
	}

	/**
	 * @return the mapLink
	 */
	public String getMapLink() {
		return mapLink;
	}

	/**
	 * @param mapLink the mapLink to set
	 */
	public void setMapLink(String mapLink) {
		this.mapLink = mapLink;
	}

	public String getAnythingPageId() {
		return anythingPageId;
	}

	public void setAnythingPageId(String anythingPageId) {
		this.anythingPageId = anythingPageId;
	}

	public String getHiddenAnythingPageId() {
		return hiddenAnythingPageId;
	}

	public void setHiddenAnythingPageId(String hiddenAnythingPageId) {
		this.hiddenAnythingPageId = hiddenAnythingPageId;
	}

	public String getIsPortrtOrLandscp() {
		return isPortrtOrLandscp;
	}

	public void setIsPortrtOrLandscp(String isPortrtOrLandscp) {
		this.isPortrtOrLandscp = isPortrtOrLandscp;
	}

	public String getAddMarker() {
		return addMarker;
	}

	public void setAddMarker(String addMarker) {
		this.addMarker = addMarker;
	}

	
	
}

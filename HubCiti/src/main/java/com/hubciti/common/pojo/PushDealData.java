package com.hubciti.common.pojo;

import java.util.List;

public class PushDealData {
	
	
	private List<Deals> dealList;
	
	private String link;
	
	private String response;
	
	private List<Device> deviceIdList;
	
	private String notiMgs;
	
	private Integer dealStatusNo;
	


	public List<Deals> getDealList() {
		return dealList;
	}

	public void setDealList(List<Deals> dealList) {
		this.dealList = dealList;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	public List<Device> getDeviceIdList() {
		return deviceIdList;
	}

	public void setDeviceIdList(List<Device> deviceIdList) {
		this.deviceIdList = deviceIdList;
	}

	public String getNotiMgs() {
		return notiMgs;
	}

	public void setNotiMgs(String notiMgs) {
		this.notiMgs = notiMgs;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Integer getDealStatusNo() {
		return dealStatusNo;
	}

	public void setDealStatusNo(Integer dealStatusNo) {
		this.dealStatusNo = dealStatusNo;
	}

}

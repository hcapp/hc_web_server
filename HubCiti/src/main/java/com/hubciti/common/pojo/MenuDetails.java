/**
 * 
 */
package com.hubciti.common.pojo;

import java.util.List;

/**
 * @author sangeetha.ts
 */
public class MenuDetails {
	/**
	 * variable for menu level.
	 */
	Integer level;
	/**
	 * variable for menu menuName.
	 */
	String menuName;
	/**
	 * variable for menu Type Id.
	 */
	private Integer menuId;

	/**
	 * variable for menu Type Id.
	 */
	private Integer menuTypeId;
	/**
	 * variable for menu Type Name.
	 */
	private String menuTypeName;
	/**
	 * variable for menu position.
	 */
	private Integer position;

	private ButtonDetails buttonDetails;

	private String bannerImg;
	/**
	 * @return the level
	 */
	private String bannerImageName;
	private String strBottomBtnId;

	private String menuTypeVal;

	private boolean typeFlag;

	private boolean departmentFlag;

	private Integer noOfColumns;

	private String comboBtnType;

	private String tmpltBckgrdColor;

	private String tmpltBckgrdImg;

	private String tmplBckGrdImageName;

	private Boolean displayBtnLabel;

	private String labelBckGndColor;

	private String labelFontColor;

	private Boolean isSideMenu;

	private Boolean isDefault;
	
	private Integer isBannerOrTicker;
	
	private Integer isScrollOrRotate;
	
	private String direction;
	
	private String rtDirection;
	

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsSideMenu() {
		return isSideMenu;
	}

	public void setIsSideMenu(Boolean isSideMenu) {
		this.isSideMenu = isSideMenu;
	}

	/**
	 * Added for News Template
	 * 
	 */
	private String newsBanner;

	private String newsSubCatIds;

	private String newsSubCate;

        private String tempName;

	public Boolean getIsNewsTemp() {
		return isNewsTemp;
	}

	public void setIsNewsTemp(Boolean isNewsTemp) {
		this.isNewsTemp = isNewsTemp;
	}

	private Boolean isNewsTemp;

	/**
	 * 
	 * @return the News Banner Image Path
	 */
	public String getNewsBanner() {
		return newsBanner;
	}

	/*
	 * Set news Banner Image
	 */
	public void setNewsBanner(String newsBanner) {
		this.newsBanner = newsBanner;
	}

	public Integer getLevel() {
		return level;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * @return the menuName
	 */
	public String getMenuName() {
		return menuName;
	}

	/**
	 * @param menuName
	 *            the menuName to set
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	/**
	 * variable for buttons.
	 */
	private List<ButtonDetails> buttons;

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	/**
	 * retrieves the value of menuTypeName.
	 * 
	 * @return the menuTypeName
	 */
	public String getMenuTypeName() {
		return menuTypeName;
	}

	/**
	 * Set the value for menuTypeName.
	 * 
	 * @param menuTypeName
	 *            the menuTypeName to set
	 */
	public void setMenuTypeName(String menuTypeName) {
		this.menuTypeName = menuTypeName;
	}

	/**
	 * retrieves the value of buttons.
	 * 
	 * @return the buttons
	 */
	public List<ButtonDetails> getButtons() {
		return buttons;
	}

	/**
	 * Set the value for buttons.
	 * 
	 * @param buttons
	 *            the buttons to set
	 */
	public void setButtons(List<ButtonDetails> buttons) {
		this.buttons = buttons;
	}

	/**
	 * @param position
	 *            the position to set
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}

	/**
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	public ButtonDetails getButtonDetails() {
		return buttonDetails;
	}

	public void setButtonDetails(ButtonDetails buttonDetails) {
		this.buttonDetails = buttonDetails;
	}

	public Integer getMenuTypeId() {
		return menuTypeId;
	}

	public void setMenuTypeId(Integer menuTypeId) {
		this.menuTypeId = menuTypeId;
	}

	public String getBannerImg() {
		return bannerImg;
	}

	public void setBannerImg(String bannerImg) {
		this.bannerImg = bannerImg;
	}

	public String getStrBottomBtnId() {
		return strBottomBtnId;
	}

	public void setStrBottomBtnId(String strBottomBtnId) {
		this.strBottomBtnId = strBottomBtnId;
	}

	public String getBannerImageName() {
		return bannerImageName;
	}

	public void setBannerImageName(String bannerImageName) {
		this.bannerImageName = bannerImageName;
	}

	public String getMenuTypeVal() {
		return menuTypeVal;
	}

	public void setMenuTypeVal(String menuTypeVal) {
		this.menuTypeVal = menuTypeVal;
	}

	public boolean isTypeFlag() {
		return typeFlag;
	}

	public void setTypeFlag(boolean typeFlag) {
		this.typeFlag = typeFlag;
	}

	public boolean isDepartmentFlag() {
		return departmentFlag;
	}

	public void setDepartmentFlag(boolean departmentFlag) {
		this.departmentFlag = departmentFlag;
	}

	public Integer getNoOfColumns() {
		return noOfColumns;
	}

	public void setNoOfColumns(Integer noOfColumns) {
		this.noOfColumns = noOfColumns;
	}

	/**
	 * @return the comboBtnType
	 */
	public String getComboBtnType() {
		return comboBtnType;
	}

	/**
	 * @param comboBtnType
	 *            the comboBtnType to set
	 */
	public void setComboBtnType(String comboBtnType) {
		this.comboBtnType = comboBtnType;
	}

	/**
	 * @return the tmpltBckgrdColor
	 */
	public String getTmpltBckgrdColor() {
		return tmpltBckgrdColor;
	}

	/**
	 * @param tmpltBckgrdColor
	 *            the tmpltBckgrdColor to set
	 */
	public void setTmpltBckgrdColor(String tmpltBckgrdColor) {
		this.tmpltBckgrdColor = tmpltBckgrdColor;
	}

	/**
	 * @return the tmpltBckgrdImg
	 */
	public String getTmpltBckgrdImg() {
		return tmpltBckgrdImg;
	}

	/**
	 * @param tmpltBckgrdImg
	 *            the tmpltBckgrdImg to set
	 */
	public void setTmpltBckgrdImg(String tmpltBckgrdImg) {
		this.tmpltBckgrdImg = tmpltBckgrdImg;
	}

	/**
	 * @return the tmplBckGrdImageName
	 */
	public String getTmplBckGrdImageName() {
		return tmplBckGrdImageName;
	}

	/**
	 * @param tmplBckGrdImageName
	 *            the tmplBckGrdImageName to set
	 */
	public void setTmplBckGrdImageName(String tmplBckGrdImageName) {
		this.tmplBckGrdImageName = tmplBckGrdImageName;
	}

	/**
	 * @return the displayBtnLabel
	 */
	public Boolean getDisplayBtnLabel() {
		return displayBtnLabel;
	}

	/**
	 * @param displayBtnLabel
	 *            the displayBtnLabel to set
	 */
	public void setDisplayBtnLabel(Boolean displayBtnLabel) {
		this.displayBtnLabel = displayBtnLabel;
	}

	/**
	 * @return the labelBckGndColor
	 */
	public String getLabelBckGndColor() {
		return labelBckGndColor;
	}

	/**
	 * @param labelBckGndColor
	 *            the labelBckGndColor to set
	 */
	public void setLabelBckGndColor(String labelBckGndColor) {
		this.labelBckGndColor = labelBckGndColor;
	}

	/**
	 * @return the labelFontColor
	 */
	public String getLabelFontColor() {
		return labelFontColor;
	}

	/**
	 * @param labelFontColor
	 *            the labelFontColor to set
	 */
	public void setLabelFontColor(String labelFontColor) {
		this.labelFontColor = labelFontColor;
	}

	public String getNewsSubCatIds() {
		return newsSubCatIds;
	}

	public void setNewsSubCatIds(String newsSubCatIds) {
		this.newsSubCatIds = newsSubCatIds;
	}

	public String getNewsSubCate() {
		return newsSubCate;
	}

	public void setNewsSubCate(String newsSubCate) {
		this.newsSubCate = newsSubCate;
	}


	public Integer getIsBannerOrTicker() {
		return isBannerOrTicker;
	}

	public void setIsBannerOrTicker(Integer isBannerOrTicker) {
		this.isBannerOrTicker = isBannerOrTicker;
	}

	public Integer getIsScrollOrRotate() {
		return isScrollOrRotate;
	}

	public void setIsScrollOrRotate(Integer isScrollOrRotate2) {
		this.isScrollOrRotate = isScrollOrRotate2;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getRtDirection() {
		return rtDirection;
	}

	public void setRtDirection(String rtDirection) {
		this.rtDirection = rtDirection;
	}
	
	
	



	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

}

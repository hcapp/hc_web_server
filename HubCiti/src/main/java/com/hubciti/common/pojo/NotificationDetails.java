package com.hubciti.common.pojo;

import java.util.List;

/**
 * @author kumar_dodda
 *
 */
public class NotificationDetails {

	
	private String collapse_key;
	private PushDealData data;
	private List<String> registration_ids;
	private String link;

	public String getCollapse_key() {
		return collapse_key;
	}

	public void setCollapse_key(String collapse_key) {
		this.collapse_key = collapse_key;
	}

	public PushDealData getData() {
		return data;
	}

	public void setData(PushDealData data) {
		this.data = data;
	}
	
	public List<String> getRegistration_ids() {
		return registration_ids;
	}

	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}

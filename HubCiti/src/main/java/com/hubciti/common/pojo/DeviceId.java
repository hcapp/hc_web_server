package com.hubciti.common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class DeviceId {

	private String deviceId;
	
	private String platform;
	
	private String hcName;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getHcName() {
		return hcName;
	}

	public void setHcName(String hcName) {
		this.hcName = hcName;
	}
	
}

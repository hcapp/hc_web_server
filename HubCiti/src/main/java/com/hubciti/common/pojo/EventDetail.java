package com.hubciti.common.pojo;

import java.util.List;

public class EventDetail {

	private List<Event> eventLst;
	private Integer totalSize;
	private String eventStartDate;
	private String eventEndDate;

	public String getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public String getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public List<Event> getEventLst() {
		return eventLst;
	}

	public void setEventLst(List<Event> eventLst) {
		this.eventLst = eventLst;
	}

	public Integer getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}
}

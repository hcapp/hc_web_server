/**
 * 
 */
package com.hubciti.common.pojo;

/**
 * @author sangeetha.ts
 *
 */
public class Deals {
	/**
	 * variable for deal ID
	 */
	private Integer dealId;
	/**
	 * variable for deal Name
	 */
	private String dealName;
	/**
	 * variable for deal Description
	 */
	private String dealDescription;
	/**
	 * variable for price
	 */
	private Double price;
	/**
	 * variable for sale Price
	 */
	private Double salePrice;
	/**
	 * variable for start Date
	 */
	private String startDate;
	/**
	 * variable for end Date
	 */
	private String endDate;
	/**
	 * variable for lower Limit
	 */
	private Integer lowerLimit;
	/**
	 * variable for deal SearchKey
	 */
	private String dealSearchKey;
	/**
	 * variable for pushNotification
	 */
	private String pushNotification;
	/**
	 * variable for start Date
	 */
	private String pStartDate;
	/**
	 * variable for end Date
	 */
	private String pEndDate;

	/**
	 * variable for sEndDate
	 */
	private String sStartDate;
	/**
	 * variable for sEndDate
	 */
	private String sEndDate;

	private String retailerId;
	private String retailLocationId;
	/**
	 * 
	 */
	private boolean pushFlag;

	private String type;
	private String splUrl;
	private String hcName;

	private Integer isFeatured;

	private Integer isEnded;

	public Integer getIsEnded() {
		return isEnded;
	}

	public void setIsEnded(Integer isEnded) {
		this.isEnded = isEnded;
	}

	public Integer getIsFeatured() {
		return isFeatured;
	}

	public void setIsFeatured(Integer isFeatured) {
		this.isFeatured = isFeatured;
	}

	/**
	 * variable for end Time
	 */
	private String endTime;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSplUrl() {
		return splUrl;
	}

	public void setSplUrl(String splUrl) {
		this.splUrl = splUrl;
	}

	public String getHcName() {
		return hcName;
	}

	public void setHcName(String hcName) {
		this.hcName = hcName;
	}

	/**
	 * variable for deal SearchKey
	 */
	public Integer getDealId() {
		return dealId;
	}

	/**
	 * @param dealId
	 *            the dealId to set
	 */
	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	/**
	 * @return the dealName
	 */
	public String getDealName() {
		return dealName;
	}

	/**
	 * @param dealName
	 *            the dealName to set
	 */
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	/**
	 * @return the dealDescription
	 */
	public String getDealDescription() {
		return dealDescription;
	}

	/**
	 * @param dealDescription
	 *            the dealDescription to set
	 */
	public void setDealDescription(String dealDescription) {
		this.dealDescription = dealDescription;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice() {
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit() {
		return lowerLimit;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @return the dealSearchKey
	 */
	public String getDealSearchKey() {
		return dealSearchKey;
	}

	/**
	 * @param dealSearchKey
	 *            the dealSearchKey to set
	 */
	public void setDealSearchKey(String dealSearchKey) {
		this.dealSearchKey = dealSearchKey;
	}

	public String getPushNotification() {
		return pushNotification;
	}

	public void setPushNotification(String pushNotification) {
		this.pushNotification = pushNotification;
	}

	public String getpStartDate() {
		return pStartDate;
	}

	public void setpStartDate(String pStartDate) {
		this.pStartDate = pStartDate;
	}

	public String getpEndDate() {
		return pEndDate;
	}

	public void setpEndDate(String pEndDate) {
		this.pEndDate = pEndDate;
	}

	public String getsStartDate() {
		return sStartDate;
	}

	public void setsStartDate(String sStartDate) {
		this.sStartDate = sStartDate;
	}

	public String getsEndDate() {
		return sEndDate;
	}

	public void setsEndDate(String sEndDate) {
		this.sEndDate = sEndDate;
	}

	public boolean isPushFlag() {
		return pushFlag;
	}

	public void setPushFlag(boolean pushFlag) {
		this.pushFlag = pushFlag;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	public String getRetailLocationId() {
		return retailLocationId;
	}

	public void setRetailLocationId(String retailLocationId) {
		this.retailLocationId = retailLocationId;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}

package com.hubciti.common.pojo;

import java.util.List;

public class NewsSettings {
	
	private Integer totalSize;
	private List<NewsCategory> newscatelst;
	
	private List<Coupon> couponlst;
	public Integer getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}
	public List<NewsCategory> getNewscatelst() {
		return newscatelst;
	}
	public void setNewscatelst(List<NewsCategory> newscatelst) {
		this.newscatelst = newscatelst;
	}
	public List<Coupon> getCouponlst() {
		return couponlst;
	}
	public void setCouponlst(List<Coupon> couponlst) {
		this.couponlst = couponlst;
	}

}

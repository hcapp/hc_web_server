package com.hubciti.common.pojo;

import java.util.List;

public class SpecialOfferPages {

	/**
	 * 
	 */
	private List<SpecialOfferPageDetails> pageDetails;
	/**
	 * 
	 */
	private Integer totalSize;

	/**
	 * @return the pageDetails
	 */
	public List<SpecialOfferPageDetails> getPageDetails() {
		return pageDetails;
	}

	/**
	 * @param pageDetails
	 *            the pageDetails to set
	 */
	public void setPageDetails(List<SpecialOfferPageDetails> pageDetails) {
		this.pageDetails = pageDetails;
	}

	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize() {
		return totalSize;
	}

	/**
	 * @param totalSize
	 *            the totalSize to set
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}
}

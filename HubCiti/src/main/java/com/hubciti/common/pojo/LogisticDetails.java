/**
 * 
 */
package com.hubciti.common.pojo;

import java.util.ArrayList;

/**
 * @author sangeetha.ts
 *
 */
public class LogisticDetails {
	
	private ArrayList<Logistics> logistics;

	private Integer totalSize;

	/**
	 * @return the logistics
	 */
	public ArrayList<Logistics> getLogistics() {
		return logistics;
	}

	/**
	 * @param logistics the logistics to set
	 */
	public void setLogistics(ArrayList<Logistics> logistics) {
		this.logistics = logistics;
	}

	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize() {
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

}

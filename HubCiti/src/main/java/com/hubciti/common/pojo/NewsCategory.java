package com.hubciti.common.pojo;

import java.util.ArrayList;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * This POJO is used to manage News settings categories data.
 * 
 * @author shyamsundara_hm
 * 
 */
public class NewsCategory {

	private String catName;
	private String catfeedURL;
	private String catColor;
	private String displayType;
	private String catSearchKey;
	private Integer lowerLimit;
	private String sortOder;
	private Integer newsSettingsID;
	private Integer catId;
	private String tempName;
	private String bannerImg;
	private String ThumnailPos;
	private String displayTypId;
	private Integer newsTempleteID;
	private String newsImage;
	private CommonsMultipartFile imageFile;
	private String uploadNewsImageType;
	private CommonsMultipartFile imageNewsFile;
	private String newsTempName;
	private String weatherURL;

	private Boolean isNewsTicker;
	private Integer noStories;
	private Boolean isSubCatFlag;

	private String displayTypeName;

	private Integer displayTypeId;

	private String subPageName;

	private String subPageId;

	private Integer subCatId;

	private String subCatIds;

	private String subCatName;

	private String subCatURL;

	private String hidCatName;

	private String hidDisplaytype;

	private String hidsubpage;

	private ArrayList<SubCategory> subArrayList;
	private String methodType;

	private String hidsubcatid;

	private String hidsubcaturl;

	private Boolean isSubCategoryAdded;

	private Boolean isDefault;

	private String hidIsNewsTicker;

	private String catFontColor;

	private String newsCatIds;

	private String BackButtonColor;

	private Boolean isFeed;

	private String nonfeedCatName;
	
	
	public String getNonfeedCatName() {
		return nonfeedCatName;
	}

	public void setNonfeedCatName(String nonfeedCatName) {
		this.nonfeedCatName = nonfeedCatName;
	}

	public Boolean getIsFeed() {
		return isFeed;
	}

	public void setIsFeed(Boolean isFeed) {
		this.isFeed = isFeed;
	}

	public String getNewsCatIds() {
		return newsCatIds;
	}

	public void setNewsCatIds(String newsCatIds) {
		this.newsCatIds = newsCatIds;
	}

	public String getCatFontColor() {
		return catFontColor;
	}

	public void setCatFontColor(String catFontColor) {
		this.catFontColor = catFontColor;
	}

	public Integer getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Integer subCatId) {
		this.subCatId = subCatId;
	}

	public String getSubCatURL() {
		return subCatURL;
	}

	public void setSubCatURL(String subCatURL) {
		this.subCatURL = subCatURL;
	}

	public Boolean getIsSubCatFlag() {
		return isSubCatFlag;
	}

	public void setIsSubCatFlag(Boolean isSubCatFlag) {
		this.isSubCatFlag = isSubCatFlag;
	}

	public String getDisplayTypeName() {
		return displayTypeName;
	}

	public void setDisplayTypeName(String displayTypeName) {
		this.displayTypeName = displayTypeName;
	}

	public Integer getDisplayTypeId() {
		return displayTypeId;
	}

	public void setDisplayTypeId(Integer displayTypeId) {
		this.displayTypeId = displayTypeId;
	}

	private String bannerImgPath;
	private String newsImgPath;
	private String response;

	private Integer templateId;

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getCatfeedURL() {
		return catfeedURL;
	}

	public void setCatfeedURL(String catfeedURL) {
		this.catfeedURL = catfeedURL;
	}

	public String getCatColor() {
		return catColor;
	}

	public void setCatColor(String catColor) {
		this.catColor = catColor;
	}

	public String getDisplayType() {
		return displayType;
	}

	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}

	public String getCatSearchKey() {
		return catSearchKey;
	}

	public void setCatSearchKey(String catSearchKey) {
		this.catSearchKey = catSearchKey;
	}

	public Integer getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public String getSortOder() {
		return sortOder;
	}

	public void setSortOder(String sortOder) {
		this.sortOder = sortOder;
	}

	public Integer getNewsSettingsID() {
		return newsSettingsID;
	}

	public void setNewsSettingsID(Integer newsSettingsID) {
		this.newsSettingsID = newsSettingsID;
	}

	public Integer getCatId() {
		return catId;
	}

	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public String getBannerImg() {
		return bannerImg;
	}

	public void setBannerImg(String bannerImg) {
		this.bannerImg = bannerImg;
	}

	public String getThumnailPos() {
		return ThumnailPos;
	}

	public void setThumnailPos(String thumnailPos) {
		ThumnailPos = thumnailPos;
	}

	public Integer getNewsTempleteID() {
		return newsTempleteID;
	}

	public void setNewsTempleteID(Integer newsTempleteID) {
		this.newsTempleteID = newsTempleteID;
	}

	public String getNewsImage() {
		return newsImage;
	}

	public void setNewsImage(String newsImage) {
		this.newsImage = newsImage;
	}

	public String getUploadNewsImageType() {
		return uploadNewsImageType;
	}

	public void setUploadNewsImageType(String uploadNewsImageType) {
		this.uploadNewsImageType = uploadNewsImageType;
	}

	public CommonsMultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(CommonsMultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public CommonsMultipartFile getImageNewsFile() {
		return imageNewsFile;
	}

	public void setImageNewsFile(CommonsMultipartFile imageNewsFile) {
		this.imageNewsFile = imageNewsFile;
	}

	public String getNewsTempName() {
		return newsTempName;
	}

	public void setNewsTempName(String newsTempName) {
		this.newsTempName = newsTempName;
	}

	public String getWeatherURL() {
		return weatherURL;
	}

	public void setWeatherURL(String weatherURL) {
		this.weatherURL = weatherURL;
	}

	public String getBannerImgPath() {
		return bannerImgPath;
	}

	public void setBannerImgPath(String bannerImgPath) {
		this.bannerImgPath = bannerImgPath;
	}

	public String getNewsImgPath() {
		return newsImgPath;
	}

	public void setNewsImgPath(String newsImgPath) {
		this.newsImgPath = newsImgPath;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getSubPageName() {
		return subPageName;
	}

	public void setSubPageName(String subPageName) {
		this.subPageName = subPageName;
	}

	public String getSubCatName() {
		return subCatName;
	}

	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}

	public String getHidCatName() {
		return hidCatName;
	}

	public void setHidCatName(String hidCatName) {
		this.hidCatName = hidCatName;
	}

	public String getHidDisplaytype() {
		return hidDisplaytype;
	}

	public void setHidDisplaytype(String hidDisplaytype) {
		this.hidDisplaytype = hidDisplaytype;
	}

	public String getDisplayTypId() {
		return displayTypId;
	}

	public void setDisplayTypId(String displayTypId) {
		this.displayTypId = displayTypId;
	}

	public String getSubPageId() {
		return subPageId;
	}

	public void setSubPageId(String subPageId) {
		this.subPageId = subPageId;
	}

	public String getHidsubpage() {
		return hidsubpage;
	}

	public void setHidsubpage(String hidsubpage) {
		this.hidsubpage = hidsubpage;
	}

	public ArrayList<SubCategory> getSubArrayList() {
		return subArrayList;
	}

	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	public String getHidsubcatid() {
		return hidsubcatid;
	}

	public void setHidsubcatid(String hidsubcatid) {
		this.hidsubcatid = hidsubcatid;
	}

	public String getHidsubcaturl() {
		return hidsubcaturl;
	}

	public void setHidsubcaturl(String hidsubcaturl) {
		this.hidsubcaturl = hidsubcaturl;
	}

	public Boolean getIsSubCategoryAdded() {
		return isSubCategoryAdded;
	}

	public void setIsSubCategoryAdded(Boolean isSubCategoryAdded) {
		this.isSubCategoryAdded = isSubCategoryAdded;
	}

	public String getSubCatIds() {
		return subCatIds;
	}

	public void setSubCatIds(String subCatIds) {
		this.subCatIds = subCatIds;
	}

	public void setSubArrayList(ArrayList<SubCategory> subArrayList) {
		this.subArrayList = subArrayList;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsNewsTicker() {
		return isNewsTicker;
	}

	public void setIsNewsTicker(Boolean isNewsTicker) {
		this.isNewsTicker = isNewsTicker;
	}

	public Integer getNoStories() {
		return noStories;
	}

	public void setNoStories(Integer noStories) {
		this.noStories = noStories;
	}

	public String getHidIsNewsTicker() {
		return hidIsNewsTicker;
	}

	public void setHidIsNewsTicker(String hidIsNewsTicker) {
		this.hidIsNewsTicker = hidIsNewsTicker;
	}

	public String getBackButtonColor() {
		return BackButtonColor;
	}

	public void setBackButtonColor(String backButtonColor) {
		BackButtonColor = backButtonColor;
	}

}

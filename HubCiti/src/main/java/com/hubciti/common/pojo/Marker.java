/**
 * 
 */
package com.hubciti.common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author sangeetha.ts
 *
 */
public class Marker {
	
	private String markerId;
	
	private String markerName;
	
	private String markerImageName;
	
	private String markerImagePath;
	
	private CommonsMultipartFile markerImageFile;
	
	private Double markerLatitude;
	
	private Double markerLongitude;
	
	private String oldMarkerImageName;
	
	private String anythingPageId;
	
	private String hiddenAnythingPageId;

	/**
	 * @return the markerId
	 */
	public String getMarkerId() {
		return markerId;
	}

	/**
	 * @param markerId the markerId to set
	 */
	public void setMarkerId(String markerId) {
		this.markerId = markerId;
	}

	/**
	 * @return the markerName
	 */
	public String getMarkerName() {
		return markerName;
	}

	/**
	 * @param markerName the markerName to set
	 */
	public void setMarkerName(String markerName) {
		this.markerName = markerName;
	}

	/**
	 * @return the markerImageName
	 */
	public String getMarkerImageName() {
		return markerImageName;
	}

	/**
	 * @param markerImageName the markerImageName to set
	 */
	public void setMarkerImageName(String markerImageName) {
		this.markerImageName = markerImageName;
	}

	/**
	 * @return the markerImagePath
	 */
	public String getMarkerImagePath() {
		return markerImagePath;
	}

	/**
	 * @param markerImagePath the markerImagePath to set
	 */
	public void setMarkerImagePath(String markerImagePath) {
		this.markerImagePath = markerImagePath;
	}
	

	/**
	 * @return the markerLatitude
	 */
	public Double getMarkerLatitude() {
		return markerLatitude;
	}

	/**
	 * @param markerLatitude the markerLatitude to set
	 */
	public void setMarkerLatitude(Double markerLatitude) {
		this.markerLatitude = markerLatitude;
	}

	/**
	 * @return the markerLongitude
	 */
	public Double getMarkerLongitude() {
		return markerLongitude;
	}

	/**
	 * @param markerLongitude the markerLongitude to set
	 */
	public void setMarkerLongitude(Double markerLongitude) {
		this.markerLongitude = markerLongitude;
	}

	/**
	 * @return the markerImageFile
	 */
	public CommonsMultipartFile getMarkerImageFile() {
		return markerImageFile;
	}

	/**
	 * @param markerImageFile the markerImageFile to set
	 */
	public void setMarkerImageFile(CommonsMultipartFile markerImageFile) {
		this.markerImageFile = markerImageFile;
	}

	/**
	 * @return the oldMarkerImageName
	 */
	public String getOldMarkerImageName() {
		return oldMarkerImageName;
	}

	/**
	 * @param oldMarkerImageName the oldMarkerImageName to set
	 */
	public void setOldMarkerImageName(String oldMarkerImageName) {
		this.oldMarkerImageName = oldMarkerImageName;
	}

	public String getAnythingPageId() {
		return anythingPageId;
	}

	public void setAnythingPageId(String anythingPageId) {
		this.anythingPageId = anythingPageId;
	}

	public String getHiddenAnythingPageId() {
		return hiddenAnythingPageId;
	}

	public void setHiddenAnythingPageId(String hiddenAnythingPageId) {
		this.hiddenAnythingPageId = hiddenAnythingPageId;
	}

}

package com.hubciti.common.pojo;

public class SpecialOfferPageDetails {

	private Integer pageId;

	private String pageTitle;

	private String pageType;

	private String pageLink;

	private String pageImage;

	private String startDate;

	private String endDate;

	private Integer isFeatured;

	public Integer getIsFeatured() {
		return isFeatured;
	}

	public void setIsFeatured(Integer isFeatured) {
		this.isFeatured = isFeatured;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * 
	 */
	private Boolean bottomButtonExist;
	/**
	 * 
	 */
	private Boolean menuItemExist;
	/**
	 * variable for page Content.
	 */
	private String pageView;

	public Integer getPageId() {
		return pageId;
	}

	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public String getPageImage() {
		return pageImage;
	}

	public void setPageImage(String pageImage) {
		this.pageImage = pageImage;
	}

	public Boolean getBottomButtonExist() {
		return bottomButtonExist;
	}

	public void setBottomButtonExist(Boolean bottomButtonExist) {
		this.bottomButtonExist = bottomButtonExist;
	}

	public Boolean getMenuItemExist() {
		return menuItemExist;
	}

	public void setMenuItemExist(Boolean menuItemExist) {
		this.menuItemExist = menuItemExist;
	}

	public String getPageView() {
		return pageView;
	}

	public void setPageView(String pageView) {
		this.pageView = pageView;
	}

}

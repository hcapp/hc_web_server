package com.hubciti.common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * This POJO is used for coupon operations.
 * 
 * @author shyamsundara_hm
 * 
 */
public class Coupon {

	private String coupTitle;
	private String coupBanTitle;
	private Integer couponId;
	private String coupEndDate;
	private String coupDescription;
	private String coupKeywords;
	private String coupSearchKey;
	private Long coupNum;
	private String prodCategory;
	private String coupTermsConds;
	private String retailerName;
	private String locationId;
	private String coupImg;
	private String coupStartDate;
	private String coupSTimeHrs;
	private String coupSTimeMins;
	private String coupETimeHrs;
	private String coupETimeMins;
	private String coupExpiryDate;
	private String coupExpTimeHrs;
	private String coupExpTimeMins;
	private String coupStaTime;
	private String coupEndTime;
	private Integer retailId;
	private String coupExpTime;
	private String uploadCoupImageType;
	private CommonsMultipartFile imageFile;
	private Integer lowerLimit;
	private String retailerId;
	private Integer coupfeature;
	private String coupImgName;
	private String tempretids;
	private String dealListImg;
	private CommonsMultipartFile dealimageFile;
	private String dealLstImgName;
	private String dealLstImgPath;
	private Boolean isEnded;
	

	public String getCoupEndDate() {
		return coupEndDate;
	}

	public void setCoupEndDate(String coupEndDate) {
		this.coupEndDate = coupEndDate;
	}

	public String getCoupDescription() {
		return coupDescription;
	}

	public void setCoupDescription(String coupDescription) {
		this.coupDescription = coupDescription;
	}

	public String getCoupKeywords() {
		return coupKeywords;
	}

	public void setCoupKeywords(String coupKeywords) {
		this.coupKeywords = coupKeywords;
	}

	public String getCoupSearchKey() {
		return coupSearchKey;
	}

	public void setCoupSearchKey(String coupSearchKey) {
		this.coupSearchKey = coupSearchKey;
	}



	public String getProdCategory() {
		return prodCategory;
	}

	public void setProdCategory(String prodCategory) {
		this.prodCategory = prodCategory;
	}

	public String getCoupTermsConds() {
		return coupTermsConds;
	}

	public void setCoupTermsConds(String coupTermsConds) {
		this.coupTermsConds = coupTermsConds;
	}
	public String getCoupImg() {
		return coupImg;
	}

	public void setCoupImg(String coupImg) {
		this.coupImg = coupImg;
	}



	public String getCoupStartDate() {
		return coupStartDate;
	}

	public void setCoupStartDate(String coupStartDate) {
		this.coupStartDate = coupStartDate;
	}

	public String getCoupSTimeHrs() {
		return coupSTimeHrs;
	}

	public void setCoupSTimeHrs(String coupSTimeHrs) {
		this.coupSTimeHrs = coupSTimeHrs;
	}

	public String getCoupSTimeMins() {
		return coupSTimeMins;
	}

	public void setCoupSTimeMins(String coupSTimeMins) {
		this.coupSTimeMins = coupSTimeMins;
	}

	public String getCoupETimeHrs() {
		return coupETimeHrs;
	}

	public void setCoupETimeHrs(String coupETimeHrs) {
		this.coupETimeHrs = coupETimeHrs;
	}

	public String getCoupETimeMins() {
		return coupETimeMins;
	}

	public void setCoupETimeMins(String coupETimeMins) {
		this.coupETimeMins = coupETimeMins;
	}

	public String getCoupExpiryDate() {
		return coupExpiryDate;
	}

	public void setCoupExpiryDate(String coupExpiryDate) {
		this.coupExpiryDate = coupExpiryDate;
	}

	public String getCoupExpTimeHrs() {
		return coupExpTimeHrs;
	}

	public void setCoupExpTimeHrs(String coupExpTimeHrs) {
		this.coupExpTimeHrs = coupExpTimeHrs;
	}

	public String getCoupExpTimeMins() {
		return coupExpTimeMins;
	}

	public void setCoupExpTimeMins(String coupExpTimeMins) {
		this.coupExpTimeMins = coupExpTimeMins;
	}

	public String getCoupStaTime() {
		return coupStaTime;
	}

	public void setCoupStaTime(String coupStaTime) {
		this.coupStaTime = coupStaTime;
	}

	public String getCoupEndTime() {
		return coupEndTime;
	}

	public void setCoupEndTime(String coupEndTime) {
		this.coupEndTime = coupEndTime;
	}

	public Integer getRetailId() {
		return retailId;
	}

	public void setRetailId(Integer retailId) {
		this.retailId = retailId;
	}

	public String getCoupExpTime() {
		return coupExpTime;
	}

	public void setCoupExpTime(String coupExpTime) {
		this.coupExpTime = coupExpTime;
	}

	public String getUploadCoupImageType() {
		return uploadCoupImageType;
	}

	public void setUploadCoupImageType(String uploadCoupImageType) {
		this.uploadCoupImageType = uploadCoupImageType;
	}

	public CommonsMultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(CommonsMultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public String getRetailerName() {
		return retailerName;
	}

	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Integer getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public String getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}

	public Integer getCouponId() {
		return couponId;
	}

	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}



	public String getCoupImgName() {
		return coupImgName;
	}

	public void setCoupImgName(String coupImgName) {
		this.coupImgName = coupImgName;
	}

	public void setCoupfeature(Integer coupfeature) {
		this.coupfeature = coupfeature;
	}

	public Integer getCoupfeature() {
		return coupfeature;
	}

	public String getTempretids() {
		return tempretids;
	}

	public void setTempretids(String tempretids) {
		this.tempretids = tempretids;
	}

	public String getCoupTitle() {
		return coupTitle;
	}

	public void setCoupTitle(String coupTitle) {
		this.coupTitle = coupTitle;
	}

	public String getCoupBanTitle() {
		return coupBanTitle;
	}

	public void setCoupBanTitle(String coupBanTitle) {
		this.coupBanTitle = coupBanTitle;
	}

	public String getDealListImg() {
		return dealListImg;
	}

	public void setDealListImg(String dealListImg) {
		this.dealListImg = dealListImg;
	}

	public CommonsMultipartFile getDealimageFile() {
		return dealimageFile;
	}

	public void setDealimageFile(CommonsMultipartFile dealimageFile) {
		this.dealimageFile = dealimageFile;
	}

	public String getDealLstImgName() {
		return dealLstImgName;
	}

	public void setDealLstImgName(String dealLstImgName) {
		this.dealLstImgName = dealLstImgName;
	}

	public String getDealLstImgPath() {
		return dealLstImgPath;
	}

	public void setDealLstImgPath(String dealLstImgPath) {
		this.dealLstImgPath = dealLstImgPath;
	}

	public Long getCoupNum() {
		return coupNum;
	}

	public void setCoupNum(Long coupNum) {
		this.coupNum = coupNum;
	}

	public Boolean getIsEnded() {
		return isEnded;
	}

	public void setIsEnded(Boolean isEnded) {
		this.isEnded = isEnded;
	}

}

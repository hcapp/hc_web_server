package com.hubciti.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.pojo.Coupon;
import com.hubciti.common.util.Utility;

/**
 * This class is used to validate coupon operations.
 * 
 * @author shyamsundara_hm
 * 
 */
public class CouponValidator implements Validator {

	public boolean supports(Class<?> target) {
		return Coupon.class.isAssignableFrom(target);
	}

	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub

		Coupon objCoupon = (Coupon) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "coupTitle", "coupTitle.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "coupBanTitle", "coupBanTitle.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "coupDescription", "coupDescription.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "coupNum", "coupNum.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "coupStartDate", "coupStartDate.required");
		// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "coupEndDate",
		// "coupEndDate.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "locationId", "retailerLocIds.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dealLstImgName", "coupImg.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retailerName", "retailerName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "coupImg", "dealLstImgName.required");

		if (null != objCoupon.getCoupNum() && !"".equals(objCoupon.getCoupNum()))

		{
			if (Utility.validateZero(String.valueOf(objCoupon.getCoupNum()))) {
				errors.rejectValue("coupNum", "coupNum.notvalid");

			}
		}
		/*
		 * if ((null == objCoupon.getLocationId() ||
		 * "".equals(objCoupon.getLocationId())) )
		 * 
		 * {
		 * 
		 * errors.rejectValue("prodCategory", "retailLocs.required"); }
		 */
	}

	public void validateDates(Object obj, Errors error, String status) {

		if (status.equals(ApplicationConstants.VALIDSTARTDATE)) {
			error.rejectValue("coupStartDate", "validstartdate");
		}

		if (status.equals(ApplicationConstants.VALIDENDDATE)) {
			error.rejectValue("coupEndDate", "validenddate");
		}

		if (status.equals(ApplicationConstants.VALIDEXPDATE)) {
			error.rejectValue("coupExpiryDate", "validexpdate");
		}

		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			error.rejectValue("coupStartDate", "datestartcurrent");
		}

		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			error.rejectValue("coupEndDate", "dateendcurrent");
		}

		if (status.equals(ApplicationConstants.DATEEXPCURRENT)) {
			error.rejectValue("coupExpiryDate", "dateexpcurrent");
		}

		if (status.equals(ApplicationConstants.DATEENDAFTER)) {
			error.rejectValue("coupEndDate", "datebefore");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			error.rejectValue("coupExpiryDate", "dateExpbefore");
		}

		if (status.equals(ApplicationConstants.DATEEXPENTER)) {
			error.rejectValue("coupExpiryDate", "coupentrexp");
		}
		if (status.equals(ApplicationConstants.DATEENDENTER)) {
			error.rejectValue("coupEndDate", "coupentrend");
		}

	}

}

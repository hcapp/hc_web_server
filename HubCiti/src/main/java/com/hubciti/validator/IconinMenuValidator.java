package com.hubciti.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.pojo.ScreenSettings;

public class IconinMenuValidator implements Validator {

	/**
	 * this method returns true or false
	 */
	public boolean supports(Class<?> target) {
		return ScreenSettings.class.isAssignableFrom(target);
	}

	/**
	 * this method validates mandatory fields
	 */
	public void validate(Object target, Errors errors) {

		ScreenSettings screenSettings = (ScreenSettings) target;

		/* News Template */
		/*if(screenSettings.getIsBannerOrTicker() != 0){*/
			if (null != screenSettings.getViewName() && !"setupscrollingnewsmenu".equalsIgnoreCase(screenSettings.getViewName())
					&& !"setupcombonewsmenu".equalsIgnoreCase(screenSettings.getViewName()) && !"setupnewstilemenu".equalsIgnoreCase(screenSettings.getViewName()) && !"setupsidemenu".equalsIgnoreCase(screenSettings.getViewName()) ) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logoImageName", "logoImageName.required");
			}
		/*}*/
		/*if(screenSettings.getIsBannerOrTicker() == 1){*/
			if ("setuprectangularmenu".equals(screenSettings.getViewName()) || "setuprectangular4X4menu".equals(screenSettings.getViewName())) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "iPadLogoImageName", "logoImageName.required");
			}
/*		}*/
		
		if ("setuptwoimagemenu".equals(screenSettings.getViewName()) || "setupsquareimagemenu".equals(screenSettings.getViewName())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tmplBckGrdImageName", "logoImageName.required");
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "menuBtnName", "menuBtnName.required");

		if(screenSettings.getIsBannerOrTicker() != null && screenSettings.getIsBannerOrTicker() == 1){
			if ("iconicmenutemplate".equals(screenSettings.getViewName()) || "setuprectangularmenu".equals(screenSettings.getViewName())
				|| "setuprectangular4X4menu".equals(screenSettings.getViewName()) || "setuptwoimagemenu".equals(screenSettings.getViewName())
				|| "setupsquareimagemenu".equals(screenSettings.getViewName())) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bannerImageName", "bannerimg.required");
			} else {
			if ("Yes".equals(screenSettings.getIsGroupImg())) {
				
					if ("setuptwotilemenu".equals(screenSettings.getViewName())) {
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tmplBckGrdImageName", "logoImageName.required");
					} else {
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bannerImageName", "bannerimg.required");
					}
				
				
				}
			}
		}

		if ("0".equals(screenSettings.getMenuFucntionality())) {
			errors.rejectValue("menuFucntionality", "menuFucntionality.required");
		} else {
			if (null != screenSettings.getFunctionalityType() && !"".equals(screenSettings.getFunctionalityType())
					&& (null == screenSettings.getBtnLinkId() || "null".equalsIgnoreCase(screenSettings.getBtnLinkId()))) {

				if (screenSettings.getFunctionalityType().equals(ApplicationConstants.SUBMENU)) {
					errors.rejectValue("menuFucntionality", "required.submenu");
				}

				if (screenSettings.getFunctionalityType().equals(ApplicationConstants.APPSITE)) {
					errors.rejectValue("menuFucntionality", "required.appsite");
				}

				if (screenSettings.getFunctionalityType().equals(ApplicationConstants.ANYTHINGPAGE)) {
					errors.rejectValue("menuFucntionality", "required.anythingpage");
				}

				if (screenSettings.getFunctionalityType().equals(ApplicationConstants.FIND)) {
					errors.rejectValue("menuFucntionality", "required.category");
				}

				if (screenSettings.getFunctionalityType().equals(ApplicationConstants.SETUPEVENTS)) {
					errors.rejectValue("menuFucntionality", "required.category");
				}

				if (screenSettings.getFunctionalityType().equals(ApplicationConstants.FUNDRAISERS)) {
					errors.rejectValue("menuFucntionality", "required.category");
				}

				if (screenSettings.getFunctionalityType().equals(ApplicationConstants.FILTERS)) {
					errors.rejectValue("menuFucntionality", "required.filter");
				}
				if (screenSettings.getFunctionalityType().equals(ApplicationConstants.BAND)) {
					errors.rejectValue("menuFucntionality", "required.category");
				}

			} else if ("RegionApp".equalsIgnoreCase(screenSettings.getUserType()) && (null == screenSettings.getCitiId() || "".equalsIgnoreCase(screenSettings.getCitiId()))) {
				errors.rejectValue("menuFucntionality", "citi.select");
			}
		}

		if (null == screenSettings.getMenuLevel() && null!= screenSettings.getIsSideMenu() && screenSettings.getIsSideMenu() == true) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subMenuName", "sideMenuName.required");
		}else if(null == screenSettings.getMenuLevel()){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subMenuName", "subMenuName.required");
		}

		if (null != screenSettings.getMenuFilterType() && screenSettings.getMenuFilterType().length >= 1) {
			if ("Department".equals(screenSettings.getMenuFilterType()[0])) {
				if ("0".equals(screenSettings.getBtnDept())) {
					errors.rejectValue("btnDept", "dept.required");
				}
			} else if ("Type".equals(screenSettings.getMenuFilterType()[0]) && "0".equals(screenSettings.getBtnType())) {
				errors.rejectValue("btnType", "type.required");
			}

			if (screenSettings.getMenuFilterType().length == 2) {
				if ("Department".equals(screenSettings.getMenuFilterType()[1])) {
					if ("0".equals(screenSettings.getBtnDept())) {
						errors.rejectValue("btnDept", "dept.required");
					}
				} else if ("Type".equals(screenSettings.getMenuFilterType()[1]) && "0".equals(screenSettings.getBtnType())) {
					errors.rejectValue("btnType", "type.required");
				}
			}
		}
	}

	public final void validate(Object arg0, Errors errors, String status) {

		if (ApplicationConstants.DUPLICATEBUTTONNAME.equals(status)) {
			errors.reject("duplicate.button");
		}

		if (ApplicationConstants.DUPLICATEFUNCTINALITY.equals(status)) {
			errors.reject("duplicate.functionality");
		}

		if (ApplicationConstants.DUPLICATESUBMENU.equals(status)) {
			errors.reject("duplicate.submenu");
		}

		if (ApplicationConstants.DUPLICATEANYTHINGPAGE.equals(status)) {
			errors.reject("duplicate.anythingpage");
		}

		if (ApplicationConstants.DUPLICATEAPPSITE.equals(status)) {
			errors.reject("duplicate.appsite");
		}

		if (ApplicationConstants.DUPLICATECATEGORY.equals(status)) {
			errors.reject("duplicate.category");
		}

		if (ApplicationConstants.DUPLICATECATEGORIES.equals(status)) {
			errors.reject("duplicate.categories");
		}

		if (ApplicationConstants.DUPLICATEFILTER.equals(status)) {
			errors.reject("filter.duplicate");
		}
		if(ApplicationConstants.DUPLICATENEWSSUBMENU.equals(status)){
			errors.reject("duplicate.newsSubMenu");
		}

	}

}

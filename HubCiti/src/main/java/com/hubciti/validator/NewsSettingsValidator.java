package com.hubciti.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.pojo.NewsCategory;

public class NewsSettingsValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return NewsCategory.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		NewsCategory newsCategory = (NewsCategory) target;

		
		if( null != newsCategory && null != newsCategory.getIsFeed() && newsCategory.getIsFeed() == true){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "catId", "catId.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors,"displayTypId","displayTypId.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "catfeedURL", "catfeedURL.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subPageId", "subPageId.required");
			
			if(null != newsCategory.getIsNewsTicker() && newsCategory.getIsNewsTicker() == true)
			{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "noStories", "noStories.required");
			}
			
			if("0".equals(String.valueOf(newsCategory.getNoStories()))){
				errors.rejectValue("noStories", "noStories.error");
			}
			
			if(newsCategory.getCatFontColor().equals(newsCategory.getCatColor())){
				errors.rejectValue("catColor", "catFontColor.catColor");
			}
			
			if(newsCategory.getCatColor().equals(newsCategory.getBackButtonColor())){
				errors.rejectValue("BackButtonColor", "catColor.BackButtonColor");
			}
			
				
			if(null != newsCategory.getCatId())
			{
			if ("0".equals(String.valueOf(newsCategory.getCatId()))) {
				errors.rejectValue("catId", "catId.required");
	
			}
			}
	
			if ("0".equals(newsCategory.getDisplayTypId())) {
				errors.rejectValue("displayTypId", "displayTypId.required");
	
			}
		
			if ("0".equals(newsCategory.getSubPageId())) {
				errors.rejectValue("subPageId", "subPageId.required");
			}
		}else{

			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "catfeedURL", "catfeedURL.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nonfeedCatName", "nonfeedCatName.required");
			
			if(newsCategory.getCatFontColor().equals(newsCategory.getCatColor())){
				errors.rejectValue("catColor", "catFontColor.catColor");
			}
			
			if(newsCategory.getCatColor().equals(newsCategory.getBackButtonColor())){
				errors.rejectValue("BackButtonColor", "catColor.BackButtonColor");
			}
				
		}
	}

	public final void validate(Object arg0, Errors errors, String status, String urlType) {
		if (null != urlType && urlType.equals("weather")) {
			if (status.equals(ApplicationConstants.INVALIDURL)) {
				errors.rejectValue("weatherURL", "invalid.url");
			}
		} else {
			if (status.equals(ApplicationConstants.INVALIDURL)) {
				errors.rejectValue("catfeedURL", "invalid.url");
			}
		}
	}

	public void validate(String status, Object target, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bannerImg", "bannerImg.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newsImage", "newsImage.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ThumnailPos", "ThumnailPos.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "weatherURL", "weatherURL.required");
	}

	public final void validateNewsCatUpdate(Object target, Errors errors) {
		NewsCategory newsCategory = (NewsCategory) target;
		
		if( null == newsCategory.getIsFeed() || newsCategory.getIsFeed() == true ){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "displayTypeName", "displayType.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subPageName", "subPageName.required");
			if (null != newsCategory.getIsNewsTicker() && newsCategory.getIsNewsTicker() == true) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "noStories", "noStories.required");
			}
	
			if ("0".equals(String.valueOf(newsCategory.getNoStories()))) {
				errors.rejectValue("noStories", "noStories.error");
			}
		}else{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nonfeedCatName", "nonfeedCatName.required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "catColor", "catColor.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "catfeedURL", "catfeedURL.required");
		if (newsCategory.getCatFontColor().equals(newsCategory.getCatColor())) {
			errors.rejectValue("catColor", "catFontColor.catColor");
		}

		if (newsCategory.getCatColor().equals(newsCategory.getBackButtonColor())) {
			errors.rejectValue("BackButtonColor", "catColor.BackButtonColor");
		}

	}
}

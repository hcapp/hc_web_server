/**
 * 
 */
package com.hubciti.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.pojo.Logistics;

/**
 * @author sangeetha.ts
 *
 */
public class LogisticsValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return Logistics.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logisticName", "logisticsName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logisticImageName", "logoImageName.required");
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retailerName", "retailer.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "locationId", "locationAssociation");*/
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startDate", "startDate.required");
		
	}

	/**
	 * This method validate Dates.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors.
	 * @param status
	 *            as request parameter.
	 */
	public final void validateDates(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.VALIDSTARTDATE)) {
			errors.rejectValue("startDate", "validstartdate");
		}
		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			errors.rejectValue("startDate", "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.VALIDENDDATE)) {
			errors.rejectValue("endDate", "validenddate");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			errors.rejectValue("endDate", "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue("endDate", "datebefore");
		}
	}
}

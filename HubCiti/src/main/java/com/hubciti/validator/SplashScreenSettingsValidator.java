package com.hubciti.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.pojo.ScreenSettings;
import com.hubciti.common.util.Utility;

/**
 * This class is a validator class for about us screen.
 * 
 * @author dileep_cc
 */
public class SplashScreenSettingsValidator implements Validator {
	/**
	 * this method returns true or false
	 */
	public boolean supports(Class<?> target) {
		return ScreenSettings.class.isAssignableFrom(target);
	}

	/**
	 * this method validates mandatory fields
	 */
	public void validate(Object target, Errors errors) {
		ScreenSettings settings = (ScreenSettings) target;
		if("true".equals(settings.getIsCatImg()))	{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "catImgName", "logoImageName.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "required.category");
		} else	{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logoImageName", "logoImageName.required");
		}
	}

	/**
	 * this method validates mandatory fields
	 */
	public void validateGeneralSettings(Object target, Errors errors) {
		ScreenSettings settings = (ScreenSettings) target;
		if("News".equalsIgnoreCase(settings.getPageType())){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sidemenuImageName", "sidemenuImageName.required");
		} else if ("TabBarLogo".equalsIgnoreCase(settings.getPageType())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logoImageName", "logoImageName.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bottomBtnId", "bottomBtnId.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bannerImageName", "logoImageName.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "backGroundColor", "backGroundColor.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "titleColor", "titleColor.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "homeIconName", "homeIconName.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "backButtonIconName", "backButtonIconName.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hubCitiClaimURL", "hubCitiClaimURL.required");
			
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "claimBannerText1", "claimBannerText1.required");
			
			if( settings.isClaimType() == true){
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "claimImage", "claimImage.required");
			} else{
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "claimText", "claimText.required");
			}
			
			if("".equals(Utility.checkNull(settings.getClaimBannerText2()))  &&  ( !"".equals(Utility.checkNull(settings.getClaimBannerText3())) || !"".equals(Utility.checkNull(settings.getClaimBannerText4())))){
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "claimBannerText2", "claimBannerText2.required");
			} 
			
			if("".equals(Utility.checkNull(settings.getClaimBannerText3()))  && !"".equals(Utility.checkNull(settings.getClaimBannerText4()))){
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "claimBannerText3", "claimBannerText3.required");
			} 
		}else if("CatImage".equalsIgnoreCase(settings.getPageType()))	{
		
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "catImgName", "catImageUpdate.required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "required.category");
			if(null == settings.getCategory() || "0".equals(settings.getCategory()) )	{
				errors.rejectValue("category", "required.category");
			}
		} else{
			if (null != settings.getIconSelection() && !"".equalsIgnoreCase(settings.getIconSelection())) {
				if ("image".equalsIgnoreCase(settings.getIconSelection())) {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logoImageName", "logoPath.required");
				}
			} else {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "iconSelection", "background.required");
			}
		}

	}
	
	public final void validate(Object arg0, Errors errors, String status, String urlType) {
		if (null != urlType && urlType.equals("claim")) {
			if (status.equals(ApplicationConstants.INVALIDURL)) {
				errors.rejectValue("hubCitiClaimURL", "invalid.url");
			}
		} 
	}

}

package com.hubciti.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hubciti.common.constatns.ApplicationConstants;
import com.hubciti.common.pojo.ScreenSettings;

/**
 * This class is a validator class for about us screen.
 * 
 * @author dileep_cc
 */
public class LoginScreenSettingsValidator implements Validator {
	/**
	 * this method returns true or false
	 */
	public boolean supports(Class<?> target) {
		return ScreenSettings.class.isAssignableFrom(target);
	}

	/**
	 * this method validates mandatory fields
	 */
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logoImageName", "logoPath.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bgColor", "bgColor.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fontColor", "fontColor.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "btnColor", "btnColor.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "btnFontColor", "btnFontColor.required");

	}
	
	public final void validate(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.INVALIDURL)) {
			errors.rejectValue("firstUseURL", "invalid.url");
		}
	}
	
	public void validateTrainingScreen(Object target, Errors errors,String isUrl,String IsUrlorPage) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "logoImageName", "trngScreenImage.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "screenName", "screenName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "screenType.required");
		if(isUrl.equals("1")){
		if(IsUrlorPage.equals("1"))
		ValidationUtils.rejectIfEmpty(errors, "firstUseURL", "firstUseURL.required");
		else
		ValidationUtils.rejectIfEmpty(errors, "btnLinkId", "btnLinkId.required");
		}
		
	}

}

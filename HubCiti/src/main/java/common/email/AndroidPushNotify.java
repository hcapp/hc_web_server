package common.email;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.common.pojo.Configuration;
import com.hubciti.common.pojo.Device;
import com.hubciti.common.pojo.NotificationDetails;
import com.hubciti.common.util.Utility;

/**
 * @author kumar_dodda
 *
 */
public class AndroidPushNotify implements Runnable {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AndroidPushNotify.class);

	/**
	 * instance for storing list of android devices related information
	 */
	private List<Device> androidIds;

	/**
	 * 
	 * Instance for notification details
	 * 
	 */
	private NotificationDetails notiDetails;

	/**
	 * 
	 * Instance for notification configurations
	 * 
	 */
	private List<Configuration> pushNotiConfigs;

	/**
	 * Thread for running process for sending pushnotification to GCM Server
	 * 
	 */
	private Thread thread;

	public Thread getThread() {
		return thread;
	}

	/**
	 * Constructor for Initalizing instaces
	 * @param androidIds
	 * @param notiDetails
	 * @param pushNotiConfigs
	 */
	public AndroidPushNotify(List<Device> androidIds, NotificationDetails notiDetails, List<Configuration> pushNotiConfigs) {
		super();
		this.androidIds = androidIds;
		this.notiDetails = notiDetails;
		this.pushNotiConfigs = pushNotiConfigs;
		this.thread = new Thread(this);
		this.thread.start();
	}

	/**
	 * 
	 * Thread run method for sending push notification to android devices
	 * 
	 */

	public void run() {
		try {
			Utility.sendAndroidNotification(androidIds, notiDetails, pushNotiConfigs);
		} catch (Exception e) {
			LOG.error("Inside : AndroidPushNotify : run() : " + e.getMessage());
		}
	}

}

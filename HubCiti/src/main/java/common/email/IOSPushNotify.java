package common.email;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.common.pojo.Configuration;
import com.hubciti.common.pojo.Device;
import com.hubciti.common.pojo.NotificationDetails;
import com.hubciti.common.util.Utility;


/**
 * @author kumar_dodda
 *
 */
public class IOSPushNotify implements Runnable {

	/**
	 * 
	 */
	private static final Logger LOG = LoggerFactory.getLogger(IOSPushNotify.class);
	/**
	 * 
	 */
	private List<Device> iOSDevIds;

	/**
	 * 
	 */
	private NotificationDetails notifyDetails;

	/**
	 * 
	 */
	private List<Configuration> configList;
	/**
	 * 
	 */
	private Thread thread;

	/**
	 * 
	 * @return the current thread
	 */
	public Thread getThread() {
		return thread;
	}


	/**
	 * @param iOSIds
	 * @param notiDetails
	 * @param pushNotiConfigs
	 */
	public IOSPushNotify(List<Device> iOSDevIds, NotificationDetails notifyDetails, List<Configuration> configList) {
		super();
		this.iOSDevIds = iOSDevIds;
		this.notifyDetails = notifyDetails;
		this.configList = configList;
		this.thread = new Thread(this);
		this.thread.start();
	}


	public void run() {
		try {
			Utility.sendIOSNotification(iOSDevIds, notifyDetails, configList);
		}  catch (Exception e) {
			LOG.error("Inside : IOSPushNotify : run() : " + e.getMessage());
		}
	}
}

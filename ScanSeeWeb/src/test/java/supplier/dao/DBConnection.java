/*

	//import org.apache.log4j.Logger;
	import org.springframework.jdbc.core.JdbcTemplate;
	import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
	import org.springframework.jdbc.datasource.SingleConnectionDataSource;



	*//**
	 * The class for connecting database
	 * 
	 * @author sowjanya_d
	 *//*
	public class DBConnection
	{
		*//**
		 * @return JdbcTemplate
		 * @throws DealMapBatchException
		 * The exceptions are caught and a Exception defined for the
		 *             application is thrown which is caught in the Controller
		 *             layer.
		 *          
		 *//*
		public JdbcTemplate getConnection() 
		{
			JdbcTemplate jdbcTemplate = null;
			//final Logger log = Logger.getLogger(DBConnection.class);
			try
			{
				SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
				singleConnectionDataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
				singleConnectionDataSource.setUrl("jdbc:jtds:sqlserver://10.11.201.226:1433/ScanSee_22ndJuly2011_Dev;SelectMethod=cursor");
				singleConnectionDataSource.setUsername("scansee");
				singleConnectionDataSource.setPassword("span@1234");
				jdbcTemplate = new JdbcTemplate(singleConnectionDataSource);
			}
		
			catch (Exception e)
			{
				//log.error("Exception occured in getConnection.." + e);
			//	throw new Exception();
			}
			return jdbcTemplate;
		}

		*//**
		 * To get database information to connect.
		 * @return simpleJdbcTemplate
		 * @throws DealMapBatchException
		 *             The exceptions are caught and a Exception defined for the
		 *             application is thrown which is caught in the Controller
		 *             layer.
		 *//*
		public  SimpleJdbcTemplate getSimpleJdbcTemplate() 
		{
			SimpleJdbcTemplate simpleJdbcTemplate = null;
			//final Logger log = Logger.getLogger(DBConnection.class);
			try
			{
				SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
				singleConnectionDataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
				singleConnectionDataSource.setUrl("jdbc:jtds:sqlserver://10.11.201.226:1433/ScanSee_22ndJuly2011_Dev;SelectMethod=cursor");
				singleConnectionDataSource.setUsername("scansee");
				singleConnectionDataSource.setPassword("span@1234");
				simpleJdbcTemplate = new SimpleJdbcTemplate(singleConnectionDataSource);
			}
			
			catch (Exception e)
			{
				//log.error("Exception occured in getConnection" + e);
				//throw new Exception();
			}
			return simpleJdbcTemplate;
		}
	}


*/
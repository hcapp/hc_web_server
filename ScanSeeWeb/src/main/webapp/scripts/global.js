var cont = 1;
var actIndex;
var isIE = $.browser.msie;
var bVer = $.browser.version;
var isTE8 = isIE && bVer < 9 || document.documentMode == 8;
var isChrome = /Chrome/.test(navigator.userAgent)
&& /Google Inc/.test(navigator.vendor);

$(document).ready(function() {
	
	if(!isIE && bVer >= 9 ) {
	$(".trgrUpld").click(function() {
		$(this).next('input[type="file"]').click();
	});
	
	$(".trgrUpldDetImg").click(function() {
		$(this).next('input[type="file"]').click();
	});
	}
	
	
	
	/*Update Account: Select Category & Displaying popup with filter & associated values*/
	/*$("#selectFilters").click(function() {

		  var slctdOptns = $("#bCategory option.toggleFltr:selected").map(function(){
							  return $(this).val();
							}) .get().join();
		 
		  $.ajaxSetup({
				cache : false
		  });
		  
		  $.ajax({
				type : "GET",
				url : "fetchfilters.htm",
				data : {
					'filterIds' : slctdOptns 
				},
				success : function(response) {
					$("#hiddenFilterCategory").val(slctdOptns);
					openIframePopup('ifrmPopup','ifrm','/ScanSeeWeb/retailer/associatefilter.htm',590,960,'Select Filter(s)');
					if("" !== response) {
						var resJSON = JSON.parse(response);
						var JSONLength = resJSON['categories'].length;
						if(JSONLength > 0) {
							var displayContent;
							if(JSONLength === 1) {
								displayContent = "<div class='row single'>";
							} else {
								displayContent = "<div class='row'>";
							}
							
							for(var i = 0; i < JSONLength; i++) {
								if(i > 0) {
									displayContent += "</ul></div>";
								}
								var category = resJSON['categories'][i];
								var catid = category.id;
								var catname = category.Name;
								displayContent += "<div class='col " + catid +"'><p class='pnlHdr'><input type='checkbox' class='trgrChlAll' value='" + catid + "'/><label>"; 
								displayContent += catname + "</label></p><ul class='treeview' id='"+ catid +"'>";
								var filterJSON = category["filters"];
								
								for(var j = 0; j < filterJSON.length ;j++) {
									var filter = filterJSON[j];
									var filterName = filter.Name;
									var filterId = filter.id;
									displayContent += "<li class='mainCtgry' name='"+ filterName + "'>";
									displayContent += "<input class='main' type='checkbox' value='"+ filterId + "'/><label>" + filterName + "</label></li>";
									var subfilterJSON = filter["subfilters"];
									if(subfilterJSON != 'undefined' && "undefined" !== typeof(subfilterJSON)) {
										for(var k = 0; k < subfilterJSON.length; k++) {
											displayContent += "<li class='subCtgry' name='"+ filterName +"'>";
											displayContent += "<input class='sub' type='checkbox' value='"+ subfilterJSON[k].id + "'/><label>" + subfilterJSON[k].Name + "</label></li>";
										}
									}
								}
							}
							
							displayContent += "</ul></div></div>";
							
							$("#ifrm").load(function(){	
								var iframe = $("#ifrm").contents();
								iframe.find("#message").css("display","none");
								iframe.find("#associate").removeAttr("disabled");
								iframe.find("#associate").removeClass("disabled");								
								iframe.find("div.row").remove();
								iframe.find("#tabularPnl").html(displayContent);
								
								var filCat = $("#filterCategory").val().split(",");
								var filter_split = $("#filters").val().split("!~~!");
								var subfilter_split = $("#filterValues").val().split("!~~!");
								var k=0;
								var n=0;							
								var filter_split_length = filter_split.length;
								if(filter_split_length == 1 && filter_split[0] === "") {
									filter_split_length = 0;							
								}
								for(var i =0 ; i < filter_split_length; i++) {
									var $this = $("#ifrm").contents().find("div." + filCat[i]);
									var filStr = filter_split[i].split("|");
									if(filStr.length == 1 && filStr === "NULL") {
										k++;
									} else {
										for(var j =0 ; j < filStr.length; j++) {
											$($this).find("input:checkbox[value="+ filStr[j] +"].main").attr("checked", true);
											var valFil = subfilter_split[k].split("|");
											for(var m = 0; m < valFil.length; m++) {
												$($this).find("input:checkbox[value="+ valFil[m] +"].sub").attr("checked", true);
												var li_name = $($this).find("input:checkbox[value="+ valFil[m] +"].sub").parent('li').attr("name");
												$($this).find('li[name="'+li_name+'"]').show();
												//$($this).find("input:checkbox[value="+ valFil[m] +"].sub").parent('li').show();
											}
											k++;
										}
									}

									var getId = $("#ifrm").contents().find("ul#" + filCat[i]);
									var chkbx = $(getId).find('li :checkbox').length;
									var chkbxChkd = $(getId).find('li :checkbox:checked').length;
									if(chkbx === chkbxChkd){
										$(getId).prev('p').find(':checkbox').attr('checked', true);
									}
									else {
										$(getId).prev('p').find(':checkbox').removeAttr('checked');
									}
								}														
							});
							
						
						} 
					} else {
						$("#ifrm").load(function(){
							var iframe = $("#ifrm").contents();
							iframe.find("div.row").remove();
							iframe.find("#message").css({"display":"block","margin-top":"10px"});
							iframe.find("#associate").attr("disabled","disabled");
							iframe.find("#associate").addClass("disabled");
						});
					}
					
				},
				error : function(e) {
					alert(e);
				}
			});		  	
		});*/
	
		$("#selectSubCats").click(function() {
			$('body,html').animate({scrollTop :0}, 'slow');
		  /*document.body.style.overflow = "hidden";*/
		  var slctdOptns = $("#bCategory option.toggleSubCat:selected").map(function(){
							  return $(this).val();
							}) .get().join();
		 
		  $.ajaxSetup({
				cache : false
		  });
		  
		  $.ajax({
				type : "GET",
				url : "fetchsubcats.htm",
				data : {
					'catIds' : slctdOptns 
				},
				success : function(response) {
					$("#hiddensubCategory").val(slctdOptns);
					openIframePopup('ifrmPopup','ifrm','/ScanSeeWeb/retailer/associatesubcats.htm',590,960,'Select Sub-Category(s)');
					if("" !== response) {
						var resJSON = JSON.parse(response);
						var JSONLength = resJSON['categories'].length;
						if(JSONLength > 0) {
							var displayContent;
							if(JSONLength === 1) {
								displayContent = "<div class='row single'>";
							} else {
								displayContent = "<div class='row'>";
							}
							
							for(var i = 0; i < JSONLength; i++) {
								if(i > 0) {
									displayContent += "</ul></div>";
								}
								var category = resJSON['categories'][i];
								var catid = category.id;
								var catname = category.Name;
								displayContent += "<div class='col " + catid +"'><p class='pnlHdr'><input type='checkbox' class='trgrChlAll' value='" + catid + "'/><label>"; 
								displayContent += catname + "</label></p><ul class='treeview' id='"+ catid +"'>";
								var subCatJSON = category["subCategories"];
								
								for(var j = 0; j < subCatJSON.length ;j++) {
									var subCat = subCatJSON[j];
									var subCatName = subCat.Name;
									var subCatId = subCat.id;
									displayContent += "<li class='mainCtgry' name='"+ subCatName + "'>";
									displayContent += "<input class='main' type='checkbox' value='"+ subCatId + "'/><label>" + subCatName + "</label></li>";
								}
							}
							
							displayContent += "</ul></div></div>";
							
							$("#ifrm").load(function(){	
								var iframe = $("#ifrm").contents();
								iframe.find("#message").css("display","none");
								iframe.find("#associateSubCats").removeAttr("disabled");
								iframe.find("#associateSubCats").removeClass("disabled");								
								iframe.find("div.row").remove();
								iframe.find("#tabularPnl").html(displayContent);
								
								var subCat = $("#subCategory").val().split(",");
								var subCat_split = $("#subCategories").val().split("!~~!");
								var k=0;
								var n=0;							
								var subCat_split_length = subCat_split.length;
								if(subCat_split_length == 1 && subCat_split[0] === "") {
									subCat_split_length = 0;							
								}
								for(var i =0 ; i < subCat_split_length; i++) {
									var $this = $("#ifrm").contents().find("div." + subCat[i]);
									var subCatStr = subCat_split[i].split("|");
									for(var j =0 ; j < subCatStr.length; j++) {
										$($this).find("input:checkbox[value="+ subCatStr[j] +"].main").attr("checked", true);
									}
									var getId = $("#ifrm").contents().find("ul#" + subCat[i]);
									var chkbx = $(getId).find('li :checkbox').length;
									var chkbxChkd = $(getId).find('li :checkbox:checked').length;
									if(chkbx === chkbxChkd){
										$(getId).prev('p').find(':checkbox').attr('checked', true);
									} else {
										$(getId).prev('p').find(':checkbox').removeAttr('checked');
									}
								}														
							});						
						} 
					} else {
						$("#ifrm").load(function(){
							var iframe = $("#ifrm").contents();
							iframe.find("div.row").remove();
							iframe.find("#message").css({"display":"block","margin-top":"10px"});
							iframe.find("#associateSubCats").attr("disabled","disabled");
							iframe.find("#associateSubCats").addClass("disabled");
						});
					}					
				},
				error : function(e) {
					alert(e);
				}
			});		  	
		});
		
					  
		$(document ).delegate( ".trgrChlAll", "change", function() {		
			var chkdstatus = $(this).attr("checked");
			var self = this;
			if(self.checked) {
				$(this).parents('div.col').find('.treeview input:checkbox').each(function(index, element) {
				$(this).attr("checked",chkdstatus);
				$(this).parents('div.col').find('.treeview li.subCtgry').show();
			});
			}else {
				$(this).parents('div.col').find('.treeview input:checkbox').each(function(index, element) {
				$(this).removeAttr("checked");
				$(this).parents('div.col').find('.treeview li.subCtgry').hide();
			});
			}
		});
				  
		$( document ).delegate( ".treeview li.mainCtgry input:checkbox", "click", function() {	
			var getId = $(this).parents('ul').attr("id");
			var maincatNm =  $(this).parents('li').attr('name');	
			var self = this;
			if(self.checked) {
				$('#'+getId).find('li[name="'+maincatNm+'"]').show();
				$('#'+getId).find('li[name="'+maincatNm+'"]').each(function(i) {
					if(i>0 && self.checked) {
						$(this).find('input:checkbox').attr('checked', 'checked');
					}	
				}); 
			}else {
				$('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"]').hide();
				$('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"]').each(function() {
					$(this).find('input:checkbox').removeAttr('checked');
				});
			}
			var chkbx = $('#'+getId).find(':checkbox').length;
			var chkbxChkd = $('#'+getId).find(':checkbox:checked').length;
			if(chkbx === chkbxChkd){
				$('#'+getId).prev('p').find(':checkbox').attr('checked', true);
			}
			else {
				$('#'+getId).prev('p').find(':checkbox').removeAttr('checked');
			}
		
		});
						
		$( document ).delegate( ".treeview li:not(.mainCtgry) input:checkbox", "click", function() {	
			var getId = $(this).parents('ul').attr("id");
			var maincatNm =  $(this).parents('li').attr('name');	
			if($('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length < 1) {
				$('#'+getId).find('li[name="'+maincatNm+'"]').each(function(i) {
					if(i==0) {
						$(this).find('input:checkbox').removeAttr('checked');
					}else {
						$(this).hide();	
					}
				});
			}
			/*var totChk = $('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox').length;
			var chkLen = $('#'+getId).find('li:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length;
			if(chkLen === totChk) {
				$('#'+getId).find('li.mainCtgry[name="'+maincatNm+'"] :checkbox').attr('checked','checked');
			} 
			else {	
				$('#'+getId).find('li.mainCtgry[name="'+maincatNm+'"] :checkbox').removeAttr('checked');
			}*/		
			
			totChk = $('#'+getId).find('li :checkbox').length;
			chkLen = $('#'+getId).find('li :checkbox:checked').length;
			if(chkLen === totChk) {
				$('#'+getId).prev('p').find(':checkbox').attr('checked', true);	
			} 
			else {
				$('#'+getId).prev('p').find(':checkbox').removeAttr('checked');
			}
		});
						
		/*$("#associate").click(function() {
			var mainFilter = "";
			var subFilter = "";
			var i = 0;
			var curMainFilter = "";
			var curSubFilter = "";
			var previouClass = "";
			var selectedCat = "";
			
			$("div.col").each(function(){
				if(i > 0) {
					if(curMainFilter !== "") {
						if(mainFilter !== "") {
							mainFilter += "!~~!" + curMainFilter;
						} else {
							mainFilter = curMainFilter;
						}
					} else {
						if(mainFilter !== "") {
							mainFilter += "!~~!" + 'NULL';
						} else {
							mainFilter = 'NULL';
						}
					}
					if(curSubFilter != "") {
						if(previouClass === "main") {
							curSubFilter += "!~~!" + 'NULL';
						}
						if(subFilter !== "") {
							subFilter += "!~~!" + curSubFilter;
						} else {
							subFilter = curSubFilter;
						}
					} else {
						if(subFilter !== "") {
							subFilter += "!~~!" + 'NULL';
						} else {
							subFilter = 'NULL';
						}
					}

					curMainFilter = "";
					curSubFilter = "";
					previouClass = "";
					i = 0;
				}
				var ulId = $(this).attr("class").split(" ").slice(-1);
				if(selectedCat === "") {
					selectedCat = ulId;
				} else {
					selectedCat += "," + ulId;
				}
				$("#"+ulId+" input:checkbox:checked").each(function(){					
					var curCls = $(this).attr("class");
					if(curCls === "main") {						
						if(previouClass === "") {
						  curMainFilter = $(this).val();
						} else if(previouClass === "main") {
						  curMainFilter += "|" + $(this).val();
						  if(curSubFilter != "") {
							curSubFilter += "!~~!" + 'NULL';
						  } else {
							curSubFilter = 'NULL';
						  }
						} else if(previouClass === "sub") {
						  curMainFilter += "|" + $(this).val();
						}							
					} else if(curCls === "sub") {
						if(curSubFilter != "") {
							if(previouClass === "main") {
								curSubFilter += "!~~!" + $(this).val();
							} else {
								curSubFilter += "|" + $(this).val();
							}
						} else {
							curSubFilter = $(this).val();
						}
					}
					previouClass = curCls;
				});
				i++;
			});
				
			if(curMainFilter !== "") {
				if(mainFilter !== "") {
					mainFilter += "!~~!" + curMainFilter;
				} else {
					mainFilter = curMainFilter;
				}
			} else {
				if(mainFilter !== "") {
					mainFilter += "!~~!" + 'NULL';
				} else {
					mainFilter = 'NULL';
				}
			}
			if(curSubFilter != "") {
				if(previouClass === "main") {
					curSubFilter += "!~~!" + 'NULL';
				} 
				if(subFilter !== "") {
					subFilter += "!~~!" + curSubFilter;
				} else {
					subFilter = curSubFilter;
				}
			} else {
				if(subFilter !== "") {
					subFilter += "!~~!" + 'NULL';
				} else {
					subFilter = 'NULL';
				}
			}
			
			if(mainFilter === "") {
				mainFilter = 'NULL';
				subFilter = 'NULL';
			}
			
			var cat = window.top.document.getElementById("hiddenFilterCategory").value;
			var hiddenCat = cat.split(",");
			var hiddenCatLength = hiddenCat.length;
			var hiddenMainFilter = mainFilter.split("!~~!");
			var mainLength = hiddenMainFilter.length;
			var hiddenSubFilter = subFilter.split("!~~!");
			var filter = "";
			var subFilters = "";
			var startLength = 0;
			if(hiddenCatLength > mainLength) {
				for(var m = 0; m < hiddenCatLength; m++) {
					if($("div."+hiddenCat[m]).length <= 0) {
						var assocaited = false;
						for(i = 0; i < hiddenCatLength; i++) {	
							if(i > 0) {
								if(i == m) {
									filter += "!~~!" + "NULL";
									assocaited = true;									
								} else {									
									if(assocaited == false) {
										filter += "!~~!" + hiddenMainFilter[i];
										var split = hiddenMainFilter[i].split("|");
										startLength = startLength + split.length;
									} else {
										filter += "!~~!" + hiddenMainFilter[i-1];
									}
								}
							} else {
								if(i == m) {
									filter = "NULL";
									assocaited = true;	
								} else {
									filter = hiddenMainFilter[i];
									var split = hiddenMainFilter[i].split("|");
									startLength = startLength + split.length;
								}
							}
						}
						assocaited = false;
						var hidsubFil = filter.split("!~~!");
						var hidsubFilLeng = hidsubFil.length;
						var leng = 0;
						for(var k = 0; k < hidsubFilLeng; k++) {
							var split = hidsubFil[k].split("|");
							leng = leng + split.length;
						}
						for(var j = 0; j < leng; j++) {
							if(j > 0) {
								if(j == startLength) {
									subFilters += "!~~!" + "NULL";
									assocaited = true;	
								} else {
									if(assocaited == false) {
										subFilters += "!~~!" + hiddenSubFilter[j];
									} else {
										subFilters += "!~~!" + hiddenSubFilter[j-1];
									}
								}
							} else {
								if(j == startLength) {
									subFilters = "NULL";
									assocaited = true;									
								} else {
									subFilters = hiddenSubFilter[j];
								}
							}
						}
					} 
				}
			} else {
				filter = mainFilter;
				subFilters = subFilter;
			}
			
			closeIframePopup('ifrmPopup','ifrm');
			window.top.document.getElementById("filters").value=filter;
			window.top.document.getElementById("filterValues").value=subFilters;
			window.top.document.getElementById("filterCategory").value=cat;
		});*/
		
		$("#associateCats").click(function() {
			var mainFilter = "";
			var i = 0;
			var curMainFilter = "";
			var selectedCat = "";
			
			$("div.col").each(function(){
				if(i > 0) {
					if(curMainFilter !== "") {
						if(mainFilter !== "") {
							mainFilter += "!~~!" + curMainFilter;
						} else {
							mainFilter = curMainFilter;
						}
					} else {
						if(mainFilter !== "") {
							mainFilter += "!~~!" + 'NULL';
						} else {
							mainFilter = 'NULL';
						}
					}
					
					curMainFilter = "";
					i = 0;
				}
				var ulId = $(this).attr("class").split(" ").slice(-1);
				if(selectedCat === "") {
					selectedCat = ulId;
				} else {
					selectedCat += "," + ulId;
				}
				$("#"+ulId+" input:checkbox:checked").each( function() {		
					if(curMainFilter === "") {
					  curMainFilter = $(this).val();
					} else {
					  curMainFilter += "|" + $(this).val();
					}
				});
				i++;
			});
				
			if(curMainFilter !== "") {
				if(mainFilter !== "") {
					mainFilter += "!~~!" + curMainFilter;
				} else {
					mainFilter = curMainFilter;
				}
			} else {
				if(mainFilter !== "") {
					mainFilter += "!~~!" + 'NULL';
				} else {
					mainFilter = 'NULL';
				}
			}
			
			if(mainFilter === "") {
				mainFilter = 'NULL';
			}
			
			var cat = window.top.document.getElementById("hiddensubCategory").value;
			var hiddenCat = cat.split(",");
			var hiddenCatLength = hiddenCat.length;
			var hiddenMainFilter = mainFilter.split("!~~!");
			var mainLength = hiddenMainFilter.length;
			var filter = "";
			if(hiddenCatLength > mainLength) {
				for(var m = 0; m < hiddenCatLength; m++) {
					if($("div."+hiddenCat[m]).length <= 0) {
						var assocaited = false;
						for(i = 0; i < hiddenCatLength; i++) {	
							if(i > 0) {
								if(i == m) {
									filter += "!~~!" + "NULL";
									assocaited = true;									
								} else {									
									if(assocaited == false) {
										filter += "!~~!" + hiddenMainFilter[i];
									} else {
										filter += "!~~!" + hiddenMainFilter[i-1];
									}
								}
							} else {
								if(i == m) {
									filter = "NULL";
									assocaited = true;	
								} else {
									filter = hiddenMainFilter[i];
								}
							}
						}
						assocaited = false;
					} 
				}
			} else {
				filter = mainFilter;
			}
			
			closeIframePopup('ifrmPopup','ifrm');
			window.top.document.getElementById("subCategories").value=filter;
			window.top.document.getElementById("subCategory").value=cat;
		});
					  
		$("input[name='chkAllOptns']").change(function(){
			var status = $(this).attr("checked");
			if(!status && "" !== $("#filters").val()) {
				$('#bCategory option').removeAttr('selected');				
				var isNull = false;
				var isSubCatAso = false;
				//For checking filter association
				var filCat = $("#filterCategory").val().split(",");
				if(filCat.length >= 1 && filCat[0] != "") {
					var filter_split = $("#filters").val().split("!~~!");
					for(var i=0; i<filCat.length; i++) {
						var isSelected = $('#bCategory option[value="' + filCat[i] + '"]').is(":selected");
						if(isSelected != true) {
							var newCat = "";
							var newFilt = "";
							var newSubFilt = "";
							var filStartLeng = 0;
							var filEndLeng = 0;
							if(filter_split[i] === 'NULL') {
								var subfilter_split = $("#filterValues").val().split("!~~!");
								for(var j=0; j<i; j++) {
									var filStr = filter_split[j].split("|");
									filStartLeng = filStartLeng + filStr.length;
								}
								filEndLeng = filter_split[i].split("|").length + filStartLeng;
								for(var k = 0; k < subfilter_split.length; k++) {
									if(k < filStartLeng || k >= filEndLeng) {
										if(newSubFilt === "") {
											newSubFilt = subfilter_split[k];
										} else {
											newSubFilt += "!~~!" + subfilter_split[k];
										}
									}
								}

								for(var m = 0; m < filter_split.length; m++) {
									if(m != i) {
										if(newFilt === "") {
											newFilt = filter_split[m];
										} else {
											newFilt += "!~~!" + filter_split[m];
										}
									} 
								}
								
								for(var n = 0; n < filCat.length; n++) {
									if(n != i) {
										if(newCat === "") {
											newCat = filCat[n];
										} else {
											newCat += "," + filCat[n];
										}
									} 
								}
								
								$("#filters").val(newFilt);
								$("#filterValues").val(newSubFilt);
								$("#filterCategory").val(newCat);
							} else {
								isNull = true;
								$('#bCategory option[value="' + filCat[i] + '"]').attr('selected', 'selected');
							}
						}
					}	
				}
				
				var subCat = $("#subCategory").val().split(",");
				if(subCat.length >= 1 && subCat[0] != "") {
					var subCat_split = $("#subCategories").val().split("!~~!");
					for(var i=0; i<subCat.length; i++) {
						var isSelected = $('#bCategory option[value="' + subCat[i] + '"]').is(":selected");
						if(isSelected != true) {
							var newCat = "";
							var newSubCat = "";
							if(subCat_split[i] === 'NULL') {
							
								for(var m = 0; m < subCat_split.length; m++) {
									if(m != i) {
										if(newSubCat === "") {
											newSubCat = subCat_split[m];
										} else {
											newSubCat += "!~~!" + subCat_split[m];
										}
									} 
								}
								
								for(var n = 0; n < subCat.length; n++) {
									if(n != i) {
										if(newCat === "") {
											newCat = subCat[n];
										} else {
											newCat += "," + subCat[n];
										}
									} 
								}
								
								$("#subCategories").val(newSubCat);
								$("#subCategory").val(newCat);
							} else {
								isSubCatAso = true;
								$('#bCategory option[value="' + subCat[i] + '"]').attr('selected', 'selected');
							}
						}
					}
				}
						
				/*if(isNull == true && isSubCatAso == true) {
					alert('Filters and Sub-Categories has been associated to the business categories. Please de-associate both and continue.');
				} else if(isNull == true) {
					alert('Filters has been associated to the business categories. Please de-associate Filters and continue.');
				} else */if(isSubCatAso == true) {
					alert('Sub-Categories has been associated to the business categories. Please de-associate Sub-Categories and continue.');
				}		
				
			} else {			
				$('#bCategory option').attr('selected', 'selected');
			}
			
			var isfltrTglChkd = $("#bCategory option.toggleFltr").is(":selected");
			
			if(isfltrTglChkd){
				$("#selectFilters").removeAttr("disabled").removeClass("disabled");
			}
			else {
				$("#selectFilters").attr("disabled" , "disabled").addClass("disabled");
			}	

			var isSubCatTglChkd = $("#bCategory option.toggleSubCat").is(":selected");
			
			if(isSubCatTglChkd){
				$("#selectSubCats").removeAttr("disabled").removeClass("disabled");
			}
			else {
				$("#selectSubCats").attr("disabled" , "disabled").addClass("disabled");
			}	
		});
	  
		//Check the checkbox if all options are selected & uncheck if any one option is not selected
		$('#bCategory').change(function() {	
			var totOpt = $(this).find("option").length;
			var totOptSlctd = $(this).find("option:selected").length;
			if(totOpt == totOptSlctd) {
				$("#chkAllCtrgy").attr('checked','true');
			}
			else {
				$("#chkAllCtrgy").removeAttr('checked');
			}			
			
			var isSubCatAso = false;
			var isNull = false;
			var filCat = $("#filterCategory").val().split(",");
			if(filCat.length >= 1 && filCat[0] != "" && "" != $("#filters").val()) {
				for(var i=0; i<filCat.length; i++) {
					var isSelected = $('#bCategory option[value="' + filCat[i] + '"]').is(":selected");
					if(isSelected != true) {
						var newCat = "";
						var newFilt = "";
						var newSubFilt = "";
						var filStartLeng = 0;
						var filEndLeng = 0;
						var filter_split = $("#filters").val().split("!~~!");
						if(filter_split[i] === 'NULL') {
							var subfilter_split = $("#filterValues").val().split("!~~!");
							for(var j=0; j<i; j++) {
								var filStr = filter_split[j].split("|");
								filStartLeng = filStartLeng + filStr.length;
							}
							filEndLeng = filter_split[i].split("|").length + filStartLeng;
							for(var k = 0; k < subfilter_split.length; k++) {
								if(k < filStartLeng || k >= filEndLeng) {
									if(newSubFilt === "") {
										newSubFilt = subfilter_split[k];
									} else {
										newSubFilt += "!~~!" + subfilter_split[k];
									}
								}
							}

							for(var m = 0; m < filter_split.length; m++) {
								if(m != i) {
									if(newFilt === "") {
										newFilt = filter_split[m];
									} else {
										newFilt += "!~~!" + filter_split[m];
									}
								} 
							}
							
							for(var n = 0; n < filCat.length; n++) {
								if(n != i) {
									if(newCat === "") {
										newCat = filCat[n];
									} else {
										newCat += "," + filCat[n];
									}
								} 
							}
							
							$("#filters").val(newFilt);
							$("#filterValues").val(newSubFilt);
							$("#filterCategory").val(newCat);
						} else {
							isNull = true;
							$('#bCategory option[value="' + filCat[i] + '"]').attr('selected', 'selected');
						}					
						
					}
				}
			}
			
			var subCat = $("#subCategory").val().split(",");
			if(subCat.length >= 1 && subCat[0] != "") {
				var subCat_split = $("#subCategories").val().split("!~~!");
				for(var i=0; i<subCat.length; i++) {
					var isSelected = $('#bCategory option[value="' + subCat[i] + '"]').is(":selected");
					if(isSelected != true) {
						var newCat = "";
						var newSubCat = "";
						if(subCat_split[i] === 'NULL') {
						
							for(var m = 0; m < subCat_split.length; m++) {
								if(m != i) {
									if(newSubCat === "") {
										newSubCat = subCat_split[m];
									} else {
										newSubCat += "!~~!" + subCat_split[m];
									}
								} 
							}
							
							for(var n = 0; n < subCat.length; n++) {
								if(n != i) {
									if(newCat === "") {
										newCat = subCat[n];
									} else {
										newCat += "," + subCat[n];
									}
								} 
							}
							
							$("#subCategories").val(newSubCat);
							$("#subCategory").val(newCat);
						} else {
							isSubCatAso = true;
							$('#bCategory option[value="' + subCat[i] + '"]').attr('selected', 'selected');
						}
					}
				}
			}
			
			/*if(isNull == true && isSubCatAso == true) {
				alert('Filters and Sub-Categories has been associated to the business categories. Please de-associate both and continue.');
			} else if(isNull == true) {
				alert('Filters has been associated to the business categories. Please de-associate Filters and continue.');
			} else*/ if(isSubCatAso == true) {
				alert('Sub-Categories has been associated to the business categories. Please de-associate Sub-Categories and continue.');
			}
			
			var isfltrTglChkd = $("#bCategory option.toggleFltr").is(":selected");
			if(isfltrTglChkd){
				$("#selectFilters").removeAttr("disabled").removeClass("disabled");
			}
			else {
				$("#selectFilters").attr("disabled" , "disabled").addClass("disabled");
			}	
			
			var isSubCatTglChkd = $("#bCategory option.toggleSubCat").is(":selected");
			
			if(isSubCatTglChkd){
				$("#selectSubCats").removeAttr("disabled").removeClass("disabled");
			}
			else {
				$("#selectSubCats").attr("disabled" , "disabled").addClass("disabled");
			}	
								  
		});
		
		var totOpt = $("#bCategory option").length;
		var totOptSlctd = $("#bCategory option:selected").length;
		if(totOpt == totOptSlctd) {
			$("#chkAllCtrgy").attr('checked','true');
		} else {
			$("#chkAllCtrgy").removeAttr('checked');
		}
		var isfltrTglChkd = $("#bCategory option.toggleFltr").is(":selected");
		if(isfltrTglChkd){
			$("#selectFilters").removeAttr("disabled").removeClass("disabled");
		} else {
			$("#selectFilters").attr("disabled" , "disabled").addClass("disabled");
		}
		
		var isSubCatTglChkd = $("#bCategory option.toggleSubCat").is(":selected");
		
		if(isSubCatTglChkd){
			$("#selectSubCats").removeAttr("disabled").removeClass("disabled");
		}
		else {
			$("#selectSubCats").attr("disabled" , "disabled").addClass("disabled");
		}	
	  
		/*$("#selectFilters").attr("disabled" , "disabled").addClass("disabled");

	  
		if ($("#bCategory option:selected").hasClass('toggleFltr')) {
			$("#selectFilters").removeAttr("disabled").removeClass("disabled");
		}*/
		/*----------Update Account: Select Category & Displaying popup with filter & associated values ends------*/

	
	$(".tgl-overlay").click(function(){
		$(".overlay").show();
	});
	$(".closeIframe").click(function(){
		$(".overlay").hide();
	});
	
	
	/* Portal to update events - Retailers.*/
	
	$("input[name='recurrencePatternID']").click(function(){
		dsplyRecur(this);
	});	

	$("#cstmDrpdwnInput").click(function(){
		$(this).next('.cstmDropdwn').toggle();
	});

	
	/* custom drop down control with checkbox & display selected value in input field*/
	$(".cstmDropdwn").find('li').click(function(e){
	/*	if (e.target.tagName.toUpperCase() === "LABEL") {
	        return;
	      }*/
		var $prnt = $(this);
		$(this).toggleClass("mul-slct");
		var $chkbx = $prnt.find(".recDays");
		if($prnt.hasClass('mul-slct')) {
	      $chkbx.attr('checked', true);
		  updateCstmSlct();
	    } else {
	      $chkbx.attr('checked', false);
		  updateCstmSlct();
	    }
	});
	
	
	/*close custom drop down control when clicked outside of the control allowing multi click inside*/
	$(document).click(function(e) {
		var target = $(e.target);  
		var curContx = target.parents().hasClass("cstmSlct");
		if (!curContx) {
			$(".cstmDropdwn").hide();
		}
		
	});


	 /*Remove duplicate values from multi select*/ 
	 //testDup();	

	var recurChkd = $("input[name='recurrencePatternID']:radio:checked").attr("id");
	$(".recurrence").show();
	$("."+recurChkd).show();

	/*if(!$("#onGoing").prop("checked")){
		$(".ongoing").css("display","none");
	}*/
		
		/*ongoing event option change display related form elements*/
	$("input[name='isOngoing']").bind('change.trgrOngng',function(){
		var evntOptn = $(this).attr('value');
		if(evntOptn=="yes"){
			$(".ongoing").show();
			//$(".ongoing").show();
			$(".scngRow,.not-ongng").hide();
		}
		else if(evntOptn=="no") {
			$(".ongoing,.scngRow").hide();
			$(".not-ongng").show();
			
		}
	});

	$('input[name="isOngoing"]:checked').trigger('change.trgrOngng');
		
	$("input[name='bsnsLoc']").change(function() {
	var reqOptn = $(this).attr('value');
	//var getId = $(this).attr('id');
	if(reqOptn==="yes"){
		$(".evntLoctn").hide();
	$(".bsnsLoctn").show();	
		if($("#mngevntTbl").length){
			tblHt = $("#mngevntTbl").height();
	}
		$('body,html').animate({scrollTop: $(".content").prop("scrollHeight")}, 1400);
		/*$("#menu-pnl").height($(".content").height()).trigger("resize");*/
	}
	else if(reqOptn==="no"){
		$(".bsnsLoctn").hide();	
		$(".evntLoctn").slideDown();
		//$("#menu-pnl").height(adjust);
		$('body,html').animate({scrollTop :0}, 'slow');
	}
	});

	/*------End ----- Portal to update events - Retailers.*/
	
	/* AnyThing Page Dragging and dropping table row to another position. */
	if ($("#table-1").length > 0) {
		$("#table-1").tableDnD();
	}
	
	/* Special Offer Dragging and dropping table row to another position. */
	if ($("#dealLst").length > 0) {
		$("#dealLst").tableDnD();
	}

	/*
	 * Add Product to Location popup : Web 1.3
	 * $('.textboxDate').datepicker();
	 */
	/*
	 * Applying Button $('#applyAll').click(function() {
	 * $('#tglOptions').slideToggle('normal');
	 * $('#srchCntrls').slideUp('normal'); });
	 * 
	 * Search Button $('#searchBar').click(function() {
	 * $('#tglOptions').slideUp('normal');
	 * $('#srchCntrls').slideToggle('normal'); });
	 */

	/* Progress Bar */
	// $("#loading-image").hide();
	if ($.fn.tableScroll) {
		if (document.getElementById('addProdLoctable')) {
			$('#addProdLoctable').tableScroll({
				height : 180
			}); // height for customization
		}
	}

	/*--End : Web 1.3*/

	var grdRowCnt = $('#locSetUpGrd tr').length;
	var scrolDiv = $('.scrollingDashboard,.hScroll,.scrolling');
	if (grdRowCnt == 1) {
		$('.scrollingDashboard,.hScroll,.scrolling').css({
			'height' : 'auto',
			'overflow-x' : 'hidden'
		});
		$('.navTabSec').hide();
		$('#locSetUpGrd').hide();
	} else if ($(scrolDiv).height() >= 325) {
		$('.scrollingDashboard,.hScroll,.scrolling').css({
			'height' : '325px',
			'overflow-x' : 'scroll'
		});
	} else if ($(scrolDiv).height() <= 325) {
		$('.scrollingDashboard,.hScroll,.scrolling').css({
			'height' : 'auto',
			'overflow-x' : 'scroll'
		});
		// $('.pagination').hide();
	}

	/* Validation in ManageLocation for US state */
	$('input[name="state"]')
			.keydown(
					function(event) {
						$(this).css("text-transform",
								"uppercase");
						// Allow: backspace, delete, tab,
						// escape, and enter
						if (event.keyCode == 46
								|| event.keyCode == 8
								|| event.keyCode == 9
								|| event.keyCode == 27
								|| event.keyCode == 13
								||
								// Allow: Ctrl+A
								(event.keyCode == 65 && event.ctrlKey === true)
								||
								// Allow: home, end, left, right
								(event.keyCode >= 35 && event.keyCode <= 39)) {
							return;
						} else {
							// Ensure that it is a number and
							// stop the keypress
							if (event.shiftKey
									|| (event.keyCode < 65 || event.keyCode > 90)
									&& (event.keyCode < 97 || event.keyCode > 122)) {
								event.preventDefault();
							}
							/*
							 * Will not allow numpad from 0 to
							 * 9,.,/,*,-,+
							 */
							if (event.keyCode >= 96
									&& event.keyCode <= 111) {
								event.preventDefault();
							}
						}
					});

	var rwcnt = $('#prodinfotbl tr').length;
	if (rwcnt <= 1) {
		$('.rtlrMngProdDiv').css("display", "none");
	} else {
		$('.rtlrMngProdDiv ').show();
	}

	$('#storeNum').hide();
	$('input[name="islocation"]').click(function() {
		var curChkBx = $(this);
		if ($(curChkBx).attr('checked')) {
			$('#storeNum').show();
		} else {
			$('#storeNum').hide();
		}
	});
	

	if (isTE8) {
		$('html').addClass('ie8');
	}
	
	if (isChrome) {
		$('html').addClass('chrome');
		
	}
	

//$("#trgrUpldBtn").click(function() { $("#trgrUpld").click();  });
	
		


	$('.trgrUpldCrtdPage').click(function() {
		if (!isIE && !isTE8)
			$('#trgrUpldCrtdPage').click(function(event) {
				event.stopPropagation();
			});
	});
	$('.trgrUpldSplPage').click(function() {
		if (!isIE && !isTE8)
			$('#trgrUpldSplPage').click(function(event) {
				event.stopPropagation();
			});
	});
	$('.trgrClick tr:eq(1) td').click(function(event) {
		if (event.target.type !== 'radio') {
			$(':radio', this).trigger('click');
		}
		$('.trgrClick').find(".active").removeClass("active");
		$(this).addClass("active");
	});
	$('.trgrClick tr:eq(1) td').click(
			function() {
				var gtTblCls = $(this).find(
						'input[type="radio"]').attr('value');
				$('.cstmpg').hide();
				$('#' + gtTblCls).show();
				$('#slctSpclOffr,#lndgPgSlct')
						.trigger('change');
			});
	$('.addBG :radio:checked').parent('td').trigger('click');

	// toggle input field in FIND module.
	if ($('select[name$="searchType"] option:selected').text() == "Product") {
		$('input[name$="search_find"]').show();
	}
	$('select[name$="searchType"]')
			.change(
					function() {

						if ($(
								'select[name$="searchType"] option:selected')
								.text() == "Product") {
							$('.chngImg')
									.attr('src',
											'../images/scanseeSrchIcon.png')
									.show();
							$('.stretch').hide();
						} else {
							$('.chngImg').attr('src',
									'../images/googleIcon.png')
									.show();
							$('.stretch').show();
						}
					});

	$('input[name$="deleteAll"]').click(
			function() {
				var status = $(this).attr("checked");
				$('input[name$="checkboxDel"]').attr('checked',
						status);
				if (!status) {
					$('input[name$="checkboxDel"]').removeAttr(
							'checked');
				}
			});
	$('input[name$="checkboxDel"]')
			.click(
					function() {
						var tolCnt = $('input[name$="checkboxDel"]').length;
						var chkCnt = $('input[name$="checkboxDel"]:checked').length;
						if (tolCnt == chkCnt)
							$('input[name$="deleteAll"]').attr(
									'checked', 'checked');
						else
							$('input[name$="deleteAll"]')
									.removeAttr('checked');
					});

	$('#tutorial')
			.click(
					function() {

						$('#mainMenu').slideUp();
						$('#mainMenuTutorial').slideDown();
						/*
						 * $('#mainMenu').hide('slide',
						 * {direction: 'left'}, 1000);
						 * $('#mainMenuTutorial').show('slide',
						 * {direction: 'right'}, 500);
						 */
						$('.mobCont')
								.append(
										"<div class='videoDsply'>"
												+ "<video  id='videoPlay'  type='video/ogg' poster='../images/videoPoster.png' width='730' height='500' controls='controls' autoplay='true'>"
												+ +" </video> "
												+ "</div>");

						$("#mainMenuTutorial li")
								.click(
										function() {
											var $this = $(this);
											var videoObj = document
													.getElementById('videoPlay');
											if ($this
													.attr('videoSrc')) {
												videoObj
														.setAttribute(
																'src',
																$this
																		.attr('videoSrc'));
												videoObj
														.setAttribute(
																'type',
																'video/ogv');
											}

										});

					});
	$('#tglMM').click(function() {
		$('#mainMenu').slideDown();
		$('#mainMenuTutorial').slideUp();
		$('.videoDsply').remove();
	});

	$('select[name="searchType"] option:first').attr(
			'selected', 'selected');

	$('img[name$="DeleteRow"]').click(function() {
		var catname = $(this).parents('tr').attr('name');
		// alert(catname);
		var catCnt = $('tr[name="' + catname + '"]').length;
		// alert(cpnCnt);
		if (catCnt <= 2) {
			// alert("Last Row: Deleting " + catname
			// + " Section");
			$('tr[name="' + catname + '"]:first').remove();
		}
	});
	$('img[name$="DeleteRowG"]').click(function() {
		var catname = $(this).parents('tr').attr('name');
		// alert(catname);
		var catCnt = $('tr[name="' + catname + '"]').length;
		// alert(cpnCnt);
		if (catCnt <= 2) {
			// alert("Last Row: Deleting " + catname
			// + " Section");
			$('tr[name="' + catname + '"]:first').remove();
		}
	});
	$('li #actSht').click(function() {
		$('#actionSheet').slideToggle('medium');
	});
	$('img[name$="actnCncl"],.actnShtLst li a').click(
			function() {
				$('#actionSheet').slideUp('medium');
			});
	/*----------------------For Shopper Module calculate mobLink height and assign to dataView & fluid view divs----------------------------- */
	$('.prefDblChk input[type="checkbox"]').attr('disabled',
			'true');
	var mnHt = $(".mobLink").height();
	$(".dataView .fluidView div").height(mnHt);

	$(
			".dataView .fluidView div,.dataView .fluidViewHD div.cmnPnl")
			.height(mnHt);

	if ($(".topSubBar")) {
		var subBarHt = $(".topSubBar").height();
		$(".dataView,.fluidView div,.fluidViewHD div.cmnPnl")
				.height(mnHt - subBarHt - 2);
	}
	$('.mobGrd td img[name$="DeleteRowG"]').hide();
	$('.mobGrd td').live(
			'mouseover',
			function() {
				$(this).closest('tr').find(
						'img[name$="DeleteRowG"]').show();
			}).live(
			'mouseout',
			function() {
				$(this).closest('tr').find(
						'img[name$="DeleteRowG"]').hide();
			});

	$('#searchhdbtm').hide();
	$('img[name$="ByMap"]').click(function() {
		$('#mapviews').show();
		$('#searchhdbtm').show();
		$('#hotDealDetails,#subnavTab').hide();

		$('#HotDeal_listView').click(function() {
			$('#mapviews').hide();
		});
	});
	/*
	 * if ($.browser.msie && parseInt($.browser.version) == 7) {
	 * //alert('IE7'); $('.tabsSct').css("padding-top","150px");
	 * $('.dockWrpr').css("margin-bottom","5px"); } else {
	 * $('.tabsSct').css("height","158px"); }
	 */

	// For displaying banner ad & ribbon ad
	/*
	 * $("#ribbonAd").hide(); $("#onlyAds").hide();
	 * $("#bannerAd").delay(2000).fadeOut('slow', function() {
	 * $("#ribbonAd,#onlyAds").show() });
	 */

	$("#ribbonAd").css('border-bottom', '1px solid #e3e3e3');

	$(".dataView").css("overflow-x", "hidden");
	$(".fluidView div").css("overflow-x", "hidden");
	$(".fluidViewHD div.cmnPnl").css("overflow-x", "hidden");
	// $(".dataView #rtlrLst").hide();

	// $("#hotdealList").hide();
	$("#subnav a").click(function() {
		$("#subnav a").removeClass('active');
		$(this).addClass('active')
		if ($(this).attr('id') == "prodList") {
			$("#prodList").show();
			$("#hotdealList").hide();

		} else {
			$("#hotdealList").show();
			$("#prodList").hide();

		}
	});

	// For Shopper Module
	var mnHt = $(".mobLink").height();
	// alert(mnHt);
	$(".dataView .fluidView div").height(mnHt);
	if ($(".topSubBar")) {
		var subBarHt = $(".topSubBar").height();
		$(".dataView,.fluidView div").height(
				mnHt - subBarHt - 2);
	}

	$(".dataView").css("overflow-x", "hidden");
	$(".fluidView div").css("overflow-x", "hidden");
	// $(".dataView #rtlrLst").hide();

	$(".htlt tr").mouseover(function() {
		$(this).addClass("hvrEfct");
		$(this).css("cursor", "pointer");
	}).mouseout(function() {
		$(this).removeClass("hvrEfct")
	});

	if ($.fn.tableScroll) {
		if (document.getElementById('thetable')) {
			$('#thetable').tableScroll({
				height : 120
			}); // height for customization

		}

	}

	var $scrollingDiv = $("#verticalNav");

	$(window).scroll(function() {
		// console.log($(window).scrollTop())
		if ($(window).scrollTop() > 95) {
			$scrollingDiv.stop().animate({
				"top" : $(document).scrollTop()
			}, "slow").css('position', 'absolute');
		} else {
			$scrollingDiv.css('position', 'static');
		}
	});
	$(document).keypress(
			function(e) {
				// for enter key & space bar.
				if (e.which == 13 || e.which == 32) {
					// alert('You pressed enter!');
					var bool = $('#loginSec')
							.find('ul.relogin').length > 0;
					if (bool) {
						validateUserForm([ '#pswdNew',
								'#pswdCfrm' ], 'li', chkUser);
					} else {
						validateUserForm(
								[ '#pswd', '#userName' ], 'li',
								chkUser);
					}
				}
			});
	$(".stripeMe tr").mouseover(function() {
		$(this).addClass("over");
	}).mouseout(function() {
		$(this).removeClass("over");
	});
	$(".prodSummary td:first-child").addClass("oddRow");
	$(".stripeMe tr:even").addClass("hvrEfct");

	/*
	 * $('.secRw li input').last().click(function() { /* var
	 * pathname = window.location.pathname; pathname =
	 * pathname.substring(0,pathname.substring(7).indexOf("/")+8);
	 * alert(pathname[1]);
	 * 
	 * alert(pathname); window.location.href =
	 * 'searchResults.html';
	 * 
	 * 
	 * 
	 * });
	 */

	/*
	 * $("#forWard").click(function() { alert("Forward");
	 * 
	 * });
	 */

	$("#prgMtr li").children().each(function(i) {
		$(this).click(function() {

			// alert(index);
			$('#prgMtr li').eq(1).click(function(e) {
				$("#glassoverlay").stop().animate({
					top : '-115px'
				}, {
					queue : true,
					duration : 1200
				});

				// alert("hi");
			});
			$('#prgMtr li').eq(2).click(function() {
				$("#glassoverlay").stop().animate({
					top : '-130px'
				}, {
					queue : true,
					duration : 1200
				});
				// alert("hi");
			});
			$('#prgMtr li').eq(3).click(function() {
				$("#glassoverlay").stop().animate({
					top : '-145px'
				}, {
					queue : true,
					duration : 1200
				});
				// alert("hi");
			});
			$('#prgMtr li').eq(4).click(function() {
				$("#glassoverlay").stop().animate({
					top : '-160px'
				}, {
					queue : true,
					duration : 1200
				});
				// alert("hi");
			});
			$('#prgMtr li').eq(5).click(function() {
				$("#glassoverlay").stop().animate({
					top : '-175px'
				}, {
					queue : true,
					duration : 1200
				});
				// alert("hi");
			});
			$('#prgMtr li').eq(6).click(function() {
				$("#glassoverlay").stop().animate({
					top : '-205px'
				}, {
					queue : true,
					duration : 1200
				});
				// alert("hi");
			});
		});

	});

	$(".btnNav").click(function() {
		window.location = $(this).find("a").attr("href");
		return false;
	});
	$("#slide").hide();

	$("#slide").hide();
	$("#main").mouseenter(function() {

		if ($("*:animated").attr('id') == 'main') {
			return false;
		}
		$(this).slideUp('slow', 'jswing');
		$("#slide").slideDown('slow', 'jswing');
	});
	$("#slide").mouseleave(function() {
		if ($("*:animated").attr('id') == 'slide') {
			return false;
		}
		$(this).slideUp('slow', 'jswing');
		$("#main").slideDown('slow', 'jswing');
	});

	$("#Homeslide").hide();
	$("#main_home").mouseenter(function() {
		if ($("*:animated").attr('id') == 'main_home') {
			return false;
		}
		$(this).slideUp('slow', 'jswing');
		$("#Homeslide").slideDown('slow', 'jswing');
	});
	$("#Homeslide").mouseleave(function() {
		if ($("*:animated").attr('id') == 'Homeslide') {
			return false;
		}
		$(this).slideUp('slow', 'jswing');
		$("#main_home").slideDown('slow', 'jswing');
	});

	$("#Anywhereslide").hide();
	$("#anyWhere_home").mouseenter(function() {
		if ($("*:animated").attr('id') == 'anyWhere_home') {
			return false;
		}
		$(this).slideUp('slow', 'jswing');
		$("#Anywhereslide").slideDown('slow', 'jswing');
	});
	$("#Anywhereslide").mouseleave(function() {
		if ($("*:animated").attr('id') == 'Anywhereslide') {
			return false;
		}
		$(this).slideUp('slow', 'jswing');
		$("#anyWhere_home").slideDown('slow', 'jswing');
	});

	//theRotator();
	$('div.rotator').fadeIn(1000);
	$('div.rotator ul li').fadeIn(1000); // tweek for IE

	$('img.NextNav').hover(function() {
		this.src = '/ScanSeeWeb/images/nextBtnHover.png';
	}, function() {
		this.src = '/ScanSeeWeb/images/nextBtn.png';
	});
	$('img.NextNav_R').hover(function() {
		this.src = '/ScanSeeWeb/images/nextBtnHover.png';
	}, function() {
		this.src = '/ScanSeeWeb/images/nextBtn.png';
	});
	/* Prevent bottom border for last li */
	$("#VertNav ul li:last-child").css("background-image",
			"none");

	// To avoid border for last td in each row
	$("table tr td:not(:last-child)").css({
		"border-right" : "1px solid #dadada"
	});
	$(".searchGrd table tr td:not(:last-child)").css({
		"border-right" : "1px solid #c1e4f0"
	});
	// $('#nav li').last().css('background', 'none');
	/*
	 * $('#clearFrm').click(function() {
	 * $('form').clearForm();// to clear form data });
	 */

	$('#iphoneMenu').slideDown();

	var collaspeCls = '.searchGrd h1[class="searchHeaderCollaspe"]';
	var expandCls = '.searchGrd h1[class="searchHeaderExpand"]';
	var secContentPanel = '.grdCont';
	$('.searchGrd h1').attr("title", "Collapse");

	$(expandCls).click(
			function() {
				if ($(this).hasClass('searchHeaderExpand')) {
					$('.searchGrd h1').attr("title", "Expand");
					$(this).removeClass('searchHeaderExpand')
							.addClass('searchHeaderCollaspe');
					$(this).next(secContentPanel).slideUp(
							'jswing');

				} else {
					$('.searchGrd h1')
							.attr("title", "Collapse");
					$(this).removeClass('searchHeaderCollaspe')
							.addClass('searchHeaderExpand');
					$(this).next(secContentPanel).slideDown(
							'jswing');

				}
			});

	/*
	 * $('.tabs a').click(function(e) {
	 * if($(this).is('.tabActive')) return false; var index =
	 * $(this).parent('li').index(); highLightImg(index,e.type)
	 * }); actIndex = $('.tabs
	 * a.tabActive').parent('li').index()
	 * if(document.getElementById('menu')){ var t =
	 * setTimeout(function() { highLightImg(actIndex,'click')
	 * },100); }
	 */
	$('.tglSec').hide();
	$('#togglePnl a')
			.click(
					function() {
						var $this = $(this);
						var $img = $this.find('img');
						var imgSrc = $img.attr('src');
						var imgPath = imgSrc.substr(0, imgSrc
								.lastIndexOf('/'));

						$('.tglSec')
								.slideToggle(
										'fast',
										function() {
											var htmlTest = ($(
													this).css(
													'display') == "block") ? "<img src='"
													+ imgPath
													+ "/upBtn.png'/> Hide Panel"
													: "<img src='"
															+ imgPath
															+ "/downBtn.png'/> Show Panel"
											$this
													.html(htmlTest);
										});
					});

	/*------------------------------------------Shopper Module tab bar icons rollover effect Starts-----------------------------------*/
	$(".tabBar li img,#subnavTab li img").hover(function() {
		this.src = this.src.replace("_up", "_down");
	}, function() {
		this.src = this.src.replace("_down", "_up");
	});
	// to remove highlight of a tab after adding once
	if ($('.tabBar li').hasClass('noHvr')) {

		$('.tabBar li:eq(1) img').unbind(
				'mouseenter mouseleave');
		$('.tabBar li:eq(1) img').css('cursor', 'default');
	}

	/*
	 * if ($.fn.ticker) { if
	 * (document.getElementById('js-news')) {
	 * $('#js-news').ticker(); } }
	 */

	$("#cnfrm").click(
			function() {
				$('.cnfrmImg').attr('src',
						"../images/confirmicon.png");
				$('.cnfrmImg').attr('title', "Confirmed");
				var Imgtitle = $('.cnfrmImg').attr("title");
				$('em').text(Imgtitle).fadeOut('slow');

				$('#cnfrm').attr('disabled', 'disabled');

			});
	$('#cnfrm').removeAttr('disabled', 'disabled');
	$("tr td ul li a.delRow").click(function() {
		// alert($(this).parent().parent().parent().parent().parent().html());
		// $(this).parent().parent().parent().parent().remove();
		alert("Row Deletion");
		$(this).parents('tr:eq(0)').remove();
	});

	/*
	 * hightlight table row & add/remove read only attr for
	 * inputs
	 */
	$("#highLight tr input").attr("readonly", "readonly");
	$("#highLight tr img").css('cursor', 'pointer');
	$("#highLight tr:gt(0)").click(function() {
		// $(this).css("background-color","red");
		$("#highLight tr").not(this).removeClass('hilite');
		/*
		 * debugger; $(this).find("input").each(function(index){
		 * var $thisVal = $(this).get(); var temp =
		 * $thisVal[0].value; });
		 */
		$(this).closest("tr").addClass('hilite');
		$("#highLight tr input").removeAttr("readonly");

	});
	$("#locSetUpGrd input").attr("readonly", "readonly");
	$("#locSetUpGrd tr:gt(0)").click(
			function(e) {
				$("#locSetUpGrd").find('tr.hilite')
						.removeClass('hilite').find("input")
						.attr("readonly", "true");

				$(this).addClass('hilite').find("input")
						.removeAttr("readonly");
				if (e.target.nodeName == "INPUT") {
					e.target.select();
				}
			});

	radioSel('slctOpt');
	$('input[name="slctOpt"]').click(function() {
		radioSel('slctOpt')
	});
	$('.rtlrPnl tr').mouseover(function() {
		$(this).addClass("hvrEfct");
		$(this).css("cursor", "pointer");
	}).mouseout(function() {
		$(this).removeClass("hvrEfct");
	});
	$('.rtlrPnl tr.newWnd').click(function() {
		var $curLink = $(this).find('a').attr('href');
		window.open($curLink, '_blank');
		// window.location.href=$curLink;

	});

	$('.mobGrd td img[name$="DeleteRow"]').hide();
	$('.mobGrd td').live(
			'mouseover',
			function() {
				$(this).closest('tr').find(
						'img[name$="DeleteRow"]').show();
			}).live(
			'mouseout',
			function() {
				$(this).closest('tr').find(
						'img[name$="DeleteRow"]').hide();
			});
	$('.mobGrd td img[name$="DeleteRow"]').click(function() {

		$(this).closest('tr').remove();
		/*
		 * var grpdRow = $('#alertedItmLst tr
		 * td.grpdtxt').length;
		 * 
		 * alert(grpdRow);
		 */
	});
	$('#HotDeal_listView tr').not('thead tr').click(function() {

		$('#subnavTab').show();
	});

	if ($('.preferencesLst tbody tr').hasClass('mainCtgry')) {
		$('.preferencesLst tbody tr:not(.mainCtgry)').hide();
	}
	$(".preferencesLst tr.mainCtgry input:checkbox").click(
			function() {
				var maincatNm = $(this).parents('tr').attr(
						'name');
				var self = this;
				if (self.checked) {
					$(
							'.preferencesLst tr[name="'
									+ maincatNm + '"]').show();
					var cnt = $('.preferencesLst tr[name="'
							+ maincatNm + '"]').length;
					$('tr[name="' + maincatNm + '"]').each(
							function(i) {
								if (i > 0 && self.checked) {
									$(this).find(
											'input:checkbox')
											.attr('checked',
													'checked')
								}
							});
				} else {
					$(
							'.preferencesLst tr:not(.mainCtgry)[name="'
									+ maincatNm + '"]').hide();
					/*
					 * $('tr[name="'+maincatNm+'"]').each(function(i) {
					 * $(this).find('input:checkbox').attr('checked',
					 * self.checked) });
					 */
				}
			});

	$(".preferencesLst tr:not(.mainCtgry) input:checkbox")
			.click(
					function() {
						var maincatNm = $(this).parents('tr')
								.attr('name');
						if ($('tr:not(.mainCtgry)[name="'
								+ maincatNm
								+ '"] :checkbox:checked').length < 1) {
							$('tr[name="' + maincatNm + '"]')
									.each(
											function(i) {
												if (i == 0) {
													$(this)
															.find(
																	'input:checkbox')
															.removeAttr(
																	'checked');
												} else {
													$(this)
															.hide();
												}
											});
						}
						var totChk = $('.preferencesLst tr:not(.mainCtgry)[name="'
								+ maincatNm + '"]:checkbox').length;
						var chkLen = $('.preferencesLst tr:not(.mainCtgry)[name="'
								+ maincatNm
								+ '"]:checkbox:checked').length;
						if (chkLen == totChk)
							$(
									'.preferencesLst tr.mainCtgry[name="'
											+ maincatNm
											+ '"]:checkbox')
									.attr('checked', 'checked');
					});

	$(".preferencesLst input:checkbox:checked")
			.each(
					function() {
						var maincatNm = $(this).parents('tr')
								.attr('name');

						$(
								'.preferencesLst tr[name="'
										+ maincatNm + '"]')
								.show();
						var totChk = $('.preferencesLst tr:not(.mainCtgry)[name="'
								+ maincatNm + '"]:checkbox').length;
						var chkLen = $('.preferencesLst tr:not(.mainCtgry)[name="'
								+ maincatNm
								+ '"]:checkbox:checked').length;
						if (chkLen == totChk)
							$(
									'.preferencesLst tr.mainCtgry[name="'
											+ maincatNm
											+ '"]:checkbox')
									.attr('checked', 'checked');
					});

	$("#subnav.tabdSec a").click(function() {
		$(this).removeClass('active');
		$(this).addClass('active')
		if ($(this).attr('id') == "prodDet") {
			$('table.prodDet').show();
			$('table.reviewDet').hide();
		} else {
			$('table.reviewDet').show();
			$('table.prodDet').hide();

		}
	});
	// left navigation related code
	$('#vNav li.mainTab').click(
			function() {
				var curtSec = $(this).attr('name');
				var chkLen = $('#vNav li[name="' + curtSec
						+ '"].subTab').length;
				if (chkLen > 1) {
					tglTabs(this);
				}
			});

	// To avoid border for last td in each row
	$("table tr td:not(:last-child)").css({
		"border-right" : "1px solid #dadada"
	});
	$(".searchGrd table tr td:not(:last-child)").css({
		"border-right" : "1px solid #c1e4f0"
	});

	$('input[name$="chkAllLoc"]').click(function() {

		var status = $(this).attr("checked");
		$('.retLocId option').attr('selected', status);
		if (!status) {
			$('.retLocId option').removeAttr('selected');
		}
	});

	$('input[name="pageType"]').click(function() {
		var getLink = $(this).attr('value');
		document.buildAnythingPage.action = getLink;
		document.buildAnythingPage.method = "GET";
		document.buildAnythingPage.submit();
	});

	$("#retPageShortDescription").keyup(function(event) {
		var stt = $(this).val();
		// $(".shrtDesc").text(stt);
		var reNewLines = /[\n\r]/g;
		$(".shrtDesc").html(stt.replace(reNewLines, "<br />"));
	});
	$("#rtlrshrtDsc").keyup(function(event) {
		var ltt = $(this).val();
		// $(".lngDesc").text(ltt);
		var reNewLines = /[\n\r]/g;
		$(".lngDesc").html(ltt.replace(reNewLines, "<br />"));
	});
	$("#retPageTitle").keyup(function(event) {
		var rtt = $(this).val();
		$("h2").text(rtt);
	});

	// Highlight selected image
	$('.iconsPnl li a img').click(function() {
		$('.iconsPnl li a img').removeClass('active');
		$(this).addClass("active");
	});

	$('input[name="pageType1"]').click(function() {
		var getLink = $(this).attr('value');
		document.createsplofferform.action = getLink;
		document.createsplofferform.method = "GET";
		document.createsplofferform.submit();
	});
	
});
				
				

function radioSel(radioName) {
	$('tr.slctCty').hide();
	$('tr.slctLoc').hide();
	var cls = $('input[name ="' + radioName + '"]:checked').val();
	if (cls == "slctLoc") {
		var productId = document.addhotdealform.prodcutID.value;
		loadPdtRetailer();
	} else if (cls == "slctCty") {
		$('tr.' + cls).show();
	}

}

function loadPdtRetailer() {
	var productId = $('#prodcutID').val();
	/*
	 * if (name == null) { alert("Select Retailer;"); }
	 */
	$.ajax({
		type : "GET",
		url : "fetchpdtretailer.htm",
		data : {
			'productId' : productId
		},

		success : function(response) {
			$('#myAjax2').html(response);
		},
		error : function(e) {

		}
	});
}

function showHide(obj) {
	var frm = document.forms[0];
	if (obj && obj.checked) {
		$("deletedAddresses").show();
		$("existingAddresses").hide();

	} else {
		$("existingAddresses").show();
		$("deletedAddresses").hide();

	}
}

/*
 * function updateProfile() { alert("From the global.js");
 * document.editprofileform.action = "editProfile.htm";
 * document.editprofileform.method = "POST"; document.editprofileform.submit(); }
 */

// move selected option form source to destination list.
function move_list_items(sourceid, destinationid) {
	$("#" + sourceid + "  option:selected").appendTo("#" + destinationid);
}

function theRotator() {
	// Set the opacity of all images to 0
	$('div.rotator ul li').css({
		opacity : 0.0
	});

	// Get the first image and display it (gets set to full opacity)
	$('div.rotator ul li:first').css({
		opacity : 1.0
	});

	// Call the rotator function to run the slideshow, 6000 = change to next
	// image after 6 seconds

	//setInterval('rotate()', 6000);

}
/*
function rotate() {
	// Get the first image
	var current = ($('div.rotator ul li.show') ? $('div.rotator ul li.show')
			: $('div.rotator ul li:first'));

	if (current.length == 0)
		current = $('div.rotator ul li:first');

	// Get next image, when it reaches the end, rotate it back to the first
	// image
	var next = ((current.next().length) ? ((current.next().hasClass('show')) ? $('div.rotator ul li:first')
			: current.next())
			: $('div.rotator ul li:first'));

	// Un-comment the 3 lines below to get the images in random order

	// var sibs = current.siblings();
	// var rndNum = Math.floor(Math.random() * sibs.length );
	// var next = $( sibs[ rndNum ] );

	// Set the fade in effect for the next image, the show class has higher
	// z-index
	next.css({
		opacity : 0.0
	}).addClass('show').animate({
		opacity : 1.0
	}, 1000);

	// Hide the current image
	current.animate({
		opacity : 0.0
	}, 1000).removeClass('show');

};
*/
/*
 * function hoverImgOf(filename) {
 * 
 * var re = new RegExp("(.+)\\.(gif|png|jpg)", "g"); $('#menu
 * img').each(function() { var imgSrc = $(this).attr('src'); alert(imgSrc)
 * if(imgSrc.match('_hover')) { imgSrc.replace("_hover", ""); alert(imgSrc)
 * $(this).attr('src',imgSrc); } }); if(!filename.match('_hover')) //return
 * filename.replace(re, "$1.$2");
 * 
 * return filename.replace(re, "$1_hover.$2"); } function NormalImgOf(filename) {
 * 
 * var re = new RegExp("(.+)_hover\\.(gif|png|jpg)", "g"); return
 * filename.replace(re, "$1.$2"); }
 */

// To delete row
/*
 * function deleteRowThis(obj) { alert("Row Deletion");
 * $(obj).parents('tr').remove(); }
 */
// table row delete #MngLoc
/*
 * function highLightImg(index,evtType) {
 * 
 * $("#menu img:eq("+index+")").jqDock("expand"); var imgSrc2 = $("#menu
 * img:eq("+index+")").attr("src"); if(evtType == 'click') {
 * $('.tabs').find('a.tabActive').removeClass('tabActive'); $(".tabs
 * li:eq("+index+")").find('a').addClass('tabActive');
 * $(".tabsPanel").css('display','none'); $(".tabsPanel:eq("+index+")").show();
 * $('#menu img').each(function() { var imgSrc = $(this).attr('src');
 * if(imgSrc.match('_hover')) { imgSrc.replace("_hover", "");
 * $(this).attr('src',imgSrc); } });
 * 
 * var re = new RegExp("(.+)\\.(gif|png|jpg)", "g"); var str =
 * imgSrc2.replace(re, "$1_hover.$2"); $("#menu img:eq("+index+")").attr("src",
 * imgSrc2.replace(re, "$1_hover.$2")); } }
 */
function chkUser() {
	/*
	 * var loginTxtBox = document.getElementById('userName'); var str = 'Please
	 * Enter User Name as: \n \nSupplier / Retailer'
	 * 
	 * if(loginTxtBox.value==''){ alert(str) }
	 * 
	 * else {
	 * 
	 * if(loginTxtBox.value.toUpperCase() == 'SUPPLIER') document.location.href =
	 * 'adminpage_mfg.html';
	 * 
	 * else if(loginTxtBox.value.toUpperCase()=='RETAILER')
	 * document.location.href = 'adminpage_ret.html';
	 * 
	 * else{ alert('Invalid User'); alert(str) } }
	 * document.getElementById('userName').value = '';
	 * document.getElementById('pswd').value = '';
	 */

	document.loginform.action = "login.htm";
	document.loginform.method = "POST";
	document.loginform.submit();
}

function createProfile() {

	var password = document.createprofileform.password.value;
	var passFlag = validatePassword(password);
	alert(passFlag)
	if (passFlag) {
		document.createprofileform.action = "createProfile.htm";
		document.createprofileform.method = "POST";
		document.createprofileform.submit();
	}

}

function createProfile_old() {
	var email = document.createprofileform.contactEmail.value;
	var retypeEmail = document.createprofileform.retypeEmail.value;
	var password = document.createprofileform.password.value;
	var repassword = document.createprofileform.retypePassword.value;
	var regEx = /^.+@.+\..{2,5}$/;
	if (!regEx.test(email)) {

		alert("Please Enter Valid EmailId");
		/*
		 * document.document.getElementById("contEml").value = "";
		 * document.document.getElementById("contREml").value = "";
		 * document.document.getElementById("contEml").focus();
		 */
	} else if (!(email.toString() == retypeEmail.toString())) {
		alert("Password fields does not match");
	} else if (!(password.toString() == repassword.toString())) {

		alert("Password Fileds Does't match");
	} else

	if (document.createprofileform.tandc.checked == false) {
		alert("Please accept the Terms and Conditions");
	} else {
		document.createprofileform.action = "createProfile.htm";
		document.createprofileform.method = "POST";
		document.createprofileform.submit();
	}
}

function createRetailerProfile_old() {
	var email = document.createretailerprofileform.contactEmail.value;
	var retypeEmail = document.createretailerprofileform.retypeEmail.value;
	var password = document.createretailerprofileform.password.value;
	var repassword = document.createretailerprofileform.retypePassword.value;
	var regEx = /^.+@.+\..{2,5}$/;
	if (!regEx.test(email)) {

		alert("Please Enter Valid EmailId");
		/*
		 * document.document.getElementById("contEml").value = "";
		 * document.document.getElementById("contREml").value = "";
		 * document.document.getElementById("contEml").focus();
		 */
	} else if (!(email.toString() == retypeEmail.toString())) {
		alert("Email Id's does not match");
	} else if (!(password.toString() == repassword.toString())) {

		alert("Password Fileds Does't match");
	} else

	if (document.createretailerprofileform.terms.checked == false) {
		alert("Please accept the Terms and Conditions");
	} else {
		document.createretailerprofileform.action = "createRetailerProfile.htm";
		document.createretailerprofileform.method = "POST";
		document.createretailerprofileform.submit();
	}
}

/*
 * function createRetailerProfile() { document.createretailerprofileform.action =
 * "createRetailerProfile.htm"; document.createretailerprofileform.method =
 * "POST"; document.createretailerprofileform.submit(); }
 */

function createRetailerProfile() {
	getLocationCoordinates();
}

function submitRetailerProfile() {
	/*
	 * var locCordinates = ""; if (document.getElementById("locCoordinates")) {
	 * locCordinates = document.getElementById("locCoordinates").value; }
	 * document.createretailerprofileform.action =
	 * "createRetailerProfile.htm?locationCoordinates=" + locCordinates;
	 */
	var subCatMad = false;
	
	var mainCat = $("#bCategory option.toggleSubCat:selected").map(function() {
		  return $(this).val();
		}) .get().join();
	var mainCatSpl = mainCat.split(",");
	var subCategory = document.createretailerprofileform.subCategory.value.split(",");//Selected business cat which has sub-cats
	var hiddenSubCats = document.createretailerprofileform.subCategories.value.split("!~~!");
	
	var mainCatSplLng = mainCatSpl.length;
	var subCategoryLng = subCategory.length;
	
	if(mainCatSplLng > subCategoryLng) {
		subCatMad = true;
	} else if(mainCatSplLng == subCategoryLng) {
		for(var i = 0; i < mainCatSplLng; i++) {
			if(subCategory[i] !== mainCatSpl[i]) {
				subCatMad = true;
				break;
			} else {
				if(hiddenSubCats[i] === "NULL") {
					subCatMad = true;
					break;
				}
			}
		}
	}
	
	if(subCatMad === true) {
		alert("Please select 'Sub-Categories' to the selected business categories");		
	} else {
		
		var strSubCat = "";
		var j = 0;
		var busCat = $("#bCategory option:selected").map(function() {
			  return $(this).val();
			}) .get().join();
		
		var busCatSpl = busCat.split(",");
		var busCatSplLng = busCatSpl.length;
		
		for(var i = 0; i < busCatSplLng; i++) {
			
			if(busCatSpl[i] === subCategory[j]) {
				
				if(strSubCat === "") {
					strSubCat = hiddenSubCats[j];
				} else {
					strSubCat += "!~~!" + hiddenSubCats[j];
				}
				
				j++;
			} else {
				
				if(strSubCat === "") {
					strSubCat = "NULL";
				} else {
					strSubCat += "!~~!NULL";
				}
			}
			
		}
		
		document.createretailerprofileform.hiddensubCategies.value = strSubCat;
		document.createretailerprofileform.subCategory.value = mainCat;
		
		
		/*var cat = $("#bCategory option.toggleFltr:selected").map(function(){
					  return $(this).val();
					}) .get().join();
		var filterCategory = document.createretailerprofileform.filterCategory.value.split(",");
		var hiddenCat = cat.split(",");
		var hiddenCatLength = hiddenCat.length;
		if(hiddenCatLength > filterCategory.length) {
			var hiddenMainFilter = document.createretailerprofileform.filters.value.split("!~~!");
			var hiddenSubFilter = document.createretailerprofileform.filterValues.value.split("!~~!");
			var filter = "";
			var subFilters = "";
			var startLength = 0;
			var insert = false;
			for(var m = 0; m < hiddenCatLength; m++) {
				for(var n = 0; n < filterCategory.length; n++) {
					if(hiddenCat[m] !== filterCategory[n]) {
						insert = true;
					} else {
						insert = false;
						break;
					}
				}
				if(insert == true) {
					var assocaited = false;
					for(var i = 0; i < hiddenCatLength; i++) {	
						if(i > 0) {
							if(i == m) {
								filter += "!~~!" + "NULL";
								assocaited = true;									
							} else {									
								if(assocaited == false) {
									filter += "!~~!" + hiddenMainFilter[i];
									var split = hiddenMainFilter[i].split("|");
									startLength = startLength + split.length;
								} else {
									filter += "!~~!" + hiddenMainFilter[i-1];
								}
							}
						} else {
							if(i == m) {
								filter = "NULL";
								assocaited = true;	
							} else {
								filter = hiddenMainFilter[i];
								var split = hiddenMainFilter[i].split("|");
								startLength = startLength + split.length;
							}
						}
					}
					assocaited = false;
					var hidsubFil = filter.split("!~~!");
					var hidsubFilLeng = hidsubFil.length;
					var leng = 0;
					for(var k = 0; k < hidsubFilLeng; k++) {
						var split = hidsubFil[k].split("|");
						leng = leng + split.length;
					}
					for(var j = 0; j < leng; j++) {
						if(j > 0) {
							if(j == startLength) {
								subFilters += "!~~!" + "NULL";
								assocaited = true;	
							} else {
								if(assocaited == false) {
									subFilters += "!~~!" + hiddenSubFilter[j];
								} else {
									subFilters += "!~~!" + hiddenSubFilter[j-1];
								}
							}
						} else {
							if(j == startLength) {
								subFilters = "NULL";
								assocaited = true;									
							} else {
								subFilters = hiddenSubFilter[j];
							}
						}
					}
				} 
			}
			document.createretailerprofileform.filters.value = filter;
			document.createretailerprofileform.filterValues.value = subFilters;
			document.createretailerprofileform.filterCategory.value = cat;
		}*/
		
		
		document.createretailerprofileform.action = "finalizeregistration.htm";
		document.createretailerprofileform.method = "POST";
		document.createretailerprofileform.submit();
	}
}

function getLocationCoordinates() {
	var streetAddress = $('#address1').val();
	var state = $('#Country').val();
	var city = $('#City').val();
	var zipCode = $('#postalCode').val();

	var address = streetAddress + " " + city + " " + state + " " + zipCode;
	// alert("Address "+address);
	var geocoder = new google.maps.Geocoder();
	geocoder
			.geocode(
					{
						'address' : address
					},
					function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							// alert("Location :"+results[0].geometry.location);
							document.getElementById("locCoordinates").value = results[0].geometry.location;
							submitRetailerProfile();
						} else {
							// alert("Unable to find address: " + status);
							submitRetailerProfile();
						}
					});
}

function addRebates() {

	/*
	 * var startDate = document.getElementById('datepicker1').value;
	 * alert("startDate"+startDate);
	 * 
	 * var endDate = document.getElementById('datepicker2').value;
	 * alert("endDate"+endDate);
	 */

	/*
	 * if(checkDate(startDate)== false){ alert('Please enter valid Start Date');
	 * }else if(checkDate(startDate)== false) { alert('Please enter valid End
	 * Date'); }else{
	 */

	var valstatus = validateRebateForm();
	document.addRebatesform.action = "rebatesaddmfg.htm";
	document.addRebatesform.method = "POST";
	document.addRebatesform.submit();
	// }
}

/*
 * This function for Opening the iframe popup by using the any events & we
 * cann't able to access any thing in the screen except popup parameters:
 * popupId : Popup Container ID IframeID : Iframe ID url : page path i.e src of
 * a page height : Height of a popup width : Width of a popup name : here name
 * means Header text of a popup btnBool : if you have any buttons means we can
 * use it i.e true/false
 */
 function openIframePopup(popupId, IframeID, url, height, width, name, btnBool) {
	
	             /*overlay popup code starts
                  $('body,html').animate({scrollTop :0}, 'slow');
                  document.body.style.overflow = "hidden";
                  $("#wrapper").prepend("<div class='popup-overlay'></div>");*/
                  /*overlay code ends*/


	frameDiv = document.createElement('div');
	frameDiv.className = 'framehide';
	document.body.appendChild(frameDiv);
	document.getElementById(IframeID).setAttribute('src', url);
	// frameDiv.setAttribute('onclick', closePopup(obj,popupId));
	document.getElementById(popupId).style.display = "block"
	height = (height == "100%") ? frameDiv.offsetHeight - 20 : height;
	width = (width == "100%") ? frameDiv.offsetWidth - 16 : width;
	document.getElementById(popupId).style.height = height + "px"
	document.getElementById(popupId).style.width = width + "px"
	var marLeft = -1 * parseInt(width / 2);
	var marTop = -1 * parseInt(height / 2);
	document.getElementById('popupHeader').innerHTML = name;
	document.getElementById(popupId).style.marginLeft = marLeft + "px"
	document.getElementById(popupId).style.marginTop = marTop + "px"
	var iframeHt = height - 27;
	document.getElementById(IframeID).height = iframeHt + "px";
	if (btnBool) {
		var btnHt = height - 50;
		document.getElementById(IframeID).style.height = btnHt + "px";
	}

}
function setiframe(popupId){
document.getElementById(popupId).style.marginTop = "0px";
document.getElementById(popupId).style.top = "0px";

}
function openIframePopupForImage(popupId, IframeID, url, height, width, name, btnBool) {
	
	frameDiv = document.createElement('div');
	frameDiv.className = 'framehide';
	document.body.appendChild(frameDiv);
	document.getElementById(IframeID).setAttribute('src', url);
	// frameDiv.setAttribute('onclick', closePopup(obj,popupId));
	document.getElementById(popupId).style.display = "block"
	height = (height == "100%") ? frameDiv.offsetHeight - 20 : height;
	width = (width == "100%") ? frameDiv.offsetWidth - 16 : width;
	document.getElementById(popupId).style.height = height + "%"
	document.getElementById(popupId).style.width = width + "%"
//	var marLeft = -1 * parseInt(width / 2);
//	var marTop = -1 * parseInt(height / 2);
	document.getElementById('popupHeader').innerHTML = name;
	//document.getElementById(popupId).style.marginLeft = marLeft + "px"
	//document.getElementById(popupId).style.marginTop = marTop + "px"
	//var iframeHt = height - 27;
	var setHt = getDocHeight();
	
    var iframeHt = setHt - 27;
	document.getElementById(IframeID).height = iframeHt + "px";
	if (btnBool) {
		var btnHt = height - 50;
		document.getElementById(IframeID).style.height = btnHt + "px";
	}

}
function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}

/*
 * This function for Closing the iframe popup parameters: popupId : Popup
 * Container ID IframeID : Iframe ID
 */
function closeIframePopup(popupId, IframeID, funcUrl) {

	try {		
		document.body.removeChild(frameDiv);
		document.getElementById(popupId).style.display = "none";
		document.getElementById(popupId).style.height = "0px";
		document.getElementById(popupId).style.width = "0px";
		document.getElementById(popupId).style.marginLeft = "0px";
		document.getElementById(popupId).style.marginTop = "0px";
		document.getElementById(IframeID).removeAttribute('src');
		document.body.style.overflow = "visible";
	} catch (e) {
		top.$('.framehide').remove();
		top.document.getElementById(popupId).style.display = "none";
		top.document.getElementById(popupId).style.height = "0px";
		top.document.getElementById(popupId).style.width = "0px";
		top.document.getElementById(popupId).style.marginLeft = "0px";
		top.document.getElementById(popupId).style.marginTop = "0px";
		top.document.getElementById(IframeID).removeAttribute('src');
		top.document.body.style.overflow = "visible";
	}
	
}

function callBackFunc() {
	/*
	 * var $cpNm = top.$('#couponName'); var chkArry = []
	 * $('input[name="pUpcChk"]:checked').each(function() {
	 * chkArry.push(this.value) }); $cpNm.val(chkArry)
	 */
}

function triggerFile(obj) {
	$("#imgBrwse").click().select(function() {
		$(obj).parents('td').find('input').val(this.value)
	}).change(function() {
		$(obj).parents('td').find('input').val(this.value)
	});

}

function resizeDoc() {

	var headerPanel = document.getElementById('header');
	var footerPanel = document.getElementById('footer');
	var resizePanel = document.getElementById('content');
	/* var dockPanel = document.getElementById('dockPanel'); */

	if (document.getElementById('wrapper')) {
		if (getWinDimension()[1]
				- (headerPanel.offsetHeight + footerPanel.offsetHeight + 20) > 0) {
			resizePanel.style.minHeight = getWinDimension()[1]
					- (headerPanel.offsetHeight + footerPanel.offsetHeight + 20)
					+ "px";
		}
		// alert(resizePanel.style.minHeight);
	}
	
}

// To get client height
function getWinDimension() {
	var windowHeight = "";
	var windowWidth = "";

	if (!document.all || isOpera()) {
		windowHeight = window.innerHeight;
		windowWidth = window.innerWidth;
	} else {
		windowHeight = document.documentElement.clientHeight;
		windowWidth = document.documentElement.clientWidth;
	}
	return [ windowWidth, windowHeight ]

}
// check if the client is opera
function isOpera() {
	return (navigator.appName.indexOf('Opera') != -1);
}

function validateUserForm(arryNm, parent, funcCall) {
	var errBool = true;

	$('.errIcon').removeClass('errIcon');
	for ( var i in arryNm) {
		var $frmE = $(arryNm[i]);
		var val = $.trim($frmE.val());
		val = (val == "(   )      -") ? "" : val;
		if (val == '') {
			$frmE.parents(parent).find('.errDisp').addClass('errIcon');
			errBool = false;
		}
	}

	if (errBool) {
		if (typeof funcCall == "function")
			funcCall();

		else
			window.location.href = funcCall;

	}
}

// create Attribute dynamically
function crtAtrr() {

	var selIndex = document.getElementById("attrDropDown").selectedIndex;
	var optionArr = document.getElementById("attrDropDown").options;
	var displayValue = optionArr[selIndex].text;

	var slctVals = $('select.attrList option:selected').val();
	if (slctVals == '') {
		alert("No Attributes to add to the Attributes Grid.");
		return false;
	}
	/*
	 * var attLabelArry = $('#attLabel') alert("dd "+attLabelArry.length)
	 */

	var attLabelArry = new Array();

	$('#attLabel').each(function(index) {
		attLabelArry[index] = $(this).text();
		// alert($(this).text())
	})

	if (attLabelArry.length > 0) {
		for ( var k = 0; k < attLabelArry.length; k++) {
			if (displayValue == [ attLabelArry[k] ]) {
				alert(displayValue + " attribute is already added!")
				return false;
			}
		}
	}
	// duplicate attrubutes validation need to be done
	var attrContVal = $("#attrCont").val();
	var rowCount2 = $('#attrDpsly >tbody >tr').length;

	$('.slctdValues').text(slctVals);
	var tmpId = "ContVal_" + rowCount2;
	if (attrContVal == '') {
		alert("Please enter attribute value");
		return false;
	}
	$("#attrDpsly tr td:not(:last-child)").css({
		"border-right" : "1px solid #dadada"
	});
	$("#attrDpsly tbody")
			.append(
					"<tr>"
							+ "<td width='24%'  class='Label Rtbrdr'>"
							+ "<label id='attLabel'>"
							+ displayValue
							+ "</label>"
							+ "</td>"
							+ "<td width='76%'>"
							+ "<input id="
							+ tmpId
							+ " name="
							+ slctVals
							+ " type='text' value='"
							+ attrContVal
							+ " '/>"
							+ " <a href='#'><img src='images/delete.png' alt='delete' title='delete' width='16' height='16' onclick='deleteRowThis(this)'/></a>"
							+ "</td>" + "</tr>");
	$("#" + tmpId).val(attrContVal);
	$('.tglDsply').show();
	// $('select.attrList option:selected').val("");
	$("#attrCont").val("");
	$('form').get(0).reset();
}

// To delete Attributes from Add products
function deleteRowThis(obj) {
	// alert("Delete Attribute");
	if ($(obj).parents('table').find('tr').length > 1) {
		var msg = confirm("Do u want to delete attribute")
		if (msg) {
			$(obj).parents('tr').remove();
		}
	}
}

function showHide(obj) {
	var frm = document.forms[0];
	if (obj && obj.checked) {
		$("deletedAddresses").show();
		$("existingAddresses").hide();

	} else {
		$("existingAddresses").show();
		$("deletedAddresses").hide();

	}
}

/*
 * function updateProfile() { alert("From the global.js");
 * document.editprofileform.action = "editProfile.htm";
 * document.editprofileform.method = "POST"; document.editprofileform.submit(); }
 */

/*
 * function hoverImgOf(filename) {
 * 
 * var re = new RegExp("(.+)\\.(gif|png|jpg)", "g"); $('#menu
 * img').each(function() { var imgSrc = $(this).attr('src'); alert(imgSrc)
 * if(imgSrc.match('_hover')) { imgSrc.replace("_hover", ""); alert(imgSrc)
 * $(this).attr('src',imgSrc); } }); if(!filename.match('_hover')) //return
 * filename.replace(re, "$1.$2");
 * 
 * return filename.replace(re, "$1_hover.$2"); } function NormalImgOf(filename) {
 * 
 * var re = new RegExp("(.+)_hover\\.(gif|png|jpg)", "g"); return
 * filename.replace(re, "$1.$2"); }
 */

// To delete row
/*
 * function deleteRowThis(obj) { alert("Row Deletion");
 * $(obj).parents('tr').remove(); }
 */
// table row delete #MngLoc
/*
 * function highLightImg(index,evtType) {
 * 
 * $("#menu img:eq("+index+")").jqDock("expand"); var imgSrc2 = $("#menu
 * img:eq("+index+")").attr("src"); if(evtType == 'click') {
 * $('.tabs').find('a.tabActive').removeClass('tabActive'); $(".tabs
 * li:eq("+index+")").find('a').addClass('tabActive');
 * $(".tabsPanel").css('display','none'); $(".tabsPanel:eq("+index+")").show();
 * $('#menu img').each(function() { var imgSrc = $(this).attr('src');
 * if(imgSrc.match('_hover')) { imgSrc.replace("_hover", "");
 * $(this).attr('src',imgSrc); } });
 * 
 * var re = new RegExp("(.+)\\.(gif|png|jpg)", "g"); var str =
 * imgSrc2.replace(re, "$1_hover.$2"); $("#menu img:eq("+index+")").attr("src",
 * imgSrc2.replace(re, "$1_hover.$2")); } }
 */

/* Added common functions end */

function saveMangageProducts(submitfrom, pageNumber) {

	var productJson = [];
	var productObj = {};
	var objArr = [];
	$("#highLight").find("tr").each(function(index) {
		if (index > 0) {
			var highlightObj = $($("#highLight").find("tr")[index]).children();
			var object = {};
			var stringObj;
			// alert(index)
			highlightObj.find("input").each(function(rVal) {
				// if(rVal > 0){
				var inputTagVal = $(this)[0].value;
				var name = $(this).attr("name")
				// alert(name)
				if (name == 'productID') {
					object.productID = inputTagVal;
				} else if (name == 'productUpc') {
					object.productUpc = inputTagVal;
					// object = '{"productUpc"'+":"+ inputTagVal
				} else if (name == 'prodName') {
					object.prodName = inputTagVal;
				} else if (name == 'modelNumber') {
					object.modelNumber = inputTagVal
				} else if (name == 'suggestedRetailPrice') {
					object.suggestedRetailPrice = inputTagVal
				} else if (name == 'longDesc') {
					object.longDesc = inputTagVal
				} else if (name == 'warranty') {
					object.warranty = inputTagVal
				} else if (name == 'shortDesc') {
					object.shortDesc = inputTagVal
				} else if (name == 'category') {
					object.category = inputTagVal
				}

				// }
			});

			highlightObj.find("select").each(function() {
				var inputTagVal = $(this)[0].value;
				object.category = inputTagVal;
			});

			objArr[index - 1] = object;
		}
	});

	var validationFlag = true;
	for ( var k = 0; k < objArr.length; k++) {
		var object = objArr[k];
		var prodId = object.productID;
		var prodName = object.prodName;
		var prodUpc = object.productUpc;
		var prodPrice = object.suggestedRetailPrice;
		var longDesc = object.longDesc;
		if (prodName == '' || prodName == 'undefined' || prodName == 'null') {
			$("tr#" + prodId).addClass("requiredVal");
			validationFlag = false;
		} else if (prodUpc == '' || prodUpc == 'undefined' || prodUpc == 'null') {
			$("tr#" + prodId).addClass("requiredVal");
			validationFlag = false;
		} else if (prodPrice == '' || prodPrice == 'undefined'
				|| prodPrice == 'null') {
			$("tr#" + prodId).addClass("requiredVal");
			validationFlag = false;
		} else if (longDesc == '' || longDesc == 'undefined'
				|| longDesc == 'null') {
			$("tr#" + prodId).addClass("requiredVal");
			validationFlag = false;
		} else {
			$("tr#" + prodId).removeClass("requiredVal");
		}

	}
	if (validationFlag) {
		var strinObjArr = [];
		for ( var k = 0; k < objArr.length; k++) {
			var object = objArr[k];
			// alert(object.toSource())
			// var stringObj =
			// "{productName:\""+object.prodName+"\",modelNumber:\""+object.modelNumber+"\",suggestedRetailPrice:\""+object.sugRetailPrice+"\",longDescription:\""+object.longDesc+"\",warranty:\""+object.warranty+"\",shortDescription:\""+object.shortDesc+"\"}";
			var stringObj = "{\"productName\":\"" + object.prodName + "\","
					+ "\"productID\":\"" + object.productID + "\","
					+ "\"modelNumber\":\"" + object.modelNumber + "\","
					+ "\"suggestedRetailPrice\":\""
					+ object.suggestedRetailPrice + "\","
					+ "\"longDescription\":\"" + object.longDesc + "\","
					+ "\"warranty\":\"" + object.warranty + "\","
					+ "\"shortDescription\":\"" + object.shortDesc + "\","
					+ "\"category\":\"" + object.category + "\","
					+ "\"scanCode\":\"" + object.productUpc + "\"}";
			// alert(stringObj)
			strinObjArr[k] = stringObj;
			// alert(stringObj)
		}
		// alert(strinObjArr.toString());

		// Submtitting to save the changes.
		var r = confirm("Do you want to save changes ?");
		if (r == true) {
			saveGridResult(strinObjArr.toString(), submitfrom, pageNumber);
		}
	} else {

		alert("Please Enter values for the mandatory fields for the highlighted rows in the screen");
		document.getElementById('manageform.errors').style.display = 'none';
		return false;
	}

}

function saveGridResult(objArr, submitfrom, pageNumber) {
	if (pageNumber == null || pageNumber == undefined) {
		document.myform.pageFlag.value = "false";
	} else {
		document.myform.pageNumber.value = pageNumber;
		document.myform.pageFlag.value = "true";
	}
	if (submitfrom == 'dashboard') {
		document.myform.prodJson.value = "{\"productData\":[" + objArr + "]}";
		document.myform.action = "savegridprod.htm";
		document.myform.method = "POST";
		document.myform.submit();
	} else {
		document.myform.prodJson.value = "{\"productData\":[" + objArr + "]}";
		document.myform.action = "savereggridprod.htm";
		document.myform.method = "POST";
		document.myform.submit();
	}
}

/** *************Start******* */
function saveUploadFile() {

	document.batchform.action = "savebatchupload.htm";
	document.batchform.method = "POST";
	document.batchform.submit();

}

function saveSupplierUploadFile() {

	document.batchform.action = "savesupplierbatchupload.htm";
	document.batchform.method = "POST";
	document.batchform.submit();

}

function uploadProductFile() {

	document.batchform.action = "uploadproduct.htm";
	document.batchform.method = "POST";
	document.batchform.submit();

}

function uploadAudioVideoImageFile() {

	var selFile = document.batchform.imageUploadFilePath.value
	if (selFile == 'null' || selFile == '') {
		alert('Please select Images,Audio,Video or Zip file')
	} else {
		var file = validateUploadAudioVideoImageZipFileExt(selFile);
		if (file) {
			document.batchform.action = "uploadimage.htm";
			document.batchform.method = "POST";
			document.batchform.submit();
		} else {
			alert('Please select Images,Audio,Video or Zip file');
		}

	}
}

function validateUploadAudioVideoImageFileExt(file) {
	if (file != '' && file != 'undefined') {
		var dotIndex = file.lastIndexOf('.');
		var length = file.length;
		var ext = file.substr(dotIndex + 1, length);
		var extn = ext.toLowerCase();
		if (extn == 'mp3' || extn == 'mp4' || extn == 'png') {
			return true;
		} else {
			return false;
		}
	}
}

function uploadAttribute() {

	document.batchform.action = "uploadattribute.htm";
	document.batchform.method = "POST";
	document.batchform.submit();

}

function uploadFileSupplier() {

	document.batchform.action = "uploadproductsupplier.htm";
	document.batchform.method = "POST";
	document.batchform.submit();

}

function uploadAttributeSupplier() {

	document.batchform.action = "uploadattributesupplier.htm";
	document.batchform.method = "POST";
	document.batchform.submit();

}

/** *************end******* */

function addNewRow() {
	// alert("Hi");
	var curRow = $("#locSetUpGrd tr:last").clone(true).insertAfter(
			'#locSetUpGrd tr:last');
	$("#locSetUpGrd tr:last input:text").val("");

}

// *******************startLocation*********

function saveGridResultList(objArr, pagenumber) {
	if (pagenumber == null || pagenumber == undefined) {
		document.locationform.pageFlag.value = "false";
	} else {
		document.locationform.pageFlag.value = "true";
	}
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.locationform.pageNumber.value = pagenumber;
	document.locationform.prodJson.value = "{\"locationData\":[" + objArr
			+ "]}";
	document.locationform.action = "updatelocation.htm";
	document.locationform.method = "POST";
	document.locationform.submit();

}

function editLocationSetUp(pagenumber) {
	// debugger;

	var productJson = [];
	var productObj = {};
	// debugger;

	var objArr = [];
	$("#locSetUpGrd").find("tr").each(
			function(index) {
				if (index > 0) {
					var highlightObj = $($("#locSetUpGrd").find("tr")[index])
							.children();
					var object = {};
					var stringObj;

					highlightObj.find("input").each(function(rVal) {

						var inputTagVal = $(this)[0].value;
						var name = $(this).attr("name")

						if (name == 'uploadRetaillocationsID') {
							object.uploadRetaillocationsID = inputTagVal;

						} else if (name == 'retailID') {
							object.retailID = inputTagVal;

						} else if (name == 'storeIdentification') {
							object.storeIdentification = inputTagVal;

						} else if (name == 'address1') {
							object.address1 = inputTagVal;

						} else if (name == 'city') {
							object.city = inputTagVal

						} else if (name == 'state') {
							object.state = inputTagVal

						} else if (name == 'postalCode') {
							object.postalCode = inputTagVal

						} else if (name == 'phonenumber') {
							object.phonenumber = inputTagVal

						} else if (name == 'keyword') {
							object.keyword = inputTagVal
						}
						// }
					});
					objArr[index - 1] = object;
					/*
					 * }else{ // no change }
					 */

				}
			});

	var strinObjArr = []
	for ( var k = 0; k < objArr.length; k++) {
		var object = objArr[k];
		var stringObj = "{\"retailID\":\"" + object.retailID + "\","
				+ "\"uploadRetaillocationsID\":\""
				+ object.uploadRetaillocationsID + "\","
				+ "\"storeIdentification\":\"" + object.storeIdentification
				+ "\"," + "\"address1\":\"" + object.address1 + "\","
				+ "\"city\":\"" + object.city + "\"," + "\"state\":\""
				+ object.state + "\"," + "\"postalCode\":\""
				+ object.postalCode + "\"," + "\"phonenumber\":\""
				+ object.phonenumber + "\"," + "\"keyword\":\""
				+ object.keyword + "\"}";
		strinObjArr[k] = stringObj;
	}
	// Submtitting to save the changes.
	saveGridResultList(strinObjArr.toString(), pagenumber)
}

function next() {

	document.locationform.action = "productManagement.htm";
	document.locationform.method = "POST";
	document.locationform.submit();

}

function batchUpdateLocationSetup() {
	/* show progress bar : ETA for Web 1.3 */
	showProgressBar();
	/* End */
	document.locationform.action = "locationbatchupload.htm";
	document.locationform.method = "POST";
	document.locationform.submit();

}

function saveNcallNextRetailLocation(pageNumber) {
	editLocationSetUp(pageNumber)
}

function callNextPage(pagenumber, url) {
	// for Saving Retail Location separate floe
	if (saveRetalLocation) {
		saveNcallNextRetailLocation(pagenumber);
	} else {
		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}
}

// *******************EndLocation*********
function checkMaxLength(obj, maxlength) {
	// alert('jj')
	if (obj.value.length > maxlength) {
		obj.value = obj.value.substring(0, maxlength);
	}
}

// ***********************start retailer Choose plan*******************
function totalPrice(size) {

	var qantity = document.getElementsByName("qty");
	var priceValue = document.getElementsByName("price");
	var total = document.getElementsByName("total");
	var subtotal = 0;
	if (isNaN(qantity) == false) {
		alert("qantity can be number only");
	}

	else {

		for ( var i = 0; i < size; i++) {

			total[i].value = (qantity[i].value) * (priceValue[i].value);
			subtotal += parseFloat(total[i].value)

		}
		document.getElementById("sub").value = subtotal;
		document.getElementById("discountsubtotal").value = subtotal;
		var subTotValue = document.getElementsByName("subTotValue");
		var discount = document.getElementById("discount");
		return true;
	}
}

function validatePassword(password) {
	var isDigits = /[0-9]/;
	if (password != "") {
		if (password < 8) {
			alert("Password must contain atleast eight characters!");
			return false;
		} else if (!isDigits.test(password)) {
			alert("Password must contain at least one number (0-9)!");
			return false;
		}
		return true;
	}
}

function createProfile() {

	var password = document.createprofileform.password.value;
	var passFlag = validatePassword(password);
	if (passFlag) {
		document.createprofileform.action = "createProfile.htm";
		document.createprofileform.method = "POST";
		document.createprofileform.submit();
	}

}

function retailerPlanInfo(total) {

	var productJson = [];
	var productObj = {};
	// debugger;
	var objArr = [];
	$("#choosePln").find("tr").each(function(index) {
		if (index > 0) {
			var highlightObj = $($("#choosePln").find("tr")[index]).children();
			var object = {};
			var stringObj;
			// alert(index)
			highlightObj.find("input").each(function(rVal) {
				// if(rVal > 0){
				var inputTagVal = $(this)[0].value;
				var name = $(this).attr("name")
				// alert(name)
				// alert(name+" "+inputTagVal)

				if (name == 'productId') {
					object.productId = inputTagVal;
				} else if (name == 'qty') {
					object.qty = inputTagVal;
				} else if (name == 'price') {
					object.price = inputTagVal;
				} else if (name == 'total') {
					object.total = inputTagVal;
				}

				// }
			});
			if (Object.getOwnPropertyNames(object).length > 0) {
				objArr[index - 1] = object;
			}

		}
	});

	// Converting Sting JSOn

	var strinObjArr = []
	for ( var k = 0; k < objArr.length; k++) {
		var object = objArr[k];
		var stringObj = "{\"productID\":\"" + object.productId + "\","
				+ "\"quantity\":\"" + object.qty + "\"," + "\"price\":\""
				+ object.price + "\"," + "\"subTotal\":\"" + object.total
				+ "\"}";
		strinObjArr[k] = stringObj;
	}

	document.myForm.planJson.value = "{\"planData\":[" + strinObjArr.toString()
			+ "]}";
	document.myForm.grandTotal.value = document
			.getElementById("discountsubtotal").value;
	document.myForm.productId.value = "";
	document.myForm.action = "retailerplanInfo.htm";
	document.myForm.method = "POST";
	document.myForm.submit();

}

// ***********************end retailer Choose plan*******************
function validateUploadFileExt(file) {
	if (file != '' && file != 'undefined') {
		var dotIndex = file.lastIndexOf('.');
		var length = file.length;
		var ext = file.substr(dotIndex + 1, length);
		if (ext == 'txt' || ext == 'csv') {
			return true;
		} else {
			return false
		}
	}
}

function validateUploadImageFileExt(file) {
	/*
	 * if (file != '' && file != 'undefined') { var dotIndex =
	 * file.lastIndexOf('.'); var length = file.length; var ext =
	 * file.substr(dotIndex + 1, length); if (ext == 'png') { return true; }
	 * else { return false } }
	 */
	return true;
}

function validateUploadAudioVideoFileExt(file) {
	if (file != '' && file != 'undefined') {
		var dotIndex = file.lastIndexOf('.');
		var length = file.length;
		var ext = file.substr(dotIndex + 1, length);
		if (ext == 'mp4' || ext == 'MP4') {
			return true;
		} else {
			return false
		}
	}
}

function openEMailSharePopUp(clrid, flag) {

	$('body .relatvDiv')
			.append(
					"<div class='mailpopPnl'>"
							+ "<div class='mailInfo'>"
							+ "<div class='actTitleBar'>"
							+ "Send Mail:"
							+ "<img src='../images/actnClose.png' alt='close' title='close' name='actClose' align='right'/>"
							+ "</div>"
							+ "<ul>"
							+ "<li>"
							+ "<span>"
							+ "To:"
							+ "</span>"
							+ "<input type='text' id='email'/> "
							+ "</li>"
							+ "<li>"
							+ "<input type='button' value='Send' class='btn' id='emailId' onclick='sendMail("
							+ clrid
							+ ","
							+ "\""
							+ flag
							+ "\""
							+ ")'/> <input type='reset' value='Clear' class='btn'/>"
							+ "</li>" + "<li id='ajax'>" + name + "</li>"
							+ "</ul>" + "</div>" + "</div>");

	$('img[name$="actClose"]').click(function() {
		$('.mailpopPnl').remove();
	});
}

function validateUploadAudioFileExt(file) {
	if (file != '' && file != 'undefined') {
		var dotIndex = file.lastIndexOf('.');
		var length = file.length;
		var ext = file.substr(dotIndex + 1, length);
		if (ext == 'mp4' || ext == 'MP4' || ext == 'mp3' || ext == 'MP3') {
			return true;
		} else {
			return false
		}
	}
}
function sendMail(clrId, clrType) {

	var toEmailId = document.getElementById("email").value;

	var regEx = /^.+@.+\..{2,5}$/;
	if (!regEx.test(toEmailId)) {

		alert("Please Enter Valid EmailId");

	} else {

		$.ajax({
			type : "GET",
			url : "sendmail.htm",
			data : {

				'clrId' : clrId,
				'clrType' : clrType,
				'toEmailId' : toEmailId

			},

			success : function(response) {

				// alert("response"+response);

				if (response == 'Email sent successfully.') {

					$('.mailpopPnl').remove();

					alert(response);

					// $('#ajax').replaceWith(response);

					/*
					 * setTimeout(function() { $('.mailpopPnl').remove(); },
					 * 10000);
					 */

				} else {

					alert('Error occurred while sending mail');
					// $('#ajax').replaceWith('Email sent failure.');

				}

			},
			error : function(e) {

			}
		});

	}

}
// To Validate Image Upload to accept only png format
function imageValidate() {
	var ext = $('#upldImg').val().split('.').pop().toLowerCase();
	if ($.inArray(ext, [ 'png' ]) == -1) {
		alert('Please Choose png format image');
		// For IE to clear field value
		$("#upldImg").replaceWith($("#upldImg").clone(true));
		// For other browsers to clear field value
		$("#upldImg").val("");
		return false;
	}
}

function openEMailSharePopUpGeneral(hotDealID) {

	$('body .mobCont')
			.append(
					"<div class='dealMail mailpopPnl'>"
							+ "<div class='mailInfo'>"
							+ "<div class='actTitleBar'>"
							+ "Send Mail:"
							+ "<img src='../images/actnClose.png' alt='close' title='close' name='actClose' align='right'/>"
							+ "</div>"
							+ "<ul>"
							+ "<li>"
							+ "<span>"
							+ "To:"
							+ "</span>"
							+ "<input type='text' id='email'/> "
							+ "</li>"
							+ "<li>"
							+ "<input type='button' value='Send' class='btn' id='emailId' onclick='shareViaEmail("
							+ hotDealID
							+ ")'/> <input type='reset' value='Clear' class='btn'/>"
							+ "</li>" + "<li id='ajax'>" + name + "</li>"
							+ "</ul>" + "</div>" + "</div>");

	$('img[name$="actClose"]').click(function() {
		$('.dealMail').remove();
	});
}

function shareViaEmail(hotDealId) {

	var toEmailId = document.getElementById("email").value;
	var regEx = /^.+@.+\..{2,5}$/;
	if (!regEx.test(toEmailId)) {
		alert("Please Enter Valid EmailId");

	} else {
		$.ajax({
			type : "GET",
			url : "shareHotDealInfo.htm",
			data : {
				'hotDealId' : hotDealId,
				'toEmailId' : toEmailId
			},

			success : function(response) {
				if (response == 'Email sent successfully.') {

					$('.dealMail').remove();

					alert(response);

				} else {

					alert('Error occurred while sending mail');

				}

			},
			error : function(e) {

			}
		});

	}
}

function openEMailShareProdPopUpGeneral(productId) {

	$('body .mobCont')
			.append(
					"<div class='dealMail mailpopPnl'>"
							+ "<div class='mailInfo'>"
							+ "<div class='actTitleBar'>"
							+ "Send Mail:"
							+ "<img src='../images/actnClose.png' alt='close' title='close' name='actClose' align='right'/>"
							+ "</div>"
							+ "<ul>"
							+ "<li>"
							+ "<span>"
							+ "To:"
							+ "</span>"
							+ "<input type='text' id='email'/> "
							+ "</li>"
							+ "<li>"
							+ "<input type='button' value='Send' class='btn' id='emailId' onclick='shareProdInfoViaEmail("
							+ productId
							+ ")'/> <input type='reset' value='Clear' class='btn'/>"
							+ "</li>" + "<li id='ajax'>" + name + "</li>"
							+ "</ul>" + "</div>" + "</div>");

	$('img[name$="actClose"]').click(function() {
		$('.dealMail').remove();
	});
}

function shareProdInfoViaEmail(productId) {

	var toEmailId = document.getElementById("email").value;
	var regEx = /^.+@.+\..{2,5}$/;
	if (!regEx.test(toEmailId)) {
		alert("Please Enter Valid EmailId");
	} else {
		$.ajax({
			type : "GET",
			url : "shareProdInfobyEmail.htm",
			data : {
				'productId' : productId,
				'toEmailId' : toEmailId
			},

			success : function(response) {
				if (response == 'Email sent successfully.') {

					$('.dealMail').remove();

					alert(response);

				} else {

					alert('Error occurred while sending mail');

				}

			},
			error : function(e) {

			}
		});

	}
}

function editPswd(userId) {

	$('.stretch')
			.append(
					"<div class='mailpopPnl'>"
							+ "<div class='mailInfo'>"
							+ "<div class='actTitleBar'>"
							+ "Change Password:"
							+ "<img src='../images/actnClose.png' alt='close' title='close' name='actClose' align='right'/>"
							+ "</div>"
							+ "<ul>"
							+ "<li>"
							+ "<span>"
							+ "Enter Password:"
							+ "</span>"
							+ "<input type='text' id='password' class='textboxSmall'/> "
							+ "</li>"
							+ "<li>"
							+ "<span>"
							+ "Confirm Password:"
							+ "</span>"
							+ "<input type='text' id='repassword' class='textboxSmall' /> "
							+ "</li>"
							+ "<li>"
							+ "<input type='button' value='Update' class='btn' id='emailId' onclick='updateUsrPwd("
							+ userId + ")'/>" + "</li>" + "</ul>" + "</div>"
							+ "</div>");
	$('.mailInfo li:eq(0) span').width(109);
	$('img[name$="actClose"]').click(function() {
		$('.mailpopPnl').remove();
	});
}

function updateUsrPwd(userId) {

	var password = document.getElementById("password").value;
	var repassword = document.getElementById("repassword").value;

	if (password == null || repassword == null || password == ""
			|| repassword == "") {
		alert("Please enter Password");
	} else if (password.length < 6 || repassword.length < 6) {
		alert('Password lenght shoulb be atleast six characters');
	}

	else if (!(password.toString() == repassword.toString())) {

		alert("Password Fileds Does't match");
	} else {
		$.ajax({
			type : "GET",
			url : "updateUsrPwd.htm",
			data : {
				'userId' : userId,
				'password' : password
			},

			success : function(response) {
				if (response == 'Password updated') {

					$('.mailpopPnl').remove();

					alert(response);

				} else {

					alert('Error occurred while updating the password.');

				}

			},
			error : function(e) {

			}
		});

	}
}
function validateUploadAudioVideoImageZipFileExt(file) {
	if (file != '' && file != 'undefined') {
		var dotIndex = file.lastIndexOf('.');
		var length = file.length;
		var ext = file.substr(dotIndex + 1, length);
		var extn = ext.toLowerCase();
		if (extn == 'mp3' || extn == 'mp4' || extn == 'png' || extn == 'zip') {
			return true;
		} else {
			return false;
		}
	}
}

/**
 * ETA for Web 1.3 this method show progress bar and should be called before
 * submiting request.
 */
function showProgressBar() {
	$('#loading-image').css("visibility", "visible");
	/*
	 * IE Issue Application is allowing the user to click on Bottom tool bar
	 * links
	 */
	$('#loading-image').css("height", "3000px");
	/* End */
	$('html,body').css("overflow", "hidden");
	$('html,body').css("margin-right", "8px");
	setTimeout(
			"document.getElementById('anmtdLdr').src='/ScanSeeWeb/images/ajaxblue.gif';",
			10);

	/*
	 * To get the original text and append ... to the text get animated effect
	 * Loading...
	 */
	var originalText = $("#loading-txt").text();
	i = 0;
	$("#loading-txt").css('color', '#ffffff');
	setInterval(function() {
		$("#loading-txt").append(".");
		i++;

		if (i == 4) {
			$("#loading-txt").html(originalText);
			i = 0;
		}
	}, 500);
}

function createAppSite() {
	getLocationCoordinates_AppSite();
}

function getLocationCoordinates_AppSite() {
	var streetAddress = $('#address1').val();
	var state = $('#Country').val();
	var city = $('#City').val();
	var zipCode = $('#postalCode').val();

	var address = streetAddress + " " + city + " " + state + " " + zipCode;
	// alert("Address "+address);
	var geocoder = new google.maps.Geocoder();
	geocoder
			.geocode(
					{
						'address' : address
					},
					function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							// alert("Location :"+results[0].geometry.location);
							document.getElementById("locCoordinates").value = results[0].geometry.location;
							submitAppSiteRetProfile();
						} else {
							// alert("Unable to find address: " + status);
							submitAppSiteRetProfile();
						}
					});
}

function submitAppSiteRetProfile() {
	/*
	 * var locCordinates = ""; if (document.getElementById("locCoordinates")) {
	 * locCordinates = document.getElementById("locCoordinates").value; }
	 * document.createretailerprofileform.action =
	 * "createRetailerProfile.htm?locationCoordinates=" + locCordinates;
	 */
	document.appsiteform.action = "appsite.htm";
	document.appsiteform.method = "POST";
	document.appsiteform.submit();
}

function resizePopup(){

}

function dsplyRecur(obj){
var recurId = $(obj).attr("id");
var ongTrgr = $("#onGoing").prop("checked");
 $(".recur").hide();

$("."+recurId).css("display","block");
}
 
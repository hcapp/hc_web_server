var saveRetalLocation = 'false';
var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;
var saveManageProductsPage = 'false';
var saveManageLocationPage = 'false';
var submitFlag = null;

function callNextPage(pagenumber, url) {

	var selValue = $('#selPerPage :selected').val();
	document.paginationForm.pageNumber.value = pagenumber;
	document.paginationForm.recordCount.value = 15;
	document.paginationForm.pageFlag.value = "true";
	document.paginationForm.action = url;
	document.paginationForm.method = "GET";
	document.paginationForm.submit();

}

function editCoupon(couponID) {

	document.managecouponform.couponID.value = couponID;
	document.managecouponform.action = "editcoupon.htm";
	document.managecouponform.method = "GET";
	document.managecouponform.submit();
}

function searchCoupon() {

	document.managecouponform.action = "managecoupons.htm";
	document.managecouponform.method = "POST";
	document.managecouponform.submit();
}

function rerunCoupon(couponID) {

	document.managecouponform.couponID.value = couponID;
	document.managecouponform.action = "reruncoupon.htm";
	document.managecouponform.method = "GET";
	document.managecouponform.submit();
}

function validateShooperProfile() {

	document.createshopperprofileform.action = "createShopperProfile.htm";
	document.createshopperprofileform.method = "POST";
	document.createshopperprofileform.submit();

}

function preferencesSubmit() {

	document.editdealsform.action = "preferences.htm";
	document.editdealsform.method = "POST";
	document.editdealsform.submit();
}

function schoolOnsubmit() {

	document.selectschoolform.action = "selectschool.htm";
	document.selectschoolform.method = "POST";
	document.selectschoolform.submit();

}
function backspacerUP(object, e) {
	if (e) {
		e = e
	} else {
		e = window.event
	}
	if (e.which) {
		var keycode = e.which
	} else {
		var keycode = e.keyCode
	}

	ParseForNumber1(object)

	if (keycode >= 48) {
		ValidatePhone(object)
	}
}

function backspacerDOWN(object, e) {
	if (e) {
		e = e
	} else {
		e = window.event
	}
	if (e.which) {
		var keycode = e.which
	} else {
		var keycode = e.keyCode
	}
	ParseForNumber2(object)
}
function ValidatePhone(object) {

	var p = phonevalue1

	p = p.replace(/[^\d]*/gi, "")

	if (p.length < 3) {
		object.value = p
	} else if (p.length == 3) {
		pp = p;
		d4 = p.indexOf('(')
		d5 = p.indexOf(')')
		if (d4 == -1) {
			pp = "(" + pp;
		}
		if (d5 == -1) {
			pp = pp + ")";
		}
		object.value = pp;
	} else if (p.length > 3 && p.length < 7) {
		p = "(" + p;
		l30 = p.length;
		p30 = p.substring(0, 4);
		p30 = p30 + ")"

		p31 = p.substring(4, l30);
		pp = p30 + p31;

		object.value = pp;

	} else if (p.length >= 7) {
		p = "(" + p;
		l30 = p.length;
		p30 = p.substring(0, 4);
		p30 = p30 + ")"

		p31 = p.substring(4, l30);
		pp = p30 + p31;

		l40 = pp.length;
		p40 = pp.substring(0, 8);
		p40 = p40 + "-"

		p41 = pp.substring(8, l40);
		ppp = p40 + p41;

		object.value = ppp.substring(0, maxphonelength);
	}

	GetCursorPosition()

	if (cursorposition >= 0) {
		if (cursorposition == 0) {
			cursorposition = 2
		} else if (cursorposition <= 2) {
			cursorposition = cursorposition + 1
		} else if (cursorposition <= 5) {
			cursorposition = cursorposition + 2
		} else if (cursorposition == 6) {
			cursorposition = cursorposition + 2
		} else if (cursorposition == 7) {
			cursorposition = cursorposition + 4
			e1 = object.value.indexOf(')')
			e2 = object.value.indexOf('-')
			if (e1 > -1 && e2 > -1) {
				if (e2 - e1 == 4) {
					cursorposition = cursorposition - 1
				}
			}
		} else if (cursorposition < 11) {
			cursorposition = cursorposition + 3
		} else if (cursorposition == 11) {
			cursorposition = cursorposition + 1
		} else if (cursorposition >= 12) {
			cursorposition = cursorposition
		}

		var txtRange = object.createTextRange();
		txtRange.moveStart("character", cursorposition);
		txtRange.moveEnd("character", cursorposition - object.value.length);
		txtRange.select();
	}

}
function GetCursorPosition() {

	var t1 = phonevalue1;
	var t2 = phonevalue2;
	var bool = false
	for (i = 0; i < t1.length; i++) {
		if (t1.substring(i, 1) != t2.substring(i, 1)) {
			if (!bool) {
				cursorposition = i
				bool = true
			}
		}
	}
}
function ParseForNumber1(object) {
	phonevalue1 = ParseChar(object.value, zChar);
}
function ParseForNumber2(object) {
	phonevalue2 = ParseChar(object.value, zChar);
}
function ParseChar(sStr, sChar) {
	if (sChar.length == null) {
		zChar = new Array(sChar);
	} else
		zChar = sChar;

	for (i = 0; i < zChar.length; i++) {
		sNewStr = "";

		var iStart = 0;
		var iEnd = sStr.indexOf(sChar[i]);

		while (iEnd != -1) {
			sNewStr += sStr.substring(iStart, iEnd);
			iStart = iEnd + 1;
			iEnd = sStr.indexOf(sChar[i], iStart);
		}
		sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

		sStr = sNewStr;
	}

	return sNewStr;
}
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 47 && charCode < 58) || charCode < 31)
		return true;

	return false;
}

function fbShareCLRDetails(clrType, cId) {
	
	document.clrForm.clrType.value = clrType;
	document.clrForm.couponId.value = cId;
	document.clrForm.action = "/ScanSeeWeb/shopper/fbShareclrInfo.htm";
	document.clrForm.method = "POST";
	document.clrForm.submit();
}

/*--------------------------------Coupon Email share------------------------------------------*/
function sendMail(clrId) {
	var toEmailId = $("input[name='toEmail']").val();

	var fromEmailId = $("input[name='fromEmail']").val();

	var regEx = /^.+@.+\..{2,5}$/;

	if (toEmailId == "" && fromEmailId == "") {
		alert("Please Enter Valid EmailIds");
		return true;
	} else if (!regEx.test(fromEmailId)) {
		alert("Please Enter Valid From EmailId");
		return true;
	} else if (!regEx.test(toEmailId)) {
		alert("Please Enter Valid  To EmailId");
		return true;
	} else {

		$.ajax({
			type : "GET",
			url : "conssendmail.htm",
			data : {

				'clrId' : clrId,
				'clrType' : 'coupon',
				'toEmailId' : toEmailId,
				'fromEmailId' : fromEmailId,

			},

			success : function(response) {
				if (response == 'Email sent successfully.') {
					$(".cncl-actn").click();
					alert(response);
				} else {
					alert('Error occurred while sending mail');
				}
			},
			error : function(e) {

			}
		});

	}

}

/*--------------------------------Coupon Email share------------------------------------------*/

function getHotDealforCategory() {

	var hdCat = document.hotdealmainpage.catID.value;

	if (hdCat != "") {
		document.hotdealmainpage.action = "/ScanSeeWeb/shopper/conshotdealforcat.htm";
		document.hotdealmainpage.method = "POST";
		document.hotdealmainpage.submit();
	}

}
function hotDealFBShare(hotDealId, frompage) {

	document.hotdealmainpage.hotDealID.value = hotDealId;
	document.hotdealmainpage.redirectUrl.value = frompage;
	document.hotdealmainpage.action = "HotDealfbShare.htm";
	document.hotdealmainpage.method = "POST";
	document.hotdealmainpage.submit();
}
function getHotDealforDma() {

	var hdPopCentId = document.hotdealmainpage.populationCentId.value;

	if (hdPopCentId != "") {
		document.hotdealmainpage.action = "/ScanSeeWeb/shopper/conshotdealfordma.htm";
		document.hotdealmainpage.method = "POST";
		document.hotdealmainpage.submit();
	}

}
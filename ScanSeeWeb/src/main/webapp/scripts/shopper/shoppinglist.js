/**
 * 
 */

function getShoppingList() {
	document.shoppingList.action = "shoppingList.htm";
	document.shoppingList.method = "GET";
	document.shoppingList.submit();
}

function searchShoppingList(event) {
	if(event && event.keyCode == 13){
	document.shoppingList.action = "searchShoppingList.htm";
	document.shoppingList.method = "POST";
	document.shoppingList.submit();
	}else if (event && event.which == 13){
	document.shoppingList.action = "searchShoppingList.htm";
	document.shoppingList.method = "POST";
	document.shoppingList.submit();
	}else if (event == ''){
	document.shoppingList.action = "searchShoppingList.htm";
	document.shoppingList.method = "POST";
	document.shoppingList.submit();
	}else{
		 return true;
	}
}


function addToMainShoppingList(productId) {
	document.shoppingList.action = "addToMainShoppingList.htm";
	document.shoppingList.selectedProductId.value = productId;
	document.shoppingList.method = "POST";
	document.shoppingList.submit();
}

function addTodaySLProductsBySearch(productId) {
	document.shoppingList.action = "addTodaySLProductsBySearch.htm";
	document.shoppingList.selectedProductId.value = productId;
	document.shoppingList.method = "POST";
	document.shoppingList.submit();
}

function addRemoveTodaySLProducts(userProductId, userId) {
	// showLoader()
	$.ajaxSetup({cache:false})// Added this line AS IE is caching the Ajax requests
	$.ajax({
		type : "GET",
		url : "addRemoveTodaySLProducts.htm",
		data : {
			'userProductId' : userProductId,
			'userId' : userId
		},
		success : function(response) {
			// todetach()
			if (response != "FAILURE") {
				$('#shoppingView').replaceWith(response);
			}
		},
		error : function(e) {

		}
	});
}

function addRemoveSBProducts(userProductId, cartUserProductId, userId) {
	// showLoader()
	$.ajaxSetup({cache:false});// Added this line AS IE is caching the Ajax requests
	$.ajax({
		type : "GET",
		url : "addRemoveSBProducts.htm",
		data : {
			'userProductId' : userProductId,
			'userId' : userId,
			'cartUserProductId' : cartUserProductId
		},
		success : function(response) {
		// todetach()
			if (response != "FAILURE") {
				$('#shoppingView').replaceWith(response);
			}
		},
		error : function(e) {

		}
	});
}

function callproductSummarySC(productId) {
	if(productId == 0){
		alert("No Product Summary Found.");
		return false;
	}
	document.shoppingList.productId.value = productId;
	document.shoppingList.action = "productSummary.htm";
	document.shoppingList.method = "POST";
	document.shoppingList.submit();
}

function callproductSummarySCH(productId) {
	if(productId == 0){
		alert("No Product Summary Found.");
		return false;
	}
	document.shoppernotes.productId.value = productId;
	document.shoppernotes.action = "productSummary.htm";
	document.shoppernotes.method = "POST";
	document.shoppernotes.submit();
}


function saveUserNotes() {
	if(toggleFlag == true){
	var userNotes = $('#notes').val();
	document.shoppernotes.userNotes.value = userNotes;

	$.ajax({
		type : "GET",
		url : "saveUserNotes.htm",
		data : {
			'userNotes' : userNotes
		},
		success : function(response) {
			alert("User Notes saved successfully..");
		},
		error : function(e) {
		}
	});
}
}

function moveToHstry() {
	var r = confirm("Ready to Check-out?"+"\n"+"if you do, all your items will be moved to history.");
	if (r == true) {
		document.shoppingList.action = "checkOut.htm";
		document.shoppingList.method = "POST";
		document.shoppingList.submit();
	}

}
function showLoader() {

	$('body')
			.append(
					"<div id='loading-image'>"
							+ "<img src='../images/ajaxblue.gif' alt='Loading...' title='Loading' width='128' height='15' />"
							+ "<span id='loading-txt'>" + "Loading" + "</span>"
							+ "</div>");
	// To get the original text and append ... to the text get animated effect
	// Loading...
	var originalText = $("#loading-txt").text();
	i = 0;
	$("#loading-txt").css('color', '#ffffff');
	setInterval(function() {
		$("#loading-txt").append(".");
		i++;

		if (i == 4) {
			$("#loading-txt").html(originalText);
			i = 0;
		}

	}, 500);
	
/*
 * $(window).unbind('scroll'); //Navigate to other page automatically once
 * loading screen is displayed $("#loading-image").fadeIn('slow').data("active",
 * false).delay(10000) .fadeOut('slow', function() { //toPge();//function call
 * to navigate page
 * 
 * todetach(); // to remove the loading animation by calling this function. });
 */
var top = $(window).scrollTop();
	var left = $(window).scrollLeft()
	$(window).scroll(function() {
		$(this).scrollTop(top).scrollLeft(left);

	});
}

function todetach() {
	$("#loading-image").remove();
}


function deleteProductMainShopListFav(userProductId, userId) {
	// showLoader()
	$.ajax({
		type : "GET",
		url : "deleteProductMainShopListFav.htm",
		data : {
			'userProductId' : userProductId,
			'userId' : userId
		},
		success : function(response) {
			// todetach()
			if (response != "FAILURE") {
				$('#favDiv').replaceWith(response);
			}
		},
		error : function(e) {

		}
	});
}



 function outputSelected(chckBoxLen) {
 	 var check = new Array();
	    var index = 0;
	    var opt = document.shoppernotes.check;
	    var flag = false; 
	    if(chckBoxLen == 1 && document.shoppernotes.check.checked){
	    	flag = true;
	    	  check[1] = new Object;
	          check[1].value = document.shoppernotes.check.value;
	          check[1].index = 1;
	    }else{
	    for (var intLoop = 0; intLoop <  opt.length; intLoop++) {
	    	if ((opt[intLoop].check) ||
	           (opt[intLoop].checked)) {
	    	   flag = true;
	          index = check.length;
	          check[index] = new Object;
	          check[index].value = opt[intLoop].value;
	          check[index].index = intLoop;
	       }
	    }
	    }
	    
	    if(!flag){
	    	alert("Please select the product")
	    	return true;
	    }
		var strSel="";
		for (var item in check)       
               strSel += check[item].value + "\n";

    var curVal = $("#addItemsTo option:selected").val();
	$('img.addItemsImg').attr('title',curVal);
	$.ajaxSetup({cache:false});
	if($("#addItemsTo option:first:selected").val() == "Please Select")
	{
		alert("Note: Please Select Your Option");	
	}
    else{
    	$.ajax({
    		type : "GET",
    		url : "addItemstoSL.htm",
    		data : {
    			'strSel' : strSel,
    			'curVal' : curVal
    			
    		},
    		success : function(response) {
    			alert(response);
				if(curVal == "Favorites" || curVal == "List,Favorites"){
					for (var item in check) {
						var	idName = "star"+check[item].value;	
					var theImg = document.getElementById(idName);
						theImg.src = '../images/greenStar.png';
						theImg.alt = 'favorites';
					}	
					
				}
    		},
    		error : function(e) {
    		}
    	});
    }
    
 }

 

 function addRemoveFavSLProducts() {

	   var flag = false; 
	   var opt = document.shoppingList.check;
	   if(opt != undefined){
	    for (var intLoop = 0; intLoop < opt.length; intLoop++) {
	       if ((opt[intLoop].check) ||
	           (opt[intLoop].checked)) {
	    	   flag = true;
	       }
	    }
	    
	    if(!flag){
	    	alert(" please select any product");
	    	return true;
	    }
		}
		var flag = false; 
	    var opt = document.shoppingList.favCheck;
		var checkLength = $('form input[type=checkbox]:checked').size();
	
	     if(checkLength>0){
			flag = true;
		 }

	     if(!flag){
	      alert(" please select any product");
	      return true;
	     }

 	document.shoppingList.action = "addRemoveFavSLProducts.htm";
 	document.shoppingList.method = "POST";
 	document.shoppingList.submit();
 }

  function favLoad(sladdprodtlexists) {
	  if(sladdprodtlexists!=''){
		alert(sladdprodtlexists);
		}
	  }
  
  /*function onmouseover(){
	  $(this).closest('tr').find(
		'img[name$="DeleteRow"]').show();
  }
  function onmouseout(){
	  $(this).closest('tr').find(
		'img[name$="DeleteRow"]').hide();
  }*/
  
function AddToList(){
	var productName = $('input[name$="searchValue"]').val();;
	var productDescription = $('input[name$="prodDesc"]').val();
	document.shoppingList.productName.value = productName;
	document.shoppingList.productDescription.value = productDescription;
	document.shoppingList.action = "addUnassignedProduct.htm";
 	document.shoppingList.method = "POST";
 	document.shoppingList.submit();
}

function addItemsToSL(productId){
var searchKey = $('input[name$="searchValue"]').val();
	document.shoppingList.searchKey.value=searchKey;
	document.shoppingList.productId.value=productId;
	document.shoppingList.action = "addProductToSLFav.htm";
 	document.shoppingList.method = "POST";
 	document.shoppingList.submit();
	
}

function onSubmitSLHistory(){
	
	document.shoppingList.action = "slHistory.htm";
 	document.shoppingList.method = "POST";
 	document.shoppingList.submit();	
}
function searchShoppingListBack() {
	document.shoppingList.back.value = true;
	document.shoppingList.action = "searchShoppingList.htm";
	document.shoppingList.method = "POST";
	document.shoppingList.submit();
	
}
function searchSLBack() {
	document.productsummary.action = "searchShoppingList.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
	
}
function searchScannowBack() {
	document.productsummary.action = "scannowprodsearch.htm";
	document.productsummary.method = "POST";
	document.productsummary.submit();
	
}
var toggleFlag = false;
function toggleConfirm(){
	toggleFlag = true;
}
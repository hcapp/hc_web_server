// JavaScript Document

$(document)
		.ready(
				function() {
					
				$('img[name$="cpnTgl"]').css("cursor","pointer");
				$('img[name$="cpnTgl"]').click(function() {
				tglcpnImg(this);
				}); 
					 
				/*Shopping List activate options based on tab click*/	
				/*var tabd = $(".tabdPnl li a");
				if(tabd.hasClass("favorites") || tabd.hasClass("search")){
					$(".subCntrl li:not(:first-child)").hide();
					$(".subCntrl li:first").css("width","94.6%");
				}*/
					totalChkbx = $(".chkbxGrp").length;
					$("#ttlCnt").html(totalChkbx);
					
					/*Shopping List deleting checked items and category title */
					$('#delete').click(function () {
					
					var productIds = '';
					//$("#shpngList .chkbxGrp:checked")
				$('#shpngList .chkbxGrp:checked').each(function(){        
				var values = $(this).val();
				productIds += values;
					});
			
					var ctgryNm = [];
					$('#shpngList tr.selNode').each(function() {
						var ctgry = this.getAttribute('name');
						if(ctgryNm.indexOf(ctgry) === -1) {
							ctgryNm.push(ctgry);
						}
						$(this).remove();
						getCheckedCnt();
					});
					for(var i=0,len = ctgryNm.length; i< len; i++) {
						var trCnt = $('#shpngList tr[name="'+ctgryNm[i]+'"]');
						if(trCnt.length === 1){
							trCnt.remove();
						}
					}
					addprodtoshoplist(productIds);
					alert(productIds);
					
					
					});
					
				
					
				/************************End*************************/	
					$('#accordion a.innerLink').click(function(e) {
						e.stopPropagation();

					});

					/*-------------Disable Context Menu(Right Click) for input field: class name is the parameter---------------------*/
					$(".dsblContxMenu").bind("contextmenu", function(e) {
						e.preventDefault();
					});
					/*-------------Disable Context Menu(Right Click) for input field ends---------------------*/
					/* Set Placeholder text */
					setPlaceholderText('zipCode');

					if ($("#srch-find").length) {

						var p = $("#srch-find");
						var pos = p.position();
						$(".cstm-alert").css({
							"top" : pos.top,
							"left" : pos.left
						});
					}
					$(".alert-close").click(function() {
						$(".cstm-alert").hide();
					});

					$('#FindSearchHdr')
							.click(
									function() {

										var getUsrOptn = $(
												"input[name='srchOptns']:checked")
												.attr("value");
										if (getUsrOptn == "Products") {

											var zipcode = document.SearchFormHeader.zipCode.value;
											var searchKey = document.SearchFormHeader.searchKey.value;

											if (searchKey == '') {
												alert("Note: Please Enter keyword to search!");
												return true;

											}

											document.SearchFormHeader.action = "consfindprodsearch.htm";
											document.SearchFormHeader.method = "POST";
											document.SearchFormHeader.submit();

										}
										if (getUsrOptn == "Locations") {
											var categoryName = $(
													'input:radio[name=ctgryOptn]:checked')
													.val();

											document.SearchFormHeader.categoryName.value = categoryName;
											document.SearchFormHeader.action = "/ScanSeeWeb/shopper/findresultloctnlist.htm";
											document.SearchFormHeader.method = "POST";
											document.SearchFormHeader.submit();

										}
										if (getUsrOptn == "Deals") {
											var zipcode = document.SearchFormHeader.zipCode.value;
											document.SearchFormHeader.action = "consdealssearch.htm";
											document.SearchFormHeader.method = "POST";
											document.SearchFormHeader.submit();
										}
									});

					$("#hdr input[name='srchOptns']")
							.change(
									function() {
										var curItem = $(this).attr("value");
										if (curItem == "Locations") {
											offs = $(this).parent().offset();
											var drpdwn_top = offs.top + 32;
											var drpdwn_left = offs.left;
											var $highltPnl = $("<div id='overlayActn' style='top: "
													+ drpdwn_top
													+ "px; left: "
													+ drpdwn_left
													+ "px; position:absolute'></div>");
											if ($("#overlayActn").length == 0) {
												var $cont = $("#contWrpr")
														.append($highltPnl);
												$highltPnl
														.load(
																'/ScanSeeWeb/conscatoption.jsp',
																function() {
																	$(
																			".options")
																			.slideDown(
																					'fast');
																	$(
																			'#tglDsply')
																			.show();
																});
											}
										} else {
											$("#overlayActn").remove();
										}
									});
					$("#findCntrl input:not('#Products')").change(function() {
						$(".cstm-alert").hide();
					});

					// for hot deal share email action.
					$(".hotdealshareemailActn")
							.each(
									function() {
										$(this)
												.click(
														function() {

															var $getParentNode = $(
																	this)
																	.parents(
																			'div.contBox');// parent

															var hotdealId = $(
																	this).attr(
																	"name"); // to

															// fetch
															// product
															// id.
															var $curOverlay = $("<div class='email-overlay'>"
																	+ "<ul class='overlay-form'>"
																	+ "<li class='fromEmail'>"
																	+ "<span class='pop-title'>"
																	+ "<img src='../images/consumer/email_send.png'/> "
																	+ "Send Email</span></li><br/>"
																	+ "<li><label>From:</label>"
																	+ "<input type='text'  name='fromEmail'   /></li>"
																	+ "<li class='toEmail'><label>To:</label><input type='text'  name='toEmail'/></li>"
																	+ "<li><br/><br/><button type='button'  class='btn btn-success' onclick='shareHotdealInfoViaEmail("
																	+ hotdealId
																	+ ")'>Send</button> "
																	+ "<button class='btn btn-secondary cncl-actn'>Cancel</button>"
																	+ "</li></ul>"
																	+ "</div>");
															$(".email-overlay")
																	.remove();

															$(this)
																	.parents(
																			'div.contBlks')
																	.find(
																			"div.email-overlay")
																	.remove();
															$(this)
																	.parents(
																			'div.contBlks')
																	.find(
																			"div.contBox")
																	.removeClass(
																			"relative");
															$getParentNode
																	.addClass("relative");
															var $tglNode = $getParentNode
																	.append($curOverlay);

															getHdnVal = $(
																	"#emailId")
																	.attr(
																			"value");
															// var getHdnVal="";
															$(
																	"input[name='fromEmail']")
																	.attr(
																			"value",
																			getHdnVal);

															if (getHdnVal.length) {
																$(
																		"input[name='fromEmail']")
																		.attr(
																				"readonly",
																				"readonly");

															} else if (getHdnVal.length < 0) {
																$(
																		"input[name='fromEmail']")
																		.attr(
																				"readonly",
																				"false");

															}
															if (!getHdnVal) {
																$(
																		"input[name='fromEmail']")
																		.val("");
															}
															var curHt = $getParentNode
																	.height();
															var curWdt = $getParentNode
																	.width();
															$(this)
																	.parents(
																			'div.contBox')
																	.find(
																			".email-overlay")
																	.css(
																			"height",
																			curHt)
																	.slideDown();
															$(".cncl-actn")
																	.click(
																			function() {
																				$(
																						this)
																						.parents(
																								'div.contBox')
																						.find(
																								".email-overlay")
																						.remove();

																			});

															/*
															 * To get Label left
															 * margin
															 */
															var getLeftMrgn = $(
																	".overlay-form input[name='fromEmail']")
																	.position().left;
															var curLiPdng = parseInt($(
																	".overlay-form li")
																	.css(
																			"padding-left"));
															var actMrgn = getLeftMrgn
																	- curLiPdng;
															$(
																	".overlay-form label")
																	.css(
																			{
																				"margin-left" : actMrgn,
																				"font-weight" : "bold"
																			});
														});
									});
					// ******************************************************************************************************
					// FOR HOTDEAL DETAILS PAGE EMAIL ACTION.
					$(".hotdealdetailhareemailActn")
							.each(
									function() {
										$(this)
												.click(
														function() {

															var $getParentNode = $(
																	this)
																	.parents(
																			'div.contBox');// parent

															var hotdealId = $(
																	this).attr(
																	"name"); // to

															// fetch
															// product
															// id.
															var $curOverlay = $("<div class='email-overlay'>"
																	+ "<ul class='overlay-form'>"
																	+ "<li class='fromEmail'>"
																	+ "<span class='pop-title'>"
																	+ "<img src='../images/consumer/email_send.png'/> "
																	+ "Send Email</span></li>"
																	+ "<li><label>From:</label>"
																	+ "<input type='text'  name='fromEmail'   /></li>"
																	+ "<li class='toEmail'><label>To:</label><input type='text'  name='toEmail'/></li>"
																	+ "<li><button type='button'  class='btn btn-success' onclick='shareHotdealInfoViaEmail("
																	+ hotdealId
																	+ ")'>Send</button> "
																	+ "<button class='btn btn-secondary cncl-actn'>Cancel</button>"
																	+ "</li></ul>"
																	+ "</div>");
															$(".email-overlay")
																	.remove();

															$(this)
																	.parents(
																			'div.contBlks')
																	.find(
																			"div.email-overlay")
																	.remove();
															$(this)
																	.parents(
																			'div.contBlks')
																	.find(
																			"div.contBox")
																	.removeClass(
																			"relative");
															$getParentNode
																	.addClass("relative");
															var $tglNode = $getParentNode
																	.append($curOverlay);

															getHdnVal = $(
																	"#emailId")
																	.attr(
																			"value");
															// var getHdnVal="";
															$(
																	"input[name='fromEmail']")
																	.attr(
																			"value",
																			getHdnVal);

															if (getHdnVal.length) {
																$(
																		"input[name='fromEmail']")
																		.attr(
																				"readonly",
																				"readonly");

															} else if (getHdnVal.length < 0) {
																$(
																		"input[name='fromEmail']")
																		.attr(
																				"readonly",
																				"false");

															}
															if (!getHdnVal) {
																$(
																		"input[name='fromEmail']")
																		.val("");
															}
															var curHt = $getParentNode
																	.height();
															var curWdt = $getParentNode
																	.width();
															$(this)
																	.parents(
																			'div.contBox')
																	.find(
																			".email-overlay")
																	.css(
																			"height",
																			curHt)
																	.slideDown();
															$(".cncl-actn")
																	.click(
																			function() {
																				$(
																						this)
																						.parents(
																								'div.contBox')
																						.find(
																								".email-overlay")
																						.remove();

																			});

															/*
															 * To get Label left 
															 * margin
															 */
															var getLeftMrgn = $(
																	".overlay-form input[name='fromEmail']")
																	.position().left;
															var curLiPdng = parseInt($(
																	".overlay-form li")   
																	.css(
																			"padding-left"));
															var actMrgn = getLeftMrgn
																	- curLiPdng;
															$(
																	".overlay-form label")
																	.css(
																			{
																				"margin-left" : actMrgn,
																				"font-weight" : "bold"
																			});
														});
									});

					// ####################################
					/*-------------------------------Select your preferences group select & unselect code starts --------------------------*/
	$('#checkAll').live('click',function() {
	var ctgryName =  $('.preferencesLst tr.mainCtgry').attr('name');
	var status = $(this).attr("checked");

	if(status == "checked"){
	$('input[name$="favCatId"]').attr('checked','true');
	$(".preferencesLst tr").show();
	}
	else if(!status) {
		
	$(this).removeAttr("checked");	
	$('input[name$="favCatId"]').removeAttr("checked");
	$('.preferencesLst tbody tr:not(.mainCtgry)').hide();	
	}
	
	});
	
	$(".preferencesLst tr.mainCtgry input:checkbox").live('click',function() {	
		var maincatNm =  $(this).parents('tr').attr('name');
		var self = this;
		if(self.checked) {
			
			$('.preferencesLst tr[name="'+maincatNm+'"]').show();
			var cnt = $('.preferencesLst tr[name="'+maincatNm+'"]').length;
			$('tr[name="'+maincatNm+'"]').each(function(i) {
				if(i>0 && self.checked) {
					$(this).find('input:checkbox').attr('checked', 'checked');	
				}	 
			}); 
			
		}else {
			$('.preferencesLst tr:not(.mainCtgry)[name="'+maincatNm+'"]').hide();
			$('tr[name="'+maincatNm+'"]').each(function(i) {
			$(this).find('input:checkbox').attr('checked', self.checked);
			$('#checkAll').removeAttr("checked");									
			});
		}
		
	});
	$(".preferencesLst tr:not(.mainCtgry) input:checkbox").live('click',function() {
		var maincatNm =  $(this).parents('tr').attr('name');
		$('.preferencesLst tr.mainCtgry[name="'+maincatNm+'"] :checkbox').removeAttr("checked");	
		if($('tr:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length < 1) {
			$('tr[name="'+maincatNm+'"]').each(function(i) {
				if(i==0) {
					$(this).find('input:checkbox').removeAttr('checked');
					$('.preferencesLst tr[name="'+maincatNm+'"]:not(.mainCtgry)').hide();		
				}
			});
			
		}
		
		var totChk = $('.preferencesLst tr:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox').length;
		var chkLen = $('.preferencesLst tr:not(.mainCtgry)[name="'+maincatNm+'"] :checkbox:checked').length;
		if(chkLen == totChk){
			$('.preferencesLst tr.mainCtgry[name="'+maincatNm+'"] :checkbox').attr('checked','checked');
			
		}else{
			$('#checkAll').removeAttr('checked');		
		}
	}); 
	
	$("input[name='favCatId']").live('click',function(){
	if ($("input[name='favCatId']:checked").length == $("input[name='favCatId']").length) {
		$('#checkAll').attr("checked","true");	
	}
	});
	
	$("input[name='categoryDetailresponse.favCatId']").live('click',function(){
	if ($("input[name='categoryDetailresponse.favCatId']:checked").length == $("input[name='categoryDetailresponse.favCatId']").length) {
		$('#checkAll').attr("checked","true");	
	}
	});


/*-------------------------------Select your preferences group select & unselect code ends --------------------------*/

					$(".listView li").click(function() {
						$(".appsite-yes").show();
						$(".appsite-no").hide();
					});

					/*
					 * Email pop up panel: for my gallery Event: On click of
					 * email icon having class name "emailActn"
					 */
					$(".emailActn")
							.each(
									function() {
										$(this)
												.click(
														function() {
															var $getParentNode = $(
																	this)
																	.parents(
																			'div.contBox');// parent
															// div
															// should
															// contain
															// class
															// contBox
															var productIdListId = $(
																	this).attr(
																	"name"); // to
															// fetch
															// product
															// id.
															
															var splitRes=productIdListId.split(",");
															
															var productId=splitRes[0];
															var productListId=splitRes[1];
															var $curOverlay = $("<div class='email-overlay'>"
																	+ "<ul class='overlay-form'>"
																	+ "<li class='fromEmail'>"
																	+ "<span class='pop-title'>"
																	+ "<img src='../images/consumer/email_send.png'/> "
																	+ "Send Email</span></li>"
																	+ "<li><label>From:</label>"
																	+ "<input type='text'  name='fromEmail'   /></li>"
																	+ "<li class='toEmail'><label>To:</label><input type='text'  name='toEmail'/></li>"
																	+ "<li><button type='button'  class='btn btn-success' onclick='shareProdInfoViaEmail("
																	+ productId
																	+","
																	+productListId
																	+ ")'>Send</button> "
																	+ "<button class='btn btn-secondary cncl-actn'>Cancel</button>"
																	+ "</li></ul>"
																	+ "</div>");
															$(this)
																	.parents(
																			'div.contBlks')
																	.find(
																			"div.email-overlay")
																	.remove();
															$(this)
																	.parents(
																			'div.contBlks')
																	.find(
																			"div.contBox")
																	.removeClass(
																			"relative");
															$getParentNode
																	.addClass("relative");
															var $tglNode = $getParentNode
																	.append($curOverlay);
															var getHdnVal = 0;
															getHdnVal = $(
																	"#emailId")
																	.attr(
																			"value");
															// var getHdnVal="";
															$(
																	"input[name='fromEmail']")
																	.attr(
																			"value",
																			getHdnVal);

															if (getHdnVal.length > 0) {
																$(
																		"input[name='fromEmail']")
																		.attr(
																				"readonly",
																				"readonly");
															} else if (getHdnVal.length < 0) {
																$(
																		"input[name='fromEmail']")
																		.attr(
																				"readonly",
																				"false");
															}
															var curHt = $getParentNode
																	.height();
															var curWdt = $getParentNode
																	.width();
															$(this)
																	.parents(
																			'div.contBox')
																	.find(
																			".email-overlay")
																	.css(
																			"height",
																			curHt)
																	.slideDown();
															$(".cncl-actn")
																	.click(
																			function() {
																				$(
																						this)
																						.parents(
																								'div.contBox')
																						.find(
																								".email-overlay")
																						.remove();
																			});

															/*
															 * To get Label left
															 * margin
															 */
															var getLeftMrgn = $(
																	".overlay-form input[name='fromEmail']")
																	.position().left;
															var curLiPdng = parseInt($(
																	".overlay-form li")
																	.css(
																			"padding-left"));
															var actMrgn = getLeftMrgn
																	- curLiPdng;
															$(
																	".overlay-form label")
																	.css(
																			{
																				"margin-left" : actMrgn,
																				"font-weight" : "bold"
																			});

														});
									});
					/*--------------------------------Coupon Email share------------------------------------------*/
					$(".couponEmailActn")
							.each(
									function() {
										$(this)
												.click(
														function() {
															var $getParentNode = $(
																	this)
																	.parents(
																			'div.contBox');// parent
															// div
															// should
															// contain
															// class
															// contBox
															var couponId = $(
																	this).attr(
																	"name"); // to
															// fetch
															// product
															// id.
															var $curOverlay = $("<div class='email-overlay'>"
																	+ "<ul class='overlay-form'>"
																	+ "<li class='fromEmail'>"
																	+ "<span class='pop-title'>"
																	+ "<img src='../images/consumer/email_send.png'/> "
																	+ "Send Email</span></li>"
																	+ "<li><label>From:</label>"
																	+ "<input type='text'  name='fromEmail'   /></li>"
																	+ "<li class='toEmail'><label>To:</label><input type='text'  name='toEmail'/></li>"
																	+ "<li><button type='button'  class='btn btn-success' onclick='sendMail("
																	+ couponId
																	+ ")'>Send</button> "
																	+ "<button class='btn btn-secondary cncl-actn'>Cancel</button>"
																	+ "</li></ul>"
																	+ "</div>");
															$(this)
																	.parents(
																			'div.contBlks')
																	.find(
																			"div.email-overlay")
																	.remove();
															$(this)
																	.parents(
																			'div.contBlks')
																	.find(
																			"div.contBox")
																	.removeClass(
																			"relative");
															$getParentNode
																	.addClass("relative");
															var $tglNode = $getParentNode
																	.append($curOverlay);
															var getHdnVal = $(
																	"#emailId")
																	.attr(
																			"value");
															$(
																	"input[name='fromEmail']")
																	.attr(
																			"value",
																			getHdnVal);

															if (getHdnVal.length) {
																$(
																		"input[name='fromEmail']")
																		.attr(
																				"readonly",
																				"readonly");
															} else if (getHdnVal.length < 0) {
																$(
																		"input[name='fromEmail']")
																		.attr(
																				"readonly",
																				"false");
															}
															var curHt = $getParentNode
																	.height();
															var curWdt = $getParentNode
																	.width();
															$(this)
																	.parents(
																			'div.contBox')
																	.find(
																			".email-overlay")
																	.css(
																			"height",
																			curHt)
																	.slideDown();
															$(".cncl-actn")
																	.click(
																			function() {
																				$(
																						this)
																						.parents(
																								'div.contBox')
																						.find(
																								".email-overlay")
																						.remove();
																			});

														});
									});
					/*--------------------------------Coupon Email share------------------------------------------*/

					/*-------------------------------Reusable code for text truncate functuion call--------------------------*/

					if ($('.trim2').length > 0) {
						$(".trim2").truncateText({
							txtlength : 32
						// text limit
						});
					}

					if ($('.dealtrim').length > 0) {

						$(".dealtrim").truncateText({
							txtlength : 25
						// text limit
						});
					}
					if ($('.salesLst p.trnctTxt').length > 0) {
						$(".salesLst p.trnctTxt").truncateText({
							txtlength : 100
						// text limit
						});

					}

					if ($('.productTrim').length > 0) {

						$(".productTrim").truncateText({
							txtlength : 80
						// text limit
						});
					}

					if ($('.proddesctrim').length > 0) {
						$(".proddesctrim").truncateText({
							txtlength : 40
						// text limit
						});
					}
					
					if ($('.shopplsthistorytrim').length > 0) {

						$(".shopplsthistorytrim").truncateText({
							txtlength : 25
						// text limit
						});
					}
					if ($('.shopplstfavtrim').length > 0) {

						$(".shopplstfavtrim").truncateText({
							txtlength : 30
						// text limit
						});
					}
					if ($('.shopplstsearchtrim').length > 0) {

						$(".shopplstsearchtrim").truncateText({
							txtlength : 35
						// text limit
						});
					}
					if ($('.coupproddesctrim').length > 0) {
						$(".coupproddesctrim").truncateText({
							txtlength : 60
						// text limit
						});
					}
					/*-------------------------------Reusable code for text truncate functuion call--------------------------*/

					$("#accordion li").click(function() {
						$('li div.cstm-Cont').hide();
						$(".cstmAccordion li.active").removeClass("active");
						$(this).addClass("active");
						$(this).find('div.cstm-Cont').toggle();
					});

					$("#shpngList tr").css("cursor", "pointer");
					$("#shpngList tr a").click(function() {
						window.location.href = "productInfo.html";
					});

					$(".actnCntrl a.add-btn").click(function() {
						$(".subCntrl").slideToggle();
					});

					$(".rowStrip tr").mouseover(function() {
						$(this).addClass("over");
					}).mouseout(function() {
						$(this).removeClass("over");
					});
					$(".rowStrip tr:even").addClass("alt");

					/*-------------------------------Custom Checkbox code starts--------------------------*/
						var getChkbx2 = $(".cstmChkbx td").find('input[name="chkbxGrp"]');
	var cstmChkbx2 = $("<img name='grpChkbk' src='/ScanSeeWeb/images/consumer/Unchkd.png' style='position:absolute;'>");
	$(getChkbx2).after(cstmChkbx2);
	$('.cstmChkbx td input[name="chkbxGrp"]').css("visibility","hidden");
	//var strikeOut = $("<div class='strike'>s</div>");
	$('.cstmChkbx td:first-child').click(function() {									  
	var curChkbx2 = $(this).find('input[name="chkbxGrp"]');
	var getCstmImg2 = $(this).find('img[name="grpChkbk"]').attr("src");
	if (curChkbx2.is(":checked")) {
	$(this).find('img[name="grpChkbk"]').attr("src","/ScanSeeWeb/images/consumer/Unchkd.png");
	curChkbx2.attr('checked', false);
	$(this).parents('tr').removeClass("selNode");
	//$(this).parents('tr').find('.strike').remove();
	$(this).parents('tr').find('p a').removeClass("strike-out");
	getCheckedCnt();
	} else {
	$(this).find('img[name="grpChkbk"]').attr("src","/ScanSeeWeb/images/consumer/Chkd.png");
	curChkbx2.attr('checked', true);
	$(this).parents('tr').addClass("selNode");
	//$(this).parents('tr').find('td:eq(1)').append(strikeOut);
	$(this).parents('tr').find('p a').addClass("strike-out");
	getCheckedCnt();
	var tblId = $(this).parents('table').attr('id');
	
	if(tblId == "wishLst"){

	$('table').find("a.strike-out").removeClass("strike-out");
	}
	}
	
	if(document.documentMode === 8) {
		curChkbx2[0].click();
	}
	});
					
					
					
					/*-------------------------------Custom Checkbox code ends--------------------------*/
					$("div.favorites").show();
					$(".tabdCntrl li a").click(function() {
						//switchTab(jQuery(this));
						//switchOptns(jQuery(this));
					});

					$("#tabdContTgl li a").click(function() {
						switchTab(jQuery(this));
					});

					$(".closeCntrl").click(function() {
						$('.msgBx').fadeOut();
					});
					if ($("div.round").length > 0) {
						$('div.round').css("opacity", "0");
						$('#detailedList li').hoverIntent(function() {
							$(this).find('div.round').animate({
								"left" : "+150px",
								"opacity" : "1"
							}, {
								queue : false,
								duration : 500
							}).fadeIn('slow');
							AnimateRotate(360);
						}, function() {
							$(this).find('div.round').animate({
								"left" : "50px",
								"opacity" : "0"
							}, {
								queue : false,
								duration : 200
							}).fadeOut('fast');
							AnimateRotate(180);
						});
					}
					/*-------------------More & Less text toggle code ------------------*/
					var showChar = 80;
					var ellipsestext = "...";
					var moretext = "more";
					var lesstext = "less";
					$('.more')
							.each(
									function() {
										var content = $(this).html();
										if (content.length > showChar) {

											var c = content.substr(0, showChar);
											var h = content.substr(
													showChar - 0,
													content.length - showChar);

											var html = c
													+ '<span class="moreellipses">'
													+ ellipsestext
													+ '&nbsp;</span><span class="morecontent"><span>'
													+ h
													+ '</span>&nbsp;&nbsp;<a href="" class="morelink">'
													+ moretext + '</a></span>';

											$(this).html(html);
										}

									});

					$(".morelink").click(function() {
						if ($(this).hasClass("less")) {
							$(this).removeClass("less");
							$(this).html(moretext);
						} else {
							$(this).addClass("less");
							$(this).html(lesstext);
						}
						$(this).parent().prev().toggle();
						$(this).prev().toggle();
						return false;
					});
					/*-----------------More & Less text toggle code ends---------------------------------*/
					$('.sctnHdr a img').mouseover(function() {
						var curImg = $(this).attr("alt");
						$('i').hide();
						$('i.' + curImg).fadeIn('slow');
					}).mouseout(function() {
						$('i').hide();
					});

					$('#chngTitle li a').click(function() {
						var getTitle = $(this).text();
						$('span#chngTitle').html(getTitle);
					});

					var getChkbx = $("#detailedList li").find(
							'input[name="cart_mg"]');
					var position = getChkbx.position();
					var cstmChkbx = $("<img name='cstmChkbx' src='../images/consumer/chkbxUnchkd.png' style='position:absolute;'>");
					$(getChkbx).after(cstmChkbx);
					/*-------------------------------Custom Checkbox code starts--------------------------*/
					$('.cstmChkbx ul li input[name="cart_mg"]').css(
							"visibility", "hidden");
					$('#detailedList ul li.imgBx')
							.click(
									function() {
										var curChkbx = $(this).find(
												'input[name="cart_mg"]');
										var getCstmImg = $(this).find(
												'img[name="cstmChkbx"]').attr(
												"src");
										// alert(getCstmImg);
										if (curChkbx.is(":checked")) {
											$(this)
													.find(
															'img[name="cstmChkbx"]')
													.attr("src",
															"../images/consumer/chkbxUnchkd.png");
											curChkbx.attr('checked', false);
										} else {
											$(this)
													.find(
															'img[name="cstmChkbx"]')
													.attr("src",
															"../images/consumer/chkbxChkd.png");
											curChkbx.attr('checked', true);
										}
									});

					/*-------------------------------Custom Checkbox code ends--------------------------*/
					$('.dropdwnBx li a:last').css("border-bottom", "none");
					$("#settingActn").click(
							function() {
								$this = $(this).parents('#hdr').css("position",
										"relative");
								$(".dropdwnBx").toggle("slow");
							});
					$(".dropdwnBx li a").click(function() {
						$(".dropdwnBx").hide();
					});

					/* close dropdown when clicked outside of dropdown area */
					$(document).click(
							function(e) {
								var t = (e.target)
								/*
								 * get(0):zero-based integer indicating which
								 * element to retrieve.
								 */
								if (t != $(".dropdwnBx").get(0)
										&& t != $("#settingActn").get(0)) {
									$(".dropdwnBx").hide();
								}
							});

					/* check if group1 is present and call the colorbox function */
					/*
					 * if($(".group1").length) {
					 * $(".group1").colorbox({rel:'group1'}); }
					 */
					$("#imgslide .imgWrp").each(function() {
						var $a = $(this).find('a');

						$a.colorbox({
							rel : $a.attr('class')
						});
					});
					$('#cart ul li:nth-child(4n)').css("border-right", "0px");
					$('#deal ul li:nth-child(4n)').css("border-right", "0px");
					$('#footerWrpr a:last').css("background", "none");

					$(".subTabs li.disable").click(function(e) {
						e.preventDefault();
					});
					$(window).load(
							function() {
								$("#iframe").attr("src",
										"http://www.scansee.com/welcomeconsumer/");
							});

					$(".contBlks span img").live('mouseover', function() {
						this.src = this.src.replace("_down", "_up");
					}).live('mouseout', function() {
						this.src = this.src.replace("_up", "_down");
					});

					/*-------------------------------slideshow for find page starts--------------------------*/
					$("#imgslide li div.imageBlock")
							.each(
									function() {

										// $(this).find(".group1").colorbox({rel:'group1'});

										// image slide show
										if ($("#imgslide").length) {
											// Declare variables
											var getName = $(this).find(
													".imgWrp a").attr("name");
											// alert(getName);
											var getImgName = $(this).find(
													".imgWrp a").attr("name",
													getName).length;
											// alert(getImgName);
											var totalImages = jQuery(this)
													.find(".imgWrp img").length;
											var $img = jQuery(this).find(
													".imgWrp");
											imageWidth = jQuery(this).find(
													".imgWrp img:first")
													.outerWidth(true) + 3;
											totalWidth = (imageWidth)
													* totalImages;
											stopPosition = (totalWidth - imageWidth);
											$(".imgWrp").width(totalWidth);
											// Left arrow click
											jQuery(this)
													.parents('li')
													.find(".lt")
													.click(
															function() {
																if ($img
																		.position().left < 0
																		&& !$img
																				.is(":animated")) {
																	$img
																			.animate({
																				left : "+="
																						+ imageWidth
																						+ "px"
																			});
																}
																return false;
															});
											// Right arrow click
											jQuery(this)
													.parents('li')
													.find(".rt")
													.click(
															function() {
																if (($img
																		.position().left * -1) < stopPosition
																		&& !$img
																				.is(":animated")) {
																	$img
																			.animate({
																				left : "-="
																						+ imageWidth
																						+ "px"
																			});
																}
																return false;
															});

										}
									});
					/*-------------------------------slideshow for find page ends--------------------------*/

					/*-------------------------------Tabbed Content: display content on tab click----------*/
					var getActv = $("#mainTab li").index();
					$('.splitL div.tab' + getActv).css("display", "block");
					$("#mainTab li").click(
							function() {
								var getIndex = $(this).index();
								var gtActv = $(this).find('li.active');
								$("#mainTab li").removeClass('active');
								$(this).addClass('active');
								$('.tabCont').hide();
								$('.splitL div.tab' + getIndex).css("display",
										"block");
							});
					$('#mainTab').find('li.active').trigger('click');

					var getCurCnt = $('.subTabdPnl li a img').attr("alt");
					$('.splitL div.' + getCurCnt).css("display", "block");
					$(".subTabdPnl li").click(function() {
						var getAlt = $(this).find('a img').attr("alt");
						var gtActv = $(this).find('li.active');
						$(".subTabdPnl li").removeClass('active');
						$(this).addClass('active');
						$('.subTabdCnt').hide();
						$('.splitL div.' + getAlt).css("display", "block");
					});

					$(".subTabs li a")
							.click(
									function() {
										var getClass = $(this).find('h3').attr(
												"class");
										var $li = $(
												'.subTabdPnl li img[alt ="'
														+ getClass + '"]')
												.parents('li');
										if (!$li.is('.active')) {
											$('.subTabdPnl li.active')
													.removeClass("active");
											$(
													'.subTabdPnl li img[alt ="'
															+ getClass + '"]')
													.parents('li').addClass(
															"active").show();

											$(".subTabdCnt").hide();
											$('.splitL div.' + getClass).css(
													"display", "block");
										}
									});

					/*---------Show tick mark on Category selection in category ------------ */

					$('#RtlrCtgry li').click(
							function() {
								$this = $(this);
								$this.find('input[name="ctgryOptn"]').attr(
										"checked", "checked");
								$('#RtlrCtgry li').removeClass("active");
								$this.addClass("active");
								$('#RtlrCtgry li i').removeClass("tick");
								$this.find('i').addClass('tick');
								// window.location.href="findResultLoctn.html"
							});
					/*--------- Display Categories on selection of location radio btn------------ */
					$("input[name='srchOptns']").change(function() {
						var getOptn = $(this).attr("value");
						if (getOptn == "Locations") {
							$('#CategoryDsply').show();
							$('#tglDsply').slideToggle('normal');
						} else {
							$('#tglDsply').hide();
						}

					});
					/*--------- Page Navigation based on radiobutton selection------------ */
					$('#FindSearch')
							.click(
									function() {

										var getUsrOptn = $(
												"input[name='srchOptns']:checked")
												.attr("value");
										if (getUsrOptn == "Products") {

											if ($("#srch-find").val() == "") {
												$(".cstm-alert").show();
												return false;
											} else {
												findprodsearch('');
											}

											// window.location.href=findprodsearch.;
										}
										if (getUsrOptn == "Locations") {
											var categoryName = $(
													'input:radio[name=ctgryOptn]:checked') 
													.val();

											document.SearchForm.categoryName.value = categoryName;
											document.SearchForm.action = "/ScanSeeWeb/shopper/findresultloctnlist.htm";
											document.SearchForm.method = "POST";
											document.SearchForm.submit();

										}
										if (getUsrOptn == "Deals") {
											finddealssearch('');
											// window.location.href="deals.html"
										}
									});

					$('#retlrList tr').click(function() {
						window.location.href = "findResultLoctnLstPgs.html"
					});

					/*--------- Placeholder Text ------------ */

					var plchldrtxt = $('input.plcHldr').val();
					$('input.plcHldr').focus(function() {
						curVal = $(this).val();
						// alert(curVal);
						$(this).val('');
					});
					/*
					 * $('input.plcHldr').blur(function() {
					 * $('input.plcHldr').attr('value',plchldrtxt); });
					 */

					/*--------- Fixed control bar on page scroll exceeding 260px height------------ */
					window.onscroll = function() {
						var top = window.pageXOffset ? window.pageXOffset
								: document.documentElement.scrollTop ? document.documentElement.scrollTop
										: document.body.scrollTop;
						if (top > 260) {
							$("#stickyHdr").addClass("activate").fadeIn(
									'normal');
						} else {
							$("#stickyHdr").removeClass("activate");

						}

					}
					/* checkbox checked get values in array */

					$('#clipCpn').click(function() {
						var someGlobalArray = new Array;
						someGlobalArray = [];
						$('input[name="cart_mg"]:checked').each(function() {
							someGlobalArray.push($(this).val());
						});
						console.log(someGlobalArray);

					});

					$("#accordion li.noNavigation").unbind('click').css(
							"cursor", "default").find("img[alt='arrow']").css({
						"opacity" : "0.4",
						"background" : "#fff"
					});

					$("#accordion li.noNavigation").click(function() {
						$(".cstm-Cont").hide()
					});
				});

// To resize the html page
function resizeDoc() {
	var headerPanel = document.getElementById('hdr');
	var footerPanel = document.getElementById('ftr_sec');
	var resizePanel = document.getElementById('contWrpr');
	/* var dockPanel = document.getElementById('dockPanel'); */

	if (document.getElementById('wrapper')) {
		if (getWinDimension()[1]
				- (headerPanel.offsetHeight + footerPanel.offsetHeight + 26) > 0) {
			resizePanel.style.minHeight = getWinDimension()[1]
					- (headerPanel.offsetHeight + footerPanel.offsetHeight + 26)
					+ "px";
		}
	}
}

// To get client height
function getWinDimension() {
	var windowHeight = "";
	var windowWidth = "";

	if (!document.all || isOpera()) {
		windowHeight = window.innerHeight;
		windowWidth = window.innerWidth;
	} else {
		windowHeight = document.documentElement.clientHeight;
		windowWidth = document.documentElement.clientWidth;
	}
	return [ windowWidth, windowHeight ]

}
// check if the client is opera
function isOpera() {
	return (navigator.appName.indexOf('Opera') != -1);

}

function editPswd(userId) {
	$('.contBlks')
			.append(
					"<div class='mailpopPnl'>"
							+ "<div class='mailInfo'>"
							+ "<div class='actTitleBar'>"
							+ "Change Password:"
							+ "<img src='../images/actnClose.png' alt='close' title='close' name='actClose' align='right'/>"
							+ "</div>"
							+ "<ul>"
							+ "<li>"
							+ "<span>"
							+ "Enter Password:"
							+ "</span>"
							+ "<input type='password' id='password' class='textboxSmall'/> "
							+ "</li>"
							+ "<li>"
							+ "<span>"
							+ "Confirm Password:"
							+ "</span>"
							+ "<input type='password' id='repassword' class='textboxSmall' /> "
							+ "</li>"
							+ "<li>"
							+ "<input type='button' value='Update' class='btn' id='emailId' onclick='updateUsrPwd("
							+ userId + ")'/>"
							+ "<input type='reset' value='Clear' class='btn'/>"
							+ "</li>" + "</ul>" + "</div>" + "</div>");
	$('.mailInfo li:eq(0) span').width(109);
	$('img[name$="actClose"]').click(function() {
		$('.mailpopPnl').remove();
	});
}

function updateUsrPwd(userId) {
	var password = document.getElementById("password").value;
	var repassword = document.getElementById("repassword").value;

	if (password == null || repassword == null || password == ""
			|| repassword == "") {
		alert("Please enter Password");
	} else if (password.length < 8 || repassword.length < 8) {
		alert('Password lenght shoulb be atleast eight characters');
	}

	else if (!(password.toString() == repassword.toString())) {

		alert("Password Fileds Does't match");
	} else {
		$.ajax({
			type : "GET",
			url : "updateUsrPwd.htm",
			data : {
				'userId' : userId,
				'password' : password
			},

			success : function(response) {
				if (response == 'Password updated') {

					$('.mailpopPnl').remove();

					alert(response);

				} else {

					alert('Error occurred while updating the password.');

				}

			},
			error : function(e) {

			}
		});

	}

}
function AnimateRotate(d) {
	$({
		deg : 0
	}).animate({
		deg : d
	}, {
		step : function(now, fx) {
			$(".round").css({
				transform : "rotate(" + now + "deg)"
			});
		}
	});
}
/*
 * jQuery.fn.getCheckboxVal = function(){ alert("S"); var vals = []; var i = 0;
 * this.each(function(){ vals[i++] = jQuery(this).val(); }); return vals; }
 */

function switchTab(obj) {

$('.tbdcnt').hide();
   	$('.tabdCntrl li').removeClass("active");
    var id = obj.attr("class");
	if(id == "favorites"){
		$(".subCntrl li:not(:first-child)").hide();
	}
    $('.'+id).show();
    obj.parent().addClass("active"); 
	if($(".subCntrl").length){
		$(".subCntrl").hide();
	}
	if(id == "history"){
	$(".subCntrl li:first").css("width","33.3%");
	$(".subCntrl li").show();
	}
	if(id == "favorites" || id == "search"){
	$(".subCntrl li:not(:first-child)").hide();
	$(".subCntrl li:first").css("width","94.6%");
	}
	if(id == "history" || id == "search"){
		$(".actnCntrl li a.del-btn").hide();
	}
	if(id == "favorites"){
		$(".actnCntrl li a.del-btn").show();
	}

}

/*
 * This function for Opening the iframe popup by using the any events & we
 * cann't able to access any thing in the screen except popup parameters:
 * popupId : Popup Container ID IframeID : Iframe ID url : page path i.e src of
 * a page height : Height of a popup width : Width of a popup name : here name
 * means Header text of a popup btnBool : if you have any buttons means we can
 * use it i.e true/false
 */
function openIframePopup(popupId, IframeID, url, height, width, name, btnBool) {
	frameDiv = document.createElement('div');
	frameDiv.className = 'framehide';
	document.body.appendChild(frameDiv);
	document.getElementById(IframeID).setAttribute('src', url);
	// frameDiv.setAttribute('onclick', closePopup(obj,popupId));
	document.getElementById(popupId).style.display = "block"
	height = (height == "100%") ? frameDiv.offsetHeight - 20 : height;
	width = (width == "100%") ? frameDiv.offsetWidth - 16 : width;
	document.getElementById(popupId).style.height = height + "px"
	document.getElementById(popupId).style.width = width + "px"
	var marLeft = -1 * parseInt(width / 2);
	var marTop = -1 * parseInt(height / 2);
	document.getElementById('popupHeader').innerHTML = name;
	document.getElementById(popupId).style.marginLeft = marLeft + "px"
	document.getElementById(popupId).style.marginTop = marTop + "px"
	var iframeHt = height - 27;
	document.getElementById(IframeID).height = iframeHt + "px";
	if (btnBool) {
		var btnHt = height - 50;
		document.getElementById(IframeID).style.height = btnHt + "px";
	}

}
/*
 * This function for Closing the iframe popup parameters: popupId : Popup
 * Container ID IframeID : Iframe ID
 */
function closeIframePopup(popupId, IframeID, funcUrl) {

	try {
		document.body.removeChild(frameDiv);
		document.getElementById(popupId).style.display = "none";
		document.getElementById(popupId).style.height = "0px";
		document.getElementById(popupId).style.width = "0px";
		document.getElementById(popupId).style.marginLeft = "0px";
		document.getElementById(popupId).style.marginTop = "0px";
		document.getElementById(IframeID).removeAttribute('src');
		$("#" + IframeID).attr('src', 'about:blank');
		if (funcUrl) {
			funcUrl
		}
	} catch (e) {
		top.$('.framehide').remove();
		top.document.getElementById(popupId).style.display = "none";
		top.document.getElementById(popupId).style.height = "0px";
		top.document.getElementById(popupId).style.width = "0px";
		top.document.getElementById(popupId).style.marginLeft = "0px";
		top.document.getElementById(popupId).style.marginTop = "0px";
		top.document.getElementById(IframeID).removeAttribute('src');
		if (funcUrl) {
			funcUrl
		}
	}
}
(function($) {
                // jQuery plugin definition
                $.fn.truncateText = function(params) {
                                
                                // merge default and user parameters
                                params = $.extend( {txtlength: 20}, params);
                                // traverse all nodes
                                this.each(function() {
                                var myellipse = "...";
                                var gettxt = $(this).text();
                                if(gettxt.length > params.txtlength){
                                var shortText = jQuery.trim(gettxt).substring(0, params.txtlength )
                                .split(" ").slice(0, -1).join(" ") + myellipse;
                                $(this).text(shortText);
                                }
                                else {
                                $(this).text(gettxt);
                                }
                                });
                                // allow jQuery chaining
                                return this;
                };
})(jQuery);

/*-------------------------------Reusable code for text truncate functuion call--------------------------*/

// This method is used for image slide show and will be invoked on clicking
// images.
// class id is the class attribute values of the anchor element.
function popImg(classid) {
	var cid = "." + classid;
	$(cid).colorbox({
		rel : classid,
		height : "500",
		width : "600"
	});

}

function setPlaceholderText(inputid) {

	if ($.browser.webkit) {
		// alert( "this is webkit!" );
		return false;
	}
	if ($.browser.mozilla && $.browser.version >= "2.0") {
		// alert('Mozilla above 1.9');
		return false;
	}

	var plchldrVal = $('#' + inputid);

	var zipCodeval = $('#' + inputid).val();

	if (plchldrVal.length == 0) {

	}
	if (zipCodeval == '') {

		plchldrVal.each(function(i, plchldr) {
			plchldr = $(plchldr);
			var getPlchldr = plchldr.attr('placeholder');
			if (!getPlchldr)
				return true;

			plchldr.addClass('setplchldr');
			plchldr.attr('value', getPlchldr);
			plchldr.focus(function(e) {
				if (plchldr.val() == getPlchldr) {
					plchldr.removeClass('setplchldr');
					plchldr.attr('value', '');
				}
			});
			plchldr.blur(function(e) {
				if ($.trim(plchldr.val()) == '') {
					plchldr.addClass('setplchldr');
					plchldr.attr('value', getPlchldr);
				}
			});
		});
	}
}

function getChkdArray(){
	var arr = [];
	$('input[type="checkbox"]:checked').each(function(i){
	arr.push($(this).parents('tr').index());
	});
	return arr; 
	}


function getCheckedCnt() {
var chkdCnt = $(".cstmChkbx .chkbxGrp:checked").length;
var chkBx = $(".cstmChkbx .chkbxGrp").length;
var tmpChk =  $(".chkbxGrp").length;
var cartCnt = parseInt(tmpChk-chkdCnt);
$("#slctCnt").html(chkdCnt);
$("#ttlCnt").html(cartCnt);
}
	
	
					
function tglcpnImg(obj) {
/*Coupon add icon chng*/
		var curRowData = $(obj).closest('tr').html();
		var image = $(obj);
		if (image.attr("src") == "/ScanSeeWeb/images/consumer/cpnAddicon.png")
		{		
		$(image).attr("src", "/ScanSeeWeb/images/consumer/cpnsIcon.png");
		var coupId =image.attr("id");
		
		if (coupId != '' && coupId != 'undefined')
		{
		var fields = coupId.split(',');
		conswladdcoupon(fields[0],fields[1]);
		}
		alert("Coupon Added to Gallery");
		}
		else
		{
		
		var coupId =image.attr("id");
		
		if (coupId != '' && coupId != 'undefined')
		{
		var fields = coupId.split(',');
		conswlremovecoupon(fields[0]);
		}
		image.attr("src", "/ScanSeeWeb/images/consumer/cpnAddicon.png");
		
		alert("Coupon Removed from Gallery");
		}
}
function shopwlhome()
{
alert("please log in or sign up - it's free ");
}
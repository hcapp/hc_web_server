<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="/ScanSeeWeb/scripts/shopper/consumerglobal.js"
	type="text/javascript"></script>
</head>
<body>
	<div class='nub'>&nbsp;</div>
	<div class="options" id="tglDsply">
		<ul id="RtlrCtgry">
			<c:if
				test="${sessionScope.categoryInfo ne null &&  !empty sessionScope.categoryInfo}">

				<c:forEach items="${sessionScope.categoryInfo}" var="item">
					<li class=""><i></i> <input name=ctgryOptn type="radio"
						value="${item.catDisName}" /> <img src="${item.catImgPth}"
						alt="arts" /> <span>${item.catDisName}</span>
					</li>

				</c:forEach>

			</c:if>
		</ul>
	</div>
</body>
</html>

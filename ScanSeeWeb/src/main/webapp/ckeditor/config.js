/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/*CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.language = 'es';
	config.uiColor = '#f3f3f3';
	config.height = 300;
	config.toolbarCanCollapse = true;
	config.extraPlugins = 'wordcount';
	config.wordcount = {

		// Whether or not you want to show the Word Count
		showWordCount: true,

		// Whether or not you want to show the Char Count
		showCharCount: true,
		
		// Maximum allowed Word Count
		maxWordCount: 5,

		// Maximum allowed Char Count
		maxCharCount: 20
	};
};
*/

CKEDITOR.on('instanceReady', function (evt) {
    //editor
    var editor = evt.editor;

    //webkit not redraw iframe correctly when editor's width is < 310px (300px iframe + 10px paddings)
    if (CKEDITOR.env.webkit && parseInt(editor.config.width) < 310) {
    	if(editor.name == 'retPageShortDescription' || editor.name == 'rtlrshrtDsc' || editor.name == 'spclOffrlngDesc' || editor.name == 'spclOffrshrtDesc')
    	{
		   	var iframe = document.getElementById('cke_contents_' + editor.name).getElementsByTagName('iframe')[0];        
		    iframe.style.display = 'none';
		    iframe.style.display = 'block';
    	}
    }
});
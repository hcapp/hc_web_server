<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<script type="text/javascript">
	$(document).ready(function() {
		$('#divUploadImage input').attr("disabled", true);
	});
	
		function getFileTrigger(val){
			var vFile = document.getElementById("imageFile");
			  var FileUploadPath = vFile.value;
			  if (FileUploadPath != '' && FileUploadPath != 'undefined')
			  {
						var dotIndex = FileUploadPath.lastIndexOf('.');
						var length = FileUploadPath.length;
						var ext = FileUploadPath.substr(dotIndex + 1, length);
						if (ext == 'png') {
							$('#divUploadImage input').removeAttr('disabled');
							return true;
						} else {
							$('#divUploadImage input').attr('disabled',true);
							return false;
						}
			 }
		}
		
	/*$(document).ready(function() {
		$('#uploadImage').attr("disabled", true);
	});

	$('#imageFile').live('change', function() {
		$('#uploadImage').removeAttr('disabled')
	});*/

	$('#uploadImage')
			.live(
					'click',
					function() {
						$("#productInfoVO")
								.ajaxForm(
										{

											success : function(response) {

												//alert(response.getElementsByTagName('imageScr')[0].firstChild.nodeValue)
												$('#imageUploadTemp')
														.attr(
																"src",
																response
																		.getElementsByTagName('imageScr')[0].firstChild.nodeValue);
											}

										//target : '#preview'
										}).submit();

					});

</script>



<div id="wrapper">
	<div id="dockPanel">
		<ul id="prgMtr" class="tabs">
			<!-- 	<li><a title="Home" href="#" rel="home">Home</a></li> -->
			<li><a href="https://www.scansee.net/links/aboutus.html"
				title="About HubCiti" target="_blank" rel="About HubCiti">About
					HubCiti</a></li>
			<!-- <li> <a title="Create Profile" href="Manufacturer/Mfg_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->
			<li><a class="tabActive" title="Product Setup" href="#"
				rel="Product Setup">Product Setup</a>
			</li>
			<li><a title="Choose Your Plan" href="choosePlan.htm"
				rel="Choose Your Plan">Choose Plan</a>
			</li>
			<li><a title="Dashboard" href="dashboard.htm" rel="Dashboard">Dashboard</a>
			</li>
		</ul>
		<div id="tabdPanelDesc" class="floatR tglSec">
			<div id="filledGlass">
				<img src="images/Step3.png" />
				<div id="nextNav">
					<a href="#" onclick="location.href='/ScanSeeWeb/choosePlan.htm'">
						<img class="NextNav_R" alt="Next" src="images/nextBtn.png" /> <span>Next</span>
					</a>
				</div>

			</div>
		</div>
		<div id="tabdPanel" class="floatL tglSec">
			<img alt="Flow1" src="images/Flow4_mfg.png" />
		</div>
	</div>
	<!-- <div id="togglePnl">
		<a href="#"> <img src="images/downBtn.png" alt="down" width="9"
			height="8" /> Show Panel</a>
	</div> -->
	<div id="content" class="">

		<div id="subnav">
			<ul>
				<li><a href="#" class="active" title="Add Product"><span>Add
							Product</span> </a></li>
				<li><a href="batchuploadsupplier.htm" title="Batch Upload"><span>Batch
							Upload</span> </a></li>
				<li><a href="registprodmanageprod.htm" title="Manage Products"><span>Manage
							Products</span> </a></li>
			</ul>
		</div>
		<div class="grdSec">
			<form:form method="POST" enctype="multipart/form-data"
				commandName="productInfoVO" name="productInfoVO" id="productInfoVO"
				acceptCharset="ISO-8859-1" action="/ScanSeeWeb/uploadtempimg.htm">
				<form:hidden path="prdAttributes" />
				<form:hidden path="btnType" name="btnType" />
				<form:hidden path="values" />
				<form:hidden path="attributeHtml" />
				<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
					width="100%" align="left">
					<tbody>

						<tr>
							<td class="header" colspan="4">Click on the batch upload tab
								to upload a file with all of your product information, or scroll
								below to manually enter each product. <br>For recurring XML
									or API setups, please contact <a
									href="mailto:support@hubciti.com?Subject=XML/API%20setup&body=I%20am%20interested%20in%20a%20XML/API%20setup.">support@hubciti.com</a><br>
										Uploading your product information will allow consumers to view
										your product(s) online and on the mobile application.<br>
											<label style="${requestScope.fontStyle}">${requestScope.produstatus}</label>
											<label style="${requestScope.fontStyle}">${requestScope.productexists}</label>
							</td>
						</tr>
						<tr>
							<td class="Label" width="18%"><label class="mand">Product
									UPC</label>
							</td>
							<td width="113"><form:errors cssStyle="color:red"
									path="productUPC" cssClass="error"></form:errors> <form:input
									id="productUpc" name="productUPC" path="productUPC" type="text"
									maxlength="20" />
							</td>
							<td class="Label" width="18%"><label class="mand">Product
									Name</label>
							</td>
							<td width="32%"><form:errors cssStyle="color:red"
									path="productName" cssClass="error"></form:errors> <form:input
									id="productName" path="productName" name="productName"
									type="text" maxlength="500" /></td>

						</tr>
						<tr>
							<td class="Label" width="18%">Model Number</td>
							<td width="113"><form:errors cssStyle="color:red"
									path="modelNumber" cssClass="error"></form:errors> <form:input
									id="modelNumber" name="modelNumber" path="modelNumber"
									type="text" maxlength="50" />
							</td>
							<td class="Label" width="18%"><label class="mand">Suggested
									Retail Price</label>
							</td>
							<td width="32%"><form:errors cssStyle="color:red"
									path="suggestedRetailPrice" cssClass="error"></form:errors> <form:input
									id="suggestedRetailPrice" path="suggestedRetailPrice"
									name="suggestedRetailPrice" type="text" />
							</td>
						</tr>
						<tr>
							<td class="Label">Product&nbsp;Image(s)</td>
							<td width="113" id="imgSwap">&nbsp;<form:errors
									cssStyle="color:red" path="imageFile" id="imageFile"
									cssClass="error"></form:errors> <img id="imageUploadTemp"
								src="/ScanSeeWeb/images/alternateImg.png" width="64" height="90" /><span
								style="DISPLAY: block; PADDING-TOP: 5px">&nbsp;</span>
							</td>
							<td colspan="2"><form:input class="multi" accept="png"
									maxlength="5" type="file" name="imageFile" path="imageFile"
									id="imageFile"  onchange="getFileTrigger(this.value);"/><span class="instTxt nonBlk">[ Please Choose .png format images ]</span> <br /> <br /> 
									<div id="divUploadImage">
									 <input class="btn zeroMargin" value="View" type="button" id="uploadImage" name="Cancel" alt="Upload" />
									</div>
									</td>
						</tr>
						<tr>
							<td class="Label"><label class="mand">Long
									Description</label>
							</td>

							<td width="113"><form:errors cssStyle="color:red"
									path="longDescription" cssClass="error"></form:errors> <form:textarea
									id="longDescription" class="txtAreaSmall"
									path="longDescription" name="longDescription"
									onkeyup="checkMaxLength(this,'1000')" />
							</td>
							<td class="Label">Warranty / Service Information (optional)</td>
							<td><form:textarea id="warrantyORService"
									class="txtAreaSmall" path="warrantyORService"
									name="warrantyORService" onkeyup="checkMaxLength(this,'255')" />
							</td>
						</tr>
						<tr>
							<td class="Label">Short Description</td>
							<td width="113"><form:input id="shortDescription"
									path="shortDescription" name="shortDescription" type="text"
									maxlength="255" />
							</td>
							<td class="Label">Category of Product</td>
							<td><form:select path="productCategory" class="selecBx"
									id="retailID">
									<form:option value="0" label="--Select--">--Select-</form:option>
									<c:forEach items="${sessionScope.categoryList}" var="s">
										<form:option value="${s.categoryID}"
											title="${s.parentSubCategory}">${s.parentSubCategory}</form:option>
									</c:forEach>
								</form:select></td>
						</tr>
						<tr>

							<td class="Label">Audio Upload</td>
							<td width="113" align="left"><form:errors
									cssStyle="color:red" path="audioFile" cssClass="error"></form:errors>
								<form:input class="multi" accept="mp3" maxlength="5" type="file"
									path="audioFile" id="audioFile" name="audioFile"
									onchange="checkAudioType(this);" /><span class="instTxt nonBlk">[ Please Upload .mp3 format file ]</span></td>
							<td class="Label">Video Upload</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="videoFile" cssClass="error"></form:errors> <form:input
									class="multi" accept="mp4" maxlength="5" type="file"
									path="videoFile" id="videoFile" name="videoFile"
									onchange="checkVideoType(this);" /><span class="instTxt nonBlk">[ Please Upload .mp4 format file ]</span></td>
						</tr>

					</tbody>
				</table>

				<div class="clear"></div>
				<form action="">
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="grdTbl zeroBtmMrgn brdrTop">
						<tbody>
							<tr>
								<td width="18%" class="Label"><label for="label">Attribute
								</label>
								</td>
								<td width="32%"><select name="select" class="attrList"
									id="attrDropDown">
										<option selected="selected" value="">Please Select</option>
										<c:forEach items="${sessionScope.attrList}" var="attList">
											<option value="${attList.prodAttributesID}">${attList.prodAttributeName}</option>
										</c:forEach>
								</select></td>
								<td width="18%">Attribute Content</td>
								<td width="32%"><input name="attrCont" type="textboxSmall"
									id="attrCont" /> <a href="#"> <img src="images/add.png"
										alt="add" title="add" width="16" height="16"
										onclick="crtAtrr();" /> </a>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
				<div class="attributesGrd tglDsply" id="attrDispDiv">
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="grdTbl zeroBtmMrgn" id="attrDpsly">
						<tbody>

						</tbody>
					</table>
				</div>
				<div class="navTabSec mrgnRt" align="right">
					<input class="btn" value="Clear" type="button" name="ClearValues"
						onclick="clearFormRegisterProductSetUp();" title="Clear the form" />
					<!--  <input class="btn" value="Submit" onclick="window.location.href='Manufacturer/Mfg_choosePlan.html'" type="button" name="Cancel3"/> -->
					<input class="btn" type="submit" value="Save"
						onclick="submitRegProdct('Save');" name="Cancel3" title="Save" />
					<input class="btn" type="submit" value="Continue"
						onclick="location.href='/ScanSeeWeb/choosePlan.htm'"
						name="Cancel3" title="Continue" />
				</div>
			</form:form>
		</div>

	</div>
</div>

<script>
	//Loading Dynamically generated attribute HTML if page is loaded because of validation errors

	//Loading Dynamically generated attribute HTML if page is loaded because of validation errors

	var attHtml = document.productInfoVO.attributeHtml.value;
	if (attHtml != '' && attHtml != 'undefined') {
		//var attLabel = document.productInfoVO.attLabel.value;
		//if (attLabel != '' && attLabel != 'undefined') {
		document.getElementById('attrDispDiv').innerHTML = attHtml;
		$('.tglDsply').show();
		//}
	}
</script>


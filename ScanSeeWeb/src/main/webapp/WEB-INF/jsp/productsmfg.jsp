
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
	$(document).ready(function() {
		$('#divUploadImage input').attr("disabled", true);
	});

	function getFileTrigger(val) {
		var vFile = document.getElementById("imageFile");
		var FileUploadPath = vFile.value;
		if (FileUploadPath != '' && FileUploadPath != 'undefined') {
			var dotIndex = FileUploadPath.lastIndexOf('.');
			var length = FileUploadPath.length;
			var ext = FileUploadPath.substr(dotIndex + 1, length);
			if (ext == 'png') {
				$('#divUploadImage input').removeAttr('disabled');
				return true;
			} else {
				$('#divUploadImage input').attr('disabled', true);
				return false;
			}
		}
	}

	/*$(document).ready(function() {
		$('#uploadImage').attr("disabled", true);
	});

	$('#imageFile').live('change', function() {
		$('#uploadImage').removeAttr('disabled')
	});*/
</script>
<form:form method="POST" enctype="multipart/form-data"
	modelAttribute="productInfoVO" commandName="productInfoVO"
	id="productInfoVO" name="productInfoVO" action="/ScanSeeWeb/upload.htm">
	<form:hidden path="prdAttributes" />
	<form:hidden path="values" />
	<form:hidden path="attributeHtml" />
	<div id="wrapper">
		<div id="content">
			<%@include file="leftnavigation.jsp"%>
			<div class="grdSec contDsply floatR">
				<div id="secInfo">Manage Products</div>

				<div id="subnav">
					<ul>
						<li><a href="#" class="active" title="Add Product"><span>Add
									Product</span> </a>
						</li>
						<li><a href="batchupload.htm" title="Batch Upload"><span>Batch
									Upload</span> </a>
						</li>
						<li><a href="manageProduct.htm" title="Manage Products"><span>Manage
									Products</span> </a>
						</li>
					</ul>
				</div>

				<div class="">
					<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
						width="100%" align="left">
						<tbody>

							<tr>
								<td class="header" colspan="4">Click on the batch upload
									tab to upload a file with all of your product information, or
									scroll below to manually enter each product. <br>For
									recurring XML or API setups, please contact <a
									href="mailto:support@hubciti.com?Subject=XML/API%20setup&body=I%20am%20interested%20in%20a%20XML/API%20setup.">support@hubciti.com</a><br>
									Uploading your product information will allow consumers to view
									your product(s) online and on the mobile application.<br>

									<label id="responseText"
									style="font-weight: bold; color: #00559c;"></label></td>
							</tr>

							<tr>
								<td class="Label" width="18%"><label class="mand">Product
										UPC</label>
								</td>
								<td width="113"><label id="productUpcErr"
									style="color: red; font-style: 45; font-style: italic;"></label>
									<form:input id="productUpc" path="productUPC" name="productUPC"
										type="text" maxlength="20" />
								</td>
								<td class="Label" width="18%"><label class="mand">Product
										Name</label>
								</td>
								<td width="32%"><label id="productNameErr"
									style="color: red; font-style: 45; font-style: italic;"></label>
									<form:input id="productName" path="productName"
										name="productName" type="text" maxlength="500" /></td>

							</tr>
							<tr>
								<td class="Label" width="18%">Model Number</td>
								<td width="113"><form:errors cssStyle="color:red"
										path="modelNumber" cssClass="error"></form:errors> <form:input
										id="modelNumber" path="modelNumber" name="modelNumber"
										type="text" maxlength="50" />
								</td>
								<td class="Label" width="18%"><label>Suggested
										Retail Price</label>
								</td>
								<td width="32%"><form:errors cssStyle="color:red"
										path="suggestedRetailPrice" cssClass="error"></form:errors> <form:input
										id="suggestedRetailPrice" path="suggestedRetailPrice"
										name="suggestedRetailPrice" type="text"
										onKeyup="checkMaxLength(this,'1000');"
										onkeypress="return isNumberKeyPhone(event)" />
								</td>
							</tr>
							<tr>
								<td class="Label">Product&nbsp;Image(s)</td>
								<td width="113">&nbsp;<form:errors cssStyle="color:red"
										path="imageFile" cssClass="error"></form:errors> <img
									id="image" src='/ScanSeeWeb/images/blankImage.gif' width="64"
									height="90" /><span style="DISPLAY: block; PADDING-TOP: 5px">&nbsp;</span>
								</td>
								<td colspan="2"><form:input class="multi" type="file"
										path="imageFile" id="imageFile" name="imageFile" accept="png"
										maxlength="5" onchange="getFileTrigger(this.value);" /><span
									class="instTxt nonBlk">[ Please Choose .png format
										images ]</span> <br /> <br />
									<div id="divUploadImage">
										<input class="btn zeroMargin" value="View" type="button"
											id="uploadImage" name="Cancel" alt="Upload" />
									</div></td>
							</tr>
							<tr>
								<td class="Label"><label>Long Description</label>
								</td>
								<td width="113"><form:errors cssStyle="color:red"
										path="longDescription" cssClass="error"></form:errors> <form:textarea
										id="longDescription" class="txtAreaSmall"
										path="longDescription" name="longDescription"
										onkeyup="checkMaxLength(this,'1000')" />
								</td>
								<td class="Label">Warranty / Service Information (optional)</td>
								<td><form:textarea id="warrantyORService"
										class="txtAreaSmall" path="warrantyORService"
										name="warrantyORService" onKeyup="checkMaxLength(this,'255');" />
								</td>
							</tr>
							<tr>
								<td class="Label">Short Description</td>
								<td width="113"><form:input id="shortDescription"
										name="shortDescription" path="shortDescription" type="text"
										maxlength="255" />
								</td>
								<td class="Label">Category of Product</td>
								<td><form:select path="productCategory" class="selecBx"
										id="retailID">
										<form:option value="0" label="--Select--">--Select-</form:option>
										<c:forEach items="${sessionScope.categoryList}" var="s">
											<form:option value="${s.categoryID}"
												title="${s.parentSubCategory}">${s.parentSubCategory}</form:option>
										</c:forEach>
									</form:select></td>
							</tr>
							<!--<tr>
			               <td class="Label">Warranty</td>
			               <td align="left" colspan="3">
			                 <label>
			                   <select class="textboxBig" id="select" name="select"> <option>Please Select</option></select> </label>
			               </td>
			             </tr>-->
							<tr>
								<td class="Label">Audio Upload</td>
								<td width="113" align="left"><form:errors
										cssStyle="color:red" path="audioFile" cssClass="error"></form:errors>
									<form:input class="multi" maxlength="5" accept="mp3"
										type="file" name="audioFile" path="audioFile" id="audioFile"
										onchange="checkAudioType(this);" /><span
									class="instTxt nonBlk">[ Please Upload .mp3 format file
										]</span></td>
								<td class="Label">Video Upload</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="videoFile" cssClass="error"></form:errors> <form:input
										class="multi" accept="mp4" maxlength="5" type="file"
										name="videoFile" path="videoFile" id="videoFile"
										onchange="checkVideoType(this);" /><span
									class="instTxt nonBlk">[ Please Upload .mp4 format file
										]</span>
								</td>

							</tr>
						</tbody>
					</table>
					<div class="clear"></div>
					<form action="">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="grdTbl zeroBtmMrgn brdrTop">
							<tbody>
								<tr>
									<td width="18%" class="Label"><label for="label">Attribute
									</label>
									</td>
									<td width="32%"><select name="select" class="attrList"
										id="attrDropDown">
											<option selected="selected" value="">Please Select</option>
											<c:forEach items="${sessionScope.attrList}" var="attList">
												<option value="${attList.prodAttributesID}">${attList.prodAttributeName}</option>
											</c:forEach>
									</select></td>
									<td width="18%">Attribute Content</td>
									<td width="32%"><input name="attrCont" type="textboxSmall"
										id="attrCont" /> <a href="#"> <img src="images/add.png"
											alt="add" title="add" width="16" height="16"
											onclick="crtAtrr();" /> </a>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					<div class="attributesGrd tglDsply" id="attrDispDiv">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="grdTbl zeroBtmMrgn" id="attrDpsly">
							<tbody>

							</tbody>
						</table>
					</div>
					<div class="navTabSec mrgnRt" align="right">
						<input class="btn" value="Clear" type="button" name="Cancel3"
							onClick="clearAddProductForm();" title="Clear the form" />
						<!--  <input class="btn" value="Submit" onclick="window.location.href='Manufacturer/Mfg_choosePlan.html'" type="button" name="Cancel3"/> -->
						<input class="btn" type="button" value="Submit" id="addProduct"
							name="Cancel3" title="Submit" />
					</div>
				</div>


			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>

</form:form>
<div class="clear"></div>
<script>
	//Loading Dynamically generated attribute HTML if page is loaded because of validation errors

	//Loading Dynamically generated attribute HTML if page is loaded because of validation errors

	var attHtml = document.productInfoVO.attributeHtml.value;
	if (attHtml != '' && attHtml != 'undefined') {
		//var attLabel = document.productInfoVO.attLabel.value;
		//if(attLabel != '' && attLabel != 'undefined'){
		document.getElementById('attrDispDiv').innerHTML = attHtml;
		$('.tglDsply').show();
		//}
	}
</script>
<script type="text/javascript">
	$('#uploadImage')
			.live(
					'click',
					function() {

						$("#productInfoVO").attr("action",
								"/ScanSeeWeb/uploadtempimg.htm");

						$("#productInfoVO")
								.ajaxForm(
										{

											success : function(response) {

												//	alert(response.getElementsByTagName('imageScr')[0].firstChild.nodeValue)
												$('#image')
														.attr(
																"src",
																response
																		.getElementsByTagName('imageScr')[0].firstChild.nodeValue);
											}

										//target : '#preview'
										}).submit();

					});
	$('#addProduct')
			.bind(
					'click',
					function() {
						$("#productInfoVO").attr("action",
								"/ScanSeeWeb/upload.htm");
						var attributes = [];
						var values = [];
						var errorFlag = false;
						var productUPC = document.getElementById('productUpc').value;
						var productName = document
								.getElementById('productName').value;

						if (productUPC == "") {

							$('#productUpcErr').text(
									"Please enter ProductUPC code");
							errorFlag = true;

						} else {
							$('#productUpcErr').text("");
						}

						if (productName == "") {

							$('#productNameErr').text(
									"Please enter product name");
							errorFlag = true;

						} else {
							$('#productNameErr').text("");
						}

						$("#attrDpsly")
								.find("tr")
								.each(
										function(index) {
											// if(index > 0){
											var highlightObj = $(
													$("#attrDpsly").find("tr")[index])
													.children();
											errorFlag = false;
											highlightObj
													.find("input")
													.each(
															function(rVal) {

																var inputTagVal = $(this)[0].value;
																var name = $(
																		this)
																		.attr(
																				"name");
																errorFlag = false;
																if (inputTagVal == ''
																		|| inputTagVal == 'undefined'
																		|| inputTagVal == 'null') {
																	alert('Please Enter values for attribute');
																	errorFlag = true;
																} else {
																	attributes[index] = name;
																	values[index] = inputTagVal;
																}

															});

											if (errorFlag) {
												return false;
											}
											// }
										});

						if (!errorFlag) {
							var attrHtml = $('.attributesGrd').html();
							document.productInfoVO.attributeHtml.value = attrHtml;
							document.productInfoVO.prdAttributes.value = attributes
									.toString();
							document.productInfoVO.values.value = values
									.toString();
							showProgressBar();
							$("#productInfoVO")
									.ajaxForm(
											{
												success : function(response) {
													$('#loading-image').css("visibility","visible");
													var responseText = response
															.getElementsByTagName('responseText')[0].firstChild.nodeValue;
													if (responseText== "Success") {
														alert('Product added successfully.');
														$('#image').attr("src",'/ScanSeeWeb/images/blankImage.gif');
														$('#responseText').text('Product added successfully');
														$("#productInfoVO").reset();
														$('.MultiFile-label,#attrDispDiv').hide();

													} else {
														alert(responseText);
														$('#responseText').text(responseText);
													}
													//code to hide progress bar(manju)
													$('#loading-image').css("visibility","hidden");
													$('html,body').css("overflow","auto");
												}
											}).submit();

						}

					});
</script>

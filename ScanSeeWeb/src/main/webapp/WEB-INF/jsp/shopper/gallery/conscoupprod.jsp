<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>
	
	
<body onresize="resizeDoc();" onload="resizeDoc();">

	<div id="contWrpr" class="">
		<form:form commandName="clrForm" name="clrForm">
<div class="breadCrumb">
			<ul>
				<li class="brcIcon">
					<img src="../images/consumer/mg_bcIcon.png" alt="mygallery" />
				</li>
				<li title="My Gallery">
					<a href="consmygallery.htm">My Gallery</a>
				</li>		
				<c:if test="${requestScope.couponType eq 'Clipped'}">
				<li title="Clipped">	
					<a href="consmygallery.htm">Clipped</a>
				</li>
				</c:if>
				<c:if test="${requestScope.couponType eq 'Used'}">
				<li title="Used">
					<a href="consgalleryused.htm">Used</a>
				</li>
				</c:if>
				<c:if test="${requestScope.couponType eq 'Expired'}">
				<li title="Expired">
					<a href="consgalleryexp.htm">Expired</a>
				</li>
				</c:if>
				<c:if test="${requestScope.couponType eq 'All'}">
				<li title="All">
					<a href="consgalleryall.htm">All</a>
				</li>
				</c:if>
				<li class="active">
				<a href="javascript:void(0);"
						onclick="javascript:history.go(-1);">${requestScope.couponName} </a>
					
				</li>
				<li> Qualifying Products
				</li>
				
				</ul>
				</div>
			<div class="contBlks" id="cpnDetails">
				<div class="subHdr-control clear-fix">
					<div class="floatL min-padding">
					<label
							style="color: Black; font-family: Geneva, Arial; font-size: 18px;">Select
								Products To Add</label>
								</div>
								
					
					
					<div class="floatR vertical-top" >
					<a href="javascript:void(0);"><img alt="Shopping List"
							src="/ScanSeeWeb/images/consumer/shopping-list-enable.png" 
							title="Add To Shopping List" onclick="addcoupprodtosl()" width="80" height="45"/>
							<img alt="Wish List"
							src="/ScanSeeWeb/images/consumer/wish-list-enable.png"
							width="55" height="42" title="Add To Wish List"  onclick="addcoupprodtowl()"/>
							</div> </a>
							
							
				</div>
				<div class="qualifyProdCtr">
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="brdrTblQP" id="conscoupprod">
						<tbody>

							<c:choose>
								<c:when test="${sessionScope.conscoupprod ne null}">

									<c:forEach items="${conscoupprod.productDetail}"
										var="prodDetail">

										<tr>
											<td width="5%" align="center"><input type="checkbox"
												value="${prodDetail.productId}," class="coupChkbx" name="coupChkbx">
											</td>
											<td width="10%" class="img"><img
												src="${prodDetail.imagePath}"
												onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
												alt="product1" width="70" height="52" /></td>
											<td width="85%"><p>
													<a
														href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${prodDetail.productId}&key2=1&page=CG"
														class="title proddesctrim" title="${prodDetail.productName}">
														${prodDetail.productName} </a>
												</p>
												<p align="left" class="title">
													<span class="title coupproddesctrim"
														title="${prodDetail.productShortDescription}">
														${prodDetail.productShortDescription} </span>
												</p>
											</td>
										</tr>
									</c:forEach>
								</c:when>
							</c:choose>




						</tbody>
					</table>
				</div>
			</div>


		</form:form>
	</div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script type="text/javascript">

	function addCoupon(couponId, usage,id,from) {
		// get the form values
	
      $.ajaxSetup({cache:false})
		

		$.ajax({
			type : "GET",
			url : "addcoupon.htm",
			data : {
				'couponId' : couponId,
				'usage' : usage,
				'from'	: from
			},

			success : function(response) {
				$('#'+id).replaceWith(response);
			},
			error : function(e) {
				
			}
		});

	
	}

	function addRebate(rebateId, usage,id) {
		// get the form values
	    
       $.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "addrebate.htm",
			data : {
				'rebateId' : rebateId,
				'usage' : usage
			},

			success : function(response) {
					$('#'+id).replaceWith(response);
				},
				error : function(e) {
					
				}
			});

	
	}

	function addLoyality(loyaltyDealId, usage,id) {
		// get the form values
	
       $.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "addloyality.htm",
			data : {
				'loyaltyDealId' : loyaltyDealId,
				'usage' : usage
			},

			success : function(response) {
				
				
					$('#'+id).replaceWith(response);
					
				
				
				},
				error : function(e) {
					
				}
			});

	
	}
	
	function getPageLoad() {

	document.thislocation.action = 'getpagedclr.htm';
	document.thislocation.method = "GET";
	document.thislocation.submit();

}

function couponSearch(){
	$('.popPnl').css("display","block");
	if($(".popPnl").length==0){
	$('.stretch').append("<div class='popPnl'>" + "<div class='popupwrpr'>" + "</div>" + "</div>");
	var getTbl = $("#loadTbl").css("display","block");
	$('.popupwrpr').append(getTbl);
	}
	$('.pophdr span img').click(function() {
	$('.popPnl').hide();
	});
	
}

function search(){
	
	document.clrForm.clrType.value = "C";
	document.clrForm.action = "galleryhome.htm";
	document.clrForm.method = "GET";
	document.clrForm.submit();	
}

	
</script>


<div id="content" class="topMrgn">
<form:form commandName="clrForm" name="clrForm">
<form:hidden path="couponId"/>
<form:hidden path="rebateId"/>
<form:hidden path="loyaltyDealID"/>
<form:hidden path="clrType"/>
<form:hidden path="requestType" value="all"/>
<form:hidden path="lowerLimit" value="${requestScope.allGalleryResult.lowerLimit}"/>
<form:hidden path="pageFlowType"/>
<form:hidden path="added"/> 
<form:hidden path="clrImagePath"/> 
<form:hidden path="viewableOnWeb"/>
	<div class="navBar">
      <ul>
        <!--<li><a href="#"><img src="images/backBtn.png" onclick="javascript:back();" alt="back"/></a></li>-->
        <li class="titletxt">All</li>
        <!--<li class="floatR mrgnRt"><a href="index.html"><img src="images/logoutBtn.png" alt="logout" /></a></li>-->
      </ul>
    </div>
    <div class="clear"></div>
    <%--Gallery Content Start --%>
    <div class="mobCont noBg">
     <div class="splitDsply floatR">
			<ul class="tabdBtnTgl cstmTabs">
                <li class="noRtbrdr active txtcntr"><a href="galleryhome.htm">Coupons</a></li>
				<li class="txtcntr"><a href="#">Loyalty</a></li>
            </ul> 
			<div class="fluidViewHD">
				<div class="stretch cmnPnl relatvDiv">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd infoPnl htlt mobGrd" id="cpnGallery" >
						<col width="7%"/>
						<col width="68%"/>
						<col width="11%"/>
						<col width="8%"/>
						<col width="10%"/>
						<c:if test="${empty requestScope.allGalleryResult.loygrpbyRetlst}">
								<span id="NoDataInfo">No Data found!</span>
						</c:if>
						
						<c:if test="${!empty requestScope.allGalleryResult.loygrpbyRetlst}">
								<c:forEach items="${requestScope.allGalleryResult.loygrpbyRetlst}"	var="cpnObject">
									<tr>	<td colspan="5" class="mobSubTitle">${cpnObject.cateName}</td>
									</tr>
									<c:forEach items="${cpnObject.couponDetails}"
									var="cpnObject1">
									<tr>
										<td width="7%" align="center">
										<c:if test="${cpnObject1.favFlag eq 'false'}">										
											<img src="../images/cpnAddicon.png"	id="cpnTg${cpnObject1.couponId}" 
											alt="Cpn" width="27" height="25" name="cpnTgl" onclick="addCoupon(${cpnObject1.couponId},'${cpnObject1.favFlag}','cpnTg'+${cpnObject1.couponId},'all')"/>
										</c:if>
										<c:if test="${cpnObject1.favFlag eq 'true'}">
											<img src="../images/cpnIcon.png" alt="Cpn" width="27" height="25" name="cpnTgl" />
										</c:if>
										</td>
										<td width="68%" class="wrpWord"><span>
											<c:choose>
												<%-- Checking whether coupon is External or internal coupon if External coupon link to coupon site else internal Coupons info page--%>
												<c:when	test="${cpnObject1.couponURL ne null && cpnObject1.couponURL ne 'NotApplicable'}">
														<a href="${cpnObject1.couponURL}" target="_blank">${cpnObject1.couponName}</a>
												</c:when>
												<c:otherwise>
													<a href="#"	onclick="getCouponDetails(${cpnObject1.couponId},'${cpnObject1.favFlag}','${cpnObject1.couponImagePath}','${cpnObject1.viewableOnWeb}')">${cpnObject1.couponName}</a>
												</c:otherwise>
											</c:choose></span>
											<ul>
											<c:choose>
												<c:when	test="${cpnObject1.couponDiscountAmount ne 'NotApplicable'}">
													<li>${cpnObject1.couponDiscountAmount} Off Expires ${cpnObject1.couponExpireDate}</li>
												</c:when>
												<c:otherwise>
													<li>$0.00 Off Expires ${cpnObject1.couponExpireDate}</li>
												</c:otherwise>
											</c:choose>
											</ul>
										</td>
										<td class="wrpWord">&nbsp;</td>
										<c:choose>
											<c:when	test="${cpnObject1.couponImagePath ne null && cpnObject1.couponImagePath ne 'NotApplicable'}">
												<td width="8%" align="center">
													<img src="${cpnObject1.couponImagePath}" width="38" height="38"	onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';"/>
												</td>
											</c:when>
											<c:otherwise>
												<td width="8%" align="center"></td>
											</c:otherwise>
										</c:choose>
										
										<td width="9%">
										</td>
									</tr>
								</c:forEach>
							</c:forEach>   
							</c:if>          
				</table>
				
			<div class="iphonePagn">
				<ul>
					<li><c:if test="${requestScope.allGalleryResult.lowerLimit ne 0}">
							<a href="#">
								<img src="../images/prevBtn.png" alt="previous" width="50" height="29" align="left"	title="Prev" 
									onclick="getNextClrRecords('Previous');" /> 
							</a>
						</c:if>
					</li>
					<li><c:if test="${requestScope.allGalleryResult.nextPage eq 1}">
							<a href="#">
								<img src="../images/nxtBtn.png" alt="next"	width="50" height="29" align="right" id="Next" title="Next"
										onclick="getNextClrRecords('Next')" /> 
							</a>
						</c:if>
					</li>
				</ul>
			</div>
          </div>
          <div id="loadTbl">
            <div class="subHdr pophdr">Search<span><img src="../images/actnClose.png" alt="close" /></span></div>
            <table width="60%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><form:radiobutton path="searchType" id="searchType" value="Coupon" checked="checked"/>
                  Coupon
                  </td>
                <td><form:radiobutton path="searchType" id="searchType" value="Loyalty" />
                  Loyalty</td>
              </tr>
              <tr>
                <td colspan="2"><label>
                  <form:select path="categoryID" id="categoryID">
					<c:forEach items="${sessionScope.categories}" var="s">
						<form:option value="${s.categoryID}">${s.categoryName}</form:option>
					</c:forEach>
                  <!--  <option>All Categories-->
                  </form:select>
                  </label></td>
              </tr>
              <tr>
                <td colspan="2">
                <form:select path="retailerId" id="retailerId">
                    <option>All</option>
                </form:select></td>
              </tr>
              <tr>
                <td colspan="2"><label>
                  <form:input path="searchKey" id="searchKey" />
                  </label></td>
              </tr>
              <tr>
                <td align="left"><input type="button" class="btn" value="Search" onclick="search();"/></td>
                <td align="left"></td>
              </tr>
            </table>
          </div>
        </div>
	</div>
      <jsp:include page="../leftmenu.jsp"></jsp:include>
      <div class="clear"></div>
	  
    <div class="tabBar">
      <ul id="sLTab">
		<li> <a href="coupongallery.htm">
			<img src="../images/tab_btn_up_cpnGlry.png" alt="coupongalry" width="80" height="50" />
		</a></li>
        
        <li><a href="galleryused.htm">
				<img src="../images/tab_btn_up_used.png" alt="Used" width="80" height="50" /> 
		</a></li>
		<li><a href="galleryexp.htm">
			<img src="../images/tab_btn_up_expired.png" alt="Expired" width="80" height="50" />
		</a></li>
		<li> <a href="#">
			<img src="../images/tab_btn_up_cpnSearch.png" alt="couponSearch" width="80" height="50" id="cpnsrch" onclick="couponSearch();" />
		</a></li>
      </ul>
    </div>
  </form:form>
</div>
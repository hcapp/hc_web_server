<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<link href="/ScanSeeWeb/styles/consumerstyle.css" type="text/css"
	rel="stylesheet" />
<body onresize="resizeDoc();" onload="resizeDoc();">

	<form:form commandName="clrForm" name="clrForm">
		<form:hidden path="recordCount" />
		<form:hidden path="couponId" />
		<form:hidden path="added" />
		<form:hidden path="returnURL" value="consgalleryexp.htm" />
		<form:hidden path="clrImagePath" />
		<form:hidden path="viewableOnWeb" />
		<form:hidden path="clrType" />
		<form:hidden path="lowerLimit"
			value="${requestScope.galleryResult.lowerLimit}" />
		<input type="hidden" name="emailId" id="emailId"
			value="${sessionScope.emailId}" />
		<div id="contWrpr" class="">
			<ul class="secTabs" id="chngTitle">
				<li title="Clipped Coupons"><a href="consmygallery.htm"><img
						src="../images/consumer/clippedIcon.png" alt="Clipped" />Clipped</a>
				</li>
				<li title="Used Coupons"><a href="consgalleryused.htm"><img
						src="../images/consumer/clippedUsedIcon.png" alt="Used" />Used</a>
				</li>
				<li title="Expired Coupons"><a href="consgalleryexp.htm" class="active"><img
						src="../images/consumer/clippedExpiredIcon_actv.png" alt="Expired" />Expired</a>
				</li>
				<li title="All Coupons"><a href="consgalleryall.htm"><img
						src="../images/consumer/clippedAllIcon.png" alt="All" />All</a>
				</li>
				<span id="chngTitle">Expired</span>
			</ul>
			<div class="clear"></div>
			<div class="breadCrumb">
				<ul>
					<li class="brcIcon brcIconSharp"><img
						src="../images/consumer/mg_bcIcon.png" alt="mygallery" />
					</li>
					<li><a href="consmygallery.htm" title="My Gallery">My Gallery</a>
					</li>
					<!--<li><a href="#">Deals</a></li>-->
					<li class="active">Expired</li>
				</ul>
			</div>
			<div class="contBlks cstmChkbx relative" id="detailedList">
				<c:set var="pin" value="0" />
				<c:if test="${empty requestScope.expGalleryResult.loygrpbyRetlst}">
					<div class="zeroBg contBlks ">
						<img alt="close"
						src="/ScanSeeWeb/images/consumer/noneAvailCpns_poster.png" />

					</div>
				</c:if>
				<c:if test="${!empty requestScope.expGalleryResult.loygrpbyRetlst}">
					<c:forEach items="${requestScope.expGalleryResult.loygrpbyRetlst}"
						var="cpnObject">
						<div id="category${cpnObject.cateId}" class="post">
							<h3>
								<b>${cpnObject.cateName}</b>
							</h3>
							<c:forEach items="${cpnObject.couponDetails}" var="cpnObject1">

								<div id="cpnTg${cpnObject1.couponId}" class="contBox">
									<ul>
										<c:set var="discount"
											value="${cpnObject1.couponDiscountAmount}" />
										<c:choose>
											<c:when
												test="${cpnObject1.couponImagePath ne null && cpnObject1.couponImagePath ne 'NotApplicable' && !empty cpnObject1.couponImagePath}">
												<li class="imgBx"><img class="default"
													src="${cpnObject1.couponImagePath}"
													onerror="this.src = '/ScanSeeWeb/images/consumer/noImg.png';"
													width="160px" height="160px" /> 
													<div class="round"
														style="opacity: 0; transform: rotate(180deg);">${discount}</div>
												</li>
											</c:when>
											<c:when
												test="${empty cpnObject1.couponImagePath || cpnObject1.couponImagePath eq null}">
												<li class="imgBx"><c:out
														value="${cpnObject1.couponImagePath}"></c:out> <img
													class="default" src="../images/consumer/noImg.png"
													width="160px" height="160px" /> 
													<div class="round"
														style="opacity: 0; transform: rotate(180deg);">${discount}</div>
												</li>

											</c:when>
										</c:choose>
										<li class="relative trim2" title="${discount} off ${cpnObject1.titleText}">${discount} off ${cpnObject1.titleText}</li>
										<li>
											<div class="pcurl">
												<span class="pgcurlTxt"> <a href="#"
													onclick="getCouponDetails('Expired','${cpnObject1.couponId}', '${cpnObject1.added}', '${cpnObject1.couponImagePath}','${cpnObject1.viewableOnWeb}')" title="More">
														More </a> </span>
											</div></li>
									</ul>
								</div>
							</c:forEach>
							<div class="clear"></div>
						</div>

					</c:forEach>
				</c:if>
				<div class="clear"></div>
			</div>
			<div class="pagination brdrTop">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="noBrdr" id="perpage">
					<tr>
						<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
							nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
							pageRange="${sessionScope.pagination.pageRange}"
							url="${sessionScope.pagination.url}" enablePerPage="false" />
					</tr>
				</table>
			</div>
		</div>
		</div>
		<div class="clear"></div>
	</form:form>
	<script type="text/javascript">
	pinIt();
</script>
<body>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/consumerpagination.tld"%>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<body onresize="resizeDoc();" onload="resizeDoc();">
	<script>
	function sharePin(couponId,i){
		var base = "http://pinterest.com/pin/create/button/";
		$.ajaxSetup({cache:false});
		$.ajax({
		type : "GET",
		url : "/ScanSeeWeb/shopper/pinterestShareCouponInfo.htm",
		data : {
			'couponId'  : couponId
		},
		success : function(response) {
				var responseJSON = JSON.parse(response);
				var encodedpinurl=responseJSON.couponURL;
				encodedpinurl=encodedpinurl.replace("+", "%2B");
			    encodedpinurl=encodedpinurl.replace("/", "%2F");		
				var prodName=responseJSON.couponName;
				var encodedproductName=escape(prodName);
	            encodedproductName=encodedproductName.replace("+", "%2B");
	            encodedproductName=encodedproductName.replace("/", "%2F");
	            var encodedimagePath=responseJSON.couponImagePath;
	            encodedimagePath=encodedimagePath.replace("+", "%2B");
	            encodedimagePath=encodedimagePath.replace("/", "%2F");
	            finalUrl = base + "?url=" + encodedpinurl + "&media=" + encodedimagePath + "&description=" + encodedproductName;
	            inner = "<a href='" + finalUrl + "'class='pin-it-button' count-layout='none' target='_blank'><img id='pin" + i + "' src='/ScanSeeWeb/images/consumer/pin_down.png' alt='pinterest' width='25' height='25' title='Pinterest'/></a>";
	  			$("#pin_it_"+i).html(inner);
	  			$("#pin"+i).click();
		},
		error : function(e) {
			alert('Error Occured');
		}
		});
	}

	function twitterShareCoupon(couponName,couponURL) {
		var tweetText;
		if (null != couponURL&&couponURL!='NotApplicable' && couponURL!="") 
		{
			tweetText = 'Great find at ScanSee!!! ' + couponName +" "+ "Start Date:"
						+ '${requestScope.coupondetails.couponInfo.couponStartDate}'+" "
						+ 'Expiry Date:'
						+ '${requestScope.coupondetails.couponInfo.couponExpireDate}'
						+ "  Visit:" + couponURL
	
		} else 
		{
			tweetText = 'Great find at ScanSee!!! ' + couponName+" " + "Start Date:"
						+ '${requestScope.coupondetails.couponInfo.couponStartDate}'+" "
						+ 'Expiry Date:'
						+ '${requestScope.coupondetails.couponInfo.couponExpireDate}'
	
		}
		var tweeturl = 'http://twitter.com/share?text=' + escape(tweetText);
		window.open(tweeturl);
	}
	function deleteCoupon(){
		var checkedValue = [];
		var i = 0;
		lowerLimit = document.clrForm.lowerLimit.value;
		chkdBx = $('input[name="cart_mg"]:checkbox:checked').length;
		if(chkdBx > 0) 
		{
			$('input[name="cart_mg"]:checked').each(function(){
				checkedValue[i] = $(this).val();
				i++;
			});
		
			document.clrForm.coupId.value=checkedValue;
			$.ajaxSetup({cache:false})
			$.ajax({
				type : "GET",
				url : "consdeletecoupons.htm",
				data : {
					'couponIds' : document.clrForm.coupId.value			
				},
	
				success : function(response) {
					document.clrForm.lowerLimit.value = lowerLimit;
					document.clrForm.action = "consmygallery.htm";
					document.clrForm.method = "GET";
					document.clrForm.submit();
				},
				error : function(e) {
					alert('error')
				}
			});
		}
		else
		{
			alert('Please select coupon to delete');
		}
	}

	function redeemCoupon(){
		var checkedValue = [];
		var i = 0;
		lowerLimit = document.clrForm.lowerLimit.value;
		chkdBx = $('input[name="cart_mg"]:checkbox:checked').length;
		if(chkdBx > 0) 
		{
			$('input[name="cart_mg"]:checked').each(function(){
				checkedValue[i] = $(this).val();
				i++;
			});
			document.clrForm.coupId.value=checkedValue;
			$.ajaxSetup({cache:false})
			$.ajax({
				type : "GET",
				url : "consredeemcoupon.htm",
				data : {
					'couponIds' : document.clrForm.coupId.value			
				},

				success : function(response) {
					document.clrForm.lowerLimit.value = lowerLimit;
					document.clrForm.action = "consmygallery.htm";
					document.clrForm.method = "GET";
					document.clrForm.submit();
				},
				error : function(e) {
					alert('error')
				}
			});
		}
		else
		{
			alert('Please select coupon to redeem');
		}
		
	}

</script>
	<form:form commandName="clrForm" name="clrForm">
		<form:hidden path="recordCount" />
		<form:hidden path="couponId" />
		<form:hidden path="added" />
		<form:hidden path="clrImagePath" />
		<form:hidden path="viewableOnWeb" />
		<form:hidden path="clrType" />
		<form:hidden path="lowerLimit"
			value="${requestScope.galleryResult.lowerLimit}" />
		<form:hidden path="returnURL" value="consmygallery.htm" />
		<input type="hidden" name="emailId" id="emailId"
			value="${sessionScope.emailId}" />
		<input type="hidden" id="coupId" name="coupId" />
		<div id="contWrpr" class="">
			<ul class="secTabs" id="chngTitle">
				<li><a href="consmygallery.htm" class="active"> <img
						src="../images/consumer/clippedIcon_actv.png" alt="Clipped" title="Clipped Coupons"/>
						Clipped </a>
				</li>
				<li><a href="consgalleryused.htm"> <img
						src="../images/consumer/clippedUsedIcon.png" alt="Used" title="Used Coupons"/> Used </a>
				</li>
				<li><a href="consgalleryexp.htm"> <img
						src="../images/consumer/clippedExpiredIcon.png" alt="Expired" title="Expired Coupons"/>
						Expired </a>
				</li>
				<li><a href="consgalleryall.htm"> <img
						src="../images/consumer/clippedAllIcon.png" alt="All" title="All Coupons"/> All </a>
				</li>
				<span id="chngTitle"> Clipped </span>
			</ul>
			<div class="clear"></div>
			<div class="breadCrumb">
				<ul>
					<li class="brcIcon brcIconSharp"><img
						src="../images/consumer/mg_bcIcon.png" alt="mygallery" />
					</li>
					<li><a href="consmygallery.htm" title="My Gallery"> My Gallery </a>
					</li>
					<li class="active">Clipped</li>
				</ul>
			</div>
			<div class="contBlks cstmChkbx relative" id="detailedList">
				<c:set var="pin" value="0" />
				<c:set var="catCount" value="0" />
				<c:if test="${empty requestScope.galleryResult.loygrpbyRetlst}">
					<div class="zeroBg contBlks ">
						<img alt="close"
						src="/ScanSeeWeb/images/consumer/noneAvailCpns_poster.png" />

					</div>
				</c:if>
				<c:if test="${!empty requestScope.galleryResult.loygrpbyRetlst}">
					<c:forEach items="${requestScope.galleryResult.loygrpbyRetlst}"
						var="cpnObject">
						<c:set var="catCount" value="${catCount + 1}" />
						<c:if test="${catCount == 1}">
							<div id="stickyHdr" class="" style="display: block;">
								<h3 class="sctnHdr">
									<b>&nbsp;</b> <span> <i class="cpn"> Redeem Coupon </i>
										<i class="Trash"> Delete Coupon </i> <a href="#"
										class="icon-bg"> <img src="../images/consumer/cpnIcon.png"
											alt="cpn" width="20" height="20" onclick="redeemCoupon()" />
									</a> <a href="#" class="icon-bg"> <img
											src="../images/consumer/trash.png" alt="Trash" width="20"
											height="20" onclick="deleteCoupon();" /> </a> </span>
								</h3>
							</div>
						</c:if>
						<div id="category${cpnObject.cateId}" class="post">
							<h3>
								<b>${cpnObject.cateName}</b>
							</h3>
							<c:forEach items="${cpnObject.couponDetails}" var="cpnObject1">
								<div id="cpnTg${cpnObject1.couponId}" class="contBox">
									<ul>
										<c:set var="discount"
											value="${cpnObject1.couponDiscountAmount}" />
										<c:choose>
											<c:when
												test="${cpnObject1.couponImagePath ne null && cpnObject1.couponImagePath ne 'NotApplicable' && !empty cpnObject1.couponImagePath}">
												<li class="imgBx"><img class="default"
													src="${cpnObject1.couponImagePath}"
													onerror="this.src = '/ScanSeeWeb/images/consumer/noImg.png';"
													width="160px" height="160px" />
													<input type="checkbox"
													value="${cpnObject1.couponId}" name="cart_mg"
													style="visibility: hidden;">
													 <!--<img style="position:absolute;" src="../images/consumer/chkbxUnchkd.png" name="cstmChkbx">-->
													<div class="round"
														style="opacity: 0; transform: rotate(180deg);">${discount}</div>
												</li>
											</c:when>
											<c:when
												test="${empty cpnObject1.couponImagePath || cpnObject1.couponImagePath eq null}">
												<li class="imgBx"><c:out
														value="${cpnObject1.couponImagePath}"></c:out> <img
													src="../images/consumer/noImg.png" width="160px"
													height="160px" /> 
													<input type="checkbox"
													value="${cpnObject1.couponId}" name="cart_mg"
													style="visibility: hidden;">  <!--<img style="position:absolute;" src="../images/consumer/chkbxUnchkd.png" name="cstmChkbx">-->
													<div class="round"
														style="opacity: 0; transform: rotate(180deg);">${discount}</div>
												</li>

											</c:when>
										</c:choose>
										<li class="relative trim2" title="${discount} off ${cpnObject1.titleText}">${discount} off ${cpnObject1.titleText}</li>
										<li>	<div class="pcurl">
												<span class="pgcurlTxt"><a href="#"
													onclick="getCouponDetails('Clipped','${cpnObject1.couponId}', '${cpnObject1.added}', '${cpnObject1.couponImagePath}','${cpnObject1.viewableOnWeb}')" title="More">More</a>
												</span>
											</div>
										</li>
									</ul>
									<div align="center" class="contrlStrip">
										<span> <img src="../images/consumer/fb_down.png"
											alt="facebook"
											onclick="fbShareCLRDetails('C',${cpnObject1.couponId})" width="25px" height="25px" title="Facebook"/> <img
											src="../images/consumer/twitter_down.png" alt="twitter"
											onclick="twitterShareCoupon('${cpnObject1.couponName}','${cpnObject1.couponURL}')" width="25px" height="25px" title="Twitter"/>
											<c:if test="${cpnObject1.couponImagePath ne null && !empty cpnObject1.couponImagePath}">
											<span id="pin_it_${pin}">											
												<img src='/ScanSeeWeb/images/consumer/pin_down.png' onclick="sharePin(${cpnObject1.couponId},${pin})" alt='pinterest' width="25" height="25" title="Pinterest"/>
												<c:set var="pin" value="${pin + 1}" />
											</span>
										</c:if> 
										 <img class="couponEmailActn"
											name="${cpnObject1.couponId}" alt="email"
											src="../images/consumer/email_down.png" width="25px" height="25px" title="Email"/> </span>
									</div>
								</div>
							</c:forEach>
							<div class="clear"></div>
						</div>

					</c:forEach>
				</c:if>
				<div class="clear"></div>
			</div>
			<div class="pagination brdrTop">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="noBrdr" id="perpage">
					<tr>
						<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
							nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
							pageRange="${sessionScope.pagination.pageRange}"
							url="${sessionScope.pagination.url}" enablePerPage="false" />
					</tr>
				</table>
			</div>
		</div>
		</div>
		<div class="clear"></div>
	</form:form>
<body>
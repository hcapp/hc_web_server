<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script type="text/javascript">

$(document).ready(function(){	
$('#cpnRedeemprod input:checkbox').click(function() {												  	
	totalChk = $('input[name="chkbxProd"]:checkbox').length;
	chkdBx = $('input[name="chkbxProd"]:checkbox:checked').length;
	var checkedValue = [];
	
	if(chkdBx > 0) {
		for(var i=0;i<chkdBx;i++){
		
		checkedValue[i] = $("input[type='checkbox']").val();
		
		}
		document.clrForm.hdnProdId.value=checkedValue;
		$('#sLTab li:eq(0) a img').attr('src','../images/tab_btn_up_orange.png').attr('onclick',"addWishListWithProduct();");
		$('#sLTab li:eq(1) a img').attr('src','../images/tab_btn_up_green.png').attr('onclick',"addShoppingListWithProduct();");
	}
	else { 
		$('#sLTab li:eq(0) a img').attr('src','../images/tab_btn_up_wishlist.png');
		$('#sLTab li:eq(1) a img').attr('src','../images/tab_btn_up_spngLst.png');
	}					  
	});
});
function back(){	

   document.clrForm.action = "fetchclrdetails.htm";
   document.clrForm.method = "POST";
   document.clrForm.submit();
   
}

</script>
<div class="clear"></div>
<div id="content" class="topMrgn">
	<form:form commandName="clrForm" name="clrForm">
		<form:hidden path="couponId"
			value="${requestScope.coupondetails.couponInfo.couponId}" />
		<form:hidden path="rebateId" />
		<form:hidden path="loyaltyDealID" />
		<form:hidden path="requestType" />
		<form:hidden path="lowerLimit" />
		<form:hidden path="pageFlowType" />
		<form:hidden path="clrType" />
		<form:hidden path="added" />
		<form:hidden path="viewableOnWeb"/>
		<input type="hidden" id="hdnProdId" name="hdnProdId" />
		<div class="navBar">
			<ul>
				<li><a href="#"><img src="../images/backBtn.png" onclick="back();"/></a></li>
				<li class="titletxt">Select Product To Add</li>
				<!--<li class="floatR mrgnRt"><a href="index.html"><img src="images/logoutBtn.png" alt="logout" /></a></li>-->
			</ul>
		</div>
		<div class="clear"></div>
		<div class="mobCont noBg">
			<div class="mobDsply floatR">
				<div class="fluidViewHD">
					<!--<h3>List of Retailers Based on Search Criteria</h3>-->
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="mobGrd detView zeroBtmMrgn" id="cpnRedeemprod">

						<c:forEach items="${requestScope.productdetails}"
							var="products">
							<tr>
								<td width="7%" align="center" valign="middle">
								<input type="checkbox" name="chkbxProd" value="${products.productId}"/>
								</td>
								<td width="68%"><span>${products.productName}</span>${products.productShortDescription}</td>
								<td></td>
								<td></td>
								<td width="20%"><img src="${products.imagePath} "
									width="32" height="32" align="ipod" />
								</td>
								<td width="5%"><img src="../images/rightArrow.png"
									alt="Arrow" width="11" height="15" />
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
			<jsp:include page="../leftmenu.jsp"></jsp:include>
		</div>
		<div class="clear"></div>
		<div class="tabBarNoHvr">
			<ul id="sLTab">
				<li><a href="#"><img
						src="../images/tab_btn_up_wishlist.png" alt="Wish List" width="80"
						height="50">
				</a>
				</li>
				<li><a href="#"><img width="80" height="50"
						alt="addShoppingList" src="../images/tab_btn_up_spngLst.png">
				</a>
				</li>
			</ul>
		</div>
	</form:form>
</div>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<script type="text/javascript">	
		window.onload = function()
		{
			favLoad('${fn:escapeXml(UpdatePreferedCategories)}');
			displayCategory(); 
		}
		function displayCategory()
		{
			 $('#preferedCatgry_view').html("${sessionScope.preferenceCategories}");
			 var cat = ("${sessionScope.expandCategory}");
			 var mainCat = cat.split(",");
			if($('.preferencesLst tbody tr').hasClass('mainCtgry')) {
				$('.preferencesLst tbody tr:not(.mainCtgry)').hide();	 
			}
			for(var i = 0;i<mainCat.length;i++)
			{
				$('.preferencesLst tr[name="'+mainCat[i]+'"]').show();
				$('.preferencesLst tr[name="'+mainCat[i]+',"]').show();
			}
			 
		}
	</script>
	<div id="wrapper">
		<form:form name="preferencesusrcategories" commandName="preferencesusrcategories">
		<form:hidden path="email" value="${sessionScope.categoryDetailresponse.email}"/>
		<form:hidden path="cellPhone" value="${sessionScope.categoryDetailresponse.cellPhone}"/>		
			<div id="contWrpr">
				<div class="breadCrumb">
					<ul>
						<li class="brcIcon">
							<img src="../images/consumer/find_prefIcon.png" alt="find" />
						</li>
						<li class="leftPdng">
							Preferred Category
						</li>
					</ul>
					<span class="rtCrnr">	
						&nbsp;
					</span> 
				</div>
				<div class="contBlks">
					<div class="btnCntrl">
						<ul>
							<li>
								<a href="consfetchuserinfo.htm">
									<img src="../images/consumer/user.png" alt="user" />
									User Settings
								</a>
							</li>
							<li>
								<a href="#">
									<img src="../images/consumer/ctgry.png" alt="category" />
									Preferred Category
								</a>
							</li>
							<li>
								<a href="consfetchusersettings.htm">
									<img src="../images/consumer/radius.png" alt="radius" />
									Select Radius
								</a>
							</li>
						</ul>
					</div>
					<div class="clear"></div>
					<div class="sctmScroll">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd preferencesLst listCntrl" id="preferedCatgry_view">
							<col width="4%">
							<col width="4%">
							<col width="92%">
						</table>
					</div>
					<div class="btnStrip">
						<button class="btn btn-success" onclick="saveUserPreferences()">Save</button>   
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</form:form>
	</div>
	<div class="clear"></div>
</body>
 
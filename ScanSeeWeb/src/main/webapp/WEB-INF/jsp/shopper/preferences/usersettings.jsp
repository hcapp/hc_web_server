
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form name="usersettings" commandName="usersettings">
	<div id="content" class="topMrgn">
		<div class="navBar">
			<ul>
				<li><a href="#"><img src="images/backBtn.png"
						onclick="javascript:back();" alt="back" />
				</a>
				</li>
				<li class="titletxt">Settings</li>
				
			</ul>
		</div>
		<div class="clear"></div>
		<div class="mobCont noBg">
			<div class="splitDsply floatR">
				
				<div class="clear"></div>
				<div class="fluidView">
					<div class="stretch ">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="brdrLsTbl zeroBtmMrgn">
							<tr>
								<td colspan="2" class="Label"><strong>Global Application Settings</strong>
								</td>
							</tr>
							<tr>
								<td width="50%">Local Rads</td>
								<td width="50%"><input type="text" name="firstNm"
									class="textboxSmall" />
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</form:form>
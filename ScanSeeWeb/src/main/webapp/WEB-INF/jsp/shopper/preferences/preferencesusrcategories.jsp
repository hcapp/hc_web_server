<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<script type="text/javascript">
	window.onload = function() {
	
		favLoad('${fn:escapeXml(UpdatePreferedCategories)}');
		
		}
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">

	<form:form name="preferencesusrcategories" commandName="preferencesusrcategories"
		acceptCharset="ISO-8859-1">
		<div id="content" class="topMrgn">
			<div class="navBar">
				<ul>
			<li>	<a href="preferencesmain.htm">
<img  src="../images/backBtn.png"> </a>
				</li>
					<li class="titletxt">Categories</li>

				</ul>
			</div>

			<div class="mobCont noBg">
				<div class="splitDsply floatR">

					<div class="fluidViewHD">
						<div class="stretch relatvDiv cmnPnl">

							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="gnrlGrd preferencesLst shprView">
								<thead>
             
            </thead>
								<c:forEach
									items="${sessionScope.categoryDetailresponse.mainCategoryDetaillst}"
									var="item">
									
									<tr name="${item.mainCategoryName}" class="mainCtgry" onclick="toggleConfirm();">
										<td width="4%" align="center" class="wrpWord"><input
											type="checkbox" /></td>
										<td colspan="2" class="wrpWord"><c:out
												value="${item.mainCategoryName}" /></td>
									</tr>

									<c:forEach items="${item.subCategoryDetailLst}" var="item1">
										<tr name="${item.mainCategoryName}" onclick="toggleConfirm();">
											<td align="center" class="wrpWord">&nbsp;</td>
											<td width="4%" class="wrpWord"><c:choose>

													<c:when test="${item1.displayed==0}">
														<form:checkbox path="favCatId" value="${item1.categoryId}" />
													</c:when>
													<c:otherwise>
														<form:checkbox path="favCatId" checked="checked"
															value="${item1.categoryId}" />
													</c:otherwise>
												</c:choose></td>
											<td width="92%" class="wrpWord"><c:out
													value="${item1.subCategoryName}" /></td>
										</tr>
									</c:forEach>

								</c:forEach>





							</table>
						</div>

					</div>

				</div>
				<%@include file="../leftmenu.jsp"%>
			</div>
			
			
			
			
			<div class="tabBar" align="right">
				<ul id="btmBtnBar">
					<li>
					<a href="#"><img width="53" height="30"
							onclick="saveUserPreferences()" src="../images/saveBtn.png">
					</a>
					</li>
				</ul>
			</div>


		</div>
	</form:form>

</body>
</html>


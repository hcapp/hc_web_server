<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>

<link
	href="/ScanSeeWeb/styles/jquery-ui.css"
	rel="stylesheet" type="text/css">
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script type="text/javascript">
	window.onload = function() {
	
		favLoad('${fn:escapeXml(SavedUserInfo)}');
		loadCity();
		}
</script>
<script>
	$(document).ready(function() {
		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
</script>
<script>
	function loadCity() {
		// get the form values

		var stateName = $('#state').val();
	
		$.ajaxSetup({
			cache : false
		})
		$.ajax({
			type : "GET",
			url : "usrGetCity.htm",
			data : {
				'stateName' : stateName,
				'city' : null
			},

			success : function(response) {

				$('#myAjax3').html(response);
			},
			error : function(e) {

			}
		});
	}
</script>
<script type="text/javascript">
	loadCity();
</script>


</head>
<form:form name="preferencesuserinfo" commandName="preferencesuserinfo"
	acceptCharset="ISO-8859-1">
	<div id="content" class="topMrgn">
		<div class="navBar">
			<ul>
				<li><a href="preferencesmain.htm"><img src="../images/backBtn.png"
						 /> </a>
				</li>
				<li class="titletxt">User Information</li>
			
			</ul>
		</div>

		<div class="mobCont noBg">
			<div class="splitDsply floatR">

				<div class="fluidViewHD">
					<div class="stretch relatvDiv cmnPnl">

						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="grdTbl zeroBtmMrgn">
							<tr>
								<td class="Label"><label for="lFName" >First
										Name</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="firstName"></form:errors> <form:input path="firstName"
										id="lFName" maxlength="20" tabindex="1"  onclick="toggleConfirm();"/>
								</td>
								<td class="Label" align="left"><label for="llName"
									>Last Name</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="lastName"></form:errors> <form:input path="lastName"
										id="llName" value="" maxlength="30" tabindex="2"  onclick="toggleConfirm();"/></td>
							</tr>

							<tr>
								<td class="Label">Email</td>
								<td colspan="3"><form:errors cssStyle="color:red"
										path="email"></form:errors> <form:input path="email"
										id="contPhNm" maxlength="100" tabindex="3" onclick="toggleConfirm();"/>
								</td>
							</tr>
							<tr>
								<td class="Label">&nbsp;</td>
								<td colspan="3"><a href="#" onclick="editPswd(${requestScope.userId});"  tabindex="4" >Change Password</a>
								</td>
							</tr>

							<tr>
								<td class="Label">&nbsp;</td>
								<td colspan="3">We use an address to search for offers(typically home address)
									</td>
							</tr>
							<tr>
								<td class="Label">Address1</td>
								<td><form:errors cssStyle="color:red"
										path="address1">
									</form:errors> <form:textarea rows="4" cols="45" path="address1" id="addrs1"
										onkeyup="checkMaxLength(this,'50');" tabindex="5" onclick="toggleConfirm();"
										cssStyle="height:60px;" />
								</td>
								<td class="Label">Address2</td>
								<td><form:errors cssStyle="color:red"
										path="address2">
									</form:errors> <form:textarea rows="4" cols="45" path="address2" id="address2"
										onkeyup="checkMaxLength(this,'50');" tabindex="6" onclick="toggleConfirm();"
										cssStyle="height:60px;" />
								</td>
							</tr>

							<tr>
								<td class="Label">Country</label></td>
								<td><form:errors cssStyle="color:red" path="countryID"></form:errors>
									<form:select path="countryID" class="selecBx" id="eduid"
										tabindex="7" onclick="toggleConfirm();">
										<form:option value="0" label="--Select--">--Select--</form:option>
										<c:forEach items="${sessionScope.countrylst.countryCode}"
											var="cntry">
											<form:option value="${cntry.countryID}"
												label="${cntry.countryName}" />
										</c:forEach>
									</form:select>
								</td>


								<td class="Label">State</label>
								</td>

								<td><form:errors cssStyle="color:red" path="state"></form:errors>
									<form:select path="state" class="selecBx" id="state"
										onchange="loadCity()" tabindex="8" onclick="toggleConfirm();">

										<form:option value="0" label="--Select--">--Select--</form:option>
										<c:forEach items="${sessionScope.statelst.stateInfo}"
											var="states">
											<form:option value="${states.stateName}"
												label="${states.stateName}" />


										</c:forEach>
									</form:select></td>
							</tr>

							<tr>
								<td class="Label">City</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="city"></form:errors>
									<p id="myAjax3">
										<form:select path="city" id="City" tabindex="9" onclick="toggleConfirm();">
											<form:option value="0">--Select--</form:option>
										</form:select>
									</p></td>

								<td class="Label"><label for="pCode" >Postal
										Code</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="postalCode"></form:errors> <form:input id="pCode"
										path="postalCode" maxlength="10"
										onkeypress="return isNumberKey(event);toggleConfirm();" tabindex="10" />
								</td>

							</tr>
							<tr>
								<td class="Label" align="left"><label for="phnNum"
									>Mobile #</label>
								</td>
								<td colspan="3" align="left"><form:errors cssStyle="color:red"
										path="mobileNumber"></form:errors> <form:input id="phnNum"
										path="mobileNumber" tabindex="11"
										onclick="toggleConfirm();"
										onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);" /><label
									class="label">(xxx)xxx-xxxx</label>
								</td>
							</tr>
							<tr>
								<td class="Label">Gender</td>
								<form:errors cssStyle="color:red" path="gender">
								</form:errors>

								<td><form:radiobutton path="gender" value="0" label="Male"  tabindex="12" onclick="toggleConfirm();" />
									<form:radiobutton path="gender" value="1" label="Female"   tabindex="12" onclick="toggleConfirm();"/></td>

								<td class="Label">Date of Birth</td>

								<td align="left"><form:errors cssStyle="color:red"
										path="dob"></form:errors> <form:input path="dob" name="csd"
										type="text" class="textboxDate" id="datepicker2"  tabindex="13" onclick="toggleConfirm();"/>(mm/dd/yyyy)</td>

							</tr>

							<tr>
								<td class="Label">Educational level</td>
								<td colspan="3"><form:errors cssStyle="color:red"
										path="educationLevelId"></form:errors> <form:select
										path="educationLevelId" class="selecBx" id="eduid"
										tabindex="14" onclick="toggleConfirm();">
										<form:option value="0" label="--Select--">--Select--</form:option>
										<c:forEach
											items="${sessionScope.educationlst.educationalInfo}"
											var="edu">
											<form:option value="${edu.educationalLevelId}"
												label="${edu.educationalLevelDesc}" />
										</c:forEach>
									</form:select></td>





							</tr>

							<tr>
								<td class="Label">Home Owner</td>
								<td colspan="3"><form:radiobutton path="homeOwner"
										value="0" label="Yes" /> <form:radiobutton path="homeOwner"
										value="1" label="No"  tabindex="15" onclick="toggleConfirm();"  /></td>


							</tr>

							<tr>
								<td class="Label">Marital Status</td>



								<td align="left"><form:errors cssStyle="color:red"
										path="maritalStatus"></form:errors> <form:select
										path="maritalStatus" class="selecBx" id="Country1"
										tabindex="16" onclick="toggleConfirm();">
										<form:option value="0" label="--Select--">--Select--</form:option>
										<c:forEach
											items="${sessionScope.matitalstatuslst.maritalInfo}"
											var="mari">
											<form:option value="${mari.maritalStatusId}"
												label="${mari.maritalStatusName}" />
										</c:forEach>
									</form:select></td>



								<td class="Label"># Of Children</td>
								<td><form:input path="children" id="noOfchi"
										onkeypress="return isNumberKey(event)" maxlength="2" onclick="toggleConfirm();"
										cssClass="textboxDate" tabindex="17" /></td>
							</tr>


							<tr>
								<td class="Label">Income Range</td>
								<td colspan="3"><form:errors cssStyle="color:red"
										path="incomeRangeID"></form:errors> <form:select
										path="incomeRangeID" class="selecBx" id="icomeid" tabindex="18" onclick="toggleConfirm();">
										<form:option value="0" label="--Select--">--Select--</form:option>
										<c:forEach items="${sessionScope.incomerangelst.incomeRange}"
											var="income">
											<form:option value="${income.incomeRangeId}"
												label="${income.incomeRange}" />
										</c:forEach>
									</form:select></td>







							</tr>
							
							
							
							
						</table>
<p class="userInformtn">Your privacy is important to us. ScanSee will never sell 
	or give your information away. 100% Guaranteed.</p>
					</div>
					
				</div>
				
			</div>
			<%@include file="../leftmenu.jsp"%>
		</div>
<div class="tabBar">
    <ul id="btmBtnBar">
        	<li><a href="http://208.39.97.1:8080/html/Privacy_Policy.html" target="_blank"><img width="68" height="30" src="../images/privacyPolcy.png"> </a>
			<a href="#"><img width="53" height="30" onclick="saveUserInfo()" src="../images/saveBtn.png"> </a></li>
      </ul>
    </div>


	</div>
</form:form>

</html>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	<form:form name="preferencesmain" commandName="preferencesmain">
	
	<div id="content" class="topMrgn">
    <div class="navBar">
    	<ul>
        	<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:history.go(-1);" /> </a></li>
            <li class="titletxt">User Settings</li>
            <!--<li class="floatR mrgnRt"><a href="index.html"><img src="images/logoutBtn.png" alt="logout" /></a></li>-->
      </ul>
    </div>
    <div class="clear"></div>
    <div class="mobCont noBg">
   
        <div class="splitDsply floatR">
                <!--<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
          <tr>
            <td width="30%"><select class="textboxBig" >
              <option>Select Category</option>
            </select></td>
            <td width="30%"><input type="text" class="textboxBig" /></td>
            <td width="40%"><input type="button" class="mobBtn" value="Search" /></td>
          </tr>
        </table>-->
      	<div class="fluidView lftBrdr"><!--<h3>List of Retailers Based on Search Criteria</h3>-->
      	  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd htlt ">
            <tr>
              <td width="7%" align="center"><img src="../images/UserInfoIcon.png" alt="userInfo" width="29" height="30" /></td>
              <td class="wrpWord"><a href="fetchuserinfo.htm">User Information</a></td>
              <td width="5%"><img src="../images/rightArrow.png" alt="Right" width="11" height="15" /></td>
            </tr>
            <tr>
              <td align="center"><img src="../images/SettingsIcon.png" alt="settings" width="29" height="30" /></td>
              <td class="wrpWord"><a href="fetchusersettings.htm">Settings</a></td>
              <td><img src="../images/rightArrow.png" alt="Right" width="11" height="15" /></td>
            </tr>
            <!--        <tr>
          <td align="center">&nbsp;</td>
          <td class="wrpWord">Favorites</td>
          <td><img src="images/rightArrow.png" alt="Right" width="11" height="15" /></td>
        </tr>-->
            <tr>
              <td align="center"><span class="wrpWord"><img src="../images/preferencesIcon.png" alt="preferences" width="29" height="30" /></span></td>
              <td width="88%" class="wrpWord"><a href="conspreferences.htm">Preferences</a></td>
              <td><img src="../images/rightArrow.png" alt="Right" width="11" height="15" /></td>
            </tr>
              <tr>
              <td align="center"><img src="../images/prftShrngIcon.png" alt="AboutProfitSharing" width="29" height="30" /></td>
              <td class="wrpWord"><a target="_blank" href="http://208.39.97.1:8080/html/Profitsharing.html">About profit sharing Programs with universities and colleges..</a></td>
              <td><img src="../images/rightArrow.png" alt="Right" width="11" height="15" /></td>
            </tr>
          </table>
	
	
	</div>
	</div>
	<%@include file="../leftmenu.jsp"%>
	</div>
	
	</div>
	
	
	
</form:form>

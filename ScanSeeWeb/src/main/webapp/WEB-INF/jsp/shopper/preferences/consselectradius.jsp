<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<script type="text/javascript">	
		window.onload = function(){
			favLoad('${fn:escapeXml(RadiusSaved)}');	
		}
	</script>
	<div id="wrapper">
		<form:form name="preferencesusersettings" commandName="preferencesusersettings">
			<div id="contWrpr" style="min-height: 221px;">
				<div class="breadCrumb">
					<ul>
						<li class="brcIcon">
							<img src="../images/consumer/find_prefIcon.png" alt="find" />
						</li>
						<li class="leftPdng">
							Select Radius
						</li>
					</ul>
					<span class="rtCrnr">
						&nbsp;
					</span> 
				</div>
				<div class="contBlks" >
					<div class="btnCntrl">
						<ul>
							<li>
								<a href="consfetchuserinfo.htm">
									<img src="../images/consumer/user.png" alt="user" />
									User Settings
								</a>
							</li>
							<li>
								<a href="conspreferences.htm">
									<img src="../images/consumer/ctgry.png" alt="category" />
									Preferred Category
								</a>
							</li>
							<li>
								<a href="#">
									<img src="../images/consumer/radius.png" alt="radius" />
									Select Radius
								</a>
							</li>
						</ul>
					</div>
					<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn">
						<tr>
							<td width="20%" class="Label">
								<strong>Global Application Settings</strong>
							</td>
							<td>
								<form:errors cssStyle="color:red" path="localeRadius"></form:errors>
								<form:input onkeypress="return isNumberKey(event)"  maxlength="2" 					cssClass="textboxDate" path="localeRadius"/>
							</td>
						</tr>
						<tr>
							<td class="Label">
								&nbsp;
							</td>
							<td class="Label">
								<input type="button" class="btn btn-success " value="Save" onclick="saveUserSettings('');"  title="Save"/> 
							</td>
						</tr>
					</table>
					<div class="clear"></div>
				</div>
			</div>
		</form:form>
	</div>
	<div class="clear"></div>
</body>
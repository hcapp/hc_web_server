<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;">
	<!--<h3>List of Retailers Based on Search Criteria</h3>-->

	 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd" id="findPnl">
			<c:forEach items="${requestScope.seacrhList.productList}"
				var="item">
				<tr>
					<td width="11%">
					<c:if test="${item.productImagePath == 'NotApplicable'}" >
						<img src="../images/imgIcon.png" alt="Img" width="38" height="38" />
						</c:if>
						<c:if test="${item.productImagePath != 'NotApplicable'}" >
						<img width="80" height="50" src="${item.productImagePath}">
						</c:if>
					</td>
					<td width="83%" onclick="callprodSummary(${item.productID},${requestScope.retailerId},'${item.price}',0)"><b>${item.productName}</b>
					</td>
					<td width="6%"><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" /></td>
				</tr>
			</c:forEach>
	 </table>
</div>
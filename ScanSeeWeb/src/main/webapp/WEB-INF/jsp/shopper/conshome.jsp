<!--This page used to display the consumer home page information and daily featured deals  -->


<div id="contWrpr">
	<div class="block mrgnBtm">
		<h3>Welcome to <label style="color:#2892C9">ScanSee</label></h3>
		<p>At ScanSee we are more than a Coupon website. ScanSee aggregates the best deals, coupons, loyalty, products and locations for display on the ScanSee Website and the ScanSee Mobile App. At ScanSee we believe in saving you, the consumer, time and money by doing the work for you. Search for your deals, products or locations or download the Mobile App.  Let us do the work for you by signing up and setting your preferences.  We will show you only the deals and coupons you want to see when you want to see it.</p>
	</div>
	<div class="breadCrumb">
		<ul>
			<li class="brcIcon"><img
				src="/ScanSeeWeb/images/consumer/deals_bcIcon.png" alt="find" /></li>
			<!--<li class="leftPdng"><strong>Todays Featured Deals</strong></li>
		--></ul>
		<span class="rtCrnr">&nbsp;</span>
	</div>
	<div class="block relati" id="loadExtrnPg">

		<iframe id="iframe"></iframe>

	</div>
</div>
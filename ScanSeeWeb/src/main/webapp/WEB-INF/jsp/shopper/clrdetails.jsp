<!-- This page is used for display of C,L and R details from Product Summary page -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>




      
      	<div class="dataView"><!--<h3>List of Retailers Based on Search Criteria</h3>-->
      	<c:if test="${requestScope.coupondetails ne null}">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
              <tr class="noBg">
                <td colspan="3" align="center"><img src="${requestScope.coupondetails.couponInfo.couponImagePath}"  width="105" height="73" /></td>
              </tr>
              <tr>
                	<td>&nbsp;</td>
                	<td width="20%">Start Date:</td>
                	<td width="60%" class="wrpWord">${requestScope.coupondetails.couponInfo.couponStartDate}</td>
              	</tr>
              	<tr>
                	<td>&nbsp;</td>
                	<td width="20%">Expiry Date:</td>
                	<td width="60%" class="wrpWord">${requestScope.coupondetails.couponInfo.couponExpireDate}</td>
              	</tr>
              
              <tr>
                <td colspan="3" align="left" class="iphoneTitle">PRODUCTS:</td>
              </tr>
              <tr>
                <td width="4%" align="center">&nbsp;</td>
                <td colspan="2" class="wrpWord"><span><c:forEach items="${requestScope.coupondetails.productLst}" var="products">
                   						${products.productName} 
                   	    </c:forEach> </span></td>
              </tr>
              <tr>
                <td colspan="3" align="left" class="iphoneTitle">DETAILS:</td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
                <td colspan="2" class="wrpWord"><p class="description">${requestScope.coupondetails.couponInfo.couponLongDescription} </p></td>
              </tr>
              <tr>
                <!--<td colspan="3" align="left">
                <c:out value="${requestScope.addedFlag}"/>
                    <c:if test="${requestScope.addedFlag eq '0' || requestScope.addedFlag eq ''}">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map"  onclick="discountUserRedeemCLR('coupon','${requestScope.addedFlag}')"/>
                	</c:if>
                	<c:if test="${requestScope.addedFlag eq '1'}">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map" />
                	</c:if>
                
                
                </td>
              --></tr>
          </table>
            
          <div id="actionSheet">
            <div class="actnCncl" align="center"><a href="#"><img src="../images/actionSheetCancel.png" alt="cancel" title="cancel" name="actnCncl"/></a></div>
             <div class="clear"></div>
            <ul class="actnShtLst">
            <li><a href="#">facebook</a></li>
            <li><a href="#">twitter</a></li>
           <li><a href="#" onclick="openEMailSharePopUp(${requestScope.coupondetails.couponInfo.couponId},'coupon')">send mail</a></li>
            </ul>
            </div>  
          </c:if>
        
          <c:if test="${requestScope.loyaltydetails ne null}">
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
              <tr class="noBg">
                <td colspan="3" align="center"><img src="${requestScope.loyaltydetails.loyaltyInfo.imagePath}"  width="105" height="73" /></td>
              </tr>
			<tr>
				<td>&nbsp;</td>
				<td width="20%">Start Date:</td>
				<td width="60%" class="wrpWord">${requestScope.loyaltydetails.loyaltyInfo.loyaltyDealStartDate}</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td width="20%">Expiry Date:</td>
				<td width="60%">${requestScope.loyaltydetails.loyaltyInfo.loyaltyDealExpireDate}</td>
			</tr>

			<tr>
                <td colspan="3" align="left" class="iphoneTitle">PRODUCTS:</td>
              </tr>
              <tr>
                <td width="4%" align="center">&nbsp;</td>
                <td colspan="2" class="wrpWord">
                <span>
                	<c:forEach items="${requestScope.loyaltydetails.productLst}" var="products">
                   						${products.productName} 
                   	 </c:forEach> 
                </span>
                </td>
              </tr>
              <tr>
                <td colspan="3" align="left" class="iphoneTitle">DETAILS:</td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
                <td colspan="2" class="wrpWord"><p class="description">${requestScope.loyaltydetails.loyaltyInfo.loyaltyDealDescription}  </p></td>
              </tr>
              <tr>
               <!-- <td colspan="3" align="left">
                
                <c:if test="${requestScope.addedFlag eq '0' || requestScope.addedFlag eq ''}">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map"  onclick="discountUserRedeemCLR('loyality','${requestScope.addedFlag}')"/>
                	</c:if>
                	<c:if test="${requestScope.addedFlag == '1'}">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map" />
                	</c:if>
                    
                
                
                </td>
              --></tr>
          </table>
         
          <div id="actionSheet">
            <div class="actnCncl" align="center"><a href="#"><img src="../images/actionSheetCancel.png" alt="cancel" title="cancel" name="actnCncl"/></a></div>
             <div class="clear"></div>
            <ul class="actnShtLst">
            <li><a href="#">facebook</a></li>
            <li><a href="#">twitter</a></li>
           <li><a href="#" onclick="openEMailSharePopUp(${requestScope.loyaltydetails.loyaltyInfo.loyaltyDealId},'loyality')">send mail</a></li>
            </ul>
            </div>
          </c:if>
          
          <c:if test="${requestScope.rebatedetails ne null}">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
              <tr class="noBg">
                <td colspan="3" align="center"><img src="${requestScope.rebatedetails.rebateInfo.imagePath}"  width="105" height="73" /></td>
              </tr>
              <tr>
                	<td>&nbsp;</td>
                	<td width="20%">Start Date:</td>
                	<td width="60%" class="wrpWord">${requestScope.rebatedetails.rebateInfo.rebateStartDate}</td>
              	</tr>
              	<tr>
                	<td>&nbsp;</td>
                	<td width="20%">Expiry Date:</td>
                	<td width="60%">${requestScope.rebatedetails.rebateInfo.rebateEndDate}</td>
              	</tr>
              
              <tr>
                <td colspan="3" align="left" class="iphoneTitle">PRODUCTS:</td>
              </tr>
              <tr>
                <td width="4%" align="center">&nbsp;</td>
                <td colspan="2" class="wrpWord">
                <span>
                	<c:forEach items="${requestScope.rebatedetails.productLst}" var="products">
                   						${products.productName} 
                   	</c:forEach> 
                </span>
                </td>
              </tr>
              <tr>
                <td colspan="3" align="left" class="iphoneTitle">DETAILS:</td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
                <td colspan="2" class="wrpWord"><p class="description">${requestScope.rebatedetails.rebateInfo.rebateShortDescription}</p></td>
              </tr>
              <tr><!--
                <td colspan="3" align="left">
                
                <c:if test="${requestScope.addedFlag eq '0' || requestScope.addedFlag eq ''}">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map"  onclick="discountUserRedeemCLR('rebate','${requestScope.addedFlag}')"/>
                	</c:if>
                	<c:if test="${requestScope.addedFlag eq '1'}">
                	<img src="../images/CPN_galleryBg.png" alt="redeem" width="303" height="144" border="0" usemap="#Map" />
                	</c:if>
                
                </td>
              --></tr>
          </table>
           <div id="actionSheet">
            <div class="actnCncl" align="center"><a href="#"><img src="../images/actionSheetCancel.png" alt="cancel" title="cancel" name="actnCncl"/></a></div>
             <div class="clear"></div>
            <ul class="actnShtLst">
            <li><a href="#">facebook</a></li>
            <li><a href="#">twitter</a></li>
           <li><a href="#" onclick="openEMailSharePopUp(${requestScope.rebatedetails.rebateInfo.rebateId},'rebate')">send mail</a></li>
            </ul>
            </div>  
           </c:if>
         
        </div>
        
   
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript" src=" http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.17/jquery-ui.min.js"></script>


<form:form name="scannowform" commandName="scannowform">
	<div id="content" class="topMrgn">
		<div class="navBar">
			<ul>

				<li class="titletxt">Scan Now</li>

			</ul>
		</div>
		<div class="clear"></div>
		<div class="mobCont noBg">
			<div class="splitDsply floatR">
			
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="brdrLsTbl topSubBar">
					<tr>
						<td width="7%" align="center">Search:</td>
					<td width="20%"><input name="searchKey" class="textboxSmall"  id="searchProd" />
						<td width="73%"><input type="button" class="mobBtn"
							value="Search" onclick="scanNowProductSearch();" title="Search" />
						</td>
					</tr>

			
				</table>
				
				<div class="clear"></div>
				<div class="fluidView">
				<div class="stretch">
						<c:choose>
						<c:when
							test="${requestScope.searchresults ne null && !empty requestScope.searchresults}">
							<%@include file="scannowprodsearch.jsp"%>
						</c:when>



						<c:otherwise>

						</c:otherwise>
					</c:choose>
					</div>
					</div>
			</div>



			<jsp:include page="../leftmenu.jsp" />
			<div class="clear"></div>

				<div class="clear"></div>
				<div class="pagination" style="padding-left: 210px;">
					<p>
						<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
							nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
							pageRange="${sessionScope.pagination.pageRange}"
							url="${sessionScope.pagination.url}" enablePerPage="false" />
					</p>
				</div>
		</div>
		<div class="clear"></div>
	</div>


<input type="hidden" name="productId" id="productId" value="" />
<input type="hidden" name="fromScannow" id="fromScannow" />

</form:form>

<!-- This page used to display the product reviews in product summary  screen.-->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<body class="whiteBG">
	<c:if test="${sessionScope.prodreviews ne null}">
		<div class="contBlock">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="cstmTbl title-desc">

				<c:choose>
					<c:when test="${sessionScope.prodreviews ne null}">


						<!--This for loop used to  display the product multiple images. -->
						<c:forEach items='${sessionScope.prodreviews}' var='prodReview'
							varStatus="indexnum">

							<c:if test="${prodReview.reviewComments ne null}">


								<tr>
									<td align="center"><img
										src="${prodReview.productImagePath}" alt="Img" width="40"
										onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';"
										height="40" />
									</td>
									<td><a href="${prodReview.reviewURL}" target='_blank'><strong>${prodReview.reviewComments}</strong>
									</a></td>
								</tr>



							</c:if>
						</c:forEach>

					</c:when>
					<c:otherwise>


					</c:otherwise>
				</c:choose>


			</table>

		</div>
	</c:if>
	<c:if test="${sessionScope.prodcoupons eq null}">
		<div class="zeroPdng">
			<img alt="close"
				src="/ScanSeeWeb/images/consumer/noReviews_poster.png" width="584"
				height="333" />

		</div>
	</c:if>
</body>
</html>
</html>
<!-- This page used to display the product associated coupons in product summary  screen.-->

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/consumerstyle.css" />


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
html,body {
	background-image: none !important;
}
</style>
</head>

<body class="whiteBG ZeroBg" style="background-image: none !important">

	<c:if test="${sessionScope.prodcoupons ne null}">
		<div class="contBlock">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="cstmTbl title-desc">

				<c:choose>
					<c:when test="${sessionScope.prodcoupons ne null}">


						<!--This for loop used to  display the product multiple images. -->
						<c:forEach items='${sessionScope.prodcoupons}' var='prodCoupon'
							varStatus="indexnum">

							<c:if test="${prodCoupon.couponName ne null}">

								<tr>
									<td width="8%"><img src="${prodCoupon.imagePath}"
										onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';"
										width="40" height="40" />
									</td>
									<td width="86%"><a href="javascript:void(0);">${prodCoupon.couponName}</a><span>${prodCoupon.couponDescription}</span>
									</td>
									<td width="6%"><img
										src="/ScanSeeWeb/images/consumer/rightArrow.png" alt="arrow" />
									</td>
								</tr>

							</c:if>
						</c:forEach>

					</c:when>
					<c:otherwise>

					</c:otherwise>
				</c:choose>

			</table>

		</div>
	</c:if>
	<c:if test="${sessionScope.prodcoupons eq null}">
		<div class="zeroPdng">
			<img alt="close"
				src="/ScanSeeWeb/images/consumer/noCoupons_poster.png" width="584"
				height="333" />

		</div>
	</c:if>
</body>
</html>


<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script src="https://platform.twitter.com/widgets.js" type="text/javascript"></script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js" type="text/javascript"></script>
<script type="text/javascript">
	function breadCrumbLoc() {
		var pagenumber = $('#currentPageNumber').val()
		var recordCount = $('#locSearchRecCount').val()
		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = recordCount;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = "findresultloctnlist.htm";
		document.paginationForm.method = "POST";
		document.paginationForm.submit();

		//document.findLocSearchResultForm.action = "findresultloctnlist.htm";
		//document.findLocSearchResultForm.method = "POST";
		//document.findLocSearchResultForm.submit();
	}
	function showSpecialPage() {

		document.findLocSearchResultForm.action = "findresultloctnlistspecials.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}
	function showSalePage() {

		document.findLocSearchResultForm.action = "findresultloctnlistsales.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}
	function showHotDealPage() {

		document.findLocSearchResultForm.action = "findresultloctnlistdeals.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}
	function callNextPage(pagenumber, url) {
		var selValue = $('#selPerPage :selected').val();

		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}

	function getPerPgaVal() {

		var selValue = $('#selPerPage :selected').val();
		document.findLocSearchResultForm.recordCount.value = selValue;

		//Call the method which populates grid values
		searchRetLocSales();

	}

	function searchRetLocSales() {
		document.findLocSearchResultForm.action = "findresultloctnlistsales.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}

	function twiterShareClick(hdName,hdURL){
		//function twiterShareClick(hdName){

		var tweetText;
		var hdURL;
		//var hdName='${hotDealDetails.hotDealName}';
		//var hdURL='${hotDealDetails.hdURL}';
		if(hdURL=="" && hdURL == 'NotApplicable'){
		tweetText='Great find at ScanSee!'+hdName
		}else{
		tweetText='Great find at ScanSee! '+hdName+"Visit:"+hdURL
		}
		var tweeturl = 'http://twitter.com/share?text='+escape(tweetText);

		window.open(tweeturl);

		 twttr.events.bind('tweet', function(event) {
		       alert("Lottery Entry Successful");
		       window.location = "http://www.mysite.com"
		    });
		}

		<!--Below script method is used for sharing product information via pinterest. -->

		function sharePin(){

		      var base = "http://pinterest.com/pin/create/button/";
		      var encodedpinurl=escape("https://www.scansee.net");
		      encodedpinurl=encodedpinurl.replace("+", "%2B");
		      encodedpinurl=encodedpinurl.replace("/", "%2F");
		      var i = 0;	
		               var encodedimagePath=escape('${sessionScope.hotDealsDetailsObj.hotDealImagePath}');
		                  encodedimagePath=encodedimagePath.replace("+", "%2B");
		                  encodedimagePath=encodedimagePath.replace("/", "%2F");
		     
		           var encodedproductName=escape('${sessionScope.hotDealsDetailsObj.hotDealName}');
		            encodedproductName=encodedproductName.replace("+", "%2B");
		            encodedproductName=encodedproductName.replace("/", "%2F");
		         finalUrl = base + "?url=" + encodedpinurl + "&media=" + encodedimagePath + "&description=" + encodedproductName;
		       
			
		            inner = "<a href='" + finalUrl + "'class='pin-it-button' count-layout='none' target='_blank'><img src='/ScanSeeWeb/images/consumer/pin_down.png' alt='pinterest'/></a>";
		        
					$("#pin_it_"+i).html(inner);
		            i++;
		   
		      }
			
</script>
<div id="contWrpr">
	<div class="breadCrumb">
		<ul>
			<li class="brcIcon"><img
				src="/ScanSeeWeb/images/consumer/find_bcIcon.png" alt="find" /></li>
			<li class=""><a href="consfindhome.htm">Find</a>
			</li>
			<li><a href="#" onclick="breadCrumbLoc();">Location</a>
			</li>
			<li><c:if
					test="${requestScope.displaySaleProdSummy == 'ProductSummary' }">
					<li><a href="javascript:window.history.go(-1);">${sessionScope.retailerInfo.retailerName}
							- Sales</a></li>
				</c:if> <c:if test="${requestScope.displaySaleProdSummy == 'HotDeal' }">
					<li><a href="javascript:window.history.go(-1);">${sessionScope.retailerInfo.retailerName}
							- Hot Deals</a></li>
				</c:if> <c:choose>
					<c:when test="${requestScope.displaySaleProdSummy == 'Sale' }">
						<li class="active">${sessionScope.retailerInfo.retailerName }</li>
					</c:when>
					<c:when test="${requestScope.displaySaleProdSummy == 'ProductSummary'}">
						<li class="active productTrim">${sessionScope.consproddetails.productName}</li>
					</c:when>
					<c:when test="${requestScope.displaySaleProdSummy == 'HotDeal' }">
						<li class="active">${sessionScope.hotDealsDetailsObj.hotDealName}</li>
					</c:when>
				</c:choose>
		</ul>
		<span class="rtCrnr">&nbsp;</span>
	</div>
	<div class="contBlks">
		<form:form name="findLocSearchResultForm"
			commandName="findLocSearchResultForm">
			<form:hidden path="recordCount" />
			<form:hidden path="zipCode" />
			<form:hidden path="searchKey" />
			<form:hidden path="categoryName" />
			<form:hidden path="retailerId" />
			<form:hidden path="retailLocationID" />
			<form:hidden path="retListID" />
			<form:hidden path="currentPageNumber" />
			<form:hidden path="locSearchRecCount" />
			<c:choose>
				<c:when test="${requestScope.displaySaleProdSummy == 'Sale' }">
					<ul class="cstmAccordion">
						<c:choose>
							<c:when
								test="${sessionScope.retailerInfo.retailerURL ne null && sessionScope.retailerInfo.retailerURL!=''}">
								<li class="active  cstm-opt">
							</c:when>
							<c:when
								test="${sessionScope.retailerInfo.contactPhone ne null && sessionScope.retailerInfo.contactPhone!=''}">
								<li class="active  cstm-opt">
							</c:when>
							<c:otherwise>
								<li class="active">
							</c:otherwise>
						</c:choose>
						<dl>
							<dt class="cstm-img">
								<img src="${sessionScope.retailerInfo.logoImagePath}" alt="image"
									width="48" height="48"
									onerror="this.src='/ScanSeeWeb/images/consumer/imgIcon.png'" />
							</dt>
							<dt class="cstm-title">
								<span class="title">${sessionScope.retailerInfo.retailerName}</span><span
									class="subTitle">${sessionScope.retailerInfo.retaileraddress1},${sessionScope.retailerInfo.city},${sessionScope.retailerInfo.postalCode},${sessionScope.retailerInfo.state}</span>
								<c:if
									test="${sessionScope.retailerInfo.retailerURL ne null && sessionScope.retailerInfo.retailerURL!=''}">
									<span class="col_actn"><img
										src="/ScanSeeWeb/images/consumer/FNB_browseIcon.png" width="22"
										height="22" /> <a href="${sessionScope.retailerInfo.retailerURL}"
										target="_blank" class="innerLink">Browse Website </a> </span>
								</c:if>
								<c:if
									test="${sessionScope.retailerInfo.contactPhone ne null && sessionScope.retailerInfo.contactPhone!=''}">
									<span class="col_actn"><img
										src="/ScanSeeWeb/images/consumer/phone_icon.png" alt="call" width="24"
										height="24" /> <c:out
											value="${sessionScope.retailerInfo.contactPhone}"></c:out> </span>
								</c:if>
							</dt>
							<dt class="cstm-spcl">&nbsp;</dt>
							<dt class="cstm-rtarrow">&nbsp;</dt>
						</dl>
						<div class="cstm-Cont dsply-block zeroBrdr">
							<div class="ltPnl floatL stretch">
								<ul class="four-up icon-txt brdr-inset topBrdr" id="">
									<li class="active"><a href="#" onclick="showSalePage();"
										class="Sales"><img
											src="/ScanSeeWeb/images/consumer/salesTab-icon.png" alt="sales"
											align="absmiddle" />Sales</a>
									</li>
									<li><a href="#" onclick="showSpecialPage();" class="Specials"><img
											src="/ScanSeeWeb/images/consumer/specialTab-icon.png" alt="specials"
											align="absmiddle" />Specials</a>
									</li>
									<li><a href="#" onclick="showHotDealPage();" class="HotDeals"><img
											src="/ScanSeeWeb/images/consumer/hotdealTab-icon.png" alt="hotdeals"
											align="absmiddle" />Hot Deals</a>
									</li>
									<li><a href="#" class="Coupons"><img
											src="/ScanSeeWeb/images/consumer/cpnTab-icon.png" alt="coupons"
											align="absmiddle" />Coupons</a>
									</li>
								</ul>
								<div class="sales cmnPnlShow">
									<c:choose>
										<c:when test="${requestScope.saleProductList eq null}">
											<img width="970" height="373" alt="none"
												src="/ScanSeeWeb/images/consumer/noneAvailable.png">
										</c:when>
										<c:otherwise>
											<table width="100%" border="0" cellspacing="0" cellpadding="0"
												class="cstmTbl salesLst">
												<c:forEach items="${requestScope.saleProductList}" var="item">
													<tr>
														<td width="8%"><img src="${item.imagePath}"
															onerror="this.src='/ScanSeeWeb/images/imageNotFound.png'"
															alt="image" width="48" height="48" /></td>
														<td width="86%"><a
															href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${item.productId}&key2=${item.saleListID}&page=location"
															class="title salesLst" title="${item.productName}">
																<p class="trnctTxt">${item.productName}</p> </a> <span
															class="multiVal"><c:out value="${item.price}">
																</c:out> </span><span class="multiVal sale-icon"><c:out
																	value="${item.salePrice}">
																</c:out> </span></td>
														<td width="6%"><img src="/ScanSeeWeb/images/rightArrow.png"
															alt="arrow" />
														</td>
													</tr>
												</c:forEach>
											</table>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						</li>
					</ul>
				</c:when>
				<c:when test="${requestScope.displaySaleProdSummy == 'ProductSummary'}">
					<%@include file="consprodsummary.jsp"%>
				</c:when>
			</c:choose>
		</form:form>
		<c:if test="${requestScope.displaySaleProdSummy == 'HotDeal'}">
			<form name="dealsearchform">
				<input type="hidden" name="hotDealId" id="hotDealId" value="" /> <input
					type="hidden" name="redirectUrl" id="redirectUrl" value="" /> <input
					type="hidden" name="emailId" id="emailId"
					value="${sessionScope.userEmailId}" />
				<div class="contBlks" id="cpnDetails">
					<div class="imgView splitView contBox relative">
						<img src="${sessionScope.hotDealsDetailsObj.hotDealImagePath}"
							alt="hdimage"
							onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';" width="200"
							height="150" class="mrgnTopSmall" align="middle" />
						<div align="center" class="contrlStrip zeroBrdr">
							<span> <img src="/ScanSeeWeb/images/consumer/fb_down.png"
								alt="facebook"
								onclick="hotDealfbshare(${sessionScope.hotDealsDetailsObj.hotDealId},'dealdetails')" />
								<img src="/ScanSeeWeb/images/consumer/twitter_down.png" alt="twitter"
								onClick="twiterShareClick('${sessionScope.hotDealsDetailsObj.hotDealName}','${sessionScope.hotDealsDetailsObj.hdURL}')" />
								<span id="pin_it_${i}" class="inline"></span> <c:set var="i"
									value="${i + 1}"></c:set> <img
								src="/ScanSeeWeb/images/consumer/email_down.png"
								class="hotdealdetailhareemailActn"
								name="${sessionScope.hotDealsDetailsObj.hotDealId}" alt="email" /> </span>
						</div>
					</div>
					<div class="contView splitView">
						<dl>
							<dt>HotDeal Name:</dt>
							<dd>${sessionScope.hotDealsDetailsObj.hotDealName}</dd>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.retName ne null && !empty sessionScope.hotDealsDetailsObj.retName}">
								<dt>Retailer Name:</dt>
								<dd>${sessionScope.hotDealsDetailsObj.retName}</dd>
							</c:if>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.retAddr ne null && !empty sessionScope.hotDealsDetailsObj.retAddr}">
								<dt>Retailer Address:</dt>
								<dd>${hotDealsDetailsObj.retAddr}</dd>
							</c:if>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.city ne null && !empty sessionScope.hotDealsDetailsObj.city}">
								<dt>City:</dt>
								<dd>${hotDealsDetailsObj.city}</dd>
							</c:if>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.state ne null && !empty sessionScope.hotDealsDetailsObj.state}">
								<dt>State:</dt>
								<dd>${hotDealsDetailsObj.state}</dd>
							</c:if>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.hDshortDescription ne null && !empty sessionScope.hotDealsDetailsObj.hDshortDescription}">
								<dt>Short Description:</dt>
								<dd>${hotDealsDetailsObj.hDshortDescription}</dd>
							</c:if>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.hDLognDescription ne null && !empty sessionScope.hotDealsDetailsObj.hDLognDescription}">
								<dt>Long Description:</dt>
								<dd>${hotDealsDetailsObj.hDLognDescription}</dd>
							</c:if>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.hDStartDate ne null && !empty sessionScope.hotDealsDetailsObj.hDStartDate}">
								<dt>Start Date:</dt>
								<dd>${hotDealsDetailsObj.hDStartDate}</dd>
							</c:if>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.hDEndDate ne null && !empty sessionScope.hotDealsDetailsObj.hDEndDate}">
								<dt>End Date:</dt>
								<dd>${hotDealsDetailsObj.hDEndDate}</dd>
							</c:if>
							<c:if
								test="${sessionScope.hotDealsDetailsObj.poweredBy ne null && !empty sessionScope.hotDealsDetailsObj.poweredBy}">
								<dt>Powered by:</dt>
								<dd>${hotDealsDetailsObj.poweredBy}</dd>
							</c:if>
						</dl>
					</div>
				</div>
			</form>
		</c:if>
		<c:choose>
			<c:when test="${requestScope.displaySaleProdSummy == 'Sale' }">
				<div class="pagination brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="noBrdr" id="perpage">
						<tr>
							<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" enablePerPage="true" />
						</tr>
					</table>
				</div>
			</c:when>
		</c:choose>
	</div>
</div>
<div class="clear"></div>

<!-- This page used to display the find  search product list results  -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>

<script src="https://platform.twitter.com/widgets.js"
	type="text/javascript"></script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>


<script type="text/javascript">	
function callNextPage(pagenumber, url) {
			var selValue = $('#selPerPage :selected').val();

		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}

function getPerPgaVal() {

	var selValue = $('#selPerPage :selected').val();

		document.dealsearchform.recordCount.value = selValue;
		//Call the method which populates grid values
		findpaginationdealssearch();

	}
	
function twiterShareClick(hdName,hdURL){
//function twiterShareClick(hdName){

var tweetText;
var hdURL;
//var hdName='${hotDealDetails.hotDealName}';
//var hdURL='${hotDealDetails.hdURL}';
if(hdURL=="" && hdURL == 'NotApplicable'){
tweetText='Great find at ScanSee!'+hdName
}else{
tweetText='Great find at ScanSee! '+hdName+"Visit:"+hdURL
}
var tweeturl = 'http://twitter.com/share?text='+escape(tweetText);

window.open(tweeturl);

 twttr.events.bind('tweet', function(event) {
       alert("Lottery Entry Successful");
       window.location = "http://www.mysite.com"
    });
}

<!--Below script method is used for sharing product information via pinterest. -->

function sharePin(){
      var base = "http://pinterest.com/pin/create/button/";
      var encodedpinurl=escape("https://www.scansee.net");
      encodedpinurl=encodedpinurl.replace("+", "%2B");
      encodedpinurl=encodedpinurl.replace("/", "%2F");
      var i = 0;
	  <c:forEach items="${dealsearhresults.hotDealsCategoryInfo}"
							var="hotDealCat">
	<c:forEach items="${hotDealCat.hotDealsDetailsArrayLst}" var="hotDeal">
               var encodedimagePath=escape('${hotDeal.hotDealImagePath}');
                  encodedimagePath=encodedimagePath.replace("+", "%2B");
                  encodedimagePath=encodedimagePath.replace("/", "%2F");
     
           var encodedproductName=escape('${hotDeal.hotDealName}');
            encodedproductName=encodedproductName.replace("+", "%2B");
            encodedproductName=encodedproductName.replace("/", "%2F");
         finalUrl = base + "?url=" + encodedpinurl + "&media=" + encodedimagePath + "&description=" + encodedproductName;
       
	
            inner = "<a href='" + finalUrl + "'class='pin-it-button' count-layout='none' target='_blank'><img src='/ScanSeeWeb/images/consumer/pin_down.png' width='25' height='25'  alt='pinterest'/></a>";
        
			$("#pin_it_"+i).html(inner);
            i++;
    </c:forEach>
	</c:forEach>
      }

</script>

<body onresize="resizeDoc();" onload="resizeDoc();">

	<div id="contWrpr">
		<form:form name="dealsearchform" commandName="dealsearchform">
			<input type="hidden" name="hotDealId" id="hotDealId" value="" />
			<input type="hidden" name="redirectUrl" id="redirectUrl" value="" />
			<input type="hidden" name="emailId" id="emailId"
				value="${sessionScope.userEmailId}" />
			<input type="hidden" name="recordCount" id="recordCount" value="" />

			<div class="breadCrumb">
				<ul>
					<li class="brcIcon"><img
						src="/ScanSeeWeb/images/consumer/find_bcIcon.png" alt="find" /></li>
					<li><a href="consfindhome.htm">Find</a></li>
					<li><a href="consfindhome.htm?searchType=Deals">Deals</a></li>

					<li class="active"><c:out value="${sessionScope.searchKey}"></c:out>
						( Showing <c:out value="${sessionScope.totalresults}"></c:out>
						results )</li>

				</ul>




				<span class="rtCrnr">&nbsp;</span>
			</div>
			<div id="detailedList" class="forDeals">
				<c:set var="i" value="0" />
				<c:choose>

					<c:when test="${sessionScope.dealsearhresults ne null}">
						<!--This for loop used to  display the product details. -->
						<c:forEach items="${dealsearhresults.hotDealsCategoryInfo}"
							var="hotDealCat">
							<div class="contBlks">
								<h3 class="subTitleBg">${hotDealCat.categoryName}</h3>


								<c:forEach items="${hotDealCat.hotDealsDetailsArrayLst}"
									var="hotDeal">
									<div class="contBox">
										<ul>

											<li class="imgBx">
											
											<a
												href="/ScanSeeWeb/shopper/conshddetails.htm?hotdealId=${hotDeal.hotDealId}
												&hdlstid=${hotDeal.hotdealLstId}">
											<img src="${hotDeal.hotDealImagePath}"
												alt="hdimage"
												onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
												width="200" height="150" />
												</a></li>
											<li class="title " title="${hotDeal.hotDealName}"><a
												href="/ScanSeeWeb/shopper/conshddetails.htm?hotdealId=${hotDeal.hotDealId}&hdlstid=${hotDeal.hotdealLstId}"
												class="dealtrim">${hotDeal.hotDealName}</a></li>
												
												<c:if test="${hotDeal.apiPartnerName ne  null}">
													<li><label>Powered By:</label><label class="labeltxt">
															${hotDeal.apiPartnerName}</label></li>
												</c:if>
												
												
											<li class="desc more">${hotDeal.hDshortDescription}</li>
											<li class="desc more">${hotDeal.hDLognDescription}</li>
										</ul>
										<div align="center" class="contrlStrip">
											<span> <img
												src="/ScanSeeWeb/images/consumer/fb_down.png" alt="facebook"
												width="25" height="25"
												onclick="hotDealfbshare(${hotDeal.hotDealId},'dealsearch')" />
												<img src="/ScanSeeWeb/images/consumer/twitter_down.png"
												width="25" height="25" alt="twitter"
												onClick="twiterShareClick('${hotDeal.hotDealName}','${hotDeal.hdURL}')" />
												<span id="pin_it_${i}" class="inline"></span> <c:set var="i"
													value="${i + 1}"></c:set> <img
												src="/ScanSeeWeb/images/consumer/email_down.png"
												class="hotdealshareemailActn" name="${hotDeal.hotDealId}"
												width="25" height="25" alt="email" /> </span>
										</div>

									</div>

								</c:forEach>
							</div>
						</c:forEach>
					</c:when>

					<c:otherwise>
						<div class="zeroBg contBlks ">
							<img alt="close"
								src="/ScanSeeWeb/images/consumer/nodeals_poster.png" />
						</div>

					</c:otherwise>
				</c:choose>
				<div class="clear"></div>

			</div>
		</form:form>
		<c:if
			test="${sessionScope.dealsearhresults ne null && !empty sessionScope.dealsearhresults}">


			<div class="pagination brdrTop">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="noBrdr" id="perpage">
					<tr>
						<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
							nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
							pageRange="${sessionScope.pagination.pageRange}"
							url="${sessionScope.pagination.url}" enablePerPage="true" />
					</tr>
				</table>
			</div>
		</c:if>
		<div class="clear"></div>

		<script type="text/javascript">
	sharePin();
	
</script>

	</div>

</body>

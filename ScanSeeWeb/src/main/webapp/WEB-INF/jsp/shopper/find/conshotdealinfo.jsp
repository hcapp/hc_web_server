<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- This page used to display the searched hotdeal details  -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script src="https://platform.twitter.com/widgets.js"
	type="text/javascript"></script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>

<script type="text/javascript">
function twiterShareClick(hdName,hdURL){
//function twiterShareClick(hdName){

var tweetText;
var hdURL;
//var hdName='${hotDealDetails.hotDealName}';
//var hdURL='${hotDealDetails.hdURL}';
if(hdURL=="" && hdURL == 'NotApplicable'){
tweetText='Great find at ScanSee!'+hdName
}else{
tweetText='Great find at ScanSee! '+hdName+"Visit:"+hdURL
}
var tweeturl = 'http://twitter.com/share?text='+escape(tweetText);

window.open(tweeturl);

 twttr.events.bind('tweet', function(event) {
       alert("Lottery Entry Successful");
       window.location = "http://www.mysite.com"
    });
}

<!--Below script method is used for sharing product information via pinterest. -->

function sharePin(dealId,i){
	var base = "http://pinterest.com/pin/create/button/";
	$.ajaxSetup({cache:false});
	$.ajax({
	type : "GET",
	url : "/ScanSeeWeb/shopper/pinterestShareDealInfo.htm",
	data : {
		'dealId'  : dealId
	},
	success : function(response) {
			var responseJSON = JSON.parse(response);
			var encodedpinurl=responseJSON.dealURL;
			encodedpinurl=encodedpinurl.replace("+", "%2B");
		    encodedpinurl=encodedpinurl.replace("/", "%2F");		
			var prodName=responseJSON.dealName;
			var encodedproductName=escape(prodName);
            encodedproductName=encodedproductName.replace("+", "%2B");
            encodedproductName=encodedproductName.replace("/", "%2F");
            var encodedimagePath=responseJSON.dealImagePath;
            encodedimagePath=encodedimagePath.replace("+", "%2B");
            encodedimagePath=encodedimagePath.replace("/", "%2F");
            finalUrl = base + "?url=" + encodedpinurl + "&media=" + encodedimagePath + "&description=" + encodedproductName;
            inner = "<a href='" + finalUrl + "'class='pin-it-button' count-layout='none' target='_blank'><img id='pin" + i + "' src='/ScanSeeWeb/images/consumer/pin_down.png' alt='pinterest'/></a>";
  			$("#pin_it_"+i).html(inner);
  			$("#pin"+i).click();
	},
	error : function(e) {
		alert('Error Occured');
	}
	});
}

</script>


<body onresize="resizeDoc();" onload="resizeDoc();">
	<form name="dealsearchform">
		<input type="hidden" name="hotDealId" id="hotDealId" value="" /> <input
			type="hidden" name="redirectUrl" id="redirectUrl" value="" /> <input
			type="hidden" name="emailId" id="emailId"
			value="${sessionScope.emailId}" />

		<div id="contWrpr">
			<c:set var="i" value="0" />
			<div class="breadCrumb">

				<c:choose>
					<c:when test="${requestScope.breadCrumLink== 'hotdeal' }">
						<ul>
							<li class="brcIcon"><img
								src="/ScanSeeWeb/images/consumer/hotDeals_bcIcon.png"
								alt="HotDeal" /></li>
							<li><a href="javascript:window.history.go(-1);">Hot
									Deals</a></li>
							<li class="title productTrim active"
								title="${sessionScope.hotDealsDetailsObj.hotDealName}"><c:out
									value="${sessionScope.hotDealsDetailsObj.hotDealName}" />
							</li>
						</ul>



					</c:when>
					<c:when test="${requestScope.breadCrumLink== 'WL' }">
						<ul>
							<li class="brcIcon"><img
								src="/ScanSeeWeb/images/consumer/wl_bcIcon.png"
								alt="WishList" /></li>
							<li><a href="conswishlisthome.htm">Wish List
									</a></li>
									<li><a href="javascript:window.history.go(-1);">Alerted Items
									</a></li>
									
							<li class="title productTrim active"
								title="${sessionScope.hotDealsDetailsObj.hotDealName}"><c:out
									value="${sessionScope.hotDealsDetailsObj.hotDealName}" />
							</li>
						</ul>



					</c:when>
					<c:otherwise>
						<ul>
							<li class="brcIcon"><img
								src="/ScanSeeWeb/images/consumer/find_bcIcon.png" alt="find" />
							</li>
							<li><a href="consfindhome.htm?searchType=Deals">Find</a></li>


							<c:choose>
								<c:when test="${requestScope.breadCrumLink== 'SpclHotDeal' }">

									<li><a href="javascript:window.history.go(-1);">Hot
											Deals</a>
									</li>
								</c:when>
								<c:otherwise>
									<li onclick="finddeallstsearch()"><a
										href="javascript:void(0);">Deals</a>
									</li>
								</c:otherwise>
							</c:choose>



							<li class="title productTrim active"
								title="${sessionScope.hotDealsDetailsObj.hotDealName}"><c:out
									value="${sessionScope.hotDealsDetailsObj.hotDealName}" />
							</li>
						</ul>
					</c:otherwise>
				</c:choose>





			</div>

			<span class="rtCrnr">&nbsp;</span>
			<div class="contBlks" id="cpnDetails">

				<div class="imgView splitView contBox relative">

					<img src="${sessionScope.hotDealsDetailsObj.hotDealImagePath}"
						alt="hdimage"
						onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
						width="200" height="150" class="mrgnTopSmall" align="middle" />

					<div align="center" class="contrlStrip zeroBrdr">
						<span> <img src="/ScanSeeWeb/images/consumer/fb_down.png"
							alt="facebook"
							onclick="hotDealfbshare(${sessionScope.hotDealsDetailsObj.hotDealId},'dealdetails')" />


							<img src="/ScanSeeWeb/images/consumer/twitter_down.png"
							alt="twitter"
							onClick="twiterShareClick('${sessionScope.hotDealsDetailsObj.hotDealName}','${sessionScope.hotDealsDetailsObj.hdURL}')" />
							<c:if test="${sessionScope.hotDealsDetailsObj.hotDealImagePath ne null && !empty sessionScope.hotDealsDetailsObj.hotDealImagePath}">
														<span id="pin_it_${i}" class="inline"> 
														<img src='/ScanSeeWeb/images/consumer/pin_down.png' onclick="sharePin(${sessionScope.hotDealsDetailsObj.hotDealId},${i})" alt='pinterest'/>
														<c:set var="i" value="${i + 1}"></c:set> </span> 
													</c:if><img
							src="/ScanSeeWeb/images/consumer/email_down.png"
							class="hotdealdetailhareemailActn"
							name="${sessionScope.hotDealsDetailsObj.hotDealId}" alt="email" />
						</span>
					</div>
				</div>

				<div class="contView splitView">

					<dl>
						<dt>HotDeal Name:</dt>
						<dd>${sessionScope.hotDealsDetailsObj.hotDealName}</dd>
						<c:if
							test="${sessionScope.hotDealsDetailsObj.retName ne null && !empty sessionScope.hotDealsDetailsObj.retName}">
							<dt>Retailer Name:</dt>
							<dd>${sessionScope.hotDealsDetailsObj.retName}</dd>
						</c:if>
						<c:if
							test="${sessionScope.hotDealsDetailsObj.retAddr ne null && !empty sessionScope.hotDealsDetailsObj.retAddr}">
							<dt>Retailer Address:</dt>
							<dd>${hotDealsDetailsObj.retAddr}</dd>
						</c:if>
						<c:if
							test="${sessionScope.hotDealsDetailsObj.city ne null && !empty sessionScope.hotDealsDetailsObj.city}">
							<dt>City:</dt>
							<dd>${hotDealsDetailsObj.city}</dd>
						</c:if>
						<c:if
							test="${sessionScope.hotDealsDetailsObj.state ne null && !empty sessionScope.hotDealsDetailsObj.state}">
							<dt>State:</dt>
							<dd>${hotDealsDetailsObj.state}</dd>
						</c:if>
						<c:if
							test="${sessionScope.hotDealsDetailsObj.hDshortDescription ne null && !empty sessionScope.hotDealsDetailsObj.hDshortDescription}">
							<dt>Short Description:</dt>
							<dd>${hotDealsDetailsObj.hDshortDescription}</dd>
						</c:if>
						<c:if
							test="${sessionScope.hotDealsDetailsObj.hDLognDescription ne null && !empty sessionScope.hotDealsDetailsObj.hDLognDescription}">
							<dt>Long Description:</dt>
							<dd>${hotDealsDetailsObj.hDLognDescription}</dd>
						</c:if>
						<c:if
							test="${sessionScope.hotDealsDetailsObj.hDStartDate ne null && !empty sessionScope.hotDealsDetailsObj.hDStartDate}">
							<dt>Start Date:</dt>
							<dd>${hotDealsDetailsObj.hDStartDate}</dd>
						</c:if>

						<c:if
							test="${sessionScope.hotDealsDetailsObj.hDEndDate ne null && !empty sessionScope.hotDealsDetailsObj.hDEndDate}">
							<dt>End Date:</dt>
							<dd>${hotDealsDetailsObj.hDEndDate}</dd>
						</c:if>

						<c:if test="${sessionScope.hotDealsDetailsObj.poweredBy ne null && !empty 		   				sessionScope.hotDealsDetailsObj.poweredBy}">
							<dt>Powered by:</dt>
							<dd style="padding:0px 0px">${hotDealsDetailsObj.poweredBy}
							<c:if
							test="${sessionScope.hotDealsDetailsObj.hdURL ne null && !empty sessionScope.hotDealsDetailsObj.hdURL}">
							<span class="cstm-dsply">
								<a	href="${hotDealsDetailsObj.hdURL}" target="_blank" class="icon-bg"	> 
									<img src="/ScanSeeWeb/images/consumer/button_coupon_online.png" width="20" height="20"/> 
									<i style="color:#184B72"><b>Get Deal<b></i>
								</a>
							</span>
							
							
						</c:if>
							</dd>
						</c:if>


					</dl>
				</div>
			</div>

		</div>

	</form>
</body>


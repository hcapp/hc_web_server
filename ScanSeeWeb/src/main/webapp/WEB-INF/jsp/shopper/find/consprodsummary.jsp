<!--This page used to display the consumer find product summary details. -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script src="/ScanSeeWeb/scripts/shopper/consumerjquery.rating.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>

<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product Summary page</title>
</head>
<body>




	<!--Below script method is used for escaping the aporsthope. -->
	<script type="text/javascript">
	function testEscape(prodName) {
		 
		var strInputString=	prodName;
		strInputString=strInputString.replace(/'/g, "\\'");
		return strInputString;

		}


	<!--Below script method is used for updating user rating on product. -->

	
	$(function(){
	

 $(".rating-cancel a").click(function() {
	   

$('div.test').html("0");
deleteUserProdRating('${sessionScope.productRatingReview.productId}');
});   

$(".star-rating").click(function() {

saveUserProdRating('${sessionScope.productRatingReview.productId}');
});

		});
	
	<!--Below script method is used for sharing product information via twitte. -->
function twitterShare(productId,productListId){

	
	$.ajaxSetup({cache:false});
	$.ajax({
	type : "GET",
	url : "/ScanSeeWeb/shopper/twitterShareProductInfo.htm",
	data : {
		'productId'  : productId,
		'productListId':productListId
	},
	success : function(response) {
				var tweetText;
	var shareURL=response;
	var splitRes=shareURL.split(";")
	var prodName=splitRes[1];
		prodName=testEscape(prodName);

	
	tweetText='Great find at ScanSee!  '+prodName+"  Visit:"+splitRes[0]

	var tweeturl = 'http://twitter.com/share?text='+escape(tweetText);

	window.open(tweeturl);	
	},
	error : function(e) {
		alert('Error Occured');
	}
});


	
	}

	<!--Below script method is used for sharing product information via pinterest. -->

function sharePin(productId,productListId){


		var base = "http://pinterest.com/pin/create/button/";
		$.ajaxSetup({cache:false});
		$.ajax({
		type : "GET",
		url : "/ScanSeeWeb/shopper/pinterestShareProductInfo.htm",
		data : {
			'productId'  : productId,
			'productListId':productListId
		},
		success : function(response) {
				var responseJSON = JSON.parse(response);
				var encodedpinurl=responseJSON.productURL;
				encodedpinurl=escape(encodedpinurl);
				encodedpinurl=encodedpinurl.replace("+", "%2B");
			    encodedpinurl=encodedpinurl.replace("/", "%2F");	
			    encodedpinurl=encodedpinurl.replace("&", "%26");
			    encodedpinurl=encodedpinurl.replace("&", "%26");	
				var prodName=responseJSON.productName;
				var encodedproductName=escape(prodName);
	            encodedproductName=encodedproductName.replace("+", "%2B");
	            encodedproductName=encodedproductName.replace("/", "%2F");
	            var encodedimagePath=responseJSON.productImagePath;
                encodedimagePath=encodedimagePath.replace("+", "%2B");
                encodedimagePath=encodedimagePath.replace("/", "%2F");
                finalUrl = base + "?url=" + encodedpinurl + "&media=" + encodedimagePath + "&description=" + encodedproductName;
                inner = "<a href='" + finalUrl + "'class='pin-it-button' count-layout='none' target='_blank'><img id='pin' src='/ScanSeeWeb/images/consumer/pin_down.png' alt='pinterest' width='25' height='25'/></a>";
      			$("#pin_it").html(inner);
      			$("#pin").click();
		},
		error : function(e) {
			alert('Error Occured');
		}
		});
     
}

</script>
	<form name="prodsummaryform" commandName="prodsummaryform">
		<div class="contBlks" id="prodInfo">
			<input type="hidden" name="emailId" id="emailId"
				value="${sessionScope.userEmailId}" />
				<input type="hidden" name="productListID" id="productListID" value="" />
			<!--tabdPnl : Displaying the main headers. -->
			<ul class="tabdPnl" id="mainTab">
				<li class="active"><a href="javascript:void(0);"><img
						src="/ScanSeeWeb/images/consumer/overviewIcon.png" alt="overview" /><span>Overview</span>
				</a>
				</li>
				<li><a href="javascript:void(0);"><img
						src="/ScanSeeWeb/images/consumer/videoIcon.png" alt="video" /><span>Video</span>
				</a>
				</li>
				<li><a href="javascript:void(0);"><img
						src="/ScanSeeWeb/images/consumer/audioIcon.png" alt="audio" /><span>Audio</span>
				</a>
				</li>
				<li><a href="javascript:void(0);"><img
						src="/ScanSeeWeb/images/consumer/infoIcon.png" alt="info" /><span>Detailed
							Info</span> </a>
				</li>
				<li><a href="javascript:void(0);"><img
						src="/ScanSeeWeb/images/consumer/shareIcon.png" alt="share"
						width="26" height="26" /><span>Rate/Share</span> </a>
				</li>
			</ul>
			<div class="content">
			
				<div class="splitL floatL">
					&nbsp;
					<!--tabCont tab0 : Displaying the product details like product name,image and product attributes. -->
				
					<div class="tabCont tab0 sctmScroll_small" title="Click here to view large image">
							 
						<table width="100%" border="0">
							<tr>
							
						<!--This section used to  display the product multiple images. -->		
								<td width="45%">
									 
											<ul id="imgslide" class="zeroBrdr-lst">	
											
											<li>									
												<ins><a href="#" class="lt">&nbsp;</a></ins> <ins><a href="#" class="rt">&nbsp;</a></ins>
<c:choose>
									<c:when test="${fn:length(sessionScope.consproddetails.imagelst) gt 1}">
																
												<div align="center" class="imageBlock">
													<div class="imgWrp">
														<c:forEach items="${sessionScope.consproddetails.imagelst}" var='item'>


															<c:if test="${item ne null}">
																<a href="${item}"  class="overview" onclick="popImg(${sessionScope.consproddetails.productId})" name="image1" > 
																<img src="${item}"	onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
																alt="product1" width="200" height="150" alt="ImageNotFound" /> 
															</a>
															</c:if>



														</c:forEach>

													</div>
												</div>
												
									</c:when>
									<c:otherwise>
										<c:forEach items="${sessionScope.consproddetails.imagelst}" var='item'>
				
													<c:if test="${item ne null}">
														<a href="${item}" title = "${sessionScope.consproddetails.productName}"  class="${sessionScope.consproddetails.productId}" onclick="popImg(${sessionScope.consproddetails.productId})" > 
																<img src="${item}"	onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
																alt="product1" width="200" height="150" /> 
															</a>
													</c:if>
										</c:forEach>
									</c:otherwise>


								</c:choose>
								</li>
												</ul>
								</td>
								
								
								
								<td width="55%" align="left" valign="middle">
									<ul class="detail">

										<!--Needed : style="height:200px; overflow:hidden; overflow-y:scroll" -->
										<c:set var="i" value="0" />
										<li class="titletxt bold">${sessionScope.consproddetails.productName}</li>
										<li>${sessionScope.consproddetails.productShortDescription}</li>
										<!--Displaying the product attribues. -->

										<c:choose>
											<c:when test="${sessionScope.prodmediaattriinfo ne null}">


												<!--This for loop used to  display the product multiple attributes. -->
												<c:forEach
													items='${sessionScope.prodmediaattriinfo.prodAttributeList}'
													var='prodAttri' varStatus="indexnum">

													<c:if test="${prodAttri.attributeName ne null}">


														<li>${prodAttri.attributeName} :
															${prodAttri.displayValue}</li>



													</c:if>
												</c:forEach>

											</c:when>
											<c:otherwise>
												<span> No product attributes found.</span>
											</c:otherwise>
										</c:choose>

										<li class="clear"></li>
									</ul>
								</td>

							</tr>
						</table>

					</div>

					
					<!--tabCont tab1 :Displaying the product media details like videos. -->
					<div class="tabCont tab1">


						<c:if
							test="${sessionScope.prodmediaattriinfo.prodMediaList eq null && empty sessionScope.prodmediaattriinfo.prodMediaList}">

							<li>No product video found.</li>


						</c:if>


						<c:choose>
							<c:when test="${sessionScope.prodmediaattriinfo ne null}">


								
								<c:forEach
									items='${sessionScope.prodmediaattriinfo.prodMediaList}'
									var='prodMedia' varStatus="indexnum">

									<c:if test="${prodMedia.videoFilePath ne null}">
										<ul class="media video">

											<li><a href="${prodMedia.videoFilePath}" target="_blank">${prodMedia.videoFilePath}</a>
											</li>

										</ul>

									</c:if>

								</c:forEach>

							</c:when>
							<c:otherwise>
								<p>No product video found.</p>

							</c:otherwise>

						</c:choose>



					</div>

					<!--tabCont tab2 :Displaying the product media details like audios. -->

					<div class="tabCont tab2">


						<c:if
							test="${sessionScope.prodmediaattriinfo.prodMediaList eq null && empty sessionScope.prodmediaattriinfo.prodMediaList}">

							<li>No product audio found.</li>


						</c:if>
						<c:choose>
							<c:when test="${sessionScope.prodmediaattriinfo ne null}">


								<!--This for loop used to  display the product multiple attributes. -->
								<c:forEach
									items='${sessionScope.prodmediaattriinfo.prodMediaList}'
									var='prodMedia' varStatus="indexnum">

									<c:if test="${prodMedia.audioFilePath ne null}">
										<ul class="media audio">

											<li><a href="${prodMedia.audioFilePath}" target="_blank">${prodMedia.audioFilePath}</a>
											</li>

										</ul>

									</c:if>
								</c:forEach>

							</c:when>
							<c:otherwise>
								<li><span> No product audio found.</span>
								</li>
							</c:otherwise>
						</c:choose>


					</div>

					<!--tabCont tab3 :Displaying the product  detail information -->
					<div class="tabCont tab3 sctmScroll_small" title="Click here to view large image">
						<table width="100%" border="0">
							<tr>
								<td width="45%">
									<ul id="imgslide" class="zeroBrdr-lst">	
											
											<li>
											<ins><a href="#" class="lt">&nbsp;</a></ins> <ins><a href="#" class="rt">&nbsp;</a></ins>
									<c:choose>
									<c:when test="${fn:length(sessionScope.consproddetails.imagelst) gt 1}">
																				
												<div align="center" class="imageBlock">
													<div class="imgWrp">
														<c:forEach items="${sessionScope.consproddetails.imagelst}" var='item'>


															<c:if test="${item ne null}">
																<a href="${item}"  class="detail" onclick="popImg(${sessionScope.consproddetails.productId})" name="testImg2" > 
																<img src="${item}"	onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
																alt="product1" width="200" height="150" alt="ImageNotFound"/> 
															</a>
															</c:if>



														</c:forEach>

													</div>
												</div>
									</c:when>
									<c:otherwise>
										<c:forEach items="${sessionScope.consproddetails.imagelst}" var='item'>
				
													<c:if test="${item ne null}">
														<a href="${item}" title = "${sessionScope.consproddetails.productName}"  class="${sessionScope.consproddetails.productId}" onclick="popImg(${sessionScope.consproddetails.productId})" > 
																<img src="${item}"	onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
																alt="product1" width="200" height="150"  /> 
															</a>
													</c:if>
										</c:forEach>
									</c:otherwise>


								</c:choose>
									</li>
								</ul>
									
									
								</td>
								<td width="55%" align="left" valign="middle">
									<ul class="detail">

										<li class="titletxt bold">${sessionScope.consproddetails.productName}</li>
										<li>${sessionScope.consproddetails.productShortDescription}</li>
										<!--This for loop used to  display the product multiple attributes. -->
										<c:choose>
											<c:when test="${sessionScope.prodmediaattriinfo ne null}">


												<!--This for loop used to  display the product multiple images. -->
												<c:forEach
													items='${sessionScope.prodmediaattriinfo.prodAttributeList}'
													var='prodAttri' varStatus="indexnum">

													<c:if test="${prodAttri.attributeName ne null}">


														<li>${prodAttri.attributeName} :
															${prodAttri.displayValue}</li>



													</c:if>
												</c:forEach>

											</c:when>
											<c:otherwise>
												<span> No product attributes found.</span>
											</c:otherwise>
										</c:choose>
									</ul>
								</td>
							</tr>
						</table>
						<p>${sessionScope.consproddetails.productLongDescription}</p>

					</div>






					<!--tabCont tab4 :Displaying the product  rate and share information -->
					<div class="tabCont tab4" title="Click here to view large image">

						<table width="100%" border="0">

							<tr>
								<td width="45%">
									<ul id="imgslide" class="zeroBrdr-lst">	
											
											<li>
											<ins><a href="#" class="lt">&nbsp;</a></ins> <ins><a href="#" class="rt">&nbsp;</a></ins>
									<c:choose>
									<c:when test="${fn:length(sessionScope.consproddetails.imagelst) gt 1}">
																		
												<div align="center" class="imageBlock">
													<div class="imgWrp">
														<c:forEach items="${sessionScope.consproddetails.imagelst}" var='item'>


															<c:if test="${item ne null}">
																<a href="${item}"  class="ratereview" onclick="popImg(${sessionScope.consproddetails.productId})"  name="testImg3"> 
																<img src="${item}"	onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
																alt="product1" width="200" height="150" alt="ImageNotFound" /> 
															</a>
															</c:if>



														</c:forEach>

													</div>
												</div>
									</c:when>
									<c:otherwise>
										<c:forEach items="${sessionScope.consproddetails.imagelst}" var='item'>
				
													<c:if test="${item ne null}">
														<a href="${item}" title = "${sessionScope.consproddetails.productName}"  class="${sessionScope.consproddetails.productId}" onclick="popImg(${sessionScope.consproddetails.productId})" > 
																<img src="${item}"	onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
																alt="product1" width="200" height="150" /> 
															</a>
													</c:if>
										</c:forEach>
									</c:otherwise>


								</c:choose>
								</li>
								</ul>
									
								</td>

								<td width="55%" align="left" valign="middle">
									<div class="contBox" id="rateShareHtFix">
										<ul class="rating">
											<li class="titletxt bold">${sessionScope.consproddetails.productName}</li>

											<c:if test="${sessionScope.loginuser ne null}">

												<li><span> Your Rating:</span>
													<div class="floatL">
														<div style="width: 114px; height: 22px;">
															<input
																class="star {split:1,callback:function() {$('.test').text(this.value)}}"
																type="radio" name="Rating" value="1"
																<c:if test="${sessionScope.productRatingReview.currentRating eq 1}">checked="checked"</c:if> />
															<input class="star" type="radio" name="Rating"
																id="Rating" value="2"
																<c:if test="${sessionScope.productRatingReview.currentRating eq 2}">checked="checked"</c:if> />
															<input class="star" type="radio" name="Rating"
																id="Rating" value="3"
																<c:if test="${sessionScope.productRatingReview.currentRating eq 3}">checked="checked"</c:if> />
															<input class="star" type="radio" name="Rating"
																id="Rating" value="4"
																<c:if test="${sessionScope.productRatingReview.currentRating eq 4}">checked="checked"</c:if> />
															<input class="star" type="radio" name="Rating"
																id="Rating" value="5"
																<c:if test="${sessionScope.productRatingReview.currentRating eq 5}">checked="checked"</c:if> />
														</div>
													</div></li>

											</c:if>
											<div style="width: 100px; float: left">
												<div style="width: 100px">
													<div class="test Smaller valText">&nbsp;</div>
												</div>
											</div>
											</li>

											<li class="clear"></li>
											<li><span style="display: block">Average Customer
													Rating:</span>
												<div class="floatL">
													<c:forEach begin="1"
														end="${sessionScope.productRatingReview.avgRating}"
														var="current">
														<img src="../images/Red_fullstar.png" alt="Yellow"
															width="20" height="21" />
													</c:forEach>
													<c:forEach
														begin="${sessionScope.productRatingReview.avgRating+1}"
														end="5" var="current">
														<img src="../images/greyStar.png" alt="Yellow" width="20"
															height="21" />
													</c:forEach>
													<span class="valText inline"><c:out
															value="${sessionScope.productRatingReview.avgRating}" />
													</span>
											</li>
											<li class="clear"></li>

											<li><span> <img
													src="/ScanSeeWeb/images/consumer/fb_down.png"
													alt="facebook"
													onclick="prodSummaryfacebookshare(${sessionScope.consproddetails.productId},${sessionScope.consproddetails.productListID})"
													width='25' height='25' /> <img
													src="/ScanSeeWeb/images/consumer/twitter_down.png"
													alt="twitter"
													onclick="twitterShare(${sessionScope.consproddetails.productId},${sessionScope.consproddetails.productListID})"
													width='25' height='25' /> <span id="pin_it" class="inline">
														<img src='/ScanSeeWeb/images/consumer/pin_down.png'
														onclick="sharePin(${sessionScope.consproddetails.productId},${sessionScope.consproddetails.productListID})"
														alt='pinterest' width='25' height='25'> </span> <c:set
														var="i" value="${i + 1}"></c:set> <img
													src="/ScanSeeWeb/images/consumer/email_down.png"
													class="emailActn"
													name="${sessionScope.consproddetails.productId},${sessionScope.consproddetails.productListID}"
													alt="email" width='25' height='25' /> </span></li>
										</ul>
									</div>
									</div>
								</td>
							</tr>

						</table>

					</div>



					<!--subtabPnl mrgnTopSmall:Displaying the product  online and near by retailer  information -->

					<div class="subtabPnl mrgnTopSmall">
						<ul class="tabdPnl subTabdPnl">
							<li class="active"><a href="javascript:void(0);"><img
									src="/ScanSeeWeb/images/consumer/onlineIcon.png" alt="online" /><span>Online
										Stores</span> </a></li>
							<li><a href="javascript:void(0);"><img
									src="/ScanSeeWeb/images/consumer/nearbyIcon.png" alt="nearBy"
									onclick="getFindNearByRetailer()" /><span>Near By</span> </a></li>
						</ul>
						 


						<c:if
							test="${sessionScope.OnlineStoresList eq null && empty sessionScope.OnlineStoresList && 
									sessionScope.CJOnlineStores eq null && empty sessionScope.CJOnlineStores}">
							<div class="subTabdCnt online zeroPdng">
								<table width="100%" cellspacing="0" cellpadding="0" border="0"
									id="rtlrLst" class="cstmTbl title-desc">
									<tbody>




										<span>No online stores found.</span>


									</tbody>
								</table>
							</div>
						</c:if>
						<!-- Displaying commission junction retailer  information -->

						<c:if
							test="${sessionScope.CJOnlineStores ne null && !empty sessionScope.CJOnlineStores || 
							sessionScope.OnlineStoresList ne null && !empty sessionScope.OnlineStoresList}">

							<div class="subTabdCnt online sctmScroll_small zeroPdng">
								<table width="100%" cellspacing="0" cellpadding="0" border="0"
									id="rtlrLst" class="cstmTbl title-desc">
									<tbody>

										<c:if
											test="${sessionScope.CJOnlineStores ne null && !empty sessionScope.CJOnlineStores}">
											<tr class="subHdr">
												<td colspan="4" class="mobSubTitle">ScanSee</td>
											</tr>
											<c:forEach items="${sessionScope.CJOnlineStores}" var="item">

												<tr>
													<td width="4%" align="center"><img
														src="/ScanSeeWeb/images/consumer/imgIcon.png" alt="Img"
														width="38" height="38" /></td>
													<td width="70%"><span> <c:choose>
																<c:when test="${item.buyURL ne null}">

																	<a href="<c:out value="${item.buyURL}"/>"
																		target="_blank"> <c:out
																			value="${item.retailerName}" /> </a>
																</c:when>
																<c:otherwise>
																	<a href="javascript:void(0);" />" >
																<c:out value="${item.retailerName}" />
																	</a>
																</c:otherwise>
															</c:choose> </span></td>
													<td width="20%">
														<ul class="spList">



															<li><c:if test="${item.salePrice ne null}">


																	<c:out value="${item.salePrice}" />

																</c:if>
															</li>
															<li><c:if test="${item.shipmentCost ne null}">

																	<c:out value="${item.shipmentCost}" />
																	<span>shipping</span>

																</c:if> <c:if test="${item.shipmentCost eq null}">

																	<span>Free shipping</span>

																</c:if>
															</li>
														</ul>
													</td>
													<td width="6%"><img
														src="/ScanSeeWeb/images/consumer/rightArrow.png"
														alt="Arrow" width="11" height="15" /></td>
												</tr>


											</c:forEach>
										</c:if>


										<!-- Displaying shopzilla retailer  information -->

										<c:if
											test="${sessionScope.OnlineStoresList ne null && !empty sessionScope.OnlineStoresList}">
											<tr class="subHdr">
												<td colspan="4" class="mobSubTitle">Shopzilla</td>
											</tr>
											<c:forEach items="${sessionScope.OnlineStoresList}"
												var="item">

												<tr>

													<td width="4%" align="center"><img
														src="/ScanSeeWeb/images/consumer/imgIcon.png" alt="Img"
														width="38" height="38" /></td>
													<td width="70%"><span> <c:set var="strLength"
																value="${fn:length(item.url)}" /> <c:set var="url"
																value="${fn:substring(item.url,9,strLength-3)}" /> <c:choose>
																<c:when test="${url ne null}">

																	<a href="<c:out value="${url}"/>" target="_blank"><c:out
																			value="${item.merchantName}" /> </a>
																</c:when>
																<c:otherwise>
																	<a href="javascript:void(0);" />" >
																<c:out value="${item.merchantName}" />
																	</a>
																</c:otherwise>
															</c:choose> </span></td>
													<td width="20%">
														<ul class="spList">
															<li><c:out value="${item.salePrice}" /></li>


															<li><c:if test="${item.shipAmount ne null}">


																	<c:if test="${item.shipAmount eq '$0.00'}">

																		<i>Free shipping</i>

																	</c:if>

																	<c:if test="${item.shipAmount ne '$0.00'}">
																	+<c:out value="${item.shipAmount}" />
																		<i>shipping</i>
																	</c:if>

																</c:if> <c:if test="${item.shipAmount eq null}">

																	<span>Free shipping</span>

																</c:if>
															</li>

														</ul>
													</td>
													<td width="6%"><img
														src="/ScanSeeWeb/images/consumer/rightArrow.png"
														alt="Arrow" width="11" height="15" /></td>
												</tr>
											</c:forEach>

										</c:if>
									</tbody>
								</table>
							</div>
						</c:if>
						<!-- Displaying  product find nearby  retailer  information -->

						<c:if
							test="${sessionScope.findNearByDetails eq null && empty sessionScope.findNearByDetails && 
									sessionScope.ExtApifindNearByDetails eq null && empty sessionScope.ExtApifindNearByDetails}">
							<div class="subTabdCnt nearBy zeroPdng">
								<table width="100%" cellspacing="0" cellpadding="0" border="0"
									id="rtlrLst" class="cstmTbl title-desc">
									<tbody>
										<span>No Near by stores found.</span>

									</tbody>
								</table>
							</div>
						</c:if>


						<c:if
							test="${sessionScope.findNearByDetails ne null && !empty sessionScope.findNearByDetails || 
									sessionScope.ExtApifindNearByDetails ne null && !empty sessionScope.ExtApifindNearByDetails}">




							<div class="subTabdCnt nearBy sctmScroll_small zeroPdng">

								<table width="100%" cellspacing="0" cellpadding="0" border="0"
									id="rtlrLst" class="cstmTbl title-desc">
									<tbody>


										<!-- checking if database as find near by retailers or not , if retailers are there we are not calling 
											retailgence external api.-->
										<c:choose>
											<c:when
												test="${sessionScope.findNearByDetails ne null && !empty sessionScope.findNearByDetails}">



												<c:forEach
													items="${sessionScope.findNearByDetails.findNearByDetail}"
													var="item">

													<tr>
														<td width="4%" align="center"><img
															src="/ScanSeeWeb/images/consumer/imgIcon.png" alt="Img"
															width="38" height="38" /></td>
														<td width="90%"><span> <a
																href="javascript:void(0);"> <c:out
																		value="${item.retailerName}" /> <c:out
																		value="${item.distance}" />miles </a> </span></td>
														<td>
															<ul>
																<li><span> <c:out
																			value="${item.productPrice}" /> </span>
																</li>
															</ul>
														</td>
														<td width="6%"><img
															src="/ScanSeeWeb/images/consumer/rightArrow.png"
															alt="Arrow" width="11" height="15" /></td>
													</tr>


												</c:forEach>
											</c:when>

											<c:when
												test="${sessionScope.ExtApifindNearByDetails ne null && !empty sessionScope.ExtApifindNearByDetails}">

												<c:forEach
													items="${sessionScope.ExtApifindNearByDetails.findNearByDetails}"
													var="item">

													<tr>
														<td width="4%" align="center"><img
															src="/ScanSeeWeb/images/consumer/imgIcon.png" alt="Img"
															width="38" height="38" /></td>
														<td width="90%"><span> <a
																href="javascript:void(0);"> <c:out
																		value="${item.retailerName}" /> <c:out
																		value="${item.distance}" />miles </a> </span></td>
														<td>
															<ul>
																<li><span> <c:out
																			value="${item.productPrice}" /> </span>
																</li>
															</ul>
														</td>
														<td width="6%"><img
															src="/ScanSeeWeb/images/consumer/rightArrow.png"
															alt="Arrow" width="11" height="15" /></td>
													</tr>


												</c:forEach>
											</c:when>


											<c:otherwise>
												<span>No near by retailer found.</span>
											</c:otherwise>
										</c:choose>

									</tbody>
								</table>

							</div>
						</c:if>
					</div>
				</div>

				<div class="splitR floatL ">
					<ul class="subTabs greyBg">

						<li class=""><a href="javascript:void(0);"
							onclick="openIframePopup('ifrmPopup','ifrm','consprodcoupon.htm',360,600,'Find Coupons')">
								<h3 class="find">Find Coupons</h3> </a>
						</li>

						<li class=""><a href="javascript:void(0);" class="tglDsply">
								<h3 class="online">Locate Online</h3> </a>
						</li>
						<li><a href="javascript:void(0);" class="tglDsply">
								<h3 class="nearBy">Locate Nearby</h3> </a>
						</li>
						<li><a href="javascript:void(0);"
							onclick="openIframePopup('ifrmPopup','ifrm','consprodreview.htm',360,600,'Reviews')">
								<h3 class="find">See Reviews</h3> </a></li>

								
						<c:choose>
						<c:when test="${sessionScope.loginuser ne null && !empty sessionScope.loginuser}">
<li><a href="javascript:void(0);" onclick="addsummaryprodtoshoplist()" ><h3 class="sl">Add to Shopping List</h3></a></li>
<li><a href="javascript:void(0);"/ onclick="addsummaryprodtowishlist()"><h3 class="wl">Add to Wish List</h3> </a></li>
</c:when>

<c:otherwise>
	<li class="disable"><a href="javascript:void(0);">
		<h3 class="sl">Add to Shopping List</h3> </a>
						</li>
					<li class="disable"><a href="javascript:void(0);">	
					<h3 class="wl">Add to Wish List</h3> </a>
						</li>
</c:otherwise>


							</c:choose>
						

					</ul>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
			<div class="headerIframe">
				<img src="/ScanSeeWeb/images/consumer/popupClose.png"
					class="closeIframe" alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
				height="100%" allowtransparency="yes" width="100%"
				style="background-color: White"> </iframe>

		</div>
		<input type="hidden" name="productId" id="productId" value="" /> <input
			type="hidden" name="redirectUrl" id="redirectUrl" value="" /> <input
			type="hidden" name="emailId" id="emailId"
			value="${sessionScope.userEmailId}" />
	</form>
</body>
</html>
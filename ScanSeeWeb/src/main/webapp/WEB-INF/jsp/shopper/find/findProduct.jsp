<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />
<script src="/ScanSeeWeb/scripts/autocomplete.js" type="text/javascript"></script>

<form:form name="SearchForm" commandName="SearchForm">
	<div id="content" class="topMrgn">
		<div class="navBar">
			<ul>
				<li class="titletxt">Find</li>
			</ul>
		</div>
		<div class="clear"></div>
		<div class="mobCont noBg">
			<div class="splitDsply floatR">
				<ul class="tabdBtnTgl cstmTabs">
					<li class="noRtbrdr active txtcntr">
						<a href="findDisplay.htm">Location</a>
					</li>
					<li class="txtcntr">
						<a href="findproduct.htm">Products</a>
					</li>
				</ul>
				<div class="clear"></div>
				<div class="fluidView">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
						<tr>
							<td width="5%" align="center">Search:</td>
							<td width="15%" colspan="">
								<input name="searchValue" id="searchValue" class="textboxBig" />
							</td>
							<td width="80%">
								<input type="button" class="mobBtn" value="Search" onclick="window.location.href='shopperFindprod.html'" />
							</td>
						</tr>
					</table>
					<div class="stretch">
						<table id="slctItems1" width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
						</table>							
					</div>
				</div>
			</div>
			<jsp:include page="../leftmenu.jsp"></jsp:include>
			<div class="clear"></div>
			<div class="pagination" style="padding-left: 210px;">
				<p>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}" nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
							pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" />
				</p>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<input type="hidden" name="productId" id="productId" value="" />
	<input type="hidden" name="fromFind" id="fromFind" />
</form:form>


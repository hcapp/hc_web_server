<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script type="text/javascript">
	function callNextPage(pagenumber, url) {
		var selValue = $('#selPerPage :selected').val();

		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}

	function getPerPgaVal() {

		var selValue = $('#selPerPage :selected').val();
		document.findLocSearchResultForm.recordCount.value = selValue;

		//Call the method which populates grid values
		searchRetLoc();

	}

	function searchRetLoc() {
		document.findLocSearchResultForm.action = "findresultloctnlist.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}

	function getAnyhingPagelist(retailId,retLocId,retListId,index) {
		$.ajax({
			type : "GET",
			url : "getanythingpagelist.htm",
			data : {
				'retailId' : retailId,
				'retailLocId' : retLocId,
				'retListId' : retListId
			},

			success : function(response) {

				if(response!=""){
					$('#anythingPageList'+index).html(response);
					}else{
						 $(".cstm-Cont").hide();
						}
				
			},
			error : function(e) {
				//alert('Error: ' + e);
			}
		});
	}

	function showSpecialPage(retailId,retLocId,retListId) {

		var selValue = $('#selPerPage :selected').val();
		$('#retListID').val(retListId);
		$('#retailerId').val(retailId);
		$('#retailLocationID').val(retLocId);
		$('#locSearchRecCount').val(selValue);
		
		document.findLocSearchResultForm.action = "displayspecials.htm";
		document.findLocSearchResultForm.method = "POST";
		document.findLocSearchResultForm.submit();
	}
	function loadAnythingPageDetail(url){

		$(".gnrl").attr("src",url);	

			
		}
</script>
<body onresize="resizeDoc();" onload="resizeDoc();">
<div id="contWrpr">
	<form:form name="findLocSearchResultForm" commandName="findLocSearchResultForm">
		<form:hidden path="recordCount" />
		<form:hidden path="zipCode" />
		<form:hidden path="searchKey" />
		<form:hidden path="categoryName" />
		<form:hidden path="retailerId" />
		<form:hidden path="retailLocationID" />
		<form:hidden path="retListID" />
		<form:hidden path="currentPageNumber" />
		<form:hidden path="locSearchRecCount" />
		<div class="breadCrumb">
			<ul>
				<li class="brcIcon"><img src="/ScanSeeWeb/images/find_bcIcon.png"
					alt="find" />
				</li>
				<li class=""><a href="consfindhome.htm">Find</a></li>
				<c:choose>
					<c:when test="${sessionScope.retailLocList eq null}">
						<a href="consfindhome.htm?searchType=Locations"><li class="active">Location</li>
						</a>
					</c:when>
					<c:otherwise>
						<li><a href="consfindhome.htm?searchType=Locations">Location</a></li>
						<li class="active">${requestScope.breadCrumbTxt}</li>
					</c:otherwise>
				</c:choose>
			</ul>
			<span class="rtCrnr">&nbsp;</span>
		</div>
		<div class="contBlks">
			<c:choose>
				<c:when test="${sessionScope.retailLocList eq null}">
					<img width="970" height="373" alt="none"
						src="/ScanSeeWeb/images/consumer/noneAvailable.png">
				</c:when>
				<c:otherwise>
					<ul class="cstmAccordion" id="accordion">
						<c:set value="1" var="index" />
						<c:forEach items="${sessionScope.retailLocList}" var="item">
							<c:choose>
								<c:when test="${item.anyThingPageFlag eq true }">
									<c:choose>
										<c:when test="${item.retailerURL ne null && item.retailerURL!=''}">
											<li class="active  cstm-opt">
										</c:when>
										<c:when test="${item.contactPhone ne null && item.contactPhone!=''}">
											<li class="active  cstm-opt">
										</c:when>
										<c:otherwise>
											<li class="active">
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${item.retailerURL ne null && item.retailerURL!=''}">
											<li class="noNavigation cstm-opt">
										</c:when>
										<c:when test="${item.contactPhone ne null && item.contactPhone!=''}">
											<li class="noNavigation cstm-opt">
										</c:when>
										<c:otherwise>
											<li class="noNavigation">
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
							<dl
								onclick="getAnyhingPagelist(${item.retailerId},${item.retailLocationID},${item.retListID},${index})">
								<dt class="cstm-img">
									<img src="${item.logoImagePath}" alt="image" width="48" height="48"
										onerror="this.src='/ScanSeeWeb/images/consumer/imgIcon.png'" />
								</dt>
								<dt class="cstm-title">
									<span class="title"><c:out value="${item.retailerName}"></c:out>
									</span><span class="subTitle"><c:out
											value="${item.retaileraddress1},${item.city},${item.postalCode},${item.state}"></c:out>
									</span>
									<c:if test="${item.retailerURL ne null && item.retailerURL!=''}">
										<span class="col_actn"><img
											src="/ScanSeeWeb/images/consumer/FNB_browseIcon.png" width="22"
											height="22" /> <a href="${item.retailerURL}" target="_blank"
											class="innerLink">Browse Website </a> </span>
									</c:if>
									<c:if test="${item.contactPhone ne null && item.contactPhone!=''}">
										<span class="col_actn"><img
											src="/ScanSeeWeb/images/consumer/phone_icon.png" alt="call"
											width="24" height="24" /> <c:out value="${item.contactPhone}"></c:out>
										</span>
									</c:if>
								</dt>
								<c:choose>
									<c:when test="${item.saleFlag eq true }">
										<dt class="cstm-spcl">
											<input type="button" value="Specials"
												class="btn btn-special nav-spcl"
												onclick="showSpecialPage(${item.retailerId},${item.retailLocationID},${item.retListID})">
										</dt>
									</c:when>
									<c:otherwise>
										<dt class="cstm-spcl">&nbsp;</dt>
									</c:otherwise>
								</c:choose>
								<dt class="cstm-rtarrow">
									<img src="/ScanSeeWeb/images/rightArrow.png" alt="arrow" width="12"
										height="15" align="" />
								</dt>
							</dl>
							<div class="cstm-Cont" id="anythingPageList${index}"></div>
							</li>
							<c:set value="${index+1}" var="index" />
						</c:forEach>
					</ul>
				</c:otherwise>
			</c:choose>
		</div>
	</form:form>
	<div class="pagination brdrTop">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="noBrdr"
			id="perpage">
			<tr>
				<page:pageTag currentPage="${sessionScope.paginationloc.currentPage}"
					nextPage="4" totalSize="${sessionScope.paginationloc.totalSize}"
					pageRange="${sessionScope.paginationloc.pageRange}"
					url="${sessionScope.paginationloc.url}" enablePerPage="true" />
			</tr>
		</table>
	</div>
</div>
</body>
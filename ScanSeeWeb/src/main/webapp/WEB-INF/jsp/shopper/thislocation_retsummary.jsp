<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;">
	<!--<h3>List of Retailers Based on Search Criteria</h3>-->

	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="mobGrd detView zeroBtmMrgn">


		<c:if 
			test="${requestScope.retailerPageInfoList eq null && empty requestScope.retailerPageInfoList}">
			<span id="NoDataInfo" style="color: red; text-align: center;">
				No data found!</span>


		</c:if>


		<!-- Displaying if retailer has ribbenAdImage and ribbenAdurl otherwise retailer name.  -->
		<c:choose>
			<c:when
				test="${requestScope.retailerPageInfoList ne null && !empty requestScope.retailerPageInfoList}">
				<tr class="noHvr">
					<td colspan="3" align="center" class="zeroPadding">
						<div class="topBnr">
							<!--Retailer Name-->


							<c:if
								test="${requestScope.retailerPageInfoList.ribbonAdImagePath == 'NotApplicable'}">
								<img src="../images/imgIcon.png" alt="ad" width="320"
									height="50" />
								<span><i>${requestScope.retailerPageInfoList.distance}
										mi</i>
								</span>
							</c:if>
							<c:if
								test="${requestScope.retailerPageInfoList.ribbonAdImagePath != 'NotApplicable'}">
								<img
									src="${requestScope.retailerPageInfoList.ribbonAdImagePath}"
									alt="ad" width="320" height="50" />
								<span><i>${requestScope.retailerPageInfoList.distance} mi</i> </span>
							</c:if>
						</div>
					</td>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="4" align="center">${requestScope.retailerPageInfoList.retailerName}
						<span><i>${requestScope.retailerPageInfoList.distance}
								mi</i> </span></td>
				</tr>
			</c:otherwise>
		</c:choose>


		<!-- Displaying retailer current special -->

		<c:if test="${requestScope.retailerPageInfoList.saleFlag ne null}">
			<c:choose>
				<c:when test="${requestScope.retailerPageInfoList.saleFlag > 0}">

					<tr
						onclick="callRetSeeCurrentSpecials(${retailerPageInfoList.retailerId},${retailerPageInfoList.retailLocationID})">
						<td width="13%" align="center"><img
							src="../images/curntSpcls.png" alt="nearBY" width="38"
							height="38" /></td>
						<td width="81%"><span><a href="#">See Current
									Specials</a>
						</span></td>
						<td width="6%"><img src="../images/rightArrow.png"
							alt="Arrow" width="11" height="15" /></td>
					</tr>
				</c:when>
				<c:otherwise>

				</c:otherwise>
			</c:choose>
		</c:if>


		<!-- Displaying retailer get directions.  -->
		<c:if
			test="${requestScope.retailerPageInfoList.retaileraddress1 ne null && !empty requestScope.retailerPageInfoList.retaileraddress1}">
			<tr>
				<td align="center"><img src="../images/FNB_getdirectIcon.png"
					alt="getDirection" width="47" height="46" /></td>
				<td><span><a href="#" onclick="load(${sessionScope.getdirectionlat} , ${sessionScope.getdirectionlng})">Get Directions</a> </span>
					<ul>
						<li><span>${requestScope.retailerPageInfoList.retaileraddress1}</span>
						 
						</li>
					</ul></td>
				<td><img src="../images/rightArrow.png" alt="Arrow" width="11"
					height="15" /></td>

			</tr>
		</c:if>


		<!-- Displaying retailer contact phone.  -->
		<c:if
			test="${requestScope.retailerPageInfoList.contactPhone ne null && !empty requestScope.retailerPageInfoList.contactPhone}">

			<tr>
				<td align="center"><img src="../images/phone_icon.png"
					alt="callLocation" width="50" height="50" /></td>
				<td><span><a href="#">Call Location</a> </span>
					<ul>
						<li><span>${requestScope.retailerPageInfoList.contactPhone}</span>
						</li>
					</ul></td>
			
			</tr>
		</c:if>



		<!-- Displaying retailer website if retailer url there in response -->
		<c:if
			test="${requestScope.retailerPageInfoList.retailerURL  ne null && !empty requestScope.retailerPageInfoList.retailerURL}">
			<tr class="newWnd">
				<td align="center"><img src="../images/FNB_browseIcon.png"
					alt="browse" width="47" height="46" /></td>


				<td><span>Website</span> <a
					href="${requestScope.retailerPageInfoList.retailerURL}"></a>Browse Website
				</td>
				<td><img src="../images/rightArrow.png" alt="Arrow" width="11"
					height="15" /></td>
			</tr>

		</c:if>



		<!-- Displaying retailer created pages  -->
		<c:if
			test="${requestScope.retailCreatedPageslst ne null && !empty requestScope.retailCreatedPageslst}">
			<c:forEach items="${requestScope.retailCreatedPageslst}"
				var="createdPages">

				<tr>
					<td align="center"><img src="${createdPages.pageImage}"
						alt="browse" width="47" height="46"
						onerror="this.src = '../images/blankImage.gif';" />
					<td><span><a href="${createdPages.pageLink}"
							target="_blank"> ${createdPages.pageTitle} </a> </span>
					<td><img src="../images/rightArrow.png" alt="Arrow" width="11"
						height="15" /></td>
				</tr>


			</c:forEach>
		</c:if>


	</table>
</div>
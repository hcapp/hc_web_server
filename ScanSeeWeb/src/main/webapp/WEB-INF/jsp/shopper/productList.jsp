<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="java.util.*"%>
<%@page session="true"%>
<%@ page import="common.pojo.ProductVO"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld" %>

   <form:form name="SearchForm" commandName="SearchForm"> 
  <div id="content" class="topMrgn">
    <div class="navBar">
    	<ul>
        	<li><a href="#"><img src="../images/backBtn.png" onclick="javascript:back();" /></a></li>
            <li class="titletxt">Find</li>
          
      </ul>
    </div>
    <div class="clear"></div>
    <div class="mobCont noBg">
<div class="splitDsply floatR">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
    <tr>
      <td width="7%" align="center">Find:</td>
      <td width="51%">
      <form:select path="searchType" class="textboxBig" name="searchType">
    
      	<option>Please Select</option>
         <option value="products">Product</option>
        
      </form:select>
     <form:input path="searchKey" class="textboxSmall" name="searchKey"/>
      <td width="42%">
      <input type="button" class="mobBtn" value="Search" onclick="shopperProductSearch();" /></td> 
    </tr>
  </table>
<div class="clear"></div>
  <div class="fluidView">
    <div class="stretch">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd" id="findPnl">
    
       <tbody>
						<tr>
							<td colspan="2">Search Results for: ${sessionScope.searchForm.searchKey}<b><font size=3>
							</font> </b></td>
						</tr>
						<tr>
						<c:if test="${sessionScope.pagination.totalSize >0 && sessionScope.pagination.currentPage==1}">
							<td colspan="2">${sessionScope.pagination.totalSize} results found, top ${sessionScope.pagination.pageRange} displayed<br>
								<br></td>
							</c:if>
						</tr>
						
								<c:forEach items="${sessionScope.seacrhList.productList}"
									var="item">
									
									<tr>
									   
										<td width="11%"><img width="80" height="50"
											src="${item.productImagePath}"></td>
										<td width="89%"><a id="searchinfo" 
											onclick="productInfo(${item.retailLocationID},${item.productID})"><b>${item.productName}</b>
										</a> <b>${item.productName}</b> 
										</td>
									</tr>
								</c:forEach>
							

					</tbody>
				</table>
			
			
				<p>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
						nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
						pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" enablePerPage="false" />
				</p>
			</div>
			<div class="clear"></div>
		</div>
 
    </div>
 
  <%@include file="leftmenu.jsp"%>
 <div class="clear"></div>
				
			</div>
			<div class="tabBar"></div>
</div>	
</form:form>

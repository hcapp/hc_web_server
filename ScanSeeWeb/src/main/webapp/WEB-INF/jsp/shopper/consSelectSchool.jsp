<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
var ajxRes;
function loadUniversityDisplay() {
	ajxRes=document.selectschoolform.innerText.value;
	$('#myAjax').html(ajxRes);
}

	function loadCollege() {
		var stateCode = $('#Country').val();
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/shopper/shopperfetccollege.htm",
			data : {
				'statecode' : stateCode,
				'college' : null
			},

			success : function(response) {
				$('#myAjax').html(response);
			},
			error : function(e) {

			}
		});
	}

	function onCollegeSelLoad() {
		var vStateID = '${sessionScope.universityId}';
		//alert(vStateID)
		var sel = document.getElementById("College");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vStateID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}
	function setCollege() {
		var stateCode = $('#Country').val();
		$.ajaxSetup({cache:false})
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/shopper/shopperfetccollege.htm",
			data : {
				'statecode' : stateCode,
				'college' : 'setFlag'
			},

			success : function(response) {
							
				$('#myAjax').html(response);
			},
			error : function(e) {

			}
		});
	}
</script>

<form:form name="selectschoolform" commandName="selectschoolform">
	<form:hidden path="stateHidden"/>
	<form:hidden path="universityHidden"/>
	<input type="hidden" name="innerText" id="innerText"/>
	 <div id="contWrpr">
    <div class="block mrgnBtm">
      <h3 class="zeromrgnBtm">At Scansee we give back.</h3>
      <p>50% of Profits go to Higher Education. Tell us your college of choice:</p>
    </div>
    <div class="breadCrumb">
      <ul>
        <li class="brcIcon"><img src="../images/consumer/find_schoolIcon.png" alt="find" /></li>
        <li class="leftPdng"><strong>Select School</strong></li>
      </ul>
      <span class="rtCrnr">&nbsp;</span> </div>
    <div class="contBlks" id="">
       <table width="100%" cellspacing="0" cellpadding="0" border="0" class="brdrLsTbl topMrgn">
        <tr>
          <td colspan="4" class="cmnTitle">Have a college in mind?</td>
        </tr>
        <tr>
          <td width="19%">State:</td>
          <td colspan="3">
          	<form:select path="state" class="textboxBig"
						id="Country" onchange="loadCollege()" tabindex="1">
			<form:option value="0" label="--Select--">--Select-</form:option>
			<c:forEach items="${sessionScope.statesList}" var="s">
				<form:option value="${s.stateabbr}" label="${s.state}" />
				</c:forEach>
			</form:select></td>
        </tr>
        <tr>
          <td width="19%">College or University Name:</td>
          <td colspan="3">
              <div id="myAjax">
								<form:select  path="college" id="College" tabindex="2"
									class="textboxBig">
									<form:option  value="0">--Select--</form:option>
								</form:select>
							</div>
          </td>
        </tr>
      </table>
       <div class="btnStrip" align="right">
       <input name="Cancel3" value="Next" type="submit" class="btn btn-success"
							onclick="schoolOnsubmit()" /> 
      </div>
    </div>
  </div>
<div class="clear"></div>
</form:form>
<script type="text/javascript">
		setCollege();
</script>
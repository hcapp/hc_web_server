<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
	window.onload = function() {
	
		favLoad('${fn:escapeXml(duplicateprodtxt)}');
		
		if('false' == '${sessionScope.FromWishlist}' || '' ==  '${sessionScope.FromWishlist}'){
	
		if(null == '${requestScope.productDetails}' || '' ==  '${requestScope.productDetails}'){
			if('false' == '${sessionScope.fromback}' || '' ==  '${sessionScope.fromback}'){
			 var r = confirm("No Product Found"+"\n"+"Still Want to Add")
			 if (r == true) {
			 var searchKey = $("input#searchValue").text;
				document.shoppingList.searchKey.value= searchKey;
				document.shoppingList.action = "getProductPage.htm";
				document.shoppingList.method = "POST";
				document.shoppingList.submit();
			 }
		}
		}
	}
	}
</script>
</head>
<body>
	<form:form name="shoppingList" commandName="ShoppingListRequest">
		<div class="clear"></div>
		<div id="content" class="topMrgn">
			<div class="navBar">
				<ul>
				<c:choose>
				<c:when test="${sessionScope.moduleName eq 'WLSearch'}">
					<li><a href="wishlistdisplay.htm"><img src="../images/backBtn.png" />
					</a>
					</li>
				</c:when>
				<c:when test="${sessionScope.moduleName eq 'SLSearch'}">
					<li><a href="shoppingList.htm"><img src="../images/backBtn.png" />
					</a>
					</li>
				</c:when>
				<c:when test="${sessionScope.moduleName eq 'SLFavSearch'}">
					<li><a href="favCategoryProducts.htm"><img src="../images/backBtn.png" />
					</a>
					</li>
				</c:when>
				
				<c:otherwise>
				<li><a href="#"><img src="../images/backBtn.png" onclick="javascript:history.go(-1);" />
					</a></li>
				</c:otherwise>
				</c:choose>
					
					<li class="titletxt">Search Results</li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="mobCont noBg">
				<div class="splitDsply floatR">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="brdrLsTbl topSubBar">
						<tr>
							<!--   <td width="13%"><input type="button" class="mobBtn" value="Scan" title="Scan"/></td> -->
							<td width="23%"><input type="hidden" name="userId"
								id="userId"
								value="<c:out value="${sessionScope.loginuser.userID}"/>"></input>
								<input type="text" value="${requestScope.ShoppingList.searchValue}" name="searchValue" id="searchValue"
								class="textboxSmall focustxt" onkeypress="return searchShoppingList(event)" />
							</td>
							<td width="15%">
							
							<c:choose>
							<c:when test="${sessionScope.FromWishlist eq  true}">
			
								
								<input type="hidden" name="IsWishlst" id="IsWishlst"
												value="true">
			
			<input type="button" class="mobBtn" value="Search" onclick="searchFromWishList('IsWishlst')" title="Search"/>
           
			</c:when>
			
			<c:otherwise>
							
							
							
							
							
							<input type="button" class="mobBtn"
								value="Search" onclick="return searchShoppingList('')"  title="Search"/>
								</c:otherwise>
								</c:choose>
							</td>
							<td width="49%">&nbsp;</td>
						</tr>
					</table>
					<div class="clear"></div>
					<div class="fluidView">
						<div class="stretch">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="gnrlGrd detView htlt" id="hstryItems">
								  <col width="5%"/>
 			                      <col width="8%"/>
                                  <col width="81%"/>

                                  <col width="6%"/>
								<input type="hidden" name="selectedProductId" id="selectedProductId" value="" />
								
								<c:if test="${sessionScope.FromWishlist eq  true}">
								<c:if test="${requestScope.productDetails.productDetail eq null || empty requestScope.productDetails.productDetail}">
							
							<span id="NoDataInfo" >No Product found!</span>
								
								</c:if>
								</c:if>
								
								
								
								<c:forEach items="${requestScope.productDetails.productDetail}"
									var="prodDetails">
									<tr>
										<c:choose>

											<c:when test="${sessionScope.FromWishlist eq  true}">

												<td align="center"
													onclick="javascript:addWLProductsBySearch('${prodDetails.productId}');" >
												<img src="../images/btn_chked.png" alt="unchecked"
													title="Add" ="24" height="24" class="addItemsImg" />
												</td>
												<td><img src='${prodDetails.imagePath}' width="40" height="40" alt="Image" /></td>
												 
												<td class="wrpWord" onclick="callproductSummaryFrmSearch(${prodDetails.productId})">
												<span>${prodDetails.productName}</span>
												
												<c:choose>		  
<c:when test="${prodDetails.productShortDescription ne null || 
				!empty prodDetails.productShortDescription
	|| prodDetails.productShortDescription ne 'NotApplicable'}">
<c:out value="${prodDetails.productShortDescription}" />
</c:when>
<c:otherwise>
<c:out value="${prodDetails.productLongDescription}" />
</c:otherwise>

	</c:choose>		
							</td>
												<td><img alt="Arrow" src="../images/rightArrow.png"
						height="15" /></td>

											</c:when>

											<c:otherwise>
											<c:choose>
											<c:when test="${sessionScope.favourite eq true}">
											<td align="center" onclick="addItemsToSL(${prodDetails.productId})">
													<img src="../images/btn_chked.png" alt="unchecked"
													title="Add" ="24" height="24" class="addItemsImg" /></td>
											</c:when>
											<c:otherwise>
												<td align="center"
													onclick="javascript:addTodaySLProductsBySearch('${prodDetails.productId}');">
													<img src="../images/btn_chked.png" alt="unchecked"
													title="Add" ="24" height="24" class="addItemsImg" /></td>
													</c:otherwise>
													</c:choose>
												<td><img src='${prodDetails.imagePath}' width="40" height="40" alt="Image" /></td>	
												
												<td class="wrpWord" onclick="callproductSummaryFrmSearch(${prodDetails.productId})">
												<span>${prodDetails.productName}</span>
		
	
	
	<c:choose>		  
<c:when test="${prodDetails.productShortDescription ne null || 
				!empty prodDetails.productShortDescription
	|| prodDetails.productShortDescription ne 'NotApplicable'}">
<c:out value="${prodDetails.productShortDescription}" />
</c:when>
<c:otherwise>
<c:out value="${prodDetails.productLongDescription}" />
</c:otherwise>

	</c:choose>		
		</td>
												<td><img alt="Arrow" src="../images/rightArrow.png"
						height="15" /></td>

											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
								
							</table>
						</div>
						<input type="hidden" name="searchKey" id="searchKey" value="" />
						<input type="hidden" name="searchProd" id="searchProd" value="" />
						<input type="hidden" name="productId" id="productId" value="" />
						<input type="hidden" name="addedTo" id="addedTo" value="${requestScope.addedTo}" />
						<input type="hidden" name="fav" id="fav" value="${sessionScope.favourite}" />
					</div>
				</div>
				<%@include file="../leftmenu.jsp"%>
				<div class="clear"></div>
				<div class="pagination" style="padding-left: 210px;">
					<p>
						<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
							nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
							pageRange="${sessionScope.pagination.pageRange}"
							url="${sessionScope.pagination.url}"  enablePerPage="false"/>
					</p>
				</div>
				<div class="tabBar"></div>
			</div>
		</div>
	</form:form>
</body>
</html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {

	<c:choose>
									<c:when test="${sessionScope.shopplistprods ne null}">

										<c:forEach
											items="${shopplistprods.cartProducts.categoryDetails}"
											var="shopCatName">
<c:forEach items="${shopCatName.productDetails}"
												var="shopProd">
											
	<c:if test ="${shopProd.shopCartItem eq 1}">
	$("#shpngList td").find('a.strike-out').parents('tr').find('img[name="grpChkbk"]').attr("src","/ScanSeeWeb/images/consumer/Chkd.png");
	</c:if>
	
	
</c:forEach>	
</c:forEach>	
</c:when>
</c:choose>
});

</script>

<script type="text/javascript">
<!--Below script method is used for pagination record count implementation. -->
function getPerPgaVal() {

var selValue = $('#selPerPage :selected').val();
	document.ShoppingListHome.recordCount.value = selValue;
	//Call the method which populates grid values
	shoppinglistdisplay('history');

}
	//<!--Below script method is used for pagination implementation. -->
function callNextPage(pagenumber, url) {
	var selValue = $('#selPerPage :selected').val();
document.paginationForm.screenName.value = 'history';
	document.paginationForm.pageNumber.value = pagenumber;
	document.paginationForm.recordCount.value = selValue;
	document.paginationForm.pageFlag.value = "true";
	document.paginationForm.action = url;
	document.paginationForm.method = "POST";
	document.paginationForm.submit();
}
	function shopplistprodsearch(event) {

		var searchKey = document.ShoppingListHome.searchkey.value;
		var screenName = "search";
		document.ShoppingListHome.screenName.value = screenName;
		if (event && event.which == 13) {

			if (searchKey == '') {
				alert("Note: Please Enter keyword to search!");
				return true;

			}

			document.ShoppingListHome.action = "consslsearchprods.htm";
			document.ShoppingListHome.method = "POST";
			document.ShoppingListHome.submit();
		} else if (event == '') {

			if (searchKey == '') {
				alert("Note: Please Enter keyword to search!");
				return true;

			}

			document.ShoppingListHome.action = "consslsearchprods.htm";
			document.ShoppingListHome.method = "POST";
			document.ShoppingListHome.submit();

		} else {
			return true;
		}

	}
</script>

<body onresize="resizeDoc();" onload="resizeDoc();">

	<form name="ShoppingListHome" commandName="ShoppingListHome">
		<input type="hidden" name="productId" id="productId" value="" /> <input
			type="hidden" name="redirectUrl" id="redirectUrl" value="" /> <input
			type="hidden" name="recordCount" id="recordCount" value="" /> <input
			type="hidden" name="screenName" id="screenName" value="" /> <input
			type="hidden" name="slscreenName" id="slscreenName"
			value="${requestScope.slscreenName}" />
			
		<div id="contWrpr">
			<div class="spliView" id="shpngLst">
				<div class="splitL">
					<div class="breadCrumb">
						<!--  <ul>
            <li class=""></li>
          </ul>-->
						<span class="ltCrnr">&nbsp;</span> <span class="rtCrnr">&nbsp;</span>
					</div>
					<div class="splitCont relative">
						<div class="tabdPnl">
							<ul class="tabdCntrl">

								<li><a href="consdisplayfavprods.htm"
									class="favorites"><img
										src="/ScanSeeWeb/images/consumer/faviIcon.png" alt="favorites"
										align="absmiddle" /> Favorites</a>
								</li>
								<li><a href="conssearchhome.htm" class="search"><img
										src="/ScanSeeWeb/images/consumer/srchIcon.png" alt="search"
										align="absmiddle" /> Search</a>
								</li>
								<li class="active"><a
									href="consslhistory.htm" class="history"><img
										src="/ScanSeeWeb/images/consumer/hstryIcon.png" alt="history"
										align="absmiddle" /> History</a>
								</li>


							</ul>
						</div>


						<!-- FOR DISPLAYING SHOPPING LIST FAVORITE PRODUCTS -->
						<div class="sctmScroll">




							<!-- FOR DISPLAYING SHOPPING LIST HISTORY PRODUCTS -->

							<div class="tabbedPnl history">

								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="brdrTbl" id="slhistory">
									<c:choose>
										<c:when test="${sessionScope.slhistoryprods ne null}">

											<c:forEach
												items="${slhistoryprods.cartProducts.categoryDetails}"
												var="slHistoryCat">

												<tr name="${slHistoryCat.parentCategoryId}" class="subHdr">
													<td colspan="4">${slHistoryCat.parentCategoryName}</td>
												</tr>

												<c:forEach items="${slHistoryCat.productDetails}"
													var="slHistyProd">
													<tr name="${slHistoryCat.parentCategoryId}">
														<td width="12%" align="center"><input
															name="hstryChkbx" type="checkbox" class="hstryChkbx" value="${slHistyProd.userProductId}," />
														</td>
														<td width="14%" class="img"><img
															src="${slHistyProd.productImagePath}"
															onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
															alt="product1" width="150" height="150"
															title="${slHistyProd.productName}" />
														</td>
														<td width="74%"><p>
																<a
																	href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${slHistyProd.productId}&key2=1&page=SL"
																	class="title shopplsthistorytrim active"
																	title="${slHistyProd.productName}">${slHistyProd.productName}

																</a>
															</p>
															<p class="title shopplsthistorytrim active"
																title="${slHistyProd.productShortDescription}">${slHistyProd.productShortDescription}

															</p>
														</td>
														<c:if test="${slHistyProd.favProductFlag eq 1}">

															<td><img
																src="/ScanSeeWeb/images/consumer/favtabIcon.png"
																alt="coupon" width="26" height="26" />
															</td>
														</c:if>
													</tr>



												</c:forEach>

											</c:forEach>


										</c:when>
										<c:otherwise>
									<tr><td class="zeroPdng">
		 <img src="/ScanSeeWeb/images/consumer/common_noneAvail.png" alt="common_noneAvail" width="316" height="401" />
		  </td></tr>		

										</c:otherwise>

									</c:choose>

								</table>

							</div>

							<ul class="subCntrl">
								<li><a href="javascript:void(0);" onclick="addhistoryprodtosl('List','history')"><span class="list-icon"></span>List</a>
								</li>
								<li><a href="javascript:void(0);" onclick="addhistoryprodtosl('Favorites','history')"><span class="fav-icon"></span>Favorites</a>
								</li>
								<li><a href="javascript:void(0);" onclick="addhistoryprodtosl('List,Favorites','history')"><span class="both-icon"></span>Both</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="clear"></div>
					<ul class="actnCntrl">
					<c:if test="${sessionScope.slhistoryprods ne null || !empty sessionScope.slhistoryprods}">
						<li class="tabStretch"><a href="javascript:void(0);"
							class="add-btn"><span class="actn-add"></span>Add</a>
						</li>
						</c:if>
						<!--<li><a href="#" class="del-btn" onclick="selectCheckBox()"><span class="actn-del"></span>Delete</a></li>-->
					</ul>
				</div>


				<!-- FOR DISPLAYING TODAY SHOPPING LIST PRODUCTS -->

					<div class="splitR">
					<div class="breadCrumb">
						<ul>
							<li class="brcIcon"><img
								src="/ScanSeeWeb/images/consumer/sl_bcIcon.png" alt="find" />
							</li>
							<li class="active">Shopping List</li>
						</ul>
						<span class="rtCrnr">&nbsp;</span>
					</div>
					<div class="splitCont">
						<div class="subHdr-control">

							<h3>
								Total Item's in list:<i id="ttlCnt"></i>Selected Item:
								
								<c:choose>
								<c:when test="${sessionScope.shopcartcnt gt 0}">
								<i
									id="slctCnt">${sessionScope.shopcartcnt}</i>
								
								</c:when>
								<c:otherwise>
								<i
									id="slctCnt">0</i>
								</c:otherwise>
								</c:choose>					
							
							<span> <a class="icon-bg" href="javascript:void(0);"
									id="check"><img width="20" height="20" alt="check"
										src="/ScanSeeWeb/images/consumer/check.png" title="Checkout"
										onclick="checkoutslprod('history')"> </a><i class="print"
									style="display: none;">Print Coupon</i><i class="Trash"
									style="display: none;"> Delete Coupon</i><a class="icon-bg"
									href="javascript:void(0);"><img width="20" height="20" alt="print"
										src="/ScanSeeWeb/images/consumer/print.png" title="Print" onclick="printshoplistProductInfo()">
								</a><a class="icon-bg" href="javascript:void(0);" onclick="deletetodaylstprod('history')">
										<img width="20" height="20" alt="Trash"
										src="/ScanSeeWeb/images/consumer/trash.png" title="Delete">
								</a>
								</span>
							</h3>


						</div>

						<div class="sctmScroll">

							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="brdrLsTbl rowStrip cstmChkbx" id="shpngList">


								<c:choose>
									<c:when test="${sessionScope.shopplistprods.cartProducts ne null}">

										<c:forEach
											items="${shopplistprods.cartProducts.categoryDetails}"
											var="shopCatName">

											<tr name="${shopCatName.parentCategoryId}" class="subHdr">
												<td colspan="5">${shopCatName.parentCategoryName}</td>
											</tr>

											<c:forEach items="${shopCatName.productDetails}"
												var="shopProd">


												<tr name="${shopCatName.parentCategoryId}">
													<td align="center"><div class="tglBx">
															<label>
															<c:choose>
															<c:when test ="${shopProd.shopCartItem eq 1}">
															
															<input type="checkbox" id="ads_Checkbox"
																name="chkbxGrp" class="chkbxGrp" checked="checked"
																value="${shopProd.userProductId},"
																onclick="moveprodtobasket(${shopProd.userProductId},'history')" 
																/>
															</c:when>
															<c:otherwise>
																				
															
															<input type="checkbox" id="ads_Checkbox"
																name="chkbxGrp" class="chkbxGrp"
																value="${shopProd.userProductId},"
																onclick="moveprodtobasket(${shopProd.userProductId},'history')"  />
																</c:otherwise>
																</c:choose>
															</label>
														</div>
													</td>
													<td class="imgDsply"><img
														src="${shopProd.productImagePath }"
														onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
														alt="product1" width="150" height="150"
														title="${shopProd.productName}" />
													</td>

													<td><p>
															<c:choose>
																<c:when test="${shopProd.shopCartItem eq 1}">
																	<a
																		href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${shopProd.productId}&key2=1&page=SL"
																		class="title productTrim active strike-out"
																		title="${shopProd.productName}">${shopProd.productName}
																	
																</c:when>
																<c:otherwise>
																	<a
																		href="/ScanSeeWeb/shopper/consfindprodinfo.htm?key1=${shopProd.productId}&key2=1&page=SL"
																		class="title productTrim active"
																		title="${shopProd.productName}">${shopProd.productName}
																	
																</c:otherwise>

															</c:choose>

															<c:if test="${shopProd.shopCartItem eq 1}">

															</c:if>

															</a>
														</p>
														<p class="title productTrim active"
															title="${shopProd.productShortDescription}">${shopProd.productShortDescription}

														</p>
													</td>

													<td><c:if test="${shopProd.CLRFlag eq 1}">

															<img src="/ScanSeeWeb/images/consumer/cpntabIcon.png"
																alt="coupon" width="26" height="26" />
														</c:if></td>
													<td><c:if test="${shopProd.favProductFlag eq 1}">

															<img src="/ScanSeeWeb/images/consumer/favtabIcon.png"
																alt="coupon" width="26" height="26" />
														</c:if></td>
												</tr>

											</c:forEach>


										</c:forEach>

									</c:when>
									<c:otherwise>
										<tr>
											<td class="zeroPdng"><img
												src="/ScanSeeWeb/images/consumer/noneAvail_medium.png"
												alt="common_noneAvail" width="613" height="401" /></td>
										</tr>
									</c:otherwise>
								</c:choose>
							</table>
						</div>

					</div>

					<ul class="infoCntrl">

						<li><span class="favIcon">&nbsp;</span> Favorites</li>
						<li><span class="cpnIcon">&nbsp;</span>Coupon Available</li>


					</ul>

					<c:if test="${sessionScope.shopplistprods ne null}">
						<div class="pagination mrgnTopSmall">

							<table width="100%" border="0" cellpadding="0" cellspacing="0"
								class="noBrdr" id="perpage">
								<tr>
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" enablePerPage="true" />

								</tr>

							</table>

						</div>

					</c:if>

				</div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="clear"></div>
	</form>
</body>
</html>
	
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<body onload="resizeDoc();" onresize="resizeDoc();">
<form:form name="shoppernotes" commandName="shoppernotes">
  <div id="content" class="topMrgn">
    <div class="navBar">
      <ul>
      <c:choose>
      <c:when test="${requestScope.fromSL eq true}">
        <li><a href="shoppingList.htm"><img src="../images/backBtn.png" /></a></li>
        </c:when>
        <c:when test="${requestScope.fromSLFAV eq true}">
        <li><a href="favCategoryProducts.htm"><img src="../images/backBtn.png" /></a></li>
        </c:when>
        <c:otherwise>
        <li><a href="shoppingList.htm"><img src="../images/backBtn.png" /></a></li>
        </c:otherwise>
        </c:choose>
        <li class="titletxt">History</li>
        <!--<li class="floatR mrgnRt"><a href="index.html"><img src="images/logoutBtn.png" alt="logout" /></a></li>-->
      </ul>
    </div>
    <div class="clear"></div>
    <div class="mobCont noBg">
      <div class="splitDsply floatR">

      <c:if test="${!empty requestScope.shoppingListCategoryInfo.categoryInfolst}">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
          <tr>
            <td width="19%">Add selected itmes to:</td>
            <td width="22%"><select name="select" class="textboxSmall" id="addItemsTo" >
              <option>Please Select</option>
              <option value="List">Add to List</option>
              <option value="Favorites">Add to Favorites</option>
              <option value="List,Favorites">Add to Both</option>
            </select></td>
            <td width="59%" onclick="outputSelected(${fn:length(requestScope.shoppingListCategoryInfo.categoryInfolst)});"><a href="#"><img src="../images/btn_chked.png" alt="AddBtn" title="Please Select"="24" height="24" class="addItemsImg"/></a></td>
          </tr>
        </table>
        </c:if>
		 <c:if test="${empty requestScope.shoppingListCategoryInfo.categoryInfolst}">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
         <thead>
              <tr class="noHvr">
                <th width="13%" align="right"><img src="../images/alert.png" alt="alert" width="16" height="16" />
				</th>
                <th width="87%" align="left" class="wrpWord">   No Records found</th>
              </tr>
            </thead>
        </table>
		 </c:if>
        <div class="clear"></div>
        <div class="fluidView">
          <div class="stretch">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd mobGrd htlt" id="hstryItems">
             <input type="hidden" name="productId" id="productId" value="" />
             <input type="hidden" name="fromSLHistory" id="fromSLHistory" value="SLHistory" />
             <c:forEach items="${requestScope.shoppingListCategoryInfo.categoryInfolst}" var="listDetails">
             
              <tr name="${listDetails.parentCategoryName}"  class="noHvr">
                <td style="width:100%" colspan="5" class="mobSubTitle"><c:out value ="${listDetails.parentCategoryName}"/></td>
              </tr>
              
              
			  <c:forEach items="${listDetails.productDetails}" var="listproductDetails">
			  <tr name="${listDetails.parentCategoryName}">
                <td align="center" width="10%">
                <input type="checkbox" name="check" value="${listproductDetails.productId},${listproductDetails.userProductId}" id="check" name="check"/></td>
                <td class="wrpWord" onclick="callproductSummarySCH(${listproductDetails.productId})"><c:out value="${listproductDetails.productName}" /></td>
                <td>&nbsp;</td>
                <td><img src="${listproductDetails.productImagePath}" onerror="this.src = '/ScanSeeWeb/images/blankImage.gif'"  width="50" height="50" /></td>
                <c:choose>
                <c:when test="${listproductDetails.starFlag eq 1}">
                <td><img src="../images/greenStar.png" alt="favorites" width="26" height="24" id="star${listproductDetails.productId},${listproductDetails.userProductId}" name="star${listproductDetails.productId},${listproductDetails.userProductId}"/></td>
                </c:when>
                <c:otherwise>
                 <td><img src="/ScanSeeWeb/images/blankImage.gif" alt="" width="26" height="24" id="star${listproductDetails.productId},${listproductDetails.userProductId}" name="star${listproductDetails.productId},${listproductDetails.userProductId}"/></td>
                </c:otherwise>
                </c:choose>
                <td>&nbsp;</td>
				</c:forEach>
              </tr>
			  </c:forEach>
             
            </table>
          </div>
          </div>
      </div>
      <%@include file="../leftmenu.jsp"%>
      <div class="clear"></div>
      <div class="pagination">
				<p><page:pageTag currentPage="${sessionScope.pagination.currentPage}"
					nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
					pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" enablePerPage="false"/>
				</p>
			</div>
    </div>
    <div class="tabBar">
    </div>
  </div>
</form:form>
</body>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="/ScanSeeWeb/scripts/shopper/jquery.MetaData.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/jquery.rating.js"
	type="text/javascript"></script>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcoZEmDQJTP5iDa_aZIaHS5H9xdAdWG6w&sensor=true"></script>
<script type="text/javascript">

//window.onload = function() {

 //var vCityID = '${requestScope.radius}';
 
 //var sel = document.getElementById("raiudTL");
 //for ( var i = 0; i < sel.options.length; i++) {
 // if (sel.options[i].value == vCityID) {
 //  sel.options[i].selected = true;
 //  return;
//  }
// }
//}

$(function(){
 
  $('div.rating-cancel').click(function() { 
  //var curvalue = $('div.test').text();
	$('div.test').html("");
	deleteUserRating('${requestScope.productRatingReview.userRatingInfo.productId}');
   });

	  var myOptions = {center: new google.maps.LatLng(-34.397, 150.644),
				zoom: 8,          
				mapTypeId: google.maps.MapTypeId.ROADMAP};
	var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
	
	var marker = new google.maps.Marker({
	map: map,
	draggable: true,
	position: map.getCenter()
	});
	var location = map.getCenter();
	var lat = location.lat();
	var longt = location.lng();
	
	var lattitude = '${sessionScope.objAreaInfoVO.lattitude}';
	var longitude = '${sessionScope.objAreaInfoVO.longitude}';
	
	if(lattitude != "" || longitude != ""){
	var myOptions = {center: new google.maps.LatLng(lattitude,longitude),
	zoom: 8,          
	mapTypeId: google.maps.MapTypeId.ROADMAP};
	var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
	var marker = new google.maps.Marker({
	map: map,
	draggable: true,
	position: map.getCenter()
	});
	}
	if('${sessionScope.objAreaInfoVO.zipCode}' != ""){
	
	var lattitude = '${sessionScope.zipcodeLat}';
	var longitude = '${sessionScope.zipcodeLong}';
	
	if(lattitude != "" || longitude != ""){
		var myOptions = {center: new google.maps.LatLng(lattitude,longitude),
		zoom: 8,          
		mapTypeId: google.maps.MapTypeId.ROADMAP};
		var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		var marker = new google.maps.Marker({
		map: map,
		draggable: true,
		position: map.getCenter()
		});
	}
	}
});
</script>
</head>
<body onload="load()">
	<script type="text/javascript">
function twitterShare(productId){

	
	$.ajaxSetup({cache:false});
	$.ajax({
	type : "GET",
	url : "/ScanSeeWeb/shopper/twitterShareProductInfo.htm",
	data : {
		'productId'  : productId
	},
	success : function(response) {
		
	},
	error : function(e) {
		alert('Error Occured');
	}
});
var tweetText;
var shareURL='${tweetterShareURL}';

	var prodName='${productNameReview}';
	
	tweetText='Great find at ScanSee!  '+prodName+"  Visit:"+shareURL

	var tweeturl = 'http://twitter.com/share?text='+escape(tweetText);

	window.open(tweeturl);	

	
	}
</script>
	<form:form name="productsummary" commandName="productsummary">
		<div id="content" class="topMrgn">
			<div align="center" id="airports"></div>
			<input type="hidden" name="latitude" id="latitude" value="" /> <input
				type="hidden" name="longitude" id="longitude" value="" />
			<div class="navBar">
				<ul>
					<c:set var="type" value="other" />
					<c:choose>
						<c:when
							test="${requestScope.message ne null && !empty requestScope.message}">
							<c:choose>
								<c:when
									test="${requestScope.message eq 'Media Details are not available.'}">
									<li><a href="#"><img src="../images/backBtn.png"
											onclick="callProductInfo(${requestScope.productId},${sessionScope.retailId})" />
									</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="#"><img src="../images/backBtn.png"
											onclick="javascript:history.go(-1);" /> </a></li>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${requestScope.fromwlratereview eq true }">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:callTLWishlstLocalStores(${requestScope.productId})" />
							</a></li>
						</c:when>
						<c:when test="${requestScope.fromwlonlineratereview eq true}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:callTLWishlstOnlineStores(${requestScope.productId})" />
							</a></li>
						</c:when>
						<c:when
							test="${requestScope.productRatingReview ne null && !empty requestScope.productRatingReview }">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:callProdSummaryBack(${requestScope.ratingproductid})" />
							</a></li>
						</c:when>
						<c:when
							test="${requestScope.findNearByDetails.findNearByDetail ne null && !empty requestScope.findNearByDetails.findNearByDetail}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:callProdSummaryBack(${requestScope.productId})" />
							</a></li>
						</c:when>
						<c:when
							test="${requestScope.productinfo ne null && !empty requestScope.productinfo}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:callProdSummaryBack(${requestScope.productId})" />
							</a></li>
						</c:when>
						<c:when
							test="${requestScope.discountinfo ne null && !empty requestScope.discountinfo}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:callProdSummaryBack(${requestScope.productId})" " />
							</a></li>
						</c:when>
						<c:when
							test="${requestScope.OnlineStoresList ne null && !empty requestScope.OnlineStoresList}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:history.go(-1);" /> </a></li>
						</c:when>
						<c:when
							test="${requestScope.coupondetails ne null || requestScope.loyaltydetails ne null || requestScope.rebatedetails ne null}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="javascript:getDiscountBack(${sessionScope.productId},${sessionScope.retailID})" />
							</a></li>
						</c:when>
						<c:when
							test="${requestScope.mediaDetails ne null && !empty requestScope.mediaDetails }">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="callProductInfo(${requestScope.productId},${sessionScope.retailId})" />
							</a></li>
						</c:when>
						<c:when test="${requestScope.mediaType eq type}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="callProductInfo(${requestScope.productId},${sessionScope.retailId})" />
							</a></li>
						</c:when>
						<c:when
							test="${requestScope.retailer ne null && !empty requestScope.retailer }">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="callNearBy(${requestScope.productId})" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'SLMain'}">
							<li><a href="shoppingList.htm"><img
									src="../images/backBtn.png" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'SLFAV'}">
							<li><a href="favCategoryProducts.htm"><img
									src="../images/backBtn.png" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'SLHistory'}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="onSubmitSLHistoryBack();" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'WLMain'}">
							<li><a href="wishlistdisplay.htm"><img
									src="../images/backBtn.png" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'WLHistory'}">
							<li><a href="wishlisthistorydisplay.htm"><img
									src="../images/backBtn.png" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'WLSearch'}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="searchWishListBack('IsWishlst')" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'SLSearch'}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="return searchSLBack()" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'SLFavSearch'}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="return searchSLBack()" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'fromFind'}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="callFindSearch();" /> </a></li>
						</c:when>
						<c:when test="${sessionScope.moduleName eq 'fromscannow'}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="searchScannowBack();" /> </a></li>
						</c:when>
						<c:when
							test="${requestScope.productSummaryVO ne null && !empty requestScope.productSummaryVO && (requestScope.fromSearchprod ne true || requestScope.fromSearchprod eq '') }">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="callProdDetailsBack('${sessionScope.productSummaryParametersVO.retailLocationID}',${sessionScope.productSummaryParametersVO.retailerId},'${fn:replace(sessionScope.productSummaryParametersVO.retailerName, '\'', '%27')}')" />
							</a></li>
						</c:when>
						<c:when test="${sessionScope.prodDet eq true}">
							<li><a href="#"><img src="../images/backBtn.png"
									onclick="searchProductList('')" /> </a></li>
						</c:when>

					</c:choose>
					<c:choose>
						<c:when
							test="${requestScope.prodcutDetails ne null && !empty requestScope.prodcutDetails}">
							<li class="titletxt"><c:out
									value="${requestScope.retailerName}" />
							</li>
						</c:when>
						<c:when
							test="${requestScope.productSummaryVO ne null && !empty requestScope.productSummaryVO}">
							<li class="titletxt"><c:out
									value="${requestScope.productSummaryVO.productDetail.productName}" />
							</li>
						</c:when>
						<c:when
							test="${requestScope.findNearByDetails.findNearByDetail ne null && !empty requestScope.findNearByDetails.findNearByDetail}">
							<li class="titletxt">Local Stores</li>
						</c:when>
						<c:when
							test="${requestScope.retailer ne null && !empty requestScope.retailer}">
							<li class="titletxt"><c:out
									value="${requestScope.retailer.retailName}" />
							</li>
						</c:when>
						<c:when
							test="${requestScope.productinfo ne null && !empty requestScope.productinfo}">
							<li class="titletxt">Product Details</li>
						</c:when>
						<c:when
							test="${requestScope.productRatingReview ne null && !empty requestScope.productRatingReview}">
							<li class="titletxt">Rate Share Reviews</li>
						</c:when>
						<c:when
							test="${requestScope.discountinfo ne null && !empty requestScope.discountinfo}">
							<li class="titletxt">Coupons</li>
						</c:when>
						<c:when
							test="${requestScope.OnlineStoresList ne null && !empty requestScope.OnlineStoresList}">
							<li class="titletxt">Online Stores</li>
						</c:when>
						<c:when test="${requestScope.coupondetails ne null}">
							<li class="titletxt">${requestScope.coupondetails.couponInfo.couponName}</li>
						</c:when>
						<c:when test="${requestScope.loyaltydetails ne null}">
							<li class="titletxt">${requestScope.loyaltydetails.loyaltyInfo.loyaltyDealName}</li>
						</c:when>
						<c:when test="${requestScope.rebatedetails ne null}">
							<li class="titletxt">${requestScope.rebatedetails.rebateInfo.rebateName}</li>
						</c:when>
						<c:when test="${requestScope.mediaType eq 'other'}">
							<li class="titletxt">Product Other Info</li>
						</c:when>
						<c:when test="${requestScope.mediaType eq 'audio'}">
							<li class="titletxt">Product Audio</li>
						</c:when>
						<c:when test="${requestScope.mediaType eq 'video'}">
							<li class="titletxt">Product Video</li>
						</c:when>
						<c:when
							test="${requestScope.mediaDetails ne null && !empty requestScope.mediaDetails }">
							<li class="titletxt">Media Details</li>
						</c:when>

						<c:otherwise>
							<li class="titletxt" style="padding-left: 50px;">This
								location</li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="mobCont">
				<div class="mobDsply floatR relatvDiv">
					<c:choose>
						<c:when
							test="${requestScope.productSummaryVO ne null && !empty requestScope.productSummaryVO}">
							<%@include file="thislocation_prodsummary.jsp"%>
						</c:when>
						<c:when
							test="${requestScope.findNearByDetails.findNearByDetail ne null && !empty requestScope.findNearByDetails.findNearByDetail}">
							<%@include file="thislocation_nearby.jsp"%>
						</c:when>
						<c:when
							test="${requestScope.retailer ne null && !empty requestScope.retailer}">
							<%@include file="thislocation_nearbyretailer.jsp"%>
						</c:when>
						<c:when
							test="${requestScope.productinfo ne null && !empty requestScope.productinfo}">
							<%@include file="thislocation_prodinfo.jsp"%>
						</c:when>
						<c:when
							test="${requestScope.productRatingReview ne null && !empty requestScope.productRatingReview}">
							<%@include file="thislocation_prodreview.jsp"%>
						</c:when>
						<c:when
							test="${requestScope.discountinfo ne null && !empty requestScope.discountinfo}">
							<%@include file="discount.jsp"%>
						</c:when>
						<c:when
							test="${requestScope.OnlineStoresList ne null && !empty requestScope.OnlineStoresList}">
							<%@include file="onlineStoreInfo.jsp"%>
						</c:when>
						<c:when
							test="${requestScope.mediaDetails ne null && !empty requestScope.mediaDetails }">
							<%@include file="mediadetails.jsp"%>
						</c:when>
						<c:when
							test="${requestScope.coupondetails ne null || requestScope.loyaltydetails ne null || requestScope.rebatedetails ne null}">
							<%@include file="clrdetails.jsp"%>
						</c:when>
						<c:otherwise>
							<%@include file="thislocation_message.jsp"%>
						</c:otherwise>
					</c:choose>
					<input type="hidden" name="retailerId" id="retailerId" value="" />
					<input type="hidden" name="productId" id="productId" value="" /> <input
						type="hidden" name="regularPrice" id="regularPrice" value="" /> <input
						type="hidden" name="salePrice" id="salePrice" value="" /> <input
						type="hidden" name="productPrice" id="productPrice" value="" /> <input
						type="hidden" name="distance" id="distance" value="" /> <input
						type="hidden" name="retailLocationID" id="retailLocationID"
						value="" /> <input type="hidden" name="currentRating"
						id="currentRating" value="" /> <input type="hidden"
						name="couponId" id="couponId" value="" /> <input type="hidden"
						name="usage" id="usage" value="" /> <input type="hidden"
						name="tlFind" id="tlFind" value="" /> <input type="hidden"
						name="retailerName" id="retailerName" value="" /> <input
						type="hidden" name="mediaType" id="mediaType" value="" /> <input
						type="hidden" name="loyaltyDealId" id="loyaltyDealId" value="" />
					<input type="hidden" name="flag" id="flag" value="" /> <input
						type="hidden" name="fromThisLocation" id="fromThisLocation"
						value="true" /> <input type="hidden" name="msg" id="msg"
						value="<c:out value="${requestScope.message}" />" />
					<c:choose>
						<c:when test="${requestScope.fromFind eq true}">
						</c:when>
						<c:otherwise>
							<div class="optnPnl">
								<div>
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="brdrLsTbl">
										<tr>
											<td width="70%">City Name:</td>
											<td width="20%"><input type="text" class="textboxDate"
												name="cityNameTL" value='${requestScope.cityNameTL}' "/></td>
											<td width="5%"><input type="button"
												class="mobBtn LstRtlr" value="Go"
												onclick="showAddress(''); return false" title="Go" />
											</td>
										</tr>
									</table>
								</div>
								<div align="center" id="map_canvas"
									style="width: 260px; height: 260px"></div>
								<table width="98%" border="0" cellspacing="0" cellpadding="0"
									class="brdrLsTbl">
									<c:choose>
										<c:when
											test="${requestScope.fromwlratereview eq true || requestScope.fromwlonlineratereview eq true}">
											<tr>
												<td width="40%">Zip Code:</td>
												<td width="70%"><input type="text" class="textboxSmall"
													onkeypress="return isNumberKey(event)" name="zipcodeWL"
													value="${sessionScope.objAreaInfoVO.zipCode}" /></td>
											</tr>
											<tr>
												<td colspan="2"><input type="button"
													class="mobBtn LstRtlr" value="Refresh"
													onclick="ValidateTLWishList();" title="Refresh" /></td>
											</tr>
										</c:when>
										<c:otherwise>
											<tr>
												<td width="40%">Zip Code:</td>
												<td width="70%"><input type="text" class="textboxSmall"
													onkeypress="return isNumberKey(event)" maxlength="10"
													name="zipcodeTL" value="${sessionScope.zipcode}" /></td>
											</tr><!--

											<tr>
												<td>Radius:</td>
												<td><select class="textboxSmall" name="raiudTL" id="raiudTL">
														<option value="0" label="">Select Radius</option>
														<c:forEach items="${sessionScope.radiuslst}" var="rad">
															<c:choose>
																<c:when test="${sessionScope.radius eq rad.distInMiles}">
																	<option value="${rad.distInMiles}" label=""
																		selected="selected">${rad.distInMiles} miles</option>
																</c:when>
																<c:otherwise>
																	<option value="${rad.distInMiles}" label="">${rad.distInMiles}
																		miles</option>
																</c:otherwise>
															</c:choose>
														</c:forEach>
												</select></td>
											</tr>
											--><!--<tr>
								<td>Radius:</td>
								<td><select class="textboxSmall" name="raiudTL">
										<option id="R1" value="" >Select Radius</option>
										<option id="R2" value="5" <c:if test="${requestScope.radius eq 5}">selected="selected"</c:if> >5</option>
										<option id="R3" value="10" <c:if test="${requestScope.radius eq 10}">selected="selected"</c:if> >10</option>
										<option id="R4" value="15" <c:if test="${requestScope.radius eq 15}">selected="selected"</c:if> >15</option>
										<option id="R5" value="20" <c:if test="${requestScope.radius eq 20}">selected="selected"</c:if> >20</option>
								</select>
								</td>
								</tr>
								-->
											<tr>
												<td colspan="2"><input type="button"
													class="mobBtn LstRtlr" value="List Retailers"
													onclick="ValidateRetailersList('');" title="List Retailers" />
												</td>
											</tr>
											<!--<tr>
												<td colspan="2">If you did not find the retailers you
													were looking for, Please change radius.</td>
											</tr>
										--></c:otherwise>
									</c:choose>
								</table>
							</div>
						</c:otherwise>
					</c:choose>
					<div class="clear"></div>
				</div>
				<%@include file="leftmenu.jsp"%>
			</div>
			<div class="clear"></div>

			<div class="tabBar">
				<c:if
					test="${requestScope.productSummaryVO ne null && !empty requestScope.productSummaryVO}">
					<ul id="sLTab">

						<c:choose>
							<c:when
								test="${sessionScope.fromSLFavSearch eq 'true' || sessionScope.FromSLSearch eq 'true' || sessionScope.fromSL eq 'SLMain' || sessionScope.fromSLFAV eq 'SLFAV'}">
								<li><a href="http://www.scansee.com" target="_blank"><img
										src="../images/tab_btn_up_about.png" alt="About" />
								</a>
								</li>
							</c:when>
							<c:otherwise>
								<li><a href="#"><img
										src="../images/tab_up_add_spngLst.png" alt="shoppinglist"
										onclick="addShopingList(${requestScope.productId});" />
								</a>
								</li>
							</c:otherwise>
						</c:choose>

						<li><a href="#"><img
								src="../images/tab_btn_down_wishlist.png" alt="wishlist"
								onclick="addWishList(${requestScope.productId})" /> </a>
						</li>
						<li><a href="preferencesmain.htm"><img
								src="../images/tab_btn_up_preferences.png" alt="Preferences" />
						</a>
						</li>
						<li><a href="#"
							onclick="callReviews(${requestScope.productId})"><img
								src="../images/tab_btn_up_rateshare.png" alt="RateShare" />
						</a>
						</li>
					</ul>
				</c:if>
				<c:if
					test="${requestScope.productRatingReview ne null && !empty requestScope.productRatingReview}">
					<ul id="sLTab">
						<li><a href="#"><img
								src="../images/tab_btn_up_facebook.png" alt="Facebook"
								onclick="fbShareProductInfo(${requestScope.productId})" />
						</a>
						</li>
						<li><a href="#"><img
								src="../images/tab_btn_up_twitter.png" alt="twitter"
								onclick="twitterShare(${requestScope.productId});" />
						</a>
						</li>
						<li><a href="#"><img
								src="../images/tab_btn_up_sendmail.png" alt="Mail"
								onclick="openEMailShareProdPopUpGeneral(${requestScope.productId})" />
						</a>
						</li>
						<li><a href="#"><img
								src="../images/tab_btn_up_sendtxt.png" alt="SendText" />
						</a>
						</li>
					</ul>
				</c:if>
				<c:if
					test="${requestScope.findNearByDetails.findNearByDetail ne null && !empty requestScope.findNearByDetails.findNearByDetail}">
					<ul id="sLTab">
						<li><a href="http://www.scansee.com" target="_blank"><img
								src="../images/tab_btn_up_about.png" alt="About" />
						</a>
						</li>
						<li><a href="#"><img
								src="../images/tab_btn_up_goTowishlist.png" alt="wishlist"
								onclick="addWishList(${requestScope.productId})">
						</a>
						</li>
						<li><a href="preferencesmain.htm"><img
								src="../images/tab_btn_up_preferences.png" alt="Preferences" />
						</a>
						</li>
						<li><a href="#"
							onclick="callReviews(${requestScope.productId})"><img
								src="../images/tab_btn_up_rateshare.png" alt="RateShare" />
						</a>
						</li>
					</ul>
				</c:if>
				<c:if
					test="${requestScope.retailer ne null && !empty requestScope.retailer}">
					<ul id="sLTab">
						<li><a href="http://www.scansee.com" target="_blank"><img
								src="../images/tab_btn_up_about.png" alt="About" />
						</a>
						</li>
						<li><a href="#"><img
								src="../images/tab_btn_up_goTowishlist.png" alt="wishlist"
								onclick="addWishList(${requestScope.productId})">
						</a>
						</li>
						<li><a href="preferencesmain.htm"><img
								src="../images/tab_btn_up_preferences.png" alt="Preferences" />
						</a>
						</li>
						<li><a href="#"
							onclick="callReviews(${requestScope.productId})"><img
								src="../images/tab_btn_up_rateshare.png" alt="RateShare" />
						</a>
						</li>
					</ul>
				</c:if>
				<c:if test="${requestScope.coupondetails ne null}">
					<ul id="sLTab">
						<li><a href="#"><img
								src="../images/tab_btn_up_wishlist.png" title="Wish List"
								width="80" height="50"
								onclick="addWishListWithProduct('${requestScope.productIdWithComma}')" />
						</a>
						</li>
						<c:if test="${requestScope.addedFlag == '0'}">
							<li id="myAjax"><a href="#"><img
									src="../images/tab_up_add_cpns.png" width="80" height="50"
									onclick="addCLR(${requestScope.coupondetails.couponInfo.couponId},'coupon','${requestScope.addedFlag}')" />
							</a>
							</li>
						</c:if>
						<c:if
							test="${requestScope.addedFlag == '1' || requestScope.addedFlag eq ''}">
							<li class="noHvr"><a><img
									src="../images/tab_up_add_cpns.png" width="80" height="50" />
							</a>
							</li>
						</c:if>
						<li><a href="coupongallery.htm"><img
								src="../images/tab_btn_up_cpnGlry.png" title="Gallery"
								width="80" height="50" />
						</a>
						</li>
						<li><a href="#" id="actSht"><img
								src="../images/tab_btn_up_rateshare.png" title="Rate/Share"
								width="80" height="50" />
						</a>
						</li>
					</ul>
				</c:if>
				<c:if test="${requestScope.loyaltydetails ne null}">
					<ul id="sLTab">
						<li><a href="#"><img
								src="../images/tab_btn_up_wishlist.png" title="Wish List"
								width="80" height="50"
								onclick="addWishListWithProduct('${requestScope.productIdWithComma}')" />
						</a>
						</li>
						<c:if test="${requestScope.addedFlag == '0'}">
							<li id="myAjax"><a href="#"><img
									src="../images/tab_up_add_cpns.png" width="80" height="50"
									onclick="addCLR(${requestScope.loyaltydetails.loyaltyInfo.loyaltyDealId},'loyality','${requestScope.addedFlag}')" />
							</a>
							</li>
						</c:if>
						<c:if
							test="${requestScope.addedFlag == '1' || requestScope.addedFlag eq ''}">
							<li class="noHvr"><a><img
									src="../images/tab_up_add_cpns.png" width="80" height="50" />
							</a>
							</li>
						</c:if>
						<li><a href="coupongallery.htm"><img
								src="../images/tab_btn_up_cpnGlry.png" title="Gallery"
								width="80" height="50" />
						</a>
						</li>
						<li><a href="#" id="actSht"><img
								src="../images/tab_btn_up_rateshare.png" title="Rate/Share"
								width="80" height="50" />
						</a>
						</li>
					</ul>
				</c:if>
				<c:if test="${requestScope.rebatedetails ne null}">
					<ul id="sLTab">
						<li><a href="#"><img
								src="../images/tab_btn_up_wishlist.png" title="Wish List"
								width="80" height="50"
								onclick="addWishListWithProduct('${requestScope.productIdWithComma}')" />
						</a>
						</li>
						<c:if test="${requestScope.addedFlag == '0'}">
							<li id="myAjax"><a href="#"><img
									src="../images/tab_up_add_cpns.png" width="80" height="50"
									onclick="addCLR(${requestScope.rebatedetails.rebateInfo.rebateId},'rebate','${requestScope.addedFlag}')" />
							</a>
							</li>
						</c:if>
						<c:if
							test="${requestScope.addedFlag == '1' || requestScope.addedFlag eq ''}">
							<li class="noHvr"><a><img
									src="../images/tab_up_add_cpns.png" width="80" height="50" />
							</a>
							</li>
						</c:if>
						<li><a href="coupongallery.htm"><img
								src="../images/tab_btn_up_cpnGlry.png" title="Gallery"
								width="80" height="50" />
						</a>
						</li>
						<li><a href="#" id="actSht"><img
								src="../images/tab_btn_up_rateshare.png" title="Rate/Share"
								width="80" height="50" />
						</a>
						</li>
					</ul>
				</c:if>
			</div>
		</div>
	</form:form>
</body>
</html>

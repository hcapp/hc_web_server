<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="dataView rtlrPnl relatvDiv" style="height: 450px; overflow-x: hidden;"><!--<h3>List of Retailers Based on Search Criteria</h3>-->
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="mobGrd detView zeroBtmMrgn" id="rtlrLst">
				<c:forEach items="${requestScope.retailsDetails.thisLocationRetailerInfo}" var="retdetails">				
				 				
					<tr onclick="callRetSummary(${retdetails.retailLocationID},${retdetails.retailerId},'${fn:replace(retdetails.retailerName, '\'', '%27')}')" >
						<td width="13%" align="center">
						<c:if test="${retdetails.logoImagePath == 'NotApplicable'}" >
						<img src="../images/imgIcon.png" alt="Img" width="38" height="38" />
						</c:if>
						<c:if test="${retdetails.logoImagePath != 'NotApplicable'}" >
						<img src="${retdetails.logoImagePath}" alt="Img" width="38" height="38" />
						</c:if>
						</td>
						<td width="81%">
							  <span> <c:out value="${retdetails.retailerName}" /> 
										  	 <!-- Displaying retailer sales flag -->  
									<c:if test="${retdetails.saleFlag ne null}">
			<c:choose>
				<c:when test="${retdetails.saleFlag > 0}">   
							          <img src="../images/specialIcon.png" alt="specail" width="50" height="19" /> </c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
		</c:if>	  	 </span> 							   <c:out value="${retdetails.distance}" />mi
							   <c:out value="${retdetails.retailAddress}" />
											</td>
						<td width="6%"><img src="../images/rightArrow.png" alt="Arrow" width="11" height="15" />
						</td>
					</tr>
				</c:forEach>
			</table>
 </div>
  
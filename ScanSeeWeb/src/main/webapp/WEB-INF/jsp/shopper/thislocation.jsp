<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script src="/ScanSeeWeb/scripts/shopper/jquery.MetaData.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/jquery.rating.js"
	type="text/javascript"></script>

<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcoZEmDQJTP5iDa_aZIaHS5H9xdAdWG6w&sensor=true"></script>
<script type="text/javascript"> 
 
//window.onload = function() {

 //var vCityID = '${sessionScope.radius}';

 //var sel = document.getElementById("radiusTL");
// for ( var i = 0; i < sel.options.length; i++) {
//  if (sel.options[i].value == vCityID) {
 //  sel.options[i].selected = true;
 //  return;
 // }
 //}
//}

$(function(){
 
  $('div.rating-cancel').click(function() { 
  //var curvalue = $('div.test').text();
	$('div.test').html("");
	deleteUserRating('${requestScope.productRatingReview.userRatingInfo.productId}');
   });
   
   var myOptions = {center: new google.maps.LatLng(-34.397, 150.644),
							zoom: 8,          
							mapTypeId: google.maps.MapTypeId.ROADMAP};
		   var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
		   
	       var marker = new google.maps.Marker({
	          map: map,
			  draggable: true,
	          position: map.getCenter()
	        });
	        var location = map.getCenter();
			var lat = location.lat();
			var longt = location.lng();

			var lattitude = '${sessionScope.objAreaInfoVO.lattitude}';
			var longitude = '${sessionScope.objAreaInfoVO.longitude}';
			
			if(lattitude != "" || longitude != ""){
				var myOptions = {center: new google.maps.LatLng(lattitude,longitude),
				zoom: 8,          
				mapTypeId: google.maps.MapTypeId.ROADMAP};
				var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
				var marker = new google.maps.Marker({
				map: map,
				draggable: true,
				position: map.getCenter()
				});
			}
			if('${sessionScope.objAreaInfoVO.zipCode}' != ""){
				
				var lattitude = '${sessionScope.zipcodeLat}';
				var longitude = '${sessionScope.zipcodeLong}';
				
				if(lattitude != "" || longitude != ""){
					var myOptions = {center: new google.maps.LatLng(lattitude,longitude),
					zoom: 8,          
					mapTypeId: google.maps.MapTypeId.ROADMAP};
					var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
					var marker = new google.maps.Marker({
					map: map,
					draggable: true,
					position: map.getCenter()
					});
				}
			}
});
</script>


<form:form name="thislocation" commandName="thislocation">
	<div id="content" class="topMrgn">
		<div align="center" id="airports"></div>
		<input type="hidden" name="latitude" id="latitude" value="" /> <input
			type="hidden" name="longitude" id="longitude" value="" />
		<div class="navBar">
			<ul>
				<c:set var="msg" value="Please Enter zip code to view retailers." />
				<c:set var="message"
					value="None Available ,Select another category below." />
				<c:set var="type" value="other" />
				<c:choose>
					<c:when
						test="${requestScope.message ne null && !empty requestScope.message}">
						<c:choose>
							<c:when test="${requestScope.message eq message}">
								<li><a href="#"><img src="../images/backBtn.png"
										onclick="javascript:history.go(-1);" /> </a>
								</li>
							</c:when>
							<c:when test="${requestScope.message ne msg}">
								<li><a href="#"><img src="../images/backBtn.png"
										onclick="javascript:history.go(-1);" /> </a>
								</li>
							</c:when>
						</c:choose>

					</c:when>
					<c:when
						test="${requestScope.retailsDetails.thisLocationRetailerInfo ne null && !empty requestScope.retailsDetails.thisLocationRetailerInfo}">
						<li><a href="thislocation.htm"><img
								src="../images/backBtn.png" /> </a>
						</li>
					</c:when>
					<c:when
						test="${requestScope.prodcutDetails ne null && !empty requestScope.prodcutDetails}">
						<li><a href="#"><img src="../images/backBtn.png"
								onclick="javascript:history.go(-1);" /> </a>
						</li>
					</c:when>
					<c:when
						test="${requestScope.retSummary ne null && !empty requestScope.retSummary}">
						<li><a href="#"><img src="../images/backBtn.png"
								onclick="javascript:history.go(-1);" /> </a>
						</li>
					</c:when>

					<c:when
						test="${requestScope.retspecials ne null && !empty requestScope.retspecials}">
						<li><a href="#"><img src="../images/backBtn.png"
								onclick="javascript:history.go(-1);" /> </a>
						</li>
					</c:when>
					<c:when
						test="${requestScope.rethotdeals ne null && !empty requestScope.rethotdeals}">
						<li><a href="#"><img src="../images/backBtn.png"
								onclick="javascript:history.go(-1);" /> </a>
						</li>
					</c:when>
					<c:when
						test="${requestScope.retailerPageInfoList ne null && !empty requestScope.retailerPageInfoList}">
						<li><a href="#"><img src="../images/backBtn.png"
								onclick="javascript:history.go(-1);" /> </a>
						</li>
					</c:when>

					<c:when
						test="${requestScope.seacrhList.productList ne null && !empty requestScope.seacrhList.productList}">
						<li><a href="#"><img src="../images/backBtn.png"
								onclick="javascript:history.go(-1);" /> </a>
						</li>
					</c:when>
					<c:when
						test="${requestScope.retailer ne null && !empty requestScope.retailer}">
						<li><a href="#"><img src="../images/backBtn.png"
								onclick="javascript:history.go(-1);" /> </a>
						</li>
					</c:when>
					<c:when test="${sessionScope.prodDet eq true}">
						<li><a href="#"><img src="../images/backBtn.png"
								onclick="searchProductList('')" /> </a>
						</li>
					</c:when>
				</c:choose>
				<c:choose>
					<c:when
						test="${requestScope.retailsDetails.thisLocationRetailerInfo ne null && !empty requestScope.retailsDetails.thisLocationRetailerInfo}">
						<li class="titletxt" style="padding-left: 50px;">Choose Your
							Location</li>
					</c:when>
					<c:when
						test="${requestScope.prodcutDetails ne null && !empty requestScope.prodcutDetails}">
						<li class="titletxt" style="padding-left: 50px;">This
							Location</li>
					</c:when>


					<c:when
						test="${requestScope.retailer ne null && !empty requestScope.retailer}">
						<li class="titletxt"><c:out
								value="${requestScope.retailer.retailName}" /></li>
					</c:when>

					<c:when
						test="${requestScope.retailerPageInfoList ne null && !empty requestScope.retailerPageInfoList}">
						<li class="titletxt"><c:out
								value="${requestScope.retailerPageInfoList.retailerName}" /></li>
					</c:when>



					<c:otherwise>
						<li class="titletxt" style="padding-left: 50px;">This
							Location</li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
		<!-- Displaying radius details functionality is removed   -->
		<!--	<c:if
				test="${requestScope.prodcutDetails ne null && !empty requestScope.prodcutDetails}">
				<div class="algnMnt">
					<table cellspacing="0" cellpadding="0" border="0" width="100%"
						class="brdrLsTbl">
						<tbody>
							<tr>
								<td width="8%">Find:</td>
								<td width="30%"><input name="searchKey" id="searchKey"
									class="textboxSmall"
									onkeypress="return searchProductList(event)" />
								</td>
								<td width="62%"><input type="button" class="mobBtn"
									value="Search"
									onclick="thisLocationProductSearch(${requestScope.retailerId});"
									title="Search" /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</c:if> -->
		<div class="clear"></div>
		<div class="mobCont">
			<div class="mobDsply floatR relatvDiv">
				<c:choose>


					<c:when
						test="${requestScope.prodcutDetails ne null && !empty requestScope.prodcutDetails}">
						<%@include file="thislocation_prod.jsp"%>
					</c:when>
					<c:when
						test="${requestScope.retailsDetails.thisLocationRetailerInfo ne null && !empty requestScope.retailsDetails.thisLocationRetailerInfo}">
						<%@include file="thislocation_dataview.jsp"%>
					</c:when>
					<c:when
						test="${requestScope.seacrhList.productList ne null && !empty requestScope.seacrhList.productList}">
						<%@include file="thislocation_prodsearch.jsp"%>
					</c:when>

					<c:when
						test="${requestScope.retailerPageInfoList ne null && !empty requestScope.retailerPageInfoList}">
						<%@include file="thislocation_retsummary.jsp"%>
					</c:when>
					<c:when
						test="${requestScope.retspecials ne null && !empty requestScope.retspecials}">
						<%@include file="thislocation_retspecialslst.jsp"%>
					</c:when>
					<c:when
						test="${requestScope.rethotdeals ne null && !empty requestScope.rethotdeals}">
						<%@include file="thislocation_rethotdealslst.jsp"%>
					</c:when>
						<c:when
						test="${requestScope.retcoupons eq 'retcoupons'}">
						<%@include file="thislocation_retcoupons.jsp"%>
					</c:when>
					<c:otherwise>
						<%@include file="thislocation_message.jsp"%>
					</c:otherwise>
				</c:choose>
				<input type="hidden" name="retailerId" id="retailerId" value="" />
				<input type="hidden" name="productId" id="productId" value="" /> <input
					type="hidden" name="hotDealID" id="hotDealID" value="" /> <input
					type="hidden" name="regularPrice" id="regularPrice" value="" /> <input
					type="hidden" name="salePrice" id="salePrice" value="" /> <input
					type="hidden" name="productPrice" id="productPrice" value="" /> <input
					type="hidden" name="distance" id="distance" value="" /> <input
					type="hidden" name="retailLocationID" id="retailLocationID"
					value="" /> <input type="hidden" name="currentRating"
					id="currentRating" value="" /> <input type="hidden"
					name="couponId" id="couponId" value="" /> <input type="hidden"
					name="usage" id="usage" value="" /> <input type="hidden"
					name="tlFind" id="tlFind" value="" /> <input type="hidden"
					name="retailerName" id="retailerName" value="" /> <input
					type="hidden" name="mediaType" id="mediaType" value="" /> <input
					type="hidden" name="loyaltyDealId" id="loyaltyDealId" value="" />
				<input type="hidden" name="flag" id="flag" value="" /> <input
					type="hidden" name="fromThisLocation" id="fromThisLocation"
					value="true" /> <input type="hidden" name="msg" id="msg"
					value="<c:out value="${requestScope.message}" />" />

				<div class="optnPnl">
					<div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="brdrLsTbl">
							<tr>
								<td width="70%">City Name:</td>
								<td width="20%"><input type="text" class="textboxDate"
									name="cityNameTL" value='${requestScope.cityNameTL}' "/>
								</td>
								<td width="5%"><input type="button" class="mobBtn LstRtlr"
									value="Go" onclick="showAddress(''); return false" title="Go" />
								</td>
							</tr>
						</table>
					</div>
					<div align="center" id="map_canvas"
						style="width: 260px; height: 260px"></div>
					<table width="98%" border="0" cellspacing="0" cellpadding="0"
						class="brdrLsTbl">
						<c:choose>
							<c:when
								test="${requestScope.fromwlratereview eq true || requestScope.fromwlonlineratereview eq true}">
								<tr>
									<td width="40%">Zip Code:</td>
									<td width="70%"><input type="text" class="textboxSmall"
										name="zipcodeWL" onkeypress="return isNumberKey(event)"
										value="${sessionScope.objAreaInfoVO.zipCode}" />
									</td>
								</tr>
								<tr>
									<td colspan="2"><input type="button"
										class="mobBtn LstRtlr" value="Refresh"
										onclick="ValidateTLWishList();" title="Refresh" />
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<td width="40%">Zip Code:</td>
									<td width="70%"><input type="text"
										onkeypress="return ValidateRetailers(event)" maxlength="5"
										class="textboxSmall" name="zipcodeTL"
										value="${sessionScope.zipcode}" />
									</td>
								</tr>

								<!--<tr>
												<td>Radius:</td>
												<td><select class="textboxSmall" name="raiudTL"
													id="radiusTL">
														<option value="0" label="">Select Radius</option>

														<c:forEach items="${sessionScope.radiuslst}" var="rad">

															<c:choose>

																<c:when test="${sessionScope.radius eq rad.distInMiles}">
																	<option value="${rad.distInMiles}" label=""
																		selected="selected">${rad.distInMiles} miles</option>
																</c:when>
																<c:otherwise>
																	<option value="${rad.distInMiles}" label="">${rad.distInMiles} miles</option>
																</c:otherwise>

															</c:choose>

														</c:forEach>

												</select></td>
											</tr>
											-->
								<!--<tr>
								<td>Radius:</td>
								<td><select class="textboxSmall" name="raiudTL">
										<option id="R1" value="" >Select Radius</option>
										<option id="R2" value="5" 
										<c:if test="${requestScope.radius eq 5}">selected="selected"</c:if> >5</option>
										<option id="R3" value="10" <c:if test="${requestScope.radius eq 10}">selected="selected"</c:if> >10</option>
										<option id="R4" value="15" <c:if test="${requestScope.radius eq 15}">selected="selected"</c:if> >15</option>
										<option id="R5" value="20" <c:if test="${requestScope.radius eq 20}">selected="selected"</c:if> >20</option>
								</select>
								</td>
								</tr>
								-->
								<tr>
									<td colspan="2"><input type="button"
										class="mobBtn LstRtlr" value="List Retailers"
										onclick="javascript:ValidateRetailers('');"
										title="List Retailers" />
									</td>
								</tr>
								<!--<tr>
												<td colspan="2">If you did not find the retailers you
													were looking for, Please change radius.</td>
											</tr>
										-->
							</c:otherwise>
						</c:choose>
					</table>
				</div>


				<div class="clear"></div>
			</div>
			<%@include file="leftmenu.jsp"%>

		</div>



		<div class="clear"></div>
		<c:if
			test="${requestScope.retailsDetails ne null && !empty requestScope.retailsDetails}">
			<div class="pagination algnMnt">
				<p>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
						nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
						pageRange="${sessionScope.pagination.pageRange}"
						url="${sessionScope.pagination.url}" enablePerPage="false" />
				</p>
			</div>
		</c:if>
		<c:if
			test="${requestScope.prodcutDetails ne null && !empty requestScope.prodcutDetails}">
			<div class="pagination">
				<p>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
						nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
						pageRange="${sessionScope.pagination.pageRange}"
						url="${sessionScope.pagination.url}" enablePerPage="false" />
				</p>
			</div>
		</c:if>
		<c:if
			test="${requestScope.seacrhList.productList ne null && !empty requestScope.seacrhList.productList}">
			<div class="pagination">
				<p>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
						nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
						pageRange="${sessionScope.pagination.pageRange}"
						url="${sessionScope.pagination.url}" enablePerPage="false" />
				</p>
			</div>
		</c:if>
		<div class="tabBar">

			<c:if
				test="${requestScope.retailsDetails ne null && !empty requestScope.retailsDetails}">
				<ul id="sLTab">

					<li><a href="findDisplay.htm"><img
							src="../images/tab_btn_down_find.png" alt="find"> </a></li>

					<li><a href="fetchusersettings.htm"><img
							src="../images/tab_btn_up_account.png" alt="Account" /> </a></li>

					<li><a href="fetchusersettings.htm"><img
							src="../images/tab_btn_down_settings.png" alt="settings" /> </a></li>
					<li><a href="http://www.scansee.com" target="_blank"><img
							src="../images/tab_btn_up_about.png" alt="About" /> </a></li>
				</ul>
			</c:if>

			<c:if
				test="${requestScope.prodcutDetails ne null && !empty requestScope.prodcutDetails || sessionScope.currentspecial ne null && !empty sessionScope.currentspecial  }">

				<c:choose>
					<c:when test="${requestScope.selbutn eq 'sales'}">
						<ul id="sLTab">
							<li><a href="#"><img
									src="../images/cs_sales_tab_selected.png" alt="Sales"
									onclick="callRetailerSales(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_specials_tab.png"
									alt="Specials"
									onclick="callRetailerSpecials(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_hotDeals_tab.png"
									alt="hotdeals"
									onclick="callRetailerHotdeals(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_coupons_tab.png"
									alt="coupons" class="swapSrc"
									onclick="callRetailerCoupons(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
						</ul>
					</c:when>
					<c:when test="${requestScope.selbutn eq 'specials'}">
						<ul id="sLTab">
							<li><a href="#"><img src="../images/cs_sales_tab.png"
									alt="Sales"
									onclick="callRetailerSales(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img
									src="../images/cs_specials_tab_selected.png" alt="Specials"
									onclick="callRetailerSpecials(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_hotDeals_tab.png"
									alt="hotdeals"
									onclick="callRetailerHotdeals(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_coupons_tab.png"
									alt="coupons" class="swapSrc"
									onclick="callRetailerCoupons(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
						</ul>
					</c:when>
					<c:when test="${requestScope.selbutn eq 'hotdeals'}">

						<ul id="sLTab">
							<li><a href="#"><img src="../images/cs_sales_tab.png"
									alt="Sales"
									onclick="callRetailerSales(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_specials_tab.png"
									alt="Specials"
									onclick="callRetailerSpecials(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img
									src="../images/cs_hotDeals_tab_selected.png" alt="hotdeals"
									onclick="callRetailerHotdeals(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_coupons_tab.png"
									alt="coupons"
									onclick="callRetailerCoupons(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
						</ul>
					</c:when>
					<c:when test="${requestScope.selbutn eq 'coupons'}">
						<ul id="sLTab">
							<li><a href="#"><img src="../images/cs_sales_tab.png"
									alt="Sales"
									onclick="callRetailerSales(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_specials_tab.png"
									alt="Specials"
									onclick="callRetailerSpecials(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img src="../images/cs_hotDeals_tab.png"
									alt="hotdeals"
									onclick="callRetailerHotdeals(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
							<li><a href="#"><img
									src="../images/cs_coupons_tab_selected.png" alt="coupons"
									onclick="callRetailerCoupons(${sessionScope.retailerId},${sessionScope.retLocationId})" />
							</a>
							</li>
						</ul>
					</c:when>
					<c:otherwise>

					</c:otherwise>
				</c:choose>
			</c:if>
		</div>


	</div>

</form:form>


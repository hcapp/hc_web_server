

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
  
 <form:form name="SearchForm" commandName="SearchForm"> 
  <div id="content" class="topMrgn">
    <div class="navBar">
    	<ul>
        	<li><a href="findDisplay.htm"><img src="../images/backBtn.png" /></a></li>
            <li class="titletxt">Find</li>
          
      </ul>
    </div>
    <div class="clear"></div>
    <div class="mobCont noBg">
<div class="splitDsply floatR">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topSubBar">
    <tr>
      <td width="7%" align="center">Find:</td>
      <td width="51%"><!--
      
       <select class="textboxBig" name="shprfindOpt">
      	<option>Please Select</option>
         <option value="products">Product</option>
         <option value="#" selected="selected">Location</option>
         <option value="#">Service</option>
         <option value="#">cancel</option>
         <input type="text" name="search_find" class="textboxSmall"/></td>
         <td width="42%"><input type="button" class="mobBtn" value="Search" onclick="navToPage();" /></td>
       -->
      <form:select path="searchType" class="textboxBig" name="shprfindOpt">
    
      	<option>Please Select</option>
         <option value="products">Product</option>
        
      </form:select>
     <form:input path="searchKey" class="textboxSmall" />
      <td width="42%">
      <input type="button" class="mobBtn" value="Search" onclick="shopperProductSearch();" title="Search"/></td> 
    </tr>
  </table>
  <div class="clear"></div>
  <div class="fluidView">
    <div class="stretch">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd htlt">
    
       
      </table>
    </div>
    
  </div>
</div>
<%@include file="leftmenu.jsp"%>
            <div class="clear"></div>
		
				</div>
				<div class="tabBar"></div>
				</div>
		
</form:form>		

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="gnrlGrd detView lstNm htlt" id="HotDeal_listView_2">
	<tr>
		<th align="center"><img
			src="/ScanSeeWeb/images/MM_HotDeals_Icon.png" alt="hotdeals"
			width="28" height="28" />
		</th>
		<c:choose>
			<c:when test="${dMAName == null}">
				<th align="left">Great News !!! Many Hot Deals Found Around You</th>
			</c:when>
			<c:otherwise>
				<th align="left">Great News !!! Many Hot Deals in <c:out
						value="${dMAName}" /></th>
			</c:otherwise>
		</c:choose>
		

		<th align="left">&nbsp;</th>
	</tr>
	<tbody>
		<c:forEach items="${hotDealsList.hotDealsCategoryInfo}"
			var="hotdealcat">
			<tr>
				<td colspan="4" class="mobSubTitle"><c:out
						value="${hotdealcat.categoryName}" /></td>
			</tr>
			<c:forEach items="${hotdealcat.hotDealAPIResultSetLst}"
				var="hotdealapi">
				<tr>
					<td colspan="4" class="mobApi"><c:out
							value="${hotdealapi.apiPartnerName}" /></td>
				</tr>

				<c:forEach items="${hotdealapi.hotDealsDetailslst}" var="hotdeal">
					<tr class="mobGrd detView zeroBtmMrgn"
						onclick="getHotDealInfo(<c:out value='${hotdeal.hotDealId}' />)">
						<td align="center"><c:choose>
								<c:when test="${hotdeal.hotDealImagePath == null}">
									<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img" width="38"
										height="38" />

								</c:when>
								<c:otherwise>

									<img src="<c:out value="${hotdeal.hotDealImagePath}" />"
										alt="Img" width="38" height="38" />
								</c:otherwise>
							</c:choose>
						</td>
						<td><span><c:out value="${hotdeal.hotDealName}" /> </span>
							<ul>
								<li><span>Regular Price: $</span><c:out
										value="${hotdeal.hDPrice}" />
								</li>
								<li><span>Sale Price: $</span><c:out	value="${hotdeal.hDSalePrice}" />
								</li>
							</ul></td>
						<td><img src="/ScanSeeWeb/images/rightArrow.png" alt="Arrow"
							width="11" height="15" /></td>
					</tr>
				</c:forEach>
			</c:forEach>
		</c:forEach>
	</tbody>
</table>

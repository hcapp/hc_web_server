<!-- This page used to display the find  search product list results  -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>

<script src="https://platform.twitter.com/widgets.js"
	type="text/javascript"></script>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>


<script type="text/javascript">
$(document).ready(function() {

var vCatId = '${sessionScope.catId}';
 var sel = document.getElementById("catID");
 for ( var i = 0; i < sel.options.length; i++) {
 if (sel.options[i].value == vCatId) {
   sel.options[i].selected = true;
  return;
  }
 }
 });
</script>

<script type="text/javascript">
$(document).ready(function() {
var PopulationCenterID = '${sessionScope.PopulationCenterID}';

 var sel = document.getElementById("populationCentId");
 for ( var i = 0; i < sel.options.length; i++) {
 if (sel.options[i].value == PopulationCenterID) {
   sel.options[i].selected = true;
  return;
  }
 }
 });
</script>
 
 

<script type="text/javascript">




function getPerPgaVal() {

	var selValue = $('#selPerPage :selected').val();

		document.hotdealmainpage.recordCount.value = selValue;
		document.hotdealmainpage.action = "consdisplayhotdeals.htm";
		document.hotdealmainpage.method = "POST";
		document.hotdealmainpage.submit();

	}
	function callNextPage(pagenumber, url) {
		var selValue = $('#selPerPage :selected').val();

		document.paginationForm.pageNumber.value = pagenumber;
		document.paginationForm.recordCount.value = selValue;
		document.paginationForm.pageFlag.value = "true";
		document.paginationForm.action = url;
		document.paginationForm.method = "POST";
		document.paginationForm.submit();
	}
	
function twiterShareClick(hdName,hdURL){

//function twiterShareClick(hdName){

var tweetText; 
var hdURL;
//var hdName='${hotDealDetails.hotDealName}';
//var hdURL='${hotDealDetails.hdURL}';
if(hdURL=="" && hdURL == 'NotApplicable'){
tweetText='Great find at ScanSee!'+hdName
}else{
tweetText='Great find at ScanSee! '+hdName+"Visit:"+hdURL
}
var tweeturl = 'http://twitter.com/share?text='+escape(tweetText);

window.open(tweeturl);

 twttr.events.bind('tweet', function(event) {
       alert("Lottery Entry Successful");
       window.location = "http://www.mysite.com"
    });
}

<!--Below script method is used for sharing product information via pinterest. -->

function sharePin(dealId,i){
	var base = "http://pinterest.com/pin/create/button/";
	$.ajaxSetup({cache:false});
	$.ajax({
	type : "GET",
	url : "/ScanSeeWeb/shopper/pinterestShareDealInfo.htm",
	data : {
		'dealId'  : dealId
	},
	success : function(response) {
			var responseJSON = JSON.parse(response);
			var encodedpinurl=responseJSON.dealURL;
			encodedpinurl=encodedpinurl.replace("+", "%2B");
		    encodedpinurl=encodedpinurl.replace("/", "%2F");		
			var prodName=responseJSON.dealName;
			var encodedproductName=escape(prodName);
            encodedproductName=encodedproductName.replace("+", "%2B");
            encodedproductName=encodedproductName.replace("/", "%2F");
            var encodedimagePath=responseJSON.dealImagePath;
            encodedimagePath=encodedimagePath.replace("+", "%2B");
            encodedimagePath=encodedimagePath.replace("/", "%2F");
            finalUrl = base + "?url=" + encodedpinurl + "&media=" + encodedimagePath + "&description=" + encodedproductName;
            inner = "<a href='" + finalUrl + "'class='pin-it-button' count-layout='none' target='_blank'><img id='pin" + i + "' src='/ScanSeeWeb/images/consumer/pin_down.png' alt='pinterest' width='25' height='25'/></a>";
  			$("#pin_it_"+i).html(inner);
  			$("#pin"+i).click();
	},
	error : function(e) {
		alert('Error Occured');
	}
	});
}

	  
</script>

<body onresize="resizeDoc();" onload="resizeDoc();">

	<div id="contWrpr">
		<form:form commandName="hotdealmainpage" name="hotdealmainpage"
			id="hotdealmainpage">

			<form:hidden path="hotDealID" id="hotDealID" />
			<input type="hidden" name="redirectUrl" id="redirectUrl" value="" />
			<form:hidden path="popCntCity" id="popCntCity" />
			<input type="hidden" name="latitude" id="latitude" value="" />
			<input type="hidden" name="longitude" id="longitude" value="" />
			<input type="hidden" name="recordCount" id="recordCount" value="" />
			<input type="hidden" name="emailId" id="emailId"
				value="${sessionScope.emailId}" />

			<div class="breadCrumb">
				<ul>
					<li class="brcIcon"><img
						src="/ScanSeeWeb/images/consumer/hotDeals_bcIcon.png"
						alt="hotdeals" /></li>
					<li>Hot Deals</li>
				</ul>
				<span class="rtCrnr">&nbsp;</span>
			</div>
			<div class="btnCntrl brdr_lr inputAlgn">
				<ul>
					<!--<li class="mrgnRt"><label>Search By City:</label> <span>
					
					
							<form:input path="dMAName" type="text" class="textboxGenSrch" />
							<a href="javascript:void(0);"> <img width="16" height="16"
								alt="search" src="../images/consumer/srchIcon.png" /> </a> </span>
					</li>-->

					<li><label>Search By DMA:</label> <form:select
							path="populationCentId" id="populationCentId" class=""
							onchange="getHotDealforDma()">
							<form:option value="">Select By DMA</form:option>
								<c:if test="${ populcatonCenters ne null}">
								<c:forEach items="${populcatonCenters}" var="pop">
									<form:option value="${pop.populationCentId}"
										label="${pop.dmaName},---${pop.hotdealCnt}" />
								</c:forEach>
							</c:if>
						</form:select>
					</li>
					<li><label>Search By Category:</label> <form:select
							path="catID" id="catID" class=""
							onchange="getHotDealforCategory()">
							<form:option value="">Select By Category</form:option>
							<form:option value="0">All</form:option>
							<c:if test="${sessionScope.categoryList ne null}">
								<c:forEach items="${categoryList}" var="s">
									<form:option value="${s.catID}" label="${s.category}" />
								</c:forEach>
							</c:if>
						</form:select></li>
				</ul>
			</div>
			<div id="detailedList" class="hotdeals">
				<!--	<c:if test="${FavCatFlag == 1 && displayNarrowList eq true}">
					<img src="/ScanSeeWeb/images/NarrowListImg.png" alt="Img"
						class="imgLinks"
						onclick="location.href='/ScanSeeWeb/shopper/hdcategoryPref.htm'"
						height='80px' width='970px' />
				</c:if> -->
				<c:set var="i" value="0" />
				<c:choose>
					<c:when test="${hotDealsList ne null && !empty hotDealsList}">
						<!--This for loop used to  display the product details. -->
						<c:forEach items="${hotDealsList.hotDealsCategoryInfo}"
							var="hotDealCat">
							<div class="contBlks">
								<h3 class="subTitleBg">${hotDealCat.categoryName}</h3>


								<c:forEach items="${hotDealCat.hotDealAPIResultSetLst}"
									var="hotDealAPI">
									<c:forEach items="${hotDealAPI.hotDealsDetailslst}"
										var="hotDeal">
										<div class="contBox">
											<ul>
												<li class="imgBx">
												<a href="/ScanSeeWeb/shopper/conshddetails.htm?hotdealId=${hotDeal.hotDealId}
												&page=hotdeal&hdlstid=${hotDeal.hotdealLstId}">
												<img
													src="${hotDeal.hotDealImagePath}" alt="hdimage"
													onerror="this.src = '/ScanSeeWeb/images/imageNotFound.png';"
													width="200" height="150" /></a></li>
												<li class="title " title="${hotDeal.hotDealName}"><a
													href="/ScanSeeWeb/shopper/conshddetails.htm?hotdealId=${hotDeal.hotDealId}
												&page=hotdeal&hdlstid=${hotDeal.hotdealLstId}"
													class="dealtrim">${hotDeal.hotDealName}</a>
												</li>
												<c:if test="${hotDeal.apiPartnerName ne  null}">
													<li><label>Powered By:</label><label class="labeltxt">
															${hotDeal.apiPartnerName}</label></li>
												</c:if>


												<li class="desc more">${hotDeal.hDshortDescription}</li>
												<li class="desc more">${hotDeal.hDLognDescription}</li>
											</ul>
											<div align="center" class="contrlStrip">
												<span> 
													<img src="/ScanSeeWeb/images/consumer/fb_down.png"
													alt="facebook" width="25" height="25"
													onclick="hotDealFBShare(${hotDeal.hotDealId},'menudealdetails')" />
													<img src="/ScanSeeWeb/images/consumer/twitter_down.png"
													alt="twitter" width="25" height="25"
													onClick="twiterShareClick('${hotDeal.hotDealName}','${hotDeal.hdURL}')" />
													<c:if test="${hotDeal.hotDealImagePath ne null && !empty hotDeal.hotDealImagePath}">
														<span id="pin_it_${i}" class="inline"> 
														<img src='/ScanSeeWeb/images/consumer/pin_down.png' onclick="sharePin(${hotDeal.hotDealId},${i})" alt='pinterest' width="25" height="25"/>
														<c:set var="i" value="${i + 1}"></c:set> </span> 
													</c:if>
													<img src="/ScanSeeWeb/images/consumer/email_down.png" width="25"
															height="25" class="hotdealshareemailActn" name="${hotDeal.hotDealId}" alt="email" /> 
												</span>
											</div>

										</div>
									</c:forEach>
								</c:forEach>
							</div>
						</c:forEach>
					</c:when>

					<c:otherwise>
						<div class="zeroBg contBlks ">
							<img alt="close"
								src="/ScanSeeWeb/images/consumer/nodeals_poster.png" />
						</div>

					</c:otherwise>
				</c:choose>
				<div class="clear"></div>

			</div>
		</form:form>
		<c:if test="${hotDealsList ne null && !empty hotDealsList}">


			<div class="pagination brdrTop">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="noBrdr" id="perpage">
					<tr>
						<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
							nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
							pageRange="${sessionScope.pagination.pageRange}"
							url="${sessionScope.pagination.url}" enablePerPage="true" />
					</tr>
				</table>
			</div>
		</c:if>
		<div class="clear"></div>
	</div>
</body>

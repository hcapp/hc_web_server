<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script src="/ScanSeeWeb/scripts/shopper/consfind.js"
	type="text/javascript"></script>


<body onresize="resizeDoc();" onload="resizeDoc();">

	<form name="WishListHotDeals" commandName="WishListHotDeals">
		<input type="hidden" name="productId" id="productId" value="" /> <input
			type="hidden" name="redirectUrl" id="redirectUrl" value="" /> <input
			type="hidden" name="recordCount" id="recordCount" value="" /> <input
			type="hidden" name="screenName" id="screenName" value="" /> <input
			type="hidden" name="slscreenName" id="slscreenName"
			value="${requestScope.slscreenName}" />



  <div id="contWrpr">
    <div class="spliView" id="wishLst">
      <div class="">
        <div class="breadCrumb">
          <ul>
            <li class="brcIcon"><img src="/ScanSeeWeb/images/consumer/wl_bcIcon.png" alt="find" /></li>
            <li><a href="conswishlisthome.htm">Wish List</a></li>
            <li class="active">Alerted Items</li>
            <li class="clear"></li>
          </ul>
          <span class="rtCrnr">&nbsp;</span> </div>
        <div class="splitCont">
          <div class="tabdPnl">
            <ul class="tabdCntrl cart">
              <li><a href="conswllocalstore.htm?key1=${requestScope.wlproductId}" class=""> <img src="/ScanSeeWeb/images/consumer/localCart.png" width="28" height="28" alt="local" />Local Stores</a></li>
              <li><a class="actv" href="javascript:void(0);"><img src="/ScanSeeWeb/images/consumer/onlineCart.png" width="28" height="28" alt="online" />Online Stores</a></li>
            </ul>
          </div>
          
          <div class="sctmScroll">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl rowStrip" id="">
             
          
		  	<c:if
							test="${sessionScope.OnlineStoresList eq null && empty sessionScope.OnlineStoresList && 
									sessionScope.CJOnlineStores eq null && empty sessionScope.CJOnlineStores}">
								<tr>
										<td class="zeroPdng"><img
											src="/ScanSeeWeb/images/consumer/wishlist_store_NA.png"
											alt="common_noneAvail" width="954" height="375" /></td>
									</tr>	
									
			</c:if>						
		  
		  	<c:if
							test="${sessionScope.OnlineStoresList ne null || !empty sessionScope.OnlineStoresList ||
									sessionScope.CJOnlineStores ne null || ! empty sessionScope.CJOnlineStores}">
								 <tr class="subHdr" name="Alerted Items">
											<td  colspan="2" align="left">Retailer Information</td>
										</tr>
										<tr name="Alerted Items">
											<td align="l" width="8%"><span class="imgDsply"><img
													src="${requestScope.wlprodimage}" alt="p" width="57"
													height="67" /> </span></td>

											<td><span>${requestScope.wlprodname}</span>
											${requestScope.wlproddesc}
											</td>
											</tr>	
									
			</c:if>		
		  <!--DISPLAYING COMMISSION JUNCTION RETAILER INFORMATION -->	
			  
			  <c:if
												test="${sessionScope.CJOnlineStores ne null && !empty sessionScope.CJOnlineStores}">

						

												<c:forEach
													items="${sessionScope.CJOnlineStores}"
													var="item">
													
							<tr>
                <td colspan="2" align="left" valign="top" class="imgDsply"><ul class="list-detail">
                    
					
					<c:choose>
																<c:when test="${item.buyURL ne null}">

																<li>	<a href="<c:out value="${item.buyURL}"/>"
																		target="_blank"> <c:out
																			value="${item.retailerName}" /> </a></li>
																</c:when>
																<c:otherwise>
																<li>	<a href="javascript:void(0);" />" >
																<c:out value="${item.retailerName}" />
																	</a></li>
																</c:otherwise>
															</c:choose>
							
                <c:if test="${item.price ne null && ! empty item.salePrice}"> 
                    <li><span>
					<c:if test="${item.price ne null && ! empty item.price}">
					              <label>Old Price:</label>
                      ${item.price}</span><span>
					  </c:if>
					 			  
					  <c:if test="${item.salePrice ne null && ! empty item.salePrice  }">
                      <label>New Price:</label>
                      ${item.salePrice}
					  </c:if></span></li></c:if>
					  <c:if test="${item.retailerURL ne null && ! empty item.retailerURL  }">
                   <li> WebSite:
					 <a href="javascript:void(0);">${item.retailerURL}</a></li>  
					</c:if>
					 <li> <label>Powered by:</label><label class="labeltxt"> ScanSee</label></li>
					 
					 
                  </ul></td>
              </tr>
														
													
													</c:forEach>
													</c:if>
													
			<!--DISPLAYING SHOPZILLA RETAILER INFORMATION -->										
			 <c:if
												test="${sessionScope.OnlineStoresList ne null && !empty sessionScope.OnlineStoresList}">

			

												<c:forEach
													items="${sessionScope.OnlineStoresList}"
													var="item">
													
				<tr>						
                <td  colspan="2" align="left" valign="top" class="imgDsply"><ul class="list-detail">
                  
					<li>
					<c:set var="strLength"
																value="${fn:length(item.url)}" /> <c:set var="url"
																value="${fn:substring(item.url,9,strLength-3)}" /> <c:choose>
																<c:when test="${url ne null}">

																	<a href="<c:out value="${url}"/>" target="_blank"><c:out
																			value="${item.merchantName}" /> </a>
																</c:when>
																<c:otherwise>
																	<a href="javascript:void(0);" />" >
																<c:out value="${item.merchantName}" />
																	</a>
																</c:otherwise>
															</c:choose></li>
															
															
                 	<c:if test="${item.originalPrice ne null && ! empty item.salePrice}">
                    <li><span>
					<c:if test="${item.originalPrice ne null && ! empty item.originalPrice}">
					              <label>Old Price:</label>
                      ${item.originalPrice}</span><span>
					  </c:if>
					 			  
					  <c:if test="${item.salePrice ne null && ! empty item.salePrice  }">
                      <label>New Price:</label>
                      ${item.salePrice}</span></li></c:if>
					  </c:if>
					  
                   
					 <li> <label>Powered by:</label><label class="labeltxt">  Shopzilla </label></li>
					 
					 
                  </ul></td>
              </tr>
														
													
													</c:forEach>
													</c:if>
													
            </table>
          </div>
        </div>
        <ul class="infoCntrl">
        
        </ul>
       
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
		<div class="clear"></div>
	</form>
</body>
</html>
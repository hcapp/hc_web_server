<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>





<table width="100%" border="0" cellspacing="0" cellpadding="0"
	class="mobGrd infoPnl zeroBtmMrgn">
	<thead>
		<tr>
			<th colspan="4">


				<ul class="tabdBtnTgl">

					<li class="noRtbrdr active" onclick="">Local</li>

					<li><a 
						onclick="getWLOnlineStores(${requestScope.productId});">Online</a>
					</li>
				</ul></th>

		</tr>
	</thead>


		<!--<h3>List of local stores  Based on product </h3>-->
		<c:choose>
			<c:when
				test="${requestScope.retailersDetails.retailerDetail eq null && empty requestScope.retailersDetails.retailerDetail}">

				<thead>
					<tr class="noHvr">
						
							
							<span id="NoDataInfo" style="color: red; text-align: center;">
									No local stores found!</span>
						
					</tr>
			</c:when>
			<c:when
				test="${requestScope.retailersDetails.retailerDetail ne null && !empty requestScope.retailersDetails.retailerDetail}">

				<tr>
					<c:if
						test="${sessionScope.wishlistproducts.alertProducts ne null && !empty sessionScope.wishlistproducts.alertProducts}">

						<c:forEach
							items="${sessionScope.wishlistproducts.alertProducts.alertProductDetails}"
							var="wishlistdate">
							<%-- Displaying Wish list Alerted product coupon details --%>


							<c:if test="${wishlistdate.productId==requestScope.productId}">


								<td class="mobGrd  zeroBtmMrgn" width="10%"><c:choose>
										<c:when
											test="${wishlistdate.productImagePath eq null || wishlistdate.productImagePath eq 'NotApplicable'}">
											<img src="/ScanSeeWeb/images/imgIcon.png" alt="Img"
												width="38" height="38" />

										</c:when>
										<c:otherwise>
											<img src="<c:out value="${wishlistdate.productImagePath}" />"
												alt="Img" width="38" height="38" />

										</c:otherwise>
									</c:choose></td>
								<td class="mobGrd  zeroBtmMrgn" colspan="2"><span><c:out
											value="${wishlistdate.productName}" />
								</span>
									<ul>

										<li><span>Sale Price:</span>
										<c:out value="${wishlistdate.salePrice}" />
										</li>
									</ul></td>


							</c:if>
						</c:forEach>
					</c:if>
				</tr>
				<c:forEach items="${requestScope.retailersDetails.retailerDetail}"
					var="retailerdetail">
					<tr>

						<td colspan="2"><span><c:out
									value="${retailerdetail.retailerName}" /> </span>
							<ul>

								<c:if
									test="${retailerdetail.price ne null && retailerdetail.price ne '' && retailerdetail.price ne 'NotApplicable' }">
									<li><span>Price:</span> <c:out
											value="${retailerdetail.price}" />
									</li>
								</c:if>


								<c:if
									test="${retailerdetail.salePrice ne null && retailerdetail.salePrice ne '' && retailerdetail.salePrice ne NotApplicable }">
									<li><span>Sale Price:</span> <c:out
											value="${retailerdetail.salePrice}" />
									</li>
								</c:if>

							</ul>
							<div class="newLnView">
								<span>Address:</span>
								<c:out value="${retailerdetail.retaileraddress1}" />
								,
								<c:out value="${retailerdetail.city}" />
								,
								<c:out value="${retailerdetail.state}" />
							</div></td>
						<td width="3%">&nbsp;</td>
					</tr>

				</c:forEach>

			</c:when>
		</c:choose>

	</thead>
</table>
<input type="hidden" name="wishlist" id="wishlist" value="" />
<input type="hidden" name="wlratereview" id="wlratereview" />



<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="dataView rtlrPnl" style="height: 450px; overflow-x: hidden;">
	<!--<h3>List of Retailers Based on Search Criteria</h3>-->

	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="mobGrd detView zeroBtmMrgn" id="hotdeals">

		<!-- Displaying if retailer has ribbenAdImage and ribbenAdurl otherwise retailer name.  -->
		<c:choose>
			<c:when
				test="${requestScope.rethotdeals ne null && !empty requestScope.rethotdeals}">
				<tr class="noHvr">
					<td colspan="3" align="center" class="zeroPadding">
						<div class="topBnr">
							<!--Retailer Name-->


							<c:if test="${sessionScope.retRibbenAd == 'NotApplicable'}">
								<img src="../images/imgIcon.png" alt="ad" width="320"
									height="50" />
							</c:if>
							<c:if test="${sessionScope.retRibbenAd != 'NotApplicable'}">
								<img src="${sessionScope.retRibbenAd}" alt="ad" width="320"
									height="50" />

							</c:if>
						</div></td>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="4" align="center">${sessionScope.retName}</td>
				</tr>
			</c:otherwise>
		</c:choose>

		<!-- Displaying retailer hot deals -->
		<c:if
			test="${requestScope.rethotdeals ne null && !empty requestScope.rethotdeals}">
			<c:forEach items="${requestScope.rethotdeals}" var="retspecial">
				<tr onclick="getRetailerHotDealInfo(${retspecial.hotDealId})">
					<td width="13%" align="center"><img
						src="${retspecial.hotDealImagePath}" width="38" height="38"
						onerror="this.src = '../images/blankImage.gif';" /></td>


					<td width="81%"><span>${retspecial.hotDealName} </a> </span>
						<ul>
							<li><span>${retspecial.hDPrice} </span></li>
							<li>Sale Price: ${retspecial.hDSalePrice}</li>
						</ul>
					</td>


					<td width="6%"><img src="../images/rightArrow.png" alt="Arrow"
						width="11" height="15" />
					</td>
				</tr>
			</c:forEach>
		</c:if>


	</table>
</div>
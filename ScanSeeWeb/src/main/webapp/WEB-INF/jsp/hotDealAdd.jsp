<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-16"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link
	href="/ScanSeeWeb/styles/jquery-ui.css"
	rel="stylesheet" type="text/css">
<script
	src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script
	src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="scripts/web.js" type="text/javascript"></script>	
<script src="scripts/validate.js" type="text/javascript"></script>
<script src="scripts/global.js" type="text/javascript"></script>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!--<script>
      $(document).ready(function() {
            $("#datepicker1").datepicker();
      });

      $(document).ready(function() {
          $("#datepicker2").datepicker();
	  });
      $(document).ready(function() {
			 radioSel('slctOpt')                            
			$('input[name="slctOpt"]').click(function() {
			radioSel('slctOpt')                                               
      });
 });
</script>
-->
<style>
.ui-datepicker-trigger {
                               margin-left:3px;
                               margin-top: 0.5px;
                               
                       }
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar' ,buttonImage: 'images/calendarIcon.png' });
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar',buttonImage: 'images/calendarIcon.png' });
	});
</script>

<script type="text/javascript">
function radioSel(radioName) {

    $('tr.slctCty').hide();
    $('tr.slctLoc').hide();
    var cls = $('input[name ="'+radioName+'"]:checked').val();
    if(cls == "slctLoc"){
    	var productId = document.addhotdealform.productName.value;
			if(productId == '')	{
				alert("Please select atleast one Product");
				document.getElementById('slctLoc').checked = false;
				return false;
			}else{
			document.getElementById('slctLoc').checked = true;
				loadPdtRetailer();
				$('tr.'+cls).show();
			}
    }else if(cls == "slctCty"){
    	loadPopulationCenters();
	     $('tr.'+cls).show();   
    }

}

function loadPdtRetailer() {
	var productId = $('#prodcutID').val();
		$.ajaxSetup({cache:false});
		$.ajax({
		type : "GET",
		url : "fetchpdtretailer.htm",
		data : {
			'productId'  : productId
		},
		success : function(response) {
			$('#myAjax2').html(response);
		},
		error : function(e) {
			alert('Error:' + e);
		}
	});
}

function checkAssociatedProd(){
	 var $prdID = $('#prodcutID').val();
	if($prdID!=""){
		$.ajax({
				type : "GET",
				url : "/ScanSeeWeb/checkAssociatedProd.htm",
				data : {
					'productId'  : $prdID
				},
				success : function(response) {
				openIframePopup('ifrmPopup','ifrm','prodHotDealList.htm',420,600,'View Product/UPC')
					},
				error : function(e) {
					alert('Error:' + 'Error Occured');
				}
			});

	
}else{
openIframePopup('ifrmPopup','ifrm','prodHotDealList.htm',420,600,'View Product/UPC')
}
}
	</script>
	<script type="text/javascript">

	function loadRetailerLocation() {
		var retailerId = $('#retailID').val();
		var productId = $('#prodcutID').val();
		$.ajaxSetup({cache:false});
				$.ajax({
			type : "GET",
			url : "fetchpdtretailerlocation.htm",
			data : {
				'retailerId' : retailerId,
				'productId': productId,
			},
			success : function(response) {
				$('#myAjax3').html(response);
			},
			error : function(e) {
				alert("Eror occurred while Showing Retail Loc Ids");
			}
		});
	}
	function loadPopulationCenters() {
		$.ajaxSetup({cache:false});
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/retailer/fetchhotdealpopcenters.htm",
			data : {},

			success : function(response) {
				$('#myAjax1').html(response);
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}

	function onCategoryLoad() {
		var vCategoryID = document.addhotdealform.bCategoryHidden.value;
		var sel = document.getElementById("bCategory");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCategoryID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}
	
	</script>


<div id="wrapper">
<div id="content" class="shdwBg">
 <%@include file="leftnavigation.jsp" %>
    <div class="grdSec contDsply floatR">
      <div id="secInfo">Add Hot Deals</div>
      <form:form name="addhotdealform" commandName="addhotdealform" action="/ScanSeeWeb/dailyDealsmfg.htm" acceptCharset="ISO-8859-1">
       <form:hidden path="bCategoryHidden"/>
       	<form:hidden path="dealForCityLoc" name="dealForCityLoc"
						id="dealForCityLoc" />
					
       
	<c:if test="${message ne null }">
		<div id="message"><center><h2><c:out value="${message}" /></h2></center></div>
		<script>var PAGE_MESSAGE = true;</script>
	</c:if>
	<form:hidden path="productId" name="prodcutID" id="prodcutID" />
	
	
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grdTbl">
        <tr>
          <td colspan="4" class="header">Add Hot Deal</td>
        </tr>
        <tr>
		  <td width="17%" class="Label"><label for="dealName" class="mand">Hot Deal Name</label></td>
          <td colspan="3">
          <form:errors cssStyle="color:red" path="hotDealName"></form:errors>
          <form:input path="hotDealName" type="text" name="textfield2" id="hotdealname" onkeyup="checkMaxLength(this,'50');"/>
		  
		  </td>
        </tr>
        <tr>
  		  <td class="Label"><label for="regPrice" class="mand">Regular Price $</label></td>
          <td width="33%">
          <form:errors cssStyle="color:red" path="price"></form:errors>
          <form:input path="price" type="text" name="regPrice" id="regPrice"  onkeypress="return isNumberKeyPhone(event)" />
		  		
		  </td>
		  <td width="16%" class="Label"><label for="salePrice" class="mand">Sale Price $</label></td>
          <td width="34%">
           <form:errors cssStyle="color:red" path="salePrice"></form:errors>
          <form:input path="salePrice" type="text" name="salePrice" id="salePrice"  onkeypress="return isNumberKeyPhone(event)" />
		 
		  </td>
        </tr>
        <tr>
          <td class="Label"><label for="dealDesc" class="mand">Hot Deal Description</label></td>
         
          <td colspan="3">
          <form:errors cssStyle="color:red" path="hotDealShortDescription"></form:errors>
          <form:textarea path="hotDealShortDescription" name="hotDealShortDescription" id="hotDealShortDescription" cols="45" rows="5" class="txtAreaSmall" onkeyup="checkMaxLength(this,'1000');"></form:textarea></td>
        </tr>
        <tr>
          <td class="Label">Hot Deal Long Description</td>

          <td colspan="3"><label>
            <form:textarea path="hotDealLongDescription" name="textarea" cols="45" rows="5" class="txtAreaLarge" onkeyup="checkMaxLength(this,'3000');"></form:textarea>
          </label></td>
        </tr>
        <tr>
          <td class="Label" nowrap="nowrap"><label for="dealT&C" class="mand">Hot Deal Terms</label></td>
          <td colspan="3">
             <form:errors cssStyle="color:red" path="hotDealTermsConditions"></form:errors>
          <form:textarea path="hotDealTermsConditions" name="hotDealTermsConditions" id="hotDealTermsConditions"  cols="45" rows="5" class="txtAreaLarge" onkeyup="checkMaxLength(this,'1000');"></form:textarea></td>
          
        </tr>
        <tr>
          <td class="Label">URL</td>
          <td colspan="3"><form:input path="url" type="text" name="url" id="url" onkeyup="checkMaxLength(this,'1000');"/></td>
        </tr>
        <tr>
	           <td class="Label">Product</td>
			   <td> 
				<form:input path="productName" type="text" 
				name="couponName" id="couponName"  readonly="true"/><a href="#"><img
				src="/ScanSeeWeb/images/searchIcon.png" alt="Search"
				width="17" height="20"
				onclick="checkAssociatedProd();"
				title="Click here to View Product List" /> </a>
				</td>
				
				<td class="Label"><label for="bcategory" class="mand">Business Category</label></td>
						<td align="left">
						<form:errors cssStyle="color:red" path="bCategory"></form:errors> 
						<form:select path="bCategory" id="bCategory" class="selecBx" >
							<form:option value="0" label="--Select--">--Select-</form:option>
								<c:forEach items="${sessionScope.categoryList}" var="c">
											<form:option value="${c.categoryID}" label="${c.parentSubCategory}" />
								</c:forEach>
					    </form:select>
				</td>
		</tr>
        <tr>
		  <td class="Label"><label for="dealStDate" class="mand">Deal Start Date</label></td>
          <td align="left">
           <form:errors cssStyle="color:red" path="dealStartDate"></form:errors>
          <form:input path="dealStartDate" name="startDT" id="datepicker1" class="textboxDate"/>(mm/dd/yyyy)
		 
		  </td>
		  <td class="Label"><label for="dealEndDate" class="mand">Deal End Date</label></td>
          <td>
            <form:errors cssStyle="color:red" path="dealEndDate"></form:errors>
          <form:input path="dealEndDate" name="endDT" id="datepicker2" class="textboxDate"/>(mm/dd/yyyy)
		
		  </td>
        </tr>
		<tr>
			<td class="Label"><label for="cst">Deal Start Time</label></td>
			<td><form:select path="dealStartHrs" class="slctSmall">
			<form:options items="${DealStartHours}" />
			</form:select> Hrs <form:select path="dealStartMins" 
			class="slctSmall">
			<form:options items="${DealStartMinutes}" />
			</form:select> Mins</td>
			<td class="Label"><label for="cet">Deal End Time</label>
			</td>
			<td><form:select path="dealEndhrs" 
			class="slctSmall">
			<form:options items="${DealStartHours}" />
			</form:select> Hrs <form:select path="dealEndMins" 
			class="slctSmall">
			<form:options items="${DealStartMinutes}" />
			</form:select> Mins
			</td>
		</tr>
		
		<tr>
		
		<td class="Label"><label for="timeZone">Time Zone</label></td>
							<td><form:select path="dealTimeZoneId" class="selecBx">
								<form:option value="0" label="--Select--">Please Select Time Zone</form:option>
								<c:forEach items="${sessionScope.timeZoneslst}" var="tz">
						<form:option value="${tz.timeZoneId}" label="${tz.timeZoneName}" />
					</c:forEach>
								
							</form:select>
		</tr>
        <tr>
          <td class="Label">&nbsp;</td>
          <td><input name="slctOpt" type="radio" value="slctCty" id="slctCty" />
            Deal for Specific City?</td>
          <td align="center" class="Label"><strong>OR</strong></td>
          <td><input type="radio" name="slctOpt" value="slctLoc" id="slctLoc" />
            Deal for Specific Location?</td>
        </tr>
		<tr class="slctCty">
			<td class="Label">&nbsp;</td>
			<td colspan="3" class="Label" align="left"><form:errors cssStyle="color:red" path="city"></form:errors> 
				<div id="myAjax1" class="floatL">
						<form:select path="city" id="City" class="textboxBig" name="city">
										<form:option value="0" label="">Please Select Population Center</form:option>
									</form:select>
				</div><span class="floatL"><label class="mand" for="dealStDate">&nbsp;</label></span>
			</td>
		</tr>
		<tr class="slctLoc">
            <td class="Label">&nbsp;</td>
        	<td><form:errors cssStyle="color:red" path="retailID"></form:errors>
        		<div id="myAjax2" class="floatL">
        			<form:select path="retailID" class="selecBx" id="retailID">
						<form:option value="0">--Select Business--</form:option>
						</form:select>
				</div><span class="floatL"><label class="mand" for="dealStDate">&nbsp;</label></span>
			</td>
			<td colspan="2"><form:errors cssStyle="color:red" path="retailerLocID"></form:errors>
				<div id="myAjax3" class="floatL">
					<form:select path="retailerLocID" id="retailerLocID">
						<form:option value="0">--Select--</form:option>
					</form:select>
				</div><span class="floatL"><label class="mand" for="dealStDate">&nbsp;</label></span>
			</td>
		</tr>
		      </table>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
			title="Click here to Close" align="middle" /> 
			<span id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color: White"> </iframe>
		</div>
      </form:form>
    <div class="navTabSec RtMrgn" align="right">

         <!--  <input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Cancel3"/>-->
         <input class="btn" onclick="location.href='/ScanSeeWeb/hotDeal.htm'" value="Back" type="button" name="Cancel3"/>
          <input class="btn" onclick="previewSupplierHotDeals();" value="Preview" type="button" name="Preview"/>
          <input class="btn" onclick="hotDealAddSupplier();" value="Submit" type="button" name="Cancel"/>

      </div>
    </div>
  </div>
  <div class="clear"></div>
</div>
<script>
<!-- On load call to fetch city values -->
	onCategoryLoad();
</script>

<script type="text/javascript">
	var dealForCityLoc = document.addhotdealform.dealForCityLoc.value;
	if (dealForCityLoc == 'City') {
		document.getElementById('slctCty').checked = 'checked';
		loadPopulationCenters();
	} else if (dealForCityLoc == 'Location') {
		document.getElementById('slctLoc').checked = 'checked';
		loadPdtRetailer();
	}
</script>
<script type="text/javascript">
	loadRetLoc();
	function loadRetLoc() {
		loadRetailerLocation();
	}
</script>
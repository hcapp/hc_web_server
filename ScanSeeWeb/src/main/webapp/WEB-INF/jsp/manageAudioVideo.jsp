<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Coupons</title>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.jscroll.js"
	type="text/javascript"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.MultiFile.js"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

</head>
<body class="whiteBG" >
<form:form name="manageform" commandName="manageform" method="POST" enctype="multipart/form-data" modelAttribute="manageform" action="managemedia.htm">
<form:hidden path="productID" name="productID"/>
<form:hidden path="mediaType" name="type"/>
<div class="contBlock">
    <fieldset>
    <legend>Images</legend>
    <div class="contWrap attributesGrdNS">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn">
	  <tbody>
	      <c:set var="indexnum" value="1" />
	  <c:if test="${ProductMediaList ne null && ! empty ProductMediaList}">
	   <c:forEach items="${sessionScope.ProductMediaList}" var="prodattributes" >
	   <c:if test="${prodattributes.productImagePath ne null && ! empty prodattributes.productImagePath}">	
					
		<tr>
			<td width="3%"><c:out value='${indexnum}' /></td>
            <td width="76%"><c:out value="${prodattributes.productImagePath}" /></td>
			<c:set var="indexnum" value="${indexnum+1}" />
         </tr>
          </c:if>
	   </c:forEach>  
	    </c:if> 
        </tbody>
      </table>
    </div>
    </fieldset>
<fieldset class="popUpSrch">
  <legend>Upload Images</legend>
  <div class="contWrap">
   
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn" id="manageAttbut">
<form:hidden  path="imageCount"/>
<tbody>
	<tr>
	<td width="20%" style="border-right:none!important;">
	<form:input class="multi" accept="png" maxlength="5"  type="file" path="imageFile" name="imageFile"/><span class="instTxt nonBlk">[ Please Upload .png format images ]</span>
	</td><form:errors path="imageFile" cssStyle="color:red"></form:errors>
	</tr>
	<tr>
	<td><input class="btn" type="button"  name="upldBtn" value="Choose File"  onclick="uploadProductImage();"/></td>
	</tr>
</tbody>
</table>

</div>
</fieldset>
 <fieldset>
    <legend>Audio</legend>
    <div class="contWrap attributesGrdNS">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn">
 
	  <tbody>
	  <c:set var="indexnum" value="1" />
	  <c:if test="${ProductMediaList ne null && ! empty ProductMediaList}">
	   <c:forEach items="${sessionScope.ProductMediaList}" var="prodattributes" >
	   <c:if test="${prodattributes.audioPath ne null && ! empty prodattributes.audioPath}">								
		<tr>								
			<td width="3%"><c:out value='${indexnum}' /></td>
            <td width="76%"><c:out value="${prodattributes.audioPath}" /></td>
			<c:set var="indexnum" value="${indexnum+1}" />
         </tr>
          </c:if> 
	   </c:forEach>  
	   </c:if>
        </tbody>
      </table>
    </div>
    </fieldset>
	<fieldset class="popUpSrch">
  <legend>Upload Audio</legend>
  <div class="contWrap">
    
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn" id="manageAttbut">
     <form:hidden  path="audioCount"/>
<tbody>
	<tr>
	<td width="20%" style="border-right:none!important;"><form:input path="audioFile"  class="multi" accept="mp3" maxlength="5" type="file" name="audioFile" /><span class="instTxt nonBlk">[ Please Upload .mp3 format file ]</span></td><form:errors path="audioFile" cssStyle="color:red"></form:errors>
	</tr>
	<tr>
	<td><input class="btn" type="button"  name="upldBtn" value="Upload File" onclick="UploadProdAudioVideo('Audio');"/></td>
	</tr>
</tbody>
</table>

</div>
</fieldset>
<fieldset>
    <legend>Video</legend>
    <div class="contWrap attributesGrdNS">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn">
       
	  <tbody>
	  <c:set var="indexnum" value="1" />
	  <c:if test="${ProductMediaList ne null && ! empty ProductMediaList}">
	   <c:forEach items="${sessionScope.ProductMediaList}" var="prodattributes" >
	    <c:if test="${prodattributes.videoPath ne null && ! empty prodattributes.videoPath}">
		<tr>
		    
			<td width="3%"><c:out value='${indexnum}' /></td>
            <td width="76%"><c:out value="${prodattributes.videoPath}" /></td>
			<c:set var="indexnum" value="${indexnum+1}" />
         </tr>
         </c:if>
	   </c:forEach>  
	    </c:if> 
        </tbody>
      </table>
    </div>
    </fieldset>
	<fieldset class="popUpSrch">
  <legend>Upload video</legend>
  <div class="contWrap">
   
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn" id="manageAttbut">
<form:hidden  path="videoCount"/>
<tbody>
	<tr>
	<td width="20%" style="border-right:none!important;"><form:input path="videoFile"  class="multi" accept="mp4" maxlength="5" type="file" name="videoFile" /><span class="instTxt nonBlk">[ Please Upload .mp4 format files ]</span></td><form:errors path="videoFile" cssStyle="color:red"></form:errors>
	</tr>
	<tr>
	<td><input class="btn" type="button"  name="upldBtn" value="Upload File" onclick="UploadProdAudioVideo('Video');"/></td>
	</tr>
</tbody>
</table>

</div>
</fieldset>
</form:form>
</body>
</html>

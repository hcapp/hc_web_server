<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/autocomplete.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
	function popUp(URL) {
		day = new Date();
		id = day.getTime();
		eval("page"
				+ id
				+ " = window.open(URL, '"
				+ id
				+ "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=1000,height=600,left = 390,top = 162');");
	}
// End -->
</script>

<script>
$(document).ready(function() {
	$("#City").live("keypress",function(e)
			{
			    var s = String.fromCharCode(e.which);
			    if (s.match(/[a-zA-Z\.]/)){
			       cityAutocomplete('pstlCd');
				}else if(s.match(/[\b]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).html('<option id = "state">----------------------Select---------------------</option>');
					$('#pstlCd').val("");
					cityAutocomplete('pstlCd');
				}
					
			});
var stateCode = '';
var state = '';
if($('#pstlCd').val().length == 5){
	stateCode = $('#stateCodeHidden').val();
	state = $('#stateHidden').val();
	$( "#Country" ).html('<option id = "state" value='+stateCode+'>'+ state +'</option>');				 
	}else{
	$('#stateCodeHidden').val("");
	$('#stateHidden').val("");
	$( "#Country" ).html('<option id = "state">----------------------Select---------------------</option>');
	}
});
</script>
<script type="text/javascript">
	/*function loadCity() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		var selCity = document.createshopperprofileform.cityHidden.value;

		$
				.ajax({
					type : "GET",
					url : "/ScanSeeWeb/retailer/retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : selCity
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.createshopperprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						if (cityList.length > 1) {
							document.createshopperprofileform.City.options.length = cityList.length + 1;
							for ( var i = 0; i < cityList.length; i++) {
								cityDropDown.options[optIndex].value = cityList[i];
								cityDropDown.options[optIndex].text = cityList[i];
								optIndex++;
							}
						}

						//$('#myAjax').html(response);
						onCitySelectedLoad();
					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}

	function onChangeState() {
		// get the form values
		var stateCode = $('#Country').val();
		var tabIndex = $('#tabindex').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		document.createshopperprofileform.cityHidden.value="";
		$
				.ajax({
					type : "GET",
					url : "/ScanSeeWeb/retailer/retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : "",
						'tabIndex' : tabIndex
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.createshopperprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						cityDropDown.options[0].selected = true;

						for ( var i = 0; i < cityList.length; i++) {
							cityDropDown.options[optIndex].value = cityList[i];
							cityDropDown.options[optIndex].text = cityList[i];
							optIndex++;

						}

					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}*/

	function getCityTrigger(val) {	
		if(val == ""){
			$( "#Country" ).html('<option id = "state">----------------------Select---------------------</option>');
			$( "#pstlCd" ).val( "" );
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		document.createshopperprofileform.cityHidden.value = val;
		document.createshopperprofileform.city.value = val;
	}
	function clearOnCityChange() {	
		//alert($('#citySelectedFlag').val())
		if($('#citySelectedFlag').val() != 'selected'){
		$( "#Country" ).html('<option id = "state">----------------------Select---------------------</option>');
		$( "#pstlCd" ).val( "" );
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
		}else{
		$('#citySelectedFlag').val('');
		}
}
	function isEmpty(vNum) {
		if(vNum.length < 5){
			//$( "#state" ).val("");
			$( "#Country" ).html('<option id = "state">----------------------Select---------------------</option>');
			$( "#City" ).val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		return true;
	}

	/*function onCitySelectedLoad() {
		var vCityID = document.createshopperprofileform.cityHidden.value;
		var sel = document.getElementById("City");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCityID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}*/

	function clearShopperForm() {
		var r = confirm("Are you really want to clear the form")

		if (r == true) {
			//document.createshopperprofileform.reset();

			document.createshopperprofileform.firstName.value = "";
			document.createshopperprofileform.lastName.value = "";
			document.createshopperprofileform.address1.value = "";
			document.createshopperprofileform.address2.value = "";
			document.createshopperprofileform.state.value = "0";
			document.createshopperprofileform.city.value = "0";
			document.createshopperprofileform.postalCode.value = "";
			document.createshopperprofileform.mobilePhone.value = "";
			document.createshopperprofileform.email.value = "";

			document.createshopperprofileform.userName.value = "";
			document.createshopperprofileform.password.value = "";
			document.createshopperprofileform.rePassword.value = "";
			document.createshopperprofileform.terms.checked = false;

		}

	}
	function callPreferences() {
		document.createshopperprofileform.action = "preferences.htm";
		document.createshopperprofileform.method = "POST";
		document.createshopperprofileform.submit();

	}
</script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">

		<!--<div id="banner"><img src="images/scansee-banner_2.png" alt="Banner" width="1000" height="289" border="0" usemap="#Map" />
    <map name="Map" id="Map">
      <area shape="rect" coords="190,235,375,270" href="#" alt="Download Free App" title="Download Free App" />
      <area shape="poly" coords="881,6" href="#" />
      <area shape="poly" coords="1036,101" href="#" />
      <area shape="poly" coords="887,6" href="#" />
      <area shape="poly" coords="882,3,930,3" href="#" />
      <area shape="poly" coords="883,6" href="#" alt="Sign Up" />
    </map>
  </div>-->

		<div id="dockPanel">
			<ul class="tabs" id="prgMtr">
				<!--<li><a href="/ScanSeeWeb/welcome.htm" rel="home" title="Home">Home</a></li>-->

				<!-- <li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
				target="_blank" rel="About ScanSee">About ScanSee</a></li> -->
				<li><a href="/ScanSeeWeb/" rel="About You" title="About You"
					class="tabActive">About You</a></li>
				<!--<li><a href="/ScanSeeWeb/shopper/preferences.htm" rel="Preferences"
				title="Preferences">Preference s</a></li>
			<li><a href="/ScanSeeWeb/shopper/selectschool.htm" rel="Pick Your Charity/School"
				title="Pick Your Charity/School">Select School</a></li>
			<li><a href="/ScanSeeWeb/shopper/doublecheck.htm" rel="Double Check"
				title="Double Check">Double Check</a></li>

			
			<li><a href="/ScanSeeWeb/shopper/shop.htm" rel="Shop!" title="Shop!">Shop</a></li>-->
			</ul>


			<div id="tabdPanelDesc" class="floatR tglSec">
				<div id="filledGlass">
					<img src="/ScanSeeWeb/images/Step3.png" />
					<div id="nextNav">

						<a href="#" onclick="validateShooperProfile();"><img
							src="/ScanSeeWeb/images/nextBtn.png" class="NextNav" /><span>Next</span>

						</a>
					</div>
				</div>
			</div>





			<div id="tabdPanel" class="floatL tglSec">
				<img src="/ScanSeeWeb/images/ShopperFlow1.png" alt="Shopper" />
			</div>

		</div>
		<!-- <div id="togglePnl">
			<a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down"
				width="9" height="8" /> Show Panel</a>
		</div> -->
		<div class="clear"></div>
		<div id="content" class="topMrgn">
			<div class="section topMrgn">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="brdrLsTbl">
					<tr>
						<td width="50%"><h5>Please tell us about yourself so
								HubCiti can deliver savings today!</h5>
						</td>
						<!--<td width="7%"><img src="/ScanSeeWeb/images/fb.jpeg"
							width="50" height="49" />
						</td>
						<td width="43%" align="left" valign="top">One-Click Register
							with<br /> Facebook Account.</td>-->
					</tr>
				</table>
				<div class="grdSec brdrTop">
					<form:form commandName="createshopperprofileform"
						name="createshopperprofileform" acceptCharset="ISO-8859-1">
						<form:hidden path="cityHidden" />
						<form:hidden path="stateHidden" id ="stateHidden"/>
						<form:hidden path="stateCodeHidden" id = "stateCodeHidden" />
						<form:hidden path="citySelectedFlag" id = "citySelectedFlag" />
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="grdTbl">
							<tr>
								<td class="header">Regisration Form</td>
								<td colspan="3" style="color: red;"><form:errors
										cssStyle="color:red"></form:errors>
								</td>
							</tr>
							<tr>
								<td class="Label"><label class="mand" for="rtlrName">First
										Name </label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="firstName" cssClass="error"></form:errors> <form:input
										path="firstName" type="text" name="couponName3" id="rtlrName"
										maxlength="20" tabindex="1" />
								</td>
								<td class="Label"><label class="mand" for="rtlrName">Last
										Name</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="lastName" cssClass="error"></form:errors> <form:input
										path="lastName" type="text" id="rtlrName" maxlength="50"
										tabindex="2" /></td>
							</tr>
							<tr>
								<td width="17%" class="Label"><label class="mand"
									for="addrs1">Address1</label>
								</td>
								<td width="33%"><form:errors cssStyle="color:red"
										path="address1"></form:errors> <form:textarea path="address1"
										name="textarea2" id="addrs1" cols="45" rows="5" tabindex="3"
										class="txtAreaSmall" onkeyup="checkMaxLength(this,'50');"
										cssStyle="height:60px;"></form:textarea>
								</td>
								<td class="Label"><label for="addrs3">Address2</label>
								</td>
								<td><form:textarea path="address2" name="textarea"
										tabindex="4" id="addrs3" cols="45" rows="5"
										class="txtAreaSmall" onkeyup="checkMaxLength(this,'50');"
										cssStyle="height:60px;"></form:textarea>
								</td>
							</tr>

														<tr>
								<td align="left" class="Label">
									<label class="mand"	for="pstlCd">Postal Code</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="postalCode"></form:errors>
									<form:input path="postalCode" type="text" class="loadingInput" maxlength="5" tabindex="7" name="couponName7"
										id="pstlCd" onkeypress="zipCodeAutocomplete('pstlCd');return isNumberKey(event)" onchange="isNumeric(this.value);" 
										onkeyup="isEmpty(this.value);"/>
								</td>
								<td class="Label"><label class="mand" for="cty">City</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="city"></form:errors> 
										<form:input path="city" id="City" tabindex="5" class="loadingInput"/>
								</td>
							</tr>
							<tr>
								<td class="Label" align="left">
									<label for="sts" class="mand">State</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="state"></form:errors> 
									<form:select path="state" class="selecBx" id="Country" tabindex="6" disabled="true">						
									</form:select>
								</td>
								
								<td align="left" class="Label"><label for="phnNum">Phone
										#</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="mobilePhone"></form:errors> <form:input
										path="mobilePhone" type="text" tabindex="8" name="couponName8"
										id="phnNum" onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);" /><label
									class="Label">(xxx)xxx-xxxx</label>
								</td>
							</tr>

							<tr>
								<td class="Label"><label class="mand" for="contEml">Contact
										Email</label>
								</td>
								<td align="left" colspan="3"><form:errors
										cssStyle="color:red" path="email"></form:errors> <form:input
										path="email" tabindex="9" type="text" name="contactemail"
										id="contEml" maxlength="100" />
								</td>
							<tr>
								<td class="Label"><label class="mand" for="rtlrName">UserName</label>
								</td>
								<td colspan="3" align="left"><form:errors
										cssStyle="color:red" path="userName"></form:errors> <form:input
										path="userName" type="text" maxlength="100" tabindex="10"
										name="userName" id="userName" />
								</td>
							</tr>

							<tr>
								<td class="Label"><label class="mand" for="rtlrName">Password</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="password"></form:errors> <form:input path="password"
										name="password" type="password" id="rtlrName" maxlength="100"
										tabindex="11" />
								</td>
								<td class="Label" colspan="2"><label>Must be at
										least 8 characters long and have at least one number.</label>
								</td>


							</tr>
							<tr>
								<td class="Label"><label class="mand" for="rtlrName">Confirm
										Password</label>
								</td>
								<td align="left" colspan="3"><form:errors
										cssStyle="color:red" path="rePassword"></form:errors> <form:input
										path="rePassword" name="rePassword" type="password"
										id="rtlrName" maxlength="100" tabindex="12" />
								</td>

							</tr>
							<tr>
								<td class="Label">&nbsp;</td>

								<td colspan="2" align="left"><form:errors
										cssStyle="color:red" path="terms"></form:errors> <form:checkbox
										path="terms" tabindex="13" /> <strong>Accept Terms </strong>
									<div id="acptTrms">
										<A HREF="http://www.scansee.com/links/privacypolicy.html"
											target="_blank" tabindex="14"> <u>Privacy Policy</u> </a>
									</div> <input class="btn" value="Clear" type="button"
									onclick="clearShopperForm();" name="Cancel2" tabindex="15"
									title="Clear the form" /> <input value="Submit" type="button"
									onclick="validateShooperProfile();" class="btn" tabindex="16"
									title="Submit" />
								</td>
							</tr>

						</table>
					</form:form>
					<div class="navTabSec RtMrgn"></div>
				</div>
			</div>
			<div class="clear"></div>
			<!--<div class="section topMrgn">
				<div class="block floatL">
					<h3>
						Funding Higher Education<span>Through Commerce Networking </span>
					</h3>
					<div class="section_info">
						Lorem Ipsum is simply dummy text of the printing and typesetting
						industry. Lorem Ipsum has been the industry's standard dummy text
						ever since the 1500s.
						<p class="topPadding linkMore">
							<img src="images/linkArrow.png" alt="LinkBullet" width="4"
								height="6" /> <a href="#">Click here for details</a>
						</p>
					</div>
				</div>
								<div class="block floatL clrMargin">
					<p>
						<img src="/ScanSeeWeb/images/scanseeMan.png" alt="ScanSee Man"
							align="absmiddle" />100% Happiness Guarantee
					</p>
					<p>
						<img src="/ScanSeeWeb/images/testimonial-img.png" alt="Testimonial" />
					</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
			<div class="clear"></div>
			</div>

			<div id="footer_info">Copyright � 2015 ScanSee. All rights
				reserved</div>-->


			<script>
			<!-- On load call to fetch city values -->
				//loadCity();
			</script>
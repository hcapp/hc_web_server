<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>HubCiti</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
</head>
<body class="whiteBG">
<div id="wrapper">
<div id="header">
  <div id="header_logo" class="floatL"> <a href="index.html"> <img alt="ScanSee" src="images/hubciti-logo_ret.jpg"/></a> </div>
  <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="grdSec">
  <div id="content" class="floatL">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
  <tr>
    <td colspan="2" class="title">QR code for your selection:</td>
  </tr>
  <tr>
    <td colspan="2">
    <ul class="qrImg">
    <c:if test="${requestScope.QRCodeDetails.productImagePath ne null && requestScope.QRCodeDetails.productImagePath ne ''}">
    	<li id="prod_qr"><img src="${requestScope.QRCodeDetails.productImagePath}" width="100" height="113"><span class="title1"><c:out value="${requestScope.productName}" /></span></li>
    	</c:if>
    	 <c:if test="${requestScope.QRCodeDetails.audioImagePath ne null && requestScope.QRCodeDetails.audioImagePath ne ''}">
        <li id="audio_qr"><img src="${requestScope.QRCodeDetails.audioImagePath}" width="100" height="113"><span class="title1"><c:out value="${requestScope.QRCodeDetails.audio}" /></span></li>
        </c:if>
         <c:if test="${requestScope.QRCodeDetails.videoImagePath ne null && requestScope.QRCodeDetails.videoImagePath ne ''}">
        <li id="video_qr"><img src="${requestScope.QRCodeDetails.videoImagePath}" width="100" height="113"><span class="title1"><c:out value="${requestScope.QRCodeDetails.video}" /></span></li>
        </c:if>
         <c:if test="${requestScope.QRCodeDetails.fileLinkImagePath ne null && requestScope.QRCodeDetails.fileLinkImagePath ne ''}">
        <li id="video_qr"><img src="${requestScope.QRCodeDetails.fileLinkImagePath}" width="100" height="113"><span class="title1"><c:out value="${requestScope.QRCodeDetails.fileLink}" /></span></li>
        </c:if>
        
    </ul>
</td>
    </tr>
  <tr>
    <td><img src="images/download.png" alt="download" width="16" height="16" /></td>
<td>To download, right-click on the image,
then select &quot;Save Picture As...&quot; from the pop-up menu.</td>
    </tr>
  <tr>
    <td width="4%"><img src="images/print.gif" alt="print" width="16" height="16" /></td>
    <td width="96%"><a href="#" onclick="window.print();return false;">Print</a></td>
  </tr>
  <tr>
    <td colspan="2"><input class="btn" onclick="TestAudio();history.back();" value="Back" type="button" name="back"/></td>
    </tr>
</table>

  </div>
  <div class="clear"></div>
</div>
<div class="clear"></div>
<div id="footer">
  <div id="ourpartners_info">
    <div class="clear"></div>
  </div>
</div>
</div>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Coupons</title>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.jscroll.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<script>
function saveAttributes(){

	var attributes = [];
	var values = [];
	var errorFlag = false;
	$("#formAttrDisp").find("tr").each(function(index) {
		// if(index > 0){
		var highlightObj = $($("#formAttrDisp").find("tr")[index]).children();
		errorFlag = false;
		highlightObj.find("input").each(function(rVal,value) {

			var inputTagVal = $(this)[0].value;
			var name = $(this).attr("name");
			attributes[index] = name;
			errorFlag = false;
			if(inputTagVal == '' || inputTagVal == 'undefined' || inputTagVal == 'null'){
				alert('Please Enter values for attribute ');
				errorFlag = true;
			}else{
				values[index] = inputTagVal;
			}
			

		});


	if(errorFlag){
		return false;
	}
		// }
	});
	
	var newAttributes = [];
	var newValues = [];
	if(!errorFlag){
		$("#attrDpsly").find("tr").each(function(index) {
		// if(index > 0){
		var highlightObj = $($("#attrDpsly").find("tr")[index]).children();
		errorFlag =false;
		highlightObj.find("input").each(function(rVal) {
			var inputTagVal = $(this)[0].value;
			var name = $(this).attr("name");
			newAttributes[index] = name;
			if(inputTagVal == '' || inputTagVal == 'undefined' || inputTagVal == 'null'){
				alert('Please Enter values for attribute');
				errorFlag = true;
			}else{
				var check = $.trim(inputTagVal)
				newValues[index] = check;
			}

		});
	if(errorFlag){
		return false;
	}
		// }
	});
	}
	
	if(!errorFlag){
			attributes.push(newAttributes);
			values.push(newValues);
			closeIframePopup('ifrmPopup','ifrm');
			document.myform.prodAttributeName.value = attributes.toString();
			document.myform.prodDisplayValue.value = values.toString();
			document.myform.action = "saveattributes.htm";
			document.myform.method = "POST";
			document.myform.submit();
	}
	
	
}
</script>
</head>
<body class="whiteBG">
<form:form name="myform" commandName="manageform" acceptCharset="ISO-8859-1">
<form:hidden path="prodAttributeName" />
<form:hidden path="prodDisplayValue" />
<form:hidden path="productID" />
<div class="contBlock">
    <fieldset>
    <legend>Attributes</legend>
    <div class="contWrap attributesGrd">
     
	 
	  <c:choose> 
	  <c:when test="${empty sessionScope.ProductAttrList}">
	  <label for="couponName">No Attributes Found</label>
	  </c:when>
	  <c:otherwise>
	   <table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn" id="formAttrDisp">
	  <tbody>
	  <c:forEach items="${sessionScope.ProductAttrList}" var="prodattributes">
		<tr>
            <td width="24%" class="Label"><label for="couponName">${prodattributes.prodAttributeName} </label></td>
            <td width="76%"><input name="${prodattributes.prodAttributesID}" type="text" id="couponName" value="${prodattributes.prodDisplayValue}" /></td>
         </tr>
	   </c:forEach> 
	     </tbody>
      </table>
	  </c:otherwise>
	  
	  </c:choose>
	         
      
    </div>
    </fieldset>

	<fieldset class="popUpSrch">
  <legend>Add Attributes</legend>
  <div class="contWrap">
    <form>
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn" id="manageAttbut">
      <tbody>
        <tr>
          <td width="28%" class="Label"><label for="label">Attribute </label></td>
          <td width="21%">  
			<select  class="attrList"  name="category" id="attrDropDown">
					<option selected="selected" value="">Please Select</option>
                <c:forEach items="${sessionScope.MasterList}" var="attList">
                <option  value="${attList.prodAttributesID}">${attList.prodAttributeName}</option>
				</c:forEach>
			</select>    
		  </td>
          <td width="15%">Attribute Content</td>
          <td width="35%"><input name="attrCont" type="text" id="attrCont" />
          <a href="#"> <img src="images/add.png" alt="add" title="add" width="16" height="16"  onclick="crtAtrr();" /></a></td>
        </tr>
      </tbody>
    </table>
</form>
<div class="attributesGrd tglDsply">    
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="grdTbl zeroBtmMrgn" id="attrDpsly">
      <tbody>
      </tbody>
    </table></div>
  </div>
<div class="navTabSec mrgnRt" align="right">
       
          <!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
          <input name="associate" value="Add"  type="button" class="btn" id="add" onclick="saveAttributes()" title="Add"/>
         
       
      </div>
  </fieldset>
  </div>
</form:form>	
</body>

</html>

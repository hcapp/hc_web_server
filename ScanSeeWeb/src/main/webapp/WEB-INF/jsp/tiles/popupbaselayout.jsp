
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link href="/ScanSeeWeb/styles/style.css" type="text/css" rel="stylesheet" />
<link href="/ScanSeeWeb/styles/ticker-style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/bubble-tooltip.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/validate.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>
<body class="whiteBG">
<div><tiles:insertAttribute name="body" /></div>
</body>
</html>

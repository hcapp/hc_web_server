<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.Date.*"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
	function isNumeric(vNum) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;
		var vPostal = document.getElementById("zipCode");
		for (i = 0; i < vNum.length && IsNumber == true; i++) {
			Char = vNum.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				vPostal.value = "";
				vPostal.focus();
				IsNumber = false;
			}
		}
		return IsNumber;
	}
</script>

<form name="paginationForm">
	<input type="hidden" name="pageNumber" /> <input type="hidden"
		name="recordCount" /> <input type="hidden" name="pageFlag" />
</form>


<form name="headerform">
	<input type="hidden" name="pagination" />
	<div id="header">
		<%
			String currentDate = null;
			try
			{
				DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
				Date date1 = new Date();
				currentDate = df.format(date1);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				// TODO: handle exception
			}
		%>
		<div id="usrAcces" class="floatR">
			<div class="ltCrv"></div>
			<div class="bg">
				<div class="cmnRws">
					<c:choose>
						<c:when test="${empty sessionScope.loginuser}">
							<ul>
								<li><a href="/ScanSeeWeb/login.htm" title="Log In">Log
										In</a>
								</li>
								<li><a href="/ScanSeeWeb/retailer/createRetailerProfile.htm"
									title="Sign Up">Sign Up</a>
								</li>
								<li class="last"><%=currentDate%></li>
								<li class="clear"></li>
							</ul>

							<span>
								<ul class="secRw">
									<li>Zip Code <input type="text" name="zipCode"
										id="zipCode" class="textboxDate"
										value="${requestScope.searchForm.zipCode}"
										onkeypress="return isNumberKey(event)"
										onchange="isNumeric(this.value);" maxlength="10" /></li>
									<li class="leftPdng">Search for <select name="searchType">
											<c:choose>
												<c:when test="${HomePageSearch=='products'}">
													<option value="products" selected="selected">Products
													</option>
													<option value="producthotdeal">Products & Hot
														Deals</option>
												</c:when>
												<c:when test="${HomePageSearch=='producthotdeal'}">
													<option value="producthotdeal" selected="selected">Products
														& Hot Deals</option>
													<option value="products">Products</option>
												</c:when>
												<c:otherwise>
													<option value="products">Products</option>
													<option value="producthotdeal">Products & Hot
														Deals</option>
												</c:otherwise>
											</c:choose>


									</select>&nbsp; <input type="text" class="textboxSrch " name="searchKey"
										value="${requestScope.searchForm.searchKey}" /></li>
									<li><input class="srchBtn" type="button" name="search"
										onclick="productSearch();" title="Search" /></li>
								</ul> </span>
						</c:when>
						<c:when test="${sessionScope.loginuser.userType == 1}">
							<ul>
								<li class="bgLnk">Welcome<i> <c:out
											value="${sessionScope.truncUsername}"></c:out> </i>
								</li>
								<li class="last"><%=currentDate%></li>
								<li class="last"><c:choose>
										<c:when test="${sessionScope.FaceBookLogin ne null}">
											<a href="/ScanSeeWeb/shopper/facebookSignout.htm"
												title="Logout"> <img
												src="/ScanSeeWeb/images/logoutIcon.gif" alt="Logout"
												title="Logout" /> Logout</a>
										</c:when>
										<c:otherwise>
											<a href="/ScanSeeWeb/logout.htm" title="Logout"><img
												src="/ScanSeeWeb/images/logoutIcon.gif" alt="Logout"
												title="Logout" /> Logout</a>
										</c:otherwise>
									</c:choose></li>
								<li class="clear"></li>
							</ul>
							<span>
								<ul class="secRw">
									<li class="zpcode bgLnk">Zip Code <input type="text"
										name="zipCode" class="textboxDate"
										value="${sessionScope.searchForm.zipCode}"
										onkeypress="return isNumberKey(event)" /></li>
									<li class="leftPdng">Search for products&nbsp; <input
										type="text" class="textboxSrch" name="searchKey"
										value="${sessionScope.searchForm.searchKey}" /></li>
									<li><input class="srchBtn" type="button" name="search"
										onclick="productSearch();" title="Search" /></li>
								</ul> </span>
						</c:when>
						<c:when
							test="${sessionScope.loginuser.userType == 2 || sessionScope.loginuser.userType == 3 || sessionScope.loginuser.userType == 4 }">
							<ul>
								<li class="bgLnk">Welcome<i> <c:out
											value="${sessionScope.truncFirstName}"></c:out> </i>
								</li>
								<li class="last"><%=currentDate%></li>
								<li class="last"><a href="/ScanSeeWeb/logout.htm"
									title="Logout"><img src="/ScanSeeWeb/images/logoutIcon.gif"
										alt="Logout" title="Logout" /> Logout</a>
								</li>
								<li class="clear"></li>
							</ul>
							<span>
								<ul class="secRw">
									<li class="leftPdng">Search for products&nbsp; <input
										type="text" class="textboxSrch" name="searchKey"
										value="${requestScope.searchForm.searchKey}"
										onkeydown="return event.keyCode != 13" /></li>
									<li><input class="srchBtn" type="button" name="search"
										onclick="productSearchForSupRet();" title="Search" /></li>
								</ul> </span>
						</c:when>
					</c:choose>
				</div>
				
			</div>
			<div class="rtCrv"></div>
		</div>
		<c:choose>
			<c:when test="${empty sessionScope.loginuser}">
				<div class="floatL" id="header_logo">
					<a href="http://www.hubcitiapp.com/"><img
						src="/ScanSeeWeb/images/hubciti-logo.png" alt="ScanSee"
						width="184" height="66" /> </a>
				</div>
				<div class="clear"></div>
				<c:if test="${ sessionScope.supplierReg == true}">
					<div id="header_nav">
						<ul>
							<li><a href="welcome.htm" class="current">HOW IT WORKS</a></li>
							<li><a href="welcome.htm">NEWS</a></li>
							<li><a href="welcome.htm">RETAILERS</a></li>
							<li><a href="welcome.htm">SUPPLIERS</a></li>
							<li class="last"><a href="welcome.htm">UNIVERSITY
									PROGRAMS</a></li>
						</ul>
					</div>
				</c:if>

				<div class="clear"></div>
			</c:when>
			<c:when test="${sessionScope.loginuser.userType == 2}">
				<div id="header_logo" class="floatL">
					<a href="#"> <img src="images/hubciti-logo_supplier.png"
						alt="ScanSee" width="320" height="69" /> </a>
				</div>
			</c:when>
			<c:when test="${sessionScope.loginuser.userType == 3}">
				<div id="header_logo" class="floatL">
					<a href="#"> <img src="/ScanSeeWeb/images/hubciti-logo_ret.jpg"
						alt="ScanSee" width="361" height="63" /> </a>
				</div>
			</c:when>
			<c:otherwise>
				<div id="header_logo" class="floatL">
					<a href="#"> <img src="/ScanSeeWeb/images/hubciti-logo.png"
						alt="HubCiti" width="184" height="66" /> </a>
				</div>
			</c:otherwise>
		</c:choose>

	</div>

</form>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="java.util.*"%>
<%@page session="true"%>
<%@ page import="common.pojo.ProductVO"%>

<%@ page import="common.pojo.HotDealInfo"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


      
      <form:form  name="productform" commandName="productinfoform">
		<form:hidden path="retailLocationID"/>
		<form:hidden path="productID"/>
		

		<div id="content">
		  <div id="subnav">
        <ul>
          <li><a onclick="searchProductWithDeals();" class="${requestScope.prodcTabActive== 1? 'active':''}" id="hotdealList" ><span> Products</span></a></li>
          <li>
           
          <a href="javascript:void(0);" class="${requestScope.dealTabActive== 1? 'active':''}" id="hotdealList" onclick="searchDeals();" id="hotdealList" ><span>Hot Deals</span></a>
          </li>
        </ul>
      </div>
      
			<div class="grdCont grdSec">
				<table class="brdrLsTbl" border="0" cellspacing="0" cellpadding="0"
					width="100%" id="srchTbl">
					<tbody>
						<tr>
						
							<td colspan="2">Search Results for: ${requestScope.searchForm.searchKey}<b><font size=3>
							</font> </b></td>
						
							
						</tr>
						
						<tr>
						<c:if test="${sessionScope.pagination.totalSize >0 && sessionScope.pagination.currentPage==1 && sessionScope.pagination.totalSize > 20}">
							<td colspan="2">${sessionScope.pagination.totalSize} results found, top ${sessionScope.pagination.pageRange} displayed<br>
								<br></td>
							</c:if>
						</tr>
				
				       <tr>
						<c:if test="${sessionScope.pagination.totalSize >0 && sessionScope.pagination.currentPage==1 && sessionScope.pagination.totalSize <=20 }">
							<td colspan="2">${sessionScope.pagination.totalSize} results found<br>
								<br></td>
							</c:if>
						</tr>
					<c:choose>
							<c:when
								test="${sessionScope.seacrhList.searchType == 'producthotdeal'}">
							<tr class="subheader">
								<td colspan="2"><strong>Products List:</strong></td>
							</tr>
							<div align="center" style="font-style:45"><label><form:errors cssStyle="color:red"/></label></div>
							<c:forEach items="${sessionScope.seacrhList.productList}"
									var="item">
									
									<tr>
									   
										<td width="11%"><img width="80" height="50"
											src="${item.productImagePath}" onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';"></td>
											
										<td width="89%"><a id="searchinfo" style="cursor: pointer;" 
											onclick="productInfoWithDeals(${item.productID})"><b>${item.productName}</b>
										</a>  <span><c:out
													value="${item.productLongDescription}" /> </span>
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
							<tr class="subheader">
						<td colspan="2"><strong>Deals List:</strong>
						</td>
					</tr> 
					<div align="center" style="font-style:45"><label><form:errors cssStyle="color:red"/></label></div>
					      <form:hidden path="hotDealID"/>
								<c:forEach items="${sessionScope.seacrhList.dealList}"
									var="item">
									<tr>
										<td width="11%">
											<img width="80" height="50" src="${item.dealImgPath}" onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';">			
										</td>
										<td width="89%"><a  style="cursor: pointer;" onclick="productDealInfo(${item.hotDealID})">
										<b>${item.hotDealName}</b>
										</a>  <span><c:out
													value="${item.hotDealShortDescription}" /> </span>
										</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>

					</tbody>
				</table>
			</div>
			<div class="pagination">
			
				<p>
					<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
						nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
						pageRange="${sessionScope.pagination.pageRange}" url="${sessionScope.pagination.url}" enablePerPage="false" />
				</p>
			</div>
			<div class="clear"></div>
				<div  class="navTabSec mrgnRt" align="right">
      <div align="center">
        <input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Cancel"/>
      </div>
    </div>
		</div>
	</form:form>
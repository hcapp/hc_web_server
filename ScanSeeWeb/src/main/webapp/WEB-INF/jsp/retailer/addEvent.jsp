<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>



<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet" type="text/css">
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.form.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/cityAutocomplete.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/zipcodeAutocomplete.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<script src="/ScanSeeWeb/ckeditor/adapters/jquery.js"></script>

<script>
$(".evntLoctn").show();
var retailerId = $("#retailerId").val();
var locId = $("#hiddenLocationId").val();		

if(null != retailerId && "" != retailerId) {
	getLogRetailerLocs(retailerId, locId);	
} 

	$(document).ready( function() {		

		CKEDITOR.on('instanceCreated', function(e) {
			//	alert("q : "+e.editor.name);
			var editorName = e.editor.name;
			document.getElementById('longDescription').innerHTML = '';
		});
		CKEDITOR.config.uiColor = '#FFFFFF';
		CKEDITOR.replace('longDescription',{
			/* filebrowserBrowseUrl: '/browser/browse.php', */
			/* filebrowserImageBrowseUrl : '/ScanSeeWeb/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://localhost:8080/ScanSeeWeb/ckeditor/filemanager/connectors/php/connector.php', */
			/* filebrowserUploadUrl: 'http://localhost:8080/ScanSeeWeb/ckeditor/filemanager/connectors/php/upload.php?Type=Image', */
			/* filebrowserImageUploadUrl: '/uploader/upload.php?type=Images', */
			extraPlugins : 'onchange',
			width : "100%",
			toolbar : [
					/* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
					{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
					{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
					'/',
					{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
					{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
					{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
					{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
					'/',
					{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
					{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
					{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
					{
						name : 'basicstyles',
						items : [ 'Bold',
								'Italic',
								'Underline' ]
					},
					{
						name : 'paragraph',
						items : [
								'JustifyLeft',
								'JustifyCenter',
								'JustifyRight',
								'JustifyBlock' ]
					},
					'/',
					{
						name : 'styles',
						items : [ 'Styles',
								'Format' ]
					},
					'/',
					{
						name : 'tools',
						items : [ 'Font',
								'FontSize',
								'RemoveFormat' ]
					},
					'/',
					{
						name : 'colors',
						items : [ 'BGColor' ]
					},
					{
						name : 'paragraph',
						items : [ 'Outdent',
								'Indent' ]
					},
					{
						name : 'links',
						items : [ 'Link',
								'Unlink' ]
					}, /* {
						name : 'insert',
						items : [ 'Image' ]
					}, */ ],
			removePlugins : 'resize'
		});
		
		/*To place long description data inside CKEditor*/
		var longDescription = document.addediteventform.hidLongDescription.value;
		if(null != longDescription && "" != longDescription)	{
			$("#longDescription").val(longDescription);
		} 
		
						$('#retlocationID').change( function() {
							var totOpt = $(this).find("option").length;
							var totOptSlctd = $(this).find(
									"option:selected").length;
							if (totOpt == totOptSlctd) {
								$('input[name$="chkAllLoc"]').attr(
										'checked', 'true');
							} else {
								$("#chkAllLoc").removeAttr('checked');
							}
						});

						$("#retlocationID").change(
								function() {
									var totOpt = $(this).find("option").length;
									var totOptSlctd = $(this).find(
											"option:selected").length;
									if (totOpt == totOptSlctd) {
										$('input[name$="chkAllLoc"]').attr(
												'checked', 'true');
									} else {
										$("#chkAllLoc").removeAttr('checked');
									}
								});

						$("#City").live("keydown", function(e) {
							cityAutocomplete('pstlCd');
						});

						var geoError = $("#geoError").val();
						if (geoError == 'true') {
							//if (geoError) {
							$("#dispLatLang").show();
						} else {
							$("#dispLatLang").hide();
						}

						$("input[name='recurrencePatternID']")
								.on(
										'click',
										function() {
											var patternName = $(this).attr(
													"patternName");
											//var hiddenDays = document.addediteventform.hiddenDays.value;

											if (patternName == "Daily") {
												$('input:radio[name=isOngoingDaily]')[0].checked = true;
												document.addediteventform.everyWeekDay.value = 1;

												if (document
														.getElementById('everyWeekDay.errors') != null) {
													document
															.getElementById('everyWeekDay.errors').style.display = 'none';
												}
											} else if (patternName == "Weekly") {
												document.addediteventform.everyWeek.value = 1;
												var hiddenDays = document.addediteventform.hiddenDay.value;
												if (null != hiddenDays
														&& "" != hiddenDays) {
													$('input[name="days"]')
															.prop('checked',
																	false);
													var arr = hiddenDays
															.split(',');

													jQuery.each(arr, function(
															i, val) {
														val = val.replace(
																/\s+/g, '');
														$('#days' + val).prop(
																'checked',
																'checked');
													});
												}

												if (document
														.getElementById('days.errors') != null) {
													document
															.getElementById('days.errors').style.display = 'none';
												}
												if (document
														.getElementById('everyWeek.errors') != null) {
													document
															.getElementById('everyWeek.errors').style.display = 'none';
												}

											} else if (patternName == "Monthly") {
												$('input:radio[name=isOngoingMonthly]')[0].checked = true;
												document.addediteventform.everyMonth.value = 1;
												document.addediteventform.everyDayMonth.value = 1;
												document.addediteventform.dateOfMonth.value = document.addediteventform.hiddenDate.value;
												document.addediteventform.dayNumber.value = document.addediteventform.hiddenWeek.value;
												document.addediteventform.everyWeekDayMonth.value = document.addediteventform.hiddenWeekDay.value;

												if (document
														.getElementById('dateOfMonth.errors') != null) {
													document
															.getElementById('dateOfMonth.errors').style.display = 'none';
												}
												if (document
														.getElementById('everyMonth.errors') != null) {
													document
															.getElementById('everyMonth.errors').style.display = 'none';
												}
												if (document
														.getElementById('everyDayMonth.errors') != null) {
													document
															.getElementById('everyDayMonth.errors').style.display = 'none';
												}
											}
											dsplyRecur(this);
										});

						/*Range of recurrence: restrict input to 3 chars & accept only digits*/
						$('.numeric').on('input', function(event) {
							this.value = this.value.replace(/[^0-9]/g, '');
						});

						function dsplyRecur(obj) {
							document.addediteventform.recurrencePatternName.value = $(
									obj).attr("patternName");
							;
							var recurId = $(obj).attr("id");
							//var ongTrgr = $("#onGoing").prop("checked");
							$(".recur").hide();
							$(".recurrence").show();
							$("." + recurId).css("display", "block");
						}

						$("#datepicker1,#datepicker2,#datepicker3,#datepicker4")
								.datepicker(
										{
											showOn : 'both',
											buttonImageOnly : true,
											buttonText : 'Click to show the calendar',
											buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
										});

						var hiddenCategory = document.addediteventform.hiddenCategory.value;
						if (null != hiddenCategory && "" != hiddenCategory) {
							$('#eventCategory').val(hiddenCategory).change();
						}

						/* ongoing event option change display related form elemnts */
						$("input[name='isOngoing']")
								.on(
										'change.trgrOngng',
										function() {
											var evntOptn = $(this)
													.attr('value');
											if (evntOptn == "yes") {
												$("tr.ongoing").show();
												$(".ongoing").show();
												$(".scngRow,.not-ongng").hide();
												dsplyRecur("input[name='recurrencePatternID']:checked");
												//$("input[name='recurrencePatternID']:checked").trigger('click');
											} else if (evntOptn === "no") {
												$(".ongoing,.scngRow").hide();
												$(".not-ongng").show();
												$("#menu-pnl").height(
														$(".content").height())
														.trigger("resize");
											}
										});
						$('input[name="isOngoing"]:checked').trigger('change');
						// <!-- ashrith -->
						$("input[name='bsnsLoc']").change(function() {
							var reqOptn = $(this).attr('value');
							//var getId = $(this).attr('id');
							if (reqOptn === "yes") {
								$(".bsnsLoctn").show();

							} else if (reqOptn === "no") {
								$(".bsnsLoctn").hide();
								$(".evntLoctn").slideDown();
							}
						});

						$("input[name='bsnsLoc']:checked").trigger('change');

						var hiddenWeekDay = document.addediteventform.hiddenWeekDay.value;
						if (null != hiddenWeekDay && "" != hiddenWeekDay) {
							$('input[name="everyWeekDayMonth"]').prop(
									'checked', false);
							var arr = hiddenWeekDay.split(',');
							jQuery.each(arr, function(i, val) {
								val = val.replace(/\s+/g, '');
								$('#everyWeekDayMonth' + val).prop('checked',
										'checked');
							});
							document.addediteventform.hiddenWeekDay.value = '';
							updateCstmSlct();
						}

					});
	function logRetNameAutocomplete(retaName) {
		$("#retailerName").autocomplete({
			
			minLength : 3,
			delay : 200,
			source : '/ScanSeeWeb/retailer/displaylogretnames.htm',
			select : function(event, ui) {
				
				if (ui.item.value == "No Records Found") {
					$("#retailerName").val("");
				} else {
					$("#retailerName").val(ui.item.rname);
					$("#retailerId").val(ui.item.retId);
					getLogRetailerLocs(ui.item.retId, "");
				}
				return false;
			}
		});
	}
	
	function getLogRetailerLocs(retId, selectedLocId) {
		
		$('#locationId').find('option:not(:first)').remove();

		$.ajaxSetup({
			cache : false
		});

		$.ajax({
			type : "GET",
			url : "displaylogretLoc.htm",
			data : {
				'retId' : retId

			},

			success : function(response) {

				var rLocations = response;

				var objs = JSON.parse(rLocations);

				slctbox = $('#locationId');
				$('#locationId').find('option:not(:first)').remove();
				if (objs != null && objs != 'undefined') {
					for ( var i = 0; i < objs.length; i++) {
						slctbox.append(new Option(objs[i].address, objs[i].retLocId));
					}
					
					if("" !== selectedLocId) {
						$("#locationId option[value='" + selectedLocId + "']").prop('selected', 'selected');
					}
					
				}
			},
			error : function(e) {
				alert('Error occured while fetching retailer locations');
			}
		});
	}
</script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>

<script type="text/javascript">
	window.onload = function() {
		var vBackBtn = document.addediteventform.backButton.value;
		if (vBackBtn == 'no') {
			document.getElementById('back').style.visibility = 'hidden';
		}
	};

	var changeImgDim = '${sessionScope.ChangeImageDim}';
	if (null != changeImgDim && changeImgDim == 'true') {
		$('#eventImg').width('300px');
		$('#eventImg').height('150px');
	}
	
	
	var vChangeImgDim = '${sessionScope.ChangeImageDim1}';
	if (null != vChangeImgDim && vChangeImgDim == 'true') {
		$('#eventImg1').width('44px');
		$('#eventImg1').height('44px');
	}

	function onLoadRetLocationID() {
		var vRetLocID = document.addediteventform.hiddenLocationIDs.value;
		var vRetLocVal = document.getElementById("retlocationID");
		var vRetLocList = [];
		if (vRetLocID != "null" && vRetLocID != "") {
			vRetLocList = vRetLocID.split(",");
		}
		if (vRetLocVal.length != 0 && vRetLocList.length != 0) {
			if (vRetLocVal.length == vRetLocList.length) {
				document.getElementById('chkAllLoc').checked = true;
			}
		}

		for ( var i = 0; i < vRetLocVal.length; i++) {
			for (var j = 0; j < vRetLocList.length; j++) {
				if (vRetLocVal.options[i].value == vRetLocList[j]) {
					vRetLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("retlocationID");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}

	function clearForm() {
		var r = confirm("Do you really want to clear the form");
		if (r == true) {
		
		$(".error").remove();
		$(".error-msg").remove();
		
			/*if (document.getElementById('eventName.errors') != null) {
				document.getElementById('eventName.errors').style.display = 'none';
			}
			if (document.getElementById('eventCategory.errors') != null) {
				document.getElementById('eventCategory.errors').style.display = 'none';
			}
			if (document.getElementById('eventDate.errors') != null) {
				document.getElementById('eventDate.errors').style.display = 'none';
			}
			if (document.getElementById('address.errors') != null) {
				document.getElementById('address.errors').style.display = 'none';
			}
			if (document.getElementById('city.errors') != null) {
				document.getElementById('city.errors').style.display = 'none';
			}
			if (document.getElementById('postalCode.errors') != null) {
				document.getElementById('postalCode.errors').style.display = 'none';
			}
			if (document.getElementById('state.errors') != null) {
				document.getElementById('state.errors').style.display = 'none';
			}
			if (document.getElementById('eventImageFile.errors') != null) {
				document.getElementById('eventImageFile.errors').style.display = 'none';
			}                            
			if (document.getElementById('evtThumbImage.errors') != null) {
				document.getElementById('evtThumbImage.errors').style.display = 'none';
			}
			
			if (document.getElementById('evtImageFile.errors') != null) {
				document.getElementById('evtImageFile.errors').style.display = 'none';
			}
			if (document.getElementById('shortDescription.errors') != null) {
				document.getElementById('shortDescription.errors').style.display = 'none';
			}
			if (document.getElementById('latitude.errors') != null) {
				document.getElementById('latitude.errors').style.display = 'none';
			}
			if (document.getElementById('longitude.errors') != null) {
				document.getElementById('longitude.errors').style.display = 'none';
			}
			if (document.getElementById('moreInfoURL.errors') != null) {
				document.getElementById('moreInfoURL.errors').style.display = 'none';
			}
			if (document.getElementById('retailLocationIDs.errors') != null) {
				document.getElementById('retailLocationIDs.errors').style.display = 'none';
			} */

			document.addediteventform.eventName.value = "";
			

			document.addediteventform.eventCategory.value = "";
			document.addediteventform.hiddenCategory.value = "";

			document.addediteventform.eventDate.value = "";
			document.addediteventform.eventTimeHrs.value = "00";
			document.addediteventform.eventTimeMins.value = "";
			document.addediteventform.eventLocationTitle.value = "";
			document.addediteventform.address.value = "";
			document.addediteventform.city.value = "";
			document.addediteventform.postalCode.value = "";
			document.addediteventform.latitude.value = "";
			document.addediteventform.longitude.value = "";
			document.addediteventform.geoError.value = false;

			document.addediteventform.state.value = "";
			document.addediteventform.eventImageName.value = "";
			document.addediteventform.evtImageName.value = "";
			document.addediteventform.longDescription.value = "";
			document.addediteventform.shortDescription.value = "";
			document.addediteventform.moreInfoURL.value = "";

			$('#retlocationID option').removeAttr('selected');
			document.addediteventform.hiddenLocationIDs.value = "";

			$("input[name='chkAllLoc']").prop("checked", false);
			$("#dispLatLang").hide();
			$("#bsnsNo").prop('checked', true);
			$("#ongnNo").prop('checked', true);

			$('input[name="isOngoing"]:checked').trigger('change');
			$("input[name='bsnsLoc']:checked").trigger('change');

			$("#eventImg").prop('src',
					'/ScanSeeWeb/images/upload_imageRtlr.png');

		}
	}

	function updateCstmSlct() {
		var allVals = [];
		var $chkVal = $('.recDays:checked');
		$chkVal.each(function() {
			allVals.push($(this).attr("display"));
		});

		if ($chkVal.length) {
			$('#cstmDrpdwnInput').val(allVals);
		} else {
			$('#cstmDrpdwnInput').val("Select Day(s)");
		}

	}

	function eventChange() {
		document.addediteventform.hiddenCategory.value = $('#eventCategory')
				.val();
	}

	function isLatLong(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
				|| charCode == 45 || charCode == 43)
			return true;
		return false;
	}
</script>


<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR evntsSctn">
			<div class="grpTitles">
				<h1 class="mainTitle">Build Event Page <div class="sub-actn fund"><a href="/ScanSeeWeb/retailer/eventinstructions.htm">View Instructions</a> </div> </h1>
			</div>
			<div class="section">
				<div class="grdSec brdrTop">
					<form:form name="addediteventform" commandName="addediteventform"
						action="/ScanSeeWeb/retailer/uploadeventimg.htm"
						acceptCharset="ISO-8859-1" enctype="multipart/form-data">
						<form:hidden path="backButton" name="backButton" id="backButton"
							value="${sessionScope.backButton}" />

						<form:hidden path="viewName" value="addevent" />
						<form:hidden path="hiddenState" />
						<form:hidden path="hiddenCategory" />
						<form:hidden path="hiddenDays" />
						<form:hidden path="hiddenDay" />
						<form:hidden path="hiddenDate" />
						<form:hidden path="hiddenWeek" />
						<form:hidden path="hiddenWeekDay" />
						<form:hidden path="geoError" id="geoError" />
						<form:hidden path="eventImageName" id="eventImgPath" />
						<form:hidden path="recurrencePatternName" id="recurrencePatternName" />
						<form:hidden path="hiddenLocationIDs" />
						<form:hidden path="uploadedImage" />
						<form:hidden path="hidLongDescription" />
						<form:hidden path="imageType" />
						<form:hidden path="uploadedThumbImg" />
						<form:hidden path="evtImageName" id="eventImgPath1" />
						<form:hidden path="retailerId" />
						<form:hidden path="hiddenLocationId" />

						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="editorTbl">
							<tbody>
							
								<tr>
									<td width="20%" style="border-right: 1px solid rgb(218, 218, 218);"><label
										class="mand">Title</label></td>
								<td width="30%" colspan="3" style="border-right: 1px solid rgb(218, 218, 218);"><form:errors
											cssClass="error" path="eventName" cssStyle="color:red"></form:errors>
										<form:input path="eventName" type="text" maxlength="40"
											tabindex="1" /></td>
									
								</tr>
								
								
								<tr>
									
									<td width="20%" align="left"
										style="border-right: 1px solid rgb(218, 218, 218);"><label
										class="mand">Image</label></td>
									<td width="30%"><ul class="imgInfoSplit" id="eventUpld">
											<li><label><img id="eventImg" alt="upload"
													src="${sessionScope.eventImagePath}" height="80" width="80"
													onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
											</label><span class="topPadding forceBlock"><label
													for="trgrUpld"> <input type="button" value="Choose File"
														id="trgrUpldBtn" width="350" height="460"
														class="btn trgrUpld" title="Choose File" 
														tabindex="2"> <form:input path="eventImageFile"
															type="file" class="textboxBig" id="trgrUpld"
															onchange="checkEventImgValidate(this);" tabindex="3" />
												</label></span></li>
											<li>Suggested Minimum Size:<br>70px/70px<br>Maximum
												Size:800px/600px<br>
                                            
										</ul>
										<form:errors path="eventImageFile" cssClass="error" cssStyle="color:red"></form:errors> 
										<label class="error-msg" id="eventImagePathErr" style="color: red; font-style: 45"></label>
										</td>
										
										
									<td width="12%" align="left"
										style="border-right: 1px solid rgb(218, 218, 218);"><label
										class="mand">List Image</label></td>
										
									<td><ul class="imgInfoSplit" id="eventUpld">
											<li><label><img id="eventImg1" alt="upload" src="${sessionScope.evtImagePath}" height="80" width="80"
													onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
											</label><span class="topPadding forceBlock"><label
													for="trgrUpld1"> <input type="button" value="Choose File"
														id="trgrUpldBtn1" width="350" height="460"
														class="btn trgrUpld" title="Choose File" 
														tabindex="4"> <form:input path="evtThumbImage"
															type="file" class="textboxBig" id="trgrUpld1" onchange="checkEventThumbImgValidate(this);" tabindex="5" />
												</label></span></li>
											<li>Suggested Minimum Size:<br>70px/70px<br>Maximum Size:800px/600px<br>
											
											
										</ul>
									
										<form:errors path="evtThumbImage" cssClass="error" cssStyle="color:red"></form:errors> 
										<label class="error-msg" id="eventImagePathErr1" style="color: red; font-style: 45"></label>
										</td>
										
										
								</tr>
								<tr>
									<td valign="top" align="left"
										style="border-right: 1px solid rgb(218, 218, 218);"><label
										class="mand">Short Description</label></td>
									<td colspan="3"><form:errors cssClass="error" path="shortDescription"
											cssStyle="color:red" /> <form:textarea
											path="shortDescription" name="textarea" cols="25" rows="5"
											class="shortDesc" maxlength="255" tabindex="6"></form:textarea></td>
								</tr>
								<tr>
									<td valign="top"><label>Long Description</label></td>
									<td colspan="3">
										<form:errors cssClass="error" path="longDescription" cssStyle="color:red" />
										<form:textarea path="longDescription" id="longDescription" name="textarea" rows="5" cols="25"
											class="textareaTxt txtAreaLarge" maxlength="5000" tabindex="7" />
									</td>
								</tr>
								<tr>
									<td valign="top" align="left"
										style="border-right: 1px solid rgb(218, 218, 218);"><label
										class="mand">Category</label></td>
									<td><form:errors cssClass="error" path="eventCategory"
											cssStyle="color:red" /> <form:select path="eventCategory"
											name="select2" id="eventCategory" class="slctBx textareaTxt"
											onchange="eventChange();" tabindex="8">
											<option value="">---Select---</option>
											<c:forEach items="${sessionScope.eventCategoryList}"
												var="item">
												<option value="${item.categoryID }">${item.categoryName
													}</option>
											</c:forEach>
										</form:select></td>
									<td valign="top" align="left"
										style="border-right: 1px solid rgb(218, 218, 218);">More
										Information URL</td>
									<td valign="top" align="left"><form:errors
											cssClass="error" path="moreInfoURL" cssStyle="color:red" />
										<form:input path="moreInfoURL" type="text" tabindex="9" /></td>
								</tr>
								
								<tr class="subHdr">
									<td valign="bottom" align="left" colspan="4"><span
										class="setLbl">Is Event Ongoing? </span><span class="spacing">
											<form:radiobutton path="isOngoing" value="yes" id="ongnYes"
												tabindex="10" /> <label for="ongnYes"> Yes</label> <form:radiobutton
												path="isOngoing" value="no" id="ongnNo" tabindex="11" /> <label
											for="ongnNo"> No</label>
									</span></td>
								</tr>


								<tr class="scngRow" style="display: none;">
									<td colspan="4"></td>
								</tr>
								<tr class="not-ongng" style="display: none;">
									<td style="border-right: 1px solid rgb(218, 218, 218);"><label
										class="mand">Event Start Date</label></td>
									<td style="border-right: 1px solid rgb(218, 218, 218);"><form:errors
											cssClass="error" path="eventDate" cssStyle="color:red"></form:errors>
										<form:input path="eventDate" id="datepicker1" type="text"
											class="textboxDate" tabindex="12" readonly="true" /></td>
									<td style="border-right: 1px solid rgb(218, 218, 218);">
										Event Start Time</td>
									<td><form:select path="eventTimeHrs" class="slctSmall"
											name="etHr" tabindex="13">
											<form:options items="${StartHours}" />
										</form:select> Hrs <form:select path="eventTimeMins" class="slctSmall"
											name="stMin" tabindex="14">
											<form:options items="${StartMinutes}" />
										</form:select> Mins</td>
									</td>
								</tr>
								<tr class="not-ongng" style="display: none;">
									<td style="border-right: 1px solid rgb(218, 218, 218);">Event
										End Date</td>
									<td style="border-right: 1px solid rgb(218, 218, 218);"><form:errors
											cssClass="error" path="eventEDate" cssStyle="color:red"></form:errors>
										<form:input path="eventEDate" id="datepicker2" type="text"
											class="textboxDate" tabindex="15" readonly="true" /></td>
									<td style="border-right: 1px solid rgb(218, 218, 218);">Event
										End Time</td>
									<td><form:select path="eventETimeHrs" class="slctSmall"
											name="etHr" tabindex="16">
											<form:options items="${StartHours}" />
										</form:select> Hrs <form:select path="eventETimeMins" class="slctSmall"
											name="stMin" tabindex="17">
											<form:options items="${StartMinutes}" />
										</form:select> Mins</td>
									</td>
								</tr>
								<tr class="ongoing  grey-bg" style="display: table-row;">
									<td style="border-right: 1px solid rgb(218, 218, 218);">Start
										Time</td>
									<td style="border-right: 1px solid rgb(218, 218, 218);"><form:select
											path="eventStartTimeHrs" class="slctSmall" name="etHr"
											tabindex="18">
											<form:options items="${StartHours}" />
										</form:select> Hrs <form:select path="eventStartTimeMins" class="slctSmall"
											name="stMin" tabindex="19">
											<form:options items="${StartMinutes}" />
										</form:select> Mins</td>
									<td style="border-right: 1px solid rgb(218, 218, 218);">End
										Time</td>
									<td><span class="cntrl-grp zeroBrdr"> <form:select
												path="eventEndTimeHrs" class="slctSmall" name="etHr"
												tabindex="20">
												<form:options items="${StartHours}" />
											</form:select> Hrs <form:select path="eventEndTimeMins" class="slctSmall"
												name="stMin" tabindex="21">
												<form:options items="${StartMinutes}" />
											</form:select> Mins
									</span></td>
								</tr>





								<tr class="ongoing subHdr" style="display: table-row;">
									<td colspan="4"><span class="setLbl">Recurrence
											Pattern</span> <span class="mrgn-left spacing"> <c:forEach
												items="${sessionScope.eventPatterns}" var="patterns">
												<form:radiobutton path="recurrencePatternID"
													value="${patterns.recurrencePatternID}"
													id="actn-${patterns.recurrencePatternName}"
													patternName="${patterns.recurrencePatternName}"
													tabindex="22" />
												<label for="actn-${patterns.recurrencePatternName}">${patterns.recurrencePatternName}</label>
											</c:forEach>
									</span></td>
								</tr>
								<tr class="grey-bg equalPdng recurrence ongoing"
									style="display: table-row;">
									<td colspan="4"><div class="recurenceCont">
											<div class="brdr actn-Daily recur" style="display: block;">
												<table width="100%" cellspacing="0" cellpadding="0"
													class="white-bg formTbl brdrLsTbl">
													<thead>
														<tr class="tblHdr">
															<th colspan="2">Daily</th>
														</tr>
													</thead>
													<tbody>
														<td colspan="2"><form:errors path="everyWeekDay"
																cssClass="error" cssStyle="color:red"></form:errors></td>

														<tr class="">
															<td width="30%" align="left"
																style="border-right: 1px solid rgb(218, 218, 218);"><form:radiobutton
																	path="isOngoingDaily" value="days" tabindex="23" />
																Every <form:input path="everyWeekDay"
																	cssClass="textboxSmaller" maxlength="3" tabindex="24" />
																day(s)</td>
															<td width="60%" align="left"><form:radiobutton
																	path="isOngoingDaily" value="weekDays" id="everyweek"
																	tabindex="25" /> Every Weekday</td>
														</tr>
													</tbody>
												</table>
											</div>




											<div class="actn-Weekly recur brdr">
												<table width="100%" cellspacing="0" cellpadding="0"
													class="white-bg formTbl grdTbl">
													<thead>
														<tr class="tblHdr">
															<th>Weekly</th>
														</tr>
													</thead>
													<tbody>
														<tr class="">
															<td colspan="1"><form:errors path="everyWeek"
																	cssClass="error" cssStyle="color:red">
																</form:errors> <form:errors path="days" cssClass="error"
																	cssStyle="color:red">
																</form:errors></td>
														</tr>
														<tr class="">
															<td align="left">Recur every <form:input
																	path="everyWeek" cssClass="inputText small numeric"
																	maxlength="2" tabindex="26" /> week(s) on:
															</td>
														</tr>
														<tr class="">
															<td align="left"><ul class="week-actn">
																	<li><form:checkbox path="days" value="1"
																			id="days1" name="week-recur" /> <label for="Sunday">Sunday</label>
																	</li>
																	<li><form:checkbox path="days" value="2"
																			id="days2" name="week-recur" /> <label for="Monday">Monday</label>
																	</li>
																	<li><form:checkbox path="days" value="3"
																			id="days3" name="week-recur" /> <label for="Tuesday">Tuesday</label>
																	</li>
																	<li><form:checkbox path="days" value="4"
																			id="days4" name="week-recur" /> <label
																		for="Wednesday">Wednesday</label></li>
																	<li><form:checkbox path="days" value="5"
																			id="days5" name="week-recur" /> <label
																		for="Thursday">Thursday</label></li>
																	<li><form:checkbox path="days" value="6"
																			id="days6" name="week-recur" /> <label for="Friday">Friday</label>
																	</li>
																	<li><form:checkbox path="days" value="7"
																			id="days7" name="week-recur" /> <label
																		for="Saturday">Saturday</label></li>
																</ul></td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="actn-Monthly recur brdr">
												<table width="100%" cellspacing="0" cellpadding="0"
													class="white-bg formTbl brdrLsTbl">
													<thead>
														<tr class="tblHdr">
															<th>Monthly</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td colspan="2"><form:errors cssClass="error"
																	path="dateOfMonth" cssStyle="color:red"></form:errors>
																<form:errors cssClass="error" path="everyMonth"
																	cssStyle="color:red"></form:errors></td>
														</tr>
														<tr class="">
															<td valign="top" align="left"><form:radiobutton
																	path="isOngoingMonthly" value="date" tabindex="27" />
																Day <form:input path="dateOfMonth"
																	cssClass="inputText small numeric range" maxlength="2"
																	onkeyup="isThirtyFirst(this);" tabindex="28" /> of
																every <form:input path="everyMonth"
																	cssClass="inputText small numeric range" maxlength="2"
																	tabindex="29" /> month(s)</td>
														</tr>
														<tr class="">
															<td valign="top" align="left"><form:errors
																	cssClass="error" path="everyDayMonth"
																	cssStyle="color:red"></form:errors> <form:radiobutton
																	path="isOngoingMonthly" value="day" tabindex="30" />
																The <form:select path="dayNumber"
																	cssClass="slctBx medium range" id="recOptn"
																	tabindex="31">
																	<form:option value="1">First</form:option>
																	<form:option value="2">Second</form:option>
																	<form:option value="3">Third</form:option>
																	<form:option value="4">Fourth</form:option>
																	<form:option value="5">last</form:option>
																</form:select>

																<div class="cstmSlct relative">
																	<input type="text" id="cstmDrpdwnInput" value="Select"
																		class="dsblSlct" />
																	<ul id="cstmdd" class="cstmDropdwn">
																		<li><form:checkbox path="everyWeekDayMonth"
																				cssClass="recDays" value="1" display="Sun" />
																			Sunday <strong></strong></li>
																		<li><form:checkbox path="everyWeekDayMonth"
																				cssClass="recDays" value="2" display="Mon" />
																			Monday</li>
																		<li><form:checkbox path="everyWeekDayMonth"
																				cssClass="recDays" value="3" display="Tue" />
																			Tuesday</li>
																		<li><form:checkbox path="everyWeekDayMonth"
																				cssClass="recDays" value="4" display="Wed" />
																			Wednesday</li>
																		<li><form:checkbox path="everyWeekDayMonth"
																				cssClass="recDays" value="5" display="Thur" />
																			Thursday</li>
																		<li><form:checkbox path="everyWeekDayMonth"
																				cssClass="recDays" value="6" display="Fri" />
																			Friday</li>
																		<li><form:checkbox path="everyWeekDayMonth"
																				cssClass="recDays" value="7" display="Sat" />
																			Saturday</li>
																	</ul>
																</div> of every <form:input path="everyDayMonth"
																	cssClass="inputText small numeric range" maxlength="2" />
																month(s)</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div></td>
								</tr>




								<tr class="ongoing grey-bg" style="display: table-row;">
									<td colspan="4"><div class="ongoing brdr"
											style="display: block;">
											<table width="100%" cellspacing="0" cellpadding="0"
												class="white-bg formTbl innrTbl rangeActn">
												<thead>
													<tr class="tblHdr">
														<th colspan="3">Range of recurrence</th>
													</tr>
												</thead>
												<tbody>
													<tr class="">
														<td width="7%" valign="top" align="left"
															style="border-right: 1px solid rgb(218, 218, 218);">Start</td>
														<td width="21%" valign="top" align="left"
															style="border-right: 1px solid rgb(218, 218, 218);"><form:errors
																cssClass="error" path="eventStartDate"
																cssStyle="color:red"></form:errors> <form:input
																path="eventStartDate" id="datepicker3" type="text"
																class="textboxDate" tabindex="32" readonly="true" /></td>
														<td width="72%" valign="bottom" align="left"><form:radiobutton
																name="range" path="occurenceType" value="noEndDate"
																id="noEndDt" /> <label for="noEndDt" tabindex="33">No
																end date</label></td>
													</tr>
													<tr class="">
														<td valign="top" align="left"
															style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
														<td valign="top" align="left"
															style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
														<td valign="top" align="left"><form:errors
																cssClass="error" path="endAfter" cssStyle="color:red"></form:errors>
															<form:radiobutton name="range" path="occurenceType"
																value="endAfter" id="endAftr" tabindex="34" /> <label
															for="endAftr">End after</label> <form:input
																path="endAfter" cssClass="inputText small range numeric"
																maxlength="3" tabindex="35" /> occurrences</td>
													</tr>
													<tr class="">
														<td valign="top" align="left"
															style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
														<td valign="top" align="left"
															style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
														<td valign="top" align="left"><form:errors
																cssClass="error" path="eventEndDate"
																cssStyle="color:red"></form:errors> <form:radiobutton
																name="range" path="occurenceType" value="endBy"
																id="endBy" tabindex="36" /> <label class="lblEndDt"
															for="endBy">End By</label> <form:input
																path="eventEndDate" id="datepicker4" type="text"
																class="textboxDate" tabindex="37" readonly="true" /></td>
													</tr>
												</tbody>
											</table>
										</div></td>
								</tr>

								<tr class="ongoing" style="display: table-row;">
									<td colspan="4"></td>
								</tr>
						
								<tr class="subHdr">
									<td colspan="4"><span class="setLbl">Is Event at a
											Business?</span> <span class="mrgn-left spacing"> <form:radiobutton
												path="bsnsLoc" type="radio" id="bsnsYes" value="yes"
												tabindex="38" /> <label for="bsnsYes">Yes</label> <form:radiobutton
												path="bsnsLoc" type="radio" id="bsnsNo" value="no"
												tabindex="39" /> <label for="bsnsNo">No</label>
									</span></td>
								</tr>
								
								<c:if test="${!sessionScope.ban}">
								<tr class="grey-bg bsnsLoctn">

									<td width="19%" align="left" valign="top" class="Label"><label
										for="locationId" class="mand">Location(s)</label></td>

									<td><form:select path="retailLocationIDs"
											class="txtAreaBox" size="10" id="retlocationID"
											multiple="true" tabindex="40">
											<c:forEach items="${sessionScope.eventLocationList}" var="s">
												<form:option value="${s.retailLocationID}"
													label="${s.address1}" />
											</c:forEach>

										</form:select> <br /> <form:label path="retailLocationIDs">Hold Ctrl to select more than one location</form:label>
										<br /> <form:errors path="retailLocationIDs"
											cssStyle="color:red" cssClass="error">
										</form:errors></td>

									<td colspan="2" align="left" valign="top" class="Label"><label>
											<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
											onclick="SelectAllLocation(this.checked);" tabindex="41" />
											Select All Locations
									</label> <br> <br></td>
								</tr>
								</c:if>
								<c:if test="${sessionScope.ban}">
								<tr class="grey-bg bsnsLoctn">
			                  		<td><label class="mand">Retailer Name</label></td>
			                  			<td>
			                  				<form:errors cssStyle="color:red" path="retailerName"></form:errors>
			                  				<div class="cntrl-grp">
			                      				<form:input path="retailerName" class="loadingInput" onkeypress='logRetNameAutocomplete(retailerName);' maxlength="30" tabindex="3"/>
			                   	 			</div></td>
			                  			<td><label class="mand">Locations</label></td>
			                  			<td>
			                  				<form:errors cssStyle="color:red" path="locationId"></form:errors>
			                  				<div class="cntrl-grp zeroBrdr">
			                  				
			                  				<form:select path="locationId" class="slctBx textareaTxt" tabindex="4">
			                  					<form:option value="">Select Location</form:option>
			                  				</form:select>
			                      		</div></td>
			                		</tr>
								</tr>
								</c:if>


								<tr class="grey-bg equalPdng evntLoctn">
									<td colspan="4"><div style="display: block;"
											class="evntLoctn brdr">
											<table width="100%" cellspacing="0" cellpadding="0"
												class="white-bg brdrLsTbl formTbl">
												<thead>
													<tr class="tblHdr">
														<th colspan="4">Event Location</th>
													</tr>
												</thead>
												<tbody>
												<c:if test="${sessionScope.ban}"> 
												<tr>
												<td valign="top" align="left" ><lable >Location Title</lable></td>
												<td  style="border-right: 1px solid rgb(218, 218, 218);">
												<form:errors cssClass="error" path="EventLocationKeyword" cssStyle="color:red"></form:errors>
										<form:input path="EventLocationKeyword" type="text" maxlength="40"
											tabindex="1" /></td>
												</tr>
												</c:if>
												
													<tr class="">
													
														<td valign="top" align="left" rowspan="2"
															style="border-right: 1px solid rgb(218, 218, 218);"><label
															class="mand">Address</label></td>
														<td valign="top" align="left" rowspan="2"
															style="border-right: 1px solid rgb(218, 218, 218);"><form:errors
																cssClass="error" path="address" cssStyle="color:red"></form:errors>

															<form:textarea path="address" name="textarea2" cols="25"
																rows="5" class="txtAreaSmall" tabindex="42"></form:textarea>
														</td>
														<td valign="top" align="left"
															style="border-right: 1px solid rgb(218, 218, 218);"><label
															class="mand">Postal Code</label></td>
														<td valign="top"><div class="ui-widget">
																<form:errors cssStyle="color:red" path="postalCode"
																	cssClass="error"></form:errors>
																<form:input path="postalCode" type="text"
																	class="loadingInput dsblContxMenu" maxlength="5"
																	tabindex="43" id="pstlCd"
																	onkeypress="zipCodeAutocomplete('pstlCd');return isNumberKey(event)"
																	onchange="isNumeric(this.value);"
																	onkeyup="isEmpty(this.value);" />
															</div></td>
													</tr>
													<tr class="">
														<td valign="top" align="left"
															style="border-right: 1px solid rgb(218, 218, 218);">
															<label class="mand">City</label>
														</td>
														<td valign="top" align="left"><div class="cntrl-grp">
																<form:errors cssStyle="color:red" path="city"
																	cssClass="error"></form:errors>
																<form:input path="city" id="City" tabindex="44"
																	class="loadingInput dsblContxMenu" />
															</div></td>
													</tr>
													<tr class="">
														<td style="border-right: 1px solid rgb(218, 218, 218);"><label
															class="mand">State</label></td>
														<td><div class="cntrl-grp zeroBrdr">
																<form:errors cssStyle="color:red" path="state"
																	cssClass="error"></form:errors>
																<form:select path="state" id="Country" tabindex="45">
																	<form:option value="">--Select--</form:option>
																	<c:forEach items="${sessionScope.states}" var="s">
																		<form:option value="${s.stateabbr}"
																			label="${s.stateName}" />
																	</c:forEach>
																</form:select>
															</div></td>
													</tr>
											
											
													<tr id="dispLatLang">
														<td><label class="mand">Latitude</label></td>
														<td><form:errors cssClass="error" path="latitude"
																cssStyle="color:red"></form:errors>
															<div class="cntrl-grp">
																<form:input path="latitude" id="latitude" type="text"
																	class="inputTxt inputTxtBig"
																	onkeypress="return isLatLong(event)" tabindex="46" />
															</div></td>
														<td><label class="mand">Longitude</label></td>
														<td><form:errors cssClass="error" path="longitude"
																cssStyle="color:red"></form:errors>
															<div class="cntrl-grp">
																<form:input path="longitude" id="longitude" type="text"
																	class="inputTxt inputTxtBig"
																	onkeypress="return isLatLong(event)" tabindex="47" />
															</div></td>
													</tr>
												</tbody>
											</table>
										</div></td>
								</tr>

							</tbody>
						</table>
						<div class="navTabSec mrgnRt" align="right">
							<input type="button" id="saveEventBtn" value="Submit"
								onclick="saveEvent();" class="btn" title="save" tabindex="48" />
							<input type="button" value="Clear" onclick="clearForm();"
								class="btn" title="clear" tabindex="49" /> <input type="button"
								id="back" value="Back" onclick="backManageEvents();" class="btn"
								 title="back" tabindex="50" />
						</div>

						<div class="ifrmPopupPannelImage" id="ifrmPopup"
							style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
						
						<div class="ifrmPopupPannelImage" id="ifrmPopup2" style="display: none;background-color: White">
					<div class="headerIframe">
						<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
							alt="close"
							onclick="javascript:closeIframePopup('ifrmPopup2','ifrm2')"
							title="Click here to Close" align="middle" /> <span
							id="popupHeader"></span>
					</div>
					<iframe frameborder="0" scrolling="no" id="ifrm2" src=""
						height="100%" allowtransparency="yes" width="100%"
						style="background-color: White"> </iframe>
				    </div>
						
					 </form:form>
				</div>
				
				<div id="results"></div>
			</div>


		</div>
		<div class="clear"></div>
	</div>
</div>

	<script>
		onLoadRetLocationID();

		$("#latitude").focusout(function() {
			/* Matches	 90.0,-90.9,1.0,-23.343342
			Non-Matches	 90, 91.0, -945.0,-90.3422309*/
			var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
			var vLat = $('#latitude').val();

			//Validate for Latitude.
			if (0 === vLat.length || !vLat || vLat == "") {
				return false;
			} else {
				if (!vLatLngVal.test(vLat)) {
					alert("Invalid Latitude");
					$("#latitude").val("").focus();
					return false;
				}
			}
		});

		$("#longitude")
				.focusout(
						function() {
							/* Matches	180.0, -180.0, 98.092391
							Non-Matches	181, 180, -98.0923913*/
							var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
							var vLong = $('#longitude').val();

							//Validate for Longitude.
							if (0 === vLong.length || !vLong || vLong == "") {
								return false;
							} else {
								if (!vLatLngVal.test(vLong)) {
									alert("Invalid Longitude");
									$("#longitude").val("").focus();
									return false;
								}
							}
						});
	</script>
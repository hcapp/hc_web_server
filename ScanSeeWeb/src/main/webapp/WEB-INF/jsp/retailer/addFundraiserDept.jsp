<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.1)"/>
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.1)"/>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="scripts/global.js" type="text/javascript"></script>
<script src="scripts/jquery.tablescroll.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready( function()	{
		var deptName = $("#deptName").val();
		$('p.emptydept').hide();
		$('p.emptydept').css("display", "none");
		$('p.dupdept').hide();
		$('p.dupdept').css("display", "none");
	});
	
</script>
<script type="text/javascript">
function saveFundraiserDept()	{
	var deptName = $("#deptName").val();
	if(($.trim(deptName)).length == 0)	{
		$('p.dupdept').hide();
		$('p.emptydept').show();
		$('p.emptydept').css("display", "block");
		$('p.dupdept').css("display", "none");
	} else	{
		$.ajaxSetup({
			cache : false
		});
		
		$.ajax({
			type : "POST",
			url : "saveFundraiserDept.htm",
			data : {
				'deptName' : deptName
			},
			success : function(response)	{
				if (response === "Department name exist")	{
					$('p.emptydept').hide();
					$('p.dupdept').show();
					$('p.emptydept').css("display", "block");
					$('p.emptydept').css("display", "none");
				} else	{
					closeIframePopup('ifrmPopupDept','ifrmDept');
					$(parent.document).find('#department').append('<option value="' + response + '" selected="selected">' + deptName + '</option>');
				}
				
			},
			error : function(e) {
				alert("Error occurred while saving department");
			}
		});
	}
}
</script>
</head>
<body class="whiteBG">
	<div class="contBlock">
		<form:form name="addFundraiserDeptForm" commandName="addFundraiserDeptForm">
			<fieldset class="popUpSrch">
				<div class="grdCont searchGrd">
					<table id="thetable" class="stripeMe" border="0" cellspacing="0" cellpadding="0" width="98%">
						<thead></thead>
						<tbody>
							<tr>
								<td align="left">Department Name</td>
								<td align="left">
									<form:input id="deptName" type="text" class="textboxMedium" path="deptName"/>
									<p class="emptydept error" style="color: red;"><i class="emptydept">Department name required</i></p>
									<p class="dupdept error" style="color: red;"><i class="dupdept">Department name already exists</i></p>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<div class="navTabSec RtMrgn" align="right">
										<input class="btn" value="Save Department" type="button" name="submit" onclick="saveFundraiserDept()"/>
									 	<input class="btn" value="Clear" type="reset" name="submit" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</fieldset>
		</form:form>
	</div>
</body>
</html>
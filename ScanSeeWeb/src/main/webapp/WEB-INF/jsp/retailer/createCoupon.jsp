<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Create Deal</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)" />
<link href="styles/style.css" type="text/css" rel="stylesheet" />
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.form.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/cityAutocomplete.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/zipcodeAutocomplete.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<script src="/ScanSeeWeb/ckeditor/adapters/jquery.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(
			function() {

				CKEDITOR.config.uiColor = '#FFFFFF';
				CKEDITOR.editorConfig = function(config) {
					//config.uiColor = '#AADC6E';
					config.language = 'es';
					config.uiColor = '#f3f3f3';
					config.toolbarCanCollapse = true;
					config.extraPlugins = 'wordcount';
					config.wordcount = {

						// Whether or not you want to show the Word Count
						showWordCount : true,

						// Whether or not you want to show the Char Count
						showCharCount : true,

						// Maximum allowed Char Count
						maxCharCount : 500
					};
				};
				CKEDITOR.replace('couponLongDesc', {

					extraPlugins : 'onchange',
					width : "100%",
					toolbar : [
							{
								name : 'basicstyles',
								items : [ 'Bold', 'Italic', 'Underline' ]
							},
							{
								name : 'paragraph',
								items : [ 'JustifyLeft', 'JustifyCenter',
										'JustifyRight', 'JustifyBlock' ]
							}, {
								name : 'colors',
								items : [ 'BGColor' ]
							}, {
								name : 'paragraph',
								items : [ 'Outdent', 'Indent' ]
							}, {
								name : 'links',
								items : [ 'Link', 'Unlink' ]
							}, '/', {
								name : 'styles',
								items : [ 'Styles', 'Format' ]
							}, {
								name : 'tools',
								items : [ 'Font', 'FontSize', 'RemoveFormat' ]
							} ],
					removePlugins : 'resize'
				});

				$("#datepicker1").datepicker({
					showOn : 'both',
					buttonImageOnly : true,
					buttonText : 'Click to show the calendar',
					buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
				});
			});
	$(document).ready(function() {
		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
	$(document).ready(function() {
		$("#datepicker3").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
	/*$(document).ready(function() {
		$('#locationID').change(function(){
		 var len = $(this).find('option').length;
		 var sLen = $(this).find('option:selected').length;
		 if(len < sLen) {
		   $('#chkAllLoc').attr('checked','checked');
		 }else {
			$('#chkAllLoc').removeAttr('checked');
		 }
		});
		$('#locationID').trigger('change');
	});*/

	$(document).ready(function() {
		$('#locationID option').click(function() {
			var totOpt = $('#locationID option').length;
			var totOptSlctd = $('#locationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		$("#locationID").change(function() {
			var totOpt = $('#locationID option').length;
			var totOptSlctd = $('#locationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
	});
</script>

<script type="text/javascript">
	window.onload = function() {
		var vBackBtn = document.createcouponform.backButton.value;
		if (vBackBtn == 'no') {
			document.getElementById('back').style.visibility = 'hidden';
		}
	}

	var changeImgDim = '${sessionScope.ChangeImageDim}';
	if (null != changeImgDim && changeImgDim == 'true') {
		$('#couponImg').width('70px');
		$('#couponImg').height('70px');
	}

	function checkCouponImgValidate(input) {
		var vCouponImg = document.getElementById("trgrUpld").value;
		if (vCouponImg != '') {
			var checkCouponImg = vCouponImg.toLowerCase();
			if (!checkCouponImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Deal List image with following extensions :  .png, .gif, .bmp, .jpg, .jpeg");
				document.createcouponform.imageFile.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}

	function checkDealImgValidate(input) {
		var vCouponImg = document.getElementById("trgrUpldDetImg").value;
		if (vCouponImg != '') {
			var checkCouponImg = vCouponImg.toLowerCase();
			if (!checkCouponImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Deal image with following extensions :  .png, .gif, .bmp, .jpg, .jpeg");
				document.createcouponform.imageFile.value = "";
				document.getElementById("trgrUpldDetImg").focus();
				return false;
			}
		}
	}

	function checkAssociatedCouponProd() {
		var $prdID = $('#productID').val();
		$.ajaxSetup({
			cache : false
		});
		if ($prdID != "") {
			$.ajax({
				type : "GET",
				url : "/ScanSeeWeb/checkAssociatedCouponProd.htm",
				data : {
					'productId' : $prdID
				},
				success : function(response) {
					openIframePopup('ifrmPopup2', 'ifrm2', 'produpclist.htm',
							420, 600, 'View Product/UPC')
				},
				error : function(e) {
					alert('Error:' + 'Error Occured');
				}
			});

		} else {
			openIframePopup('ifrmPopup2', 'ifrm2', 'produpclist.htm', 420, 600,
					'View Product/UPC')
		}
	}

	function onLoadRetLocationID() {
		var vRetLocID = document.createcouponform.retLocationIDs.value;
		var vRetLocVal = document.getElementById("locationID");
		var vRetLocList = [];
		if (vRetLocID != "null" && vRetLocID != "") {
			vRetLocList = vRetLocID.split(",");
		}
		if (vRetLocVal.length != 0 && vRetLocList.length != 0) {
			if (vRetLocVal.length == vRetLocList.length) {
				document.getElementById('chkAllLoc').checked = true;
			}
		}

		for (var i = 0; i < vRetLocVal.length; i++) {
			for (j = 0; j < vRetLocList.length; j++) {
				if (vRetLocVal.options[i].value == vRetLocList[j]) {
					vRetLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("locationID");
		if (checked == true) {
			for (var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for (var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
</script>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>

</head>
<body>
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@include file="retailerLeftNavigation.jsp"%>
			<form:form name="createcouponform" commandName="createcouponform"
				method="POST" action="/ScanSeeWeb/retailer/uploadcouponimg.htm"
				acceptCharset="ISO-8859-1" enctype="multipart/form-data">
				<form:hidden path="retLocationID" id="retLocationID" />
				<form:hidden path="retLocationIDs" id="retLocationIDs" />
				<input type="hidden" name="formName" value="createCoupon" />
				<input type="hidden" id="selRelLoc" name="selRelLoc" />
				<form:hidden path="viewName" value="createcoupon" />
				<form:hidden path="couponImgPath" id="createcoupon" />
				<form:hidden path="productID" id="productID" />
				<form:hidden path="backButton" id="backButton" />
				<form:hidden path="uploadCoupImageType" />
				<form:hidden path="detailImgPath" id="createcoupon" />
				<div class="rtContPnl floatR">


					<div class="grpTitles">
						<h1 class="mainTitle">Build Deal</h1>
					</div>

					<div class="section">
						<div class="grdSec brdrTop">


							<table width="100%" cellspacing="0" cellpadding="0" border="0"
								class="grdTbl zeroBtmMrgn">

								<tbody>
									<tr>
										<td width="100%"><ul class="imgtxt">
												<li class="floatL"><img width="96" height="96"
													alt="buildBannerAd"
													src="/ScanSeeWeb/images/buildCoupon.png"></li>
												<li>
													<h3>Please fill out the information below to build</h3>
												</li>
												<li>

													<h3>your Deal</h3>
												</li>
											</ul></td>
									</tr>
								</tbody>
							</table>
							<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
								width="100%">
								<tbody>
									<tr>
										<td class="header" colspan="4">Create New Deal
											<div class="sub-actn">
												<a href="#"
													onclick="location='/ScanSeeWeb/retailer/addcouponinstructions.htm?cptype=Build'">View
													Instructions</a>
											</div> <c:if test="${message ne null }">
												<span class="alertTxt-dsply leftPdng"> <c:out
														value="${message}" />
												</span>
												<script>
													var PAGE_MESSAGE = true;
												</script>
											</c:if>
										</td>
									</tr>

									<tr>
										<td width="15%" class="Label"><label for="cpnName"
											class="mand">Title</label></td>

										<td width="30%"><form:errors path="couponName"
												cssStyle="color:red"></form:errors> <form:input
												path="couponName" id="cpnName" name="textfield2" type="text"
												maxlength="40" tabindex="1" /></td>
										<td class="Label" width="20%"><label for="couponAmt"
											class="mand"> Banner Title</label></td>

										<td width="35%"><form:errors path="bannerTitle"
												cssStyle="color:red">
											</form:errors> <form:input path="bannerTitle" id="bannerTitle" type="text"
												name="bannerTitle" maxlength="40" tabindex="2" /></td>
									</tr>
									<tr>
										<td class="Label"><label for="numofCpn" class="mand">#
												of Deals to issue</label></td>
										<td width="20%"><form:errors path="numOfCouponIssue"
												cssStyle="color:red"></form:errors> <form:input
												path="numOfCouponIssue" id="numofCpn" type="text"
												name="numOfCouponIssue" maxlength="4"
												onkeypress="return isNumberKey(event)" tabindex="3" /></td>
										<td class="Label"><label for="slctLoc" class="mand">Location(s)</label>
										</td>
										<td width="2%">
											<div>
												<form:errors path="locationID" cssStyle="color:red">
												</form:errors>
												<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
													onclick="SelectAllLocation(this.checked);" tabindex="4" />
												Select All Locations </label>
											</div> <form:select path="locationID" id="locationID"
												class="txtAreaBox" multiple="true" tabindex="5">
												<!--<form:option value="" label="--Select Location--" />-->
												<c:forEach items="${sessionScope.retailerLocList}" var="s">
													<form:option value="${s.retailerLocationID}"
														label="${s.address1}" />
												</c:forEach>
											</form:select>
											<p>Hold ctrl to select more than one location</p>
										</td>
									</tr>





									<tr>
										<td valign="top"><label class="mand"> Description</label></td>
										<td colspan="3"><form:errors cssClass="error"
												path="couponLongDesc" cssStyle="color:red" /> <form:textarea
												path="couponLongDesc" id="couponLongDesc" name="textarea"
												rows="5" cols="25" class="textareaTxt txtAreaLarge"
												maxlength="5000" tabindex="6" /></td>

									</tr>
									<tr>
										<td class="Label"><label for="cpnTC">Terms &amp;
												Conditions</label></td>
										<td><form:textarea path="couponTermsCondt" id="cpnTC"
												class="txtAreaSmall" rows="5" cols="45" name="textarea2"
												onkeyup="checkMaxLength(this,'50');" tabindex="7"></form:textarea></td>

										<td class="Label" align="left"><label for="keywords"></label>Keywords</td>
										<td align="left"><form:textarea id="keywords"
												class="txtAreaSmall" rows="5" cols="45" path="keyword"
												tabindex="8" cssStyle="height:60px;"
												onkeyup="checkMaxLength(this,'50');" /></td>

									</tr>


									<tr>
										<td class="Label"><label for="upldImg" class="mand">
												Deal List Image</label></td>
										<td colspan=""><form:errors path="couponImgPath"
												cssStyle="color:red"></form:errors>
											<ul class="imgInfoSplit">
												<li><label><img id="couponImg" alt="upload"
														src="${sessionScope.couponImagePath}" height="80"
														width="80"
														onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
												</label><span class="topPadding forceBlock"><label
														for="trgrUpld"> <input type="button"
															value="Choose File" id="trgrUpldBtn" width="350"
															height="460" class="btn trgrUpld" title="Choose File"
															tabindex="9"> <form:input path="imageFile"
																type="file" class="textboxBig" id="trgrUpld"
																onchange="checkCouponImgValidate(this);" />
													</label> </span></li>
												<li align="center">Suggested Minimum Size:<br>600px/300px<br>Maximum
													Size:800px/600px<br></li>
												<li><label id="couponImagePathErr"
													style="color: red; font-style: 45"></label></li>
											</ul></td>

										<td class="Label"><label for="upldImg" class="mand">
												Deal Image</label></td>
										<td colspan=""><form:errors path="detailImgPath"
												cssStyle="color:red"></form:errors>
											<ul class="imgInfoSplit">
												<li><label><img id="couponImg" alt="upload"
														src="${sessionScope.couponDetImagePath}" height="80"
														width="80"
														onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
												</label><span class="topPadding forceBlock"><label
														for="trgrUpldDetImg"> <input type="button"
															value="Choose File" id="trgrUpldBtn" width="350"
															height="460" class="btn trgrUpldDetImg"
															title="Choose File" tabindex="10"> <form:input
																path="detailImage" type="file" class="textboxBig"
																id="trgrUpldDetImg"
																onchange="checkDealImgValidate(this);" />
													</label> </span></li>
												<li align="center">Suggested Minimum Size:<br>600px/300px<br>Maximum
													Size:800px/600px<br></li>
												<li><form:errors path="detailImage"
														cssStyle="color:red"></form:errors> <label
													id="couponImageDetPathErr"
													style="color: red; font-style: 45"></label></li>
											</ul></td>

									</tr>
									<tr>
										<td class="Label"><label for="csd" class="mand">
												Start Date</label></td>

										<td align="left"><div class="coupdis">
												<form:errors path="couponStartDate" cssStyle="color:red"></form:errors>
											</div> <form:input path="couponStartDate" id="datepicker1"
												class="textboxDate" name="csd" tabindex="11" />(mm/dd/yyyy)</td>
										<td class="Label"><label for="cst">Start Time</label></td>
										<td><form:select path="couponStartTimeHrs"
												class="slctSmall" tabindex="12">
												<form:options items="${CouponStartHrs}" />
											</form:select> Hrs <form:select path="couponStartTimeMins"
												class="slctSmall" tabindex="13">
												<form:options items="${CouponStartMin}" />
											</form:select> Mins</td>

									</tr>
									<tr>

										<td class="Label"><label for="ced" class=""> End
												Date</label></td>
										<td align="left"><div class="coupdis">
												<form:errors path="couponEndDate" cssStyle="color:red"></form:errors>
											</div> <form:input path="couponEndDate" id="datepicker2"
												class="textboxDate" name="ced" tabindex="14" />(mm/dd/yyyy)</td>


										<td class="Label"><label for="cet">End Time</label></td>
										<td><form:select path="couponEndTimeHrs"
												class="slctSmall" tabindex="15">
												<form:options items="${CouponStartHrs}" />
											</form:select> Hrs <form:select path="couponEndTimeMins" class="slctSmall"
												tabindex="16">
												<form:options items="${CouponStartMin}" />
											</form:select> Mins</td>
									</tr>
									<!--	<tr>

										<td class="Label"><label for="timeZone">Time Zone</label>
										</td>
										<td><form:select path="timeZoneId"
												class="selecBx" tabindex="16">
												<form:option value="0" label="--Select--">Please Select Time Zone</form:option>
												<c:forEach items="${sessionScope.timeZoneslst}" var="tz">
													<form:option value="${tz.timeZoneId}"
														label="${tz.timeZoneName}" />
												</c:forEach>

											</form:select></td> 
											
											
									</tr> -->
									<tr>
										<td class="Label"><label for="csd" class="">Expiration
												Date</label></td>

										<td align="left"><div class="coupdis">
												<form:errors path="couponExpireDate" cssStyle="color:red"></form:errors>
											</div> <form:input path="couponExpireDate" id="datepicker3"
												class="textboxDate" name="csd" tabindex="17" />(mm/dd/yyyy)</td>
										<!--	<td><label for="couponName">Select Product</label></td>
										<td><form:input path="scanCode" type="text"
												name="textfield" id="couponName" readonly="true"
												tabindex="18" class="textboxMedium" /> <a href="#"><img
												src="/ScanSeeWeb/images/searchIcon.png" alt="Search"
												width="20" height="17" onclick="checkAssociatedCouponProd()"
												title="Click here to View Product/UPC List" /> </a></td>-->

										<td class="Label"><label for="cet">Expiration
												Time</label></td>
										<td><form:select path="couponExpTimeHrs"
												class="slctSmall" tabindex="18">
												<form:options items="${CouponStartHrs}" />
											</form:select> Hrs <form:select path="couponExpTimeMins" class="slctSmall"
												tabindex="19">
												<form:options items="${CouponStartMin}" />
											</form:select> Mins</td>



									</tr>
									<!--  <tr>
								<td class="Label">Do you like to integrate with your POS</td>
								<td colspan="3"><label> <form:radiobutton path="strPos" id="radio1"
										value="1"  type="radio" name="radio" tabindex="17"/>
								</label> Yes <label> <form:radiobutton path="strPos" id="radio2" value="0"
										type="radio" name="radio" tabindex="18"/> No</label>
								</td>
							</tr>-->
								</tbody>
							</table>

							<div class="navTabSec mrgnRt" align="right">

								<c:if test="${couponList ne null && ! empty couponList}">
									<input class="btn"
										onclick="location='/ScanSeeWeb/retailer/managecoupons.htm'"
										id="back" value="Back" type="button" name="Cancel3"
										title="Back" tabindex="20" />
								</c:if>
								<input class="btn" value="Preview" type="button" name="Cancel"
									title="Preview" onclick="previewCouponPopUp();" tabindex="21" />
								<input class="btn" value="Submit" type="button"
									onclick="submitCouponInfo()" name="Cancel" title="Submit"
									tabindex="22" />
							</div>
						</div>
						<div class="ifrmPopupPannel" id="ifrmPopup2"
							style="display: none; background-color: White">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup2','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="auto" id="ifrm2" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
						<div class="ifrmPopupPannelImage" id="ifrmPopup"
							style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
			</form:form>
		</div>
	</div>
	</div>
	</div>
	<div class="clear"></div>
</body>
<script>
	onLoadRetLocationID();
</script>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {
						$("#createcouponform").attr('action',
								'/ScanSeeWeb/retailer/uploadcouponimg.htm');
						/*show progress bar : ETA for Web 1.3*/
						//showProgressBar();/* Commented to fix body scroll disable issue*/
						/*End*/
						$("#uploadBtn").val("trgrUpldBtn")
						var uploadImageType = $(this).attr('name');

						document.createcouponform.uploadCoupImageType.value = uploadImageType;
						$.ajaxSetup({
							cache : false
						});
						$("#createcouponform")
								.ajaxForm(
										{
											success : function(response) {
												$('#loading-image').css(
														"visibility", "hidden");
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue
												if (imgRes == 'UploadLogoMaxSize') {
													$('#couponImagePathErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#couponImagePathErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {
													$('#couponImagePathErr')
															.text("");
													var substr = imgRes
															.split('|');
													if (substr[0] == 'ValidImageDimention') {

														$('#couponImg').width(
																'70px');
														$('#couponImg').height(
																'70px');

														var imgName = substr[1];
														$('#createcoupon').val(
																imgName);
														$('#couponImg').attr(
																"src",
																substr[2]);
													} else {

														/*commented to fix iframe popup scroll issue
														/$('body').css("overflow-y","hidden");*/
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}
											}
										}).submit();
					});

	$('#trgrUpldDetImg')
			.bind(
					'change',
					function() {
						$("#createcouponform").attr('action',
								'/ScanSeeWeb/retailer/uploadcouponimg.htm');
						/*show progress bar : ETA for Web 1.3*/
						//showProgressBar();/* Commented to fix body scroll disable issue*/
						/*End*/
						$("#uploadBtn").val("trgrUpldBtn")
						var uploadImageType = $(this).attr('name');

						document.createcouponform.uploadCoupImageType.value = uploadImageType;
						$.ajaxSetup({
							cache : false
						});
						$("#createcouponform")
								.ajaxForm(
										{
											success : function(response) {
												$('#loading-image').css(
														"visibility", "hidden");
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue
												if (imgRes == 'UploadLogoMaxSize') {
													$('#couponImageDetPathErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#couponImageDetPathErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {
													$('#couponImageDetPathErr')
															.text("");
													var substr = imgRes
															.split('|');
													if (substr[0] == 'ValidImageDimention') {

														$('#couponImg').width(
																'70px');
														$('#couponImg').height(
																'70px');

														var imgName = substr[1];
														$('#createcoupon').val(
																imgName);
														$('#couponImg').attr(
																"src",
																substr[2]);
													} else {

														/*commented to fix iframe popup scroll issue
														/$('body').css("overflow-y","hidden");*/
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}
											}
										}).submit();
					});
</script>
</html>




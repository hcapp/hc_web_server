<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script src="scripts/web.js" type="text/javascript"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<link
	href="/ScanSeeWeb/styles/jquery-ui.css"
	rel="stylesheet" type="text/css">
<script
	src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script
	src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<style>
.ui-datepicker-trigger {
                               margin-left:3px;
                               margin-top: 0.5px;
                               
                       }
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar' ,buttonImage: '/ScanSeeWeb/images/calendarIcon.png' });
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar',buttonImage: '/ScanSeeWeb/images/calendarIcon.png' });
	});
</script>
<script type="text/javascript">
	
	function validateRebateForm() {
		//var rebName = document.getElementById("rbtName").value;
		//var rbtAmount = document.getElementById("rbtAmount").value;
		var startDate = document.getElementById("datepicker1").value;
		var endDate = document.getElementById("datepicker2").value;
		//var retLoc = document.getElementById("retailerLocID").value;
		//if (isNotEmpty(rebName, "Rebate Name")) {
			//if (isDecimal(rbtAmount, "Rebate Ammount")) {
				//if (isNotEmpty(startDate, "Start Date")) {
					//if (isNotEmpty(endDate, "End Date")) {
						//if (checkDate(startDate, "Start Date")) {
							//if (checkDate(endDate, "End Date")) {
								if (compareDate(startDate, endDate, "No")) {
									//if (modeSelection(retLoc,"Retailer location")) {
										saveRetRerunRebates();
										return true;
									//}
								//}
							//}
						//}
					//}
				//}
			//}
		}
		return false;
	}
</script>
</head>
<div id="wrapper">
	<div id="content" class="topMrgn">
		<div class="section topMrgn">
			<form:form name="reRunretRebatesForm"
				commandName="reRunretRebatesForm" acceptCharset="ISO-8859-1">
				<form:hidden path="retailerLocationID" name="retailerLocationID" />
				<form:hidden path="rebproductId" name="rebproductId" />
				    <form:hidden path="rebateID" name="rebID" id="rebID"/>
				<div class="grdSec brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<div align="center" style="font-size:24"><label><form:errors cssStyle="color:red"/></label></div>
						<tbody>
							<tr>
								<td colspan="4" class="header"> Re-Run Rebates</td>
							</tr>
							<tr>
								<td width="18%" class="Label"><label for="productId"
									class="mand">Rebate Name</label>
								</td>
								<td width="33%"><form:errors path="rebName"
										cssStyle="color:red"></form:errors> <form:input path="rebName"
										type="text" name="rbtNm" id="rbtName" maxlength="100" /></td>
								<td class="Label" width="17%"><label for="rbtAmt"
									class="mand">Rebate Amount</label>
								</td>
								<td width="32%"><form:errors path="rebAmount"
										cssStyle="color:red"></form:errors> <form:input
										path="rebAmount" type="text" name="rbtAmount" id="rbtAmount" onkeypress="return isNumberKeyPhone(event)"/>
								</td>
							</tr>
							<tr>
								<td class="Label"><label for="rbtDsc" class="mand">Rebate
										Description</label>
								</td>
								<td><form:errors path="rebLongDescription"
										cssStyle="color:red"></form:errors> 
										<form:textarea
										path="rebLongDescription" name="rbtDesc" id="rbtDesc"
										cols="45" rows="5" class="txtAreaSmall"></form:textarea></td>
								<td class="Label"><label for="rbtTC" class="mand">Rebate
										Terms &amp; Conditions</label>
								</td>
								<td><form:errors path="rebTermCondtn" cssStyle="color:red"></form:errors>
									<form:textarea path="rebTermCondtn" name="rbtTC" id="rbtTC"
										cols="45" rows="5" class="txtAreaSmall"></form:textarea></td>
							</tr>
			 <tr>
            <td class="Label"><label for="rbtIssued" class="mand">Number of Rebates Issued</label></td>
            <td align="left" colspan="3"><form:errors path="noOfrebatesIsued"
										cssStyle="color:red"></form:errors><form:input path="noOfrebatesIsued" name="noofrebatesissued" class="textboxDate" type="text" id="rebateIsued" onkeypress="return isNumberKey(event)"/>
              </td>
            
          </tr>
							<tr>
								<td class="Label"><label for="csd" class="mand">Start
										Date</label>
								</td>
								<td align="left"><form:errors path="rebStartDate"
										cssStyle="color:red"></form:errors> <form:input
										path="rebStartDate" name="csd" type="text" class="textboxDate"
										id="datepicker1" />(mm/dd/yyyy)</td>
								<td class="Label"><label for="ced" class="mand">End
										Date</label>
								</td>
								<td align="left"><form:errors path="rebEndDate"
										cssStyle="color:red"></form:errors> <form:input
										path="rebEndDate" name="ced" type="text" class="textboxDate"
										id="datepicker2" />(mm/dd/yyyy)</td>
							</tr>
							<tr>
								<td class="Label"><label for="cst">Rebate Start
										Time </label></td>
								<td><form:select path="rebStartHrs" class="slctSmall">
										<form:options items="${RebateStartHrs}" />
									</form:select> Hrs <form:select path="rebStartMins" class="slctSmall">
										<form:options items="${RebateStartMins}" />
									</form:select> Mins</td>
								<td class="Label"><label for="cet">Rebate End Time</label>
								</td>
								<td><form:select path="rebEndhrs" class="slctSmall">
										<form:options items="${RebateStartHrs}" />
									</form:select> Hrs <form:select path="rebEndMins" class="slctSmall">
										<form:options items="${RebateStartMins}" />
									</form:select> Mins</td>

							</tr>
								<tr>
		
		<td class="Label"><label for="timeZone">Time Zone</label></td>
							<td colspan="3"><form:select path="rebateTimeZoneId" class="selecBx">
								<form:option value="0" label="--Select--">Please Select Time Zone</form:option>
								<c:forEach items="${sessionScope.rebateTimeZoneslst}" var="tz">
						<form:option value="${tz.timeZoneId}" label="${tz.timeZoneName}" />
					</c:forEach>
								
							</form:select>
		</tr>
							<tr>

								<td class="Label"><label for="retailerLocID" class="mand">Select
										Business Loc
								</td>
								<td colspan="3"><form:errors path="retailerLocID"
										cssStyle="color:red"></form:errors> <form:select
										path="retailerLocID" class="selecBx" id="retailerLocID">
										<form:option value="0">--Select--</form:option>
										<c:forEach items="${sessionScope.retailerLocList}" var="s">
											<form:option value="${s.retailerLocationID}"
												label="${s.address1}" />
										</c:forEach>
									</form:select></td>
							</tr>
						</tbody>
					</table>
				</div>
			</form:form>
			<div class="navTabSec">
				<div align="right">
				<input name="Back" value="Back" type="button" class="btn"
						onclick="location='/ScanSeeWeb/retailer/rebatesretailMfg.htm'" title="Back" />
					<input name="Preview" value="Preview" type="button" class="btn" onclick="previewRetRerunRebates()" title="Preview" />
					<input name="Save" value="Save" class="btn" type="button"
						onclick="saveRetRerunRebates();" title="Save"/>
				</div>
			</div>
		</div>
	</div>
</div>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<html>
<head>
<link href="/ScanSeeWeb/styles/style.css" type="text/css"
	rel="stylesheet" />
<link href="/ScanSeeWeb/styles/ticker-style.css" type="text/css"
	rel="stylesheet" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/bubble-tooltip.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.jscroll.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/validate.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

<script type="text/javascript">
$(function() {
    //Get actual content and display as title on mouse hover on it.
    var actText = $("td.genTitle").text();
    $("td.genTitle").attr('title',actText);
    var limit = 20;// character limit restricted to 20
    var chars = $("td.genTitle").text(); 
    if (chars.length > limit) {
        var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
        var dots = $("<span class='dots'>...</span>");
        $("td.genTitle").empty()
            .append(visibleArea)
            .append(dots);//append trailing dots to the visibile part 
    }                            
});
</script>

<div id="iphonePanel">
	<form:form name="retaileruploadlogoinfoform"  commandName="retaileruploadlogoinfoform">	
	  <div class="navBar iphone">
	
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
            <tr>
              <td width="19%"><img src="/ScanSeeWeb/images/backBtn.png" alt="back" width="50" height="30" /></td>
              <td width="54%" class="genTitle">${sessionScope.previewRetailerUploadList.retailerName}</td>
              <td width="27%"><img src="/ScanSeeWeb/images/mainMenuBtn.png" alt="mainmenu" width="78" height="30" /></td>
            </tr>
         </table>
       </div>
		
		<div class="previewArea">	
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobGrd detView zeroBtmMrgn htlt">
					<tbody>
								<tr>
								  <td align="center"><img src="${sessionScope.previewRetailerUploadList.retailerImagePath}" alt="${sessionScope.previewRetailerUploadList.retailerName}" width="45" height="35"/></td>
								  <td><span>${sessionScope.previewRetailerUploadList.retailerName}</span>${sessionScope.previewRetailerUploadList.address1}</td>
								  <td align="right"><img src="/ScanSeeWeb/images/rightArrow.png" alt="Arrow" width="11" height="15"/></td>	   
								</tr>
					</tbody>

				</table>					
			
		 </div>
    </form:form>		
</div>	

</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet" type="text/css">
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.form.js" type="text/javascript"></script>

<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="/WEB-INF/jsp/retailer/retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR evntsSctn">
			<div class="grpTitles">
				<h1 class="mainTitle">Build Event Page</h1>
			</div>
			<div class="section">
				<div class="grdSec brdrTop">
					 <ul class="instinfo">
						<li class="sub-title">Instructions</li>
						<li>You can add an Event to both your AppSite as well as the event section of the app by completing the information noted below.</li>
						<li>Step 1:  Enter the title of your Event.  There is a 40 character limit.</li>
						<li>Step 2:  Choose an image to appear next to your Event.  You can upload from your desktop and use the built in cropping tool to size it correctly.</li>
						<li>Step 3:  Provide a short description.  This can be a short marketing teaser for your Event.  There is a 200 character limit.</li>
						<li>Step 4:  Provide a detailed (or long) description of your Event.  There is no character limit. </li>
						<li>Step 5:  Select a category for your Event.  This is the grouping that your Event will be a part of when searched by event type (e.g. Family, Arts, etc.). </li>
						<li>Step 6:  If you have additional details on a website you would like to reference, add that URL.</li>
						<li>Step 7:  If the event is re-occurring, check Yes for Event Ongoing</li>
						<li>Step 8:  Select a start date and time for your Event. You may also select and end date and time but this is optional.</li>
						<li>Step 9:  If your Event is being held at your place of business, check Yes for the Is Event at a Business?</li>
						<li>Step 10:  Enter the address where your Event is being held</li>
						<li>Step 11:  Click Submit to save your Event information.</li>
					</ul>
					<div class="navTabSec mrgnRt" align="right">
						<input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Back"/>
					</div>
				</div>
				
				<div id="results"></div>
			</div>


		</div>
		<div class="clear"></div>
	</div>
</div>
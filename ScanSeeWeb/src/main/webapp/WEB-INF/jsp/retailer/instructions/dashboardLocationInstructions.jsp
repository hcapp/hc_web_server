<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.RetailerLocation"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<link rel="stylesheet" type="text/css" href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>

	<div id="bubble_tooltip">
		<div class="bubble_top">
			<span></span>
		</div>
		<div class="bubble_middle">
			<span id="bubble_tooltip_content"></span>
		</div>
		<div class="bubble_bottom"></div>
	</div>
	<div id="wrapper">
			<div id="content" class="shdwBg">
			<%@ include file="/WEB-INF/jsp/retailer/retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">
						<img width="36" height="36" alt="addloc"
							src="../images/addLocIcon.png"> Update Locations
					</h1>
				</div>
				<div class="clear"></div>
				<div class="grdSec brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<tr>
							<td colspan="2" class="">Add a single location at a time by
								selecting <strong>'Input'</strong>, add multiple locations by
								selecting <strong>'Choose'</strong> or select a location below
								to create a <strong>QR code</strong> or edit location
								information.
								<div>You can make changes to any single location.  You can also generate a QR code to put on any promotional materials to direct customers to your AppSite.</div>
								<div class="MrgnTop btmPadding">
									<input type="button" value="Input" class="btn" title="Input"
										onclick="window.location.href='/ScanSeeWeb/retailer/addlocation_dashboard.htm'" />
									<input type="button" value="Choose" class="btn" title="Choose"
										onclick="window.location.href='/ScanSeeWeb/retailer/location_dashboardsetup.htm'" />
								</div>
							</td>
						</tr>
					</table>
					<div align="center" style="font-style: 90">
						<label style="${requestScope.manageLocation}"><c:out
								value="${requestScope.successMSG}" /> </label>
					</div>
					<ul class="instinfo">
						<li class="sub-title">Instructions</li>
						<li>Step 1:  Enter your Store number in the Store Identification field. You will enter one location at a time.</li>
						<li>Step 2:  Enter the address for this Store.</li>
						<li>Step 3:  Add a general number for the Store.</li>
						<li>Step 4:  Add your website URL.</li>
						<li>Step 5:  Add keywords. Enter all of the terms you think someone would use to search for what you do. Add all variations of your name (e.g. common misspellings).</li>
						<li>Step 6:  Choose a Logo that you want to use for this location.</li>
						<li>Step 7:  Click Submit when you are happy with everything.</li>
					</ul>
					<div class="navTabSec mrgnRt" align="right">
						<input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Back"/>
					</div>
				</div>				
			</div>
		</div>
	</div>
<div class="clear"></div>


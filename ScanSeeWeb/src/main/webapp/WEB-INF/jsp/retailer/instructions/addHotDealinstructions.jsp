<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<script src="scripts/global.js" type="text/javascript"></script>

<!--<script>
      $(document).ready(function() {
            $("#datepicker1").datepicker();
      });

      $(document).ready(function() {
          $("#datepicker2").datepicker();
	  });
      $(document).ready(function() {
			 radioSel('slctOpt')                            
			$('input[name="slctOpt"]').click(function() {
			radioSel('slctOpt')                                               
      });
 });
</script>
-->
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>


<div id="wrapper">
<div id="content" class="shdwBg">
		<%@include file="/WEB-INF/jsp/retailer/retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR">
			<div class="grpTitles">
        <h1 class="mainTitle">${requestScope.dealtype} Deal</h1>
      </div>
	  <div class="section">
	  <div class="grdSec brdrTop">
	 <ul class="instinfo">
						<li class="sub-title">Instructions</li>
	  
<li>Step 1:  Provide a title for your Deal.  The title will be how your deal is listed on the Deals Page.</li>
<li>Step 2:  Enter the List Price of your item. Please note that you need to create a Deal for each item.</li>
<li>Step 3:  Enter the Sales Price for your item(s).</li>
<li>Step 4:  Enter a short Description.  This will be on the menu and should be less than 6 words.  </li>
<li>Step 5:  Enter a Long Description.  This should include the details of your Deal.  </li>
<li>Step 6:  Choose an image to include that represents your special.</li>
<li>Step 7:  Select the location you want to apply this Special to.  Each location can have a distinct and different Special.</li>
<li>Step 8:  Select a Product Category that you want your Deal to be listed in.</li>
<li>Step 9:  Enter a Start Date.  This is the first day your Deal will appear. </li>
<li>Step 10.  Enter an End Date.  This is the last day your Deal will be removed from the app. </li>
<li>Optional:  </li>
<li>You can enter a Start and End Time to further refine when you Special will be visible and when it will go away.  </li>
<li>You can specify the date and time that the customer has to redeem the Deal by.  </li>
<li>You can restrict the number of Deals that you make available.  For instance, the Deal will go away after 7 of them have been redeemed.</li>
<li>Step 11:  Preview how your Deal will look in the app before applying in the app. Android, iPhone 6, iPhone 6+</li>
<li>Step 12:  Click Submit when you are happy with your Deal.</li>
	</ul> 
	  			
			<div class="navTabSec RtMrgn" align="right">

				<input class="btn"
						onclick="location='javascript:history.back()'"
					id = "back" value="Back" type="button" name="Cancel3" /> 
			</div>
			<div>
					
			
			
			</div>
			</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>

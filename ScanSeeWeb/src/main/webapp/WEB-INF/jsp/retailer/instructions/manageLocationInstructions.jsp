<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.RetailerLocation"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<link rel="stylesheet" type="text/css" href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript" src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>

	<div id="bubble_tooltip">
		<div class="bubble_top">
			<span></span>
		</div>
		<div class="bubble_middle">
			<span id="bubble_tooltip_content"></span>
		</div>
		<div class="bubble_bottom"></div>
	</div>
	<div id="wrapper">
			<div id="dockPanel">
				<ul id="prgMtr" class="tabs">
				<li><a title="Choose Ads/Logos" href="uploadRetailerLogo.htm"
					rel="Choose Ads/Logos">Choose Logo</a></li>
				<li><a class="tabActive" title="Location Setup"
					href="locationsetup.htm" rel="Location Setup">Location Setup</a></li>
				<li><a title="Choose Plan"
					href="/ScanSeeWeb/retailer/retailerchoosePlan.htm"
					rel="Choose Plan">Choose Plan</a></li>
				<li><c:choose>
						<c:when test="${isPaymentDone == null}">
							<a title="Dashboard" href="/ScanSeeWeb/retailer/retailerhome.htm"
								rel="Dashboard">Dashboard</a>
						</c:when>
						<c:otherwise>
							<a title="Dashboard" href="#" rel="Dashboard">Dashboard</a>
						</c:otherwise>
					</c:choose></li>
				</ul>
				<a id="Mid" name="Mid"></a>
				<div class="floatR tglSec" id="tabdPanelDesc">
					<div id="filledGlass">
						<img src="../images/Step5.png" />
						<div id="nextNav">
							<a href="#"
								onclick="location.href='/ScanSeeWeb/retailer/regaddseachprod.htm'">
								<img class="NextNav_R" alt="Next" src="../images/nextBtn.png"
								border="0" /> <span>Next</span>
							</a>
						</div>
					</div>
				</div>
				<div class="floatL tglSec" id="tabdPanel">
					<img height="283" alt="Flow1" src="../images/Flow5_ret.png"
						width="782" />
				</div>
			</div>
			<div class="clear"></div>
			<div id="content">
				<div id="subnav">
					<ul>
						<li><a href="/ScanSeeWeb/retailer/addlocation.htm"><span>Add
									Location(s)</span> </a></li>
						<li><a href="/ScanSeeWeb/retailer/locationsetup.htm"><span>Upload
									Location</span> </a></li>
						<li><a href="#" class="active"><span>Update
									Locations</span> </a></li>
					</ul>
				</div>
				<div class="grdSec">
					<ul class="instinfo">
						<li class="sub-title">Instructions</li>
						<li>Step 1:  Enter your Store number in the Store Identification field. You will enter one location at a time.</li>
						<li>Step 2:  Enter the address for this Store.</li>
						<li>Step 3:  Add a general number for the Store.</li>
						<li>Step 4:  Add your website URL.</li>
						<li>Step 5:  Add keywords. Enter all of the terms you think someone would use to search for what you do. Add all variations of your name (e.g. common misspellings).</li>
						<li>Step 6:  Choose a Logo that you want to use for this location.</li>
						<li>Step 7:  Click Submit when you are happy with everything.</li>
					</ul>
					<div class="navTabSec mrgnRt" align="right">
						<input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Back"/>
					</div>
				</div>
			</div>
	</div>
<div class="clear"></div>


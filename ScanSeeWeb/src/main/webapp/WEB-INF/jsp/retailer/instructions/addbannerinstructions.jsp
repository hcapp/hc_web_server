<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>HubCiti</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)" />
<link href="styles/style.css" type="text/css" rel="stylesheet" />
<link href="styles/ticker-style.css" type="text/css" rel="stylesheet" />
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>

<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>

<script src="scripts/global.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">

<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>

	


<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>

</head>
<body>
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@include file="/WEB-INF/jsp/retailer/retailerLeftNavigation.jsp"%>
		
				<div class="rtContPnl floatR">

				
				
					<div class="grpTitles">
						<h1 class="mainTitle">C. ${requestScope.bptype} Banner </h1>
					</div>

					<div class="section">
						<div class="grdSec brdrTop">
						
							   <ul class="instinfo">
							<li class="sub-title">Instructions</li>
						   <li>Step 1: Select the location(s) where you would like for this Banner to show. Each location can have a distinct and different Banner.</li>
							<li>Step 2: Enter a name for your Banner.</li>
							<li>Step 3: If you want your banner to link to your website or social page, enter a URL in the Banner Web Link box.</li>
							<li>Step 4: Click on the Choose File button to upload the image for your Banner. Then select the file.</li>
							<li>Step 5: Crop the image to fit the supported dimensions.  To achieve the highest resolution start with an image that is 800X600px.</li>
							<li>Step 6: Click Save Image.</li>
							<li>Step 7: Select the Preview button to see how the image will appear in the app.  If you want to make modifications, either re-crop the image, modify it and re-upload or select a new image.</li>
							<li>Step 8:  If you want to manage when the splash page appears, enter a Start Date and End Date for the page.</li>
							<li>Step 9:  Click the Submit button</li>

			
						</ul>
						</div>
															
							<div class="navTabSec mrgnRt" align="right">

							
									<input class="btn"
										onclick="location='javascript:history.back()'"
										id="back" value="Back" type="button" name="Cancel3"
										title="Back" tabindex="17" />
						<div>
					
			
			
			</div>
			</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>

</body>			
						

</html>




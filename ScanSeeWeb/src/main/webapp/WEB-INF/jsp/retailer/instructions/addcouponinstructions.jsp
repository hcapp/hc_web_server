<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Deal Instructions</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)" />
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)" />
<link href="styles/style.css" type="text/css" rel="stylesheet" />
<link href="styles/ticker-style.css" type="text/css" rel="stylesheet" />
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>

<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>

<script src="scripts/global.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">

<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>

	


<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>

</head>
<body>
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@include file="/WEB-INF/jsp/retailer/retailerLeftNavigation.jsp"%>
		
				<div class="rtContPnl floatR">

				
				
					<div class="grpTitles">
						<h1 class="mainTitle">${requestScope.cptype} Deal</h1>
					</div>

					<div class="section">
						<div class="grdSec brdrTop">
						
							   <ul class="instinfo">
						<li class="sub-title">Instructions</li>
		<li>Step 1: Provide a title for your Deal. The title will be how your deal is listed on the Deals Page.</li>
		<li>Step 2: Enter the Banner title of your item. Please note that you need to create a Deal for each item.</li>
		<li>Step 3: Enter the total number of deals that you want to issue. The Deal will go away once that number is reached.</li>
		<li>Step 4: Select the location(s) you want to apply this Deal to. Each location can have a distinct and different Deal.</li>
		<li>Step 5: Enter a Description. This should include the details of your Deal that you want the customer to know about.</li>
		<li>Step 6: Enter the Terms and Conditions (e.g. Limit one deal per guest per day).</li>
		<li>Step 7: Choose an image to include that represents your deal.</li>
		<li>Step 8: Enter a Start Date. This is the first day your Deal will appear.</li>
		<li>Step 9: Enter an End Date. This is the last day your Deal will be removed from the app.</li>
		<li>Step 10: Enter an Expiry Date. This is the expiration date of your Deal.</li>
		<li>Optional:</li>
		<li>You can enter a Start and End Time to further refine when you Deal will be visible and when it will go away.</li>
		<li>Step 11: Click Submit when you are happy with your Deal.</li>
			
						</ul>
						</div>
															
							<div class="navTabSec mrgnRt" align="right">

							
									<input class="btn"
										onclick="location='javascript:history.back()'"
										id="back" value="Back" type="button" name="Cancel3"
										title="Back" tabindex="17" />
						<div>
					
			
			
			</div>
			</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>

</body>			
						

</html>




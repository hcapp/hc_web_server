 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.form.js" type="text/javascript"></script>

<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="/WEB-INF/jsp/retailer/retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR evntsSctn">
			<div class="grpTitles">
				<h1 class="mainTitle">Build Fundraiser</h1>
			</div>
			<div class="section">
				<div class="grdSec brdrTop">
					<ul class="instinfo">
						<li class="sub-title">Instructions</li>
						<li>You can add promote your Fundraiser in your AppSite as well as the event section of the app by completing the information noted below.  </li>
						<li>Step 1:  Enter the title of your Fundraiser.  There is a 40 character limit.</li>
						<li>Step 2:  Choose an image to appear next to your Fundraiser.  You can upload from your desktop and use the built in cropping tool to size it correctly.</li>
						<li>Step 3:  Provide a short description.  This can be a short marketing teaser for your Fundraiser.  There is a 200 character limit.</li>
						<li>Step 4:  Provide a detailed (or long) description of your Fundraiser.  There is no character limit.</li>
						<li>Step 5:  Select a category for your Fundraiser.  This is the grouping that your event will be a part of when searched by event type (e.g. Family, Arts, etc.).</li>
						<li>Step 6:  If you have additional details on a website you would like to reference, add that URL.</li>
						<li>Step 7:  Select a start date and time for your Fundraiser. You may also select and end date and time but this is optional.</li>
						<li>Step 8:  If your Fundraiser is tied to an event, you can make that connection through the Tied to an Event checkbox and then selecting the appropriate events.</li>
						<li>Step 10:  Enter the address where your Fundraiser is being held.</li>
						<li>Step 11:  You can display your goals for the Fundraiser.  Enter that in the Fundraising Goal box.</li>
						<li>Step 12:  Enter a description of what you are selling for your Fundraiser.</li>
						<li>Step 13:  Keep people updating on how close you are to your goal in the Current Level box.</li>
						<li>Step 14:  Click Submit to save your event</li>
					</ul>
					<div class="navTabSec mrgnRt" align="right">
						<input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Back"/>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
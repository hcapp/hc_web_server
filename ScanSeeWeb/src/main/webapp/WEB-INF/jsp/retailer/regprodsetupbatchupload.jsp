
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="common.pojo.ProductVO"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script src="scripts/web.js" type="text/javascript"></script>
<div id="wrapper">


	<div id="dockPanel">
		<ul id="prgMtr" class="tabs">
			<li><a href="https://www.scansee.net/links/aboutus.html" title="About HubCiti"
				target="_blank" rel="About HubCiti">About HubCiti</a>
			</li>
			<!--<li> <a title="Create Profile" href="Retailer/Retailer_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->

			<li><a title="Upload Ads/Logos" href="uploadRetailerLogo.htm"
				rel="Upload Ads/Logos">Upload Logo</a>
			</li>
			<li><a title="Location Setup" href="addlocation.htm" rel="Location Setup">Location Setup</a>
			</li>
			<li><a class="tabActive" title="Product Setup"
				href="/ScanSeeWeb/retailer/regbatchuploadretprod.htm"
				rel="Product Setup">Product Setup</a>
			</li>
			<li><a title="Choose Plan"
				href="/ScanSeeWeb/retailer/retailerchoosePlan.htm" rel="Choose Plan">Choose
					Plan</a>
			</li>
			<li>
				<c:choose>
					<c:when test="${isPaymentDone == null}">
						<a title="Dashboard" href="/ScanSeeWeb/retailer/retailerhome.htm" rel="Dashboard">Dashboard</a>
					</c:when>
					<c:otherwise>
						<a title="Dashboard" href="#" rel="Dashboard">Dashboard</a>
					</c:otherwise>
				</c:choose>	
			</li>
		</ul>
		<a id="Mid" name="Mid"></a>
		<div class="floatR tglSec" id="tabdPanelDesc">
			<div id="filledGlass">
				<img src="/ScanSeeWeb/images/Step3.png" />
				<div id="nextNav">
					<a href="#"
						onclick="location.href='/ScanSeeWeb/retailer/retailerchoosePlan.htm'">
						<img class="NextNav_R" alt="Next" src="/ScanSeeWeb/images/nextBtn.png" /> <span>Next</span>
					</a>
				</div>

			</div>
		</div>
		<div class="floatL tglSec" id="tabdPanel">
			<img alt="Flow1" src="/ScanSeeWeb/images/Flow6_ret.png" />
		</div>
	</div>
	<!-- <div id="togglePnl">
		<a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down" width="9"
			height="8" /> Show Panel</a>
	</div> -->
	<div class="clear"></div>

	<div id="content">
		<div id="subnav">

			<ul>
				<li><a href="/ScanSeeWeb/retailer/regbatchuploadretprod.htm"
					class="active"><span>Batch Upload</span> </a></li>
				<li><a href="/ScanSeeWeb/retailer/regaddseachprod.htm"><span>Search
							/ Add Product</span> </a></li>
			</ul>
		</div>
		<div class="grdSec">
			<ul class="lstInfo">
				<form:form commandName="batchUploadretprodform"
					name="batchUploadretprodform" enctype="multipart/form-data">
					<li>Please upload a file with all of your product information,
						or scroll below to manually enter each location.</li>

					<li>Uploading your product information will allow consumers to
						view your product(s) online and on the mobile application as shown
						in the preview below.</li>
					<li>Choose a Product File to Upload:</li>
					<li><form:input class="textboxBig" id="fileName"
							path="productuploadFilePath" type="file" /><span class="instTxt nonBlk">[ Please Upload .csv  format file ]</span><form:errors cssStyle="${requestScope.productuploadFile}" path="productuploadFilePath"/></li>
					<li><input class="btn" type="button" value="Choose File"
						name="Cancel2" title="Submit" onclick="uploadRetProductFile();" />
					
	<a href="/ScanSeeWeb/fileController/download.htm?TType=rproductpload"><img alt="Download Template" src="../images/download.png"/>&nbsp Download template</a>
					</li>
					<li>For upload instructions,<a href="javascript:void(0);"
						onClick="window.open('/ScanSeeWeb/html/Retailer_ProdSetup_instructions.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">
							click here.</a></li>
					<li>For frequently asked questions,<a
						href="javascript:void(0);"
						onClick="window.open('/ScanSeeWeb/html/Retailer_LocSetup_Faq.html','mywindow','width=1000,height=600,left=130,top=140,scrollbars=yes')">
							click here.</a></li>
					<li>For recurring XML or API setups, please contact <a
						href="mailto:support@hubciti.com?Subject=XML/API%20setup&body=I%20am%20interested%20in%20a%20XML/API%20setup.">support@hubciti.com.</a>
					</li>

				</form:form>
			</ul>
		</div>

		<div class="clear"></div>
	</div>
</div>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="content">
	<%@include file="retailerLeftNavigation.jsp"%>

    <div class="rtContPnl floatR">
      <div class="grpTitles">
        <h1 class="mainTitle">Events</h1>
      </div>
      <div class="textSec brdrTop">
        <p>Want people to know something special about what is going on at your location?</p>
        <!-- <h4 class="MrgnTop">Step by Step</h4>-->
        <p class="MrgnTop"> Add Information and Promote Special Events!</p>
        <div class="MrgnTop">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
            <tr>
              <td width="25%" align="center"><img src="../images/iphone_events.png" alt="Music Events" width="176" height="344" /></td>
              <td width="25%" align="center"><img src="../images/iphone_events2.png" alt="School Events" width="176" height="344" /></td>
              <td width="25%" align="center"><img src="../images/iphone_events3.png" alt="Sports Events" width="176" height="344" /></td>
            </tr>
            <tr>
              <td align="center" valign="top">Music Events</td>
              <td align="center" valign="top">School Events</td>
              <td align="center" valign="top">Sports Events</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="center"><input type="button" value="Add Event" class="btn MrgnTop" onclick="window.location.href='addevent.htm'"
									title="Add Event"/></td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </div>

      </div>
    </div>




<div class="clear"></div>
</div>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<script src="/ScanSeeWeb/ckeditor/config.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script type="text/javascript">
	$(document).ready(
			function() {

				CKEDITOR.on('instanceCreated', function(e) {
					//	alert("q : "+e.editor.name);
					var editorName = e.editor.name;
					e.editor.on('change', function(ev) {
						if (editorName == 'longDescription') {

						} else if (editorName == 'shortDescription') {

						}

					});
				});

				/* 	CKEDITOR.config.toolbar =
						[
						    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
						    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
						    { name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
						    '/',
						    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
						    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
						    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
						    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
						    '/',
						    { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
						    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
						    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] }
						]; */

				/* CKEDITOR.config.toolbar = [
				                    		{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] } ,
				                    		{ name: 'styles', items: [ 'Styles', 'Format' ] },
				                    	];
				
				CKEDITOR.config.toolbarGroups = [
				                    	{ name: 'basicstyles', groups: [ 'basicstyles', 'styles' ] }
				                    	
				                    ]; */
				CKEDITOR.config.uiColor = '#FFFFFF';
				CKEDITOR.replace('longDescription', {
					extraPlugins : 'onchange',
					width : "660",
					toolbar : [
							/* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
							{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
							{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
							'/',
							{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
							{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
							{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
							{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
							'/',
							{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
							{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
							{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
							{
								name : 'basicstyles',
								items : [ 'Bold', 'Italic', 'Underline' ]
							},
							{
								name : 'paragraph',
								items : [ 'JustifyLeft', 'JustifyCenter',
										'JustifyRight', 'JustifyBlock' ]
							}, '/', {
								name : 'styles',
								items : [ 'Styles', 'Format' ]
							}, '/', {
								name : 'tools',
								items : [ 'Font', 'FontSize', 'RemoveFormat' ]
							}, '/', {
								name : 'colors',
								items : [ 'BGColor' ]
							}, {
								name : 'paragraph',
								items : [ 'Outdent', 'Indent' ]
							}, {
								name : 'links',
								items : [ 'Link', 'Unlink' ]
							} ],
					removePlugins : 'resize'
				});
				CKEDITOR.replace('shortDescription', {
					extraPlugins : 'onchange',
					width : "660",
					toolbar : [
							{
								name : 'basicstyles',
								items : [ 'Bold', 'Italic', 'Underline' ]
							},
							{
								name : 'paragraph',
								items : [ 'JustifyLeft', 'JustifyCenter',
										'JustifyRight', 'JustifyBlock' ]
							}, '/', {
								name : 'styles',
								items : [ 'Styles', 'Format' ]
							}, '/', {
								name : 'tools',
								items : [ 'Font', 'FontSize', 'RemoveFormat' ]
							}, '/', {
								name : 'colors',
								items : [ 'BGColor' ]
							}, {
								name : 'paragraph',
								items : [ 'Outdent', 'Indent' ]
							}, {
								name : 'links',
								items : [ 'Link', 'Unlink' ]
							} ],
					removePlugins : 'resize'
				});

				$("#StrtDT").datepicker({
					showOn : 'both',
					buttonImageOnly : true,
					buttonText : 'Click to show the calendar',
					buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
				});
				$('.ui-datepicker-trigger').css("padding-left", "5px");
				$("#EndDT").datepicker({
					showOn : 'both',
					buttonImageOnly : true,
					buttonText : 'Click to show the calendar',
					buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
				});
				$('.ui-datepicker-trigger').css("padding-left", "5px");

				// Populate Short Description in load
				if ($("#shortDescription").val() != '') {
					var stt = $("#shortDescription").val();
					var reNewLines = /[\n\r]/g;
					$(".shrtDesc").html(stt.replace(reNewLines, "<br />"));
				}

				// Populate long Description in load
				if ($("#longDescription").val() != '') {
					var ltt = $("#longDescription").val();
					var reNewLines = /[\n\r]/g;
					$(".lngDesc").html(ltt.replace(reNewLines, "<br />"));
				}

			});

	function showPagePreview(pageTyp) {

		var createForm = document.createCustomPage;
		var imagePath = $("#customPageImg").attr('src')

		var specialImagepath = $("#customSplPageImg").attr('src');
		// Do validation before you call for Preview

		if (pageTyp == 'SplOffer') {

			//For specail offer page validtion

			var title = document.createCustomPage.retPageTitle.value;

			var retShortDesc = document.createCustomPage.longDescription.value;
			var retPageDesc = document.createCustomPage.shortDescription.value;
			var specialStartDate = document.createCustomPage.retCreatedPageStartDate.value;
			var specialEndDate = document.createCustomPage.retCreatedPageEndDate.value;

			if (title == "") {
				alert('Please enter title');
				return;
			} else if (retPageDesc == "") {
				alert('Please enter  page description');
				return;
			} else if (retShortDesc == "") {
				alert('Please enter  short description');
				return;
			} else if (specialStartDate == "") {
				alert('Please enter special offer start date');
				return;
			} else {
				showGiveawayPagePview(createForm, imagePath);
			}

		}
	}
</script>

<div id="wrapper">
	<div id="content">

		<%@ include file="retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR">
			<div class="grpTitles">
				<h1 class="mainTitle">Giveaway Page</h1>
			</div>
			<div class="section">
				<div class="grdSec brdrTop">
					<form:form name="createCustomPage" id="createCustomPage"
						commandName="createCustomPage" action="uploadtempretimg.htm"
						enctype="multipart/form-data" acceptCharset="ISO-8859-1">
						<form:hidden path="viewName" value="updategiveawaypage" />
						<form:hidden path="retailerImg" id="retailerImg" />
						<form:hidden path="imageName" id="imageName" />
						<form:hidden type="hidden" id="pageId" path="pageId" />
						<input type="hidden" id="uploadBtn" name="uploadBtn">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="lblSpan cstmpg" id="rtlr">
							<tr>
								<td colspan="4" class="header">Giveaway Main Page:</td>
							</tr>
							<tr>
								<td class="Label"><label for="atchLinkTitle" class="mand">Title</label>
								</td>
								<td colspan="3"><form:input name="rtlrTitle" tabindex="1"
										path="retPageTitle" type="text" id="retPageTitle"
										maxlength="1000" style=" width: 656px;" /> <form:errors
										path="retPageTitle" cssStyle="color:red"></form:errors>
								</td>
							</tr>
							<tr>
								<td width="10%" class="Label"><label for="atchLinkTitle"
									class="mand">Photo</label>
								</td>
								<td colspan="4" class="brdRt"><ul class="imgInfoSplit">
										<li><label for="hdSp"></label> <label><img
												id="customPageImg" width="80" height="80" alt="upload"
												src="${sessionScope.customPageRetImgPath}"
												onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
										</label><span class="topPadding forceBlock"><label
												for="trgrUpld"><input type="button"
													value="${sessionScope.customPageBtnVal }" id="trgrUpldBtn"
													class="btn trgrUpld" title="Choose File" tabindex="2">
													<form:input type="file" id="trgrUpld" class="textboxBig"
														path="retImage" /> <form:errors path="retImage"
														cssStyle="color:red" /> </label><label id="customPageImgErr"
												style="color: red; font-style: 45"></label> </span>
										</li>
										<li>Suggested Minimum Size:<br>70px/70px<br>Maximum Size:800px/600px<br>
											<!-- <b> Maximum Size:</b><br>950px/1024px --></li>
									</ul>
								</td>
							</tr>
							<tr>
								<td class="Label"><label for="rtlrPgDesc">Rules</label></td>
								<td colspan="3" class="brdRt"><form:textarea tabindex="3"
										readonly="readonly" path="rule" name="rules"
										class="txtAreaLarge" id="rules"></form:textarea></td>
							</tr>
							<tr>
								<td class="Label">Terms &amp; Conditions</td>
								<td colspan="3" class="brdRt"><form:textarea
										path="termsandConditions" name="tandc" class="txtAreaLarge"
										tabindex="4" id="tandc"></form:textarea><span class="terms">HubCiti
										is not the sponsor of the deal</span></td>
							</tr>
							<tr>
								<td class="Label"><label for="atchLinkTitle" class="mand">Short
										Description</label></td>
								<td colspan="3"><form:textarea id="shortDescription"
										name="rtlrshrtDsc" tabindex="5" path="shortDescription"></form:textarea>
									<form:errors path="shortDescription" cssStyle="color:red"></form:errors>
								</td>
							</tr>
							<tr>
								<td class="Label"><label for="atchLinkTitle" class="mand">Long
										Description</label></td>
								<td colspan="3" class="brdRt"><form:textarea
										id="longDescription" name="rtlrlngDsc" path="longDescription"
										tabindex="6" cols="45" rows="5" class="txtAreaSmall"></form:textarea>
									<form:errors path="longDescription" cssStyle="color:red"></form:errors>
								</td>
							</tr>

							<tr>
								<td class="Label"><label for="atchLinkTitle" class="mand">Start
										Date</label></td>
								<td class="brdRt"><label><form:input
											path="retCreatedPageStartDate" type="text" id="StrtDT"
											class="textboxDate" tabindex="7" /> <form:errors
											path="retCreatedPageStartDate" cssStyle="color:red">
										</form:errors> </label></td>
								<td width="9%" align="left" valign="top" class="Label">End
									Date</td>
								<td width="49%" align="left" valign="top"><form:input
										path="retCreatedPageEndDate" type="text" id="EndDT"
										class="textboxDate" name="EndDT2" tabindex="8" /> <form:errors
										path="retCreatedPageEndDate" cssStyle="color:red">
									</form:errors> <br> <span class="instTxt nonBlk">[End date is not
										required]</span></td>
							</tr>
							<tr>
								<td class="Label"><label for="atchLinkTitle" class="mand">Quantity</label>
								</td>
								<td colspan="3" class="brdRt"><label> <form:input
											path="quantity" type="text" name="qty" id="qty" tabindex="9"
											onkeypress="return isNumberKeyPhone(event)"
											class="textboxDate" /> <form:errors path="quantity"
											cssStyle="color:red"></form:errors> </label></td>
							</tr>
							<tr>
								<td class="Label">&nbsp;</td>
								<td colspan="3" class="brdRt"><input type="button"
									class="btn" value="Save" onclick="updateGiveawayInfo();"
									tabindex="10" title="Save" /> <input type="button" class="btn"
									value="Preview"
									onclick="javascript:showPagePreview('SplOffer')" tabindex="11"
									title="Preview" /><input class="btn"
									onclick="javascript:history.back()" value="Back" type="button"
									name="Back" tabindex="12" title="Back" /></td>
							</tr>
						</table>
						<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>


	<div class="clear"></div>
</div>

<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {
						$("#uploadBtn").val("trgrUpldBtn");

						$("#createCustomPage")
								.ajaxForm(
										{
											success : function(response) {

												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue

												if (imgRes == 'UploadLogoMaxSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {

													//$('#customPageImg').attr("src",imgRes);
													//	$('#customImg').attr("src", imgRes);
													$('#customPageImgErr')
															.text("");
													var substr = imgRes
															.split('|');

													if (substr[0] == 'ValidImageDimention') {
														var imgName = substr[1];
														$('#retailerImg').val(
																imgName);
														$('#customPageImg')
																.attr(
																		"src",
																		substr[2]);
														$('#customImg').attr(
																"src",
																substr[2]);

													} else {
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}

											}
										}).submit();

					});
</script>
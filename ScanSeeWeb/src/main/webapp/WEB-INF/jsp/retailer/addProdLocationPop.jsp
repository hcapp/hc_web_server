<%@ page language="java" contentType="text/html;"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page import="common.pojo.RetailerLocationProduct"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<head>
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.tablescroll.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>

</head>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>


$(document).ready(function() {
$("#aSaleStartDate").datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar' ,buttonImage: '../images/calendarIcon.png' });
$("#aSaleEndDate").datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar' ,buttonImage: '../images/calendarIcon.png' });
	if($('#prodinfotbl tr').length <= 3) { $('.hrzscroll').css('height','auto'); }
<c:forEach items="${sessionScope.productDetails}"
									var="item">
		$("#startdate"+${item.rowNumber}).datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar' ,buttonImage: '../images/calendarIcon.png' });
$("#enddate"+${item.rowNumber}).datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar',buttonImage: '../images/calendarIcon.png' });						
	</c:forEach>	

	//In manage product pop screen: Check & Uncheck checkboxes 
	 $('input[name$="selectAll"]').click(function() {
	  var status = $(this).attr("checked");
	  $('input[name$="locationSelected"]').attr('checked',status);
	  if(!status) {
	  $('input[name$="locationSelected"]').removeAttr('checked');
	  } 
	 });
	 
	 /*Onload checking for Selected All*/
	 var tolCnt = $('input[name$="locationSelected"]').length; 
	 var chkCnt = $('input[name$="locationSelected"]:checked').length;
		if(tolCnt!=0&&chkCnt!=0){
			 if(tolCnt == chkCnt){
		  $('input[name$="selectAll"]').attr('checked','checked');
			 }else{
			 $('input[name$="selectAll"]').removeAttr('checked');
			 }
		  
			}
	
	  /*End */
	  
	 /*In manage product pop screen: if all child checkbox is unchecked then uncheck parent check box.
	 If all child checkbox is checked check parent checkbox*/
	 $('input[name$="locationSelected"]').click(function() {              
	 var tolCnt = $('input[name$="locationSelected"]').length; 
	 var chkCnt = $('input[name$="locationSelected"]:checked').length;
	 if(tolCnt == chkCnt)
	  $('input[name$="selectAll"]').attr('checked','checked');
	 else
	  $('input[name$="selectAll"]').removeAttr('checked');
	 });
});	
		
</script>
<script type="text/javascript">
function rowCounts() {
	var vRowNo = $('#addProdLoctable tr').length;
	var vLocsltd =$('input[name$="locationSelected"]:checked').length;
		if (vRowNo <= 1) {
			alert("No records to save");
			return false;
		}
		
		if (vLocsltd > 0) {
			var result = confirm("Do you want to save the changes");
			if (result) {
				editLocationDetails();
			}
		} else {
			alert("Need to select at least one Business Location!");
			return false;
		}
}
function submit() {
	alert("Inside submint");
	var vLocsltd = $('input[name$="locationSelected"]:checked').length;
	alert(vLocsltd);
	for (i=0; i<vLocsltd.length; i++){
		if (vLocsltd[i].checked==true) {
			alert("Checkbox at index "+i+" is checked!")
		}
	}
}



</script>

<form:form action="/ScanSeeWeb/retailer/fetchretaillocationlist.htm"
	path="manageProductForm" commandName="manageProductForm"
	id="manageProductForm" name="manageProductForm"
	acceptCharset="ISO-8859-1">
	<form:hidden path="productID" />

	<div class="noBg contWrap">
		<div id="bubble_tooltip">
			<div class="bubble_top">
				<span></span>
			</div>
			<div class="bubble_middle">
				<span id="bubble_tooltip_content">Content is comming here as
					you probably can see.Content is comming here as you probably can
					see.</span>
			</div>
			<div class="bubble_bottom"></div>
		</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="grdTbl zeroBtmMrgn">
			<tr>
				<td width="6%"><img height="46" alt="Product Image" width="49"
					src="${sessionScope.productImage}"
					onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';" />
				</td>
				<td width="94%"><c:out value="${sessionScope.productName}" />
				</td>
				</td>
			</tr>
			<!-- <tr>
      <td colspan="2" align="left"><ul class="cntrlBar">
          <li> <a href="#" id="applyAll" onclick="clearApplyAll();"><img src="/ScanSeeWeb/images/addexpnd.png" alt="Apply All" width="19" height="17" title="Apply All" /> Apply All</a></li>
          <li>|</li>
          <li><a href="#" id="searchBar" onclick="clearSearch();"><img src="/ScanSeeWeb/images/searchIcon.png" alt="search" width="17" height="15" title="Search" /> Search</a> </li>
        </ul></td>
    </tr> -->
		</table>
		<!--  <div id="tglOptions">
    <table  border="0" cellspacing="0" cellpadding="0" width="100%" class="grdTbl zeroBtmMrgn" id="saveToLocation">
      <thead>
        <tr class="Label">
          <td>Price</td>
          <td>Sale Price</td>
          <td>Start Date (mm/dd/yyyy)</td>
          <td>End Date (mm/dd/yyyy) </td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td align="left"><input type="text" class="textboxPrice" id="aPrice" name="price" value=""  onkeypress="return isNumberKeyPhone(event)" /></td>
          <td align="left"><input type="text" class="textboxPrice" id="aSalePrice" name="salePrice" value=""  onkeypress="return isNumberKeyPhone(event)"/></td>
          <td align="left"><input type="text" class="textboxDate" id="aSaleStartDate" value=""  name="saleStartDate"  /></td>
          <td align="left"><input type="text" class="textboxDate" id="aSaleEndDate" value=""  name="saleEndDate"  /></td>
        </tr>
        <tr>
          <td colspan="5" align="right"><input type="button" class="btn" value="Save" id="save" onclick="saveRetailLocationDetails();"/></td>
        </tr>
      </tbody>
    </table>
  </div>-->
		<div id="srchCntrls">
			<table class="grdTbl zeroBtmMrgn" cellspacing="0" cellpadding="0"
				width="100%" border="0">
				<tbody>
					<tr>
						<td class="Label" width="18%">Location</td>
						<td colspan="3"><form:input path="searchKey"
								class="textboxSmall" id="searchKey" /> <a href="#"> <!-- <input id="save" class="btn" type="submit" title="search" onclick="fetchRetailerLocation();" value="Search" name="Save">-->

								<img title="Search" height="17" alt="Search"
								src="/ScanSeeWeb/images/searchIcon.png" width="20"
								onclick="fetchRetailerLocation();" /> </a></td>
					</tr>
					<tr>
						<td class="Label">Price</td>
						<td width="17%">Sale Price</td>
						<td width="33%">Start Date (mm/dd/yyyy)</td>
						<td width="32%">End Date (mm/dd/yyyy)</td>
					</tr>
					<tr>
						<td align="left"><input type="text" class="textboxPrice"
							id="aPrice" name="price" value=""
							onkeypress="return isNumberKeyPhone(event)" />
						</td>
						<td align="left"><input type="text" class="textboxPrice"
							id="aSalePrice" name="salePrice" value=""
							onkeypress="return isNumberKeyPhone(event)" />
						</td>
						<td align="left"><input type="text" class="textboxDate"
							id="aSaleStartDate" value="" name="saleStartDate" />
						</td>
						<td align="left"><input type="text" class="textboxDate"
							id="aSaleEndDate" value="" name="saleEndDate" />
						</td>
					</tr>
					<tr>
						<td colspan="3" align="right"><input type="button"
							class="btn" value="Apply All" id="applyAll" name="applyAll"
							onclick="saveRetailLocationDetails('applyAll');" /> <a href="#"
							onmousemove="showToolTip(event,'Applies for all products associated to the retailer');return false"
							onmouseout="hideToolTip()"><img alt="helpIcon"
								src="../images/helpIcon.png" /> </a>
						</td>
						<td align="left"><input type="button" class="btn"
							value="Apply To Current Search" id="currentSearch"
							name="currentSearch"
							onclick="saveRetailLocationDetails('currentSearch');" /> <a
							href="#"
							onmousemove="showToolTip(event,'Applies to current search results');return false"
							onmouseout="hideToolTip()"><img alt="helpIcon"
								src="../images/helpIcon.png" /> </a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>


	<div class="">
		<c:if test="${message ne null }">
			<div id="message">
				<center>
					<c:out value="${message}" />
				</center>
			</div>
			<script>var PAGE_MESSAGE = true;</script>
		</c:if>
		<fieldset class="popupWrap">
			<div class="searchGrd zeroBtmMrgn">
				<table id="addProdLoctable" class="stripeMe" border="0"
					cellspacing="0" cellpadding="0" width="98%">
					<thead>
						<tr class="header">

							<td><input type="checkbox" name="selectAll" /></td>
							<td>Locations</td>
							<td>Price</td>
							<td>Sale Price</td>
							<td>Start Date (mm/dd/yyyy)</td>
							<td>End Date (mm/dd/yyyy)</td>
						</tr>
					</thead>
					<tbody>
						<c:if
							test="${sessionScope.productDetails ne null && ! empty sessionScope.productDetails}">
							<c:forEach items="${sessionScope.productDetails}" var="item">
								<tr id="${item.retailLocationID}">
									<c:choose>
										<c:when test="${item.productAdded == 1}">
											<td align="center"><input id="locationSelected"
												name="locationSelected" type="checkbox" checked="checked"
												value="${item.retailLocationID}" />
											</td>
										</c:when>
										<c:otherwise>
											<td align="center"><input id="locationSelected"
												name="locationSelected" type="checkbox"
												value="${item.retailLocationID}" />
											</td>
										</c:otherwise>
									</c:choose>
									<td align="left"><c:out
											value="${item.storeIdentification}" />
									</td>
									<td align="left"><input id="price" type="text"
										name="price" class="textboxPrice" value="${item.price}"
										onkeypress="return isNumberKeyPhone(event)" />
									</td>
									<td align="left"><input id="salePrice" type="text"
										name="salePrice" class="textboxPrice"
										value="${item.salePrice}"
										onkeypress="return isNumberKeyPhone(event)" />
									</td>
									<td align="left"><input id="startdate${item.rowNumber}"
										type="text" name="saleStartDate" class="textboxDate"
										value="${item.saleStartDate}" />
									</td>
									<td align="left"><input id="enddate${item.rowNumber}"
										type="text" name="saleEndDate" class="textboxDate"
										value="${item.saleEndDate}" />
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</tbody>
				</table>
			</div>
		</fieldset>
	</div>
	<div class="navTabSec RtMrgn" align="right">
		<input class="btn" value="Submit" type="button" name="btnBubmit"
			onclick="editLocationDetails();" />
	</div>
</form:form>


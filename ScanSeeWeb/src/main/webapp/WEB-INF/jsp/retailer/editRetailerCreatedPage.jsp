<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script>
	$(document).ready(function() {
		$("#StrtDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$("#EndDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		var pageType = '${sessionScope.landingPageType}';
		$('#selLandingPageType').val(pageType);
		if (pageType == 'AttachLink') {
			$('#editLndgPgSlct').val("AttachLink").trigger('change');
		} else {
			$('#editLndgPgSlct').val("Landing").trigger('change');
		}
	});
	function selectRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("rtlrcrtdLoc");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function selectAttachLinkRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("retCreatedPageAttachLinkLocId");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function deleteImage(imageName) {

		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "deleteimage.htm",
			data : {
				'imageName' : imageName
			},
			success : function(response) {

				document.getElementById(imageName).style.display = "none";
				document.createCustomPage.uploadedFileLength.value = response;

			},
			error : function(e) {
				alert('Error:' + e);
			}
		});
	}
</script>
<div class="shdwBg" id="content" style="min-height: 73px;">
	<%@include file="retailerLeftNavigation.jsp"%>
	<div class="grdSec contDsply floatR">
		<div id="secInfo">Edit Page</div>
		<form:form commandName="createCustomPage" id="createCustomPage"
			name="createCustomPage" enctype="multipart/form-data"
			action="uploadtempretimg.htm" acceptCharset="ISO-8859-1">
			<form:hidden path="pageId" id="pageId" />
			<form:hidden path="hiddenLocId" />
			<form:hidden path="imageName" id="imageName" />
			<input type="hidden" id="selLandingPageType" name="selLandingPageType">
			<input type="hidden" id="uploadBtn" name="uploadBtn">
			<input type="hidden" id="uploadedFileLength"
				name="uploadedFileLength">
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="rtlcrtd" class="grdTbl cstmpg">
				<tbody>
					<tr>
						<td class="header" colspan="4">Edit Landing Page::</td>
					</tr>
					<tr>
						<td class="Label" width="17%">Select Type</td>
						<td colspan="3"><select name="editLndgPgSlct" id="editLndgPgSlct">
								<option value="AttachLink">Attach Link</option>
								<option value="Landing" selected="selected">Landing</option>
						</select></td>
					</tr>
					<tr class="AttachLink">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="atchLinkTitle" class="mand">Title</label>
						</td>
						<td colspan="3"><form:input
								path="retCreatedPageAttachLinkTitle" type="text"
								id="atchLinkTitle" /> <form:errors
								path="retCreatedPageAttachLinkTitle" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>



					<tr class="AttachLink" style="display: table-row;">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="label2" class="mand">URL</label></td>
						<td colspan="3"><form:input path="retCreatedPageattachLink"
								type="text" id="label" name="spclOffrAtF" /> <form:errors
								path="retCreatedPageattachLink" cssStyle="color:red">
							</form:errors></td>
					</tr>

					<tr class="AttachLink">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="spclOffrML" class="mand">Locations</label>
						</td>
						<td colspan="3"><label> <form:select
									class="txtAreaSmall" path="retCreatedPageAttachLinkLocId"
									id="retCreatedPageAttachLinkLocId" multiple="multiple" size="1">
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>
								</form:select> </label> <form:errors path="retCreatedPageAttachLinkLocId"
								cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr>
						<td width="17%" class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlcrtdTitle" class="mand">Title</label>
						</td>
						<td colspan="3"><form:input path="retCreatedPageTitle"
								type="text" id="retCreatedPageTitle" /> <form:errors
								path="retCreatedPageTitle" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="hdRp">Photo</label>
						</td>

						<td colspan="3"><label for="hdSp"></label> <label><img
								id="createRetCustomPage" width="80" height="80" alt="upload"
								src="${customLandingPageImgPath}"
								onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
						</label><span class="topPadding"><label for="trgrUpldCrtdPage"><input
									type="button" title="Choose File" value="Choose File" id="trgrUpldCrtdPageBtn"
									class="btn trgrUpldCrtdPage"><span
									class="instTxt nonBlk">[ Please Choose .png format
										images ]</span> <form:input type="file" id="trgrUpldCrtdPage"
										path="retCreatedPageImage" /> </label> </span>
						</td>

					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrcrtdPgDesc" class="mand">Page Description</label>
						</td>
						<td colspan="3"><form:input path="retCreatedPageDescription"
								maxlength="100" type="text" id="retCreatedPageDescription" /> <form:errors
								path="retCreatedPageDescription" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrcrtdshrtDesc" class="mand">Short Description</label>
						</td>
						<td colspan="3"><form:textarea
								path="retCreatedPageShortDescription" class="txtAreaSmall"
								maxlength="255" rows="5" cols="45"
								id="retCreatedPageShortDescription" /> <form:errors
								path="retCreatedPageShortDescription" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrcrtdlngDesc">Long Description</label>
						</td>
						<td colspan="3"><form:textarea
								path="retCreatedPageLongDescription" class="txtAreaSmall"
								maxlength="1000" rows="5" cols="45" id="rtlrcrtdlngDesc"
								name="rtlrcrtdlngDesc" /></td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="rtlrcrtdLoc" class="mand">Locations</label>
						</td>
						<td colspan="3"><label> <form:select
									class="txtAreaSmall" path="retCreatedPageLocId"
									id="rtlrcrtdLoc" multiple="multiple" size="1">
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>
								</form:select> </label> <form:errors path="retCreatedPageLocId" cssStyle="color:red"></form:errors>
						</td>
					</tr>
					<tr class="grdTbl">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);">Start
							Date</td>
						<td align="left"
							style="border-right: 1px solid rgb(218, 218, 218);"><form:input
								path="retCreatedPageStartDate" type="text" id="StrtDT"
								class="textboxDate" /> <form:errors
								path="retCreatedPageStartDate" cssStyle="color:red">
							</form:errors></td>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);">End Date</td>
						<td><form:input path="retCreatedPageEndDate" type="text"
								id="EndDT" class="textboxDate" name="EndDT2" /> <span
						class="instTxt nonBlk">[End date is not required]</span><form:errors
								path="retCreatedPageEndDate" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr>
						<td class="Label"><label for="cst">Start Time</label></td>
						<td><form:select path="landingPageStartTimeHrs"
								class="slctSmall">
								<form:options items="${DealStartHours}" />
							</form:select> Hrs <form:select path="landingPageStartTimeMin"
								class="slctSmall">
								<form:options items="${DealStartMinutes}" />
							</form:select> Mins</td>
						<td class="Label"><label for="cet">End Time</label>
						</td>
						<td><form:select path="landingPageEndTimeHrs"
								class="slctSmall">
								<form:options items="${DealStartHours}" />
							</form:select> Hrs <form:select path="landingPageEndTimeMin" class="slctSmall">
								<form:options items="${DealStartMinutes}" />
							</form:select> Mins</td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="hdRp">Upload File</label>
						</td>
						<td><form:input path="retCreatedPageFile" type="file"
								class="multi" accept="png|PNG|jpg|JPG|MP3|mp3|MP4|mp4|pdf|PDF"
								maxlength="3" id="retCreatedPageFile" name="retCreatedPageFile" />
						</td>
						<td colspan="2"><c:forEach items="${UploadedFile}"
								var="myarr">


								<p style="font-style: italic; font-weight: 100;" id="${myarr}">
									<img alt="" id="deletefile" onclick="deleteImage('${myarr}')"
										src="../images/delete.png"> ${myarr}

								</p>

							</c:forEach>
						</td>
					</tr>
					<tr class="cmnActn">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
						<td colspan="3"><input type="button" value="Save" class="btn" title="Save"
							onclick="updateRetailerCreatedPage();"> <input
							type="button" value="Preview" class="btn" title="Preview"
							onclick="javascript:showPagePreview('LandingPage')">
						</td>
					</tr>

				</tbody>
			</table>
		</form:form>
	</div>
</div>
<div class="clear"></div>
<script type="text/javascript">
	var pageType = '${sessionScope.landingPageType}';

	document.createCustomPage.uploadedFileLength.value = '${sessionScope.UploadedFileLength}';

	if (pageType == 'AttachLink') {
		selectAttachLinkRtlLocations();
	} else {
		selectRtlLocations();
	}
</script>
<script type="text/javascript">
	$('#trgrUpldCrtdPage')
			.bind(
					'change',
					function() {
						$("#uploadBtn").val("trgrUpldCrtdPageBtn")
						var imgpath = document
								.getElementById('trgrUpldCrtdPage').value;
						if (!validateUploadImageFileExt(imgpath)) {
							alert("Please Choose PNG format image")
						} else {
							$("#createCustomPage")
									.ajaxForm(
											{

												success : function(response) {

													//alert(response.getElementsByTagName('imageScr')[0].firstChild.nodeValue)
													$('#createRetCustomPage')
															.attr(
																	"src",
																	response
																			.getElementsByTagName('imageScr')[0].firstChild.nodeValue);
												}

											//target : '#preview'
											}).submit();
						}

					});

	function showPagePreview(pageTyp) {

		var createForm = document.createCustomPage;
		var imagePath = $("#customPageImg").attr('src')
		var retStoreName = '${sessionScope.retailStoreName}';
		var retStoreImage = '${sessionScope.retailStoreImage}';
	var landingImagepath=$("#createRetCustomPage").attr('src');
		// Do validation before you call for Preview
		if (pageTyp == 'LandingPage') {
			//For landing page validtion

			var title = document.createCustomPage.retCreatedPageTitle.value;
			var locid = document.createCustomPage.rtlrcrtdLoc.value;
			var retShortDesc = document.createCustomPage.retCreatedPageDescription.value;
			var retPageDesc = document.createCustomPage.retCreatedPageShortDescription.value;

			if (title == "") {
				alert('Please enter title');
				return;
			} else if (locid = "" || locid == '0') {
				alert('Please select location');
				return;
			} else if (retPageDesc == "") {
				alert('Please enter  page description');
				return;
			} else if (retShortDesc == "") {
				alert('Please enter  short description');
				return;
			} else {
				showLoadingPagePview(createForm, landingImagepath, retStoreName,
						retStoreImage);
			}
		}
	}
</script>
<script type="text/javascript">
	$('#editLndgPgSlct')
							.change(
									function() {
										var pageType = $('#selLandingPageType')
												.val();

										$this = $(this);
										var curVal = $this.find(
												'option:selected').val();
										var curTxt = $this.find(
												'option:selected').text();
										var curTbl = $(this).parents('table')
												.attr('id');
											
										if (curVal == "AttachLink") {
											var title = $(
													'#retCreatedPageTitle')
													.val();
											if (title != "") {
												$('#atchLinkTitle').val(title);

											}

											if (pageType != 'AttachLink') {

												var vLocID = "";
												$(
														"#rtlrcrtdLoc option:selected")
														.each(
																function() {
																	vLocID += $(
																			this)
																			.val()
																			+ ",";
																});
												$("#retCreatedPageAttachLinkLocId option:selected").removeAttr("selected");
												var vLocVal = $('#retCreatedPageAttachLinkLocId')[0];

												if (vLocID != "null") {
													var vCategoryList = vLocID
															.split(",");
												}
												for ( var i = 0; i < vLocVal.length; i++) {
													for (j = 0; j < vCategoryList.length; j++) {
														if (vLocVal.options[i].value == vCategoryList[j]) {
															vLocVal.options[i].selected = true;
															break;
														}
													}
												}

											}
											$('#' + curTbl + ' tr:gt(4)')
													.hide();
											$('.AttachLink,.cmnActn').show()
											$('#' + curTbl + ' td.header')
													.text(curTxt + " Page:");
											$this.parents('table').find(
													'input[value="Preview"]')
													.hide();
										} else {

											var title = $('#atchLinkTitle')
													.val();

											if (title != "") {
												$('#retCreatedPageTitle').val(
														title);

											}
											if (pageType != 'Landing') {
												var vLocID = "";

												$(
														"#retCreatedPageAttachLinkLocId option:selected")
														.each(
																function() {
																	vLocID += $(
																			this)
																			.val()
																			+ ",";
																});
												$("#rtlrcrtdLoc option:selected").removeAttr("selected");
												var vLocVal = $('#rtlrcrtdLoc')[0];

												if (vLocID != "null") {
													var vCategoryList = vLocID
															.split(",");
												}
												for ( var i = 0; i < vLocVal.length; i++) {
													for (j = 0; j < vCategoryList.length; j++) {
														if (vLocVal.options[i].value == vCategoryList[j]) {
															vLocVal.options[i].selected = true;
															break;
														}
													}
												}

											}

											$('#' + curTbl + ' tr.AttachLink')
													.hide();
											$('#' + curTbl + ' tr:gt(4)')
													.show();
											$('#' + curTbl + ' td.header')
													.text(curTxt + " Page:");
											$this.parents('table').find(
													'input[value="Preview"]')
													.show();

										}
									});

		</script>		

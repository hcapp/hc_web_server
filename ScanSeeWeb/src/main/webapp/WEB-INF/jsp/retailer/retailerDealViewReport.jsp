<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>

<script type="text/javascript">
function getPerPgaVal(){
	var selValue=$('#selPerPage :selected').val();
	document.retailerDealsViewForm.recordCount.value=selValue;
    getHDealsViewReport();
}

function getHDealsViewReport() {
	showProgressBar();
	document.retailerDealsViewForm.action = "hotdealreportview.htm";
	document.retailerDealsViewForm.method = "POST";
	document.retailerDealsViewForm.submit();
}

function onLoadHotDealName() {
	var vHotDealID = document.retailerDealsViewForm.hotDealID.value;
	var sel = document.getElementById("hotDealName");
	for ( var i = 0; i < sel.options.length; i++) {
		if (sel.options[i].value == vHotDealID) {
			sel.options[i].selected = true;
			return;
		}
	}
}

function printReport() {
	var vHDealId = $('#hotDealID').val();
	if (vHDealId == 0 || vHDealId === 0) {
		alert("Please select HotDeal Name");
	} else {
		var vOpenTab = window.open('/ScanSeeWeb/retailer/hotdealviewreportprint.htm', '_blank');
		vOpenTab.focus();
	}
}
</script>
<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="retailerLeftNavigation.jsp"%>
		<form:form name="retailerDealsViewForm" commandName="retailerDealsViewForm"
			acceptCharset="ISO-8859-1">
			<form:hidden path="hotDealID" id="hotDealID"/>
			<form:hidden path="recordCount"/>
			<form:hidden path="viewReport" value="viewReport"/>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">Deals</h1>
				</div>
				<div class="grdSec brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<tr>
							<td colspan="3" class="header">Search Deal</td>
							<td align="right" class="header "><a href="#"  onclick="printReport();" ><img src="/ScanSeeWeb/images/print.gif" width="16" height="16" alt="print" class="imgAlgn" title="Print"/> Print</a></td>
						</tr>
						<tr>
							<td class="Label" width="18%">Deal Name</td>
									<td width="59%" colspan="3"><form:select path="hotDealName" id="hotDealName" class="selecBx" onchange="getHDealsViewReport();">
										<form:option value="0" label="--Select--">--Select--</form:option>
										<c:forEach items="${sessionScope.hotDealsList}" var="h">
										<form:option value="${h.hotDealID}" label="${h.hotDealName}" />
								</c:forEach>
							</form:select></td>
						</tr>
						<tr class="sub-hdr">
							<td align="left">Deal Name</td>
							<td width="32%"># Of deals Issued</td>
							<td width="25%"># Of Users Claimed</td>
							<td width="24%"># Of Users Redeemed</td>
						</tr>
						<tr>
							<td align="left"><c:out value="${sessionScope.dealInfo.hotDealName}"/></td>
							<td><c:out value="${sessionScope.dealInfo.numOfHotDeals}" /></td>
							<td><c:out value="${sessionScope.dealInfo.claimCount}"  /></td>
							<td><c:out value="${sessionScope.dealInfo.redeemCount}"  /></td>
						</tr>
					</table>
					<c:if test="${sessionScope.dealInfo.contact ne null && ! empty sessionScope.dealInfo.contact }">
					<div class="searchGrd">
						<h1 class="searchHeaderExpand">
							<a href="#" class="floatR">&nbsp;</a>User Details
						</h1>
						<div class="grdCont tableScroll zeroPadding">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="stripeMe" id="dealLst">
								<tr class="header">
									<td width="19%">First Name</td>
									<td width="33%">Email Id</td>
									<td width="24%">Claimed</td>
									<td width="24%">Redeemed</td>
								</tr>
								<c:forEach items="${sessionScope.dealInfo.contact}"
									var="userDetails">
									<tr width="25%"  >
										<td><c:out value='${userDetails.contactFirstName}' />
										</td>
										<td ><c:out value='${userDetails.contactEmail}'/>
										</td>
										<td ><c:out value='${userDetails.claimed}'/>
										</td>
										<td ><c:out value='${userDetails.redeemed}'/>
										</td>
									</tr>
								</c:forEach>
							</table>
						</div>
						<div class="pagination brdrTop">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="noBrdr" id="perpage">
								<tr>
									<page:pageTag
										currentPage="${sessionScope.pagination.currentPage}"
										nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
										pageRange="${sessionScope.pagination.pageRange}"
										url="${sessionScope.pagination.url}" enablePerPage="true" />
								</tr>
							</table>
						</div>
					</div>
					</c:if>
					<div class="navTabSec RtMrgn LtMrgn" align="right">
							<input class="btn"
							onclick="location='/ScanSeeWeb/retailer/hotDealRetailer.htm'"
							value="Back" type="Button" name="Back" title="Back"/>
					</div>
				</div>
			</div>
		</form:form>
	</div>
</div>
<div class="clear"></div>
<script>
	onLoadHotDealName();
</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<script src="/ScanSeeWeb/ckeditor/config.js"></script>
<script>
	$(document)
			.ready(
					function() {

						CKEDITOR
								.on(
										'instanceCreated',
										function(e) {
											//	alert("q : "+e.editor.name);
											var editorName = e.editor.name;
											document.getElementById('lngDesc').innerHTML = 'Long Description goes here';
											document.getElementById('shrtDesc').innerHTML = 'Short Description goes here';

											e.editor
													.on(
															'change',
															function(ev) {
																if (editorName == 'rtlrshrtDsc') {
																	document
																			.getElementById('lngDesc').innerHTML = ev.editor
																			.getData();
																} else if (editorName == 'retPageShortDescription') {
																	document
																			.getElementById('shrtDesc').innerHTML = ev.editor
																			.getData();
																}

															});
										});

						/* 	CKEDITOR.config.toolbar =
								[
								    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
								    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
								    { name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
								    '/',
								    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
								    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
								    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
								    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
								    '/',
								    { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
								    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
								    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] }
								]; */

						/* CKEDITOR.config.toolbar = [
						                    		{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] } ,
						                    		{ name: 'styles', items: [ 'Styles', 'Format' ] },
						                    	];
						
						CKEDITOR.config.toolbarGroups = [
						                    	{ name: 'basicstyles', groups: [ 'basicstyles', 'styles' ] }
						                    	
						                    ]; */
						CKEDITOR.config.uiColor = '#FFFFFF';
						CKEDITOR.replace('rtlrshrtDsc',
								{
									extraPlugins : 'onchange',
									width : "226",
									toolbar : [
											/* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
											{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
											{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
											'/',
											{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
											{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
											{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
											{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
											'/',
											{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
											{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
											{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
											{
												name : 'basicstyles',
												items : [ 'Bold', 'Italic',
														'Underline' ]
											},
											{
												name : 'paragraph',
												items : [ 'JustifyLeft',
														'JustifyCenter',
														'JustifyRight',
														'JustifyBlock' ]
											},
											'/',
											{
												name : 'styles',
												items : [ 'Styles', 'Format' ]
											},
											'/',
											{
												name : 'tools',
												items : [ 'Font', 'FontSize',
														'RemoveFormat' ]
											}, '/', {
												name : 'colors',
												items : [ 'BGColor' ]
											}, {
												name : 'paragraph',
												items : [ 'Outdent', 'Indent' ]
											}, {
												name : 'links',
												items : [ 'Link', 'Unlink' ]
											} ],
									removePlugins : 'resize'
								});
						CKEDITOR.replace('retPageShortDescription',
								{
									extraPlugins : 'onchange',
									width : "226",
									toolbar : [
											{
												name : 'basicstyles',
												items : [ 'Bold', 'Italic',
														'Underline' ]
											},
											{
												name : 'paragraph',
												items : [ 'JustifyLeft',
														'JustifyCenter',
														'JustifyRight',
														'JustifyBlock' ]
											},
											'/',
											{
												name : 'styles',
												items : [ 'Styles', 'Format' ]
											},
											'/',
											{
												name : 'tools',
												items : [ 'Font', 'FontSize',
														'RemoveFormat' ]
											}, '/', {
												name : 'colors',
												items : [ 'BGColor' ]
											}, {
												name : 'paragraph',
												items : [ 'Outdent', 'Indent' ]
											}, {
												name : 'links',
												items : [ 'Link', 'Unlink' ]
											} ],
									removePlugins : 'resize'
								});

						$('#splOfferLocId option')
								.click(
										function() {
											var totOpt = $('#splOfferLocId option').length;
											var totOptSlctd = $('#splOfferLocId option:selected').length;
											if (totOpt == totOptSlctd) {
												$('input[name$="chkAllLoc"]')
														.attr('checked', 'true');
											} else {
												$('input[name$="chkAllLoc"]')
														.removeAttr('checked');
											}
										});
						$("#splOfferLocId")
								.change(
										function() {
											var totOpt = $('#splOfferLocId option').length;
											var totOptSlctd = $('#splOfferLocId option:selected').length;
											if (totOpt == totOptSlctd) {
												$('input[name$="chkAllLoc"]')
														.attr('checked', 'true');
											} else {
												$('input[name$="chkAllLoc"]')
														.removeAttr('checked');
											}
										});
						$("#StrtDT").datepicker({
							showOn : 'both',
							buttonImageOnly : true,
							buttonText : 'Click to show the calendar',
							buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
						});
						$('.ui-datepicker-trigger').css("padding-left", "5px");
						$("#EndDT").datepicker({
							showOn : 'both',
							buttonImageOnly : true,
							buttonText : 'Click to show the calendar',
							buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
						});
						$('.ui-datepicker-trigger').css("padding-left", "5px");

						// Populate Title on load
						if ($("#retPageTitle").val() != '') {
							$("h2").text($("#retPageTitle").val());
						}

						// Populate Short Description in load
						if ($("#retPageShortDescription").val() != '') {
							var stt = $("#retPageShortDescription").val();
							//var reNewLines = /[\n\r]/g;
							//$(".shrtDesc").html(stt.replace(reNewLines, "<br />"));
							$(".shrtDesc").html(stt);
						}

						// Populate long Description in load
						if ($("#rtlrshrtDsc").val() != '') {
							var ltt = $("#rtlrshrtDsc").val();
							//var reNewLines = /[\n\r]/g;
							//$(".lngDesc").html(ltt.replace(reNewLines, "<br />"));
							$(".lngDesc").html(ltt);
						}

						//populateDurationPeriod_SpecialOfferPage(document.getElementById("durationCheck").checked);

					});

	function selectRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("splOfferLocId");
		var vCategoryList = [];
		if (vLocID != "null" && vLocID != "") {
			vCategoryList = vLocID.split(",");
		}
		if (vLocVal.length != 0 && vCategoryList.length != 0) {
			if (vLocVal.length == vCategoryList.length) {
				document.getElementById('chkAllLoc').checked = true;
			}
		}

		for ( var i = 0; i < vLocVal.length; i++) {
			for ( var j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function SelectAllLocation(checked) {
		var sel = document.getElementById("splOfferLocId");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}

	function checkImageSize(input) {

		var bannerImage = document.getElementById("trgrUpld").value;
		/*alert("bannerImage" + bannerImage);
		if (input.files && input.files[0].size > (100 * 1024)) {
			alert("File too large. Max 100 KB allowed.");
			input.value = null;
		} else */
		if (bannerImage != '') {
			var checkbannerimg = bannerImage.toLowerCase();
			if (!checkbannerimg.match(/(\.png)$/)) {
				alert("You must upload Splash Page image with following extensions : .png ");
				document.createCustomPage.retImage.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}
</script>
<div id="wrapper">
	<form:form name="createCustomPage" id="createCustomPage"
		commandName="createCustomPage" action="uploadtempretimg.htm"
		enctype="multipart/form-data" acceptCharset="ISO-8859-1">
		<form:hidden path="hiddenLocId" />
		<form:hidden path="retailerImg" id="retailerImg" />
		<form:hidden path="pageId" id="pageId" />
		<form:hidden path="imageUplType" value="usrUpld" />
		<form:hidden path="splOfferType" value="Special Offer" />
		<form:hidden path="viewName" value="splOfferMain" />
		<input type="hidden" id="uploadBtn" name="uploadBtn">
		<div id="content" class="shdwBg">
			<%@ include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">Special Offer Page</h1>
						 <div class="sub-actn sub-link"><a href="#" onclick="location='/ScanSeeWeb/retailer/specialoffinstructions.htm'">View Instructions</a> </div>					
				</div>
				
					<div class="section">
					<div class="grdSec brdrTop">					
				
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="lblSpan cstmpg" id="rtlr">
							<tr>
								<td colspan="5" class="header">Special Offer Page:
					
								
								</td>
								</tr>
							<tr>
								<td class="Label"><label for="atchLinkTitle" class="mand">Title</label></td>
								<td class="brdRt" colspan="4"><form:input
										path="splOfferTitle" type="text" id="retPageTitle"
										name="rtlrTitle" class="textbox" tabindex="1" maxlength="1000"
										style=" width: 656px;" /> <form:errors path="splOfferTitle"
										cssStyle="color:red">
									</form:errors></td>
							</tr>
							<tr>
								<td><label for="hdRp" class="mand">Photo</label></td>
								<td colspan="2"><ul class="imgInfoSplit">
										<li><label for="hdSp"></label> <label><img
												id="customPageImg" width="70" height="70" alt="upload"
												src="${sessionScope.customPageRetImgPath}"
												onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
										</label><span class="topPadding forceBlock"><label
												for="trgrUpld"><input type="button" value="Choose File"
													id="trgrUpldBtn" class="btn trgrUpld" title="Choose File"
													tabindex="2"> <form:input type="file" id="trgrUpld"
														class="textboxBig" path="retImage" /> <form:errors
														path="retImage" cssStyle="color:red" /> </label><label
												id="customPageImgErr" style="color: red; font-style: 45"></label>
										</span></li>
										<li>Suggested Minimum Size:<br>300px/150px<br>Maximum
											Size:800px/600px<br> <!-- <b> Maximum Size:</b><br>950px/1024px --></li>
									</ul></td>
								<td><label for="rtlrLocn" class="mand">Location(s)</label></td>
								<td>
									<div class="btmMrgn">
										<label> <input type="checkbox" name="chkAllLoc"
											id="chkAllLoc" onclick="SelectAllLocation(this.checked);" />
											Select All
										</label> Locations
									</div>
									<label> <form:select path="splOfferLocId"
											id="splOfferLocId" name="select" class="txtAreaBox"
											multiple="true" tabindex="3">


											<c:forEach items="${sessionScope.retailerLocList}" var="s">
												<form:option value="${s.retailerLocationID}"
													label="${s.address1}" />
											</c:forEach>

										</form:select> <br /> <form:label path="splOfferLocId">Hold Ctrl to select more than one location</form:label><br />
										<form:errors path="splOfferLocId" cssStyle="color:red">
										</form:errors>
								</label>
								</td>
							</tr>
							<tr>
								<td><label for="rtlrlngDsc" class="mand">Short
										Description</label></td>
								<td colspan="2" class="brdRt"><form:textarea
										path="splOfferShortDescription" id="retPageShortDescription"
										name="splOfferShortDescription" tabindex="4" /> <form:errors
										path="splOfferShortDescription" cssStyle="color:red">
									</form:errors></td>
								<td colspan="2" rowspan="2" valign="top"><div
										id="iphonePanel" style="margin-top: 10px;">
										<div class="navBar iphone">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0" class="titleGrd">
												<tr>
													<td width="15%"><img src="../images/backBtn.png"
														alt="back" width="50" height="30" /></td>
													<td width="65%" class="genTitle">Special Offer Page</td>
													<td width="20%"><img src="../images/mainMenuBtn.png"
														alt="mainmenu" width="70" height="30" /></td>
												</tr>
											</table>
										</div>
										<div class="viewArea">
											<table width="100%" border="0" cellspacing="0"
												cellpadding="0">
												<tr>
													<td colspan="4"><h2 class="rtlrTitle">Title</h2></td>
												</tr>
												<tr>
													<td colspan="4" align="center"><div class="image">
															<img src="${sessionScope.customPageRetImgPath}"
																id="customImg" alt="images" width="70" height="70"
																onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';" />
														</div></td>
												</tr>

												<tr>
													<td colspan="4"><div class="shrtDesc" id="shrtDesc">Short
															Description goes here</div></td>
												</tr>
												<tr>
													<td colspan="4"><div class="lngDesc" id="lngDesc">Long
															Description goes here</div></td>
												</tr>
											</table>
										</div>
									</div></td>
							</tr>
							<tr>
								<td><label for="rtlrshrtDsc" class="mand">Long
										Description</label></td>
								<td colspan="2" class="brdRt"><form:textarea
										id="rtlrshrtDsc" path="splOfferLongDescription"
										name="rtlrshrtDsc" tabindex="5" /> <form:errors
										path="splOfferLongDescription" cssStyle="color:red">
									</form:errors></td>

							</tr>

							<tr>
								<td class="Label"><label for="strtDT" class="mand">Start
										Date</label></td>
								<td colspan="2" class="brdRt"><form:input
										path="splOfferStartDate" type="text" id="StrtDT"
										class="textboxDate" tabindex="6" /> <form:errors
										path="splOfferStartDate" cssStyle="color:red">
									</form:errors></td>
								<td class="Label"><label for="endDT">End Date</label></td>
								<td class="brdRt" colspan="2"><form:input
										path="splOfferEndDate" type="text" id="EndDT"
										class="textboxDate" name="EndDT2" tabindex="7" /> <form:errors
										path="splOfferEndDate" cssStyle="color:red">
									</form:errors> <br>
								<span class="instTxt nonBlk"></span></td>
							</tr>
							<tr>
								<td class="Label"><label for="cst">Start Time</label></td>
								<td colspan="2" class="brdRt"><form:select
										path="splofferStartTimeHrs" class="slctSmall" tabindex="8">
										<form:options items="${DealStartHours}" />
									</form:select> Hrs <form:select path="splofferStartTimeMin" class="slctSmall"
										tabindex="9">
										<form:options items="${DealStartMinutes}" />
									</form:select> Mins</td>
								<td class="Label"><label for="cet">End Time</label></td>
								<td colspan="2"><form:select path="splOfferEndTimeHrs"
										class="slctSmall" tabindex="10">
										<form:options items="${DealStartHours}" />
									</form:select> Hrs <form:select path="splOfferEndTimeMin" class="slctSmall"
										tabindex="11">
										<form:options items="${DealStartMinutes}" />
									</form:select> Mins</td>
							</tr>
							<tr>
							<td class="Label" ><label for="keywords"></label>Keywords</td>
								<td  colspan="4"> <form:textarea id="keywords"
										class="txtAreaSmall" rows="5" cols="45" path="keyword"
										tabindex="12" cssStyle="height:60px;" /></td>
							
							</tr>
							<tr>
								<td class="Label">&nbsp;</td>
								
								<td colspan="4" class="brdRt" align="right">
								
								<input class="btn"
								onclick="location='/ScanSeeWeb/retailer/retailersplofferpage.htm'" value="Back" type="button"
									name="Back" tabindex="13" />
								<input type="button"
									class="btn" value="Submit" onclick="saveSplOfferPage();"
									tabindex="14" /> </td>
							</tr>
						
						</table>
						<div class="ifrmPopupPannelImage" id="ifrmPopup"
							style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</form:form>
</div>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {
						$("#uploadBtn").val("trgrUpldBtn");

						$("#createCustomPage")
								.ajaxForm(
										{
											success : function(response) {

												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue

												if (imgRes == 'UploadLogoMaxSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {

													//$('#customPageImg').attr("src",imgRes);
													//	$('#customImg').attr("src", imgRes);
													$('#customPageImgErr')
															.text("");
													var substr = imgRes
															.split('|');

													if (substr[0] == 'ValidImageDimention') {
														var imgName = substr[1];
														$('#retailerImg').val(
																imgName);
														$('#customPageImg')
																.attr(
																		"src",
																		substr[2]);
														$('#customImg').attr(
																"src",
																substr[2]);

													} else {
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}

											}
										}).submit();

					});
</script>
<script type="text/javascript">
	selectRtlLocations();
</script>

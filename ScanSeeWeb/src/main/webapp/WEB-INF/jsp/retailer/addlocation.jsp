<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.ManageProducts"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<spring:message code='googleApiKey'/>&sensor=true"></script>
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/cityAutocomplete.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/zipcodeAutocomplete.js"></script>

<script>
	$(document).ready(function() {
		$(".alertBx").hide();
		$("#City").live("keydown", function(e) {
			cityAutocomplete('pstlCd');
		});

		$("#retailUrl").focusin(function() {
			var urlVal = $(this).val();
			if (urlVal === "") {
				$(this).val("http://");
			}
		});
		$("#retailUrl").focusout(function() {
			var urlVal = $(this).val();
			if (urlVal === "" || urlVal === "http://") {
				$(this).val("");
			}
		});
	});

	var changeImgDim = '${sessionScope.ChangeImageDim}';
	if (null != changeImgDim && changeImgDim == 'true') {
		$('#locationImg').width('70px');
		$('#locationImg').height('70px');
	}
	
	function showModal() {
		$(".alertBx").hide();
        var bodyHt = $('body').height();
        var setHt = parseInt(bodyHt);
        $(".modal-popupWrp").show();
        $(".modal-popupWrp").height(setHt);
        $(".modal-popup").slideDown('fast');
	}

	/*close modal popup on click of x*/
	function closeModal() {
       
		$(".alertBx").hide();
       var validateOpenClose = null;
		var timezone = $("#storeTimeZoneId").val();
		
        $('select[name^="storeStartHrs"]').each(function() {
    			storeStartHrs = $(this).val();
    			if(storeStartHrs != "00" && timezone == ""){
    				validateOpenClose = false;
    			}	
    	});
        
        $('select[name^="storeStartMins"]').each(function() {
        	storeStartMins = $(this).val();
			if(storeStartMins != "00" && timezone == ""){
				validateOpenClose = false;
			}	
		});
        
        $('select[name^="storeEndHrs"]').each(function() {
        	storeEndHrs = $(this).val();
			if(storeEndHrs != "00" && timezone == ""){
				validateOpenClose = false;
			}	
		});
        $('select[name^="storeEndMins"]').each(function() {
        	storeEndMins = $(this).val();
			if(storeEndMins != "00" && timezone == ""){
				validateOpenClose = false;
			}	
		});
        if(validateOpenClose == false){
        	$(".failure").show();
        } else {
        	$(".alertBx").hide();
        	 $(".modal-popupWrp").hide();
             $(".modal-popup").slideUp();
             $(".modal-popup").find(".modal-bdy input").val("");
             $(".modal-popup i").removeClass("errDsply");
             $(".overlay").hide();
        }
        
	}
	
</script>
<script type="text/javascript">
	window.onload = function() {

		if ('${requestScope.message}' == 'true') {
			document.addlocationform.storeID.value = "";
			document.addlocationform.storeAddress.value = "";
			document.addlocationform.postalCode.value = "";
			document.addlocationform.state.value = "";
			document.addlocationform.city.value = "";
			document.addlocationform.postalCode.value = "";
			document.addlocationform.retailUrl.value = "";
			document.getElementById('phoneNumber').value = "";
			document.addlocationform.keyword.value = "";
			document.addlocationform.retailerLocationLatitude.value = "";
			document.addlocationform.retailerLocationLongitude.value = "";
			document.addlocationform.storeStartHrs.value = "00";
			document.addlocationform.storeStartMins.value = "00";
			document.addlocationform.storeEndHrs.value = "00";
			document.addlocationform.storeEndMins.value = "00";
			document.addlocationform.storeTimeZoneId.value = "";

			$('#locationImgPath').val("");
			document.addlocationform.locationImgPath.value = "";
			$('#locationImg').attr("src",
					"/ScanSeeWeb/images/upload_imageRtlr.png");
			document.addlocationform.imageFile.value = "";

			if (document.getElementById('storeID.errors') != null) {
				document.getElementById('storeID.errors').style.display = 'none';
			}
			if (document.getElementById('storeAddress.errors') != null) {
				document.getElementById('storeAddress.errors').style.display = 'none';
			}
			if (document.getElementById('state.errors') != null) {
				document.getElementById('state.errors').style.display = 'none';
			}
			if (document.getElementById('city.errors') != null) {
				document.getElementById('city.errors').style.display = 'none';
			}
			if (document.getElementById('postalCode.errors') != null) {
				document.getElementById('postalCode.errors').style.display = 'none';
			}
			if (document.getElementById('retailUrl.errors') != null) {
				document.getElementById('retailUrl.errors').style.display = 'none';
			}
			if (document.getElementById('storeStartHrs.errors') != null) {
				document.getElementById('storeStartHrs.errors').style.display = 'none';
			}
			if (document.getElementById('storeStartMins.errors') != null) {
				document.getElementById('storeStartMins.errors').style.display = 'none';
			}
			if (document.getElementById('storeEndHrs.errors') != null) {
				document.getElementById('storeEndHrs.errors').style.display = 'none';
			}
			if (document.getElementById('storeEndMins.errors') != null) {
				document.getElementById('storeEndMins.errors').style.display = 'none';
			}
			if (document.getElementById('storeTimeZoneId.errors') != null) {
				document.getElementById('storeTimeZoneId.errors').style.display = 'none';
			}
			if (document.getElementById('addlocationform.errors') != null) {
				document.getElementById('addlocationform.errors').style.display = 'none';
			}

		}
	}

	window.onload = function() {
		var vGeoErr = $('#geoError').val();

		if (vGeoErr != "null" && vGeoErr != "") {
			document.getElementById("LatLong").style.display = "";
		} else {
			document.getElementById("LatLong").style.display = "none";
		}
	}

	/*function loadCity() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		var selCity = document.addlocationform.cityHidden.value;

		$
				.ajax({
					type : "GET",
					url : "/ScanSeeWeb/retailer/retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : selCity
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.addlocationform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						if (cityList.length > 1) {
							document.addlocationform.City.options.length = cityList.length + 1;
							for ( var i = 0; i < cityList.length; i++) {
								cityDropDown.options[optIndex].value = cityList[i];
								cityDropDown.options[optIndex].text = cityList[i];
								optIndex++;
							}
						}

						//$('#myAjax').html(response);
						onCitySelectedLoad();
					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}
	function onChangeState() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		document.addlocationform.cityHidden.value="";
		$
				.ajax({
					type : "GET",
					url : "/ScanSeeWeb/retailer/retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : ""
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.addlocationform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						cityDropDown.options[0].selected = true;

						for ( var i = 0; i < cityList.length; i++) {
							cityDropDown.options[optIndex].value = cityList[i];
							cityDropDown.options[optIndex].text = cityList[i];
							optIndex++;

						}

					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}




	function getCityTrigger(val) {
		document.addlocationform.cityHidden.value = val.options[val.selectedIndex].value;
	}
	function onCitySelectedLoad() {
		var vCityID = document.addlocationform.cityHidden.value;
		var sel = document.getElementById("City");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCityID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}*/
	function clearLocationForm() {
		var r = confirm("Do you really want to clear the form")
		var cityHtml = "<select name='city' id='City' tabindex='5'><option value='0'>--Select--</option></select>";
		if (r == true) {
			document.addlocationform.storeID.value = "";
			document.addlocationform.storeAddress.value = "";
			document.addlocationform.postalCode.value = "";
			document.addlocationform.state.value = "";
			document.addlocationform.city.value = "";
			document.addlocationform.postalCode.value = "";
			document.getElementById('phoneNumber').value = "";
			document.addlocationform.retailUrl.value = "";
			document.addlocationform.keyword.value = "";
			document.addlocationform.retailerLocationLatitude.value = "";
			document.addlocationform.retailerLocationLongitude.value = "";
			document.addlocationform.storeStartHrs.value = "00";
			document.addlocationform.storeStartMins.value = "00";
			document.addlocationform.storeEndHrs.value = "00";
			document.addlocationform.storeEndMins.value = "00";
			document.addlocationform.storeTimeZoneId.value = "";

			$('#locationImgPath').val("");
			document.addlocationform.locationImgPath.value = "";
			$('#locationImg').attr("src",
					"/ScanSeeWeb/images/upload_imageRtlr.png");
			document.addlocationform.imageFile.value = "";

			/*hide latitude and longitude field */
			document.addlocationform.geoError.value = "";
			document.getElementById("LatLong").style.display = "none";
			if (document.getElementById('storeID.errors') != null) {
				document.getElementById('storeID.errors').style.display = 'none';
			}
			if (document.getElementById('storeAddress.errors') != null) {
				document.getElementById('storeAddress.errors').style.display = 'none';
			}
			if (document.getElementById('state.errors') != null) {
				document.getElementById('state.errors').style.display = 'none';
			}
			if (document.getElementById('city.errors') != null) {
				document.getElementById('city.errors').style.display = 'none';
			}
			if (document.getElementById('postalCode.errors') != null) {
				document.getElementById('postalCode.errors').style.display = 'none';
			}
			if (document.getElementById('phoneNumber.errors') != null) {
				document.getElementById('phoneNumber.errors').style.display = 'none';
			}
			if (document.getElementById('retailUrl.errors') != null) {
				document.getElementById('retailUrl.errors').style.display = 'none';
			}
			if (document.getElementById('addlocationform.errors') != null) {
				document.getElementById('addlocationform.errors').style.display = 'none';
			}
			if (document.getElementById('retailerLocationLongitude.errors') != null) {
				document.getElementById('retailerLocationLongitude.errors').style.display = 'none';
			}
			if (document.getElementById('retailerLocationLatitude.errors') != null) {
				document.getElementById('retailerLocationLatitude.errors').style.display = 'none';
			}
			if (document.getElementById('storeStartHrs.errors') != null) {
				document.getElementById('storeStartHrs.errors').style.display = 'none';
			}
			if (document.getElementById('storeStartMins.errors') != null) {
				document.getElementById('storeStartMins.errors').style.display = 'none';
			}
			if (document.getElementById('storeEndHrs.errors') != null) {
				document.getElementById('storeEndHrs.errors').style.display = 'none';
			}
			if (document.getElementById('storeEndMins.errors') != null) {
				document.getElementById('storeEndMins.errors').style.display = 'none';
			}
			if (document.getElementById('storeTimeZoneId.errors') != null) {
				document.getElementById('storeTimeZoneId.errors').style.display = 'none';
			}
			if (document.getElementById('addlocationform.errors') != null) {
				document.getElementById('addlocationform.errors').style.display = 'none';
			}
			
			 $('select[name^="storeStartHrs"]').each(function() {
				 $("select[name='storeStartHrs']").val("00");	
	    	});
			 $('select[name^="storeStartMins"]').each(function() {
				 $("select[name='storeStartMins']").val("00");	
	    	});
			 $('select[name^="storeEndHrs"]').each(function() {
				 $("select[name='storeEndHrs']").val("00");	
	    	});
			 $('select[name^="storeEndMins"]').each(function() {
				 $("select[name='storeEndMins']").val("00");	
	    	});
		}
	}

	function isLatLong(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
				|| charCode == 45 || charCode == 43)
			return true;
		return false;
	}
</script> 
<script>
$(function() {
	$('#storeTimeZoneId').change(function() {
	   $('#timeZoneHidden').val($("#storeTimeZoneId option:selected").text());
	    });
	});
</script>
<form:form name="addlocationform" commandName="addlocationform" action="/ScanSeeWeb/retailer/addlocationimg.htm" acceptCharset="ISO-8859-1"
	enctype="multipart/form-data">
	<form:hidden path="viewName" value="addlocation" />
	<form:hidden path="locationImgPath" id="locationImgPath" />
	<form:hidden path="cityHidden" />
	<form:hidden path="stateHidden" id="stateHidden" />
	<form:hidden path="stateCodeHidden" id="stateCodeHidden" />
	<form:hidden path="timeZoneHidden" id="timeZoneHidden" />
	<form:hidden path="geoError" id="geoError" value="${requestScope.GEOERROR}" />
	<div id="wrapper">

		<div id="dockPanel">
			<ul id="prgMtr" class="tabs">
				<li><a title="Choose Ads/Logos" href="uploadRetailerLogo.htm" rel="Choose Ads/Logos">Choose Logo</a></li>
				<li><a class="tabActive" title="Location Setup" href="#" rel="Location Setup">Location Setup</a></li>
				<li><a title="Choose Plan" href="/ScanSeeWeb/retailer/retailerchoosePlan.htm" rel="Choose Plan">Choose Plan</a></li>
				<li><c:choose>
						<c:when test="${isPaymentDone == null}">
							<a title="Dashboard" href="/ScanSeeWeb/retailer/retailerhome.htm" rel="Dashboard">Dashboard</a>
						</c:when>
						<c:otherwise>
							<a title="Dashboard" href="#" rel="Dashboard">Dashboard</a>
						</c:otherwise>
					</c:choose></li>
			</ul>
			<!--<a id="Mid" name="Mid"></a>
			<div class="floatR tglSec" id="tabdPanelDesc">
				<div id="filledGlass">
					<img src="../images/Step5.png" />
					<div id="nextNav">
						<a href="#"
							onclick="location.href='/ScanSeeWeb/retailer/regaddseachprod.htm'">
							<img class="NextNav_R" alt="Next" src="../images/nextBtn.png"
							border="0" /> <span>Next</span> </a>
					</div>
				</div>
			</div>-->
			<!--<div class="floatL tglSec" id="tabdPanel">
				<img height="283" alt="Flow1" src="../images/Flow5_ret.png"
					width="782" />
			</div>-->
		</div>

		<div class="clear"></div>
		<div id="content" align="center">
			<!--<form:errors cssStyle="${requestScope.locationSetUpFileFont}" path="locationSetUpFile" />-->
			<label style="${requestScope.locationSetUpFileFont}"><c:out value="${requestScope.locationuploadMsg}" /></label>
			<div id="subnav">
				<ul>
					<li><a href="#" class="active"><span>Add Location(s)</span> </a></li>
					<li><a href="/ScanSeeWeb/retailer/locationsetup.htm"><span>Upload Location</span> </a></li>
					<li><a href="/ScanSeeWeb/retailer/fetchbatchlocationlist.htm"><span>Update Locations</span> </a></li>
				</ul>
			</div>
			<div class="grdSec">
				<table class="grdTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
					<tbody>

						<tr>
							<td class="Label" align="left"><label for="stID" class="mand">Store Identification</label></td>
							<td width="30%" align="left"><form:errors cssStyle="color:red" path="storeID"></form:errors> <form:input path="storeID" type="text"
									id="storeID" name="storeID" maxlength="100" tabindex="1" /></td>
							<td class="Label" align="left"><label for="stAdr" class="mand">Store Address</label></td>
							<td width="30%" align="left"><form:errors cssStyle="color:red" path="storeAddress"></form:errors> <form:textarea rows="5"
									cssStyle="height:60px;" cols="45" path="storeAddress" id="storeAddress" name="storeAddress" onkeyup="checkMaxLength(this,'50');" tabindex="2" /></td>
						</tr>



						<tr id="LatLong" style="display: none">
							<td class="Label" align="left"><label for="Latitude" class="mand">Latitude </label></td>
							<td align="left"><form:errors cssStyle="color:red" path="retailerLocationLatitude"></form:errors> <form:input
									id="retailerLocationLatitude" tabindex="3" name="retailerLocationLatitude" path="retailerLocationLatitude" /></td>
							<td class="Label" align="left"><label for="Longitude" class="mand">Longitude</label></td>
							<td align="left"><form:errors cssStyle="color:red" path="retailerLocationLongitude"></form:errors> <form:input
									id="retailerLocationLongitude" tabindex="4" path="retailerLocationLongitude" name="retailerLocationLongitude" /> <br /></td>
						</tr>
						<tr>
							<td class="Label" align="left"><label for="pCode" class="mand"> Postal Code </label></td>
							<td align="left"><form:errors cssStyle="color:red" path="postalCode" cssClass="error"></form:errors> <form:input path="postalCode"
									type="text" class="loadingInput dsblContxMenu" maxlength="5" tabindex="5" name="couponName7" id="pstlCd"
									onkeypress="zipCodeAutocomplete('pstlCd');return isNumberKey(event)" onchange="isNumeric(this.value);" onkeyup="isEmpty(this.value);" /></td>
							<td class="Label" align="left"><label for="cty" class="mand">City</label></td>
							<td align="left"><form:errors cssStyle="color:red" path="city" cssClass="error"></form:errors> <form:input path="city" id="City"
									tabindex="6" class="loadingInput dsblContxMenu" /></td>
						</tr>
						<tr>
							<td class="Label" align="left"><label for="sts" class="mand">State</label></td>
							<td align="left"><form:errors cssStyle="color:red" path="state" cssClass="error"></form:errors> <form:select path="state" id="Country"
									tabindex="7">
									<form:option value="">--Select--</form:option>
									<c:forEach items="${sessionScope.states}" var="s">
										<form:option value="${s.stateabbr}" label="${s.stateName}" />
									</c:forEach>
								</form:select></td>

							<td class="Label" align="left"><label for="phnNum" class="mand">Phone #</label></td>
							<td align="left"><form:errors cssStyle="color:red" path="phoneNumber"></form:errors> <form:input id="phoneNumber" tabindex="8"
									path="phoneNumber" name="phoneNumber" onkeyup="javascript:backspacerUP(this,event);" onkeydown="javascript:backspacerDOWN(this,event);" /> <br />
								<label class="Label">(xxx)xxx-xxxx</label></td>
						</tr>
						<tr>
							<td class="Label" align="left"><label for="url">Website URL</label></td>
							<td align="left"><form:errors cssStyle="color:red" path="retailUrl"></form:errors> <form:input id="retailUrl" tabindex="9"
									path="retailUrl" name="retailUrl" /> <br /></td>

							<td class="Label" align="left"><label for="keywords"></label>Keywords</td>
							<td align="left"><form:textarea id="keywords" class="txtAreaSmall" rows="5" cols="45" path="keyword" tabindex="10" cssStyle="height:60px;" /></td>
						</tr>

						<tr>
							<td class="Label" align="left"><label for="upldImg">Location Icon</label></td>
							<td colspan="3">
								<ul class="imgInfoSplit" id="hotdealUpld">
									<li><label><img id="locationImg" alt="upload" src="${sessionScope.locationImgPath}" height="80" width="80"
											onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';" tabindex="11"> </label><span class="topPadding forceBlock"><label
											for="trgrUpld"> <input type="button" value="Choose File" id="trgrUpldBtn" width="350" height="460" class="btn trgrUpld"
												title="Choose File" tabindex="10"> <form:input path="imageFile" type="file" class="textboxBig" id="trgrUpld"
													onchange="checkLocationImgValidate(this);" tabindex="12" />
										</label></span></li>
									<li>Suggested Minimum Size:<br>70px/70px<br>Maximum Size:800px/600px<br>
									<li><br> <form:errors path="imageFile" cssStyle="color:red">
										</form:errors> <label id="locationImgPathErr" style="color: red; font-style: 45"></label></li>
								</ul>
							</td>

						</tr>
						<tr>
							<td class="Label" align="left"><label for="openOrClose">Business Hours</label>
							</td>
							<td colspan="3"><input type="button" class="btn" value="Add Business Hours" onclick="showModal();"/>
									
						</tr>
						
						<tr>
							<td colspan="4" align="Center"><em> <input type="hidden" id="locCoordinates" value="" /> <input class="btn" value="Clear"
									type="button" onclick="javascript:clearLocationForm();" name="Cancel2" tabindex="13" /> <input class="btn"
									onclick="javascript:addLocation();" value="Submit" type="button" name="Cancel" tabindex="14" /> <!-- <input class="btn" onclick="getAddLocationCoordinates();" value="Save" type="button" name="Cancel" tabindex="13" />-->
									<input class="btn" onclick="location.href='/ScanSeeWeb/retailer/retailerchoosePlan.htm'" value="Continue" type="button" name="Cancel"
									tabindex="15" />
							</em></td>
						</tr>
					</tbody>
				</table>
				<div class="required" align="left">
					<label class="mand" for="sts"></label> Required field.
				</div>
				<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
					<div class="headerIframe">
						<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
							title="Click here to Close" align="middle" /> <span id="popupHeader"></span>
					</div>
					<iframe frameborder="0" scrolling="no" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color: White">
					</iframe>
				</div>
				
				

			</div>
			<div class="clear"></div>
		</div>
	</div>

	<!--Modal popup div starts-->
	<div class="modal-popupWrp">
		<div class="modal-popup">
			<div class="modal-hdr">
				Setup Business Hours<a class="modal-close" title="close"
					onclick="closeModal()"><img src="../images/popupClose.png" /></a>
			</div>
			<div class="modal-bdy">
			<div class="alertBx failure">
					<p class="msgBx">
						<c:out value="Please select Time Zone" />
					</p>
				</div>
				<div class="modal-cont brdr">
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="grdTbl">
						<tbody>
							<tr>
								<td class="Label"><label for="timeZone">Select
										TimeZone</label></td>
								<td align="left" colspan="3" class="timeZoneRow"><form:errors
										cssStyle="color:red" path="storeTimeZoneId" cssClass="error"></form:errors>
									<form:select path="storeTimeZoneId" class="selecBx">
										<form:option value="" label="--Select--">Please Select Time Zone</form:option>
										<c:forEach items="${sessionScope.retailerTimeZoneslst}"
											var="tz">
											<form:option value="${tz.timeZoneId}"
												label="${tz.timeZoneName}" />
										</c:forEach>
									</form:select></td>
							</tr>

							<tr>
								<th class="Label hrsLabel" align="center"><label>Day</label></th>
								<th class="Label hrsLabel" align="center"><label>Open
										Time</label></th>
								<th class="Label hrsLabel" align="center"><label>Close
										Time</label></th>
							</tr>
							<c:forEach items="${sessionScope.businessHoursList}" var="item">
								<tr>
									<td class="Label" align="left"><label for="cst"><c:out
												value="${item.weekDayName}" /></label>
									<td>
										<div class="mslct">
											<form:select path="storeStartHrs" class="slctSmall">

												<c:forEach items="${StoreStartHours}" var="hours">
													<c:choose>
														<c:when test="${hours.value eq item.storeStartHrs}">
															<form:option value="${hours.value}" selected="selected" />
														</c:when>
														<c:otherwise>
															<form:option value="${hours.value}" />
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</form:select>
											Hrs
											<form:select path="storeStartMins" class="slctSmall">
												<c:forEach items="${StoreStartMinutes}" var="minutes">
													<c:choose>
														<c:when test="${minutes.value eq item.storeStartMins}">
															<form:option value="${minutes.value}" selected="selected" />
														</c:when>
														<c:otherwise>
															<form:option value="${minutes.value}" />
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</form:select>
											Mins
										</div>
									</td>
									<td>
										<div class="mslct">
											<form:select path="storeEndHrs" class="slctSmall">

												<c:forEach items="${StoreStartHours}" var="hours">
													<c:choose>
														<c:when test="${hours.value eq item.storeEndHrs}">
															<form:option value="${hours.value}" selected="selected" />
														</c:when>
														<c:otherwise>
															<form:option value="${hours.value}" />
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</form:select>
											Hrs
											<form:select path="storeEndMins" class="slctSmall">
												<c:forEach items="${StoreStartMinutes}" var="minutes">
													<c:choose>
														<c:when test="${minutes.value eq item.storeEndMins}">
															<form:option value="${minutes.value}" selected="selected" />
														</c:when>
														<c:otherwise>
															<form:option value="${minutes.value}" />
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</form:select>
											Mins
										</div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-ftr">
				<p align="right">

					<!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
					<input name="Save" value="Save" type="button" class="btn" id="save"
						onclick="closeModal()" />
				</p>
			</div>
		</div>
	</div>
</form:form>

<script>
	$("#retailerLocationLatitude").focusout(function() {
		/* Matches	 90.0,-90.9,1.0,-23.343342
		 Non-Matches	 90, 91.0, -945.0,-90.3422309*/
		var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
		var vLat = $('#retailerLocationLatitude').val();
		;
		//Validate for Latitude.
		if (0 === vLat.length || !vLat || vLat == "") {
			return false;
		} else {
			if (!vLatLngVal.test(vLat)) {
				alert("Latitude are not correctly typed");
				$("#retailerLocationLatitude").val("").focus();
				return false;
			}
		}
	});

	$("#retailerLocationLongitude")
			.focusout(
					function() {
						/* Matches	180.0, -180.0, 98.092391
						 Non-Matches	181, 180, -98.0923913*/
						var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
						//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
						var vLong = $('#retailerLocationLongitude').val();
						;
						//Validate for Longitude.
						if (0 === vLong.length || !vLong || vLong == "") {
							return false;
						} else {
							if (!vLatLngVal.test(vLong)) {
								alert("Longitude are not correctly typed");
								$("#retailerLocationLongitude").val("").focus();
								return false;
							}
						}
					});

	function checkLocationImgValidate(input) {
		var vLocationImg = document.getElementById("trgrUpld").value;
		if (vLocationImg != '') {
			var checkLocationImg = vLocationImg.toLowerCase();
			if (!checkLocationImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Location image with following extensions :  .png, .gif, .bmp, .jpg, .jpeg");
				document.addlocationform.imageFile.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}

	$('#trgrUpld')
			.bind(
					'change',
					function() {
						/*show progress bar : ETA for Web 1.3*/
						//showProgressBar();/* Commented to fix body scroll disable issue*/
						/*End*/
						$("#uploadBtn").val("trgrUpldBtn")
						$.ajaxSetup({
							cache : false
						});
						$("#addlocationform")
								.ajaxForm(
										{
											success : function(response) {
												$('#loading-image').css(
														"visibility", "hidden");
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue
												if (imgRes == 'UploadLogoMaxSize') {
													$('#locationImgPathErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#locationImgPathErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {
													$('#locationImgPathErr')
															.text("");
													var substr = imgRes
															.split('|');
													if (substr[0] == 'ValidImageDimention') {
														$('#locationImg')
																.width('70px');
														$('#locationImg')
																.height('70px');
														var imgName = substr[1];
														$('#locationImgPath')
																.val(imgName);
														$('#locationImg').attr(
																"src",
																substr[2]);
													} else {
														/*commented to fix iframe popup scroll issue
														/$('body').css("overflow-y","hidden");*/
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}
											}
										}).submit();
					});
</script>
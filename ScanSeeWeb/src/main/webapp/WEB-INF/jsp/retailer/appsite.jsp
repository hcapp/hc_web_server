<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="content">
    <%@include file="retailerLeftNavigation.jsp"%>
	
	<form name="appsiteform" commandName="editretailerprofileform" acceptCharset="ISO-8859-1"> 
	  <div class="rtContPnl floatR">
      <div class="mainPgTitle">
        <h1 class="mainTitle">Welcome to the ${sessionScope.businessName} AppSite<sup>&trade;</sup></h1>
        <h5 class="highlight-text-lighter  MrgnBtm">Please choose what you want to do from the menu on the left</h5>
        <p class="MrgnBtm">To get started building your AppSite, choose "My AppSite" and follow the steps below:</p>
      </div>
      <div class="grpTitles">
        <h1 class="mainTitle">My AppSite<sup>&trade;</sup> Setup and Change</h1>
      </div>
      <div class="section">
        <div class="textSec brdrTop">
          <ul class="prgrsInfo">
          
          <c:forEach items="${sessionScope.appsitehome}" var='apphome'>
          
          <c:choose>
          <c:when test="${apphome.isLogo eq true}">
            <li class="stsCmpltd" title="Setup Logo"><a alt="Setup Logo" class="prgrs-logo" href="/ScanSeeWeb/retailer/uploadRetailerLogo/uploadRetailerLogoDashboard.htm"></a><span>1. Logo</span></li>
          </c:when>
          <c:otherwise>
            <li class="" title="Setup Logo"><a alt="Setup Logo" class="prgrs-logo" href="/ScanSeeWeb/retailer/uploadRetailerLogo/uploadRetailerLogoDashboard.htm"></a><span>1. Logo</span></li>
          
          </c:otherwise>
          
          </c:choose>
          
          
            <c:choose>
          <c:when test="${apphome.isSplash eq true}">
             <li class="stsCmpltd" title="Setup Welcome Screen - Pending"><a alt="Setup Welcome Screen" class="prgrs-splash" href="manageads.htm"></a><span>2. Splash Page</span></li>
          </c:when>
          <c:otherwise>
          
             <li class="" title="Setup Welcome Screen - Pending"><a alt="Setup Welcome Screen" class="prgrs-splash" href="manageads.htm"></a><span>2. Splash Page</span></li>
          </c:otherwise>
          
          </c:choose>
       
            <c:choose>
          <c:when test="${apphome.isBanner eq true}">
            <li class="stsCmpltd" title="Setup Banner - Pending"><a alt="Setup Banner" class="prgrs-banner" href="managebannerads.htm"></a><span>3. Banner</span></li>
          </c:when>
          <c:otherwise>
          
            <li class="" title="Setup Banner - Pending"><a alt="Setup Banner" class="prgrs-banner" href="managebannerads.htm"></a><span>3. Banner</span></li>
          </c:otherwise>
          
          </c:choose>
          
               <c:choose>
          <c:when test="${apphome.isAnyThingPage eq true}">
           <li  class="stsCmpltd" title="Setup Anything Pages - Pending"><a alt="Setup Anything Pages" class="prgrs-anything-page" href="buildAnythingPage.htm"></a><span>4. Anything Pages<sup>&trade;</sup></span></li>
          </c:when>
          <c:otherwise>
          
           <li title="Setup Anything Pages - Pending"><a alt="Setup Anything Pages" class="prgrs-anything-page" href="buildAnythingPage.htm"></a><span>4. Anything Pages<sup>&trade;</sup></span></li>
          </c:otherwise>
          
          </c:choose>
           
          
          
          </c:forEach>
          
           </ul>
          <p class="clear"></p>
          <p><span class="highlight-text-dark">After</span> you have finished the four steps above,</p>
          <p class="MrgnTop text-italicise">Click on:</p>
          <p class="MrgnTop"><span class="highlight-text-dark">Manage Locations -</span> This section must be completed. (Choose and manage your business Location(s). </p>
          <p class="MrgnTop text-italicise">Then click on:</p>
          <p class="MrgnTop"><span class="highlight-text-dark">Promotions -</span>This section is optional. (To create Special Offers, Deals and Coupons for your AppSite<sup>&trade;.)


		  </sup> </p>
		  <p>Note: By completing this form, you are agreeing to HubCiti's
			<a class="txt-dec" href="http://www.hubcitiapp.com/terms-of-use" target="_blank">terms and conditions.</a></p>
        </div>
      </div>
   
    </div>
	   </form>
	
	
	
	
	<!-- commented as per the changes asked by customer and will these comments once customer approves the design -->
	<!--
    <div class="rtContPnl floatR">
      <div class="grpTitles">
        <h1 class="mainTitle">My AppSite<sup class="smallsup">TM&nbsp;</sup></h1>
      </div>
      <div class="section">
        <div class="textSec brdrTop">
          <p>Your AppSite<sup class="smallsup">TM&nbsp;</sup> is a custom app and website rolled into one!  Just like any app and website there is a "menu" with buttons that connect to "Pages" in your AppSite <sup class="smallsup">TM</sup>.  
          In this section, you will create (or change) each of your "Pages" and "Menu".  It is easy and fun to watch your creation come to life as you build it!. </p>
          <h4 class="MrgnTop">Step by Step</h4>
          <ul class="MrgnTop">
          	<li>A. Choose your Logo</li>
            <li>B. Build Your Splash Page </li>
            <li>C. Build Your Banner</li>
            <li>D. Build your Anything Pages <sup class="smallsup">TM</sup></li>
          </ul>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
  <tr>
    <td width="25%" >
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" style="height:128px; width:126px;">
    		<tr>
    			<td><b>A.</b></td>
    			<td align="center"><a href="/ScanSeeWeb/retailer/uploadRetailerLogo/uploadRetailerLogoDashboard.htm">
    			<img src="../images/yourLogo.png" alt="YourLogo" width="110" height="110" /></a></td>
    		</tr>    		
    	</table>
    </td>
     <td width="25%">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" style="height:128px; width:126px;">
    		<tr>
    			<td><b>B.</b></td>
    			<td><a href="/ScanSeeWeb/retailer/manageads.htm"><img src="../images/buildWelcomePage.png" alt="BuildWelcomePage" width="110" height="110" /></a></td>
    		</tr>    		
    	</table>
    </td>
     <td width="25%">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" style="height:128px; width:126px;">
    		<tr>
    			<td><b>C.</b></td>
    			<td><a href="/ScanSeeWeb/retailer/managebannerads.htm"><img src="../images/buildBannerAd.png" alt="BuildBannerAd" width="110" height="110" /></a></td>
    		</tr>    		
    	</table>
    </td>
     <td width="25%">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl" style="height:128px; width:126px;">
    		<tr>
    			<td><b>D.</b></td>
    			<td><a href="/ScanSeeWeb/retailer/buildAnythingPage.htm"><img src="../images/buildAnythingPgs.png" alt="BuildYourAnythingPage" width="110" height="110" /></a></td>
    		</tr>    		
    	</table>
    </td>    
  </tr>
  <tr>
    <td align="center"><a href="/ScanSeeWeb/retailer/uploadRetailerLogo/uploadRetailerLogoDashboard.htm"><img src="../images/iphone_uploadlogo.png" alt="uploadLogo_iPhone" width="124" height="242" /></a></td>
    <td align="center"><a href="/ScanSeeWeb/retailer/manageads.htm"><img src="../images/iphone_buildWelcomePg.png" alt="buildWelcomePage" width="124" height="242" /></a></td>
    <td align="center"><a href="/ScanSeeWeb/retailer/managebannerads.htm"><img src="../images/iphone_buildBannerAd.png" alt="BuildBannerAd" width="124" height="242" /></a></td>
    <td align="center"><a href="/ScanSeeWeb/retailer/buildAnythingPage.htm"><img src="../images/iphone_buildAnythingPg.png" alt="BuilAnythingPage" width="124" height="242" /></a></td>
  </tr>
  <tr>
    <td align="left" valign="top">Your logo appears as the icon next to your listing.</td>
    <td align="left" valign="top">Your Splash Page displays for 3-5 seconds when someone clicks on your listing.  It is a way to reinforce your key message and branding.</td>
    <td align="left" valign="top">Your Banner can be used to prominently display your branding.</td>
    <td><p>Anything Pages<sup class="smallsup">TM&nbsp;</sup> make up your menu to detail your product, services and what you want people to know about your businesses.  You can create PDF documents, HTML pages or links to websites or documents.</p>
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

        </div>
      </div>
    </div>
	-->
    <div class="clear"></div>
  </div>
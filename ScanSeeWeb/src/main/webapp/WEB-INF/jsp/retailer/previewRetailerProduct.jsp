<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>HubCiti</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
</head>
<body>

<script type="text/javascript">
$(function() {
    //Get actual content and display as title on mouse hover on it.
    var actText = $("td.genTitle").text();
    $("td.genTitle").attr('title',actText);
    var limit = 20;// character limit restricted to 20
    var chars = $("td.genTitle").text(); 
    if (chars.length > limit) {
        var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
        var dots = $("<span class='dots'>...</span>");
        $("td.genTitle").empty()
            .append(visibleArea)
            .append(dots);//append trailing dots to the visibile part 
    }
                            
});
function closeWinDow() 
{
	window.close();
}
</script>

<div id="iphonePanel">
 <form:form name="myform" commandName="retprodsetupform"> 
  	<div class="viewAreaProduct">
	<div class="navBar">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
            <tr>
              <td width="19%"><img src="/ScanSeeWeb/images/backBtn.png" alt="back" width="50" height="30" /></td>
              <td width="54%" class="genTitle">${requestScope.previewlist.productName}</td>
              <td width="27%"><img src="/ScanSeeWeb/images/mainMenuBtn.png" alt="mainmenu" width="78" height="30" /></td>
            </tr>
          </table>
        </div>      
  	
    <ul id="prodInformation" class="MrgnTop">
    	<li id="ImgArea"><img src="${requestScope.previewlist.productImagePath}" alt="imageSize" width="125" height="130"/></li>
        <li id="ProdName">${requestScope.previewlist.productName}</li>
      	<li class="clear"></li>
    </ul>
 
    <div id="descriptionArea">
      <textarea id="addrs1" class="txtAreaIphone" rows="5" cols="45" name="textarea2" readonly>${requestScope.previewlist.longDescription}</textarea>
    </div>
    <div id="attributeInfo">
	 <table cellspacing="0" cellpadding="0" border="0" width="100%" class="prodSummary">
            <tbody>
              <tr>
                <td width="42%"><h4>Product UPC : </h4></td>
                <td width="58%">${requestScope.previewlist.scanCode}</td>
              </tr>
              <tr>
                <td><h4>Product Name : </h4></td>
                <td>${requestScope.previewlist.productName}</td>
              </tr>
              <tr>
                <td><h4>Model Number : </h4></td>
                <td>${requestScope.previewlist.modelNumber}</td>
              </tr>
              <tr>
                <td><h4>Retail Price : </h4></td>
                <c:choose>
      					<c:when test="${requestScope.previewlist.price ne null && ! empty requestScope.previewlist.price}">
							<td>$${requestScope.previewlist.price}</td>
						</c:when>
						<c:otherwise>
							<td>&nbsp;</td>
						</c:otherwise>
				</c:choose>
					
              </tr>
              <tr>
                <td><h4>Category : </h4></td>
                <td>${requestScope.previewlist.categoryName}</td>
              </tr>             
          </tbody>
		 </table>
    </div>

    </div>
    <div id="mediaPanel">
    	<ul>
          <li><img src="/ScanSeeWeb/images/Audio.png" alt="audio" /></li>
          <li><img src="/ScanSeeWeb/images/video.png" alt="video" /></li>
          <li><img src="/ScanSeeWeb/images/OtherInfo.png" alt="Other Info" /></li>
        </ul>
    </div>
</form:form>
</div>
  <div class="clear"></div>
		<div align="center">
			<input class="btn" onclick="closeWinDow()" type="button" value="Close" name="Cancel2" title="Close"/>
		</div>
  <div> </div>
 <div></div>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=<spring:message code='googleApiKey'/>&sensor=true"></script>
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/autocomplete.js"></script>
<script>
$(document).ready(function() {
	$("#City").live("keydown",function(e)
			{
				var s = String.fromCharCode(e.which);
				if (s.match(/[a-zA-Z0-9\.]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#postalCode').val("");
				   cityAutocomplete('postalCode');
				}else if(s.match(/[\b]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#postalCode').val("");
					cityAutocomplete('postalCode');
				}
							
			});
var stateCode = '';
var state = '';
if($('#postalCode').val().length == 5){
	stateCode = $('#stateCodeHidden').val();
	state = $('#stateHidden').val();
	$( "#Country" ).val(state);				 
	}else{
	$('#stateCodeHidden').val("");
	$('#stateHidden').val("");
	$( "#Country" ).val("");
	}
	
	$('#webUrl').focusin(function (){
		
		var webUrlvalue = $(this).val();
		if(webUrlvalue === "")
			{
			$(this).val("http://");
			
			}	
		
	});
	$("#webUrl").focusout(function (){
		
		var webUrlvalue = $(this).val();
		if(webUrlvalue === "" || webUrlvalue == "http://")
			{
			$(this).val("");
			}
		
		
	});

	

});
</script>
<script type="text/javascript">
	/*function loadCity() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		var selCity = document.editretailerprofileform.cityHidden.value;

		$
				.ajax({
					type : "GET",
					url : "displayselectedcity.htm",
					data : {
						'statecode' : stateCode
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.editretailerprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						if (cityList.length > 1) {
							document.editretailerprofileform.City.options.length = cityList.length + 1;
							for ( var i = 0; i < cityList.length; i++) {
								cityDropDown.options[optIndex].value = cityList[i];
								cityDropDown.options[optIndex].text = cityList[i];
								optIndex++;
							}
						}

						//$('#myAjax').html(response);
						onCitySelectedLoad();
					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}

	function onChangeState() {
		// get the form values
		var stateCode = $('#Country').val();
		var tabIndex = $('#tabindex').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		document.editretailerprofileform.cityHidden.value="";
		$
				.ajax({
					type : "GET",
					url : "retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : "",
						'tabIndex' : tabIndex
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.editretailerprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						cityDropDown.options[0].selected = true;

						for ( var i = 0; i < cityList.length; i++) {
							cityDropDown.options[optIndex].value = cityList[i];
							cityDropDown.options[optIndex].text = cityList[i];
							optIndex++;

						}

					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}
*/
	function onLoad() {

		var vCategoryID = document.editretailerprofileform.bCategoryHidden.value;
		var vCategoryVal = document.getElementById("bCategory");
		if (vCategoryID != "null") {
			var vCategoryList = vCategoryID.split(",");
		}

		for ( var i = 0; i < vCategoryVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {

				if (vCategoryVal.options[i].value == vCategoryList[j]) {
					vCategoryVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function getCityTrigger(val) {
		if(val == ""){
			$( "#Country" ).val("");
			$( "#postalCode" ).val( "" );
		}
		document.editretailerprofileform.cityHidden.value = val;
		document.editretailerprofileform.city.value = val;
	}
	
	function clearOnCityChange() {
		if($('#citySelectedFlag').val() != 'selected'){
		$( "#Country" ).val("");
		$( "#postalCode" ).val( "" );
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
		}else{
		$('#citySelectedFlag').val('');
		}
}

	/*function onCitySelectedLoad() {
		var vCityID = document.editretailerprofileform.cityHidden.value;
		var sel = document.getElementById("City");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCityID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}*/

	/*function getEditRetLocationCoordinates() {
		
		//alert("Inside Location Co-ordinates!!!");
		var streetAddress = $('#address1').val();
		var state = $('#Country').val();
		var city = $('#City').val();
		var zipCode = $('#postalCode').val();

		var address = streetAddress + " " + city + " " + state + " " + zipCode;
		// alert("Address "+address);
		var geocoder = new google.maps.Geocoder();
		geocoder
				.geocode(
						{
							'address' : address
						},
						function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								// alert("Location :"+results[0].geometry.location);
								document.getElementById("locCoordinates").value = results[0].geometry.location;
								submitEditRetailPage();
							} else {
								alert("Unable to find address: " + status);
								submitEditRetailPage();
							}
						});
	}*/

	function submitEditRetailPage() {
		/*var locCordinates = "";
		if (document.getElementById("locCoordinates")) {
			locCordinates = document.getElementById("locCoordinates").value;
		}
		//document.editretailerprofileform.action = "editRetailerProfile.htm?locationCoordinates=" + locCordinates;*/
		
		var subCatMad = false;
	
		var mainCat = $("#bCategory option.toggleSubCat:selected").map(function() {
			  return $(this).val();
			}) .get().join();
		var mainCatSpl = mainCat.split(",");
		var subCategory = document.editretailerprofileform.subCategory.value.split(",");//Selected bussiness cat which has sub-cats
		var hiddenSubCats = document.editretailerprofileform.subCategories.value.split("!~~!");
		
		var mainCatSplLng = mainCatSpl.length;
		var subCategoryLng = subCategory.length;
		
		if(mainCatSplLng > subCategoryLng) {
			subCatMad = true;
		} else if(mainCatSplLng == subCategoryLng) {
			for(var i = 0; i < mainCatSplLng; i++) {
				if(subCategory[i] !== mainCatSpl[i]) {
					subCatMad = true;
					break;
				} else {
					if(hiddenSubCats[i] === "NULL") {
						subCatMad = true;
						break;
					}
				}
			}
		}
		
		if(subCatMad === true) {
			alert("Please select 'Sub-Categories' to the selected business categories");		
		} else {
			
			var strSubCat = "";
			var j = 0;
			var busCat = $("#bCategory option:selected").map(function() {
				  return $(this).val();
				}) .get().join();
			
			var busCatSpl = busCat.split(",");
			var busCatSplLng = busCatSpl.length;
			
			for(var i = 0; i < busCatSplLng; i++) {
				
				if(busCatSpl[i] === subCategory[j]) {
					
					if(strSubCat === "") {
						strSubCat = hiddenSubCats[j];
					} else {
						strSubCat += "!~~!" + hiddenSubCats[j];
					}
					
					j++;
				} else {
					
					if(strSubCat === "") {
						strSubCat = "NULL";
					} else {
						strSubCat += "!~~!NULL";
					}
				}
				
			}
			
			document.editretailerprofileform.hiddensubCategies.value = strSubCat;
			document.editretailerprofileform.subCategory.value = mainCat;
		
			/*var cat = $("#bCategory option.toggleFltr:selected").map(function(){
								  return $(this).val();
								}) .get().join();
			var filterCategory = document.editretailerprofileform.filterCategory.value.split(",");
			var hiddenCat = cat.split(",");
			var hiddenCatLength = hiddenCat.length;
			if(hiddenCatLength > filterCategory.length) {
				var hiddenMainFilter = document.editretailerprofileform.filters.value.split("!~~!");
				var hiddenSubFilter = document.editretailerprofileform.filterValues.value.split("!~~!");
				var filter = "";
				var subFilters = "";
				var startLength = 0;
				var insert = false;
				for(var m = 0; m < hiddenCatLength; m++) {
					for(var n = 0; n < filterCategory.length; n++) {
						if(hiddenCat[m] !== filterCategory[n]) {
							insert = true;
						} else {
							insert = false;
							break;
						}
					}
					if(insert == true) {
						var assocaited = false;
						for(var i = 0; i < hiddenCatLength; i++) {	
							if(i > 0) {
								if(i == m) {
									filter += "!~~!" + "NULL";
									assocaited = true;									
								} else {									
									if(assocaited == false) {
										filter += "!~~!" + hiddenMainFilter[i];
										var split = hiddenMainFilter[i].split("|");
										startLength = startLength + split.length;
									} else {
										filter += "!~~!" + hiddenMainFilter[i-1];
									}
								}
							} else {
								if(i == m) {
									filter = "NULL";
									assocaited = true;	
								} else {
									filter = hiddenMainFilter[i];
									var split = hiddenMainFilter[i].split("|");
									startLength = startLength + split.length;
								}
							}
						}
						assocaited = false;
						var hidsubFil = filter.split("!~~!");
						var hidsubFilLeng = hidsubFil.length;
						var leng = 0;
						for(var k = 0; k < hidsubFilLeng; k++) {
							var split = hidsubFil[k].split("|");
							leng = leng + split.length;
						}
						for(var j = 0; j < leng; j++) {
							if(j > 0) {
								if(j == startLength) {
									subFilters += "!~~!" + "NULL";
									assocaited = true;	
								} else {
									if(assocaited == false) {
										subFilters += "!~~!" + hiddenSubFilter[j];
									} else {
										subFilters += "!~~!" + hiddenSubFilter[j-1];
									}
								}
							} else {
								if(j == startLength) {
									subFilters = "NULL";
									assocaited = true;									
								} else {
									subFilters = hiddenSubFilter[j];
								}
							}
						}
					} 
				}
				document.editretailerprofileform.filters.value = filter;
				document.editretailerprofileform.filterValues.value = subFilters;
				document.editretailerprofileform.filterCategory.value = cat;
			}*/
			document.editretailerprofileform.action = "editRetailerProfile.htm";
			document.editretailerprofileform.method = "POST";
			document.editretailerprofileform.submit();
		}
	}
	
	function isNumeric(vNum) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;
		var vPostal = document.getElementById("postalCode");
		for (i = 0; i < vNum.length && IsNumber == true; i++) {
			Char = vNum.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				vPostal.value = "";
				vPostal.focus();
				IsNumber = false;
			}
		}
		return IsNumber;
	}
	function isEmpty(vNum) {
		if(vNum.length < 5){
			//$( "#state" ).val("");
			$( "#Country" ).val("");
			$( "#City" ).val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		return true;
	}

	window.onload = function() {
		var vGeoErr = $('#geoError').val();

		if (vGeoErr != "null" && vGeoErr != "") {
				document.getElementById("LatLong").style.display="";
			} else {
				document.getElementById("LatLong").style.display="none";
			}
	}

	function isLatLong(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31 ||charCode == 45 || charCode == 43)
			return true;
		return false;
		}
</script>



<form:form name="editretailerprofileform"
	commandName="editretailerprofileform" acceptCharset="ISO-8859-1">
	<form:hidden path="cityHidden" />
	<form:hidden path="bCategoryHidden" />
	<form:hidden path="ishiddenlocation" />
	<form:hidden path="stateHidden" id ="stateHidden"/>
	<form:hidden path="stateCodeHidden" id = "stateCodeHidden" />
	<form:hidden path="citySelectedFlag" id = "citySelectedFlag" />
	<form:hidden path="geoError" id="geoError" value="${requestScope.GEOERROR}" />
	<form:hidden path="filters" id ="filters"/>
	<form:hidden path="filterValues" id ="filterValues"/>
	<form:hidden path="filterCategory" id ="filterCategory"/>
	<form:hidden path="hiddenFilterCategory" id ="hiddenFilterCategory"/>			
	<form:hidden path="subCategories" id ="subCategories"/>
	<form:hidden path="subCategory" id ="subCategory"/>
	<form:hidden path="hiddensubCategory" id ="hiddensubCategory"/>
	<form:hidden path="hiddensubCategies" id ="hiddensubCategies"/>
	
	<div id="bubble_tooltip">
		<div class="bubble_top">
			<span></span>
		</div>
		<div class="bubble_middle">
			<span id="bubble_tooltip_content">Content is comming here as
				you probably can see.Content is comming here as you probably can
				see.</span>
		</div>
		<div class="bubble_bottom"></div>
	</div>
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@include file="retailerLeftNavigation.jsp"%>

			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">Update Account</h1>
				</div>
				<div class="grdSec brdrTop">
					<div id="subnav">
						<ul>
							<li><a href=" " class="active" title="Profile Settings"
								rel="Profile Settings"><span>Profile Settings</span>
							</a>
							</li>
							<!--
							<li><a href="/ScanSeeWeb/retailer/planSettingsRet.htm"
								title="Plan Settings" rel="Plan Settings"><span>Plan
										Settings</span>
							</a>
							</li>
							<li><a href="/ScanSeeWeb/retailer/accountPaymentRet.htm"
								title="Payment Settings" rel="Payment Settings"><span>Payment
										Settings</span>
							</a>
							</li> -->
						</ul>
					</div>
					<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
						width="100%">
						<tbody>
							<tr>
								<td class="header" colspan="1">Profile Settings</td>
								<td colspan="3" style="color: red;"><form:errors
									cssStyle="color:red"></form:errors></td>
							</tr>
							<tr>
								<td class="Label"><label for="rtlrName" class="mand"><a
										href="#"
										onmousemove="showToolTip(event,'This is your storefront name or corporate name.');return false"
										onmouseout="hideToolTip()">Business Name<img
											alt="helpIcon" src="../images/helpIcon.png" />
									</a>
								</label>
								</td>
								<td colspan="3"><form:errors cssStyle="color:red"
										path="retailerName" /> <form:input id="retailerName"
										path="retailerName" tabindex="1" /></td>
							</tr>
							<tr>
								<td class="Label" width="17%"><label for="addrs1"
									class="mand">Corporate Address</label></td>
								<td width="33%"><form:errors cssStyle="color:red"
										path="address1" /> <form:textarea id="address1"
										class="txtAreaSmall" rows="5" cols="45" path="address1"
										onkeyup="checkMaxLength(this,'100');" tabindex="2"
										cssStyle="height:60px;" /></td>
								<td class="Label"><label for="addrs3">Address 2</label></td>
								<td><form:textarea id="address2" class="txtAreaSmall"
										rows="5" cols="45" path="address2"
										onkeyup="checkMaxLength(this,'50');" tabindex="3"
										cssStyle="height:60px;" />
									<form:errors cssStyle="color:red" path="address2" /></td>
							</tr>
							
						  <tr id="LatLong" style="display: none">
							<td class="Label" align="left"><label for="Latitude" class="mand">Latitude
									</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="retailerLocationLatitude"></form:errors> <form:input id="retailerLocationLatitude"
									tabindex="3" name="retailerLocationLatitude" path="retailerLocationLatitude"/>
							</td>
							<td class="Label" align="left"><label for="Longitude"
								class="mand">Longitude</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="retailerLocationLongitude"></form:errors> <form:input id="retailerLocationLongitude"
									tabindex="4" path="retailerLocationLongitude" name="retailerLocationLongitude"/> <br />
							</td>
						   </tr>
						
							<tr>
								<td class="Label" align="left">
									<label for="pstlCd"	class="mand">Postal Code</label></td>
								<td align="left">
									<form:errors cssStyle="color:red" path="postalCode" /> 
									<form:input id="postalCode"	path="postalCode" class="loadingInput" maxlength="10" onkeypress="zipCodeAutocomplete('postalCode');return isNumberKey(event)" 
												onchange="isNumeric(this.value);" onkeyup="isEmpty(this.value);" tabindex="4" />
								</td>
								<form:hidden path="tabIndex" value="5" id="tabindex" />
								<td class="Label"><label for="label2" class="mand">City</label></td>
								<td align="left">
									<form:errors cssStyle="color:red" path="city" />
									<form:input path="city" id="City" tabindex="5" class="loadingInput"/>
								</td>
							</tr>
							<tr>
								<td class="Label" align="left">
								<label for="label" class="mand">State</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="state" /> 
									<i class="errDisp">Please select State</i>
									<form:input id="Country" path="state" tabindex="6" disabled="true"/>
								</td>
								<td class="Label" align="left"><label for="phnNum"
									class="mand">Corporate Phone #</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactPhone"></form:errors> <form:input
										path="contactPhone" id="contactPhone" value="" tabindex="7"
										onkeypress="return isNumberKeyPhone(evt)"
										onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);" />(xxx)xxx-xxxx</td>
							</tr>

							<tr>
								<td class="Label">
									<!--<label for="url" class="mand">-->
									<label for="url">Website URL</label>
								</td>
								<td colspan="3" align="left"><form:errors cssStyle="color:red" path="webUrl" /> 
									<form:input id="webUrl" tabindex="8" path="webUrl" name="webUrl" />
								</td>
							</tr>
							<tr>
								<td class="Label" align="left" rowspan="2"><label for="bcategory" class="mand">Business Category</label></td>
								<td align="left" rowspan="2"><form:errors cssStyle="color:red" path="bCategory"></form:errors>
									<form:select path="bCategory" id="bCategory" class="txtAreaBox" size="1" multiple="true" tabindex="9">
										<c:forEach items="${sessionScope.categoryList}" var="c">
											<c:choose> 
											<c:when test="${c.isAssociated eq true && c.isSubCat eq true}">
												<form:option value="${c.businessCategoryID}" label="${c.businessCategoryName}" cssClass="toggleFltr toggleSubCat" />
											</c:when>
											<c:when test="${c.isAssociated eq true}">
												<form:option value="${c.businessCategoryID}" label="${c.businessCategoryName}" cssClass="toggleFltr" />
											</c:when>
											<c:when test="${c.isSubCat eq true}">
												<form:option value="${c.businessCategoryID}" label="${c.businessCategoryName}" cssClass="toggleSubCat" />
											</c:when>
											<c:otherwise>
												<form:option value="${c.businessCategoryID}" label="${c.businessCategoryName}" />
											</c:otherwise>
										</c:choose>									
										</c:forEach>
									</form:select>
								</td>
								<td colspan="2" align="left" valign="middle" class="Label"><label>
					                <input type="checkbox" name="chkAllOptns" id="chkAllCtrgy" />
					                Select All </label> Categories 
					                <p>To select up to 3 categories, hold down the CTRL or CMD button on your keyboard</p>
				               	</td>
				               
							</tr>
							<tr>
				            	<td colspan="2" align="left" valign="middle" class="">
				            		<!-- 08/10/2016 new Requirement Removed Select Filters from create profile screen-->	
				            		<input type="button" name="selectSubCats" id="selectSubCats" title="Select Sub-Categoies" value="Select Sub-Categoies" class="btn"/>
				            	</td>
				            </tr>
							<tr>
								<td class="Label">Is Billing and Business Address the Same? If you have a different billing location from your business location, enter your business location to the Add Location tab</td>
								<td><form:checkbox path="islocation" id="isLoc"
										name="islocation" tabindex="11" disabled="true" /></td>
								<td class="Label" align="left"><label for="keywords">Keywords
										<!-- Non-profit Status-->
								</label></td>
								<td align="left"><form:textarea id="keywords"
										class="txtAreaSmall" rows="5" cols="45" path="keyword"
										tabindex="10" cssStyle="height:60px;" />
										<p>(Please use a comma to separate each keyword or string of keywords.)</p>
										 <!--<form:errors cssStyle="color:red" path="nonProfit"></form:errors>
					<form:checkbox  path="nonProfit" id="nonProfit" tabindex="8"/>My organization is a registered and recognized<br />
					                501(c)(3) non-profit. </td>-->
							</tr>
							<tr>
								<td class="Label" colspan="4">&nbsp;</td>
							</tr>
							<!--  <tr>
              <td class="Label" colspan="4"><label for="lglAuhrty"> <a href="#" onmousemove="showToolTip(event,'The name of the person that has given you authority to act.');return false" onmouseout="hideToolTip()"> Legal Authority Name <img alt="helpIcon" src="/ScanSeeWeb/images/helpIcon.png"></a></label>              </td>
            </tr>
            
            <tr>
              <td class="Label"><label for="lFName" class="mand">First Name</label>              </td>
              <td align="left"> <form:errors cssStyle="color:red" path="legalAuthorityFName"/> <form:input id="legalAuthorityFName" path="legalAuthorityFName" tabindex="10"/>  
                         </td>
              <td class="Label" align="left"><label for="llName" class="mand">Last Name</label>              </td>
              <td align="left"><form:errors cssStyle="color:red" path="legalAuthorityLName"/>  <form:input id="legalAuthorityLName" path="legalAuthorityLName" tabindex="11"/>
                               </td>
            </tr>-->
							<tr>
								<td class="Label" colspan="4"><a href="#">Contact
										Details</a>
								</td>
							</tr>
							<tr>
								<td class="Label"><label for="contFName" class="mand">Contact
										First Name</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactFName" />
									<form:input id="contactFName" path="contactFName" tabindex="12" />
								</td>
								<td class="Label"><label for="contLName" class="mand">Contact
										Last Name</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactLName" />
									<form:input id="contactLName" path="contactLName" tabindex="13" />
								</td>

							</tr>
							<tr>
								<td class="Label"><label for="contEml" class="mand">Contact
										Email</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactEmail" />
									<form:input id="contactEmail" path="contactEmail" tabindex="14" />
								</td>
								<td class="Label" align="left"><label for="contactPhoneNo"
									class="mand">Contact Phone #</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactPhoneNo"></form:errors> <form:input
										path="contactPhoneNo" id="contactPhoneNo" tabindex="15"
										onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);" />(xxx)xxx-xxxx</td>
							</tr>
						</tbody>
					</table>
					<div class="navTabSec mrgnRt" align="right">
						<input type="hidden" id="locCoordinates" value="" />
						<!--  <input class="btn" value="Update" type="button" onclick="getEditRetLocationCoordinates();"  name="Update" tabindex="14" title="Update"/>-->
						<input class="btn" value="Update" type="button"
							onclick="submitEditRetailPage();" name="Update" tabindex="16"
							title="Update" />
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div style="display: none; height: 0px; width: 0px; margin-left: 0px; margin-top: 0px;" id="ifrmPopup" class="ifrmPopupPannel">
		<div class="headerIframe"> 
			<img align="middle" title="Click here to Close" onclick="closeIframePopup('ifrmPopup','ifrm')" alt="close" class="closeIframe" src="/ScanSeeWeb/images/popupClose.png"> 
			<span id="popupHeader">Select Filters</span> 
		</div>
		<iframe width="100%" height="273px" frameborder="0" style="background-color:White" allowtransparency="yes" id="ifrm" scrolling="auto"> </iframe>
	</div>
</form:form>
<script type="text/javascript">

	onLoad();
	//loadCityDD();
	//function loadCityDD() {
		//loadCity();
	//}
	
	
$("#retailerLocationLatitude").focusout(function() {
/* Matches	 90.0,-90.9,1.0,-23.343342
Non-Matches	 90, 91.0, -945.0,-90.3422309*/
	var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
			  var vLat = $('#retailerLocationLatitude').val();;
			  //Validate for Latitude.
			  if (0 === vLat.length || !vLat || vLat == "") {
				return false;
			 } else {
				if(!vLatLngVal.test(vLat)) {
			      alert("Latitude are not correctly typed");
			      $("#retailerLocationLatitude").val("").focus();
			      return false;
			  }
			 }
});

$("#retailerLocationLongitude").focusout(function() {
/* Matches	180.0, -180.0, 98.092391
Non-Matches	181, 180, -98.0923913*/
var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
			  var vLong = $('#retailerLocationLongitude').val();;
			  //Validate for Longitude.
			  if (0 === vLong.length || !vLong || vLong == "") {
				return false;
			 } else {
				if(!vLatLngVal.test(vLong)) {
			      alert("Longitude are not correctly typed");
			      $("#retailerLocationLongitude").val("").focus();
			      return false;
			  }
			 }
});

</script>



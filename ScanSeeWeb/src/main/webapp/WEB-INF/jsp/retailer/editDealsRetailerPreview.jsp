<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<script type="text/javascript">
$(function() {
    //Get actual content and display as title on mouse hover on it.
    var actText = $("td.genTitle").text();
    $("td.genTitle").attr('title',actText);
    var limit = 20;// character limit restricted to 20
    var chars = $("td.genTitle").text(); 
    if (chars.length > limit) {
        var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
        var dots = $("<span class='dots'>...</span>");
        $("td.genTitle").empty()
            .append(visibleArea)
            .append(dots);//append trailing dots to the visibile part 
    }                            
});
</script>

<div id="iphonePanel">
<form:form name="previeweditdealsform" commandName="previeweditdealsform">
  <div class="navBar iphone">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
	  <tr>
              <td width="19%"><img src="../images/backBtn.png" alt="back" width="50" height="30" /></td>
              <td width="54%" class="genTitle"><c:out value="${sessionScope.editHotDealName}"/></td>
              <td width="27%"><img src="../images/mainMenuBtn.png" alt="mainmenu" width="78" height="30" /></td>
      </tr>
	 </table>
	</div>  
  <div class="previewArea">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">  
      <tr>
        <td colspan="2" class="cntCart"><c:out value="${sessionScope.editHotDealName}"/></td>
      </tr>
      <!--/ScanSeeWeb/images/sharpTv.jpg-->
      <tr>
        <td width="35%"><img src="${sessionScope.editretProdImage}" alt="Image" width="108" height="66" /></td>
        <td width="65%" align="left" valign="top">
        	<ul class="lblValLst">
            	<li><b>Deal:<span class="amountTxt">$<c:out value="${sessionScope.editSalePrice}"/></span></b></li>
                <li><b>Regular Price:<span>$<c:out value="${sessionScope.editPrice}"/></span></b></li>
                <li><b>When:</b><span class="nrmltxt"><c:out value="${sessionScope.editDealStartDate}"/></span></li>
            </ul>        
	    </td>
      </tr>
      <tr>
        <td class="cntCart">Details:</td>
         <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2"><div class="scrolling">
                  <c:out value="${sessionScope.editHotDealLongDescription}"/>
               </div>
        </td>      
      </tr>
      <tr>
        <td><img src="../images/getDeal.png" alt="GetDeal" width="76" height="19" /></td>
        <td align="right" valign="top">Powered By:<strong>N/A</strong></td>
      </tr>
      </table>

  </div>
 </form:form> 
  <div id="TabBarIphone">
    <ul id="iphoneTab">
        <li><img src="../images/tab_btn_up_facebook.png" alt="facebook" width="80" height="50" /></li>
        <li><img src="../images/tab_btn_up_twitter.png" alt="twitter" width="80" height="50" /></li>
        <li><img src="../images/tab_btn_up_sendtxt.png" alt="sendtext" width="80" height="50" /></li>
        <li><img src="../images/tab_btn_up_sendmail.png" alt="sendmail" width="80" height="50" /></li>
    </ul>
  </div>
  
</div>

<br/>
<div align="center">
     <input class="btn"  value="Back" type="button" name="Back" onclick="javascript:history.back()"/>
</div>
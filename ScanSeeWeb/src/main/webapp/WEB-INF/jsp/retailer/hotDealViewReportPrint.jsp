<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html>
<body>
<script type="text/javascript">
window.onload = function() {
	window.print();
	window.close();
}
</script>
<div id="wrapper">
 <div id="header">
    <div class="floatL" id="header_logo">  <img alt="HubCiti" src="../images/hubciti-logo_ret.jpg"/> </div>
    <div class="clear"></div>
 </div>
  <div class="clear"></div>
	<div id="content" class="shdwBg">
			<div class="">
				<!--<div class="grpTitles">
					<h1 class="mainTitle">Hot Deals</h1>
				</div>-->
				<div class="grdSec brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl"  align="center">
						<tr ><td colspan="4" class="header">Hot Deals</td></tr>
						<tr class="sub-hdr">
							<td align="left">HotDeal Name</td>
							<td width="32%"># Of Hotdeals Issued</td>
							<td width="25%"># Of Users Claimed</td>
							<td width="24%"># Of Users Redeemed</td>
						</tr>
						<tr>
							<td align="left"><c:out value="${sessionScope.dealInfo.hotDealName}"/></td>
							<td><c:out value="${sessionScope.dealInfo.numOfHotDeals}" /></td>
							<td><c:out value="${sessionScope.dealInfo.claimCount}"  /></td>
							<td><c:out value="${sessionScope.dealInfo.redeemCount}"  /></td>
						</tr>
					</table>
					<c:if test="${sessionScope.dealInfo.contact ne null && ! empty sessionScope.dealInfo.contact }">
					<div class="searchGrd printGrd">
						<h1 class="searchHeaderExpand">
							<a href="#" class="floatR">&nbsp;</a>User Details
						</h1>
						<div class="grdCont zeroPadding">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="stripeMe" id="dealLst" align="center">
								<tr class="header">
									<td width="19%">First Name</td>
									<td width="33%">Email Id</td>
									<td width="24%">Claimed</td>
									<td width="24%">Redeemed</td>
								</tr>
								<c:forEach items="${sessionScope.dealInfo.contact}"
									var="userDetails">
									<tr>
										<td width="25%"><c:out value='${userDetails.contactFirstName}' />
										</td>
										<td ><c:out value='${userDetails.contactEmail}'/>
										</td>
										<td ><c:out value='${userDetails.claimed}'/>
										</td>
										<td ><c:out value='${userDetails.redeemed}'/>
										</td>
									</tr>
								</c:forEach>
							</table>
						</div>
					</div>
					</c:if>
					<!--  <div class="navTabSec RtMrgn LtMrgn" align="right">
							<input class="btn"
							onclick="location='/ScanSeeWeb/retailer/hotDealRetailer.htm'"
							value="Back" type="Button" name="Back" title="Back"/>
					</div>-->
				</div>
			</div>
	</div>
</div>
<div class="clear"></div>
</body>


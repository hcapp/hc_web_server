<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.RetailerLocation"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script>
	saveManageLocationPage = 'true';
	
	
	var changeImgDim = '${sessionScope.ChangeImageDim}';
	if (null != changeImgDim && changeImgDim == 'true') {
		$('#upldLocatnImg').width('28px');
		$('#upldLocatnImg').height('28px');
	}
</script>
<script>
$(document)
		.ready(
				function() {
					
					$(".alertBx").hide();
				
					var getRow = $("#locSetUpGrd tr:gt(1)").length;
					if(getRow == 0)
					{
						$(".searchGrd").removeClass("hrzVrtScrollTbl"); 
					}
					else {
						$(".searchGrd").addClass("hrzVrtScrollTbl"); 
					}
					});
					
function showModal(retLocId) {
	$("#retailLocationID").val(retLocId);
	
	$.ajax({
		cache: false,
        type : "GET",
        url : "displayBusinessHours.htm",
        data : {
        "locationId" : retLocId
        },
        success: function(response){
        		$(".alertBx").hide();
        		$('.modal-cont').html(response);
        	 	var bodyHt = $('body').height();
        	    var setHt = parseInt(bodyHt);
        	    $(".modal-popupWrp").show();
        	    $(".modal-popupWrp").height(setHt);
        	    $(".modal-popup").slideDown('fast');
        }
    });
}

/*close modal popup on click of x*/
function closeModal() {
    $(".modal-popupWrp").hide();
    $(".modal-popup").slideUp();
    $(".modal-popup").find(".modal-bdy input").val("");
    $(".modal-popup i").removeClass("errDsply");
    $(".overlay").hide();
}
</script>
<script type="text/javascript">

function isLatLong(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31 ||charCode == 45 || charCode == 43)
		return true;
	return false;
}

window.onload = function() {
	var vRetailerLocID = document.locationform.retailerLocID.value;
	var vRetLocList = [];
	var objArr = [];
	var vAllLocationList = [];
	if (vRetailerLocID != "null" && vRetailerLocID != "") {
		vRetLocList = vRetailerLocID.split(",");
		for(i=0; i<vRetLocList.length ; i++){
		$("tr#" + vRetLocList[i]).removeClass("hilite").addClass("requiredVal");	
		}
	}
}


	function numRows() {
		var rowCount = $('#locSetUpGrd tr').length;
		if (rowCount <= 1) {
			alert("No records to save");
			return false;
		}
		
		var name = new Array(rowCount);
		var i = 1;
		$('#locSetUpGrd tr').each(function(i) {
			name[i] = $(this).find("td:eq(0) input[type='text']").val();
			i = i + 1;
		});
		var flag = hasDuplicates(name);
		
		if (flag) {
			alert("Store ID should be unique.");
		} else {
			var result = confirm("Do you want to save the changes");
			if (result) {
				editManageLocation();
			}
			
		}
	}
	function hasDuplicates(array) {
		var valuesSoFar = {};
		for ( var i = 0; i < array.length; ++i) {
			var value = array[i];
			if (Object.prototype.hasOwnProperty.call(valuesSoFar, value)) {
				return true;
			}
			valuesSoFar[value] = true;
		}
		return false;
	}
	function navigtoproductSetup() {
		document.locationform.action = "retailerchoosePlan.htm";
		document.locationform.method = "GET";
		document.locationform.submit();
	}
	
	function saveBusinessHours() {	
		$(".alertBx").hide();
	var timeZone = $("select[name='storeTimeZoneId'] option:selected").text();
	var storeStartHrs = null;
	var storeStartMins = null;
	var storeEndHrs = null;
	var storeEndMins = null;
	var retLocId = $("#retailLocationID").val();
	
	$('select[name^="storeStartHrs"]').each(function() {
		if(storeStartHrs == null) {
			storeStartHrs = $(this).val() +",";
		} else {
			storeStartHrs += $(this).val() +",";
		}
	});
	$('select[name^="storeStartMins"]').each(function() {
		if(storeStartMins == null) {
			storeStartMins = $(this).val() +",";
		} else {
			storeStartMins += $(this).val() +",";
		}
	});
	$('select[name^="storeEndHrs"]').each(function() {
		if(storeEndHrs == null) {
			storeEndHrs = $(this).val() +",";
		} else {
			storeEndHrs += $(this).val() +",";
		}
	});
	$('select[name^="storeEndMins"]').each(function() {
		if(storeEndMins == null) {
			storeEndMins = $(this).val() +",";
		} else {
			storeEndMins += $(this).val() +",";
		}
	});
	
	$.ajax({
		cache: false,
        type : "GET",
        url : "saveBusinessHours.htm",
        data : {
       "storeStartHrs" : storeStartHrs,
        "storeStartMins" : storeStartMins,
       "storeEndHrs" : storeEndHrs,
        "storeEndMins" : storeEndMins,
        "timeZone" : timeZone,
        "retailerLocationID" : retLocId
        },
        success: function(response){
        	if(response == "SUCCESS"){
        		 $(".success").show();
         	} else {
        		$(".failure").show();
        	} 
        } 
    });

	}
	function populateHttp(obj) {
		var value = $(obj).val();
		alert(value);
	}

</script>
<script>
$(function() {
	$('#storeTimeZoneId').change(function() {
	   $('#timeZoneHidden').val($("#storeTimeZoneId option:selected").text());
	   
	    });
	});
</script>
<form:form name="locationform" commandName="locationform"
	action="/ScanSeeWeb/retailer/managelocationimg.htm"
	acceptCharset="ISO-8859-1" enctype="multipart/form-data">
	<div id="bubble_tooltip">
		<div class="bubble_top">
			<span></span>
		</div>
		<div class="bubble_middle">
			<span id="bubble_tooltip_content"></span>
		</div>
		<div class="bubble_bottom"></div>
	</div>
	<div id="wrapper">
		<form:hidden path="retailID" value="${item.retailID}" />
		<form:hidden path="uploadRetaillocationsID"
			value="${item.uploadRetaillocationsID}" />
		<form:hidden path="prodJson" />
		<form:hidden path="pageNumber" />
		<form:hidden path="pageFlag" />
		<form:hidden path="retailLocationID" value="${item.retailLocationID}" />
		<form:hidden path="retailerLocID" id="retailerLocID"
			value="${requestScope.rejectedLocations}" />
		<form:hidden path="rowIndex" />
		<form:hidden path="viewName" value="fetchbatchlocationlist" />
		<form:hidden path="prodJson1" />
		<form:hidden path="timeZoneHidden" id="timeZoneHidden" />
		<div id="dockPanel">
			<ul id="prgMtr" class="tabs">
				<li><a title="Upload Ads/Logos" href="uploadRetailerLogo.htm"
					rel="Upload Ads/Logos">Upload Logo</a></li>
				<li><a class="tabActive" title="Location Setup"
					href="locationsetup.htm" rel="Location Setup">Location Setup</a></li>
				<li><a title="Choose Plan"
					href="/ScanSeeWeb/retailer/retailerchoosePlan.htm"
					rel="Choose Plan">Choose Plan</a></li>
				<li><c:choose>
						<c:when test="${isPaymentDone == null}">
							<a title="Dashboard" href="/ScanSeeWeb/retailer/retailerhome.htm"
								rel="Dashboard">Dashboard</a>
						</c:when>
						<c:otherwise>
							<a title="Dashboard" href="#" rel="Dashboard">Dashboard</a>
						</c:otherwise>
					</c:choose></li>
			</ul>
			<a id="Mid" name="Mid"></a>
			<div class="floatR tglSec" id="tabdPanelDesc">
				<div id="filledGlass">
					<img src="../images/Step5.png" />
					<div id="nextNav">
						<a href="#"
							onclick="location.href='/ScanSeeWeb/retailer/regaddseachprod.htm'">
							<img class="NextNav_R" alt="Next" src="../images/nextBtn.png"
							border="0" /> <span>Next</span>
						</a>
					</div>
				</div>
			</div>
			<div class="floatL tglSec" id="tabdPanel">
				<img height="283" alt="Flow1" src="../images/Flow5_ret.png"
					width="782" />
			</div>
		</div>

		<div class="clear"></div>
		<div id="content">
			<div id="subnav">
				<ul>
					<li><a href="/ScanSeeWeb/retailer/addlocation.htm"><span>Add
								Location(s)</span> </a></li>
					<li><a href="/ScanSeeWeb/retailer/locationsetup.htm"><span>Upload
								Location</span> </a></li>
					<li><a href="#" class="active"><span>Update
								Locations</span> </a></li>
				</ul>
			</div>
			<div class="grdSec">
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="grdTbl">
					<tr>
						<td colspan="2" class="header">Update Locations</td>
					</tr>
					<tr>
						<td class="Label" width="18%">Store Identification</td>
						<td><form:input path="searchKey" type="text" name="searchKey" />
							<!-- IE JS Error "Object Expected" <a onclick="fetchBatchLocationSetup();" >-->
							<img class=imgLinks src="/ScanSeeWeb/images/searchIcon.png"
							alt="Search" title="Search" width="20" height="17"
							onclick="fetchBatchLocationSetup();" /> <c:if
								test="${requestScope.errorMSG ne null && !empty requestScope.errorMSG}">
								<font color="red"> <c:out
										value="${requestScope.errorMSG}" />
								</font>
							</c:if>
							<div class="sub-actn" style="margin-left: 54% !important;">
								<a href="/ScanSeeWeb/retailer/locationinstructions.htm">View
									Instructions</a>
							</div></td>
					</tr>
				</table>
				<div align="center" style="font-style: 90">
					<label style="${requestScope.manageLocation}"><c:out
							value="${requestScope.successMSG}" /> </label>
				</div>

				<c:choose>
					<c:when
						test="${requestScope.rejectedLocations ne null && requestScope.rejectedLocations ne ''}">
						<div class="searchGrd zeroPadding hScroll">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="tblEffect" id="locSetUpGrd">
								<tr class="header">
									<td width="21%"><p>
											<a href="#" class="hdr-white"
												onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
												onmouseout="hideToolTip()">Store</a>
										</p>
										<p>
											<a href="#" class="hdr-white"
												onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
												onmouseout="hideToolTip()">Identification</a><a href="#"
												onmousemove="showToolTip(event,'Store Identification can either be your store number or other unique identifier.');return false"
												onmouseout="hideToolTip()"><label class="mand"><img
													alt="helpIcon" src="../images/helpIcon.png" /> </label> </a>
										</p> <form:errors cssStyle="color:red" path="storeIdentification"></form:errors>
									</td>
									<td width="18%"><label class="mand">Store Address</label></td>
									<td width="17%"><label class="mand">Latitude</label></td>
									<td width="17%"><label class="mand">Longitude</label></td>
									<td width="17%"><label class="mand">City</label></td>
									<td width="17%"><label class="mand">State</label></td>
									<td width="10%"><label class="mand">Postal Code</label></td>
									<td width="17%"><label class="mand">Phone Number</label></td>
									<td width="17%"><label>Website URL</label></td>
									<td width="17%"><label>Keywords</label></td>
									<td width="17%">Image Upload</td>
									<td width="17%"><label>Open Time</label></td>
									<td width="17%"><label>Close Time</label></td>
									<td width="17%"><label>Time Zone</label></td>
								</tr>
								<c:set var="rowIndex" value="0"></c:set>
								<c:forEach items="${requestScope.locResult.locationList}"
									var="item">
									<tr id="${item.retailLocationID}">
										<td><form:hidden path="retailLocationID"
												value="${item.retailLocationID}" /> <form:input
												path="storeIdentification" cssClass="textboxSmall"
												value="${item.storeIdentification}" /></td>
										<td><form:input path="address1" cssClass="textboxAddress"
												maxlength="150" value="${item.address1}" /></td>
										<td><form:input path="retailerLocationLatitude"
												cssClass="textboxSmall latChk"
												value="${item.retailerLocationLatitude}" id="locLatitude" /></td>
										<td><form:input path="retailerLocationLongitude"
												cssClass="textboxSmall lonChk"
												value="${item.retailerLocationLongitude}" id="locLongitude" /></td>
										<td><form:input path="city" cssClass="textboxSmall"
												maxlength="50" value="${item.city}" /></td>
										<td><form:input path="state" cssClass="textboxSmaller"
												maxlength="2" value="${item.state}" onpaste="return false"
												ondrop="return false" ondrag="return false"
												oncopy="return false" /></td>
										<td><form:input path="postalCode"
												cssClass="textboxSmaller" maxlength="10"
												value="${item.postalCode}"
												onkeypress="return isNumberKey(event)" /></td>
										<td><form:input path="phonenumber" maxlength="10"
												name="phonenumber" cssClass="textboxSmaller"
												value="${item.phonenumber}"
												onkeypress="return isNumberKey(event)" /></td>
										<td><form:input path="retailLocationUrl"
												cssClass="textboxSmall" value="${item.retailLocationUrl}"
												onkeydown="populateHttp('this')" /></td>
										<td><form:input path="keyword" cssClass="textboxSmall"
												value="${item.keyword}" /></td>
										<td>
											<div class="img-row">
												<!--  <span class="col"><img width="30" height="30" title="click to edit" alt="image" src="images/dfltImg.png" class="img-preview"></span>-->
												<span class="col"><img width="30" height="30"
													alt="image"
													name="logoPreview+${item.uploadRetaillocationsID}"
													src="${item.imgLocationPath}" class="img-preview"> <form:hidden
														path="gridImgLocationPath"
														value="${item.gridImgLocationPath}" /> <form:hidden
														path="imgLocationPath" value="${item.imgLocationPath}" />
													<form:hidden path="uploadImage" value="${item.uploadImage}" />
													<span class="col"><input type="file"
														class="textboxBig" id="img${item.uploadRetaillocationsID}"
														name="imageFile"
														onChange="manageLocationImgValidate(this, ${rowIndex });"></span>
											</div>
										</td>

										<td><input type="button" class="btn"
											value="Add Business Hours"
											onclick="showModal('${item.retailLocationID}');" /></td>


									</tr>
									<c:set var="rowIndex" value="${rowIndex+1}"></c:set>
								</c:forEach>
							</table>
						</div>
					</c:when>
					<c:otherwise>
						<div class="searchGrd zeroPadding hScroll">
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								class="tblEffect" id="locSetUpGrd">
								<tr class="header">
									<td width="21%"><p>
											<a href="#" class="hdr-white"
												onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
												onmouseout="hideToolTip()">Store</a>
										</p>
										<p>
											<a href="#" class="hdr-white"
												onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
												onmouseout="hideToolTip()">Identification</a><a href="#"
												onmousemove="showToolTip(event,'Store Identification can either be your store number or other unique identifier.');return false"
												onmouseout="hideToolTip()"><label class="mand"><img
													alt="helpIcon" src="../images/helpIcon.png" /> </label> </a>
										</p> <form:errors cssStyle="color:red" path="storeIdentification"></form:errors>
									</td>
									<td width="18%"><label class="mand">Store Address</label>
									</td>
									<td width="17%"><label>Latitude</label></td>
									<td width="17%"><label>Longitude</label></td>
									<td width="17%"><label class="mand">City</label></td>
									<td width="17%"><label class="mand">State</label></td>
									<td width="10%"><label class="mand">Postal Code</label></td>
									<td width="17%"><label class="mand">Phone Number</label></td>
									<td width="17%"><label>Website URL</label></td>
									<td width="17%"><label>Keywords</label></td>
									<td width="17%">Image Upload</td>
									<td width="17%"><label>Business Hours</label></td>
								</tr>
								<c:set var="rowIndex" value="0"></c:set>
								<c:forEach items="${requestScope.locResult.locationList}"
									var="item">
									<tr id="${item.retailLocationID}">
										<td><form:hidden path="uploadRetaillocationsID"
												value="${item.uploadRetaillocationsID}" /> <form:hidden
												path="retailLocationID" value="${item.retailLocationID}" />
											<form:input path="storeIdentification"
												cssClass="textboxSmall" value="${item.storeIdentification}" />
										</td>
										<td><form:input path="address1" cssClass="textboxAddress"
												maxlength="150" value="${item.address1}" /></td>
										<td><form:input path="retailerLocationLatitude"
												cssClass="textboxSmall latChk"
												value="${item.retailerLocationLatitude}" id="locLatitude" /></td>
										<td><form:input path="retailerLocationLongitude"
												cssClass="textboxSmall lonChk"
												value="${item.retailerLocationLongitude}" id="locLongitude" /></td>


										<td><form:input path="city" cssClass="textboxSmall"
												maxlength="50" value="${item.city}" /></td>
										<td><form:input path="state" cssClass="textboxSmaller"
												maxlength="2" value="${item.state}" onpaste="return false"
												ondrop="return false" ondrag="return false"
												oncopy="return false" /></td>
										<td><form:input path="postalCode"
												cssClass="textboxSmaller" maxlength="10"
												value="${item.postalCode}"
												onkeypress="return isNumberKey(event)" /></td>
										<td><form:input path="phonenumber" maxlength="10"
												name="phonenumber" cssClass="textboxSmaller"
												value="${item.phonenumber}"
												onkeypress="return isNumberKey(event)" /></td>
										<td><form:input path="retailLocationUrl" maxlength="75"
												cssClass="textboxSmall" value="${item.retailLocationUrl}" />
										</td>
										<td><form:input path="keyword" cssClass="textboxSmall"
												value="${item.keyword}" /></td>
										<td>
											<div class="img-row">
												<!--  <span class="col"><img width="30" height="30" title="click to edit" alt="image" src="images/dfltImg.png" class="img-preview"></span>-->
												<form:hidden path="gridImgLocationPath"
													value="${item.gridImgLocationPath}" />
												<form:hidden path="imgLocationPath"
													value="${item.imgLocationPath}" />
												<form:hidden path="uploadImage" value="${item.uploadImage}" />
												<span class="col"><img width="30" height="30"
													alt="image"
													name="logoPreview+${item.uploadRetaillocationsID}"
													src="${item.imgLocationPath}" class="img-preview"></span> <span
													class="col"><input type="file" class="textboxBig"
													id="img${item.uploadRetaillocationsID}" name="imageFile"
													onChange="manageLocationImgValidate(this, ${rowIndex });"></span>
											</div>
										</td>

										<td><input type="button" class="btn"
											value="Add Business Hours"
											onclick="showModal('${item.retailLocationID}');" /></td>

									</tr>
									<c:set var="rowIndex" value="${rowIndex+1}"></c:set>
								</c:forEach>
							</table>
							<div class="ifrmPopupPannelImage" id="ifrmPopup2"
								style="display: none;">
								<div class="headerIframe">
									<img src="/ScanSeeWeb/images/popupClose.png"
										class="closeIframe" alt="close"
										onclick="javascript:closeIframePopup('ifrmPopup2','ifrm2')"
										title="Click here to Close" align="middle" /> <span
										id="popupHeader"></span>
								</div>
								<iframe frameborder="0" scrolling="no" id="ifrm2" src=""
									height="100%" allowtransparency="yes" width="100%"
									style="background-color: White"> </iframe>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
				<div class="pagination brdrTop">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="noBrdr" id="perpage">
						<tr>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" enablePerPage="false" />
						</tr>
					</table>
				</div>
			</div>
			<div class="navTabSec RtMrgn LtMrgn">
				<div align="right">
					<input class="btn" onclick="numRows();" type="button"
						value="Submit" name="Cancel" title="Submit" /> <input
						onclick="location.href='/ScanSeeWeb/retailer/retailerchoosePlan.htm'"
						class="btn" type="button" value="Continue" name="Cancel"
						title="Continue" />
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="modal-popupWrp">
		<div class="modal-popup">
			<div class="modal-hdr">
				Setup Business Hours<a class="modal-close" title="close"
					onclick="closeModal()"><img src="../images/popupClose.png" /></a>
			</div>
			<div class="modal-bdy">
				<div class="alertBx failure">
					<p class="msgBx">
						<c:out value="Please select Time Zone" />
					</p>
				</div>
				<div class="alertBx success">
					<p class="msgBx">
						<c:out value="Business Hours saved Successfully" />
					</p>
				</div>
				<div class="modal-cont brdr"></div>

			</div>
			<div class="modal-ftr">
				<p align="right">
					<input name="Save" value="Save" type="button" class="btn" id="save"
						onclick="saveBusinessHours();" />
				</p>
			</div>
		</div>
	</div>

</form:form>


<script>
$('.latChk').bind('blur',function() {
/* Matches	 90.0,-90.9,1.0,-23.343342
Non-Matches	 90, 91.0, -945.0,-90.3422309*/
	var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
			  var vLat = this.value;
			  //Validate for Latitude.
			  if (0 === vLat.length || !vLat || vLat == "") {
				return false;
			 } else {
				if(!vLatLngVal.test(vLat)) {
			      alert("Latitude are not correctly typed");
			      this.value = '';
			      return false;
			  }
			 }
});

$('.lonChk').bind('blur',function() {
/* Matches	180.0, -180.0, 98.092391
Non-Matches	181, 180, -98.0923913*/
var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
			  var vLong = this.value;
			  //Validate for Longitude.
			  if (0 === vLong.length || !vLong || vLong == "") {
				return false;
			 } else {
				if(!vLatLngVal.test(vLong)) {
			      alert("Longitude are not correctly typed");
			      this.value = '';
			      return false;
			  }
			 }
});

</script>
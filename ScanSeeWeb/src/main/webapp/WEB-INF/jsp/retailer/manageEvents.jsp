<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script type="text/javascript">




</script>
<body onload="resizeDoc();" onresize="resizeDoc();">
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">Events</h1>
				</div>
				<form:form commandName="manageeventform" name="manageeventform"
					acceptCharset="ISO-8859-1">	
					<form:hidden path="eventID" />
					
					<div class="overlay"></div>
					<div class="grdSec brdrTop">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="tranTbl">
							<tr>
								<td colspan="3" class="header">Search Events</td>
							</tr>
							<tr>
								<td width="20%" align="left"><label for="label">
										Event Name</label></td>
								<td width="60%"><form:input path="eventSearchKey"
										type="text" name="textfield" id="gnrlSrch" /> <a href="#">
										<img src="/ScanSeeWeb/images/searchIcon.png" alt="Search"
										title="Search" onclick="searchEvents();" width="25"
										height="24" />
								</a> <label for="gnrlSrch"></label></td>
								<td width="20%"><input type="button" class="btn"
									value="Add Event" onclick="window.location.href='addevent.htm'"
									title="Add Event" /></td>
							</tr>
						</table>
						
						<div class="searchGrd">
							<h1 class="searchHeaderExpand">
								<a href="#" class="floatR">&nbsp;</a>Current Events
							</h1>
							<div class="grdCont tableScroll zeroPadding">

								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="gr">
									<tbody>

										<tr class="header">
											<td width="18%">Event Name</td>
											<c:if test="${!sessionScope.ban}">
											<td>Location</td>
											</c:if>
											<td width="22%">Start Date</td>
											<td width="22%">End Date</td>
											<td width="28%">Action</td>

										</tr>
										<c:if test="${sessionScope.responseEventMsg ne null }">
											<div id="message">
													<center><label style="${sessionScope.eventMessageFont}" >
													<c:out value="${sessionScope.responseEventMsg}" /></label></center>
											</div>
											<script>var PAGE_MESSAGE = true;</script>
										</c:if>

										<c:if
											test="${eventManageList ne null && ! empty eventManageList}">
											<c:forEach items='${sessionScope.eventManageList.eventList}'
												var='item' varStatus="indexnum">
												<tr>
													<td><c:out value="${item.eventName}" /></td>
													<c:if test="${!sessionScope.ban}">
													<td><label> <input type="button" name="button"
															class="btn tgl-overlay" id="button" value="Show Locations"
															title="Show Event Locations"
															onclick="javascript:showEventLocation(${item.eventID})"  /></label>
													</td>
													</c:if>
													<td><c:out value="${item.eventStartDate}" /></td>
													<td><c:out value="${item.eventEndDate}" /></td>
													<td><label> <a href="#"><img
																src="/ScanSeeWeb/images/edit_icon.png" title="edit"
																alt="edit" width="25" height="19"
																onclick="javascript:editEvent(<c:out value='${item.eventID}'/>);" />
														</a> &nbsp;&nbsp;&nbsp; <a href="#"><img
																src="/ScanSeeWeb/images/deleteRedIcon.png"
																title="delete" alt="delete" width="24" height="22"
																onclick="javascript:deleteEvent(<c:out value='${item.eventID}'/>);" />
														</a></label></td>
												</tr>
											</c:forEach>
										</c:if>
								</table>
							</div>

							<c:if
								test="${sessionScope.eventManageList ne null && !empty sessionScope.eventManageList}">
								<div class="pagination brdrTop">

									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="noBrdr" id="perpage">
										<tr>
											<page:pageTag
												currentPage="${sessionScope.pagination.currentPage}"
												nextPage="4"
												totalSize="${sessionScope.pagination.totalSize}"
												pageRange="${sessionScope.pagination.pageRange}"
												url="${sessionScope.pagination.url}" enablePerPage="false" />
										</tr>
									</table>
								</div>
							</c:if>
						</div>
					</div>
				</form:form>
			</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup"
			style="display: none; background-color: White">
			<div class="headerIframe">
				<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
					alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
				height="100%" allowtransparency="yes" width="100%"
				style="background-color: White"> </iframe>
		</div>

		<div class="clear"></div>
	</div>
	<script>	$(".searchGrd table tr td:not(:last-child)").css({ "border-right":"1px solid #c1e4f0" });</script>
</body>

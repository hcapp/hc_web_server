<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Coupons</title>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.jscroll.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.tablescroll.js" type="text/javascript"></script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
function getselectedProductID(){
	 var allVals = [];
	var allScanCode = [];
	var allProdId = [];
	 $('input[name ="pUpcChk"]:checked').each(function() {
	       allVals.push($(this).val());
	     });
		 
		 for(var i=0;i<allVals.length;i++){
		 var temp = allVals[i].split(",");
		 allScanCode[i]=temp[0];
		 allProdId[i]=temp[1];
		 }

			if(allProdId.length >0){
				 var $prdID = top.$('#prodcutID');
				 var $scanCode = top.$('#couponName');
				 
				$prdID.val(allProdId.toString());
				$scanCode.val(allScanCode.toString());
				closeIframePopup('ifrmPopup','ifrm',callBackFunc())

			}else{
				alert("Please select Products")
				}
				
	}

$(document).keypress(function(e) {
    if(e.which == 13) {
     //  alert('You pressed enter!');
    	retailerRebateProdUPC();
    }
 });

function clearForm()
{
document.getElementById("couponName").value="";
}
</script>
</head>
<body class="whiteBG">
	<div class="contBlock">
		<form:form commandName="retprodupclistForm" name="retprodupclistForm">
			<form:hidden path="retailerLocID" id="retailerLocID" name="retailerLocID"/>
			<form:hidden path="retailID" id="retailID" />
			<fieldset>
				<legend>Product/UPC Search</legend>
				<div class="contWrap">
					<table width="100%" cellsRpacing="0" cellpadding="0" border="0"
						class="grdTbl">
						<tbody>
							<tr>
								<td width="18%" class="Label"><label for="couponName">Product/
										UPC</label></td>

								<td width="29%"><form:input path="productName" type="text"
										name="textfield2" id="couponName"  /></td>

							</tr>
						</tbody>
					</table>

					<div class="navTabSec mrgnRt" align="right">
						<!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
						<input name="Save" value="Search" type="button" class="btn"
							onclick="retailerRebateProdUPC();" id="save" /> <input name="Save"
							value="Clear" type="button" class="btn" id="clr" onclick="clearForm();"/>
					</div>
				</div>
			</fieldset>
		</form:form>
		<fieldset class="popUpSrch">
			<legend>Product/UPC Details</legend>
			<div id="" class="grdCont searchGrd">
				<table id="thetable" class="stripeMe" border="0" cellspacing="0"
					cellpadding="0" width="100%">
					<thead>
						<tr class="header hvrEfct">
							<td width="28%">Product Name</td>
							<td width="20%">UPC</td>
							<td width="38%">Short Description</td>
							<td align="center">Action</td>
						</tr>
						</thead>
						<tbody>
						<c:if test="${message ne null }">
							<div id="message">
								<center>
									<c:out value="${message}" />
								</center>
							</div>
							<script>var PAGE_MESSAGE = true;</script>
						</c:if>
							<c:if test="${pdtInfoList ne null && ! empty pdtInfoList}">
							<c:forEach items="${sessionScope.pdtInfoList}"
								var="earlierAddedPdts">
								<tr>
									<td><c:out value="${earlierAddedPdts.productName}" /></td>
									<td><c:out value="${earlierAddedPdts.scanCode}" /></td>
									<td><c:out value="${earlierAddedPdts.shortDescription}" />
									</td>
									<td width="6%" align="center"><input type="checkbox"
										checked="checked" id="searchCheckBox" class="check"
										name="pUpcChk" value="${earlierAddedPdts.scanCode},${earlierAddedPdts.productID}"
										 />
									</td>
								</tr>
							</c:forEach>
						</c:if>
						<c:forEach items="${sessionScope.rebateprodupclist}" var="item">
							<tr>
								<td><c:out value="${item.productName}" />
								</td>
								<td><c:out value="${item.scanCode}" />
								</td>
								<td><c:out value="${item.shortDescription}" />
								</td>
								<td align="center"><input type="checkbox"
									id="searchCheckBox" class="check" name="pUpcChk"
									value="${item.scanCode},${item.productID}"
									 /></td>
								<!--<td style ="display:none"><input type="checkbox" name="UpcChk" value="${item.productID}" id="hiddencheck"/></td>
							-->
							</tr>

						</c:forEach>
					
					</tbody>
				</table>
			</div>
			<div class="navTabSec mrgnRt" align="right">
				<input name="associate" value="Associate"
					onclick="getselectedProductID();" type="button" class="btn"
					id="associate" title="Associate" />
			</div>
		</fieldset>
	</div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
	});
	$(document).ready(
			function() {

				CKEDITOR.config.uiColor = '#FFFFFF';
				CKEDITOR.editorConfig = function(config) {
					//config.uiColor = '#AADC6E';
					config.language = 'es';
					config.uiColor = '#f3f3f3';
					config.toolbarCanCollapse = true;
					config.extraPlugins = 'wordcount';
					config.wordcount = {

						// Whether or not you want to show the Word Count
						showWordCount : true,

						// Whether or not you want to show the Char Count
						showCharCount : true,

						// Maximum allowed Char Count
						maxCharCount : 500
					};
				};
				CKEDITOR.replace('couponLongDesc', {

					extraPlugins : 'onchange',
					width : "100%",
					toolbar : [
							{
								name : 'basicstyles',
								items : [ 'Bold', 'Italic', 'Underline' ]
							},
							{
								name : 'paragraph',
								items : [ 'JustifyLeft', 'JustifyCenter',
										'JustifyRight', 'JustifyBlock' ]
							}, {
								name : 'colors',
								items : [ 'BGColor' ]
							}, {
								name : 'paragraph',
								items : [ 'Outdent', 'Indent' ]
							}, {
								name : 'links',
								items : [ 'Link', 'Unlink' ]
							}, '/', {
								name : 'styles',
								items : [ 'Styles', 'Format' ]
							}, {
								name : 'tools',
								items : [ 'Font', 'FontSize', 'RemoveFormat' ]
							} ],
					removePlugins : 'resize'
				});
				$("#datepicker3").datepicker({
					showOn : 'both',
					buttonImageOnly : true,
					buttonText : 'Click to show the calendar',
					buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
				});
			});
	$(document).ready(function() {
		$('#locationID option').click(function() {
			var totOpt = $('#locationID option').length;
			var totOptSlctd = $('#locationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		$("#locationID").change(function() {
			var totOpt = $('#locationID option').length;
			var totOptSlctd = $('#locationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
	});

	/*$(document).ready(function() {
		$('#locationID').change(function(){
		 var len = $(this).find('option').length;
		 var sLen = $(this).find('option:selected').length;
		 if(len < sLen) {
		   $('#chkAllLoc').attr('checked','checked');
		 }else {
			$('#chkAllLoc').removeAttr('checked');
		 }
		});
		$('#locationID').trigger('change');
	});/*

	
	/*function onClaimTypeLoad() {
	var claimType = document.reruncouponform.couponDiscountTypehidden.value;
	var sel = document.getElementById("clmType");
	for ( var i = 0; i < sel.options.length; i++) {
	if (sel.options[i].value == claimType) {
	sel.options[i].selected = true;
	return;
	}
	}
	} */
	/*function onLoadRetLoc() {

	 var vlocationID = document.reruncouponform.retailLocationID.value;
	 var vlocationVal = document.getElementById("locationID");
	 if (vlocationID != "null") {
	 var vlocationIDList = vlocationID.split(",");
	 }

	 for ( var i = 0; i < vlocationVal.length; i++) {
	 for (var j = 0; j < vlocationIDList.length; j++) {

	 if (vlocationVal.options[i].value == vlocationIDList[j]) {
	 vlocationVal.options[i].selected = true;
	 break;
	 }
	 }
	 }

	 } */
</script>
<script type="text/javascript">
	var changeImgDim = '${sessionScope.ChangeImageDim}';
	if (null != changeImgDim && changeImgDim == 'true') {
		$('#couponImg').width('70px');
		$('#couponImg').height('70px');
	}

	function checkCouponImgValidate(input) {
		var vCouponImg = document.getElementById("trgrUpld").value;
		if (vCouponImg != '') {
			var checkCouponImg = vCouponImg.toLowerCase();
			if (!checkCouponImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Deal List image with following extensions :  .png, .gif, .bmp, .jpg, .jpeg");
				document.reruncouponform.imageFile.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}

	function checkAssociatedCouponProd() {
		var $prdID = $('#productID').val();
		$.ajaxSetup({
			cache : false
		});
		if ($prdID != "") {
			$.ajax({
				type : "GET",
				url : "/ScanSeeWeb/checkAssociatedCouponProd.htm",
				data : {
					'productId' : $prdID
				},
				success : function(response) {
					openIframePopup('ifrmPopup2', 'ifrm2', 'produpclist.htm',
							420, 600, 'View Product/UPC')
				},
				error : function(e) {
					alert('Error:' + 'Error Occured');
				}
			});

		} else {
			openIframePopup('ifrmPopup2', 'ifrm2', 'produpclist.htm', 420, 600,
					'View Product/UPC')
		}
	}
	function checkDealImgValidate(input) {
		var vCouponImg = document.getElementById("trgrUpldDetImg").value;
		if (vCouponImg != '') {
			var checkCouponImg = vCouponImg.toLowerCase();
			if (!checkCouponImg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Deal image with following extensions :  .png, .gif, .bmp, .jpg, .jpeg");
				document.createcouponform.imageFile.value = "";
				document.getElementById("trgrUpldDetImg").focus();
				return false;
			}
		}
	}

	function onLoadRetLocationID() {
		var vRetLocID = document.reruncouponform.retLocationIDs.value;
		var vRetLocVal = document.getElementById("locationID");
		var vRetLocList = [];
		if (vRetLocID != "null" && vRetLocID != "") {
			vRetLocList = vRetLocID.split(",");
		}
		if (vRetLocVal.length != 0 && vRetLocList.length != 0) {
			if (vRetLocVal.length == vRetLocList.length) {
				document.getElementById('chkAllLoc').checked = true;
			}
		}

		for (var i = 0; i < vRetLocVal.length; i++) {
			for (j = 0; j < vRetLocList.length; j++) {
				if (vRetLocVal.options[i].value == vRetLocList[j]) {
					vRetLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("locationID");
		if (checked == true) {
			for (var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for (var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
</script>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>
</head>
<body>
	<div id="wrapper">
		<form:form name="reruncouponform" commandName="reruncouponform"
			method="POST" action="/ScanSeeWeb/retailer/uploadcouponimg.htm"
			acceptCharset="ISO-8859-1" enctype="multipart/form-data">
			<div id="content" class="topMrgn">
				<div class="section topMrgn">
					<div class="grdSec brdrTop">
						<form:input type="hidden" path="couponID" id="couponID" />
						<form:input type="hidden" path="retailLocationID"
							id="retailLocationID" />
						<form:hidden path="retLocationIDs" id="retLocationIDs" />
						<input type="hidden" id="selRelLoc" name="selRelLoc" /> <input
							type="hidden" name="formName" value="rerunCoupon" />
						<form:hidden path="couponDiscountTypehidden"
							name="couponDiscountTypehidden" />
						<form:hidden path="viewName" value="reruncoupon" />
						<form:hidden path="couponImgPath" id="createcoupon" />
						<form:hidden path="couponImagePath" id="couponImagePath" />
						<form:hidden path="productID" id="productID" />
						<form:hidden path="uploadCoupImageType" />
						<form:hidden path="detailImgPath" id="createcoupon" />
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="grdTbl">
							<tr>
								<td colspan="4" class="header">Deal Re-run
									<div class="sub-actn">
										<a href="#"
											onclick="location='/ScanSeeWeb/retailer/addcouponinstructions.htm?cptype=Re-Run'">View
											Instructions</a>
									</div> <c:if test="${message ne null }">
										<span class="alertTxt-dsply leftPdng"> <c:out
												value="${message}" />
										</span>
										<script>
											var PAGE_MESSAGE = true;
										</script>
									</c:if>
								</td>
							</tr>

							<tr>
								<td width="15%" class="Label"><label for="productId"
									class="mand">Title</label></td>
								<td width="36%"><form:errors path="couponName"
										cssStyle="color:red">
									</form:errors> <form:input path="couponName" type="text" name="imprsns"
										id="cpnNm" tabindex="1" maxlength="40" /></td>

								<td class="Label"><label for="couponAmt" class="mand">
										Banner Title</label></td>
								<td><form:errors path="bannerTitle" cssStyle="color:red">
									</form:errors> <form:input path="bannerTitle" id="bannerTitle" type="text"
										name="bannerTitle" maxlength="40" tabindex="2" /></td>
							</tr>
							<tr>

								<td class="Label"><label for="Clicks" class="mand">#
										of Deals to issue</label></td>
								<td><form:errors path="numOfCouponIssue"
										cssStyle="color:red"></form:errors> <form:input
										path="numOfCouponIssue" type="text" name="textfield"
										id="cpnAmt" onkeypress="return isNumberKey(event)"
										tabindex="3" maxlength="4" /></td>
								<td class="Label"><label for="slctLoc" class="mand">Location</label></td>
								<td>
									<div>
										<form:errors path="locationID" cssStyle="color:red">
										</form:errors>
										<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
											onclick="SelectAllLocation(this.checked);" tabindex="4" />
										Select All Locations </label>
									</div> <form:select path="locationID" id="locationID"
										class="txtAreaBox" multiple="true" tabindex="5">
										<!--<form:option value="" label="--Select Location--" />-->
										<c:forEach items="${sessionScope.retailerLocList}" var="s">
											<form:option value="${s.retailerLocationID}"
												label="${s.address1}" />
										</c:forEach>
									</form:select>
									<p>Hold ctrl to select more than one location</p>
								</td>
							</tr>
							<tr>
								<td class="Label"><label for="cpnDesc" class="mand">Description</label>
								</td>


								<td colspan="3"><form:errors cssClass="error"
										path="couponLongDesc" cssStyle="color:red" /> <form:textarea
										path="couponLongDesc" id="couponLongDesc" name="textarea"
										rows="5" cols="25" class="textareaTxt txtAreaLarge"
										maxlength="5000" tabindex="6" /></td>

							</tr>

							<tr>
								<td class="Label"><label for="cpnTC">Terms &amp;
										Conditions</label></td>
								<td><form:textarea path="couponTermsCondt" id="cpnTC"
										class="txtAreaSmall" rows="5" cols="45" name="textarea2"
										onkeyup="checkMaxLength(this,'50');" tabindex="7"></form:textarea></td>

								<td class="Label" align="left"><label for="keywords"></label>Keywords</td>
								<td align="left"><form:textarea id="keywords"
										class="txtAreaSmall" rows="5" cols="45" path="keyword"
										tabindex="8" cssStyle="height:60px;"
										onkeyup="checkMaxLength(this,'50');" /></td>

							</tr>
							<tr>
								<td class="Label"><label for="upldImg" class="mand">Deal
										List Image</label></td>
								<td>
									<ul class="imgInfoSplit">
										<li><label><img id="couponImg" alt="upload"
												src="${sessionScope.couponImagePath}" height="80" width="80"
												onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
										</label><span class="topPadding forceBlock"><label
												for="trgrUpld"> <input type="button"
													value="Choose File" id="trgrUpldBtn" width="350"
													height="460" class="btn trgrUpld" title="Choose File"
													tabindex="9"> <form:input path="imageFile"
														type="file" class="textboxBig" id="trgrUpld"
														onchange="checkCouponImgValidate(this);" /> <form:errors
														path="imageFile" cssStyle="color:red"></form:errors>
											</label><label id="couponImagePathErr"
												style="color: red; font-style: 45"></label> </span></li>
										<li>Suggested Minimum Size:<br>600px/300px<br>Maximum
											Size:800px/600px<br></li>
									</ul>
								</td>

								<td class="Label"><label for="upldImg" class="mand">
										Deal Image</label></td>
								<td colspan=""><form:errors path="detailImgPath"
										cssStyle="color:red"></form:errors>
									<ul class="imgInfoSplit">
										<li><label><img id="couponImg" alt="upload"
												src="${sessionScope.couponDetImagePath}" height="80"
												width="80"
												onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
										</label><span class="topPadding forceBlock"><label
												for="trgrUpldDetImg"> <input type="button"
													value="Choose File" id="trgrUpldBtn" width="350"
													height="460" class="btn trgrUpldDetImg" title="Choose File"
													tabindex="10"> <form:input path="detailImage"
														type="file" class="textboxBig" id="trgrUpldDetImg"
														onchange="checkDealImgValidate(this);" />
											</label> </span></li>
										<li align="center">Suggested Minimum Size:<br>600px/300px<br>Maximum
											Size:800px/600px<br></li>
										<li><form:errors path="detailImage" cssStyle="color:red"></form:errors>
											<label id="couponImageDetPathErr"
											style="color: red; font-style: 45"></label></li>
									</ul></td>


							</tr>

							<tr>
								<td class="Label"><label for="csd" class="mand">Start
										Date</label></td>

								<td align="left"><div class="coupdis">
										<form:errors path="couponStartDate" cssStyle="color:red"></form:errors>
									</div>
									<form:input path="couponStartDate" id="datepicker1"
										class="textboxDate" name="csd" tabindex="11" />(mm/dd/yyyy)</td>

								<td class="Label"><label for="cst">Start Time</label></td>
								<td><form:select path="couponStartTimeHrs"
										class="slctSmall" tabindex="12">
										<form:options items="${CouponStartHrs}" />
									</form:select> Hrs <form:select path="couponStartTimeMins" class="slctSmall"
										tabindex="13">
										<form:options items="${CouponStartMin}" />
									</form:select> Mins</td>
							</tr>
							<tr>
								<td class="Label"><label for="ced" class="">End
										Date</label></td>

								<td align="left"><div class="coupdis">
										<form:errors path="couponEndDate" cssStyle="color:red"></form:errors>
									</div>
									<form:input path="couponEndDate" id="datepicker2"
										class="textboxDate" name="ced" tabindex="14" />(mm/dd/yyyy)</td>

								<td class="Label"><label for="cet">End Time</label></td>
								<td><form:select path="couponEndTimeHrs" class="slctSmall"
										tabindex="15">
										<form:options items="${CouponStartHrs}" />
									</form:select> Hrs <form:select path="couponEndTimeMins" class="slctSmall"
										tabindex="16">
										<form:options items="${CouponStartMin}" />
									</form:select> Mins</td>

							</tr>
							<tr>
								<!-- <td class="Label"><label for="timeZone">Time Zone</label></td>
							<td><form:select path="timeZoneId" class="selecBx" tabindex="16">
											<form:option value="0" label="">Please Select Time Zone</form:option>
									<c:forEach items="${sessionScope.CoupontimeZoneslst}" var="tz">
										<form:option value="${tz.timeZoneId}" label="${tz.timeZoneName}" />
									</c:forEach>
								</form:select></td> -->


								<td class="Label"><label for="csd" class="">Expiration
										Date</label></td>

								<td align="left"><div class="coupdis">
										<form:errors path="couponExpireDate" cssStyle="color:red"></form:errors>
									</div>
									<form:input path="couponExpireDate" id="datepicker3"
										class="textboxDate" name="csd" tabindex="17" />(mm/dd/yyyy)</td>
								<td class="Label"><label for="cet">Expiration Time</label></td>
								<td><form:select path="couponExpTimeHrs" class="slctSmall"
										tabindex="18">
										<form:options items="${CouponStartHrs}" />
									</form:select> Hrs <form:select path="couponExpTimeMins" class="slctSmall"
										tabindex="19">
										<form:options items="${CouponStartMin}" />
									</form:select> Mins</td>

							</tr>


							<!--	<td><label for="couponName">Select Product</label>
								</td>
								<td><form:input path="scanCode" type="text"
										name="textfield" id="couponName" readonly="true" tabindex="18"/> <a href="#"><img
										src="/ScanSeeWeb/images/searchIcon.png" alt="Search"
										width="20" height="17"
										onclick="checkAssociatedCouponProd()"
										title="Click here to View Product/UPC List" /> </a>
								</td>-->



						</table>
					</div>
					<div class="navTabSec">

						<div align="right">
							<input name="Back" value="Back" type="button" class="btn"
								onclick="location='/ScanSeeWeb/retailer/managecoupons.htm'"
								title="Back" tabindex="20" /> <input name="Preview"
								value="Preview" type="button" class="btn" title="Preview"
								onclick="previewReRunCouponPopUp()" tabindex="21" /> <input
								name="Save" value="Submit" type="button"
								onclick="saveRerunCoupon();" class="btn" title="Save"
								tabindex="22" />
						</div>
					</div>
				</div>
			</div>
			<div class="ifrmPopupPannel" id="ifrmPopup2"
				style="display: none; background-color: White">
				<div class="headerIframe">
					<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
						alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup2','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="auto" id="ifrm2" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
			<div class="ifrmPopupPannelImage" id="ifrmPopup"
				style="display: none;">
				<div class="headerIframe">
					<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
						alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="no" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
		</form:form>
	</div>

	<script type="text/javascript">
		//var claimType = document.reruncouponform.couponDiscountTypehidden.value;
		//onClaimTypeLoad();
		//onLoadRetLoc();
		onLoadRetLocationID();
	</script>
	<script type="text/javascript">
		$('#trgrUpld')
				.bind(
						'change',
						function() {
							/*show progress bar : ETA for Web 1.3*/
							//showProgressBar();/* Commented to fix body scroll disable issue*/
							/*End*/
							$("#uploadBtn").val("trgrUpldBtn")
							var uploadImageType = $(this).attr('name');

							document.reruncouponform.uploadCoupImageType.value = uploadImageType;
							$.ajaxSetup({
								cache : false
							});
							$("#reruncouponform")
									.ajaxForm(
											{
												success : function(response) {
													$('#loading-image').css(
															"visibility",
															"hidden");
													var imgRes = response
															.getElementsByTagName('imageScr')[0].firstChild.nodeValue
													if (imgRes == 'UploadLogoMaxSize') {
														$('#couponImagePathErr')
																.text(
																		"Image Dimension should not exceed Width: 800px Height: 600px");
													} else if (imgRes == 'UploadLogoMinSize') {
														$('#couponImagePathErr')
																.text(
																		"Image Dimension should be Minimum Width: 70px Height: 70px");
													} else {
														$('#couponImagePathErr')
																.text("");
														var substr = imgRes
																.split('|');
														if (substr[0] == 'ValidImageDimention') {
															$('#couponImg')
																	.width(
																			'70px');
															$('#couponImg')
																	.height(
																			'70px');
															var imgName = substr[1];
															$('#createcoupon')
																	.val(
																			imgName);
															$('#couponImg')
																	.attr(
																			"src",
																			substr[2]);
														} else {
															/*commented to fix iframe popup scroll issue
															/$('body').css("overflow-y","hidden");*/
															openIframePopupForImage(
																	'ifrmPopup',
																	'ifrm',
																	'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																	100, 99.5,
																	'Crop Image');
														}
													}
												}
											}).submit();
						});

		$('#trgrUpldDetImg')
				.bind(
						'change',
						function() {
							//$("#reruncouponform").attr('action', '/ScanSeeWeb/retailer/uploadcouponimg.htm');
							/*show progress bar : ETA for Web 1.3*/
							//showProgressBar();/* Commented to fix body scroll disable issue*/
							/*End*/
							$("#uploadBtn").val("trgrUpldBtn")
							var uploadImageType = $(this).attr('name');

							document.reruncouponform.uploadCoupImageType.value = uploadImageType;
							$.ajaxSetup({
								cache : false
							});
							$("#reruncouponform")
									.ajaxForm(
											{
												success : function(response) {
													$('#loading-image').css(
															"visibility",
															"hidden");
													var imgRes = response
															.getElementsByTagName('imageScr')[0].firstChild.nodeValue
													if (imgRes == 'UploadLogoMaxSize') {
														$(
																'#couponImageDetPathErr')
																.text(
																		"Image Dimension should not exceed Width: 800px Height: 600px");
													} else if (imgRes == 'UploadLogoMinSize') {
														$(
																'#couponImageDetPathErr')
																.text(
																		"Image Dimension should be Minimum Width: 70px Height: 70px");
													} else {
														$(
																'#couponImageDetPathErr')
																.text("");
														var substr = imgRes
																.split('|');
														if (substr[0] == 'ValidImageDimention') {

															$('#couponImg')
																	.width(
																			'70px');
															$('#couponImg')
																	.height(
																			'70px');

															var imgName = substr[1];
															$('#createcoupon')
																	.val(
																			imgName);
															$('#couponImg')
																	.attr(
																			"src",
																			substr[2]);
														} else {

															/*commented to fix iframe popup scroll issue
															/$('body').css("overflow-y","hidden");*/
															openIframePopupForImage(
																	'ifrmPopup',
																	'ifrm',
																	'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																	100, 99.5,
																	'Crop Image');
														}
													}
												}
											}).submit();
						});
	</script>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.RetailerLocation"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script>
	saveManageLocationPage = 'true';
	var changeImgDim = '${sessionScope.ChangeImageDim}';
	if (null != changeImgDim && changeImgDim == 'true') {
		$('#upldLocatnImg').width('28px');
		$('#upldLocatnImg').height('28px');
	}
</script>

<script>					
function showModal(retLocId) {
	$("#retailLocationID").val(retLocId);
	$.ajax({
		cache: false,
        type : "GET",
        url : "displayBusinessHours.htm",
        data : {
        "locationId" : retLocId
        },
        success: function(response){
        		$(".alertBx").hide();
        		$('.modal-cont').html(response);
        	 	var bodyHt = $('body').height();
        	    var setHt = parseInt(bodyHt);
        	    $(".modal-popupWrp").show();
        	    $(".modal-popupWrp").height(setHt);
        	    $(".modal-popup").slideDown('fast');
        }
    }); 
}
/*close modal popup on click of x*/
function closeModal() {
    $(".modal-popupWrp").hide();
    $(".modal-popup").slideUp();
    $(".modal-popup").find(".modal-bdy input").val("");
    $(".modal-popup i").removeClass("errDsply");
    $(".overlay").hide();
}
</script>

<script type="text/javascript">

window.onload = function() {
	var vRetailerLocID = document.locationform.retailerLocID.value;
	var vRetLocList = [];
	var objArr = [];
	var vAllLocationList = [];
	if (vRetailerLocID != "null" && vRetailerLocID != "") {
		vRetLocList = vRetailerLocID.split(",");
		for(i=0; i<vRetLocList.length ; i++){
		   $("tr#" + vRetLocList[i]).removeClass("hilite").addClass("requiredVal");	
		}
	}
}

function isLatLong(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	//if ((charCode == 67 || charCode == 17 || charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31 ||charCode == 45 || charCode == 43 || charCode == 86)
	//	return true;
	//return false;
	}

	function numRows() {
		var rowCount = $('#locSetUpGrd tr').length;
		if (rowCount <= 1) {
			alert("No records to save");
			return false;
		}
			var result = confirm("Do you want to save the changes");
			if (result) {
				editManageLocation();
			}
	}
	function navigtoproductSetup() {
		document.locationform.action = "retailerchoosePlan.htm";
		document.locationform.method = "GET";
		document.locationform.submit();
	}
	function navigtoPlanSetup() {
		document.locationform.action = "retailerchoosePlan.htm";
		document.locationform.method = "GET";
		document.locationform.submit();
	}
	
	function saveBusinessHours() {	
		$(".alertBx").hide();
		var timeZone = $("select[name='storeTimeZoneId'] option:selected").text();
		var storeStartHrs = null;
		var storeStartMins = null;
		var storeEndHrs = null;
		var storeEndMins = null;
		var retLocId = $("#retailLocationID").val();
		
		$('select[name^="storeStartHrs"]').each(function() {
			if(storeStartHrs == null) {
				storeStartHrs = $(this).val() +",";
			} else {
				storeStartHrs += $(this).val() +",";
			}
		});
		$('select[name^="storeStartMins"]').each(function() {
			if(storeStartMins == null) {
				storeStartMins = $(this).val() +",";
			} else {
				storeStartMins += $(this).val() +",";
			}
		});
		$('select[name^="storeEndHrs"]').each(function() {
			if(storeEndHrs == null) {
				storeEndHrs = $(this).val() +",";
			} else {
				storeEndHrs += $(this).val() +",";
			}
		});
		$('select[name^="storeEndMins"]').each(function() {
			if(storeEndMins == null) {
				storeEndMins = $(this).val() +",";
			} else {
				storeEndMins += $(this).val() +",";
			}
		});
		
		$.ajax({
			cache: false,
	        type : "GET",
	        url : "saveBusinessHours.htm",
	        data : {
	       "storeStartHrs" : storeStartHrs,
	        "storeStartMins" : storeStartMins,
	       "storeEndHrs" : storeEndHrs,
	        "storeEndMins" : storeEndMins,
	        "timeZone" : timeZone,
	        "retailerLocationID" : retLocId
	        },
	        success: function(response){
	        	if(response == "SUCCESS"){
	        		 $(".success").show();
	         	} else {
	        		$(".failure").show();
	        	}	 
	        } 
	    });
		}
	function populateHttp(obj) {
		var value = $(obj).val();
		alert(value)
	}
</script>
<script>
$(function() {
	$('#storeTimeZoneId').change(function() {
	   $('#timeZoneHidden').val($("#storeTimeZoneId option:selected").text());
	   
	    });
	});
</script>
<form:form name="locationform" commandName="locationform"
	action="/ScanSeeWeb/retailer/managelocationimg.htm"
	acceptCharset="ISO-8859-1" enctype="multipart/form-data">
	<div id="bubble_tooltip">
		<div class="bubble_top">
			<span></span>
		</div>
		<div class="bubble_middle">
			<span id="bubble_tooltip_content"></span>
		</div>
		<div class="bubble_bottom"></div>
	</div>
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@ include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">
						<img width="36" height="36" alt="addloc"
							src="../images/addLocIcon.png"> Update Locations
					</h1>
				</div>
				<form:hidden path="retailID" value="${item.retailID}" />
				<form:hidden path="uploadRetaillocationsID"
					value="${item.uploadRetaillocationsID}" />
				<form:hidden path="prodJson" />
				<form:hidden path="pageNumber" />
				<form:hidden path="pageFlag" />
				<input type="hidden" name="dashBoardFlag" id="dashBoardFlag"
					value="fromDashBoard" />
				<form:hidden path="retailerLocID" id="retailerLocID"
					value="${requestScope.rejectedLocations}" />
				<form:hidden path="totalSize" />
				<form:hidden path="currentPage" />
				<form:hidden path="rowIndex" />
				<form:hidden path="viewName" value="manageLocationDash" />
				<form:hidden path="prodJson1" />
				<form:hidden path="timeZoneHidden" id="timeZoneHidden" />
				<!--<input type="hidden" name="dashBoardSearch" id="dashBoardSearch" value="fromDashBoardSearch"/>-->
				<div class="clear"></div>
				<div class="grdSec brdrTop">


					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<tr>
							<td colspan="2" class="">Add a single location at a time by
								selecting <strong>'Input'</strong>, add multiple locations by
								selecting <strong>'Upload'</strong> or select a location below
								to create a <strong>QR code</strong> or edit location
								information.
								<div class="MrgnTop btmPadding">
									<input type="button" value="Input" class="btn" title="Input"
										onclick="window.location.href='/ScanSeeWeb/retailer/addlocation_dashboard.htm'" />
									<input type="button" value="Upload" class="btn" title="Upload"
										onclick="window.location.href='/ScanSeeWeb/retailer/location_dashboardsetup.htm'" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="Label" width="18%">Store Identification</td>
							<td><form:input path="searchKey" type="text"
									name="searchKey" /> <!-- IE JS Error "Object Expected" <a onclick="fetchBatchLocationSetup();" >-->
								<img class=imgLinks src="/ScanSeeWeb/images/searchIcon.png"
								alt="Search" title="Search" width="20" height="17"
								onclick="fetchBatchLocationSetup();" /> <c:if
									test="${requestScope.errorMSG ne null && !empty requestScope.errorMSG}">
									<font color="red"> <c:out
											value="${requestScope.errorMSG}" />
									</font>
								</c:if>
								<div class="sub-actn" style="margin-left: 42% !important;">
									<a href="/ScanSeeWeb/retailer/dashboardlocinstructions.htm">View
										Instructions</a>
								</div></td>
						</tr>
					</table>
					<div align="center" style="font-style: 90">
						<label style="${requestScope.manageLocation}"><c:out
								value="${requestScope.successMSG}" /> </label>
					</div>

					<c:choose>
						<c:when
							test="${requestScope.rejectedLocations ne null && requestScope.rejectedLocations ne ''}">

							<div class="searchGrd scrollingDashboard brdrTop">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="tblEffect" id="locSetUpGrd">
									<tr class="header">
										<td width="10%" align="center"><label>QR Code</label></td>
										<td width="21%"><p>
												<a href="#" class="hdr-white"
													onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
													onmouseout="hideToolTip()">Store</a>
											</p>
											<p>
												<a href="#" class="hdr-white"
													onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
													onmouseout="hideToolTip()">Identification</a><a href="#"
													onmousemove="showToolTip(event,'Store Identification can either be your store number or other unique identifier.');return false"
													onmouseout="hideToolTip()"><label class="mand"><img
														alt="helpIcon" src="../images/helpIcon.png" /> </label> </a>
											</p> <form:errors cssStyle="color:red" path="storeIdentification"></form:errors>
										</td>
										<td width="18%"><label class="mand">Store Address</label></td>
										<td width="17%"><label class="mand">Latitude</label></td>
										<td width="17%"><label class="mand">Longitude</label></td>
										<td width="17%"><label class="mand">City</label></td>
										<td width="17%"><label class="mand">State</label></td>
										<td width="10%"><label class="mand">Postal Code</label></td>
										<td width="17%"><label class="mand">Phone Number</label></td>
										<td width="17%"><label>Website URL</label></td>
										<td width="17%"><label>Keywords</label></td>
										<td width="17%"><label>Image Upload</label></td>
										<td width="17%">Business Hours</td>

									</tr>
									<c:set var="rowIndex" value="0"></c:set>
									<c:forEach items="${requestScope.locResult.locationList}"
										var="item">
										<tr id="${item.retailLocationID}">
											<td align="center"><a href="#"
												onclick="openShowRetlrLocQrPopUp(${item.retailLocationID})"><img
													src="/ScanSeeWeb/images/QR-icon.png" alt="QR-Code"
													width="18" height="18" /> </a></td>
											<td><form:hidden path="uploadRetaillocationsID"
													value="${item.uploadRetaillocationsID}" /> <form:hidden
													path="retailLocationID" value="${item.retailLocationID}" />

												<form:input path="storeIdentification"
													cssClass="textboxSmall" value="${item.storeIdentification}" />
											</td>

											<td><form:input path="address1"
													cssClass="textboxAddress" maxlength="150"
													value="${item.address1}" /></td>
											<td><form:input path="retailerLocationLatitude"
													cssClass="textboxSmall latChk"
													value="${item.retailerLocationLatitude}" id="locLatitude" /></td>
											<td><form:input path="retailerLocationLongitude"
													cssClass="textboxSmall lonChk"
													value="${item.retailerLocationLongitude}" id="locLongitude" /></td>
											<td><form:input path="city" cssClass="textboxSmall"
													maxlength="50" value="${item.city}" /></td>
											<td><form:input path="state" cssClass="textboxSmaller"
													maxlength="2" value="${item.state}" onpaste="return false"
													ondrop="return false" ondrag="return false"
													oncopy="return false" /></td>
											<td><form:input path="postalCode"
													cssClass="textboxSmaller" maxlength="10"
													value="${item.postalCode}"
													onkeypress="return isNumberKey(event)" /></td>

											<td><form:input path="phonenumber"
													cssClass="textboxSmaller" value="${item.phonenumber}"
													maxlength="10" onkeypress="return isNumberKey(event)" /></td>
											<td><form:input path="retailLocationUrl" maxlength="75"
													cssClass="textboxSmall" value="${item.retailLocationUrl}"
													onkeydown="populateHttp('this')" /></td>
											<td><form:input path="keyword" cssClass="textboxSmall"
													value="${item.keyword}" /></td>
											<td>
												<div class="img-row">
													<!--  <span class="col"><img width="30" height="30" title="click to edit" alt="image" src="images/dfltImg.png" class="img-preview"></span>-->
													<span class="col"><img width="30" height="30"
														alt="image"
														name="logoPreview+${item.uploadRetaillocationsID}"
														src="${item.imgLocationPath}" class="img-preview"> <form:hidden
															path="gridImgLocationPath"
															value="${item.gridImgLocationPath}" /> <form:hidden
															path="imgLocationPath" value="${item.imgLocationPath}" />
														<form:hidden path="uploadImage"
															value="${item.uploadImage}" /> <span class="col"><input
															type="file" class="textboxBig"
															id="img${item.uploadRetaillocationsID}" name="imageFile"
															onChange="manageLocationImgValidate(this, ${rowIndex });"></span>
												</div>
											</td>
											<td>
										<input type="button" class="btn" value="Edit Business Hours" onclick="showModal('${item.retailLocationID}');"/>

										</td>

										</tr>
	
										<c:set var="rowIndex" value="${rowIndex+1}"></c:set>
									</c:forEach>
								</table>
							</div>
						</c:when>
						<c:otherwise>
							<div class="searchGrd scrollingDashboard brdrTop">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="tblEffect" id="locSetUpGrd">
									<tr class="header">
										<td width="10%" align="center"><label>QR Code</label></td>
										<td width="21%"><p>
												<a href="#" class="hdr-white"
													onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
													onmouseout="hideToolTip()">Store</a>
											</p>
											<p>
												<a href="#" class="hdr-white"
													onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
													onmouseout="hideToolTip()">Identification</a><a href="#"
													onmousemove="showToolTip(event,'Store Identification can either be your store number or other unique identifier.');return false"
													onmouseout="hideToolTip()"><label class="mand"><img
														alt="helpIcon" src="../images/helpIcon.png" /> </label> </a>
											</p> <form:errors cssStyle="color:red" path="storeIdentification"></form:errors>
										</td>
										<td width="18%"><label class="mand">Store Address</label>
										</td>
										<td width="17%"><label class="mand">Latitude</label></td>
										<td width="17%"><label class="mand">Longitude</label></td>
										<td width="17%"><label class="mand">City</label></td>
										<td width="17%"><label class="mand">State</label></td>
										<td width="10%"><label class="mand">Postal Code</label></td>
										<td width="17%"><label class="mand">Phone Number</label>
										</td>
										<td width="17%"><label>Website URL</label></td>
										<td width="17%"><label>Keywords</label></td>
										<td width="17%">Image Upload</td>
										<td width="17%">Business Hours</td>
										
									</tr>
									<c:set var="rowIndex" value="0"></c:set>
									<c:forEach items="${requestScope.locResult.locationList}"
										var="item">
										<tr id="${item.retailLocationID}">
											<td align="center"><a href="#"
												onclick="openShowRetlrLocQrPopUp(${item.retailLocationID})"><img
													src="/ScanSeeWeb/images/QR-icon.png" alt="QR-Code"
													width="18" height="18" /> </a></td>
											<td><form:hidden path="uploadRetaillocationsID"
													value="${item.uploadRetaillocationsID}" /> <form:hidden
													path="retailLocationID" value="${item.retailLocationID}" />

												<form:input path="storeIdentification"
													cssClass="textboxSmall" value="${item.storeIdentification}" />
											</td>
											<td><form:input path="address1"
													cssClass="textboxAddress" maxlength="150"
													value="${item.address1}" /></td>
											<td><form:input path="retailerLocationLatitude"
													cssClass="textboxSmall latChk"
													value="${item.retailerLocationLatitude}" id="locLatitude" /></td>
											<td><form:input path="retailerLocationLongitude"
													cssClass="textboxSmall lonChk"
													value="${item.retailerLocationLongitude}" id="locLongitude" /></td>
											<td><form:input path="city" cssClass="textboxSmall"
													maxlength="50" value="${item.city}" /></td>
											<td><form:input path="state" cssClass="textboxSmaller"
													maxlength="2" value="${item.state}" onpaste="return false"
													ondrop="return false" ondrag="return false"
													oncopy="return false" /></td>
											<td><form:input path="postalCode"
													cssClass="textboxSmaller" maxlength="10"
													value="${item.postalCode}"
													onkeypress="return isNumberKey(event)" /></td>
											<td><form:input path="phonenumber"
													cssClass="textboxSmaller" value="${item.phonenumber}"
													maxlength="10" onkeypress="return isNumberKey(event)" /></td>
											<td><form:input path="retailLocationUrl" maxlength="75"
													cssClass="textboxSmall" value="${item.retailLocationUrl}" />
											</td>
											<td><form:input path="keyword" cssClass="textboxSmall"
													value="${item.keyword}" /></td>
											<td>
												<div class="img-row">
													<!--  <span class="col"><img width="30" height="30" title="click to edit" alt="image" src="images/dfltImg.png" class="img-preview"></span>-->
													<form:hidden path="gridImgLocationPath"
														value="${item.gridImgLocationPath}" />
													<form:hidden path="imgLocationPath"
														value="${item.imgLocationPath}" />
													<form:hidden path="uploadImage" value="${item.uploadImage}" />
													<span class="col"><img width="30" height="30"
														alt="image"
														name="logoPreview+${item.uploadRetaillocationsID}"
														src="${item.imgLocationPath}" class="img-preview"></span> <span
														class="col"><input type="file" class="textboxBig"
														id="img${item.uploadRetaillocationsID}" name="imageFile"
														onChange="manageLocationImgValidate(this, ${rowIndex });"></span>
												</div>
											</td>
											
											<td>
												<input type="button" class="btn" value="Edit Business Hours" onclick="showModal('${item.retailLocationID}');"/>

										</td>
										</tr>
										<c:set var="rowIndex" value="${rowIndex+1}"></c:set>
									</c:forEach>
								</table>

							</div>
						</c:otherwise>
					</c:choose>
					<div class="pagination">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="noBrdr" id="perpage">
							<tr>
								<page:pageTag
									currentPage="${sessionScope.pagination.currentPage}"
									nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
									pageRange="${sessionScope.pagination.pageRange}"
									url="${sessionScope.pagination.url}" enablePerPage="false" />
							</tr>
						</table>
					</div>
				</div>
				<div class="navTabSec RtMrgn LtMrgn">
					<div align="right">
						<input class="btn" onclick="numRows();" type="button"
							value="Submit" name="Cancel" title="Submit" />
					</div>
				</div>

				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>

	<div class="modal-popupWrp">
		<div class="modal-popup">
			<div class="modal-hdr">
				Setup Business Hours<a class="modal-close" title="close"
					onclick="closeModal()"><img src="../images/popupClose.png" /></a>
			</div>
			<div class="modal-bdy">
				<div class="alertBx failure">
					<p class="msgBx">
						<c:out value="Please select Time Zone" />
					</p>
				</div>
				<div class="alertBx success">
					<p class="msgBx">
						<c:out value="Business Hours saved Successfully" />
					</p>
				</div>
				<div class="modal-cont brdr"></div>
			</div>
			<div class="modal-ftr">
				<p align="right">

					<!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
					<input name="Save" value="Save" type="button" class="btn" id="save"
						onclick="saveBusinessHours();" />
				</p>
			</div>
		</div>
	</div>

</form:form>

<div class="ifrmPopupPannel" id="ifrmPopup"
	style="display: none; background-color: White">
	<div class="headerIframe">
		<img src="../images/popupClose.png" class="closeIframe" alt="close"
			onclick="javascript:closeIframePopup('ifrmPopup','ifrm');"
			title="Click here to Close" align="middle" /> <span id="popupHeader"></span>
	</div>
	<iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%"
		allowtransparency="yes" width="100%" style="background-color: White">
	</iframe>
</div>

<div class="ifrmPopupPannelImage" id="ifrmPopup2"
	style="display: none; background-color: White">
	<div class="headerIframe">
		<img src="../images/popupClose.png" class="closeIframe" alt="close"
			onclick="javascript:closeIframePopup('ifrmPopup2','ifrm2');"
			title="Click here to Close" align="middle" /> <span id="popupHeader"></span>
	</div>
	<iframe frameborder="0" scrolling="auto" id="ifrm2" src=""
		height="100%" allowtransparency="yes" width="100%"
		style="background-color: White"> </iframe>
</div>
<div class="clear"></div>

<script>
$('.latChk').bind('blur',function() {
/* Matches	 90.0,-90.9,1.0,-23.343342
Non-Matches	 90, 91.0, -945.0,-90.3422309*/
	var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
			  var vLat = this.value;
			  //Validate for Latitude.
			  if (0 === vLat.length || !vLat || vLat == "") {
				return false;
			 } else {
				if(!vLatLngVal.test(vLat)) {
			      alert("Latitude are not correctly typed");
			      this.value = '';
			      return false;
			  }
			 }
});

$('.lonChk').bind('blur',function() {
	/* Matches	180.0, -180.0, 98.092391
	Non-Matches	181, 180, -98.0923913*/
	//alert("lonChk");
	var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
		//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
			// var vLatLngVal = /^-?([1]?[0-7][1-9]|[1-9]?[0-9]|[1][0][0-9])?(\.\d*)?)|-?180(\.[0]*)?/;
				  var vLong = this.value;
				  //Validate for Longitude.
				  if (0 === vLong.length || !vLong || vLong == "") {
					return false;
				 } else {
					if(!vLatLngVal.test(vLong)) {
				      alert("Longitude are not correctly typed");
				      this.value = '';
				      return false;
				  }
				 }
	});

</script>

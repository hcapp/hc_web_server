<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<div id="wrapper">

	<form:form name="buildAnythingPage" commandName="buildAnythingPage">

		<div id="content" class="shdwBg">
			<%@ include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">
						Build Anything Page <sup class="smallsup">TM</sup>
					</h1>
					<div class="sub-actn sub-link">
						<a onclick="location='/ScanSeeWeb/retailer/addanythingpageinstructions.htm?atype=alnkpage'" href="#">View Instructions</a>
					</div>
				</div>
				<div class="section">
					<div class="grdSec brdrTop">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="grdTbl">

							<tr>
								<td colspan="4"><ul class="imgtxt">
										<li class="floatL"><img
											src="../images/buildAnythingPgs.png" alt="buildAnythingPgs"
											width="96" height="96" /></li>
										<li>
											<h3>
												Anything Pages <sup class="smallsup">TM</sup>
											</h3>
										</li>
									</ul>
									<p>Please choose the type of page you would like to
										create:&#13;</p></td>
							</tr>


							<tr>
								<td width="5%" class="Label"><input type="radio"
									name="pageType" id="existingWebPg" value="linktoExstngPg.htm" />
								</td>
								<td align="left"><label for="existingWebPg">Link to an Existing Web or Social Page</label></td>
							</tr>
							<tr>
								<td align="left" class="Label"><input type="radio"
									name="pageType" id="upldPdf" value="uploadpdf.htm" />
								</td>
								<td align="left"><label for="upldPdf">Upload Your
										Own Image, PDF or HTML File</label>
								</td>
							</tr>
							<tr>
								<td width="5%" class="Label"><input type="radio"
									name="pageType" id="makeYourOwnPg" value="cstmMainPg.htm" />
								</td>
								<td align="left"><label for="makeYourOwnPg">Custom Anything Page <sup class="smallsup">TM</sup></label>
								</td>
							</tr>
		
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</form:form>


</div>

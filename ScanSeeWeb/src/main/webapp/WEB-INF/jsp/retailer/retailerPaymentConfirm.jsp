<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>HubCiti</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="scripts/global.js" type="text/javascript"></script>
</head>
<body>
<div id="wrapper">
<div id="header">
  <!--<div id="header_link" class="floatR">
    <ul>
      <li> <img alt="User" align="left" src="images/icon_user.png"/> Welcome <span>Stephanie Blackwell</span> </li>
      <li>Aug 10, 2011 </li>
      <li> <img alt="Logut" align="left" src="images/logoutIcon.gif"/> <a href="index.html">Logout</a> </li>
    </ul>
  </div>-->
  <div id="header_logo" class="floatL"> <a href="index.html"> <img alt="ScanSee" src="images/hubciti-logo_ret.jpg"/></a> </div>
  <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="grdSec">
  <div style="WIDTH: 300px" id="content" class="floatL"> <br />
    <br />
    Confirm Payment Receipt <br />
    <br />
    Your payment has been authorized. A receipt will be sent to the email provided. <br />
    <br />
    Please print the following information for your records: <br />
    <br />
    Amount: $1200.00 <br />
    <br />
    Contact Information: <br />
    Name: James Johnson <br />
    Phone: 972-222-3456 <br />
    <br />
    Payment Information: <br />
    Card: VISA - ************2525 <br />
    <br />
    <img src="images/print_icon.jpg" width="15" height="15"> Print </div>
  <div class="clear"></div>
  <div class="navTabSec">
    <div align="right">
      <!--<input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Cancel3"/> <input class="btn" onclick="window.location.href='Manufacturer/adminpage.html'" value="Preview" type="button" name="Cancel"/> <input class="btn" onclick="window.location.href='Manufacturer/adminpage.html'" value="Submit" type="button" name="Cancel"/>-->
    </div>
  </div>
</div>
    <div class="clear"></div>
<div id="footer">
  <div id="ourpartners_info">
    <div id="followus_section" class="floatL">
      <div class="section_title">Connect with us!</div>
  <img border="0" alt="followus" src="images/followus-img.png" usemap="#Map2"/>
        <map id="Map2" name="Map2">
          <area title="Flickr" href="Retailer/Retailer_abtscansee.html#" alt="Flickr"/>
          <area title="RSS" href="Retailer/Retailer_abtscansee.html#" alt="RSS"/>
          <area title="Email" href="Retailer/Retailer_abtscansee.html#" alt="Email"/>
          <area title="YouTube" href="Retailer/Retailer_abtscansee.html#" alt="YouTube"/>
          <area title="Twitter" href="Retailer/Retailer_abtscansee.html#" alt="Twitter"/>
          <area title="Facebook" href="Retailer/Retailer_abtscansee.html#" alt="Facebook"/>
        </map>
 
    </div>
    <div class="clear"></div>
  </div>
  <!--<div id="footer_nav">
    <ul>
      <li> <a href="#">About Us</a> </li>
      <li> <a href="#">Terms of Use</a> </li>
      <li> <a href="#">Privacy Policy</a> </li>
      <li> <a href="#">Contact Us</a> </li>
      <li> <a href="#">FAQ's</a> </li>
      <li> <a href="#">Help</a> </li>
      <li class="last"> <a href="#">Employment Opportunities</a> </li>
      <li class="clear"></li>
    </ul>
    <div class="clear"></div>
    <div id="footer_info">Copyright &copy; 2015 HubCiti. All rights reserved
      
    </div>
    <p align="center"></p>
  </div>-->
</div>
</body>
</html>

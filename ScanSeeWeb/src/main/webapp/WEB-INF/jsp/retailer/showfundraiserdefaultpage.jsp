<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="content">
	<%@include file="retailerLeftNavigation.jsp"%>
	
	    <div class="rtContPnl floatR">
      <div class="grpTitles">
        <h1 class="mainTitle">Fundraisers</h1>
      </div>
      <div class="textSec brdrTop">
        <p>Want to make people aware of giving opportunities you care about?</p>
        <!-- <h4 class="MrgnTop">Step by Step</h4>-->
        <p class="MrgnTop"> Link to and promote fundraising opportunities at your location!</p>
        <div class="MrgnTop">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
            <tr>
              <td width="25%" align="center"><img src="../images/iphone_fundraisers.png" alt="School Fundraisers" width="176" height="344" /></td>
              <td width="25%" align="center"><img src="../images/iphone_fundraisers2.png" alt="Church Fundraisers" width="176" height="344" /></td>
              <td width="25%" align="center"><img src="../images/iphone_fundraisers3.png" alt="Non-profit Fundraisers" width="176" height="344" /></td>
            </tr>
            <tr>
              <td align="center" valign="top">School Fundraisers</td>
              <td align="center" valign="top">Church Fundraisers</td>
              <td align="center" valign="top">Non-profit Fundraisers</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td align="center"><input type="button" value="Add Fundraiser" class="btn MrgnTop" onclick="window.location.href='addFundraiser.htm'"/></td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </div>

      </div>
    </div>
	
	
	
	
	
	
<div class="clear"></div>
</div>
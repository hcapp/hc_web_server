<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet" type="text/css">
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.form.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/cityAutocomplete.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/zipcodeAutocomplete.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<script src="/ScanSeeWeb/ckeditor/adapters/jquery.js"></script>



<script type="text/javascript">

$(document).ready(function(e) {
    initVideo();
});

$(document).ready(function(){			
//Table scroll with fixed header. Check if that element exists than set the properties.
	if($.fn.tableScroll)		{	
		if(document.getElementById('thetable')){
		
			$('#thetable').tableScroll({height:200});//Height for customization
		}

	}
	});
</script>


<body class="whiteBG overflow-hidden">
<div class="noBg">
  <ul class="cstm-tabs">
    <li><a href="#" class="active" name="create" title="Create">Create</a></li>
    <li><a href="#" name="newFeature" title="New Features">New Features</a></li>
    <li><a href="#" name="gettingStarted" title="Getting Started" >Getting Started</a></li>
    <li><a href="#" name="tipstechnique" title="Tips & Techniques">Tips & Techniques</a></li>
  </ul>
  <!-- "Video For Everybody" http://camendesign.com/code/video_for_everybody -->
  <video controls="controls" id="videopnl"  width="620" height="340">
    <source src="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" type="video/mp4" />
    </video>

	
	
	  <div class="carousel">
    <div class="controls"><a href="#" class="left"></a> <a href="#" class="right"></a></div>
    <div class="view">
	
		  
	  <c:if test="${sessionScope.createtutoriallst ne null}">
	   	<ul class="video-thumbnail" id="create">
	  <c:forEach items="${sessionScope.createtutoriallst}" var="tutorial">
	 
	   <c:if test="${tutorial.tutorialType eq 'Create' }">
	
	  	   <li id="${tutorial.tutorialId}" class="shadow"><img src="${tutorial.poster}" 
			width="122" height="80"/ videosrc="${tutorial.videoPath}" postersrc="${tutorial.poster}"></li>
      
	  </c:if>
	  </c:forEach>
	     </ul>
		 </c:if>
		 
		 
		   <c:if test="${sessionScope.getstrttutoriallst ne null}">
	  <ul class="video-thumbnail" id="gettingStarted">
	  <c:forEach items="${sessionScope.getstrttutoriallst}" var="tutorial">
	 
	   <c:if test="${tutorial.tutorialType eq 'GettingStarted' }">
	
	  	   <li id="${tutorial.tutorialId}" class="shadow"><img src="${tutorial.poster}" 
			width="122" height="80"/ videosrc="${tutorial.videoPath}" postersrc="${tutorial.poster}"></li>
      
	  </c:if>
	  </c:forEach>
	     </ul>
		 </c:if>
		 
		 		   <c:if test="${sessionScope.newfeattutoriallst ne null}">
	     <ul class="video-thumbnail" id="newFeature">
	  <c:forEach items="${sessionScope.newfeattutoriallst}" var="tutorial">
	 
	   <c:if test="${tutorial.tutorialType eq 'NewFeatures' }">
	
	  	   <li id="${tutorial.tutorialId}" class="shadow"><img src="${tutorial.poster}" 
			width="122" height="80"/ videosrc="${tutorial.videoPath}" postersrc="${tutorial.poster}"></li>
      
	  </c:if>
	  </c:forEach>
	     </ul>
		 </c:if>
		 
		   <c:if test="${sessionScope.tiptechtutoriallst ne null}">
	      <ul class="video-thumbnail" id="tipstechnique">
	  <c:forEach items="${sessionScope.tiptechtutoriallst}" var="tutorial">
	 
	   <c:if test="${tutorial.tutorialType eq 'TipsTechniques' }">
	
	  	   <li id="${tutorial.tutorialId}" class="shadow"><img src="${tutorial.poster}" 
			width="122" height="80"/ videosrc="${tutorial.videoPath}" postersrc="${tutorial.poster}"></li>
      
	  </c:if>
	  </c:forEach>
	     </ul>
		 </c:if>
		 
	  </div>
  </div>
  
</div>
	

<script type="text/javascript">
$(window).scroll(function() {
		var scrlPos = $(this).scrollTop();
});
$(".controls").bind('click',function(e) {
	var time = (new Date).getTime();
	
	var curCntrl = e.target;
	var curNav = $(curCntrl).attr("class");
	var getCtgry = $(".cstm-tabs").find('a.active').attr('name');
	var ele = $("#"+getCtgry);
	var eleCnt = $(ele).find("li").length;
	var start = $(ele).css("margin-left");
	var contnrWidth = $(".view").width() - 11;
	var diff = parseInt(start, 10);
	var $leftPos = $(ele).find("li").position();
	if(curNav === "right" && eleCnt > 4 && contnrWidth >= Math.abs(diff)) {
		console.log(time);
		 $(ele).animate({"margin-left":"-=154px"});
	} 
	if(curNav === "left" && $leftPos.left < 0) {  
		$(ele).animate({"margin-left":"+=154px"});
	}
});

$(".cstm-tabs").bind('click','a', function(e) {
	var prntEle = $(this).attr("class");
	var curTab = e.target;
	var ctgry =   $(curTab).attr('name');
	var isAnchor = curTab.nodeName.toLowerCase() === 'a';
	if(isAnchor) {
		$("."+prntEle).find('li a').removeClass("active");
		$(curTab).addClass("active");
		$('.video-thumbnail').hide();
		$("#"+ctgry).fadeIn();
	}
	toggleControls();
});

$(".video-thumbnail").bind('click',function(e){
  if(e.target.nodeName.toLowerCase() === 'img'){
    var ele = e.target;
    var videosrc = $(ele).attr("videosrc");
	var postersrc = $(ele).attr("postersrc");
	var video = document.querySelectorAll("#videopnl");
    if(videosrc) {
      $(video).find('source').attr("src",videosrc);
	//  $("#videopnl").attr("poster",postersrc);
       $(video).load();
	   $(video)[0].play();
    }
  }
});

function toggleControls(){
	var thumbnail = $(".video-thumbnail:visible").attr("id");
	var videoCnt = $("#"+thumbnail).find('li').length;
	var video = document.querySelectorAll("#videopnl");
	var firstSrc = $("#"+thumbnail).find('li:first img').attr("videosrc");
	$(video).find('source').attr("src",firstSrc);
	$(video).load();
	videoCnt < 4 ? $(".controls").hide() : $(".controls").show();
}

function initVideo() {
	$("#create").fadeIn();
}
    </script>
	</body>

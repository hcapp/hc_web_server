
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="common.pojo.ProductVO"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script src="scripts/web.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<script type="text/javascript">

function retRegProdSearch()
{
	var option=$('input:radio[name=searcAdd]:checked').val();
	if(option=='searcAdd')
		{
			regRetProdSearch();
		}else if(option=='manageProd'){
			regRetLocProductsSearch();
		}
}

 function deselectLoc(){
 $("#retLocID option:selected").removeAttr("selected");
 location.href='/ScanSeeWeb/retailer/regaddseachprod.htm'
 }

 function onLoadRetailerID()
 {
	 var vRetLocID = document.retprodsetupform.retailLocIDHidden.value;
		var vRetLocIDVal = document.getElementById("retLocID");
		if (vRetLocID != "null") {
			var vRetLocIDList = vRetLocID.split(",");
		}
		for ( var i = 0; i < vRetLocIDVal.length; i++) {
			for (j = 0; j < vRetLocIDList.length; j++) {
				if (vRetLocIDVal.options[i].value == vRetLocIDList[j]) {
					vRetLocIDVal.options[i].selected = true;
					break;
				}
			}
		}
	}

</script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
$(document).ready(function() {

	if($('#prodinfotbl tr').length <= 3) { $('.hrzscrolllarge').css('height','auto'); }
<c:forEach items="${sessionScope.seacrhList.productList}"
									var="item">
		$("#startdate"+${item.rowNumber}).datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar' ,buttonImage: '../images/calendarIcon.png' });
$("#enddate"+${item.rowNumber}).datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar',buttonImage: '../images/calendarIcon.png' });						
	</c:forEach>	
});	

</script>
<div id="wrapper">
	<div id="dockPanel">
		<ul id="prgMtr" class="tabs">
			<li><a href="https://www.scansee.net/links/aboutus.html"
				title="About HubCiti" target="_blank" rel="About HubCiti">About
					HubCiti</a>
			</li>
			<!--<li> <a title="Create Profile" href="Retailer/Retailer_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->

			<li><a title="Upload Ads/Logos" href="uploadRetailerLogo.htm"
				rel="Upload Ads/Logos">Upload Logo</a>
			</li>
			<li><a title="Location Setup" href="addlocation.htm"
				rel="Location Setup">Location Setup</a>
			</li>
			<li><a class="tabActive" title="Product Setup"
				href="/ScanSeeWeb/retailer/regbatchuploadretprod.htm"
				rel="Product Setup">Product Setup</a>
			</li>
			<li><a title="Choose Plan"
				href="/ScanSeeWeb/retailer/retailerchoosePlan.htm" rel="Choose Plan">Choose
					Plan</a>
			</li>
			<li>			
				<c:choose>
					<c:when test="${isPaymentDone == null}">
						<a title="Dashboard" href="/ScanSeeWeb/retailer/retailerhome.htm" rel="Dashboard">Dashboard</a>
					</c:when>
					<c:otherwise>
						<a title="Dashboard" href="#" rel="Dashboard">Dashboard</a>
					</c:otherwise>
				</c:choose>	
			</li>
		</ul>
		<a id="Mid" name="Mid"></a>
		<div class="floatR tglSec" id="tabdPanelDesc">
			<div id="filledGlass">
				<img src="/ScanSeeWeb/images/Step3.png" />
				<div id="nextNav">
					<a href="#"
						onclick="location.href='/ScanSeeWeb/retailer/retailerchoosePlan.htm'">
						<img class="NextNav_R" alt="Next"
						src="/ScanSeeWeb/images/nextBtn.png" /> <span>Next</span> </a>
				</div>

			</div>
		</div>
		<div class="floatL tglSec" id="tabdPanel">
			<img alt="Flow1" src="/ScanSeeWeb/images/Flow6_ret.png" />
		</div>
	</div>
	<!-- <div id="togglePnl">
		<a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down"
			width="9" height="8" /> Show Panel</a>
	</div> -->
	<div class="clear"></div>

	<div id="content">
		<div id="subnav">
			<ul>
				<li><a href="/ScanSeeWeb/retailer/regbatchuploadretprod.htm"><span>Batch
							Upload</span> </a></li>
				<li><a href="/ScanSeeWeb/retailer/regaddseachprod.htm"
					class="active"><span>Search / Add Product</span> </a></li>
			</ul>
		</div>
		<div class="clear"></div>

		<div class="grdSec">
			<form:form commandName="retprodsetupform" name="retprodsetupform"
				acceptCharset="ISO-8859-1">
				<form:hidden path="salePrice" />
				<form:hidden path="retailLocIDHidden" />
				<div id="bubble_tooltip">
					<div class="bubble_top">
						<span></span>
					</div>
					<div class="bubble_middle">
						<span id="bubble_tooltip_content">Content is comming here
							as you probably can see.Content is comming here as you probably
							can see.</span>
					</div>
					<div class="bubble_bottom"></div>
				</div>
				<table class="grdTbl" cellspacing="0" cellpadding="0" width="100%"
					border="0">
					<div align="center" style="font-style: 45">
						<label><form:errors cssStyle="color:red" /> </label>
					</div>
					<tbody>
						<tr>
							<c:choose>
								<c:when test="${manageAddProd eq 'manageProd'}">
									<td class="header"><input type="radio" value="searcAdd"
										name="searcAdd" onclick="deselectLoc()" /> Search / Add
										Products</td>
									<td class="header" colspan="3"><input type="radio"
										value="manageProd" checked="checked"
										onclick="regRetLocProducts()" name="searcAdd" /> Manage
										Product</td>
								</c:when>
								<c:when test="${manageAddProd eq 'addProduct'}">
									<td class="header"><input type="radio" value="searcAdd"
										name="searcAdd" checked="checked" onclick="deselectLoc()" />
										Search / Add Products</td>
									<td class="header" colspan="3"><input type="radio"
										value="manageProd" onclick="regRetLocProducts()"
										name="searcAdd" /> Manage Product</td>
								</c:when>
								<c:otherwise>
									<td class="header"><input type="radio" value="searcAdd"
										name="searcAdd" checked="checked" onclick="deselectLoc()" />
										Search / Add Products</td>
									<td class="header" colspan="3"><input type="radio"
										value="manageProd" onclick="regRetLocProducts()"
										name="searcAdd" /> Manage Product</td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<td class="Label" width="25%">Hold Ctrl to select more than
								one location</td>
							<td width="25%"><form:select path="LocID" multiple="true"
									id="retLocID" class="txtAreaBox">
									<c:forEach items="${sessionScope.retLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.storeIdentification}" />
									</c:forEach>


								</form:select> <!--<select class="textboxBig" id="select" name="select"> <option>ALL</option></select>-->
							</td>

							<td class="Label" width="16%">Product/UPC</td>
							<td width="34%"><form:input path="productName"
									name="searchKey" class="textboxBig" id="searchKey" /> <img
								class="imgLinks" title="Search" height="17" alt="Search"
								src="/ScanSeeWeb/images/searchIcon.png" width="20"
								onclick="retRegProdSearch()" /></td>
						</tr>
					</tbody>
				</table>




				<div class="searchGrd brdrTop">
					<h1 class="searchHeaderExpand">
						<a class="floatR" href="#">&nbsp;</a>Search Results
					</h1>

					<div class="grdCont hrzscrolllarge">

						<form:hidden path="productID" />
						<form:hidden path="price" />
						<form:hidden path="retailLocationProductDesc" />
						<form:hidden path="saleStartDate" />
						<form:hidden path="saleEndDate" />
						<table cellspacing="0" cellpadding="0" width="100%" border="0"
							id="prodinfotbl">
							<tbody>
								<c:if
									test="${! empty sessionScope.searchFormProduct.searchKey }">
									<tr>
										<td colspan="10">Search Results for:
											${sessionScope.searchFormProduct.searchKey}<b><font
												size=3> </font> </b></td>
									</tr>
								</c:if>

								<tr>


									<c:if
										test="${sessionScope.pagination.totalSize >0 && sessionScope.pagination.currentPage==1 && sessionScope.pagination.totalSize >= 20}">
										<td colspan="7">${sessionScope.pagination.totalSize}
											results found, top ${sessionScope.pagination.pageRange}
											displayed<br> <br>
										</td>
									</c:if>
									<c:if
										test="${sessionScope.pagination.totalSize >0 && sessionScope.pagination.currentPage==1 && sessionScope.pagination.totalSize < 20}">
										<td colspan="7">${sessionScope.pagination.totalSize}
											results found. <br> <br>
										</td>
									</c:if>
								<tr class="header">
									<td width="">Image</td>
									<td width="">Product UPC</td>
									<td width="">Product Name</td>
									<td width="">Description</td>

									<td width=""><a href="#"
										onmousemove="showToolTip(event,'Regular price, not sales price.');return false"
										onmouseout="hideToolTip()"> <font color="white">Price&nbsp;&nbsp;</font><img
											height="14" width="14" src="/ScanSeeWeb/images/helpIcon.png">
									</a></td>
									<td width="">Sale Price</td>
									<td><div class="dateCol">
											Start Date
											<p>(mm/dd/yyyy)</P>
										</div>
									</td>
									<td class="dateCol"><div class="dateCol">
											End Date
											<p>(mm/dd/yyyy)</P>
										</div>
									</td>
									<td width="">Actions</td>
								</tr>


								<c:forEach items="${sessionScope.seacrhList.productList}"
									var="item">
									<tr>
										<td><img height="46" alt="Image"
											src="${item.productImagePath}" width="49"
											onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';" />
										</td>
										<td><div class="brkwrdSmall">
												<c:out value="${item.scanCode}"></c:out>
											</div>
										</td>
										<td><c:out value="${item.productName}"></c:out></td>
										<td><textarea class="txtAreaSmall" id="textarea"
												name="textarea" rows="3" cols="55">${item.productShortDescription}</textarea>


										</td>
										<td><input id="prodprice" type="text" size="9"
											value="${item.price}" name="textfield" class="textboxPrice"
											onkeypress="return isNumberKeyPhone(event)" />
										</td>

										<td><input id="prodsaleprice" type="text" size="9"
											value="${item.salePrice}" name="textfield"
											class="textboxPrice"
											onkeypress="return isNumberKeyPhone(event)" /></td>
										<td><input type="text" size="9" name="textboxSD"
											value="${item.saleStartDate}" id="startdate${item.rowNumber}"
											class="textboxDate" />
										</td>
										<td><input type="text" size="9" name="textboxED"
											value="${item.saleEndDate}" id="enddate${item.rowNumber}"
											class="textboxDate" />
										</td>
										<td><ul class="actnLst rtlr">

												<c:if
													test="${manageAddProd ne null && manageAddProd eq 'addProduct'}">
													<li><a href="#"><img height="20" alt="Add"
															src="/ScanSeeWeb/images/imgAdd.png" title="Add"
															class="imgLinks"
															onclick="regAssociateRetProd(<c:out value='${item.productID}'/>,this);"
															width="22" /> </a></li>
												</c:if>
												<c:if
													test="${manageAddProd ne null && manageAddProd eq 'manageProd'}">
													<li><a href="#"><img height="20" alt="Add"
															src="/ScanSeeWeb/images/imgUpdate.png" title="Update"
															class="imgLinks"
															onclick="regUpdateManageProd(<c:out value='${item.productID}'/>,this);"
															width="22" /> </a></li>
												</c:if>
												<li><a href="#"> <img height="18" alt="Preview"
														title="Preview" src="/ScanSeeWeb/images/imgPreview.png"
														width="19"
														onclick="previewManageProductPopUp(${item.productID},'${item.productName}')" />
												</a></li>

											</ul></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</form:form>
		</div>
		<div class="pagination">
			<p>
				<page:pageTag currentPage="${sessionScope.pagination.currentPage}"
					nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
					pageRange="${sessionScope.pagination.pageRange}"
					url="${sessionScope.pagination.url}"  enablePerPage="false"/>
			</p>
		</div>
	</div>

	<div class="clear"></div>
	<div class="navTabSec">
		<div align="right">
			<input class="btn"
				onclick="window.location.href='/ScanSeeWeb/retailer/retailerchoosePlan.htm'"
				type="button" value="Next" name="Cancel" title="Next" />
		</div>

	</div>

</div>
<script>
        onLoadRetailerID();
  </script>
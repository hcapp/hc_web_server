<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(function() {
		$('#retlocationID option').click(function() {
			var totOpt = $('#retlocationID option').length;
			var totOptSlctd = $('#retlocationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		$("#retlocationID").change(function() {
			var totOpt = $('#retlocationID option').length;
			var totOptSlctd = $('#retlocationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
		
		$("#datepicker1").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});

		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		
		//populateDurationPeriod(document.getElementById("durationCheck").checked);
	});
</script>

<script type="text/javascript">
	function SelectAllLocation(checked) {
		var sel = document.getElementById("retlocationID");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}

	function onLoadRetLocationID() {
		var vRetLocID = document.editManageAdsForm.retailLocationIDHidden.value;
		var vRetLocVal = document.getElementById("retlocationID");
		var vRetLocList = [];
		if (vRetLocID != "null") {
			vRetLocList = vRetLocID.split(",");
		}
		if (vRetLocVal.length == vRetLocList.length) {
			document.getElementById('chkAllLoc').checked = true;
		}
		for ( var i = 0; i < vRetLocVal.length; i++) {
			for (j = 0; j < vRetLocList.length; j++) {
				if (vRetLocVal.options[i].value == vRetLocList[j]) {
					vRetLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function validateImage() {
		var bannerImage = document.getElementById("trgrUpld").value;
		if (bannerImage != '') {
			var checkbannerimg = bannerImage.toLowerCase();
			if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Splash Page image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
				document.buildbanneradform.bannerAdImagePath.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			} else {
				previewRetailerAds();
				return true;
			}
		} else {
			alert("You must upload Splash Page image file  with  following extensions : .png, .gif, .bmp, .jpg, .jpeg");
		}
	}

	function checkBannerSize(input) {
		var bannerImage = document.getElementById("trgrUpld").value;
		/*if (input.files && input.files[0].size > (100 * 1024)) {
			alert("File too large. Max 100 KB allowed.");
			input.value = null;
		} else */
		if (bannerImage != '') {
			var checkbannerimg = bannerImage.toLowerCase();
			if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Banner image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
				document.buildbanneradform.bannerAdImagePath.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}

	function showWelcomePagePopup() {
		var imgSrc = document.getElementById('bannerADImg').src;
		showWelcomePagePreview(imgSrc)
	}
</script>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	text-align: center;
}
</style>
</head>
<body>
	<div id="wrapper">
		<form:form name="editManageAdsForm" commandName="editManageAdsForm"
			id="editManageAdsForm" enctype="multipart/form-data"
			action="uploadAdsImg.htm" acceptCharset="ISO-8859-1">
			<input type="hidden" id="uploadBtn" name="uploadBtn">
			<form:hidden path="retailLocationIDHidden" />
			<form:hidden path="retailLocationAdvertisementID" />
			<form:hidden path="tempImageName" />
			<form:hidden path="strBannerAdImagePath" type="hidden"
				name="strBannerAdImagePath" id="strBannerAdImagePath" />
			<div id="content" class="shdwBg">

				<%@include file="retailerLeftNavigation.jsp"%>


				<div class="rtContPnl floatR">
					<div class="grpTitles">
						<h1 class="mainTitle">Splash Page View</h1>
							<div class="sub-actn sub-link"><a href="#" onclick="location='/ScanSeeWeb/retailer/addsplashinstructions.htm?sptype=Edit'">View Instructions</a> </div>
					</div>
					<div class="grdSec">
						<div align="center" style="font-style: 45">
							<label><form:errors cssStyle="color:red" /> </label>
						</div>

						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="grdTbl">

							<tr>
								<td colspan="4" class="header">Edit Splash Page
								<div id="message"></div>
							</td>
									
							</tr>
								<tr>
								<td class="Label"><label for="couponName" class="mand">Splash
										Page Name</label>
								</td>
								<td colspan="3"><form:input path="advertisementName"
										type="text" name="textfield2" id="couponName" tabindex="3" />
									<form:errors path="advertisementName" cssStyle="color:red">
									</form:errors></td>
							</tr>
							
							<tr>
								<td class="Label"><label for="upldImg" class="mand">Splash
										Page Image</label>
								</td>
								<td colspan="2"><label><img id="bannerADImg"
										width="139" height="200" alt="upload"
										src="${sessionScope.welcomePageCreateImgPath}"
										onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
								</label> <span class="topPadding"><label for="trgrUpld">
											<input type="button" value="Choose File" id="trgrUpldBtn"
											class="btn trgrUpld" title="Choose File" tabindex="4">
											<form:input path="bannerAdImagePath" type="file"
												class="textboxBig" id="trgrUpld"
												onchange="checkBannerSize(this);" /> </label> </span><label
									id="bannerAdImagePathErr" style="color: red; font-style: 45"></label>
								</td>
								<td>
									<ul class="actnLst">

										<li><strong>Upload Image Size:</strong><br>Suggested Minimum
											Size:320px/568px<br>Maximum Size:800px/600px<br><!--  Maximum Size:950px/1024px --></li>
									</ul></td>
							</tr>
							
							
							
							<tr>
								<td width="17%" class="Label"><label for="locationId"
									class="mand">Location</label></td>
								<td><form:select path="retailLocationIds"
										class="txtAreaBox" size="10" id="retlocationID"
										multiple="true" tabindex="1">
										<c:forEach items="${sessionScope.retailerLocList}" var="s">
											<form:option value="${s.retailerLocationID}"
												label="${s.address1}" />
										</c:forEach>

									</form:select> <br/>
									
									<form:label path="retailLocationIds">Hold Ctrl to select more than one location</form:label><br/>
									<form:errors path="retailLocationIds" cssStyle="color:red">
									</form:errors></td>

								<td colspan="2" align="left" valign="top" class="Label"><label>
										<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
										onclick="SelectAllLocation(this.checked);" tabindex="2" />
										Select All Locations </label> <br> <br> <c:if
										test="${message ne null }">
										<div id="message">
											<center>
												<label style="color: red; font-weight: regular;"> <c:out
														value="${message}" /> </label>
											</center>
										</div>
										<script>
											var PAGE_MESSAGE = true;
										</script>
									</c:if>
								</td>
							</tr>

						

							<tr>
								<td class="Label"><label for="strtDT" class="mand">Add
										Start Date</label></td>
								<td align="left"><form:input path="advertisementDate"
										id="datepicker1" class="textboxDate" name="csd" tabindex="5" />
									<form:errors path="advertisementDate" cssStyle="color:red">
									</form:errors>(mm/dd/yyyy)</td>
								<td class="Label"><label for="endDT" >Add
										End Date</label>
								</td>
								<td align="left"><form:input path="advertisementEndDate"
										id="datepicker2" class="textboxDate" name="csd" tabindex="6" />
									<form:errors path="advertisementEndDate" cssStyle="color:red">
									</form:errors>(mm/dd/yyyy)<br><span
						class="instTxt nonBlk">[End date is not required]</span></td>
							</tr>
							<!-- <tr>
								<td></td>
								<td></td>
								<td></td>
								<td align="left"><form:checkbox path="indefiniteAdDurationFlag" id="durationCheck" name="durationCheck" onchange="populateDurationPeriod(this.checked);"/> No end date
								</td>
							</tr>-->
						</table>
						<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
						<div class="navTabSec mrgnRt" align="right">
							<input class="btn"
								onclick="window.location.href='/ScanSeeWeb/retailer/manageads.htm'"
								value="Back" type="Button" name="Back" title="Back" tabindex="7" />
							<input class="btn" onclick="showWelcomePagePopup();"
								value="Preview" type="button" name="Preview" tabindex="8" /> <input
								class="btn" value="Submit" type="button"
								onclick="saveEditAds();" name="Cancel" title="Submit"
								tabindex="9" />
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</form:form>
	</div>

	<script>
		onLoadRetLocationID();
	</script>
	<script type="text/javascript">
		$('#trgrUpld')
				.bind(
						'change',
						function() {
							$("#uploadBtn").val("trgrUpldBtn")

							$("#editManageAdsForm")
									.ajaxForm(
											{
												success : function(response) {
													var imgRes = response
															.getElementsByTagName('imageScr')[0].firstChild.nodeValue

													if (imgRes == 'UploadLogoMaxSize') {
														$(
																'#bannerAdImagePathErr')
																.text(
																		"Image Dimension should not exceed Width: 800px Height: 600px");
													} else if (imgRes == 'UploadLogoMinSize') {
														$(
																'#bannerAdImagePathErr')
																.text(
																		"Image Dimension should be Minimum Width: 320px Height: 568px");
													} else {
														$(
																'#bannerAdImagePathErr')
																.text("");
														var substr = imgRes
																.split('|');

														if (substr[0] == 'ValidImageDimention') {
															var imgName = substr[1];
															$('#strBannerAdImagePath').val(imgName);
															$('#bannerADImg')
																	.attr(
																			"src",
																			substr[2]);
														} else {
															openIframePopupForImage(
																	'ifrmPopup',
																	'ifrm',
																	'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																		100, 99.5,
																	'Crop Image');
														}

													}
												}
											}).submit();

						});
	</script>
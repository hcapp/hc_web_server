<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/cityAutocomplete.js"></script>
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/zipcodeAutocomplete.js"></script>
<script type="text/javascript">
	function popUp(URL) {
		day = new Date();
		id = day.getTime();
		eval("page"
				+ id
				+ " = window.open(URL, '"
				+ id
				+ "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=1000,height=600,left = 390,top = 162');");
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		/* if ($('#stateHidden').val() != null) {
			stateCode = $('#stateCodeHidden').val();
			state = $('#stateHidden').val();
			$("#Country").val("");
		} */
		if ($('input[name="islocation"]').attr('checked')) {
			$('#storeNum').show();
		} else {
			$('#storeNum').hide();
		}
		$("#City").live("keydown", function(e) {
			var s = String.fromCharCode(e.which);
			if (s.match(/[a-zA-Z0-9\.]/)) {
				$('#stateCodeHidden').val("");
				$('#stateHidden').val("");
				$("#Country").val("");
				$('#postalCode').val("");
				cityAutocomplete('postalCode');
			} else if (s.match(/[\b]/)) {
				$('#stateCodeHidden').val("");
				$('#stateHidden').val("");
				$("#Country").val("");
				$('#postalCode').val("");
				cityAutocomplete('postalCode');
			}

		});

	/* 	var stateCode = '';
		var state = '';
		if ($('#postalCode').val().length == 5) {
			stateCode = $('#stateCodeHidden').val();
			state = $('#stateHidden').val();
			$("#Country").val(state);
		} else {
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
			$("#Country").val("");
		} */

		var geoError = $("#geoError").val();
		if (geoError == 'true') {
			$("#dispLatLang").show();

		} else {

			$("#dispLatLang").hide();
		}
	});
	/* function getCityTrigger(val) {
		if (val == "") {
			$("#Country").val("");
			$("#postalCode").val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		document.createshopperprofileform.cityHidden.value = val;
		document.createshopperprofileform.city.value = val;
	} */
	function clearOnCityChange() {
		if ($('#citySelectedFlag').val() != 'selected') {
			$("#Country").val("");
			$("#postalCode").val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		} else {
			$('#citySelectedFlag').val('');
		}
	}
	function isEmpty(vNum) {
		if (vNum.length < 5) {
			$("#Country").val("");
			$("#City").val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		return true;
	}
	function changeStoreNum(checked) {
		if (checked == false) {
			if (document.getElementById('storeNum.errors') != null) {
				document.getElementById('storeNum.errors').style.display = 'none';
			}
		}
	}
	function verifyEmail() {
		var status = false;
		if (document.appsiteform.contactEmail.value != document.appsiteform.retypeEmail.value) {
			alert("Email addresses do not match.  Please retype them to make sure they are the same.");
		} else {
			status = true;
		}
		return status;
	}

	function validatePassword() {
		var isDigits = /[0-9]/;
		var password = document.appsiteform.password.value;
		if (password != "") {
			if (document.appsiteform.password.value.length < 8) {
				alert("Password must contain at least eight characters!");
				document.appsiteform.password.focus();
				return false;
			} else if (!isDigits.test(document.appsiteform.password.value)) {
				alert("Password must contain at least one number (0-9)!");
				document.appsiteform.password.focus();
				return false;
			} else if (document.appsiteform.password.value != document.appsiteform.retypePassword.value) {
				alert("Your password and retype password do not match.");
				document.appsiteform.password.focus();
				return false;
			}
			return true;
		}
	}

	function clearProfile() {
		var r = confirm("Do you really want to clear the form")

		if (r == true) {
			document.appsiteform.retailerName.value = "";
			document.appsiteform.address1.value = "";
			document.appsiteform.address2.value = "";
			document.appsiteform.state.value = "";
			document.appsiteform.stateHidden.value = "";
			document.appsiteform.stateCodeHidden.value = "";
			document.appsiteform.city.value = "";
			document.appsiteform.postalCode.value = "";
			document.appsiteform.webUrl.value = "";

			document.appsiteform.referralName.value = "";
			document.appsiteform.keyword.value = "";
			document.getElementById('bCategory').selectedIndex = -1;
			document.appsiteform.contactFName.value = "";
			document.appsiteform.contactLName.value = "";
			/*document.appsiteform.legalAuthorityFName.value="";
			document.appsiteform.legalAuthorityLName.value="";*/
			document.appsiteform.terms.checked = false;
			document.appsiteform.islocation.checked = false;
			document.getElementById('contactPhone').value = "";
			document.getElementById('contactPhoneNo').value = "";
			document.appsiteform.contactEmail.value = "";

			document.appsiteform.storeNum.value = "";
			document.getElementById('storeNum').style.display = "none";

			if (document.getElementById('retailerName.errors') != null) {
				document.getElementById('retailerName.errors').style.display = 'none';
			}
			if (document.getElementById('address1.errors') != null) {
				document.getElementById('address1.errors').style.display = 'none';
			}
			if (document.getElementById('state.errors') != null) {
				document.getElementById('state.errors').style.display = 'none';
			}
			if (document.getElementById('city.errors') != null) {
				document.getElementById('city.errors').style.display = 'none';
			}
			if (document.getElementById('postalCode.errors') != null) {
				document.getElementById('postalCode.errors').style.display = 'none';
			}
			if (document.getElementById('contactPhone.errors') != null) {
				document.getElementById('contactPhone.errors').style.display = 'none';
			}
			if (document.getElementById('bCategory.errors') != null) {
				document.getElementById('bCategory.errors').style.display = 'none';
			}
			if (document.getElementById('storeNum.errors') != null) {
				document.getElementById('storeNum.errors').style.display = 'none';
			}
			/*if(document.getElementById('legalAuthorityFName.errors')!= null){
			document.getElementById('legalAuthorityFName.errors').style.display='none';
			}
			if(document.getElementById('legalAuthorityLName.errors')!= null){
			document.getElementById('legalAuthorityLName.errors').style.display='none';
			}*/
			if (document.getElementById('contactFName.errors') != null) {
				document.getElementById('contactFName.errors').style.display = 'none';
			}
			if (document.getElementById('contactLName.errors') != null) {
				document.getElementById('contactLName.errors').style.display = 'none';
			}
			if (document.getElementById('contactPhoneNo.errors') != null) {
				document.getElementById('contactPhoneNo.errors').style.display = 'none';
			}
			if (document.getElementById('contactEmail.errors') != null) {
				document.getElementById('contactEmail.errors').style.display = 'none';
			}

			if (document.getElementById('terms.errors') != null) {
				document.getElementById('terms.errors').style.display = 'none';
			}
			if (document.getElementById('webUrl.errors') != null) {
				document.getElementById('webUrl.errors').style.display = 'none';
			}
		}
	}

	function isNumeric(vNum) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;
		var vPostal = document.getElementById("postalCode");
		for (i = 0; i < vNum.length && IsNumber == true; i++) {
			Char = vNum.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				vPostal.value = "";
				vPostal.focus();
				IsNumber = false;
			}
		}
		return IsNumber;
	}
	
	function isLatLong(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
				|| charCode == 45 || charCode == 43)
			return true;
		return false;
	}
</script>
<div id="dockPanel">
	<ul id="prgMtr" class="tabs">
		<!--<li> <a href="https://www.scansee.net/links/aboutus.html" title="About HubCiti" target="_blank" rel="About HubCiti">About HubCiti</a> </li>-->
		<li><a class="tabActive" title="Create Profile" href="#"
			rel="Create Profile">Create Profile</a></li>
	</ul>
</div>
<div id="togglePnl">
	<!-- <a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down" width="9" height="8" /> Show Panel</a>-->
</div>
<div class="clear"></div>
<div id="content" class="topMrgn">
	<div class="section shadowImg">
		<div class="infoSecB">HubCiti<sup>&reg;</sup> helps you deliver the right
			message to your customer. To provide you with the best plan options,
			we need to learn little bit more about you.</div>
		<div class="grdSec brdrTop">

			<form:form name="appsiteform" id="appsiteform"
				action="regppsiteretailer.htm" commandName="appsiteform"
				acceptCharset="ISO-8859-1">
				<form:hidden path="bCategoryHidden" />
				<form:hidden path="cityHidden" />
				<form:hidden path="stateHidden" id="stateHidden" />
				<form:hidden path="stateCodeHidden" id="stateCodeHidden" />
				<form:hidden path="geoErr" id="geoError" />
				<div id="bubble_tooltip">
					<div class="bubble_top">
						<span></span>
					</div>
					<div class="bubble_middle">
						<span id="bubble_tooltip_content">Content is comming here
							as you probably can see.Content is comming here as you probably
							can see.</span>
					</div>
					<div class="bubble_bottom"></div>
				</div>
				<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
					width="100%">
					<tbody>
						<tr>
							<td colspan="2" class="header">Business Registration Form:
								Please enter your Corporate Information</td>
							<td colspan="2" style="color: red;"><form:errors
									cssStyle="color:red"></form:errors></td>
						</tr>
						<tr>
							<td class="Label"><label for="rtlrName" class="mand"><a
									href="javascript:void(0)"
									onmousemove="showToolTip(event,'This is your storefront name or corporate name.');return false"
									onmouseout="hideToolTip()">Business Name<img alt="helpIcon"
										src="../images/helpIcon.png" />
								</a> </label></td>
							<td colspan="3"><form:errors cssStyle="color:red"
									path="retailerName"></form:errors> <form:input
									path="retailerName" id="retailerName" maxlength="100"
									tabindex="1" />
								<p class="smlTxt">This is your storefront name.</p></td>
						</tr>
						<tr>
							<td class="Label" width="17%"><label for="addrs1"
								class="mand">Business Address1</label></td>

							<td width="33%"><form:errors cssStyle="color:red"
									path="address1"></form:errors> <form:textarea path="address1"
									id="address1" class="txtAreaSmall" rows="5" cols="45"
									onkeyup="checkMaxLength(this,'100');" tabindex="2"
									cssStyle="height:60px;" /></td>
							<td class="Label"><label for="addrs3">Business
									Address2</label></td>
							<td><form:textarea path="address2" id="address2"
									class="txtAreaSmall" rows="5" cols="45"
									onkeyup="checkMaxLength(this,'50');" tabindex="3"
									cssStyle="height:60px;" /></td>
						</tr>
						<tr id="dispLatLang" style="display: none">
							<td class="Label" align="left"><label for="Latitude"
								class="mand">Latitude </label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="retailerLocationLatitude"></form:errors> <form:input
									id="retailerLocationLatitude" tabindex="4"
									path="retailerLocationLatitude"
									onkeypress="return isLatLong(event)" /></td>
							<td class="Label" align="left"><label for="Longitude"
								class="mand">Longitude</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="retailerLocationLongitude"></form:errors> <form:input
									id="retailerLocationLongitude" tabindex="5"
									path="retailerLocationLongitude"
									onkeypress="return isLatLong(event)" /> <br /></td>
						</tr>
						<tr>
							<td class="Label"><label for="pCode" class="mand">
									Postal Code </label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="postalCode"></form:errors> <form:input path="postalCode"
									type="text" class="loadingInput dsblContxMenu" maxlength="6"
									tabindex="4" name="couponName7" id="postalCode"
									onkeypress="zipCodeAutocomplete('postalCode');return isNumberKey(event)"
									onchange="isNumeric(this.value);"
									onkeyup="isEmpty(this.value);" /></td>
							<td class="Label"><label for="cty" class="mand">City</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="city" cssClass="error"></form:errors> <form:input
									path="city" id="City" tabindex="7"
									class="loadingInput dsblContxMenu" /></td>
						</tr>
						<tr>
							<td class="Label" align="left"><label for="sts" class="mand">State</label>
							</td>
							<td align="left"><form:errors cssStyle="color:red"
									path="state" cssClass="error"></form:errors> <form:select
									path="state" id="Country" tabindex="8">
									<form:option value="">--Select--</form:option>
									<c:forEach items="${sessionScope.statesListCreat}" var="s">
										<form:option value="${s.stateabbr}" label="${s.stateName}" />
									</c:forEach>
								</form:select></td>
							<td class="Label" align="left"><label for="phnNum"
								class="mand">Corporate Phone #</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="contactPhone" onkeypress="return isNumberKeyPhone(evt)"></form:errors>
								<form:input path="contactPhone" class="textboxSmall"
									id="contactPhone" value="" tabindex="9"
									onkeyup="javascript:backspacerUP(this,event);"
									onkeydown="javascript:backspacerDOWN(this,event);" />
								(xxx)xxx-xxxx
								<p class="smlTxt">Where your customers would call your
									store/business.</p></td>
						</tr>


					</tbody>
				</table>
				<div class="navTabSec RtMrgn">
					<div align="right">
						<input type="submit" class="btn" value="Next" tabindex="10" />
					</div>
				</div>
			</form:form>

		</div>
	</div>
	<div class="clear"></div>
	<div class="section topMrgn">
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$("#retailerLocationLatitude").focusout(function() {
		/* Matches	 90.0,-90.9,1.0,-23.343342
		 Non-Matches	 90, 91.0, -945.0,-90.3422309*/
		var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
		var vLat = $('#retailerLocationLatitude').val();
		//Validate for Latitude.
		if (0 === vLat.length || !vLat || vLat == "") {
			return false;
		} else {
			if (!vLatLngVal.test(vLat)) {
				alert("Invalid Latitude");
				$("#retailerLocationLatitude").val("");
				$("#retailerLocationLatitude").focus();
				return false;
			}
		}
	});
	

	$("#retailerLocationLongitude")
			.focusout(
					function() {
						/* Matches	180.0, -180.0, 98.092391
						 Non-Matches	181, 180, -98.0923913*/
						 var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
							//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
						var vLong = $('#retailerLocationLongitude').val();
							//Validate for Longitude.
						if (0 === vLong.length || !vLong || vLong == "") {
							return false;
						} else {
							if (!vLatLngVal.test(vLong)) {
								alert("Invalid Longitude");
								$("#retailerLocationLongitude").val("").focus();
								return false;
							}
						}
					});
</script>
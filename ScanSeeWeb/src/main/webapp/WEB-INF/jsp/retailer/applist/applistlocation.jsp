<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@page session="true"%>
<%@ page import="common.pojo.RetailerLocation"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>

<script type="text/javascript">
function DisableBackButton() {
window.history.forward()
}
DisableBackButton();
window.onload = DisableBackButton;
window.onpageshow = function(evt) { if (evt.persisted) DisableBackButton() }
window.onunload = function() { void (0) }
</script>

<script type="text/javascript">

function onSumbitLocation(btnVal){

				var isLocationFlag='${sessionScope.isLocationSelectedFlg}';

				var corporateStoreId='${sessionScope.CorporateStoreId}';

				var appSiteLocGeoErr= '${requestScope.appSiteLocGeoErr}';
				
				saveAppSiteLoc(isLocationFlag,btnVal,corporateStoreId,appSiteLocGeoErr);
	
		}


function onLoadSetState(){

	var stateCode = '${sessionScope.appListCreatedRetailerInfo.state}';
	
	$("#state0").val(stateCode);
	
	onLoadStateChage('0');
	
}

function onLoadStateChage(index) {

	// get the form values
	var stateCode = $('#state'+index).val();
	var tabIndex = $('#tabindex').val();
	
	var optIndex = 1;
	var cityDropDown = document.getElementById("city"+index);

	$.ajax({
		type : "GET",
		url : "retailerfetchcity.htm",
		data : {
			'statecode' : stateCode,
			'city' : ""
		},

		success : function(response) {
			var responseJSON = JSON.parse(response);

			var cityStr = responseJSON.City;
			var cityList = cityStr.split(",");
			
			cityDropDown.options.length = cityList.length;
			cityDropDown.options[0].value = '0';
			cityDropDown.options[0].text = '--Select--';
			cityDropDown.options[0].selected = true;
			for ( var i = 0; i < cityList.length-1; i++) {
			//alert(optIndex)
				cityDropDown.options[optIndex].value = cityList[i];
				cityDropDown.options[optIndex].text = cityList[i];
				optIndex++;

			}
				var cityCode = '${sessionScope.appListCreatedRetailerInfo.city}';

				$("#city0").val(cityCode);
		},
		error : function(e) {
			alert('Error: ' + e);
		}
	});
}

function onLoadSetCity(index) {

	// get the form values
	var stateCode = $('#state'+index).val();
	var tabIndex = $('#tabindex').val();
	
	var optIndex = 1;
	var cityDropDown = document.getElementById("city"+index);

	$.ajax({
		type : "GET",
		url : "retailerfetchcity.htm",
		data : {
			'statecode' : stateCode,
			'city' : ""
		},

		success : function(response) {
			var responseJSON = JSON.parse(response);

			var cityStr = responseJSON.City;
			var cityList = cityStr.split(",");
			
			cityDropDown.options.length = cityList.length;
			cityDropDown.options[0].value = '0';
			cityDropDown.options[0].text = '--Select--';
			cityDropDown.options[0].selected = true;
			for ( var i = 0; i < cityList.length-1; i++) {
			//alert(optIndex)
				cityDropDown.options[optIndex].value = cityList[i];
				cityDropDown.options[optIndex].text = cityList[i];
				optIndex++;

			}
			
			var cityCode= $("#hiddenCity"+index).val();
			$("#city"+index).val(cityCode);
		},
		error : function(e) {
			alert('Error: ' + e);
		}
	});
}
function onChangeState(index) {
		// get the form values
		var stateCode = $('#state'+index).val();
		var tabIndex = $('#tabindex').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("city"+index);

		$.ajax({
			type : "GET",
			url : "retailerfetchcity.htm",
			data : {
				'statecode' : stateCode,
				'city' : ""
			},

			success : function(response) {
				var responseJSON = JSON.parse(response);

				var cityStr = responseJSON.City;
				var cityList = cityStr.split(",");
				cityDropDown.options.length = cityList.length;
				cityDropDown.options[0].value = '0';
				cityDropDown.options[0].text = '--Select--';
				cityDropDown.options[0].selected = true;

				for ( var i = 0; i < cityList.length; i++) {
					cityDropDown.options[optIndex].value = cityList[i];
					cityDropDown.options[optIndex].text = cityList[i];
					optIndex++;

				}

			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}
function isLatLong(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
			|| charCode == 45 || charCode == 43)
		return true;
	return false;
}

</script>
<div id="dockPanel"></div>
<div id="togglePnl">&nbsp;</div>
<div class="clear"></div>
<div id="content" class="topMrgn">
	<div class="section shadowImg">
		<div class="infoSecB">

			<c:choose>

				<c:when test="${sessionScope.isLocationSelectedFlg eq true}">
					<p>Please enter your store location below.</p>
				</c:when>
				<c:otherwise>
					<p>Do you have any additional locations? If so, add them below,
						if not press skip.</p>
				</c:otherwise>
			</c:choose>

		</div>
		<div class="grdSec brdrTop">

			<div id="bubble_tooltip">
				<div class="bubble_top">
					<span></span>
				</div>
				<div class="bubble_middle">
					<span id="bubble_tooltip_content"></span>
				</div>
				<div class="bubble_bottom"></div>
			</div>
			<h2 class="zeroMargin">App Listing add locations</h2>
			<div class="searchGrd hrzscrol brdrTop">
				<form:form name="locationsetupform" commandName="locationsetupform"
					enctype="multipart/form-data">
					<form:hidden path="prodJson" />


					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="tblEffect" id="locSetUpGrd">
						<tr class="header">
							<!--<td width="10%" align="center"><label> <input
									type="checkbox" name="deleteAll" /> </label>
							</td>-->
							<td width="11%"><p>
									<a href="#" class="hdr-white"
										onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
										onmouseout="hideToolTip()">Store </a>
								</p>
								<p>
									<a href="#" class="hdr-white"
										onmousemove="showToolTip(event,'Store Identification can either be your store number or other identifier.');return false"
										onmouseout="hideToolTip()">Identification</a><a href="#"
										onmousemove="showToolTip(event,'Store Identification can either be your store number or other unique identifier.');return false"
										onmouseout="hideToolTip()"><label class="mand"><img
											alt="helpIcon" src="../images/helpIcon.png" /> </label> </a>
								</p></td>
							<td width="17%"><label class="mand">Store Address</label></td>
							<td width="17%"><label class="mand">State</label></td>
							<td width="10%"><label class="mand">City</label></td>
							<td width="10%"><label class="mand">Postal Code</label></td>
							<td width="20%"><label class="mand">Phone Number</label></td>
							<td width="17%"><label>Latitude</label></td>
							<td width="17%"><label>Longitude</label></td>
							<td width="17%">Website URL</td>
							<td width="17%">Keywords</td>
						</tr>


						<c:set var="index" value="0"></c:set>

						<c:if test="${sessionScope.isLocationSelectedFlg eq true }">
							<tr>
								<td><form:input path="address1" cssClass="textboxSmall"
										disabled="true"
										value="${sessionScope.appListCreatedRetailerInfo.storeNum }" />
								</td>
								<td><form:input path="address1" cssClass="textboxSmall"
										value="${sessionScope.appListCreatedRetailerInfo.address1 }"
										disabled="true" /></td>
								<td><form:select id="state0" path="state" disabled="true">
										<form:option value="0">--Select--</form:option>
										<c:forEach items="${sessionScope.statesListCreat}" var="s">
											<form:option value="${s.stateabbr}" label="${s.stateName}" />
										</c:forEach>
									</form:select></td>
								<td><form:select path="city" id="city0" disabled="true">
										<form:option value="0">--Select--</form:option>
									</form:select></td>

								<td><form:input path="postalCode" cssClass="textboxSmaller"
										value="${sessionScope.appListCreatedRetailerInfo.postalCode}"
										disabled="true" maxlength="10"
										onkeypress="return isNumberKey(event)" /></td>

								<td><form:input path="phonenumber" cssClass="textboxSmall"
										value="${sessionScope.appListCreatedRetailerInfo.contactPhone}"
										disabled="true" onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);"
										onkeypress="return isNumberKey(event)" /></td>

								<td><form:input path="retailerLocationLatitude"
										cssClass="textboxSmall latChk" disabled="true"
										value="${sessionScope.appListCreatedRetailerInfo.retailerLocationLatitude}"
										id="locLatitude" /></td>
								<td><form:input path="retailerLocationLongitude"
										cssClass="textboxSmall lonChk" disabled="true"
										value="${sessionScope.appListCreatedRetailerInfo.retailerLocationLongitude}"
										id="locLongitude" /></td>
								<td><form:input path="retailLocationUrl"  maxlength="75"
										value="${sessionScope.appListCreatedRetailerInfo.webUrl}"
										cssClass="textboxSmall" disabled="true" /></td>
								<td><form:input path="keyword" cssClass="textboxSmall"
										value="${sessionScope.appListCreatedRetailerInfo.keyword}"
										disabled="true" /></td>
							</tr>
							<c:set var="index" value="${index+1}"></c:set>
						</c:if>
						<c:set var="j" value="0" />


						<c:if
							test="${requestScope.appSiteLocGeoErr ne 'null' && requestScope.appSiteLocGeoErr eq true }">
							<c:forEach items="${sessionScope.appSiteLocList}" var="item">


								<tr id="${j}">
									<td><form:hidden path="retailID"
											value="${sessionScope.appListCreatedRetailId}" /> <form:input
											path="storeIdentification" cssClass="textboxSmall"
											value="${item.storeIdentification}" /></td>

									<td><form:input path="address1" cssClass="textboxSmall"
											maxlength="150" value="${item.address1}" /></td>

									<td><form:select id="state${index}" path="state"
											onchange="onChangeState(${index})">
											<form:option value="0">--Select--</form:option>
											<c:forEach items="${sessionScope.statesListCreat}" var="s">
												<c:choose>
													<c:when test="${item.state eq s.stateabbr }">
														<form:option selected="true" value="${s.stateabbr}"
															label="${s.stateName}" />
													</c:when>


													<c:otherwise>
														<form:option value="${s.stateabbr}" label="${s.stateName}" />
													</c:otherwise>
												</c:choose>


											</c:forEach>
										</form:select></td>
									<td><form:select path="city" id="city${index}">
											<form:option value="0">--Select--</form:option>
										</form:select> <input type="hidden" value="${item.city}"
										id="hiddenCity${index}"></td>

									<td><form:input path="postalCode"
											cssClass="textboxSmaller" maxlength="10"
											onkeypress="return isNumberKey(event)"
											value="${item.postalCode}" /></td>

									<td><form:input path="phonenumber" cssClass="textboxSmall"
											onkeyup="javascript:backspacerUP(this,event);"
											onkeydown="javascript:backspacerDOWN(this,event);"
											onkeypress="return isNumberKey(event)"
											value="${item.phonenumber}" /></td>


									<td><form:input path="retailerLocationLatitude"
											cssClass="textboxSmall latChk"
											value="${item.retailerLocationLatitude}"
											id="locLatitude${index}" onkeypress="return isLatLong(event)" /></td>
									<td><form:input path="retailerLocationLongitude"
											cssClass="textboxSmall lonChk"
											value="${item.retailerLocationLongitude}"
											id="locLongitude${index}"
											onkeypress="return isLatLong(event)" /></td>
									<td><form:input path="retailLocationUrl"
											cssClass="textboxSmall" value="${item.retailLocationUrl}" /></td>
									<td><form:input path="keyword" cssClass="textboxSmall"
											value="${item.keyword}" /></td>
								</tr>
								<c:set var="j" value="${j+1 }" />
								<c:set var="index" value="${index+1}"></c:set>
							</c:forEach>


						</c:if>



						<c:forEach var="i" begin="${index}" end="4">


							<tr id="${j}">
								<td><form:hidden path="retailID"
										value="${sessionScope.appListCreatedRetailId}" /> <form:input
										path="storeIdentification" cssClass="textboxSmall" /></td>

								<td><form:input path="address1" cssClass="textboxSmall"
										maxlength="150" /></td>

								<td><form:select id="state${index}" path="state"
										onchange="onChangeState(${index})">
										<form:option value="0">--Select--</form:option>
										<c:forEach items="${sessionScope.statesListCreat}" var="s">
											<form:option value="${s.stateabbr}" label="${s.stateName}" />
										</c:forEach>
									</form:select></td>
								<td><form:select path="city" id="city${index}">
										<form:option value="0">--Select--</form:option>
									</form:select></td>

								<td><form:input path="postalCode" cssClass="textboxSmaller"
										maxlength="10" onkeypress="return isNumberKey(event)" /></td>

								<td><form:input path="phonenumber" cssClass="textboxSmall"
										onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);"
										onkeypress="return isNumberKey(event)" /></td>


								<td><form:input path="retailerLocationLatitude"
										cssClass="textboxSmall latChk" id="locLatitude${i}"
										onkeypress="return isLatLong(event)" /></td>
								<td><form:input path="retailerLocationLongitude"
										cssClass="textboxSmall lonChk" id="locLongitude${i}"
										onkeypress="return isLatLong(event)" /></td>
								<td><form:input path="retailLocationUrl"
										cssClass="textboxSmall" /></td>
								<td><form:input path="keyword" cssClass="textboxSmall" />
								</td>
							</tr>
							<c:set var="index" value="${index+1}"></c:set>
							<c:set var="j" value="${j+1 }" />
						</c:forEach>




					</table>
				</form:form>
			</div>
			<div class="navTabSec RtMrgn">
				<div align="right">
					<input type="button" class="btn" value="Save"
						onclick="onSumbitLocation('save')" /> <input type="button"
						class="btn" value="Skip" onclick="onSumbitLocation('skip')" />
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
var isLocationFlag='${sessionScope.isLocationSelectedFlg}';
var appSiteLocGeoErr= '${requestScope.appSiteLocGeoErr}';


if(isLocationFlag=='true'){

	onLoadSetState();
	
	if(appSiteLocGeoErr !="" && appSiteLocGeoErr == 'true'){
			
		for(var i=1;i<=4;i++){
			
			onLoadSetCity(i);
		
		}
		$('td:nth-child(7)').show();
		$('td:nth-child(8)').show();
	}else{
		$('td:nth-child(7)').hide();
		$('td:nth-child(8)').hide();
		
	}
	
}else{
	
	
	if(appSiteLocGeoErr !="" && appSiteLocGeoErr == 'true'){
		
		for(var i=0;i<=4;i++){
			
			onLoadSetCity(i);
		}
		$('td:nth-child(7)').show();
		$('td:nth-child(8)').show();
	}else{
		$('td:nth-child(7)').hide();
		$('td:nth-child(8)').hide();
		
	}
	
	
}

if(appSiteLocGeoErr !="" && appSiteLocGeoErr == 'true'){
	
	
	var appSiteLocHigltRows='${requestScope.appSiteLocHigltRows}';
	var rows = appSiteLocHigltRows.split(',');
	
	for(var k=0;k<rows.length;k++){
		
		$("tr#" + rows[k]).removeClass("hilite").addClass("requiredVal");
	}
	
	alert("Could not locate your address, Please enter valid address or enter Latitude & Longitude for the highlighted rows");
	
	
}


$("#locLatitude0,#locLatitude1,#locLatitude2,#locLatitude3,#locLatitude4").focusout(function() {
	/* Matches	 90.0,-90.9,1.0,-23.343342
	 Non-Matches	 90, 91.0, -945.0,-90.3422309*/
	var vLatLngVal = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
	var vLat = $(this).val();
	//Validate for Latitude.
	if (0 === vLat.length || !vLat || vLat == "") {
		return false;
	} else {
		if (!vLatLngVal.test(vLat)) {
			alert("Invalid Latitude");
			$(this).val("");
			$(this).focus();
			return false;
		}
	}
});

$("#locLongitude0,#locLongitude1,#locLongitude2,#locLongitude3,#locLongitude4")
		.focusout(
				function() {
					/* Matches	180.0, -180.0, 98.092391
					 Non-Matches	181, 180, -98.0923913*/
					 var vLatLngVal = /^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9]|[1][0][0-9])\.{1}\d{1,6}/;
						//var vLatLngVal = /^-?([1]?[0-7][0-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}/;
					var vLong = $(this).val();
						//Validate for Longitude.
					if (0 === vLong.length || !vLong || vLong == "") {
						return false;
					} else {
						if (!vLatLngVal.test(vLong)) {
							alert("Invalid Longitude");
							$(this).val("").focus();
							return false;
						}
					}
				});
</script>

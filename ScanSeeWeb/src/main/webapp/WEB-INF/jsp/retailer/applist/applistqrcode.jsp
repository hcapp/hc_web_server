<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>


<script type="text/javascript">
	function DisableBackButton() {
		window.history.forward()
	}
	DisableBackButton();
	window.onload = DisableBackButton;
	window.onpageshow = function(evt) {
		if (evt.persisted)
			DisableBackButton()
	}
	window.onunload = function() {
		void (0)
	}
</script>

<div id="content" class="topMrgn">
	<div class="section">
		<div class="infoSecB">
			<p>Congratulations, You have built your Free App Listing at	HubCiti!</p>
			<p>Below is your unique QR code:</p>
		</div>
		<div class="grdSec brdrTop">
			<table class="grdTbl zeroBtmMrgn" border="0" cellspacing="0"
				cellpadding="0" width="100%">
				<tbody>
					<tr>
						<td class="header" colspan="6">Download your QR code. <span></span>
						</td>
					</tr>

					<tr>

						<td align="center" class="zeroPadding">

							<ul class="col6">
								<c:forEach items="${sessionScope.retailerLocationQrList}"
									var="item">
									<li><img id="${item.storeIdentification}"
										src="${item.qrImagePath}" alt="qr code" width="115"
										height="115" /> <span><a
											href='/ScanSeeWeb/fileController/downloadimg.htm?ImagName=${item.imagName}'
											title="Download QR code" target="_blank"><img
												id="saveimage" src="../images/dwnlIcon.png" alt="download"
												width="18" height="18" />${item.storeIdentification}</a> </span>
									</li>
								</c:forEach>
							</ul>
						</td>


					</tr>


				</tbody>
			</table>

		</div>
		<div>
			<h3 class="topMrgn">So what is a QR code?</h3>
			<p>&nbsp;</p>
			<p>This funny looking little square is full of digital
				information that when scanned takes people instantly to your Free
				App Listing hosted at HubCiti. Here they can see all the information
				you just provided including the "About Us" information. It is a
				one-of-a-kind piece of art and technology - and it is all yours!</p>
			<p>&nbsp;</p>
			<p>The first thing you want to do is add this QR code to your
				business cards. Just give the file you downloaded above to your
				printing company and ask them to add it to your business card . You
				should also add this QR code to all your handouts, brochures,
				business cards, table tents, flyers, stationary, post cards, and
				anywhere else you can imagine. Just give your print layout and this
				file to your printing company, they know what to do.</p>
			<p>&nbsp;</p>
			<p>Give us a few days to process your Free App Listing and then
				try it with the HubCiti app "Scan Now" button and see it work!
				Presto- Your Free App Listing.</p>
			<p>&nbsp;</p>
			<p>When you are ready to add your logo, website links, banners,
				AnyThing Pages<sup class="smallsup">TM</sup>, other cool stuff, please feel free to upgrade to a
				full AppSite<sup class="smallsup">TM</sup>. The cost is unbelievably affordable and we will be
				happy to help advance your business.</p>
		</div>
	</div>
	<div class="clear"></div>
	<div class="section topMrgn">
		<div class="clear"></div>
	</div>
</div>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<head>
<title>HubCiti</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Enter"/>
<meta content="blendTrans(Duration=0.0)" http-equiv="Page-Exit"/>
<link rel="stylesheet" type="text/css" href="styles/style.css"/>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/global.js" type="text/javascript"></script>
</head>
<body style="background:#FFFFFF">
<script type="text/javascript">
$(function() {
    //Get actual content and display as title on mouse hover on it.
    var actText = $("td.genTitle").text();
    $("td.genTitle").attr('title',actText);
    var limit = 20;// character limit restricted to 20
    var chars = $("td.genTitle").text(); 
    if (chars.length > limit) {
        var visibleArea = $("<span> "+ chars.substr(0, limit-1) +"</span>");
        var dots = $("<span class='dots'>...</span>");
        $("td.genTitle").empty()
            .append(visibleArea)
            .append(dots);//append trailing dots to the visibile part 
    }                            
});
</script>
<div id="iphonePanel">
<form:form name="previewcouponform" commandName="previewcouponform">
     <div class="navBar iphone">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="titleGrd">
            <tr>
              <td width="19%"><img src="../images/backBtn.png" alt="back" width="50" height="30" /></td>
                            		
              <td width="54%" class="genTitle">${sessionScope.couponDetails.couponName}</td>
              	
              <td width="27%"><img src="../images/mainMenuBtn.png" alt="mainmenu" width="78" height="30" /></td>
            </tr>
          </table>
    </div>

  <div class="previewAreaScroll newScrollArea">
   	<div class="stretch cmnPnl relatvDiv">    
	<div class="txthglight">	
    <c:choose>
    	<c:when test="${null ne sessionScope.couponDetails.bannerTitle && !empty sessionScope.couponDetails.bannerTitle}">              		
   		 <h3 class="overallPadding">${sessionScope.couponDetails.bannerTitle}</h3>
    	</c:when>
    	<c:otherwise>
    		<h3 class="overallPadding">Title</h3>
    	</c:otherwise>
    </c:choose>
	</div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="zeroBrdrTbl dashedBrdr">
      <tr class="noBg">
        <c:choose>
        	<c:when test="${(sessionScope.couponImage ne null && sessionScope.couponImage ne '') || (sessionScope.prodImage ne null && sessionScope.prodImage ne '')}">
        		<c:choose>
        			<c:when test="${sessionScope.couponImage ne null && sessionScope.couponImage ne ''}">
		              <td width="40%" align="center" valign="top" colspan="2"><img src="${sessionScope.couponImage}" alt="Image" width="130" height="130" /></td>
		           </c:when>
		           <c:otherwise>
		             <td width="40%" align="center" valign="top" colspan="2"><img src="${sessionScope.prodImage}" alt="Image" width="130" height="130" /></td>
		           </c:otherwise>
        		</c:choose>        	
        	</c:when>
        	<c:otherwise>
        		<td width="40%" align="center" valign="top" colspan="2"><img src="../images/imagePreview.png" alt="Image" width="130" height="130" /></td>
        	</c:otherwise>
          </c:choose>
          
      </tr>
	  
	       <tr class="hglight">      	
      	<c:choose>
           	<c:when test="${null ne sessionScope.couponDetails.couponStartDate && !empty sessionScope.couponDetails.couponStartDate}">              		
           		 <td colspan="2"  align="left">Valid From ${sessionScope.couponDetails.couponStartDate} to ${sessionScope.couponDetails.couponEndDate}</td>
           	</c:when>
           	<c:otherwise>
           		<td colspan="2" align="left">Valid From Date to Date</td>
           	</c:otherwise>
           </c:choose>
      </tr>
	  <tr>
	  <td width="87%" colspan="2" align="center" valign="top" class="wrpWord">
          	${sessionScope.couponDetails.couponLongDesc} 
          </td> </tr>  
 
	  	  
	  <tr>
	  <td class="txt-center">
	  <input type="button" value="Claim" class="ipn-btn" />
	  </td>
	  <td class="txt-center">
	   <input type="button" value="Redeem" class="ipn-btn" />
	  </td>
	  </tr>
	  
	  <tr>
	 <td align="center" colspan="2">
	  <input class="ipn-btn" type="button" value="Locations">
	  </td>
	  </tr>
  <!--    <tr>
	     <td colspan="3" align="left">
	     	<ul class="sublinkList">
	       		<li class="linkBoldHdr">
	       			<a href="javascript:void(0);">Qualifying Products<span>&nbsp;</span></a>
	       		</li>
	       		<li>
	       			<a href="javascript:void(0);">This is a web coupon, Click here to view<span>&nbsp;</span></a>
	       		</li>
	     	</ul>
	     </td>
      </tr>
      <tr>
      	<td colspan="3" align="left">
	      	<div class="btmPadding imgtabFix">
	      		<a href="javascript:void(0);" class="dsbl">
	      			<img src="../images/redeemCoupon@2x.png" alt="mark as used" width="140" height="30"/>
	      		</a><a href="javascript:void(0);">
	      				<img src="../images/claimCoupons@2x.png" alt="clip coupon" width="140" height="30" />
	      			</a>
	      		
	      			
	      		
	      	</div>
      	</td>
      </tr>
      <tr>
      	<td colspan="3" align="left" class="redeemRow"><div class="redeemIcon">&nbsp;</div></td>
      </tr> -->
     </table>
	</div>
   </div>
   
  </form:form>
  <div id="TabBarIphone">
    <ul id="iphoneTab">
      <li><img src="../images/tab_btn_up_sortfilter.png" alt="Sort Filter" width="80" height="50" /></li>
	  <li><img src="../images/tab_btn_up_nearby.png" alt="NearBy" width="80" height="50" /></li>
      <li><img src="../images/tab_btn_up_filters.png" alt="Filters" width="80" height="50" /></li>
      <li><img src="../images/my_deals.png" alt="MyDeals" width="80" height="50" /></li>
    </ul>
  </div>      
  
 </div>
       <div class="clear"></div>

	<div align="center">
		<input class="btn" onclick="javascript:history.back()" type="button" value="Back" name="Cancel2" title="Back"/>
	</div>
</body>

</html>

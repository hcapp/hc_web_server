<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<script src="/ScanSeeWeb/ckeditor/config.js"></script>
<script>
	$(document).ready(function() {
		
		 CKEDITOR.config.uiColor = '#FFFFFF'; 
		 CKEDITOR.replace( 'spclOffrlngDesc', {
			 	width:"660",
			 	toolbar :
					[
					    /* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
					    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
					    { name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
					    '/',
					    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
					    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
					    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
					    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
					    '/',
					    { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
					    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
					    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
					    { name: 'basicstyles', items : [ 'Bold','Italic','Underline'] },
						{ name: 'paragraph',   items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
						{ name: 'styles',    items : [ 'Styles','Format' ] },
						'/',
						{ name: 'tools',    items : [ 'Font','FontSize','RemoveFormat'] },
						{ name: 'colors',      items : [ 'BGColor' ] },
						{ name: 'paragraph',   items : [ 'Outdent','Indent'] },		
						{ name: 'links',       items : [ 'Link','Unlink'] }
					],
		 			removePlugins : 'resize'
	        });
		  CKEDITOR.replace( 'spclOffrshrtDesc', {
			 width:"660",
			 toolbar :
					[
						{ name: 'basicstyles', items : [ 'Bold','Italic','Underline'] },
						{ name: 'paragraph',   items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
						{ name: 'styles',    items : [ 'Styles','Format' ] },
						'/',
						{ name: 'tools',    items : [ 'Font','FontSize','RemoveFormat'] },
						{ name: 'colors',      items : [ 'BGColor' ] },
						{ name: 'paragraph',   items : [ 'Outdent','Indent'] },		
						{ name: 'links',       items : [ 'Link','Unlink'] }
					],
			removePlugins : 'resize'
	        }); 
	

		
		
		$('#splOfferLocId option').click(function() {
			var totOpt = $('#splOfferLocId option').length;
			var totOptSlctd = $('#splOfferLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
		$("#splOfferLocId").change(function() {
			var totOpt = $('#splOfferLocId option').length;
			var totOptSlctd = $('#splOfferLocId option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
		$("#SplOffrStrtDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$('.ui-datepicker-trigger').css("padding-left", "5px");
		$("#SplOffrEndDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$('.ui-datepicker-trigger').css("padding-left", "5px");
		
		//populateDurationPeriod_EditSpecialOfferPage(document.getElementById("durationCheck").checked);
		
	});
	function selectsplLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;
		var vLocVal = document.getElementById("splOfferLocId");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function selectAttchLinkLocations() {
		var vLocID = document.createCustomPage.hiddenLocId.value;
		var vLocVal = document.getElementById("splOfferAttatchLinkLocId");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		if (vLocVal.length == vCategoryList.length) {
			document.getElementById('chkAllLoc').checked = true;
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function showPagePreview(pageTyp) {

		var createForm = document.createCustomPage;
		var imagePath = $("#customPageImg").attr('src')
		var retStoreName = '${sessionScope.retailStoreName}';
		var retStoreImage = '${sessionScope.retailStoreImage}';
		var specialImagepath = $("#customSplPageImg").attr('src');
		// Do validation before you call for Preview

		if (pageTyp == 'SplOffer') {

			//For specail offer page validtion

			var title = document.createCustomPage.splOfferTitle.value;
			var locid = document.createCustomPage.splOfferLocId.value;
			var retShortDesc = document.createCustomPage.spclOffrshrtDesc.value;
			var retPageDesc = document.createCustomPage.splOfferLongDescription.value;
			var specialStartDate = document.createCustomPage.SplOffrStrtDT.value;
			var specialEndDate = document.createCustomPage.SplOffrEndDT.value;

			if (title == "") {
				alert('Please enter title');
				return;
			} else if (locid = "" || locid == '0') {
				alert('Please select location');
				return;
			} else if (retPageDesc == "") {
				alert('Please enter  page description');
				return;
			} else if (retShortDesc == "") {
				alert('Please enter  short description');
				return;
			} else if (specialStartDate == "") {
				alert('Please enter special offer start date');
				return;
			} /* else if (specialEndDate == "") {
				alert('Please enter special offer end date');
				return;
			} */ else {
				showSpecialOffPagePview(createForm, specialImagepath);
			}

		}
	}

	function selectRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("splOfferLocId");
		var vCategoryList = [];
		if (vLocID != "null" && vLocID != "") {
			vCategoryList = vLocID.split(",");
		}
		if (vLocVal.length == vCategoryList.length) {
			document.getElementById('chkAllLoc').checked = true;
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for ( var j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("splOfferLocId");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
	
	function populateDurationPeriod_EditSpecialOfferPage(checked)
	{
		if(checked == true)
		{
			// $('#SplOffrStrtDT').val("");			 
			 $('#SplOffrEndDT').val("");
			 
			// $("#splofferStartTimeHrs").val("00");
			// $("#splofferStartTimeMin").val("00");
			 $("#splOfferEndTimeHrs").val("00");
			 $("#splOfferEndTimeMin").val("00");
			 
			// $("#SplOffrStrtDT").css({"background-color": "#494949"});
			 $("#SplOffrEndDT").css({"background-color": "#494949"});
			// $("#SplOffrStrtDT").attr("disabled", "disabled");       
			 $("#SplOffrEndDT").attr("disabled", "disabled");
			// $('#splofferStartTimeHrs').attr('disabled', 'disabled');
			// $('#splofferStartTimeMin').attr('disabled', 'disabled');
			 $('#splOfferEndTimeHrs').attr('disabled', 'disabled');
			 $('#splOfferEndTimeMin').attr('disabled', 'disabled');
			// $("#SplOffrStrtDT").datepicker( "disable" );
			 $("#SplOffrEndDT").datepicker( "disable" );
		}
		else
		{
		//	$("#SplOffrStrtDT").css({"background-color": "#000"});
			$("#SplOffrEndDT").css({"background-color": "#000"});
		//	$("#SplOffrStrtDT").removeAttr('disabled');
			$("#SplOffrEndDT").removeAttr('disabled');
		//	$('#splofferStartTimeHrs').attr('disabled', false);
		//	$('#splofferStartTimeMin').attr('disabled', false);
			$('#splOfferEndTimeHrs').attr('disabled', false);
			$('#splOfferEndTimeMin').attr('disabled', false);
		//	$("#SplOffrStrtDT").datepicker( "enable" );
			$("#SplOffrEndDT").datepicker( "enable" );
		}		
	}
</script>
<div class="shdwBg" id="content" style="min-height: 73px;">
	<%@include file="retailerLeftNavigation.jsp"%>
	<div class="grdSec rtContPnl floatR">
		<div id="secInfo">Edit Special Offer Page</div>
		<form:form commandName="createCustomPage" id="createCustomPage"
			name="createCustomPage" enctype="multipart/form-data"
			action="uploadtempretimg.htm" acceptCharset="ISO-8859-1">
			<form:hidden path="pageId" id="pageId" />
			<form:hidden path="hiddenLocId" />
			<form:hidden path="retailerImg" id="retailerImg" />
			<form:hidden path="landigPageType" value="Special Offer" />
			<form:hidden path="imageName" id="imageName" />
			<input type="hidden" id="uploadBtn" name="uploadBtn">
			<form:hidden path="viewName" value="editSpecialOfferPage" />
			<input type="hidden" id="uploadedFileLength"
				name="uploadedFileLength">
			<input type="hidden" name="editSlctSpclOffr" id="editSlctSpclOffr"
				value="Special Offer" />
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="spclOffr" class="lblSpan cstmpg" style="display: table;">
				<tbody>
					<tr>
						<td class="header" colspan="4">Special Offer Page:
						 <div class="sub-actn"><a href="#" onclick="location='/ScanSeeWeb/retailer/specialoffinstructions.htm'">View Instructions</a> </div>	
						
						</td>
					</tr>
					<tr class="AttachLink">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="atchLinkTitle" class="mand">Title</label></td>
						<td colspan="3"><form:input path="splOfferTitle" type="text"  tabindex="1"
								id="splOfferAttatchLinkTitle" maxlength="1000" style=" width: 656px;"/> <form:errors
								path="splOfferTitle" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr class="AttachLink">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="spclOffrML" class="mand">Location(s)</label></td>
						<td colspan="1"><label> <form:select
									class="txtAreaBox" path="splOfferLocId" id="splOfferLocId"
									multiple="true">
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>
								</form:select> </label>
								<br/>
								<form:label path="retCreatedPageLocId">Hold Ctrl to select more than one location</form:label>
								<br/> 
								<form:errors path="splOfferLocId" cssStyle="color:red">
							</form:errors>
						</td>
						<td colspan="2" align="left" valign="top" class="Label"><label>
								<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
								onclick="SelectAllLocation(this.checked);" tabindex="3" />
								Select All Locations </label></td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="hdRp" class="mand">Photo</label>
						</td>
						<td colspan="2"><label for="hdSp"></label> <label><img
								id="customSplPageImg" width="70" height="70" alt="upload"
								src="${customPageRetImgPath}"
								onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';">
						</label><span class="topPadding forceBlock"><label for="trgrUpldSplPage"><input
									type="button" value="Choose File" title="Choose File"
									id="trgrUpldSplPageBtn" class="btn trgrUpldSplPage"> <form:input
										type="file" id="trgrUpldSplPage" path="retImage" tabindex="4" /> </label><label
								id="customPageImgErr" style="color: red; font-style: 45"></label>
						</span> <form:errors path="retImage" cssStyle="color:red" />
						</td>
						<td>
							<ul class="actnLst">

								<li><strong>Upload Image Size:</strong><br>Suggested Minimum
									Size:70px/70px<br>Maximum Size:800px/600px<br><!--  Maximum Size:950px/1024px --></li>
							</ul></td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="spclOffrshrtDesc" class="mand">Short Description</label>
						</td>
						<td colspan="3"><form:textarea path="splOfferShortDescription" 
								id="spclOffrshrtDesc" name="spclOffrshrtDesc" tabindex="5"/> <form:errors
								path="splOfferShortDescription" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="spclOffrlngDesc" class="mand">Long Description</label>
						</td>
						<td colspan="3"><form:textarea path="splOfferLongDescription" 
								id="spclOffrlngDesc" name="spclOffrlngDesc" tabindex="6" /> <form:errors
								path="splOfferLongDescription" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr class="grdTbl">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="spclOffrML" class="mand">Start Date</label>
						</td>
						<td align="left"
							style="border-right: 1px solid rgb(218, 218, 218);"><form:input
								path="splOfferStartDate" type="text" id="SplOffrStrtDT"
								class="textboxDate" tabindex="7"/> <form:errors path="splOfferStartDate"
								cssStyle="color:red">
							</form:errors>
						</td>
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);"><label
							for="spclOffrML">End Date</label>
						</td>
						<td><form:input path="splOfferEndDate" type="text"
								id="SplOffrEndDT" class="textboxDate" name="EndDT" tabindex="8"/> <form:errors
								path="splOfferEndDate" cssStyle="color:red">
							</form:errors><br><span
						class="instTxt nonBlk">[End date is not required]</span>
						</td>
					</tr>
					<tr>
						<td class="Label"><label for="cst">Start Time</label></td>
						<td><form:select path="splofferStartTimeHrs"
								class="slctSmall" tabindex="9">
								<form:options items="${DealStartHours}" />
							</form:select> Hrs <form:select path="splofferStartTimeMin" class="slctSmall" tabindex="10">
								<form:options items="${DealStartMinutes}" />
							</form:select> Mins</td>
						<td class="Label"><label for="cet">End Time</label>
						</td>
						<td><form:select path="splOfferEndTimeHrs" class="slctSmall">
								<form:options items="${DealStartHours}" tabindex="11"/>
							</form:select> Hrs <form:select path="splOfferEndTimeMin" class="slctSmall" tabindex="12">
								<form:options items="${DealStartMinutes}" />
							</form:select> Mins</td>
					</tr>
						<tr>
							<td class="Label" ><label for="keywords"></label>Keywords</td>
								<td  colspan="4"> <form:textarea id="keywords" 
										class="txtAreaSmall" rows="5" cols="45" path="keyword"
										tabindex="13" cssStyle="height:60px;" /></td>
							
							</tr>
					<tr class="cmnActn" style="display: table-row;">
						<td class="Label"
							style="border-right: 1px solid rgb(218, 218, 218);">&nbsp;</td>
						<td colspan="3" align="right"><input class="btn"
								onclick="location='/ScanSeeWeb/retailer/specialOffer.htm'" value="Back" type="button" value="Back" type="button"
							name="Back" tabindex="14"/> <input type="button" title="Preview"
							value="Preview" class="btn"
							onclick="javascript:showPagePreview('SplOffer')" tabindex="15"/> <input
							type="button" onclick="updateSpecialOfferPage();" value="Save"
							title="Save" class="btn" tabindex="16" />
						</td>
					</tr>

				</tbody>
			</table>
			<div class="ifrmPopupPannelImage" id="ifrmPopup" style="display: none;">
				<div class="headerIframe">
					<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
						alt="close"
						onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
						title="Click here to Close" align="middle" /> <span
						id="popupHeader"></span>
				</div>
				<iframe frameborder="0" scrolling="no" id="ifrm" src=""
					height="100%" allowtransparency="yes" width="100%"
					style="background-color: White"> </iframe>
			</div>
		</form:form>
	</div>
</div>
<div class="clear"></div>
<script type="text/javascript">
	$('#trgrUpldSplPage')
			.bind(
					'change',
					function() {
						$("#uploadBtn").val("trgrUpldBtn");

						$("#createCustomPage")
								.ajaxForm(
										{

											success : function(response) {

												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue

												if (imgRes == 'UploadLogoMaxSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#customPageImgErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {

													//$('#customPageImg').attr("src",imgRes);
													//	$('#customImg').attr("src", imgRes);
													$('#customPageImgErr')
															.text("");
													var substr = imgRes
															.split('|');

													if (substr[0] == 'ValidImageDimention') {
														var imgName = substr[1];
														$('#retailerImg').val(imgName);
														$('#customSplPageImg')
																.attr(
																		"src",
																		substr[2]);

													} else {
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}

												//alert(response.getElementsByTagName('imageScr')[0].firstChild.nodeValue)
												//$('#customSplPageImg')
												//	.attr(
												//	"src",
												//			response
												//				.getElementsByTagName('imageScr')[0].firstChild.nodeValue);
											}

										}).submit();

					});
</script>
<script>
	selectRtlLocations();
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>

<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script type="text/javascript">
	function loadReport(reportName) {
		var inpFromDate = document.getElementById("fromDate").value;
		var inpToDate = document.getElementById("toDate").value;
		var today = new Date();
		var fromDate = new Date(inpFromDate);
		var toDate = new Date(inpToDate);

		if (inpFromDate == "") {
			alert("Please Select From Date");
		} else if (inpToDate == "") {
			alert("Please Select To Date");
		} else if (fromDate > toDate) {
			alert('From Date should be less than or equal to To Date')

		} else if (toDate > today) {
			alert('To date should be less than or equal to Current Date');
		} else {
			if (reportName == 'Overview') {
				document.dashboardform.action = "dashboard.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();
			} else if (reportName == 'Demographics') {
				document.dashboardform.action = "getdemographicsrep.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();
			} else if (reportName == 'AppSite') {
				document.dashboardform.action = "getappsitereport.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();
			} else if (reportName == 'Coupon&Deals') {
				document.dashboardform.action = "gethdcouponreport.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();
			}
		}
	}

	function viewReportData() {

		var inpFromDate = document.getElementById("fromDate").value;
		var inpToDate = document.getElementById("toDate").value;
		var today = new Date();
		var fromDate = new Date(inpFromDate);
		var toDate = new Date(inpToDate);

		if (inpFromDate == "") {
			alert("Please Select From Date");
		} else if (inpToDate == "") {
			alert("Please Select To Date");
		} else if (fromDate > toDate) {
			alert('From Date should be less than or equal to To Date')

		} else if (toDate > today) {
			alert('To date should be less than or equal to Current Date');
		} else {
			var reportName = $("#insightTabs li a.active").attr("name");

			if (reportName == 'Overview') {
				document.dashboardform.action = "dashboard.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();
			} else if (reportName == 'Demographics') {
				document.dashboardform.action = "getdemographicsrep.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();
			} else if (reportName == 'AppSite') {
				document.dashboardform.action = "getappsitereport.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();
			}else if (reportName == 'Coupon&Deals') {
				document.dashboardform.action = "gethdcouponreport.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();
			}
		}

	}
	function exportReport() {

		var inpFromDate = document.getElementById("fromDate").value;
		var inpToDate = document.getElementById("toDate").value;
		var today = new Date();
		var fromDate = new Date(inpFromDate);
		var toDate = new Date(inpToDate);

		if (inpFromDate == "") {
			alert("Please Select From Date");
		} else if (inpToDate == "") {
			alert("Please Select To Date");
		} else if (fromDate > toDate) {
			alert('From Date should be less than or equal to To Date')

		} else if (toDate > today) {
			alert('To date should be less than or equal to Current Date');
		} else {
			var reportName = $("#insightTabs li a.active").attr("name");
			var selValue = $('#exprtOptn :selected').val();

			if (selValue != 'Please Select') {
				document.dashboardform.reportName.value = reportName;
				document.dashboardform.exportFormat.value = selValue;
				document.dashboardform.action = "exportreport.htm";
				document.dashboardform.method = "POST";
				document.dashboardform.submit();

			}
		}

	}

	function loadDemGhyRepOnChange() {

		var inpFromDate = document.getElementById("fromDate").value;
		var inpToDate = document.getElementById("toDate").value;
		var today = new Date();
		var fromDate = new Date(inpFromDate);
		var toDate = new Date(inpToDate);

		if (inpFromDate == "") {
			alert("Please Select From Date");
		} else if (inpToDate == "") {
			alert("Please Select To Date");
		} else if (fromDate > toDate) {
			alert('From Date should be less than or equal to To Date')

		} else if (toDate > today) {
			alert('To date should be less than or equal to Current Date');
		} else {

			document.dashboardform.action = "getdemographicsrep.htm";
			document.dashboardform.method = "POST";
			document.dashboardform.submit();

		}

	}
</script>

<script>
	$(document).ready(function() {
		$(window).load(function() {
     		$('table td div').filter(function() {     return $(this).text() == "CstmTbl"; })
.parent()
.parent()
.parent()
.parent()
.addClass('dshbrdTbl')
.wrap("<div style='height:300px; overflow-y:scroll'></div>");
$('table td div').filter(function() {     return $(this).text() == "CstmTbl"; })
.parent()
.parent()
.parent()
.parent().find('tr td:first-child').css("display","none");

var test = $(".dshbrdTbl tr:first td").length;
});
		$("#fromDate").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});

		$("#toDate").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});

		var reportName = $("#insightTabs li a.active").attr("name");
		if (reportName == "Overview") {
			$('#overviewContArea').html('${sessionScope.reportContent}');
		} else if (reportName == "AppSite") {
			$('#appSiteContArea').html('${sessionScope.reportContent}');
		} else if (reportName == "Demographics") {
			$('#demographicsContArea').html('${sessionScope.reportContent}');
		}else if (reportName == "Coupon&Deals") {
			$('#couponDealContArea').html('${sessionScope.reportContent}');
		}

	});
</script>
<div id="wrapper">
	<div id="content">
		<%@include file="retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR">
			<div class="grpTitles">
				<h1 class="mainTitle">Insight & Analytics</h1>
			</div>

			<form:form commandName="dashboardform" name="dashboardform">
				<form:hidden path="exportFormat" />
				<form:hidden path="reportName" />

				<div class="section">
					<div class="grdSec brdrTop">
						<table class="tranTbl" border="0" cellspacing="0" cellpadding="0"
							width="100%">
							<tbody>
								<tr>
									<td class="header" width="34%">Date Selection: From <form:input
											path="fromDate" id="fromDate" class="textboxSmaller "></form:input>
									</td>
									<td class="header" width="21%">To <form:input id="toDate"
											path="toDate" class="textboxSmaller " size="11" name="toDate"></form:input>
									</td>
									<td width="14%" align="center" class="header"><label>
											<input type="button" name="button" id="viewReport"
											onclick="viewReportData();" value="View Report"
											class="btn zeroPadding" /> </label>
									</td>
									<td width="31%" class="header">Export <select
										onchange="exportReport();" id="exprtOptn"
										class="textboxMedium" name="exprtOptn">
											<option>Please Select</option>
											<option>PDF</option>
											<option>EXCEL</option>
									</select>
									</td>
								</tr>
							</tbody>
						</table>
						<div id="subnav">
							<ul id="insightTabs">
								<li><a href="#" onclick="loadReport('Overview');"
									name="Overview" class="${sessionScope.overviewTabStyle}" id=""><span>Overview</span>
								</a>
								</li>
								<li><a href="#" id="" name="Demographics"
									class="${sessionScope.demographicsTabStyle}"
									onclick="loadReport('Demographics');"><span>Demographics</span>
								</a>
								</li>
								<li><a href="#" onclick="loadReport('AppSite');"
									name="AppSite" class="${sessionScope.appSiteTabStyle}" id=""><span>AppSite</span>
								</a>
								</li>
								<li><a href="#" onclick="loadReport('Coupon&amp;Deals');"
									name="Coupon&amp;Deals" class="${sessionScope.couponDealTabStyle}" id=""><span>Promotions</span>
								</a>
								</li>
								<!--
								<li><a href="javascript:void(0);" name="Interest"
									class="${sessionScope.interestTabStyle}" id=""><span>Interest</span>
								</a>
								</li>
							-->
							</ul>
						</div>
						<div class="reportCont" id="Overview"
							style="${sessionScope.overviewDivStyle}">

							<div class="contArea" id="overviewContArea"></div>
						</div>
						<div class="reportCont" id="Demographics"
							style="${sessionScope.demographicsDivStyle}">
							<div class="subHdr">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="brdrLsTbl">
									<tr>
										<td width="50%" class="zeroPadding"><form:select
												onchange="loadDemGhyRepOnChange();" path="moduleId"
												class="textboxMedium">

												<c:forEach items="${sessionScope.reportDropdownList}"
													var="item">
													<form:option value="${item.id}" label="${item.name}" />
												</c:forEach>
											</form:select></td>
									</tr>
								</table>
							</div>
							<div class="contArea" id="demographicsContArea"></div>
						</div>
						<div class="reportCont" id="AppSite"
							style="${sessionScope.appSiteDivStyle}">

							<div class="contArea" id="appSiteContArea"></div>
						</div>
						<div class="reportCont" id="Coupon&amp;Deals"
							style="${sessionScope.couponDealDivStyle}">

							<div class="contArea" id="couponDealContArea"></div>
						</div>
						<div class="reportCont" id="Interest"
							style="${sessionScope.interestDivStyle}">

							<div class="contArea"></div>
						</div>
					</div>
				</div>
			</form:form>
		</div>

	</div>
</div>
<div class="clear"></div>
<script type="text/javascript">
	//onloadReports();
</script>
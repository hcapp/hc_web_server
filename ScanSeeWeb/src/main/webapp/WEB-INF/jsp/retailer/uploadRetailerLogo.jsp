<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!-- <script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script> -->
<!-- <script src="/ScanSeeWeb/scripts/web.js"></script>
	<script src="/ScanSeeWeb/scripts/global.js"></script> -->
<script src="/ScanSeeWeb/scripts/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery.Jcrop.css"
	type="text/css" />
<script type="text/javascript">
	var jcrop_api;
	var xPixel;
	var yPixel;
	var wPixel;
	var hPixel;

	$(document).ready(function() {
		cropImage();

	});

	function readURL(id) {
		var input = document.getElementById("retailerLogo");
		//	alert('Inside ReadURL !!');
		$("#imgId").attr("src", "");
		stopJcrop();
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				//  alert("File Read Complete!!");
				$("#imgId").attr("src", e.target.result);
				// jcrop_api.setImage($("#imgId").attr("src")); 
			};
			reader.readAsDataURL(input.files[0]);
			$("#popup:visible").hide(); //hide popup if it is open
			// e.preventDefault(); // don't follow link
			//  $("#imgId").attr("src", $(this).attr("href")); // replace image src with href from the link that was clicked
			//  alert("Src Value : "+ $("#imgId").attr("src").value);
			$("#popup").fadeIn("fast"); //show popup

			//initJcrop('imgId'); 
			$("#imgId").focus(function() {
				//  	alert("Initiating Cropping!!");
				initJcrop('imgId');
			});
			$("#imgId").focusout(function() {
				//	alert("Disabling Cropping!!");
				stopJcrop();
			});
			setTimeout(function() {
				$('#imgId').focus();
			}, 3000);
		}
	}

	function initJcrop(id) {
		//document.getElementById("retailerLogo").disabled = true;
		jcrop_api = $.Jcrop('#' + id, {

			onSelect : showCoords,
			onChange : showCoords,
			bgColor : 'black',
			bgOpacity : .4,
			aspectRatio : 1,
			minSize : [ 70, 70 ],
			//   maxSize: [ 70, 70 ],
			// boxWidth:   picture_width,
			// boxHeight:  picture_height,
			setSelect : [ 300, 300, 300 + 70, 300 + 70 ]
		});
	}

	function stopJcrop() {
		if (jcrop_api) {
			jcrop_api.destroy();
			return (false);
		}

	}

	function showCoords(c) {
		xPixel = c.x;
		yPixel = c.y;
		wPixel = c.w;
		hPixel = c.h;
		// jQuery('#x').val(c.x);
		// jQuery('#y').val(c.y);
		// jQuery('#x2').val(c.x2);
		// jQuery('#y2').val(c.y2);
		// jQuery('#w').val(c.w);
		// jQuery('#h').val(c.h);
	};

	/*  function enableCropButton()
	 {          
	 	stopJcrop();
	 	document.getElementById("cropButton").disabled = false;
	 }; */

	function saveCroppedImage() {
		//	if (parseInt($('#w').val())) 
		//	{
		/* window.opener.document.getElementById("xId").value = jQuery('#x').val();
		window.opener.document.getElementById("yId").value = jQuery('#y').val();
		window.opener.document.getElementById("wId").value = jQuery('#w').val();
		window.opener.document.getElementById("hId").value = jQuery('#h').val(); */
		//	alert("wPixel : "+wPixel)
		submitCroppedImage();

		//	}
		//	else
		//	{
		//		alert('Please select a crop region then press submit.');
		//	}

	};

	function submitCroppedImage() {
		var logoSrc = "${sessionScope.logoSrc}";
		//	alert("logoSrc : "+logoSrc);
		uploadRetLogoWithCroppedImage(xPixel, yPixel, wPixel, hPixel);
		/* var imagePath = jQuery('#retailerLogo').val();
		alert("Image Path :"+ imagePath)
		if (imagePath == '' || imagePath == 'undefined') {
			alert('Please Select Retailer Logo to upload')
			return false;
		} else {
			var validImage = validateUploadImageFileExt(imagePath);
			if (validImage) {
				uploadRetLogoWithCroppedImage(xPixel,yPixel,wPixel,hPixel);
				//uploadRetLogo();
				window.close();
			} else {
				alert('Image file format should be png')
			}
		}		 */
	}

	function cropImage() {
		//  alert("Start of document load!!");
		$("#popup:visible").hide(); //hide popup if it is open
		$("#popup").fadeIn("fast"); //show popup
		$("#imgId").focus(function() {
			//	alert("Initiating Cropping!!");
			initJcrop('imgId');
		});
		$("#imgId").focusout(function() {
			//	alert("Disabling Cropping!!");
			stopJcrop();
		});
		setTimeout(function() {
			$('#imgId').focus();
		}, 1500);
		//  alert("End of document load!!");
	}
</script>

<div id="wrapper">
	<div id="dockPanel">
		<ul id="prgMtr" class="tabs">
			<!--  <li> <a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee" target="_blank" rel="About ScanSee">About ScanSee</a> </li> -->
			<!-- <li> <a title="Create Profile" href="Retailer/Retailer_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->
			<li><a class="tabActive" title="Upload Ads/Logos"
				href="uploadRetailerLogo.htm" rel="Upload Ads/Logos">Choose Logo</a>
			</li>
			<li><a title="Location Setup" href="addlocation.htm"
				rel="Location Setup">Location Setup</a></li>
			<!-- <li> <a title="Product Setup" href="/ScanSeeWeb/retailer/regbatchuploadretprod.htm" rel="Product Setup">Product Setup</a> </li> -->
			<li><a title="Choose Plan"
				href="/ScanSeeWeb/retailer/retailerchoosePlan.htm" rel="Choose Plan">Choose
					Plan</a></li>
			<li><c:choose>
					<c:when test="${isPaymentDone == null}">
						<a title="Dashboard" href="/ScanSeeWeb/retailer/retailerhome.htm"
							rel="Dashboard">Dashboard</a>
					</c:when>
					<c:otherwise>
						<a title="Dashboard" href="#" rel="Dashboard">Dashboard</a>
					</c:otherwise>
				</c:choose></li>
		</ul>
		<!-- <div class="floatR tglSec" id="tabdPanelDesc">
		<div id="filledGlass">
			<img src="/ScanSeeWeb/images/Step3.png" />
			<div id="nextNav">
				<a href="#"
					onclick="location.href='/ScanSeeWeb/retailer/addlocation.htm'">
					<img class="NextNav_R" alt="Next"
					src="/ScanSeeWeb/images/nextBtn.png" /> <span>Next</span> </a>
			</div>
		</div>
	</div>
	<div id="tabdPanel" class="floatL tglSec">
		<img alt="Flow1" src="/ScanSeeWeb/images/Flow4_ret.png" />
	</div> -->
	</div>
	<!-- <div id="togglePnl"><a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down" width="9" height="8" /> Show Panel</a></div> -->
	<div class="clear"></div>
	<div id="content" class="topMrgn">

		<!--<h2 class="MrgnBtm">Upload Logo</h2>-->
		<div class="grdSec brdrTop">
			<form:form name="retaileruploadlogoinfoform"
				commandName="retaileruploadlogoinfoform"
				action="/ScanSeeWeb/retailer/uploadRetailerLogo.htm"
				enctype="multipart/form-data">
				<form:hidden path="navigation" value="" />
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="grdTbl">
					<tr>
						<td colspan="3" class="header">Please choose company logo if
							available, if not click next.</br> Choosing your company logo will
							allow consumers to view your logo in different areas of the app
							as shown in App View below.
						</td>
					</tr>
					<tr>



						<td width="29%" align="center"><img
							src='<%=session.getAttribute("logosrc")%>' id="retLogo"
							width="70" height="70" alt="Upload Image"
							onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';" />

							<!--<img src="images/hubciti-logo_ret.jpg"--></td>
						<td width="38%"><form:input type="file" path="retailerLogo"
								class="textboxBig" id="retailerLogo" /> <form:hidden
								path="retailerID" id="retailerID" /> <form:errors
								cssStyle="color:red" path="retailerLogo" /> </br> <label
							id="retailerLogoErr" style="color: red; font-style: 45"></label>
							<form:errors cssStyle="color:red" path="retailerLogo" />
							<div style="font-style: 45">
								<label><form:errors cssStyle="color:red" /></td>
						<td width="31%" align="left"><ul class="actnLst">

								<li><strong>Upload Image Size:</strong><br>Suggested
									Minimum Size:70px/70px<br>Maximum Size:800px/600px<br>
								<!--  Maximum Size:950px/1024px --></li>
							</ul></td>

					</tr>

					<tr class="Label">
						<td width="29%" align="center"><span class="Label"> <input
								name="appview" value="Preview" type="button" class="btn"
								onclick="openUploadPreviewPopUp()" title="Preview" />
						</span></td>
						<td colspan="2"><input class="btn mrgnRt"
							onclick="location.href='/ScanSeeWeb/retailer/addlocation.htm'"
							value="Next" type="button" name="next" /></td>

					</tr>
				</table>
				<div class="ifrmPopupPannelImage" id="ifrmPopup"
					style="display: none;">
					<div class="headerIframe">
						<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
							alt="close"
							onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
							title="Click here to Close" align="middle" /> <span
							id="popupHeader"></span>
					</div>
					<iframe frameborder="0" scrolling="no" id="ifrm" src=""
						height="100%" allowtransparency="yes" width="100%"
						style="background-color: White"> </iframe>
				</div>
			</form:form>
		</div>
		<div class="clear"></div>
	</div>
	<script type="text/javascript">
		$('#retailerLogo')
				.bind(
						'change',
						function() {
							document.retaileruploadlogoinfoform.navigation.value = 'registration';
							$("#retaileruploadlogoinfoform")
									.ajaxForm(
											{

												success : function(response) {
													var imgRes = response
															.getElementsByTagName('imageScr')[0].firstChild.nodeValue

													if (imgRes == 'UploadLogoMaxSize') {
														$('#retailerLogoErr')
																.text(
																		"Image Dimension should not exceed Width: 800px Height: 600px");
													} else if (imgRes == 'UploadLogoMinSize') {
														$('#retailerLogoErr')
																.text(
																		"Image Dimension should be Minimum Width: 70px Height: 70px");
													} else {

														$('#retailerLogoErr')
																.text("");

														//$('#retLogo').attr(
														//	"src", imgRes);

														var substr = imgRes
																.split(',');

														if (substr[0] == 'ValidImageDimention') {
															$('#retLogo').attr(
																	"src",
																	substr[1]);
														} else {
															openIframePopupForImage(
																	'ifrmPopup',
																	'ifrm',
																	'/ScanSeeWeb/retailer/cropImage.htm',
																	100, 99.5,
																	'Upload Logo')
														}

													}

												}

											}).submit();

						});

		//Function for limiting the image size.
		/* function readImage(file) {
		
		var reader = new FileReader();
		var image  = new Image();
		
		reader.readAsDataURL(file);  
		reader.onload = function(_file) {
		    image.src    = _file.target.result;              // url.createObjectURL(file);
		    image.onload = function() {
		        var w = this.width,
		            h = this.height,
		            t = file.type,                           // ext only: // file.type.split('/')[1],
		            n = file.name,
		            s = ~~(file.size/1024) +'KB';
		        $('#uploadPreview').append('<img src="'+ this.src +'"> '+w+'x'+h+' '+s+' '+t+' '+n+'<br>');
		    };
		    image.onerror= function() {
		        alert('Invalid file type: '+ file.type);
		    };      
		};
		
		}
		$("#choose").change(function (e) {
		if(this.disabled) return alert('File upload not supported!');
		var F = this.files;
		if(F && F[0]) for(var i=0; i<F.length; i++) readImage( F[i] );
		}); */
	</script>
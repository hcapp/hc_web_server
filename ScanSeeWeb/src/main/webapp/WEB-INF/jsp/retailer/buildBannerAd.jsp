<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	var changeImgDim = '${sessionScope.ChangeImageDim}';

	if (null != changeImgDim && changeImgDim == 'true') {

		$('#bannerADImg').width('320px');
		$('#bannerADImg').height('50px');

	}

	$(document).ready(function() {
		$('#retlocationID option').click(function() {
			var totOpt = $('#retlocationID option').length;
			var totOptSlctd = $('#retlocationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});

		$("#retlocationID").change(function() {
			var totOpt = $('#retlocationID option').length;
			var totOptSlctd = $('#retlocationID option:selected').length;
			if (totOpt == totOptSlctd) {
				$('input[name$="chkAllLoc"]').attr('checked', 'true');
			} else {
				$('input[name$="chkAllLoc"]').removeAttr('checked');
			}
		});
		$("#datepicker1").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});

		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});

		//populateDurationPeriod(document.getElementById("durationCheck").checked);

		$('#ribbonXAdURL').focusin(function() {

			var bannerUrl = $(this).val();
			if (bannerUrl === "") {
				$(this).val("http://");

			}
		});
		$('#ribbonXAdURL').focusout(function() {
			var bannerUrl = $(this).val();
			if (bannerUrl == "" || bannerUrl == "http://") {
				$(this).val("");
			}

		});

	});
</script>

<script type="text/javascript">
	function checkBannerSize(input) {
		var bannerImage = document.getElementById("trgrUpld").value;
		/*if (input.files && input.files[0].size > (100 * 1024)) {
			alert("File too large. Max 100 KB allowed.");
			input.value = null;
		} else */
		if (bannerImage != '') {
			var checkbannerimg = bannerImage.toLowerCase();
			if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Banner image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
				document.buildbanneradform.bannerAdImagePath.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			}
		}
	}

	function validateImage() {
		var bannerImage = document.getElementById("trgrUpld").value;
		if (bannerImage != '') {
			var checkbannerimg = bannerImage.toLowerCase();
			if (!checkbannerimg.match(/(\.png|\.jpeg|\.jpg|\.gif|\.bmp)$/)) {
				alert("You must upload Banner image with following extensions : .png, .gif, .bmp, .jpg, .jpeg");
				document.buildbanneradform.bannerAdImagePath.value = "";
				document.getElementById("trgrUpld").focus();
				return false;
			} else {
				previewBannerRetailerAds();
				return true;
			}
		} else {
			alert("You must upload Banner image file  with  following extensions : .png ");
		}
	}

	function onLoadRetLocationID() {
		var changeImgDim = '${sessionScope.ChangeImageDim}';

		if (null != changeImgDim && changeImgDim == 'true') {

			$('#bannerADImg').width('320px');
			$('#bannerADImg').height('50px');

		}
		var vRetLocID = document.buildbanneradform.retailLocationIDHidden.value;
		var vRetLocVal = document.getElementById("retlocationID");
		var vRetLocList = [];
		if (vRetLocID != "null" && vRetLocID != "") {
			vRetLocList = vRetLocID.split(",");
		}
		if (vRetLocVal.length != 0 && vRetLocList.length != 0) {
			if (vRetLocVal.length == vRetLocList.length) {
				document.getElementById('chkAllLoc').checked = true;
			}
		}

		for ( var i = 0; i < vRetLocVal.length; i++) {
			for (j = 0; j < vRetLocList.length; j++) {
				if (vRetLocVal.options[i].value == vRetLocList[j]) {
					vRetLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function SelectAllLocation(checked) {
		var sel = document.getElementById("retlocationID");
		if (checked == true) {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = true;
			}
		} else {
			for ( var i = 0; i < sel.options.length; i++) {
				sel.options[i].selected = false;
			}
		}
	}
	function showBannerPagePopup() {
		var imgSrc = document.getElementById('bannerADImg').src;
		showBannerPagePreview(imgSrc)
	}
</script>

<div id="wrapper">
	<form:form name="buildbanneradform" commandName="buildbanneradform"
		enctype="multipart/form-data" action="uploadBannerAdsImg.htm"
		acceptCharset="ISO-8859-1">
		<form:hidden path="retailLocationIDHidden" />
		<form:hidden path="strBannerAdImagePath" id="strBannerAdImagePath" />
		<form:hidden path="viewName" value="buildBannerAd" />
		<div id="content" class="shdwBg">
			<%@include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">C. Build Banner</h1>
					<div class="sub-actn sub-link">
						<a href="#"
							onclick="location='/ScanSeeWeb/retailer/addbannerinstructions.htm?bptype=Build'">View
							Instructions</a>
					</div>
				</div>
				<div class="grdSec">
					<div align="center" style="font-style: 45">
						<label><form:errors cssStyle="color:red" /> </label>
					</div>
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl brdrTop">


						<tr>
							<td colspan="4">
								<ul class="imgtxt">
									<li class="floatL"><img
										src="/ScanSeeWeb/images/buildBannerAd.png" alt="buildBanner"
										width="96" height="88" title="image" /></li>
									<li><h3>Choose an image and where you</h3></li>
									<li><h3>would like for your customers to go, it's
											that simple!</h3></li>
								</ul>
							</td>
						</tr>

						<tr>
							<td class="Label"><label for="couponName" class="mand">Banner
									Name</label></td>
							<td colspan="3"><form:input path="advertisementName"
									type="text" name="textfield2" id="couponName" maxlength="100"
									tabindex="3" /> <form:errors path="advertisementName"
									cssStyle="color:red">
								</form:errors></td>
						</tr>

						<tr>
							<td class="Label"><label for="upldImg" class="mand">Banner
									Image</label></td>
							<td colspan="2"><label><img id="bannerADImg"
									alt="upload" src="${sessionScope.bannerImagePath}" height="80"
									width="80"> </label> <span class="topPadding"><label
									for="trgrUpld"> <input type="button"
										value="Choose File" id="trgrUpldBtn" width="350" height="50"
										class="btn trgrUpld" title="Choose File" tabindex="5">
										<form:input path="bannerAdImagePath" type="file"
											class="textboxBig" id="trgrUpld"
											onchange="checkBannerSize(this);" /> <form:errors
											path="bannerAdImagePath" cssStyle="color:red"></form:errors>
								</label><label id="bannerAdImagePathErr"
									style="color: red; font-style: 45"></label> </span></td>
							<td>
								<ul class="actnLst">

									<li><strong>Upload Image Size:</strong><br>Suggested
										Minimum Size:320px/50px<br>Maximum Size:800px/600px<br>
									<!--  Maximum Size:950px/1024px --></li>
								</ul>
							</td>
						</tr>
						<c:if test="${!sessionScope.ban}">
						<tr>
							
									<td width="19%" align="left" valign="top" class="Label"><label
										for="locationId" class="mand">Location(s)</label></td>
									<td>
										<div id="retailLocation">

											<form:select path="retailLocationIds" class="txtAreaBox"
												size="10" id="retlocationID" multiple="true" tabindex="1">
												<c:forEach items="${sessionScope.retLocationList}" var="s">
													<form:option value="${s.retailerLocationID}"
														label="${s.address1}" />
												</c:forEach>

											</form:select>
											<br />
											<form:label path="retailLocationIds">Hold Ctrl to select more than one location</form:label>
											<br />
											<form:errors path="retailLocationIds" cssStyle="color:red">
											</form:errors>
										</div>
									</td>


									<td colspan="2" align="left" valign="top" class="Label"><label>
											<input type="checkbox" name="chkAllLoc" id="chkAllLoc"
											onclick="SelectAllLocation(this.checked);" tabindex="2" />
											Select All Locations
									</label> <br> <br> <c:if test="${message ne null }">
											<div id="message">
												<center>
													<h2>
														<font color="red"><c:out value="${message}" /> </font>
													</h2>
												</center>
											</div>
										</c:if></td>
								
						</tr>
						</c:if>


						<tr>
							<td class="Label"><label for="couponName" class="">Banner
									Web Link</label></td>
							<td width="33%" colspan="3"><form:input path="ribbonXAdURL"
									type="text" id="ribbonXAdURL" tabindex="4" /> <form:errors
									path="ribbonXAdURL" cssStyle="color:red">
								</form:errors></td>
						</tr>

						<tr>
							<td class="Label"><label for="strtDT" class="mand">Add
									Start Date</label></td>
							<td width="31%" align="left"><form:input
									path="advertisementDate" id="datepicker1" class="textboxDate"
									name="csd" title="Calendar View" tabindex="6" /> <form:errors
									path="advertisementDate" cssStyle="color:red">
								</form:errors>(mm/dd/yyyy)</td>
							<td width="16%" class="Label"><label for="endDT">Add
									End Date</label></td>
							<td width="34%" align="left"><form:input
									path="advertisementEndDate" id="datepicker2"
									class="textboxDate" name="csd" title="Calendar View"
									tabindex="7" /> <form:errors path="advertisementEndDate"
									cssStyle="color:red">
								</form:errors>(mm/dd/yyyy) <br>
							<span class="instTxt nonBlk">[End date is not required]</span></td>
						</tr>
						<!--  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td align="left"><form:checkbox path="indefiniteAdDurationFlag" id="durationCheck" name="durationCheck" onchange="populateDurationPeriod(this.checked);"/> No end date
							</td>
							
						</tr>-->
					</table>
					<div class="ifrmPopupPannelImage" id="ifrmPopup"
						style="display: none;">
						<div class="headerIframe">
							<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
								alt="close"
								onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
								title="Click here to Close" align="middle" /> <span
								id="popupHeader"></span>
						</div>
						<iframe frameborder="0" scrolling="no" id="ifrm" src=""
							height="100%" allowtransparency="yes" width="100%"
							style="background-color: White"> </iframe>
					</div>
					<div class="navTabSec mrgnRt" align="right">
						<input class="btn"
							onclick="location='/ScanSeeWeb/retailer/managebannerads.htm'"
							value="Back" type="Button" name="Back" title="Back" tabindex="8" />
						<input class="btn" onclick="showBannerPagePopup();"
							value="Preview" type="button" name="Preview" title="Preview"
							tabindex="9" /> <input class="btn" value="Submit" type="button"
							onclick="submitBannerAds();" name="Cancel" title="Submit"
							tabindex="10" />
					</div>


				</div>

				<div class="triContpnl MrgnTop">
					<div class="contBlkDsply">
						<span><a href="#"><img
								src="/ScanSeeWeb/images/BBiphone1.png" alt="BBiphone1"
								width="238" height="354" tabindex="11" title="image" /> </a> </span>
					</div>
					<div class="contBlkDsply">
						<img src="/ScanSeeWeb/images/BBiphone2.png" alt="BBiphone2"
							width="238" height="354" tabindex="12" title="image" />
					</div>
					<div class="contBlkDsply">
						<img src="/ScanSeeWeb/images/BBiphone3.png" alt="BBiphone3"
							width="238" height="354" tabindex="13" title="image" />
					</div>
				</div>

				<div class="clear"></div>
	</form:form>
</div>
<div class="clear"></div>
<script>
	onLoadRetLocationID();
</script>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {
						/*show progress bar : ETA for Web 1.3*/
						//showProgressBar();/* Commented to fix body scroll disable issue*/
						/*End*/
						$("#uploadBtn").val("trgrUpldBtn")
						$.ajaxSetup({
							cache : false
						});
						$("#buildbanneradform")
								.ajaxForm(
										{
											success : function(response) {
												$('#loading-image').css(
														"visibility", "hidden");
												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue
												if (imgRes == 'UploadLogoMaxSize') {
													$('#bannerAdImagePathErr')
															.text(
																	"Image Dimension should not exceed Width: 800px Height:600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#bannerAdImagePathErr')
															.text(
																	"Image Dimension should be Minimum Width: 320px Height: 50px");
												} else {

													$('#bannerAdImagePathErr')
															.text("");
													var substr = imgRes
															.split('|');

													if (substr[0] == 'ValidImageDimention') {
														$('#bannerADImg')
																.width('320px');
														$('#bannerADImg')
																.height('50px');
														var imgName = substr[1];
														$(
																'#strBannerAdImagePath')
																.val(imgName);
														$('#bannerADImg').attr(
																"src",
																substr[2]);
													} else {
														//$('body').css("overflow-y","hidden");
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImageGeneral.htm',
																100, 99.5,
																'Crop Image');
													}
												}
											}
										}).submit();

					});
</script>
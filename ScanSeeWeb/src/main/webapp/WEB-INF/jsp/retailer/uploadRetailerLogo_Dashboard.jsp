<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<div id="wrapper">

	<form:form name="retaileruploadlogoinfoform"
		commandName="retaileruploadlogoinfoform"
		action="/ScanSeeWeb/retailer/uploadRetailerLogo.htm"
		enctype="multipart/form-data">

		
		<div id="content" class="shdwBg">
			<%@ include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle">A. Choose Logo
						
					</h1>
				<div class="sub-actn sub-link"><a href="#" onclick="location='/ScanSeeWeb/retailer/uploadRetailerLogo/addlogoinstructions.htm'">View Instructions</a> </div>
				</div>
				<div class="section">
					<div class="grdSec brdrTop">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="grdTbl">
							<tr>
								<td width="29%" align="center"><img
									src="/ScanSeeWeb/images/yourLogo.png" alt="" width="109"
									height="108" /></td>
								<td colspan="2" align="left" class="txtSet">
								
								
								
								<h4>
										Your Logo appears many places in your AppSite<sup class="smallsup">TM</sup> and
									</h4>
									<h4>helps drive brand impressions and awareness.</h4>
									<h4>Fear Not!</h4>
									<h4>If you can locate existing files on your computer and
									</h4>
									<h4>can type, you can do this! We make it easy!</h4></td>
							</tr>
							<tr>
								<td width="29%" align="center"><img id="retLogo"
									src='<%=session.getAttribute("logosrc")%>' width="70"
									height="70" alt="Upload Image"
									onerror="this.src = '/ScanSeeWeb/images/upload_imageRtlr.png';" />
								</td>
								<td width="34%" align="left"><form:input type="file"
										path="retailerLogo" class="textboxBig" id="retailerLogo" /> <form:hidden
										path="retailerID" id="retailerID" /> <form:hidden
										path="navigation" value="" /> <input type="hidden"
									name="dashBoardFlag" id="dashBoardFlag" value="fromDashBoard" />

									</br> <label id="retailerLogoErr" style="color: red; font-style: 45"></label>
									<form:errors cssStyle="color:red" path="retailerLogo" />
									<div style="font-style: 45">
										<form:errors cssStyle="color:red" />
									</div></td>
								<td width="37%" align="left"><ul class="actnLst">
										<li><strong>Upload Image Size:</strong><br>Suggested
											Minimum Size:70px/70px<br>Maximum Size:800px/600px<br>
											<!--  Maximum Size:950px/1024px --></li>

										<!--  Maximum Size:950px/1024px -->
									</ul></td>
							</tr>
							<tr class="Label">
								<td width="29%" align="center"><span class="Label">
										<input name="appview" value="Preview" type="button"
										class="btn" onclick="openUploadPreviewPopUp_forDashboard()"
										title="Preview Logo" />
								</span></td>
								<td colspan="2"></td>
							</tr>
						</table>
						<div class="ifrmPopupPannelImage" id="ifrmPopup"
							style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</form:form>


</div>
<script type="text/javascript">
	$('#retailerLogo')
			.bind(
					'change',
					function() {

						document.retaileruploadlogoinfoform.navigation.value = 'fromDashBoard';

						$("#retaileruploadlogoinfoform")
								.ajaxForm(
										{

											success : function(response) {

												var imgRes = response
														.getElementsByTagName('imageScr')[0].firstChild.nodeValue

												if (imgRes == 'UploadLogoMaxSize') {
													$('#retailerLogoErr')
															.text(
																	"Please select another image. Dimensions can not exceed Width: 800px, Height: 600px");
												} else if (imgRes == 'UploadLogoMinSize') {
													$('#retailerLogoErr')
															.text(
																	"Image Dimension should be Minimum Width: 70px Height: 70px");
												} else {
													$('#retailerLogoErr').text(
															"");
													var substr = imgRes
															.split(',');

													if (substr[0] == 'ValidImageDimention') {
														$('#retLogo').attr(
																"src",
																substr[1]);
													} else {
														openIframePopupForImage(
																'ifrmPopup',
																'ifrm',
																'/ScanSeeWeb/retailer/cropImage.htm',
																100, 99.5,
																'Upload Logo')
													}
												}
											}

										}).submit();

					});
</script>


















<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- below conditional css is used for Manage product grid screen for IE 9 -->
<!--[if lte IE 9]>
<style type="text/css" rel="stylesheet">
.editorTbl td span.inputCntrl {
    display: block!important;
    margin-top: 10px;
}
</style>
<![endif]-->
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet" type="text/css">
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.form.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/cityAutocomplete.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/zipcodeAutocomplete.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/ckeditor/ckeditor.js"></script>
<script src="/ScanSeeWeb/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$("input[type='radio'][name='isRetLoc']").click(function(){
							var selectedOpt = $(this).val();
							if(selectedOpt === "no") {
								$(".OrgAddress").show();
							} else {
								$(".OrgAddress").hide();
							}
						});
						$("input[type='radio'][name='isRetLoc']:checked").trigger('click');
						
						$("#City").live("keydown", function(e) {
							cityAutocomplete('postalCode');
						});
						
						var geoError = $("#geoError").val();
						if (geoError == 'true') {
							//if (geoError) {
							$("#dispLatLang").show();
						} else {
							$("#dispLatLang").hide();
						}
						
						$("#datepicker1,#datepicker2,#datepicker3,#datepicker4")
								.datepicker(
										{
											showOn : 'both',
											buttonImageOnly : true,
											buttonText : 'Click to show the calendar',
											buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
										});

						$("input[name='isEventTied']").change(function() {
							var frEvntId = $(this).attr("id");
							if (frEvntId == "fndYes") {
								$("." + frEvntId).show();
								var eventAssociatedIds = document.addeditfundraiserform.hidEventAssociatedIds.value;
								var arr = eventAssociatedIds.split(',');	
								jQuery.each(arr, function(i, val) { 
									$("input[name='eventAssociatedIds'][value='" + val + "']").attr("checked",true);
									//$("#eventCategory option[value='" + val + "']").prop('selected', 'selected');
								}); 
							} else {
								$(".fndYes").hide()
							}
						});
						$('input[name="isEventTied"]:checked').trigger('change');
						
						CKEDITOR.on('instanceCreated', function(e) {
							//	alert("q : "+e.editor.name);
							var editorName = e.editor.name;
							document.getElementById('longDescription').innerHTML = '';
						});

						CKEDITOR.config.uiColor = '#FFFFFF';
						CKEDITOR.replace('longDescription',{
											/* filebrowserBrowseUrl: '/browser/browse.php', */
											/* filebrowserImageBrowseUrl : '/ScanSeeWeb/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://localhost:8080/ScanSeeWeb/ckeditor/filemanager/connectors/php/connector.php', */
											/* filebrowserUploadUrl: 'http://localhost:8080/ScanSeeWeb/ckeditor/filemanager/connectors/php/upload.php?Type=Image', */
											/* filebrowserImageUploadUrl: '/uploader/upload.php?type=Images', */
											extraPlugins : 'onchange',
											width : "100%",
											toolbar : [
													/* { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
													{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
													{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
													'/',
													{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
													{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
													{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
													{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
													'/',
													{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
													{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
													{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] } */
													{
														name : 'basicstyles',
														items : [ 'Bold',
																'Italic',
																'Underline' ]
													},
													{
														name : 'paragraph',
														items : [
																'JustifyLeft',
																'JustifyCenter',
																'JustifyRight',
																'JustifyBlock' ]
													},
													'/',
													{
														name : 'styles',
														items : [ 'Styles',
																'Format' ]
													},
													'/',
													{
														name : 'tools',
														items : [ 'Font',
																'FontSize',
																'RemoveFormat' ]
													},
													'/',
													{
														name : 'colors',
														items : [ 'BGColor' ]
													},
													{
														name : 'paragraph',
														items : [ 'Outdent',
																'Indent' ]
													},
													{
														name : 'links',
														items : [ 'Link',
																'Unlink' ]
													}, /* {
														name : 'insert',
														items : [ 'Image' ]
													}, */ ],
											removePlugins : 'resize'
										});
						
						/*To place long description data inside CKEditor*/
						var longDescription = document.addeditfundraiserform.hidLongDescription.value;
						if(null != longDescription && "" != longDescription)	{
							$("#longDescription").val(longDescription);
						} 
						
						/*To keep data on refresh of page in CKEditor*/
						/* $(window).on('beforeunload',function() {
							document.addeditfundraiserform.hidLongDescription.value = $("iframe").contents().find("body").html();
						}); */
						
						/*For Edit screen*/
						var eventId = document.addeditfundraiserform.eventID.value;
						if(null != eventId && "" != eventId)	{
							//$("#saveFund").val("Update");
							$("#clearBtn").hide();
						}
						
						/*Disabling alphabets entered from keyboard for price*/
						$('.priceVal').keypress(function(event) {	   
							if(event.keyCode != 9) {			
								if (event.which != 46 && (event.which < 47 || event.which > 59) && event.which != 8  )
							    {
							        event.preventDefault();
							        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
							            event.preventDefault();
							        }
							    }	
							}	
						});
						
						/* $('input[name="isRetLoc"]:checked').trigger('change');	
						$('input[name="isEventTied"]:checked').trigger('change'); */	
						
						$(window).load(function() {
							/*Range of recurrence: trgiger click on radio button when input:text is clicked*/
							$(".range").on('keydown.trgrRng click',function() { 
								var curInput = $(this).attr("id"); 
								var curIn = $("."+curInput).find('input[type="radio"]');
								var curName = $(curIn).attr("name");
								$('input[name="'+curName+'"]').removeAttr("checked");
								$(curIn).click();
							});
						});
						
					});
</script>
<script type="text/javascript">
	function addFundraiserDept() {
		openIframePopup('ifrmPopupDept', 'ifrmDept', '/ScanSeeWeb/retailer/addFundraiserDept.htm', 240, 400, '&nbsp;Create Department');
	}

	function fundAssociatedEvents() {
		document.addeditfundraiserform.action = "addFundraiser.htm";
		document.addeditfundraiserform.method = "POST";
		document.addeditfundraiserform.submit();
	}

	function saveFundraiserEvents() {
		document.addeditfundraiserform.action = "saveFundraiser.htm";
		document.addeditfundraiserform.method = "POST";
		document.addeditfundraiserform.submit();
	}
	
	function clearForm()	{
		var clear = confirm("Do you really want to clear the form");
		if(clear == true)	{
			document.addeditfundraiserform.action = "clearFundraiser.htm";
			document.addeditfundraiserform.method = "POST";
			document.addeditfundraiserform.submit();
			
			/* if (document.getElementById('eventName.errors') != null) {
				document.getElementById('eventName.errors').style.display = 'none';
			}
			if (document.getElementById('eventCategory.errors') != null) {
				document.getElementById('eventCategory.errors').style.display = 'none';
			}
			if (document.getElementById('eventStartDate.errors') != null) {
				document.getElementById('eventStartDate.errors').style.display = 'none';
			}
			if (document.getElementById('eventImageFile.errors') != null) {
				document.getElementById('eventImageFile.errors').style.display = 'none';
			}
			if (document.getElementById('shortDescription.errors') != null) {
				document.getElementById('shortDescription.errors').style.display = 'none';
			}
			if (document.getElementById('longDescription.errors') != null) {
				document.getElementById('longDescription.errors').style.display = 'none';
			}
			if (document.getElementById('retLocId.errors') != null) {
				document.getElementById('retLocId.errors').style.display = 'none';
			}
			if (document.getElementById('eventImageFile.errors') != null) {
				document.getElementById('eventImageFile.errors').style.display = 'none';
			}
			if (document.getElementById('eventImagePathErr') != null) {
				document.getElementById('eventImagePathErr').style.display = 'none';
			}
			if (document.getElementById('organization.errors') != null) {
				document.getElementById('organization.errors').style.display = 'none';
			}
			if (document.getElementById('eventAssociatedIds.errors') != null) {
				document.getElementById('eventAssociatedIds.errors').style.display = 'none';
			}
			if (document.getElementById('eventEndDate.errors') != null) {
				document.getElementById('eventEndDate.errors').style.display = 'none';
			}
			if (document.getElementById('moreInfo.errors') != null) {
				document.getElementById('moreInfo.errors').style.display = 'none';
			}
			if (document.getElementById('purchaseProduct.errors') != null) {
				document.getElementById('purchaseProduct.errors').style.display = 'none';
			}
			if (document.getElementById('fundraisingGoal.errors') != null) {
				document.getElementById('fundraisingGoal.errors').style.display = 'none';
			}
			if (document.getElementById('currentLevel.errors') != null) {
				document.getElementById('currentLevel.errors').style.display = 'none';
			}
			
			document.addeditfundraiserform.eventName.value = "";
			document.addeditfundraiserform.eventImageName.value = "";
			document.addeditfundraiserform.organization.value = ""
			document.addeditfundraiserform.shortDescription.value = "";
			document.addeditfundraiserform.longDescription.value = "";
			document.addeditfundraiserform.eventCategory.value = "";
			document.addeditfundraiserform.department.value = "";
			document.addeditfundraiserform.eventStartDate.value = "";
			document.addeditfundraiserform.eventEndDate.value = "";
			document.addeditfundraiserform.moreInfo.value = "";
			document.addeditfundraiserform.purchaseProduct.value = "";
			document.addeditfundraiserform.currentLevel.value = "";
			document.addeditfundraiserform.fundraisingGoal.value = "";
			document.addeditfundraiserform.eventAssociatedIds.value = "";
			document.addeditfundraiserform.retLocId.value = "";
			$('#retLocId option').removeAttr('selected');			
			$("#fundImg").attr('src', '/ScanSeeWeb/images/upload_imageRtlr.png');
			$("iframe").contents().find("body").html('');
			$("#fndYes").attr('checked',false);
			$(".fndYes").hide();
			$("#fndNo").attr('checked',true);
			$("input[name='eventAssociatedIds']").prop("checked", false);
			$("#isRetLocFund").attr('checked',false);
			$("#isOrg").attr('checked',true);
			/* $("#eventAssociatedIds.errors").hide(); 
			$(".error").remove();
			(function( $ ) {
				$(".editorTbl").find('input[type="text"],select,textarea').val("");
			})( jQuery );  */
		}
	}
	
	function isLatLong(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if ((charCode > 47 && charCode < 58) || charCode == 46 || charCode < 31
				|| charCode == 45 || charCode == 43)
			return true;
		return false;
	}

</script>
<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="/WEB-INF/jsp/retailer/retailerLeftNavigation.jsp"%>
		<div class="rtContPnl floatR evntsSctn">
			<div class="grpTitles">
				<h1 class="mainTitle">Build Fundraiser <div class="sub-actn fund"><a href="/ScanSeeWeb/retailer/fundraiserinstructions.htm">View Instructions</a> </div> </h1>
			</div>
			<div class="section">
				<div class="grdSec brdrTop">
					<form:form name="addeditfundraiserform"
						commandName="addeditfundraiserform" id="addeditfundraiserform"
						action="/ScanSeeWeb/retailer/uploadfundraiserimg.htm"
						acceptCharset="ISO-8859-1" enctype="multipart/form-data">
						<form:hidden path="eventImageName" id="fundImgPath" />
						<form:hidden path="viewName" value="addfundraiser" />
						<form:hidden path="eventID" />
						<form:hidden path="hidEventAssociatedIds" />
						<form:hidden path="uploadedImage" />
						<form:hidden path="hidLongDescription" />
						<form:hidden path="eventDate" />
						<form:hidden path="geoError" id="geoError" />
						<table width="100%" border="0" cellpadding="0" cellspacing="0"
							class="editorTbl" id="editorTbl">
							<tr>
								<td width="18%"><label class="mand">Title</label></td>
								<td width="27%"><form:errors cssClass="error"
										path="eventName" cssStyle="color:red" /> <form:input
										path="eventName" type="text" class="inputTxt textboxSmall"
										maxlength="40" tabindex="1" /></td>
								<td width="20%" align="left"><label class="mand">Organization
										Image</label></td>
								<td width="35%">
									<ul class="imgInfoSplit" id="eventUpld">
										<li><label> <img id="fundImg"
												src="${sessionScope.fundraiserImagePath}" alt="image"
												width="80" height="80" 
												onerror="this.src='/ScanSeeWeb/images/upload_imageRtlr.png';">
										</label> <span class="navTabSec mrgnRt inputCntrl topPadding"> <label for="trgrUpld">
													<input class="btn trgrUpld" value="Choose File" type="button" id="trgrUpldBtn"
													name="save2" title="Choose File" /> <form:input
														path="eventImageFile" type="file" class="textboxBig"
														id="trgrUpld" onchange="checkFundraiserImgValidate(this);"
														tabindex="2" />
											</label>
										</span></li>
										<li>Suggested Minimum Size:<br>70px/70px<br>Maximum
											Size:800px/600px<br>
										<li><br> <form:errors path="eventImageFile"
												cssClass="error" cssStyle="color:red"></form:errors> <label
											id="eventImagePathErr" style="color: red; font-style: 45"></label>
										</li>
									</ul>
								</td>
							</tr>


							<tr>
								<td valign="top"><label class="mand">Short Description</label></td>
								<td colspan="3">
									<form:errors cssClass="error" path="shortDescription" cssStyle="color:red" />
									<form:textarea path="shortDescription" name="textarea" cols="25" rows="5"
										class="shortDesc" maxlength="255" width="" tabindex="3" />
								</td>
							</tr>
							<tr>
								<td valign="top"><label class="mand">Long Description</label></td>
								<td colspan="3">
									<form:errors cssClass="error" path="longDescription" cssStyle="color:red" />
									<form:textarea path="longDescription" id="longDescription" name="textarea" rows="5" cols="25"
										class="textareaTxt txtAreaLarge" maxlength="5000" tabindex="4" />
								</td>
							</tr>

							<tr>
								<td valign="top"><label class="mand">Category</label></td>
								<td><form:errors cssClass="error" path="eventCategory"
										cssStyle="color:red" /> <form:select path="eventCategory"
										class="slctBx textboxSmall" id="eventCategory" tabindex="5">
										<option value="" selected="selected">Select Category</option>
										<c:forEach items="${sessionScope.fundraiserCatList}" var="item">
											<c:choose>
												<c:when test="${addeditfundraiserform.eventCategory eq item.categoryID}">
													<option selected="selected" value="${item.categoryID }">${item.categoryName }</option>
												</c:when>
												<c:otherwise>
													<option value="${item.categoryID }">${item.categoryName }</option>
												</c:otherwise>
											</c:choose>
											<%-- <option value="${item.categoryID }">${item.categoryName }</option> --%>
										</c:forEach>
									</form:select></td>
									<td valign="top">More Information URL</td>
									<td>
										<form:errors cssClass="error" path="moreInfo" cssStyle="color:red" />
										<form:input path="moreInfo" type="text" class="inputTxt" tabindex="6" maxlength="100"/>
									</td>
								<%-- <td align="left" valign="top">Department</td>
								<td><form:select path="department"
										class="slctBx textboxSmall" tabindex="8">
										<option value="" selected="selected">Select Department</option>
										<c:forEach items="${sessionScope.departmentsList }" var="item">
											<c:choose>
												<c:when test="${addeditfundraiserform.department eq item.deptID}">
													<option value="${item.deptID }" selected="selected">${item.deptName }</option>
												</c:when>
												<c:otherwise>
													<option value="${item.deptID }" >${item.deptName }</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</form:select> <a href="#" onclick="addFundraiserDept()"><img
										src="/ScanSeeWeb/images/add.png" width="18" height="18" /></a></td> --%>
							</tr>
							
							<tr>
								<td valign="top" class="retLocId"><form:radiobutton path="isRetLoc"
										id="isRetLocFund" value="yes" /> <label for="isRetLocFund">Retail
										Locations</label></td>
								<td><form:errors cssClass="error" path="retLocId"
										cssStyle="color:red" /> <form:select class="slctBx textboxSmall range" name="fndAppsites" path="retLocId"
										tabindex="7">
										<option value="" >Select Location</option>
										<c:forEach items="${sessionScope.retLocList}" var="item">
											<c:choose>
												<c:when test="${addeditfundraiserform.retLocId eq item.retailLocationID}">
													<option selected="selected" value="${item.retailLocationID }">${item.address1 }, ${item.city }, ${item.state }, ${item.postalCode }</option>
												</c:when>
												<c:otherwise>
													<option value="${item.retailLocationID }">${item.address1 }, ${item.city }, ${item.state }, ${item.postalCode }</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</form:select></td>
								<td align="left" valign="top" class="organization"><form:radiobutton
										path="isRetLoc" type="radio" id="isOrg" value="no" /> <label
									for="isOrg">Organization Hosting</label></td>
								<td><form:errors cssClass="error" path="organization"
										cssStyle="color:red" /> <form:input type="text"
										class="inputTxt range" path="organization" maxlength="40" tabindex="8" /></td>
							</tr>
							
							<tr class="OrgAddress">
								<td class="">
									<label for="address" class="mand">Address</label>
								</td>
								<td width="33%">
									<form:errors cssStyle="color:red" path="address" /> 
									<form:textarea id="address" class="txtAreaSmall" rows="5" cols="45" path="address" onkeyup="checkMaxLength(this,'100');" tabindex="9" cssStyle="height:60px;" />
								</td>
								<td class="">
									<label for="pstlcode" class="mand">Postal Code</label>
								</td>
								<td>
									<form:errors cssStyle="color:red" path="postalCode" /> 
									<form:input id="postalCode"	path="postalCode" class="loadingInput" maxlength="10" onkeypress="zipCodeAutocomplete('postalCode');return isNumberKey(event)" 
												onchange="isNumeric(this.value);" onkeyup="isEmpty(this.value);" tabindex="10" />
								</td>
							</tr>
							<tr class="OrgAddress">
								<td class="">
									<label for="city" class="mand">City</label>
								</td>
								<td>
									<form:errors cssStyle="color:red" path="city" /> 
									<form:input path="city" id="City" tabindex="11" class="loadingInput"/>
								</td>
								<td class="">
									<label for="state" class="mand">State</label>
								</td>
								<td>
									<form:errors cssStyle="color:red" path="state" /> 
									<form:select path="state" id="Country" tabindex="12">
																	<form:option value="">--Select--</form:option>
																	<c:forEach items="${sessionScope.states}" var="s">
																		<form:option value="${s.stateabbr}"
																			label="${s.stateName}" />
																	</c:forEach>
																</form:select>
								</td>
							</tr>
							<tr id="dispLatLang">
								<td><label class="mand">Latitude</label></td>
								<td><form:errors cssClass="error" path="latitude"
										cssStyle="color:red"></form:errors>
									<div class="cntrl-grp">
										<form:input path="latitude" id="latitude" type="text"
											class="inputTxt inputTxtBig"
											onkeypress="return isLatLong(event)" tabindex="13" />
									</div></td>
								<td><label class="mand">Longitude</label></td>
								<td><form:errors cssClass="error" path="longitude"
										cssStyle="color:red"></form:errors>
									<div class="cntrl-grp">
										<form:input path="longitude" id="longitude" type="text"
											class="inputTxt inputTxtBig"
											onkeypress="return isLatLong(event)" tabindex="14" />
									</div></td>
							</tr>

							<tr>
								<td valign="top"><label class="mand">Event Start
										Date</label></td>
								<td><form:errors cssClass="error" path="eventStartDate"
										cssStyle="color:red"></form:errors> <form:input
										path="eventStartDate" id="datepicker1" type="text"
										class="inputTxt textboxDate" readonly="true" tabindex="15" /></td>
								<td align="left" valign="top">Event End Date</label></td>
								<td><form:errors cssClass="error" path="eventEndDate"
										cssStyle="color:red"></form:errors> <form:input
										path="eventEndDate" id="datepicker2" type="text"
										class="inputTxt textboxDate" readonly="true" tabindex="16" />
								</td>
							</tr>
							<tr class="subHdr">
								<td colspan="4"><span class="setLbl">Is this tied to an event?</span> 
									<span class="mrgn-left spacing"> 
										<form:radiobutton name="isEventTied" id="fndYes" value="yes" path="isEventTied" type="radio" tabindex="17" /> 
										<label for="fndYes">Yes </label> 
										<form:radiobutton name="isEventTied" id="fndNo" value="no" path="isEventTied" type="radio" tabindex="18" /> 
										<label for="fndNo"> No</label>
										<form:errors cssClass="error" path="eventAssociatedIds" cssStyle="color:red" />
									</span>
								</td>
							</tr>

							<tr class="fndYes">
								<td><label>Search Events</label></td>
								<td colspan="3"><form:input path="eventSearchKey"
										type="text" style="height: 22px;" class="textboxSmall" maxlength="50"/> <a
									href="#"> <img width="20" height="17" title="Search Events"
										alt="Search" onclick="fundAssociatedEvents()"
										src="/ScanSeeWeb/images/searchIcon.png">
								</a></td>
							</tr>
							<tr class="grey-bg fndYes">
								<td colspan="4" class="">
									<table cellspacing="0" cellpadding="0" width="100%" border="0" id="mngevntTbl" class="white-bg brdrLsTbl formTbl">
									<c:if test="${sessionScope.associatedEventList eq null || empty sessionScope.associatedEventList}">
										<center>
											<label style="${sessionScope.fundMessageFont}" >
												<c:out value="${sessionScope.responseFundMsg}" />
											</label>
										</center>
									</c:if>
									<c:if test="${sessionScope.associatedEventList ne null && !empty sessionScope.associatedEventList}">
										<thead>
											<tr class="tblHdr">
												<th width="22%">Event Name</th>
												<th width="32%">Location</th>
												<th width="18%">Start Date</th>
												<th width="17%">End Date</th>
												<th width="11%">Action</th>
											</tr>
										</thead>
										<c:if test="${sessionScope.associatedEventList eq null || empty sessionScope.associatedEventList}">
											<center>
												<label style="${sessionScope.fundMessageFont}" >
													<c:out value="${sessionScope.responseFundMsg}" />
												</label>
											</center>
										</c:if>
										<tbody class="scrollContent">
											<c:forEach items="${sessionScope.associatedEventList}" var="fundEve">
												<tr>
													<td>${fundEve.eventName}</td>
													<td>${fundEve.address},${fundEve.city}, ${fundEve.state}, ${fundEve.postalCode}</td>
													<td>${fundEve.eventStartDate}</td>
													<td>${fundEve.eventEndDate}</td>
													<td align="center">
														<form:checkbox path="eventAssociatedIds" value="${fundEve.eventID}" /> <label for="checkbox" ></label>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</c:if>
									</table>
								</td>
							</tr>

							<tr>								
								<td>Fundraising Goal</td>
								<td>
									<form:errors cssClass="error" path="fundraisingGoal" cssStyle="color:red" />
									<form:input path="fundraisingGoal" type="text" class="inputTxt textboxSmall priceVal" tabindex="20" maxlength="10"/>
								</td>
								<td>Purchase Products</td>
								<td>
									<form:errors cssClass="error" path="purchaseProduct" cssStyle="color:red" />
									<form:input path="purchaseProduct" type="text" class="inputTxt textboxSmall" tabindex="21" maxlength="100"/>
								</td>
							</tr>
							<tr>
								<td>Current Level</td>
								<td colspan="3">
									<form:errors cssClass="error" path="currentLevel" cssStyle="color:red" />
									<form:input path="currentLevel" type="text" class="inputTxt textboxSmall priceVal" tabindex="22" maxlength="10"/>
								</td>
							</tr>
						</table>
						<div class="navTabSec mrgnRt" align="right">
							<input class="btn" value="Submit" id="saveFund" type="button" name="save" onclick="saveFundraiserEvents()" />
							<%-- <c:if test="${requestScope.fmEditScreen ne 'yes'}"> --%>
								<input class="btn" value="Clear" type="reset" name="clear" id="clearBtn" onclick="clearForm()"/>							
							<%-- </c:if> --%>
							<c:if test="${sessionScope.hideBackButton ne 'yes'}">
								<input class="btn" onclick="javascript:backManageFundraisingEvents()" value="Back" type="button" name="Back" />						
							</c:if>
							<!-- <input class="btn" onclick="javascript:backManageFundraisingEvents()" value="Back" type="button" name="Back" /> -->
						</div>
						<div class="ifrmPopupPannel" id="ifrmPopupDept"
							style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopupDept','ifrmDept')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrmDept" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
						<div class=ifrmPopupPannelImage id="ifrmPopup"
							style="display: none;">
							<div class="headerIframe">
								<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe"
									alt="close"
									onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
									title="Click here to Close" align="middle" /> <span
									id="popupHeader"></span>
							</div>
							<iframe frameborder="0" scrolling="no" id="ifrm" src=""
								height="100%" allowtransparency="yes" width="100%"
								style="background-color: White"> </iframe>
						</div>
					</form:form>
				</div>
				<div id="results"></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
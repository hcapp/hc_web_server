<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="common.pojo.RetailerLocationAdvertisement"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/custompagepview.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/jquery.form.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(function() {
		$("#StrtDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$("#EndDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$("#SplOffrStrtDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$("#SplOffrEndDT").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : '/ScanSeeWeb/images/calendarIcon.png'
		});
		$('tr.AttachLink').hide();
		var pageType = '${sessionScope.SpclOffrType}';
		if(pageType!=""){
		if (pageType == 'AttachLink') {
			$('#slctSpclOffr').val("AttachLink").trigger('change');
		} else {
			$('#slctSpclOffr').val("Special Offer").trigger('change');
		}
		}
		var landingPageType = '${sessionScope.landingPageType}';
		if(landingPageType!=""){
		if (landingPageType == 'AttachLink') {
			$('#lndgPgSlct').val("AttachLink").trigger('change');
		} else {
			$('#lndgPgSlct').val("Landing").trigger('change');
		}
		}
		
		var retStoreName="";
		var retStoreAddress="";
		var retailContactNumber="";
		//var imagePath="";
		if('${sessionScope.retailStoreName}')
			retStoreName='${sessionScope.retailStoreName}';
		if('${sessionScope.retailStoreAddress}')
			retStoreAddress='${sessionScope.retailStoreAddress}';
		if('${sessionScope.retailContactNumber}')
			retailContactNumber='${sessionScope.retailContactNumber}';
		
		/* if(document.getElementById("retPageTitle"))
		{
			previewTitleMainPage(document.getElementById("retPageTitle").value,retStoreName,retStoreAddress,retailContactNumber);
		}
		else{
			previewTitleMainPage("",retStoreName,retStoreAddress,retailContactNumber);	
		} */
		
		if(document.getElementById("retCreatedPageTitle"))
		{
			previewTitleLandingPage(document.getElementById("retCreatedPageTitle").value,retStoreName,retStoreAddress,retailContactNumber);
		}
		else{
			previewTitleLandingPage("",retStoreName,retStoreAddress,retailContactNumber);	
		}
		
		if(document.getElementById("spclOffrTitle"))
		{
			previewTitleSpecialOffPage(document.getElementById("spclOffrTitle").value,retStoreName,retStoreAddress,retailContactNumber);
		}
		else{
			previewTitleSpecialOffPage("",retStoreName,retStoreAddress,retailContactNumber);	
		}
		
		
		invokePreview("onload");
		
	});
	function selectPageType() {

		var pageType = '${sessionScope.pageType}';
		if (null != pageType && pageType != "") {
			$('input[value="${sessionScope.pageType}"]').attr('checked',
					'checked');
		}

	}

	function selectRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("rtlrcrtdLoc");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function selectAttachLinkRtlLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;

		var vLocVal = document.getElementById("retCreatedPageAttachLinkLocId");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function selectsplLocations() {

		var vLocID = document.createCustomPage.hiddenLocId.value;
		var vLocVal = document.getElementById("splOfferLocId");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function selectAttchLinkLocations() {
		var vLocID = document.createCustomPage.hiddenLocId.value;
		var vLocVal = document.getElementById("splOfferAttatchLinkLocId");
		if (vLocID != "null") {
			var vCategoryList = vLocID.split(",");
		}
		for ( var i = 0; i < vLocVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vLocVal.options[i].value == vCategoryList[j]) {
					vLocVal.options[i].selected = true;
					break;
				}
			}
		}
	}

	function showPagePreview(pageTyp){
	
		var createForm = document.createCustomPage;
		var imagePath = $("#customPageImg").attr('src')
		var retStoreName='${sessionScope.retailStoreName}';
		var retStoreImage='${sessionScope.retailStoreImage}';
		var landingImagepath=$("#createRetCustomPage").attr('src');
		var specialImagepath=$("#customSplPageImg").attr('src');
		
		// Do validation before you call for Preview
			
		if(pageTyp == 'Menu'){
		//For main page validtion
		var title=document.createCustomPage.retPageTitle.value;
		var locid=document.createCustomPage.retLocId.value;
		var retPageDesc=document.createCustomPage.retPageDescription.value;
		var retShortDesc=document.createCustomPage.retPageShortDescription.value;
		if(title==""){
		alert('Please enter title');
		return;
		}else if(locid=="" || locid=='0')
		{
		alert('Please select location');
		return;
		}else if(retPageDesc=="")
		{
		alert('Please enter  page description');
		return;
		}else if(retShortDesc=="")
		{
		alert('Please enter  short description');
		return;
		}else{
			showMainPagePview(createForm,imagePath,retStoreName,retStoreImage); 
			}
		}else if(pageTyp == 'LandingPage'){
			//For landing page validtion
			
		var title=document.createCustomPage.retCreatedPageTitle.value;
		var locid =document.createCustomPage.rtlrcrtdLoc.value;
		var retShortDesc=document.createCustomPage.retCreatedPageDescription.value;
		var retPageDesc=document.createCustomPage.retCreatedPageShortDescription.value;
		if(title==""){
		alert('Please enter title');
		return;
		}else if(locid="" || locid=='0')
		{
		alert('Please select location');
		return;
		}else if(retPageDesc=="")
		{
		alert('Please enter  page description');
		return;
		}else if(retShortDesc=="")
		{
		alert('Please enter  short description');
		return;
		}else{
			showLoadingPagePview(createForm,landingImagepath,retStoreName,retStoreImage);
			}
		}else if(pageTyp == 'SplOffer'){
		
		//For specail offer page validtion
			
		var title=document.createCustomPage.spclOffrTitle.value;
		var locid =document.createCustomPage.rtlrcrtdLoc.value;
		var retShortDesc=document.createCustomPage.spclOffrPgDesc.value;
		var retPageDesc=document.createCustomPage.spclOffrshrtDesc.value;
		var specialStartDate=document.createCustomPage.SplOffrStrtDT.value;
		var specialEndDate=document.createCustomPage.SplOffrEndDT.value;
		
		if(title==""){
		alert('Please enter title');
		return;
		}else if(locid="" || locid=='0')
		{
		alert('Please select location');
		return;
		}else if(retPageDesc=="")
		{
		alert('Please enter  page description');
		return;
		}else if(retShortDesc=="")
		{
		alert('Please enter  short description');
		return;
		}else if(specialStartDate=="")
		{
		alert('Please enter special offer start date');
		return;
		}else if(specialEndDate=="")
		{
		alert('Please enter special offer end date');
		return;
		}		
		else{
			showSpecialOffPagePview(createForm,specialImagepath,retStoreName,retStoreImage);
			}
			
		}
		}
	
	function invokePreview(pageType)
	{
		var createForm = "";
		var imagePath = "";
		var retStoreName="";
		var retStoreImage="";

		
		if(document.createCustomPage)		
			createForm = document.createCustomPage;
		
		if('${sessionScope.retailStoreName}')
			retStoreName='${sessionScope.retailStoreName}';
		if('${sessionScope.retailStoreImage}')
			retStoreImage='${sessionScope.retailStoreImage}';
	
		if(pageType == 'mainpage')
		{
			if($("#customPageImg").attr('src'))
				imagePath = $("#customPageImg").attr('src');
			showMainPagePreview_Dashboard(createForm,imagePath,retStoreName,retStoreImage); 
		
		}else if(pageType == 'landingpage')
		{
			if($("#createRetCustomPage").attr('src'))
				imagePath = $("#createRetCustomPage").attr('src');
			showLandingPagePreview_Dashboard(createForm,imagePath,retStoreName,retStoreImage); 
		
		}else if(pageType == 'specialpage')
		{
			if($("#customSplPageImg").attr('src'))
				imagePath = $("#customSplPageImg").attr('src');
			showSpecialPagePreview_Dashboard(createForm,imagePath,retStoreName,retStoreImage);
		}			 
		else if(pageType == 'onload')
		{
			if($("#customPageImg").attr('src'))
				imagePath = $("#customPageImg").attr('src');
			showMainPagePreview_Dashboard(createForm,imagePath,retStoreName,retStoreImage);
			imagePath = "";
			
			if($("#createRetCustomPage").attr('src'))
				imagePath = $("#createRetCustomPage").attr('src');
			showLandingPagePreview_Dashboard(createForm,imagePath,retStoreName,retStoreImage); 
			imagePath = "";
			
			
			if($("#customSplPageImg").attr('src'))
				imagePath = $("#customSplPageImg").attr('src');
			showSpecialPagePreview_Dashboard(createForm,imagePath,retStoreName,retStoreImage);
			imagePath = "";
		}
		
		
	}
	
</script>
<div class="shdwBg" id="content" style="min-height: 73px;">
	<%@include file="retailerLeftNavigation.jsp"%>
	<div class="grdSec contDsply floatR">
		<div id="secInfo">Add Page</div>
		<form:form commandName="createCustomPage" id="createCustomPage"
			name="createCustomPage" enctype="multipart/form-data"
			action="uploadtempretimg.htm" acceptCharset="ISO-8859-1">
			<form:hidden path="hiddenLocId" />
			<input type="hidden" id="uploadBtn" name="uploadBtn">
			<form:hidden path="pageId" id="pageId" />
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				class="grdTbl trgrClick zeroBtmMrgn zeroBtmPdng">
				<tbody>
					<tr>
						<td class="header" colspan="5">Select Page Type:</td>
					</tr>
					<tr class="addBG">
						<td width="30%" class="">Main Menu Page <input
							type="radio" checked="checked" value="rtlr" name="radio" tabindex="1">
							<img width="74" height="69" title="Business Page" alt="template"
							src="../images/tmpltIcon.png"></td>
						<td width="36%">Landing Page <input type="radio"
							value="rtlcrtd" name="radio" tabindex="1"> <img width="74" height="69"
							title="Business Created Page" alt="template"
							src="../images/tmpltIcon.png"></td>
						<td width="34%" colspan="3" class="active">Special Offer Page
							<input type="radio" value="spclOffr" name="radio" tabindex="1"> <img
							width="74" height="69" title="Special Offer Page" alt="template"
							src="../images/tmpltIcon.png"></td>
					</tr>
					<tr class="pageInfo">
						<td>The information entered will be displayed if a user does
							not have the HubCiti app. If the user does have the app, they
							will be directed to your retailer main menu on the app.</td>
						<td align="left" valign="top">The landing page is used to
							create more menu options on your retailer main menu page. This
							page can be used to display location hours, company history, ect.</td>
						<td colspan="3" align="left" valign="top">The special offer
							page will display offers on the retailer main menu page.</td>
					</tr>
				</tbody>
			</table>
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="rtlr" class="grdTbl cstmpg" style="display: none;">
				<tbody>
					<tr>
						<td class="header" colspan="4">Main Menu Page:</td>
					</tr>

					<tr>
						<td class="Label"><label for="rtlrLocn" class="mand">Locations</label>
						</td>
						<td colspan="3"><label> <form:select path="retLocId"
									id="retLocId" name="select"  tabindex="2">

									<form:option value="0">--Select--</form:option>
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>

								</form:select> <form:errors path="retLocId"  cssStyle="color:red">
								</form:errors> </label></td>
					</tr>
					<tr>
						<td width="17%" class="Label"><label for="rtlrTitle"
							class="mand">Title</label></td>
					<%-- 	<td colspan="1"><form:input path="retPageTitle" type="text"
								id="retPageTitle" name="rtlrTitle"  tabindex="3" onkeyup="previewTitleMainPage(this.value,'${sessionScope.retailStoreName}','${sessionScope.retailStoreAddress}','${sessionScope.retailContactNumber}')" /> <form:errors
								path="retPageTitle"  cssStyle="color:red">
							</form:errors></td> --%>
							<td colspan="3"><form:input path="retPageTitle" type="text"
								id="retPageTitle" name="rtlrTitle"  tabindex="3" /> <form:errors
								path="retPageTitle"  cssStyle="color:red">
							</form:errors></td>
						<!-- <td colspan="2" id="titlePreviewId">							
				
						</td> -->	
							
							
					</tr>
					<tr style="width: 999px;height: 200px;">
						<td class="Label"><label for="hdRp">Photo</label></td>


						<td colspan="1"><label for="hdSp"></label> <label><img
								id="customPageImg" width="80" height="80" alt="upload"
								src="${customPageRetImgPath}"> </label><span class="topPadding"><label
								for="trgrUpld"><input type="button" value="Choose File"
									id="trgrUpldBtn" class="btn trgrUpld"  title="Choose File"  tabindex="4" onclick="invokePreview('mainpage');"><!-- <span class="instTxt nonBlk">[ Please Upload .png format images ]</span> --> <form:input
										type="file" id="trgrUpld" path="retImage" /> </label> </span>
						</td>
						
						<!-- iphone Preview starts -->
						<td colspan="1" rowspan="4" id="iphonePreviewSpace">
							<img id="iphoneFrm" class="iphonePanelPreview_Dashboard"  alt="iphoneFrame" src="../images/iphoneFrame.png"> 
							
							
							
						</td>
						<!-- iphone Preview ends -->


					</tr>
					<tr style="width: 999px;height: 100px;">
						<td class="Label"><label for="rtlrPgDesc"
							class="mand">Page Description</label></td>
						<td colspan="1"><form:input path="retPageDescription" maxlength="100"
								type="text" id="retPageDescription" name="retPageDescription" tabindex="5" onkeyup="invokePreview('mainpage');"/>
							<form:errors path="retPageDescription" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr style="width: 999px;height: 250px;">
						<td class="Label"><label for="rtlrlngDsc"
							class="mand">Short Description</label></td>
						<td colspan="1"><form:textarea path="retPageShortDescription" cssStyle="width:241px ; height:185px; resize: none;" maxlength="255"
								id="retPageShortDescription" class="txtAreaSmall" rows="10"
								cols="45" name="retPageShortDescription" tabindex="6" onkeyup="invokePreview('mainpage');"/> <form:errors
								path="retPageShortDescription" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr style="width: 999px;height: 290px;">
						<td class="Label"><label for="rtlrshrtDsc">Long
								Description</label></td>
						<td colspan="1"><form:textarea id="rtlrshrtDsc"  cssStyle="width:241px ; height:185px; resize: none;"  maxlength="1000"
								path="retPageLongDescription" class="txtAreaSmall" rows="10"
								cols="45" name="rtlrshrtDsc" tabindex="7"  onkeyup="invokePreview('mainpage');"/></td>
					</tr>
					<tr>
						<td class="Label">&nbsp;</td>
						<td colspan="3">
							<input type="button" onclick="saveRetailerPage();" value="Save" class="btn"  title="Save" tabindex="8"> 
							<input type="button" value="Preview" class="btn" onclick="javascript:showPagePreview('Menu')"  title="Preview"  tabindex="9">						
						</td>
					</tr>

				</tbody>
			</table>
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="rtlcrtd" class="grdTbl cstmpg" style="display: none;">
				<tbody>
					<tr>
						<td class="header" colspan="4">Landing Page:</td>
					</tr>

					<tr>
						<td class="Label" width="17%">Select Type</td>
						<td colspan="3"><select name="lndgPgSlct" id="lndgPgSlct" tabindex="2">
								<option value="AttachLink">Attach Link</option>
								<option value="Landing" selected="selected">Landing</option>
						</select>
						</td>
					</tr>

					<tr class="AttachLink">
						<td class="Label"><label for="atchLinkTitle"
							class="mand">Title</label>
						</td>
						<td colspan="1"><form:input
								path="retCreatedPageAttachLinkTitle" type="text"
								id="atchLinkTitle" tabindex="3" onkeyup="previewTitleLandingPage(this.value,'${sessionScope.retailStoreName}','${sessionScope.retailStoreAddress}','${sessionScope.retailContactNumber}')" /> <form:errors
								path="retCreatedPageAttachLinkTitle" cssStyle="color:red">
							</form:errors>
						</td>
						<!-- <td colspan="2" id="titlePreviewIdLandingPage">							
				
						</td>	 -->
					</tr>



					<tr class="AttachLink" style="display: table-row;">
						<td class="Label"><label for="label2" class="mand">URL</label></td>
						<td colspan="3"><form:input path="retCreatedPageattachLink"
								type="text" id="label" name="spclOffrAtF"  tabindex="4"/> <form:errors
								path="retCreatedPageattachLink" cssStyle="color:red">
							</form:errors></td>
					</tr>

					<tr class="AttachLink">
						<td class="Label"><label for="spclOffrML"
							class="mand">Locations</label>
						</td>
						<td colspan="3"><label> <form:select
									class="txtAreaSmall" path="retCreatedPageAttachLinkLocId"
									id="retCreatedPageAttachLinkLocId" multiple="multiple" size="1" tabindex="5">
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>
								</form:select> </label> <form:errors path="retCreatedPageAttachLinkLocId"
								cssStyle="color:red">
							</form:errors></td>
					</tr>


					<tr>
						<td width="17%" class="Label"><label
							for="rtlcrtdTitle" class="mand">Title</label></td>
						<td colspan="1"><form:input path="retCreatedPageTitle"
								type="text" id="retCreatedPageTitle" tabindex="3" onkeyup="invokePreview('landingpage');previewTitleLandingPage(this.value,'${sessionScope.retailStoreName}','${sessionScope.retailStoreAddress}','${sessionScope.retailContactNumber}')" /> <form:errors
								path="retCreatedPageTitle" cssStyle="color:red">
							</form:errors></td>
						<td colspan="2" id="titlePreviewIdLandingPage">							
				
						</td>	
					</tr>
					<tr style="width: 999px;height: 200px;">
						<td class="Label"><label for="hdRp">Photo</label></td>

						<td colspan="1"><label for="hdSp"></label> <label><img
								id="createRetCustomPage" width="80" height="80" alt="upload"
								src="${customLandingPageImgPath}"> </label><span
							class="topPadding"><label for="trgrUpldCrtdPage"><input
									type="button" value="Choose File" id="trgrUpldCrtdPageBtn"
									class="btn trgrUpldCrtdPage"  title="Choose File" tabindex="4" onclick="invokePreview('landingpage');previewTitleLandingPage(this.value,'${sessionScope.retailStoreName}','${sessionScope.retailStoreAddress}','${sessionScope.retailContactNumber}')"><!-- <span class="instTxt nonBlk">[ Please Upload .png format images ]</span> --> <form:input type="file"
										id="trgrUpldCrtdPage" path="retCreatedPageImage" /> </label> </span></td>
						<!-- iphone Preview starts -->
						<td colspan="2" rowspan="4" id="iphonePreviewSpace_LandingPage">
							<img id="iphoneFrm" class="iphonePanelPreview_Dashboard"  alt="iphoneFrame" src="../images/iphoneFrame.png"> 
							
							
							
						</td>
						<!-- iphone Preview ends -->

					</tr>
					<tr style="width: 999px;height: 100px;">
						<td class="Label"><label for="rtlrcrtdPgDesc"
							class="mand">Page Description</label></td>
						<td colspan="1"><form:input path="retCreatedPageDescription" maxlength="100"
								type="text" id="retCreatedPageDescription" tabindex="5" /> <!-- onkeyup="invokePreview('landingpage');" -->  <form:errors
								path="retCreatedPageDescription" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr style="width: 999px;height: 250px;">
						<td class="Label"><label for="rtlrcrtdshrtDesc"
							class="mand">Short Description</label></td>
						<td colspan="1"><form:textarea cssStyle="width:241px ; height:185px; resize: none;"
								path="retCreatedPageShortDescription" class="txtAreaSmall" maxlength="255" 
								rows="5" cols="45" id="retCreatedPageShortDescription" tabindex="6" onkeyup="invokePreview('landingpage');" /> <form:errors
								path="retCreatedPageShortDescription" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>
					<tr style="width: 999px;height: 290px;">
						<td class="Label"><label for="rtlrcrtdlngDesc">Long
								Description</label></td>
						<td colspan="1"><form:textarea cssStyle="width:241px ; height:185px; resize: none;" 
								path="retCreatedPageLongDescription" class="txtAreaSmall" maxlength="1000"
								rows="5" cols="45" id="rtlrcrtdlngDesc" name="rtlrcrtdlngDesc" tabindex="7" onkeyup="invokePreview('landingpage');"/>
						</td>
					</tr>
					<tr style="width: 999px;height: 125px;">
						<td class="Label"><label for="rtlrcrtdLoc"
							class="mand">Locations</label></td>
						<td colspan="3"><label> <form:select
									class="txtAreaSmall" path="retCreatedPageLocId"
									id="rtlrcrtdLoc" multiple="multiple" size="1" tabindex="8">
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>
								</form:select> </label> <form:errors path="retCreatedPageLocId" cssStyle="color:red"></form:errors>
						</td>
					</tr>
					<!-- <tr><td></td>
					</tr>
					<tr><td></td>
					</tr> -->
					<tr class="grdTbl">
						<td class="Label">Start Date</td>
						<td align="left"><form:input
								path="retCreatedPageStartDate" type="text" id="StrtDT"
								class="textboxDate" tabindex="9"/> <form:errors
								path="retCreatedPageStartDate" cssStyle="color:red"><br><span
						class="instTxt nonBlk">[Start date is not required]</span>
							</form:errors>
						</td>
						<td class="Label">End Date</td>
						<td><form:input path="retCreatedPageEndDate" type="text"
								id="EndDT" class="textboxDate" name="EndDT2" tabindex="10"/> <form:errors
								path="retCreatedPageEndDate" cssStyle="color:red">
							</form:errors>(mm/dd/yyyy)<br><span
						class="instTxt nonBlk">[End date is not required]</span>
						</td>
					</tr>

					<tr>
						<td class="Label"><label for="cst">Start Time</label>
						</td>
						<td><form:select path="landingPageStartTimeHrs"
								class="slctSmall" tabindex="11">
								<form:options items="${DealStartHours}" />
							</form:select> Hrs <form:select path="landingPageStartTimeMin"
								class="slctSmall" tabindex="12">
								<form:options items="${DealStartMinutes}" />
							</form:select> Mins</td>
						<td class="Label"><label for="cet">End Time</label></td>
						<td><form:select path="landingPageEndTimeHrs"
								class="slctSmall" tabindex="13">
								<form:options items="${DealStartHours}" />
							</form:select> Hrs <form:select path="landingPageEndTimeMin" class="slctSmall" tabindex="14">
								<form:options items="${DealStartMinutes}" />
							</form:select> Mins</td>
					</tr>
					<tr>
						<td class="Label"><label for="hdRp">Choose File
								</label></td>
						<td colspan="3"><form:input path="retCreatedPageFile"
								type="file" class="multi"
								accept="png|PNG|jpg|JPG|MP3|mp3|MP4|mp4|pdf|PDF" maxlength="3"
								id="rtlrcrtdPhto2"  title="Browse" tabindex="15"/>
						</td>
					</tr>
					<tr class="cmnActn">
						<td class="Label">&nbsp;</td>
						<td colspan="3"><input type="button" value="Save" class="btn"
							onclick="saveRetailerCreatedPage();" title="Save" tabindex="16"> 
							
							<input type="button" value="Preview" class="btn" onclick="javascript:showPagePreview('LandingPage')" title="Preview" tabindex="17">
				</td>	</tr>

				</tbody>
			</table>
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="spclOffr" class="grdTbl cstmpg" style="display: table;">
				<tbody>
					<tr>
						<td class="header" colspan="4">Special Offer Page:</td>
					</tr>

					<tr>
						<td width="17%" class="Label">Select Type</td>
						<td colspan="3"><select id="slctSpclOffr" name="slctSpclOffr" tabindex="2">
								<option value="AttachLink">Attach Link</option>
								<option selected="selected" value="Special Offer">Special
									Offer</option>
						</select>
						</td>
					</tr>

					<tr class="AttachLink">
						<td class="Label"><label for="atchLinkTitle" class="mand">Title</label>
						</td>
						<td colspan="3"><form:input path="splOfferAttatchLinkTitle"
								type="text" id="atchLinkTitle"  tabindex="3"/> <form:errors
								path="splOfferAttatchLinkTitle" cssStyle="color:red">
							</form:errors>
						</td>
					</tr>



					<tr class="AttachLink" style="display: table-row;">
						<td class="Label"><label for="label2" class="mand">URL</label></td>
						<td colspan="3"><form:input path="splOfferattachLink"
								type="text" id="label" name="spclOffrAtF" tabindex="4"/> <form:errors
								path="splOfferattachLink" cssStyle="color:red">
							</form:errors></td>
					</tr>

					<tr class="AttachLink">
						<td class="Label"><label for="spclOffrML" class="mand">Location(s)</label>
						</td>
						<td colspan="3"><label> <form:select
									class="txtAreaSmall" path="splOfferAttatchLinkLocId"
									id="splOfferAttatchLinkLocId" multiple="multiple" size="1" tabindex="5">
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>
								</form:select> </label> <form:errors path="splOfferAttatchLinkLocId"
								cssStyle="color:red">
							</form:errors></td>
					</tr>


					<tr>
						<td class="Label"><label for="spclOffrTitle" class="mand">Title</label>
						</td>
						<td colspan="1"><form:input path="splOfferTitle" type="text"
								id="spclOffrTitle" tabindex="3" onkeyup="invokePreview('specialpage');previewTitleSpecialOffPage(this.value,'${sessionScope.retailStoreName}','${sessionScope.retailStoreAddress}','${sessionScope.retailContactNumber}')"/> <form:errors path="splOfferTitle"
								cssStyle="color:red" >
							</form:errors></td>
						<td colspan="2" id="titlePreviewIdSpecialPage">							
				
						</td>	
					</tr>
					<tr style="width: 999px;height: 168px;">
						<td class="Label"><label for="hdRp">Photo</label></td>


						<td colspan="1"><label for="hdSp"></label> <label><img
								id="customSplPageImg" width="80" height="80" alt="upload"
								src="${customSplOfferPageImgPath}"> </label><span
							class="topPadding"><label for="trgrUpldSplPage"><input
									type="button" value="Choose File" id="trgrUpldSplPageBtn"
									class="btn trgrUpldSplPage"  title="Choose File" tabindex="4" onclick="invokePreview('specialpage');"> <!-- <span class="instTxt nonBlk">[ Please Upload .png format images ]</span> --><form:input type="file"
										id="trgrUpldSplPage" path="splOfferImage" /> </label> </span></td>
						<!-- iphone Preview starts -->
						<td colspan="2" rowspan="4" id="iphonePreviewSpace_SpecialPage">
							<img id="iphoneFrm" class="iphonePanelPreview_Dashboard"  alt="iphoneFrame" src="../images/iphoneFrame.png"> 
							
							
							
						</td>
						<!-- iphone Preview ends -->

					</tr>
					<tr style="width: 999px;height: 147px;">
						<td class="Label"><label for="spclOffrPgDesc"
							class="mand">Page Description</label></td>
						<td colspan="1"><form:input path="splOfferDescription" maxlength="100"  
								type="text" id="spclOffrPgDesc" name="spclOffrPgDesc" tabindex="5" /><!-- onkeyup="invokePreview('specialpage');"   --><form:errors
								path="splOfferDescription" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr style="width: 999px;height: 232px;">
						<td class="Label"><label for="spclOffrshrtDesc"
							class="mand">Short Description</label></td>
						<td colspan="1"><form:textarea class="txtAreaSmall" rows="5" maxlength="255" cssStyle="width:241px ; height:185px; resize: none;"
								path="splOfferShortDescription" cols="45" id="spclOffrshrtDesc"
								name="spclOffrshrtDesc" tabindex="6" onkeyup="invokePreview('specialpage');"/> <form:errors
								path="splOfferShortDescription" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr style="width: 999px;height: 237px;">
						<td class="Label"><label for="spclOffrlngDesc">Long
								Description</label></td>
						<td colspan="1"><form:textarea class="txtAreaSmall" rows="5" maxlength="1000" cssStyle="width:241px ; height:185px; resize: none;"
								path="splOfferLongDescription" cols="45" id="spclOffrlngDesc"
								name="spclOffrlngDesc" tabindex="7" onkeyup="invokePreview('specialpage');"/></td>
					</tr>
					<tr style="width: 999px;height: 110px;">
						<td class="Label"><label for="spclOffrML"
							class="mand">Locations</label>
						</td>
						<td colspan="3"><label> <form:select
									class="txtAreaSmall" path="splOfferLocId" id="splOfferLocId"
									multiple="multiple" size="1" tabindex="8">
									<c:forEach items="${sessionScope.retailerLocList}" var="s">
										<form:option value="${s.retailerLocationID}"
											label="${s.address1}" />
									</c:forEach>
								</form:select> </label> <form:errors path="splOfferLocId" cssStyle="color:red">
							</form:errors></td>
					</tr>
					<tr class="grdTbl">
						<td class="Label"><label for="spclOffrML"
							class="mand">Start Date</label></td>
						<td align="left"><form:input path="splOfferStartDate"
								type="text" id="SplOffrStrtDT" class="textboxDate" tabindex="9" onchange="invokePreview('specialpage');"/> <form:errors
								path="splOfferStartDate" cssStyle="color:red">
							</form:errors></td>
						<td class="Label"><label for="spclOffrML"
							class="mand">End Date</label></td>
						<td><form:input path="splOfferEndDate" type="text"
								id="SplOffrEndDT" class="textboxDate" name="EndDT" tabindex="10" onchange="invokePreview('specialpage');"/> <form:errors
								path="splOfferEndDate" cssStyle="color:red">
							</form:errors>(mm/dd/yyyy)<br><span
						class="instTxt nonBlk">[End date is not required]</span></td>
					</tr>

					<tr>
						<td class="Label"><label for="cst">Start Time</label>
						</td>
						<td><form:select path="splofferStartTimeHrs"
								class="slctSmall" tabindex="11">
								<form:options items="${DealStartHours}" />
							</form:select> Hrs <form:select path="splofferStartTimeMin" class="slctSmall" tabindex="12">
								<form:options items="${DealStartMinutes}" />
							</form:select> Mins</td>
						<td class="Label"><label for="cet">End Time</label></td>
						<td><form:select path="splOfferEndTimeHrs" class="slctSmall" tabindex="13">
								<form:options items="${DealStartHours}" />
							</form:select> Hrs <form:select path="splOfferEndTimeMin" class="slctSmall" tabindex="14">
								<form:options items="${DealStartMinutes}" />
							</form:select> Mins</td>
					</tr>
					<tr>
						<td class="Label"><label for="hdRp">Choose File
								</label></td>
						<td colspan="3"><form:input path="splOfferFile" maxlength="3"
								type="file" class="multi"
								accept="png|PNG|jpg|JPGMP3|mp3|MP4|mp4|pdf|PDF"
								id="spclOffrPhto"  title="Browse" tabindex="15"/>
						</td>
					</tr>

					<tr class="cmnActn" style="display: table-row;">
						<td class="Label">&nbsp;</td>
						<td colspan="3"><input type="button"
							onclick="saveSplOfferPage();" value="Save" class="btn" title="Save" tabindex="16"> <input
							type="button"
							onclick="javascript:showPagePreview('SplOffer')"
							value="Preview" class="btn" title="Preview" tabindex="17"></td>
					</tr>
				</tbody>
			</table>
		</form:form>
	</div>
	<div class="clear"></div>
</div>
<script>
	selectPageType();

	if ($('input[value="spclOffr"]').attr('checked')) {
		var pageType = '${sessionScope.SpclOffrType}';
		if (pageType == 'AttachLink') {
			selectAttchLinkLocations();
		} else {
			selectsplLocations();
		}

	}

	if ($('input[value="rtlcrtd"]').attr('checked')) {

		var pageType = '${sessionScope.landingPageType}';
		if (pageType == 'AttachLink') {
			selectAttachLinkRtlLocations();
		} else {
			selectRtlLocations();
		}

	}
</script>
<script type="text/javascript">
	$('#trgrUpld')
			.bind(
					'change',
					function() {

						$("#uploadBtn").val("trgrUpldBtn")

						var imgpath = document.getElementById('trgrUpld').value;

						if (!validateUploadImageFileExt(imgpath)) {
							alert("Please Choose PNG format image")
						} else {

							$("#createCustomPage")
									.ajaxForm(
											{

												success : function(response) {

													//alert(response.getElementsByTagName('imageScr')[0].firstChild.nodeValue)
													$('#customPageImg')
															.attr(
																	"src",
																	response
																			.getElementsByTagName('imageScr')[0].firstChild.nodeValue);
												}

											}).submit();
						}

					});
	$('#trgrUpldCrtdPage')
			.bind(
					'change',
					function() {
						$("#uploadBtn").val("trgrUpldCrtdPageBtn")
						var imgpath = document
								.getElementById('trgrUpldCrtdPage').value;
						if (!validateUploadImageFileExt(imgpath)) {
							alert("Please Choose PNG format image")
						} else {

							$("#createCustomPage")
									.ajaxForm(
											{

												success : function(response) {

													//alert(response.getElementsByTagName('imageScr')[0].firstChild.nodeValue)
													$('#createRetCustomPage')
															.attr(
																	"src",
																	response
																			.getElementsByTagName('imageScr')[0].firstChild.nodeValue);
												}

											}).submit();
						}
					});
	$('#trgrUpldSplPage')
			.bind(
					'change',
					function() {
						$("#uploadBtn").val("trgrUpldSplPageBtn")
						var imgpath = document
								.getElementById('trgrUpldSplPage').value;
						if (!validateUploadImageFileExt(imgpath)) {
							alert("Please Choose PNG format image")
						} else {

							$("#createCustomPage")
									.ajaxForm(
											{

												success : function(response) {

													//alert(response.getElementsByTagName('imageScr')[0].firstChild.nodeValue)
													$('#customSplPageImg')
															.attr(
																	"src",
																	response
																			.getElementsByTagName('imageScr')[0].firstChild.nodeValue);
												}

											}).submit();
						}
					});
</script>
<script type="text/javascript">
$('#slctSpclOffr,#lndgPgSlct').change(
		function() {
			$this = $(this);
			var curVal = $this.find('option:selected')
					.val();
			var curTxt = $this.find('option:selected')
					.text();
			var curTbl = $(this).parents('table')
					.attr('id');
			if (curVal == "AttachLink") {
				$('#' + curTbl + ' tr:gt(4)').hide();
				$('.AttachLink,.cmnActn').show()
				$('#' + curTbl + ' td.header').text(
						curTxt + " Page:");
				$this.parents('table').find(
						'input[value="Preview"]').hide();
			} else {
				$('#' + curTbl + ' tr.AttachLink').hide();
				$('#' + curTbl + ' tr:gt(4)').show();
				$('#' + curTbl + ' td.header').text(
						curTxt + " Page:");
				$this.parents('table').find(
						'input[value="Preview"]').show();
			}
		});
</script>
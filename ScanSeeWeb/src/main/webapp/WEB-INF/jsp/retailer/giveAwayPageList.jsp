<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<%@ page
	import="java.util.List,common.pojo.SearchResultInfo,common.pojo.RetailerCustomPage"%>
<script src="/ScanSeeWeb/scripts/jquery.tablednd.js"></script>
<script>
function deletePromotionPage(pageId) {
	var deletePage = confirm("Are you sure you want to delete this GiveAway page ?")
	if (deletePage) {
		document.customPageForm.pageId.value = pageId;
		document.customPageForm.action = "deleteGiveAwayPage.htm";
		document.customPageForm.method = "POST";
		document.customPageForm.submit();
	}
}
function searchPage(){
	showProgressBar();
	var searchKey =  $('input[name="searchKey1"]').val();
	document.customPageForm.searchKey.value = searchKey;
	document.customPageForm.pageFlag.value = "false";
	document.customPageForm.action = "managegiveawaypages.htm";
	document.customPageForm.method = "POST";
	document.customPageForm.submit();
}
function getPerPgaVal(){
	var selValue=$('#selPerPage :selected').val();
	document.customPageForm.recordCount.value=selValue;
	searchPage();
			
}
function buildPage(){
	document.customPageForm.action = "buildgiveawaypage.htm";
	document.customPageForm.method = "POST";
	document.customPageForm.submit();
}
function getGiveAwayPageInfo(pageId) {
	document.customPageForm.pageId.value = pageId;
	document.customPageForm.action = "displaygiveawaypageinfo.htm";
	document.customPageForm.method = "POST";
	document.customPageForm.submit();

}
</script>
<div id="wrapper">
	<form:form commandName="createCustomPage" name="customPageForm">
		<form:input type="hidden" id="pageId" path="pageId"  />
		<input type="hidden" id="searchKey" name="searchKey" />
		<form:hidden path="pageNumber" />
		<form:hidden path="pageFlag" />
		<form:hidden path="recordCount" />
		<div id="content" class="shdwBg">
			<%@include file="retailerLeftNavigation.jsp"%>
			<div class="rtContPnl floatR">
				<div class="grpTitles">
					<h1 class="mainTitle"> Giveaway </h1>
				</div>
				<div class="section">
					<div class="grdSec brdrTop">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tranTbl">
							<tr>
								<td width="27%" align="left">
									<label for="gnrlSrch">Search Giveaway Pages</label>
								</td>
								<td width="49%">
									<input type="text" name="searchKey1" id="gnrlSrch" />
									<a href="#">
										<img src="../images/searchIcon.png" alt="Search" title="Search"  width="20" height="17" onclick="javascript:searchPage();"/>
									</a>
									<label for="gnrlSrch"></label>
								</td>
								<td width="24%">
									<input type="button" class="btn" value="Build Giveaway Page" title="Build Giveaway Page" onclick="buildPage();" />
								</td>
							</tr>
						</table>
					<div align="center" style="font-style: 90">
						<label style="${requestScope.manageAnyThing}"><c:out
								value="${requestScope.successMSG}" /> </label>
					</div>
					<div class="searchGrd">
						<h1 class="searchHeaderExpand">
							<a href="#" class="floatR">&nbsp;</a>
							&nbsp;
						</h1>
						<div class="grdCont tableScroll zeroPadding">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gr">
								<thead>
									<tr class="header">
										<td width="20%">Title</td>
										<td width="19%">Image</td>
										<td width="11%">Quantity</td>
										<td width="13%">Start Date (mm-dd-yyyy) </td>
										<td width="13%">End Date (mm-dd-yyyy) </td>
										<td width="24%">Action</td>
									</tr>
								</thead>
								<tbody>
									<c:if test="${message ne null }">
										<div id="message">
											<center>
												<label style="color: red; font-weight: regular;"> <c:out
														value="${message}" /> </label>
											</center>
										</div>

									</c:if>
									<c:if test="${requestScope.customPageList ne null }">
										<c:forEach items="${requestScope.customPageList}"
											var="item">
											<tr id="<c:out value='${item.QRRetailerCustomPageID}'/>">
												<td><a href="#"
													id="<c:out value='${item.QRRetailerCustomPageID}'/>"
													onclick="getGiveAwayPageInfo(${item.QRRetailerCustomPageID})"title="Edit">${item.pageTitle}</a>
												</td>
												<td><c:choose>
														<c:when
															test="${item.imagePath == null || item.imagePath eq 'null' || item.imagePath eq ''}">
															<img src="../images/imgIcon.png" alt="Image" width="49" height="46" />
												</c:when>
														<c:otherwise>
											       <img src="${item.imagePath}" alt="Image" width="49" height="46" onerror="this.src = '../images/imgIcon.png';"/>
											    </c:otherwise>
													</c:choose>
												</td>
												 <td>${item.quantity}</td>
												<td><c:choose>
														<c:when
															test="${item.startDate == null || item.startDate eq 'null' || item.startDate eq ''}">
													No start Date
												</c:when>
														<c:otherwise>
											       ${item.startDate}
											    </c:otherwise>
													</c:choose>
												</td>
												<td><c:choose>
														<c:when
															test="${item.endDate == null || item.endDate eq 'null' || item.endDate eq ''}">
											       No End Date
											    </c:when>
														<c:otherwise>
											       ${item.endDate}
											    </c:otherwise>
													</c:choose>
												</td>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="button" class="btn" value="Show QR" title="Show QR" 
													onclick="openShowGiveAwayQrPopUp(${item.QRRetailerCustomPageID})" />
													&nbsp;&nbsp;
													<img src="/ScanSeeWeb/images/deleteRedIcon.png"
													style="cursor: pointer;" title="Delete" alt="Delete"
													width="24" height="22"
													onclick="deletePromotionPage(${item.QRRetailerCustomPageID})" />
												</td>

											</tr>
										</c:forEach>
									</c:if>
								</tbody>								
							</table>
							
						</div>
						<div class="pagination brdrTop">
								<table width="100%" border="0" cellspacing="0" cellpadding="0"
									class="noBrdr" id="perpage">
									<tr>
										<page:pageTag
											currentPage="${sessionScope.pagination.currentPage}"
											nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
											pageRange="${sessionScope.pagination.pageRange}"
											url="${sessionScope.pagination.url}" enablePerPage="true" />
									</tr>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;background-color: White">
			<div class="headerIframe">
				<img src="../images/popupClose.png" class="closeIframe" alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm');"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
				height="100%" allowtransparency="yes" width="100%"
				style="background-color: White"> </iframe>
		</div>

		<div class="clear"></div>
	</form:form>
</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Coupons</title>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/style.css" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery.ticker.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.jscroll.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/global.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/jquery.tablescroll.js"
	type="text/javascript"></script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
function getselectedProductID(){
	var allVals = [];
	var allScanCode = [];
	var allProdId = [];
	var allProdImage = [];
	var allProdPrice = [];
	 $('input[name ="pUpcChk"]:checked').each(function() {
	       allVals.push($(this).val());
	     });
		 
		 for(var i=0;i<allVals.length;i++){
			 var temp = allVals[i].split(",");
			 allScanCode[i] = temp[0];
			 allProdId[i] = temp[1];
			 allProdPrice[i] = temp[2];
			 allProdImage[i] = temp[3];
		 }
		 
		var vRegPrice = 0;
		$.each(allProdPrice,function(i,val) {
		val = Number(val)
		if(val)
			vRegPrice += val;
		});
		
		var vProductImg;
			for(var i in allProdImage){
				if(allProdImage[i].match(/.png|.jpg|.jpeg|.bmp|.gif/i)){
				vProductImg = allProdImage[i];
				break;
			}
		}
		//top.$('#regPrice').val(0);
		if(allProdId.length >0){
			 var $prdID = top.$('#prodcutID');
			 var $scanCode = top.$('#couponName');
			 var $prodImage = top.$('#productImage');
			 var $prodPrice = top.$('#regPrice');
			 
			$prdID.val(allProdId.toString());
			$scanCode.val(allScanCode.toString());
			if (vProductImg != undefined || vProductImg != null || vProductImg != '') {
				$prodImage.val(vProductImg);
			}
			$prodPrice.val(vRegPrice);
			var $prdID = top.$('#prodcutID').val();
			
if(top.$('#slctLoc').attr('checked')){
		loadRetailerLocation()
		}
		if($prdID!=""){
		$.ajax({
				type : "GET",
				url : "/ScanSeeWeb/removeProduct.htm",
				data : {
					'productId'  : $prdID
				},
				success : function(response) {
				 closeIframePopup('ifrmPopup2','ifrm',callBackFunc())
				},
				error : function(e) {
					alert('Error:' + 'Error Occured');
				}
			});
		} else {
			 closeIframePopup('ifrmPopup2','ifrm',callBackFunc())
			}
		} else {
			alert("Please select Products")
		}
	}

	function loadRetailerLocation() {
		var vProductId = top.$('#prodcutID').val();
		var vTabIndexLoc = top.$('#tabIndexLoc').val();
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/retailer/fetchretailerlocation.htm",
			data : {
				'productId' : vProductId,
				'tabIndexLoc' : vTabIndexLoc
			},
			success : function(response) {
				top.$('#myAjax3').html(response);
			},
			error : function(e) {
				alert("Error occurred while Showing Retail Loc Ids");
			}
		});
	}

</script>
<script type="text/javascript">
function clearForm()
{
	document.getElementById("couponName").value="";
	document.getElementById("message").style.display='none';
}

</script>


</head>
<body class="whiteBG">
	<script type="text/javascript">
	
</script>
	<div class="contBlock">
		<form:form commandName="produpclisthotdealform"
			name="produpclisthotdealform">

			<form:hidden path="retLocID" id="retLocID" />



			<fieldset>
				<legend>Product/UPC Search</legend>
				<div class="contWrap">
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="grdTbl">
						<tbody>
							<tr>
								<td width="18%" class="Label"><label for="couponName">Product/
										UPC</label></td>

								<td width="29%"><form:input path="productName" type="text"
										name="textfield2" id="couponName" /></td>
							</tr>
						</tbody>
					</table>

					<div class="navTabSec mrgnRt" align="right">

						<!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->
						<input name="Save" value="Search" type="button" class="btn"
							onclick="searchProdUPCHotdeal();" id="save" title="search" /> <input
							name="Save" value="Clear" type="button" class="btn" id="clr"
							title="clear" onclick="clearForm();" />

					</div>

				</div>
			</fieldset>
		</form:form>
		<fieldset class="popUpSrch">
			<legend>Product/UPC Details</legend>
			<div id="" class="grdCont searchGrd">
				<table id="thetable" class="stripeMe" border="0" cellspacing="0"
					cellpadding="0" width="100%">
					<thead>

						<tr class="header hvrEfct">
							<td width="28%">Product Name</td>
							<td width="20%">UPC</td>
							<td width="38%">Short Description</td>
							<td align="center">Action</td>
						</tr>
					</thead>
					<tbody>
						<c:if test="${message ne null }">
							<div id="message">
								<center>
									<c:out value="${message}" />
								</center>
							</div>
							<script>var PAGE_MESSAGE = true;</script>
						</c:if>
						<!--<c:if test="${displayassociate ne 'no'}">-->
						<c:if test="${pdtInfoList ne null && ! empty pdtInfoList}">
							<c:forEach items="${sessionScope.pdtInfoList}"
								var="earlierAddedPdts">
								<tr>
									<td><c:out value="${earlierAddedPdts.productName}" />
									</td>
									<td><c:out value="${earlierAddedPdts.scanCode}" />
									</td>
									<td><c:out value="${earlierAddedPdts.shortDescription}" />
									</td>
									<td align="center"><input type="checkbox"
										checked="checked" id="searchCheckBox" class="check"
										name="pUpcChk" value="${earlierAddedPdts.scanCode},${earlierAddedPdts.productID},${earlierAddedPdts.price}, ${earlierAddedPdts.imagePath}"
										 />
									</td>
								</tr>
							</c:forEach>
						</c:if>
						<!--</c:if>-->
						<c:forEach items="${sessionScope.manageproductlist}" var="item">
							<tr>
								<td><c:out value="${item.productName}" />
								</td>
								<td><c:out value="${item.scanCode}" />
								</td>
								<td><c:out value="${item.shortDescription}" />
								</td>
								<td align="center"><input type="checkbox"
									id="searchCheckBox" name="pUpcChk" value="${item.scanCode},${item.productID},${item.price}, ${item.productImagePath}"
									/></td>
							</tr>
						</c:forEach>
						

					</tbody>
				</table>
			</div>
			<div class="navTabSec mrgnRt" align="right">

				<!--<input name="Cancel2" value="Back" type="button" class="btn" onclick="javascript:history.back()" />-->

				<input name="associate" value="Associate"
					onclick="getselectedProductID()" type="button" class="btn"
					id="associate" title="Associate" />
			</div>
		</fieldset>
	</div>
</body>
</html>

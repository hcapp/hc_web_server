

<body class="whiteBG">
	<script type="text/javascript">
		function printQRCode() {

			var html = "<html><table align='center' cellspacing='0' cellpadding='0' ><tr><td align='center'><img src='../images/hubciti-logo_ret.jpg'/></td></tr><td align='center'>";
			html += document.getElementById('areaToPrint').innerHTML
					+ "</td></tr>";
			html += "<tr><td align='center'>" + "Store Identification:"
					+ '${requestScope.pageDetails.storeID}'
					+ "</td></tr></table>"
			html += "</html>";

			var printWin = window.open('', '_blank');
			printWin.document.write(html);
			printWin.document.close();
			printWin.focus();
			printWin.print();
			printWin.close();

		}
	</script>
	<div id="wrapper">
		<div id="header">
			<div id="header_logo" class="floatL">
				<a href="index.html"> <img alt="ScanSee"
					src="../images/hubciti-logo_ret.jpg" /> </a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div class="grdSec">
			<div id="content" class="floatL">
				<table width="94%" border="0" cellspacing="0" cellpadding="0"
					class="brdrLsTbl">
					<tr>
						<td colspan="3" class="title"><strong>QR code for
								your selection:</strong>
						</td>
					</tr>
					<tr class="print">

						<td width="100%" align="left" colspan="3" valign="top">
							<ul class="qrImg">
								<li id="prod_qr"><div id="areaToPrint">
										<img src="${requestScope.pageDetails.qrImagePath}" width="100"
											height="100" />
									</div>
								</li>
								<li id="audio_qr"></li>
							</ul>
							<p>This QR code will send customers directly to your appsite
								when scanned! Download and add directly to any of your marketing
								material or emails.</p>
							<p>&nbsp;</p>
							<p>Try it! Download the HubCiti app and scan it with our
								all-in-one scanner.</p>
						</td>
					</tr>
					<tr>
						<td colspan="3"><strong>Store Identification:
								${requestScope.pageDetails.storeID}</strong>
						</td>
					</tr>

					<td><img src="../images/download.png" alt="download"
						width="16" height="16" />
					</td>
					<td colspan="2">To download, right-click on the image, then
						select &quot;Save Picture As...&quot; from the pop-up menu.</td>
					</tr>
					<tr>
						<td width="3%"><img src="../images/print.gif" alt="print"
							width="16" height="16" />
						</td>
						<td colspan="2"><a href="#" onclick="printQRCode();">Print</a>
						</td>
					</tr>
				</table>

			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<div id="footer">
			<div id="ourpartners_info">
				<!--<div id="followus_section" class="floatL">
      <div class="section_title">Connect with us!</div>
      <img border="0" alt="followus" src="images/followus-img.png" usemap="#Map2"/>
      <map id="Map2" name="Map2">
        <area title="Flickr" href="Retailer/Retailer_abtscansee.html#" alt="Flickr"/>
        <area title="RSS" href="Retailer/Retailer_abtscansee.html#" alt="RSS"/>
        <area title="Email" href="Retailer/Retailer_abtscansee.html#" alt="Email"/>
        <area title="YouTube" href="Retailer/Retailer_abtscansee.html#" alt="YouTube"/>
        <area title="Twitter" href="Retailer/Retailer_abtscansee.html#" alt="Twitter"/>
        <area title="Facebook" href="Retailer/Retailer_abtscansee.html#" alt="Facebook"/>
      </map>
    </div>-->
				<div class="clear"></div>
			</div>
		</div>
	</div>
</body>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript" src="scripts/jquery-1.6.2.min.js"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<script src="scripts/web.js" type="text/javascript"></script>
<link
	href="/ScanSeeWeb/styles/jquery-ui.css"
	rel="stylesheet" type="text/css">
<script
	src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script
	src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<style>
.ui-datepicker-trigger {
                               margin-left:3px;
                               margin-top: 0.5px;
                               
                       }
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar' ,buttonImage: '/ScanSeeWeb/images/calendarIcon.png' });
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({ showOn: 'both', buttonImageOnly: true, buttonText: 'Click to show the calendar',buttonImage: '/ScanSeeWeb/images/calendarIcon.png' });
	});
</script>
<script type="text/javascript">
function validateRebateForm() 
{
	var startDate = document.getElementById("datepicker1").value;
	var endDate = document.getElementById("datepicker2").value;
	if(compareDate(startDate,endDate,"yes"))
		{
		addRetailerRebate();
		return true;
	    }
	return false;
}
function checkAssociatedProd(){
	var $prdID = $('#prodcutID').val();
		if ($prdID != "") {
			$.ajax({
				type : "GET",
				url : "/ScanSeeWeb/checkAssociatedProd.htm",
				data : {
					'productId' : $prdID
				},
				success : function(response) {
					openIframePopupRebate('ifrmPopup', 'ifrm',
							'rebateretprodupclist.htm', 430, 600,
							'View Product/UPC')
				},
				error : function(e) {
					alert('Error:' + 'Error Occured');
				}
			});
		} else {
			openIframePopupRebate('ifrmPopup', 'ifrm','rebateretprodupclist.htm', 430, 600, 'View Product/UPC')
		}
	}

function getRetailerLocTrigger() {
	document.addretrebatesForm.productName.value = "";
	document.addretrebatesForm.productID.value = "";
}
</script>

<div id="wrapper">
	<div id="content" class="shdwBg">
		<%@include file="retailerLeftNavigation.jsp"%>
		<div class="grdSec rtContPnl floatR">
			<form:form name="addretrebatesForm" commandName="addretrebatesForm"
				method="POST" enctype="multipart/form-data"
				action="addretrebates.htm" acceptCharset="ISO-8859-1">
				<form:hidden path="productID" name="textfield2" id="prodcutID" />
				<form:hidden path="retailID" id="retailID"/>
				
				<div id="secInfo">Add Rebates</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="grdTbl">
					<div align="center" style="font-style:45"><label><form:errors cssStyle="color:red"/></label></div>
					<tr>
						<td colspan="4" class="header">Add Rebate</td>
					</tr>
					<tr>
						<td width="17%" class="Label"><label for="rbtName" class="mand">Rebate
								Name</label></td>
						<td width="33%"><form:errors path="rebName" cssStyle="color:red">
							</form:errors>
						<form:input path="rebName" type="text"
								name="textfield2" id="rbtName" />
							</td>
						<td width="16%" class="Label"><label for="rbtAmt" class="mand">Rebate
								AMT $ </label></td>
						<td width="34%"><form:errors path="rebAmount" cssStyle="color:red">
							</form:errors>
						<form:input path="rebAmount" type="text"  onkeypress="return isNumberKeyPhone(event)"
								name="rbtAmt" id="rbtAmt" />
							</td>
					</tr>
					<tr>
						<td class="Label"><label for="rbtDsc"  class="mand">Rebate
								Description</label></td>
						<td><form:errors path="rebLongDescription" cssStyle="color:red"></form:errors>
						<form:textarea path="rebLongDescription" name="rbtDsc"
								id="rbtDsc" cols="45" rows="5" class="txtAreaSmall"></form:textarea>
						</td>
						<td class="Label"><label for="rbtTC"  class="mand">Rebate Terms
								&amp; Conditions</label></td>

						<td><form:errors path="rebTermCondtn" cssStyle="color:red"></form:errors>
						<form:textarea path="rebTermCondtn" name="textarea"
								id="rbtTC" cols="45" rows="5" class="txtAreaSmall"></form:textarea>
						</td>
					</tr>
					<tr>
            <td class="Label"><label for="rbtAmount" class="mand">Number of Rebates Issued</label></td>
            <td colspan="3"><form:errors path="noOfrebatesIsued" cssStyle="color:red"></form:errors>
            <form:input path="noOfrebatesIsued" name="noofrebatesisuued" type="text" class="textboxDate" id="rebateIsued" onkeypress="return isNumberKey(event)"/>
              </td>
              </tr>
					<tr>
						<td class="Label"><label for="rsd" class="mand">Rebate Start Date</label>
						</td>
						<td align="left"><form:errors path="rebStartDate"
								cssStyle="color:red">
							</form:errors>
						<form:input path="rebStartDate"
								name="textfield4" type="text" class="textboxDate"
								id="datepicker1" />(mm/dd/yyyy)</td>
						<td class="Label"><label for="red" class="mand">Rebate End Date</label></td>

						<td align="left"><form:errors path="rebEndDate"
								cssStyle="color:red">
							</form:errors>
						<form:input path="rebEndDate"
								name="textfield4" type="text" class="textboxDate"
								id="datepicker2" />(mm/dd/yyyy)</td>
					</tr>
					<!--<tr>
						<td class="Label">Rebate Start Time</td>
						<td><form:select path="rebStartTime" name="select5"
								class="slctSmall">
								<c:forEach var="num" begin="00" end="23" step="01">
									<form:option value="${num}">
										<c:out value="${num}" />
									</form:option>
								</c:forEach>
							</form:select>Hrs <form:select path="rebStartTime" name="select5"
								class="slctSmall">
								<c:forEach var="num" begin="00" end="55" step="5">
									<form:option value="${num}">
										<c:out value="${num}" />
									</form:option>
								</c:forEach>
							</form:select>Mins</td>
						<td class="Label">Rebate End Time</td>
						<td><form:select path="rebEndTime" name="select6"
								class="slctSmall">
								<c:forEach var="num" begin="00" end="23" step="01">
									<form:option value="${num}">
										<c:out value="${num}" />
									</form:option>
								</c:forEach>
							</form:select>Hrs <form:select path="rebEndTime" name="select6"
								class="slctSmall">
								<c:forEach var="num" begin="00" end="55" step="5">
									<form:option value="${num}">
										<c:out value="${num}" />
									</form:option>
								</c:forEach>
							</form:select>Mins
					</tr>
						-->
						<tr>
								<td class="Label"><label for="cst">Rebate Start
										Time </label></td>
								<td><form:select path="rebStartHrs" class="slctSmall">
										<form:options items="${RebateStartHrs}" />
									</form:select> Hrs <form:select path="rebStartMins" class="slctSmall">
										<form:options items="${RebateStartMins}" />
									</form:select> Mins</td>
								<td class="Label"><label for="cet">Rebate End Time</label>
								</td>
								<td><form:select path="rebEndhrs" class="slctSmall">
										<form:options items="${RebateStartHrs}" />
									</form:select> Hrs <form:select path="rebEndMins" class="slctSmall">
										<form:options items="${RebateStartMins}" />
									</form:select> Mins</td>
							</tr>
						<tr>
		
		<td class="Label"><label for="timeZone">Time Zone</label></td>
							<td colspan="3"><form:select path="rebateTimeZoneId" class="selecBx">
								<form:option value="0" label="--Select--">Please Select Time Zone</form:option>
								<c:forEach items="${sessionScope.rebateTimeZoneslst}" var="tz">
						<form:option value="${tz.timeZoneId}" label="${tz.timeZoneName}" />
					</c:forEach>
								
							</form:select>
		</tr>
					<tr>
						<td class="Label"><label for="retailerLocID" class="mand">Select Business Loc</td>
						<td colspan="3"><form:errors path="retailerLocID" cssStyle="color:red"></form:errors>
						<form:select path="retailerLocID" name="retailerLocID"
								class="selecBx" id="retailerLocID"  onchange="getRetailerLocTrigger();">
								<form:option value="0">--Select--</form:option>
								<c:forEach items="${sessionScope.retailerLocList}" var="s">
									<form:option value="${s.retailerLocationID}"
										label="${s.address1}" />
								</c:forEach>
							</form:select></td>
							
					</tr>
					<tr>
						<td class="Label"><label for="productName" class="mand">Select Product</td>
						<td colspan="3"><form:errors path="productName"
								cssStyle="color:red"></form:errors>
						<form:input path="productName" type="text"
								name="textfield" id="couponName"  readonly="true"/> <a href="#"><img
								src="/ScanSeeWeb/images/searchIcon.png" alt="Search" width="25"
								height="24"

								onclick="checkAssociatedProd();"
                                title="Click here to View Product/UPC List" /> </a></td>
					</tr>
			
				</table>
			</form:form>
			<div class="navTabSec mrgnRt" align="right">
			<input name="Back" value="Back" type="button" class="btn"
						onclick="location='/ScanSeeWeb/retailer/rebatesretailMfg.htm'" title="Back" />
				<input class="btn" onclick="previewRetAddRebPopUp()"
					value="Preview" type="button" name="Cancel"  title="Preview" /> <input class="btn"
					onclick="addRetailerRebate();" value="Submit" type="button"
					name="Cancel" title="Submit"/>
			</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
			<div class="headerIframe">
				<img src="/ScanSeeWeb/images/popupClose.png" class="closeIframe" alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
				height="100%" allowtransparency="yes" width="100%"
				style="background-color: White"> </iframe>
		</div>

	</div>
	<div class="clear"></div>
</div>
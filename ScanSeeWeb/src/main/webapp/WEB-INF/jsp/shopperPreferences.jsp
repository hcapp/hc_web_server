<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<body onload="resizeDoc();" onresize="resizeDoc();">
<form:form name="editdealsform" commandName="preferencesform">
<div id="dockPanel">
			<ul class="tabs" id="prgMtr">
			<!-- 	<li><a href="/ScanSeeWeb/welcome.htm" rel="home" title="Home">Home</a> -->
				</li>

				<li><a href="https://www.scansee.net/links/aboutus.html" title="About HubCiti"
				target="_blank" rel="About HubCiti">About HubCiti</a></li>
				<!--<li><a href="/ScanSeeWeb/shopper/createShopperProfile.htm" rel="About You" title="About You">About
					You</a></li>
			-->
				<li><a href="/ScanSeeWeb/shopper/preferences.htm"
					rel="Preferences" title="Preferences" class="tabActive">Preferences</a>
				</li>
				<li><a href="/ScanSeeWeb/shopper/selectschool.htm"
					rel="Pick Your Charity/School" title="Pick Your Charity/School">Select
						School</a>
				</li>
				<li><a href="#" onclick="doubleCheck()"
					rel="Double Check" title="Double Check">Double Check</a>
				</li>

				<li><a href="/ScanSeeWeb/shopper/shopperdashboard.htm" rel="Shop!"
					title="Shop!">Shop</a>
				</li>
			</ul>
			<!--	<a name="Mid" id="Mid"></a>
			<div id="tabdPanelDesc" class="floatR tglSec">
				<div id="filledGlass">
					<img src="/ScanSeeWeb/images/Step4.png" />
					<div id="nextNav">
						<a href="#" onclick="preferencesSubmit()"><img
							src="/ScanSeeWeb/images/nextBtn.png" alt="Next" class="NextNav" /><span>Next</span>
						</a>
					</div>
				</div>

			</div>
			<div id="tabdPanel" class="floatL tglSec">
				<img src="/ScanSeeWeb/images/ShopperFlow1.png" alt="Shopper" />
			</div>-->
		</div>
		<!-- <div id="togglePnl">
			<a href="#"> <img src="/ScanSeeWeb/images/downBtn.png" alt="down"
				width="9" height="8" /> Show Panel</a>
		</div> -->
  <div class="clear"></div>
  <div id="content" class="topMrgn">
    <div class="section shadowImg topMrgn">
      <h5>Here we capture your preferences.</h5>
      <p>HubCiti will never send you a hot-deal on something that  does not fit your lifestyle!</p>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl topMrgn">
        <tr>
          <td colspan="4"><strong>Alerts Preference?</strong></td>
        </tr>
        <tr>
        
						<td width="30%"><form:checkbox path="email" /> </label>  Email
						</td>
						<td width="30%"><form:checkbox path="cellPhone" /> </label> Cell Phone</td>

					
        </tr>
        <tr>
          <td colspan="4"><strong>What type of things are you interested in receiveing deals on?</strong></td>
        </tr>
      
      </table>
      <div class="scrollViewShpr">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="gnrlGrd preferencesLst shprView" >
      <!--<col width="5%" />
      <col width="5%" />
      <col width="90%" />-->
      
            <thead>
              <tr>
                <th align="center" class="shprHdr"><img src="/ScanSeeWeb/images/setting.png" alt="settings" width="16" height="16" /></th>
                <th colspan="2" align="left" class="shprHdr">Select Your Preferences</th>
              </tr>
            </thead>
            <tbody>
			
			
			<c:forEach items="${sessionScope.categoryDetailresponse.mainCategoryDetaillst}" var="item">
              
			  <tr name="${item.mainCategoryName}" class="mainCtgry">
                <td width="4%" align="center"  class="wrpWord">
			
				<input type="checkbox" />
              
				</td>
                <td colspan="2"  class="wrpWord">
				<c:out value ="${item.mainCategoryName}"/>
				
				</td>
              </tr>
			  
			  <c:forEach items="${item.subCategoryDetailLst}" var="item1">
              <tr name="${item.mainCategoryName}">
                <td align="center" class="wrpWord">&nbsp;</td>
                <td width="4%" class="wrpWord">
				
				
				
				
				
				<c:choose>
				
				<c:when test="${item1.displayed==0}">
				<form:checkbox path="favCatId" value="${item1.categoryId}"/>
				</c:when>
				<c:otherwise>
				<form:checkbox path="favCatId" checked = "checked" value="${item1.categoryId}"/>
				</c:otherwise>
				</c:choose>
				
				
				
				
				
				
				
				
				
				</td>
                <td width="92%" class="wrpWord">
				 <c:out value ="${item1.subCategoryName}"/>
				
				</td>
              </tr>
            </c:forEach>
               
            </c:forEach>  
             
             
             
              
            </tbody>
          </table></div>
      <div class="navTabSec RtMrgn">
        <div align="right">
          <input name="Cancel3" value="Next" type="button" class="btn" onclick="preferencesSubmit()" title="Next"/>
        </div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="section topMrgn">
     <!--  <div class="block floatL">
        <h3>Funding Higher Education<span>Through Commerce Networking </span></h3>
        <div class="section_info"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
          <p class="topPadding linkMore"><img src="images/linkArrow.png" alt="LinkBullet" width="4" height="6" /> <a href="#">Click here for details</a></p>
        </div>
      </div>
      <div class="block floatL">
        <h3>Privacy<span>Respected</span></h3>
        <div class="section_info"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
          <p class="topPadding linkMore"><img src="images/linkArrow.png" alt="LinkBullet" width="4" height="6" /> <a href="#">Click here for details</a></p>
        </div>
      </div>
      <div class="block floatL clrMargin">
        <p><img src="../images/scanseeMan.png" alt="ScanSee Man" align="absmiddle" />100% Happiness Guarantee</p>
        <p><img src="../images/testimonial-img.png" alt="Testimonial" /></p>
      </div>
      <div class="clear"></div> -->
    </div>
  </div>
 

</form:form>
</body>
</html>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<%@ page import="java.text.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.Date.*"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="menu" uri="/WEB-INF/ConumerMenu.tld"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script type="text/javascript">
$(document).ready(function() {

	var consumerType = '${sessionScope.consumerReqObj.consumerType}'

	if (consumerType == 'GuestUser') {
		setInterval(activateSession, 1080000);
		//setInterval(activateSession, 1000);
	}

});

	function isNumeric(vNum) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;
		var vPostal = document.getElementById("zipCode");
		for (i = 0; i < vNum.length && IsNumber == true; i++) {
			Char = vNum.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				vPostal.value = "";
				vPostal.focus();
				IsNumber = false;
			}
		}
		return IsNumber;
	}

	function activateSession() {

		$.ajaxSetup({
			cache : false
		});
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/shopper/activatesession.htm",
			data : {

			},

			success : function(response) {

			},
			error : function(e) {
				// alert('Error: ' + e);
			}
		});
	}
</script>

<form name="paginationForm">
	<input type="hidden" name="pageNumber" /> <input type="hidden"
		name="recordCount" /> <input type="hidden" name="pageFlag" />
		<input name="screenName" type="hidden" />
</form>

<form name="SearchFormHeader">
	<input type="hidden" name="pagination" /> <input name="recordCount"
		type="hidden" /> <input name="categoryName" type="hidden" />

	<div id="wrapper">
		<div id="hdr">
			<%
				String currentDate = null;
				try
				{
					DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
					Date date1 = new Date();
					currentDate = df.format(date1);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					// TODO: handle exception
				}
			%>
			<div id="hdrWrpr">
				<div class="row">
					<div class="col1 logo floatL">

						<c:if
							test="${sessionScope.loginuser eq null && empty sessionScope.loginuser}">
							<a href="https://www.scansee.net" target="_blank"> <img
								src="/ScanSeeWeb/images/consumer/scanseeLogo.png" alt="ScanSee" />
							</a>
						</c:if>

						<c:if
							test="${sessionScope.loginuser ne null && !empty sessionScope.loginuser}">
							<a href="javascript:void(0);"> <img
								src="/ScanSeeWeb/images/consumer/scanseeLogo.png" alt="ScanSee" />
							</a>
						</c:if>


					</div>
					<div class="col2">
						<div id="usrAcces" class="usr_setting">
							<c:choose>
								<c:when test="${empty sessionScope.loginuser}">
									<ul class="beforeLogin">
										<li><a title="Log In" href="/ScanSeeWeb/login.htm">
												Log In </a></li>
										<li><a href="https://www.scansee.net/scansee_signup.html"
											title="Sign Up"> Sign Up </a></li>
										<li class="last"><%=currentDate%></li>
									</ul>
								</c:when>
								<c:when test="${sessionScope.loginuser.userType == 1}">
									<ul>
										<label> Welcome </label>
										<span> <c:out value="${sessionScope.truncFirstName}"></c:out>
											<i class="normal"><%=currentDate%></i> </span>
										<a href="#"> <img src="../images/consumer/settingicon.png"
											alt="settings" id="settingActn" /> </a>
										<li class="last"></li>
										<li class="clear"></li>
									</ul>
								</c:when>
							</c:choose>
						</div>
						<div class="dropdwnBx">
							<ul>
								<li><a href="consfetchuserinfo.htm"> <img
										src="../images/consumer/user.png" alt="user" /> User Settings
								</a></li>
								<li><a href="conspreferences.htm"> <img
										src="../images/consumer/ctgry.png" alt="category" />
										Preferred Category </a></li>
								<li><a href="consfetchusersettings.htm"> <img
										src="../images/consumer/radius.png" alt="radius" /> Select
										Radius </a></li>
								<li><c:choose>
										<c:when test="${sessionScope.FaceBookLogin ne null}">
											<a href="/ScanSeeWeb/shopper/facebookSignout.htm"> <img
												src="/ScanSeeWeb/images/consumer/logout.png" alt="Logout" />
												Logout </a>
										</c:when>
										<c:otherwise>
											<a href="/ScanSeeWeb/logout.htm" title="Logout"> <img
												src="/ScanSeeWeb/images/consumer/logout.png" alt="Logout"
												title="Logout" /> Logout </a>
										</c:otherwise>
									</c:choose></li>
							</ul>
						</div>
						<c:if test="${requestScope.showHeaderSearch eq null}">
							<div class="srchPnl">
								<div class="row">
									<label class="bold"> Find </label> <input name="searchKey"
										title="Enter keyword to search" type="text"
										onkeypress="return validateFind(event)" /> &nbsp; <input
										name="zipCode" type="text" class="inputSmaller"
										placeholder="Zipcode" title="Enter zipcode" maxlength="5"
										onkeypress="return isNumberKey(event)"
										onchange="isNumeric(this.value);" /> <a
										href="javascript:void(0);"> <img
										src="/ScanSeeWeb/images/consumer/search.png" alt="search"
										id="FindSearchHdr" /> </a>
								</div>
								<div class="row srchOptns">
									<input name="srchOptns" checked="checked" type="radio"
										value="Products" /> <label>Products</label> <input
										name="srchOptns" type="radio" value="Locations" /> <label>Locations</label>
									<input name="srchOptns" type="radio" value="Deals" /> <label>Deals</label>
								</div>
							</div>
						</c:if>
					</div>
				</div>
				<div class="clear"></div>
				<c:choose>
				<c:when test="${sessionScope.consumerReqObj.fromModule ne null && sessionScope.consumerReqObj.fromModule  eq 'shoplist'}">
				
				<menu:MenuTag menuTitle="${sessionScope.consumerReqObj.moduleName}" />
				</c:when>
				<c:otherwise>
				<menu:MenuTag menuTitle="${sessionScope.consumerReqObj.moduleName}" />
				</c:otherwise>
				</c:choose>
				
			</div>
		</div>
	</div>
</form>


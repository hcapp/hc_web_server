<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

<!-- Styles  -->

<link rel="shortcut icon" href="/ScanSeeWeb/images/ScanSeeLogo.png" />
<link href="/ScanSeeWeb/styles/consumercolorbox.css" type="text/css"
	rel="stylesheet" />
<link href="/ScanSeeWeb/styles/consumerstyle.css" type="text/css"
	rel="stylesheet" />

<!--  Scripts -->
<script src="/ScanSeeWeb/scripts/shopper/consumerjquery-1.6.2.min.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consumerjquery.colorbox.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consumerjquery.corner.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consumerjquery.hoverIntent.js"
	type="text/javascript"></script>
<script
	src="/ScanSeeWeb/scripts/shopper/consumerjquery.hoverIntent.R6.minified.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consumerjquery.MetaData.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consumerjquery.rating.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consumerglobal.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/consumerweb.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/shopper.js"
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/thislocation.js" 
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/shopper/shoppinglist.js" 
	type="text/javascript"></script>
<script src="/ScanSeeWeb/scripts/autocomplete.js" 
	type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:getAsString name="title" ignore="true" /></title>
</head>
<body>
	<div>
		<tiles:insertAttribute name="consumerheader" />
	</div>
	<div id="wrapper">

		<div>
			<tiles:insertAttribute name="body" />
		</div>
	</div>
	<div>
		<tiles:insertAttribute name="consumerfooter" />
	</div>

</body>
</html>
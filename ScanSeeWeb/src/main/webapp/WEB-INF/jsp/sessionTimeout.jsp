<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>HubCitiHubCiti</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.0)"/>
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.0)"/>
<link href="/ScanSeeweb/styles/style.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/ScanSeeweb/scripts/jquery-1.6.2.min.js"></script>
<script src="/ScanSeeweb/scripts/jquery.ticker.js" type="text/javascript"></script>
<script src="/ScanSeeweb/scripts/jquery.jscroll.js" type="text/javascript"></script>
<script src="/ScanSeeweb/scripts/global.js" type="text/javascript"></script>
<script type="text/javascript">
<!-- Begin
function popUp(URL) {
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=300,left = 390,top = 362');");
}
// End -->
</script>
<link rel="stylesheet" href="styles/bubble-tooltip.css" media="screen"/>
<script type="text/javascript" src="scripts/bubble-tooltip.js"></script>
</head>
<body onload="resizeDoc();" onresize="resizeDoc();">
<div id="bubble_tooltip">
  <div class="bubble_top"><span></span></div>
  <div class="bubble_middle"><span id="bubble_tooltip_content">Content is comming here as you probably can see.Content is comming here as you probably can see.</span></div>
  <div class="bubble_bottom"></div>
</div>
<div id="wrapper">
  <div id="header">
    <!--<div class="floatR" id="header_link">
      <ul>
        <li> <img alt="User" src="images/icon_user.png" align="left"/> Welcome <span>Stephanie Blackwell</span> </li>
        <li>Nov 2, 2011 </li>
        <li> <img alt="Logout" src="images/logoutIcon.gif" align="left"/> <a href="../index.html">Logout</a> </li>
      </ul>
    </div>-->
    <div class="floatL" id="header_logo"> <a href="/ScanSeeWeb/welcome.htm"> <img alt="ScanSee" src="images/hubciti-logo.png" width="184" height="66"/></a> </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
  <div id="content" class="PageNf">
		<h1 align="center">Your session has been expired.</h1>
		<br />
		<br />
	
		<h4 align="center"><u><a href="/ScanSeeWeb/login.htm">Please Login Again</a></u></h4>
		
  </div>
  <div id="footer">
    <div id="ourpartners_info">
      <div class="floatL" id="followus_section">
        <div class="section_title">Connect with us!</div>
        <p> <img alt="followus" src="images/followus-img-new.png" border="0"/> </p>
      </div>
      <div class="clear"></div>
    </div>
    <div id="footer_nav">
      <ul>
        <li> <a href="#">About Us</a> </li>
        <li> <a href="#">Terms of Use</a> </li>
        <li> <a href="#">Privacy Policy</a> </li>
        <li> <a href="#">Contact Us</a> </li>
        <li> <a href="#">FAQ&rsquo;s</a> </li>
        <li> <a href="#">Help</a> </li>
        <li class="last"> <a href="#">Employment Opportunities</a> </li>
        <li class="clear"></li>
      </ul>
      <div class="clear"></div>
      <div id="footer_info">Copyright &copy; 2015 HubCiti. All rights reserved </div>
    </div>
  </div>
</div>
</body>
</html>
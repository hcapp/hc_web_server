<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>  
<%@page session="true"%>
<%@ page import="common.pojo.PlanInfo"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<script src="scripts/web.js" type="text/javascript"></script>
 <% ArrayList<PlanInfo> planinfolist = (ArrayList<PlanInfo>)session.getAttribute("planinfolist"); %>
<script type="text/javascript">
	

	function checkCheckBox() {
		if (document.myForm.agreeToPay.checked == false){
		alert ('You didn\'t choose  the checkbox!');
		return false;
		}else{
		return true;
             
		
		}
		}
	$(document).ready(function() {
		  $('#checkBoxError').hide();
		  totalMonthlyYearlyRetailerPrice(<%= planinfolist.size()%>,false);
		  
		});
</script>
<form:form name="myForm" commandName="myForm">
<form:form name="plandetailform" commandName="chooseplanform">
  <input type="hidden"  name="planJson"/>
  <input type="hidden"  name="grandTotal"/>
  <div id="dockPanel">
    <ul id="prgMtr" class="tabs">
      <!--   <li> <a title="Home" href="#" rel="home">Home</a> </li> -->
     <!--  <li> <a title="About ScanSee" href="https://www.scansee.net/links/aboutus.html" rel="About ScanSee" target="_blank">About ScanSee</a> </li> -->
     <!-- <li> <a title="Create Profile" href="Mfg_createProfile.html" rel="Create Profile">Create Profile</a> </li>-->
     <!--  <li> <a title="Product Setup" href="registprodsetup.htm" rel="Product Setup">Product Setup</a> </li> -->
      <li> <a class="tabActive" title="Choose Your Plan" href="choosePlan.htm" rel="Choose Your Plan">Choose Plan</a> </li>
      <!-- <li> <a title="Dashboard" href="dashboard.htm" rel="Dashboard">Dashboard</a> </li> -->
        <li> <a title="Dashboard" href="#" rel="Dashboard">Dashboard</a> </li>
    </ul>
    
   
    		<!--Commented this code for Show panel issue --Manju-->
    	<!--	<a id="Mid" name="Mid"></a>
			<div id="tabdPanelDesc" class="floatR tglSec">
				<div id="filledGlass">
					<img src="images/Step4.png" />
					<div id="nextNav">
						<a href="#" onclick="planInfo()"> <img class="NextNav_R"
							border="0" alt="Next" src="images/nextBtn.png" /> <span>Check-out
								and<br />Get started!</span> </a>
					</div>
				</div>
			</div>
			<div id="tabdPanel" class="floatL tglSec">
				<img alt="Flow5" src="images/Flow5_mfg.png" />
			</div>-->
		</div>
		<!-- <div id="togglePnl">
			<a href="#"> <img src="images/downBtn.png" alt="down" width="9"
				height="8" /> Show Panel</a>
		</div> -->
		<div id="content" class="MrgnTop">
    <div class="section shadowImg ">
      <table class="brdrLsTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
          <tr>
            <td width="85%"><h5>Thank you! It looks like you qualify for our basic starter package,plus a few unique ads you can start immediately!</h5></td>
            <td valign="top" width="15%" align="right"><img alt="shopping cart" src="images/shopingCart-img.png" width="71" height="46"/> </td>
          </tr>
        </tbody>
      </table>
<!--  <a id="Mid" name="Mid"></a>
    <div id="tabdPanelDesc" class="floatR tglSec">
        <div id="filledGlass"> <img src="images/Step4.png"/>
          <div id="nextNav"> 
          	<a href="#" onclick="planInfo()"> 
          		<img class="NextNav_R" alt="Next" src="images/nextBtn.png"/> 
          			<span>Check-out and Get started!</span>
          	</a> 
          </div>
        </div>
    </div>
      <div id="tabdPanel" class="floatL tglSec"> <img alt="Flow5" src="images/Flow5_mfg.png"/> </div>

  </div>
  <div class="clear"></div>
  <div id="togglePnl"><a href="#">  <img src="images/downBtn.png" alt="down" width="9" height="8" /> Show Panel</a></div>
  
  <div id="content" class="MrgnTop">
    <div class="section shadowImg ">
      <table class="brdrLsTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
          <tr>
            <td width="85%"><h5>Thank you! It looks like you qualify for our basic starter package,plus a few unique ads you can start immediately!</h5></td>
            <td valign="top" width="15%" align="right"><img alt="shopping cart" src="images/shopingCart-img.png" width="71" height="46"/> </td>
          </tr>
        </tbody>
      </table>
      
      <a id="Mid" name="Mid"></a>
    <div id="tabdPanelDesc" class="floatR tglSec">
      <div id="filledGlass"> <img src="/images/Step4.png"/>
        <div id="nextNav"> 
        		<a href="#" onclick="planInfo()">
        		 <img class="NextNav_R" border="0" alt="Next" src="/images/nextBtn.png"/> 
        		 	<span>Check-out and<br/>Get started!</span></a> 
        </div>
       </div>
    </div>
    <div id="tabdPanel" class="floatL tglSec"> <img alt="Flow5" src="/images/Flow5_mfg.png"/> </div>
  </div>
  <div id="togglePnl"><a href="#"> <img src="/images/downBtn.png" alt="down" width="9" height="8" /> Show Panel</a></div>
  <div id="content" class="MrgnTop">
    <div class="section shadowImg">
      <table class="brdrLsTbl" border="0" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
          <tr>
            <td><h5>Thank you! Modular Advertising opportunities to fit your budget, season or individual locations. Learn about upgrades on your Dashboard:</h5></td>
            <td valign="top" width="43%" align="right"><img alt="shopping cart" src="/images/shopingCart-img.png" width="71" height="46"/> </td>
          </tr>
        </tbody>
      </table>-->
      
      <div class="grdSec brdrTop">
        <table class="cstmTbl"  border="0" cellspacing="0" cellpadding="0" width="100%" id="choosePln">
          <tbody>
            <tr class="header">
             <td style="width: 90px;">PRODUCT</td>
              <td style="width: 260px;">DESCRIPTION</td>
              <td style="width: 200px;">PAYMENT PLAN</td>
              <td style="width: 113px;">AMOUNT</td>
              <td style="width: 82px;">PRICE($)</td>
              <td style="width: 84px;">SUBTOTAL($)</td>
            </tr>
         
           <% int count = 0; %>
		   <c:forEach items="${sessionScope.planinfolist}" var="item" >
		    <% count++;%>
		   <input type="hidden"  id="defaultMonthlyPrice<%=count%>"  value="${item.retailerBillingMonthly}"/>
            <input type="hidden"  id="defaultYearlyPrice<%=count%>"  value="${item.retailerBillingYearly}"/>
            <tr id="<%=count%>" >
			  
              <td class="editLink"><c:out value="${item.productName}"/></td>
              <td style="width: 500px;" class="editLink"><c:out value="${item.description}"/></td>
              
              <td  class="editLink">
              
              <%-- <c:out value="${item.description}"/> --%>
              
                  <p>
                     <input type="hidden"  id="selectedMonthlyPrice<%=count%>"  value="${item.retailerBillingMonthly}"/>
                     <input type="hidden"  id="selectedYearlyPrice<%=count%>"  value="${item.retailerBillingYearly}"/>
                  	<label><input name="radio<%=count%>" type="radio" id="monthly" value="monthly" checked="checked" onclick="loadPricePerLocation('radio<%=count%>','selectedMonthlyPrice<%=count%>',<%= planinfolist.size()%>,'price<%=count%>',true)"/> Monthly</label>
                  	&nbsp; 
                  	<span id="defaultMonthlyPriceSpanId<%=count%>">
                  		($${item.retailerBillingMonthly})
                  	</span>                
                  	              
				  </p>
                  <p>&nbsp;</p>
                  <p>
                  	<input type="radio" name="radio<%=count%>" id="Yearly" value="Yearly" onclick="loadPricePerLocation('radio<%=count%>','selectedYearlyPrice<%=count%>',<%= planinfolist.size()%>,'price<%=count%>',true)" />
                  	<label for="Yearly"> Yearly</label> &nbsp; 
                  	<span id="defaultYearlyPriceSpanId<%=count%>">
                  		($${item.retailerBillingYearly})
                  	</span>                
                  	         
                  	
                  </p>
              
              
              </td>
            
            
              <td class="editLink">
			   <%-- <input type="hidden"  name="productId" value="${item.productId}"/>
              <input type="text"  name="qty" class="textboxSmall" onkeypress="return isNumberKey(event)"  onchange ="totalPrice(<%= planinfolist.size()%>);"/ > --%> 
              
              <input type="hidden"  name="productId" value="<c:out value='${item.productId}'/>"/>
              <%-- <input type="text"   name="qty"   onkeypress="return isNumberKey(event)" onchange ="totalMonthlyYearlyRetailerPrice(<%= planinfolist.size()%>,false);"  value="${sessionScope.retailerProfile.numOfStores}" style="color: #465259;	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;  width: 50px;"/> --%>
              <input type="text"   name="qty"   onkeypress="return isNumberKey(event)" onchange ="totalMonthlyYearlyRetailerPrice(<%= planinfolist.size()%>,false);"  value="1" style="color: #465259;	font-family: Arial, Helvetica, sans-serif;	font-size: 12px;  width: 50px;"/>
              </td>
              
              <td class="editLink">
              <label class="textbox"></label>
              <%-- <input  readonly="readonly" class="textboxSmall"  name="price" value="${item.price}"/> --%>
              	<input  type="text" readonly="readonly" style="color: #465259;	font-family: Arial, Helvetica, sans-serif; height: 22px; line-height: 20px;	font-size: 12px;  width: 50px;"  name="price" id="price<%=count%>" value="${item.retailerBillingMonthly}"/>
              </td>
              <td class="editLink">
                  <%--  <input readonly="readonly"  class="textboxSmall"  name="total" value="${item.total}"/> --%>
               <input type="text" readonly="readonly"   name="total" value="${item.total}" style="color: #465259; font-family: Arial, Helvetica, sans-serif; height: 22px; line-height: 20px; font-size: 12px;  width: 50px;"/>
               </td>
               </tr>
              
         </c:forEach>
           
            <tr class="dottedLine">
              <td class="dottedLine"></td>
              <td></td>
              <td></td>
              <td class="highlightCol"></td>
              <td align="center"></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>Subtotal($)</td>
              <td> <input readonly="readonly" id="sub" class="textboxSmall" value="${item.subTotal}"/>
			  </td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td><strong>Discount Code:</strong>&nbsp;&nbsp;&nbsp;<br>
               <!-- <input type="text"  id= "discountCode" class="textboxSmall" size="3" onkeypress="return isNumberKey(event)" onchange="getDiscount()"/> -->
                  <input type="text"  id= "discountCode" style="width: 106px;" size="3"  onchange="getSupplierDiscountMonthlyAndYearly(<%=count%>,<%= planinfolist.size()%>)"/>             
			   </td>
                <td><strong>Total before Taxes($)</strong></td>
              <td><strong><input readonly="readonly" id="discountsubtotal" class="textboxSmall" value="${item.discountSubTotal}"/></strong></td>
            </tr>
            <tr class="dottedLine">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            
            <!--  <tr height="10px"/> -->
            
           <%--   <tr class="header">
				<td colspan="5" class="editLink">Monthly invoice items</td>
			</tr>
              
             <% ArrayList<PlanInfo> planinfofeelist = (ArrayList<PlanInfo>)session.getAttribute("planinfofeelist"); %>
                 <tr>
                   <td>Product Fee (per UPC)</td>
				  <td>
                  <c:forEach items="${sessionScope.planinfofeelist}" var="items" >
				     <p><c:out value="${items.feedescription}"/></p>
  				  </c:forEach>
                  </td> 
                 </tr>
               
                 <tr>  
                  <td></td>
                  <td><a href="#" onclick="openIframePopup('ifrmPopup','ifrm','./productfee.html',420,860,'Pay Later')" 
                                  title="View details">View details</a></td>
                  <td></td>
                 </tr> 
                <tr>
            
                  <td></td>
                  <td><span class="editLink" style="width: 500px;">
                  	 <div id="checkBoxError" style="color:red"  >Please check the checkbox</div>
                    <input id="checkbox" value="on" type="checkbox" name="agreeToPay" onclick="validateCheckbox()"/><!--onclick="checkCheckBox();"
                    --><font color="#465259"> <strong>I agree to pay for product fees</strong></font></span></td>
                  <td></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr> --%>
          </tbody>
        </table>
         <div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
		<div class="headerIframe">
			<img src="images/popupClose.png" class="closeIframe" alt="close" onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
			title="Click here to Close" align="middle" /> 
			<span id="popupHeader"></span>
		</div>
		<iframe frameborder="0" scrolling="auto" id="ifrm" src="" height="100%" allowtransparency="yes" width="100%" style="background-color: White"> </iframe>
		</div>
        
        
          
        <div class="navTabSec mrgnRt" align="right">
         <input type="hidden"  id="skipPayment"/>
          <input class="btn"   onclick="planInfo()" value="Next" type="button" name="Cancel3" title="Next" />

        </div>
      </div>

    </div>
    <div class="clear"></div>
    <div class="section topMrgn">
      <div class="clear"></div>
    </div>
  </div>
 
</form:form>
</form:form>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script src="/ScanSeeWeb/scripts/web.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css"
	href="/ScanSeeWeb/styles/bubble-tooltip.css" media="screen" />
<script type="text/javascript"
	src="/ScanSeeWeb/scripts/bubble-tooltip.js"></script>
<link rel="stylesheet" href="/ScanSeeWeb/styles/jquery-ui.css" />
<script src="/ScanSeeWeb/scripts/jquery-1.8.3.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.js"></script>
<script type="text/javascript" src="/ScanSeeWeb/scripts/autocomplete.js"></script>
<%
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script>
$(document).ready(function() {
	$("#City").live("keydown",function(e)
			{
				var s = String.fromCharCode(e.which);
				if (s.match(/[a-zA-Z0-9\.]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#pCode').val("");
				   cityAutocomplete('pCode');
				}else if(s.match(/[\b]/)){
					$('#stateCodeHidden').val("");
					$('#stateHidden').val("");
					$( "#Country" ).val("");
					$('#pCode').val("");
					cityAutocomplete('pCode');
				}
							
			});
var stateCode = '';
var state = '';
if($('#pCode').val().length == 5){
	stateCode = $('#stateCodeHidden').val();
	state = $('#stateHidden').val();
	$( "#Country" ).val(state);			 
	}else{
	$('#stateCodeHidden').val("");
	$('#stateHidden').val("");
	$( "#Country" ).val("");
	}
});
</script>
<SCRIPT LANGUAGE="JavaScript">
<!-- Begin
	function popUp(URL) {
		day = new Date();
		id = day.getTime();
		eval("page"
				+ id
				+ " = window.open(URL, '"
				+ id
				+ "', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=1000,height=600,left = 390,top = 162');");
	}
// End -->
</script>

<script type="text/javascript">
/*	function loadCity() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		var selCity = document.createprofileform.cityHidden.value;

		$
				.ajax({
					type : "GET",
					url : "/ScanSeeWeb/retailer/retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : selCity
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.createprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						if (cityList.length > 1) {
							document.createprofileform.City.options.length = cityList.length + 1;
							for ( var i = 0; i < cityList.length; i++) {
								cityDropDown.options[optIndex].value = cityList[i];
								cityDropDown.options[optIndex].text = cityList[i];
								optIndex++;
							}
						}

						//$('#myAjax').html(response);
						onCitySelectedLoad();
					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}
	function onChangeState() {
		// get the form values
		var stateCode = $('#Country').val();
		var optIndex = 1;
		var cityDropDown = document.getElementById("City");
		document.createprofileform.cityHidden.value="";
		$
				.ajax({
					type : "GET",
					url : "/ScanSeeWeb/retailer/retailerfetchcity.htm",
					data : {
						'statecode' : stateCode,
						'city' : ""
					},

					success : function(response) {
						var responseJSON = JSON.parse(response);
						var cityStr = responseJSON.City;
						var cityList = cityStr.split(",");
						document.createprofileform.City.options.length = cityList.length;
						cityDropDown.options[0].value = '0';
						cityDropDown.options[0].text = '--Select--';
						cityDropDown.options[0].selected = true;

						for ( var i = 0; i < cityList.length; i++) {
							cityDropDown.options[optIndex].value = cityList[i];
							cityDropDown.options[optIndex].text = cityList[i];
							optIndex++;

						}

					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}*/

	function clearSupplierForm() {
		var r = confirm("Do you really want to clear the form")
		var cityHtml = "<select name='city' id='City' tabindex='5'><option value='0'>--Select--</option></select>";
		if (r == true) {
			document.createprofileform.supplierName.value = "";
			document.createprofileform.corpAdderess.value = "";
			document.createprofileform.Address2.value = "";
			document.createprofileform.postalCode.value = "";
			document.createprofileform.state.value = "0";
			document.createprofileform.city.value = "0";
			$('#myAjax').html(cityHtml);
			document.createprofileform.postalCode.value = "";
			//document.createprofileform.nonProfit.checked = false;
			document.getElementById('bCategory').selectedIndex = -1;
			document.createprofileform.contactFName.value = "";
			document.createprofileform.contactLName.value = "";
			document.createprofileform.legalAuthorityFName.value = "";
			document.createprofileform.legalAuthorityLName.value = "";
			document.createprofileform.acceptTerm.checked = false;
			document.getElementById('phnNum').value = "";
			document.getElementById('contPhNm').value = "";
			document.createprofileform.contactEmail.value = "";
			document.createprofileform.userName.value = "";
			if (document.getElementById('supplierName.errors') != null) {
				document.getElementById('supplierName.errors').style.display = 'none';
			}
			if (document.getElementById('corpAdderess.errors') != null) {
				document.getElementById('corpAdderess.errors').style.display = 'none';
			}
			if (document.getElementById('state.errors') != null) {
				document.getElementById('state.errors').style.display = 'none';
			}
			if (document.getElementById('city.errors') != null) {
				document.getElementById('city.errors').style.display = 'none';
			}
			if (document.getElementById('postalCode.errors') != null) {
				document.getElementById('postalCode.errors').style.display = 'none';
			}
			if (document.getElementById('corpPhoneNo.errors') != null) {
				document.getElementById('corpPhoneNo.errors').style.display = 'none';
			}
			if (document.getElementById('bCategory.errors') != null) {
				document.getElementById('bCategory.errors').style.display = 'none';
			}
			if (document.getElementById('legalAuthorityFName.errors') != null) {
				document.getElementById('legalAuthorityFName.errors').style.display = 'none';
			}
			if (document.getElementById('legalAuthorityLName.errors') != null) {
				document.getElementById('legalAuthorityLName.errors').style.display = 'none';
			}
			if (document.getElementById('contactFName.errors') != null) {
				document.getElementById('contactFName.errors').style.display = 'none';
			}
			if (document.getElementById('contactLName.errors') != null) {
				document.getElementById('contactLName.errors').style.display = 'none';
			}
			if (document.getElementById('contactPhoneNo.errors') != null) {
				document.getElementById('contactPhoneNo.errors').style.display = 'none';
			}
			if (document.getElementById('contactEmail.errors') != null) {
				document.getElementById('contactEmail.errors').style.display = 'none';
			}
			if (document.getElementById('userName.errors') != null) {
				document.getElementById('userName.errors').style.display = 'none';
			}
			if (document.getElementById('acceptTerm.errors') != null) {
				document.getElementById('acceptTerm.errors').style.display = 'none';
			}
			if (document.getElementById('createprofileform.errors') != null) {
				document.getElementById('createprofileform.errors').style.display = 'none';
			}
		}
	}

	function onLoadCategory() {
		var vCategoryID = document.createprofileform.bCategoryHidden.value;
		var vCategoryVal = document.getElementById("bCategory");
		if (vCategoryID != "null") {
			var vCategoryList = vCategoryID.split(",");
		}
		for ( var i = 0; i < vCategoryVal.length; i++) {
			for (j = 0; j < vCategoryList.length; j++) {
				if (vCategoryVal.options[i].value == vCategoryList[j]) {
					vCategoryVal.options[i].selected = true;
					break;
				}
			}
		}
	}
	function getCityTrigger(val) {	
		if(val == ""){
			$( "#Country" ).val("");
			$( "#pCode" ).val( "" );
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		document.createprofileform.cityHidden.value = val;
		document.createprofileform.city.value = val;
	}
	
	function clearOnCityChange() {	
		//alert($('#citySelectedFlag').val())
		if($('#citySelectedFlag').val() != 'selected'){
		$( "#Country" ).val("");
		$( "#pCode" ).val( "" );
		$('#stateCodeHidden').val("");
		$('#stateHidden').val("");
		}else{
		$('#citySelectedFlag').val('');
		}
	}
	function isEmpty(vNum) {
		if(vNum.length < 5){
			//$( "#state" ).val("");
			$( "#Country" ).val("");
			$( "#City" ).val("");
			$('#stateCodeHidden').val("");
			$('#stateHidden').val("");
		}
		return true;
	}

	/*function onCitySelectedLoad() {
		var vCityID = document.createprofileform.cityHidden.value;
		var sel = document.getElementById("City");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCityID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}*/

	function isNumeric(vNum) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;
		var vPostal = document.getElementById("pCode");
		for (i = 0; i < vNum.length && IsNumber == true; i++) {
			Char = vNum.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				vPostal.value = "";
				vPostal.focus();
				IsNumber = false;
			}
		}
		return IsNumber;
	}
</script>

<div id="wrapper">
	<div id="dockPanel">
		<ul id="prgMtr" class="tabs">
			<!--	<li><a title="Home" href="#" rel="home">Home</a>-->
			</li>
			<!-- <li><a href="https://www.scansee.net/links/aboutus.html" title="About ScanSee"
				target="_blank" rel="About ScanSee">About ScanSee</a>
			</li> -->
			<li><a class="tabActive" title="Create Profile" href="#"
				rel="Create Profile">Create Profile</a></li>
			<!--    <li> <a title="Product Setup" href="../prodMgmt_mfg.html" rel="Product Setup">Product Setup</a> </li>
      <li> <a title="Choose Your Plan" href="Mfg_choosePlan.html" rel="Choose Your Plan">Choose Plan</a> </li>
      <li> <a title="Dashboard" href="../adminpage_mfg.html" rel="Dashboard">Dashboard</a> </li>-->
		</ul>
	<!--Commented this code for Show panel issue --Manju-->
	<!--	<div id="tabdPanelDesc" class="floatR tglSec">
			<div id="filledGlass">
				<img src="images/Step3.png" />
				<div id="nextNav">
					<a href="#" onclick="createProfile();"> <img class="NextNav_R"
						alt="Next" src="images/nextBtn.png" /> <span>Set up your
							profile so that customers can find you!</span> </a>
				</div>
			</div>
		</div>
		<div id="tabdPanel" class="floatL tglSec">
			<img alt="Flow3" src="images/Flow3a_mfg.png" />
		</div>-->
	</div>
	<!-- <div id="togglePnl">
		<a href="#"> <img src="images/downBtn.png" alt="down" width="9"
			height="8" /> Show Panel</a>
	</div> -->
	<div id="content" class="topMrgn">
		<div class="section">

			<div class="infoSecB">HubCiti helps you deliver the right
				message to your customer. To provide you with the best plan options,
				we need to learn little bit more about you.</div>
			<form:form name="createprofileform" commandName="createprofileform"
				acceptCharset="ISO-8859-1">
				<form:hidden path="bCategoryHidden" />
				<form:hidden path="cityHidden" />
				<form:hidden path="stateHidden" id ="stateHidden"/>
				<form:hidden path="stateCodeHidden" id = "stateCodeHidden" />
				<form:hidden path="citySelectedFlag" id = "citySelectedFlag" />
				<!--<form:hidden path="citySelectedFlag" id = "citySelectedFlag" />-->
				<div id="bubble_tooltip">
					<div class="bubble_top">
						<span></span>
					</div>
					<div class="bubble_middle">
						<span id="bubble_tooltip_content">Content is comming here
							as you probably can see.Content is comming here as you probably
							can see.</span>
					</div>
					<div class="bubble_bottom"></div>
				</div>
				<div class="grdSec brdrTop MrgnTop">

					<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
						width="100%">
						<tbody>
							<tr>
								<td class="header">Supplier Registration Form</td>
								<td colspan="3" style="color: red;"><form:errors
										cssStyle="color:red"></form:errors></td>
							</tr>
							<tr>
								<td class="Label"><label for="rtlrName" class="mand">Supplier
										Name</label>
								</td>
								<td colspan="3"><form:errors cssStyle="color:red"
										path="supplierName"></form:errors> <form:input
										path="supplierName" type="text" id="rtlrName"
										name="couponName3" maxlength="100" tabindex="1" /></td>

							</tr>
							<tr>
								<td class="Label" width="17%"><label for="addrs1"
									class="mand">Corporate Address</label>
								</td>
								<td width="33%"><form:errors cssStyle="color:red"
										path="corpAdderess"></form:errors> <form:textarea rows="5"
										cssStyle="height:60px;" cols="45" path="corpAdderess"
										id="addrs1" name="textarea2"
										onkeyup="checkMaxLength(this,'50');" tabindex="2" />
								</td>
								<td class="Label"><label for="addrs3">Address 2</label>
								</td>
								<td><form:textarea rows="5" cols="45" path="Address2"
										id="addrs1" name="textarea2"
										onkeyup="checkMaxLength(this,'50');" tabindex="3"
										cssStyle="height:60px;" />
								</td>
							</tr>
								<tr>
								<td class="Label">
									<label for="pCode" class="mand">Postal Code</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="postalCode"></form:errors>
									<form:input id="pCode"
										tabindex="4" path="postalCode" class="loadingInput"
										maxlength="5" onkeypress="zipCodeAutocomplete('pCode');return isNumberKey(event);"
										onchange="isNumeric(this.value);" onkeyup="isEmpty(this.value);"/>
								</td>
								<td class="Label">
									<label for="cty" class="mand">City</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="city"></form:errors> 
									<form:input path="city" id="City" tabindex="5" class="loadingInput"/>
								</td>
							</tr>
							<tr>
								<td class="Label" align="left">
									<label for="sts" class="mand">State</label>
								</td>
								<td align="left">
									<form:errors cssStyle="color:red" path="state"></form:errors> 
									<form:input path="state" id="Country" tabindex="6" disabled="true"/>
								</td>
								<td class="Label" align="left"><label for="phnNum"
									class="mand">Corporate Phone #</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="corpPhoneNo"></form:errors> <form:input id="phnNum"
										tabindex="7" path="corpPhoneNo" name="couponName8"
										onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);" /><label
									class="Label">(xxx)xxx-xxxx</label></td>
							</tr>
							<tr>
								<td class="Label" align="left"><label for="bcategory"
									class="mand">Business Category</label> (Hold CTRL to select
									multiple categories)</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="bCategory"></form:errors> <form:select path="bCategory"
										id="bCategory" class="txtAreaBox" size="1" multiple="true"
										tabindex="8">
										<c:forEach items="${sessionScope.categoryList}" var="c">
											<form:option value="${c.categoryID}"
												label="${c.parentSubCategory}" />
										</c:forEach>
									</form:select></td>
								<td class="Label" align="left"><label for="nonprofit">
										<!-- Non-profit
										Status --> </label></td>
								<td align="left">
									<!--<form:errors cssStyle="color:red"
										path="nonProfit"></form:errors> <form:checkbox
										path="nonProfit" id="nonProfit" tabindex="9" />  My
									organization is a registered and recognized<br /> 501(c)(3)
									non-profit. -->
								</td>
							</tr>
							<tr>
								<td class="Label" colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td class="Label" colspan="4"><a href="javascript:void(0)"
									onmousemove="showToolTip(event,'The name of the person that has given you authority to act.');return false"
									onmouseout="hideToolTip()"> Legal Authority Name <img
										alt="helpIcon" src="images/helpIcon.png"> </a>
								</td>
							</tr>
							<tr>
								<td class="Label"><label for="lFName" class="mand">First
										Name</label>
								</td>

								<td align="left"><form:errors cssStyle="color:red"
										path="legalAuthorityFName"></form:errors> <form:input
										path="legalAuthorityFName" id="lFName" name="couponName5"
										maxlength="20" tabindex="9" />
								</td>
								<td class="Label" align="left"><label for="llName"
									class="mand">Last Name</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="legalAuthorityLName"></form:errors> <form:input
										path="legalAuthorityLName" id="llName" name="couponName9"
										tabindex="10" value="" maxlength="30" />
								</td>
							</tr>
							<tr>
								<td class="Label hdrTxt" colspan="4">Contact Details</td>
							</tr>
							<tr>
								<td class="Label"><label for="contFName" class="mand">First
										Name</label></td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactFName"></form:errors> <form:input
										path="contactFName" id="contFName" name="couponName5"
										maxlength="30" tabindex="11" /></td>
								<td class="Label"><label for="contLName" class="mand">Last
										Name</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactLName"></form:errors> <form:input
										path="contactLName" id="lname" maxlength="30" tabindex="12" />
								</td>
							</tr>
							<tr>
								<td class="Label" align="left"><label for="contPhNm"
									class="mand">Contact Phone #</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactPhoneNo"></form:errors> <form:input
										path="contactPhoneNo" id="contPhNm" name="couponName"
										tabindex="13" onkeyup="javascript:backspacerUP(this,event);"
										onkeydown="javascript:backspacerDOWN(this,event);" />(xxx)xxx-xxxx</td>
								<td class="Label" align="left">&nbsp;</td>
								<td align="left">&nbsp;</td>
							</tr>
							<tr>
								<td class="Label"><label for="contEml" class="mand">Contact
										Email</label>
								</td>
								<td align="left"><form:errors cssStyle="color:red"
										path="contactEmail"></form:errors> <form:input
										path="contactEmail" id="contEml" name="contactEmail"
										tabindex="14" maxlength="100" onpaste="return false"
										ondrop="return false" ondrag="return false"
										oncopy="return false" autocomplete="off" />
								</td>
								<td class="Label" colspan="2"><label for="contEml">
										This email address will be used to receive your temporary
										password and any future password requests.</label>
								</td>
							</tr>
							<tr>
								<!--  <td class="Label"><label for="contREml" class="mand">Retype
										Email</label></td>
								<td align="left" colspan="3"><form:errors
										cssStyle="color:red" path="retypeEmail"></form:errors> <form:input
										path="retypeEmail" id="contREml" name="retypeEmail"
										tabindex="16" maxlength="100" onpaste="return false"
										ondrop="return false" ondrag="return false"
										oncopy="return false" autocomplete="off" />
								</td>-->

								<td class="Label"><label class="mand" for="manufName">UserName</label>
								</td>
								<td colspan="3" align="left"><form:errors
										cssStyle="color:red" path="userName"></form:errors> <form:input
										path="userName" type="text" maxlength="100" name="userName"
										id="userName" tabindex="15" /></td>
							</tr>

							<tr>
								<td class="Label">&nbsp;</td>
								<td align="left">
									<em> <form:errors cssStyle="color:red" path="acceptTerm"></form:errors> 
										<form:checkbox	path="acceptTerm" tabindex="16" /> <font color="#465259">
										<strong>Accept Terms of Service and Use </strong> </font>
										<div id="acptTrms">
											<A HREF="http://www.scansee.com/links/termsconditions.html"
												target="_blank" tabindex="17"> <u>Terms &amp;
													conditions of HubCiti</u> </a>
										</div> <input class="btn" value="Clear" type="button"
										onclick="clearSupplierForm();" name="Cancel2" tabindex="18"
										title="Clear the form" /> <input class="btn"
										onclick="createProfile();" value="Submit" type="button"
										name="Cancel" tabindex="19" title="Submit" /> </em> <!-- <font color="#50940d">For company
										authorization, ScanSee verifies companies and non-profits
										through &nbsp;Lexis Nexis. You must have legal authority with
										your organization to initiate the &nbsp;ScanSee program.</font> -->
								</td>
								<td colspan="2" align="left">
									<%-- <em> <form:errors
											cssStyle="color:red" path="acceptTerm"></form:errors> <form:checkbox
											path="acceptTerm" tabindex="19" /> <font color="#465259">
											<strong>Accept Terms of Service and Use </strong> </font>

										<div id="acptTrms">
											<A HREF="http://www.scansee.com/links/termsconditions.html" target="_blank" tabindex="20"> <u>Terms &amp; conditions of ScanSee</u>
											</a>
										</div> <input class="btn" value="Clear" type="button"
										onclick="clearSupplierForm();" name="Cancel2" tabindex="21"
										title="Clear the form" /> <input class="btn"
										onclick="createProfile();" value="Submit" type="button"
										name="Cancel" tabindex="22" title="Submit" /> </em> --%></td>
							</tr>
						</tbody>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</div>
<script>
<!-- On load call to fetch city values -->
	<!--loadCity();-->
	onLoadCategory();
</script>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<script src="scripts/web.js" type="text/javascript"></script>
<script src="scripts/validate.js" type="text/javascript"></script>
<link href="/ScanSeeWeb/styles/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/calendarIcon.png'
		});
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/calendarIcon.png'
		});
	});
</script>
<script type="text/javascript">
function loadRetailerLoc() {
	$.ajaxSetup({cache:false});
		var retailerId = $('#retailID').val();
		//alert(retailerId);
		if (name == null) {
			alert("Select Business;");
		}
		$.ajax({
			type : "GET",
			url : "fetchretailerlocation.htm",
			data : {
				'retailerId' : retailerId,
			},
			success : function(response) {
				$('#myAjax').html(response);

			},
			error : function(e) {
				alert("Eror occurred while Showing Retail Locations");
			}
		});
	}


	
</script>

<script type="text/javascript">
	function validateRebateForm() {
		var startDate = document.getElementById("datepicker1").value;
		var endDate = document.getElementById("datepicker2").value;
		if (compareDate(startDate, endDate, "No")) {
			saveEditRebate();
			return true;
		}
		return false;
	}
</script>
</head>
<div id="wrapper">
	<div id="content" class="topMrgn">
		<div class="section topMrgn">
			<div align="center">
				<label style="font: 25"><form:errors cssStyle="color:red" />
				</label>
			</div>
			<div class="grdSec brdrTop">
				<form:form name="editRebatesForm" commandName="editRebatesForm"
					acceptCharset="ISO-8859-1">
					<form:hidden path="retailerLocationID" name="retailerLocationID" />

					<form:hidden path="locationHidden" />
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<tbody>
							<tr>
								<td colspan="4" class="header">Edit Rebate</td>
							</tr>
							<tr>
								<td width="18%" class="Label"><label for="rbtName"
									class="mand">Rebate Name</label></td>
								<td width="33%"><form:errors path="rebName"
										cssStyle="color:red"></form:errors> <form:input path="rebName"
										type="text" name="rbtName" id="rbtName" maxlength="100" />
								</td>
								<td class="Label" width="17%"><label for="rbtAmount"
									class="mand">Rebate AMT $ </label></td>

								<td width="32%"><form:errors path="rebAmount"
										cssStyle="color:red"></form:errors> <form:input
										path="rebAmount" type="text" name="rbtAmount" id="rbtAmount"
										onkeypress="return isNumberKeyPhone(event)" />
							</tr>
							<tr>
								<td class="Label"><label for="rbtAmount" class="mand">Rebate
										Description</label></td>
								<td><form:errors path="rebDescription" cssStyle="color:red"></form:errors>
									<form:textarea path="rebDescription" name="rbtDesc"
										id="rbtDesc" class="txtAreaSmall"
										onkeyup="checkMaxLength(this,'255');"></form:textarea>
								</td>
								<td class="Label"><label for="rbtAmount" class="mand">Rebate
										Terms &amp; Conditions</label></td>
								<td><form:errors path="rebTermCondtn" cssStyle="color:red"></form:errors>
									<form:textarea path="rebTermCondtn" name="rbtTC" id="rbtTC"
										class="txtAreaSmall" onkeyup="checkMaxLength(this,'1000');"></form:textarea>
								</td>
							</tr>
							<tr>
								<td class="Label"><label for="">Number of Rebates
										Issued</label>
								</td>
								<td align="left"><form:input path="noOfrebatesIsued"
										name="noofrebatesissued" class="textboxDate" readonly="true"
										type="text" id="rebateIsued" /></td>
								<td class="Label"><label for="">Number of Rebates
										Used</label>
								</td>
								<td align="left"><form:input path="noOfRebatesUsed"
										name="rebatsUsed" type="text" class="textboxDate"
										readonly="true" id="rebatsUsed" /></td>
							</tr>
							<tr>
								<td class="Label"><label for="rebStartDate" class="mand">
										Rebate Start Date</label></td>
								<td align="left"><form:errors path="rebStartDate"
										cssStyle="color:red"></form:errors> <form:input
										path="rebStartDate" name="csd" type="text" class="textboxDate"
										id="datepicker1" />(mm/dd/yyyy)
								</td>
								<td class="Label"><label for="rebEndDate" class="mand">Rebate
										End Date</label></td>
								<td align="left"><form:errors path="rebEndDate"
										cssStyle="color:red"></form:errors> <form:input
										path="rebEndDate" name="ced" type="text" class="textboxDate"
										id="datepicker2" />(mm/dd/yyyy)
								</td>
							</tr>
							<form:hidden path="rebateID" name="rebID" id="rebID" />
							<tr>
								<td class="Label"><label for="cst">Rebate Start
										Time </label>
								</td>
								<td><form:select path="rebStartHrs" class="slctSmall">
										<form:options items="${RebateStartHrs}" />
									</form:select> Hrs <form:select path="rebStartMins" class="slctSmall">
										<form:options items="${RebateStartMins}" />
									</form:select> Mins</td>
								<td class="Label"><label for="cet">Rebate End Time</label>
								</td>
								<td><form:select path="rebEndhrs" class="slctSmall">
										<form:options items="${RebateStartHrs}" />
									</form:select> Hrs <form:select path="rebEndMins" class="slctSmall">
										<form:options items="${RebateStartMins}" />
									</form:select> Mins</td>
							</tr>
							<tr>

								<td class="Label"><label for="timeZone">Time Zone</label>
								</td>
								<td colspan="3"><form:select path="rebateTimeZoneId"
										class="selecBx">
										<form:option value="0" label="--Select--">Please Select Time Zone</form:option>
										<c:forEach items="${sessionScope.RebateTimeZoneslst}" var="tz">
											<form:option value="${tz.timeZoneId}"
												label="${tz.timeZoneName}" />
										</c:forEach>

									</form:select>
							</tr>
							<tr>
								<td class="Label"><label for="retailID" class="mand">Select
										Business 
								</td>
								<td><form:errors path="retailID" cssStyle="color:red"></form:errors>
									<form:select path="retailID" class="selecBx" id="retailID"
										name="retailID" onchange="loadRetailerLoc()">
										<form:option value="0" label="--Select--">--Select-</form:option>
										<c:forEach items="${sessionScope.retailerList}" var="s">
											<form:option value="${s.retailID}" label="${s.retailName}" />
										</c:forEach>
									</form:select>
								</td>
								<td class="Label"><label for="retailerLocID" class="mand">Select
										Business Loc 
								</td>
								<td><form:errors path="retailerLocID" cssStyle="color:red"></form:errors>
									<div id="myAjax">
										<form:select path="retailerLocID" id="retailerLocID"
											name="retailerLocID">
											<form:option value="0" label="--Select--">--Select--</form:option>
										</form:select>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</form:form>
			</div>
			<div class="navTabSec">
				<div align="right">
					<!-- <input name="Back" value="Back" type="button" class="btn"
						onclick="javascript:history.back()" title="Back" />  -->
					<input name="Back" value="Back" type="button" class="btn"
						onclick="location.href='/ScanSeeWeb/rebatesMfg.htm'" title="Back" />
					<input name="Preview" value="Preview" type="button" class="btn"
						onclick="previewEditRebate()" /> <input name="Save" value="Save"
						type="button" class="btn" title="Save" onclick="saveEditRebate();" />
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
		loadRetailerLoc();
	
</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="java.util.*"%>
<%@page session="true"%>
<%@ page import="common.pojo.ProductVO"%>
<div id="wrapper">
  
  <div id="content" class="topMrgn separator">
	<div class="detailPnl floatL">
    	<div class="section">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
                  <tr class="mainTitle">
                    <td colspan="2"  class="mainTitle"><c:out value="${sessionScope.productinfo.productName}"/></td>
                    </tr>
                    
                     <tr class="subTitle">  
                
               <c:choose> 
                
                <c:when test="${requestScope.OnlineStoresInfo ne null }">
                    <tr class="subTitle">
                    <td width="50%">Price Online from<span id="onlinePrice"></span></td>
					
                   
                  </tr>
				  
				  
				   <tr class="subTitle">
                    <td width="50%"><span id="onlinePrice">
                    
                           <a href="<c:out value="${requestScope.OnlineStoresInfo.url}"/>"><c:out
									value="${requestScope.OnlineStoresInfo.merchantName}" /> </a></span>
                    
                    
                   <span id="onlinePrice">  <i class="pricetxt"> <c:out value="${requestScope.OnlineStoresInfo.salePrice}"/></i></span></td>
					
                   
                  </tr>
                  </c:when>
                  <c:otherwise>
                  
                    <c:if test="${requestScope.findNearByDetails ne null}">
                    <tr class="subTitle">
                
					
                    <td width="50%">Price Nearby from<span id="nearrbyPrice"></span></td>
                  </tr>
				   <tr class="subTitle">
                   <td width="50%"><span id="nearrbyPrice">
                          
                   <c:out value="${requestScope.findNearByDetails.retailerName}"/> <i class="pricetxt"> $<c:out value="${requestScope.findNearByDetails.productPrice}"/></i></span></td>
                  </tr>
                  
                  </c:if>
                  </c:otherwise>
                  </c:choose><!--   
                    
                 
			  
				  
                  <tr>
                    <td colspan="2"><img src="images/facebookWidget.png" alt="facebook" width="362" height="26" /></td>
                  </tr>
                --></table>
				
        </div>
        <div class="section topMrgn" align="center">
        	<img src="${sessionScope.productinfo.productImagePath}" onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';"/>
        </div>
        <div id="subnav" class="tabdSec">
            <ul>
              <li><a href="#" class="active" id="prodDet"><span>Product Details</span></a></li>
              <li><a href="#" id="reviewDet"><span>Reviews</span></a></li>
             
          </ul>
          </div>

			<!--<div class="section grdSec">
         <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
  <tr>
    <td width="43%"><span class="section topMrgn"><img src="${sessionScope.productinfo.productImagePath}"  width="186" height="102" /></span></td>
    <td width="57%" align="left" valign="bottom"><span class="subTitle"><img src="images/audioIcon.png" alt="audio" width="36" height="27" align="top" /> Listen Audio Recording About Product</span></td>
  </tr>
  <tr>
    <td class="subTitle">Product Overview</td>
    <td align="left" valign="bottom" class="subTitle">&nbsp;</td>
  </tr>
  <tr>
    <td><c:out value="${sessionScope.productinfo.productLongDescription}"/>
     </td>
    <td align="left" valign="top" class="subTitle"><img src="images/videoIcon.png" alt="Video" width="36" height="31" align="middle" /> Watch - See Video About Product</td>
  </tr>
</table>

       </div> -->
       
       
       <div class="section grdSec tabdView">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl prodDet">
          <tr>
            <td width="43%"><span class="section topMrgn"><img src="${sessionScope.productinfo.productImagePath}" onerror="this.src = '/ScanSeeWeb/images/blankImage.gif';"/></span></td>
           
          </tr>
          <tr>
            <td class="subTitle">Product Overview</td>
            <td align="left" valign="bottom" class="subTitle">&nbsp;</td>
          </tr>
          <tr>
            <td><c:out value="${sessionScope.productinfo.productLongDescription}"/></td>
            
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl reviewDet">
         
		  <c:if test="${requestScope.productReviewslist ne null}">
          <tr>
            <td class="subTitle">Product Reviews</td>
           
            
             <tr>
             <td width="50%">
             <span id="onlinePrice">
             <a href="<c:out value="${requestScope.productReviewslist.reviewURL}"/>"><c:out
			                 value="${requestScope.productReviewslist.comments}" /> </a></span>
             
             </td> 
            </tr>
          </c:if>
         
        </table>
      </div>
       
       
       
       
       
       
       
       
       
       
			<div class="section topMrgn" align="center"> 
         <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
  <tr>
    <td width="28%"><span class="subTitle">Item Description</span></td><!--
    <td width="36%"><a href="#"><img src="images/addToShpLstBtn.png" alt="addToShpngLst" width="167" height="39" /></a></td>
    <td width="36%"><a href="#"><img src="images/addToWshLstBtn.png" alt="addToWshLst" width="156" height="39" /></a></td>
  --></tr>
  <tr>
    <td colspan="3"><c:out value="${sessionScope.productinfo.productShortDescription}"/></td>
  </tr>
  <tr>
    <td colspan="3"><span class="subTitle">Features</span></td>
  </tr>
  <tr>
    <td colspan="3"><c:out value="${sessionScope.productinfo.productLongDescription}"/></td>
  </tr>
  <tr>
    <td colspan="3"><span class="subTitle">Warranty</span></td>
  </tr>
  <tr>
    <td colspan="3"><c:out value="${sessionScope.productinfo.warrantyServiceInformation}"/></td>
  </tr>
</table>

      </div>
         <div class="clear"></div>
    </div>
    <div class="miscPnl floatR"> <div align="right">
        <input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Cancel"/>
      </div><img src="/ScanSeeWeb/images/scanseebarcode.png" alt="barcode" /><img src="/ScanSeeWeb/images/RtImg.png" width="331" height="1045" /></div>
  
    <div class="clear"></div>
    <div  class="navTabSec mrgnRt" align="right">
      <div align="center">
        <input class="btn" onclick="javascript:history.back()" value="Back" type="button" name="Cancel"/>
      </div>
    </div>
  </div>
  
</div>


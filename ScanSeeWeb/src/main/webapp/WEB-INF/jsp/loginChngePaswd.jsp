<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>


  <div id="content" class="topMrgn">
   <form:form name="loginform" commandName="loginform">
   <form:hidden path="changePwd" value="Y"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="brdrLsTbl">
  
  <tr>
    <td colspan="2" align="left" valign="top">
	<div id="loginSec">
      <h1 class="relogin">&nbsp;</h1>
      <ul class="relogin">
        <li><form:errors path="*" cssStyle="color:red"></form:errors>
		</li>
        <li> 
        <form:password path="newPassword"  class="floatR textboxLog" id="pswdNew" tabindex="1"/>
          <span class="floatL">
            <label class="mand">New Password</label>
          </span> <span class="clear"></span> <i class="errDisp">Please enter Password</i> </li>
		  
              <li>
              <form:password path="confrmPassword"  class="floatR textboxLog" id="pswdCfrm" tabindex="2"/>
          <span class="floatL">
            <label class="mand">Confirm Password</label>
          </span> <span class="clear"></span> <i class="errDisp">Please re enter Password</i> </li>
		  
		  
        <li><img width="81" align="right" height="25" id="login" onclick="validateUserForm(['#pswdNew','#pswdCfrm'],'li',chkUser)" class="loginBtn" alt="login" src="/ScanSeeWeb/images/reloginBtn.png"  tabindex="3"/></li>
      </ul>
      <div class="loginBtm">&nbsp;</div>
    </div>
	</td>
  </tr>
</table>

</form:form>
  </div>     


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<link
	href="/ScanSeeWeb/styles/jquery-ui.css"
	rel="stylesheet" type="text/css">
<script
	src="/ScanSeeWeb/scripts/jquery.min.js"></script>
<script
	src="/ScanSeeWeb/scripts/jquery-ui.min.js"></script>
<script src="scripts/web.js" type="text/javascript"></script>	
<!--<script type="text/javascript">
	$(document).ready(function() {
		$("#datepicker1").datepicker();
	});

	$(document).ready(function() {
		$("#datepicker2").datepicker();
	});
	$(document).ready(function() {
		radioSel('slctOpt');
		$('input[name="slctOpt"]').click(function() {
			radioSel('slctOpt');
		});
	});
</script>
-->
<style>
.ui-datepicker-trigger {
	margin-left: 3px;
	margin-top: 0.5px;
}
</style>
<script>
	$(document).ready(function() {
		$("#datepicker1").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/calendarIcon.png'
		});
	});
	$(document).ready(function() {
		$("#datepicker2").datepicker({
			showOn : 'both',
			buttonImageOnly : true,
			buttonText : 'Click to show the calendar',
			buttonImage : 'images/calendarIcon.png'
		});
	});
</script>
<script type="text/javascript">
	function radioSel(radioName) {
		$('tr.slctCty').hide();
		$('tr.slctLoc').hide();
		var cls = $('input[name ="' + radioName + '"]:checked').val();
		if (cls == "slctLoc") {
			var productId = document.editdealsform.prodcutID.value;
			if (productId == '') {
				alert("Please select atleast one Product");
				document.getElementById('slctLoc').checked = false;
				return false;
			} else {
			document.getElementById('slctLoc').checked = true;
				loadPdtRetailer();
				$('tr.' + cls).show();
			}
		} else if (cls == "slctCty") {
			loadPopulationCenters();
			$('tr.' + cls).show();
		}

	}

	function loadPdtRetailer() {
		var productId = $('#prodcutID').val();
		$.ajaxSetup({cache:false});
		$.ajax({
			type : "GET",
			url : "fetchpdtretailer.htm",
			data : {
				'productId' : productId
			},

			success : function(response) {
				$('#myAjax2').html(response);
			},
			error : function(e) {

			}
		});
	}
	
	function loadRetailerLocation() {
		var retailerId = $('#retailID').val();
		var productId = $('#prodcutID').val();
		$.ajaxSetup({cache:false});
		$.ajax({
			type : "GET",
			url : "fetchpdtretailerlocation.htm",
			data : {
				'retailerId' : retailerId,
				'productId' : productId
			},
			success : function(response) {
				$('#myAjax3').html(response);
			},
			error : function(e) {
				//	alert("Eror occurred while Showing Retail Loc Ids");
			}
		});
	}
	function loadPopulationCenters() {
		$.ajaxSetup({cache:false});
		$.ajax({
			type : "GET",
			url : "/ScanSeeWeb/retailer/fetchhotdealpopcenters.htm",
			data : {},

			success : function(response) {
				$('#myAjax1').html(response);
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}

	function onCategoryLoad() {
		var vCategoryID = document.editdealsform.bCategoryHidden.value;
		var sel = document.getElementById("bCategory");
		for ( var i = 0; i < sel.options.length; i++) {
			if (sel.options[i].value == vCategoryID) {
				sel.options[i].selected = true;
				return;
			}
		}
	}
	function checkAssociatedProd(){
		 var $prdID = $('#prodcutID').val();
		if($prdID!=""){
			$.ajax({
					type : "GET",
					url : "/ScanSeeWeb/checkAssociatedProd.htm",
					data : {
						'productId'  : $prdID
					},
					success : function(response) {
					openIframePopup('ifrmPopup','ifrm','prodHotDealList.htm',420,600,'View Product/UPC')
						},
					error : function(e) {
						alert('Error:' + 'Error Occured');
					}
				});

		
	}else{
	openIframePopup('ifrmPopup','ifrm','prodHotDealList.htm',420,600,'View Product/UPC')
	}
	}
</script>


<div id="wrapper">


	<div class="clear"></div>
	<div id="content" class="topMrgn">
		<div class="section topMrgn">
			<div class="grdSec brdrTop">
				<form:form name="editdealsform" commandName="editdealsform" acceptCharset="ISO-8859-1">
					<form:hidden path="productId" name="prodcutID" id="prodcutID" />
					<form:hidden path="existingProductIds" name="existingProductIds"
						id="existingProductIds" />
					<form:hidden path="bCategoryHidden"/>


					<form:hidden path="dealForCityLoc" name="dealForCityLoc"
						id="dealForCityLoc" />

					<c:if test="${message ne null }">
						<div id="message">
							<center>
								<h2>
									<c:out value="${message}" />
								</h2>
							</center>
						</div>
						<script>
							var PAGE_MESSAGE = true;
						</script>
					</c:if>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<form:hidden path="cityHidden" />
						<tr>
							<td colspan="4" class="header">Edit Deal</td>
						</tr>
						<tr>
							<td width="18%" class="Label"><label for="dealName"
								class="mand">Hot Deal Name</label></td>
							<td colspan="3"><form:errors cssStyle="color:red"
									path="hotDealName"></form:errors> <form:input
									path="hotDealName" type="text" name="imprsns" id="hotdealname" />
							</td>
						</tr>
						<tr>
							<td class="Label"><label for="regPrice" class="mand">Regular
									Price $</label></td>
							<td width="33%"><form:errors cssStyle="color:red"
									path="price"></form:errors> <form:input path="price"
									type="text" name="regular price" id="regPrice"
									onkeypress="return isNumberKeyPhone(event)" /></td>

							<td width="17%" class="Label"><label for="salePrice"
								class="mand">Sale Price $</label></td>
							<td width="32%"><form:errors cssStyle="color:red"
									path="salePrice"></form:errors> <form:input path="salePrice"
									type="text" name="textfield" id="salePrice"
									onkeypress="return isNumberKeyPhone(event)" /></td>
						</tr>
						<tr>
							<td class="Label"><label for="htdealDesc" class="mand">Hot
									Deal Description</label></td>
							<td colspan="3"><form:errors cssStyle="color:red"
									path="hotDealShortDescription"></form:errors> <form:textarea
									path="hotDealShortDescription" name="textarea2" id="htdealDesc"
									cols="45" rows="5" class="txtAreaSmall"></form:textarea></td>
						</tr>
						<tr>
							<td class="Label">Hot Deal Long Description</td>
							<td colspan="3"><label> <form:textarea
										path="hotDealLongDescription" name="textarea" cols="45"
										rows="5" class="txtAreaLarge" id="htdealLngDesc"></form:textarea>
							</label></td>
						</tr>
						<tr>
							<td class="Label"><label for="htdealTems" class="mand">Hot
									Deal Terms</label></td>

							<td colspan="3"><form:errors cssStyle="color:red"
									path="hotDealTermsConditions" /> <form:textarea
									path="hotDealTermsConditions" name="textarea3" cols="45"
									rows="5" class="txtAreaLarge" id="htdealTerms"></form:textarea>
							</td>
						</tr>
						<tr>
							<td class="Label">URL</td>
							<td colspan="3"><form:input path="url" type="text"
									name="age2" id="url" /></td>
						</tr>
						<tr>
							<td class="Label">Product</td>
							<td><form:input path="scanCode" type="text"
									readonly="true" name="textfield" id="couponName" /><a
								href="#"><img src="/ScanSeeWeb/images/searchIcon.png"
									alt="Search" width="20" height="24"
									onclick="checkAssociatedProd();"
									title="Click here to View Product List" /> </a>
							</td>
							<td class="Label"><label for="bcategory" class="mand">Business Category</label></td>
							<td align="left">
							<form:errors cssStyle="color:red" path="bCategory"></form:errors> 
							<form:select path="bCategory" id="bCategory" class="selecBx" >
								<form:option value="0" label="--Select--">--Select-</form:option>
									<c:forEach items="${sessionScope.categoryList}" var="c">
											<form:option value="${c.categoryID}" label="${c.parentSubCategory}" />
									</c:forEach>
					    	</form:select>
						</td>
						</tr>
						<tr>
							<td class="Label"><label for="dealStDate" class="mand">Deal
									Start Date</label></td>
							<td align="left"><form:errors cssStyle="color:red"
									path="dealStartDate"></form:errors> <form:input
									path="dealStartDate" name="startDT" id="datepicker1"
									class="textboxDate" />(mm/dd/yyyy)</td>
							<td class="Label"><label for="dealEndDate" class="mand">Deal
									End Date</label></td>
							<td><form:errors cssStyle="color:red" path="dealEndDate"></form:errors>
								<form:input path="dealEndDate" name="endDT" id="datepicker2"
									class="textboxDate" />(mm/dd/yyyy)</td>
						</tr>
						<tr>
							<td class="Label"><label for="cst">Deal Start Time </label>
							</td>
							<td><form:select path="dealStartHrs" class="slctSmall">
									<form:options items="${DealStartHrs}" />
								</form:select> Hrs <form:select path="dealStartMins" class="slctSmall">
									<form:options items="${DealStartMins}" />
								</form:select> Mins</td>
							<td class="Label"><label for="cet">Deal End Time</label>
							</td>
							<td><form:select path="dealEndhrs" class="slctSmall">
									<form:options items="${DealStartHrs}" />
								</form:select> Hrs <form:select path="dealEndMins" class="slctSmall">
									<form:options items="${DealStartMins}" />
								</form:select> Mins</td>

						</tr>
						<tr>

							<td class="Label"><label for="timeZone">Time Zone</label>
							</td>
							<td colspan="3"><form:select path="dealTimeZoneId"
									class="selecBx">
									<form:option value="0" label="">Please Select Time Zone</form:option>
									<c:forEach items="${sessionScope.timeZoneslst}" var="tz">
										<form:option value="${tz.timeZoneId}"
											label="${tz.timeZoneName}" />
									</c:forEach>
								</form:select>
							</td>

						</tr>

						<tr>
							<td class="Label">&nbsp;</td>
							<td><input name="slctOpt" type="radio" value="slctCty"
								id="slctCty" /> Deal for Specific City?</td>
							<td align="center" class="Label"><strong>OR</strong></td>
							<td><input type="radio" name="slctOpt" value="slctLoc"
								id="slctLoc" /> Deal for Specific Location?</td>
						</tr>
						<tr class="slctCty">
							<td class="Label">&nbsp;</td>
							<td colspan="3" class="Label" align="left"><form:errors cssStyle="color:red" path="city"></form:errors>
								<div id="myAjax1" class="floatL">
									<form:select path="city" id="City" class="textboxBig">
										<form:option value="0" label="">Please Select Population Center</form:option>
									</form:select>
								</div><span class="floatL"><label class="mand" for="dealStDate">&nbsp;</label></span>
								</td>
						</tr>
						<tr class="slctLoc">
							<td class="Label">&nbsp;</td>
							<td><form:errors cssStyle="color:red" path="retailID"></form:errors>
								<div id="myAjax2" class="floatL">
									<form:select path="retailID" class="selecBx" id="retailID">
										<form:option value="0">--Select Business--</form:option>
									</form:select>
								</div><span class="floatL"><label class="mand" for="dealStDate">&nbsp;</label></span>
							</td>
							<td colspan="2"><form:errors cssStyle="color:red" path="retailerLocID"></form:errors>
							<div id="myAjax3" class="floatL">
									<form:select path="retailerLocID" id="retailerLocID">
										<form:option value="0">--Select--</form:option>
									</form:select>
								</div><span class="floatL"><label class="mand" for="dealStDate">&nbsp;</label></span>
							</td>
						</tr>
						<form:hidden path="hotDealID" />
					</table>
					<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
						<div class="headerIframe">
							<img src="images/popupClose.png" class="closeIframe" alt="close"
								onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
								title="Click here to Close" align="middle" /> <span
								id="popupHeader"></span>
						</div>

						<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
							height="100%" allowtransparency="yes" width="100%"
							style="background-color: White"> </iframe>
					</div>
				</form:form>
			</div>
			<div class="navTabSec">
				<div align="right">
					<!-- <input name="Back" value="Back" type="button" class="btn"
						onclick="javascript:history.back()" title="Back" />  -->
						<input name="Back" value="Back" type="button" class="btn"
						onclick="location.href='/ScanSeeWeb/hotDeal.htm'" title="Back" />
						<input
						name="Preview" value="Preview" type="button" class="btn"
						onclick="previewEditSupplPopUp()" title="Preview" /> <input
						name="Save" value="Save" type="button" class="btn"
						onclick="hotDealEditSupplier();" title="Save" />
				</div>

			</div>
		</div>
	</div>

</div>
<script type="text/javascript">
	var CategoryID = document.editdealsform.bCategoryHidden.value;
	onCategoryID();
	function onCategoryID() {
		onCategoryLoad();
	}
</script>

<script type="text/javascript">
	loadRetLoc();
	function loadRetLoc() {
		loadRetailerLocation();
	}
</script>

<script type="text/javascript">
	var dealForCityLoc = document.editdealsform.dealForCityLoc.value;
	if (dealForCityLoc == 'City') {
		document.getElementById('slctCty').checked = 'checked';
		loadPopulationCenters();
	} else if (dealForCityLoc == 'Location') {
		document.getElementById('slctLoc').checked = 'checked';
		loadPdtRetailer();
	}
</script>


<script type="text/javascript">
	var hashVal = location.hash;
	hashVal = hashVal.substring(1, hashVal.length)
	if (hashVal) {
		var items = hashVal.split('&&');
		$('#snNo').val(items[0]);
		$('#dlNm').val(items[1]);
		$('#sp').val(items[2]);
		$('#dsd').val(items[3]);
		$('#ded').val(items[4]);
	}
</script>

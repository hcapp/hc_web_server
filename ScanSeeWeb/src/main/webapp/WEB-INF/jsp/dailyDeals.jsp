<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>


<div id="content" class="shdwBg">
	<%@include file="leftnavigation.jsp"%>
	<form:form name="displayDealsForm" commandName="displayDealsForm"
		acceptCharset="ISO-8859-1">
		<form:hidden path="hotDealID" />
		<div class="grdSec contDsply floatR">
			<div id="secInfo">Hot Deals</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="grdTbl">
				<tr>
					<td colspan="3" class="header">Search Daily Deals</td>
				</tr>
				<tr>
					<td class="Label" width="18%">Hot Deal Name</td>
					<td width="59%"><form:input path="hotDealName"
							name="hotDealName" id="hotDealName" /> <a href="#"><img
							src="images/searchIcon.png" alt="Search" title="Search"
							onclick="getDealsByName();" width="25" height="24" />
					</a>
					</td>
					<td width="23%"><input type="button" class="btn"
						name="addDeals" value="Add"
						onclick="window.location.href='dailyDealsmfg.htm'" title="Add" />
					</td>
				</tr>
			</table>
			<div class="searchGrd">
				<h1 class="searchHeaderExpand">
					<a href="#" class="floatR">&nbsp;</a>Current Deals
				</h1>
				<div class="grdCont">
					<!--
				<c:if test="${message ne null }">
							<div id="message">
								<center>
									<h2>
										<c:out value="${message}" />
									</h2>
								</center>
							</div>
							<script>var PAGE_MESSAGE = true;</script>
						</c:if>
					-->
					<div align="center">
						<label><form:errors
								cssStyle="${requestScope.manageDealsFont}" /> </label>
					</div>
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="stripeMe" id="dealLst">
						<tr class="header">

							<td width="25%">Deal Name</td>
							<td width="13%">Sale Price($)</td>
							<td width="18%">Deal Start Date</td>
							<td width="18%">Deal End Date</td>
							<td width="23%">Actions</td>
						</tr>

						<c:forEach items="${sessionScope.listOfDeals.dealList}"
							var="hotDeals">
							<tr>
								<!--<td width="21%"><c:out value='${hotDeals.hotDealName}' />
								</td>
								-->
								<td width="25%"><a href="#"
									onclick="hotDealEdit(<c:out value='${hotDeals.hotDealID}' />);"
									title="Click Here to Edit"><c:out
											value='${hotDeals.hotDealName}' /> </a></td>
								<td width="13%"><c:out value='${hotDeals.salePrice}' />
								</td>
								<td width="18%"><c:out value='${hotDeals.dealStartDate}' />
								</td>
								<td width="18%"><c:out value='${hotDeals.dealEndDate}' />
								</td>
								<td width="23%"><input name="Re-Run4" value="Re-Run"
									type="button" name="rerunbutton" id="rerunbutton" class="btn"
									onclick="hotDealReRun(<c:out value='${hotDeals.hotDealID}' />);"
									title="Re-Run" /> <input name="edit" value="Edit" type="button"
									name="editbutton" id="editbutton" class="btn"
									onclick="hotDealEdit(<c:out value='${hotDeals.hotDealID}' />);"
									title="Edit" /></td>
							</tr>
						</c:forEach>
					</table>
					<div class="pagination">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							class="noBrdr" id="perpage">
							<tr>
								<page:pageTag
									currentPage="${sessionScope.pagination.currentPage}"
									nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
									pageRange="${sessionScope.pagination.pageRange}"
									url="${sessionScope.pagination.url}" enablePerPage="false" />
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
			<div class="headerIframe">
				<img src="images/popupClose.png" class="closeIframe" alt="close"
					onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
					title="Click here to Close" align="middle" /> <span
					id="popupHeader"></span>
			</div>
			<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
				height="100%" allowtransparency="yes" width="100%"
				style="background-color: White"> </iframe>
		</div>

	</form:form>


</div>

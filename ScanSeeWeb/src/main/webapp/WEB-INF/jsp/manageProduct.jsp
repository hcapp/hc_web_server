<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page session="true"%>
<%@ page import="common.pojo.ManageProducts"%>
<%@taglib prefix="page" uri="/WEB-INF/pagination.tld"%>
<script src="scripts/web.js" type="text/javascript"></script>

<script>
 saveManageProductsPage = 'true';
 submitFlag = 'dashboard';
</script>
<script type="text/javascript">
$(document).ready(function() {
if ($('#highLight tr').length == "1") { $('input[name$="sub"]').css('visibility','hidden'); } else { $('input[name$="sub"]').css('visibility','visible');}
});
</script>
<form:form name="myform" commandName="manageform"
	acceptCharset="ISO-8859-1">
	<form:hidden path="prodJson" />
	<form:hidden path="pageNumber" />
	<form:hidden path="pageFlag" />
	<div id="wrapper">
		<div id="content" class="shdwBg">
			<%@include file="leftnavigation.jsp"%>
			<div class="grdSec contDsply floatR">
				<div id="secInfo">Manage Products</div>
				<div id="subnav">
					<ul>
						<li><a href="upload.htm" title="Add Product"><span>Add
									Product</span> </a></li>
						<li><a href="batchupload.htm" title="Batch Upload"><span>Batch
									Upload</span> </a></li>
						<li><a href="manageProduct.htm" class="active"
							title="Manage Products"><span>Manage Products</span> </a></li>
					</ul>
				</div>
				<div class="clear"></div>
				<div class="grdSec ">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="grdTbl">
						<tr>
							<td colspan="2" class="header">Search Products</td>
						</tr>
						<tr>
							<td class="Label" width="23%">Product name or UPC code</td>
							<td><form:errors cssStyle="color:red" path="productName"
									cssClass="error"></form:errors> <form:input path="productName"
									type="text" name="productName" id="textfield" /> <a href="#">
									<img src="images/searchIcon.png" alt="Search" title="Search"
									style="cursor: pointer;" width="25" height="24"
									onclick="searchResult();" />
							</a></td>
						</tr>
					</table>
					<div align="center" style="font-style: 90">
						<label><form:errors
								cssStyle="${requestScope.manageProductFont}" /> </label>
					</div>
					<div id="batchGrid" class="brdrTop">
						<input type="file" name="mediaFiles" id="imgBrwse"
							style="display: none" />

						<table class="grdTbl" border="0" cellspacing="0" cellpadding="0"
							width="100%" id="highLight">
							<tbody>
								<tr>
									<td class="Label" width="18%">Preview</td>
									<td class="Label" width="18%">Create QR</td>
									<td class="Label" width="18%"><label class="mand">Product
											UPC</label></td>
									<td class="Label" width="18%"><label class="mand">Product
											Name</label></td>
									<td class="Label" width="18%">Model Number</td>
									<td class="Label" width="18%"><label class="mand">Suggested
											Retail Price</label></td>
									<td class="Label" width="18%"><label class="mand">Long
											Description</label></td>
									<td class="Label" width="18%">Warranty / Service
										Information</td>
									<td class="Label" width="18%">Short Description</td>
									<td class="Label" width="18%">Category of Product</td>
									<td class="Label" width="18%">Manage Attributes</td>
									<td class="Label" width="18%">Manage Media</td>
									<td class="Label" width="18%">Preview</td>
								</tr>
								<c:forEach items="${sessionScope.seacrhList.manageProductList}"
									var="item">

									<tr id="${item.productID}">

										<td align="center"><a href="#"
											onclick="previewManageProductPopUp(${item.productID},'${item.prodNameEscape}')"><img
												src="images/searchIcon.png" width="15" height="15"
												title="Preview" /> </a></td>
										<td align="center"><a href="#"
											onclick="openQRCodePopUp(${item.productID},'${item.prodNameEscape}')"><img
												src="images/QR-icon.png" alt="Qrcode" width="16" height="16" />
										</a></td>
										<td><input type="hidden" id="textfield" size="10"
											name="productID" value="${item.productID}" /> <input
											type="hidden" id="textfield" size="10" name="productNm"
											id="productNm" value="${item.productName}" /> <input
											type="text" id="textfield" size="10" name="productUpc"
											maxlength="20" value="${item.scanCode}" /></td>
										<td><input type="text" id="textfield" size="10"
											name="prodName" maxlength="500" value="${item.productName}" />
										</td>
										<td><input type="text" id="textfield" size="10"
											name="modelNumber" maxlength="50" value="${item.modelNumber}" />
										</td>
										<td><input type="text" id="textfield" size="10"
											name="suggestedRetailPrice"
											onkeypress="return isNumberKeyPhone(event)"
											value="${item.suggestedRetailPrice}" /></td>
										<td><input type="text" id="textfield" size="10"
											name="longDesc" maxlength="1000"
											value="${item.longDescription}" /></td>
										<td><input type="text" id="textfield" size="10"
											name="warranty" maxlength="255" value="${item.warranty}" />
										</td>
										<td><input type="text" id="textfield" size="10"
											name="shortDesc" maxlength="255"
											value="${item.shortDescription}" /></td>
										<td><select class="selecBx" id="retailID" name="category">
												<option value="0" label="--Select--">--Select-</option>
												<c:forEach items="${sessionScope.categoryList}" var="s">
													<option value="${s.categoryID}"
														title="${s.parentSubCategory}"
														${s.categoryID==item.category ? 'selected' : ''}>${s.parentSubCategory}</option>
												</c:forEach>
										</select></td>
										<td><input type="button" class="btn" value="Add/Edit"
											onclick="openManageAttrPopUp(${item.productID})"
											title="Click here to Manage Attributes" /></td>
										<td align="center"><input type="button" class="btn"
											value="Add/Edit"
											onclick="openManageAdioVideoPopUp(${item.productID})"
											title="Click Here To Get Product Media" /></td>
										<td align="center"><a href="#"
											onclick="previewManageProductPopUp(${item.productID},'${item.prodNameEscape}')"><img
												src="images/searchIcon.png" width="15" height="15"
												title="Preview" /> </a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div class="pagination">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						class="noBrdr" id="perpage">
						<tr>
							<page:pageTag
								currentPage="${sessionScope.pagination.currentPage}"
								nextPage="4" totalSize="${sessionScope.pagination.totalSize}"
								pageRange="${sessionScope.pagination.pageRange}"
								url="${sessionScope.pagination.url}" enablePerPage="false" />
						</tr>
					</table>
				</div>
				<div align="right">
					<!--  <input class="btn"  value="Clear" type="button" name="Cancel3"/> -->
					<input class="btn" value="Submit"
						onclick="saveMangageProducts('dashboard');" type="button"
						name="sub" title="Submit" />
				</div>
				<div class="ifrmPopupPannel" id="ifrmPopup" style="display: none;">
					<div class="headerIframe">
						<img src="images/popupClose.png" class="closeIframe" alt="close"
							onclick="javascript:closeIframePopup('ifrmPopup','ifrm')"
							title="Click here to Close" align="middle" /> <span
							id="popupHeader"></span>
					</div>
					<iframe frameborder="0" scrolling="auto" id="ifrm" src=""
						height="100%" allowtransparency="yes" width="100%"
						style="background-color: #ffffff"> </iframe>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
</form:form>
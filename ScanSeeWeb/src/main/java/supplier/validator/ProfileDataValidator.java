package supplier.validator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.pojo.SupplierRegistrationInfo;
import common.constatns.ApplicationConstants;

/**
 * validation used in supplier registration screen.
 * 
 * @author Created by SPAN.
 */
public class ProfileDataValidator implements Validator {

	/**
	 * Getting the SupplierRegistrationInfo Instance.
	 * 
	 * @param arg0 as request parameter.
	 * @return SupplierRegistrationInfo instance.
	 */
	public final boolean supports(Class<?> arg0) {
		return SupplierRegistrationInfo.class.isAssignableFrom(arg0);
	}

	/**
	 * This method validate supplier registration screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.DUPLICATE_USER)) {
			errors.reject("userName.exist");
		} 
		if (status.equals(ApplicationConstants.DUPLICATE_SUPPLIERNAME)) {
			errors.reject("duplicate.suppliername");
		} 
		if (status.equals(ApplicationConstants.ERROR_SAVE)) {
			errors.reject("error.save");
		}
		if (status.equals(ApplicationConstants.ERROR_FETCH)) {
			errors.reject("error.fetch");
		}
		/*if (status.equals(ApplicationConstants.EMAIL)) {
			errors.reject("email.match");
		} */
		if (status.equals(ApplicationConstants.INVALIDCORPPHONE)) {
			//errors.reject("invalid.email");
			errors.rejectValue("corpPhoneNo", "invalid.Phonenumber");
		}
		if (status.equals(ApplicationConstants.INVALIDCONTPHONE)) {
			//errors.reject("invalid.email");
			errors.rejectValue("contactPhoneNo", "invalid.Phonenumber");
		}
		if (status.equals(ApplicationConstants.INVALIDEMAIL)) {
			//errors.reject("invalid.email");
			errors.rejectValue("contactEmail", "invalid.email");
		}
		if (status.equals(ApplicationConstants.STATE)) {
			errors.rejectValue("state", "select.State");
		} 
		if (status.equals(ApplicationConstants.CITY)) {
			errors.rejectValue("city", "city.enter");
		} 
		if (status.equals(ApplicationConstants.TERM)) {
			errors.rejectValue("acceptTerm", "select.termcondition");
		} 
		if (status.equals(ApplicationConstants.BUSINESSCATEGORY)) {
			errors.rejectValue("bCategory", "select.category");
		}
		/*if (status.equals(ApplicationConstants.INVALID_CAPTCHA))
		{
			errors.rejectValue("captch", "captcha.invalid");
		}*/
/*
		ValidationUtils.rejectIfEmpty(errors, "supplierName",
				"supplierName.required");
		ValidationUtils.rejectIfEmpty(errors, "corpAdderess",
				"corpAdderess.rquired");
		ValidationUtils.rejectIfEmpty(errors, "Address2", "Address2.rquired");
		ValidationUtils.rejectIfEmpty(errors, "state", "state.rquired");
		ValidationUtils.rejectIfEmpty(errors, "postalCode",
				"postalCode.rquired");
		ValidationUtils.rejectIfEmpty(errors, "corpPhoneNo",
				"corpPhoneNo.rquired");
		ValidationUtils.rejectIfEmpty(errors, "legalAuthorityFName",
				"legalAuthorityFName.rquired");
		ValidationUtils.rejectIfEmpty(errors, "legalAuthorityLName",
				"legalAuthorityLName.rquired");
		ValidationUtils.rejectIfEmpty(errors, "contactFName",
				"contactFName.rquired");
		ValidationUtils.rejectIfEmpty(errors, "contactLName",
				"contactLName.rquired");
		ValidationUtils.rejectIfEmpty(errors, "contactPhoneNo",
				"contactPhoneNo.rquired");
		ValidationUtils.rejectIfEmpty(errors, "contactEmail",
				"contactEmail.rquired");
		ValidationUtils.rejectIfEmpty(errors, "password", "password.rquired");
		// ValidationUtils.rejectIfEmpty(errors, "acptTrms",
		// "acptTrms.rquired");

		if (status.equals("Y")) {
			LOG.info("Inside Validator");
			errors.reject("createProfile.invalid");
		}*/
	}

	/**
	 * This method validate supplier registration screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors..
	 */
	public final void validate(Object arg0, Errors errors) {
		//SupplierRegistrationInfo supplierRegistrationInfo = (SupplierRegistrationInfo) arg0;
		/*if (null == supplierRegistrationInfo.getCaptch() || supplierRegistrationInfo.getCaptch().equals(""))
		{

			ValidationUtils.rejectIfEmpty(errors, "captch", "captcha.invalid");
		}*/
		ValidationUtils.rejectIfEmpty(errors, "supplierName", "supplierName");
		ValidationUtils.rejectIfEmpty(errors, "corpAdderess", "corpAdderess");
		ValidationUtils.rejectIfEmpty(errors, "postalCode", "postalCode");
		ValidationUtils.rejectIfEmpty(errors, "corpPhoneNo", "corpPhoneNo");
		ValidationUtils.rejectIfEmpty(errors, "legalAuthorityFName", "legalAuthorityFName");
		ValidationUtils.rejectIfEmpty(errors, "legalAuthorityLName", "legalAuthorityLName");
		ValidationUtils.rejectIfEmpty(errors, "contactFName", "contactFName");
		ValidationUtils.rejectIfEmpty(errors, "contactLName", "contactLName");
		ValidationUtils.rejectIfEmpty(errors, "contactPhoneNo", "contactPhoneNo");
		ValidationUtils.rejectIfEmpty(errors, "contactEmail", "contactEmail");
		ValidationUtils.rejectIfEmpty(errors, "acceptTerm", "acceptTerm");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "userName.required");
		/*ValidationUtils.rejectIfEmpty(errors, "retypeEmail", "retypeEmail");
		ValidationUtils.rejectIfEmpty(errors, "bCategory", "bCategory");
		*/
	}
	
	/**
	 * This method validate supplier edit registration screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validateEdit(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "companyName", "supplierName");
		ValidationUtils.rejectIfEmpty(errors, "corporateAddress", "corpAdderess");	
		ValidationUtils.rejectIfEmpty(errors, "state", "state");
		ValidationUtils.rejectIfEmpty(errors, "postalCode", "postalCode");
		ValidationUtils.rejectIfEmpty(errors, "phone", "corpPhoneNo");
		
		ValidationUtils.rejectIfEmpty(errors, "contactFName", "contactFName");
		ValidationUtils.rejectIfEmpty(errors, "contactLName", "contactLName");
		ValidationUtils.rejectIfEmpty(errors, "contactPhone", "contactPhoneNo");
		ValidationUtils.rejectIfEmpty(errors, "email", "contactEmail");
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bCategory", "bCategory");
		ValidationUtils.rejectIfEmpty(errors, "firstName", "firstName.required");
		ValidationUtils.rejectIfEmpty(errors, "lastName", "lastName.required");*/
	}
	
	
	/**
	 * This method validate supplier edit registration screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validateEdit(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.EMAIL)) {
			errors.reject("email.match");
		} 
		if (status.equals(ApplicationConstants.INVALIDCORPPHONE)) {
			errors.rejectValue("phone", "invalid.Phonenumber");
		}
		if (status.equals(ApplicationConstants.INVALIDCONTPHONE)) {
			errors.rejectValue("contactPhone", "invalid.Phonenumber");
		}
		if (status.equals(ApplicationConstants.INVALIDEMAIL)) {
			errors.rejectValue("email", "invalid.email");
		}
		if (status.equals(ApplicationConstants.STATE)) {
			errors.rejectValue("state", "select.State");
		} 
		if (status.equals(ApplicationConstants.CITY)) {
			errors.rejectValue("city", "city.enter");
		} 
		if (status.equals(ApplicationConstants.BUSINESSCATEGORY)) {
			errors.rejectValue("bCategory", "select.category");
		}
	}
	
	/**
	 * @param arg0
	 * @param errors
	 */
	/*
	public void validateEdit(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "companyName", "supplierName");
		ValidationUtils.rejectIfEmpty(errors, "corporateAddress", "corpAdderess");
		
		ValidationUtils.rejectIfEmpty(errors, "state", "state");
		ValidationUtils.rejectIfEmpty(errors, "postalCode", "postalCode");
		ValidationUtils.rejectIfEmpty(errors, "phone", "corpPhoneNo");
		ValidationUtils.rejectIfEmpty(errors, "legalAuthorityFName", "legalAuthorityFName");
		ValidationUtils.rejectIfEmpty(errors, "legalAuthorityLName", "legalAuthorityLName");
		ValidationUtils.rejectIfEmpty(errors, "contactFName", "contactFName");
		ValidationUtils.rejectIfEmpty(errors, "contactLName", "contactLName");
		ValidationUtils.rejectIfEmpty(errors, "contactPhoneNo", "contactPhoneNo");
		ValidationUtils.rejectIfEmpty(errors, "email", "contactEmail");
	}*/
}

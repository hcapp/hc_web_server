package supplier.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.pojo.ProductInfoVO;

/**
 * validation used in supplier manage product screen.
 * 
 * @author Created by SPAN.
 */
public class ManageProductValidator implements Validator {
	
	/**
	 * Variable dealEndDate declared as String.
	 */
	private final String productName = "productName";
	
	/**
	 * Getting the ProductInfoVO Instance.
	 * 
	 * @param arg0 as request parameter.
	 * @return ProductInfoVO instance.
	 */
	public final boolean supports(Class<?> arg0) {
		return ProductInfoVO.class.isAssignableFrom(arg0);
	}

	/*public void validate(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.PRODUCTNAMEEMPTY)) {
			errors.reject("emptyproduct");
		} 
		ValidationUtils.rejectIfEmpty(errors, "supplierName",
				"supplierName.required");
		ValidationUtils.rejectIfEmpty(errors, "corpAdderess",
				"corpAdderess.rquired");
		ValidationUtils.rejectIfEmpty(errors, "Address2", "Address2.rquired");
		ValidationUtils.rejectIfEmpty(errors, "state", "state.rquired");
		ValidationUtils.rejectIfEmpty(errors, "postalCode",
				"postalCode.rquired");
		ValidationUtils.rejectIfEmpty(errors, "corpPhoneNo",
				"corpPhoneNo.rquired");
		ValidationUtils.rejectIfEmpty(errors, "legalAuthorityFName",
				"legalAuthorityFName.rquired");
		ValidationUtils.rejectIfEmpty(errors, "legalAuthorityLName",
				"legalAuthorityLName.rquired");
		ValidationUtils.rejectIfEmpty(errors, "contactFName",
				"contactFName.rquired");
		ValidationUtils.rejectIfEmpty(errors, "contactLName",
				"contactLName.rquired");
		ValidationUtils.rejectIfEmpty(errors, "contactPhoneNo",
				"contactPhoneNo.rquired");
		ValidationUtils.rejectIfEmpty(errors, "contactEmail",
				"contactEmail.rquired");
		ValidationUtils.rejectIfEmpty(errors, "password", "password.rquired");
		// ValidationUtils.rejectIfEmpty(errors, "acptTrms",
		// "acptTrms.rquired");

		if (status.equals("Y")) {
			LOG.info("Inside Validator");
			errors.reject("createProfile.invalid");
		}
	}*/

	/**
	 * This method validate supplier manage product screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors..
	 */
	public final void validate(Object arg0, Errors errors) {
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "imageFile", "imageFile");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "audioFile", "audioFile");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "videoFile", "videoFile");*/
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productUPC", "productUPC");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, productName, "emptyproduct");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "modelNumber", "modelNumber");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "suggestedRetailPrice", "suggestedRetailPrice");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "longDescription", "longDescription");
		/*ProductInfoVO productInfoVO = (ProductInfoVO) arg0;
		FieldError fieldPriceErr = errors.getFieldError("suggestedRetailPrice");
		if(fieldPriceErr == null){
			if(!Utility.isNumber(productInfoVO.getSuggestedRetailPrice())){
				errors.rejectValue("suggestedRetailPrice","suggestedRetailPrice.invalid");
			}
		}*/
		/*if(productInfoVO.getLongDescription() != null && !"".equals(productInfoVO.getLongDescription())){
			String longDesc = productInfoVO.getLongDescription();
			if(longDesc.length()>1000){
				errors.rejectValue("longDescription","longDescription.invalid");
			}
		}*/
	}
	
	/**
	 * This method validate supplier manage product screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors..
	 */
	public final void validateManageProduct(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, productName, "productName.required");
	}
	
	/**
	 * This method validate supplier product upload screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Object.
	 */
	public final void validateProductUpload(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, productName, "productName.required");
	}
}

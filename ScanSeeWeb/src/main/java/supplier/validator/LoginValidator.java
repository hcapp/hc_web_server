package supplier.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.LoginVO;

/**
 * validation used in user login screen.
 * 
 * @author Created by SPAN.
 */
public class LoginValidator implements Validator {
	/**
	 * Getting the LoginVO Instance.
	 * 
	 * @param arg0 as request parameter.
	 * @return LoginVO instance.
	 */
	public final boolean supports(Class<?> arg0) {
		return LoginVO.class.isAssignableFrom(arg0);
	}
	
	/**
	 * This method validate user login screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.PASSWORD)) {
			errors.reject("password.match");
		} 
		if (status.equals(ApplicationConstants.PASSWORDLength)) {
			errors.rejectValue("newPassword", "password.length");
		} else if (status.equals(ApplicationConstants.PASSWORDMATCH)) {
			errors.rejectValue("newPassword", "password.digit");
		}
	}
	
	/**
	 * This method validate user login screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors..
	 */
	public final void validate(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "userName", "userName.required");
		ValidationUtils.rejectIfEmpty(errors, "password", "password.rquired");
	}
}

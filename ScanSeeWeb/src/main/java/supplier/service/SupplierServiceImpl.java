package supplier.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import supplier.controller.LoginController;
import supplier.dao.SupplierDAO;

import common.constatns.ApplicationConstants;
import common.email.EmailComponent;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AccountType;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.ContactType;
import common.pojo.DropDown;
import common.pojo.HotDealInfo;
import common.pojo.ManageProducts;
import common.pojo.PlanInfo;
import common.pojo.Product;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfoVO;
import common.pojo.QRCodeResponse;
import common.pojo.QRDetails;
import common.pojo.Rebates;
import common.pojo.ReportConfigInfo;
import common.pojo.Retailer;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.SearchZipCode;
import common.pojo.State;
import common.pojo.SupplierProfile;
import common.pojo.SupplierQRCodeData;
import common.pojo.SupplierQRCodeResponse;
import common.pojo.SupplierRegistrationInfo;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.QRCodeGenerator;
import common.util.ScanSeeProperties;
import common.util.Utility;

/**
 * SupplierServiceImpl implements SupplierService methods.
 * 
 * @author Created by SPAN.
 */
public class SupplierServiceImpl implements SupplierService
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();

	/**
	 * Variable supplierDAO declared as instance of SupplierDAO.
	 */
	private SupplierDAO supplierDAO;

	/**
	 * To set setSupplierDAO.
	 * 
	 * @param supplierDAO
	 *            to set.
	 */
	public final void setSupplierDAO(SupplierDAO supplierDAO)
	{
		this.supplierDAO = supplierDAO;
	}

	public Users loginAuthentication(String userName, String password) throws ScanSeeServiceException
	{

		final String methodName = "loginAuthentication";
		LOG.info("In Service...." + ApplicationConstants.METHODSTART + methodName);
		Users userInfo = null;

		try
		{

			userInfo = supplierDAO.loginAuthentication(userName, password);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return userInfo;
	}

	public ArrayList<PlanInfo> getAllPlanList() throws ScanSeeServiceException
	{

		ArrayList<PlanInfo> planList = null;

		planList = supplierDAO.fetchAllPlanList();

		return planList;

	}

	public PlanInfo getPlanInfo(Integer retailLocationID, Integer productID)
	{

		PlanInfo planInfo = null;

		// planInfo = searchDAO.fetchProductInfo(retailLocationID, productID);
		return planInfo;

	}

	public SearchResultInfo getAllManageProduct(String productName, int manufacturerID, int lowerLimit) throws ScanSeeServiceException
	{
		final String methodName = "getAllManageProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo result = null;

		try
		{
			result = supplierDAO.getProductsByProductUpcOrName(productName, manufacturerID, lowerLimit);
		}
		catch (ScanSeeWebSqlException exception)
		{

			LOG.info("Exception in  getAllManageProduct", exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return result;

	}

	public String insertUserProfileDetails(SupplierRegistrationInfo supplierRegistrationInfo, String strLogoImage) throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl : insertUserProfileDetails ");
		String isDataInserted = null;
		String smtpHost = null;
		String smtpPort = null;
		String strResponse = null;
		String strAdminEmailId = null;
		try
		{
			isDataInserted = supplierDAO.supplierRegistration(supplierRegistrationInfo);
			if (isDataInserted != null && isDataInserted.equals(ApplicationConstants.SUCCESS))
			{
				ArrayList<AppConfiguration> emailConf = supplierDAO.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				ArrayList<AppConfiguration> adminEmailList = supplierDAO.getAppConfig(ApplicationConstants.WEBREGISTRATION);
				for (int j = 0; j < adminEmailList.size(); j++)
				{
					if (adminEmailList.get(j).getScreenName().equals(ApplicationConstants.ADMINEMAILID))
					{
						strAdminEmailId = adminEmailList.get(j).getScreenContent();
					}
				}
				ArrayList<AppConfiguration> list = supplierDAO.getAppConfig(ApplicationConstants.FACEBOOKCONFG);
				for (int j = 0; j < list.size(); j++)
				{
					if (list.get(j).getScreenName().equals(ApplicationConstants.SCANSEEBASEURL))
					{

						supplierRegistrationInfo.setScanSeeUrl(list.get(j).getScreenContent());

					}
				}
				strResponse = Utility.sendMailSupplierLoginSuccess(supplierRegistrationInfo, smtpHost, smtpPort, strAdminEmailId, strLogoImage);
				if (strResponse != null && strResponse.equals(ApplicationConstants.SUCCESS))
				{
					isDataInserted = ApplicationConstants.SUCCESS;
				}

			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl : insertUserProfileDetails " + e.getMessage());
			// throw new ScanSeeServiceException();
		}

		// TODO Auto-generated method stub
		return isDataInserted;

	}

	public String addProduct(ProductInfoVO productInfoVO, String realPath, Users loginUser) throws ScanSeeServiceException
	{
		final String methodName = "addProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		long userId = loginUser.getUserID();
		String response = ApplicationConstants.SUCCESS;
		String regEx = "\\|\\|";
		String aPath = null;
		String iPath = null;
		String vPath = null;
		String[] imagesName = null;
		try
		{
			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.SUPPLIER, loginUser.getSupplierId());
			String suppMediaPath = mediaPathBuilder.toString();
			CommonsMultipartFile[] imageFile = productInfoVO.getImageFile();
			CommonsMultipartFile[] audioFile = productInfoVO.getAudioFile();
			CommonsMultipartFile[] videoFile = productInfoVO.getVideoFile();

			if (null != imageFile)
			{

				StringBuilder namebuilder = new StringBuilder();
				for (int i = 0; i < imageFile.length; i++)
				{
					if (!imageFile[i].isEmpty())
					{
						String path = suppMediaPath + "/" + imageFile[i].getOriginalFilename();
						LOG.info("Saving Image File to Disk Path " + path);
						namebuilder.append(imageFile[i].getOriginalFilename());
						if (i != (imageFile.length - 1))
						{
							namebuilder.append("||");
						}
						Utility.writeFileData(imageFile[i], path);

					}
				}
				imagesName = namebuilder.toString().split(regEx);
				iPath = Utility.getCommaSeparatedVal(imagesName);
			}

			if (null != audioFile)
			{
				String[] audios = new String[Utility.getArrayLength(audioFile)];
				for (int i = 0; i < audioFile.length; i++)
				{
					if (!audioFile[i].isEmpty())
					{
						String path = suppMediaPath + "/" + audioFile[i].getOriginalFilename();
						LOG.info("Saving Audio File to Disk Path " + path);
						audios[i] = audioFile[i].getOriginalFilename();
						Utility.writeFileData(audioFile[i], path);
					}
				}
				aPath = Utility.getCommaSeparatedVal(audios);
			}
			if (null != videoFile)
			{
				String[] videos = new String[Utility.getArrayLength(videoFile)];
				for (int i = 0; i < videoFile.length; i++)
				{
					if (!videoFile[i].isEmpty())
					{
						String path = suppMediaPath + "/" + videoFile[i].getOriginalFilename();
						LOG.info("Saving Video File to Disk Path " + path);
						videos[i] = videoFile[i].getOriginalFilename();
						Utility.writeFileData(videoFile[i], path);
					}
				}
				vPath = Utility.getCommaSeparatedVal(videos);
			}

			if (iPath == null || "".equals(iPath) || "null".equals(iPath))
			{
				productInfoVO.setDefaultImagePath(null);
				productInfoVO.setImagePaths(null);
			}
			else
			{
				productInfoVO.setDefaultImagePath(imagesName[0]);
				productInfoVO.setImagePaths(iPath);
			}

			if (aPath == null || "".equals(aPath) || "null".equals(aPath))
			{
				productInfoVO.setAudioFilePath(null);
			}
			else
			{
				productInfoVO.setAudioFilePath(aPath);
			}

			if (vPath == null || "".equals(vPath) || "null".equals(vPath))
			{
				productInfoVO.setVideoFilePath(null);
			}
			else
			{
				productInfoVO.setVideoFilePath(vPath);
			}

			/*
			 * productInfoVO.setDefaultImagePath(images[0]);
			 * productInfoVO.setImagePaths
			 * (Utility.getCommaSeparatedVal(images));
			 * productInfoVO.setAudioFilePath
			 * (Utility.getCommaSeparatedVal(videos));
			 * productInfoVO.setVideoFilePath
			 * (Utility.getCommaSeparatedVal(audio));
			 */
			response = supplierDAO.addManageProducts(productInfoVO, userId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.info("Exception Occurred in addProduct " + exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String addProductMedia(ManageProducts productInfoVO, String realPath, Users loginUser, int productId, String type)
			throws ScanSeeServiceException
	{
		final String methodName = "addProductMedia";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		long userId = loginUser.getUserID();
		int supplierId = loginUser.getSupplierId();
		LOG.info("supplierId is*************" + supplierId + "User Id is*****" + userId);

		ScanSeeProperties properties = new ScanSeeProperties();
		properties.readProperties();
		String mediaPath = ScanSeeProperties.getPropertyValue("productMediaPath");
		String response = ApplicationConstants.SUCCESS;
		try
		{
			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.SUPPLIER, loginUser.getSupplierId());
			String suppMediaPath = mediaPathBuilder.toString();
			CommonsMultipartFile[] imageFile = productInfoVO.getImageFile();
			CommonsMultipartFile[] audioFile = productInfoVO.getAudioFile();
			CommonsMultipartFile[] videoFile = productInfoVO.getVideoFile();
			// If Image Uploaded,
			if (type.equalsIgnoreCase("image"))
			{
				if (imageFile != null)
				{

					String[] images = new String[imageFile.length - 1];
					for (int i = 0; i < imageFile.length - 1; i++)
					{

						if (!imageFile[i].isEmpty())
						{
							String path = suppMediaPath + "/" + imageFile[i].getOriginalFilename();
							LOG.info("Saving Image File to Disk Path " + path);
							images[i] = imageFile[i].getOriginalFilename();
							Utility.writeFileData(imageFile[i], path);
						}
					}

					String iPath = Utility.getCommaSeparatedVal(images);
					if ("null".equals(iPath))
					{
						productInfoVO.setImagePaths(null);
					}
					else
					{
						// boolean flag = false ;
						boolean flag = Utility.isCorrectFileExtForImage(iPath);
						if (flag == true)
						{
							productInfoVO.setImagePaths(iPath);
						}
					}
				}
			}
			// If Audio Uploaded
			if (type.equalsIgnoreCase("audio"))
			{
				if (audioFile != null)
				{
					String[] audio = new String[audioFile.length - 1];
					for (int i = 0; i < audioFile.length - 1; i++)
					{
						if (!audioFile[i].isEmpty())
						{
							String path = suppMediaPath + "/" + audioFile[i].getOriginalFilename();
							LOG.info("Saving Audio File to Disk Path " + path);
							audio[i] = audioFile[i].getOriginalFilename();
							Utility.writeFileData(audioFile[i], path);
						}
					}
					String aPath = Utility.getCommaSeparatedVal(audio);
					if ("null".equals(aPath))
					{
						productInfoVO.setAudioFilePath(null);
					}
					else
					{
						// boolean flag = false ;
						boolean flag = Utility.isCorrectFileExtForAudio(aPath);
						if (flag == true)
						{
							productInfoVO.setAudioFilePath(aPath);
						}
					}
				}
			}
			// If Videos Uploaded
			if (type.equalsIgnoreCase("video"))
			{
				if (videoFile != null)
				{
					String[] videos = new String[videoFile.length - 1];
					for (int i = 0; i < videoFile.length - 1; i++)
					{
						if (!videoFile[i].isEmpty())
						{
							String path = suppMediaPath + "/" + videoFile[i].getOriginalFilename();
							LOG.info("Saving video File to Disk Path " + path);
							videos[i] = videoFile[i].getOriginalFilename();
							Utility.writeFileData(videoFile[i], path);
						}
					}
					String vPath = Utility.getCommaSeparatedVal(videos);
					if ("null".equals(vPath))
					{
						productInfoVO.setVideoFilePath(null);
					}
					else
					{
						// boolean flag = false;
						boolean flag = Utility.isCorrectFileExtForAudioVideo(vPath);
						if (flag == true)
						{
							productInfoVO.setVideoFilePath(vPath);
						}
					}

				}
			}
			response = supplierDAO.addProductMedia(productInfoVO, userId, supplierId, productId);
		}
		catch (Exception exception)
		{
			LOG.info("Exception Occurred in addProductMedia " + exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method Add the Rebates details into database.
	 * 
	 * @param rebatesVo
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeServiceException
	 */
	public String addRebates(Rebates rebatesVo) throws ScanSeeServiceException
	{
		final String methodName = "addRebates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String isDataInserted = null;
		try
		{
			isDataInserted = supplierDAO.addRebates(rebatesVo);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return isDataInserted;

	}

	/**
	 * This method Add the Rebates details into database.
	 * 
	 * @param rebatesVo
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeServiceException
	 */
	public String addRerunRebates(Rebates rebatesVo) throws ScanSeeServiceException
	{
		String isDataInserted = null;
		final String methodName = "addRerunRebates";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		try
		{
			isDataInserted = supplierDAO.addRerunRebates(rebatesVo);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return isDataInserted;

	}

	/**
	 * This method Add the Rebates details into database.
	 * 
	 * @param rebatesVo
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeServiceException
	 */
	public String editRebates(Rebates rebatesVo) throws ScanSeeServiceException
	{
		String isDataInserted = null;
		final String methodName = "editRebates";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		try
		{
			isDataInserted = supplierDAO.updateRebates(rebatesVo);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return isDataInserted;

	}

	public List<SupplierProfile> getUpdateProfile(Long userId)
	{

		List<SupplierProfile> userProfile = null;
		try
		{
			userProfile = supplierDAO.getUpdateProfile(userId);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return userProfile;

	}

	public List<Rebates> getRebatesForDisplay(int rebateId)
	{

		List<Rebates> rebatesList = null;
		try
		{
			rebatesList = supplierDAO.getRebatesForDisplay(rebateId);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rebatesList;

	}

	public String updateProfile(SupplierProfile supplierProfile)
	{
		String isProfileUpdated = "";
		try
		{
			isProfileUpdated = supplierDAO.addUpdateProfile(supplierProfile);
		}
		catch (Exception e)
		{

			e.printStackTrace();
		}

		return isProfileUpdated;

	}

	public SearchResultInfo getRebatesAll(Long userId, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl :  getRebatesAll ");
		SearchResultInfo listOfRebates = null;
		try
		{
			listOfRebates = supplierDAO.getRebatesAll(userId, objForm.getSearchKey(), lowerLimit);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return listOfRebates;
	}

	/**
	 * This method Add the deals details into database.
	 * 
	 * @param dealsVo
	 *            instance of HotDealInfo.
	 * @return success or failure .
	 * @throws ScanSeeServiceException
	 */
	public String addDeals(int supplierID, HotDealInfo dealsVo) throws ScanSeeServiceException
	{
		String isDataInserted = null;
		final String methodName = "addDeals";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		try
		{
			isDataInserted = supplierDAO.addHotDeal(supplierID, dealsVo);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return isDataInserted;

	}

	/**
	 * This method to get deals inforamtion.
	 * 
	 * @param dealId
	 *            input parameter
	 * @return success or failure .
	 * @throws ScanSeeServiceException
	 *             Exception.
	 */
	public List<HotDealInfo> getHotDealsDetails(int dealId) throws ScanSeeServiceException
	{
		List<HotDealInfo> dealsList = null;
		final String methodName = "getHotDealsDetails";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		try
		{
			dealsList = supplierDAO.getHotDealsDetails(dealId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return dealsList;

	}

	/**
	 * updateHotDeals method to update deals information in databse based on
	 * dealid.
	 * 
	 * @param dealsVo
	 *            As input instance
	 * @return Success or Failure.
	 * @throws ScanSeeServiceException
	 *             As service exception.
	 */

	public String updateHotDeals(HotDealInfo dealsVo) throws ScanSeeServiceException
	{
		String isDataInserted = null;
		final String methodName = "updateHotDeals";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		try
		{
			isDataInserted = supplierDAO.hotDealUpdate(dealsVo);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return isDataInserted;

	}

	/**
	 * The serviceImpl method for displaying all the states and it will call
	 * DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return states,List of states.
	 */
	public final ArrayList<State> getAllStates() throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl : getAllStates ");
		ArrayList<State> states = null;
		try
		{
			states = supplierDAO.getAllStates();
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return states;
	}

	/**
	 * The serviceImpl method for displaying all the states and it will call
	 * DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return states,List of states.
	 */
	public final ArrayList<State> getState(String state) throws ScanSeeServiceException
	{
		final String methodName = "getState";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		ArrayList<State> states = null;
		try
		{
			states = supplierDAO.getState(state);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return states;
	}

	/**
	 * The serviceImpl method for displaying all the cities and it will call
	 * DAO.
	 * 
	 * @param stateCode
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return cities,List of cities.
	 */
	public final ArrayList<City> getAllCities(String stateCode) throws ScanSeeServiceException
	{
		final String methodName = "getAllCities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		ArrayList<City> cities = null;
		try
		{
			cities = supplierDAO.getAllCities(stateCode);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return cities;
	}

	public SearchResultInfo getDealsForDisplay(Long userId, String pdtName, int lowerLimit) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageHotDealController : getDealsForDisplay ");
		List<HotDealInfo> dealsList = null;
		SearchResultInfo searchResultInfo = null;
		try
		{
			searchResultInfo = supplierDAO.getDealsForDisplay(userId, pdtName, lowerLimit);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside ManageHotDealController : getDealsForDisplay : " + e.getMessage());
			e.printStackTrace();
		}
		return searchResultInfo;
	}

	public ArrayList<Retailer> getRetailers() throws ScanSeeServiceException
	{
		ArrayList<Retailer> retailers = null;
		try
		{
			retailers = supplierDAO.getRetailers();
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retailers;
	}

	public ArrayList<Retailer> getRetailerLoc(int retailerId) throws ScanSeeServiceException
	{
		final String methodName = "getRetailerLoc";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Retailer> retailers = null;
		if (ISDUBUGENABLED)
		{
			LOG.debug("fetching Retail locations for Retailerid {}", retailerId);
		}
		try
		{
			retailers = supplierDAO.getRetailerLoc(retailerId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Exception occurred::", exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailers;
	}

	/**
	 * The serviceImpl method for displaying all the retailer location for
	 * products and it will call DAO.
	 * 
	 * @param productIDs
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             as service exception.
	 * @return location,List of location.
	 */
	public ArrayList<Retailer> getRetailersLocForProducts(String productIDs, int retailID) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerServiceImpl : getRetailersLocForProducts ");
		ArrayList<Retailer> retailers = null;
		if (ISDUBUGENABLED)
		{
			LOG.debug("fetching Retail locations for Retailerid {}", retailID, productIDs);
		}
		try
		{
			retailers = supplierDAO.getRetailerLocForProducts(productIDs, retailID);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return retailers;
	}

	/**
	 * This method will return the all Category List .
	 * 
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<Category> getAllCategory() throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl :  getAllCategory ");
		ArrayList<Category> arCategoriesList = null;
		try
		{
			arCategoriesList = supplierDAO.getAllCategory();
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  getAllCategory : " + e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}
		return arCategoriesList;
	}

	public List<ProductAttributes> getMasterAttributesList() throws ScanSeeServiceException
	{
		final String methodName = "getMasterAttributesList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductAttributes> attrList = null;
		try
		{
			attrList = supplierDAO.getMasterProdAttrList();
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return attrList;
	}

	public String saveBatchFile(ManageProducts manageProducts, int manufacturerID, long userId) throws ScanSeeServiceException
	{
		final String methodName = "saveBatchFile";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String isDataInserted = null;
		ManageProducts prodList;
		List<ManageProducts> discardedProductsList = null;
		String strLogoImage = getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;

		String strSubject = null;
		String strContent = null;
		String strToMail = null;
		boolean bStatus = false;
		String strFname = null;
		try
		{
			/* Get valid and invalid uploaded products list. */
			prodList = Utility.getProductList(manageProducts.getProductuploadFilePath());

			/*
			 * Create an entry in the Log table that holds the summary about the
			 * batch process.
			 */
			Integer logId = supplierDAO.getStageTableLogId(manufacturerID, userId, manageProducts.getProductuploadFilePath().getOriginalFilename());
			manageProducts.setLogId(logId);

			if (prodList != null && prodList.getValidProductList().size() > 0)
			{
				/* Move valid products to stage table. */
				isDataInserted = supplierDAO.addBatchUploadProductsToStage(prodList.getValidProductList(), manufacturerID, userId);

				if (null != isDataInserted && !isDataInserted.equals(ApplicationConstants.FAILURE))
				{
					/* Move data from stage table to production table. */
					isDataInserted = moveProductDataFromStagingToProduction(manageProducts, manufacturerID, userId, manageProducts
							.getProductuploadFilePath().getOriginalFilename(), null);
				}
			}

			if ((null != isDataInserted && !isDataInserted.equals(ApplicationConstants.FAILURE)) || prodList.getValidProductList().isEmpty())
			{
				if (prodList != null && prodList.getInvalidProductList().size() > 0)
				{
					/* Move invalid products to discarded records table. */
					isDataInserted = supplierDAO.insertDiscardedProductse(prodList.getInvalidProductList(), manufacturerID, userId, logId,
							manageProducts.getProductuploadFilePath().getOriginalFilename());
				}

				/*
				 * Get Discarded records that needs to be notify to the user via
				 * Email.
				 */
				discardedProductsList = getDiscardedRecordsForProducts(userId, manufacturerID, manageProducts);

				if (discardedProductsList != null && discardedProductsList.size() > 0)
				{
					/* Write discarded records into CSV file. */
					Utility.writeDiscardedProductsDataToCSV(discardedProductsList, manageProducts.getDiscardedRecordsFIleName());

					strToMail = manageProducts.getContantEmail();
					strFname = manageProducts.getFirstName();
					strSubject = ApplicationConstants.SUBJECT_MSG_FOR_PRODUCT_MAILSEND;
					strContent = "<Html>Hi " + strFname + ",<br/><br/>" + ApplicationConstants.CONTENT_MSG_FOR_PRODUCT_MAILSEND
							+ "<br/><br/>Regards,<br/>HubCiti Team<br/>" + "<img src= " + strLogoImage
							+ " alt=\"scansee logo\"  border=\"0\"></Html>";
					/*
					 * Send discarded records mail to the user with CSV file
					 * attached.
					 */
					bStatus = sendMailForProductBatchUpload(manageProducts.getDiscardedRecordsFIleName(), strToMail, strSubject, strContent);

					if (bStatus)
					{
						File f = new File(manageProducts.getDiscardedRecordsFIleName());
						bStatus = f.delete();
					}
					isDataInserted = ApplicationConstants.RECORDSDISCARDED;
				}

				/* Clear stage table. */
				deleteAllFromProductsStageTable(manufacturerID, userId);
			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		catch (Exception exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		return isDataInserted;
	}

	public List<Product> getProductAssociatedto(String pdtName, Long supplierID) throws ScanSeeServiceException
	{
		List<Product> pdtSupplierList = null;
		try
		{
			pdtSupplierList = supplierDAO.getProductAssociatedto(pdtName, supplierID);
		}
		catch (ScanSeeWebSqlException exception)
		{
			exception.printStackTrace();
		}
		return pdtSupplierList;

	}

	public String uploadAttributeFile(ManageProducts manageProducts, int manufacturerID, long userId) throws ScanSeeServiceException
	{
		final String methoName = "uploadAttributeFile";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		String isAttributeInserted = null;
		ManageProducts objManageProducts = null;
		String attributeFileName = manageProducts.getAttributeuploadFile().getOriginalFilename();
		List<ManageProducts> arMgProductsList = null;
		String strSubject = null;
		String strContent = null;
		String strToMail = null;
		boolean bStatus = false;
		String strFname = null;
		String strLogoImage = getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
		try
		{

			objManageProducts = Utility.getAttributeList(manageProducts.getAttributeuploadFile());
			// Create an entry in the Log table that holds the summary about the
			// batch process
			Integer logId = supplierDAO.getAttributStageTableLogId(manufacturerID, userId, manageProducts.getAttributeuploadFile()
					.getOriginalFilename());
			manageProducts.setLogId(logId);

			if (null != objManageProducts && objManageProducts.getValidProductList().size() > 0)
			{

				isAttributeInserted = supplierDAO.uploadAttributeToStage(objManageProducts.getValidProductList(), manufacturerID, userId);

				if (null != isAttributeInserted && !isAttributeInserted.equals(ApplicationConstants.FAILURE))
				{
					isAttributeInserted = moveAttributesDataFromStagingToProduction(manageProducts, manufacturerID, userId, null, attributeFileName);

				}

			}

			if ((null != isAttributeInserted && !isAttributeInserted.equals(ApplicationConstants.FAILURE))
					|| objManageProducts.getValidProductList().isEmpty())
			{
				if (objManageProducts != null && objManageProducts.getInvalidProductList().size() > 0)
				{
					objManageProducts.setAttributeuploadFile(manageProducts.getAttributeuploadFile());
					// Move invalid products to discarded records table
					isAttributeInserted = supplierDAO.insertDiscardedAttributes(objManageProducts, manufacturerID, userId, logId);

				}
				arMgProductsList = getDiscardedRecordsForAttributes(userId, manufacturerID, manageProducts);

				if (!arMgProductsList.isEmpty())
				{

					Utility.writeDiscardedAttributesDataToCSV(arMgProductsList, manageProducts.getDiscardedAttrRecordsFIleName());
					strToMail = manageProducts.getContantEmail();
					strFname = manageProducts.getFirstName();
					strSubject = ApplicationConstants.SUBJECT_MSG_FOR_ATTRIBUTE_MAILSEND;
					strContent = "<Html>Hi " + strFname + ",<br/><br/>" + ApplicationConstants.CONTENT_MSG_FOR_ATTRIBUTE_MAILSEND
							+ "<br/><br/>Regards,<br/>HubCiti Team<br/>" + "<img src= " + strLogoImage
							+ " alt=\"scansee logo\"  border=\"0\"></Html>";
					bStatus = sendMailForProductBatchUpload(manageProducts.getDiscardedAttrRecordsFIleName(), strToMail, strSubject, strContent);

					if (bStatus)
					{
						File f = new File(manageProducts.getDiscardedAttrRecordsFIleName());
						bStatus = f.delete();
					}

					isAttributeInserted = ApplicationConstants.RECORDSDISCARDED;
				}

				deleteAllFromProductAttributesStageTable(manufacturerID, userId);
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return isAttributeInserted;
	}

	public String moveProductDataFromStagingToProduction(ManageProducts objMgProducts, int manufacturerID, long userId, String productFileName,
			String attributeFileName) throws ScanSeeServiceException
	{
		final String methoName = "moveProductDataFromStagingToProduction";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		String isDataMovedFromStagingToProduction = null;

		try
		{

			isDataMovedFromStagingToProduction = supplierDAO.moveProductDataStagingToProductionTable(objMgProducts, manufacturerID, userId,
					productFileName, attributeFileName);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return isDataMovedFromStagingToProduction;
	}

	public String moveAttributesDataFromStagingToProduction(ManageProducts manageProducts, int manufacturerID, long userId, String productFileName,
			String attributeFileName) throws ScanSeeServiceException
	{
		final String methoName = "moveAttributesDataFromStagingToProduction";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		String isDataMovedFromStagingToProduction = null;

		try
		{

			isDataMovedFromStagingToProduction = supplierDAO.moveAttributesDataStagingToProductionTable(manageProducts, manufacturerID, userId,
					productFileName, attributeFileName);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return isDataMovedFromStagingToProduction;
	}

	public String saveProductGridData(ManageProducts manageProducts, Users user) throws ScanSeeServiceException
	{
		final String methoName = "saveProductGridData";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		String response = null;
		try
		{
			LOG.info("Converting JSON to List");
			String prodJson = manageProducts.getProdJson();
			List<ManageProducts> prodList = Utility.jsonToObject(prodJson);
			response = supplierDAO.saveGridProducts(prodList, user);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return response;
	}

	public ArrayList<Rebates> getProdToAssociateRebate(String searchKey, int retailID, long retailerID, int retailerLocId)
			throws ScanSeeServiceException
	{
		ArrayList<Rebates> prodList = null;
		final String methodName = "getProdToAssociateRebate";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		try
		{
			prodList = supplierDAO.getProductListForRebates(searchKey, retailID, retailerID, retailerLocId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return prodList;
	}

	/*
	 * public ArrayList<HotDealInfo> getProdToAssociateHotDeal(String searchKey,
	 * int retailID) throws ScanSeeServiceException { ArrayList<HotDealInfo>
	 * prodList = null; final String methodName = "getProdToAssociateDeal";
	 * LOG.info("In service..." + ApplicationConstants.METHODSTART +
	 * methodName); try { prodList =
	 * supplierDAO.getProductListForHotDeal(searchKey, retailID); } catch
	 * (ScanSeeWebSqlException exception) { throw new
	 * ScanSeeServiceException(exception.getMessage(), exception); }
	 * LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
	 * return prodList; }
	 */

	public List<ProductAttributes> fetchProductAttributes(int productId, int supplierId, String type) throws ScanSeeServiceException
	{
		final String methoName = "fetchProductAttributes";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		List<ProductAttributes> attrList = null;
		try
		{
			attrList = supplierDAO.fetchProductAttributes(productId, supplierId, type);
		}
		catch (Exception e)
		{

		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return attrList;
	}

	public String updateHotDeal(HotDealInfo hotDealInfo, Long supplierID, String flag) throws ScanSeeServiceException
	{
		String isDataInserted = null;
		final String methodName = "updateHotDeal";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		try
		{
			isDataInserted = supplierDAO.updateHotDeal(hotDealInfo, supplierID, flag);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return isDataInserted;

	}

	public List<HotDealInfo> getHotDealByID(int dealID) throws ScanSeeServiceException
	{
		List<HotDealInfo> hotDealList = null;
		try
		{
			hotDealList = supplierDAO.getHotDealByID(dealID);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hotDealList;
	}

	public String saveAttributes(ManageProducts manageProducts, Users loginUser) throws ScanSeeServiceException
	{
		final String methoName = "saveAttributes";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		String response = null;
		try
		{
			response = supplierDAO.saveAttributes(manageProducts, loginUser.getUserID(), loginUser.getSupplierId());
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return response;
	}

	public ArrayList<Retailer> getRetailersForProducts(String productId) throws ScanSeeServiceException
	{
		ArrayList<Retailer> retailers = null;
		try
		{
			retailers = supplierDAO.getRetailersForProducts(productId);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retailers;
	}

	public ArrayList<Retailer> getRetailerLocForProducts(String pdtId, int retLocID) throws ScanSeeServiceException
	{
		ArrayList<Retailer> retailers = null;
		try
		{
			retailers = supplierDAO.getRetailerLoc(retLocID);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retailers;
	}

	public List<Product> getProductUPCList(Long supplierId, String productName) throws ScanSeeServiceException
	{
		List<Product> productList = null;
		try
		{
			productList = supplierDAO.getProductUPCList(supplierId, productName);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productList;
	}

	public PlanInfo getDiscountValue(String discountCode) throws ScanSeeServiceException
	{

		Double discountValue = null;

		PlanInfo planInfo = null;

		try
		{
			planInfo = supplierDAO.getDiscount(discountCode);
		}
		catch (ScanSeeWebSqlException e)
		{

			e.printStackTrace();
		}

		return planInfo;
	}

	public String savePlanInfo(List<PlanInfo> pInfoList, int manufacturerID, long userId, Integer discountPlanManufacturerID)
			throws ScanSeeServiceException
	{
		String response = null;

		try
		{
			response = supplierDAO.savePlan(pInfoList, manufacturerID, userId, discountPlanManufacturerID);
		}
		catch (ScanSeeWebSqlException e)
		{

			e.printStackTrace();
		}

		return response;
	}

	public List<Product> getPdtInfoForDealRerun(String productIds)
	{
		List<Product> productList = null;
		try
		{
			productList = supplierDAO.getPdtInfoForDealRerun(productIds);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productList;
	}

	/**
	 * This method will return the time zones list .
	 * 
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<TimeZones> getAllTimeZones() throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl :  getAllTimeZones ");
		ArrayList<TimeZones> timeZonelst = null;
		try
		{
			timeZonelst = supplierDAO.getAllTimeZones();

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  getAllCategory : " + e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}
		return timeZonelst;
	}

	/**
	 * This method will return the all Business Category List .
	 * 
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<Category> getAllBusinessCategory() throws ScanSeeServiceException
	{
		final String methodName = "getAllBusinessCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Category> arCategoriesList = null;
		try
		{
			arCategoriesList = supplierDAO.getAllBusinessCategory();
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  getAllBusinessCategory : " + e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arCategoriesList;
	}

	public ArrayList<PlanInfo> getAllFeeDetailsList() throws ScanSeeServiceException
	{
		ArrayList<PlanInfo> planList = null;
		try
		{
			planList = supplierDAO.fetchAllFeeList();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return planList;

	}

	/**
	 * This method will return the all AccountType List .
	 * 
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<AccountType> getAllAcountType() throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl :  getAllAcountType ");
		ArrayList<AccountType> arAccountTypeList = null;
		try
		{
			arAccountTypeList = supplierDAO.getAllAcountType();
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  getAllAcountType : " + e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}
		return arAccountTypeList;
	}

	/**
	 * This method save Manufacturer Bank Information details.
	 * 
	 * @param manufacturerID
	 * @param userId
	 * @param objManufBankInfo
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public String ManufBankInfo(Long manufacturerID, Long userId, String isPaymentType, PlanInfo objPlanInfo) throws ScanSeeServiceException
	{
		{
			String response = null;
			try
			{
				response = supplierDAO.ManufBankInfo(manufacturerID, userId, isPaymentType, objPlanInfo);
			}
			catch (ScanSeeWebSqlException e)
			{
				e.printStackTrace();
			}
			return response;
		}
	}

	public List<Product> fetchProductMedia(int productId) throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl :  fetchProductMedia ");
		List<Product> mediaList = null;
		try
		{
			mediaList = supplierDAO.fetchProductMedia(productId);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  fetchProductMedia : " + e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}
		LOG.info("Exiting SupplierServiceImpl :  fetchProductMedia ");
		return mediaList;
	}

	public List<ManageProducts> getProductPreviewList(Long productId) throws ScanSeeServiceException
	{

		List<ManageProducts> prodList = null;
		try
		{
			prodList = supplierDAO.fetchProductPreviewList(productId);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  fetchProductPreview : " + e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}
		LOG.info("Exiting SupplierServiceImpl :  getProductPreviewList ");

		return prodList;
	}

	public ReportConfigInfo fetchReportConfiguration() throws ScanSeeServiceException
	{
		final String methodName = "fetchReportConfiguration";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		ReportConfigInfo reportConfigInfo = new ReportConfigInfo();
		try
		{
			appConfigurationList = supplierDAO.fetchReportServerConfiguration();

			for (int j = 0; j < appConfigurationList.size(); j++)
			{
				if (appConfigurationList.get(j).getScreenName().equals(ApplicationConstants.REPORT_PATH))
				{
					reportConfigInfo.setReportPath(appConfigurationList.get(j).getScreenContent());
				}
				if (appConfigurationList.get(j).getScreenName().equals(ApplicationConstants.REPORT_URL))
				{
					reportConfigInfo.setReportURL(appConfigurationList.get(j).getScreenContent());
				}
				if (appConfigurationList.get(j).getScreenName().equals(ApplicationConstants.REPORTSERVER_PASSWORD))
				{
					reportConfigInfo.setReortServerPassword(appConfigurationList.get(j).getScreenContent());
				}
				if (appConfigurationList.get(j).getScreenName().equals(ApplicationConstants.REPORTSERVER_USERNAME))
				{
					reportConfigInfo.setReportServerUserName(appConfigurationList.get(j).getScreenContent());
				}
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  fetchProductMedia : " + e);
			throw new ScanSeeServiceException(e.getMessage(), e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return reportConfigInfo;
	}

	public List<Rebates> getProductDetail(String productName, int supplierId) throws ScanSeeServiceException
	{

		List<Rebates> prodList = null;
		final String methodName = "getProductDetail";
		LOG.info("In service..." + ApplicationConstants.METHODSTART + methodName);
		try
		{
			prodList = supplierDAO.getProductList(productName, supplierId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return prodList;
	}

	public List<Rebates> fetchProductInformation(int rebateId) throws ScanSeeServiceException
	{
		List<Rebates> rebatesprodList = null;
		try
		{
			rebatesprodList = supplierDAO.getProductInformation(rebateId);
		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}

		return rebatesprodList;
	}

	public List<HotDealInfo> getProdDetailsInfo(Long supplierId, String productName) throws ScanSeeServiceException
	{
		List<HotDealInfo> productList = null;
		try
		{
			productList = supplierDAO.getProductInfoDetails(supplierId, productName);
		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}
		return productList;
	}

	public List<Rebates> getProdImage(String scancodes) throws ScanSeeServiceException
	{

		List<Rebates> productList = null;
		try
		{
			productList = supplierDAO.getProdImage(scancodes);
		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}
		return productList;
	}

	public ArrayList<ContactType> getAllContactTypes() throws ScanSeeServiceException
	{

		ArrayList<ContactType> contactTypes = new ArrayList<ContactType>();
		try
		{
			contactTypes = supplierDAO.getAllContactTypes();
		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}

		return contactTypes;

	}

	public Users changePassword(Long userId, String password) throws ScanSeeServiceException
	{
		Users userInfo = null;
		try
		{

			userInfo = supplierDAO.changePassword(userId, password);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}

		return userInfo;
	}

	/**
	 * The service Method takes the username. Calls the dao method.
	 * 
	 * @param strUsername
	 * @return success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public String forgotPwd(String strUsername, Users objUsers, String strLogoImage) throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl :  forgotPwd");
		String strResponse = null;
		String smtpHost = null;
		String smtpPort = null;
		String strAdminEmailId = null;
		String isPwdSend = null;
		try
		{
			strResponse = supplierDAO.forgotPwd(strUsername, objUsers);
			if (strResponse != null && strResponse.equals(ApplicationConstants.SUCCESS))
			{
				ArrayList<AppConfiguration> emailConf = supplierDAO.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}
				ArrayList<AppConfiguration> adminEmailList = supplierDAO.getAppConfig(ApplicationConstants.WEBREGISTRATION);
				for (int j = 0; j < adminEmailList.size(); j++)
				{
					if (adminEmailList.get(j).getScreenName().equals(ApplicationConstants.ADMINEMAILID))
					{
						strAdminEmailId = adminEmailList.get(j).getScreenContent();
					}
				}
				/*
				 * if (!Utility.checkNull(objUsers.getEmail()).equals("") ) {
				 */
				isPwdSend = Utility.sendMailForgetPassword(objUsers, smtpHost, smtpPort, strAdminEmailId, strLogoImage);
				if (isPwdSend != null && isPwdSend.equals(ApplicationConstants.SUCCESS))
				{
					strResponse = ApplicationConstants.SUCCESS;
				}
				/* } */
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  forgotPwd : " + e);
		}
		return strResponse;
	}

	/**
	 * This method is used to get rejected records in the Product Attributes
	 * Upload file.
	 * 
	 * @param iLogId
	 * @param iManufactureId
	 * @param lUserId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public List<ManageProducts> getDiscardedRecordsForAttributes(long lUserId, int iManufacturerID, ManageProducts objMgProducts)
			throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl : getDiscardedRecords");
		List<ManageProducts> arMgProductsList = new ArrayList<ManageProducts>();
		try
		{
			arMgProductsList = supplierDAO.getDiscardedRecordsForAttributes(lUserId, iManufacturerID, objMgProducts);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :getDiscardedRecords : " + e);
		}
		return arMgProductsList;
	}

	/**
	 * The service Method is used to save the Supplier QR code
	 * 
	 * @param SupplierQRCodeData
	 * @return SupplierQRCodeResponse
	 * @throws ScanSeeWebSqlException
	 */
	public QRDetails supplierQRCodeGeneration(ManageProducts manageProducts, int userId, List<Product> mediaList) throws ScanSeeServiceException
	{
		final String methodName = "supplierQRCodeGeneration";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		QRDetails pageDetails = new QRDetails();
		SupplierQRCodeResponse objSupplierQRCodeResponse = new SupplierQRCodeResponse();
		try
		{
			int productId = (int) manageProducts.getProductID();
			Integer audioID = manageProducts.getQRAudioId();
			Integer videoID = manageProducts.getQRVideoId();
			Integer prodID = manageProducts.getQRProduct();
			String audioFilePath = null;
			String videoFilePath = null;
			String audioMediaPath = null;
			String videoMediaPath = null;
			String fileLink = manageProducts.getFileLink();
			fileLink = fileLink.substring(0, fileLink.length() - 1);
			String fileName = null;
			Product objProduct = new Product();
			String supProdPageUrl = null;

			SupplierQRCodeData objSupplierQRCodeData = new SupplierQRCodeData();
			for (int i = 0; i < mediaList.size(); i++)
			{
				objProduct = mediaList.get(i);
				if (audioID != null)
				{
					if (audioID.equals(objProduct.getProductMediaID()))
					{
						audioFilePath = objProduct.getFullMediaPath();
						audioMediaPath = objProduct.getAudioPath();
					}
				}
				if (videoID != null)
				{
					if (videoID.equals(objProduct.getProductMediaID()))
					{
						videoFilePath = objProduct.getFullMediaPath();
						videoMediaPath = objProduct.getVideoPath();
					}
				}
			}
			if (!fileLink.equals(""))
			{
				if (!Utility.validateURL(fileLink))
				{
					pageDetails.setMessage("Please Enter Valid Link");
				}
				int index = fileLink.lastIndexOf("//");
				if (index < 0)
				{
					index = fileLink.lastIndexOf("'\'");
				}
				fileName = fileLink.substring(index + 2, fileLink.length());
			}
			if (pageDetails.getMessage() == null)
			{
				objSupplierQRCodeData.setUserId(userId);
				objSupplierQRCodeData.setProductId(productId);
				objSupplierQRCodeData.setAudioID(audioID);
				objSupplierQRCodeData.setVideoID(videoID);
				objSupplierQRCodeData.setFilePath(fileLink);
				QRCodeResponse objProductQRCode = new QRCodeResponse();

				ArrayList<AppConfiguration> appConfiguration = supplierDAO.getAppConfig(ApplicationConstants.QRCODECONFIGURATION);
				for (int j = 0; j < appConfiguration.size(); j++)
				{
					supProdPageUrl = appConfiguration.get(j).getScreenContent();
				}
				supProdPageUrl = supProdPageUrl + ApplicationConstants.SUPPLIERPRODUCTPAGEURL + "key1=" + productId;
				if (prodID != null)
				{
					objProductQRCode = QRCodeGenerator.generateQRCode(productId + "", supProdPageUrl);

					if (objProductQRCode.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						String tempQrPath = "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.QRPATH + "/" + productId
								+ ApplicationConstants.PNGIMAGEFORMAT;
						pageDetails.setProductImagePath(tempQrPath);
						pageDetails.setProductId(productId);
					}
				}
				if (audioFilePath != null)
				{
					objProductQRCode = QRCodeGenerator.generateQRCode(audioMediaPath, audioFilePath);
					if (objProductQRCode.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						String tempQrPath = "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.QRPATH + "/" + audioMediaPath
								+ ApplicationConstants.PNGIMAGEFORMAT;
						pageDetails.setAudioImagePath(tempQrPath);
						pageDetails.setAudio(audioMediaPath);
					}
				}

				if (videoFilePath != null)
				{
					objProductQRCode = QRCodeGenerator.generateQRCode(videoMediaPath, videoFilePath);
					if (objProductQRCode.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						String tempQrPath = "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.QRPATH + "/" + videoMediaPath
								+ ApplicationConstants.PNGIMAGEFORMAT;
						pageDetails.setVideoImagePath(tempQrPath);
						pageDetails.setVideo(videoMediaPath);
					}
				}

				if (fileName != null)
				{
					objProductQRCode = QRCodeGenerator.generateQRCode(fileName, fileLink);
					if (objProductQRCode.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						String tempQrPath = "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.QRPATH + "/" + fileName
								+ ApplicationConstants.PNGIMAGEFORMAT;
						pageDetails.setFileLinkImagePath(tempQrPath);
						pageDetails.setFileLink(fileName);
					}
				}

				objSupplierQRCodeResponse = supplierDAO.supplierQRCodeGeneration(objSupplierQRCodeData);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(e.getMessage(), e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return pageDetails;
	}

	/**
	 * This method is used to send mail to ManufactureMailID, if the rejected
	 * records in the Product Batch upload file.
	 * 
	 * @param fileName
	 * @param toContactMail
	 * @param strSubject
	 * @param strContent
	 * @return
	 */
	public boolean sendMailForProductBatchUpload(String fileName, String toAddress, String strSubject, String strMsgBody)
			throws ScanSeeServiceException
	{
		String smtpHost = null;
		String smtpPort = null;
		boolean strFlag = false;
		ArrayList<AppConfiguration> emailConf = null;
		ArrayList<AppConfiguration> adminEmailList = null;
		String fromAddress = null;
		try
		{
			emailConf = supplierDAO.getAppConfig(ApplicationConstants.EMAILCONFIG);
			for (int j = 0; j < emailConf.size(); j++)
			{
				if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
				{
					smtpHost = emailConf.get(j).getScreenContent();
				}
				if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
				{
					smtpPort = emailConf.get(j).getScreenContent();
				}
			}

			adminEmailList = supplierDAO.getAppConfig(ApplicationConstants.WEBREGISTRATION);
			for (int j = 0; j < adminEmailList.size(); j++)
			{
				if (adminEmailList.get(j).getScreenName().equals(ApplicationConstants.ADMINEMAILID))
				{
					fromAddress = adminEmailList.get(j).getScreenContent();
				}
			}
			if (null != smtpHost || null != smtpPort)
			{
				strFlag = EmailComponent
						.sendMailWithAttachmentWithoutCC(toAddress, fromAddress, strSubject, strMsgBody, fileName, smtpHost, smtpPort);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}
		return strFlag;
	}

	/**
	 * This method is used to get rejected records in the Product Attributes
	 * Upload file.
	 * 
	 * @param iLogId
	 * @param iManufactureId
	 * @param lUserId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public List<ManageProducts> getDiscardedRecordsForProducts(long lUserId, int iManufacturerID, ManageProducts objMgProducts)
			throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl : getDiscardedRecordsForProducts");
		List<ManageProducts> arMgProductsList = new ArrayList<ManageProducts>();
		try
		{
			arMgProductsList = supplierDAO.getDiscardedRecordsForProducts(lUserId, iManufacturerID, objMgProducts);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :getDiscardedRecords : " + e);
		}
		return arMgProductsList;
	}

	/**
	 * This method is used to deleted all the Products Attributes records in the
	 * staging table.
	 * 
	 * @param iManufacture
	 * @param iUserID
	 * @return Success or Failure
	 * @throws ScanSeeServiceException
	 */
	public String deleteAllFromProductAttributesStageTable(int iManufacture, long iUserId) throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl : deleteAllFromProductAttributesStageTable");
		String strResponse = null;
		try
		{
			strResponse = supplierDAO.deleteAllFromProductAttributesStageTable(iManufacture, iUserId);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl : deleteAllFromProductAttributesStageTable :" + e);
			throw new ScanSeeServiceException(e);
		}
		return strResponse;
	}

	/**
	 * This method is used to deleted all the Products records in the staging
	 * table..
	 * 
	 * @param iManufacture
	 * @param iUserID
	 * @return Success or Failure
	 * @throws ScanSeeServiceException
	 */
	public String deleteAllFromProductsStageTable(int iManufacture, long iUserId) throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl : deleteAllFromProductsStageTable");
		String strResponse = null;
		try
		{
			strResponse = supplierDAO.deleteAllFromProductsStageTable(iManufacture, iUserId);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl : deleteAllFromProductsStageTable : " + e);
			throw new ScanSeeServiceException(e);
		}
		return strResponse;
	}

	/**
	 * This method is used to domain Name for Application configuration.
	 * 
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public String getDomainName() throws ScanSeeServiceException
	{
		String strDomainName = null;
		try
		{
			ArrayList<AppConfiguration> domainNameList = supplierDAO.getAppConfigForGeneralImages(ApplicationConstants.SERVER_CONFIGURATION);
			for (int j = 0; j < domainNameList.size(); j++)
			{
				strDomainName = domainNameList.get(j).getScreenContent();
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl : getDomainName : " + e);
			throw new ScanSeeServiceException(e);
		}
		return strDomainName;
	}

	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeServiceException
	{

		LOG.info("Inside getAppConfig");
		ArrayList<AppConfiguration> appConfig = null;
		try
		{
			appConfig = supplierDAO.getAppConfig(configType);
		}

		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Exception occured in SupplierServiceImpl : getAppConfig : " + e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit getAppConfig");

		return appConfig;
	}

	public String uploadMediaFiles(ManageProducts manageProducts, int supplierId) throws ScanSeeServiceException
	{
		LOG.info("Inside SupplierServiceImpl : uploadMediaFiles ");
		String response = ApplicationConstants.SUCCESS;
		String fileName = null;
		String fileExtn = null;
		ZipEntry zipentry = null;
		ZipInputStream zipInputStream = null;
		int dotIndex;
		FileOutputStream fileoutputstream;
		ArrayList<String> rejectedFileList = new ArrayList<String>();
		String path = null;
		String zipFilepath = null;
		String entryName = null;
		long twoGBinBytes = 2147483648L;
		int n;
		byte[] buf = new byte[1024];
		LOG.info("Exit SupplierServiceImpl : uploadMediaFiles ");
		try
		{
			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.SUPPLIER, supplierId);
			String suppMediaPath = mediaPathBuilder.toString();
			LOG.info(" Images path -->" + suppMediaPath);
			CommonsMultipartFile[] imageFile = manageProducts.getImageUploadFilePath();
			if (imageFile.length > 0)
			{
				for (int i = 0; i < imageFile.length; i++)
				{
					if (imageFile[i].getSize() <= twoGBinBytes)
					{
						fileName = imageFile[i].getOriginalFilename();
						dotIndex = fileName.lastIndexOf(".");
						fileExtn = fileName.substring(dotIndex + 1, fileName.length());

						if (fileExtn.equals(ApplicationConstants.ZIPFILE))
						{
							zipFilepath = suppMediaPath + "/" + imageFile[i].getOriginalFilename();
							Utility.writeFileData(imageFile[i], zipFilepath);
							zipInputStream = new ZipInputStream(new FileInputStream(zipFilepath));
							zipentry = zipInputStream.getNextEntry();

							while (zipentry != null)
							{
								// for each entry to be extracted
								entryName = zipentry.getName();
								if (entryName.contains("/"))
								{

									String[] folderStructure = entryName.split("/");
									entryName = folderStructure[folderStructure.length - 1];
								}
								dotIndex = entryName.lastIndexOf(".");
								fileExtn = entryName.substring(dotIndex + 1, entryName.length());

								if (fileExtn.equalsIgnoreCase(ApplicationConstants.MP3FORMAT)
										|| fileExtn.equalsIgnoreCase(ApplicationConstants.MP4FORMAT)
										|| fileExtn.equalsIgnoreCase(ApplicationConstants.PNGIMAGE))
								{
									path = suppMediaPath + "/" + entryName;
									fileoutputstream = new FileOutputStream(path);
									while ((n = zipInputStream.read(buf, 0, 1024)) > -1)
										fileoutputstream.write(buf, 0, n);
									fileoutputstream.close();
									zipInputStream.closeEntry();
									zipentry = zipInputStream.getNextEntry();
									LOG.info("==Image Saved to path==" + path);

								}
								else
								{

									zipInputStream.closeEntry();
									zipentry = zipInputStream.getNextEntry();
									if (dotIndex != -1)
									{
										rejectedFileList.add(entryName);
									}

								}

							}
							zipInputStream.close();
							File f1 = new File(zipFilepath);
							f1.delete();
						}
						else
						{
							path = suppMediaPath + "/" + imageFile[i].getOriginalFilename();
							Utility.writeFileData(imageFile[i], path);
							LOG.info("==Image Saved to path==" + path);

						}
					}
					else
					{

						response = "  Please Select a file less than 2GB";
					}

				}

				if (!rejectedFileList.isEmpty())
				{
					if (rejectedFileList.size() <= 1)
					{
						response = rejectedFileList.size() + " Unsupported file has been rejected";

					}
					else
					{
						response = rejectedFileList.size() + " Unsupported files have been rejected";
					}
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside SupplierServiceImpl : uploadMediaFiles : " + e);
			response = ApplicationConstants.FAILURE;
		}
		catch (IOException e)
		{
			LOG.info("Inside SupplierServiceImpl : uploadMediaFiles : " + e);
			response = ApplicationConstants.FAILURE;
		}
		return response;
	}

	/**
	 * Retrieves discount plan list for the Supplier return discountPlanList;
	 */
	public Map<String, Map<String, String>> getSupplierDiscountPlans() throws ScanSeeServiceException
	{
		Map<String, Map<String, String>> discountPlanList = null;
		try
		{
			discountPlanList = supplierDAO.getSupplierDiscountPlans();
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  getSupplierDiscountPlans : " + e);
			throw new ScanSeeServiceException(e);
		}

		return discountPlanList;
	}

	public ArrayList<SearchZipCode> getZipStateCity(String zipCode) throws ScanSeeServiceException
	{
		ArrayList<SearchZipCode> zipCodes = null;
		LOG.info("Inside SupplierServiceImpl : getZipStateCity methos");
		try
		{
			zipCodes = (ArrayList<SearchZipCode>) supplierDAO.getZipStateCity(zipCode);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  getZipStateCity : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Existing SupplierServiceImpl : getZipStateCity methos");
		return zipCodes;

	}

	public ArrayList<SearchZipCode> getCityStateZip(String city) throws ScanSeeServiceException
	{
		ArrayList<SearchZipCode> cities = null;
		LOG.info("Inside SupplierServiceImpl : getZipStateCity methos");
		try
		{
			cities = (ArrayList<SearchZipCode>) supplierDAO.getCityStateZip(city);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  getZipStateCity : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Existing SupplierServiceImpl : getZipStateCity methos");
		return cities;

	}

	public ArrayList<DropDown> displayDropDown() throws ScanSeeServiceException
	{
		ArrayList<DropDown> dropDowns = null;
		try
		{
			dropDowns = (ArrayList<DropDown>) supplierDAO.displayDropDown();
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl :  displayDropDown : " + e);
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Existing SupplierServiceImpl : displayDropDown methos");
		return dropDowns;
	}
}

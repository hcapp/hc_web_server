package supplier.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import common.exception.ScanSeeServiceException;
import common.pojo.AccountType;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.ContactType;
import common.pojo.DropDown;
import common.pojo.HotDealInfo;
import common.pojo.ManageProducts;
import common.pojo.PlanInfo;
import common.pojo.Product;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfoVO;
import common.pojo.QRDetails;
import common.pojo.Rebates;
import common.pojo.ReportConfigInfo;
import common.pojo.Retailer;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.SearchZipCode;
import common.pojo.State;
import common.pojo.SupplierProfile;
import common.pojo.SupplierRegistrationInfo;
import common.pojo.TimeZones;
import common.pojo.Users;

/**
 * service layer of Supplier module.
 * 
 * @author Created by SPAN.
 */
public interface SupplierService
{
	/**
	 * This service method will send user with username and Password as input
	 * parameter by calling DAO method.
	 * 
	 * @param userName
	 *            as request parameter.
	 * @param password
	 *            as request parameter.
	 * @return Users instance.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	Users loginAuthentication(String userName, String password) throws ScanSeeServiceException;

	public ArrayList<PlanInfo> getAllPlanList() throws ScanSeeServiceException;

	public SearchResultInfo getAllManageProduct(String productName, int manufacturerID, int lowerLimit) throws ScanSeeServiceException;

	/**
	 * This service method display supplier register details by calling DAO
	 * method.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @return SupplierProfile details list.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<SupplierProfile> getUpdateProfile(Long userId) throws ScanSeeServiceException;

	/**
	 * This service method save supplier register details by calling DAO method.
	 * 
	 * @param supplierRegistrationInfo
	 *            instance of SupplierRegistrationInfo.
	 * @param strLogoImage
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String insertUserProfileDetails(SupplierRegistrationInfo supplierRegistrationInfo, String strLogoImage) throws ScanSeeServiceException;

	/**
	 * This service method update supplier register details by calling DAO
	 * method.
	 * 
	 * @param supplierProfile
	 *            instance of SupplierProfile.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String updateProfile(SupplierProfile supplierProfile) throws ScanSeeServiceException;

	/**
	 * This service method add new rebates for Supplier by calling DAO methods.
	 * 
	 * @param rebateVo
	 *            instance of Rebates.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String addRebates(Rebates rebateVo) throws ScanSeeServiceException;

	String addRerunRebates(Rebates rebatesVo) throws ScanSeeServiceException;

	ArrayList<Rebates> getProdToAssociateRebate(String searchKey, int retailID, long retailerID, int retailerLocId) throws ScanSeeServiceException;

	public List<Rebates> getRebatesForDisplay(int rebateId);

	/**
	 * This method Add the Rebates details into database.
	 * 
	 * @param rebateVo
	 *            instance of Rebates.
	 * @return success or failure .
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String editRebates(Rebates rebateVo) throws ScanSeeServiceException;

	/**
	 * This service method return Rebates info based on input parameter by
	 * calling DAO method.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param objForm
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return SearchResultInfo instance.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	SearchResultInfo getRebatesAll(Long userId, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException;

	/**
	 * This service method add new hot deals for Supplier by calling DAO
	 * methods.
	 * 
	 * @param supplierID
	 *            as request parameter.
	 * @param dealVo
	 *            instance of HotDealInfo.
	 * @return success or failure .
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String addDeals(int supplierID, HotDealInfo dealVo) throws ScanSeeServiceException;

	/**
	 * This method to get hot deals details based on hotdeal id..
	 * 
	 * @param dealId
	 *            instance of HotDealInfo.
	 * @return List of deals
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<HotDealInfo> getHotDealsDetails(int dealId) throws ScanSeeServiceException;

	/**
	 * This method to update the deals details.
	 * 
	 * @param dealsVo
	 *            instance of HotDealInfo.
	 * @return success or failure .
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String updateHotDeals(HotDealInfo dealsVo) throws ScanSeeServiceException;

	public String addProduct(ProductInfoVO productInfoVO, String realPath, Users loginUser) throws ScanSeeServiceException;

	/**
	 * The service method for displaying all the states and it will calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return states,List of states.
	 */
	ArrayList<State> getAllStates() throws ScanSeeServiceException;

	/**
	 * The service method for displaying all the cities and it will calling DAO.
	 * 
	 * @param stateCode
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return cities,List of cities.
	 */
	ArrayList<City> getAllCities(String stateCode) throws ScanSeeServiceException;

	/**
	 * This service method return product Hot deals info based on input
	 * parameter by calling DAO method.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param pdtName
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return SearchResultInfo instance.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	SearchResultInfo getDealsForDisplay(Long userId, String pdtName, int lowerLimit) throws ScanSeeServiceException;

	/**
	 * This service method will return the all Retailer List.
	 * 
	 * @return retailer,List of Retailer.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	ArrayList<Retailer> getRetailers() throws ScanSeeServiceException;

	/**
	 * The service method for displaying all the retailer location(retailer
	 * address (Address1, City, State, Postal code) for products and it will
	 * calling DAO.
	 * 
	 * @param retLocID
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return location,List of location.
	 */
	ArrayList<Retailer> getRetailerLoc(int retLocID) throws ScanSeeServiceException;

	/**
	 * The service method for displaying all the retailer location(retailer
	 * address (Address1, City, State, Postal code) for products and it will
	 * calling DAO.
	 * 
	 * @param productIDs
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return location,List of location.
	 */
	ArrayList<Retailer> getRetailersLocForProducts(String productIDs, int retailID) throws ScanSeeServiceException;

	/**
	 * This method will return the all Category List .
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public ArrayList<Category> getAllCategory() throws ScanSeeServiceException;

	public List<ProductAttributes> getMasterAttributesList() throws ScanSeeServiceException;

	public String saveBatchFile(ManageProducts manageProducts, int manufacturerID, long userId) throws ScanSeeServiceException;

	// public ArrayList<HotDealInfo> getProductAssociatedtoHotDeal(String
	// pdtName, Long supplierID) throws ScanSeeServiceException;;

	public String uploadAttributeFile(ManageProducts manageProducts, int manufacturerID, long userId) throws ScanSeeServiceException;

	public String saveProductGridData(ManageProducts manageProducts, Users user) throws ScanSeeServiceException;

	public List<ProductAttributes> fetchProductAttributes(int productId, int supplierId, String type) throws ScanSeeServiceException;

	/**
	 * This service method will update existing HotDeals for supplier product by
	 * calling DAO method.
	 * 
	 * @param hotDealInfo
	 *            instance of HotDealInfo.
	 * @param supplierID
	 *            as request parameter.
	 * @param flag
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String updateHotDeal(HotDealInfo hotDealInfo, Long supplierID, String flag) throws ScanSeeServiceException;

	/**
	 * This service method will display the HotDeals deals by passing HotDealId
	 * as input parameter by call service and DAO methods.
	 * 
	 * @param dealID
	 *            as request parameter.
	 * @return HotDealList, All the HotDeals.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<HotDealInfo> getHotDealByID(int dealID) throws ScanSeeServiceException;

	public String saveAttributes(ManageProducts manageProducts, Users loginUser) throws ScanSeeServiceException;

	/**
	 * This service method return all the retailer based on input
	 * parameter(productId) by calling DAO method.
	 * 
	 * @param productId
	 *            as request parameter.
	 * @return retailerList, All the retailers.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	ArrayList<Retailer> getRetailersForProducts(String productId) throws ScanSeeServiceException;

	public ArrayList<Retailer> getRetailerLocForProducts(String productId, int retLocID) throws ScanSeeServiceException;

	/**
	 * The DAO method return all the product details and its status return to
	 * service layer.
	 * 
	 * @param productName
	 *            as request parameter.
	 * @param supplierId
	 *            as request parameter.
	 * @return ProductList,List of products.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<Product> getProductUPCList(Long supplierId, String productName) throws ScanSeeServiceException;

	public PlanInfo getDiscountValue(String discountCode) throws ScanSeeServiceException;

	public String savePlanInfo(List<PlanInfo> pInfoList, int manufacturerID, long userId, Integer discountplanManufacturerid)
			throws ScanSeeServiceException;

	/**
	 * This service method return all the products that are associated with
	 * supplier hot deals on input parameter(productId) by calling DAO method.
	 * 
	 * @param productId
	 *            as request parameter.
	 * @return productList, All the products.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<Product> getPdtInfoForDealRerun(String productId) throws ScanSeeServiceException;

	/**
	 * This service method will display the all standard time like
	 * (Atlantic,Central, Eastern, Mountain, Pacific Standard Time) by calling
	 * DAO methods.
	 * 
	 * @return AllTime,List of standard time.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	ArrayList<TimeZones> getAllTimeZones() throws ScanSeeServiceException;

	/**
	 * The service method for displaying list of categories by calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return categories,List of categories.
	 */
	ArrayList<Category> getAllBusinessCategory() throws ScanSeeServiceException;

	public ArrayList<PlanInfo> getAllFeeDetailsList() throws ScanSeeServiceException;

	/**
	 * This method will return the all AccountType List .
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public ArrayList<AccountType> getAllAcountType() throws ScanSeeServiceException;

	/**
	 * This method save Manufacturer Bank Information details.
	 * 
	 * @param manufacturerID
	 * @param userId
	 * @param objManufBankInfo
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String ManufBankInfo(Long manufacturerID, Long userId, String isPaymentType, PlanInfo objPlanInfo) throws ScanSeeServiceException;

	public List<Product> fetchProductMedia(int productId) throws ScanSeeServiceException;

	public String addProductMedia(ManageProducts productInfo, String realPath, Users loginUser, int productId, String flag)
			throws ScanSeeServiceException;

	public List<ManageProducts> getProductPreviewList(Long productId) throws ScanSeeServiceException;

	public ReportConfigInfo fetchReportConfiguration() throws ScanSeeServiceException;

	public List<Rebates> getProductDetail(String productName, int supplierId) throws ScanSeeServiceException;

	public List<Rebates> fetchProductInformation(int rebateId) throws ScanSeeServiceException;

	/**
	 * The service method return if product name is empty then display product
	 * image path by calling DAO.
	 * 
	 * @param productName
	 *            as request parameter.
	 * @param supplierId
	 *            as request parameter.
	 * @return ProductList,List of products.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<HotDealInfo> getProdDetailsInfo(Long supplierId, String productName) throws ScanSeeServiceException;

	public String moveProductDataFromStagingToProduction(ManageProducts objMgProducts, int manufacturerID, long userId, String productFileName,
			String attributeFileName) throws ScanSeeServiceException;

	public String moveAttributesDataFromStagingToProduction(ManageProducts manageProducts, int manufacturerID, long userId, String productFileName,
			String attributeFileName) throws ScanSeeServiceException;

	public List<Rebates> getProdImage(String scancodes) throws ScanSeeServiceException;

	/**
	 * 
	 */
	public ArrayList<ContactType> getAllContactTypes() throws ScanSeeServiceException;

	public Users changePassword(Long userId, String password) throws ScanSeeServiceException;

	/**
	 * The service method takes the username and send the Forgot Password
	 * credential through mail by calling DAO.
	 * 
	 * @param strUsername
	 *            as request parameter.
	 * @param objUsers
	 *            as request parameter.
	 * @param strLogoImage
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String forgotPwd(String strUsername, Users objUsers, String strLogoImage) throws ScanSeeServiceException;

	/**
	 * This method is used to get the rejected records in the Product Attributes
	 * upload file.
	 * 
	 * @param iLogId
	 * @param ManufactureId
	 * @param lUserID
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<ManageProducts> getDiscardedRecordsForAttributes(long lUserID, int iManufacturerID, ManageProducts objMgProducts)
			throws ScanSeeServiceException;

	/**
	 * This method is used to send mail to ManufactureMailID, if the rejected
	 * records in the Product Batch upload file.
	 * 
	 * @param fileName
	 * @param toContactMail
	 * @param strSubject
	 * @param strContent
	 * @return
	 */
	public boolean sendMailForProductBatchUpload(String fileName, String toAddress, String strSubject, String strMsgBody)
			throws ScanSeeServiceException;;

	/**
	 * This method is used to get the rejected records in the Products upload
	 * file.
	 * 
	 * @param iLogId
	 * @param ManufactureId
	 * @param lUserID
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<ManageProducts> getDiscardedRecordsForProducts(long lUserID, int iManufacturerID, ManageProducts objMgProducts)
			throws ScanSeeServiceException;

	/**
	 * The service method is used to save the Supplier QR code
	 * 
	 * @param SupplierQRCodeData
	 * @return SupplierQRCodeResponse
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public QRDetails supplierQRCodeGeneration(ManageProducts manageProducts, int userId, List<Product> mediaList) throws ScanSeeServiceException;

	/**
	 * This method is used to deleted all the Products Attributes records in the
	 * staging table.
	 * 
	 * @param iManufacture
	 * @param iUserID
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteAllFromProductAttributesStageTable(int iManufacture, long iUserID) throws ScanSeeServiceException;

	/**
	 * This method is used to deleted all the Products records in the staging
	 * table.
	 * 
	 * @param iManufacture
	 * @param iUserID
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteAllFromProductsStageTable(int iManufacture, long iUserID) throws ScanSeeServiceException;

	/**
	 * This method is used to domain Name for Application configuration.
	 * 
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception. as service exception.
	 */
	public String getDomainName() throws ScanSeeServiceException;

	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeServiceException;

	public String uploadMediaFiles(ManageProducts manageProducts, int supplierId) throws ScanSeeServiceException;

	public Map<String, Map<String, String>> getSupplierDiscountPlans() throws ScanSeeServiceException;

	public ArrayList<SearchZipCode> getZipStateCity(String zipCode) throws ScanSeeServiceException;

	public ArrayList<SearchZipCode> getCityStateZip(String city) throws ScanSeeServiceException;

	/**
	 * The service method for displaying all the states and it will calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return states,List of states.
	 */
	ArrayList<State> getState(String state) throws ScanSeeServiceException;

	public ArrayList<DropDown> displayDropDown() throws ScanSeeServiceException;
}

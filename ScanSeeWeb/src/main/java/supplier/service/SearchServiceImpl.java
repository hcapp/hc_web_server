package supplier.service;



import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import supplier.dao.SearchDAO;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.HotDealInfo;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductReview;

public class SearchServiceImpl implements SearchService
{

	private static final Logger LOG = LoggerFactory.getLogger(SearchServiceImpl.class);
	private SearchDAO searchDAO;

	/**
	 * @return the searchDAO
	 */
	public SearchDAO getSearchDAO()
	{
		return searchDAO;
	}

	/**
	 * @param searchDAO
	 *            the searchDAO to set
	 */
	public void setSearchDAO(SearchDAO searchDAO)
	{
		this.searchDAO = searchDAO;
	}

	public ArrayList<ProductVO> getAllProduct(String zipcode, String productName, int lowerLimit, String screenName)
	{

		ArrayList<ProductVO> productList = null;

		// productList = searchDAO.fetchAllProduct(zipcode,
		// productName,lowerLimit, screenName);

		return productList;

	}

	public ProductVO getAllProductInfo(Integer retailLocationID,
			Integer productID) throws ScanSeeServiceException {

		ProductVO productInfo = null;
		try {

			productInfo = searchDAO.fetchProductInfo(retailLocationID,
					productID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return productInfo;

	}

	public SearchResultInfo searchProducts(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException
	{
		SearchResultInfo resultInfo = null;
		try
		{
			if (objForm.getSearchType().equalsIgnoreCase("products"))
			{
				// product Search
				resultInfo = searchDAO.fetchAllProduct(objForm, loginUser, lowerLimit);
			}
			else if(objForm.getSearchType().equalsIgnoreCase("deals"))
			{
				// HotDeals Search
				resultInfo = searchDAO.searchHotDeals(objForm, loginUser,lowerLimit);
			}else if(objForm.getSearchType().equalsIgnoreCase("producthotdeal"))
			{
				// product with HotDeals Search
				resultInfo = searchDAO.getProductWithDeal(objForm, loginUser,lowerLimit);
			}
			
			
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}
		
		return resultInfo;
	}
	
	
	
	public SearchResultInfo searchDeals(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException
	{
		SearchResultInfo resultInfo = null;
		try
		{

			resultInfo = searchDAO.searchHotDeals(objForm, loginUser, lowerLimit);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return resultInfo;
	}
	
	
	public ProductVO getAllProductInfoWithDeals(Integer productID) throws ScanSeeServiceException {

		ProductVO productInfo = null;
		try {

			productInfo = searchDAO.fetchProductInfoWithDeals(productID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return productInfo;

	}
	
	
	public HotDealInfo getDealsInfo(int hotDealID) throws ScanSeeServiceException {

		HotDealInfo hotDealInfo = null;
		try {

			hotDealInfo = searchDAO.fetchDealsInfo(hotDealID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return hotDealInfo;

	}

	
	
	public List<ProductVO> getProductAssociatedWithDeals(Integer productID) throws ScanSeeServiceException {

		List<ProductVO> prodAssociatedHotDealList = null;
		try {

			prodAssociatedHotDealList = searchDAO.fetchProductAssociatedWithDeals(productID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return prodAssociatedHotDealList;

	}
	
	
	public List<ProductVO> getDealWithProductAssociated(Integer hotDealID) throws ScanSeeServiceException{
		
		List<ProductVO> hotDealAssociatedProductList = null;
		try {

			hotDealAssociatedProductList = searchDAO.fetchDealWithProductAssociated(hotDealID);

		} catch (Exception e) {
			throw new ScanSeeServiceException(e.getMessage(), e);
		}

		return hotDealAssociatedProductList;
		
	}
	
	/**
	 * This is Service method for retrieving find near by retailers.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to get the Retailers.
	 * 
	 * @param AreaInfoVO
	 *            area information dtoes in the instance.
	 * @return FindNearByDetails 
	 *  		  instance containing list of retailers.
	 *  
	 */

	public FindNearByDetails findNearBy(AreaInfoVO objAreaInfoVO)	{

		final String methodName = "findNearBy";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		FindNearByDetails findNearByDetails = new FindNearByDetails();
		try{
		findNearByDetails = searchDAO.fetchNearByInfo(objAreaInfoVO);
		}catch (Exception e) {
			LOG.error("", e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return findNearByDetails;
	}

	public ExternalAPIInformation externalApiInfo(String moduleName) throws ScanSeeServiceException
	{

		ExternalAPIInformation externalAPIInformation = null;

		try
		{

			externalAPIInformation = searchDAO.getExternalApiInfo(moduleName);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return externalAPIInformation;

	}
	
	
	public ProductDetail fetchProductDetails(String productId) throws ScanSeeServiceException
	{

		ProductDetail prdDetail = null;

		try
		{

			prdDetail = searchDAO.getProductDetails(productId);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return prdDetail;

	}
	
	
	/**
	 * This is a RestEasy WebService Method for fetching product reviews and
	 * user ratings.Method Type:GET.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML Containing Product Reviews and User Ratings.
	 * @throws ScanSeeServiceException 
	 */

	public ArrayList<ProductReview> getProductReviews(Integer productId) throws ScanSeeServiceException
	{
		final String methodName = "getProductReviews";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		ArrayList<ProductReview> productReviewslist = null;
	
		try
		{
			if (null != productId)
			{
				productReviewslist = searchDAO.getProductReviews(productId);
			}
			

		}
		catch (ScanSeeWebSqlException e) {
			throw new ScanSeeServiceException(e.getMessage());
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productReviewslist;
	}
	

}

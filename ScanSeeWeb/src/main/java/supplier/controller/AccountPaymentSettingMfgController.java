/**
 * @ (#) AccountPaymentSettingMfgController.java 04-April-2012
 * Project       :ScanSeeWeb
 * File          : AccountPaymentSettingMfgController.java
 * Author        : Kumar. D 
 * Company       : Span Systems Corporation
 * Date Created  : 04-April-2012
 *
 * @author       :  Kumar
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package supplier.controller;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;

import common.pojo.AccountType;
import common.pojo.PlanInfo;
import common.pojo.State;

/**
 * AccountPaymentSettingController is a controller class for view plan details, Account type Information.
 * 
 * @author Created by SPAN.
 */
@Controller
public class AccountPaymentSettingMfgController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AccountPaymentSettingMfgController.class);
	
	/**
	 * This controller method will get Account type information from
	 * service layer based on input parameter.
	 * 
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param model as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/accountPaymentMfg.htm", method = RequestMethod.GET)
	public final ModelAndView showAccountPaymentPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside AccountPaymentSettingMfgController : showAccountPaymentPage ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		
		ArrayList<State> statesList = null;
		ArrayList<AccountType> accountTypeList = null;
		
		final PlanInfo  plnInfo =  new PlanInfo();
		plnInfo.setSelectedTab(ApplicationConstants.ACHBNKTAB);
		session.setAttribute("selectedTab", ApplicationConstants.ACHBNKTAB);
		model.addAttribute("accountpaymentsettingform", new PlanInfo());
		try
		{
			statesList = supplierService.getAllStates();
			accountTypeList = supplierService.getAllAcountType();
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		session.setAttribute("statesList", statesList);
		session.setAttribute("accountTypes", accountTypeList);
		return new ModelAndView("AccountPaymentMfg");
	}
	
	/**
	 * This controller method will display type of ACH/Bank or Credit Card Information  details screen.
	 * 
	 * @param planInfo instance of PlanInfo.
	 * @param model as request parameter.
	 * @param request as request parameter.
	 * @param response as request parameter.
	 * @param session as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown 
	 */
	@RequestMapping(value = "/loadPaymentType.htm", method = RequestMethod.POST)
	public final ModelAndView loadPaymentsType(@ModelAttribute("accountpaymentsettingform") PlanInfo planInfo, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside AccountPaymentSettingMfgController : loadPaymentsType ");
		final String selTab = planInfo.getSelectedTab();
		if (null != selTab && selTab.equals(ApplicationConstants.CREDITCARDPAY))
		{
			session.setAttribute("selectedTab", ApplicationConstants.CREDITCARDPAY);
		} else {
			session.setAttribute("selectedTab", ApplicationConstants.ACHBNKTAB);
		}
		return new ModelAndView("AccountPaymentMfg");
	}
}

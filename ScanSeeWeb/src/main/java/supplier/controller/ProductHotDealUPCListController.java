/**
 * @ (#) ProductUPCListController.java 29-Dec-2011
 * Project       :ScanSeeWeb
 * File          : ProductUPCListController.java
 * Author        : Nanda bhat
 * Company       : Span Systems Corporation
 * Date Created  : 29-Dec-2011
 *
 * @author       :  Nanda bhat
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */
package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.HotDealInfo;
import common.pojo.Product;
import common.pojo.Users;

/**
 * ProductHotDealUPCListController is a controller class for supplier product hot deals.
 * 
 * @author Created by SPAN.
 */
@Controller
@RequestMapping("/prodHotDealList.htm")
public class ProductHotDealUPCListController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ProductHotDealUPCListController.class);
	/**
	 * Variable RETURN_VIEW declared as constant string.
	 */
	private static final  String RETURN_VIEW = "prodHotDealList";

	/**
	 * This controller method will display the HotDeals products.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param model  ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public final String showCreateRebatePage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		LOG.info("Inside ProductHotDealUPCListController : showCreateRebatePage");
		session.removeAttribute("hotdealprodupclist");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		//final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		request.getSession().removeAttribute("message");
		final HotDealInfo hotdealInfo = new HotDealInfo();
		session.removeAttribute("PreviewProductID");
		/*
		 * try { hotdealProductList = (ArrayList<HotDealInfo>)
		 * supplierService.getDealsForDisplay(loginUser.getUserID(),null);
		 * if(hotdealProductList !=null && hotdealProductList.size()>0){
		 * session.setAttribute("hotdealprodupclist", hotdealProductList);
		 * }else{ request.getSession().setAttribute("message",
		 * "No Products to display"); } } catch (Exception e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * session.setAttribute("hotdealprodupclist", hotdealProductList);
		 */
		model.put("hotdealprodupclistForm", hotdealInfo);

		LOG.info("showCreateRebatePage:: Inside Exit Get Method");
		return RETURN_VIEW;
	}

	/**
	 * This controller method will display the HotDeals deals by passing HotDealId as input parameter by call service and DAO methods.
	 * 
	 * @param hotdealInfo HotDealInfo instance as request parameter
	 * @param request HttpServletRequest instance.
	 * @param result  BindingResult instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 * @throws IOException will be thrown.
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/prodHotDealList.htm", method = RequestMethod.POST)
	public ModelAndView searchResult(@ModelAttribute("hotdealprodupclistForm") HotDealInfo hotdealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside ProductHotDealUPCListController : searchResult");
		List<Product> hotdealProductList = null;
		request.getSession().removeAttribute("hotdealprodupclist");
		request.getSession().removeAttribute("message");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		ArrayList<Product> pdtInfoList = null;
		final String productName = hotdealInfo.getProductName();

		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		try
		{
			hotdealProductList = (List<Product>) supplierService.getProductUPCList(Long.valueOf(loginUser.getSupplierId()), productName);
			pdtInfoList = (ArrayList<Product>) request.getSession().getAttribute("pdtInfoList");
			if (pdtInfoList != null && !pdtInfoList.isEmpty())
			{
				long prodID = 0;
				for (int i = 0; i < pdtInfoList.size(); i++)
				{
					prodID = pdtInfoList.get(i).getProductID();
					for (int j = 0; j < hotdealProductList.size(); j++)
					{
						if (prodID == hotdealProductList.get(j).getProductID())
						{
							hotdealProductList.remove(j);
						}
					}
				}
			}
			if (hotdealProductList != null && !hotdealProductList.isEmpty()) {
				session.setAttribute("hotdealprodupclist", hotdealProductList);
			} else {
				request.getSession().setAttribute("message", "Product not found !!!");
			}
		}
		catch (ScanSeeServiceException e)
		{
			 LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			 throw e;
		}
		return new ModelAndView(RETURN_VIEW);
	}
}

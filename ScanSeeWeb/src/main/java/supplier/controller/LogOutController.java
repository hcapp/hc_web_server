package supplier.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class LogOutController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LogOutController.class);

	@RequestMapping(value = "/logout.htm", method = RequestMethod.GET)
	public ModelAndView logOut(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
	{

		LOG.info("Inside logout method");
		session.invalidate();
		LOG.info("Exiting logout method");
		// return "welcome";
		return new ModelAndView(new RedirectView("http://www.hubcitiapp.com"));

	}

	@RequestMapping(value = "/sessionTimeout.htm", method = RequestMethod.GET)
	public String SessionTimeOut(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
	{
		LOG.info("Session Time-out");
		return "sessionTimeOut";
	}
}

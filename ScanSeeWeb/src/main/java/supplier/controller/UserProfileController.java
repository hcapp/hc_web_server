package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;
import supplier.validator.ProfileDataValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.State;
import common.pojo.SupplierRegistrationInfo;
import common.util.Utility;

/**
 * UserProfileController is a controller class for  supplier registered screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class UserProfileController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserProfileController.class);

	/**
	 * Variable objProfileDataVal declared as instance of ProfileDataValidator.
	 */
	private ProfileDataValidator profileDataValidator;
	
	/**
	 * Variable LOGIN_URL declared as String.
	 */
	private final String loginURL = "login.htm";
	
	/**
	 * Variable DUPLICATE_USER_VIEW declared as String.
	 */
	private final String duplicateUserView = "createProfile";

	/**
	 * To set profileDataValidator.
	 * 
	 * @param profileDataValidator to set.
	 */
	@Autowired
	public final void setProfileDataValidator(ProfileDataValidator profileDataValidator)
	{
		this.profileDataValidator = profileDataValidator;
	}

	
	/**
	 * This controller method will display supplier Registration screen.
	 * 
	 * @param supplierRegistration instance of SupplierRegistrationInfo.
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param session1 as request parameter.
	 * @param model as request parameter.
	 * @param result as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException will be thrown. 
	 */
	@RequestMapping(value = "/createProfile.htm", method = RequestMethod.GET)
	public final ModelAndView showPage(@ModelAttribute("createprofileform") SupplierRegistrationInfo supplierRegistration, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		LOG.info("Inside UserProfileController : showPage ");
		final SupplierRegistrationInfo supplierRegistrationInfo = new SupplierRegistrationInfo();
		model.put("createprofileform", supplierRegistrationInfo);
		ArrayList<State> states = null;
		ArrayList<Category> arBCategoryList = null;
		/*
		 * ArrayList<AppConfiguration> appConfig = null; String
		 * reCaptchaPrivateKey = null; String reCaptchaPublicKey = null;
		 */
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			/*
			 * reCaptchaPrivateKey = (String)
			 * session.getAttribute("reCaptchaPrivateKey"); reCaptchaPublicKey =
			 * (String) session.getAttribute("reCaptchaPublicKey"); if (null ==
			 * reCaptchaPrivateKey || null == reCaptchaPublicKey) { appConfig =
			 * supplierService.getAppConfig(ApplicationConstants.RECAPTCHA); if
			 * (null != appConfig) { for (int j = 0; j < appConfig.size(); j++)
			 * { if
			 * (appConfig.get(j).getScreenName().equals(ApplicationConstants
			 * .RECAPTCHAPRIVATEKEY)) { reCaptchaPrivateKey =
			 * appConfig.get(j).getScreenContent(); } if
			 * (appConfig.get(j).getScreenName
			 * ().equals(ApplicationConstants.RECAPTCHAPUBLICKEY)) {
			 * reCaptchaPublicKey = appConfig.get(j).getScreenContent(); } } }
			 * session.setAttribute("reCaptchaPrivateKey", reCaptchaPrivateKey);
			 * session.setAttribute("reCaptchaPublicKey", reCaptchaPublicKey); }
			 */
			//states = supplierService.getAllStates();
			arBCategoryList = supplierService.getAllBusinessCategory();
			//session.setAttribute("statesListCreat", states);
			session.setAttribute("categoryList", arBCategoryList);
			// this is for to remove How it works, news, retailers….programs in
			// header
			final boolean supregistration = false;
			session.setAttribute("supplieReg", supregistration);
		} catch (ScanSeeServiceException e) {
			profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.ERROR_FETCH);
			if (result.hasErrors()) {
				return new ModelAndView(duplicateUserView);
			}
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(duplicateUserView);
	}

	/**
	 * This controller method save supplier register details  by calling service method.
	 * 
	 * @param supplierRegistrationInfo instance of SupplierRegistrationInfo.
	 * @param result as request parameter.
	 * @param model as request parameter.
	 * @param request as request parameter.
	 * @param response as request parameter.
	 * @param session as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException will be thrown.  
	 * @throws IOException will be thrown. 
	 */
	@RequestMapping(value = "/createProfile.htm", method = RequestMethod.POST)
	public final ModelAndView createProfile(@ModelAttribute("createprofileform") SupplierRegistrationInfo supplierRegistrationInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside UserProfileController : createProfile ");
		String isProfileCreated = "";
		String returnView = null;
		String state = null;
		String city = null;
		String emailId = null;
		//String retypeEmailId = null;

		boolean acceptTerm = false;
		boolean isValidEmail = true;
		boolean isValidCorpPhoneNum = true;
		boolean isValidContactNum = true;

		String strCategory = null;
		// String captchResponse = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			final String strLogoImage = supplierService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
			emailId = supplierRegistrationInfo.getContactEmail();
			//retypeEmailId = supplierRegistrationInfo.getRetypeEmail();
			supplierRegistrationInfo.setState(supplierRegistrationInfo.getStateCodeHidden());
			state = supplierRegistrationInfo.getState();
			city = supplierRegistrationInfo.getCity();
			supplierRegistrationInfo.setCityHidden(supplierRegistrationInfo.getCity());
			strCategory = supplierRegistrationInfo.getbCategory();
			supplierRegistrationInfo.setbCategoryHidden(supplierRegistrationInfo.getbCategory());
			session.setAttribute("retailerCity", city);
			acceptTerm = supplierRegistrationInfo.isAcceptTerm();

			isValidEmail = Utility.validateEmailId(emailId);
			isValidCorpPhoneNum = Utility.validatePhoneNum(supplierRegistrationInfo.getCorpPhoneNo());
			isValidContactNum = Utility.validatePhoneNum(supplierRegistrationInfo.getContactPhoneNo());

			/*
			 * String remoteAddr = request.getRemoteAddr(); String
			 * challengeField =
			 * request.getParameter("recaptcha_challenge_field"); String
			 * responseField = request.getParameter("recaptcha_response_field");
			 * supplierRegistrationInfo.setCaptch(responseField); String
			 * reCaptchaPrivateKey = (String)
			 * session.getAttribute("reCaptchaPrivateKey"); captchResponse =
			 * Utility.validateCaptcha(challengeField, responseField,
			 * remoteAddr, reCaptchaPrivateKey);
			 */

			profileDataValidator.validate(supplierRegistrationInfo, result);
			if (state.equalsIgnoreCase("--Select--") || state.equalsIgnoreCase("0"))
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.STATE);
			}
			
			if (null == city || city.isEmpty())
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.CITY);
			}
			if (!acceptTerm)
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.TERM);
			}
			if (null == strCategory || "".equals(strCategory))
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.BUSINESSCATEGORY);
			}
			if (result.hasErrors())
			{
				returnView = duplicateUserView;
				/*
				 * FieldError fieldcaptch = result.getFieldError("captch"); if
				 * (fieldcaptch == null) { if (captchResponse ==
				 * ApplicationConstants.FAILURE) {
				 * profileDataValidator.validate(supplierRegistrationInfo,
				 * result, ApplicationConstants.INVALID_CAPTCHA); } }
				 */
				return new ModelAndView(returnView);
			}
			if (!isValidCorpPhoneNum)
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.INVALIDCORPPHONE);
				// returnView = DUPLICATE_USER_VIEW;
				// return new ModelAndView(returnView);
			}
			if (!isValidContactNum)
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.INVALIDCONTPHONE);
				// returnView = DUPLICATE_USER_VIEW;
				// return new ModelAndView(returnView);
			}
			if (!isValidEmail)
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.INVALIDEMAIL);
				// returnView = DUPLICATE_USER_VIEW;
				// return new ModelAndView(returnView);
			}
			/*if (captchResponse == ApplicationConstants.FAILURE)
			{

				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.INVALID_CAPTCHA);
			}*/
			/*
			 * else if (!(emailId.equals(retypeEmailId))) {
			 * profileDataValidator.validate(supplierRegistrationInfo, result,
			 * ApplicationConstants.EMAIL); if (result.hasErrors()) { returnView
			 * = DUPLICATE_USER_VIEW; } return new ModelAndView(returnView); }
			 */

			if (result.hasErrors())
			{
				return new ModelAndView(duplicateUserView);
			}
			isProfileCreated = supplierService.insertUserProfileDetails(supplierRegistrationInfo, strLogoImage);
			if (isProfileCreated != null && isProfileCreated.equals(ApplicationConstants.SUCCESS))
			{
				returnView = loginURL;
				final String s = "1";
				session.setAttribute("registration", s);
			}

			else if (isProfileCreated != null && isProfileCreated.equals(ApplicationConstants.DUPLICATE_USER))
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.DUPLICATE_USER);
				if (result.hasErrors())
				{
					returnView = duplicateUserView;
				}
			}
			else if (isProfileCreated != null && isProfileCreated.equals(ApplicationConstants.DUPLICATE_SUPPLIERNAME))
			{
				profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.DUPLICATE_SUPPLIERNAME);
				if (result.hasErrors())
				{
					returnView = duplicateUserView;
				}
			}
			else if (isProfileCreated != null && isProfileCreated.equals(ApplicationConstants.FAILURE)) 
			{ 
				LOG.info("Inside UserProfileController : createProfile  : Failure or Problem Occured ");
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			profileDataValidator.validate(supplierRegistrationInfo, result, ApplicationConstants.ERROR_SAVE);
			if (result.hasErrors())
			{
				return new ModelAndView(duplicateUserView);
			}
		}
		if (returnView == loginURL) {
			session.removeAttribute("supplierCity");
			return new ModelAndView(new RedirectView(returnView));
		} else {
			return new ModelAndView(returnView);
		}
	}

/*	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView editProfile(@ModelAttribute("editprofileform") SupplierRegistrationInfo supplierRegistrationInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		
		 * SupplierService supplierService = (SupplierService) appContext
		 * .getBean(ApplicationConstants.SUPPLIERSERVICE); String view = null; String
		 * isProfileCreated = ""; isProfileCreated =
		 * supplierService.insertUserProfileDetails(supplierRegistrationInfo) ;
		 * if(isProfileCreated != null && isProfileCreated.equals("success")){
		 * view = "createProfile"; }
		 * profileDataValidator.validate(supplierRegistrationInfo, result,
		 * isProfileCreated); if (result.hasErrors()) {
		 * request.setAttribute("message", "Unable to Create profile");
		 * LOG.info("Failureeeeeeeeeeeeee"); view = "createProfile"; }
		 * else { request.setAttribute("message",
		 * "The Profile is successfully created");
		 * LOG.info("SUCCCCCCCCCES"); view = "createProfile"; }
		 
		supplierRegistrationInfo.setSupplierName("FirstRegnUser");
		supplierRegistrationInfo.setCorpAdderess("Street 1,Cross 2");
		supplierRegistrationInfo.setAddress2("Adjacent to Wallmart");
		supplierRegistrationInfo.setState("Florida");
		supplierRegistrationInfo.setPostalCode("2EF6TH");
		supplierRegistrationInfo.setContactPhoneNo("063456789");
		request.setAttribute("supplierRegistrationInfo", supplierRegistrationInfo);
		String view = "editProfile";

		return new ModelAndView(view);
	}*/
}

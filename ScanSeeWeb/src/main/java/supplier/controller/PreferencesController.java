package supplier.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import common.exception.ScanSeeServiceException;
import common.pojo.LoginVO;

@Controller
@RequestMapping("/preferences.htm")
public class PreferencesController {

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(PreferencesController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showPage(HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1) throws ScanSeeServiceException{

		LOG.info("Controller Layer:: Inside Get Method");
		// LoginVO loginVO = new LoginVO();
		// model.put("loginform", loginVO);
		LOG.info("Controller Layer:: Exit Get Method");
		return "shopperPreferences";
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView searchResult(
			@ModelAttribute("loginform") LoginVO loginVO, BindingResult result,
			HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws IOException , ScanSeeServiceException {

		return new ModelAndView("shopperPreferences");
	}
}

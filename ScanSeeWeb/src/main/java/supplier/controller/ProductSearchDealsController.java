/**
 * @ (#) ProductUPCListController.java 26-Dec-2011
 * Project       :ScanSeeWeb
 * File          : ProductUPCListController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 26-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Product;
import common.pojo.Users;

@Controller
@RequestMapping("/prodSuppList.htm")
public class ProductSearchDealsController {

	/**
	 * Getting the logger Instance.
	 */
	final public String RETURN_VIEW = "prodSuppList";
	private static final Logger LOG = LoggerFactory
			.getLogger(ProductSearchDealsController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showCreateCouponPage(HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException{

		LOG.info("showCreateCouponPage:: Inside Get Method");
		session.removeAttribute("manageproductlist");
		Product product = new Product();
		model.put("prodsupplierform", product);
		LOG.info("showCreateCouponPage:: Inside Exit Get Method");
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/prodSuppList.htm", method = RequestMethod.POST)
	public ModelAndView searchResult(
			@ModelAttribute("produpclistform") Product productInfo,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException ,ScanSeeServiceException {
		ArrayList<Product> manageProductList = null;
		final ServletContext servletContext = request.getSession()
				.getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext
				.getBean(ApplicationConstants.SUPPLIERSERVICE);

		String productName = productInfo.getProductName();
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		Long userId = user.getUserID();

		/*
		 * manageProductValidator.validateManageProduct(manageProducts, result);
		 * if (result.hasErrors()) {
		 * 
		 * return new ModelAndView("manageProduct"); }
		 */

		// Users loginUser = (Users)
		// request.getSession().getAttribute(ApplicationConstants.LOGINUSER);

		// int manufacturerID = loginUser.getSupplierId();
		// int manufacturerID = loginUser.getSupplierId();
		// LOG.info("manufacturerID*************"+manufacturerID);

	/*	try {
			manageProductList = (ArrayList<Product>) supplierService.getProductAssociatedto(productName,userId);
		} catch (ScanSeeServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
		session.setAttribute("manageproductlist", manageProductList);

		return new ModelAndView(RETURN_VIEW);
	}
}

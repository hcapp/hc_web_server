package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;
import supplier.validator.ProfileDataValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.State;
import common.pojo.SupplierProfile;
import common.pojo.Users;
import common.util.Utility;

/**
 * EditUserProfileController is a controller class for edit supplier registered
 * screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class EditUserProfileController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditUserProfileController.class);
	/**
	 * Variable objProfileDataVal declared as instance of ProfileDataValidator.
	 */
	private ProfileDataValidator objProfileDataVal;
	/**
	 * Variable viewName declared as constant string.
	 */
	private final String viewName = "editProfile";

	/**
	 * To set objProfileDataVal.
	 * 
	 * @param objProfileDataVal
	 *            to set.
	 */
	@Autowired
	public void setProfileDataValidator(ProfileDataValidator objProfileDataVal)
	{
		this.objProfileDataVal = objProfileDataVal;
	}

	/**
	 * This controller method will get user UpdateProfile record from service
	 * based on parameter(userID).
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param session1
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/editProfile.htm", method = RequestMethod.GET)
	public final String showPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{
		LOG.info("Inside EditUserProfileController : showPage ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		ArrayList<State> states = null;
		ArrayList<Category> arBCategoryList = null;
		try
		{

			// states = supplierService.getAllStates();
			arBCategoryList = supplierService.getAllBusinessCategory();
			// session.setAttribute("statesList", states);
			session.setAttribute("categoryList", arBCategoryList);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		/*
		 * for (int i = 0; i < states.size(); i++) {
		 * mapStates.put(states.get(i).getStateabbr(), states.get(i)
		 * .getStateName()); }
		 */

		final SupplierProfile profileInfo1 = new SupplierProfile();
		final List<SupplierProfile> profileInfoList = (List<SupplierProfile>) supplierService.getUpdateProfile(Long.valueOf(user.getSupplierId()));
		if (profileInfoList != null && !profileInfoList.isEmpty())
		{
			for (SupplierProfile profile : profileInfoList)
			{
				profileInfo1.setCompanyName(profile.getCompanyName());
				profileInfo1.setCorporateAddress(profile.getCorporateAddress());
				profileInfo1.setAddress(profile.getAddress());
				profileInfo1.setState(profile.getState());
				states = supplierService.getState(profile.getState());
				for (State state : states)
				{
					profileInfo1.setStateHidden(state.getStateName());
				}
				profileInfo1.setStateCodeHidden(profile.getState());
				profileInfo1.setCity(profile.getCity());
				profileInfo1.setCityHidden(profile.getCity());
				profileInfo1.setPostalCode(profile.getPostalCode());
				profileInfo1.setPhone(Utility.getPhoneFormate(profile.getPhone()));
				profileInfo1.setFirstName(profile.getFirstName());
				profileInfo1.setLastName(profile.getLastName());
				profileInfo1.setContactFName(profile.getContactFName());
				profileInfo1.setContactLName(profile.getContactLName());
				profileInfo1.setContactPhone(Utility.getPhoneFormate(profile.getContactPhone()));
				profileInfo1.setEmail(profile.getEmail());
				profileInfo1.setNonProfit(profile.isNonProfit());
				profileInfo1.setbCategory(profile.getbCategory());
				profileInfo1.setbCategoryHidden(profile.getbCategory());

				profileInfo1.setCorporateAddress(profileInfo1.getCorporateAddress() != null ? profileInfo1.getCorporateAddress().trim() : null);
				profileInfo1.setAddress(profileInfo1.getAddress() != null ? profileInfo1.getAddress().trim() : null);
			}
			model.put("editprofileform", profileInfo1);
		}
		else
		{
			model.put("editprofileform", profileInfo1);
		}
		return viewName;
	}

	/**
	 * This service method will return the City List based on input parameter.
	 * 
	 * @param stateCode
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param city
	 *            as request parameter.
	 * @param tabIndex
	 *            as request parameter.
	 * @return City List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/fetchcity", method = RequestMethod.GET)
	@ResponseBody
	public final String getCities(@RequestParam(value = "statecode", required = true) String stateCode,
			@RequestParam(value = "tabIndex", required = true) String tabIndex, @RequestParam(value = "city", required = true) String city,
			HttpServletRequest request, HttpServletResponse response) throws ScanSeeServiceException
	{
		LOG.info("Inside EditUserProfileController : getCities ");
		ArrayList<City> cities = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='city' tabindex= " + tabIndex
				+ " id='City'  onchange='getCityTrigger(this);'><option value='0'>--Select--</option>";
		innerHtml.append(finalHtml);
		String cityName = null;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			cities = supplierService.getAllCities(stateCode);
			response.setContentType("text/xml");
			String selCity = (String) request.getSession().getAttribute("retailerCity");
			if (selCity == null || "".equals(selCity))
			{
				selCity = city;
			}
			for (int i = 0; i < cities.size(); i++)
			{
				cityName = cities.get(i).getCityName();
				if (null != selCity && cityName.equals(selCity))
				{
					innerHtml.append("<option selected=true >" + cities.get(i).getCityName() + "</option>");
				}
				else
				{
					innerHtml.append("<option>" + cities.get(i).getCityName() + "</option>");
				}
			}
			innerHtml.append("</select>");
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return innerHtml.toString();
	}

	/*
	 * @ModelAttribute("statesList") public ArrayList<State>
	 * findAllStates(HttpServletRequest request, HttpServletResponse
	 * response,HttpSession session) { ArrayList<State> states = null;
	 * HashMap<String, String> mapStates = new HashMap<String, String>(); try {
	 * ServletContext servletContext = request.getSession()
	 * .getServletContext(); WebApplicationContext appContext =
	 * WebApplicationContextUtils .getWebApplicationContext(servletContext);
	 * SupplierService supplierService = (SupplierService) appContext
	 * .getBean(ApplicationConstants.SUPPLIERSERVICE); states =
	 * supplierService.getAllStates(); for (int i = 0; i < states.size(); i++) {
	 * mapStates.put(states.get(i).getStateabbr(), states.get(i)
	 * .getStateName()); } session.setAttribute("statesList", states); } catch
	 * (ScanSeeServiceException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } return states; }
	 */

	/**
	 * This controller method Edit supplier register details by calling service
	 * method.
	 * 
	 * @param supplierProfile
	 *            instance of SupplierProfile.
	 * @param result
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/editProfile.htm", method = RequestMethod.POST)
	public final ModelAndView updateProfile(@ModelAttribute("editprofileform") SupplierProfile supplierProfile, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside EditUserProfileController : UpdateProfile ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		supplierProfile.setSupProfileID(Long.valueOf(user.getSupplierId()));
		supplierProfile.setUserID(user.getUserID());
		String isProfileUpdated = "";
		// final String view = "editProfile";
		String state = null;
		String city = null;
		boolean isValidCorpPhoneNum = true;
		boolean isValidEmail = true;
		boolean isValidContactNum = true;
		// String returnView = null;
		final String strBCategory = supplierProfile.getbCategory();
		supplierProfile.setState(supplierProfile.getStateCodeHidden());
		state = supplierProfile.getState();
		city = supplierProfile.getCity();
		isValidEmail = Utility.validateEmailId(supplierProfile.getEmail());
		isValidCorpPhoneNum = Utility.validatePhoneNum(supplierProfile.getPhone());
		isValidContactNum = Utility.validatePhoneNum(supplierProfile.getContactPhone());

		objProfileDataVal.validateEdit(supplierProfile, result);
		// if (null == state || state.equalsIgnoreCase(""))
		// {
		// objProfileDataVal.validateEdit(supplierProfile, result,
		// ApplicationConstants.STATE);
		// }
		if (null == city || city.equalsIgnoreCase(""))
		{
			objProfileDataVal.validateEdit(supplierProfile, result, ApplicationConstants.CITY);
		}
		if (!isValidCorpPhoneNum)
		{
			objProfileDataVal.validateEdit(supplierProfile, result, ApplicationConstants.INVALIDCORPPHONE);
			// returnView = viewName;
			// return new ModelAndView(returnView);
		}
		if (!isValidContactNum)
		{
			objProfileDataVal.validateEdit(supplierProfile, result, ApplicationConstants.INVALIDCONTPHONE);
			// returnView = viewName;
		}
		if (!isValidEmail)
		{
			objProfileDataVal.validateEdit(supplierProfile, result, ApplicationConstants.INVALIDEMAIL);
			// returnView = viewName;
		}
		if (null == strBCategory)
		{
			supplierProfile.setbCategoryHidden(null);
			objProfileDataVal.validateEdit(supplierProfile, result, ApplicationConstants.BUSINESSCATEGORY);
		}
		if (result.hasErrors())
		{
			return new ModelAndView(viewName);
		}
		else
		{
			isProfileUpdated = supplierService.updateProfile(supplierProfile);
			if (isProfileUpdated != null && isProfileUpdated.equalsIgnoreCase("success"))
			{
				session.removeAttribute("supplierCity");
				request.setAttribute("message", "User Profile updated successfully");
				return new ModelAndView(new RedirectView("dashboard.htm"));
			}
			else if (isProfileUpdated != null && isProfileUpdated.equals(ApplicationConstants.DUPLICATE_SUPPLIERNAME))
			{
				objProfileDataVal.validate(supplierProfile, result, ApplicationConstants.DUPLICATE_SUPPLIERNAME);
			}
			else if (isProfileUpdated != null && isProfileUpdated.equals(ApplicationConstants.FAILURE))
			{
				LOG.info("Inside UserProfileController : createProfile  : Failure or Problem Occured ");
			}
			else
			{
				request.setAttribute("message", "Error occured while Updating User Profile, Contact the Administrator");
			}
		}
		return new ModelAndView(viewName);
	}
}

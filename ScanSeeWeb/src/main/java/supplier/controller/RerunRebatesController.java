package supplier.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;
import supplier.validator.RebatesValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Rebates;
import common.pojo.Retailer;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class RerunRebatesController 
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RerunRebatesController.class);

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "rerunRebate";
	
	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();

	/**
	 * Variable VIEW_NAME declared as String.
	 */
	String view = VIEW_NAME;
	/**
	 * Variable rebatesValidator declared as instance of ReabtesValidator.
	 */
	RebatesValidator rebatesValidator;

	/**
	 * To set rebatesValidator.
	 * 
	 * @param rebatesValidator
	 *            To set
	 */
	@Autowired
	public void setRebatesValidator(RebatesValidator rebatesValidator)
	{
		this.rebatesValidator = rebatesValidator;
	}

	/**
	 * The getRetailer method is to get retailer locations based on retailer id.
	 * @param retailerId
	 * @param locationId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/fetchretailer.htm", method = RequestMethod.GET)
	public @ResponseBody
	String getRetailer(@RequestParam(value = "retailerId", required = true) int retailerId,@RequestParam(value = "locationId", required = true) String locationId,HttpServletRequest request, HttpServletResponse response) throws ScanSeeServiceException 
	{
		final String methodName = "getRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<Retailer> retailerLoc = null;
		StringBuffer innerHtml = new StringBuffer();
		String finalHtml = "<select name='retailerLocID' id='retailerLocID'  onchange='getLocationTrigger(this);'><option value='0'>--Select--</option>";
		innerHtml.append(finalHtml);

		try {

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			if(ISDUBUGENABLED)
			{
				LOG.debug("fetching Retail locations for Retailerid {}",retailerId);
			}
			retailerLoc = supplierService.getRetailerLoc(retailerId);
			response.setContentType("text/xml");
			for (int i = 0; i < retailerLoc.size(); i++) 
			{
				StringBuffer value = new StringBuffer();
				if (retailerLoc.get(i).getAddress1() != null) 
				{
					value.append(retailerLoc.get(i).getAddress1());
					value.append(", ");
					if (retailerLoc.get(i).getCity() != null)
					{
						value.append(retailerLoc.get(i).getCity());
						value.append(", ");
						if (retailerLoc.get(i).getState() != null) 
						{
							value.append(retailerLoc.get(i).getState());
							value.append(", ");
							if (retailerLoc.get(i).getPostalCode() != null) 
							{
								value.append(retailerLoc.get(i).getPostalCode());
								value.append(", ");
							}
						}
					}
				}
				if (Integer.valueOf(locationId) == retailerLoc.get(i)
						.getRetailLocationID()) 
				{
					innerHtml.append("<option value=\""
							+ retailerLoc.get(i).getRetailLocationID()
							+ "\" selected =true >" + value.toString()
							+ "</option>");
				} else 
				{
					innerHtml.append("<option value=\""
							+ retailerLoc.get(i).getRetailLocationID() + "\">"
							+ value.toString() + "</option>");
				}
			}

			innerHtml.append("</select>");

		} catch (ScanSeeServiceException e) 
		{
			LOG.error("Exception occurred in getRetailer:::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}
		if(ISDUBUGENABLED)
		{
			LOG.debug("Retailer location response {}",innerHtml.toString());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	
	/**
	 * 
	 * @param rebatesVo
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 * @throws ParseException
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/rerunrebate.htm", method = RequestMethod.GET)
	public String showPage(@ModelAttribute("displayrebatesform") Rebates rebatesVo,HttpServletRequest request, HttpSession session, ModelMap model,HttpSession session1) throws ScanSeeServiceException
	{
		LOG.info("Controller Layer:: Inside Get Method");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

		String rebateStartTime = null;
		String rebateEndTime = null;

		String[] tempStartTimeHrsMin = null;
		String[] tempEndTimeHrsMin = null;
		session.removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		request.getSession().removeAttribute("pdtInfoList");
	try{
		LOG.info("rebateId" +rebatesVo.getRebateID());
		List<Rebates> rebatesList = (List<Rebates>) supplierService.getRebatesForDisplay(rebatesVo.getRebateID());
		if (rebatesList != null && rebatesList.size() > 0) 
		{
			for (int i = 0; i < rebatesList.size(); i++) 
			{
				rebatesVo.setRebName(rebatesList.get(i).getRebName());
				String ammount = rebatesList.get(i).getRebAmount();
				ammount = Utility.formatDecimalValue(ammount);
				rebatesVo.setRebAmount(ammount);
				
				rebatesVo.setRebDescription(rebatesList.get(i)
						.getRebShortDescription());
				String sDate = rebatesList.get(i).getRebStartDate();
				if (sDate != null) 
				{
					sDate = Utility.formattedDate(sDate);
					rebatesVo.setRebStartDate(sDate);
				}
				String eDate = rebatesList.get(i).getRebEndDate();
				if (eDate != null)
				{
					eDate = Utility.formattedDate(eDate);
					rebatesVo.setRebEndDate(eDate);
				}
				rebateStartTime = rebatesList.get(i).getRebStartTime();
				
				tempStartTimeHrsMin = rebateStartTime.split(":");
				rebatesVo.setRebStartHrs(tempStartTimeHrsMin[0]);
				rebatesVo.setRebStartMins(tempStartTimeHrsMin[1]);
				rebateEndTime = rebatesList.get(i).getRebEndTime();
				tempEndTimeHrsMin = rebateEndTime.split(":");
				rebatesVo.setRebEndhrs(tempEndTimeHrsMin[0]);
				rebatesVo.setRebEndMins(tempEndTimeHrsMin[1]);
				rebatesVo.setRebateTimeZoneId(rebatesList.get(i).getRebateTimeZoneId());
				rebatesVo.setRebTermCondtn(rebatesList.get(i).getRebTermCondtn());
				rebatesVo.setRetailID(Long.valueOf(rebatesList.get(i).getRetailerID()));
				rebatesVo.setRebateTimeZoneId(rebatesList.get(i).getRebateTimeZoneId());
				rebatesVo.setRetailerLocID(rebatesList.get(i).getRetailerLocID());
				rebatesVo.setRetailerLocationID(rebatesList.get(i).getRetailerLocID());
				rebatesVo.setNoOfrebatesIsued(rebatesList.get(i).getNoOfrebatesIsued());
				//rebatesVo.setNoOfRebatesUsed(rebatesList.get(i).getNoOfRebatesUsed());
				rebatesVo.setRebateTimeZoneId(rebatesList.get(i).getRebateTimeZoneId());
				rebatesVo.setRebproductId(rebatesList.get(i).getProductID());
				if (!Utility.checkNull(String.valueOf(rebatesList.get(i).getRetailerLocID())).equals(""))
				{
					session.setAttribute("retailerLocHidden",String.valueOf(rebatesList.get(i).getRetailerLocID()));
				}
				
				if (!Utility.checkNull(String.valueOf(rebatesList.get(i).getRetailID())).equals(""))
				{
					session.setAttribute("retailerIdHidden",String.valueOf(rebatesList.get(i).getRetailID()));
				}
			}
			ArrayList<Retailer> retailers = null;
			retailers = supplierService.getRetailers();
			session.setAttribute("retailerList", retailers);
			ArrayList<TimeZones> timeZonelst=null;
			timeZonelst=supplierService.getAllTimeZones();
			session.setAttribute("RebateTimeZoneslst", timeZonelst);
			model.put("reRunRebatesForm", rebatesVo);
		} else 
		{
			model.put("reRunRebatesForm", rebatesVo);
	    }
	   }catch(ParseException exception)
		{
			LOG.error("Exception occurred::", exception.getMessage());
	    }catch(ScanSeeServiceException e)
		{
	    	LOG.error("Exception occurred in showPage:::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}
		LOG.info("Controller Layer:: Exit Get Method");
		return "rerunRebate";
	}

	
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("RebateStartHrs")
	public Map<String, String> populateRebateStartHrs() throws ScanSeeServiceException
	{
		HashMap<String, String> mapRebateStartHrs = new HashMap<String, String>();

		for (int i = 0; i < 24; i++)
		{
			if (i < 10) 
			{
				mapRebateStartHrs.put("0" + i, "0" + i);
			} else 
			{
				mapRebateStartHrs.put(String.valueOf(i), String.valueOf(i));
			}

		}
		Iterator iterator = mapRebateStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapRebateStartHrs);
		return sortedMap;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException
	{

		List list = new LinkedList(unsortMap.entrySet());

		// sort list based on comparator
		Collections.sort(list, new Comparator() 
		{
			public int compare(Object o1, Object o2) 
			{
				return ((Comparable) ((Map.Entry) (o1)).getValue())
						.compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		// put sorted list into map again
		Map sortedMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) 
		{
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	@SuppressWarnings("rawtypes")
	@ModelAttribute("RebateStartMins")
	public Map<String, String> populatemapRebateStartMins() throws ScanSeeServiceException
	{
		HashMap<String, String> mapRebateStartHrs = new HashMap<String, String>();

		for (int i = 0; i <= 55; i++) 
		{
			if (i < 10) 
			{

				mapRebateStartHrs.put("0" + i, "0" + i);
				i = i + 4;
			} else 
			{
				mapRebateStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}

		}
		@SuppressWarnings("unused")
		Iterator iterator = mapRebateStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapRebateStartHrs);
		return sortedMap;

	}

	/**
	 * The editRebates method is to call service and dao methods.
	 * @param rebatesVo
	 *            Rebates instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return VIEW_NAME editRebates view.
	 */
	@RequestMapping(value = "/rerunrebate.htm", method = RequestMethod.POST)
	public ModelAndView editRebates(@ModelAttribute("reRunRebatesForm") Rebates rebatesVo,BindingResult result, HttpServletRequest request,HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("editRebates:: inside POST Method");
		String compDate = null;
		Date currentDate = new Date();
		session.removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		try 
		{

			String isDataInserted = null;
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

			Users user = (Users) request.getSession().getAttribute("loginuser");
			String productID = rebatesVo.getRebproductId();
			LOG.info("productID**********" + productID);

			/*
			 * String startDate = rebatesVo.getRebStartDate(); String sDate =
			 * Utility.getFormattedDate(startDate);
			 * rebatesVo.setRebStartDate(sDate);
			 * 
			 * String endDate = rebatesVo.getRebEndDate(); String eDate =
			 * Utility.getFormattedDate(endDate);
			 * rebatesVo.setRebEndDate(eDate);
			 */

			String sTime = String.valueOf(rebatesVo.getRebStartHrs()) + ":"
					+ String.valueOf(rebatesVo.getRebStartMins());
			String eTime = String.valueOf(rebatesVo.getRebEndhrs()) + ":"
					+ String.valueOf(rebatesVo.getRebEndMins());

			rebatesVo.setRebStartTime(sTime);
			rebatesVo.setRebEndTime(eTime);

			rebatesVo.setUserID(user.getUserID());
			rebatesVo.setSupplierId(user.getSupplierId());

			rebatesValidator.validateRerun(rebatesVo, result);
			if (!result.hasErrors())
			{
			compDate = Utility.compareCurrentDate(rebatesVo.getRebEndDate(), currentDate);
			if (null != compDate)
			{

				rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEENDCURRENT);
			}
			
			else
			{
				

				compDate = Utility.compareDate(rebatesVo.getRebStartDate(), rebatesVo.getRebEndDate());

				if (null != compDate)
				{

					rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEAFTER);
				}
			}
			}
			

			if (result.hasErrors())
			{
				if (!Utility.checkNull(String.valueOf(rebatesVo.getRetailerLocID())).equals(""))
				{
					session.setAttribute("retailerLocHidden",String.valueOf(rebatesVo.getRetailerLocID()));
				}
				if (!Utility.checkNull(String.valueOf(rebatesVo.getRetailerID())).equals(""))
				{
					session.setAttribute("retailerIdHidden", String.valueOf(rebatesVo.getRetailID()));
				}
				view = VIEW_NAME;

			} else 
			{
				isDataInserted = supplierService.addRerunRebates(rebatesVo);

				if (isDataInserted
						.equalsIgnoreCase(ApplicationConstants.FAILURE)
						|| isDataInserted == null) 
				{
					view = VIEW_NAME;
					result.reject("rerunRebate.error");

				} else 
				{
					view = "rebatesMfg.htm";
					
				}
			}
		} catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in editRebates:::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}

		LOG.info("onSubmitRebatesInfo:: Exiting POST Method");

		if (view.equals(VIEW_NAME)) 
		{
			return new ModelAndView(view);
		} else 
		{
			return new ModelAndView(new RedirectView(view));
		}
	}
	
	@RequestMapping(value = "/previewsupplrerunrebate.htm", method = RequestMethod.POST)
	public ModelAndView previewPage(@ModelAttribute("previewsupplrerunrebform")Rebates rebatesVo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{

		LOG.info("previewPage in RerunRebatesContrller in Supplier:: Inside POST Method");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

		String rebName = request.getParameter("rebName");
		String rebDescription = request.getParameter("rebDescription");
		String productName = request.getParameter("productName");
		List<Rebates> prodList =null;  
		session.removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		request.getSession().setAttribute("reRunRebateName", rebName);
		request.getSession().setAttribute("reRunRebateDescription", rebDescription);
		request.getSession().setAttribute("reRunRebproductName", productName);
        
		rebatesVo.setRebName(rebName);
		rebatesVo.setRebDescription(rebDescription); 
		rebatesVo.setProductName(productName);
		
		if (!Utility.checkNull(String.valueOf(rebatesVo.getRetailerLocID())).equals(""))
		{
			session.setAttribute("retailerLocHidden",String.valueOf(rebatesVo.getRetailerLocID()));
		}
		if (!Utility.checkNull(String.valueOf(rebatesVo.getRetailerID())).equals(""))
		{
			session.setAttribute("retailerIdHidden", String.valueOf(rebatesVo.getRetailID()));
		}
		try{
			prodList=(List<Rebates>)supplierService.fetchProductInformation(rebatesVo.getRebateID());
			
			if(prodList!=null&& !prodList.isEmpty()){
				session.setAttribute("rerunSupProductNames", prodList.get(0).getProductNames());
				session.setAttribute("rerunSupProductImagePath", prodList.get(0).getImagePath());
				}
		}
		catch(ScanSeeServiceException e)
		{
			e.printStackTrace();
		}
		
		
		model.put("previewsupplrerunrebform", rebatesVo);
		LOG.info("previewPage in RerunRebatesContrller :: Exiting POST Method");
		return new ModelAndView("rerunsupplrebpreview");

	}
	
}

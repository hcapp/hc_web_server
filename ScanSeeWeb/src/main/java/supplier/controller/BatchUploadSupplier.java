package supplier.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ManageProducts;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class BatchUploadSupplier
{

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BatchUploadSupplier.class);

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "batchuploadsupplier";

	/**
	 * The showbatchUploadPage method to show addHotDeals view.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance as parameter.
	 * @return VIEW_NAME batchupload view.
	 */
	@RequestMapping(value = "/batchuploadsupplier.htm", method = RequestMethod.GET)
	public String showbatchUploadPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{

		LOG.info("Inside BatchUploadSupplier : showbatchUploadPage ");
		ManageProducts manageProducts = new ManageProducts();
		model.put("batchUploadform", manageProducts);
		return VIEW_NAME;
	}

	/**
	 * The saveBatchUpload controller method is to call service and dao methods.
	 * 
	 * @param dealsVo
	 *            Rebates instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return VIEW_NAME addHotDeals view.
	 */
	@SuppressWarnings("finally")
	@RequestMapping(value = "/savesupplierbatchupload.htm", method = RequestMethod.POST)
	public ModelAndView saveBatchUpload(@ModelAttribute("batchUploadform") ManageProducts manageProducts, BindingResult result,HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap map) throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadSupplier : saveBatchUpload ");
		String isDataInserted = null;
		String isAttributeInserted = null;
		String IsProductDataMoveFromStagingToProductionTable = null;
		String IsAttributesDataMoveFromStagingToProductionTable = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		String strLogoImage = supplierService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING ;
		map.put("batchUploadform", manageProducts);
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int iManufacturerID = loginUser.getSupplierId();
		long userId = loginUser.getUserID();
		String productFileName = manageProducts.getProductuploadFilePath().getOriginalFilename();
		String attributeFileName = manageProducts.getAttributeuploadFile().getOriginalFilename();
		LOG.info("ManufacturerID -->"+iManufacturerID+"\nuserId -->"+ userId);
		
		List <ManageProducts> arMgProductsList = new ArrayList<ManageProducts>();
		String strAttributeSubject = null;
		String strAttributeContent = null;
		String strProductSubject = null;
		String strProductContent = null;
		String strToMail = null;
		boolean bStatus = false;
		String strFname = null;
		String success = null;
		try
		{
			CommonsMultipartFile productuploadFilePath = manageProducts.getProductuploadFilePath();
			long productFileSize = productuploadFilePath.getSize();
			LOG.info("File Size -->"+ productFileSize);
			
			if(productFileSize <= (1024*1024)){
				isDataInserted = supplierService.saveBatchFile(manageProducts, iManufacturerID, userId);
				LOG.info("isDataInserted -->" + isDataInserted);

				IsProductDataMoveFromStagingToProductionTable = supplierService.moveProductDataFromStagingToProduction(manageProducts, iManufacturerID, userId, productFileName,
						attributeFileName);
				LOG.info("IsProductDataMoveFromStagingToProductionTable -->" + IsProductDataMoveFromStagingToProductionTable);
				if (!Utility.checkNull(isDataInserted).equals("") && !Utility.checkNull(IsProductDataMoveFromStagingToProductionTable).equals("")
						&& isDataInserted.equals(ApplicationConstants.SUCCESS) && IsProductDataMoveFromStagingToProductionTable.equals(ApplicationConstants.SUCCESS))
				{
					result.rejectValue("productuploadFilePath", "Product(s) Uploaded Successfully","  Product(s) Uploaded Successfully");
					request.setAttribute("productuploadFile", "font-weight:bold;color:#00559c;");
				}
				else
				{
					arMgProductsList = supplierService.getDiscardedRecordsForProducts(userId,iManufacturerID, manageProducts);
					if(!arMgProductsList.isEmpty()){
						String strFilename = request.getRealPath(iManufacturerID+"_Products.csv");
						Utility.writeDiscardedProductsDataToCSV(arMgProductsList, strFilename);
						strToMail = manageProducts.getContantEmail();
						strFname = manageProducts.getFirstName();
						strProductSubject = ApplicationConstants.SUBJECT_MSG_FOR_PRODUCT_MAILSEND;
						strProductContent = "<Html>Hi " + strFname + ",<br/><br/>" + ApplicationConstants.CONTENT_MSG_FOR_PRODUCT_MAILSEND  
											+ "<br/><br/>Regards,<br/>HubCiti Team<br/>"
			 			 					+ "<img src= "+strLogoImage+" alt=\"scansee logo\"  border=\"0\"></Html>";
						bStatus = supplierService.sendMailForProductBatchUpload(strFilename, strToMail, strProductSubject, strProductContent);
						result.rejectValue("productuploadFilePath", "Product(s) information is not uploaded, please check your mail box for unsuccessful upload Product(s).",
								"Product(s) information is not uploaded, please check your mail box for unsuccessful upload Product(s).");
						request.setAttribute("productuploadFile", "font-weight:bold;color:red;");	
						if(bStatus){
							File f = new File(strFilename);
							bStatus = f.delete();
						}
					}
				}
			}else
			{
				result.rejectValue("productuploadFilePath", "Maximum size of uploaded file should be less than 1MB","  Maximum size of uploaded file should be less than 1MB");
				request.setAttribute("productuploadFile", "font-weight:bold;color:red;");
			}
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.info("Inside BatchUploadSupplier : saveBatchUpload : "+  exception.getMessage());
			result.rejectValue("productuploadFilePath", "Error Occurred while uploading Products","  Error Occurred while uploading Products");
			request.setAttribute("productuploadFile", "font-weight:bold;color:red");
		}
		finally
		{
			supplierService.deleteAllFromProductsStageTable(iManufacturerID, userId);
			try{
				CommonsMultipartFile attributeuploadFile = manageProducts.getAttributeuploadFile();
				long attributeFileSize = attributeuploadFile.getSize();
				LOG.info("File Size -->"+ attributeFileSize);
				//Checking File Size,size should be less than 1MB
				if(attributeFileSize <= (1024*1024))
				{
					isAttributeInserted = supplierService.uploadAttributeFile(manageProducts, iManufacturerID, userId);
					LOG.info("isAttributeInserted -->" + isAttributeInserted);

					IsAttributesDataMoveFromStagingToProductionTable = supplierService.moveAttributesDataFromStagingToProduction(manageProducts, iManufacturerID, userId, productFileName,
							attributeFileName);
					LOG.info("IsAttributesDataMoveFromStagingToProductionTable -->" + IsAttributesDataMoveFromStagingToProductionTable);

					if (!Utility.checkNull(isAttributeInserted).equals("") && !Utility.checkNull(IsAttributesDataMoveFromStagingToProductionTable).equals("")
							&& isAttributeInserted.equals(ApplicationConstants.SUCCESS) && IsAttributesDataMoveFromStagingToProductionTable.equals(ApplicationConstants.SUCCESS))
					{
						result.rejectValue("attributeuploadFile", "Product(s) attributes are uploaded successfully","  Product(s) attributes are uploaded successfully");
						request.setAttribute("attributeupload", "font-weight:bold;color:#00559c;");
					}
					else
					{
						arMgProductsList = supplierService.getDiscardedRecordsForAttributes(userId,  iManufacturerID, manageProducts);
						if(!arMgProductsList.isEmpty()){
							String strFilename = request.getRealPath(iManufacturerID+"_Attributes.csv");
							Utility.writeDiscardedAttributesDataToCSV(arMgProductsList, strFilename);
							strToMail = manageProducts.getContantEmail();
							strAttributeSubject = ApplicationConstants.SUBJECT_MSG_FOR_ATTRIBUTE_MAILSEND;
							strAttributeContent = "<Html>Hi " + strFname + ",<br/><br/>" + ApplicationConstants.CONTENT_MSG_FOR_ATTRIBUTE_MAILSEND  
												  + "<br/><br/>Regards,<br/>HubCiti Team<br/>"
			 					                  + "<img src= "+strLogoImage+" alt=\"scansee logo\"  border=\"0\"></Html>";
							bStatus = supplierService.sendMailForProductBatchUpload(strFilename, strToMail, strAttributeSubject, strAttributeContent);
							result.rejectValue("attributeuploadFile", "Product(s) attributes information is not uploaded, please check your mail box for unsuccessful upload Attribute(s).",
									"Product(s) attributes information is not uploaded, please check your mail box for unsuccessful upload Attribute(s).");
							request.setAttribute("attributeupload", "font-weight:bold;color:red;");	
							if(bStatus){
								File f = new File(strFilename);
								bStatus = f.delete();
							}
						}
					}
				}else{
					result.rejectValue("attributeuploadFile", "Maximum size of uploaded file should be less than 1MB","  Maximum size of uploaded file should be less than 1MB");
					request.setAttribute("attributeupload", "font-weight:bold;color:red;");
				}
			}
			catch (ScanSeeServiceException exception)
			{
				LOG.error("Inside BatchUploadSupplier : saveBatchUpload : finally : "+  exception.getMessage());
				LOG.info("Inside BatchUploadSupplier : saveBatchUpload : finally : "+  exception.getMessage());
				result.rejectValue("attributeuploadFile", "Error Occurred while uploading Products Attributes","  Error Occurred while uploading Products Attributes");
				request.setAttribute("attributeupload", "font-weight:bold;color:red");
			}finally{
				supplierService.deleteAllFromProductAttributesStageTable(iManufacturerID, userId);
			}
			
			try{
				
				StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.SUPPLIER, loginUser.getSupplierId());
				String suppMediaPath = mediaPathBuilder.toString();
				LOG.info(" Images path -->" + suppMediaPath);
				
				CommonsMultipartFile[] imageFile = manageProducts.getImageUploadFilePath();
				if (imageFile.length > 0)
				{
					for (int i = 0; i < imageFile.length; i++)
					{
						String path = suppMediaPath + "/" + imageFile[i].getOriginalFilename();
						success = Utility.writeFileData(imageFile[i], path);
					}
				}
				result.rejectValue("imageUploadFilePath", "Media File(s) are Uploaded Successfully .","  Media File(s) are Uploaded Successfully .");
				request.setAttribute("imageUploadFile", "font-weight:bold;color:#00559c;");
				LOG.info("Image uploaded  -->" + success);
			}
			catch (ScanSeeServiceException exception)
			{
				LOG.info("Inside BatchUploadSupplier : saveBatchUpload : finally : "+  exception.getMessage());
				result.rejectValue("imageUploadFilePath", "Error Occurred while uploading .","  Error Occurred while uploading .");
				request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
			}
			return new ModelAndView(VIEW_NAME);
		}
	}

	@RequestMapping(value = "/uploadproductsupplier", method = RequestMethod.POST)
	public ModelAndView uploadFileSupplier(@ModelAttribute("batchUploadform") ManageProducts manageProducts,BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadSupplier : uploadFileSupplier ");
		String IsDataMoveFromStagingToProductionTable = null;
		String isDataInserted = null;

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		String strLogoImage = supplierService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING ;
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int iManufacturerID = loginUser.getSupplierId();
		long userId = loginUser.getUserID();
		String productFileName = manageProducts.getProductuploadFilePath().getOriginalFilename();
		String attributeFileName = manageProducts.getAttributeuploadFile().getOriginalFilename();
		LOG.info("manufacturerID -->" + iManufacturerID+"\nuserId -->" + userId);
		List <ManageProducts> arMgProductsList = new ArrayList<ManageProducts>();
		String strSubject = null;
		String strContent = null;
		String strToMail = null;
		boolean bStatus = false;
		String strFilename =null;
		String strFname = null;
		try
		{
			CommonsMultipartFile productuploadFilePath = manageProducts.getProductuploadFilePath();
			long productFileSize = productuploadFilePath.getSize();
			LOG.info("File Size -->"+ productFileSize);
			//Checking File Size,size should be less than 1MB
			if(productFileSize <= (1024 * 1024))
			{
				isDataInserted = supplierService.saveBatchFile(manageProducts, iManufacturerID, userId);
				LOG.info("isDataInserted -->" + isDataInserted);
				IsDataMoveFromStagingToProductionTable = supplierService.moveProductDataFromStagingToProduction(manageProducts, iManufacturerID, userId, productFileName,
						attributeFileName);
				if (IsDataMoveFromStagingToProductionTable != null && IsDataMoveFromStagingToProductionTable.equals(ApplicationConstants.SUCCESS)){
					LOG.info("IsDataMoveFromStagingToProductionTable -->" + IsDataMoveFromStagingToProductionTable);
					result.rejectValue("productuploadFilePath", "Product(s) Uploaded Successfully","  Product(s) Uploaded Successfully");
					request.setAttribute("productuploadFile", "font-weight:bold;color:#00559c;");
					//return new ModelAndView(VIEW_NAME);
				}else{
					arMgProductsList = supplierService.getDiscardedRecordsForProducts(userId,  iManufacturerID, manageProducts);
					if(!arMgProductsList.isEmpty()){
						strFilename = request.getRealPath(iManufacturerID+"_Product.csv");
						Utility.writeDiscardedProductsDataToCSV(arMgProductsList, strFilename);
						strToMail = manageProducts.getContantEmail();
						strFname = manageProducts.getFirstName();
						strSubject = ApplicationConstants.SUBJECT_MSG_FOR_PRODUCT_MAILSEND;
						strContent = "<Html>Hi " + strFname + ",<br/><br/>" + ApplicationConstants.CONTENT_MSG_FOR_PRODUCT_MAILSEND  
									 + "<br/><br/>Regards,<br/>HubCiti Team<br/>"
						 			 + "<img src= "+strLogoImage+" alt=\"scansee logo\"  border=\"0\"></Html>";
						strToMail = manageProducts.getContantEmail();
						bStatus = supplierService.sendMailForProductBatchUpload(strFilename, strToMail, strSubject, strContent);
						result.rejectValue("productuploadFilePath", "Product(s) information is not uploaded, please check your mail box for unsuccessful upload Product(s).",
								"Product(s) information is not uploaded, please check your mail box for unsuccessful upload Product(s).");
						request.setAttribute("productuploadFile", "font-weight:bold;color:red;");	
						if(bStatus){
							File f = new File(strFilename);
							bStatus = f.delete();
						}
						//return new ModelAndView(VIEW_NAME);
					}
				}
			}else{
				result.rejectValue("productuploadFilePath", "Maximum size of uploaded file should be less than 1MB","  Maximum size of uploaded file should be less than 1MB");
				request.setAttribute("productuploadFile", "font-weight:bold;color:red;");
			}
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.info("Inside BatchUploadSupplier : uploadFileSupplier :"+exception);
			result.rejectValue("productuploadFilePath", "Error Occurred while uploading Products","  Error Occurred while uploading Products");
			request.setAttribute("productuploadFile", "font-weight:bold;color:red");
			//return new ModelAndView(VIEW_NAME);
		}finally{
			supplierService.deleteAllFromProductsStageTable(iManufacturerID, userId);
			return new ModelAndView(VIEW_NAME);
		}
	}

	@RequestMapping(value = "/uploadaimageesupplier.htm", method = RequestMethod.POST)
	public ModelAndView uploadImageSupplier(@ModelAttribute("batchUploadform") ManageProducts manageProducts,BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadSupplier : uploadImageSupplier ");
		String success = null;
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		try
		{
			StringBuilder mediaPathBuilder = Utility.getMediaPath(ApplicationConstants.SUPPLIER, loginUser.getSupplierId());
			String suppMediaPath = mediaPathBuilder.toString();
			LOG.info(" Images path -->" + suppMediaPath);
			CommonsMultipartFile[] imageFile = manageProducts.getImageUploadFilePath();
			if (imageFile.length > 0)
			{
				for (int i = 0; i < imageFile.length; i++)
				{
					String path = suppMediaPath + "/" + imageFile[i].getOriginalFilename();
					success = Utility.writeFileData(imageFile[i], path);
					LOG.info("==Image Saved to path=="+path);
				}
			}
			request.setAttribute("imageUploadFile", "font-weight:bold;color:#00559c;");
			LOG.info("Image uploaded -->" + success);
		}
		catch (ScanSeeServiceException e)
		{
			
			LOG.info("Inside BatchUploadSupplier : uploadImageSupplier :"+e);
            result.rejectValue("imageUploadFilePath", "Error Occurred while uploading Products","  Error Occurred while uploading Products");
			request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
			return new ModelAndView(VIEW_NAME);
		}
		return new ModelAndView(VIEW_NAME);

	}

	@RequestMapping(value = "/uploadattributesupplier.htm", method = RequestMethod.POST)
	public ModelAndView uploadAttributeSupplier(@ModelAttribute("batchUploadform") ManageProducts manageProducts,BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside BatchUploadSupplier : uploadAttributeSupplier ");
		String IsDataMoveFromStagingToProductionTable = null;
		String isAttributeInserted = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		String strLogoImage = supplierService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING ;
		List <ManageProducts> arMgProductsList = new ArrayList<ManageProducts>();
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int iManufacturerID = loginUser.getSupplierId();
		long userId = loginUser.getUserID();
		String strSubject = null;
		String strContent = null;
		String strToMail = null;
		boolean bStatus = false;
		String strFname = null;
		try
		{
			String productFileName = manageProducts.getProductuploadFilePath().getOriginalFilename();
			String attributeFileName = manageProducts.getAttributeuploadFile().getOriginalFilename();
			LOG.info("manufacturerID -->" + iManufacturerID+"\nuserId -->" + userId);
			CommonsMultipartFile attributeuploadFile = manageProducts.getAttributeuploadFile();
			long attributeFileSize = attributeuploadFile.getSize();
			LOG.info("File Size -->"+ attributeFileSize);
			//Checking File Size,size should be less than 1MB
			if(attributeFileSize <= (1024*1024))
			{
				isAttributeInserted = supplierService.uploadAttributeFile(manageProducts, iManufacturerID, userId);
				LOG.info("isAttributeInserted -->" + isAttributeInserted);
				IsDataMoveFromStagingToProductionTable = supplierService.moveAttributesDataFromStagingToProduction(manageProducts, iManufacturerID, userId, productFileName,
						attributeFileName);

				if (IsDataMoveFromStagingToProductionTable != null && IsDataMoveFromStagingToProductionTable.equals(ApplicationConstants.SUCCESS)){
					LOG.info("IsDataMoveFromStagingToProductionTable -->" + IsDataMoveFromStagingToProductionTable);
					result.rejectValue("attributeuploadFile", "Product(s) attributes are uploaded successfully","  Product(s) attributes are uploaded successfully");
					request.setAttribute("attributeupload", "font-weight:bold;color:#00559c;");
					//return new ModelAndView(VIEW_NAME);
				}else{
					arMgProductsList = supplierService.getDiscardedRecordsForAttributes(userId, iManufacturerID, manageProducts);
					if(!arMgProductsList.isEmpty()){
						String strFilename = request.getRealPath(iManufacturerID+"_Attributes.csv");
						Utility.writeDiscardedAttributesDataToCSV(arMgProductsList, strFilename);
						strToMail = manageProducts.getContantEmail();
						strFname = manageProducts.getFirstName();
						strSubject = ApplicationConstants.SUBJECT_MSG_FOR_ATTRIBUTE_MAILSEND;
						strContent = "<Html>Hi " + strFname + ",<br/><br/>" + ApplicationConstants.CONTENT_MSG_FOR_ATTRIBUTE_MAILSEND 
									 + "<br/><br/>Regards,<br/>HubCiti Team<br/>"
									 + "<img src= "+strLogoImage+" alt=\"scansee logo\"  border=\"0\"></Html>";
						bStatus = supplierService.sendMailForProductBatchUpload(strFilename, strToMail, strSubject, strContent);
						result.rejectValue("attributeuploadFile", "Product(s) Attribute�s information is not uploaded, please check your mail box for unsuccessful upload Products Attributes.",
								"Product(s) Attribute�s information is not uploaded, please check your mail box for unsuccessful upload Products Attributes.");
						request.setAttribute("attributeupload", "font-weight:bold;color:red;");	
						if(bStatus){
							File f = new File(strFilename);
							bStatus = f.delete();
						}
						//return new ModelAndView(VIEW_NAME);
					}
				}
			}else
			{
				result.rejectValue("attributeuploadFile", "Maximum size of uploaded file should be less than 1MB","  Maximum size of uploaded file should be less than 1MB");
				request.setAttribute("attributeupload", "font-weight:bold;color:red;");	
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside BatchUploadSupplier : uploadAttributeSupplier "+ e.getMessage());
            result.rejectValue("attributeuploadFile", "Error Occurred while uploading Products Attributes","  Error Occurred while uploading Products Attributes");
			request.setAttribute("attributeupload", "font-weight:bold;color:red");
			//return new ModelAndView(VIEW_NAME);
		}finally{
			supplierService.deleteAllFromProductAttributesStageTable(iManufacturerID, userId);
			return new ModelAndView(VIEW_NAME);
		}
	}
}

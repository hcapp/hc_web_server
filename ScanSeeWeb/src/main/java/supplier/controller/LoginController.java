package supplier.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import supplier.service.SupplierService;
import supplier.validator.LoginValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.LoginVO;
import common.pojo.RetailerProfile;
import common.pojo.SupplierProfile;
import common.pojo.Users;
import common.util.Utility;

/**
 * LoginController is a controller class for Sign In users screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class LoginController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	/**
	 * Variable loginValidator declared as instance of LoginValidator.
	 */
	private LoginValidator loginValidator;

	/**
	 * Variable isPaymentDone declared as constant string.
	 */
	private final String isPaymentDone = "isPaymentDone";
	/**
	 * Variable viewName declared as constant string.
	 */
	private final String viewName = "login";

	/**
	 * To loginValidator to set.
	 * 
	 * @param loginvalidator
	 *            to set.
	 */
	@Autowired
	public final void setLoginvalidator(LoginValidator loginvalidator)
	{
		this.loginValidator = loginvalidator;
	}

	/**
	 * This controller method will display the Sign-In users details screen.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/login.htm", method = RequestMethod.GET)
	public final ModelAndView showPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside LoginController : showPage");
		final LoginVO loginVO = new LoginVO();
		String signUpURL=null;
		
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		model.put("loginform", loginVO);
		session.removeAttribute("searchForm");
		session.setAttribute("linkTab", ApplicationConstants.LOGIN);
		Users userInfo = null;
		Boolean hasLoggedIn = (Boolean) session.getAttribute("hasLoggedIn");
		if (hasLoggedIn != null && hasLoggedIn)
		{
			userInfo = (Users) session.getAttribute("loginuser");

			if (null != userInfo)
			{

				loginVO.setUserName(userInfo.getUserName());
				model.put("loginform", loginVO);
			}

		}
		
		//Deprecated this implementation 08/10/2016
		/*if(null!=request.getServerName()&&(request.getServerName().equals("66.228.143.27"))||request.getServerName().equals("localhost")){
			signUpURL="http://66.228.143.27:8080/ScanSeeWeb/retailer/createRetailerProfile.htm";
		}else{
			signUpURL="https://www.scansee.net/ScanSeeWeb/retailer/createRetailerProfile.htm";
		}
		session.setAttribute("signUp", signUpURL);
		*/
		
		LOG.info("Controller Layer:: Exit Get Method");
		return new ModelAndView(viewName);
	}

	/**
	 * This controller method display Sign-In users details screen by call
	 * service and DAO methods.
	 * 
	 * @param loginVO
	 *            LoginVO instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	public final ModelAndView login(@ModelAttribute("loginform") LoginVO loginVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside LoginController : login");
		Users userInfo = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		/*
		 * session.removeAttribute("msgResponse");
		 * session.removeAttribute("fontStyle");
		 */
		String view = viewName;
		boolean defaultCase = false;
		String pageForwardValue = null;
		String newpassword = null;
		String cnfrmPswd = null;
		final String viewChangePwd = "changePassword";
		Boolean isValidPassword = true;
		
		try
		{
			/* Checking whether change password steps is done already are not. */
			final String chPwd = loginVO.getChangePwd();
			if (null == chPwd)
			{
				loginValidator.validate(loginVO, result);
				if (result.hasErrors())
				{
					return new ModelAndView(view);
				}
				/* Login the user with user name and Password. */
				userInfo = supplierService.loginAuthentication(loginVO.getUserName(), loginVO.getPassword());
				Boolean isBand = null;
				if(userInfo.getUserType() == 4){
					userInfo.setIsBandRetailer(true);
					isBand = userInfo.getIsBandRetailer();
				}
				// Login is not success
				if (!userInfo.isLoginSuccess())
				{
					result.reject("login.invalid");
					return new ModelAndView(view);
				}
				else
				{
					/*
					 * Meyy related code
					 */
					/* Load Supplier list in session. */
					if (null != userInfo && userInfo.getSupplierId() > 0)
					{
						final List<SupplierProfile> supplierProfileList = supplierService.getUpdateProfile((long) userInfo.getSupplierId());
						if (null != supplierProfileList)
						{
							session.setAttribute("supplierProfileList", supplierProfileList);
						}
					}
					/* Load Retailer list in session. */
					if (null != userInfo && userInfo.getRetailerId() > 0)
					{
						// In hot deal, If more than one product has a valid
						// image any one can be used to associate to the hot
						// deal.
						// If none of the products has images then we can use
						// the Retailer logo image.

						/*
						 * if (!"".equals(Utility.checkNull(userInfo.
						 * getRetailerLogoImage()))) {
						 * session.setAttribute("promotionRetLogoImage",
						 * userInfo.getRetailerLogoImage()); }
						 */
						final RetailerProfile retailerProfile = retailerService.getRetailerProfile(userInfo, isBand);
						if (null != retailerProfile)
						{
							session.setAttribute("retailerProfile", retailerProfile);
						}
					}
					/* Login is success. */
					pageForwardValue = userInfo.getPageForwardValue();
					/* Truncate of username and firstname. */
					final String uname = loginVO.getUserName();
					final String fname = userInfo.getFirstName();
					final String emailId = userInfo.getEmail();
					session.setAttribute("truncUsername", Utility.truncate(uname, 15));
					session.setAttribute("truncFirstName", Utility.truncate(fname, 15));
					session.setAttribute("emailId", emailId);
					userInfo.setUserName(loginVO.getUserName());
					session.setAttribute("loginuser", userInfo);
					
					
					
					/*
					 * check when user type is 1(Consumer) should not go to
					 * change password screen and remaining
					 * 2(Supplier),3(Retailer) should only go change password
					 * screen reset password a flag is used for knowing whether
					 * it is first time change password are not ,from second
					 * time it wont come to change password screen
					 */
					session.setAttribute("hasLoggedIn", new Boolean(true));
					if (!(userInfo.getUserType() == 1) && !userInfo.getResetPassword())
					{
						LOG.info("Going to Change password Page");
						return new ModelAndView(viewChangePwd);
					}
				}
			}
			else
			{
				userInfo = (Users) session.getAttribute("loginuser");
				pageForwardValue = userInfo.getPageForwardValue();
				newpassword = loginVO.getNewPassword();
				cnfrmPswd = loginVO.getConfrmPassword();

				/* Password validation for login change password screen. */
				if (null != newpassword && !"".equals(newpassword))
				{
					/* Password should be minimum 8 characters. */
					if (newpassword.length() < 8)
					{
						loginValidator.validate(loginVO, result, ApplicationConstants.PASSWORDLength);
					}
					/*
					 * Check whether new password and confirm password is same
					 * are not
					 */
					else if (!(newpassword.equals(cnfrmPswd)))
					{
						loginValidator.validate(loginVO, result, ApplicationConstants.PASSWORD);
					}
					else
					{
						/* Checking for valid password */
						isValidPassword = Utility.validatePassword(newpassword);
						if (!isValidPassword)
						{
							loginValidator.validate(loginVO, result, ApplicationConstants.PASSWORDMATCH);
						}
					}
				}
				if (result.hasErrors())
				{
					return new ModelAndView(viewChangePwd);
				}
				/* used for changing and saving the new password in database */
				supplierService.changePassword(userInfo.getUserID(), loginVO.getNewPassword());
			}
			
			/*If Retailer is assocaited with HubCiti through zipcode.*/
			session.setAttribute("eventActive", userInfo.getEventActive());
			
			session.setAttribute(isPaymentDone, null);
			
			userInfo.setIsBandRetailer(false);
            session.setAttribute("ban",userInfo.getIsBandRetailer());
			
			// pageForwardValue = "DASHBOARD";
			switch (userInfo.getUserType())
			{
			case 1:
				LOG.info("Logged in user as a shopper :" + userInfo.getUserName());
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOSHOPPERPREFERENCES))
				{
					view = "/ScanSeeWeb/shopper/preferences.htm";
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOSHOPPERSELECTSCHOOL))
				{
					view = "/ScanSeeWeb/shopper/selectschool.htm";
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOSHOPPERDOUBLECHECK))
				{
					view = "/ScanSeeWeb/shopper/doublecheck.htm";
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.SHOPPERDASHBOARD))
				{
					view = "/ScanSeeWeb/shopper/shopperdashboard.htm";
				}
				break;
			case 2:
				LOG.info("Logged in user as a supplier :" + userInfo.getUserName());
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.SUPPLIERDASHBORAD))
				{
					view = "dashboard.htm";
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOPRODUCTSETUPDONE))
				{
					// view = "registprodsetup.htm"; Commented as per meeting
					// held on 06/19/2012
					view = "choosePlan.htm";
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOCHOOSEPLANSETUPDONE))
				{
					view = "choosePlan.htm";
				}
				break;
			case 3:
				LOG.info("Logged in user as a retailer :" + userInfo.getUserName() + " pageForwardValue : " + pageForwardValue);
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.RETAILERDASHBORAD))
				{
					view = "/ScanSeeWeb/retailer/retailerhome.htm";
					session.setAttribute(isPaymentDone, null);
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NORETAILERLOGO))
				{
					view = "/ScanSeeWeb/retailer/uploadRetailerLogo.htm";
					session.setAttribute(isPaymentDone, "false");
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NORETAILERLOCATION))
				{
					view = "/ScanSeeWeb/retailer/locationsetup.htm";
					session.setAttribute(isPaymentDone, "false");
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NORETAILERCHOOSEPLAN))
				{
					view = "/ScanSeeWeb/retailer/retailerchoosePlan.htm";
					session.setAttribute(isPaymentDone, "false");
				}
				if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NORETAILERPRODUCT))
				{
					// view = "/ScanSeeWeb/retailer/regbatchuploadretprod.htm";
					/*
					 * Changed as per meeting on 06/19/2012 where in retailer
					 * won't land on choose product page.
					 */
					view = "/ScanSeeWeb/retailer/retailerchoosePlan.htm";
					session.setAttribute(isPaymentDone, "false");
				}
				break;
			
			case 4:
				
                LOG.info("Logged in user as a retailer :" + userInfo.getUserName() + " pageForwardValue : " + pageForwardValue);
                
                userInfo.setIsBandRetailer(true);
                session.setAttribute("ban",userInfo.getIsBandRetailer());
                
                if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.BandDASHBOARD))
				{
					view = "/ScanSeeWeb/retailer/retailerhome.htm";
					session.setAttribute(isPaymentDone, null);
				}
                if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOBandLOGO))
				{
					view = "/ScanSeeWeb/retailer/uploadRetailerLogo.htm";
					session.setAttribute(isPaymentDone, "false");
				}
                if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOBandLOCATION))
                {
                       view = "/ScanSeeWeb/retailer/retailerhome.htm";
                       session.setAttribute(isPaymentDone, "false");
                }
                if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOBandCHOOSEPLAN))
				{
					view = "/ScanSeeWeb/retailer/retailerchoosePlan.htm";
					session.setAttribute(isPaymentDone, "false");
				}
                
                break;

				
			default:
				defaultCase = true;
				break;
			}
			LOG.info("Payment Status : " + session.getAttribute(isPaymentDone));
			if (defaultCase)
			{
				return new ModelAndView(new RedirectView(view));
			}
			return new ModelAndView(new RedirectView(view));
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
	}

	/**
	 * This controller method will display the Forgot Password screen.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/forgotpassword.htm", method = RequestMethod.GET)
	public final String showForGotPasswordPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside LoginController : showForGotPasswordPage ");
		final LoginVO loginVO = new LoginVO();
		model.put("loginform", loginVO);
		return viewName;
	}

	/**
	 * This controller method will send the Forgot Password credential through
	 * mail.
	 * 
	 * @param loginVO
	 *            instance of LoginVO.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/forgetPwd.htm", method = RequestMethod.POST)
	public final ModelAndView forgotPassword(@ModelAttribute("loginform") LoginVO loginVO, BindingResult result, HttpServletRequest request,
			HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside LoginController : forgotPassword ");
		String strResponse = null;
		String strUsername = null;
		// String strLogoImage =
		// request.getRealPath(ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING);
		// String strLogoImage =
		// ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;

		final Users objUsers = new Users();
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			final String strLogoImage = supplierService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
			strUsername = request.getParameter("UsrName");
			strResponse = supplierService.forgotPwd(strUsername, objUsers, strLogoImage);
			if (strResponse.equalsIgnoreCase(ApplicationConstants.SUCCESS))
			{
				request.setAttribute("msgResponse", "Check the message sent to you at " + objUsers.getEmail() + " to find out what to do next.");
				request.setAttribute("fontStyle", "font-weight:bold;color:#00559c;");
			}
			else if (strResponse.equalsIgnoreCase(ApplicationConstants.FAILURE))
			{
				request.setAttribute("msgResponse", "Username does not exist.");
				request.setAttribute("fontStyle", "font-weight:bold;color:red");
			}
			else
			{
				request.setAttribute("msgResponse", "Error occurred while send the password.");
				request.setAttribute("fontStyle", "font-weight:bold;color:red");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			request.setAttribute("msgResponse", "Error occurred while send the password.");
			request.setAttribute("fontStyle", "font-weight:bold;color:red");
			return new ModelAndView(viewName);
		}
		return new ModelAndView(viewName);
	}

	/**
	 * This controller method will display Forgot Password screen or Login
	 * Screen based on input parameter.
	 * 
	 * @param objUsers
	 *            instance of Users.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/linkTab.htm", method = RequestMethod.POST)
	public final ModelAndView loadFormTab(@ModelAttribute("loginform") Users objUsers, HttpServletRequest request, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside LoginController : loadFormTab ");
		final String strSelectedLink = objUsers.getLinkTab();
		if (null != strSelectedLink && strSelectedLink.equals(ApplicationConstants.FORGOT_PWD))
		{
			session.setAttribute("linkTab", ApplicationConstants.FORGOT_PWD);
			return new ModelAndView(new RedirectView("forgotpassword.htm"));
		}
		else if (null != strSelectedLink && strSelectedLink.equals(ApplicationConstants.LOGIN))
		{
			session.setAttribute("linkTab", ApplicationConstants.LOGIN);
			return new ModelAndView(new RedirectView("login.htm"));
		}
		else
		{
			session.setAttribute("linkTab", ApplicationConstants.LOGIN);
			return new ModelAndView(new RedirectView("login.htm"));
		}
	}
}
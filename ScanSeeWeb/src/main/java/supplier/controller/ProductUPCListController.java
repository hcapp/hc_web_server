/**
 * @ (#) ProductUPCListController.java 29-Dec-2011
 * Project       :ScanSeeWeb
 * File          : ProductUPCListController.java
 * Author        : Nanda bhat
 * Company       : Span Systems Corporation
 * Date Created  : 29-Dec-2011
 *
 * @author       :  Nanda bhat
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */
package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SupplierService;

import common.exception.ScanSeeServiceException;
import common.pojo.Product;
import common.pojo.Rebates;
import common.pojo.Users;

@Controller
@RequestMapping("/rebateprodupclist.htm")
public class ProductUPCListController
{

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ProductUPCListController.class);
	/**
	 * Variable RETURN_VIEW declared as constant String.
	 */
	final public String RETURN_VIEW = "rebateprodupclist";

	/**
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showCreateRebatePage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{

		LOG.info("showCreateRebatePage:: Inside Get Method");
		session.removeAttribute("rebateprodupclist");
		Rebates product = new Rebates();
		model.put("produpclistForm", product);
		LOG.info("showCreateRebatePage:: Inside Exit Get Method");
		return RETURN_VIEW;
	}

	/**
	 * @param productInfo
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/rebateprodupclist.htm", method = RequestMethod.POST)
	public ModelAndView searchResult(@ModelAttribute("produpclistForm") Rebates productInfo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{
		ArrayList<Rebates> rebateProductList = null;
		ArrayList<Product> pdtInfoList = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

		String productName = productInfo.getProductName().trim();
		LOG.info("productName*************" + productName);
		long retailerID = productInfo.getRetailID();
		// long retailerID =517;
		LOG.info("retailerID*************" + retailerID);
		int retailerLocId = productInfo.getRetailerLocID();
		// int retailerLocId=91412;
		LOG.info("retailerLocId*************" + retailerLocId);

		int supplierId = 0;
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		supplierId = loginUser.getSupplierId();
		// supplierId =59;
		LOG.info("supplierId*************" + supplierId);

		try
		{
			rebateProductList = supplierService.getProdToAssociateRebate(productName, supplierId, retailerID, retailerLocId);

			pdtInfoList = (ArrayList<Product>) request.getSession().getAttribute("pdtInfoList");
			if (pdtInfoList != null && !pdtInfoList.isEmpty())
			{
				for (int i = 0; i < pdtInfoList.size(); i++)
				{

					for (int j = 0; j < rebateProductList.size(); j++)
					{
						String selProdID = String.valueOf(pdtInfoList.get(i).getProductID());
						String prodID = rebateProductList.get(j).getProductID();
						if (selProdID.equals(prodID))
						{
							rebateProductList.remove(j);
						}
					}
				}
			}

			if (rebateProductList == null || rebateProductList.isEmpty())
			{
				result.reject("searchProdRebate.status");
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in searchResult::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		session.setAttribute("rebateprodupclist", rebateProductList);

		return new ModelAndView(RETURN_VIEW);
	}
}

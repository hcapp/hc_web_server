/**
 * 
 */
package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import retailer.service.RetailerService;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.dashBoard.ReportManager;
import common.exception.ScanSeeServiceException;
import common.pojo.DashBoard;
import common.pojo.DropDown;
import common.pojo.ReportConfigInfo;
import common.pojo.Users;

/**
 * This Controller handles request for Supplier Dashboard and redirect user to
 * Dashboard.
 * 
 * @author manjunatha_gh
 */
@Controller
public class DashBoardController
{
	/**
	 * getting Logger instance for this controller.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DashBoardController.class);

	final private String RETURN_VIEW = "adminpagemfg";

	final private String MODULE_NAME = "Supplier";

	/**
	 * This method handles request for Supplier Dashboard and redirect user to
	 * Dashboard.
	 * 
	 * @param request
	 *            http request object.
	 * @param session
	 *            http session object.
	 * @param model
	 *            Spring Model object
	 * @return dashboard View name.
	 * @throws ScanSeeServiceException
	 *             if any exception in processing the request.
	 */
	@RequestMapping(value = "/dashboard.htm", method = RequestMethod.GET)
	public String showSupplierOverviewReport(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "showSupplierOverviewReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final Users user = (Users) request.getSession().getAttribute("loginuser");

		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		Calendar now = Calendar.getInstance();
		DashBoard dashBoard = new DashBoard();
		String toDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);
		now.add(Calendar.MONTH, -1);
		String fromDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;

		session.removeAttribute("reportConfigInfo");

		// Set Report Parameters
		dashBoard.setSsrsReportName("Supplier");
		dashBoard.setFromDate(fromDate);
		dashBoard.setToDate(toDate);
		dashBoard.setId(String.valueOf(user.getSupplierId()));
		dashBoard.setModuleName(MODULE_NAME);

		session.setAttribute("overviewTabStyle", "active");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display:block;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display: none;");
		session.setAttribute("interestDivStyle", "display: none;");
		try
		{
			reportConfigInfo = supplierService.fetchReportConfiguration();

			session.setAttribute("reportConfigInfo", reportConfigInfo);

			reportContent = reportManager.getOverviewReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);

			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showSupplierOverviewReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/dashboard.htm", method = RequestMethod.POST)
	public String showSupplierOverviewReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "showSupplierOverviewReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final Users user = (Users) request.getSession().getAttribute("loginuser");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;
		String reportContent = null;
		ReportManager reportManager = new ReportManager();

		session.setAttribute("overviewTabStyle", "active");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display:block;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display: none;");
		session.setAttribute("interestDivStyle", "display: none;");

		// Set Report Parameters
		dashBoard.setSsrsReportName("Supplier");
		dashBoard.setId(String.valueOf(user.getSupplierId()));
		dashBoard.setModuleName(MODULE_NAME);

		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}

			reportContent = reportManager.getOverviewReport(dashBoard, reportConfigInfo);
			session.setAttribute("reportContent", reportContent);
			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showSupplierOverviewReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/getdemographicsrep.htm", method = RequestMethod.POST)
	public String showDemographicReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{

		final String methodName = "showDemographicReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		Users user = (Users) session.getAttribute("loginuser");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;

		session.setAttribute("overviewTabStyle", "");
		session.setAttribute("demographicsTabStyle", "active");
		session.setAttribute("appSiteTabStyle", "");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display: none;");
		session.setAttribute("demographicsDivStyle", "display:block;");
		session.setAttribute("appSiteDivStyle", "display: none;");
		session.setAttribute("interestDivStyle", "display: none;");

		String moduleId = dashBoard.getModuleId();

		// Set Report Parameters
		dashBoard.setSsrsReportName("SupplierDemographics");
		dashBoard.setId(String.valueOf(user.getSupplierId()));
		dashBoard.setModuleName(MODULE_NAME);

		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}
			ArrayList<DropDown> reportDropdownList = retailerService.displaySupplierRetailerDemographicsDD();
			if (null == moduleId)
			{
				dashBoard.setModuleId(String.valueOf(reportDropdownList.get(0).getId()));
			}
			reportContent = reportManager.getDemographicsReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);
			session.setAttribute("reportDropdownList", reportDropdownList);
			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showDemographicReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/getproductrep.htm", method = RequestMethod.POST)
	public String showProductReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{

		final String methodName = "showProductReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		Users user = (Users) session.getAttribute("loginuser");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

		ReportConfigInfo reportConfigInfo = null;

		session.setAttribute("overviewTabStyle", "");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "active");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display: none;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display: block;");
		session.setAttribute("interestDivStyle", "display: none;");

		String moduleId = dashBoard.getProductModuleId();

		// Set Report Parameters
		dashBoard.setSsrsReportName("SupplierProducts");
		dashBoard.setId(String.valueOf(user.getSupplierId()));
		dashBoard.setModuleName(MODULE_NAME);
		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}
			ArrayList<DropDown> reportDropdownList = supplierService.displayDropDown();
			if (null == moduleId)
			{
				dashBoard.setProductModuleId(String.valueOf(reportDropdownList.get(0).getId()));
			}

			reportContent = reportManager.getSupplierProductReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);
			session.setAttribute("reportProductDropdownList", reportDropdownList);
			model.put("dashboardform", dashBoard);
		}
		catch (Exception e)
		{
			LOG.error("Exception occurred in showProductReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/getinterestreport.htm", method = RequestMethod.POST)
	public String showSupplierInterestReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		final String methodName = "showSupplierInterestReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		Users user = (Users) session.getAttribute("loginuser");

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;

		session.setAttribute("overviewTabStyle", "");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "");
		session.setAttribute("interestTabStyle", "active");

		session.setAttribute("overviewDivStyle", "display: none;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display: none;");
		session.setAttribute("interestDivStyle", "display:block;");

		// Set Report Parameters
		dashBoard.setSsrsReportName("SupplierInterest");
		dashBoard.setId(String.valueOf(user.getSupplierId()));
		dashBoard.setModuleName(MODULE_NAME);

		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}
			reportContent = reportManager.getSupplierInterestReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);

			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showSupplierInterestReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/getprdcttdemghyrep.htm", method = RequestMethod.POST)
	public String showProductDemoReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		final String methodName = "showProductDemoReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		Users user = (Users) session.getAttribute("loginuser");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");

		ReportConfigInfo reportConfigInfo = null;

		session.setAttribute("overviewTabStyle", "");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "active");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display: none;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display: block;");
		session.setAttribute("interestDivStyle", "display: none;");

		String moduleId = dashBoard.getProductModuleId();

		// Set Report Parameters
		dashBoard.setSsrsReportName("SupplierProducts");
		dashBoard.setId(String.valueOf(user.getSupplierId()));
		dashBoard.setModuleName(MODULE_NAME);

		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}
			ArrayList<DropDown> reportDropdownList = supplierService.displayDropDown();
			if (null == moduleId)
			{
				dashBoard.setProductModuleId(String.valueOf(reportDropdownList.get(0).getId()));
			}

			reportContent = reportManager.getSupplierThisLocReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);
			session.setAttribute("reportProductDropdownList", reportDropdownList);
			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showProductDemoReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/exportreport.htm", method = RequestMethod.POST)
	public void exportReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException, IOException
	{
		final String methodName = "exportReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String downloadFilePath = null;
		ReportManager reportManager = new ReportManager();
		Users user = (Users) session.getAttribute("loginuser");
		String reportName = null;
		String fromDate = dashBoard.getFromDate();
		String toDate = dashBoard.getToDate();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;
		String fileExtension = null;
		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}

			if (null != dashBoard.getReportName())
			{

				if (dashBoard.getReportName().equals("Overview"))
				{
					reportName = "Supplier";
					downloadFilePath = reportManager.exportReport(reportName, String.valueOf(user.getSupplierId()), fromDate, toDate,
							dashBoard.getExportFormat(), MODULE_NAME, reportConfigInfo);
				}
				else if (dashBoard.getReportName().equals("Interest"))
				{
					reportName = "SupplierInterest";
					downloadFilePath = reportManager.exportReport(reportName, String.valueOf(user.getSupplierId()), fromDate, toDate,
							dashBoard.getExportFormat(), MODULE_NAME, reportConfigInfo);
				}
				else if (dashBoard.getReportName().equals("Demographics"))
				{
					reportName = "SupplierDemographics";

					downloadFilePath = reportManager.exportModuleReport(reportName, String.valueOf(user.getSupplierId()), fromDate, toDate,
							dashBoard.getExportFormat(), dashBoard.getModuleId(), MODULE_NAME, reportConfigInfo);

				}
				else if (dashBoard.getReportName().equals("Product"))
				{
					reportName = "SupplierProducts";

					downloadFilePath = reportManager.exportModuleReport(reportName, String.valueOf(user.getSupplierId()), fromDate, toDate,
							dashBoard.getExportFormat(), dashBoard.getProductModuleId(), MODULE_NAME, reportConfigInfo);

				}
			}

			if (dashBoard.getExportFormat().equals("PDF"))
			{
				fileExtension = ".pdf";
			}
			else if (dashBoard.getExportFormat().equals("EXCEL"))
			{
				fileExtension = ".xls";
			}
			response.sendRedirect("/ScanSeeWeb/fileController/downloadexportfile.htm?" + "fileFromat=" + dashBoard.getExportFormat() + "&fileName="
					+ reportName + fileExtension + "&filePath=" + downloadFilePath);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in exportReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODSTART + methodName);
	}
}

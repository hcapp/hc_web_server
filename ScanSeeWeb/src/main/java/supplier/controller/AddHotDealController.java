package supplier.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;
import supplier.validator.HotDealsDetailsValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.HotDealInfo;
import common.pojo.Product;
import common.pojo.Retailer;
import common.pojo.State;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

/**
 * AddHotDealRetailerController is a controller class for adding supplier product hot deals
 * screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class AddHotDealController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AddHotDealController.class);

	/**
	 * Variable dealsDetailsValidator declared as instance of
	 * HotDealRetailerDetailsValidator.
	 */
	private HotDealsDetailsValidator dealsDetailsValidator;

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private final String viewName = "addHotDeals";

	/**
	 * Variable view declared as String.
	 */
	private String view = viewName;
	
	/**
	 * Variable viewRedirect declared as String.
	 */
	private final String viewRedirect = "hotDeal.htm";
	
	/**
	 * To dealsDetailsValidator to set.
	 * @param dealsDetailsValidator to set.
	 */
	@Autowired
	public final void setDealsDetailsValidator(HotDealsDetailsValidator dealsDetailsValidator)
	{
		this.dealsDetailsValidator = dealsDetailsValidator;
	}

	/**
	 * This controller method will display the new HotDeals screen.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/dailyDealsmfg.htm", method = RequestMethod.GET)
	public final String showPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealController : showPage ");
		ArrayList<State> states = null;
		ArrayList<TimeZones> timeZonelst = null;
		ArrayList<Category> arBCategoryList = null;
		try
		{
			request.getSession().removeAttribute("selRetailerLoc");
			request.getSession().removeAttribute("selRetailer");
			request.getSession().removeAttribute("hotdealcity");
			request.getSession().removeAttribute("addProductImage");
			request.getSession().removeAttribute("pdtInfoList");
			request.getSession().removeAttribute("PreviewProductID");
 
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
			states = supplierService.getAllStates();
			arBCategoryList = supplierService.getAllBusinessCategory();
			session.setAttribute("statesListCreat", states);
			session.setAttribute("categoryList", arBCategoryList);
			/*
			 * ArrayList<Retailer> retailers = null; retailers =
			 * supplierService.getRetailers();
			 * session.setAttribute("retailerList", retailers);
			 */
			timeZonelst = supplierService.getAllTimeZones();
			session.setAttribute("timeZoneslst", timeZonelst);
			final HotDealInfo hotdealInfo = new HotDealInfo();
			model.put("addhotdealform", hotdealInfo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return viewName;
	}

	/**
	 * This controller method will display all the retailer names list by calling service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param productId request as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/fetchpdtretailer", method = RequestMethod.GET)
	@ResponseBody
	public final String getRetailers(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request, HttpServletResponse response)
			throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealController : getRetailers ");
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='retailID' id='retailID' onchange=\"loadRetailerLocation();\"><option value='0'>--Select Retailer--</option>";
		innerHtml.append(finalHtml);
		if ("".equals(productId))
		{
			final String strProductID = (String) request.getSession().getAttribute("PreviewProductID");
			if (null != strProductID)
			{
				productId = strProductID;
			}
		}
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			final String strSelRetailer = (String) request.getSession().getAttribute("selRetailer");
			int selRetailer = 0;
			if (strSelRetailer != null)
			{
				selRetailer = Integer.valueOf(strSelRetailer);
			}
			ArrayList<Retailer> retailers = null;
			retailers = supplierService.getRetailersForProducts(productId);
			response.setContentType("text/xml");
			for (int i = 0; i < retailers.size(); i++)
			{
				if (selRetailer == retailers.get(i).getRetailID())
				{
					innerHtml.append("<option selected=true value=" + retailers.get(i).getRetailID() + ">" + retailers.get(i).getRetailName() + "</option>");
				} else {
					innerHtml.append("<option value=" + retailers.get(i).getRetailID() + ">" + retailers.get(i).getRetailName() + "</option>");
				}
			}
			innerHtml.append("</select>");
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return innerHtml.toString();
	}

	/**
	 * This controller method will display all the retailer address (Address1, City, State, Postal code) by calling service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param retailerId request as parameter.
	 * @param productId request as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/fetchpdtretailerlocation.htm", method = RequestMethod.GET)
	 @ResponseBody
	 public final String getRetailerLocation(@RequestParam(value = "retailerId", required = true) int retailerId,
			@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request, HttpServletResponse response)
			throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealController : getRetailerLocation ");
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='retailerLocID' id='retailerLocID'><option value='0'>Please Select Location</option>";

		innerHtml.append(finalHtml);
		int selRetailerLoc = 0;
		final String strSelRetailerLoc = (String) request.getSession().getAttribute("selRetailerLoc");

		if ("".equals(productId))
		{
			final String strProductID = (String) request.getSession().getAttribute("PreviewProductID");
			// request.getSession().removeAttribute("PreviewProductID");

			if (null != strProductID)
			{
				productId = strProductID;
			}
		}
		if (retailerId == 0)
		{
			final String strSelRetailer = (String) request.getSession().getAttribute("selRetailer");
			if (strSelRetailer != null)
			{
				retailerId = Integer.valueOf(strSelRetailer);
			}
		}
		if (strSelRetailerLoc != null)
		{
			selRetailerLoc = Integer.valueOf(strSelRetailerLoc);
			request.getSession().removeAttribute("selRetailerLoc");
		} try {

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

			ArrayList<Retailer> retailers = null;
			retailers = supplierService.getRetailersLocForProducts(productId, retailerId);
			response.setContentType("text/xml");
			 StringBuffer value = new StringBuffer();
			for (int i = 0; i < retailers.size(); i++)
			{
				value = new StringBuffer();
				if (retailers.get(i).getAddress1() != null)
				{
					value.append(retailers.get(i).getAddress1());
					value.append(ApplicationConstants.COMMA);
					if (retailers.get(i).getCity() != null)
					{
						value.append(retailers.get(i).getCity());
						value.append(ApplicationConstants.COMMA);
						if (retailers.get(i).getState() != null)
						{
							value.append(retailers.get(i).getState());
							value.append(ApplicationConstants.COMMA);
							if (retailers.get(i).getPostalCode() != null)
							{
								value.append(retailers.get(i).getPostalCode());
								value.append(ApplicationConstants.COMMA);
							}
						}
					}
				}
				if (selRetailerLoc == retailers.get(i).getRetailLocationID()) {
					innerHtml.append("<option  selected=true value=" + retailers.get(i).getRetailLocationID() + ">" + value.toString() + "</option>");
				} else {
					innerHtml.append("<option value=" + retailers.get(i).getRetailLocationID() + ">" + value.toString() + "</option>");
				}
			}
			innerHtml.append("</select>");
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return innerHtml.toString();
	}

	/**
	 * The controller method add new hot deals for Supplier by calling service and DAO methods.
	 * 
	 * @param dealsVo HotDealInfo instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/dailyDealsmfg.htm", method = RequestMethod.POST)
	public final ModelAndView addHotDeals(@ModelAttribute("addhotdealform") HotDealInfo dealsVo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealController : addHotDeals ");
		String compDate = null;
		final Date currentDate = new Date();
		String strCategory = null;
		String strCity = null;
		String strpCity = null;
		int strRet = 0;
		int strRetLoc = 0;
		try
		{
			String isDataInserted = null;
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			request.getSession().removeAttribute("message");
			final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final StringBuffer allCities = new StringBuffer();
			strCity = request.getParameter("slctOpt");
			
			/*
			 * if (startDate.contains("/")) { startDate =
			 * Utility.getFormattedDate(startDate);
			 * dealsVo.setDealStartDate(startDate); } else if
			 * (startDate.contains("-")) { dealsVo.setDealStartDate(startDate);
			 * } if (endDate.contains("/")) { endDate =
			 * Utility.getFormattedDate(endDate);
			 * dealsVo.setDealEndDate(endDate); } else if
			 * (endDate.contains("-")) { dealsVo.setDealEndDate(endDate); }
			 */
			dealsVo.setCityHiddenChecked(Utility.checkNull(strCity));
			String sTime = String.valueOf(dealsVo.getDealStartHrs()) + ":" + String.valueOf(dealsVo.getDealStartMins());
			String endTime = String.valueOf(dealsVo.getDealEndhrs()) + ":" + String.valueOf(dealsVo.getDealEndMins());
			strCategory = dealsVo.getbCategory();
		    strpCity = dealsVo.getCity();
		    strRet = dealsVo.getRetailID();
		    strRetLoc = dealsVo.getRetailerLocID();
			dealsVo.setbCategoryHidden(dealsVo.getbCategory());

			dealsVo.setCityHidden(dealsVo.getCity());
			dealsVo.setLocationHidden(String.valueOf(dealsVo.getRetailerLocID()));
			dealsVo.setProductNameHidden(dealsVo.getProductNameHidden());
			// String stTime = dealsVo.getDealStartTime();
			if (sTime != null && !"".equals(sTime))
			{
				sTime = sTime.replace(',', ':');
			} else {
				sTime = "00:00";
			}

			dealsVo.setDealStartTime(sTime);
			// String endTime = dealsVo.getDealEndTime();
			if (endTime != null && !"".equals(endTime)) {
				endTime = endTime.replace(',', ':');
			} else {
				endTime = "00:00";
			}

			dealsVo.setDealEndTime(endTime);
			if (strCategory.equalsIgnoreCase(ApplicationConstants.ZERO))
			{
				dealsDetailsValidator.validate(dealsVo, result, ApplicationConstants.BUSINESSCATEGORY);
			}
			if ("City".equals(dealsVo.getDealForCityLoc()))
			{
				if (strpCity.equalsIgnoreCase(""))
				{
					dealsDetailsValidator.validate(dealsVo, result, ApplicationConstants.POPULATIONCENTER);
				}
			}
			if ("Location".equals(dealsVo.getDealForCityLoc())) {
				if (strRet == 0) {
					dealsDetailsValidator.validate(dealsVo, result, ApplicationConstants.DEALRETAILER);
				}
				if (strRetLoc == 0) {
					dealsDetailsValidator.validate(dealsVo, result, ApplicationConstants.DEALRETAILERLOC);
				}
			}

			if (dealsVo != null) {
				dealsDetailsValidator.validate(dealsVo, result);
				if (!result.hasErrors())
				{
					compDate = Utility.compareCurrentDate(dealsVo.getDealStartDate(), currentDate);
					if (null != compDate) {
						dealsDetailsValidator.validate(dealsVo, result, ApplicationConstants.DATESTARTCURRENT);
					}
					compDate = Utility.compareCurrentDate(dealsVo.getDealEndDate(), currentDate);
					if (null != compDate) {
						dealsDetailsValidator.validate(dealsVo, result, ApplicationConstants.DATEENDCURRENT);
					}
					else {
						compDate = Utility.compareDate(dealsVo.getDealStartDate(), dealsVo.getDealEndDate());
						if (null != compDate)
						{
							dealsDetailsValidator.validate(dealsVo, result, ApplicationConstants.DATEAFTER);
						}
					}
				}
			}
			if (result.hasErrors()) {
				if ("City".equals(dealsVo.getDealForCityLoc()))
				{
					request.getSession().setAttribute("hotdealcity", dealsVo.getCity());
					session.removeAttribute("selRetailer");
					session.removeAttribute("selRetailerLoc");
				} else if ("Location".equals(dealsVo.getDealForCityLoc())) {
					session.removeAttribute("hotdealcity");
					session.setAttribute("selRetailer", String.valueOf(dealsVo.getRetailID()));
					session.setAttribute("selRetailerLoc", String.valueOf(dealsVo.getRetailerLocID()));
				}
				view = viewName;
			} else {
				if (null != dealsVo.getCity())
				{
					if ("All".equals(dealsVo.getCity()))
					{
						final ArrayList<City> cities = (ArrayList<City>) request.getSession().getAttribute("hotdealpopcenters");
						for (int i = 0; i < cities.size(); i++)
						{
							allCities.append(cities.get(i).getPopulationCenterID());
							allCities.append(ApplicationConstants.COMMA);
						}
						dealsVo.setCity(allCities.toString().substring(0, allCities.toString().length() - 1));
					}
				}
				isDataInserted = supplierService.addDeals(user.getSupplierId(), dealsVo);
				if (isDataInserted == null || isDataInserted == ApplicationConstants.FAILURE)
				{
					view = viewName;
					//request.getSession().setAttribute("message", "Error While Adding HotDeal");
				} else {
					view = viewRedirect;
					request.getSession().setAttribute("message", "Hot Deal Added Successfully");
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		/*
		 * catch (ParseException e) {
		 * LOG.error("Exception occured while parsing date.");
		 * e.printStackTrace(); }
		 */
		if (view.equals(viewRedirect))
		{
			return new ModelAndView(new RedirectView(view));
		} else {
			return new ModelAndView(view);
		}
	}

	/**
	 * This ModelAttribute sort Deal start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("DealStartHours")
	public final Map<String, String> populateDealStartHrs() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method returns a negative integer, zero, or a positive 
	 *  integer as the first argument is less than, equal to, or greater than the second.
	 * 
	 * @param unsortMap as request parameter.
	 * @return sortedMap returns a integer value.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException
	{
		final List list = new LinkedList(unsortMap.entrySet());
		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2)
			{
				return ((Comparable) ((Map.Entry) o1).getValue()).compareTo(((Map.Entry) o2).getValue());
			}
		});
		// put sorted list into map again
		final Map sortedMap = new LinkedHashMap();
		Map.Entry entry = null;
		for (final Iterator it = list.iterator(); it.hasNext();)
		{
			entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort Deal start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("DealStartMinutes")
	public final Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method will display supplier HotDeals details in iphone screen formate.
	 * 
	 * @param objHotdealInfo HotDealInfo instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @param model ModelMap instance as parameter.
	 * @return VIEW_NAME addHotDeals view.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/previewHotDealSupplier.htm", method = RequestMethod.POST)
	public final String previewPage(HotDealInfo objHotdealInfo, BindingResult result, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealController : previewPage ");
		final String viewPreview = "previewSupplierHotDeals";
		String strSalePrice = null;
		String strRegularPrice = null;
		String strStartDT = null;
		String longDescription = null;
		String hotDealName = null;
		List<HotDealInfo> dealProductList = null;

		request.getSession().removeAttribute("saleprice");
		request.getSession().removeAttribute("regularprice");
		request.getSession().removeAttribute("startDT");
		request.getSession().removeAttribute("longDescription");
		request.getSession().removeAttribute("hotDealName");

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		strSalePrice = objHotdealInfo.getSalePrice();
		strRegularPrice = objHotdealInfo.getPrice();
		strStartDT = objHotdealInfo.getDealStartDate();
		longDescription = objHotdealInfo.getHotDealLongDescription();
		hotDealName = objHotdealInfo.getHotDealName();
		final String prdname = objHotdealInfo.getProductName();
		final String []str = prdname.split(ApplicationConstants.COMMA);
		final String productName = str[0].trim();
		objHotdealInfo.setProductName(productName);

		session.setAttribute("saleprice", strSalePrice);
		session.setAttribute("regularprice", strRegularPrice);
		session.setAttribute("startDT", strStartDT);
		session.setAttribute("longDescription", longDescription);
		session.setAttribute("hotDealName", hotDealName);

		if (null != objHotdealInfo.getProductId() && !"".equals(objHotdealInfo.getProductId()))
		{
			session.setAttribute("PreviewProductID", objHotdealInfo.getProductId());
		}

		if (null != objHotdealInfo.getDealForCityLoc())
		{
			if ("City".equals(objHotdealInfo.getDealForCityLoc()))
			{
				request.getSession().setAttribute("hotdealcity", objHotdealInfo.getCity());
				request.getSession().removeAttribute("selRetailer");
				request.getSession().removeAttribute("selRetailerLoc");
			} else if ("Location".equals(objHotdealInfo.getDealForCityLoc())) {
				session.setAttribute("selRetailer", String.valueOf(objHotdealInfo.getRetailID()));
				session.setAttribute("selRetailerLoc", String.valueOf(objHotdealInfo.getRetailerLocID()));
				request.getSession().removeAttribute("hotdealcity");
			}
		}

		try {
			if (productName != null && !"".equals(productName))
			{
				dealProductList = supplierService.getProdDetailsInfo(Long.valueOf(loginUser.getSupplierId()), productName);
				if (dealProductList != null && !dealProductList.isEmpty())
				{
					session.setAttribute("addProductImage", dealProductList.get(0).getProductImagePath());
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("previewhotdealretailerform", objHotdealInfo);
		return viewPreview;
	}

	/**
	 * This controller method will remove products which are associated with supplier add hot deals screen.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param productId request as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/removeProduct", method = RequestMethod.GET)
	@ResponseBody
	public final String removeProduct(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request, HttpSession session,
			HttpServletResponse response) throws ScanSeeServiceException
	{
		final ArrayList<Product> pdtInfoList = (ArrayList<Product>) session.getAttribute("pdtInfoList");
		final ArrayList<Product> pdtInfoListProd = new ArrayList<Product>();
		String[] temp;
		temp = productId.split(ApplicationConstants.COMMA);
		if (null != pdtInfoList && !pdtInfoList.isEmpty())
		{
			for (int i = 0; i < temp.length; i++)
			{
				for (int j = 0; j < pdtInfoList.size(); j++)
				{
					if (pdtInfoList.get(j).getProductID() == Long.parseLong(temp[i].toString()))
					{
						pdtInfoListProd.add(pdtInfoList.get(j));
						break;
					}
				}
			}
		}
		if (null != pdtInfoListProd && !pdtInfoListProd.isEmpty())
		{
			request.getSession().setAttribute("pdtInfoList", pdtInfoListProd);
		}
		return null;
	}

	/**
	 * This controller method will check products are associated with supplier add hot deals screen.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param productId request as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/checkAssociatedProd", method = RequestMethod.GET)
	@ResponseBody
	public final String checkAssociatedProd(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request, HttpSession session,
			HttpServletResponse response) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealController : checkAssociatedProd ");
		session.removeAttribute("pdtInfoList");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final ArrayList<Product> arPdtInfoList = (ArrayList<Product>) supplierService.getPdtInfoForDealRerun(productId);
		if (arPdtInfoList != null && !arPdtInfoList.isEmpty())
		{
			for (int i = 0; i < arPdtInfoList.size(); i++)
			{
				if ("".equals(Utility.checkNull(arPdtInfoList.get(i).getPrice()))) {
					arPdtInfoList.get(i).setPrice("0");
				}
				if ("".equals(Utility.checkNull(arPdtInfoList.get(i).getImagePath()))) {
					arPdtInfoList.get(i).setImagePath(ApplicationConstants.UPLOAD_IMAGE_PATH);
				}
			}
			request.getSession().setAttribute("pdtInfoList", arPdtInfoList);
		}
		return null;
	}
	
	/**
	 * This controller method will check products are associated with supplier add hot deals screen.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param productId request as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/checkAssociatedCouponProd", method = RequestMethod.GET)
	@ResponseBody
	public final String checkAssociatedCouponProd(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request, HttpSession session,
			HttpServletResponse response) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealController : checkAssociatedCouponProd ");
		session.removeAttribute("scanCodeList");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final ArrayList<Product> arProductList = (ArrayList<Product>) supplierService.getPdtInfoForDealRerun(productId);
		request.getSession().setAttribute("scanCodeList", arProductList);
		return null;
	}
	
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("CouponStartHrs")
	public Map<String, String> populateCouponStartHrs() throws ScanSeeServiceException
	{
		HashMap<String, String> mapCouponStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapCouponStartHrs.put("0" + i, "0" + i);
			} else {
				mapCouponStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		Iterator iterator = mapCouponStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapCouponStartHrs);
		return sortedMap;
	}

	@SuppressWarnings("rawtypes")
	@ModelAttribute("CouponStartMin")
	public Map<String, String> populateCouponStartMin() throws ScanSeeServiceException
	{
		HashMap<String, String> mapCouponStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapCouponStartHrs.put("0" + i, "0" + i);
				i = i + 4;
			} else {
				mapCouponStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		Iterator iterator = mapCouponStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		Map<String, String> sortedMap = sortByComparator(mapCouponStartHrs);
		return sortedMap;
	}
}

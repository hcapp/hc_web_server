package supplier.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;
import supplier.validator.RebatesValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Rebates;
import common.pojo.Retailer;
import common.pojo.TimeZones;
import common.util.Utility;

/**
 * EditRebatesController is a controller class, screen for edit/update supplier rebates.
 * 
 * @author Created by SPAN.
 */
@Controller
public class EditRebatesController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditRebatesController.class);
	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "editRebates";

	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();

	/**
	 * Variable view declared as String.
	 */
	private String view = VIEW_NAME;

	/**
	 * Variable rebatesValidator declared as instance of RebatesValidator.
	 */
	private RebatesValidator rebatesValidator;

	/**
	 * To set rebatesValidator.
	 * 
	 * @param rebatesValidator to set.
	 */
	@Autowired
	public final void setRebatesValidator(RebatesValidator rebatesValidator)
	{
		this.rebatesValidator = rebatesValidator;
	}

	/**
	 * This controller method will display all the retailer address (Address1, City, State, Postal code) by calling service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param response HttpServletResponse instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param retailerId request as parameter.
	 * @param locationId request as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/getRetailer.htm", method = RequestMethod.GET)
	@ResponseBody
	public final String getRetailer(@RequestParam(value = "retailerId", required = true) int retailerId,
			@RequestParam(value = "locationId", required = true) String locationId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside EditRebatesController :  getRetailer ");
		ArrayList<Retailer> retailerLoc = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='retailerLocID' id='retailerLocID'  onchange='getLocationTrigger(this);'><option value='0'>--Select--</option>";
		innerHtml.append(finalHtml);
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			if (ISDUBUGENABLED)
			{
				LOG.debug("fetching Retail locations for Retailerid {}", retailerId);
			}
			retailerLoc = supplierService.getRetailerLoc(retailerId);
			response.setContentType("text/xml");
			final StringBuffer value = new StringBuffer();
			for (int i = 0; i < retailerLoc.size(); i++)
			{
				if (retailerLoc.get(i).getAddress1() != null)
				{
					value.append(retailerLoc.get(i).getAddress1());
					value.append(ApplicationConstants.COMMA);
					if (retailerLoc.get(i).getCity() != null)
					{
						value.append(retailerLoc.get(i).getCity());
						value.append(ApplicationConstants.COMMA);
						if (retailerLoc.get(i).getState() != null)
						{
							value.append(retailerLoc.get(i).getState());
							value.append(ApplicationConstants.COMMA);
							if (retailerLoc.get(i).getPostalCode() != null)
							{
								value.append(retailerLoc.get(i).getPostalCode());
								value.append(ApplicationConstants.COMMA);
							}
						}
					}
				}
				if (Integer.valueOf(locationId) == retailerLoc.get(i).getRetailLocationID())
				{
					innerHtml.append("<option value=\"" + retailerLoc.get(i).getRetailLocationID() + "\" selected =true >" + value.toString()
							+ "</option>");
				} else {
					innerHtml.append("<option value=\"" + retailerLoc.get(i).getRetailLocationID() + "\">" + value.toString() + "</option>");
				}
			}
			innerHtml.append("</select>");
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (NullPointerException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (ISDUBUGENABLED)
		{
			LOG.debug("Retailer location response {}", innerHtml.toString());
		}
		return innerHtml.toString();
	}

	/**
	 * This controller method will display the rebate by passing rebateId as input parameter by call service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param model  ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/rebatesEdit.htm", method = RequestMethod.GET)
	public final String showPage(HttpServletRequest request, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside EditRebatesController : showPage ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		final String rebateId = request.getParameter("rebateID");
		session.removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		request.getSession().removeAttribute("pdtInfoList");
		final int rebId = Integer.parseInt(rebateId);
		final Rebates rebatesVo = new Rebates();
		rebatesVo.setRebateID(rebId);
		String rebateStartTime = null;
		String rebateEndTime = null;

		String[] tempStartTimeHrsMin = null;
		String[] tempEndTimeHrsMin = null;
		try
		{
			final List<Rebates> rebatesList = (List<Rebates>) supplierService.getRebatesForDisplay(rebId);
			if (rebatesList != null && !rebatesList.isEmpty())
			{
				String strAmount = null;
				String startDate = null;
				String endDate = null;
				for (int i = 0; i < rebatesList.size(); i++)
				{
					rebatesVo.setRebName(rebatesList.get(i).getRebName());
					strAmount = rebatesList.get(i).getRebAmount();
					strAmount = Utility.formatDecimalValue(strAmount);
					rebatesVo.setRebAmount(strAmount);

					// rebatesVo.setRebAmount(rebatesList.get(i).getRebAmount());
					rebatesVo.setRebDescription(rebatesList.get(i).getRebShortDescription());
					startDate = rebatesList.get(i).getRebStartDate();
					if (startDate != null)
					{
						startDate = Utility.formattedDate(startDate);
						rebatesVo.setRebStartDate(startDate);
					}
					endDate = rebatesList.get(i).getRebEndDate();
					if (endDate != null)
					{
						endDate = Utility.formattedDate(endDate);
						rebatesVo.setRebEndDate(endDate);
					}
					rebateStartTime = rebatesList.get(i).getRebStartTime();
					tempStartTimeHrsMin = rebateStartTime.split(":");
					rebatesVo.setRebStartHrs(tempStartTimeHrsMin[0]);
					rebatesVo.setRebStartMins(tempStartTimeHrsMin[1]);
					rebateEndTime = rebatesList.get(i).getRebEndTime();
					tempEndTimeHrsMin = rebateEndTime.split(":");
					rebatesVo.setRebEndhrs(tempEndTimeHrsMin[0]);
					rebatesVo.setRebEndMins(tempEndTimeHrsMin[1]);
					rebatesVo.setRebateTimeZoneId(rebatesList.get(i).getRebateTimeZoneId());
					rebatesVo.setRebTermCondtn(rebatesList.get(i).getRebTermCondtn());
					rebatesVo.setRebateTimeZoneId(rebatesList.get(i).getRebateTimeZoneId());
					rebatesVo.setRetailID(Long.valueOf(rebatesList.get(i).getRetailerID()));
					rebatesVo.setRetailerLocID(rebatesList.get(i).getRetailerLocID());
					rebatesVo.setRetailerLocationID(rebatesList.get(i).getRetailerLocID());
					rebatesVo.setNoOfrebatesIsued(rebatesList.get(i).getNoOfrebatesIsued());
					rebatesVo.setNoOfRebatesUsed(rebatesList.get(i).getNoOfRebatesUsed());
					rebatesVo.setRebateTimeZoneId(rebatesList.get(i).getRebateTimeZoneId());

					if (!"".equals(Utility.checkNull(String.valueOf(rebatesList.get(i).getRetailerLocID())))) {
						session.setAttribute("retailerLocHidden", String.valueOf(rebatesList.get(i).getRetailerLocID()));
					}
					if (!"".equals(Utility.checkNull(String.valueOf(rebatesList.get(i).getRetailID())))) {
						session.setAttribute("retailerIdHidden", String.valueOf(rebatesList.get(i).getRetailID()));
					}
				}
				ArrayList<Retailer> retailers = null;
				retailers = supplierService.getRetailers();
				session.setAttribute("retailerList", retailers);
				ArrayList<TimeZones> timeZonelst = null;
				timeZonelst = supplierService.getAllTimeZones();
				session.setAttribute("RebateTimeZoneslst", timeZonelst);
			}
			model.put("editRebatesForm", rebatesVo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (NullPointerException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (ParseException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return VIEW_NAME;
	}

	/**
	 * This ModelAttribute sort rebate start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("RebateStartHrs")
	public Map<String, String> populateRebateStartHrs() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method returns a negative integer, zero, or a positive 
	 *  integer as the first argument is less than, equal to, or greater than the second.
	 * 
	 * @param unsortMap as request parameter.
	 * @return sortedMap returns a integer value.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map sortByComparator(Map unsortMap) throws ScanSeeServiceException
	{
		final List list = new LinkedList(unsortMap.entrySet());
		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2)
			{
				return ((Comparable) ((Map.Entry) o1).getValue()).compareTo(((Map.Entry) o2).getValue());
			}
		});
		// put sorted list into map again
		final Map sortedMap = new LinkedHashMap();
		Map.Entry entry = null;
		for (final Iterator it = list.iterator(); it.hasNext();)
		{
			entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * This ModelAttribute sort rebate start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("RebateStartMins")
	public Map<String, String> populatemapRebateStartMins() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++)
		{
			if (i < 10)
			{
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method will update existing Rebate details screen by call service and DAO methods.
	 * 
	 * @param rebatesVo Rebates instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/rebatesEdit.htm", method = RequestMethod.POST)
	public final ModelAndView editRebates(@ModelAttribute("editRebatesForm") Rebates rebatesVo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside EditRebatesController : editRebates ");
		String compDate = null;
		final Date currentDate = new Date();
		session.removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		try
		{
			String isDataInserted = null;
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			/*
			 * String startDate = rebatesVo.getRebStartDate(); String sDate =
			 * Utility.getFormattedDate(startDate);
			 * rebatesVo.setRebStartDate(sDate); String endDate =
			 * rebatesVo.getRebEndDate(); String eDate =
			 * Utility.getFormattedDate(endDate);
			 * rebatesVo.setRebEndDate(eDate);
			 */
			rebatesValidator.validateEdit(rebatesVo, result);
			/*
			 * compDate =
			 * Utility.compareCurrentDate(rebatesVo.getRebStartDate(),
			 * currentDate); if (null != compDate) {
			 * rebatesValidator.validate(rebatesVo, result,
			 * ApplicationConstants.DATESTARTCURRENT); }
			 */
			if (!result.hasErrors())
			{
				compDate = Utility.compareCurrentDate(rebatesVo.getRebEndDate(), currentDate);
				if (null != compDate)
				{
					rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEENDCURRENT);
				} else {
					compDate = Utility.compareDate(rebatesVo.getRebStartDate(), rebatesVo.getRebEndDate());
					if (null != compDate)
					{
						rebatesValidator.validate(rebatesVo, result, ApplicationConstants.DATEAFTER);
					}
				}
			}
			if (result.hasErrors())
			{
				if (!"".equals(Utility.checkNull(String.valueOf(rebatesVo.getRetailerLocID())))) {
					session.setAttribute("retailerLocHidden", String.valueOf(rebatesVo.getRetailerLocID()));
				}
				if (!"".equals(Utility.checkNull(String.valueOf(rebatesVo.getRetailerID())))) {
					session.setAttribute("retailerIdHidden", String.valueOf(rebatesVo.getRetailID()));
				}
				view = VIEW_NAME;
			} else {
				isDataInserted = supplierService.editRebates(rebatesVo);
				if (isDataInserted == null || isDataInserted == ApplicationConstants.FAILURE)
				{
					view = VIEW_NAME;
					result.reject("addRebate.error");
				} else {
					view = "rebatesMfg.htm";
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (NullPointerException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (view.equals(VIEW_NAME)) {
			return new ModelAndView(view);
		} else {
			return new ModelAndView(new RedirectView(view));
		}
	}

	/**
	 * This controller method will display supplier rebates details in iphone screen formate.
	 * 
	 * @param rebatesVo Rebates instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @param model ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/previewsuppleditrebate.htm", method = RequestMethod.POST)
	public final ModelAndView previewPage(@ModelAttribute("previewsuppleditrebform") Rebates rebatesVo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside EditRebatesController : previewPage ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

		List<Rebates> prodList = null;
		final String rebName = request.getParameter("rebName");
		final String rebDescription = request.getParameter("rebDescription");
		session.removeAttribute("retailerLocHidden");
		session.removeAttribute("retailerIdHidden");
		request.getSession().setAttribute("editRebateName", rebName);
		request.getSession().setAttribute("editRebateDescription", rebDescription);
		rebatesVo.setRebName(rebName);
		rebatesVo.setRebDescription(rebDescription);
		if (!"".equals(Utility.checkNull(String.valueOf(rebatesVo.getRetailerLocID())))) {
			session.setAttribute("retailerLocHidden", String.valueOf(rebatesVo.getRetailerLocID()));
		}
		if (!"".equals(Utility.checkNull(String.valueOf(rebatesVo.getRetailerID())))) {
			session.setAttribute("retailerIdHidden", String.valueOf(rebatesVo.getRetailID()));
		}
		try
		{
			prodList = (List<Rebates>) supplierService.fetchProductInformation(rebatesVo.getRebateID());
			if (prodList != null && !prodList.isEmpty())
			{
				session.setAttribute("editSupProductNames", prodList.get(0).getProductNames());
				session.setAttribute("editSupProductImagePath", prodList.get(0).getImagePath());
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("previewsuppleditrebform", rebatesVo);
		return new ModelAndView("editsupplrebpreview");
	}
}

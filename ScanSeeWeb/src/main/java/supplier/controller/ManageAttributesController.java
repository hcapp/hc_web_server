package supplier.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ManageProducts;
import common.pojo.ProductAttributes;
import common.pojo.Users;

@Controller
public class ManageAttributesController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageAttributesController.class);

	@RequestMapping(value = "/manageattr.htm", method = RequestMethod.GET)
	public ModelAndView manageAttributes(@ModelAttribute("manageform") ManageProducts manageProducts, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException,ScanSeeServiceException
	{
		final String methoName = "manageAttributes";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		Users loginuser = (Users) session.getAttribute("loginuser");
		List<ProductAttributes> attrList = null;
		List<ProductAttributes> masterList = null;
		int productId = (int) manageProducts.getProductID();
		try
		{
			// to fetch Attributes associated with products
			LOG.info("fetch Attributes associated with products");
			attrList = supplierService.fetchProductAttributes(productId, loginuser.getSupplierId(), "productattributes");

			// To fetch Master attributes;
			LOG.info("fetch Master attributes");
			masterList = supplierService.fetchProductAttributes(productId, loginuser.getSupplierId(), "MasterList");

		}
		catch (ScanSeeServiceException e)
		{
			  LOG.error("Exception occurred in manageAttributes:::::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}
		model.addAttribute("manageform", manageProducts);
		session.setAttribute("ProductAttrList", attrList);
		session.setAttribute("MasterList", masterList);
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return new ModelAndView("manageattr");
	}

	@RequestMapping(value = "/saveattributes.htm", method = RequestMethod.POST)
	public ModelAndView saveAttributes(@ModelAttribute("manageform") ManageProducts manageProducts, BindingResult result, HttpServletRequest request,
			HttpServletResponse response) throws ScanSeeServiceException
	{
		final String methoName = "saveAttributes";
		LOG.info(ApplicationConstants.METHODSTART + methoName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		try
		{
			supplierService.saveAttributes(manageProducts, loginUser);

		}
		
		catch (ScanSeeServiceException e) {
			  LOG.error("Exception occurred in saveAttributes:::::"+ e.getMessage());
	          throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methoName);
		return new ModelAndView(new RedirectView("displayProduct.htm"));

	}
}

package supplier.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import supplier.service.SearchService;

import common.exception.ScanSeeServiceException;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.Utility;

@Controller
// @RequestMapping("/header.htm")
public class SearchController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SearchController.class);
	SearchService searchService;

	@Autowired
	public void setSearchService(SearchService searchService)
	{
		this.searchService = searchService;
	}

	/**
	 * @param searchService
	 *            the searchService to set
	 */

	@RequestMapping(value = "/header.htm", method = RequestMethod.POST)
	public ModelAndView searchProduct(@ModelAttribute("productinfoform") ProductVO productInfo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SearchService searchService = (SearchService) appContext.getBean("searchService");
		ArrayList<ProductVO> productList = null;
		SearchForm objForm = null;
		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";
		String view = null;

		/*
		 * String zipCode = "91303"; String productName =
		 * "Coolpix S4000 Compact Camera";
		 */

		/*
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 */
		if (null != pageFlag && pageFlag.equals("true"))
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			searchType = objForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}

		if (objForm == null)
		{

			zipCode = (String) request.getParameter("zipCode");

			searhKey = (String) request.getParameter("searchKey");
			searchType = (String) request.getParameter("searchType");

			if (loginUser != null)
			{
				if (loginUser.getUserType() == 1 || loginUser.getUserType() == 2)
				{
					searchType = "products";
				}

			}

			if (loginUser != null)
			{
				zipCode = loginUser.getPostalCode();
				// zipCode = "75244";
				objForm = new SearchForm(zipCode, searhKey, searchType);
			}
			else
			{
				objForm = new SearchForm(zipCode, searhKey, searchType);
				session.setAttribute("searchForm", objForm);
			}

			if (null != searhKey)
			{
				objForm.setSearchKey(searhKey.trim());
			}
			else
			{
				objForm.setSearchKey(searhKey);
			}
			// objForm = new SearchForm(zipCode, searhKey, searchType);
			session.setAttribute("searchFormDeals", objForm);
			session.setAttribute("searchForm", objForm);
		}

		/*
		 * Pagination check
		 */

		try
		{

			SearchResultInfo resultInfo = searchService.searchProducts(loginUser, objForm, lowerLimit);

			if (searchType.equals("products"))
			{
				if (resultInfo.getProductList().isEmpty() || resultInfo.getProductList() == null)
				{
					result.reject("product.error");
					view = "searchProduct";
				}
				else
				{
					resultInfo.setSearchType("products");
					view = "searchProduct";
				}

				session.setAttribute("HomePageSearch", "products");
			}
			else if (searchType.equals("deals"))
			{
				if (resultInfo.getProductList().isEmpty() || resultInfo.getProductList() == null)
				{
					result.reject("product.error");
					view = "searchProduct";
				}
				else
				{
					resultInfo.setSearchType("deals");
					view = "searchProduct";
				}
			}
			else if (searchType.equals("producthotdeal"))
			{
				session.setAttribute("HomePageSearch", "producthotdeal");
				if (resultInfo.getProductList().isEmpty() || resultInfo.getProductList() == null)
				{
					result.reject("product.error");
					view = "searchProductwithdeals";
				}
				else
				{
					resultInfo.setSearchType("producthotdeal");
					view = "searchProductwithdeals";
				}
				request.setAttribute("prodcTabActive", "1");
			}
			session.setAttribute("seacrhList", resultInfo);

			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "header.htm");

			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException e)
		{

			LOG.error("Exception occurred in searchProduct:::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		return new ModelAndView(view);
	}

	@RequestMapping(value = "/searchproductwithdeal.htm", method = RequestMethod.POST)
	public ModelAndView searchProductWithDeals(@ModelAttribute("productinfoform") ProductVO productInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SearchService searchService = (SearchService) appContext.getBean("searchService");

		SearchForm objForm = null;
		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";
		String view = null;

		/*
		 * String zipCode = "91303"; String productName =
		 * "Coolpix S4000 Compact Camera";
		 */

		/*
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 */
		if (null != pageFlag && pageFlag.equals("true"))
		{
			objForm = (SearchForm) session.getAttribute("searchFormDeals");
			searchType = objForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}

		if (objForm == null)
		{
			objForm = (SearchForm) session.getAttribute("searchFormDeals");

			zipCode = objForm.getZipCode();
			searhKey = objForm.getSearchKey();
			searchType = objForm.getSearchType();
			if (loginUser != null)
			{
				if (loginUser.getUserType() == 1 || loginUser.getUserType() == 2)
				{
					searchType = "products";
				}

			}

			objForm = new SearchForm(zipCode, searhKey, searchType);
			// session.setAttribute("searchForm", objForm);
			request.setAttribute("searchForm", objForm);
		}

		/*
		 * Pagination check
		 */

		try
		{
			SearchResultInfo resultInfo = searchService.searchProducts(loginUser, objForm, lowerLimit);
			if (resultInfo.getProductList().isEmpty() || resultInfo.getProductList() == null)
			{
				result.reject("product.error");
				view = "searchProductwithdeals";
			}

			resultInfo.setSearchType("producthotdeal");
			view = "searchProductwithdeals";

			session.setAttribute("seacrhList", resultInfo);

			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "searchproductwithdeal.htm");

			session.setAttribute("pagination", objPage);
			// below code is used for highlighting the tabs: 0 means inactive
			// and 1 means Active
			request.setAttribute("dealTabActive", "0");
			request.setAttribute("prodcTabActive", "1");

		}
		catch (ScanSeeServiceException e)
		{

			LOG.error("Exception occurred in searchProductWithDeals:::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		return new ModelAndView(view);
	}

	@RequestMapping(value = "/searchdeal.htm", method = RequestMethod.POST)
	public ModelAndView searchDeals(@ModelAttribute("productinfoform") ProductVO productInfo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SearchService searchService = (SearchService) appContext.getBean("searchService");

		SearchForm objForm = null;
		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";
		String view = null;

		if (null != pageFlag && pageFlag.equals("true"))
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			searchType = objForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}

		if (objForm == null)
		{
			objForm = (SearchForm) session.getAttribute("searchFormDeals");
			zipCode = objForm.getZipCode();
			searhKey = objForm.getSearchKey();
			searchType = objForm.getSearchType();
			if (loginUser != null)
			{
				if (loginUser.getUserType() == 1 || loginUser.getUserType() == 2)
				{
					searchType = "products";
				}

			}

			objForm = new SearchForm(zipCode, searhKey, searchType);
			 session.setAttribute("searchForm", objForm);
		//	request.setAttribute("searchForm", objForm);
		}

		/*
		 * Pagination check
		 */

		try
		{
			SearchResultInfo resultInfo = searchService.searchDeals(loginUser, objForm, lowerLimit);

			resultInfo.setSearchType("hotdeal");
			/*
			 * if(resultInfo.getProductList().isEmpty()||
			 * resultInfo.getProductList() == null) {
			 * result.reject("product.error"); view = "searchProductwithdeals";
			 * }
			 */
			view = "searchProductwithdeals";

			session.setAttribute("seacrhList", resultInfo);

			Pagination objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "searchdeal.htm");

			session.setAttribute("pagination", objPage);
			request.setAttribute("dealTabActive", "1");
			request.setAttribute("prodcTabActive", "0");
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in searchDeals:::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		return new ModelAndView(view);
	}

}

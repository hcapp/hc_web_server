package supplier.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AccountType;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.ContactType;
import common.pojo.DropDown;
import common.pojo.HotDealInfo;
import common.pojo.ManageProducts;
import common.pojo.PlanInfo;
import common.pojo.Product;
import common.pojo.ProductAttributes;
import common.pojo.ProductInfoVO;
import common.pojo.Rebates;
import common.pojo.Retailer;
import common.pojo.RetailerInfo;
import common.pojo.SearchResultInfo;
import common.pojo.SearchZipCode;
import common.pojo.State;
import common.pojo.SupplierProfile;
import common.pojo.SupplierQRCodeData;
import common.pojo.SupplierQRCodeResponse;
import common.pojo.SupplierRegistrationInfo;
import common.pojo.TimeZones;
import common.pojo.Users;

/**
 * DAO layer of Supplier module.
 * 
 * @author Created by SPAN.
 */
public interface SupplierDAO
{
	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password and its status return to service layer.
	 * 
	 * @param userName as request parameter.
	 * @param password as request parameter.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 * @return AuthenticateUser object.
	 */
	Users loginAuthentication(String userName, String password) throws ScanSeeWebSqlException;

	/**
	 * This DAO method save supplier register details and its status return to service layer.
	 * 
	 * @param supplierRegistrationInfoObj instance of SupplierRegistrationInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */

	String supplierRegistration(SupplierRegistrationInfo supplierRegistrationInfoObj) throws ScanSeeWebSqlException;

	/**
	 * This DAO method add new rebates for Supplier and its status return to service layer.
	 * 
	 * @param objRebates instance of Rebates.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	String addRebates(Rebates objRebates) throws ScanSeeWebSqlException;

	/**
	 * This method update Rebates information.
	 * 
	 * @param objRebates
	 * @return objRebates instance of Rebates.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */

	public String updateRebates(Rebates objRebates) throws ScanSeeWebSqlException;

	/**
	 * This method will return the RebatesNames List based on parameter. This
	 * method will return the product Names List based on parameter not yet
	 * implement
	 * 
	 * @param strRebateName
	 * @return RebatesNames List based on parameter.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public List<Rebates> getRebatesByRebateName(String strRebateName) throws ScanSeeWebSqlException;

	/**
	 * This DAO method return Rebates info based on input parameter by calling DAO method.
	 * 
	 * @param userId as request parameter.
	 * @param strRebName as request parameter.
	 * @param lowerLimit as request parameter.
	 * @return SearchResultInfo instance.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	SearchResultInfo getRebatesAll(Long userId, String strRebName, int lowerLimit) throws ScanSeeWebSqlException;

	/**
	 * This DAO method update supplier register details and its status return to service layer.
	 * 
	 * @param objSupProfile instance of SupplierProfile.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	String addUpdateProfile(SupplierProfile objSupProfile) throws ScanSeeWebSqlException;

	/**
	 * This method get the searched hot deal details.
	 * 
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 * @throws Exception
	 */
	// String hotDealSearch(HotDealInfo hotDealEditObj) throws
	// ScanSeeWebSqlException, Exception;

	/**
	 * This method update hot deal information.
	 * 
	 * @param hotDealObj
	 *            As instance of HotDealInfo
	 * @return Success or failure of method execution.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	String hotDealUpdate(HotDealInfo hotDealObj) throws ScanSeeWebSqlException;

	/**
	 * This DAO method add new hot deals for Supplier and its status return to service layer.
	 * 
	 * @param supplierID as request parameter.
	 * @param dealsVo instance of HotDealInfo.
	 * @return success or failure .
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	String addHotDeal(int supplierID, HotDealInfo dealsVo) throws ScanSeeWebSqlException;

	/**
	 * This method get hot deals city state and location information based on
	 * search parameter..
	 * 
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	List<RetailerInfo> getHotdealCityStateRetailerList(String search) throws ScanSeeWebSqlException;

	/**
	 * This method insert Manage Product information.
	 * 
	 * @param objManageProducts
	 *            .
	 * @return objManageProducts instance of ManageProducts.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public String addManageProducts(ProductInfoVO objManageProduct, long userId) throws ScanSeeWebSqlException;

	/**
	 * This method will return the Product UPC or Product Name List based on
	 * parameter.
	 * 
	 * @param strProductUpcOrName
	 * @return strProductUpcOrName List based on parameter.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public SearchResultInfo getProductsByProductUpcOrName(String strProductUpcOrName, int manufacturerID, int lowerLimit)
			throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method display supplier register details and its status return to service layer.
	 * 
	 * @param userId as request parameter.
	 * @return SupplierProfile details list.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	List<SupplierProfile> getUpdateProfile(Long userId) throws ScanSeeWebSqlException;

	public ArrayList<PlanInfo> fetchAllPlanList();

	/**
	 * This method update's Manage Products information .
	 * 
	 * @param objManageProducts
	 * @return objManageProducts instance of ManageProducts.
	 * @throws Exception
	 */
	public String updateManageProducts(ManageProducts objManageProducts) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying all the states and it return to service layer.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 * @return states,List of states.
	 */
	ArrayList<State> getAllStates() throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying all the cities and it return to service layer.
	 * @param stateCode as request parameter.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 * @return cities,List of cities.
	 */
	ArrayList<City> getAllCities(String stateCode) throws ScanSeeWebSqlException;

	public List<Rebates> getRebatesForDisplay(int rebateId) throws ScanSeeWebSqlException;

	/**
	 * This DAO method return product Hot deals info based on input parameter and its status return to service layer.
	 * 
	 * @param userId as request parameter.
	 * @param pdtName as request parameter.
	 * @param lowerLimit as request parameter.
	 * @return SearchResultInfo instance.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	SearchResultInfo getDealsForDisplay(Long userId, String pdtName, int lowerLimit) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will return the all Retailer List.
	 * @return retailer,List of Retailer.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	ArrayList<Retailer> getRetailers() throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying all the retailer location(retailer address (Address1, City, State, Postal code) for products and it will calling DAO.
	 * @param retailerId as request parameter.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 * @return location,List of location.
	 */
	ArrayList<Retailer> getRetailerLoc(int retailerId) throws ScanSeeWebSqlException;

	/**
	 * This method will return the all Category List .
	 * 
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public ArrayList<Category> getAllCategory() throws ScanSeeWebSqlException;

	/**
	 * This method to get hot deals details.
	 * 
	 * @param dealId
	 *            as input parameter
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	List<HotDealInfo> getHotDealsDetails(int dealId) throws ScanSeeWebSqlException;

	/**
	 * This method upload a file containing all of your product
	 * information,product images, audio, and video files.
	 * 
	 * @param listManageProducts
	 * @return listManageProducts list of ManageProducts.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public String addBatchUploadProductsToStage(ArrayList<ManageProducts> prodList, int manufacturerID, long userId) throws ScanSeeWebSqlException;

	public String uploadAttributeToStage(ArrayList<ManageProducts> attributeList, int manufacturerID, long userId) throws ScanSeeWebSqlException;

	// public List<Product> getMasterProdAttrList()throws
	// ScanSeeWebSqlException;

	public List<Product> getProductAssociatedto(String pdtName, Long supplierID) throws ScanSeeWebSqlException;

	// public List<ProductAttributes> getMasterProdAttrList()throws
	// ScanSeeWebSqlException;

	public String saveGridProducts(List<ManageProducts> prodLis, Users user) throws ScanSeeWebSqlException;

	ArrayList<Rebates> getProductListForRebates(String searchkey, int RetailID, long retailerID, int retailerLocId) throws ScanSeeWebSqlException;

	String addRerunRebates(Rebates objRebates) throws ScanSeeWebSqlException;

	public List<ProductAttributes> getMasterProdAttrList() throws ScanSeeWebSqlException;

	public List<ProductAttributes> fetchProductAttributes(int productId, int supplierId, String type) throws ScanSeeWebSqlException;

	ArrayList<HotDealInfo> getProductListForHotDeal(String searchkey, int supplierID) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will update existing HotDeals for supplier product and its status return to service layer.
	 * 
	 * @param hotDealInfo instance of HotDealInfo.
	 * @param supplierID as request parameter.
	 * @param flag as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	String updateHotDeal(HotDealInfo hotDealInfo, Long supplierID, String flag) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will display the HotDeals deals by passing HotDealId and its status return to service layer.
	 * 
	 * @param dealID as request parameter.
	 * @return HotDealList, All the HotDeals.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	List<HotDealInfo> getHotDealByID(int dealID) throws ScanSeeWebSqlException;

	public String saveAttributes(ManageProducts manageProducts, Long userId, int supplierId) throws ScanSeeWebSqlException;

	/**
	 * This DAO method return all the retailer based on input parameter(productId) and its status return to service layer.
	 * 
	 * @param productId as request parameter.
	 * @return retailerList, All the retailers.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	ArrayList<Retailer> getRetailersForProducts(String productId) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying all the retailer location for products and its status return to service layer.
	 * @param productIDs as request parameter.
	 * @param retailID as request parameter.
	 * @throws ScanSeeServiceException as service exception.
	 * @return location,List of location.
	 */
	ArrayList<Retailer> getRetailerLocForProducts(String productId, int retLocID) throws ScanSeeWebSqlException;

	/**
	 * The DAO method return all the  product details by calling DAO.
	 * @param productName as request parameter.
	 * @param supplierId as request parameter.
	 * @return ProductList,List of products.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	List<Product> getProductUPCList(Long supplierId, String productName) throws ScanSeeWebSqlException;

	public PlanInfo getDiscount(String discountCode) throws ScanSeeWebSqlException;

	public String savePlan(List<PlanInfo> pInfoList, int manufacturerID, long userId, Integer discountplanManufacturerid)
			throws ScanSeeWebSqlException;

	/**
	 * This DAO method return all the products that are associated with supplier hot deals on input parameter(productId) and its status return to service layer.
	 * 
	 * @param productIds as request parameter.
	 * @return productList, All the products.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	List<Product> getPdtInfoForDealRerun(String productIds) throws ScanSeeWebSqlException;

	/**
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying list of TimeZones (all standard time like (Atlantic,Central, Eastern, Mountain, Pacific Standard Time) and its status return to service method.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 * @return timeZonelst,List of TimeZones.
	 */
	ArrayList<TimeZones> getAllTimeZones() throws ScanSeeWebSqlException;

	ArrayList<PlanInfo> fetchAllFeeList() throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying list of categories and its status return to service method.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 * @return categories,List of categories.
	 */
	ArrayList<Category> getAllBusinessCategory() throws ScanSeeWebSqlException;

	public List<Product> fetchProductMedia(int productId) throws ScanSeeWebSqlException;

	/**
	 * This method will return the all AccountType List .
	 * 
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public ArrayList<AccountType> getAllAcountType() throws ScanSeeWebSqlException;

	/**
	 * This method save Manufacturer Bank Information details.
	 * 
	 * @param manufacturerID
	 * @param userId
	 * @param objManufBankInfo
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public String ManufBankInfo(Long manufacturerID, Long userId, String isPaymentType, PlanInfo objPlanInfo) throws ScanSeeWebSqlException;

	public List<ManageProducts> fetchProductPreviewList(Long productId) throws ScanSeeWebSqlException;

	public String addProductMedia(ManageProducts productInfoVO, long userId, int supplierId, int productId) throws ScanSeeWebSqlException;

	public ArrayList<AppConfiguration>  fetchReportServerConfiguration() throws ScanSeeWebSqlException;

	public List<Rebates> getProductList(String productName, int supplierId) throws ScanSeeWebSqlException;

	public List<Rebates> getProductInformation(int rebateId) throws ScanSeeWebSqlException;

	/**
	 * The DAO method return if product name is empty then display product image path and its status return to service method.
	 * @param productName as request parameter.
	 * @param supplierId as request parameter.
	 * @return prodList,List of products.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	List<HotDealInfo> getProductInfoDetails(Long supplierId, String productName) throws ScanSeeWebSqlException;

	public String moveAttributesDataStagingToProductionTable(ManageProducts manageProducts, int manufacturerID, long userId, String productFileName,
			String attributeFileName) throws ScanSeeWebSqlException;

	public String moveProductDataStagingToProductionTable(ManageProducts objMgProducts, int manufacturerID, long userId, String productFileName,
			String attributeFileName) throws ScanSeeWebSqlException;

	public List<Rebates> getProdImage(String scancodes) throws ScanSeeWebSqlException;

	public ArrayList<ContactType> getAllContactTypes() throws ScanSeeWebSqlException;

	public Users changePassword(Long userId, String password) throws ScanSeeWebSqlException;

    /**
	 * The DAO method takes the username and send the Forgot Password credential return to service layer.
	 * @param strUsername as request parameter.
	 * @param objUsers as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	String forgotPwd(String strUsername, Users objUsers) throws ScanSeeWebSqlException;

	/**
	 * This method is used to get Rejected records from the Products Attributes
	 * upload File.
	 * 
	 * @param iLogId
	 * @param iManufactureId
	 * @param lUserId
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public List<ManageProducts> getDiscardedRecordsForAttributes(long lUserId, int iManufacturerID, ManageProducts objMgProducts)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to get Rejected records from the Products upload
	 * File.
	 * 
	 * @param iLogId
	 * @param iManufactureId
	 * @param lUserId
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public List<ManageProducts> getDiscardedRecordsForProducts(long lUserId, int iManufacturerID, ManageProducts objMgProducts)
			throws ScanSeeWebSqlException;

	/**
	 * The dao Method is used to save the Supplier QR code
	 * 
	 * @param SupplierQRCodeData
	 * @return SupplierQRCodeResponse
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public SupplierQRCodeResponse supplierQRCodeGeneration(SupplierQRCodeData objSupplierQRCodeData) throws ScanSeeWebSqlException;

	/**
	 * This method is used to deleted all the Products Attributes records in the
	 * staging table.
	 * 
	 * @param iManufacture
	 * @param iUserID
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public String deleteAllFromProductAttributesStageTable(int iManufacture, long iUserID) throws ScanSeeWebSqlException;

	/**
	 * This method is used to deleted all the Products records in the staging
	 * table.
	 * 
	 * @param iManufacture
	 * @param iUserID
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public String deleteAllFromProductsStageTable(int iManufacture, long iUserID) throws ScanSeeWebSqlException;

	/**
	 * This method is used to domain Name for Application configuration
	 * 
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 */
	public ArrayList<AppConfiguration> getAppConfigForGeneralImages(String configType) throws ScanSeeWebSqlException;

	public Integer getStageTableLogId(int manufacturerID, Long userId, String fileName) throws ScanSeeWebSqlException;

	public String insertDiscardedProductse(ArrayList<ManageProducts> prodList, int manufacturerID, long userId, Integer logId, String fileName)
			throws ScanSeeWebSqlException;

	public String insertDiscardedAttributes( ManageProducts manageProducts, int manufacturerID, long userId,Integer logID) throws ScanSeeWebSqlException;
	
	public Integer getAttributStageTableLogId(int manufacturerID, Long userId, String fileName) throws ScanSeeWebSqlException;

	public Map<String, Map<String, String>> getSupplierDiscountPlans() throws ScanSeeWebSqlException;
	
	public List<SearchZipCode> getZipStateCity(String zipCode) throws ScanSeeWebSqlException;

	public List<SearchZipCode> getCityStateZip(String zipCode)throws ScanSeeWebSqlException;
	/**
	 * The DAO method for displaying all the states and it return to service layer.
	 * @throws ScanSeeWebSqlException as SQL Exception will be thrown.
	 * @return states,List of states.
	 */
	ArrayList<State> getState(String state) throws ScanSeeWebSqlException;
	public ArrayList<DropDown> displayDropDown() throws ScanSeeWebSqlException;
}

package retailer.controller;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerSearchService;

import common.exception.ScanSeeServiceException;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.Utility;

@Controller
@RequestMapping("/header.htm")
public class RetailerSearchController
{

	RetailerSearchService retailerSearchService;
	private static final Logger LOG = LoggerFactory.getLogger(RetailerSearchController.class);

	@Autowired
	public void setRetailerSearchService(RetailerSearchService retailerSearchService)
	{
		this.retailerSearchService = retailerSearchService;
	}

	@RequestMapping(value = "/header.htm", method = RequestMethod.POST)
	public ModelAndView searchProduct(@ModelAttribute("retailerproductinfoform") ProductVO productInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{
		session.removeAttribute("seacrhList");
		//session.removeAttribute("pagination");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerSearchService retailerSearchService = (RetailerSearchService) appContext.getBean("retailerSearchService");
		SearchForm objForm = null;
		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";

		/*
		 * String zipCode = "91303"; String productName =
		 * "Coolpix S4000 Compact Camera";
		 */

		/*
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 */
		if (null != pageFlag && pageFlag.equals("true"))
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			searchType = objForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			} else {
				lowerLimit = 0;
			}
		}

		if (objForm == null)
		{
			zipCode = (String) request.getParameter("zipCode");
			searhKey = (String) request.getParameter("searchKey");
			searchType = (String) request.getParameter("searchType");
			if (loginUser != null)
			{
				if (loginUser.getUserType() == 1 || loginUser.getUserType() == 2 || loginUser.getUserType() == 3)
				{
					searchType = "products";
				}
			}
			// request.setAttribute("searchKey", searchType);

			objForm = new SearchForm(zipCode, searhKey, searchType);
			if (null != searhKey)
			{
				objForm.setSearchKey(searhKey.trim());
			} else {
				objForm.setSearchKey(searhKey);
			}
			session.setAttribute("searchForm", objForm);
		}

		/*
		 * Pagination check
		 */
		Pagination objPage = new Pagination();
		try
		{
			SearchResultInfo resultInfo = retailerSearchService.searchProducts(loginUser, objForm, lowerLimit);
			if (searchType.equals("products"))
			{
				resultInfo.setSearchType("products");
			} else {
				resultInfo.setSearchType("deals");
			}
			if (resultInfo.getProductList().isEmpty() || resultInfo.getProductList() == null)
			{
				result.reject("product.error");
				objPage.setTotalSize(0);
				session.setAttribute("pagination", objPage);
				session.setAttribute("seacrhList", resultInfo);
				return new ModelAndView("retailerSearchProduct");
			}
			session.setAttribute("seacrhList", resultInfo);
			 objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "header.htm");
			session.setAttribute("pagination", objPage);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in  searchProduct:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView("retailerSearchProduct");
	}
}

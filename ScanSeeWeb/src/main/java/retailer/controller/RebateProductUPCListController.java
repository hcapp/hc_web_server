package retailer.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.exception.ScanSeeServiceException;
import common.pojo.Product;
import common.pojo.Rebates;
import common.pojo.Users;

@Controller
@RequestMapping("/rebateretprodupclist.htm")
public class RebateProductUPCListController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RebateProductUPCListController.class);

	final public String RETURN_VIEW = "productUpc";

	@RequestMapping(method = RequestMethod.GET)
	public String showProductRebatePage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{

		LOG.info("Inside RebateProductUPCListController : showProductRebatePage");
		session.removeAttribute("rebateprodupclist");
		Rebates product = new Rebates();
		model.put("retprodupclistForm", product);
		LOG.info("showProductRebatePage:: Inside Exit Get Method");
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/rebateretprodupclist.htm", method = RequestMethod.POST)
	public ModelAndView searchResult(@ModelAttribute("retprodupclistForm") Rebates productInfo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside RebateProductUPCListController : searchResult");
		List<Rebates> rebateprodupclist = null;
		ArrayList<Product> pdtInfoList = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

		String productName = productInfo.getProductName().trim();
		LOG.info("productName*************" + productName);
		int retailerLocId = productInfo.getRetailerLocID();
		LOG.info("retailerLocId*************" + retailerLocId);
		Long retailID = null;
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());
		request.removeAttribute("message");
		try
		{
			rebateprodupclist = retailerService.getRebateProductToAssociate(productName, retailID, retailerLocId);

			pdtInfoList = (ArrayList<Product>) request.getSession().getAttribute("pdtInfoList");
			if (pdtInfoList != null && !pdtInfoList.isEmpty())
			{
				for (int i = 0; i < pdtInfoList.size(); i++)
				{

					for (int j = 0; j < rebateprodupclist.size(); j++)
					{
						String selProdID = String.valueOf(pdtInfoList.get(i).getProductID());
						String prodID = rebateprodupclist.get(j).getProductID();
						if (selProdID.equals(prodID))
						{
							rebateprodupclist.remove(j);
						}
					}
				}
			}

			if (rebateprodupclist != null && rebateprodupclist.size() > 0)
			{
				session.setAttribute("rebateprodupclist", rebateprodupclist);
			}
			else
			{
				request.setAttribute("message", "Product not found !!!");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in searchResult:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView(RETURN_VIEW);
	}
}

package retailer.controller;

import java.net.Authenticator.RequestorType;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.Tutorial;

@Controller
public class SetupTutorialController {
	
	

	private static final Logger LOG = Logger.getLogger(SetupTutorialController.class);
	
	@RequestMapping(value="/tutorial.htm",method=RequestMethod.GET)
	public ModelAndView getTutorialHome(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	
	{
		final String strMethodName  ="getTutorialHome";
		final String strViewName = "tutorial";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		Category tutorial = null;
		
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) webApplicationContext.getBean(ApplicationConstants.RETAILERSERVICE);
		
		
		tutorial = retailerService.fetchTutorials();
		
		if(null != tutorial)
		{
			if(null != tutorial.getCreateTutorialLst() && ! tutorial.getCreateTutorialLst().isEmpty())
			{
				session.setAttribute("createtutoriallst", tutorial.getCreateTutorialLst());
			}
			if(null != tutorial.getGetStrtTutorialLst() && ! tutorial.getGetStrtTutorialLst().isEmpty())
			{
				session.setAttribute("getstrttutoriallst", tutorial.getGetStrtTutorialLst());
			}
			
			if(null != tutorial.getNewFeatTutorialLst() && ! tutorial.getNewFeatTutorialLst().isEmpty())
			{
				session.setAttribute("newfeattutoriallst", tutorial.getNewFeatTutorialLst());
			}
			
			if(null != tutorial.getTipsTechTutorialLst() && ! tutorial.getTipsTechTutorialLst().isEmpty())
			{
				session.setAttribute("tiptechtutoriallst", tutorial.getTipsTechTutorialLst());
			}
			
			
			
			
		}
		
		
		
		
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}
	
	
	
	

}

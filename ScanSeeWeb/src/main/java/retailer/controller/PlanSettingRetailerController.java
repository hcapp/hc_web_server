/**
 * @ (#) PlanSettingRetailerController.java 04-April-2012
 * Project       :ScanSeeWeb
 * File          : PlanSettingRetailerController.java
 * Author        : Kumar. D 
 * Company       : Span Systems Corporation
 * Date Created  : 04-April-2012
 *
 * @author       :  Kumar
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */


package retailer.controller;


import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.PlanInfo;

/**
 * PlanSettingRetailerController is a controller class for view plan details of product name, description, Quantity and Price.
 * 
 * @author Created by SPAN.
 */
@Controller
public class PlanSettingRetailerController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PlanSettingRetailerController.class);
	/**
	 * This controller method will display plan details of Product name, Description, Quantity and Price from service method and DAO method.
	 * 
	 * @param request as request parameter.
	 * @param response as request parameter.
	 * @param session as request parameter.
	 * @param model as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/planSettingsRet.htm", method = RequestMethod.GET)
	public final ModelAndView planSettingRet(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside PlanSettingRetailerController : planSettingRet ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		model.addAttribute("retplansettingform", new PlanInfo());
		return new ModelAndView("PlanSettingRet");
	}
}

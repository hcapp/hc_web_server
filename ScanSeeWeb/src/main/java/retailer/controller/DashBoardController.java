/**
 * @ (#) DashBoardController.java 29-Dec-2011
 * Project       :ScanSeeWeb
 * File          : DashBoardController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 29-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import retailer.service.RetailerService;
import supplier.service.SupplierService;
import common.constatns.ApplicationConstants;
import common.dashBoard.ReportManager;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.DashBoard;
import common.pojo.DropDown;
import common.pojo.ReportConfigInfo;
import common.pojo.RetailerProfile;
import common.pojo.Users;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

@Controller
public class DashBoardController
{
	/**
	 * Getting the logger Instance.
	 */
	final public String RETURN_VIEW = "retailerdashboard";
	final public static String HOME_VIEW = "retailerhome";
	final public static String APP_VIEW = "appsiteview";
	final private String MODULE_NAME = "Retailer";
	private static final Logger LOG = LoggerFactory.getLogger(DashBoardController.class);

	@RequestMapping(value = "/dashboard.htm", method = RequestMethod.GET)
	public String showRetailerOverviewReport(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{
		final String methodName = "showRetailerOverviewReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		Calendar now = Calendar.getInstance();
		DashBoard dashBoard = new DashBoard();
		String toDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);

		now.add(Calendar.MONTH, -1);

		String fromDate = (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DATE) + "/" + now.get(Calendar.YEAR);
		Users user = (Users) session.getAttribute("loginuser");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;
		session.removeAttribute("reportConfigInfo");
		// Set Report Parameters
		dashBoard.setSsrsReportName("Retailer");
		dashBoard.setFromDate(fromDate);
		dashBoard.setToDate(toDate);
		dashBoard.setId(String.valueOf(user.getRetailerId()));
		dashBoard.setModuleName(MODULE_NAME);

		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");

		session.setAttribute("overviewTabStyle", "active");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "");
		session.setAttribute("couponDealTabStyle", "");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display:block;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display: none;");
		session.setAttribute("couponDealDivStyle", "display: none;");
		session.setAttribute("interestDivStyle", "display: none;");

		if (null == leftNav)
		{
			leftNav = new RetailerLeftNavigation();

		}
		leftNav = UtilCode.setRetailerModulesStyle("Analytics", null, leftNav);
		session.setAttribute("retlrLeftNav", leftNav);
		try
		{
			reportConfigInfo = supplierService.fetchReportConfiguration();

			session.setAttribute("reportConfigInfo", reportConfigInfo);

			reportContent = reportManager.getOverviewReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);

			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showRetailerOverviewReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/dashboard.htm", method = RequestMethod.POST)
	public String showRetailerOverviewReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		final String methodName = "showRetailerOverviewReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;
		Users user = (Users) session.getAttribute("loginuser");
		session.setAttribute("overviewTabStyle", "active");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "");
		session.setAttribute("couponDealTabStyle", "");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display:block;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display: none;");
		session.setAttribute("couponDealDivStyle", "display: none;");
		session.setAttribute("interestDivStyle", "display: none;");

		// Set Report Parameters
		dashBoard.setSsrsReportName("Retailer");
		dashBoard.setId(String.valueOf(user.getRetailerId()));
		dashBoard.setModuleName(MODULE_NAME);

		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		if (null == leftNav)
		{
			leftNav = new RetailerLeftNavigation();

		}
		leftNav = UtilCode.setRetailerModulesStyle("Analytics", null, leftNav);
		session.setAttribute("retlrLeftNav", leftNav);
		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}

			reportContent = reportManager.getOverviewReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);

			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showRetailerOverviewReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/retailerhome.htm", method = RequestMethod.GET)
	public String showRetailerHome(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{

		final String methodName = "showRetailerHome";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		RetailerProfile retailerProfile = null;
		List<Category> appsiteHomelst=null;
		Boolean isBand = null;
		try{
			
			//Adding code for displaying retailer name.
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users user = (Users) session.getAttribute("loginuser");
		if (leftNav == null)
		{
			leftNav = new RetailerLeftNavigation();
		}
		String appsite = request.getParameter("appsite");
		
		if(null != user)
		{
			isBand =user.getIsBandRetailer();
			
		}
		
		retailerProfile = 	retailerService.getRetailerProfile(user, isBand);
		if(null != retailerProfile)
		{
		session.setAttribute("businessName", retailerProfile.getRetailerName());
		}
		
		
		if (Utility.checkNullString(appsite) != "")
		{
			
			if(null != retailerProfile)
			{
			appsiteHomelst =	retailerService.fetchAppsiteHome(user.getRetailerId());
			
			session.setAttribute("appsitehome", appsiteHomelst);
			}
			
			
			leftNav = UtilCode.setRetailerModulesStyle("MyAppsite", null, leftNav);
			return APP_VIEW;
		}
		else
		{
			leftNav = UtilCode.setRetailerModulesStyle("Home", null, leftNav);
		}

	
		}catch(ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showDemographyReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		session.setAttribute("retlrLeftNav", leftNav);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return HOME_VIEW;
	}

	@RequestMapping(value = "/getdemographicsrep.htm", method = RequestMethod.POST)
	public String showDemographyReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		final String methodName = "showDemographyReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		Users user = (Users) session.getAttribute("loginuser");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;

		session.setAttribute("overviewTabStyle", "");
		session.setAttribute("demographicsTabStyle", "active");
		session.setAttribute("appSiteTabStyle", "");
		session.setAttribute("couponDealTabStyle", "");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display: none;");
		session.setAttribute("demographicsDivStyle", "display:block;");
		session.setAttribute("appSiteDivStyle", "display: none;");
		session.setAttribute("couponDealDivStyle", "display: none;");
		session.setAttribute("interestDivStyle", "display: none;");

		// Set Report Parameters
		dashBoard.setSsrsReportName("RetailerDemographics");
		dashBoard.setId(String.valueOf(user.getRetailerId()));
		dashBoard.setModuleName(MODULE_NAME);

		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		if (null == leftNav)
		{
			leftNav = new RetailerLeftNavigation();

		}
		leftNav = UtilCode.setRetailerModulesStyle("Analytics", null, leftNav);
		session.setAttribute("retlrLeftNav", leftNav);
		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}

			ArrayList<DropDown> reportDropdownList = retailerService.displaySupplierRetailerDemographicsDD();
			if (null == dashBoard.getModuleId())
			{
				dashBoard.setModuleId(String.valueOf(reportDropdownList.get(0).getId()));
			}
			reportContent = reportManager.getDemographicsReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);
			session.setAttribute("reportDropdownList", reportDropdownList);
			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showDemographyReport:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/getappsitereport.htm", method = RequestMethod.POST)
	public String showAppSiteReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{

		final String methodName = "showAppSiteReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;

		Users user = (Users) session.getAttribute("loginuser");

		session.setAttribute("overviewTabStyle", "");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "active");
		session.setAttribute("couponDealTabStyle", "");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display: none;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display:block;");
		session.setAttribute("couponDealDivStyle", "display: none;");
		session.setAttribute("interestDivStyle", "display: none;");

		// Set Report Parameters
		dashBoard.setSsrsReportName("Retailer-Appsite");
		dashBoard.setId(String.valueOf(user.getRetailerId()));
		dashBoard.setModuleName(MODULE_NAME);

		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		if (null == leftNav)
		{
			leftNav = new RetailerLeftNavigation();

		}
		leftNav = UtilCode.setRetailerModulesStyle("Analytics", null, leftNav);
		session.setAttribute("retlrLeftNav", leftNav);
		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}

			reportContent = reportManager.getRetailerAppSiteReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);

			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showDashBoard:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}
	@RequestMapping(value = "/gethdcouponreport.htm", method = RequestMethod.POST)
	public String showHDCouponReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{

		final String methodName = "showHDCouponReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String reportContent = null;
		ReportManager reportManager = new ReportManager();
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;

		Users user = (Users) session.getAttribute("loginuser");

		session.setAttribute("overviewTabStyle", "");
		session.setAttribute("demographicsTabStyle", "");
		session.setAttribute("appSiteTabStyle", "");
		session.setAttribute("couponDealTabStyle", "active");
		session.setAttribute("interestTabStyle", "");

		session.setAttribute("overviewDivStyle", "display: none;");
		session.setAttribute("demographicsDivStyle", "display: none;");
		session.setAttribute("appSiteDivStyle", "display: none;");		
		session.setAttribute("couponDealDivStyle", "display:block;");
		session.setAttribute("interestDivStyle", "display: none;");

		// Set Report Parameters
		dashBoard.setSsrsReportName("Retailer-CouponsHotDeals");
		dashBoard.setId(String.valueOf(user.getRetailerId()));
		dashBoard.setModuleName(MODULE_NAME);

		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		if (null == leftNav)
		{
			leftNav = new RetailerLeftNavigation();

		}
		leftNav = UtilCode.setRetailerModulesStyle("Analytics", null, leftNav);
		session.setAttribute("retlrLeftNav", leftNav);
		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}

			reportContent = reportManager.getRetailerCouponDealReport(dashBoard, reportConfigInfo);

			session.setAttribute("reportContent", reportContent);

			model.put("dashboardform", dashBoard);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showDashBoard:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return RETURN_VIEW;
	}

	@RequestMapping(value = "/exportreport.htm", method = RequestMethod.POST)
	public void exportReport(@ModelAttribute("dashboardform") DashBoard dashBoard, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException, IOException
	{

		final String methodName = "exportReport";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String downloadFilePath = null;
		ReportManager reportManager = new ReportManager();
		Users user = (Users) session.getAttribute("loginuser");
		String reportName = null;
		String fromDate = dashBoard.getFromDate();
		String toDate = dashBoard.getToDate();
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		ReportConfigInfo reportConfigInfo = null;
		String fileExtension = null;
		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		if (null == leftNav)
		{
			leftNav = new RetailerLeftNavigation();

		}
		leftNav = UtilCode.setRetailerModulesStyle("Analytics", null, leftNav);
		session.setAttribute("retlrLeftNav", leftNav);
		try
		{
			reportConfigInfo = (ReportConfigInfo) session.getAttribute("reportConfigInfo");

			if (null == reportConfigInfo)
			{
				reportConfigInfo = supplierService.fetchReportConfiguration();
			}

			if (null != dashBoard.getReportName())
			{

				if (dashBoard.getReportName().equals("Overview"))
				{
					reportName = "Retailer";
					downloadFilePath = reportManager.exportReport(reportName, String.valueOf(user.getRetailerId()), fromDate, toDate,
							dashBoard.getExportFormat(), MODULE_NAME, reportConfigInfo);
				}
				else if (dashBoard.getReportName().equals("AppSite"))
				{
					reportName = "Retailer-Appsite";
					downloadFilePath = reportManager.exportReport(reportName, String.valueOf(user.getRetailerId()), fromDate, toDate,
							dashBoard.getExportFormat(), MODULE_NAME, reportConfigInfo);
				}
				else if (dashBoard.getReportName().equals("Demographics"))
				{
					reportName = "RetailerDemographics";

					downloadFilePath = reportManager.exportModuleReport(reportName, String.valueOf(user.getRetailerId()), fromDate, toDate,
							dashBoard.getExportFormat(), dashBoard.getModuleId(), MODULE_NAME, reportConfigInfo);

				}
				else if (dashBoard.getReportName().equals("Coupon&Deals"))
				{
					reportName = "Retailer-CouponsHotDeals";

					downloadFilePath = reportManager.exportReport(reportName, String.valueOf(user.getRetailerId()), fromDate, toDate,
							dashBoard.getExportFormat(), MODULE_NAME, reportConfigInfo);

				}
			}

			if (dashBoard.getExportFormat().equals("PDF"))
			{
				fileExtension = ".pdf";
			}
			else if (dashBoard.getExportFormat().equals("EXCEL"))
			{
				fileExtension = ".xls";
			}

			response.sendRedirect("/ScanSeeWeb/fileController/downloadexportfile.htm?" + "fileFromat=" + dashBoard.getExportFormat() + "&fileName="
					+ reportName + fileExtension + "&filePath=" + downloadFilePath);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showDashBoard:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
	}
	

}

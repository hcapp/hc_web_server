/**
 * @ (#) BatchUploadRetProdController.java 05-Jan-2012
 * Project       :ScanSeeWeb
 * File          : BatchUploadRetProdController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 05-Jan-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;
import supplier.controller.LoginController;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailProduct;
import common.pojo.Users;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

@Controller
@RequestMapping("/batchuploadretprod.htm")
public class BatchUploadRetProdController
{

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "batchuploadretprod";
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showbatchUploadPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException
	{

		LOG.info("Controller Layer:: Inside Get Method");

		RetailProduct retailProduct = new RetailProduct();
		model.put("batchUploadretprodform", retailProduct);
		
		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		leftNav = UtilCode.setRetailerModulesStyle("ManageProducts", null, leftNav);
		session.setAttribute("retlrLeftNav",leftNav);
		
		LOG.info("Controller Layer:: Exit Get Method");
		return VIEW_NAME;

	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView saveBatchUpload(@ModelAttribute("batchUploadretprodform") RetailProduct retailProduct, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{

		String isDataInserted = null;
		String isDataMovedToProdTab = null;
		String strLocationContent = null;
		String autoGenKey = null;
		final boolean bKeyStatus = Utility.isKeySame(session, retailProduct.getKey());
		if (bKeyStatus) 
		{
			try
			{
				ServletContext servletContext = request.getSession().getServletContext();
				WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
				RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
				Users loginUser = (Users) request.getSession().getAttribute("loginuser");
				int retailtID = loginUser.getRetailerId();
				long userId = loginUser.getUserID();
				String strLogoImage = retailerService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;

				String productFileName = retailProduct.getProductuploadFilePath().getOriginalFilename();
				LOG.info("Retailer ID*************" + retailtID);
				LOG.info("userId*************" + userId);
				LOG.info("productFileName*************" + productFileName);
				String filename = request.getRealPath(retailtID + ".csv");
				CommonsMultipartFile productuploadFilePath = retailProduct.getProductuploadFilePath();
				long productFileSize = productuploadFilePath.getSize();
				LOG.info("File Size********" + productFileSize);
				// Checking File Size,size should be less than 1MB
				if (productFileSize <= (1024 * 1024))
				{
					retailProduct.setDicardedProductsCSVFilePath(filename);
					isDataInserted = retailerService.saveBatchFile(retailProduct, retailtID, userId);
					if (isDataInserted.equals(ApplicationConstants.SUCCESS))
					{
						result.rejectValue("productuploadFilePath", "Products Uploaded Successfully", "   Products Uploaded Successfully");
						request.setAttribute("productuploadFile", "font-weight:bold;color:green;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					}
					else if (isDataInserted.equals(ApplicationConstants.RECORDSDISCARDED))
					{
						result.rejectValue("productuploadFilePath", "Products Uploaded Partially, Please check your mail box for Rejected Products.",
						"   Products Uploaded Partially, please check your mail box for Rejected Products.");
						request.setAttribute("productuploadFile", "font-weight:bold;color:green;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					}
					else
					{
						result.rejectValue("productuploadFilePath", "   Error Occurred while uploading Products",
						"   Error Occurred while uploading Products");
						request.setAttribute("productuploadFile", "font-weight:bold;color:red;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					}
				}
				else
				{
					result.rejectValue("productuploadFilePath", "Maximum size of uploaded file should be less than 1MB",
					"  Maximum size of uploaded file should be less than 1MB");
					request.setAttribute("productuploadFile", "font-weight:bold;color:red;");
				}

				LOG.info("isDataInserted********************" + isDataInserted);
				LOG.info(ApplicationConstants.METHODEND + "saveBatchUpload");
			}
			catch (ScanSeeServiceException exception)
			{
				LOG.error("Error occurred in uploadProductFile", exception);
				result.rejectValue("productuploadFilePath", "Error Occurred while uploading Products", "  Error Occurred while uploading Products");
				request.setAttribute("productuploadFile", "font-weight:bold;color:red;");
				return new ModelAndView(VIEW_NAME);
			}
		}
		return new ModelAndView(VIEW_NAME);
	}
}

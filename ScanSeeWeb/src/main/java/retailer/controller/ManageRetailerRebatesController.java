package retailer.controller;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.exception.ScanSeeServiceException;
import common.pojo.Rebates;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

@Controller
@RequestMapping("/rebatesretailMfg.htm")
public class ManageRetailerRebatesController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageRetailerRebatesController.class);

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "retailerdisplayrebate";

	/**
	 * Variable VIEW_NAME declared as String.
	 */
	String view = VIEW_NAME;
	
	/**
	 * Debugger Log flag.
	 */
	private static final boolean ISDUBUGENABLED = LOG.isDebugEnabled();

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showPage(@ModelAttribute("RebatesearchForm") Rebates rebatesVo,HttpServletRequest request, HttpSession session,ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String sDate = null;
		String eDate = null;
		int lowerLimit = 0;
		//Rebates rebatesVo = new Rebates();
		LOG.info("Controller Layer:: ManageRetailerRebatesController:Inside Get Method");
		try {
		// session.invalidate();
		request.getSession().removeAttribute("message");
		request.getSession().removeAttribute("listOfRebates");
		session.removeAttribute("pagination");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		
		String pageFlag = (String) request.getParameter("pageFlag");

		if (null != pageFlag && pageFlag.equals("true")) 
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0) 
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			} else 
			{
				lowerLimit = 0;
			}

		}

		if (objForm == null)
		{
			searhKey = (String) request.getParameter("searchKey");
			objForm = new SearchForm();
			objForm.setSearchKey(searhKey);
			session.setAttribute("searchform", objForm);
		}
		Users user = (Users) session.getAttribute("loginuser");
		if(ISDUBUGENABLED){
			LOG.debug("Supplier ID****" + user.getSupplierId());
			}
		//Users user = (Users) request.getSession().getAttribute("loginuser");
		rebatesVo.setRebName(null);
		//List<Rebates> listOfRebates = null;
		SearchResultInfo listOfRebates =  retailerService.getRetailerByRebateName(Long.valueOf(user.getRetailerId()),
					objForm,lowerLimit);
		
		
			if (listOfRebates != null ) 
			{
				for(int i = 0; i < listOfRebates.getRebatesList().size(); i++)
				{
					String ammount = listOfRebates.getRebatesList().get(i).getRebAmount();
					ammount = Utility.formatDecimalValue(ammount);
					listOfRebates.getRebatesList().get(i).setRebAmount(ammount);
					
					sDate = listOfRebates.getRebatesList().get(i).getRebStartDate();
					sDate = Utility.getUsDateFormat(sDate);
					listOfRebates.getRebatesList().get(i).setRebStartDate(sDate);

					eDate = listOfRebates.getRebatesList().get(i).getRebEndDate();
					eDate = Utility.getUsDateFormat(eDate);
					listOfRebates.getRebatesList().get(i).setRebEndDate(eDate);
				}
				request.getSession().setAttribute("listOfRebates",listOfRebates);
				Pagination objPage = Utility.getPagination(
						listOfRebates.getTotalSize(), currentPage,
						"/ScanSeeWeb/retailer/rebatesretailMfg.htm");
				session.setAttribute("pagination", objPage);
			}else{
				request.getSession().setAttribute("message","No More Rebates To Display");
			}
			
			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("Promotions", "Rebates", leftNav);
			session.setAttribute("retlrLeftNav",leftNav);
			
		
		} catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in showPage:::::"+ e.getMessage());
            throw new ScanSeeServiceException(e);
		}catch (NullPointerException exception)
		{
			LOG.error("Null pointer Exception occurred::ManageRebatesController:GetMethod:" + exception.getMessage());
		} catch (ParseException exception) {
			LOG.error("Parse error occurred Inside ManageRebatesController :: " + exception.getMessage());
		}
		LOG.info("Controller Layer:: ManageRetailerRebatesController:Exit Get Method");
		//model.put("searchForm", rebatesVo);
		return new ModelAndView(VIEW_NAME);
	}

	
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView getRebatesByName(@ModelAttribute("RebatesearchForm") Rebates rebatesVo,BindingResult result, HttpServletRequest request,HttpServletResponse response, ModelMap model, HttpSession session)
			throws IOException,ScanSeeServiceException 
			{
		LOG.info("Controller Layer:: ManageRetailerRebatesController:Inside POST Method");
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		Pagination objPage = null;
		String sDate = null;
		String eDate = null;
		
		request.getSession().removeAttribute("message");
		request.getSession().removeAttribute("listOfRebates");
		//session.removeAttribute("pagination");
		
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
	try 
		{
		String pageFlag = (String) request.getParameter("pageFlag");

		if (null != pageFlag && pageFlag.equals("true")) 
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0) 
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			} else 
			{
				lowerLimit = 0;
			}

		}

		if (objForm == null)
		{
			searhKey = (String) request.getParameter("searchKey");
			objForm = new SearchForm();
			objForm.setSearchKey(searhKey);
			session.setAttribute("searchform", objForm);
		}
		Users user = (Users) session.getAttribute("loginuser");
		if(ISDUBUGENABLED){
			LOG.debug("Supplier ID****" + user.getRetailerId());
			LOG.debug("searchKey***" + searhKey);
			}
		//Users user = (Users) request.getSession().getAttribute("loginuser");
		if(rebatesVo.getRebName()!= null)
		{
		String rebName = rebatesVo.getRebName().trim();
		objForm.setSearchKey(rebName);
		}
		//objForm.setSearchKey(rebatesVo.getRebName());
		
		SearchResultInfo listOfRebates =  retailerService.getRetailerByRebateName(Long.valueOf(user.getRetailerId()),
				objForm,lowerLimit);
				
		if(listOfRebates.getRebatesList().size() != 0){
			if (listOfRebates.getRebatesList() != null || !listOfRebates.getRebatesList().isEmpty() ) 
			{
				for(int i = 0; i < listOfRebates.getRebatesList().size(); i++)
				{
					String ammount = listOfRebates.getRebatesList().get(i).getRebAmount();
					ammount = Utility.formatDecimalValue(ammount);
					listOfRebates.getRebatesList().get(i).setRebAmount(ammount);
					
					sDate = listOfRebates.getRebatesList().get(i).getRebStartDate();
					sDate = Utility.getUsDateFormat(sDate);
					listOfRebates.getRebatesList().get(i).setRebStartDate(sDate);

					eDate = listOfRebates.getRebatesList().get(i).getRebEndDate();
					eDate = Utility.getUsDateFormat(eDate);
					listOfRebates.getRebatesList().get(i).setRebEndDate(eDate);
				}
				request.getSession().setAttribute("listOfRebates",listOfRebates);
				 objPage = Utility.getPagination(
							listOfRebates.getTotalSize(), currentPage,
							"/ScanSeeWeb/retailer/rebatesretailMfg.htm");
				
				 session.setAttribute("pagination", objPage);
				///view = VIEW_NAME;
			}
		}else
			{
			result.reject("Rebates.status");
			request.setAttribute("manageRbtssFont", "font-weight:regular;color:red;");
			session.removeAttribute("pagination");
				//request.getSession().setAttribute("message","No Matching Rebates Found");
				//view = VIEW_NAME;
			}
		
		}catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in getRebatesByName:::::"+ e.getMessage());
            throw new ScanSeeServiceException(e);
		}catch (ParseException exception) {
			LOG.error("Parse error occurred Inside ManageRebatesController : getRebatesByName : " + exception.getMessage());
			throw new ScanSeeServiceException(exception);
		}
		LOG.info("Controller Layer::ManageRetailerRebatesController: Exit Post Method");

		return new ModelAndView(VIEW_NAME);
	}

}

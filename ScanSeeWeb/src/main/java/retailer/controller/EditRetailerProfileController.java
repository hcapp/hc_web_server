package retailer.controller;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.RetailerProfileValidator;
import supplier.service.SupplierService;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.GAddress;
import common.pojo.RetailerProfile;
import common.pojo.State;
import common.pojo.Users;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * EditRetailerProfileController is a controller class for Edit Retailer
 * registration screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class EditRetailerProfileController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(EditRetailerProfileController.class);

	/**
	 * Variable retailerProfileValidator declared as instance of
	 * RetailerProfileValidator.
	 */
	private RetailerProfileValidator retailerProfileValidator;

	/**
	 * Variable city declared as String.
	 */
	private final String supplierCity = "supplierCity";

	/**
	 * Variable commas declared as String.
	 */
	private final String commas = ",";
	/**
	 * Variable DUPLICATE_USER_VIEW declared as String.
	 */
	private final String duplicateUserView = "updateRetailerProfile";

	/**
	 * To set retailerProfileValidator.
	 * 
	 * @param retailerProfileValidator
	 *            to set.
	 */
	@Autowired
	public final void setRetailerProfileValidator(RetailerProfileValidator retailerProfileValidator)
	{
		this.retailerProfileValidator = retailerProfileValidator;
	}

	/**
	 * This controller method will get one Retailer Registration record from
	 * service layer based on input parameter.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param session1
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/editRetailerProfile.htm", method = RequestMethod.GET)
	public final String showPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{
		LOG.info("Inside EditRetailerProfileController : showPage ");
		RetailerProfile profileInfo1 = new RetailerProfile();

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
		final Users user = (Users) request.getSession().getAttribute("loginuser");
		ArrayList<State> states = null;
		ArrayList<Category> arBCategoryList = null;
		RetailerLeftNavigation leftNav = null;
		String stateName = null;
		Boolean isBand = null;
		try
		{

			arBCategoryList = retailerService.getAllRetailerProfileBusinessCategory();
			
			if(null != user)
			{
				isBand =user.getIsBandRetailer();
				
			}
			
			profileInfo1 = retailerService.getRetailerProfile(user, isBand);
			states = supplierService.getState(profileInfo1.getState());
			for (State state : states)
			{
				stateName = state.getStateName();
			}
			profileInfo1.setContactPhone(Utility.getPhoneFormate(profileInfo1.getContactPhone()));
			profileInfo1.setContactPhoneNo(Utility.getPhoneFormate(profileInfo1.getContactPhoneNo()));
			profileInfo1.setCityHidden(profileInfo1.getCity());
			profileInfo1.setStateHidden(stateName);
			profileInfo1.setStateCodeHidden(profileInfo1.getState());
			profileInfo1.setState(stateName);
			profileInfo1.setNonProfit(profileInfo1.isNonProfit());
			// profileInfo1.setIsLocation(profileInfo1.getIsLocation());
			profileInfo1.setIslocation(profileInfo1.isIslocation());
			profileInfo1.setIshiddenlocation(profileInfo1.isIslocation());
			profileInfo1.setbCategory(profileInfo1.getbCategory());
			profileInfo1.setbCategoryHidden(profileInfo1.getbCategory());
			profileInfo1.setWebUrl(profileInfo1.getWebUrl());
			profileInfo1.setNumOfStores(profileInfo1.getNumOfStores());
			profileInfo1.setKeyword(profileInfo1.getKeyword());

			profileInfo1.setAddress1(profileInfo1.getAddress1() != null ? profileInfo1.getAddress1().trim() : null);
			profileInfo1.setAddress2(profileInfo1.getAddress2() != null ? profileInfo1.getAddress2().trim() : null);
			session.setAttribute("statesList", states);
			session.setAttribute("categoryList", arBCategoryList);

			session.setAttribute("beforeEditRetProfile", profileInfo1);
			model.put("editretailerprofileform", profileInfo1);

			leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			if (null == leftNav)
			{
				leftNav = new RetailerLeftNavigation();

			}
			leftNav = UtilCode.setRetailerModulesStyle("UpdateAccounts", null, leftNav);

			session.setAttribute("retlrLeftNav", leftNav);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return "editRetailerProfile";
	}

	/**
	 * This controller method will return the City List based on input
	 * parameter.
	 * 
	 * @param stateCode
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param city
	 *            as request parameter.
	 * @param tabIndex
	 *            as request parameter.
	 * @return City List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/editretailerfetchcity", method = RequestMethod.GET)
	@ResponseBody
	public final String getCities(@RequestParam(value = "statecode", required = true) String stateCode,
			@RequestParam(value = "tabIndex", required = true) String tabIndex, @RequestParam(value = "city", required = true) String city,
			HttpServletRequest request, HttpServletResponse response) throws ScanSeeServiceException
	{
		LOG.info("Inside EditRetailerProfileController : getCities ");
		ArrayList<City> cities = null;
		final StringBuffer innerHtml = new StringBuffer();
		final String finalHtml = "<select name='city' tabindex=" + tabIndex
				+ " id='City' onchange='getCityTrigger(this);'><option value='0'>--Select--</option>";
		innerHtml.append(finalHtml);
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
			cities = supplierService.getAllCities(stateCode);
			response.setContentType("text/xml");
			String selCity = (String) request.getSession().getAttribute(supplierCity);
			if (selCity == null || "".equals(selCity))
			{
				selCity = city;
			}
			String cityName = null;
			for (int i = 0; i < cities.size(); i++)
			{
				cityName = cities.get(i).getCityName();
				if (null != selCity && cityName.equals(selCity))
				{
					innerHtml.append("<option selected=true  value=\"" + cities.get(i).getCityName() + "\">" + cities.get(i).getCityName()
							+ "</option>");
				}
				else
				{
					innerHtml.append("<option value=\"" + cities.get(i).getCityName() + "\">" + cities.get(i).getCityName() + "</option>");
				}
			}
			innerHtml.append("</select>");
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return innerHtml.toString();
	}

	/**
	 * This controller method edit/Update Retailer register details by calling
	 * service method.
	 * 
	 * @param retailerProfile
	 *            instance of RetailerProfile.
	 * @param result
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/editRetailerProfile.htm", method = RequestMethod.POST)
	public final ModelAndView updateProfile(@ModelAttribute("editretailerprofileform") RetailerProfile retailerProfile, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws Exception
	{
		LOG.info("Inside EditRetailerProfileController : updateProfile ");
		final Users user = (Users) request.getSession().getAttribute("loginuser");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		// retailerProfile.setSupProfileID(Long.valueOf(user.getSupplierId()));

		String isProfileUpdated = "";
		final String view = "editRetailerProfile";
		boolean isValidCorpPhoneNum = true;
		boolean isValidContactNum = true;
		boolean isValidEmail = true;
		String strBCategory = null;
		boolean isValidUrl = true;
		boolean bGAddress = false;
		//String strGeoAddress = null;
		final double latLng = 0.0;
		// String view = null;
		try
		{
			retailerProfile.setState(retailerProfile.getStateCodeHidden());
			isValidCorpPhoneNum = Utility.validatePhoneNum(retailerProfile.getContactPhone());
			isValidEmail = Utility.validateEmailId(retailerProfile.getContactEmail());
			isValidContactNum = Utility.validatePhoneNum(retailerProfile.getContactPhoneNo());
			strBCategory = retailerProfile.getbCategory();
			/*
			 * Update Account: �Is Location� check box is getting deselected,
			 * when the user updates the Account after missing few mandatory
			 * fields.
			 */
			retailerProfile.setIslocation(retailerProfile.isIshiddenlocation());
			if (!"".equals(Utility.checkNull(retailerProfile.getWebUrl())))
			{
				isValidUrl = Utility.validateURL(retailerProfile.getWebUrl());
			}
			retailerProfileValidator.validateEditProfile(retailerProfile, result);
			if (!isValidCorpPhoneNum)
			{
				retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.INVALIDCORPPHONE);
			}
			if (!isValidEmail)
			{
				retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.INVALIDEMAIL);
			}
			if (!isValidContactNum)
			{
				retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.INVALIDCONTPHONE);
			}
			if (null == strBCategory)
			{
				retailerProfile.setbCategoryHidden(null);
				retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.BUSINESSCATEGORY);
			}
			// if (retailerProfile.getState().equalsIgnoreCase("0"))
			// {
			// retailerProfileValidator.validate(retailerProfile, result,
			// ApplicationConstants.STATE);
			// }
			// if (retailerProfile.getCity().equalsIgnoreCase("--Select--") ||
			// retailerProfile.getCity().equalsIgnoreCase("0"))
			// {
			// retailerProfileValidator.validate(retailerProfile, result,
			// ApplicationConstants.CITY);
			// }
			if (!isValidUrl)
			{
				retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.WEBURL);
			}
			
			if (!"".equals(Utility.checkNull(retailerProfile.getGeoError()))) {
				if (null == retailerProfile.getRetailerLocationLatitude()) {
					retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.LATITUDE);
				}
				if (null == retailerProfile.getRetailerLocationLongitude()) {
					retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.LONGITUDE);
				}
			}
			if (result.hasErrors()) {
				return new ModelAndView(view);
			} else {
				retailerProfile.setRetailID(user.getRetailerId());
				if ("".equals(Utility.checkNull(retailerProfile.getGeoError()))) {
					/* Latitude and Longitude coordinates for retailer */
					final RetailerProfile objBeforeEditRetProfile = (RetailerProfile) session.getAttribute("beforeEditRetProfile");
					objBeforeEditRetProfile.setState(objBeforeEditRetProfile.getStateCodeHidden());
					if (!objBeforeEditRetProfile.getAddress1().trim().equalsIgnoreCase(retailerProfile.getAddress1().trim())
							|| !objBeforeEditRetProfile.getState().equals(retailerProfile.getState())
							|| !objBeforeEditRetProfile.getCity().equals(retailerProfile.getCity())
							|| !objBeforeEditRetProfile.getPostalCode().equals(retailerProfile.getPostalCode()))
					{
						bGAddress = true;
					}
					if (bGAddress)
					{
						/*strGeoAddress = Utility.checkNull(retailerProfile.getAddress1().trim().toUpperCase()) + commas + retailerProfile.getState()
							+ commas + retailerProfile.getCity() + commas + retailerProfile.getPostalCode();*/
						String strAddress = Utility.checkNull(retailerProfile.getAddress1().trim()) + commas + retailerProfile.getCity() + commas +  retailerProfile.getState();
						/*	final GAddress objGAddress = Utility.geocode(strGeoAddress);
					if (!"".equals(Utility.checkNull(objGAddress)))
					{
						retailerProfile.setRetailerLocationLongitude(objGAddress.getLng());
						retailerProfile.setRetailerLocationLatitude(objGAddress.getLat());
					}
					else
					{
						retailerProfile.setRetailerLocationLongitude(latLng);
						retailerProfile.setRetailerLocationLatitude(latLng);
					}
						 */
						final GAddress objGAddress = UtilCode.getGeoDetails(strAddress);
						// If user provide valid address or zipcode.
						if (!"".equals(Utility.checkNull(objGAddress))) {
							if ("OK".equals(objGAddress.getStatus()) && "ROOFTOP".equals(objGAddress.getLocationType())) {
								retailerProfile.setRetailerLocationLongitude(objGAddress.getLng());
								retailerProfile.setRetailerLocationLatitude(objGAddress.getLat());
							} else {
								LOG.error("Could not find Lat and Lang for the address:" + strAddress);
								request.setAttribute("GEOERROR", ApplicationConstants.GEOERROR);
								retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.GEOERROR);
								retailerProfile.setRetailerLocationLongitude(null);
								retailerProfile.setRetailerLocationLatitude(null);
								if(result.hasErrors()){
									return new ModelAndView(view);
								}
							}
						} else {
							// If user provide invalid address or zipcode for Geocode
							// Address (Lat/Lng) is null and default value is initialized to
							// 0.0 .
							retailerProfile.setRetailerLocationLongitude(latLng);
							retailerProfile.setRetailerLocationLatitude(latLng);
						}
					} else {
						retailerProfile.setRetailerLocationLongitude(objBeforeEditRetProfile.getRetailerLocationLongitude());
						retailerProfile.setRetailerLocationLatitude(objBeforeEditRetProfile.getRetailerLocationLatitude());
					}
				}
				/* End */
				retailerProfile.setState(retailerProfile.getStateCodeHidden());
				isProfileUpdated = retailerService.updateProfile(retailerProfile);
				if (isProfileUpdated != null && isProfileUpdated.equalsIgnoreCase("success"))
				{
					session.removeAttribute(supplierCity);
					// update location latitude and longitude fields from google
					// maps
					if (retailerProfile.isIslocation())
					{
						final String locationCoordinates = request.getParameter("locationCoordinates");
						retailerService.addCorpAndStoreLocationCoordinates(locationCoordinates, retailerProfile.getRetailerName());
					}
				//	return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/dashboard.htm"));
					return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/retailerhome.htm"));
				}
				else if (isProfileUpdated != null && isProfileUpdated.equals(ApplicationConstants.DUPLICATE_RETAILERNAME))
				{
					retailerProfileValidator.validate(retailerProfile, result, ApplicationConstants.DUPLICATE_RETAILERNAME);
					if (isProfileUpdated != null && isProfileUpdated.equals(ApplicationConstants.FAILURE))
					{
						LOG.info("Inside EditRetailerProfileController : EditRetailerProfileController : Failure or Problem Occured ");
					}
				}
				else
				{
					request.setAttribute("message", "Error occured while Updating User Profile");
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			request.setAttribute("message", "Error occured while Updating User Profiler");
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(view);
	}
	
	/**
	 * Controller to associate filter
	 * 
	 * @param objCategory
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/associatefilter.htm", method = RequestMethod.GET)
	public final ModelAndView associateFilter(@ModelAttribute("associateFilters") Category objCategory, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session)	{
		LOG.info("Inside EditRetailerProfileController : associateFilter() ");
		
		return new ModelAndView("associatefilters");
	}
	
	@RequestMapping(value = "/fetchfilters", method = RequestMethod.GET)
	@ResponseBody
	public final String fetchFilters(@RequestParam(value = "filterIds", required = true) String filterIds, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside EditRetailerProfileController : fetchFilters ");
		ArrayList<Category> filters = null;
		StringBuffer filterStr = null;
		
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			
			filters = (ArrayList<Category>) retailerService.fetchFilters(filterIds);
			
			if(null != filters && !filters.isEmpty()) {
				int i = 0;
				Boolean subFilterFlag = false;
				Long categoryId = filters.get(0).getBusinessCategoryID();
				String categoryName = filters.get(0).getBusinessCategoryName();
				Integer filterId = filters.get(0).getFilterID();
				String filterName = filters.get(0).getFilterName();
				filterStr = new StringBuffer();
				filterStr.append("{\"categories\": [{\"id\":" + categoryId + ",\"Name\":\"" + categoryName + "\",\"filters\": [{\"id\":" + filterId + ",\"Name\":\"" + filterName + "\"");
				
				for(Category category : filters) {
					if(categoryName.equals(category.getBusinessCategoryName())) {
						if(filterName.equals(category.getFilterName())) {
							if(null != category.getFilterValueID() && !"".equals(category.getFilterValueID())) {
								if(i == 0) {
									filterStr.append(",\"subfilters\":[{\"id\":" + category.getFilterValueID() + ",\"Name\":\"" + category.getFilterValueName() + "\"}");
									subFilterFlag = true;
									i++;
								} else {
									filterStr.append(",{\"id\":" + category.getFilterValueID() + ",\"Name\":\"" + category.getFilterValueName() + "\"}");
								}
							}
						} else {
							i = 0;							
							filterId = category.getFilterID();
							filterName = category.getFilterName();
							if(subFilterFlag != false) {
								filterStr.append("]");
							}
							subFilterFlag = false;
							filterStr.append("},{\"id\":" + filterId + ",\"Name\":\"" + filterName + "\"");
							if(null != category.getFilterValueID() && !"".equals(category.getFilterValueID())) {
								filterStr.append(",\"subfilters\":[{\"id\":" + category.getFilterValueID() + ",\"Name\":\"" + category.getFilterValueName() + "\"}");
								subFilterFlag = true;
								i++;
							}
						}
					} else {
						i = 0;
						categoryId = category.getBusinessCategoryID();
						categoryName = category.getBusinessCategoryName();
						filterId = category.getFilterID();
						filterName = category.getFilterName();
						if(subFilterFlag != false) {
							filterStr.append("]");
						}
						subFilterFlag = false;
						filterStr.append("}]},{\"id\":" + categoryId + ",\"Name\":\"" + categoryName + "\",\"filters\": [{\"id\":" + filterId + ",\"Name\":\"" + filterName + "\"");
						if(null != category.getFilterValueID() && !"".equals(category.getFilterValueID())) {
							filterStr.append(",\"subfilters\":[{\"id\":" + category.getFilterValueID() + ",\"Name\":\"" + category.getFilterValueName() + "\"}");
							subFilterFlag = true;
							i++;
						}
					}
				}
				
				if(subFilterFlag != false) {
					filterStr.append("]");
				}
				filterStr.append("}]}]}");			
			}
			
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if(null != filterStr) {
			return filterStr.toString();
		} else {
			return "";
		}
		
	}
	
	@RequestMapping(value = "/fetchsubcats", method = RequestMethod.GET)
	@ResponseBody
	public final String fetchSubCategories(@RequestParam(value = "catIds", required = true) String catIds, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside EditRetailerProfileController : fetchSubCategories ");
		ArrayList<Category> subCategories = null;
		StringBuffer subCatStr = null;
		
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			
			subCategories = (ArrayList<Category>) retailerService.fetchSubCategories(catIds);
			
			if(null != subCategories && !subCategories.isEmpty()) {
				int i = 0;
				Long categoryId = subCategories.get(0).getBusinessCategoryID();
				String categoryName = subCategories.get(0).getBusinessCategoryName();
				subCatStr = new StringBuffer();
				subCatStr.append("{\"categories\": [{\"id\":" + categoryId + ",\"Name\":\"" + categoryName + "\",\"subCategories\": [");
				
				for(Category category : subCategories) {
					if(categoryName.equals(category.getBusinessCategoryName())) {
						if(i == 0) {
							subCatStr.append("{\"id\":" + category.getSubCategoryID() + ",\"Name\":\"" + category.getSubCategoryName() + "\"}");							
						} else {
							subCatStr.append(",{\"id\":" + category.getSubCategoryID() + ",\"Name\":\"" + category.getSubCategoryName() + "\"}");
						}
						i++;
					} else {
						i = 0;
						categoryId = category.getBusinessCategoryID();
						categoryName = category.getBusinessCategoryName();
						subCatStr.append("]},{\"id\":" + categoryId + ",\"Name\":\"" + categoryName + "\",\"subCategories\": [");
					}
				}
				
				subCatStr.append("]}]}");			
			}
			
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if(null != subCatStr) {
			return subCatStr.toString();
		} else {
			return "";
		}
		
	}
	
	/**
	 * Controller to associate filter
	 * 
	 * @param objCategory
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/associatesubcats.htm", method = RequestMethod.GET)
	public final ModelAndView associateSubCats(@ModelAttribute("associateSubCats") Category objCategory, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session)	{
		LOG.info("Inside EditRetailerProfileController : associateFilter() ");
		
		return new ModelAndView("associatesubcats");
	}
	
}

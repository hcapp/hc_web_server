/**
 * @ (#) ManageBannerAdsController.java 08-Aug-2011
 * Project       : ScanSeeWeb
 * File          : ManageBannerAdsController.java
 * Author        : Kumar D
 * Company       : Span Systems Corporation
 * Date Created  : 08-Aug-2011
 */

package retailer.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * ManageBannerAdsController is a controller class for Managing all the Banner
 * Page created by retailer.
 * 
 * @author Created by SPAN.
 */
@Controller
public class ManageBannerAdsController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageBannerAdsController.class);

	/**
	 * Variable RETURN_VIEW declared as constant string.
	 */
	private final String returnView = "managebannerview";

	/**
	 * Debugger Log flag.
	 */
	private final boolean isDebugEnable = LOG.isDebugEnabled();

	/**
	 * This controller method will return the Banner details list.
	 * 
	 * @param addsVo
	 *            instance of RetailerLocationAdvertisement.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/managebannerads.htm", method = RequestMethod.GET)
	public final ModelAndView showAllBanner(@ModelAttribute("managebanneradform") RetailerLocationAdvertisement addsVo, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageBannerAdsController : showAllBanner");
		SearchResultInfo adList = null;
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String sDate = null;
		String eDate = null;
		int lowerLimit = 0;
		session.removeAttribute("ChangeImageDim");
		int recordCount = 20;
		try
		{
			session.removeAttribute(ApplicationConstants.PAGINATION);
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != addsVo.getRecordCount() && !"".equals(addsVo.getRecordCount()) && !"undefined".equals(addsVo.getRecordCount()))
			{
				recordCount = Integer.valueOf(addsVo.getRecordCount());
			}
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
				}

				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));
			}
			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			final Users loginUser = (Users) session.getAttribute(ApplicationConstants.LOGINUSER);
			Boolean isBand = loginUser.getIsBandRetailer();
			if (isDebugEnable)
			{
				LOG.debug("Retailer Id*********" + loginUser.getRetailerId());
				LOG.debug("searchKey*********" + searhKey);
			}

			final Long retailID = Long.valueOf(loginUser.getRetailerId());
			addsVo.setAdvertisementName(null);
			adList = retailerService.getBannerAdsByAdsNames(retailID, objForm, lowerLimit, recordCount, isBand);
			// to Set selection in left navigation.
			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("MyAppsite", "Banner", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);

			if (adList != null && adList.getAddsList().size() <= 0)
			{
				return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/buildbannerad.htm"));
			}
			else if (adList != null)
			{
				for (int i = 0; i < adList.getAddsList().size(); i++)
				{
					sDate = adList.getAddsList().get(i).getAdvertisementDate();
					/*
					 * sDate = Utility.convertDBdate(sDate);
					 * adList.getAddsList().get(i).setAdvertisementDate(sDate);
					 */
					addsVo.setDbAdvertisementStartDate(sDate);
					adList.getAddsList().get(i).setAdvertisementDate(addsVo.getDbAdvertisementStartDate());

					if (!"".equals(adList.getAddsList().get(i).getAdvertisementEndDate()))
					{
						eDate = adList.getAddsList().get(i).getAdvertisementEndDate();
						/*
						 * eDate = Utility.convertDBdate(eDate);
						 * adList.getAddsList
						 * ().get(i).setAdvertisementEndDate(eDate);
						 */
						addsVo.setDbAdvertisementEndDate(eDate);
						adList.getAddsList().get(i).setAdvertisementEndDate(addsVo.getDbAdvertisementEndDate());
					}
				}
				session.setAttribute("adsList", adList);
				final Pagination objPage = Utility.getPagination(adList.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/managebannerads.htm",
						recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
			else
			{
				request.setAttribute("message", "No records found!");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(returnView);
	}

	@RequestMapping(value = "/managebannerads.htm", method = RequestMethod.POST)
	public ModelAndView onSubmitSearch(@ModelAttribute("managebanneradform") RetailerLocationAdvertisement retailerLocationAdvertisement,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside ManageBannerAdsController : onSubmitSearch ");
		SearchResultInfo adList = null;
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String sDate = null;
		String eDate = null;
		int lowerLimit = 0;
		int recordCount = 20;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != retailerLocationAdvertisement.getRecordCount() && !"".equals(retailerLocationAdvertisement.getRecordCount())
					&& !"undefined".equals(retailerLocationAdvertisement.getRecordCount()))
			{
				recordCount = Integer.valueOf(retailerLocationAdvertisement.getRecordCount());
			}
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
			
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
				}

				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));
			}
			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			final Users loginUser = (Users) session.getAttribute(ApplicationConstants.LOGINUSER);
			Boolean isBand = loginUser.getIsBandRetailer();
			if (isDebugEnable)
			{
				LOG.debug("Retailer Id*********" + loginUser.getRetailerId());
				LOG.debug("searchKey*********" + searhKey);
			}
			final Long retailID = Long.valueOf(loginUser.getRetailerId());
			if (!"".equals(Utility.checkNull(retailerLocationAdvertisement.getAdvertisementName())))
			{
				final String adsName = Utility.checkNull(retailerLocationAdvertisement.getAdvertisementName());
				objForm.setSearchKey(adsName);
			}

			adList = retailerService.getBannerAdsByAdsNames(retailID, objForm, lowerLimit, recordCount, isBand);
			if (!adList.getAddsList().isEmpty() && adList != null)
			{
				for (int i = 0; i < adList.getAddsList().size(); i++)
				{
					sDate = adList.getAddsList().get(i).getAdvertisementDate();
					sDate = Utility.convertDBdate(sDate);
					adList.getAddsList().get(i).setAdvertisementDate(sDate);
					if (!"".equals(adList.getAddsList().get(i).getAdvertisementEndDate()))
					{
						eDate = adList.getAddsList().get(i).getAdvertisementEndDate();
						retailerLocationAdvertisement.setDbAdvertisementEndDate(eDate);
						adList.getAddsList().get(i).setAdvertisementEndDate(retailerLocationAdvertisement.getDbAdvertisementEndDate());
					}
				}
				session.setAttribute("adsList", adList);
				final Pagination objPage = Utility.getPagination(adList.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/managebannerads.htm",
						recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
			else
			{
				// result.reject("searchAds.status");
				// request.setAttribute("manageAdsFont",
				// "font-weight:regular;color:red;");
				session.setAttribute("adsList", adList);
				session.removeAttribute(ApplicationConstants.PAGINATION);
				request.setAttribute("message", "No Banner Name matching for the Given String");
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (NullPointerException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(returnView);
	}

	/**
	 * This controller method will delete Banner Page based on input parameter
	 * (bannerId) by calling service method.
	 * 
	 * @param objretLocAd
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/deleteBannerPage.htm", method = RequestMethod.POST)
	public final ModelAndView deleteBannerPage(@ModelAttribute("managebanneradform") RetailerLocationAdvertisement objretLocAd,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageBannerAdsController : deleteBannerPage ");
		String strResponseStatus = null;
		SearchResultInfo adList = null;
		final int iCurrentPage = 1;
		final SearchForm objForm = new SearchForm();
		String sDate = null;
		String eDate = null;
		final int iLowerLimit = 0;
		int recordCount = 20;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final Users loginUser = (Users) session.getAttribute(ApplicationConstants.LOGINUSER);
			Boolean isBand = loginUser.getIsBandRetailer();
			objretLocAd.setRetailID(Long.valueOf(loginUser.getRetailerId()));

			strResponseStatus = retailerService.deleteBannerPage(objretLocAd, isBand);

			if (null != objretLocAd.getRecordCount() && !"".equals(objretLocAd.getRecordCount()) && !"undefined".equals(objretLocAd.getRecordCount()))
			{
				recordCount = Integer.valueOf(objretLocAd.getRecordCount());
			}
			if (strResponseStatus.equals(ApplicationConstants.SUCCESS))
			{
				LOG.info("Inside ManageBannerAdsController : deleteBannerPage : Deleted Successfull !!!!");
				adList = retailerService.getBannerAdsByAdsNames(objretLocAd.getRetailID(), objForm, iLowerLimit, recordCount, isBand);
				if (adList != null && adList.getAddsList().size() <= 0)
				{
					return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/buildbannerad.htm"));
				}
				else if (adList != null)
				{
					for (int i = 0; i < adList.getAddsList().size(); i++)
					{
						sDate = adList.getAddsList().get(i).getAdvertisementDate();
						sDate = Utility.convertDBdate(sDate);
						adList.getAddsList().get(i).setAdvertisementDate(sDate);
						if (!"".equals(adList.getAddsList().get(i).getAdvertisementEndDate()))
						{
							eDate = adList.getAddsList().get(i).getAdvertisementEndDate();
							objretLocAd.setDbAdvertisementEndDate(eDate);
							adList.getAddsList().get(i).setAdvertisementEndDate(objretLocAd.getDbAdvertisementEndDate());
						}
					}
					session.setAttribute("adsList", adList);
					final Pagination objPage = Utility.getPagination(adList.getTotalSize(), iCurrentPage, "/ScanSeeWeb/retailer/managebannerads.htm",
							recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				}
				if (objretLocAd.getExpireFlag() == 0 && objretLocAd.getDeleteFlag() == 1)
				{
					request.setAttribute("message", "Banner Page is deleted Successfully!!!");
				}
				else if (objretLocAd.getDeleteFlag() == 0 && objretLocAd.getExpireFlag() == 1)
				{
					request.setAttribute("message", "Banner page campaign has been stopped successfully!!!");
				}
			}
			else
			{
				if (objretLocAd.getExpireFlag() == 0 && objretLocAd.getDeleteFlag() == 1)
				{
					request.setAttribute("message", "Error occurred while deleting the banner page!");
				}
				else if (objretLocAd.getDeleteFlag() == 0 && objretLocAd.getExpireFlag() == 1)
				{
					request.setAttribute("message", "Error occurred while stop compaigning the banner page!");
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(returnView);
	}

	/**
	 * This controller method will return location details info associated with
	 * Banner page from Database based on parameter(adsIDs)..
	 * 
	 * @param objretLocAd
	 *            instance of RetailerLocationAdvertisement.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws IOException
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/showBannerLocation.htm", method = RequestMethod.GET)
	public final ModelAndView showLocation(@ModelAttribute("showLocationForm") RetailerLocationAdvertisement objretLocAd, BindingResult result,
			Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside ManageBannerAdsController : showLocation ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users loginUser = (Users) session.getAttribute(ApplicationConstants.LOGINUSER);
		Boolean isBand = loginUser.getIsBandRetailer();
		List<RetailerLocation> arLocationList = null;
		try
		{
			arLocationList = retailerService.getLocationIDForBannerPage(objretLocAd.getRetailLocationAdvertisementID(), isBand);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.addAttribute("showLocationForm", objretLocAd);
		session.setAttribute("locationList", arLocationList);
		return new ModelAndView("showLocation");
	}
}

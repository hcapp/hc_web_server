/**
 * @ (#) ProductUPCListController.java 26-Dec-2011
 * Project       :ScanSeeWeb
 * File          : ProductUPCListController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 26-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Product;
import common.pojo.Users;

@Controller
@RequestMapping("/produpclist.htm")
public class ProductUPCListController {

	/**
	 * Getting the logger Instance.
	 */
	final public String RETURN_VIEW = "produpclist";
	private static final Logger LOG = LoggerFactory
			.getLogger(ProductUPCListController.class);

	/**
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showCreateCouponPage(HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException {

		LOG.info("showCreateCouponPage:: Inside Get Method");
		session.removeAttribute("manageproductlist");
		Product product = new Product();
		model.put("produpclistform", product);
		LOG.info("showCreateCouponPage:: Inside Exit Get Method");
		return RETURN_VIEW;
	}

	/**
	 * @param productInfo
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws IOException
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/produpclist.htm", method = RequestMethod.POST)
	public ModelAndView searchResult(
			@ModelAttribute("produpclistform") Product productInfo,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException,ScanSeeServiceException {
		ArrayList<Product> manageProductList = null;
		ArrayList<Product> pdtInfoList = null;
		request.getSession().removeAttribute("manageproductlist");
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext
				.getBean("retailerService");

		String productName = productInfo.getProductName().trim();
		LOG.info("productName*************" + productName);
		
		Long retailID = null;
		Users loginUser = (Users) request.getSession().getAttribute(
				"loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());
	
		try {
			manageProductList = retailerService.getProdToAssociateCoupon(
					productName, productInfo.getRetLocID(), retailID);
			
			pdtInfoList = (ArrayList<Product>) request.getSession().getAttribute("scanCodeList");
			if (pdtInfoList != null && !pdtInfoList.isEmpty())
			{
				for (int i = 0; i < pdtInfoList.size(); i++)
				{
					long prodID = pdtInfoList.get(i).getProductID();
					for (int j = 0; j < manageProductList.size(); j++)
					{
						if (prodID == manageProductList.get(j).getProductID())
						{
							manageProductList.remove(j);
						}
					}
				}
			}
			if (manageProductList != null && manageProductList.size() > 0) {
				session.setAttribute("manageproductlist", manageProductList);
			} else {
					request.setAttribute(ApplicationConstants.MESSAGE, "Product not found !!!");
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Exception occurred in searchResult:::::"+ e.getMessage());
            throw new ScanSeeServiceException(e);
		}

		return new ModelAndView(RETURN_VIEW);
	}
}

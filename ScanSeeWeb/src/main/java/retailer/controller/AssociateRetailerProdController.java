/**
 * @ (#) AssociateRetailerProdController.java 03-Jan-2012
 * Project       :ScanSeeWeb
 * File          : AssociateRetailerProdController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 03-Jan-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocationProduct;
import common.pojo.RetailerLocationProductJson;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class AssociateRetailerProdController
{
	final public String RETURN_VIEW = "addsearchprod";
	private static final Logger LOG = LoggerFactory.getLogger(AssociateRetailerProdController.class);

	@RequestMapping(value = "/associateprod.htm", method = RequestMethod.POST)
	@ResponseBody
	public String productSetupPage(@RequestParam(value = "data", required = true) String data, HttpServletRequest request, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside AssociateRetailerProdController :productSetupPage ");
		String response = ApplicationConstants.FAILURE;
		String compStartDate = null;
		String compEndDate = null;
		String compDate = null;
		Date currentDate = new Date();
		try
		{
			request.getSession().removeAttribute("message");
			request.getSession().removeAttribute("errormessage1");
			request.getSession().removeAttribute("errormessage2");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			Long retailID = Long.valueOf(loginUser.getRetailerId());

			RetailerLocationProductJson prodJson = Utility.jsonToProdInfo(data);
			RetailerLocationProduct objRetailLoctnProduct = new RetailerLocationProduct();
			objRetailLoctnProduct.setRetailID(retailID);

			if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
			{
				if (!Utility.isValidDate(prodJson.getSaleStartDate()))
				{
					response = "Please Enter Valid Start Date";
					return response;
				}
				else
				{
					compStartDate = Utility.compareCurrentDate(prodJson.getSaleStartDate(), currentDate);
					if (null != compStartDate)
					{

						response = "Start date should be greater than or equal to current date";
					}
				}

			}
			if (!prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
			{
				if (!Utility.isValidDate(prodJson.getSaleEndDate()))
				{
					response = "Please Enter Valid End Date";
					return response;
				}
				else
				{
					compEndDate = Utility.compareCurrentDate(prodJson.getSaleEndDate(), currentDate);
					if (null != compEndDate)
					{

						response = "End date should be greater than or equal to current date";
					}
				}

			}

			if (null != compStartDate && null != compEndDate)
			{
				response = "Start date and End date should be greater than or equal to current date";
				return response;

			}
			if (null != compStartDate && null != compEndDate)
			{

				if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals("")
						&& !prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
				{
					compDate = Utility.compareDate(prodJson.getSaleStartDate(), prodJson.getSaleEndDate());

					if (null != compDate)
					{

						response = "End date should be greater than or equal to Start date";

					}
				}

			}

			if (null == compStartDate && null == compEndDate && null == compDate)
			{

				if (!prodJson.getPrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getPrice())).equals(""))
				{
					objRetailLoctnProduct.setPrice(Double.valueOf(prodJson.getPrice()));
				}
				if (!prodJson.getSalePrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getSalePrice())).equals(""))
				{
					objRetailLoctnProduct.setSalePrice(Double.valueOf(prodJson.getSalePrice()));
				}
				if (!Utility.checkNull(prodJson.getDescription()).equals(""))
				{
					objRetailLoctnProduct.setRetailLocationProductDesc(prodJson.getDescription());
				}
				if (!Utility.checkNull(prodJson.getLocation()).equals(""))
				{
					objRetailLoctnProduct.setLocID(prodJson.getLocation());
				}
				if (!Utility.checkNull(prodJson.getProductID()).equals(""))
				{
					objRetailLoctnProduct.setProductID(Long.valueOf(prodJson.getProductID()));
				}
				if (!Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
				{
					objRetailLoctnProduct.setSaleStartDate(prodJson.getSaleStartDate());
				}
				if (!Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
				{
					objRetailLoctnProduct.setSaleEndDate(prodJson.getSaleEndDate());
				}
				response = retailerService.associateUpdateRetProd(objRetailLoctnProduct);
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in productSetupPage:::::" + e.getMessage());
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeServiceException(e);

		}
		if (null != response)
		{
			if (response.equals(ApplicationConstants.SUCCESS))
			{

				response = "product is added successfully for selected location(s).";
			}
		}

		return response;
	}

	@RequestMapping(value = "/updateRetLocProd.htm", method = RequestMethod.POST)
	@ResponseBody
	public String updateManageProduct(@RequestParam(value = "data", required = true) String data, HttpServletRequest request, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside AssociateRetailerProdController :updateManageProduct ");
		String response = ApplicationConstants.FAILURE;
		String compStartDate = null;
		String compEndDate = null;
		String compDate = null;
		// Date currentDate = new Date();
		try
		{
			request.getSession().removeAttribute("message");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			Long retailID = Long.valueOf(loginUser.getRetailerId());
			RetailerLocationProductJson prodJson = Utility.jsonToProdInfo(data);
			RetailerLocationProduct objRetailLoctnProduct = new RetailerLocationProduct();
			objRetailLoctnProduct.setRetailID(retailID);

			if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
			{
				if (!Utility.isValidDate(prodJson.getSaleStartDate()))
				{
					response = "Please Enter Valid Start Date";
					return response;
				}

			}
			if (!prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
			{
				if (!Utility.isValidDate(prodJson.getSaleEndDate()))
				{
					response = "Please Enter Valid End Date";
					return response;
				}

			}

			if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals("")
					&& !prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
			{
				compDate = Utility.compareDate(prodJson.getSaleStartDate(), prodJson.getSaleEndDate());

				if (null != compDate)
				{
					response = "End date should be greater than or equal to Start date";
				}
			}

			if (null == compStartDate && null == compEndDate && null == compDate)
			{

				if (!prodJson.getPrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getPrice())).equals(""))
				{
					objRetailLoctnProduct.setPrice(Double.valueOf(prodJson.getPrice()));
				}
				if (!prodJson.getSalePrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getSalePrice())).equals(""))
				{
					objRetailLoctnProduct.setSalePrice(Double.valueOf(prodJson.getSalePrice()));
				}
				if (!Utility.checkNull(prodJson.getDescription()).equals(""))
				{
					objRetailLoctnProduct.setRetailLocationProductDesc(prodJson.getDescription());
				}
				if (!Utility.checkNull(prodJson.getLocation()).equals(""))
				{
					objRetailLoctnProduct.setLocID(prodJson.getLocation());
				}
				if (!Utility.checkNull(prodJson.getProductID()).equals(""))
				{
					objRetailLoctnProduct.setProductID(Long.valueOf(prodJson.getProductID()));
				}
				if (!Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
				{
					objRetailLoctnProduct.setSaleStartDate(prodJson.getSaleStartDate());
				}
				if (!Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
				{
					objRetailLoctnProduct.setSaleEndDate(prodJson.getSaleEndDate());
				}
				response = retailerService.associateUpdateRetProd(objRetailLoctnProduct);
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AssociateRetailerProdController :updateManageProduct " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		if (response.equals(ApplicationConstants.SUCCESS))
		{
			response = "Product details updated successfully!";
		}
		return response;
	}

	/**
	 * This controller method will return Product associated with locations
	 * details info.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/addProdLocationPop.htm", method = RequestMethod.GET)
	public ModelAndView associateProdToLocation(@ModelAttribute("manageProductForm") RetailerLocationProduct objretLocAd, BindingResult result,
			Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside AssociateRetailerProdController : associateProdToLocation ");
		session.removeAttribute("isApplyToggle");
		session.removeAttribute("isSearchToggle");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		request.getSession().removeAttribute("productDetails");
		RetailerLocationProduct objRetLocationProduct = new RetailerLocationProduct();
		objRetLocationProduct.setProductID(objretLocAd.getProductID());
		objRetLocationProduct.setRetailID(Long.valueOf(loginUser.getRetailerId()));
		List<RetailerLocationProduct> arProductDetails = null;
		String strStartDate = null;
		String strEndDate = null;

		try
		{
			arProductDetails = (List<RetailerLocationProduct>) retailerService.getAssocProdToLocationPop(objRetLocationProduct);
			for (int i = 0; i < arProductDetails.size(); i++)
			{

				arProductDetails.get(i).setRowNumber(i);
				// productVO.setRowNumber(i);

				if (!"".equals(arProductDetails.get(i).getSaleStartDate()))
				{
					strStartDate = arProductDetails.get(i).getSaleStartDate();
					objRetLocationProduct.setDbSaleDate(strStartDate);
					arProductDetails.get(i).setSaleStartDate(objRetLocationProduct.getDbSaleDate());
				}
				if (!"".equals(arProductDetails.get(i).getSaleEndDate()))
				{
					strEndDate = arProductDetails.get(i).getSaleEndDate();
					objRetLocationProduct.setDbSaleDate(strEndDate);
					arProductDetails.get(i).setSaleEndDate(objRetLocationProduct.getDbSaleDate());
				}
			}
			session.setAttribute("productDetails", arProductDetails);
			session.setAttribute("productImage", objRetLocationProduct.getProductImagePath());
			session.setAttribute("productName", objRetLocationProduct.getProductName());
			// session.setAttribute("productId",
			// objRetLocationProduct.getProductID());
			/*
			 * arRetLocationList = (List<RetailerLocationProduct>)
			 * retailerService
			 * .getAllRetailerLocationList(Long.valueOf(loginUser.
			 * getRetailerId()), null); if (arRetLocationList != null &&
			 * !arRetLocationList.isEmpty()) {
			 * session.setAttribute("retailerLocationList", arRetLocationList);
			 * } else { request.setAttribute("message",
			 * "Location not found !!!"); }
			 */
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AssociateRetailerProdController : associateProdToLocation : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		model.addAttribute("manageProductForm", objRetLocationProduct);
		return new ModelAndView("addprodtolocation");
	}

	/**
	 * This controller method will return the all RetailerLocation list based on
	 * input parameter.
	 * 
	 * @param searchKey
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/fetchretaillocationlist.htm", method = RequestMethod.POST)
	public ModelAndView fetchRetailLocation(@ModelAttribute("manageProductForm") RetailerLocationProduct objRetLocProduct, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside AssociateRetailerProdController : fetchRetailLocation");
		int iSearchStatus = 1;
		int iApplyStatus = 0;
		session.removeAttribute("productDetails");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		RetailerLocationProduct objRetLocationProduct = new RetailerLocationProduct();
		objRetLocationProduct.setProductID(objRetLocProduct.getProductID());
		objRetLocationProduct.setSearchKey(Utility.checkNull(objRetLocProduct.getSearchKey()));
		objRetLocationProduct.setSearchStatus(iSearchStatus);
		objRetLocationProduct.setApplyStatus(iApplyStatus);
		objRetLocationProduct.setRetailID(Long.valueOf(loginUser.getRetailerId()));
		List<RetailerLocationProduct> arProductDetails = null;
		String strStartDate = null;
		String strEndDate = null;

		try
		{
			arProductDetails = (List<RetailerLocationProduct>) retailerService.getAssocProdToLocationPop(objRetLocationProduct);
			session.setAttribute("productImage", objRetLocationProduct.getProductImagePath());
			session.setAttribute("productName", objRetLocationProduct.getProductName());
			session.setAttribute("isSearchToggle", objRetLocationProduct.getSearchStatus());
			if (arProductDetails != null && !arProductDetails.isEmpty())
			{
				for (int i = 0; i < arProductDetails.size(); i++)
				{

					arProductDetails.get(i).setRowNumber(i);
					if (!"".equals(arProductDetails.get(i).getSaleStartDate()))
					{
						strStartDate = arProductDetails.get(i).getSaleStartDate();
						objRetLocationProduct.setDbSaleDate(strStartDate);
						arProductDetails.get(i).setSaleStartDate(objRetLocationProduct.getDbSaleDate());
					}
					if (!"".equals(arProductDetails.get(i).getSaleEndDate()))
					{
						strEndDate = arProductDetails.get(i).getSaleEndDate();
						objRetLocationProduct.setDbSaleDate(strEndDate);
						arProductDetails.get(i).setSaleEndDate(objRetLocationProduct.getDbSaleDate());
					}
				}
				session.setAttribute("productDetails", arProductDetails);

			}
			else
			{
				request.setAttribute("message", "Location not found !!!");
			}
			model.addAttribute("manageProductForm", objRetLocationProduct);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred inside LocationSetupController : fetchBatchLocationSetup:" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		return new ModelAndView("addprodtolocation");
	}

	/**
	 * This controller method will return the all RetailerLocation list based on
	 * input parameter.
	 * 
	 * @param searchKey
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/saveretaillocationlist.htm", method = RequestMethod.POST)
	public ModelAndView saveRetailLocation(@ModelAttribute("manageProductForm") RetailerLocationProduct objRetLocProduct, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside AssociateRetailerProdController : saveRetailLocation");
		session.removeAttribute("productDetails");
		session.removeAttribute("isSearchToggle");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int iSearchStatus = 0;
		int iApplyStatus = 1;
		RetailerLocationProduct objRetLocationProduct = new RetailerLocationProduct();
		objRetLocationProduct.setProductID(objRetLocProduct.getProductID());
		objRetLocationProduct.setSearchStatus(iSearchStatus);
		objRetLocationProduct.setApplyStatus(iApplyStatus);
		objRetLocationProduct.setRetailID(Long.valueOf(loginUser.getRetailerId()));
		List<RetailerLocationProduct> arProductDetails = null;
		String strStartDate = null;
		String strEndDate = null;

		try
		{
			arProductDetails = (List<RetailerLocationProduct>) retailerService.getAssocProdToLocationPop(objRetLocationProduct);
			session.setAttribute("productImage", objRetLocationProduct.getProductImagePath());
			session.setAttribute("productName", objRetLocationProduct.getProductName());
			request.setAttribute("isApplyToggle", objRetLocationProduct.getApplyStatus());
			if (arProductDetails != null && !arProductDetails.isEmpty())
			{
				for (int i = 0; i < arProductDetails.size(); i++)
				{
					if (!"".equals(arProductDetails.get(i).getSaleStartDate()))
					{
						strStartDate = arProductDetails.get(i).getSaleStartDate();
						objRetLocationProduct.setDbSaleDate(strStartDate);
						arProductDetails.get(i).setSaleStartDate(objRetLocationProduct.getDbSaleDate());
					}
					if (!"".equals(arProductDetails.get(i).getSaleEndDate()))
					{
						strEndDate = arProductDetails.get(i).getSaleEndDate();
						objRetLocationProduct.setDbSaleDate(strEndDate);
						arProductDetails.get(i).setSaleEndDate(objRetLocationProduct.getDbSaleDate());
					}
				}
				session.setAttribute("productDetails", arProductDetails);
			}
			else
			{
				request.setAttribute("message", "Location not found !!!");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred inside LocationSetupController : fetchBatchLocationSetup:" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView("addprodtolocation");
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveretlocation.htm", method = RequestMethod.POST)
	@ResponseBody
	public String retailerSetupPage(@RequestParam(value = "data", required = true) String data,
			@RequestParam(value = "search", required = true) String strSearch, HttpServletRequest request, HttpSession session)
			throws ScanSeeServiceException
	{
		LOG.info("Inside AssociateRetailerProdController : retailerSetupPage ");
		String response = ApplicationConstants.FAILURE;
		List<RetailerLocationProduct> arProductDetailSes = null;

		int iApplyAll = 1;
		String compStartDate = null;
		String compEndDate = null;
		String compDate = null;
		Date currentDate = new Date();
		Long iRetLocations = new Long(0);
		StringBuffer strLocationIds = new StringBuffer();
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			arProductDetailSes = (List<RetailerLocationProduct>) request.getSession().getAttribute("productDetails");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");

			if (strSearch.equals("submitSelected"))
			{
				RetailerLocationProduct objRetailLoctnProduct = new RetailerLocationProduct();
				objRetailLoctnProduct.setRetailID(Long.valueOf(loginUser.getRetailerId()));
				objRetailLoctnProduct.setStrRetailerLocationProductJson(data);
				response = retailerService.updateRetailerProductList(objRetailLoctnProduct);
				if (response != null && response.equals(ApplicationConstants.SUCCESS))
				{
					response = ApplicationConstants.SUCCESS;
				} else {
					response = ApplicationConstants.FAILURE;
				}
			}
			else
			{
				if ("currentSearch".equals(strSearch))
				{
					iApplyAll = 0;
					if (arProductDetailSes != null && !arProductDetailSes.isEmpty())
					{
						for (int i = 0; i < arProductDetailSes.size(); i++)
						{
							iRetLocations = arProductDetailSes.get(i).getRetailLocationID();
							strLocationIds.append(iRetLocations);
							strLocationIds.append(",");
						}
					}
				}

				if ("applyAll".equals(strSearch) || (arProductDetailSes != null && !arProductDetailSes.isEmpty()))
				{
					RetailerLocationProductJson prodJson = Utility.jsonToProdInfo(data);
					List<RetailerLocationProduct> arProductDetails = null;
					RetailerLocationProduct objRetailLoctnProduct = new RetailerLocationProduct();
					objRetailLoctnProduct.setRetailID(Long.valueOf(loginUser.getRetailerId()));
					objRetailLoctnProduct.setRetailLocationIDs(strLocationIds.toString());
					objRetailLoctnProduct.setApplyAll(iApplyAll);

					String strStartDate = null;
					String strEndDate = null;

					if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
					{
						if (!Utility.isValidDate(prodJson.getSaleStartDate()))
						{
							response = "Please Enter Valid Start Date";
							return response;
						}
						else
						{
							compStartDate = Utility.compareCurrentDate(prodJson.getSaleStartDate(), currentDate);
							if (null != compStartDate)
							{
								response = "Start date should be greater than or equal to current date";
							}
						}
					}
					if (!prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
					{
						if (!Utility.isValidDate(prodJson.getSaleEndDate()))
						{
							response = "Please Enter Valid End Date";
							return response;
						}
						else
						{
							compEndDate = Utility.compareCurrentDate(prodJson.getSaleEndDate(), currentDate);
							if (null != compEndDate)
							{
								response = "End date should be greater than or equal to current date";
							}
						}
					}
					if (null != compStartDate && null != compEndDate)
					{
						response = "Start date and End date should be greater than or equal to current date";
						return response;
					}
					if (null != compStartDate && null != compEndDate)
					{
						if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals("")
								&& !prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
						{
							compDate = Utility.compareDate(prodJson.getSaleStartDate(), prodJson.getSaleEndDate());
							if (null != compDate)
							{
								response = "End date should be greater than or equal to Start date";
							}
						}
					}
					if (null == compStartDate && null == compEndDate && null == compDate)
					{
						if (!prodJson.getPrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getPrice())).equals(""))
						{
							objRetailLoctnProduct.setPrice(Double.valueOf(prodJson.getPrice()));
						}
						if (!prodJson.getSalePrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getSalePrice())).equals(""))
						{
							objRetailLoctnProduct.setSalePrice(Double.valueOf(prodJson.getSalePrice()));
						}
						if (!Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
						{
							objRetailLoctnProduct.setSaleStartDate(prodJson.getSaleStartDate());
						}
						if (!Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
						{
							objRetailLoctnProduct.setSaleEndDate(prodJson.getSaleEndDate());
						}
						if (!Utility.checkNull(prodJson.getProductID()).equals(""))
						{
							objRetailLoctnProduct.setProductID(Long.valueOf(prodJson.getProductID()));
						}
						response = retailerService.associateUpdateRetProd(objRetailLoctnProduct);
						if (null != response)
						{
							if (response.equals(ApplicationConstants.SUCCESS))
							{
								arProductDetails = (List<RetailerLocationProduct>) retailerService.getAssocProdToLocationPop(objRetailLoctnProduct);
								if (arProductDetails != null && !arProductDetails.isEmpty())
								{
									for (int i = 0; i < arProductDetails.size(); i++)
									{
										if (!"".equals(arProductDetails.get(i).getSaleStartDate()))
										{
											strStartDate = arProductDetails.get(i).getSaleStartDate();
											objRetailLoctnProduct.setDbSaleDate(strStartDate);
											arProductDetails.get(i).setSaleStartDate(objRetailLoctnProduct.getDbSaleDate());
										}
										if (!"".equals(arProductDetails.get(i).getSaleEndDate()))
										{
											strEndDate = arProductDetails.get(i).getSaleEndDate();
											objRetailLoctnProduct.setDbSaleDate(strEndDate);
											arProductDetails.get(i).setSaleEndDate(objRetailLoctnProduct.getDbSaleDate());
										}
									}
									session.setAttribute("productDetails", arProductDetails);
								}
								response = ApplicationConstants.SUCCESS;
							}
						}
					}
				}
				else
				{
					response = "No location(s) matching your current search.";
				}
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AssociateRetailerProdController : retailerSetupPage  : Exception : " + e.getMessage());
			response = "Error While Adding product details to location(s)";
			throw new ScanSeeServiceException(e);
		}
		return response;
	}

}

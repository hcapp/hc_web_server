/**
 * @ (#) RerunCouponController.java 27-Dec-2011
 * Project       :ScanSeeWeb
 * File          : RerunCouponController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 27-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.CreateCouponValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Coupon;
import common.pojo.Product;
import common.pojo.RetailerLocation;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

/**
 * RerunCouponController is a controller class for create coupon with existing
 * coupon details.
 * 
 * @author Created by SPAN.
 */
@Controller
public class RerunCouponController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(RerunCouponController.class);

	/**
	 * Variable returnView declared as string.
	 */
	private final String returnView = "reruncoupon";

	/**
	 * Variable createCouponValidator declared as instance of
	 * CreateCouponValidator.
	 */
	private CreateCouponValidator createCouponValidator;

	/**
	 * Getting the CreateCouponValidator Instance.
	 * 
	 * @param createCouponValidator
	 *            is instance of CreateCouponValidator. createCouponValidator
	 *            the CreateCouponValidator to set.
	 */
	@Autowired
	public final void setCreateCouponValidator(
			CreateCouponValidator createCouponValidator) {
		this.createCouponValidator = createCouponValidator;
	}

	/**
	 * The controller method to show rerun coupon screen.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return returnView .
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/reruncoupon.htm", method = RequestMethod.GET)
	public String getCouponInfo(HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside RerunCouponController : getCouponInfo");
		String couponStartTime = null;
		String couponEndTime = null;
		String[] temp = null;
		String[] tempStartTimeHrsMin = null;
		String[] tempEndTimeHrsMin = null;
		String strCouponImg = null;
		session.removeAttribute("scanCodeList");
		final ArrayList<Product> scanCodeList = new ArrayList<Product>();
		try {
			int iCouponID = Integer.parseInt(request.getParameter("couponID"));
			Coupon couponinfo = null;
			final ServletContext servletContext = request.getSession()
					.getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext
					.getBean(ApplicationConstants.RETAILERSERVICE);
			ArrayList<Product> manageProductList = null;
			ArrayList<RetailerLocation> arRetLocationList = null;
			Long retailID = null;
			final Users loginUser = (Users) request.getSession().getAttribute(
					ApplicationConstants.LOGINUSER);
			retailID = Long.valueOf(loginUser.getRetailerId());
			session.setAttribute("imageCropPage", "ReRunCouponRetailer");
			
			couponinfo = retailerService.fetchCouponInfo(Long
					.valueOf(iCouponID));
			couponinfo.setRetailID(retailID);
			arRetLocationList = retailerService.getRetailerLocations(retailID);
			String tempExpiryTimeHrsMin[] = null;
			String couponExpTime = null;
			if (arRetLocationList != null) {
				session.setAttribute("retailerLocList", arRetLocationList);
			} else {
				session.setAttribute("retailerLocList", "");
			}
			if (couponinfo != null) {
				String sDate = couponinfo.getCouponStartDate();
				if (sDate != null) {
					sDate = Utility.formattedDate(sDate);
					couponinfo.setCouponStartDate(sDate);
				}
				String eDate = couponinfo.getCouponEndDate();
				if (eDate != null) {
					eDate = Utility.formattedDate(eDate);
					couponinfo.setCouponEndDate(eDate);
				}

				if (!"".equals(Utility.checkNull(couponinfo.getLocationID()))) {
					couponinfo.setRetLocationIDs(couponinfo.getLocationID());
				}
				/*String couponDiscountamt = couponinfo.getCouponDiscountAmt();
				couponDiscountamt = Utility
						.formatDecimalValue(couponDiscountamt);
				couponinfo.setCouponDiscountAmt(couponDiscountamt);*/

				couponStartTime = couponinfo.getCouponStartTime();
				tempStartTimeHrsMin = couponStartTime.split(":");
				couponinfo.setCouponStartTimeHrs(tempStartTimeHrsMin[0]);
				couponinfo.setCouponStartTimeMins(tempStartTimeHrsMin[1]);
				couponEndTime = couponinfo.getCouponEndTime();
				
				if(null != couponEndTime && !"".equals(couponEndTime))
				{
				tempEndTimeHrsMin = couponEndTime.split(":");
				}
				
				if(null != tempEndTimeHrsMin)
				{
					couponinfo.setCouponEndTimeHrs(tempEndTimeHrsMin[0]);
					couponinfo.setCouponEndTimeMins(tempEndTimeHrsMin[1]);
				}else
				{
					couponinfo.setCouponEndTimeHrs("00");
					couponinfo.setCouponEndTimeMins("00");
					
				}

				// For coupon expiry date.

				String expDate = couponinfo.getCouponExpireDate();
				if (null != expDate) {
					expDate = Utility.formattedDate(expDate);
					couponinfo.setCouponExpireDate(expDate);
				}
				couponExpTime = couponinfo.getCouponExpTime();
				if (!Utility.isEmptyOrNullString(couponExpTime)) {
					tempExpiryTimeHrsMin = couponExpTime.split(":");

				}
				if (null != tempExpiryTimeHrsMin) {
					couponinfo.setCouponExpTimeHrs(tempExpiryTimeHrsMin[0]);
					couponinfo.setCouponExpTimeMins(tempExpiryTimeHrsMin[1]);

				} else {
					couponinfo.setCouponExpTimeHrs("00");
					couponinfo.setCouponExpTimeMins("00");

				}

				
				/*
				 * String couponDiscountType =
				 * couponinfo.getCouponDiscountType();
				 * couponinfo.setCouponDiscountType(couponDiscountType);
				 * couponinfo.setCouponDiscountTypehidden(couponDiscountType);
				 */
				if (null != couponinfo.getScanCode()) {
					temp = couponinfo.getScanCode().split(",");
				}
				strCouponImg = couponinfo.getCouponImagePath();
				if (!"".equals(Utility.checkNull(strCouponImg))
						&& !ApplicationConstants.BLANKIMAGE
								.equals(strCouponImg)) {
					session.setAttribute("couponImagePath",
							couponinfo.getCouponImagePath());
					final int slashIndex = strCouponImg.lastIndexOf("/");
					final int dotIndex = strCouponImg.lastIndexOf('.');
					if (dotIndex == -1) {
						couponinfo.setCouponImgPath(null);
					} else {
						couponinfo.setCouponImgPath(strCouponImg.substring(
								slashIndex + 1, strCouponImg.length()));
					}
				} else {
					couponinfo.setCouponImgPath(null);
				}
			}
			if (null == couponinfo.getCoupDetImgPath() || "".equals(couponinfo.getCoupDetImgPath())) {
				session.setAttribute("couponDetImagePath", ApplicationConstants.BLANKIMAGE);
			} else {

				session.setAttribute("couponDetImagePath", couponinfo.getCoupDetImgPath());
			}

/*			manageProductList = retailerService.getProdToAssociateCoupon(null,
					null, retailID);
			if (temp != null) {
				for (int i = 0; i < manageProductList.size(); i++) {
					for (int j = 0; j < temp.length; j++) {
						if (!"".equals(Utility.checkNull(manageProductList.get(
								i).getScanCode()))) {
							if (manageProductList.get(i).getScanCode()
									.equals(temp[j])) {
								scanCodeList.add(manageProductList.get(i));
								break;
							}
						}
					}
				}
			}
			ArrayList<TimeZones> timeZonelst = null;
			timeZonelst = retailerService.getAllTimeZones();
			session.setAttribute("CoupontimeZoneslst", timeZonelst);*/
			
			session.setAttribute("scanCodeList", scanCodeList);
			model.put("reruncouponform", couponinfo);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside RerunCouponController : getCouponInfo : Exception : "
					+ e.getMessage());
			throw e;
		} catch (ParseException e) {
			LOG.error("Inside RerunCouponController : getCouponInfo : Exception : "
					+ e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return returnView;
	}

	/**
	 * This controller method return time(Start and end hrs).
	 * 
	 * @return sortedMap map object.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("CouponStartHrs")
	public final Map<String, String> populateCouponStartHrs()
			throws ScanSeeServiceException {
		final HashMap<String, String> mapCouponStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapCouponStartHrs.put(ApplicationConstants.ZERO + i,
						ApplicationConstants.ZERO + i);
			} else {
				mapCouponStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapCouponStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility
				.sortByComparator(mapCouponStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method return sorted minutes(Start and end minutes).
	 * 
	 * @return sortedMap map object.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("CouponStartMin")
	public final Map<String, String> populateCouponStartMin()
			throws ScanSeeServiceException {
		final HashMap<String, String> mapCouponStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapCouponStartHrs.put(ApplicationConstants.ZERO + i,
						ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapCouponStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapCouponStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility
				.sortByComparator(mapCouponStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method update rerun Existing Coupon details.
	 * 
	 * @param objCouponReRun
	 *            instance as Coupon.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/reruncoupon.htm", method = RequestMethod.POST)
	public final ModelAndView onSubmitCouponInfo(
			@ModelAttribute("reruncouponform") Coupon objCouponReRun,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, ScanSeeServiceException {
		LOG.info("Inside RerunCouponController : onSubmitCouponInfo");
		String view = null;
		String serviceResponse = null;
		String compDate = null;
		// Date currentDate = new Date();
		session.setAttribute("imageCropPage", "ReRunCouponRetailer");

		Date currentDate = new Date();
		try {
			final ServletContext servletContext = request.getSession()
					.getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext
					.getBean(ApplicationConstants.RETAILERSERVICE);
			final Users loginUser = (Users) request.getSession().getAttribute(
					ApplicationConstants.LOGINUSER);

			Long retailID = null;
			retailID = Long.valueOf(loginUser.getRetailerId());
			objCouponReRun.setRetailID(retailID);
			objCouponReRun.setRetLocationIDs(null);
			if (!"".equals(Utility.checkNull(objCouponReRun.getLocationID()))) {
				objCouponReRun
						.setRetLocationIDs(objCouponReRun.getLocationID());
			}
			
			createCouponValidator.validate(objCouponReRun, result);

			 FieldError fieldStartDate = result
					.getFieldError("couponStartDate");
			if (fieldStartDate == null) {
				compDate = Utility.compareCurrentDate(
						objCouponReRun.getCouponStartDate(), currentDate);
				if (null != compDate) {
					createCouponValidator.validate(objCouponReRun, result,
							ApplicationConstants.DATESTARTCURRENT);
				}
			}
			
			 FieldError fieldEndDate = result
					.getFieldError("couponEndDate");
			if (fieldEndDate == null) {
				
				if (!Utility
						.isEmptyOrNullString(objCouponReRun.getCouponEndDate())) {
				compDate = Utility.compareCurrentDate(
						objCouponReRun.getCouponEndDate(), currentDate);
				if (null != compDate) {
					createCouponValidator.validate(objCouponReRun, result,
							ApplicationConstants.DATEENDCURRENT);
				} else {
					compDate = Utility.compareDate(
							objCouponReRun.getCouponStartDate(),
							objCouponReRun.getCouponEndDate());
					if (null != compDate) {
						createCouponValidator.validate(objCouponReRun, result,
								ApplicationConstants.DATEAFTER);
					}
				}
				}
			}
			
			 fieldStartDate = result.getFieldError("couponStartDate");
			 fieldEndDate = result.getFieldError("couponExpireDate");
			if (fieldEndDate == null && fieldStartDate == null) {
				if (!Utility
						.isEmptyOrNullString(objCouponReRun.getCouponEndDate())){
				compDate = Utility.compareDate(
						objCouponReRun.getCouponStartDate(),
						objCouponReRun.getCouponEndDate());
				if (null != compDate) {
					createCouponValidator.validate(objCouponReRun, result,
							ApplicationConstants.DATEAFTER);
				}
				}
			}// for coupon expiration date..
			if (!Utility.isEmptyOrNullString(objCouponReRun
					.getCouponExpireDate())) {
				// Compate current date.
				compDate = Utility.compareCurrentDate(
						objCouponReRun.getCouponExpireDate(), currentDate);

				if (null != compDate) {

					createCouponValidator.validate(objCouponReRun, result,
							ApplicationConstants.EXPIREDDATE);

				} else {
					
					if (!Utility
							.isEmptyOrNullString(objCouponReRun.getCouponEndDate())){

					compDate = Utility.compareDate(
							objCouponReRun.getCouponEndDate(),
							objCouponReRun.getCouponExpireDate());
					if (null != compDate) {
						createCouponValidator.validate(objCouponReRun, result,
								ApplicationConstants.DATEEXPAFTER);

					}
					}
				}
			}
		//for coupon end time...couponEndTimeHrs, couponEndTimeMins
			

			if (null != objCouponReRun.getCouponEndTimeHrs() && !"00".equals(objCouponReRun.getCouponEndTimeHrs())
					|| (null != objCouponReRun.getCouponEndTimeMins() && !"00".equals(objCouponReRun.getCouponEndTimeMins()))) {

				if (null == objCouponReRun.getCouponEndDate() || "".equals(objCouponReRun.getCouponEndDate())) {

					createCouponValidator.validate(objCouponReRun, result, ApplicationConstants.DATEENDENTER);
				}

			}
			
			//for coupon exp time...couponExpTimeHrs, couponExpTimeMins
			
			if (null != objCouponReRun.getCouponExpTimeHrs() && !"00".equals(objCouponReRun.getCouponExpTimeHrs())
					|| (null != objCouponReRun.getCouponExpTimeMins() && !"00".equals(objCouponReRun.getCouponExpTimeMins()))) {

				if (null == objCouponReRun.getCouponExpireDate() || "".equals(objCouponReRun.getCouponExpireDate())) {

					createCouponValidator.validate(objCouponReRun, result, ApplicationConstants.DATEEXPENTER);
				}

			}

			if (result.hasErrors()) {
				view = returnView;
			} else {
				serviceResponse = retailerService
						.insertCouponInfo(objCouponReRun);
				
				if (serviceResponse.equals(ApplicationConstants.SUCCESS)) {
					view = "/ScanSeeWeb/retailer/managecoupons.htm";
				} else {
					request.setAttribute(ApplicationConstants.MESSAGE,
							"Error while rerun Deal");
					view = "/ScanSeeWeb/retailer/managecoupons.htm";
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside RerunCouponController : onSubmitCouponInfo : Exception : "
					+ e.getMessage());
			throw e;
		}
		if (view.equals(returnView)) {
			return new ModelAndView(view);
		} else {
			return new ModelAndView(new RedirectView(view));
		}
	}

	/**
	 * This controller method will display Coupon details of retailer in
	 * preview(iphone).
	 * 
	 * @param objCouponRerunPrev
	 *            Coupon instance as request parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return viewPreview, display hot deals details of retailer in
	 *         preview(iphone) .
	 * @throws ScanSeeServiceException
	 *             on input error.
	 * @throws IOException
	 *             on input error.
	 */
	@RequestMapping(value = "/previewreruncouponretailer.htm", method = RequestMethod.POST)
	public final ModelAndView previewCoupons(
			@ModelAttribute("previewreruncouponform") Coupon objCouponRerunPrev,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session)
			throws IOException, ScanSeeServiceException {
		LOG.info("Inside RerunCouponController : previewCoupons");
		request.getSession().removeAttribute("reRunCouponImage");
		request.getSession().removeAttribute("reRunProductName");
		request.getSession().removeAttribute("prdRerunImage");
		CommonsMultipartFile imageFile = null;
		String couponName = null;
		String couponLongDesc = null;
		String coupBannerTitle=null;
		final Long lCouponID = Long.parseLong(request.getParameter("couponID"));

		List<Coupon> prods = null;
		List<Coupon> couponList = null;
		couponName = objCouponRerunPrev.getCouponName();
		couponLongDesc = objCouponRerunPrev.getCouponLongDesc();
		coupBannerTitle = objCouponRerunPrev.getBannerTitle();
		String strCouponImage = null;
		imageFile = objCouponRerunPrev.getImageFile();
		session.setAttribute("reRunCouponName", couponName);
		session.setAttribute("reRunCouponLongDesc", couponLongDesc);
		session.setAttribute("bannerTitle", coupBannerTitle);
		if (!"".equals(Utility.checkNull(objCouponRerunPrev.getLocationID()))) {
			objCouponRerunPrev.setRetLocationIDs(objCouponRerunPrev
					.getLocationID());
		}

		try {
			final ServletContext servletContext = request.getSession()
					.getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext
					.getBean(ApplicationConstants.RETAILERSERVICE);
			strCouponImage = (String) session.getAttribute("couponImagePath");
			if (!"".equals(Utility.checkNull(strCouponImage))
					&& !ApplicationConstants.UPLOAD_IMAGE_PATH
							.equals(strCouponImage)) {
				session.setAttribute("editCouponImage", strCouponImage);
			}
			couponList = retailerService.fetchCouponImage(lCouponID);
			if (couponList != null && !couponList.isEmpty()) {
				final String strImgPath = couponList.get(0)
						.getCouponImagePath();
				if ("".equals(Utility.checkNull(strCouponImage))) {
					if (strImgPath.endsWith(".png")) {
						session.setAttribute("editCouponImage", couponList.get(0).getCouponImagePath());
					}
				}
				final String strScanCodes = objCouponRerunPrev.getScanCode();
				if (strScanCodes != null && !strScanCodes.isEmpty()) {
					prods = retailerService.getProdCoupon(strScanCodes);
					if (prods != null && !prods.isEmpty()) {
						session.setAttribute("reRunProductName", prods.get(0)
								.getProductName());
						final String strProdImg = prods.get(0)
								.getProductImagePath();
						if (strProdImg != null && !"".equals(strProdImg)) {
							final String[] arrStrProdImg = strProdImg
									.split(",");
							final String strImg = arrStrProdImg[0].trim();
							session.setAttribute("prdRerunImage", strImg);
						}
					}
				}
			}
		} catch (ScanSeeServiceException exception) {
			LOG.error(
					"Inside RerunCouponController : previewCoupons : Exception ",
					exception.getMessage());
		}
		session.setAttribute("couponDetails", couponList.get(0));
		model.put("previewreruncouponform", objCouponRerunPrev);
		return new ModelAndView("retailerpreviewreruncoupon");
	}
}

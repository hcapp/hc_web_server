/**
 * @ (#) CreateCouponController.java 26-Dec-2011
 * Project       :ScanSeeWeb
 * File          : CreateCouponController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 26-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.CreateCouponValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Coupon;
import common.pojo.Product;
import common.pojo.RetailerLocation;
import common.pojo.SearchResultInfo;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

/**
 * CreateCouponController is a controller class for create new coupon screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class CreateCouponController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(CreateCouponController.class);
	/**
	 * Variable returnView declared as constant string.
	 */
	private final String returnView = "createcoupon";

	/**
	 * createCouponValidator as instance of CreateCouponValidator.
	 */
	private CreateCouponValidator createCouponValidator;

	/**
	 * Getting the CreateCouponValidator Instance.
	 * 
	 * @param createCouponValidator
	 *            is instance of CreateCouponValidator. CreateCouponValidator
	 *            the createCouponValidator to set.
	 */
	@Autowired
	public final void setCreateCouponValidator(
			CreateCouponValidator createCouponValidator) {
		this.createCouponValidator = createCouponValidator;
	}

	/**
	 * The controller method to show new coupon screen.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return returnView .
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/createcoupon.htm", method = RequestMethod.GET)
	public final String showCreateCouponPage(HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		LOG.info("Inside CreateCouponController : showCreateCouponPage");
		try {
			request.getSession().removeAttribute("message");
			session.removeAttribute("prdList");
			final Coupon coupon = new Coupon();
			coupon.setStrPos("1");
			SearchResultInfo objCouponList = null;
			objCouponList = (SearchResultInfo) session
					.getAttribute("couponList");
			if (null == objCouponList) {
				coupon.setBackButton("no");
			}
			model.put("createcouponform", coupon);
			final ServletContext servletContext = request.getSession()
					.getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext
					.getBean(ApplicationConstants.RETAILERSERVICE);
			ArrayList<RetailerLocation> arRetLocationList = null;

			Long retailID = null;
			final Users loginUser = (Users) request.getSession().getAttribute(
					ApplicationConstants.LOGINUSER);
			retailID = Long.valueOf(loginUser.getRetailerId());
			session.setAttribute("imageCropPage", "AddCouponRetailer");
			
			
			
			
			session.setAttribute("couponImagePath",	ApplicationConstants.UPLOAD_IMAGE_PATH);
			session.setAttribute("couponDetImagePath",	ApplicationConstants.UPLOAD_IMAGE_PATH);
			
			arRetLocationList = retailerService.getRetailerLocations(retailID);
			if (arRetLocationList != null) {
				session.setAttribute("retailerLocList", arRetLocationList);
			} else {
				session.setAttribute("retailerLocList", "");
			}

			ArrayList<TimeZones> timeZonelst = null;
			timeZonelst = retailerService.getAllTimeZones();
			session.setAttribute("timeZoneslst", timeZonelst);
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside CreateCouponController : showCreateCouponPage : Exception : "
					+ e.getMessage());
			throw e;
		}
		return returnView;
	}

	/**
	 * This controller method create New Coupon details.
	 * 
	 * @param objCouponAdd
	 *            instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name and a model.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/createcoupon.htm", method = RequestMethod.POST)
	public final ModelAndView onSubmitCouponInfo(
			@ModelAttribute("createcouponform") Coupon objCouponAdd,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws IOException, ScanSeeServiceException {
		LOG.info("Inside CreateCouponController : onSubmitCouponInfo");
		String view = returnView;
		String serviceResponse = null;
		String compDate = null;
		final Date currentDate = new Date();
		try {
			final ServletContext servletContext = request.getSession()
					.getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext
					.getBean(ApplicationConstants.RETAILERSERVICE);
			Long retailID = null;
			final Users loginUser = (Users) request.getSession().getAttribute(
					ApplicationConstants.LOGINUSER);
			retailID = Long.valueOf(loginUser.getRetailerId());
			if (!"".equals(Utility.checkNull(objCouponAdd.getLocationID()))) {
				objCouponAdd.setRetLocationIDs(objCouponAdd.getLocationID());
			}
			SearchResultInfo objCouponList = null;
			objCouponList = (SearchResultInfo) session
					.getAttribute("couponList");
			if (null == objCouponList) {
				objCouponAdd.setBackButton("no");
			}
			objCouponAdd.setRetailID(retailID);
			createCouponValidator.validate(objCouponAdd, result);
			
			final FieldError fieldStartDate = result
					.getFieldError("couponStartDate");
			if (fieldStartDate == null) {
				compDate = Utility.compareCurrentDate(
						objCouponAdd.getCouponStartDate(), currentDate);
				if (null != compDate) {
					createCouponValidator.validate(objCouponAdd, result,
							ApplicationConstants.DATESTARTCURRENT);
				}
			}
			final FieldError fieldEndDate = result
					.getFieldError("couponEndDate");
			if (fieldEndDate == null) {
				
				if (!Utility
						.isEmptyOrNullString(objCouponAdd.getCouponEndDate())) {
				compDate = Utility.compareCurrentDate(
						objCouponAdd.getCouponEndDate(), currentDate);
				if (null != compDate) {
					createCouponValidator.validate(objCouponAdd, result,
							ApplicationConstants.DATEENDCURRENT);
				} else {
					compDate = Utility.compareDate(
							objCouponAdd.getCouponStartDate(),
							objCouponAdd.getCouponEndDate());
					if (null != compDate) {
						createCouponValidator.validate(objCouponAdd, result,
								ApplicationConstants.DATEAFTER);
					}
				}
				}
			}

			// for coupon expiration date..
			if (!Utility
					.isEmptyOrNullString(objCouponAdd.getCouponExpireDate())) {
				// Compate current date.
				compDate = Utility.compareCurrentDate(
						objCouponAdd.getCouponExpireDate(), currentDate);

				if (null != compDate) {

					createCouponValidator.validate(objCouponAdd, result,
							ApplicationConstants.EXPIREDDATE);

				} else {
					
					if (!Utility
							.isEmptyOrNullString(objCouponAdd.getCouponEndDate())){

					compDate = Utility.compareDate(
							objCouponAdd.getCouponEndDate(),
							objCouponAdd.getCouponExpireDate());
					if (null != compDate) {
						createCouponValidator.validate(objCouponAdd, result,
								ApplicationConstants.DATEEXPAFTER);

					}
					}

				}
			}

			//for coupon end time...couponEndTimeHrs, couponEndTimeMins
			

			if (null != objCouponAdd.getCouponEndTimeHrs() && !"00".equals(objCouponAdd.getCouponEndTimeHrs())
					|| (null != objCouponAdd.getCouponEndTimeMins() && !"00".equals(objCouponAdd.getCouponEndTimeMins()))) {

				if (null == objCouponAdd.getCouponEndDate() || "".equals(objCouponAdd.getCouponEndDate())) {

					createCouponValidator.validate(objCouponAdd, result, ApplicationConstants.DATEENDENTER);
				}

			}
			
			//for coupon exp time...couponExpTimeHrs, couponExpTimeMins
			
			if (null != objCouponAdd.getCouponExpTimeHrs() && !"00".equals(objCouponAdd.getCouponExpTimeHrs())
					|| (null != objCouponAdd.getCouponExpTimeMins() && !"00".equals(objCouponAdd.getCouponExpTimeMins()))) {

				if (null == objCouponAdd.getCouponExpireDate() || "".equals(objCouponAdd.getCouponExpireDate())) {

					createCouponValidator.validate(objCouponAdd, result, ApplicationConstants.DATEEXPENTER);
				}

			}
			
			
			
			if (result.hasErrors()) {
				view = returnView;
			} else {
				serviceResponse = retailerService
						.insertCouponInfo(objCouponAdd);
				if (serviceResponse.equals(ApplicationConstants.SUCCESS)) {
					request.setAttribute("couponmsg",ApplicationConstants.COUPONADDEDMSG);
					view = "/ScanSeeWeb/retailer/managecoupons.htm";
				} else {
					request.setAttribute(ApplicationConstants.MESSAGE,
							"Error While creating Deal");
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside CreateCouponController : onSubmitCouponInfo : Exception : "
					+ e.getMessage());
			throw e;
		}
		if (view.equals(returnView)) {
			return new ModelAndView(view);
		} else {
			return new ModelAndView(new RedirectView(view));
		}
	}

	/**
	 * This controller method get all retailer locations.
	 * 
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return mapRelLoc,list of retailer locations.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public final Map<String, String> populateRelLoc(HttpServletRequest request,
			HttpSession session) throws ScanSeeServiceException {
		final HashMap<String, String> mapRelLoc = new HashMap<String, String>();
		try {
			final ServletContext servletContext = request.getSession()
					.getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext
					.getBean(ApplicationConstants.RETAILERSERVICE);

			ArrayList<RetailerLocation> retailerLocations = null;

			Long retailID = null;
			final Users loginUser = (Users) request.getSession().getAttribute(
					ApplicationConstants.LOGINUSER);
			retailID = Long.valueOf(loginUser.getRetailerId());
			retailerLocations = retailerService.getRetailerLocations(retailID);

			if (retailerLocations != null) {
				session.setAttribute("retailerLocList", retailerLocations);
			} else {
				session.setAttribute("retailerLocList", "");
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside CreateCouponController : populateRelLoc : Exception :"
					+ e.getMessage());
			throw e;
		}
		return mapRelLoc;
	}

	/**
	 * This controller method will display new coupon details of retailer in
	 * preview(iphone).
	 * 
	 * @param objCouponPreview
	 *            Coupon instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return viewPreview, display hot deals details of retailer in
	 *         preview(iphone) .
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	@RequestMapping(value = "/previewcouponretaler.htm", method = RequestMethod.POST)
	public final ModelAndView previewCoupons(
			@ModelAttribute("previewcouponform") Coupon objCouponPreview,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException {
		LOG.info("Inside CreateCouponController : previewCoupons");
		final ServletContext servletContext = request.getSession()
				.getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext
				.getBean(ApplicationConstants.RETAILERSERVICE);
		session.removeAttribute("couponImage");
		session.removeAttribute("prodImage");
		session.removeAttribute("couponDetails");
		/*
		 * String path = null; InputStream isCoupon = null; OutputStream
		 * osCoupon = null;
		 */
		// CommonsMultipartFile imageFile = null;
		ArrayList<Coupon> prodList = null;
		String strCouponName = null;
		String strCouponLongDesc = null;
		String strProductName = null;
		String strCouponImage = null;
		String strBannerTitle = null;
		strProductName = objCouponPreview.getProductName();
		if (!"".equals(Utility.checkNull(objCouponPreview.getLocationID()))) {
			objCouponPreview
					.setRetLocationIDs(objCouponPreview.getLocationID());
		}
		objCouponPreview.setProductName(strProductName);
		try {
			strCouponName = objCouponPreview.getCouponName();
			strCouponLongDesc = objCouponPreview.getCouponLongDesc();
			strBannerTitle= objCouponPreview.getBannerTitle();
			session.setAttribute("couponName", strCouponName);
			session.setAttribute("bannerTitle", strBannerTitle);
			session.setAttribute("couponLongDesc", strCouponLongDesc);
		
			strCouponImage = (String) session.getAttribute("couponImagePath");
			if (!"".equals(Utility.checkNull(strCouponImage))
					&& !ApplicationConstants.UPLOAD_IMAGE_PATH
							.equals(strCouponImage)) {
				session.setAttribute("prodImage", strCouponImage);
			} else if (!"".equals(Utility.checkNull(strProductName))) {
				prodList = retailerService.getProdCoupon(strProductName);
				// if (imageFile == null || imageFile.isEmpty()) {
				final String strProdImage = prodList.get(0)
						.getProductImagePath();
				final String[] arrStrProdImg = strProdImage.split(",");
				final String image = arrStrProdImg[0].trim();
				session.setAttribute("prodImage", image);
				/* } */
				if (prodList != null && !prodList.isEmpty()) {
					session.setAttribute("prdList", prodList.get(0));
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error("Inside CreateCouponController : previewCoupons : "
					+ e.getMessage());
			throw e;
		}
		session.setAttribute("couponDetails", objCouponPreview);
		model.put("previewcouponform", objCouponPreview);
		return new ModelAndView("retailerpreviewcoupon");
	}

	/**
	 * This controller method deletes the Product details that has been selected
	 * from list.
	 * 
	 * @param scanCode
	 *            the value to be scanCode.
	 * @param request
	 *            HttpServletRequest instance.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param response
	 *            HttpServletResponse instance as parameter.
	 * @return response,added successfully or not.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	@RequestMapping(value = "/removeProduct", method = RequestMethod.GET)
	@ResponseBody
	public final String removeProduct(
			@RequestParam(value = "productId", required = true) String scanCode,
			HttpServletRequest request, HttpSession session,
			HttpServletResponse response) throws ScanSeeServiceException {
		LOG.info("Inside CreateCouponController : removeProduct");
		final ArrayList<Product> pdtInfoList = (ArrayList<Product>) session
				.getAttribute("scanCodeList");
		final ArrayList<Product> pdtInfoListProd = new ArrayList<Product>();
		String[] temp;
		temp = scanCode.split(",");
		if (null != pdtInfoList && !pdtInfoList.isEmpty()) {
			for (int i = 0; i < temp.length; i++) {
				for (int j = 0; j < pdtInfoList.size(); j++) {
					if (pdtInfoList.get(j).getScanCode().equals(temp[i])) {
						pdtInfoListProd.add(pdtInfoList.get(j));
						break;
					}
				}
			}
		}
		if (null != pdtInfoListProd && !pdtInfoListProd.isEmpty()) {
			request.getSession().setAttribute("scanCodeList", pdtInfoListProd);
		}
		return null;
	}

	/**
	 * This controller method will upload Coupon image .
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param couponinfo
	 *            instance of Coupon.
	 * @param response
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws Exception
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadcouponimg.htm", method = RequestMethod.POST)
	public final String onSubmitImage(
			@ModelAttribute("createcouponform") Coupon couponinfo,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {
		LOG.info("Inside CreateCouponController : onSubmitImage ");
		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");
	

		if (null != couponinfo.getUploadCoupImageType()) {
			if (couponinfo.getUploadCoupImageType().equals("imageFile")) {
				
				session.setAttribute("minCropHt", 300);
				session.setAttribute("minCropWd", 600);

				imageSizeValFlg = Utility.validMinDimension(
						ApplicationConstants.CROPIMAGEHEIGHT,
						ApplicationConstants.CROPIMAGEWIDTH, couponinfo.getImageFile()
								.getInputStream());
				if (imageSizeValFlg) {
					session.setAttribute("couponImagePath",
							ApplicationConstants.UPLOADIMAGEPATH);
					response.getWriter().write(
							"<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
					return null;

					
				} else {
					final BufferedImage img = Utility.getBufferedImageForMinDimension(
							couponinfo.getImageFile().getInputStream(), request
									.getRealPath("images"), couponinfo.getImageFile()
									.getOriginalFilename(), "Coupon");
					w = img.getWidth(null);
					h = img.getHeight(null);
					session.setAttribute("imageHt", h);
					session.setAttribute("imageWd", w);

					final String tempImgPath = Utility.getTempMediaPath(
							ApplicationConstants.TEMP).toString();
					imageSource = couponinfo.getImageFile().getOriginalFilename();
					imageSource = FilenameUtils.removeExtension(imageSource);
					imageSource = imageSource + ".png" + "?" + date.getTime();
					final String filePath = tempImgPath + fileSeparator
							+ couponinfo.getImageFile().getOriginalFilename();
					Utility.writeImage(img, filePath);
					if (imageValidSizeValFlg) {
						strResponse.append("ValidImageDimension");
						strResponse.append("|"
								+ couponinfo.getImageFile().getOriginalFilename());
						session.setAttribute("couponImagePath", "/"
								+ ApplicationConstants.IMAGES + "/"
								+ ApplicationConstants.TEMPFOLDER + "/" + imageSource);
					}
					strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.TEMPFOLDER + "/" + imageSource);
					response.getWriter().write(
							"<imageScr>" + strResponse.toString() + "</imageScr>");
					session.setAttribute("cropImageSource", "/"
							+ ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				}
				
				
			}else if(couponinfo.getUploadCoupImageType().equals("detailImage"))
			{

				session.setAttribute("minCropHt", 300);
				session.setAttribute("minCropWd", 600);
				imageSizeValFlg = Utility.validMinDimension(
						ApplicationConstants.CROPIMAGEHEIGHT,
						ApplicationConstants.CROPIMAGEWIDTH, couponinfo.getDetailImage()
								.getInputStream());
				if (imageSizeValFlg) {
					session.setAttribute("couponDetImagePath",
							ApplicationConstants.UPLOADIMAGEPATH);
					response.getWriter().write(
							"<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
					return null;

					
				} else {
					final BufferedImage img = Utility.getBufferedImageForMinDimension(
							couponinfo.getDetailImage().getInputStream(), request
									.getRealPath("images"), couponinfo.getDetailImage()
									.getOriginalFilename(), "Coupon");
					w = img.getWidth(null);
					h = img.getHeight(null);
					session.setAttribute("imageHt", h);
					session.setAttribute("imageWd", w);

					final String tempImgPath = Utility.getTempMediaPath(
							ApplicationConstants.TEMP).toString();
					imageSource = couponinfo.getDetailImage().getOriginalFilename();
					imageSource = FilenameUtils.removeExtension(imageSource);
					imageSource = imageSource + ".png" + "?" + date.getTime();
					final String filePath = tempImgPath + fileSeparator
							+ couponinfo.getDetailImage().getOriginalFilename();
					Utility.writeImage(img, filePath);
					if (imageValidSizeValFlg) {
						strResponse.append("ValidImageDimension");
						strResponse.append("|"
								+ couponinfo.getDetailImage().getOriginalFilename());
						session.setAttribute("couponDetImagePath", "/"
								+ ApplicationConstants.IMAGES + "/"
								+ ApplicationConstants.TEMPFOLDER + "/" + imageSource);
					}
					strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.TEMPFOLDER + "/" + imageSource);
					response.getWriter().write(
							"<imageScr>" + strResponse.toString() + "</imageScr>");
					session.setAttribute("cropImageSource", "/"
							+ ApplicationConstants.IMAGES + "/"
							+ ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				}
			}
		
	
	
}

		return null;
}
	

	/**
	 * This controller method return time(Start and end hrs).
	 * 
	 * @return sortedMap map object.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("CouponStartHrs")
	public final Map<String, String> populateCouponStartHrs()
			throws ScanSeeServiceException {
		final HashMap<String, String> mapCouponStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapCouponStartHrs.put(ApplicationConstants.ZERO + i,
						ApplicationConstants.ZERO + i);
			} else {
				mapCouponStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapCouponStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility
				.sortByComparator(mapCouponStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method return sorted minutes(Start and end minutes).
	 * 
	 * @return sortedMap map object.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("CouponStartMin")
	public final Map<String, String> populateCouponStartMin()
			throws ScanSeeServiceException {
		final HashMap<String, String> mapCouponStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapCouponStartHrs.put(ApplicationConstants.ZERO + i,
						ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapCouponStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapCouponStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility
				.sortByComparator(mapCouponStartHrs);
		return sortedMap;
	}

	@RequestMapping(value = "/addcouponinstructions.htm", method = RequestMethod.GET)
	public final String addCouponInstructions(HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String strMethodName = "addCouponInstructions";
		final String strViewName = "addcouponinstructions";

		LOG.info("Start CreateCouponController : " + strMethodName);
		String strPage = request.getParameter("cptype");
		request.setAttribute("cptype", strPage);

		LOG.info("End CreateCouponController : " + strMethodName);
		return strViewName;
	}
}

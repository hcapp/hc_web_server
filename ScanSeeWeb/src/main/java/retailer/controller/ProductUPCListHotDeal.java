/**
 * @ (#) ProductUPCListHotDeal.java 12-Jan-2012
 * Project       :ScanSeeWeb
 * File          : ProductUPCListHotDeal.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 12-Jan-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Product;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class ProductUPCListHotDeal
{
	/**
	 * Getting the logger Instance.
	 */
	final public String RETURN_VIEW = "produpclisthotdeal";
	private static final Logger LOG = LoggerFactory.getLogger(ProductUPCListHotDeal.class);

	@RequestMapping(value = "/produpclisthotdeal.htm",method = RequestMethod.GET)
	public String showCreateCouponPage(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		LOG.info("Inside ProductUPCListHotDeal : showCreateCouponPage");
		session.removeAttribute("manageproductlist");
		request.getSession().removeAttribute("message");
		Product product = new Product();
		model.put("produpclisthotdealform", product);
		return RETURN_VIEW;
	}
	
	@RequestMapping(value = "/produpclisthotdealadd.htm",method = RequestMethod.GET)
	public String showCreate(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		LOG.info("Inside ProductUPCListHotDeal : showCreate");
		session.removeAttribute("manageproductlist");
		Product product = new Product();
		model.put("produpclisthotdealform", product);
		return RETURN_VIEW;
	}


	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView searchResult(@ModelAttribute("produpclisthotdealform") Product productInfo, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException,ScanSeeServiceException
	{
		LOG.info("Inside ProductUPCListHotDeal : searchResult");
		request.getSession().removeAttribute("manageproductlist");
		//request.getSession().removeAttribute("pdtInfoList");
		//request.getSession().removeAttribute("message");
		ArrayList<Product> manageProductList = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		ArrayList<Product> pdtInfoList = null;
		String productName = productInfo.getProductName().trim();
		Long retailID = null;
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());
		try
		{
			manageProductList = retailerService.getProdToAssociateCoupon(productName, productInfo.getRetLocID(), retailID);
			pdtInfoList = (ArrayList<Product>) request.getSession().getAttribute("pdtInfoList");
			if (pdtInfoList != null && !pdtInfoList.isEmpty())
			{
				for (int i = 0; i < pdtInfoList.size(); i++)
				{
					long prodID = pdtInfoList.get(i).getProductID();
					for (int j = 0; j < manageProductList.size(); j++)
					{
						if (prodID == manageProductList.get(j).getProductID())
						{
							pdtInfoList.get(i).setPrice(manageProductList.get(j).getPrice());
							manageProductList.remove(j);
						}
					}
				}
				session.setAttribute("pdtInfoList", pdtInfoList);
			}
			if (manageProductList != null && manageProductList.size() > 0) {
				session.setAttribute("manageproductlist", manageProductList);
			} else {
				request.setAttribute(ApplicationConstants.MESSAGE, "Product not found !!!");
			}
		}
		catch (ScanSeeServiceException e)
		{

			LOG.error("Exception occurred in searchResult:::::"+ e.getMessage());
            throw new ScanSeeServiceException(e);
		}

		return new ModelAndView(RETURN_VIEW);
	}
}

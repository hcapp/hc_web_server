/**
 * @ (#) AssociateRetailerProdDeleteController.java 01-Feb-2012
 * Project       :ScanSeeWeb
 * File          : AssociateRetailerProdDeleteController.java
 * Author        : Kumar D
 * Company       : Span Systems Corporation
 * Date Created  : 01-Feb-2012
 */

package retailer.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Users;

@Controller
public class AssociateRetailerProdDeleteController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AssociateRetailerProdDeleteController.class);
	final public String RETURN_VIEW = "addsearchprod";

	@RequestMapping(value = "/deleteprod.htm", method = RequestMethod.POST)
	public String productSetupPage(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request,
			HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside UserProfileController : productSetupPage ");
		String response = null;
		Long objRetailID = new Long(0);
		Long objProductID = new Long(0);
		try
		{
			request.getSession().removeAttribute("message");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			objRetailID = Long.valueOf(loginUser.getRetailerId());
			
			response = retailerService.deleteProductsFromLocHoldControl(objRetailID, Long.valueOf(productId));

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in productSetupPage:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		if (response.equals(ApplicationConstants.SUCCESS))
		{
			LOG.info("Inside UserProfileController : productSetupPage : Deleted Successfull !!!!");
			request.getSession().setAttribute("message", "This product associated with Retailer Location(s) is deleted Successfully!!!");
		}

		return response;
	}
}

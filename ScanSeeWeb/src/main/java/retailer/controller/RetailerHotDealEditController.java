package retailer.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.HotDealRetailerDetailsValidator;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.HotDealInfo;
import common.pojo.Product;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.util.Utility;

/**
 * EditHotDealRetailerController is a controller class for Edit/Update retailer
 * product hot deals screen.
 * 
 * @author Created by SPAN.
 */
@Controller
public class RetailerHotDealEditController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RetailerHotDealEditController.class);

	/**
	 * Variable retailerDealsValidator declared as instance of
	 * HotDealRetailerDetailsValidator.
	 */
	private HotDealRetailerDetailsValidator retailerDealsValidator;

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private final String viewName = "retailerHotDealsEdit";

	/**
	 * Variable view declared as String.
	 */
	private String view = viewName;

	/**
	 * Variable viewRedirect declared as String.
	 */
	private final String viewRedirect = "hotDealRetailer.htm";

	/**
	 * To hotdealRetailerValidator to set.
	 * 
	 * @param retailerDealsValidator
	 *            to set.
	 */
	@Autowired
	public void setRetailerDealsDetailsValidator(HotDealRetailerDetailsValidator retailerDealsValidator)
	{
		this.retailerDealsValidator = retailerDealsValidator;
	}

	/**
	 * To view to set.
	 * 
	 * @param view
	 *            to set.
	 */
	public final void setView(String view)
	{
		this.view = view;
	}

	/**
	 * This controller method will display the HotDeals by passing HotDealId as
	 * input parameter by call service and DAO methods.
	 * 
	 * @param hotDealInfo
	 *            HotDealInfo instance as request parameter
	 * @param request
	 *            HttpServletRequest instance.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws ParseException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/retailerdealedit.htm", method = RequestMethod.GET)
	public final String showPage(@ModelAttribute("edithotdealretailerform") HotDealInfo hotDealInfo, HttpServletRequest request, HttpSession session,
			ModelMap model) throws ParseException, ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealRetailerController : showPage ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);
		request.getSession().removeAttribute("hotdeallocation");
		request.getSession().removeAttribute("hotdealcity");
		request.getSession().removeAttribute("editretProdImage");
		request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
		session.setAttribute("imageCropPage", "EditHotDealRetailer");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		ArrayList<Category> arBCategoryList = null;
		String dealStartTime = null;
		String dealEndTime = null;
		String strExpireTime = null;

		String[] tempStartTimeHrsMin = null;
		String[] tempEndTimeHrsMin = null;
		String[] arExpireTimeHrsMin = null;
		try
		{
			arBCategoryList = supplierService.getAllBusinessCategory();
			session.setAttribute("categoryList", arBCategoryList);
			final List<HotDealInfo> dealsList = (List<HotDealInfo>) retailerService.getHotDealByID(hotDealInfo.getHotDealID());
			if (dealsList != null && !dealsList.isEmpty())
			{
				String salePrice = null;
				String regPrice = null;
				String sDate = null;
				String eDate = null;
				String strExpiredDate = null;
				String strHotDealImg = null;
				for (int i = 0; i < dealsList.size(); i++)
				{
					hotDealInfo.setHotDealID(hotDealInfo.getHotDealID());
					hotDealInfo.setHotDealName(dealsList.get(i).getHotDealName());
					hotDealInfo.setPrice(dealsList.get(i).getPrice());
					salePrice = dealsList.get(i).getSalePrice();
					salePrice = Utility.formatDecimalValue(salePrice);
					hotDealInfo.setSalePrice(salePrice);
					regPrice = dealsList.get(i).getPrice();

					regPrice = Utility.formatDecimalValue(regPrice);
					hotDealInfo.setPrice(regPrice);

					hotDealInfo.setHotDealShortDescription(dealsList.get(i).getHotDealShortDescription());
					hotDealInfo.setHotDealLongDescription(dealsList.get(i).getHotDealLongDescription());
					hotDealInfo.setHotDealTermsConditions(dealsList.get(i).getHotDealTermsConditions());
					hotDealInfo.setbCategory(dealsList.get(i).getbCategory());
					hotDealInfo.setbCategoryHidden(dealsList.get(i).getbCategory());
					//hotDealInfo.setUrl(dealsList.get(i).getUrl());
					hotDealInfo.setCouponCode(dealsList.get(i).getCouponCode());
					hotDealInfo.setNumOfHotDeals(dealsList.get(i).getNumOfHotDeals());
					hotDealInfo.setRetailID(user.getRetailerId());
					strHotDealImg = dealsList.get(i).getHotDealImagePath();
					if (!"".equals(Utility.checkNull(strHotDealImg)) && !ApplicationConstants.IMAGEICON.equals(strHotDealImg))
					{
						session.setAttribute("addHotDealImagePath", dealsList.get(i).getHotDealImagePath());
						/* If ProductImageFlag is true, ProductImage will be set.*/
						if (dealsList.get(i).isProductImageFlag()) {
							hotDealInfo.setProductImage(dealsList.get(i).getHotDealImagePath());
							hotDealInfo.setDealImgPath(null);
						} else {
							final int slashIndex = strHotDealImg.lastIndexOf("/");
							final int dotIndex = strHotDealImg.lastIndexOf('.');
							if (dotIndex == -1) {
								hotDealInfo.setDealImgPath(null);
								hotDealInfo.setProductImage(null);
							} else {
								hotDealInfo.setDealImgPath(strHotDealImg.substring(slashIndex + 1, strHotDealImg.length()));
								//hotDealInfo.setProductImage(dealsList.get(i).getHotDealImagePath());
							}
						}
					} else {
						hotDealInfo.setDealImgPath(null);
						hotDealInfo.setProductImage(null);
						session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOAD_IMAGE_PATH);
					}

					hotDealInfo.setScanCode(dealsList.get(i).getScanCode());
					sDate = dealsList.get(i).getDealStartDate();
					strExpiredDate = dealsList.get(i).getExpireDate();
					if (!"".equals(Utility.checkNull(strExpiredDate)))
					{
						strExpiredDate = Utility.formattedDate(strExpiredDate);
						hotDealInfo.setExpireDate(strExpiredDate);
					}
					if (sDate != null)
					{
						sDate = Utility.formattedDate(sDate);
						hotDealInfo.setDealStartDate(sDate);
					}
					eDate = dealsList.get(i).getDealEndDate();
					if (eDate != null)
					{
						eDate = Utility.formattedDate(eDate);
						hotDealInfo.setDealEndDate(eDate);
					}
					dealStartTime = dealsList.get(i).getDealStartTime();
					tempStartTimeHrsMin = dealStartTime.split(":");
					hotDealInfo.setDealStartHrs(tempStartTimeHrsMin[0]);
					hotDealInfo.setDealStartMins(tempStartTimeHrsMin[1]);
					dealEndTime = dealsList.get(i).getDealEndTime();
					tempEndTimeHrsMin = dealEndTime.split(":");
					hotDealInfo.setDealEndhrs(tempEndTimeHrsMin[0]);
					hotDealInfo.setDealEndMins(tempEndTimeHrsMin[1]);

					strExpireTime = dealsList.get(i).getExpireTime();
					if (!"".equals(Utility.checkNull(strExpireTime)))
					{
						arExpireTimeHrsMin = strExpireTime.split(":");
						hotDealInfo.setExpireHrs(arExpireTimeHrsMin[0]);
						hotDealInfo.setExpireMins(arExpireTimeHrsMin[1]);
					}

					hotDealInfo.setDealTimeZoneId(dealsList.get(i).getDealTimeZoneId());
					hotDealInfo.setHotDealTermsConditions(dealsList.get(i).getHotDealTermsConditions());
					hotDealInfo.setHotDealID(dealsList.get(i).getHotDealID());
					hotDealInfo.setProductId(dealsList.get(i).getProductId());
					if (!"".equals(dealsList.get(i).getProductId()) && dealsList.get(i).getProductId() != null)
					{
						hotDealInfo.setProductId(dealsList.get(i).getProductId());
						hotDealInfo.setExistingProductIds(dealsList.get(i).getProductId());
					}
					if (dealsList.get(i).getRetailerLocID() == null)
					{
						dealsList.get(i).setRetailerLocID(0);
					}
					if (dealsList.get(i).getCity() != null)
					{
						final StringTokenizer tokenizer = new StringTokenizer(dealsList.get(i).getCity(), ApplicationConstants.COMMA);
						if (tokenizer.countTokens() > 1)
						{
							hotDealInfo.setCity("All");
							hotDealInfo.setCityHidden("All");
							hotDealInfo.setDealForCityLoc("City");
							request.getSession().setAttribute("hotdealcity", "All");
						}
						else
						{
							hotDealInfo.setCity(dealsList.get(i).getCity());
							hotDealInfo.setCityHidden(dealsList.get(i).getCity());
							hotDealInfo.setDealForCityLoc("City");
							request.getSession().setAttribute("hotdealcity", dealsList.get(i).getCity());
						}
						request.getSession().removeAttribute("selRetailerLoc");
						request.getSession().removeAttribute("selRetailer");
					}
					else if (dealsList.get(i).getRetailerLocID() != 0)
					{
						hotDealInfo.setRetailerLocID(dealsList.get(i).getRetailerLocID());
						hotDealInfo.setDealForCityLoc("Location");
						request.getSession().setAttribute("hotdeallocation", String.valueOf(dealsList.get(i).getRetailerLocID()));
						request.getSession().removeAttribute("hotdealcity");
					}
					else
					{
						hotDealInfo.setDealForCityLoc("");
					}
				}
				request.getSession().removeAttribute("pdtInfoList");
				final ArrayList<Product> arPdtInfoList = (ArrayList<Product>) supplierService.getPdtInfoForDealRerun(hotDealInfo.getProductId());
				if (arPdtInfoList != null && !arPdtInfoList.isEmpty())
				{
					for (int i = 0; i < arPdtInfoList.size(); i++)
					{
						if ("".equals(Utility.checkNull(arPdtInfoList.get(i).getPrice())))
						{
							arPdtInfoList.get(i).setPrice("0");
						}
						if ("".equals(Utility.checkNull(arPdtInfoList.get(i).getImagePath())))
						{
							arPdtInfoList.get(i).setImagePath(ApplicationConstants.UPLOAD_IMAGE_PATH);
						}
					}
					request.getSession().setAttribute("pdtInfoList", arPdtInfoList);
				}

				ArrayList<TimeZones> timeZonelst = null;
				timeZonelst = retailerService.getAllTimeZones();
				session.setAttribute("retailerTimeZoneslst", timeZonelst);
				model.put("edithotdealretailerform", hotDealInfo);
			}
			else
			{
				model.put("edithotdealretailerform", hotDealInfo);
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (NullPointerException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return viewName;
	}

	/**
	 * This controller method will update existing HotDeals details screen by
	 * call service and DAO methods.
	 * 
	 * @param objHotDealsInfo
	 *            HotDealInfo instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/retailerdealedit.htm", method = RequestMethod.POST)
	public final ModelAndView updateHotDeals(@ModelAttribute("edithotdealretailerform") HotDealInfo objHotDealsInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside AddHotDealRetailerController : updateHotDeals ");
		boolean bPercentageFlag = false;
		try
		{
			String compDate = null;
			// final Date currentDate = new Date();
			String isDataInserted = null;
			String strpCity = null;
			int strRetLoc = 0;
			String strBCategory = null;
			String strExpireTime = null;
			String strRetailerLogo = null;
			String strLocRCity = null;
			session.setAttribute("imageCropPage", "EditHotDealRetailer");
			session.setAttribute("minCropWd", 70);
			session.setAttribute("minCropHt", 70);
			final MultipartFile fileHotDealImage = objHotDealsInfo.getImageFile();
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
			final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			String strUploadSecTime = null;
			objHotDealsInfo.setRetailID(user.getRetailerId());
			strBCategory = objHotDealsInfo.getbCategory();
			strpCity = objHotDealsInfo.getCity();
			if (null == objHotDealsInfo.getRetailerLocID()) {
				strRetLoc = 0;
			} else {
				strRetLoc = objHotDealsInfo.getRetailerLocID();
			}
			objHotDealsInfo.setbCategoryHidden(objHotDealsInfo.getbCategory());
			final StringBuffer allCities = new StringBuffer();

			final String sTime = String.valueOf(objHotDealsInfo.getDealStartHrs()) + ":" + String.valueOf(objHotDealsInfo.getDealStartMins());
			final String eTime = String.valueOf(objHotDealsInfo.getDealEndhrs()) + ":" + String.valueOf(objHotDealsInfo.getDealEndMins());
			strExpireTime = String.valueOf(objHotDealsInfo.getExpireHrs()) + ":" + String.valueOf(objHotDealsInfo.getExpireMins());
			objHotDealsInfo.setDealStartTime(sTime);
			objHotDealsInfo.setDealEndTime(eTime);
			objHotDealsInfo.setExpireTime(strExpireTime);
			strLocRCity = request.getParameter("slctOpt");
			objHotDealsInfo.setCityHiddenChecked(Utility.checkNull(strLocRCity));

			/* Third priority is for the image uploaded by the retailer logo. */
			strRetailerLogo = (String) session.getAttribute("logosrc");
			if (!"".equals(Utility.checkNull(strRetailerLogo))) {
					if (!ApplicationConstants.UPLOADIMAGEPATH.equals(strRetailerLogo)) {
						user.setRetailerLogoImage(strRetailerLogo);
					}
			}

			if (",".equals(objHotDealsInfo.getProductImage())) {
				objHotDealsInfo.setProductImage(null);
			}
			/* First priority is for the image uploaded by the user. */
			if (!"".equals(Utility.checkNull(objHotDealsInfo.getDealImgPath()))
					&& !ApplicationConstants.BLANKIMAGE.equals(objHotDealsInfo.getDealImgPath()))
			{
				final String strHotDealImg = (String) session.getAttribute("addHotDealImagePath");
				final int slashIndex = strHotDealImg.lastIndexOf("/");
				final int dotIndex = strHotDealImg.lastIndexOf('.');
				if (dotIndex != -1)
				{
					strUploadSecTime = strHotDealImg.substring(slashIndex + 1, strHotDealImg.length());
					if (!"".equals(Utility.checkNull(strUploadSecTime)) 
							&& strUploadSecTime.equals(objHotDealsInfo.getDealImgPath()))
					{
						objHotDealsInfo.setProductImage(objHotDealsInfo.getDealImgPath());
						objHotDealsInfo.setDealImgPath(null);
					} else {
						objHotDealsInfo.setProductImage(objHotDealsInfo.getDealImgPath());
					}
				}
				/* Second priority is for the image from Products Associated. */
			}
			else if (!"".equals(Utility.checkNull(objHotDealsInfo.getProductImage())))
			{
				objHotDealsInfo.setProductImage(null);
			}
			/* Third priority is for the image uploaded by the retailer logo user.*/
			else if (!"".equals(Utility.checkNull(user.getRetailerLogoImage())))
			{
				objHotDealsInfo.setProductImage(user.getRetailerLogoImage().substring(user.getRetailerLogoImage().lastIndexOf("/") + 1));
			}
			else
			{
				objHotDealsInfo.setDealImgPath(null);
				objHotDealsInfo.setProductImage(null);
				result.rejectValue("imageFile", "Please Choose Hot Deal Image since there is no image for Product or Retailer logo",
						"Please Choose Hot Deal Image since there is no image for Product or Retailer logo");
			}
			if (strBCategory.equalsIgnoreCase(ApplicationConstants.ZERO))
			{
				retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.BUSINESSCATEGORY);
			}
			if ("City".equals(objHotDealsInfo.getDealForCityLoc()))
			{
				if ("".equals(Utility.checkNull(strpCity)))
				{
					retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.POPULATIONCENTER);
				}
			}

			if ("Location".equals(objHotDealsInfo.getDealForCityLoc()))
			{
				if (strRetLoc == 0)
				{
					retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.DEALRETAILERLOC);
				}
			}
			if (objHotDealsInfo != null)
			{
				retailerDealsValidator.validate(objHotDealsInfo, result);
				/*
				 * compDate =
				 * Utility.compareCurrentDate(objHotDealsInfo.getDealEndDate(),
				 * currentDate); if (null != compDate) {
				 * retailerDealsValidator.validate(objHotDealsInfo, result,
				 * ApplicationConstants.DATEENDCURRENT); } else {
				 */
				if (!"".equals(Utility.checkNull(objHotDealsInfo.getDealEndDate()))) {
					compDate = Utility.compareDate(objHotDealsInfo.getDealStartDate(), objHotDealsInfo.getDealEndDate());
					if (null != compDate)
					{
						retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.DATEAFTER);
					}
				} else if (!"".equals(Utility.checkNull(objHotDealsInfo.getDealStartDate())) && !"".equals(Utility.checkNull(objHotDealsInfo.getExpireDate()))) {
					compDate = Utility.compareDate(objHotDealsInfo.getDealStartDate(), objHotDealsInfo.getExpireDate());
					if (null != compDate)
					{
						retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.EXPIREDDATE_GRTN_STARTDATE);
					}
				}
				/* } */

				if (!"".equals(Utility.checkNull(objHotDealsInfo.getDealEndDate())) && !"".equals(Utility.checkNull(objHotDealsInfo.getExpireDate())))
				{
					compDate = Utility.compareDate(objHotDealsInfo.getDealEndDate(), objHotDealsInfo.getExpireDate());
					if (null != compDate)
					{
						retailerDealsValidator.validate(objHotDealsInfo, result, ApplicationConstants.EXPIREDDATEBEFORE);
					}
				}
			}

			if (result.hasErrors())
			{
				view = viewName;
			}
			else
			{
				if (null != objHotDealsInfo.getCity())
				{
					if ("All".equals(objHotDealsInfo.getCity()))
					{
						final ArrayList<City> cities = (ArrayList<City>) request.getSession().getAttribute("hotdealpopcenters");
						for (int i = 0; i < cities.size(); i++)
						{
							allCities.append(cities.get(i).getPopulationCenterID());
							allCities.append(ApplicationConstants.COMMA);
						}
						objHotDealsInfo.setCity(allCities.toString().substring(0, allCities.toString().length() - 1));
					}
				}
				objHotDealsInfo.setRetailID(user.getRetailerId());
				isDataInserted = retailerService.updateRetailerProductHotDeal(objHotDealsInfo);
				String [] strResponse = isDataInserted.split(";");
				bPercentageFlag = Boolean.parseBoolean(strResponse[1]);
			}
			// add validation code here
			if (isDataInserted == null || isDataInserted == ApplicationConstants.FAILURE)
			{
				view = viewName;
				// request.setAttribute(ApplicationConstants.MESSAGE,
				// "Error While Edit HotDeal");
			} else {
				view = viewRedirect;
				request.setAttribute(ApplicationConstants.MESSAGE, "Hot Deal Updated Successfully");
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		/* PercentageFlag returns true, then the discount less than 50%. */
		if (bPercentageFlag)
		{
			view = viewName;
			request.setAttribute(ApplicationConstants.MESSAGE, ApplicationConstants.PERCENTAGEDISCOUNTFLAG);
		}
		if (view.equals(viewRedirect))
		{
			return new ModelAndView(new RedirectView(view));
		}
		else
		{
			if ("City".equals(objHotDealsInfo.getDealForCityLoc()))
			{
				request.getSession().setAttribute("hotdealcity", objHotDealsInfo.getCity());
				session.removeAttribute("hotdeallocation");
			}
			else if ("Location".equals(objHotDealsInfo.getDealForCityLoc()))
			{
				session.removeAttribute("hotdealcity");
				session.setAttribute("hotdeallocation", String.valueOf(objHotDealsInfo.getRetailerLocID()));
			}
			return new ModelAndView(view);
		}
	}

	/**
	 * This ModelAttribute sort Deal start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("DealStartHours")
	public Map<String, String> populateDealStartHrs() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++)
		{
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}


	/**
	 * This ModelAttribute sort Deal start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("DealStartMinutes")
	public Map<String, String> populatemapDealStartMins() throws ScanSeeServiceException
	{
		final HashMap<String, String> mapDealStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++)
		{
			if (i < 10) {
				mapDealStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapDealStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapDealStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapDealStartHrs);
		return sortedMap;
	}

	/**
	 * This controller method will display edit HotDeals details in iphone
	 * screen formate.
	 * 
	 * @param hotDealInfo HotDealInfo instance as request parameter
	 * @param result BindingResult instance as parameter.
	 * @param request HttpServletRequest instance as request parameter.
	 * @param response HttpServletResponse instance as request parameter.
	 * @param session HttpSession instance as parameter.
	 * @param model ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 * @throws IOException will be thrown.
	 */
	@RequestMapping(value = "/previeweditdeals.htm", method = RequestMethod.POST)
	public final ModelAndView previewPage(@ModelAttribute("previeweditdealsform") HotDealInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws IOException,
			ScanSeeServiceException
	{

		LOG.info("Inside RetailerHotDealEditCntroller : previewPage");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		Long retailID = null;
		List<HotDealInfo> dealsProdList = null;
		final String salePrice = request.getParameter("salePrice");
		final String price = request.getParameter("price");
		final String dealStartDate = request.getParameter("dealStartDate");
		final String hotDealLongDescription = request.getParameter("hotDealLongDescription");
		final String hotDealName = request.getParameter("hotDealName");
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		final String prdname = hotDealInfo.getScanCode();
		final String[] str = prdname.split(ApplicationConstants.COMMA);
		final String productName = str[0].trim();
		hotDealInfo.setProductName(productName);
		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		retailID = Long.valueOf(loginUser.getRetailerId());

		request.getSession().setAttribute("editSalePrice", salePrice);
		request.getSession().setAttribute("editPrice", price);
		request.getSession().setAttribute("editDealStartDate", dealStartDate);
		request.getSession().setAttribute("editHotDealLongDescription", hotDealLongDescription);
		request.getSession().setAttribute("editHotDealName", hotDealName);
		session.removeAttribute("hotdeallocation");
		session.removeAttribute("hotdealcity");
		hotDealInfo.setHotDealName(hotDealName);
		hotDealInfo.setDealStartDate(dealStartDate);
		hotDealInfo.setPrice(price);
		hotDealInfo.setSalePrice(salePrice);
		hotDealInfo.setHotDealLongDescription(hotDealLongDescription);
		hotDealInfo.setRetailID(user.getRetailerId());
		if ("City".equals(hotDealInfo.getDealForCityLoc()))
		{
			request.getSession().setAttribute("hotdealcity", hotDealInfo.getCity());
			session.removeAttribute("hotdeallocation");
		}
		else if ("Location".equals(hotDealInfo.getDealForCityLoc()))
		{
			session.removeAttribute("hotdealcity");
			session.setAttribute("hotdeallocation", String.valueOf(hotDealInfo.getRetailerLocID()));
		}
		/*
		 * if (null != hotDealInfo.getDealForCityLoc()) { if
		 * (hotDealInfo.getDealForCityLoc().equals("City")) {
		 * request.getSession().setAttribute("hotdealcity",
		 * hotDealInfo.getCity());
		 * request.getSession().removeAttribute("hotdeallocation"); } else if
		 * (hotDealInfo.getDealForCityLoc().equals("Location")) {
		 * request.getSession().setAttribute("hotdeallocation",
		 * String.valueOf(hotDealInfo.getRetailerLocID()));
		 * request.getSession().removeAttribute("hotdealcity"); } }
		 */
		try
		{
			if (productName != null && !"".equals(productName))
			{
				dealsProdList = retailerService.getProdDetails(productName, retailID);
				if (dealsProdList != null && !dealsProdList.isEmpty())
				{
					session.setAttribute("editretProdImage", dealsProdList.get(0).getProductImagePath());
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("previeweditdealsform", hotDealInfo);
		return new ModelAndView("editdealspreview");
	}

	/**
	 * This controller method will upload Hot deal image .
	 * 
	 * @param request as request parameter.
	 * @param session as request parameter.
	 * @param result as request parameter.
	 * @param objHotDealsInfo instance of HotDealInfo.
	 * @param response as request parameter.
	 * @return string given a view name.
	 * @throws Exception will be thrown.
	 */
	@RequestMapping(value = "/uploadedithotdealsimg.htm", method = RequestMethod.POST)
	public final String onSubmitImage(@ModelAttribute("edithotdealretailerform") HotDealInfo objHotDealsInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws  Exception
	{
		LOG.info("Inside RetailerHotDealEditCntroller : onSubmitImage ");
		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

		//imageValidSizeValFlg = Utility.validImageDimension(70, 70, objHotDealsInfo.getImageFile().getInputStream());

		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, objHotDealsInfo.getImageFile().getInputStream());
		
		
		
		if (imageSizeValFlg)
		{
			session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
			
			//commented code for fixing the crop image issue validation.
			
			/*imageSizeValFlg = true;
			if (imageSizeValFlg)
			{
				final BufferedImage img = ImageIO.read(objHotDealsInfo.getImageFile().getInputStream());
				w = img.getWidth(null);
				h = img.getHeight(null);
				session.setAttribute("imageHt", h);
				session.setAttribute("imageWd", w);

				final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
				// session.removeAttribute("welcomePageBtnVal");
				imageSource = objHotDealsInfo.getImageFile().getOriginalFilename();
				imageSource = FilenameUtils.removeExtension(imageSource);
				imageSource = imageSource + ".png" + "?" + date.getTime();
				final String filePath = tempImgPath + fileSeparator + objHotDealsInfo.getImageFile().getOriginalFilename();
				Utility.writeFileData(objHotDealsInfo.getImageFile(), filePath);
				if (imageValidSizeValFlg)
				{
					strResponse.append("ValidImageDimention");
					strResponse.append("|" + objHotDealsInfo.getImageFile().getOriginalFilename());
					session.setAttribute("addHotDealImagePath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
							+ imageSource);
				}
				strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
				response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
				session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			}
			else
			{
				session.setAttribute("addHotDealImagePath", ApplicationConstants.UPLOADIMAGEPATH);
				response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
				return null;
			}*/
		}
		else
		{
			final BufferedImage img = Utility.getBufferedImageForMinDimension(objHotDealsInfo.getImageFile().getInputStream(),
					request.getRealPath("images"), objHotDealsInfo.getImageFile().getOriginalFilename(), "HotDeal");
			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();
			imageSource = objHotDealsInfo.getImageFile().getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();
			final String filePath = tempImgPath + fileSeparator + objHotDealsInfo.getImageFile().getOriginalFilename();
			Utility.writeImage(img, filePath);
			if (imageValidSizeValFlg)
			{
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objHotDealsInfo.getImageFile().getOriginalFilename());
				session.setAttribute("addHotDealImagePath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/"
						+ imageSource);
			}
			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
		}
		return null;
	}
}

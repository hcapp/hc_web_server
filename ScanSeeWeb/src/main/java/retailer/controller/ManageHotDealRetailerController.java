package retailer.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.HotDealInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * AddHotDealRetailerController is a controller class for managing all retailer
 * product hot deals screen.
 * 
 * @author Created by SPAN.
 */
@Controller
//@RequestMapping("/hotDealRetailer.htm")
public class ManageHotDealRetailerController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageHotDealRetailerController.class);

	/**
	 * This controller method will display all HotDeals or by passing
	 * HotDealName as input parameter by call service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/hotDealRetailer.htm", method = RequestMethod.GET)
	public final ModelAndView showPage (@ModelAttribute("retailerDealsForm") HotDealInfo objHotDealInfo, HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageHotDealRetailerController : showPage");
		request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
		request.getSession().removeAttribute(ApplicationConstants.PAGINATION);
		session.removeAttribute("hotDealsList");
		session.removeAttribute("hotDealId");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		final HotDealInfo dealInfo = new HotDealInfo();
		ArrayList<HotDealInfo> arHotDealInfoList = null;
		
		int lowerLimit = 0;
		dealInfo.setHotDealName(null);
		SearchResultInfo objHotdealList = null;
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		String sDate = null;
		String eDate = null;
		final int iRecordCount = 20;
		try
		{
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				}
				else
				{
					lowerLimit = 0;
				}
			}
			if (objForm == null)
			{
				searhKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}

			objHotdealList = retailerService.getDealsForDisplay(Long.valueOf(user.getRetailerId()), dealInfo.getHotDealName(), lowerLimit,iRecordCount);
			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("Promotions", "Hotdeals", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);
			/*If user has not created any coupon for the first time.It will redirect to Create Coupon page*/
			if (objHotdealList != null && objHotdealList.getDealList().size() <= 0) {
				return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/hotDealRetailorShowPage.htm"));
			}
			if (objHotdealList != null)	
			{
				arHotDealInfoList = retailerService.getAllHotDealsName(Long.valueOf(user.getRetailerId()));
				session.setAttribute("hotDealsList", arHotDealInfoList);
				String salePrice = null;
				for (int i = 0; i < objHotdealList.getDealList().size(); i++)
				{
					salePrice = objHotdealList.getDealList().get(i).getSalePrice();
					salePrice = Utility.formatDecimalValue(salePrice);
					objHotdealList.getDealList().get(i).setSalePrice(salePrice);

					sDate = objHotdealList.getDealList().get(i).getDealStartDate();
					if (!"".equals(Utility.checkNull(sDate))) {
						sDate = Utility.getUsDateFormat(sDate);
						objHotdealList.getDealList().get(i).setDealStartDate(sDate);
					}
					
					eDate = objHotdealList.getDealList().get(i).getDealEndDate();
					if (!"".equals(Utility.checkNull(eDate))) {
						eDate = Utility.getUsDateFormat(eDate);
						objHotdealList.getDealList().get(i).setDealEndDate(eDate);
					}
				}
				session.setAttribute("listOfDeals", objHotdealList);
				final Pagination objPage = Utility.getPagination(objHotdealList.getTotalSize(), currentPage, "hotDealRetailer.htm", iRecordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (NullPointerException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		model.put("retailerDealsForm", dealInfo);
		return new ModelAndView("retailerHotDeal");
	}

	
	/**
	 * This controller method will display all HotDeals or by passing
	 * HotDealName as input parameter by call service and DAO methods.
	 * 
	 * @param dealInfo HotDealInfo instance as request parameter
	 * @param request HttpServletRequest instance.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @param result BindingResult instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws IOException will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/hotDealRetailer.htm", method = RequestMethod.POST)
	public final ModelAndView getRebatesByName(@ModelAttribute("retailerDealsForm") HotDealInfo dealInfo, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws IOException, ScanSeeServiceException
	{
		LOG.info("Inside ManageHotDealRetailerController : getRebatesByName");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		request.getSession().removeAttribute("message");
		request.getSession().removeAttribute("listOfDeals");
		
		// request.getSession().removeAttribute(ApplicationConstants.PAGINATION);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		SearchResultInfo searchResultInfo = null;
		String searhKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		int lowerLimit = 0;
		String sDate = null;
		String eDate = null;
		String hotDealName = null;
		int recordCount = 20;
		try
		{
			if (!"".equals(Utility.checkNull(dealInfo.getRecordCount())))
			{
				recordCount = Integer.valueOf(dealInfo.getRecordCount());
			}
			
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
			}
			if (objForm == null) {
				searhKey = (String) dealInfo.getHotDealName();
				objForm = new SearchForm();
				objForm.setSearchKey(searhKey);
				session.setAttribute("searchForm", objForm);
			}
			if (dealInfo.getHotDealName() != null)
			{
				hotDealName = dealInfo.getHotDealName().trim();
				dealInfo.setHotDealName(hotDealName);
			} else {
				if (objForm != null)
				{
					hotDealName = dealInfo.getHotDealName();
					if (null != hotDealName && !"".equals(hotDealName))
					{
						hotDealName = hotDealName.trim();
					}
					dealInfo.setHotDealName(hotDealName);
				}
			}
			searchResultInfo = retailerService.getDealsForDisplay(Long.valueOf(user.getRetailerId()), dealInfo.getHotDealName(), lowerLimit,recordCount);
			if (!searchResultInfo.getDealList().isEmpty())
			{
				if (searchResultInfo != null)
				{
					String salePrice = null;
					for (int i = 0; i < searchResultInfo.getDealList().size(); i++)
					{
						salePrice = searchResultInfo.getDealList().get(i).getSalePrice();
						salePrice = Utility.formatDecimalValue(salePrice);
						searchResultInfo.getDealList().get(i).setSalePrice(salePrice);

						sDate = searchResultInfo.getDealList().get(i).getDealStartDate();
						if (!"".equals(Utility.checkNull(sDate))) {
							sDate = Utility.getUsDateFormat(sDate);
							searchResultInfo.getDealList().get(i).setDealStartDate(sDate);
						}

						eDate = searchResultInfo.getDealList().get(i).getDealEndDate();
						if (!"".equals(Utility.checkNull(eDate))) {
							eDate = Utility.getUsDateFormat(eDate);
							searchResultInfo.getDealList().get(i).setDealEndDate(eDate);
						}
					}
					session.setAttribute("listOfDeals", searchResultInfo);
					final Pagination objPage = Utility.getPagination(searchResultInfo.getTotalSize(), currentPage, "hotDealRetailer.htm",recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				}
			}
			else
			{
				result.reject("Deals.status");
				request.setAttribute("manageDealsFont", "font-weight:regular;color:red;");
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (NullPointerException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView("retailerHotDeal");
	}
	
	
	
	/**
	 * This controller method will display all HotDeals or by passing
	 * HotDealName as input parameter by call service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/hotdealreportview.htm", method = RequestMethod.GET)
	public final ModelAndView showViewReportPage (@ModelAttribute("retailerDealsForm") HotDealInfo dealInfo, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageHotDealRetailerController : showViewReportPage");
		request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
		request.getSession().removeAttribute(ApplicationConstants.PAGINATION);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		HotDealInfo objHotDealInfo = new HotDealInfo();
		objHotDealInfo.setHotDealID(dealInfo.getHotDealID());
		ArrayList<HotDealInfo> arHotDealInfoList = null;
		int lowerLimit = 0;
		String strSearchKey = null;
		int currentPage = 1;
		String pageNumber = "0";
		SearchForm objForm = null;
		final int iRecordCount = 20;
		
		try {
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
			}
			if (null == objForm) {
				strSearchKey = (String) request.getParameter("searchKey");
				objForm = new SearchForm();
				objForm.setSearchKey(strSearchKey);
				session.setAttribute("searchForm", objForm);
			}
			dealInfo = retailerService.getHotDealViewReport(dealInfo.getHotDealID(), Long.valueOf(user.getRetailerId()), lowerLimit, iRecordCount);
			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("Promotions", "Hotdeals", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);
			if (dealInfo != null) {
				arHotDealInfoList = retailerService.getAllHotDealsName(Long.valueOf(user.getRetailerId()));
				session.setAttribute("hotDealsList", arHotDealInfoList);
				session.setAttribute("dealInfo", dealInfo);
				session.setAttribute("hotDealId", Integer.toString(dealInfo.getHotDealID()));
				final Pagination objPage = Utility.getPagination(dealInfo.getTotalSize(), currentPage, "hotdealreportview.htm", iRecordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("retailerDealsViewForm", objHotDealInfo);
		return new ModelAndView("retHotDealViewReport");
	}
	
	
	/**
	 * This controller method will display all HotDeals details or by passing
	 * HotDealName as input parameter by call service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/hotdealreportview.htm", method = RequestMethod.POST)
	public final ModelAndView showHDealViewReportPage (@ModelAttribute("retailerDealsViewForm") HotDealInfo dealInfo, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageHotDealRetailerController : showHDealViewReportPage");
		request.getSession().removeAttribute(ApplicationConstants.MESSAGE);
		session.removeAttribute("dealInfo");
		session.removeAttribute("hotDealsList");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		ArrayList<HotDealInfo> arHotDealInfoList = null;
		HotDealInfo objHotDealInfo = new HotDealInfo();
		
		int iLowerLimit = 0;
		String strSearchKey = null;
		int iCurrentPage = 1;
		String pageNumber = "0";
		int iHotDealId = 0;
		SearchForm objForm = null;
		int iRecordCount = 20;
		try {
			if (!"".equals(Utility.checkNull(dealInfo.getRecordCount()))) {
				iRecordCount = Integer.valueOf(dealInfo.getRecordCount());
			}
			final String pageFlag = (String) request.getParameter("pageFlag");
			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) {
					iCurrentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(iCurrentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					iLowerLimit = pageSize * Integer.valueOf(number);
				} else {
					iLowerLimit = 0;
				}
			}
			if (objForm == null) {
				strSearchKey = (String) dealInfo.getHotDealName();
				objForm = new SearchForm();
				objForm.setSearchKey(strSearchKey);
				session.setAttribute("searchForm", objForm);
			}
			if (!"".equals(Utility.checkNull(dealInfo.getHotDealName()))) {
				session.removeAttribute("hotDealId");
				iHotDealId = Integer.parseInt(dealInfo.getHotDealName());
				objHotDealInfo.setHotDealID(iHotDealId);
				session.setAttribute("hotDealId", dealInfo.getHotDealName());
			} else {
				iHotDealId = Integer.parseInt((String) session.getAttribute("hotDealId"));
			}
			dealInfo = retailerService.getHotDealViewReport(iHotDealId, Long.valueOf(user.getRetailerId()), iLowerLimit, iRecordCount);
			if (null != dealInfo) {
				arHotDealInfoList = retailerService.getAllHotDealsName(Long.valueOf(user.getRetailerId()));
				session.setAttribute("hotDealsList", arHotDealInfoList);
				session.setAttribute("dealInfo", dealInfo);
				final Pagination objPage = Utility.getPagination(dealInfo.getTotalSize(), iCurrentPage, "hotdealreportview.htm", iRecordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				session.removeAttribute(ApplicationConstants.PAGINATION);
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		};
		model.put("retailerDealsViewForm", objHotDealInfo);
		return new ModelAndView("retHotDealViewReport");
	}
	
	
	/**
	 * This controller method will display all HotDeals details or by passing
	 * HotDealName as input parameter by call service and DAO methods.
	 * 
	 * @param request HttpServletRequest instance.
	 * @param model ModelMap instance as parameter.
	 * @param session HttpSession instance as parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException will be thrown.
	 */
	@RequestMapping(value = "/hotdealviewreportprint.htm", method = RequestMethod.GET)
	public final ModelAndView showHDealViewReportPrint (HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside ManageHotDealRetailerController : showHDealViewReportPrint");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users user = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		HotDealInfo objHDealInfo = null;
		int iHotDealId = 0;
		final int iLowerLimit = 0;
		final int iRecordCount = 0;
		iHotDealId = Integer.parseInt((String) session.getAttribute("hotDealId"));
		objHDealInfo = retailerService.getHotDealViewReport(iHotDealId, Long.valueOf(user.getRetailerId()), iLowerLimit, iRecordCount);
		if (null != objHDealInfo) {
			session.setAttribute("dealInfo", objHDealInfo);
		} 
		return new ModelAndView("retHotDealViewReportPrint");
	}
}


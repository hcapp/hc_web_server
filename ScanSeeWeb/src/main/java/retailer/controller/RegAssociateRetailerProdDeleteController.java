/**
 * @ (#) Associate RegAssociateRetailerProdDeleteController.java 02-Feb-2012
 * Project       :ScanSeeWeb
 * File          : AssociateRetailerProdDeleteController.java
 * Author        : Kumar D
 * Company       : Span Systems Corporation
 * Date Created  : 02-Feb-2012
 */
package retailer.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocationProduct;
import common.pojo.Users;
@Controller

public class RegAssociateRetailerProdDeleteController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(RegAssociateRetailerProdDeleteController.class);
	
	final public String RETURN_VIEW = "regaddsearchprod";

	@RequestMapping(value = "/registProductsetup.htm", method = RequestMethod.POST)
	public ModelAndView productSetupPage(
			@ModelAttribute("retprodsetupform") RetailerLocationProduct objRetailerLocationProd,
			HttpServletRequest request, HttpSession session) throws ScanSeeServiceException{
		log.info("Inside RegAssociateRetailerProdDeleteController : productSetupPage ");
		String response = null;
		Long objRetailID = new Long(0);
		Long objProductID = new Long(0);
		try {
			request.getSession().removeAttribute("message");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			objRetailID = Long.valueOf(loginUser.getRetailerId());
			objProductID = objRetailerLocationProd.getProductID();
			response = retailerService.deleteProductsFromLocHoldControl(objRetailID, objProductID);
		} catch (ScanSeeServiceException e) {
			log.error("Exception occurred in  productSetupPage:::::"+ e.getMessage());
            throw new ScanSeeServiceException(e);
		}
		if(response.equals(ApplicationConstants.SUCCESS)){
			log.info("Inside RegAssociateRetailerProdDeleteController : productSetupPage : Deleted Successfull !!!!" );
			request.getSession().setAttribute("message", "This product associated with Retailer Location(s) is deleted Successfully!!!");
		}
		return new ModelAndView(RETURN_VIEW);
	}
}

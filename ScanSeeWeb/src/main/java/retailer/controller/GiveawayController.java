package retailer.controller;

import java.text.ParseException;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;
import retailer.validator.GiveawayValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerCustomPage;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

@Controller
public class GiveawayController
{

	GiveawayValidator giveawayValidator;

	@Autowired
	public void setGiveawayValidator(GiveawayValidator giveawayValidator)
	{
		this.giveawayValidator = giveawayValidator;
	}

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(GiveawayController.class);

	/**
	 * This method is for displaying Build Giveaway page.
	 * 
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return viewname 'giveawaypage'
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/buildgiveawaypage", method = RequestMethod.POST)
	public String showGiveAwayPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		LOG.info("Inside the Method showGiveAwayPage ");
		String viewName = "giveawaypage";
		session.setAttribute("imageCropPage", "SpecialOfferPage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		model.put("createCustomPage", retailerCustomPage);
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);

		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		leftNav = UtilCode.setRetailerModulesStyle("Promotions", "Giveaway", leftNav);
		session.setAttribute("retlrLeftNav", leftNav);

		LOG.info("Exit Method showCreatCustomPage ");
		return viewName;
	}

	/**
	 * This method validates Giveaway page information and if everything valid
	 * saves the details and generates QR code image.
	 * 
	 * @param retailerCustomPage
	 *            modelAttribute as a parameter
	 * @param result
	 *            as a parameter
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return viewname 'showqrcode'
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/savegiveawaypage", method = RequestMethod.POST)
	public ModelAndView saveGiveAwayPage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		LOG.info("Inside the Method saveGiveAwayPage ");
		String response = null;
		String showQRCode = "showqrcode";
		String compDate = null;
		Date currentDate = new Date();
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int retailID = loginUser.getRetailerId();
		retailerCustomPage.setRetailerId(retailID);
		String viewName = "giveawaypage";
		giveawayValidator.validate(retailerCustomPage, result);
		FieldError fieldStartDate = result.getFieldError("retCreatedPageStartDate");
		FieldError fieldEndDate = result.getFieldError("retCreatedPageEndDate");

		FieldError fieldQuantity = result.getFieldError("quantity");

		session.setAttribute("qrCodeImageTitle", retailerCustomPage.getRetPageTitle());
		// retailerCustomPage.setHiddenLocId(retailerCustomPage.getSplOfferLocId());
		if (null == fieldStartDate)
		{
			if (!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageStartDate())))
			{
				compDate = Utility.compareCurrentDate(retailerCustomPage.getRetCreatedPageStartDate(), currentDate);
				if (null != compDate)
				{
					giveawayValidator.validate(retailerCustomPage, result, ApplicationConstants.DATESTARTCURRENT);
				}
			}
		}
		if (!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate())))
		{
			if (null == fieldEndDate)
			{
				compDate = Utility.compareCurrentDate(retailerCustomPage.getRetCreatedPageEndDate(), currentDate);
				if (null != compDate)
				{
					giveawayValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEENDCURRENT);
				}

				else if (null == fieldStartDate)
				{
					compDate = Utility.compareDate(retailerCustomPage.getRetCreatedPageStartDate(), retailerCustomPage.getRetCreatedPageEndDate());
					if (null != compDate)
					{
						giveawayValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEAFTER);
					}
				}
			}
		}

		if (null == fieldQuantity)
		{

			if (retailerCustomPage.getQuantity() <= 0)
			{
				giveawayValidator.validate(retailerCustomPage, result, ApplicationConstants.QUANTITY);
			}
		}

		if (result.hasErrors())
		{

			return new ModelAndView(viewName);

		}
		else
		{
			response = retailerService.createGiveAwayPage(retailerCustomPage);
			if (null != response && !response.equals(ApplicationConstants.FAILURE))
			{
				session.setAttribute("qrCodeImagePath", response);
				request.setAttribute("splOffer", "giveaway");
				return new ModelAndView(showQRCode);
			}
		}
		LOG.info("Exit Method saveGiveAwayPage ");
		return new ModelAndView(viewName);
	}

	/**
	 * This method displays Giveaway page information for updation.
	 * 
	 * @param retailerCustomPage
	 *            modelAttribute as a parameter
	 * @param result
	 *            as a parameter
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return viewname 'updategiveawaypage'
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/displaygiveawaypageinfo", method = RequestMethod.POST)
	public ModelAndView displayGiveawayDetails(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPageObj,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		LOG.info("Inside the Method displayGiveawayDetails ");
		String viewName = "updategiveawaypage";
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		session.removeAttribute("customPageImgPath");
		session.removeAttribute("UploadedFile");
		session.removeAttribute("UploadedFileLength");
		session.setAttribute("imageCropPage", "SpecialOfferPage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Long retailID = null;
		Long pageId;
		pageId = retailerCustomPageObj.getPageId();
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		retailID = Long.valueOf(loginUser.getRetailerId());
		
		RetailerCustomPage retailerCustomPage = null;
		try
		{

			retailerCustomPage = retailerService.getGiveAwayPageInfo(retailID, pageId);
			retailerCustomPage.setPageId(pageId);
			retailerCustomPage.setRetCreatedPageStartDate(Utility.getCalenderFormattedDate(retailerCustomPage.getStartDate()));
			retailerCustomPage.setRetCreatedPageEndDate(Utility.getCalenderFormattedDate(retailerCustomPage.getEndDate()));

			if (null != retailerCustomPage.getImagePath() && !"".equals(retailerCustomPage.getImagePath()))
			{
				session.setAttribute("customPageRetImgPath", retailerCustomPage.getImagePath());
				session.setAttribute("customPageBtnVal", "Change Photo");
				retailerCustomPage.setRetailerImg(retailerCustomPage.getImageName());

			}
			else
			{
				session.setAttribute("customPageBtnVal", "Upload Photo");
				session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			}
			model.put("createCustomPage", retailerCustomPage);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw new ScanSeeServiceException(exception);

		}
		catch (ParseException e)
		{
			LOG.error(e.getMessage(), e);
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Exit Method displayGiveawayDetails ");
		return new ModelAndView(viewName);
	}

	/**
	 * This method validates Giveaway page information , if everything valid
	 * updates details and generates QR code image.
	 * 
	 * @param retailerCustomPage
	 *            modelAttribute as a parameter
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return viewname 'showqrcode'
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/updategiveawaypage.htm", method = RequestMethod.POST)
	public ModelAndView updateGiveawayPage(@ModelAttribute("createCustomPage") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside the Method updateGiveawayPage ");
		String response = null;
		String viewName = "updategiveawaypage";
		String showQRCode = "showqrcode";
		String compDate = null;
		session.removeAttribute("qrCodeImagePath");
		session.removeAttribute("qrCodeImageTitle");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		int retailID = loginUser.getRetailerId();
		retailerCustomPage.setRetailerId(retailID);

		giveawayValidator.validate(retailerCustomPage, result);
		FieldError fieldStartDate = result.getFieldError("retCreatedPageStartDate");
		FieldError fieldEndDate = result.getFieldError("retCreatedPageEndDate");
		FieldError fieldQuantity = result.getFieldError("quantity");
		session.setAttribute("qrCodeImageTitle", retailerCustomPage.getRetPageTitle());
		// retailerCustomPage.setHiddenLocId(retailerCustomPage.getSplOfferLocId());

		if (!"".equals(Utility.checkNull(retailerCustomPage.getRetCreatedPageEndDate())))
		{
			if (null == fieldStartDate && null == fieldEndDate)
			{

				compDate = Utility.compareDate(retailerCustomPage.getRetCreatedPageStartDate(), retailerCustomPage.getRetCreatedPageEndDate());
				if (null != compDate)
				{
					giveawayValidator.validate(retailerCustomPage, result, ApplicationConstants.DATEAFTER);
				}

			}
		}
		if (null == fieldQuantity)
		{

			if (retailerCustomPage.getQuantity() <= 0)
			{
				giveawayValidator.validate(retailerCustomPage, result, ApplicationConstants.QUANTITY);
			}
		}

		if (result.hasErrors())
		{
			return new ModelAndView(viewName);
		}
		else
		{

			response = retailerService.updateGiveawayPageInfo(retailerCustomPage);
			if (null != response && !response.equals(ApplicationConstants.FAILURE))
			{
				request.setAttribute("splOffer", "giveaway");
				session.setAttribute("qrCodeImagePath", response);
				return new ModelAndView(showQRCode);
			}
		}
		LOG.info("Exit Method updateGiveawayPage ");
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used to display the list of give away promotion pages.
	 * 
	 * @param objCustomPage
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/managegiveawaypages.htm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView manageGiveAwayPages(RetailerCustomPage objCustomPage, ModelMap model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "manageGiveAwayPages";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		final String pageFlag = (String) request.getParameter("pageFlag");
		String pageNumber = "0";
		int lowerLimit = 0;
		int currentPage = 1;
		String searchKey = null;
		SearchResultInfo result = new SearchResultInfo();
		int recordCount = 20;
		session.setAttribute("customPageRetImgPath", ApplicationConstants.UPLOADIMAGEPATH);
		session.setAttribute("imageCropPage", "SpecialOfferPage");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);
		try
		{
			if (null != objCustomPage.getRecordCount() && !"".equals(objCustomPage.getRecordCount())
					&& !"undefined".equals(objCustomPage.getRecordCount()))
			{
				recordCount = Integer.valueOf(objCustomPage.getRecordCount());
			}
			if (null != pageFlag && "true".equals(pageFlag))
			{
				pageNumber = request.getParameter("pageNumber");
				objCustomPage.setPageNumber(pageNumber);
				session.setAttribute("pageNumber", pageNumber);
				final Pagination pageSess = (Pagination) session.getAttribute("pagination");
				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);
				}
				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));

			}
			else
			{
				objCustomPage.setPageNumber(String.valueOf(currentPage));
			}
			searchKey = request.getParameter("searchKey");
			result = retailerService.getGiveAwayList(loginUser.getRetailerId(), searchKey, lowerLimit, recordCount);
			if (searchKey == null)
			{
				searchKey = "";
			}
			request.setAttribute("customPageList", result.getPageList());
			final Pagination objPage = Utility.getPagination(result.getTotalSize(), currentPage, "managegiveawaypages.htm", recordCount);
			if (objPage.getTotalSize() == 20)
			{
				objPage.setPageRange(20);
			}
			session.setAttribute("pagination", objPage);
			model.put("createCustomPage", objCustomPage);

			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("Promotions", "Giveaway", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (result.getTotalSize() > 0 || !"".equals(searchKey))
		{
			if (result.getTotalSize() == 0)
			{
				request.setAttribute("message", "No records found!");
			}
			return new ModelAndView("giveAwayPageList");
		}

		return new ModelAndView("giveawaypage");
	}

	/**
	 * This method is used for deleting Give Away Promotion page.
	 * 
	 * @param customPageForm
	 * @param result
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/deleteGiveAwayPage", method = RequestMethod.POST)
	public ModelAndView deleteGiveAwayPage(@ModelAttribute("createCustomPage") RetailerCustomPage customPageForm, BindingResult result,
			ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "deleteGiveAwayPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		SearchResultInfo resultLst = null;
		final Pagination pageSess = (Pagination) session.getAttribute("pagination");
		String pageNumber = "0";
		pageNumber = request.getParameter("pageNumber");
		int recordCount = 20;
		try
		{
			final String status = retailerService.deleteGiveAwayPage(customPageForm.getPageId(), loginUser.getRetailerId());

			if (null != customPageForm.getRecordCount() && !"".equals(customPageForm.getRecordCount())
					&& !"undefined".equals(customPageForm.getRecordCount()))
			{
				recordCount = Integer.valueOf(customPageForm.getRecordCount());
			}

			if (ApplicationConstants.SUCCESS.equals(status))
			{
				request.setAttribute("message", "Deleted Successfully.");
				final Long lSize = pageSess.getTotalSize();
				if (lSize == 21)
				{
					pageNumber = "1";
					pageSess.setTotalSize(20);
					session.setAttribute("pageNumber", pageNumber);
				}
			}
			final String pageFlag = (String) request.getParameter("pageFlag");

			int lowerLimit = 0;
			int currentPage = 1;
			String searchKey = null;
			int pageSize = 0;
			if (null != pageFlag && "true".equals(pageFlag))
			{
				if (Integer.valueOf(pageNumber) != 0)
				{
					if (pageSess.getTotalSize() > 20)
					{
						currentPage = Integer.valueOf(pageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					}
					else
					{
						lowerLimit = 0;
					}
				}
				else
				{
					lowerLimit = 0;
				}
			}
			searchKey = customPageForm.getSearchKey();
			resultLst = retailerService.getGiveAwayList(loginUser.getRetailerId(), searchKey, lowerLimit, recordCount);
			request.setAttribute("customPageList", resultLst);
			final Pagination objPage = Utility.getPagination(resultLst.getTotalSize(), currentPage, "managegiveawaypages.htm", recordCount);
			session.setAttribute("pagination", objPage);
			request.setAttribute("customPageList", resultLst.getPageList());
			model.put("createCustomPage", customPageForm);

			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("Promotions", "Giveaway", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("giveAwayPageList");

	}

	/**
	 * This method is for displaying the QRCode for give away page.
	 * 
	 * @param customPageForm
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/getgiveawaypageqr", method = RequestMethod.GET)
	public ModelAndView showQrCodeForGAPage(@ModelAttribute("customPageForm") RetailerCustomPage customPageForm, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "showQrCodeForGAPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		try
		{

			final RetailerCustomPage pageDetails = retailerService.getGAQrCodeDetails(customPageForm.getPageId(), loginUser.getRetailerId());
			request.setAttribute("pageDetails", pageDetails);

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		request.setAttribute("QRType", "GiveAway");
		return new ModelAndView("showQrpage");
	}

}

/**
 * 
 */
package retailer.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.scansee.externalapi.common.constants.ApplicationConstants;

import common.exception.ScanSeeServiceException;
import common.pojo.Event;
import common.pojo.RetailerLocation;

/**
 * @author sangeetha.ts
 *
 */
@Controller
public class InstructionsController {
	
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(InstructionsController.class);
	
	@RequestMapping(value = "/eventinstructions.htm", method = RequestMethod.GET)
	public final String showEventIntructionstPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "showEventIntructionstPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);	
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "eventinstructions";		
	}
	
	@RequestMapping(value = "/fundraiserinstructions.htm", method = RequestMethod.GET)
	public final String showFundraiserIntructionstPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "showFundraiserIntructionstPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "fundraiserinstructions";		
	}
	
	@RequestMapping(value = "/locationinstructions.htm", method = RequestMethod.GET)
	public final String showLocationIntructionstPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "showLocationIntructionstPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "locationinstructions";		
	}
	
	@RequestMapping(value = "/dashboardlocinstructions.htm", method = RequestMethod.GET)
	public final String showDashboardLocIntructionstPage(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "showDashboardLocIntructionstPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "dashboardlocinstructions";		
	}
}

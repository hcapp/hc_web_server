/**
 * @ (#) RegAssociateRetailerProdController.java 06-Jan-2012
 * Project       :ScanSeeWeb
 * File          : RegAssociateRetailerProdController.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 06-Jan-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.controller;

import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import retailer.service.RetailerService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.RetailerLocationProduct;
import common.pojo.RetailerLocationProductJson;
import common.pojo.Users;
import common.util.Utility;

@Controller
public class RegAssociateRetailerProdController
{
	private static final Logger LOG = LoggerFactory.getLogger(RebateProductUPCListController.class);
	final public String RETURN_VIEW = "regaddsearchprod";

	@RequestMapping(value = "/regassociateprod.htm", method = RequestMethod.POST)
	@ResponseBody
	public String productSetupPage(@RequestParam(value = "data", required = true) String data, HttpServletRequest request, HttpSession session)
			throws ScanSeeServiceException
	{
		String response = ApplicationConstants.FAILURE;
		String compStartDate = null;
		String compEndDate = null;
		String compDate = null;
		Date currentDate = new Date();

		try
		{
			request.getSession().removeAttribute("message");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			Long retailID = Long.valueOf(loginUser.getRetailerId());
			RetailerLocationProductJson prodJson = Utility.jsonToProdInfo(data);
			RetailerLocationProduct objRetailLoctnProduct = new RetailerLocationProduct();
			objRetailLoctnProduct.setRetailID(retailID);

			if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
			{
				if (!Utility.isValidDate(prodJson.getSaleStartDate()))
				{
					response = "Please Enter Valid Start Date";
					return response;
				}
				else
				{
					compStartDate = Utility.compareCurrentDate(prodJson.getSaleStartDate(), currentDate);
					if (null != compStartDate)
					{

						response = "Start date should be greater than or equal to current date";
					}
				}

			}
			if (!prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
			{
				if (!Utility.isValidDate(prodJson.getSaleEndDate()))
				{
					response = "Please Enter Valid End Date";
					return response;
				}
				else
				{
					compEndDate = Utility.compareCurrentDate(prodJson.getSaleEndDate(), currentDate);
					if (null != compEndDate)
					{

						response = "End date should be greater than or equal to current date";
					}
				}
			}
			if (null != compStartDate && null != compEndDate)
			{
				response = "Start date and End date should be greater than or equal to current date";
			}
			if (null != compStartDate && null != compEndDate)
			{
				if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals("")
						&& !prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
				{
					compDate = Utility.compareDate(prodJson.getSaleStartDate(), prodJson.getSaleEndDate());

					if (null != compDate)
					{

						response = "End date should be greater than or equal to Start date";

					}
				}

			}

			if (null == compStartDate && null == compEndDate && null == compDate)
			{

				if (!prodJson.getPrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getPrice())).equals(""))
				{
					objRetailLoctnProduct.setPrice(Double.valueOf(prodJson.getPrice()));
				}
				if (!prodJson.getSalePrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getSalePrice())).equals(""))
				{
					objRetailLoctnProduct.setSalePrice(Double.valueOf(prodJson.getSalePrice()));
				}
				if (!Utility.checkNull(prodJson.getDescription()).equals(""))
				{
					objRetailLoctnProduct.setRetailLocationProductDesc(prodJson.getDescription());
				}
				if (!Utility.checkNull(prodJson.getLocation()).equals(""))
				{
					objRetailLoctnProduct.setLocID(prodJson.getLocation());
				}
				if (!Utility.checkNull(prodJson.getProductID()).equals(""))
				{
					objRetailLoctnProduct.setProductID(Long.valueOf(prodJson.getProductID()));
				}
				if (!Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
				{
					objRetailLoctnProduct.setSaleStartDate(prodJson.getSaleStartDate());
				}
				if (!Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
				{
					objRetailLoctnProduct.setSaleEndDate(prodJson.getSaleEndDate());
				}
				response = retailerService.associateUpdateRetProd(objRetailLoctnProduct);
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in  productSetupPage:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		if (null != response)
		{
			if (response.equals(ApplicationConstants.SUCCESS))
			{

				response = "product is added successfully for selected location(s).";
			}
		}

		return response;

	}

	@RequestMapping(value = "/updateRegRetAssociateProd.htm", method = RequestMethod.POST)
	@ResponseBody
	public String updateRegRetManageProduct(@RequestParam(value = "data", required = true) String data, HttpServletRequest request,
			HttpSession session) throws ScanSeeServiceException
	{
		LOG.info("Inside RegAssociateRetailerProdController :updateRegRetManageProduct ");
		String response = null;
		String compStartDate = null;
		String compEndDate = null;
		String compDate = null;
		Date currentDate = new Date();
		try
		{
			request.getSession().removeAttribute("message");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			Long retailID = Long.valueOf(loginUser.getRetailerId());
			RetailerLocationProductJson prodJson = Utility.jsonToProdInfo(data);
			RetailerLocationProduct objRetailLoctnProduct = new RetailerLocationProduct();
			objRetailLoctnProduct.setRetailID(retailID);
			
			if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
			{
				if (!Utility.isValidDate(prodJson.getSaleStartDate()))
				{
					response = "Please Enter Valid Start Date";
					return response;
				}

			}
			if (!prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
			{
				if (!Utility.isValidDate(prodJson.getSaleEndDate()))
				{
					response = "Please Enter Valid End Date";
					return response;
				}

			}

			if (!prodJson.getSaleStartDate().equals("") && !Utility.checkNull(prodJson.getSaleStartDate()).equals("")
					&& !prodJson.getSaleEndDate().equals("") && !Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
			{
				compDate = Utility.compareDate(prodJson.getSaleStartDate(), prodJson.getSaleEndDate());

				if (null != compDate)
				{

					response = "End date should be greater than or equal to Start date";

				}
			}
			if (null == compStartDate && null == compEndDate && null == compDate)
			{

				if (!prodJson.getPrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getPrice())).equals(""))
				{
					objRetailLoctnProduct.setPrice(Double.valueOf(prodJson.getPrice()));
				}
				if (!prodJson.getSalePrice().equals("") && !Utility.checkNull(Double.valueOf(prodJson.getSalePrice())).equals(""))
				{
					objRetailLoctnProduct.setSalePrice(Double.valueOf(prodJson.getSalePrice()));
				}
				if (!Utility.checkNull(prodJson.getDescription()).equals(""))
				{
					objRetailLoctnProduct.setRetailLocationProductDesc(prodJson.getDescription());
				}
				if (!Utility.checkNull(prodJson.getLocation()).equals(""))
				{
					objRetailLoctnProduct.setLocID(prodJson.getLocation());
				}
				if (!Utility.checkNull(prodJson.getProductID()).equals(""))
				{
					objRetailLoctnProduct.setProductID(Long.valueOf(prodJson.getProductID()));
				}
				if (!Utility.checkNull(prodJson.getSaleStartDate()).equals(""))
				{
					objRetailLoctnProduct.setSaleStartDate(prodJson.getSaleStartDate());
				}
				if (!Utility.checkNull(prodJson.getSaleEndDate()).equals(""))
				{
					objRetailLoctnProduct.setSaleEndDate(prodJson.getSaleEndDate());
				}
				response = retailerService.associateUpdateRetProd(objRetailLoctnProduct);
			}
			objRetailLoctnProduct.setProductID(Long.valueOf(prodJson.getProductID()));
			response = retailerService.associateUpdateRetProd(objRetailLoctnProduct);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info("Inside AssociateRetailerProdController :updateManageProduct " + e.getMessage());
			LOG.error("Inside AssociateRetailerProdController :updateManageProduct " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		if (response.equals(ApplicationConstants.SUCCESS))
		{
			response = "Product details updated successfully!";
		}
		return response;
	}
}

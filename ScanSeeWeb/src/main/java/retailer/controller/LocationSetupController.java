package retailer.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.annotation.JsonFormat.Value;

import retailer.service.RetailerService;
import retailer.validator.LocationSetUpValidator;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ContactType;
import common.pojo.HotDealInfo;
import common.pojo.LocationResult;
import common.pojo.RetailProduct;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerLocation;
import common.pojo.SearchResultInfo;
import common.pojo.State;
import common.pojo.StoreInfoVO;
import common.pojo.TimeZones;
import common.pojo.Users;
import common.tags.Pagination;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

/**
 * LocationSetupController is a controller class for user to add new location,
 * upload multiple locations with the help of CSV file templates and manage
 * locations.
 * 
 * @author Created by SPAN.
 */
@Controller
public class LocationSetupController {
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(LocationSetupController.class);

	/**
	 * Variable VIEW_NAME declared as constant string.
	 */
	private static final String VIEW_NAME = "locationsetup";

	/**
	 * Variable ADD_LOCATION declared as constant string.
	 */
	private static final String ADD_LOCATION = "addlocation";
	/**
	 * Variable UPLOADLOGO declared as constant string.
	 */
	private static final String UPLOADLOGO = "backRetailerLogo";
	/**
	 * Variable UPLOADLOGO declared as constant string.
	 */
	private static final String LOCATIONLIST = "locationlist";
	/**
	 * Variable UPLOADLOGO declared as constant string.
	 */
	private static final String CONTACTTYPES = "contactTypes";
	/**
	 * Variable UPLOADLOGO declared as constant string.
	 */
	private static final String LOCATIONSETUPFORM = "locationsetupform";
	/**
	 * Variable FROMDASHBOARD declared as constant string.
	 */
	private static final String FROMDASHBOARD = "fromDashBoard";

	/**
	 * Variable MANAGELOCATIONDASHBOARD_HTM declared as constant string.
	 */
	private static final String MGLOCATIONDASHBOARD_HTM = "/ScanSeeWeb/retailer/manageLocationDashboard.htm";

	/**
	 * Variable locationSetUpValidator declared as instance of
	 * LocationSetUpValidator.
	 */
	private LocationSetUpValidator locationSetUpValidator;

	/**
	 * Variable view declared.
	 */
	private String view = null;

	/**
	 * To set retailerProfileValidator.
	 * 
	 * @param locationSetUpValidator
	 *            to set.
	 */
	@Autowired
	public final void setLocationSetUpValidator(LocationSetUpValidator locationSetUpValidator) {
		this.locationSetUpValidator = locationSetUpValidator;
	}

	/**
	 * This controller method will display the manage location screen by call
	 * service layer.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/locationsetup.htm", method = RequestMethod.GET)
	public final String showLocationSetupPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : showLocationSetupPage");
		setupUpdateLocationPage(request, model, session);
		return VIEW_NAME;
	}

	/**
	 * This controller method will edit/update all the location in grid by call
	 * service layer..
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	private void setupUpdateLocationPage(HttpServletRequest request, ModelMap model, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : setupUpdateLocationPage");

		session.removeAttribute(LOCATIONLIST);
		session.removeAttribute(ApplicationConstants.PAGINATION);

		ArrayList<ContactType> contactTypes = null;
		final int currentPage = 1;
		final int lowerLimit = 0;

		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		final int retailerId = loginUser.getRetailerId();
		final Long userId = loginUser.getUserID();

		session.setAttribute("imageCropPage", "Locationsetup");
		session.setAttribute("minCropWd", 28);
		session.setAttribute("minCropHt", 28);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		final RetailerLocation retailerLocation = new RetailerLocation();
		retailerLocation.setRetailerID((long) loginUser.getRetailerId());

		final SearchResultInfo locResult = retailerService.getRetailerLocationList(retailerId, userId, lowerLimit);
		session.setAttribute(LOCATIONLIST, locResult);

		final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "updatelocationpageinfo.htm");
		session.setAttribute(ApplicationConstants.PAGINATION, objPage);

		contactTypes = retailerService.getAllContactTypes();
		request.setAttribute(CONTACTTYPES, contactTypes);
		model.put(LOCATIONSETUPFORM, retailerLocation);
	}

	/**
	 * This controller method upload multiple locations with the help of CSV
	 * file templates by call service layer.
	 * 
	 * @param retailerLocation
	 *            as request parameter.
	 * @param request
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             on input error.
	 * @throws NoSuchAlgorithmException
	 *             on input error.
	 * @throws NoSuchPaddingException
	 *             on input error.
	 * @throws IOException
	 *             on input error.
	 */
	@RequestMapping(value = "/locationbatchupload.htm", method = RequestMethod.POST)
	public final String batchUpdateLocationSetup(@ModelAttribute(LOCATIONSETUPFORM) RetailerLocation retailerLocation, BindingResult result, ModelMap model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException, ScanSeeServiceException,
			NoSuchAlgorithmException, NoSuchPaddingException {
		LOG.info("Inside LocationSetupController : batchUpdateLocationSetup");
		String serviceResponse = null;
		final int lowerLimit = 0;
		final int currentPage = 1;

		final boolean bKeyStatus = Utility.isKeySame(session, retailerLocation.getKey());
		final String dashboardFlag = (String) request.getParameter("dashBoardFlag");
		final RetailerLocation retailerLocation1 = new RetailerLocation();
		model.put(LOCATIONSETUPFORM, retailerLocation1);
		/*
		 * To avoid On F5 key pressed, ctrl + r, right click select reload,
		 * Location upload will happen repeatedly.
		 */
		if (bKeyStatus) {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			String autoGenKey = null;
			try {
				final CommonsMultipartFile locationFile = retailerLocation.getLocationSetUpFile();
				final long locationFileSize = locationFile.getSize();
				LOG.info("Location File Size --->" + locationFileSize);

				if (locationFileSize <= (1024 * 1024)) {
					final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
					final int retailerId = loginUser.getRetailerId();
					final Long userId = loginUser.getUserID();
					serviceResponse = retailerService.saveBatchLocation(retailerLocation, userId, retailerId);

					final SearchResultInfo locResult = retailerService.getRetailerLocationList(retailerId, userId, lowerLimit);
					String phoneNum = null;
					for (int i = 0; i < locResult.getLocationList().size(); i++) {
						phoneNum = locResult.getLocationList().get(i).getPhonenumber().trim();
						locResult.getLocationList().get(i).setPhonenumber(phoneNum);
					}

					session.setAttribute(LOCATIONLIST, locResult);
					final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "locationbatchupload.htm");
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);

					if (serviceResponse.equals(ApplicationConstants.SUCCESS)) {
						request.setAttribute("locationuploadMsg", "Locations uploaded Successfully");
						request.setAttribute("locationuploadFile", "font-weight:bold;color:#00559c;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					} else if (serviceResponse.equals(ApplicationConstants.FAILURE)) {
						request.setAttribute("locationuploadMsg",
								"Location(s) information is not uploaded, please check your mail box for unsuccessful upload Location(s).");
						request.setAttribute("locationuploadFile", "font-weight:bold;color:red;");
						autoGenKey = Utility.randomString(5);
						session.setAttribute("autoGenKey", autoGenKey);
					} else if (serviceResponse.equals(ApplicationConstants.EMPTY_LIST)) {
						request.setAttribute("locationuploadMsg", "The file which you are trying to upload is empty, Please fill the data and try again.");
						request.setAttribute("locationuploadFile", "font-weight:bold;color:red;");
					}
				} else {
					request.setAttribute("locationuploadMsg", "Maximum size of uploaded file should be less than 1MB");
					request.setAttribute("locationuploadFile", "font-weight:bold;color:red;");
				}
			} catch (ScanSeeServiceException e) {
				LOG.info("Inside LocationSetupController : batchUpdateLocationSetup : " + e.getMessage());
				request.setAttribute("locationuploadMsg", "Error occurred while uploading locations");
				request.setAttribute("locationuploadFile", "font-weight:bold;color:red;");
			}
		}
		/*
		 * End - To avoid On F5 key pressed, ctrl + r, right click select
		 * reload, Location upload will happen repeatedly.
		 */

		session.setAttribute("minCropWd", 28);
		session.setAttribute("minCropHt", 28);
		if (FROMDASHBOARD.equals(dashboardFlag)) {
			session.setAttribute("imageCropPage", "UploadLocationDash");
			return "updateLocationDash";
		}
		session.setAttribute("imageCropPage", "Locationsetup");
		return VIEW_NAME;
	}

	/**
	 * The controller method will update retailer multiple locations by call
	 * service and DAO methods.
	 * 
	 * @param retailerLocation
	 *            RetailerLocation instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param map
	 *            ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws FileNotFoundException
	 */
	@RequestMapping(value = "/updatelocation.htm", method = RequestMethod.POST)
	public final ModelAndView updateLocationSetUp(@ModelAttribute(LOCATIONSETUPFORM) RetailerLocation retailerLocation, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map) throws ScanSeeServiceException, FileNotFoundException {
		LOG.info("Inside LocationSetupController : updateLocationSetUp");
		final String dashboardFlag = (String) request.getParameter("dashBoardFlag");
		try {
			String status = null;
			String isDataInserted = null;
			String strLocationContent = null;
			int currentPage = 1;
			String pageNumber = "0";
			int lowerLimit = 0;
			boolean bValue = false;
			String strRejLocation = null;
			List<RetailerLocation> arLocationList = null;
			session.setAttribute("minCropWd", 28);
			session.setAttribute("minCropHt", 28);
			if (null != dashboardFlag && "fromDashBoard".equals(dashboardFlag)) {
				session.setAttribute("imageCropPage", "UploadLocationDash");
			} else {
				session.setAttribute("imageCropPage", "Locationsetup");
			}

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final String strLogoImage = retailerService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
			final RetailerLocation retailerLocationobj = new RetailerLocation();
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final long userId = loginUser.getUserID();
			final String mailId = retailerService.getUserMailId(retailerId, userId);
			final String fileName = retailerLocation.getLocationSetUpFile().getOriginalFilename();
			final String pageFlag = retailerLocation.getPageFlag();
			pageNumber = retailerLocation.getPageNumber();
			retailerLocation.setPhonenumber(null);
			retailerLocation.setContactMobilePhone(null);
			LocationResult locationResult = new LocationResult();
			SearchResultInfo objLocationList = new SearchResultInfo();
			ArrayList<RetailerLocation> arLocationRejectList = new ArrayList<RetailerLocation>();
			final SearchResultInfo objUploadlocation = (SearchResultInfo) session.getAttribute(LOCATIONLIST);
			final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
			if (pageFlag != null && ApplicationConstants.FALSE.equals(pageFlag)) {
				isDataInserted = retailerService.updateLocationSetUp(retailerLocation, userId, retailerId, "temp file", objUploadlocation, session);
				bValue = isDataInserted.startsWith(ApplicationConstants.REJECTED);
				if (bValue) {
					final String[] strArray = isDataInserted.split(":");
					isDataInserted = strArray[0];
					strRejLocation = strArray[1];
				}
				if (!isDataInserted.equalsIgnoreCase(ApplicationConstants.REJECTED)) {
					locationResult = retailerService.moveDataFromstgTOProduction(retailerLocation, userId, retailerId, "temp file");
					status = locationResult.getStatus();
					arLocationRejectList = retailerService.getRejectedRecords(retailerId, userId);
					if (null != arLocationRejectList && !arLocationRejectList.isEmpty()) {
						final String filename = request.getRealPath(retailerId + ".csv");
						Utility.writeDatToCSV(arLocationRejectList, filename);
						strLocationContent = "<Html>Hi ,<br/><br/>" + ApplicationConstants.SUBJECT_MSG_FOR_LOCATION_MAILSEND
								+ "<br/><br/>Regards,<br/>HubCiti Team<br/>" + "<img src= " + strLogoImage + " alt=\"scansee logo\"  border=\"0\"></Html>";
						final boolean strFlag = retailerService.sendMailWithAttachment(filename, mailId, strLocationContent);
						if (strFlag) {
							final File f = new File(filename);
							f.delete();
						}
					}
					/*
					 * List<RetailerLocation> locationList =
					 * Utility.jsonToObjectList(retailerLocation.getProdJson());
					 */

					/*
					 * From removing location reject List within Grid Location
					 * list.
					 */
					/*
					 * if (null != locationList && !locationList.isEmpty() &&
					 * null != arLocationRejectList &&
					 * !arLocationRejectList.isEmpty() ) { for (int i = 0; i <
					 * locationList.size(); i++) { for (int j = 0; j <
					 * arLocationRejectList.size(); j++) { if
					 * (locationList.get(i).getStoreIdentification() ==
					 * arLocationRejectList.get(j).getStoreIdentification()) {
					 * locationList.remove(i); arLocationRejectList.remove(j); }
					 * } } }
					 */

					/* Batch Upload, Grid Images are upload to server. */
					/*
					 * if (null != locationList && !locationList.isEmpty() ) {
					 * for (int i = 0; i < locationList.size(); i++) { if
					 * (locationList.get(i).isUploadImage()) { if
					 * (!"".equals(Utility
					 * .checkNull(locationList.get(i).getGridImgLocationPath
					 * ()))) { final String fileSeparator =
					 * System.getProperty("file.separator"); StringBuilder
					 * mediaPathBuilder =
					 * Utility.getMediaLocationPath(ApplicationConstants
					 * .RETAILER,Integer.valueOf(retailerId)); StringBuilder
					 * mediaTempPathBuilder =
					 * Utility.getTempMediaPath(ApplicationConstants.TEMP);
					 * String retMediaPath = mediaPathBuilder.toString(); String
					 * retTempMediaPath = mediaTempPathBuilder.toString();
					 * InputStream inputStream = new BufferedInputStream(new
					 * FileInputStream(retTempMediaPath + fileSeparator +
					 * locationList.get(i).getGridImgLocationPath())); if (null
					 * != inputStream) { Utility.writeFileData(inputStream,
					 * retMediaPath + fileSeparator +
					 * locationList.get(i).getGridImgLocationPath()); } } } } }
					 */

					/* Delete all the location records from staging table. */
					retailerService.deleteRetailerLocationStageTable(retailerId, userId);

					if (FROMDASHBOARD.equals(dashboardFlag)) {
						return new ModelAndView(new RedirectView(MGLOCATIONDASHBOARD_HTM));
					}
					return new ModelAndView(new RedirectView("/ScanSeeWeb/retailer/retailerchoosePlan.htm"));
				} else {
					currentPage = pageSess.getCurrentPage();
					arLocationList = (List<RetailerLocation>) session.getAttribute("RetlocationList");
					objLocationList.setLocationList(arLocationList);
					// objLocationList.setLocationID(strRejLocation);
					request.setAttribute("rejectedLocations", strRejLocation);
					session.setAttribute(LOCATIONLIST, objLocationList);
					final Pagination objPage = Utility.getPagination(objUploadlocation.getTotalSize(), currentPage, "updatelocationpageinfo.htm");
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					request.setAttribute("uploadLocation", "font-weight:bold;color:red;");
					request.setAttribute("successMSG", ApplicationConstants.UPDATE_LAT_LONG);
					map.put(LOCATIONSETUPFORM, retailerLocationobj);
					if (null != dashboardFlag && "fromDashBoard".equals(dashboardFlag)) {
						return new ModelAndView("updateLocationDash");
					}
					view = VIEW_NAME;
				}
			} else {
				pageNumber = request.getParameter("pageNumber");
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
				isDataInserted = retailerService.updateLocationSetUp(retailerLocation, userId, retailerId, fileName, objUploadlocation, session);
				final SearchResultInfo locResult = retailerService.getRetailerLocationList(retailerId, userId, lowerLimit);
				session.setAttribute(LOCATIONLIST, locResult);
				final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "updatelocation.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				map.put(LOCATIONSETUPFORM, retailerLocationobj);
				/*
				 * contactTypes = retailerService.getAllContactTypes();
				 * request.setAttribute(CONTACTTYPES, contactTypes);
				 */
				view = VIEW_NAME;
			}
			LOG.info("Inside LocationSetupController : updateLocationSetUp : IsData moved from staging table to production table : " + status);
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (FROMDASHBOARD.equals(dashboardFlag)) {
			return new ModelAndView(new RedirectView(MGLOCATIONDASHBOARD_HTM));
		}
		return new ModelAndView(view);

	}

	/**
	 * The controller method ...is to call service and dao methods.
	 * 
	 * @param retailerLocation
	 *            RetailerLocation instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/productManagement.htm", method = RequestMethod.GET)
	public final ModelAndView next(@ModelAttribute(LOCATIONSETUPFORM) RetailerLocation retailerLocation, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : next");
		view = "regprodsetupbatch";
		return new ModelAndView(view);
	}

	/**
	 * The controller method ...is to call service and dao methods.
	 * 
	 * @param retailerLocation
	 *            RetailerLocation instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/backRetailerLogo.htm", method = RequestMethod.POST)
	public final ModelAndView back(@ModelAttribute(LOCATIONSETUPFORM) RetailerLocation retailerLocation, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : back ");
		view = UPLOADLOGO;
		return new ModelAndView(view);
	}

	/**
	 * The controller method will display add location screen by call service
	 * and DAO methods.
	 * 
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/addlocation.htm", method = RequestMethod.GET)
	public final String showAddocationSetupPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : showAddocationSetupPage ");
		final StoreInfoVO storeInfoVO = new StoreInfoVO();
		ArrayList<State> states = null;
		ArrayList<ContactType> contactTypes = null;
		ArrayList<TimeZones> timeZonelst = null;
		String[] dayName = ApplicationConstants.WEEKDAYNAME;
		List<String> weekDay = new ArrayList<String>(Arrays.asList(dayName));
		ArrayList<StoreInfoVO> businessHoursList = new ArrayList<StoreInfoVO>();
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			timeZonelst = retailerService.getAllTimeZones();

			session.setAttribute("retailerTimeZoneslst", timeZonelst);
			session.setAttribute("imageCropPage", "AddLocation");
			session.setAttribute("minCropWd", 70);
			session.setAttribute("minCropHt", 70);

			session.setAttribute("locationImgPath", ApplicationConstants.UPLOAD_IMAGE_PATH);

			states = supplierService.getAllStates();
			session.setAttribute("states", states);
			contactTypes = supplierService.getAllContactTypes();
			session.setAttribute(CONTACTTYPES, contactTypes);

			for (int i = 0; i <= 6; i++) {
				StoreInfoVO businessHours = new StoreInfoVO();
				businessHours.setStoreStartHrs("00");
				businessHours.setStoreStartMins("00");
				businessHours.setStoreEndHrs("00");
				businessHours.setStoreEndMins("00");
				businessHours.setWeekDayName(weekDay.get(i));
				businessHoursList.add(businessHours);

				session.setAttribute("businessHoursList", businessHoursList);
				session.setAttribute("openCloseTime", businessHoursList);
			}

		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("addlocationform", storeInfoVO);
		return ADD_LOCATION;
	}

	/**
	 * The controller method will add one location details by call service and
	 * DAO methods.
	 * 
	 * @param storeInfoVO
	 *            StoreInfoVO instance as request parameter
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/saveLocation.htm", method = RequestMethod.POST)
	public final String saveLocationSetupPage(@ModelAttribute("addlocationform") StoreInfoVO storeInfoVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException {
		return addLocation(storeInfoVO, result, request, model, session, ADD_LOCATION);
	}

	/**
	 * The controller method will add one location details by call service and
	 * DAO methods.
	 * 
	 * @param storeInfoVO
	 *            StoreInfoVO instance as request parameter
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param viewName
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public final String addLocation(StoreInfoVO storeInfoVO, BindingResult result, HttpServletRequest request, ModelMap model, HttpSession session,
			String viewName) throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : addLocation");
		String state = null;
		String city = null;
		boolean isValidContactNum = true;
		boolean isValidURL = true;
		String startTime = null;
		String endTime = null;
		String startTimeUTC = null;
		String endTimeUTC = null;
		List<String> startHrs = new ArrayList<String>();
		List<String> startMins = new ArrayList<String>();
		List<String> endHrs = new ArrayList<String>();
		List<String> endMins = new ArrayList<String>();
		List<String> weekDay = new ArrayList<String>();
		
		ArrayList<StoreInfoVO> businessHoursList = new ArrayList<StoreInfoVO>();

		boolean isValidatePostalCode = true;
		String isProfileCreated = null;
		state = storeInfoVO.getState();
		final String showQRCode = "showqrcode";
		city = storeInfoVO.getCity();

		final String dashboardFlag = (String) request.getParameter("dashBoardFlag");
		session.setAttribute("minCropWd", 70);
		session.setAttribute("minCropHt", 70);

		session.removeAttribute("pageTitle");
		session.setAttribute("pageTitle", storeInfoVO.getStoreID());
		if (FROMDASHBOARD.equals(dashboardFlag)) {
			session.setAttribute("imageCropPage", "AddLocationDash");
		} else {
			session.setAttribute("imageCropPage", "AddLocation");
		}

		final Users loginUser = (Users) session.getAttribute(ApplicationConstants.LOGINUSER);
		final Integer userId = loginUser.getRetailerId();
		storeInfoVO.setRetailID(userId);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		try {
			isValidatePostalCode = Utility.isNumber(storeInfoVO.getPostalCode());
			isValidContactNum = Utility.validatePhoneNum(storeInfoVO.getPhoneNumber());
			isValidURL = Utility.validateURL(storeInfoVO.getRetailUrl());

			locationSetUpValidator.validate(storeInfoVO, result);

			if (!isValidContactNum) {
				locationSetUpValidator.validate(storeInfoVO, result, ApplicationConstants.INVALIDCONTPHONE);
			}

			if (!isValidatePostalCode) {
				locationSetUpValidator.validate(storeInfoVO, result, ApplicationConstants.INVALIDPOSTALCODE);
			}
			if (!isValidURL) {
				locationSetUpValidator.validate(storeInfoVO, result, ApplicationConstants.INVALIDURL);
			}

			if (!"".equals(Utility.checkNull(storeInfoVO.getGeoError()))) {
				if (null == storeInfoVO.getRetailerLocationLatitude()) {
					locationSetUpValidator.validate(storeInfoVO, result, ApplicationConstants.LATITUDE);
				}
				if (null == storeInfoVO.getRetailerLocationLongitude()) {
					locationSetUpValidator.validate(storeInfoVO, result, ApplicationConstants.LONGITUDE);
				}

			}
			if (null != storeInfoVO.getStoreStartHrs() && null != storeInfoVO.getStoreStartMins() && null != storeInfoVO.getStoreEndHrs()
					&& null != storeInfoVO.getStoreEndMins() && null != storeInfoVO.getTimeZoneHidden()) {

				startHrs = new ArrayList<String>(Arrays.asList(storeInfoVO.getStoreStartHrs().split(",")));
				startMins = new ArrayList<String>(Arrays.asList(storeInfoVO.getStoreStartMins().split(",")));
				endHrs = new ArrayList<String>(Arrays.asList(storeInfoVO.getStoreEndHrs().split(",")));
				endMins = new ArrayList<String>(Arrays.asList(storeInfoVO.getStoreEndMins().split(",")));
				weekDay = new ArrayList<String>(Arrays.asList(ApplicationConstants.WEEKDAYNAME));

				for (int i = 0; i < startHrs.size(); i++) {
					StoreInfoVO businessHours = new StoreInfoVO();
					businessHours.setStoreStartHrs(startHrs.get(i));
					businessHours.setStoreStartMins(startMins.get(i));
					businessHours.setStoreEndHrs(endHrs.get(i));
					businessHours.setStoreEndMins(endMins.get(i));
					businessHours.setWeekDayName(weekDay.get(i));
					businessHoursList.add(businessHours);

					session.setAttribute("businessHoursList", businessHoursList);
				
					String sTime = startHrs.get(i) + ":" + startMins.get(i);
					String eTime = endHrs.get(i) + ":" + endMins.get(i);
					String TimeZoneId = storeInfoVO.getTimeZoneHidden();

					if (null != startTime) {
						startTime += sTime + ",";
					} else {
						startTime = sTime + ",";
					}
					if (null != endTime) {
						endTime += eTime + ",";
					} else {
						endTime = eTime + ",";
					}
					if (!"00:00".equals(sTime)) {
						String sTimeUTC = Utility.convertLocalTimeToUTC(TimeZoneId, sTime);
						if (null != startTimeUTC) {
							startTimeUTC += sTimeUTC + ",";
						} else {
							startTimeUTC = sTimeUTC + ",";
						}
					} else {
						if (null != startTimeUTC) {
							startTimeUTC += sTime + ",";
						} else {
							startTimeUTC = sTime + ",";
						}
					}
					if (!"00:00".equals(eTime)) {
						String eTimeUTC = Utility.convertLocalTimeToUTC(TimeZoneId, eTime);
						if (null != endTimeUTC) {
							endTimeUTC += eTimeUTC + ",";
						} else {
							endTimeUTC = eTimeUTC + ",";
						}
					} else {
						if (null != endTimeUTC) {
							endTimeUTC += eTime + ",";
						} else {
							endTimeUTC = eTime + ",";
						}
					}
				}
				if (null != startTime && startTime.endsWith(",")) {
					startTime = startTime.substring(0, startTime.length() - 1);
					startTimeUTC = startTimeUTC.substring(0, startTimeUTC.length() - 1);
					storeInfoVO.setStartTime(startTime);
					storeInfoVO.setStartTimeUTC(startTimeUTC);
				}
				if (null != endTime && endTime.endsWith(",")) {
					endTime = endTime.substring(0, endTime.length() - 1);
					endTimeUTC = endTimeUTC.substring(0, endTimeUTC.length() - 1);
					storeInfoVO.setEndTime(endTime);
					storeInfoVO.setEndTimeUTC(endTimeUTC);
				}
			}

			if (result.hasErrors()) {
				if (FROMDASHBOARD.equals(dashboardFlag)) {
					view = "addLocationDash";
				} else {
					view = ADD_LOCATION;
				}
				return view;
			} else {
				isProfileCreated = retailerService.addLocation(storeInfoVO, loginUser);

				storeInfoVO.setGeoError(null);
				request.removeAttribute("GEOERROR");
				if (isProfileCreated.equals(ApplicationConstants.DUPLICATE_STORE) || "GEOERROR".equals(isProfileCreated)) {
					if (isProfileCreated.equals(ApplicationConstants.DUPLICATE_STORE)) {
						locationSetUpValidator.validate(storeInfoVO, result, ApplicationConstants.DUPLICATE_STORE);
					} else {
						locationSetUpValidator.validate(storeInfoVO, result, ApplicationConstants.GEOERROR);
						request.setAttribute("GEOERROR", ApplicationConstants.GEOERROR);
						storeInfoVO.setRetailerLocationLatitude(null);
						storeInfoVO.setRetailerLocationLongitude(null);
					}

					if (result.hasErrors()) {
						if (FROMDASHBOARD.equals(dashboardFlag)) {
							view = "addLocationDash";
						} else {
							view = ADD_LOCATION;
						}
					}
				} else if (isProfileCreated != null && !isProfileCreated.equals(ApplicationConstants.FAILURE)
						&& !isProfileCreated.equals(ApplicationConstants.DUPLICATE_STORE)) {

					/*
					 * // Update google location coordinates for given address
					 * retailerService .addLocationCoordinatesWithRetailID
					 * (locationCoordinates, storeInfoVO.getRetailID(),
					 * storeInfoVO.getStoreID());
					 */

					if (FROMDASHBOARD.equals(dashboardFlag)) {
						view = "addLocationDash";
					} else {
						view = ADD_LOCATION;
					}
					
					session.removeAttribute("businessHoursList");
					session.setAttribute("businessHoursList", session.getAttribute("openCloseTime"));
					session.removeAttribute(LOCATIONLIST);
					session.removeAttribute(ApplicationConstants.PAGINATION);
					session.removeAttribute("supplierCity");
					session.removeAttribute("locationImgPath");
					StoreInfoVO objStoreInfoVO = new StoreInfoVO();
					model.put("addlocationform", objStoreInfoVO);
					request.setAttribute("locationuploadMsg", "Location Saved Successfully");
					request.setAttribute("message", "true");
					request.setAttribute("locationSetUpFileFont", "font-weight:bold;color:#00559c;");
					if (FROMDASHBOARD.equals(dashboardFlag)) {
						session.setAttribute("qrCodeImageTitle", storeInfoVO.getStoreID());
						session.setAttribute("qrCodeImagePath", isProfileCreated);
						request.setAttribute("splOffer", "addLocation");
						LOG.info("Retailer Registration Successful... ");
						return showQRCode;
					}
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		} catch (ScanSeeWebSqlException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			e.printStackTrace();
		}
		return view;
	}

	/**
	 * The controller method will display all retailer locations/fetch the
	 * retailer location based on input parameter by call service and DAO
	 * methods.
	 * 
	 * @param retailerLocation
	 *            RetailerLocation instance as request parameter
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws IOException
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/fetchbatchlocationlist.htm", method = RequestMethod.POST)
	public final ModelAndView fetchBatchLocationSetup(@ModelAttribute("locationform") RetailerLocation retailerLocation, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws IOException, ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : fetchBatchLocationSetup");
		int lowerLimit = 0;
		String pageNumber = "0";
		int currentPage = 1;
		String storeIdentification = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		ArrayList<ContactType> contactTypes = null;
		final String pageFlag = retailerLocation.getPageFlag();
		pageNumber = retailerLocation.getPageNumber();
		final String dashboardFlag = (String) request.getParameter("dashBoardFlag");
		session.setAttribute("minCropWd", 28);
		session.setAttribute("minCropHt", 28);
		try {
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final Long userId = loginUser.getUserID();
			storeIdentification = retailerLocation.getSearchKey();
			if (null != storeIdentification && "".equals(storeIdentification)) {
				storeIdentification = null;
			}
			session.removeAttribute(ApplicationConstants.PAGINATION);
			if (pageFlag != null && pageFlag.equals(ApplicationConstants.FALSE)) {
				final SearchResultInfo locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, storeIdentification);
				if (locResult.getLocationList() == null || locResult.getLocationList().isEmpty()) {
					final String message = "No Retail Store matching for the Given String";
					request.setAttribute("errorMSG", message);
				}
				contactTypes = retailerService.getAllContactTypes();
				request.setAttribute(CONTACTTYPES, contactTypes);
				request.setAttribute("locResult", locResult);
				final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/fetchbatchlocationlist.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (!"".equals(pageNumber)) {
					if (Integer.valueOf(pageNumber) != 0) {
						currentPage = Integer.valueOf(pageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						final int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					}
				} else {
					lowerLimit = 0;
				}
				final SearchResultInfo locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, storeIdentification);
				if (locResult.getLocationList() == null || locResult.getLocationList().isEmpty()) {
					final String message = "No Retail Store matching for the Given String";
					request.setAttribute("errorMSG", message);
				}
				contactTypes = retailerService.getAllContactTypes();
				request.setAttribute(CONTACTTYPES, contactTypes);
				request.setAttribute("locResult", locResult);
				final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/savelocationlist.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				model.put("locationform", new RetailerLocation());
				// status =
				// retailerService.moveDataFromstgTOProduction(retailerLocation,
				// userId, retailerId, fileName);
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (FROMDASHBOARD.equals(dashboardFlag)) {
			session.setAttribute("imageCropPage", "ManageLocationDash");
			return new ModelAndView("manageLocationDash");
		}
		session.setAttribute("imageCropPage", "ManageLocation");
		return new ModelAndView("fetchbatchlocationlist");
	}

	/**
	 * The controller method will update retailer multiple locations by call
	 * service and DAO methods.
	 * 
	 * @param retailerLocation
	 *            RetailerLocation instance as request parameter
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws FileNotFoundException
	 */
	@RequestMapping(value = "/savelocationlist.htm", method = RequestMethod.POST)
	public final ModelAndView saveLocationList(@ModelAttribute("locationform") RetailerLocation retailerLocation, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException, FileNotFoundException {
		LOG.info("Inside LocationSetupController : saveLocationList");
		final String dashboardFlag = (String) request.getParameter("dashBoardFlag");
		final String returnView = null;
		boolean bValue = false;
		String strRejLocation = null;
		try {
			String isDataInserted = null;
			ArrayList<ContactType> contactTypes = null;

			int currentPage = 1;
			String pageNumber = "0";
			int lowerLimit = 0;
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final RetailerLocation location = new RetailerLocation();
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final long userId = loginUser.getUserID();
			final SearchResultInfo objBeforelocation = (SearchResultInfo) session.getAttribute("beforelocationResult");
			final String pageFlag = retailerLocation.getPageFlag();
			List<RetailerLocation> arLocationList = null;
			List<RetailerLocation> locationList = null;
			pageNumber = retailerLocation.getPageNumber();
			SearchResultInfo locResult = new SearchResultInfo();
			Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
			locationList = Utility.jsonToObjectList(retailerLocation.getProdJson());

			/*
			 * location.setPageNumber(retailerLocation.getPageNumber());
			 * location.setPageFlag(retailerLocation.getPageFlag());
			 */
			if (pageFlag != null && pageFlag.equals(ApplicationConstants.FALSE)) {
				isDataInserted = retailerService.saveLocationSetUpList(retailerLocation, userId, retailerId, objBeforelocation, session);
				bValue = isDataInserted.startsWith(ApplicationConstants.REJECTED);
				if (bValue) {
					final String[] strArray = isDataInserted.split(":");
					isDataInserted = strArray[0];
					strRejLocation = strArray[1];
				}
				if (isDataInserted.equalsIgnoreCase(ApplicationConstants.SUCCESS) || isDataInserted.equalsIgnoreCase(ApplicationConstants.REJECTED)) {
					if (isDataInserted.equalsIgnoreCase(ApplicationConstants.REJECTED)) {
						arLocationList = (List<RetailerLocation>) session.getAttribute("RetlocationList");
						locResult.setLocationList(arLocationList);
						// locResult.setLocationID(strRejLocation);
						request.setAttribute("rejectedLocations", strRejLocation);
						/*
						 * locResult.setTotalSize(retailerLocation.getTotalSize()
						 * ); currentPage = retailerLocation.getCurrentPage();
						 */
						locResult.setTotalSize(objBeforelocation.getTotalSize());
						currentPage = pageSess.getCurrentPage();
						request.setAttribute("successMSG", ApplicationConstants.UPDATE_LAT_LONG);
						request.setAttribute("manageLocation", "font-weight:bold;color:red;");
						// session.setAttribute("locResult", locResult);
					} else {
						locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, null);
						if (isDataInserted.equalsIgnoreCase(ApplicationConstants.SUCCESS)) {
							request.setAttribute("successMSG", "Location Info is saved successfully.");
							request.setAttribute("manageLocation", "font-weight:bold;color:#00559c;");
							request.setAttribute("rejectedLocations", null);
						}
					}
					contactTypes = retailerService.getAllContactTypes();
					request.setAttribute(CONTACTTYPES, contactTypes);
					request.setAttribute("locResult", locResult);
					session.setAttribute("beforelocationResult", locResult);

					final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "savelocationlist.htm");
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					model.put("locationform", new RetailerLocation());
					if (FROMDASHBOARD.equals(dashboardFlag)) {
						// return new ModelAndView(new
						// RedirectView(MANAGELOCATIONDASHBOARD_HTM));
						return new ModelAndView("manageLocationDash");
					}
					// return new ModelAndView(new
					// RedirectView("/ScanSeeWeb/retailer/fetchbatchlocationlist.htm"));
					return new ModelAndView("fetchbatchlocationlist");
				}
			} else {
				pageNumber = request.getParameter("pageNumber");
				// final Pagination pageSess = (Pagination)
				// session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
				isDataInserted = retailerService.saveLocationSetUpList(retailerLocation, userId, retailerId, objBeforelocation, session);
				bValue = isDataInserted.startsWith(ApplicationConstants.REJECTED);
				if (bValue) {
					final String[] strArray = isDataInserted.split(":");
					isDataInserted = strArray[0];
					strRejLocation = strArray[1];
				}
				if (isDataInserted.equalsIgnoreCase(ApplicationConstants.SUCCESS) || isDataInserted.equalsIgnoreCase(ApplicationConstants.REJECTED)) {

					if (isDataInserted.equalsIgnoreCase(ApplicationConstants.REJECTED)) {
						arLocationList = (List<RetailerLocation>) session.getAttribute("RetlocationList");
						locResult.setLocationList(arLocationList);
						// locResult.setLocationID(strRejLocation);
						request.setAttribute("rejectedLocations", strRejLocation);
						currentPage = pageSess.getCurrentPage();
						locResult.setTotalSize(objBeforelocation.getTotalSize());
						request.setAttribute("successMSG", ApplicationConstants.UPDATE_LAT_LONG);
						request.setAttribute("manageLocation", "font-weight:bold;color:red;");
					} else {
						locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, null);
						if (isDataInserted.equalsIgnoreCase(ApplicationConstants.SUCCESS)) {
							request.setAttribute("successMSG", "Location Info is saved successfully.");
							request.setAttribute("manageLocation", "font-weight:bold;color:#00559c;");
						}
					}

					contactTypes = retailerService.getAllContactTypes();
					request.setAttribute(CONTACTTYPES, contactTypes);
					request.setAttribute("locResult", locResult);
					session.setAttribute("beforelocationResult", locResult);
					// location.setTotalSize(locResult.getTotalSize());
					final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "savelocationlist.htm");
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					model.put("locationform", location);
					if (FROMDASHBOARD.equals(dashboardFlag)) {
						return new ModelAndView("manageLocationDash");
					}
					return new ModelAndView("fetchbatchlocationlist");
				}
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (FROMDASHBOARD.equals(dashboardFlag)) {
			return new ModelAndView(new RedirectView(MGLOCATIONDASHBOARD_HTM));
		}
		return new ModelAndView(returnView);
	}

	/**
	 * The controller method will delete one retailer location from manage
	 * Location grid by call service and DAO methods.
	 * 
	 * @param retailerLocation
	 *            RetailerLocation instance as request parameter
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 * @throws IOException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/deletebatchlocationlist.htm", method = RequestMethod.POST)
	public final ModelAndView deleteBatchLocationSetup(@ModelAttribute("locationform") RetailerLocation retailerLocation, BindingResult result,
			HttpServletRequest request, HttpSession session) throws IOException, ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : deleteBatchLocationSetup");
		String serviceResponse = null;
		final int lowerLimit = 0;
		final int currentPage = 1;
		String retailLocationId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		try {
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final Long userId = loginUser.getUserID();
			retailLocationId = retailerLocation.getCheckboxDel();
			serviceResponse = retailerService.deleteBatchLocation(retailLocationId);
			if (serviceResponse.equalsIgnoreCase(ApplicationConstants.SUCCESS)) {
				final SearchResultInfo locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, null);
				if (locResult.getLocationList() == null || locResult.getLocationList().isEmpty()) {
					final String message = "No Retail Store matching for the Given String";
					request.setAttribute("errorMSG", message);
				}
				request.setAttribute("locResult", locResult);
				final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "fetchbatchlocationlist.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView("fetchbatchlocationlist");
	}

	/**
	 * The controller method will display all retailer locations/fetch the
	 * retailer location based on input parameter by call service and DAO
	 * methods.
	 * 
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws IOException
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/fetchbatchlocationlist.htm", method = RequestMethod.GET)
	public final String fetchBatchLocation(HttpServletRequest request, ModelMap model, HttpSession session) throws IOException, ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : fetchBatchLocation");
		String pageNumber = "0";
		try {
			int currentPage = 1;
			int lowerLimit = 0;
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final Long userId = loginUser.getUserID();
			final ServletContext servletContext = request.getSession().getServletContext();
			ArrayList<ContactType> contactTypes = null;
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final RetailerLocation retailerLocation = new RetailerLocation();
			session.setAttribute("imageCropPage", "ManageLocation");
			session.setAttribute("minCropWd", 28);
			session.setAttribute("minCropHt", 28);
			session.removeAttribute("locResult");
			session.removeAttribute(ApplicationConstants.PAGINATION);
			model.put("locationform", retailerLocation);
			final String pageFlag = retailerLocation.getPageFlag();
			pageNumber = retailerLocation.getPageNumber();

			if (pageFlag != null && pageFlag.equals(ApplicationConstants.FALSE)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
				final SearchResultInfo locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, null);
				contactTypes = retailerService.getAllContactTypes();
				request.setAttribute(CONTACTTYPES, contactTypes);
				request.setAttribute("locResult", locResult);
				session.setAttribute("beforelocationResult", locResult);
				final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/fetchbatchlocationlist.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			} else {
				final SearchResultInfo locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, null);
				contactTypes = retailerService.getAllContactTypes();
				request.setAttribute(CONTACTTYPES, contactTypes);
				request.setAttribute("locResult", locResult);
				session.setAttribute("beforelocationResult", locResult);
				final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "/ScanSeeWeb/retailer/fetchbatchlocationlist.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return "fetchbatchlocationlist";
	}

	/**
	 * This controller method will display the location setup screen.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/showbatchuploadprod.htm", method = RequestMethod.GET)
	public final String showbatchUploadPage(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : showbatchUploadPage");
		final RetailProduct retailProduct = new RetailProduct();
		model.put("batchUploadretprodform", retailProduct);
		view = "regprodsetupbatch";
		return view;
	}

	// Code added for ManageLocation - Dashbard - Meyy

	/**
	 * This controller method will display the manage location screen.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/manageLocationDashboard.htm", method = RequestMethod.GET)
	public final ModelAndView showManageLocationDashboardPage(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : showManageLocationDashboardPage ");
		String pageNumber = "0";
		String viewName = "";
		try {
			int currentPage = 1;
			int lowerLimit = 0;
			ArrayList<TimeZones> timeZonelst = null;
			session.setAttribute("minCropWd", 28);
			session.setAttribute("minCropHt", 28);
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final Long userId = loginUser.getUserID();
			final ServletContext servletContext = request.getSession().getServletContext();
			ArrayList<ContactType> contactTypes = null;
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			final RetailerLocation retailerLocation = new RetailerLocation();
			request.removeAttribute("locResult");
			session.removeAttribute(ApplicationConstants.PAGINATION);
			model.put("locationform", retailerLocation);
			/* ManageLocation - Dashboard pagination */
			final String pageFlag = retailerLocation.getPageFlag();
			pageNumber = retailerLocation.getPageNumber();
			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("ManageLocations", "ManageLocations", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);
			if (pageFlag != null && pageFlag.equals(ApplicationConstants.FALSE)) {
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
				if (Integer.valueOf(pageNumber) != 0) {
					currentPage = Integer.valueOf(pageNumber);
					final int number = Integer.valueOf(currentPage) - 1;
					final int pageSize = pageSess.getPageRange();
					lowerLimit = pageSize * Integer.valueOf(number);
				} else {
					lowerLimit = 0;
				}
				final SearchResultInfo locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, null);
				if (null != locResult && !locResult.getLocationList().isEmpty()) {

					contactTypes = retailerService.getAllContactTypes();
					request.setAttribute(CONTACTTYPES, contactTypes);
					request.setAttribute("locResult", locResult);
					session.setAttribute("beforelocationResult", locResult);
					final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, MGLOCATIONDASHBOARD_HTM);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					viewName = "manageLocationDash";
					session.setAttribute("imageCropPage", "ManageLocationDash");
				} else {
					viewName = "locationSetupMainPage";
					leftNav = UtilCode.setRetailerModulesStyle("ManageLocations", null, leftNav);
					session.setAttribute("retlrLeftNav", leftNav);
					session.setAttribute("imageCropPage", "ManageLocation");
				}
			} else {
				final SearchResultInfo locResult = retailerService.getRetailerBatchLocationList(retailerId, userId, lowerLimit, null);
				if (null != locResult && !locResult.getLocationList().isEmpty()) {
					contactTypes = retailerService.getAllContactTypes();
					request.setAttribute(CONTACTTYPES, contactTypes);
					request.setAttribute("locResult", locResult);
					session.setAttribute("beforelocationResult", locResult);
					timeZonelst = retailerService.getAllTimeZones();
					session.setAttribute("retailerTimeZoneslst", timeZonelst);
					final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, MGLOCATIONDASHBOARD_HTM);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);
					viewName = "manageLocationDash";
					session.setAttribute("imageCropPage", "ManageLocationDash");
				} else {
					session.setAttribute("imageCropPage", "ManageLocation");
					viewName = "locationSetupMainPage";
					leftNav = UtilCode.setRetailerModulesStyle("ManageLocations", null, leftNav);
					session.setAttribute("retlrLeftNav", leftNav);
				}
			}
			/* End of ManageLocation - Dashboard pagination */
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return new ModelAndView(viewName);
	}

	/**
	 * The controller method will add one retailer location details in dash
	 * board by call service and DAO methods.
	 * 
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/addlocation_dashboard.htm", method = RequestMethod.GET)
	public final ModelAndView showAddLocationSetupDashboard(HttpServletRequest request, ModelMap model, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : showAddLocationSetupDashboard ");
		final StoreInfoVO storeInfoVO = new StoreInfoVO();
		ArrayList<State> states = null;
		ArrayList<ContactType> contactTypes = null;
		ArrayList<TimeZones> timeZonelst = null;
		String[] dayName = ApplicationConstants.WEEKDAYNAME;
		List<String> weekDay = new ArrayList<String>(Arrays.asList(dayName));
		ArrayList<StoreInfoVO> businessHoursList = new ArrayList<StoreInfoVO>();
		try {
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			timeZonelst = retailerService.getAllTimeZones();
			session.setAttribute("retailerTimeZoneslst", timeZonelst);

			states = supplierService.getAllStates();
			session.setAttribute("states", states);

			contactTypes = supplierService.getAllContactTypes();
			session.setAttribute(CONTACTTYPES, contactTypes);

			session.setAttribute("imageCropPage", "AddLocationDash");
			session.setAttribute("minCropWd", 70);
			session.setAttribute("minCropHt", 70);

			session.setAttribute("locationImgPath", ApplicationConstants.UPLOAD_IMAGE_PATH);

			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("ManageLocations", ADD_LOCATION, leftNav);
			session.setAttribute("retlrLeftNav", leftNav);
			
			for (int i = 0; i <= 6; i++) {
				StoreInfoVO businessHours = new StoreInfoVO();
				businessHours.setStoreStartHrs("00");
				businessHours.setStoreStartMins("00");
				businessHours.setStoreEndHrs("00");
				businessHours.setStoreEndMins("00");
				businessHours.setWeekDayName(weekDay.get(i));
				businessHoursList.add(businessHours);

				session.setAttribute("businessHoursList", businessHoursList);
				session.setAttribute("openCloseTime", businessHoursList);
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		model.put("addlocationform", storeInfoVO);
		return new ModelAndView("addLocationDash");
	}

	/**
	 * The controller method will display location setup screen in dash board.
	 * 
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/location_dashboardsetup.htm", method = RequestMethod.GET)
	public final ModelAndView locationSetupDashboard(HttpServletRequest request, ModelMap model, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : locationSetupDashboard ");
		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");

		session.setAttribute("imageCropPage", "UploadLocationDash");
		session.setAttribute("minCropWd", 28);
		session.setAttribute("minCropHt", 28);

		leftNav = UtilCode.setRetailerModulesStyle("ManageLocations", "UploadLocations", leftNav);
		session.setAttribute("retlrLeftNav", leftNav);

		setupUpdateLocationPage(request, model, session);
		return new ModelAndView("updateLocationDash");
	}

	/**
	 * The controller method will add one location details in dash board by call
	 * service and DAO methods.
	 * 
	 * @param storeInfoVO
	 *            StoreInfoVO instance as request parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param model
	 *            ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/saveLocationDashboard.htm", method = RequestMethod.POST)
	public final ModelAndView saveLocationSetupDashboardPage(@ModelAttribute("addlocationform") StoreInfoVO storeInfoVO, BindingResult result,
			HttpServletRequest request, ModelMap model, HttpSession session) throws ScanSeeServiceException {
		final String view = addLocation(storeInfoVO, result, request, model, session, "addLocationDash");
		return new ModelAndView(view);
	}

	/**
	 * The controller method will get Quick response code(QR code)for retailer
	 * Location based on input parameter by call service and DAO methods.
	 * 
	 * @param customPageForm
	 *            instance of RetailerCustomPage.
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/getlocqr", method = RequestMethod.GET)
	public final ModelAndView showQrCodeForLocation(@ModelAttribute("customPageForm") RetailerCustomPage customPageForm, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : showQrCodeForLocation ");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		final String locationId = (String) request.getParameter("locId");
		try {
			final RetailerCustomPage pageDetails = retailerService.getLocQrCodeDetails(Long.valueOf(locationId), loginUser.getRetailerId(),
					loginUser.getUserID());
			request.setAttribute("pageDetails", pageDetails);
		} catch (ScanSeeServiceException exception) {
			LOG.error(exception.getMessage(), exception);
			throw exception;
		}
		return new ModelAndView("showlocqrcode");
	}

	/**
	 * This controller method will display the manage location screen.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param response
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/manageLocation.htm", method = RequestMethod.GET)
	public final ModelAndView showManageLocationSetUpPage(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : showManageLocationSetUpPage ");
		RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
		leftNav = UtilCode.setRetailerModulesStyle("ManageLocations", null, leftNav);
		session.setAttribute("retlrLeftNav", leftNav);
		return new ModelAndView("locationSetupMainPage");
	}

	/**
	 * The controller method will update retailer multiple locations by call
	 * service and DAO methods.
	 * 
	 * @param retailerLocation
	 *            RetailerLocation instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @param map
	 *            ModelMap instance as parameter.
	 * @return ModelAndView given a view name.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/updatelocationpageinfo.htm", method = RequestMethod.POST)
	public final ModelAndView updateLocationInfoInStageTable(@ModelAttribute(LOCATIONSETUPFORM) RetailerLocation retailerLocation, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap map) throws ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : updateLocationSetUp");
		final String dashboardFlag = (String) request.getParameter("dashBoardFlag");
		try {
			session.setAttribute("imageCropPage", "UploadLocationDash");
			session.setAttribute("minCropWd", 28);
			session.setAttribute("minCropHt", 28);
			String status = null;
			String isDataInserted = null;
			// ArrayList<ContactType> contactTypes = null;
			int currentPage = 1;
			String pageNumber = "0";
			int lowerLimit = 0;
			boolean bValue = false;
			String strRejLocation = null;
			List<RetailerLocation> arLocationList = null;

			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

			final RetailerLocation retailerLocationobj = new RetailerLocation();
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final long userId = loginUser.getUserID();

			final String pageFlag = retailerLocation.getPageFlag();
			pageNumber = retailerLocation.getPageNumber();
			retailerLocation.setPhonenumber(null);
			retailerLocation.setContactMobilePhone(null);
			final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);
			SearchResultInfo locResult = new SearchResultInfo();
			locResult = (SearchResultInfo) session.getAttribute(LOCATIONLIST);
			isDataInserted = retailerService.updateLocationSetUp(retailerLocation, userId, retailerId, "temp file", locResult, session);

			bValue = isDataInserted.startsWith(ApplicationConstants.REJECTED);
			if (bValue) {
				final String[] strArray = isDataInserted.split(":");
				isDataInserted = strArray[0];
				strRejLocation = strArray[1];
			}
			if (isDataInserted.equalsIgnoreCase(ApplicationConstants.REJECTED)) {
				arLocationList = (List<RetailerLocation>) session.getAttribute("RetlocationList");
				locResult.setLocationList(arLocationList);
				// locResult.setLocationID(strRejLocation);
				request.setAttribute("rejectedLocations", strRejLocation);
				session.setAttribute(LOCATIONLIST, locResult);
				currentPage = pageSess.getCurrentPage();
				request.setAttribute("uploadLocation", "font-weight:bold;color:red;");
				request.setAttribute("successMSG", ApplicationConstants.UPDATE_LAT_LONG);
			} else {
				if (pageFlag != null && "true".equals(pageFlag)) {
					pageNumber = request.getParameter("pageNumber");
					if (Integer.valueOf(pageNumber) != 0) {
						currentPage = Integer.valueOf(pageNumber);
						final int number = Integer.valueOf(currentPage) - 1;
						final int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					} else {
						lowerLimit = 0;
					}
					locResult = retailerService.getRetailerLocationList(retailerId, userId, lowerLimit);
					session.setAttribute(LOCATIONLIST, locResult);
					request.setAttribute("uploadLocation", "font-weight:bold;color:#00559c;");
					request.setAttribute("successMSG", "Location Saved Successfully");
				}
				final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "updatelocationpageinfo.htm");
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
			map.put(LOCATIONSETUPFORM, retailerLocationobj);
			LOG.info("Inside LocationSetupController : updateLocationSetUp : IsData moved from staging table to production table : " + status);
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		if (null != dashboardFlag && "fromDashBoard".equals(dashboardFlag)) {
			return new ModelAndView("updateLocationDash");
		} else {
			return new ModelAndView(VIEW_NAME);
		}
	}

	/**
	 * This controller method will upload retailer logo image in add location.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param objStoreInfoVO
	 *            instance of StoreInfoVO.
	 * @param response
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws Exception
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/addlocationimg.htm", method = RequestMethod.POST)
	public final String addLocationImage(@ModelAttribute("addlocationform") StoreInfoVO objStoreInfoVO, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception, ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : addLocationImage ");

		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		session.removeAttribute("cropImageSource");

		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, objStoreInfoVO.getImageFile()
				.getInputStream());

		if (imageSizeValFlg) {
			session.setAttribute("locationImgPath", ApplicationConstants.UPLOADIMAGEPATH);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
		} else {
			final BufferedImage img = Utility.getBufferedImageForMinDimension(objStoreInfoVO.getImageFile().getInputStream(), request.getRealPath("images"),
					objStoreInfoVO.getImageFile().getOriginalFilename(), "Location");

			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();

			imageSource = objStoreInfoVO.getImageFile().getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();

			final String filePath = tempImgPath + fileSeparator + objStoreInfoVO.getImageFile().getOriginalFilename();

			Utility.writeImage(img, filePath);

			if (imageValidSizeValFlg) {
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objStoreInfoVO.getImageFile().getOriginalFilename());
				session.setAttribute("locationImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			}

			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
		}
		return null;
	}

	/**
	 * This controller method will upload retailer logo image in upload location
	 * screen.
	 * 
	 * @param objStoreInfoVO
	 * @param result
	 * @param request
	 * @param response
	 * @param session
	 * @return ModelAndView
	 * @throws ScanSeeServiceException
	 * @throws IOException
	 */
	@RequestMapping(value = "/uploadlocationimages.htm", method = RequestMethod.POST)
	public ModelAndView uploadLocationImage(@ModelAttribute("locationsetupform") RetailerLocation objRetLocation, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException, IOException {
		LOG.info("Inside LocationSetupController : uploadLocationImage ");
		String strViewName = null;

		if (objRetLocation.getViewName().equals(VIEW_NAME)) {
			strViewName = VIEW_NAME;
		} else {
			strViewName = "updateLocationDash";
		}

		String fileName = null;
		String fileExtn = null;
		ZipEntry zipentry = null;
		ZipInputStream zipInputStream = null;
		int dotIndex;
		FileOutputStream fileoutputstream;
		long twoFiveMBinBytes = 26214400L;
		String zipFilepath = null;
		String entryName = null;
		String path = null;
		String extension = null;
		ArrayList<String> rejectedFileList = new ArrayList<String>();
		int n;
		byte[] buf = new byte[1024];
		boolean bErrorMsg = false;

		final int currentPage = 1;
		final int lowerLimit = 0;
		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		final int retailerId = loginUser.getRetailerId();
		final Long userId = loginUser.getUserID();

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);

		try {
			StringBuilder serverPathBuild = Utility.getMediaLocationPath(ApplicationConstants.RETAILER, loginUser.getRetailerId());
			String strMediaPath = serverPathBuild.toString();

			CommonsMultipartFile[] imageFile = objRetLocation.getImageFilePath();

			if (imageFile.length > 0) {

				for (int i = 0; i < imageFile.length; i++) {

					if (imageFile[i].getSize() <= twoFiveMBinBytes) {
						fileName = imageFile[i].getOriginalFilename();
						dotIndex = fileName.lastIndexOf(".");
						fileExtn = fileName.substring(dotIndex + 1, fileName.length());

						if (fileExtn.equals(ApplicationConstants.ZIPFILE)) {
							zipFilepath = strMediaPath + "/" + imageFile[i].getOriginalFilename();
							Utility.writeFileData(imageFile[i], zipFilepath);
							zipInputStream = new ZipInputStream(new FileInputStream(zipFilepath));
							zipentry = zipInputStream.getNextEntry();

							while (zipentry != null) {
								// for each entry to be extracted
								entryName = zipentry.getName();
								if (entryName.contains("/")) {

									String[] folderStructure = entryName.split("/");
									entryName = folderStructure[folderStructure.length - 1];
								}

								extension = FilenameUtils.getExtension(entryName.trim());
								if (!"".equals(Utility.checkNull(extension))) {
									if (!extension.equals("png")) {
										entryName = FilenameUtils.removeExtension(entryName);
										if (!"".equals(Utility.checkNull(entryName))) {
											entryName = entryName + ApplicationConstants.PNGIMAGEFORMAT;
										}
									}
								}

								dotIndex = entryName.lastIndexOf(".");
								fileExtn = entryName.substring(dotIndex + 1, entryName.length());

								if ("gif".equalsIgnoreCase(fileExtn) || "bmp".equalsIgnoreCase(fileExtn) || "jpg".equalsIgnoreCase(fileExtn)
										|| "jpeg".equalsIgnoreCase(fileExtn) || fileExtn.equalsIgnoreCase(ApplicationConstants.PNGIMAGE)) {

									path = strMediaPath + "/" + entryName;
									fileoutputstream = new FileOutputStream(path);
									while ((n = zipInputStream.read(buf, 0, 1024)) > -1)
										fileoutputstream.write(buf, 0, n);
									fileoutputstream.close();
									zipInputStream.closeEntry();
									zipentry = zipInputStream.getNextEntry();
								} else {
									zipInputStream.closeEntry();
									zipentry = zipInputStream.getNextEntry();
									if (dotIndex != -1) {
										rejectedFileList.add(entryName);
									}
								}
							}

							zipInputStream.close();
							File f1 = new File(zipFilepath);
							f1.delete();
						} else {
							path = strMediaPath + "/" + imageFile[i].getOriginalFilename();
							Utility.writeFileData(imageFile[i], path);
						}
					} else {
						bErrorMsg = true;
						request.setAttribute("imageFilePath", "Please Select a file less than 25 MB");
						request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
					}
				}

				if (!rejectedFileList.isEmpty()) {
					if (rejectedFileList.size() <= 1) {
						bErrorMsg = true;
						request.setAttribute("imageFilePath", rejectedFileList.size() + " Unsupported files have been rejected");
						request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
					} else {
						bErrorMsg = true;
						request.setAttribute("imageFilePath", rejectedFileList.size() + " Unsupported files have been rejected");
						request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
					}
				}
			}

			final SearchResultInfo locResult = retailerService.getRetailerLocationList(retailerId, userId, lowerLimit);
			String phoneNum = null;
			for (int i = 0; i < locResult.getLocationList().size(); i++) {
				phoneNum = locResult.getLocationList().get(i).getPhonenumber().trim();
				locResult.getLocationList().get(i).setPhonenumber(phoneNum);
			}

			session.setAttribute(LOCATIONLIST, locResult);

			final Pagination objPage = Utility.getPagination(locResult.getTotalSize(), currentPage, "locationbatchupload.htm");
			session.setAttribute(ApplicationConstants.PAGINATION, objPage);

			if (!bErrorMsg) {
				request.setAttribute("imageFilePath", "Location Image File(s) are Uploaded Successfully.");
				request.setAttribute("imageUploadFile", "font-weight:bold;color:#00559c;");
			}

		} catch (ScanSeeServiceException e) {
			LOG.info("Inside LocationSetupController : uploadLocationImage : " + e);
			request.setAttribute("imageFilePath", "Error Occurred while uploading Location image");
			request.setAttribute("imageUploadFile", "font-weight:bold;color:red");
			return new ModelAndView(strViewName);
		}
		final RetailerLocation retailerLocation = new RetailerLocation();
		model.put(LOCATIONSETUPFORM, retailerLocation);
		return new ModelAndView(strViewName);
	}

	/**
	 * This controller method will upload retailer logo image in upload
	 * location.
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param objRetLocation
	 *            instance of RetailerLocation.
	 * @param response
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws Exception
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/uploadlocationimg.htm", method = RequestMethod.POST)
	public final String batchUplaodImage(@ModelAttribute("locationsetupform") RetailerLocation objRetLocation, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws Exception, ScanSeeServiceException {

		LOG.info("Inside LocationSetupController : batchUplaodImage ");
		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		InputStream inputStream = null;
		session.removeAttribute("cropImageSource");
		int rowIndex = 0;
		int w = 0;
		int h = 0;
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");
		try {
			objRetLocation.getImageFile()[objRetLocation.getRowIndex()].getInputStream();
			/* If IE browser row index. */
			rowIndex = objRetLocation.getRowIndex();
		} catch (Exception e) {
			/* If FireFox browser row index. */
			rowIndex = 0;
		}
		inputStream = objRetLocation.getImageFile()[rowIndex].getInputStream();
		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, inputStream);
		if (imageSizeValFlg) {
			session.setAttribute("locationImgPath", ApplicationConstants.UPLOADGRIDIMAGE);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
		} else {
			final BufferedImage img = Utility.getBufferedImageForMinDimension(objRetLocation.getImageFile()[rowIndex].getInputStream(),
					request.getRealPath("images"), objRetLocation.getImageFile()[rowIndex].getOriginalFilename(), "BatchUpload");
			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();

			imageSource = objRetLocation.getImageFile()[rowIndex].getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();
			final String filePath = tempImgPath + fileSeparator + objRetLocation.getImageFile()[rowIndex].getOriginalFilename();

			Utility.writeImage(img, filePath);

			if (imageValidSizeValFlg) {
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objRetLocation.getImageFile()[rowIndex].getOriginalFilename());
				session.setAttribute("locationImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			}

			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
		}
		model.put("locationsetupform", objRetLocation);
		return null;
	}

	/**
	 * This controller method will upload retailer logo image in manage location
	 * .
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param result
	 *            as request parameter.
	 * @param objRetLocation
	 *            instance of RetailerLocation.
	 * @param response
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws Exception
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/managelocationimg.htm", method = RequestMethod.POST)
	public final String manageUplaodImage(@ModelAttribute("locationform") RetailerLocation objRetLocation, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws Exception, ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : manageUplaodImage ");

		final String fileSeparator = System.getProperty("file.separator");
		String imageSource = null;
		boolean imageSizeValFlg = false;
		boolean imageValidSizeValFlg = false;
		final StringBuffer strResponse = new StringBuffer();
		final Date date = new Date();
		int rowIndex = 0;
		InputStream inputStream = null;

		session.removeAttribute("cropImageSource");

		int w = 0;
		int h = 0;

		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

		try {
			objRetLocation.getImageFile()[objRetLocation.getRowIndex()].getInputStream();
			/* If IE browser row index. */
			rowIndex = objRetLocation.getRowIndex();
		} catch (Exception e) {
			/* If FireFox browser row index. */
			rowIndex = 0;
		}

		inputStream = objRetLocation.getImageFile()[rowIndex].getInputStream();
		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, inputStream);

		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH,
				objRetLocation.getImageFile()[rowIndex].getInputStream());
		if (imageSizeValFlg) {
			session.setAttribute("locationImgPath", ApplicationConstants.UPLOADGRIDIMAGE);
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;
		} else {
			final BufferedImage img = Utility.getBufferedImageForMinDimension(objRetLocation.getImageFile()[rowIndex].getInputStream(),
					request.getRealPath("images"), objRetLocation.getImageFile()[rowIndex].getOriginalFilename(), "BatchUpload");
			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			final String tempImgPath = Utility.getTempMediaPath(ApplicationConstants.TEMP).toString();

			imageSource = objRetLocation.getImageFile()[rowIndex].getOriginalFilename();
			imageSource = FilenameUtils.removeExtension(imageSource);
			imageSource = imageSource + ".png" + "?" + date.getTime();
			final String filePath = tempImgPath + fileSeparator + objRetLocation.getImageFile()[rowIndex].getOriginalFilename();

			Utility.writeImage(img, filePath);

			if (imageValidSizeValFlg) {
				strResponse.append("ValidImageDimention");
				strResponse.append("|" + objRetLocation.getImageFile()[rowIndex].getOriginalFilename());
				session.setAttribute("locationImgPath", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			}

			strResponse.append("|" + "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			session.setAttribute("cropImageSource", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMPFOLDER + "/" + imageSource);
		}
		model.put("locationform", objRetLocation);
		return null;
	}

	/**
	 * This ModelAttribute sort Store start and end hours property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	@ModelAttribute("StoreStartHours")
	public Map<String, String> populateStoreStartHrs(HttpSession session) throws ScanSeeServiceException {
		final HashMap<String, String> mapStoreStartHrs = new HashMap<String, String>();
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				mapStoreStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
			} else {
				mapStoreStartHrs.put(String.valueOf(i), String.valueOf(i));
			}
		}
		final Iterator iterator = mapStoreStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapStoreStartHrs);
		session.setAttribute("StoreStartHours", sortedMap);
		return sortedMap;
		
		
		
		
	}

	/**
	 * This ModelAttribute sort Store start and end minutes property.
	 * 
	 * @return sortedMap.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@SuppressWarnings("rawtypes")
	@ModelAttribute("StoreStartMinutes")
	public Map<String, String> populatemapStoreStartMins() throws ScanSeeServiceException {
		final HashMap<String, String> mapStoreStartHrs = new HashMap<String, String>();
		for (int i = 0; i <= 55; i++) {
			if (i < 10) {
				mapStoreStartHrs.put(ApplicationConstants.ZERO + i, ApplicationConstants.ZERO + i);
				i = i + 4;
			} else {
				mapStoreStartHrs.put(String.valueOf(i), String.valueOf(i));
				i = i + 4;
			}
		}
		@SuppressWarnings("unused")
		final Iterator iterator = mapStoreStartHrs.entrySet().iterator();
		@SuppressWarnings("unchecked")
		final Map<String, String> sortedMap = Utility.sortByComparator(mapStoreStartHrs);

		return sortedMap;
	}

	/**
	 * This controller method will display the business Hours in Manage Location
	 * Screen
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param objRetLocation
	 *            instance of RetailerLocation.
	 * @param locationId
	 *            as request parameter.
	 * @return string buffer as html content
	 * @throws Exception
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/displayBusinessHours.htm", method = RequestMethod.GET)
	@ResponseBody
	public final String displayBusinessHours(@RequestParam("locationId") String locationId, HttpServletRequest request, ModelMap model, HttpSession session)
			throws IOException, ScanSeeServiceException {
		LOG.info("Inside LocationSetupController : displayBusinessHours");

		StringBuffer buffer = new StringBuffer();

		try {

			int lowerLimit = 0;
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final Long userId = loginUser.getUserID();
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			SearchResultInfo businessHours = null;

			businessHours = retailerService.getBusinessHours(retailerId, userId, lowerLimit, locationId);

			if (null != businessHours.getLocationList() && !businessHours.getLocationList().isEmpty()) {

				Map<String, String> minutes = populatemapStoreStartMins();
				Map<String, String> hours = populateStoreStartHrs(session);

				buffer.append("<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"grdTbl\">");
				buffer.append("<tbody class=\"addbody\">");

				buffer.append("<tr>");
				buffer.append("<td class=\"Label\"><label for=\"timeZone\">Select TimeZone</label></td>");
				buffer.append("<td colspan=\"3\">");
				buffer.append("<select name=\"storeTimeZoneId\">");
				buffer.append("<option value=\"0\" label=\"Please Select Time Zone\">Please Select Time Zone</option>");

				@SuppressWarnings("unchecked")
				List<TimeZones> zones = (ArrayList<TimeZones>) session.getAttribute("retailerTimeZoneslst");

				int index = 1;
				if (null != zones && !zones.isEmpty()) {
					for (TimeZones zoneItem : zones) {
						buffer.append("<option");
						if (null != businessHours.getTimeZone() && zoneItem.getTimeZoneId() == Integer.parseInt(businessHours.getTimeZone())) {
							buffer.append(" selected=\"selected\"");
						}
						buffer.append(" value=\"" + (index++) + "\">");
						buffer.append(zoneItem.getTimeZoneName());
						buffer.append("</option>");
					}
				}
				buffer.append("</td>");
				buffer.append("</tr>");

				buffer.append("<tr>");
				buffer.append("<th class=\"Label hrsLabel\" align=\"center\"><label>Day</label></th>");
				buffer.append("<th class=\"Label hrsLabel\" align=\"center\"><label>Open Time</label></th>");
				buffer.append("<th class=\"Label hrsLabel\" align=\"center\"><label>Close Time</label></th>");
				buffer.append("</tr>");

				if (null != businessHours.getLocationList() && !businessHours.getLocationList().isEmpty()) {
					for (RetailerLocation businessTime : businessHours.getLocationList()) {

						buffer.append("<tr>");
						buffer.append("<td class=\"Label\" align=\"left\"><label for=\"cst\">" + businessTime.getWeekDayName() + "</label>");
						buffer.append("</td>");

						buffer.append("<td>");
						buffer.append("<div class=\"mslct\">");

						buffer.append("<select name=\"storeStartHrs\" class=\"slctSmall\">");
						if (null != businessTime.getStoreStartHrs() && !businessTime.getStoreStartHrs().isEmpty()) {
							for (Map.Entry<String, String> entry : hours.entrySet()) {
								buffer.append("<option");
								if (!businessTime.getStoreStartHrs().equals("00")) {
									if (businessTime.getStoreStartHrs().equals(entry.getValue())) {
										buffer.append(" selected=\"selected\"");
									}
								}
								buffer.append(" value=\"" + entry.getValue() + "\">");
								buffer.append(entry.getValue());
								buffer.append("</option>");
							}
						}
						buffer.append("</select>Hrs");

						buffer.append("<select name=\"storeStartMins\" class=\"slctSmall\">");
						if (null != businessTime.getStoreStartMins() && !businessTime.getStoreStartMins().isEmpty()) {
							for (Map.Entry<String, String> entry : minutes.entrySet()) {
								buffer.append("<option");
								if (!businessTime.getStoreStartMins().equals("00")) {
									if (businessTime.getStoreStartMins().equals(entry.getValue())) {
										buffer.append(" selected=\"selected\"");
									}
								}
								buffer.append(" value=\"" + entry.getValue() + "\">");
								buffer.append(entry.getValue());
								buffer.append("</option>");
							}
						}
						buffer.append("</select>Mins");
						buffer.append("</div>");
						buffer.append("</td>");

						buffer.append("<td>");
						buffer.append("<div class=\"mslct\">");
						buffer.append("<select name=\"storeEndHrs\" class=\"slctSmall\">");
						if (null != businessTime.getStoreEndHrs() && !businessTime.getStoreEndHrs().isEmpty()) {
							for (Map.Entry<String, String> entry : hours.entrySet()) {
								buffer.append("<option");
								if (!businessTime.getStoreEndHrs().equals("00")) {
									if (businessTime.getStoreEndHrs().equals(entry.getValue())) {
										buffer.append(" selected=\"selected\"");
									}
								}
								buffer.append(" value=\"" + entry.getValue() + "\">");
								buffer.append(entry.getValue());
								buffer.append("</option>");
							}
						}
						buffer.append("</select>Hrs");

						buffer.append("<select name=\"storeEndMins\" class=\"slctSmall\">");
						if (null != businessTime.getStoreEndMins() && !businessTime.getStoreEndMins().isEmpty()) {
							for (Map.Entry<String, String> entry : minutes.entrySet()) {
								buffer.append("<option");
								if (!businessTime.getStoreEndMins().equals("00")) {
									if (businessTime.getStoreEndMins().equals(entry.getValue())) {
										buffer.append(" selected=\"selected\"");
									}
								}
								buffer.append(" value=\"" + entry.getValue() + "\">");
								buffer.append(entry.getValue());
								buffer.append("</option>");
							}
						}
						buffer.append("</select>Mins");
						buffer.append("</div>");
						buffer.append("</td>");
						buffer.append("</tr>");
					}
				}
				buffer.append("</tbody></table>");
			}
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
		return buffer.toString();
	}

	/**
	 * This controller method will save business hours in manage location screen
	 * 
	 * @param request
	 *            as request parameter.
	 * @param session
	 *            as request parameter.
	 * @param model
	 *            as request parameter.
	 * @param storeEndMins
	 *            as request parameter.
	 * @param storeStartHrs
	 *            as request parameter.
	 * @param storeStartMins
	 *            as request parameter
	 * @param retailerLocationID
	 *            as request parameter
	 * @param storeEndMins
	 *            as request parameter.
	 * @return string given a view name.
	 * @throws Exception
	 *             will be thrown.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	@RequestMapping(value = "/saveBusinessHours.htm", method = RequestMethod.GET)
	@ResponseBody
	public final String saveBusinessHours(@RequestParam("storeStartHrs") String storeStartHrs, @RequestParam("storeStartMins") String storeStartMins,
			@RequestParam("storeEndHrs") String storeEndHrs, @RequestParam("storeEndMins") String storeEndMins, @RequestParam("timeZone") String timeZone,
			@RequestParam("retailerLocationID") String retailerLocationID, HttpServletRequest request, ModelMap model, HttpSession session) throws IOException,
			ScanSeeServiceException {

		LOG.info("Inside LocationSetupController : fetchBatchLocation");
		List<String> locStartHrs = new ArrayList<String>();
		List<String> locStartMins = new ArrayList<String>();
		List<String> locEndHrs = new ArrayList<String>();
		List<String> locEndMins = new ArrayList<String>();

		try {

			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			final int retailerId = loginUser.getRetailerId();
			final Long userId = loginUser.getUserID();
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final RetailerService retailerService = (RetailerService) appContext.getBean(ApplicationConstants.RETAILERSERVICE);
			String businessHours = null;
			RetailerLocation businessHoursList = new RetailerLocation();

			businessHoursList.setStoreStartHrs(storeStartHrs);
			businessHoursList.setStoreStartMins(storeStartMins);
			businessHoursList.setStoreEndHrs(storeEndHrs);
			businessHoursList.setStoreEndMins(storeEndMins);
			businessHoursList.setTimeZoneHidden(timeZone);

			locStartHrs = new ArrayList<String>(Arrays.asList(businessHoursList.getStoreStartHrs().split(",")));
			locStartMins = new ArrayList<String>(Arrays.asList(businessHoursList.getStoreStartMins().split(",")));
			locEndHrs = new ArrayList<String>(Arrays.asList(businessHoursList.getStoreEndHrs().split(",")));
			locEndMins = new ArrayList<String>(Arrays.asList(businessHoursList.getStoreEndMins().split(",")));
			for (int i = 0; i < locStartHrs.size(); i++) {
				if (((!"00".equals(locStartHrs.get(i)) || !"00".equals(locStartMins.get(i))) || (!"00".equals(locEndHrs.get(i)) || !"00".equals(locEndMins
						.get(i))))) {
					if (timeZone.equals("Please Select Time Zone")) {
						return "saveBusinessHours";
					} else if (!timeZone.equals("Please Select Time Zone")) {
						businessHours = retailerService.saveBusinessHours(retailerId, userId, businessHoursList, retailerLocationID);
					}
				}
			}
			for (int i = 0; i < locStartHrs.size(); i++) {
				if ((("00".equals(locStartHrs.get(i)) || "00".equals(locStartMins.get(i))) || ("00".equals(locEndHrs.get(i)) || "00".equals(locEndMins.get(i))))) {
					businessHours = retailerService.saveBusinessHours(retailerId, userId, businessHoursList, retailerLocationID);
				}
			}
			return businessHours;
		} catch (ScanSeeServiceException e) {
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED, e.getMessage());
			throw e;
		}
	}

}
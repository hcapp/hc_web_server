package retailer.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import retailer.service.RetailerService;
import retailer.validator.RetailerUploadLogoValidator;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Retailer;
import common.pojo.RetailerUploadLogoInfo;
import common.pojo.Users;
import common.util.RetailerLeftNavigation;
import common.util.UtilCode;
import common.util.Utility;

@Controller
@RequestMapping(value = "/uploadRetailerLogo")
public class RetailerUploadLogoController
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RetailerUploadLogoController.class);

	RetailerUploadLogoValidator retailerUploadLogoValidator;

	@Autowired
	public void setRetailerUploadLogoValidator(RetailerUploadLogoValidator retailerUploadLogoValidator)
	{
		this.retailerUploadLogoValidator = retailerUploadLogoValidator;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showRetailerUploadLogo(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{

		LOG.info("RetailerUploadLogoController:: Inside Get Method");
		Users user = (Users) request.getSession().getAttribute("loginuser");
		Boolean isBand = user.getIsBandRetailer();
		Long retailerId = Long.valueOf(user.getRetailerId());
		RetailerUploadLogoInfo retailerUploadLogoInfo = new RetailerUploadLogoInfo();
		retailerUploadLogoInfo.setRetailerID(retailerId);
		List<Retailer> uploadLogoInfo = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			uploadLogoInfo = retailerService.fetchRetailerPreviewList(retailerId, isBand);

			if (uploadLogoInfo != null && !uploadLogoInfo.isEmpty())
			{
				Retailer retailerObj = uploadLogoInfo.get(0);

				if (null == retailerObj.getRetailerImagePath() || retailerObj.getRetailerImagePath().equals(""))
				{
					session.setAttribute("logosrc", ApplicationConstants.UPLOADIMAGEPATH);
				}
				else
				{
					session.setAttribute("logosrc", retailerObj.getRetailerImagePath());

				}
			}
			// Meyy Added
			String strCroppedImgStatus = (String) session.getAttribute("croppedlogosrc");
			if (Utility.checkNull(strCroppedImgStatus).equals(""))
			{
				session.setAttribute("croppedlogosrc", ApplicationConstants.UPLOADIMAGEPATH);
			}
			model.put("retaileruploadlogoinfoform", retailerUploadLogoInfo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in  preview:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("RetailerUploadLogoController:: Inside Exit Get Method");
		return "uploadRetailerLogo";

	}

	@SuppressWarnings("deprecation")
	@RequestMapping(method = RequestMethod.POST)
	public String createRetailerUploadLogo(@ModelAttribute("retaileruploadlogoinfoform") RetailerUploadLogoInfo retailerUploadLogoInfo,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws ScanSeeServiceException, IOException
	{
		CommonsMultipartFile retailerLogoInfo = retailerUploadLogoInfo.getRetailerLogo();

		LOG.info("Inside RetailerUploadLogoController : createRetailerUploadLogo ");

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");

		Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		/*
		 * String dashboardFlag = (String)
		 * request.getParameter("dashBoardFlag"); String realPath =
		 * request.getRealPath(""); String view = null; String status = null;
		 */
		String strImageStatus = null;
		boolean imageSizeValFlg = false;
		int w = 0;
		int h = 0;
		String navigation = retailerUploadLogoInfo.getNavigation();
		boolean imageValidSizeValFlg = false;
		StringBuffer strResponse = new StringBuffer();
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/xml");

		strImageStatus = (String) session.getAttribute("logosrc");
		if (Utility.checkNull(strImageStatus).equals(""))
		{
			session.setAttribute("logosrc", ApplicationConstants.UPLOADIMAGEPATH);
		}
		LOG.info("retailerUploadLogoInfo: " + retailerUploadLogoInfo.getRetailerLogo());
		LOG.info("X Pixel  :::::" + request.getParameter("x"));
		LOG.info("Y Pixel  :::::" + request.getParameter("y"));
		LOG.info("W Pixel  :::::" + request.getParameter("w"));
		LOG.info("H Pixel  :::::" + request.getParameter("h"));
		LOG.info("Img   :::::" + request.getParameter("imgId"));

		/*
		 * retailerUploadLogoValidator.validate(retailerUploadLogoInfo, result);
		 * if (result.hasErrors()) { if ("fromDashBoard".equals(dashboardFlag))
		 * { view = "imgCropPopup"; } else { view = "uploadRetailerLogo"; } //
		 * return new ModelAndView(view); } else {
		 */
		String filePathImages = request.getRealPath("") + "/" + ApplicationConstants.IMAGES + "/"
				+ retailerUploadLogoInfo.getRetailerLogo().getOriginalFilename();
		// String filePathImages = request.getRealPath("images") ;

		// imageValidSizeValFlg = Utility.validImageDimension(70, 70,
		// retailerUploadLogoInfo.getRetailerLogo().getInputStream());
		// Validate for minimum size- Span/Dileep
		imageSizeValFlg = Utility.validMinDimension(ApplicationConstants.CROPIMAGEHEIGHT, ApplicationConstants.CROPIMAGEWIDTH, retailerUploadLogoInfo
				.getRetailerLogo().getInputStream());

		// imageSizeValFlg = true;
		// imageValidSizeValFlg = true;
		if (imageSizeValFlg)
		{
			session.setAttribute("logosrc", ApplicationConstants.UPLOADIMAGEPATH);
			session.setAttribute("croppedlogosrc", ApplicationConstants.UPLOADIMAGEPATH);
			// Span-Dileep added
			response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" + "</imageScr>");
			return null;

			// commented code for fixing the crop image issue validation.

			// Validate for Maximum size- Span/Dileep
			// imageSizeValFlg = Utility.validDimension(1024, 950,
			// retailerUploadLogoInfo.getRetailerLogo().getInputStream());
			/*
			 * imageSizeValFlg = true; if (imageSizeValFlg) { BufferedImage img
			 * =
			 * ImageIO.read(retailerUploadLogoInfo.getRetailerLogo().getInputStream
			 * ()); // Image size to set cropping dimensions dynamically- //
			 * Span/Dileep w = img.getWidth(null); h = img.getHeight(null);
			 * session.setAttribute("imageHt", h);
			 * session.setAttribute("imageWd", w); status =
			 * retailerService.processRetailerUploadLogo(retailerUploadLogoInfo,
			 * filePathImages, loginUser); if
			 * (status.equalsIgnoreCase("SUCCESS")) { String outputFileName =
			 * retailerUploadLogoInfo.getRetailerLogo().getOriginalFilename();
			 * if (!Utility.isEmptyOrNullString(outputFileName)) {
			 * outputFileName =
			 * FilenameUtils.removeExtension(retailerUploadLogoInfo
			 * .getRetailerLogo().getOriginalFilename()); outputFileName =
			 * outputFileName + ".png"; } session.setAttribute("logosrc", "/" +
			 * ApplicationConstants.IMAGES + "/" + ApplicationConstants.RETAILER
			 * + "/" + loginUser.getRetailerId() + "/" + outputFileName);
			 * session.setAttribute("croppedlogosrc", "/" +
			 * ApplicationConstants.IMAGES + "/" + ApplicationConstants.RETAILER
			 * + "/" + loginUser.getRetailerId() + "/" + outputFileName); //
			 * session.setAttribute("logosrc","/ScanSeeWeb/images/flow_old/" +
			 * outputFileName); // session.setAttribute("croppedlogosrc",
			 * "/ScanSeeWeb/images/flow_old/" + outputFileName); Map<String,
			 * CommonsMultipartFile> uploadedLogoInfo = new HashMap<String,
			 * CommonsMultipartFile>();
			 * uploadedLogoInfo.put(String.valueOf(retailerUploadLogoInfo
			 * .getRetailerID()), retailerLogoInfo);
			 * session.setAttribute("uploadedLogoInfo", uploadedLogoInfo); //
			 * Following code is not required because common popup // is used
			 * for cropping in both registration and // dash board screens-
			 * Span/Dileep if (navigation != null && !"".equals(navigation)) {
			 * if (navigation.equals("dashboard") ||
			 * "fromDashBoard".equals(dashboardFlag)) { view = "imgCropPopup"; }
			 * else { view = "uploadRetailerLogo"; } } } else {
			 * session.setAttribute("logosrc",
			 * ApplicationConstants.UPLOADIMAGEPATH);
			 * session.setAttribute("croppedlogosrc",
			 * ApplicationConstants.UPLOADIMAGEPATH); if
			 * ("fromDashBoard".equals(dashboardFlag)) { view = "imgCropPopup";
			 * } else { view = "uploadRetailerLogo"; } // return new
			 * ModelAndView(view); } } } } else {
			 * session.setAttribute("logosrc",
			 * ApplicationConstants.UPLOADIMAGEPATH);
			 * session.setAttribute("croppedlogosrc",
			 * ApplicationConstants.UPLOADIMAGEPATH); // Span-Dileep added
			 * response.getWriter().write("<imageScr>" + "UploadLogoMaxSize" +
			 * "</imageScr>"); return null; }
			 */
		}
		else
		{
			// Mey related code

			/*
			 * session.setAttribute("logosrc",
			 * ApplicationConstants.UPLOADIMAGEPATH);
			 * session.setAttribute("croppedlogosrc",
			 * ApplicationConstants.UPLOADIMAGEPATH); // Span-Dileep added
			 * response.getWriter().write("<imageScr>" + "UploadLogoMinSize" +
			 * "</imageScr>");
			 */

			BufferedImage img = Utility.getBufferedImageForMinDimension(retailerUploadLogoInfo.getRetailerLogo().getInputStream(),
					request.getRealPath("images"), retailerUploadLogoInfo.getRetailerLogo().getOriginalFilename(), "Logo");

			// Image size to set cropping dimensions dynamically-
			w = img.getWidth(null);
			h = img.getHeight(null);
			session.setAttribute("imageHt", h);
			session.setAttribute("imageWd", w);

			String outputFileName = retailerUploadLogoInfo.getRetailerLogo().getOriginalFilename();

			if (!Utility.isEmptyOrNullString(outputFileName))
			{
				retailerService.processRetailerUploadLogoForMinDimension(retailerUploadLogoInfo, filePathImages, loginUser, img);
				outputFileName = FilenameUtils.removeExtension(retailerUploadLogoInfo.getRetailerLogo().getOriginalFilename());
				outputFileName = outputFileName + ".png";
				
				session.setAttribute("croppedlogosrc", "/" + ApplicationConstants.IMAGES + "/" + ApplicationConstants.TEMP + "/" + outputFileName);
			}
			/*
			 * session.setAttribute("logosrc", "/" + ApplicationConstants.IMAGES
			 * + "/" + ApplicationConstants.RETAILER + "/" +
			 * loginUser.getRetailerId() + "/" + outputFileName);
			 */
			

			// session.setAttribute("logosrc","/ScanSeeWeb/images/flow_old/" +
			// outputFileName);
			// session.setAttribute("croppedlogosrc",
			// "/ScanSeeWeb/images/flow_old/" + outputFileName);
			Map<String, CommonsMultipartFile> uploadedLogoInfo = new HashMap<String, CommonsMultipartFile>();
			uploadedLogoInfo.put(String.valueOf(retailerUploadLogoInfo.getRetailerID()), retailerLogoInfo);
			session.setAttribute("uploadedLogoInfo", uploadedLogoInfo);
			strResponse.append("ShowPopUp");
			response.getWriter().write("<imageScr>" + strResponse.toString() + "</imageScr>");
			// Following code is not required because common popup
			// is used for cropping in both registration and
			// dash board screens- Span/Dileep
			/*
			 * if (navigation != null && !"".equals(navigation)) { if
			 * (navigation.equals("dashboard") ||
			 * "fromDashBoard".equals(dashboardFlag)) { view = "imgCropPopup"; }
			 * else { view = "uploadRetailerLogo"; } } } else {
			 * session.setAttribute("logosrc",
			 * ApplicationConstants.UPLOADIMAGEPATH);
			 * session.setAttribute("croppedlogosrc",
			 * ApplicationConstants.UPLOADIMAGEPATH); if
			 * ("fromDashBoard".equals(dashboardFlag)) { view = "imgCropPopup";
			 * } else { view = "uploadRetailerLogo"; } // return new
			 * ModelAndView(view); } }
			 */

			// return null;
		}

		/*if (imageValidSizeValFlg)
		{
			
		}*/
		//strResponse.append("," + (String) session.getAttribute("logosrc"));
		// Span-Dileep added
		
		return null;
	}

	@RequestMapping(value = "/previewretaler.htm", method = RequestMethod.GET)
	public ModelAndView preview(@ModelAttribute("retaileruploadlogoinfoform") Retailer retailer, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws Exception, ScanSeeServiceException
	{
		LOG.info("preview in RetailerUploadLogoController:: Inside GET Method");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		List<Retailer> previewUploadList = null;
		Users user = (Users) request.getSession().getAttribute("loginuser");
		Boolean isBand = user.getIsBandRetailer();
		Long retailerID = retailer.getRetailerID();
		try
		{
			previewUploadList = retailerService.fetchRetailerPreviewList(retailerID,isBand);

			if (previewUploadList != null && !previewUploadList.isEmpty())
			{
				Retailer retailer2 = previewUploadList.get(0);

				if (null == retailer2.getRetailerImagePath() || retailer2.getRetailerImagePath().equals(""))
				{
					retailer2.setRetailerImagePath("/ScanSeeWeb/images/upload_imageRtlr.png");
				}
				session.setAttribute("previewRetailerUploadList", retailer2);
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in  preview:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		LOG.info("preview in RetailerUploadLogoController:: Exiting GET Method");
		return new ModelAndView("retailerPreviewUpload");

	}

	@RequestMapping(value = "/uploadRetailerLogoDashboard.htm", method = RequestMethod.GET)
	public ModelAndView showUploadRetailerLogoDashboardPage(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerUploadLogoController : showUploadRetailerLogoDashboardPage ");
		Users user = (Users) request.getSession().getAttribute("loginuser");
		Boolean isBand = user.getIsBandRetailer();
		Long retailerId = Long.valueOf(user.getRetailerId());
		RetailerUploadLogoInfo retailerUploadLogoInfo = new RetailerUploadLogoInfo();
		retailerUploadLogoInfo.setRetailerID(retailerId);
		String strImageStatus = null;
		List<Retailer> uploadLogoInfo = null;
		session.removeAttribute("logosrc");

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			uploadLogoInfo = retailerService.fetchRetailerPreviewList(retailerId, isBand);

			if (uploadLogoInfo != null && !uploadLogoInfo.isEmpty())
			{
				Retailer retailerObj = uploadLogoInfo.get(0);

				if (null == retailerObj.getRetailerImagePath() || retailerObj.getRetailerImagePath().equals(""))
				{
					session.setAttribute("logosrc", ApplicationConstants.UPLOADIMAGEPATH);
				}
				else
				{
					session.setAttribute("logosrc", retailerObj.getRetailerImagePath());

				}
			}

			RetailerLeftNavigation leftNav = (RetailerLeftNavigation) session.getAttribute("retlrLeftNav");
			leftNav = UtilCode.setRetailerModulesStyle("MyAppsite", "Logo", leftNav);
			session.setAttribute("retlrLeftNav", leftNav);

			model.put("retaileruploadlogoinfoform", retailerUploadLogoInfo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in  preview:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return new ModelAndView("uploadRetailerLogoDash");
	}

	@RequestMapping(value = "/uploadCroppedLogo.htm", method = RequestMethod.POST)
	public ModelAndView uploadCroppedLogo(HttpServletRequest request, HttpServletResponse response, BindingResult result, ModelMap model)
			throws ScanSeeServiceException
	{

		LOG.info("Inside Choose Cropped Logo");
		/*
		 * int xPixel = Integer.parseInt(request.getParameter("x")); int yPixel
		 * = Integer.parseInt(request.getParameter("y")); int wPixel =
		 * Integer.parseInt(request.getParameter("w")); int hPixel =
		 * Integer.parseInt(request.getParameter("h"));
		 * LOG.info("xPixel : "+xPixel); LOG.info("yPixel : "+yPixel);
		 * LOG.info("wPixel : "+wPixel); LOG.info("hPixel : "+hPixel);
		 * LOG.info("Content Type : "+request.getContentType());
		 */

		// boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		// LOG.info("isMultipart : "+isMultipart);
		// if(isMultipart)
		// {
		// MultipartHttpServletRequest multipartRequest =
		// (MultipartHttpServletRequest) request;

		// MultipartFile multipartFile =
		// multipartRequest.getFile("retailerLogo");
		// LOG.info("Multipart File :"+multipartFile.getOriginalFilename());

		// UploadCroppedImageBean obj = (UploadCroppedImageBean) command;
		// LOG.info("Command Object :"+uploadCroppedImageBean.getxPixel());
		/*
		 * FileItemFactory factory = new DiskFileItemFactory();
		 * ServletFileUpload upload = new ServletFileUpload(factory); try { List
		 * items = upload.parseRequest(request); Iterator iterator =
		 * items.iterator(); while (iterator.hasNext()) { FileItem item =
		 * (FileItem) iterator.next(); if (!item.isFormField()) {
		 * LOG.info("File Name :"+ item.getName()); LOG.info("Input Stream :"+
		 * item.getInputStream()); } else {
		 * LOG.info("Field Name : "+item.getFieldName());
		 * LOG.info("String Value : "+item.getString()); } } } catch (Exception
		 * e) { e.printStackTrace(); } // } /* ServletContext servletContext =
		 * request.getSession().getServletContext(); WebApplicationContext
		 * appContext =
		 * WebApplicationContextUtils.getWebApplicationContext(servletContext);
		 * RetailerService retailerService = (RetailerService)
		 * appContext.getBean("retailerService");
		 */

		Users user = (Users) request.getSession().getAttribute("loginuser");
		Long retailerId = Long.valueOf(user.getRetailerId());
		RetailerUploadLogoInfo retailerUploadLogoInfo = new RetailerUploadLogoInfo();
		retailerUploadLogoInfo.setRetailerID(retailerId);

		String strImageStatus = null;

		strImageStatus = (String) request.getSession().getAttribute("logosrc");
		if (Utility.checkNull(strImageStatus).equals(""))
		{
			request.getSession().setAttribute("logosrc", ApplicationConstants.UPLOADIMAGEPATH);
		}
		model.put("retaileruploadlogoinfoform", retailerUploadLogoInfo);

		return new ModelAndView("imgCropPopup");

	}
	
	@RequestMapping(value = "/addlogoinstructions.htm", method = RequestMethod.GET)
	public final String addLogoInstructions(HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException {
		final String strMethodName = "addLogoInstructions";
		final String strViewName = "addlogoinstructions";

		LOG.info("Start addLogoInstructions : " + strMethodName);
		/*String strPage = request.getParameter("cptype");
		request.setAttribute("cptype", strPage);*/

		LOG.info("End addLogoInstructions : " + strMethodName);
		return strViewName;
	}

}
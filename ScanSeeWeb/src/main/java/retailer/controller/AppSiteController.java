package retailer.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import retailer.service.RetailerService;
import retailer.validator.RetailerPageValidator;
import retailer.validator.RetailerProfileValidator;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Category;
import common.pojo.Retailer;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerLocation;
import common.pojo.RetailerRegistration;
import common.pojo.State;
import common.util.Utility;

@Controller
public class AppSiteController
{

	private static final Logger LOG = LoggerFactory.getLogger(AppSiteController.class);

	RetailerPageValidator retailerPageValidator;
	/**
	 * Variable retailerProfileValidator declared as instance of
	 * RetailerProfileValidator.
	 */
	RetailerProfileValidator retailerProfileValidator;

	/**
	 * Variable LOGIN_URL declared as String.
	 */
	final String APPSITE_LOCATION = "createappsiteloc.htm";

	/**
	 * Variable DUPLICATE_USER_VIEW declared as String.
	 */
	final String VIEW_NAME = "appsite";

	/**
	 * To set objProfileDataVal.
	 * 
	 * @param objProfileDataVal
	 *            to set.
	 */
	@Autowired
	public void setRetailerProfileValidator(RetailerProfileValidator retailerProfileValidator)
	{
		this.retailerProfileValidator = retailerProfileValidator;
	}

	@Autowired
	public void setRetailerPageValidator(RetailerPageValidator retailerPageValidator)
	{
		this.retailerPageValidator = retailerPageValidator;
	}

	@RequestMapping(value = "/appsite.htm", method = RequestMethod.GET)
	public String showAppSiteFrom(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : showAppSiteFrom Registration Form ");
		RetailerRegistration retailerRegistrationInfo = new RetailerRegistration();
		model.put("appsiteform", retailerRegistrationInfo);
		ArrayList<State> states = null;

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			SupplierService supplierService = (SupplierService) appContext.getBean("supplierService");
			states = supplierService.getAllStates();
			session.setAttribute("statesListCreat", states);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AppSiteController : showAppSiteFrom Registration Form   : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Exit AppSiteController : showAppSiteFrom Registration Form ");
		return "appsite";
	}

	@RequestMapping(value = "/regppsiteretailer", method = RequestMethod.POST)
	public ModelAndView createAppsite(@ModelAttribute("appsiteform") RetailerRegistration retailerRegistrationInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside AppSiteController : createAppsite ");

		boolean isValidCorpPhoneNum = true;

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		String strLogoImage = retailerService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
		String view = null;
		session.removeAttribute("appListCreatedRetailId");
		session.removeAttribute("appListCreatedUserId");
		session.removeAttribute("isLocationSelectedFlg");
		session.removeAttribute("CorporateStoreId");
		session.removeAttribute("appListCreatedRetailLocId");
		try
		{
			// retailerRegistrationInfo.setState(retailerRegistrationInfo.getStateCodeHidden());

			isValidCorpPhoneNum = Utility.validatePhoneNum(retailerRegistrationInfo.getContactPhone());

			retailerProfileValidator.validateAppSite(retailerRegistrationInfo, result);

			// till here only for mandatiory validation done
			// from here checking the valida email id or not and pass word
			// double check
			if (!isValidCorpPhoneNum)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.INVALIDCORPPHONE);
			}

			if (result.hasErrors())
			{
				view = VIEW_NAME;
				return new ModelAndView(view);
			}

			RetailerRegistration objRetailRegResponse = retailerService.registerFreeAppListRetailer(retailerRegistrationInfo, strLogoImage);
			if (objRetailRegResponse.getResponse() != null && objRetailRegResponse.getResponse().equals(ApplicationConstants.SUCCESS))
			{
				// view = APPSITE_LOCATION;
				session.setAttribute("appListCreatedRetailId", objRetailRegResponse.getRetailID());
				session.setAttribute("appListCreatedRetailLocId", objRetailRegResponse.getRetailLocId());
				// session.setAttribute("appListCreatedUserId",
				// objRetailRegResponse.getUserId());
				session.setAttribute("appListCreatedRetailerName", retailerRegistrationInfo.getRetailerName().trim());
				session.setAttribute("appListCreatedRetailerInfo", retailerRegistrationInfo);
				session.removeAttribute("supplierCity");
				LOG.info("AppSite Retailer Registration Stage one Successful... ");
				// LOG.info("User ID:" + objRetailRegResponse.getUserId());
				LOG.info("Retail ID:" + objRetailRegResponse.getRetailID());
				List<Retailer> duplicateRetList = objRetailRegResponse.getDuplicateRetList();

				if (duplicateRetList != null && !duplicateRetList.isEmpty())
				{

					session.setAttribute("duplicateRetList", duplicateRetList);
					view = "duplicateretailerlist";

				}
				else
				{

					return new ModelAndView(new RedirectView("finalizereg.htm"));

				}

			}
			else if (objRetailRegResponse.getResponse() != null && objRetailRegResponse.getResponse().equals(ApplicationConstants.DUPLICATE_USER))
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.DUPLICATE_USER);

				if (result.hasErrors())
				{
					view = VIEW_NAME;
				}
				else if (objRetailRegResponse.getResponse() != null && objRetailRegResponse.getResponse().equals(ApplicationConstants.FAILURE))
				{
					LOG.info("Inside AppSiteController : createAppsite : Failure or Problem Occured ");
				}
			}
			else if (objRetailRegResponse.getResponse() != null
					&& objRetailRegResponse.getResponse().equals(ApplicationConstants.DUPLICATE_BUSINESSNAME))
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.DUPLICATE_BUSINESSNAME);
				if (result.hasErrors())
				{
					view = VIEW_NAME;
				}
				else if (objRetailRegResponse.getResponse() != null && objRetailRegResponse.getResponse().equals(ApplicationConstants.FAILURE))
				{
					LOG.info("Inside AppSiteController : createAppsite : Failure or Problem Occured ");
				}
			}
			else if (objRetailRegResponse.getResponse() != null && objRetailRegResponse.getResponse().equals(ApplicationConstants.GEOERROR))
			{

				if (objRetailRegResponse.isGeoErr())
				{

					if (null == objRetailRegResponse.getRetailerLocationLatitude())
					{
						retailerProfileValidator.validate(objRetailRegResponse, result, ApplicationConstants.LATITUDE);
					}

					if (null == objRetailRegResponse.getRetailerLocationLongitude())
					{
						retailerProfileValidator.validate(objRetailRegResponse, result, ApplicationConstants.LONGITUDE);
					}
				}
				else
				{
					// request.setAttribute("GEOERROR",
					// ApplicationConstants.GEOERROR);
					retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.GEOERROR);
					retailerRegistrationInfo.setGeoErr(true);

				}

				if (result.hasErrors())
				{
					view = VIEW_NAME;
				}
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AppSiteController : createAppsite : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Inside AppSiteController : createAppsite ");
		if (view.equals(APPSITE_LOCATION))
		{
			session.removeAttribute("supplierCity");
			return new ModelAndView(new RedirectView(view));
		}
		else
		{
			return new ModelAndView(view);
		}

	}

	@RequestMapping(value = "/createappsiteloc.htm", method = RequestMethod.GET)
	public String createAppSiteLocation(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		LOG.info("Inside AppSiteController : createAppSiteLocation ");
		RetailerLocation retailerLocation = new RetailerLocation();
		model.put("locationsetupform", retailerLocation);
		LOG.info("Exit AppSiteController : createAppSiteLocation ");
		return "appsitelocation";
	}

	/**
	 * The updateLocationSetUp controller method is to call service and dao
	 * methods.
	 * 
	 * @param rebatesVo
	 *            Rebates instance as request parameter
	 * @param result
	 *            BindingResult instance as parameter.
	 * @param request
	 *            HttpServletRequest instance as request parameter.
	 * @param response
	 *            HttpServletResponse instance as request parameter.
	 * @param session
	 *            HttpSession instance as parameter.
	 * @return VIEW_NAME editRebates view.
	 */
	@RequestMapping(value = "/createappsiteloc.htm", method = RequestMethod.POST)
	public ModelAndView saveLocationList(@ModelAttribute("locationsetupform") RetailerLocation retailerLocation, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside AppSiteController : saveLocationList");

		String returnView = "appsitelocation";
		try
		{

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			List<RetailerLocation> locationList = retailerService.saveAppListLocation(retailerLocation);
			if (null != locationList && locationList.get(0).isGeoError())
			{

				request.setAttribute("appSiteLocGeoErr", true);
				request.setAttribute("appSiteLocHigltRows", locationList.get(0).getHighliteRow());
				session.setAttribute("appSiteLocList", locationList);
				retailerLocation = new RetailerLocation();
				model.put("locationsetupform", retailerLocation);
				LOG.info("Exit AppSiteController : saveLocationList");
				return new ModelAndView("appsitelocation");
			}
			else
			{
				LOG.info("Exit AppSiteController : saveLocationList");
				return new ModelAndView(new RedirectView("createAppSite.htm"));
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred inside AppSiteController : saveLocationList :" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		// return new ModelAndView(returnView);
	}

	@RequestMapping(value = "/appsiteqrcode.htm", method = RequestMethod.GET)
	public String showAppSiteQrCode(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		LOG.info("Inside AppSiteController : showAppSiteQrCode ");
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		model.put("createAppSite", retailerCustomPage);
		LOG.info("Exit AppSiteController : showAppSiteQrCode ");
		return "appSiteQRCode";
	}

	@RequestMapping(value = "/createAppSite.htm", method = RequestMethod.GET)
	public String createAppSite(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		LOG.info("Inside AppSiteController : createAppSite ");
		RetailerCustomPage retailerCustomPage = new RetailerCustomPage();
		model.put("createAppSite", retailerCustomPage);
		LOG.info("Exit AppSiteController : createAppSite ");
		return "createAppSite";
	}

	@RequestMapping(value = "/createAppSite.htm", method = RequestMethod.POST)
	public ModelAndView createAppSiteOnSubmit(@ModelAttribute("createAppSite") RetailerCustomPage retailerCustomPage, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		LOG.info("Inside AppSiteController : createAppSiteOnSubmit ");
		String respone = ApplicationConstants.FAILURE;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		String returnview = "createAppSite";
		Long appListCreatedRetailId = (Long) session.getAttribute("appListCreatedRetailId");
		Integer appListCreatedUserId = (Integer) session.getAttribute("appListCreatedUserId");
		List<RetailerLocation> retailerLocations = null;
		StringBuilder builder = new StringBuilder();
		List<RetailerLocation> retailerLocationQRDetails = new ArrayList<RetailerLocation>();
		try
		{
			retailerPageValidator.validateAppListAnythgPage(retailerCustomPage, result);
			if (result.hasErrors())
			{

				return new ModelAndView(returnview);
			}
			else
			{
				if (null != appListCreatedRetailId && !appListCreatedRetailId.equals(""))
				{
					retailerLocations = retailerService.retrievRetailLocation(appListCreatedRetailId.intValue());

					if (!retailerLocations.isEmpty())
					{

						for (int i = 0; i < retailerLocations.size(); i++)
						{
							RetailerLocation location = retailerLocations.get(i);
							builder.append(location.getRetailerLocID());
							builder.append(",");
						}
					}

					retailerCustomPage.setRetailerId(appListCreatedRetailId.intValue());
					retailerCustomPage.setRetailLocationID(builder.toString());
				}

				respone = retailerService.createAppListingAboutUsPage(retailerCustomPage);

				if (respone != null && respone.equals(ApplicationConstants.SUCCESS))
				{
					if (!retailerLocations.isEmpty())
					{

						for (int i = 0; i < retailerLocations.size(); i++)
						{
							RetailerLocation location = retailerLocations.get(i);
							RetailerLocation retailerLocationQr = new RetailerLocation();
							RetailerCustomPage pageDetails = retailerService.getLocQrCodeDetails(Long.valueOf(location.getRetailerLocID()),
									appListCreatedRetailId.intValue(), appListCreatedUserId.longValue());
							retailerLocationQr.setQrImagePath(pageDetails.getQrImagePath());

							retailerLocationQr.setImagName(new File(pageDetails.getQrImagePath()).getName());
							retailerLocationQr.setStoreIdentification(location.getStoreIdentification());
							retailerLocationQRDetails.add(retailerLocationQr);
						}
					}
					session.setAttribute("retailerLocationQrList", retailerLocationQRDetails);
					return new ModelAndView(new RedirectView("appsiteqrcode.htm"));
				}

			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred inside AppSiteController : saveLocationList :" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Exit AppSiteController : createAppSiteOnSubmit ");
		return new ModelAndView(returnview);

	}

	@RequestMapping(value = "/finalizereg", method = RequestMethod.GET)
	public String finalizeRegistration(HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
			throws ScanSeeServiceException
	{
		LOG.info("Inside RetailerProfileController : showAppSiteFrom Registration Form ");
		RetailerRegistration retailerRegistrationInfo = new RetailerRegistration();
		model.put("appsiteform", retailerRegistrationInfo);
		ArrayList<Category> arBCategoryList = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			arBCategoryList = retailerService.getAllRetailerProfileBusinessCategory();
			session.setAttribute("categoryList", arBCategoryList);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AppSiteController : showAppSiteFrom Registration Form   : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Inside AppSiteController : showAppSiteFrom Registration Form ");
		return "registerfreeappsitefinalize";
	}

	@RequestMapping(value = "/finalizereg", method = RequestMethod.POST)
	public ModelAndView finalizeRegistration(@ModelAttribute("appsiteform") RetailerRegistration retailerRegistrationInfo, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session) throws IOException,
			ScanSeeServiceException
	{
		LOG.info("Inside AppSiteController : finalizeRegistration ");
		String emailId = null;
		boolean acceptTerm = false;
		boolean isValidEmail = true;
		boolean isValidContactNum = true;
		String strCategory = null;
		boolean isValidUrl = true;

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		String strLogoImage = retailerService.getDomainName() + ApplicationConstants.SCANSEE_LOGO_FOR_MAILSENDING;
		String view = "registerfreeappsitefinalize";

		Long retailId;
		Long retailLocId;
		String retailName = null;
		RetailerRegistration objRetailRegResponse = null;
		try
		{
			emailId = retailerRegistrationInfo.getContactEmail();
			acceptTerm = retailerRegistrationInfo.isTerms();
			strCategory = retailerRegistrationInfo.getbCategory();
			retailerRegistrationInfo.setbCategoryHidden(retailerRegistrationInfo.getbCategory());

			retailerProfileValidator.validateAppSiteFinalize(retailerRegistrationInfo, result);
			if (null == strCategory || "".equals(strCategory))
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.BUSINESSCATEGORY);
			}
			if (result.hasErrors())
			{

				return new ModelAndView(view);
			}

			if (!"".equals(retailerRegistrationInfo.getWebUrl()))
			{
				isValidUrl = Utility.validateURL(retailerRegistrationInfo.getWebUrl());
			}

			if (!isValidUrl)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.WEBURL);
			}

			isValidContactNum = Utility.validatePhoneNum(retailerRegistrationInfo.getContactPhoneNo());
			if (!isValidContactNum)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.INVALIDCONTPHONE);
			}

			isValidEmail = Utility.validateEmailId(emailId);
			if (!isValidEmail)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.INVALIDEMAIL);

			}
			if (!acceptTerm)
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.TERM);
			}
			if (result.hasErrors())
			{

				return new ModelAndView(view);
			}

			/*
			 * if (null != session.getAttribute("appListCreatedUserId") &&
			 * !"".equals(session.getAttribute("appListCreatedUserId"))) {
			 * userId = (Integer) session.getAttribute("appListCreatedUserId");
			 * retailerRegistrationInfo.setUserId(userId); }
			 */
			if (null != session.getAttribute("appListCreatedRetailId") && !"".equals(session.getAttribute("appListCreatedRetailId")))
			{
				retailId = (Long) session.getAttribute("appListCreatedRetailId");
				retailerRegistrationInfo.setRetailID(retailId);
			}

			if (null != session.getAttribute("appListCreatedRetailLocId") && !"".equals(session.getAttribute("appListCreatedRetailLocId")))
			{
				retailLocId = (Long) session.getAttribute("appListCreatedRetailLocId");
				retailerRegistrationInfo.setRetailLocId(retailLocId);
			}
			if (null != session.getAttribute("appListCreatedRetailerName") && !"".equals(session.getAttribute("appListCreatedRetailerName")))
			{
				retailName = (String) session.getAttribute("appListCreatedRetailerName");
				retailerRegistrationInfo.setRetailerName(retailName);
			}

			objRetailRegResponse = retailerService.finalizeFreeAppSiteRetReg(retailerRegistrationInfo, strLogoImage);
			if (objRetailRegResponse.getResponse() != null && objRetailRegResponse.getResponse().equals(ApplicationConstants.DUPLICATE_USER))
			{
				retailerProfileValidator.validate(retailerRegistrationInfo, result, ApplicationConstants.DUPLICATE_USER);

				if (result.hasErrors())
				{
					view = VIEW_NAME;
				}

			}
			else
			{
				session.setAttribute("appListCreatedUserId", objRetailRegResponse.getUserId());
				session.setAttribute("isLocationSelectedFlg", retailerRegistrationInfo.isIslocation());
				session.setAttribute("CorporateStoreId", retailerRegistrationInfo.getStoreNum());
				RetailerRegistration retailerRegistrationInfoSession = (RetailerRegistration) session.getAttribute("appListCreatedRetailerInfo");

				retailerRegistrationInfoSession.setWebUrl(retailerRegistrationInfo.getWebUrl());
				retailerRegistrationInfoSession.setKeyword(retailerRegistrationInfo.getKeyword());
				retailerRegistrationInfoSession.setStoreNum(retailerRegistrationInfo.getStoreNum());
				retailerRegistrationInfoSession.setStateHidden(retailerRegistrationInfoSession.getState());
				session.setAttribute("appListCreatedRetailerInfo", retailerRegistrationInfoSession);

				session.removeAttribute("supplierCity");
				view = APPSITE_LOCATION;

			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AppSiteController : createAppsite : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}

		LOG.info("Inside AppSiteController : createAppsite ");
		if (view.equals(APPSITE_LOCATION))
		{
			session.removeAttribute("supplierCity");
			return new ModelAndView(new RedirectView(view));
		}
		else
		{
			return new ModelAndView(view);
		}

	}

	@RequestMapping(value = "/validateretailer", method = RequestMethod.POST)
	public ModelAndView validateDuplicateRet(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws IOException, ScanSeeServiceException
	{

		LOG.info("Inside AppSiteController : validateDuplicateRet method");
		String selRetailer = (String) request.getParameter("retailId");
		int duplicateRetailId;
		int duplicateRetLocId;
		Long retailId = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
		String responseStr = null;
		try
		{

			if (selRetailer == null || "none".equals(selRetailer))
			{
				return new ModelAndView(new RedirectView("finalizereg.htm"));

			}
			else
			{
				String[] arr = selRetailer.split(",");

				if (null != session.getAttribute("appListCreatedRetailId") && !"".equals(session.getAttribute("appListCreatedRetailId")))
				{
					retailId = (Long) session.getAttribute("appListCreatedRetailId");

				}

				duplicateRetailId = Integer.valueOf(arr[0]);
				duplicateRetLocId = Integer.valueOf(arr[1]);
			}

			responseStr = retailerService.validateDuplicateRetailer(retailId, duplicateRetailId, duplicateRetLocId);
			if (responseStr != null && responseStr.equals(ApplicationConstants.SUCCESS))
			{
				return new ModelAndView(new RedirectView("finalizereg.htm"));
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AppSiteController : validateDuplicateRet : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Exit AppSiteController : validateDuplicateRet method");
		return new ModelAndView("duplicateretailerlist");

	}

	@RequestMapping(value = "/markAsDuplicateRetailer.htm", method = RequestMethod.GET)
	@ResponseBody
	public String markAsDuplicateRetailer(@RequestParam(value = "LocId", required = true) Long retailLocId, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1) throws ScanSeeServiceException
	{
		LOG.info("Inside AppSiteController : markAsDuplicateRetailer method ");
		RetailerRegistration retailerRegistrationInfo = new RetailerRegistration();
		model.put("appsiteform", retailerRegistrationInfo);
		String response;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			RetailerService retailerService = (RetailerService) appContext.getBean("retailerService");
			Long retailId = (Long) session.getAttribute("appListCreatedRetailId");
			response = retailerService.markAsDuplicateRetailer(retailId, retailLocId);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Inside AppSiteController : markAsDuplicateRetailer method  : " + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Inside AppSiteController : markAsDuplicateRetailer method ");
		return response;
	}
}

package retailer.dao;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AccountType;
import common.pojo.AppConfiguration;
import common.pojo.AppSiteDetails;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.ContactType;
import common.pojo.Coupon;
import common.pojo.DropDown;
import common.pojo.Event;
import common.pojo.HotDealInfo;
import common.pojo.LocationResult;
import common.pojo.PlanInfo;
import common.pojo.Product;
import common.pojo.ProductHotDeal;
import common.pojo.Rebates;
import common.pojo.RetailProduct;
import common.pojo.Retailer;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerImages;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.RetailerLocationProduct;
import common.pojo.RetailerLocationProductJson;
import common.pojo.RetailerProfile;
import common.pojo.RetailerRegistration;
import common.pojo.SearchResultInfo;
import common.pojo.State;
import common.pojo.StoreInfoVO;
import common.pojo.TimeZones;
import common.pojo.Tutorial;

/**
 * DAO methods of Retailer module.
 * 
 * @author Created by SPAN.
 */
public interface RetailerDAO
{
	/**
	 * This DAO method save Retailer register details and success or failure
	 * depending upon the result of operation to service method .
	 * 
	 * @param objRetailerRegistration
	 *            instance of RetailerRegistration.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String addRetailerRegistration(RetailerRegistration objRetailerRegistration) throws ScanSeeWebSqlException;

	/**
	 * This method search product retailer.
	 * 
	 * @param
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo searchProductRetailer(String strProductUpcOrName, int usrId, int lowerLimit) throws ScanSeeWebSqlException;

	/**
	 * This method will return the Rebate Name List based on parameter.
	 * 
	 * @param RebateName
	 *            , RetailLocationID
	 * @return RebateName List based on parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getRetailerByRebateName(Long retailerid, String rebateName, int lowerLimit) throws ScanSeeWebSqlException;

	/**
	 * This DAO method return all the locations for that retailer based on input
	 * parameter and its locationList return to service method.
	 * 
	 * @param retailID
	 *            as request parameter.
	 * @return locationList, All the retailer locations.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	ArrayList<RetailerLocation> couponRetaileLocation(Long retailID) throws ScanSeeWebSqlException;

	/**
	 * This method update the retailer registration profile
	 * 
	 * @param objRetProfile
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String updateProfile(RetailerRegistration objRetProfile) throws ScanSeeWebSqlException;

	/**
	 * This method create New Coupon details.
	 * 
	 * @param objCoupon
	 *            instance of Coupon.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String addCoupon(Coupon objCoupon) throws ScanSeeWebSqlException;

	/**
	 * This method will return the Coupon Name List based on parameter.
	 * 
	 * @param strCouponName
	 * @return CouponName List based on parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public ArrayList<Product> getCouponByCouponName(String strCouponName, Long RetailerLocID, Long RetailID) throws ScanSeeWebSqlException;

	/**
	 * This method update Existing Coupon details.
	 * 
	 * @param objCoupon
	 *            instance of Coupon.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	public String updateCouponInfo(Coupon objCoupon) throws ScanSeeWebSqlException;

	/**
	 * This method displays Coupon details by its couponID.
	 * 
	 * @param retailID
	 *            .
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public SearchResultInfo retailerCouponDisplay(Long retailID, String searchKey, int lowerLimit, int iRecordCount) throws ScanSeeWebSqlException;

	/**
	 * This method will return the coupon details based on CouponID parameter.
	 * 
	 * @param CouponID
	 * @return CouponID List based on parameter.
	 * @throws Exception
	 */
	public ArrayList<Coupon> getCouponIDByCouponDetails(Long objCouponID) throws ScanSeeWebSqlException;

	/**
	 * This method will return the ads that are created by a retailer based on
	 * AdsName's parameter.
	 * 
	 * @param objRetailID
	 *            , strAdsName
	 * @return AdsName List based on parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getRetailerAdsByAdsNames(Long objRetailID, String strAdsName, int lowerLimit, int recordCount)
			throws ScanSeeWebSqlException;

	/**
	 * This DAO method create new banner page and its status return to service
	 * method.
	 * 
	 * @param retLocationAdvertisement
	 *            instance of RetailerLocationAdvertisement
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	String retailerAddsInsertion(RetailerLocationAdvertisement retLocationAdvertisement) throws ScanSeeWebSqlException;

	/**
	 * This DAO method to update Retailer register details and its status will
	 * return to service method.
	 * 
	 * @param retailerProfile
	 *            instance of RetailerProfile.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String addRetailerUpdateProfile(RetailerProfile retailerProfile) throws ScanSeeWebSqlException;

	/**
	 * This DAO method upload multiple locations with the help of CSV file
	 * templates and return status to service method.
	 * 
	 * @param retailerLocationList
	 *            instance of ArrayList<RetailerLocation>.
	 * @param userId
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String retailerLocationUpload(ArrayList<RetailerLocation> retailerLocationList, Long userId, int retailID) throws ScanSeeWebSqlException;

	/**
	 * This DAO method display Retailer register details and its status return
	 * to service layer.
	 * 
	 * @param iRetailID
	 *            as request parameter.
	 * @return RetailerProfile details as list.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	List<RetailerProfile> getRetailUpdateProfileByRetailID(int iRetailID, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This method ReRun the Retailer Profile
	 * 
	 * @param coupon
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String retailerCouponReRun(Coupon coupon) throws ScanSeeWebSqlException;

	/**
	 * This method search the rebates of retailer based on retailerID and
	 * rebateName.
	 * 
	 * @param retailID
	 * @param rebateName
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<Rebates> rebateSearchRetailer(int retailID, String rebateName) throws ScanSeeWebSqlException;

	/**
	 * This method insert the new rebates.
	 * 
	 * @param objRebates
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public String retailerAddRebate(Rebates objRebates) throws ScanSeeWebSqlException;

	/**
	 * This method displays the Rebates Information
	 * 
	 * @param retailID
	 * @param rebateSearch
	 * @return list of rebates based on retailId and rebateSearch
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public ArrayList<Rebates> retailerRebateDisplay(int retailID, String rebateSearch) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will add one new HotDeals for retailer and return status
	 * to service method.
	 * 
	 * @param objHotDeal
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String retailerAddHotDeal(HotDealInfo objHotDeal) throws ScanSeeWebSqlException;

	/**
	 * This method search the retailer productSetupProductSearch
	 * 
	 * @param searchParameter
	 * @param lowerLimit
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public SearchResultInfo retailerProductSetupProductSearch(String searchParameter, int lowerLimit, int rowCount) throws ScanSeeWebSqlException;

	/**
	 * This method will return all the Retailer Location ID and Store
	 * Identification based on Retailer ID.
	 * 
	 * @param iRetailID
	 * @return Retailer Location ID and Store Identification List based on input
	 *         parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocation> getRetailProductSetupRetailLocationSearch(Long iRetailID) throws ScanSeeWebSqlException;

	/**
	 * This method Add Retail Product Setup Search Results Modification details.
	 * 
	 * @param objRetailLoctnProduct
	 *            instance of RetailerLocationProduct.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	public String associateRetailerProd(RetailerLocationProduct objRetailLoctnProduct) throws Exception;

	/**
	 * This DAO method will return all list of Retailer Location/Store
	 * Identification by add new location, upload multiple locations with the
	 * help of CSV file templates and return status to service method..
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	SearchResultInfo getLocationList(int retailerId, long userId, int lowerLimit) throws ScanSeeWebSqlException;

	/**
	 * This method will return all the Retailer Location ID and Store
	 * Identification based on Retailer ID.
	 * 
	 * @param iRetailID
	 * @return Retailer Location ID and Store Identification List based on input
	 *         parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	// public String updateRetailerLocationSetUp(RetailerLocation
	// retailerLocation, int retailerId, long userId) throws
	// ScanSeeWebSqlException;

	/**
	 * This method upload a file containing product Image path.
	 * 'retailID','imagePath'.
	 * 
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String UpdateRetailerUploadLogo(Long retailID, String imageLogoPath, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This method deletes the Product details that has been selected from list.
	 * 
	 * @param RetailID
	 *            ,ProductID
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteProductsFromLocHoldControl(Long retailID, Long productID) throws ScanSeeWebSqlException;

	public String addBatchUploadProductsToStage(ArrayList<RetailProduct> prodList, int manufacturerID, long userId) throws ScanSeeWebSqlException;

	public ArrayList<Rebates> getRebateProductToAssociate(String strRebateName, Long RetailID, int retailerLocId) throws ScanSeeWebSqlException;

	public List<RetailerLocation> getRetailerLocations(long retailerID) throws ScanSeeWebSqlException;

	/**
	 * This DAO method updates location on changes and its status return to
	 * service method.
	 * 
	 * @param locationList
	 *            instance of List<RetailerLocation>
	 * @param userId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @param fileName
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String updateRetailerLocationSetUp(List<RetailerLocation> locationList, long userId, int retailerId, String fileName)
			throws ScanSeeWebSqlException;

	public LocationResult moveLocationListFrmStageToProductionTable(long userId, int retailId, String fileName) throws ScanSeeWebSqlException;

	public ArrayList<PlanInfo> fetchAllPlanList() throws ScanSeeWebSqlException;

	public String savePlan(List<PlanInfo> pInfoList, int manufacturerID, long userId) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will update existing HotDeals for retailer product and
	 * its status return to service method.
	 * 
	 * @param objProdtHotDeal
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String updateRetailerProductHotDeal(HotDealInfo objProdtHotDeal) throws ScanSeeWebSqlException;

	/**
	 * This DAO method return product Hot deals info based on input parameter
	 * and its status return to service method.
	 * 
	 * @param hotDealID
	 *            as request parameter.
	 * @return Hot deals details.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	List<HotDealInfo> getProductHotDealByID(int hotDealID) throws ScanSeeWebSqlException;

	public String addRetRerunRebates(Rebates objRebates) throws ScanSeeWebSqlException;

	public String updateRetRebates(Rebates objRebates) throws ScanSeeWebSqlException;

	public List<Rebates> getRebatesForDisplay(int rebateId) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying all the states and it return to service
	 * method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return states,List of states.
	 */
	ArrayList<State> getAllStates() throws ScanSeeWebSqlException;

	public ArrayList<Retailer> getRetailersForProducts(String productId) throws ScanSeeServiceException, ScanSeeWebSqlException;

	/**
	 * This DAO method return product Hot deals info based on input parameter
	 * and its status return to service layer.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param pdtName
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return SearchResultInfo instance.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	SearchResultInfo getDealsForDisplay(Long userId, String pdtName, int lowerLimit, int recordCount) throws ScanSeeWebSqlException;

	/**
	 * This method will return the Hot deal product details List based on input
	 * search parameter. 'RetailerID','strProductName'.
	 * 
	 * @return Hot deal product details list based on input parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<ProductHotDeal> getRetailerHotDealByRetailID(Long RetailerID, String strProductName) throws ScanSeeWebSqlException;

	public PlanInfo getRetailerDiscount(String discountCode) throws ScanSeeWebSqlException;

	/**
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying list of TimeZones (all standard time like
	 * (Atlantic,Central, Eastern, Mountain, Pacific Standard Time) and its
	 * status return to service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return timeZonelst,List of TimeZones.
	 */
	ArrayList<TimeZones> getAllTimeZones() throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying list of categories and its status return to
	 * service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return categories,List of categories.
	 */
	ArrayList<Category> getAllBusinessCategory() throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying list of cities and its status return to
	 * service method.
	 * 
	 * @param lUserID
	 *            as request parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return cities,List of cities.
	 */
	ArrayList<City> getHdPopulationCenters(Long lUserID) throws ScanSeeWebSqlException;

	public String batchUploadProductsFromStageToProd(Long userId, int retailId, String fileName, Integer logId) throws ScanSeeWebSqlException;

	public List<Retailer> fetchUploadPreviewList(Long retailerID, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying account type (ACH Bank information/Credit
	 * Card) and it return to service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return arAccountTypeList,List of AccountType.
	 */
	ArrayList<AccountType> getAllAcountType() throws ScanSeeWebSqlException;

	/**
	 * This DAO method delete all the locations in stage table and its status
	 * return to service method.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param retailId
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String deleteRetailerLocationStageTable(int retailId, Long userId) throws ScanSeeWebSqlException;

	public ArrayList<Rebates> getRebateProductImage(String productName, Long retailID) throws ScanSeeWebSqlException;

	public ArrayList<Coupon> getCouponImagePath(Long couponID) throws ScanSeeWebSqlException;

	public List<Rebates> getProductInfo(int rebateId) throws ScanSeeWebSqlException;

	/**
	 * The DAO method return if product name is empty then display product image
	 * path and its status return to service method.
	 * 
	 * @param prodName
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @return prodList,List of products.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	ArrayList<HotDealInfo> getProdDealDetails(String prodName, Long retailID) throws ScanSeeWebSqlException;

	public ArrayList<Coupon> getProducts(String productName) throws ScanSeeWebSqlException;

	public SearchResultInfo getRetailLocProd(String searchParameter, int lowerLimit, int retailID, String retailLocationID, int recordCount)
			throws ScanSeeWebSqlException;

	/**
	 * The DAO method will add one location details and its status return to
	 * service method.
	 * 
	 * @param storeInfoVO
	 *            StoreInfoVO instance as request parameter
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String addLocation(StoreInfoVO storeInfoVO) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for displaying all the business categories and it return
	 * to service method.
	 * 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 * @return Categories,List of categories.
	 */
	ArrayList<Category> getAllRetailerProfileBusinessCategory() throws ScanSeeWebSqlException;

	/**
	 * This DAO method will return list of Retailer Locations/fetch the retailer
	 * location based on input parameter and its status return to service
	 * method.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @param storeIdentification
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	SearchResultInfo getBatchLocationList(int retailerId, long userId, int lowerLimit, String storeIdentification) throws ScanSeeWebSqlException;

	/**
	 * The DAO method will update retailer multiple locations and its status
	 * return to service method.
	 * 
	 * @param locationList
	 *            instance of List<RetailerLocation>.
	 * @param userId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String saveLocationList(final List<RetailerLocation> locationList, long userId, final int retailerId) throws ScanSeeWebSqlException;

	public String duplicateStoreIdentityCheck(int retailId, String storeIdentification) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will delete one retailer location from manage Location
	 * grid and its status return to service method.
	 * 
	 * @param retailLocationId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String deleteBatchLocationList(String retailLocationId) throws ScanSeeWebSqlException;

	/**
	 * This method will query database and return the retailer created custom
	 * pages list.
	 * 
	 * @param retailerId
	 * @param userId
	 * @return SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getCustomPageList(int retailerId, String searchKey, int currentPage, int recordCount, Boolean isBand) throws ScanSeeWebSqlException;

	public RetailerCustomPage getQRcodeForPage(Long pageId, int retailerId, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This method will query database and return the retailer created custom
	 * pages list.
	 * 
	 * @param retailerId
	 * @param userId
	 * @return SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public ArrayList<ContactType> getAllContactTypes() throws ScanSeeWebSqlException;

	/**
	 * This method is used to get the rejected records in the location upload
	 * file
	 * 
	 * @param retailerId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<RetailerLocation> getRejectedRecords(int retailerId, Long userId) throws ScanSeeWebSqlException;

	/**
	 * This DAO method return retailer email Id based on input parameter.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @return retailer email Id.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String getUserMailId(int retailerId, Long userId) throws ScanSeeWebSqlException;

	public int insertRetailerCretaedPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException;

	public int insertRetailerPageInfo(RetailerCustomPage retailerCustomPage, Boolean isBand) throws ScanSeeWebSqlException;

	public RetailerCustomPage insertSplOfferPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException;

	public RetailerCustomPage getRetailerPageInfo(Long userId, Long retailerId, Long pageId, Boolean isBand) throws ScanSeeWebSqlException;

	public String updateRetailerCustomPageInfo(RetailerCustomPage retailerCustomPage, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * this method will return retailer name and retailer image
	 * 
	 * @param retailerId
	 * @return retailer name and image
	 * @throws ScanSeeServiceException
	 */
	public Retailer fetchRetaielrInfo(Long retailerId) throws ScanSeeServiceException;

	public String validateRetailerPage(Long retialerId, String retailerLocId, Long pageID, int qrType) throws ScanSeeServiceException;

	public String deleteRetailerPage(Long pageID) throws ScanSeeServiceException;

	/**
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<AppConfiguration> getAppConfigForGeneralImages(String configType) throws ScanSeeWebSqlException;

	/**
	 * This DAO method updates location latitude and longitude fields on changes
	 * in address, state, city and postal code .
	 * 
	 * @param longitude
	 *            as request parameter.
	 * @param latitude
	 *            as request parameter.
	 * @param retailerName
	 *            as request parameter.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	void addLocationCoordinates(String latitude, String longitude, String retailerName) throws ScanSeeWebSqlException;

	public void addCorpAndStoreLocationCoordinates(String latitude, String longitude, String retailerName) throws ScanSeeWebSqlException;

	public void addLocationCoordinatesWithRetailId(String latitude, String longitude, Integer retailerId, String storeId)
			throws ScanSeeWebSqlException;

	/**
	 * This method is used to delete custom page
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteCustomPage(Long pageId, int retailerId,Boolean isBand) throws ScanSeeWebSqlException;

	public String batchUploadDiscardedProducts(ArrayList<RetailProduct> prodList, int retailID, long userId, Integer logId)
			throws ScanSeeWebSqlException;

	public Integer getStageTableLogId(int retailerId, Long userId, String fileName) throws ScanSeeWebSqlException;

	/**
	 * This method is used to get the rejected records in the location upload
	 * file
	 * 
	 * @param retailerId
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<RetailProduct> getRejectedRetProducts(int retailerId, Long userId, Integer logId) throws ScanSeeWebSqlException;

	public String deleteRetailerBatchUploadStageTable(int retailID, long userId) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will get one Retailer Location Advertisement from
	 * Database based on parameter(adsIDs).
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocationAdvertisement> getWelcomePageForDisplay(Long adsIDs) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will update Retailer Location Advertisement Info.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public String updateWelcomePageInfo(RetailerLocationAdvertisement objRetLocAds) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will return list location info from Database based on
	 * parameter(adsSplashID).
	 * 
	 * @param adsSplashID
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocation> getLocationIDForWelcomePage(Long adsIDs) throws ScanSeeWebSqlException;

	/**
	 * This DAO method deletes the welcome page from list.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteWelcomePage(RetailerLocationAdvertisement objRetLocAds) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will create banner page.
	 * 
	 * @param retLocationAds
	 *            instance of RetailerLocationAdvertisement
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String buildBannerAdInfo(RetailerLocationAdvertisement retLocationAds, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will display retailer banner page details based on input
	 * parameter (bannerId) and its status return to service method.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return arAdsList,Retailer Location Advertisement details.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	List<RetailerLocation> getLocationIDForBannerPage(Long adsIDs, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This DAO method deletes the banner page from list.
	 * 
	 * @param objBannerAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteBannerPage(RetailerLocationAdvertisement objBannerAds, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will update existing banner page details based return
	 * based on input parameter (bannerId)and its status return to sservice
	 * method
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String updateBuildBannerInfo(RetailerLocationAdvertisement objRetLocAds, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will display list of created Banner by the retailer or by
	 * searching Banner Names and and its status return to service method.
	 * 
	 * @param lRetailID
	 *            as request parameter.
	 * @param strAdsName
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	SearchResultInfo getBannerAdsByAdsNames(Long lRetailID, String strAdsName, int lowerLimit, int recordCount, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will get one Banner Advertisement info from Database
	 * based on parameter(adsIDs).
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocationAdvertisement> getBannerPageForDisplay(Long adsIDs, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will return list of images to display
	 * 
	 * @return Retailer Image Icon List List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public ArrayList<RetailerImages> getRetailerImageIconsDisplay(String pageType, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This method is used to display the list of special offer pages.
	 * 
	 * @param retailerId
	 * @param objCustomPage
	 * @param currentPage
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getspecialOfferList(int retailerId, RetailerCustomPage objCustomPage, int currentPage, int recordCount) throws ScanSeeWebSqlException;

	public RetailerCustomPage getRetailerSplOfrPageInfo(Long retailerId, Long pageId) throws ScanSeeWebSqlException;

	/**
	 * This dao method is used for updating special offer page.
	 * 
	 * @param retailerCustomPage
	 * @return
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String updateRetailerSplOfrPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException;

	public RetailerCustomPage getRetLocQRcode(Long retLocId, int retailerId, Long userId) throws ScanSeeWebSqlException;

	public Map<String, Map<String, String>> getRetailerDiscountPlans() throws ScanSeeWebSqlException;

	/**
	 * This DAO method will return the all RetailerLocation list based on input
	 * parameter.
	 * 
	 * @param searchKey
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public List<RetailerLocationProduct> getAllRetailerLocationList(Long retailerId, String searchKey) throws ScanSeeWebSqlException;

	/**
	 * This DAO method will return the Product details based on input parameter.
	 * 
	 * @param objRetLocationProduct
	 *            as request parameter.
	 * @return Product Details.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public List<RetailerLocationProduct> getAssocProdToLocationPop(RetailerLocationProduct objRetLocationProduct) throws ScanSeeWebSqlException;

	public String UpdateRetailLocationProduct(final ArrayList<RetailerLocationProductJson> locationJsonList, final Long retailerId)
			throws ScanSeeWebSqlException;

	/**
	 * This DAO method to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param strSortOrder
	 *            as request parameter.
	 * @param strPageID
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String saveReorderList(String strSortOrder, String strPageID, int retailerId, Boolean isBand) throws ScanSeeWebSqlException;

	/**
	 * This method insert the Retailer registration details.
	 * 
	 * @param objRetailerRegistration
	 *            instance of RetailerRegistration.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */

	public RetailerRegistration saveFreeAppSiteRetailer(RetailerRegistration objRetailerRegistration) throws ScanSeeWebSqlException;

	public String saveAppListLocationList(final List<RetailerLocation> locationList) throws ScanSeeWebSqlException;

	public int insertAppListingAboutUsPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException;

	public List<RetailerLocation> retrievRetailLocation(int retailID) throws ScanSeeWebSqlException;

	/**
	 * This DAO method display the product details based on input
	 * parameter(productId) and its status return to service layer.
	 * 
	 * @param objRetLtnProduct
	 *            instance of RetailerLocationProduct.
	 * @return productList, one the products details.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	List<RetailerLocationProduct> getRetailerProductPreview(RetailerLocationProduct objRetLtnProduct) throws ScanSeeWebSqlException;

	public RetailerRegistration finalizeFreeAppSiteRetReg(RetailerRegistration objRetailerRegistration) throws ScanSeeWebSqlException;

	public String validateDuplicateRetailer(Long retailId, int duplicateRetailId, int duplicateRetailLocId) throws ScanSeeWebSqlException;

	public String markAsDuplicateRetailer(Long retailId, Long retailLocId) throws ScanSeeWebSqlException;

	public ArrayList<DropDown> displaySupplierRetailerDemographicsDD() throws ScanSeeWebSqlException;

	/**
	 * This method saves Giveaway page information.
	 * 
	 * @param retailerCustomPage
	 *            as a parameter.
	 * @return Generated PageID.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as DAO exception.
	 */
	public int saveGiveAwayPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException;

	/**
	 * Method to fetch giveaway page details.
	 * 
	 * @param retailId
	 *            as a parameter.
	 * @param pageId
	 *            as a parameter.
	 * @return RetailerCustomPage obj.
	 * @throws ScanSeeWebSqlException
	 *             will be thrown as DAO exception.
	 */
	public RetailerCustomPage getGiveAwayPageInfo(Long retailId, Long pageId) throws ScanSeeWebSqlException;

	/**
	 * Method to update giveaway page details.
	 * 
	 * @param retailerCustomPage
	 *            as parameter
	 * @return Status, Success or Failure
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String updateGiveawayPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeWebSqlException;

	/**
	 * This method is used to display the list of give away promotion pages.
	 * 
	 * @param retailerId
	 * @param searchKey
	 * @param currentPage
	 * @param recordCount
	 * @return SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public SearchResultInfo getGiveAwayList(int retailerId, String searchKey, int currentPage, int recordCount) throws ScanSeeWebSqlException;

	/**
	 * This method is used to delete Give Away page
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return String
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public String deleteGiveAwayPage(Long pageId, int retailerId) throws ScanSeeWebSqlException;

	/**
	 * This method is for displaying the QRCode for give away page.
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public RetailerCustomPage getQRcodeForGAPage(Long pageId, int retailerId) throws ScanSeeWebSqlException;

	/**
	 * This DAO method return all the products that are associated with supplier
	 * hot deals on input parameter(productId) and its status return to service
	 * layer.
	 * 
	 * @param productIds
	 *            as request parameter.
	 * @return productList, All the products.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	List<Product> getPdtInfoForDealCoupon(String productIds, int retailerId) throws ScanSeeWebSqlException;
	
	/**
	 * The DAO method for displaying list of Hot deals by calling DAO.
	 * @param userId as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return Hot deal Names,List of Hot deal Names.
	 */
	ArrayList<HotDealInfo> getAllHotDealsName(Long userId) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to get Hot dealView Report details.
	 * 
	 * @param hotDealId as request parameter.
	 * @param retailerId as request parameter.
	 * @return objHotDeal instance of HotDealInfo.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	HotDealInfo getHotDealViewReport(int hotDealId, Long retailerId, int currentPage, int recordCount) throws ScanSeeWebSqlException;

	public RetailerRegistration findDuplicateRetailer(RetailerRegistration objRetailReg) throws ScanSeeWebSqlException;
	
	
	/**
	 * This DAO method will return list of locations from Database based on
	 * parameter(pageId & retailerId).
	 * 
	 * @param pageId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	public List<RetailerLocation> getLocationIDForAnythingPage(Long pageId,int retailerId) throws ScanSeeWebSqlException;
	
	
	/**
	 * This DAO method to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param strSortOrder
	 *            as request parameter.
	 * @param strPageID
	 *            as request parameter.
	 * @param iRetailId
	 *            as request parameter.
	 * @return success or failure status.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String saveReorderSpecialOfferList(String strSortOrder, String strPageID, int iRetailId) throws ScanSeeWebSqlException;
	
	/**
	 * The DAO method for displaying all the Event categories and it return
	 * to service method.
	 * 
	 * @return Categories,List of event categories.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	ArrayList<Category> getAllEventCategory(Boolean isBand) throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method will return list of Retailer Locations.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @return list of Retailer Locations.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	ArrayList<RetailerLocation> getEventRetailerLocationList(int retailerId) throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method will create/Update Event page.
	 * 
	 * @param event instance of Event.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String addUpdateEventDetail(Event objEvent, Boolean isBand) throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method to get Retailer event page information.
	 * 
	 * @param eventId as a parameter.
	 * @return Event detail.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	Event getEventDetail(Integer eventId, Boolean isBand) throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method will return list of Retailer Events based on input parameter by calling DAO methods.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @param storeIdentification
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	SearchResultInfo getRetailerEventList(int retailerId, int lowerLimit, String eventName, Integer isFundraising, Boolean isBand)
			throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method will return list of event patterns.
	 * 
	 * @return List of event patterns. 
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	List<Event> getEventPatterns() throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method deletes the Event page from list.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String deleteEvent(Integer eventId, Boolean isBand) throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method will display all associated  Retailer Location with that Event.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	List<RetailerLocation> getAllLocationEvent(Integer eventId, int iRetailId) throws ScanSeeWebSqlException;
	
	SearchResultInfo getFundraiserEventList(int retailerId, int lowerLimit, String eventName, Long userId) throws ScanSeeWebSqlException;
	
	List<RetailerLocation> getFundraiserEventLocation(Integer eventId, int iRetailId) throws ScanSeeWebSqlException;
	
	List<Category> getFundraiserDepartmentList(Integer retailId) throws ScanSeeWebSqlException;
	
	List<Category> getFundraiserEventCategory(Integer retailId) throws ScanSeeWebSqlException;
	
	String saveFundraiserDept(Long userId, Integer retailId, String deptName) throws ScanSeeWebSqlException;
	
	String saveUpdateFundraiser(Long userId, Event objEvent) throws ScanSeeWebSqlException;
	
	/**
	 * This DAO method deletes the fundraising Event page from list.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeWebSqlException
	 *             as SQL Exception will be thrown.
	 */
	String deleteFundraiser(Integer eventId) throws ScanSeeWebSqlException;
	
	Event getFundEventDetails(Integer eventId, Integer retailID) throws ScanSeeWebSqlException;
	
	public List<Category> fetchFilters(String filterIds) throws ScanSeeWebSqlException;
	
	public List<Category> fetchSubCategories(String catIds) throws ScanSeeWebSqlException;
	
	public List<Category> fetchAppsiteHome(Integer retailerId) throws ScanSeeWebSqlException;
	
	Category fetchTutorials() throws ScanSeeWebSqlException;
	
	public List<AppSiteDetails> getLogisticsRetailer(AppSiteDetails appSiteDetails) throws ScanSeeWebSqlException ;
	
	public List<AppSiteDetails> displayLogisticsRetailLocations(AppSiteDetails appSiteDetails) throws ScanSeeWebSqlException;
	
	public SearchResultInfo displayBusinessHours(int retailerId, long userId, int lowerLimit, String retailerLocationID) throws ScanSeeWebSqlException;
	
	public String saveBusinessHours(int retailerId, long userId, RetailerLocation businessHoursList, String retailerLocationID) throws ScanSeeWebSqlException;
}
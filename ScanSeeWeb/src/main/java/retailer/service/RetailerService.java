package retailer.service;

import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.pojo.Event;
import common.exception.ScanSeeServiceException;
import common.pojo.AccountType;
import common.pojo.AppSiteDetails;
import common.pojo.Category;
import common.pojo.City;
import common.pojo.ContactType;
import common.pojo.Coupon;
import common.pojo.DropDown;
import common.pojo.HotDealInfo;
import common.pojo.LocationResult;
import common.pojo.PlanInfo;
import common.pojo.Product;
import common.pojo.Rebates;
import common.pojo.RetailProduct;
import common.pojo.Retailer;
import common.pojo.RetailerCustomPage;
import common.pojo.RetailerImages;
import common.pojo.RetailerLocation;
import common.pojo.RetailerLocationAdvertisement;
import common.pojo.RetailerLocationProduct;
import common.pojo.RetailerProfile;
import common.pojo.RetailerRegistration;
import common.pojo.RetailerUploadLogoInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.State;
import common.pojo.StoreInfoVO;
import common.pojo.TimeZones;
import common.pojo.Tutorial;
import common.pojo.Users;

/**
 * service methods of Retailer module.
 * 
 * @author Created by SPAN.
 */
public interface RetailerService
{
	/**
	 * This service method save Retailer register details by calling DAO method.
	 * 
	 * @param retailerRegistrationInfo
	 *            instance of RetailerRegistration.
	 * @param strLogoImage
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String processRetailerProfileDetails(RetailerRegistration retailerRegistrationInfo, String strLogoImage) throws ScanSeeServiceException;

	/**
	 * This service method return all the locations for that retailer based on
	 * input parameter by calling DAO method.
	 * 
	 * @param retailID
	 *            as request parameter.
	 * @return locationList, All the retailer locations.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	ArrayList<RetailerLocation> getRetailerLocations(Long retailID) throws ScanSeeServiceException;

	public String insertCouponInfo(Coupon coupon) throws ScanSeeServiceException;

	public Coupon fetchCouponInfo(Long couponID) throws ScanSeeServiceException;

	public SearchResultInfo getCouponSearchResult(Long retailID, SearchForm objForm, int lowerLimit, int iRecordCount) throws ScanSeeServiceException;

	/**
	 * This service method will return all list of Retailer Location/Store
	 * Identification by add new location, upload multiple locations with the
	 * help of CSV file templates and calling DAO method.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	SearchResultInfo getRetailerLocationList(int retailerId, Long userId, int lowerLimit) throws ScanSeeServiceException;

	/**
	 * This service method updates location on changes and calling DAO method.
	 * 
	 * @param retailerLocation
	 *            instance of RetailerLocation
	 * @param userId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @param fileName
	 *            as request parameter.
	 * @param objUploadlocation
	 *            instance of SearchResultInfo
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String updateLocationSetUp(RetailerLocation retailerLocation, long userId, int retailerId, String fileName, SearchResultInfo objUploadlocation, HttpSession session)
			throws ScanSeeServiceException;

	public ArrayList<Product> getProdToAssociateCoupon(String searchKey, Long RetailerLocID, Long RetailID) throws ScanSeeServiceException;

	public String updateCouponInfo(Coupon coupon) throws ScanSeeServiceException;

	public String processRetailerUploadLogo(RetailerUploadLogoInfo retailerUploadLogoInfo, String realPath, Users loginUser, Boolean isBand)
			throws ScanSeeServiceException;

	/**
	 * This service method create new banner page by calling DAO method.
	 * 
	 * @param retLocationAdvertisement
	 *            instance of RetailerLocationAdvertisement
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String insertBannerAdInfo(RetailerLocationAdvertisement retLocationAdvertisement) throws ScanSeeServiceException;

	public SearchResultInfo getRetailerAdsByAdsNames(Long objRetailID, SearchForm objForm, int lowerLimit, int recordCount)
			throws ScanSeeServiceException;

	/**
	 * This service method display Retailer register details by calling DAO
	 * method.
	 * 
	 * @param user
	 *            instance of Users.
	 * @return RetailerProfile instance.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	RetailerProfile getRetailerProfile(Users user, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method update Retailer register details by calling DAO
	 * method.
	 * 
	 * @param retailerProfile
	 *            instance of RetailerProfile.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String updateProfile(RetailerProfile retailerProfile) throws ScanSeeServiceException;

	public SearchResultInfo searchProducts(SearchForm objForm, int lowerLimit, int rowCount) throws ScanSeeServiceException;

	public List<RetailerLocation> getRetailerLocation(Long retailID) throws ScanSeeServiceException;

	public String associateUpdateRetProd(RetailerLocationProduct objRetailLoctnProduct) throws ScanSeeServiceException;

	/**
	 * This service method upload multiple locations with the help of CSV file
	 * templates and calling DAO method.
	 * 
	 * @param retailerLocation
	 *            instance of RetailerLocation.
	 * @param userId
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String saveBatchLocation(RetailerLocation retailerLocation, Long userId, int retailID) throws ScanSeeServiceException;

	public SearchResultInfo getRetailerByRebateName(Long retailerId, SearchForm rebateName, int lowerLimit) throws ScanSeeServiceException;

	public LocationResult moveDataFromstgTOProduction(RetailerLocation retailerLocation, long userId, int retailerId, String fileName)
			throws ScanSeeServiceException;

	public String addRetailerRebates(Rebates rebatesVo) throws ScanSeeServiceException;

	public String saveBatchFile(RetailProduct retailProduct, int retailID, long userId) throws ScanSeeServiceException;

	/**
	 * The service method for displaying all the states and it will calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return states,List of states.
	 */
	ArrayList<State> getAllStates() throws ScanSeeServiceException;

	/**
	 * This service method will add one new HotDeals for retailer by calling DAO
	 * method.
	 * 
	 * @param objHotDeal
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String retailerAddHotDeal(HotDealInfo objHotDeal) throws ScanSeeServiceException;

	public ArrayList<Retailer> getRetailersForProducts(String productId) throws ScanSeeServiceException;

	/**
	 * This service method return product Hot deals info based on input
	 * parameter by calling DAO method.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param pdtName
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return SearchResultInfo instance.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	SearchResultInfo getDealsForDisplay(Long userId, String pdtName, int lowerLimit, int recordCount) throws ScanSeeServiceException;

	/**
	 * This service method return product Hot deals info based on input
	 * parameter by calling DAO method.
	 * 
	 * @param dealID
	 *            as request parameter.
	 * @return Hot deals details.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<HotDealInfo> getHotDealByID(int dealID) throws ScanSeeServiceException;

	public List<Rebates> getRebateProductToAssociate(String strRebateName, Long retailID, int retailerLocId) throws ScanSeeServiceException;

	public List<RetailerLocation> getRebRetailerLocations(long retailerID) throws ScanSeeServiceException;

	// public ArrayList<PlanInfo> getAllPlanList();

	public ArrayList<PlanInfo> getAllPlanList() throws ScanSeeServiceException;

	public String savePlanInfo(List<PlanInfo> pInfoList, int manufacturerID, long userId) throws ScanSeeServiceException;

	public String addRetRerunRebates(Rebates objRebates) throws ScanSeeServiceException;

	public String updateRetRebates(Rebates objRebates) throws ScanSeeServiceException;

	public List<Rebates> getRebatesForDisplay(int rebateId) throws ScanSeeServiceException;

	/**
	 * This service method will display the HotDeals deals by passing HotDealId
	 * as input parameter by call service and DAO methods.
	 * 
	 * @param dealID
	 *            as request parameter.
	 * @return HotDealList, All the HotDeals.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<HotDealInfo> getProductHotDealByID(int dealID) throws ScanSeeServiceException;

	/**
	 * This service method will update existing HotDeals for retailer product by
	 * calling DAO method.
	 * 
	 * @param objHotDealInfo
	 *            instance of HotDealInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String updateRetailerProductHotDeal(HotDealInfo objHotDealInfo) throws ScanSeeServiceException;

	public PlanInfo getDiscountValue(String discountCode) throws ScanSeeServiceException;

	/**
	 * The service method for displaying list of TimeZones (all standard time
	 * like (Atlantic,Central, Eastern, Mountain, Pacific Standard Time) by
	 * calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return TimeZones,List of TimeZones.
	 */
	ArrayList<TimeZones> getAllTimeZones() throws ScanSeeServiceException;

	/**
	 * The service method for displaying list of categories by calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return categories,List of categories.
	 */
	ArrayList<Category> getAllBusinessCategory() throws ScanSeeServiceException;

	/**
	 * The service method for displaying list of cities by calling DAO.
	 * 
	 * @param lUserID
	 *            as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return cities,List of cities.
	 */
	ArrayList<City> getHdPopulationCenters(Long lUserID) throws ScanSeeServiceException;

	public String batchUploadProductsFromStageToProd(Long userId, int retailId, String fileName, Integer logId) throws ScanSeeServiceException;

	/**
	 * This service method deletes the Product Product associated with Retailer
	 * Location from list.
	 * 
	 * @param RetailID
	 *            ,ProductID
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteProductsFromLocHoldControl(Long retailID, Long productID) throws ScanSeeServiceException;

	/**
	 * The service method for displaying account type (ACH Bank
	 * information/Credit Card) and it will calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return AccountType,List of AccountType.
	 */
	ArrayList<AccountType> getAllAcountType() throws ScanSeeServiceException;

	public List<Retailer> fetchRetailerPreviewList(Long retailerID, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method delete all the locations in stage table by calling
	 * DAO method.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param retailId
	 *            as request parameter.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String deleteRetailerLocationStageTable(int retailId, Long userId) throws ScanSeeServiceException;

	public ArrayList<Rebates> getRebateProductImage(String productName, Long RetailID) throws ScanSeeServiceException;

	public ArrayList<Coupon> fetchCouponImage(Long couponID) throws ScanSeeServiceException;

	public List<Rebates> fetchProductInf(int rebateId) throws ScanSeeServiceException;

	/**
	 * The service method return if product name is empty then display product
	 * image path by calling DAO.
	 * 
	 * @param prodName
	 *            as request parameter.
	 * @param retailID
	 *            as request parameter.
	 * @return ProductList,List of products.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	ArrayList<HotDealInfo> getProdDetails(String prodName, Long retailID) throws ScanSeeServiceException;

	public ArrayList<Coupon> getProdCoupon(String productName) throws ScanSeeServiceException;

	/**
	 * The service method will add one location details by calling DAO methods.
	 * 
	 * @param storeInfoVO
	 *            StoreInfoVO instance as request parameter
	 * @param loginUser
	 *            Users instance as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String addLocation(StoreInfoVO storeInfoVO, Users loginUser) throws ScanSeeServiceException;

	public SearchResultInfo getRetailLocProd(String searchParameter, int lowerLimit, int retailID, String retailLocationID, int recordCount)
			throws ScanSeeServiceException;

	/**
	 * The service method for displaying all the business categories and it will
	 * calling DAO.
	 * 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return categories,List of categories.
	 */
	ArrayList<Category> getAllRetailerProfileBusinessCategory() throws ScanSeeServiceException;

	/**
	 * This service method will return list of Retailer Locations/fetch the
	 * retailer location based on input parameter by calling DAO methods.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @param storeIdentification
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	SearchResultInfo getRetailerBatchLocationList(int retailerId, Long userId, int lowerLimit, String storeIdentification)
			throws ScanSeeServiceException;

	/**
	 * The service method will update retailer multiple locations by calling DAO
	 * methods.
	 * 
	 * @param retailerLocation
	 *            instance of RetailerLocation
	 * @param userId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @param objBeforelocation
	 *            instance of SearchResultInfo
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String saveLocationSetUpList(RetailerLocation retailerLocation, long userId, int retailerId, SearchResultInfo objBeforelocation, HttpSession session)
			throws ScanSeeServiceException;

	public String duplicateStoreCheck(int retailId, String storeIdentification) throws ScanSeeServiceException;

	/**
	 * This service method will delete one retailer location from manage
	 * Location grid by calling DAO methods.
	 * 
	 * @param retailLocationId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String deleteBatchLocation(String retailLocationId) throws ScanSeeServiceException;

	public ArrayList<ContactType> getAllContactTypes() throws ScanSeeServiceException;

	/**
	 * This service method is used to get the rejected records in the location
	 * upload file
	 * 
	 * @param retailerId
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public ArrayList<RetailerLocation> getRejectedRecords(int retailerId, Long userId) throws ScanSeeServiceException;

	/**
	 * This service method is used sending the mail with an attachment.
	 * 
	 * @param fileName
	 *            attached file path.
	 * @param mailId
	 *            mail id of the user whom to send.
	 * @param strLocationContent
	 *            content to send.
	 * @return boolean success or failure that the mail had sent or not
	 */
	boolean sendMailWithAttachment(String fileName, String mailId, String strLocationContent);

	/**
	 * This service method return retailer email Id based on input parameter by
	 * calling DAO method.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String getUserMailId(int retailerId, Long userId) throws ScanSeeServiceException;

	public String createRetailerPage(RetailerCustomPage retailerCustomPage, Boolean isBand) throws ScanSeeServiceException;

	public String createRetailerCreatedPage(RetailerCustomPage retailerCustomPage) throws ScanSeeServiceException;

	/**
	 * This service method will return list of Retailer pages,Retailer created
	 * pages and Special Offers
	 * 
	 * @param retailerId
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public SearchResultInfo getCusomtPageList(int retailerId, String searchKey, int currentPage, int recordCount, Boolean isBand) throws ScanSeeServiceException;

	public RetailerCustomPage getQrCodeDetails(Long pageId, int retailerId, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method will return list of Retailer pages,Retailer created
	 * pages and Special Offers
	 * 
	 * @param retailerId
	 * @param userId
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */

	public String createSplOfferPage(RetailerCustomPage retailerCustomPage) throws ScanSeeServiceException;

	public RetailerCustomPage getRetailerPageInfo(Long userId, Long retailerId, Long pageId, Boolean isBand) throws ScanSeeServiceException;

	public String updateRetailerCustomPageInfo(RetailerCustomPage retailerCustomPage, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method will return retailer name and retailer image
	 * 
	 * @param retailerId
	 * @return retailer name and image
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public Retailer fetchRetaielrInfo(Long retailerId) throws ScanSeeServiceException;

	public String validateRetailerPage(Long retialerId, String retailerLocId, Long pageID, int qrType) throws ScanSeeServiceException;

	public String deleteRetailerPage(Long pageID) throws ScanSeeServiceException;

	/**
	 * This service method is used to domain Name for Application configuration.
	 * 
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String getDomainName() throws ScanSeeServiceException;

	/**
	 * This service method updates location latitude and longitude fields on
	 * changes in address, state, city and postalcode and calling DAO method.
	 * 
	 * @param locationCoordinates
	 *            as request parameter.
	 * @param retailerName
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String addLocationCoordinates(String locationCoordinates, String retailerName) throws ScanSeeServiceException;

	public String addCorpAndStoreLocationCoordinates(String locationCoordinates, String retailerName) throws ScanSeeServiceException;

	public String processRetailerCroppedLogo(CommonsMultipartFile retailerUploadLogoInfo, String realPath, Users loginUser, int x, int y, int w, int h, Boolean isBand)
			throws ScanSeeServiceException;

	/**
	 * This service method is used for deleting custom page.
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteCustomPage(Long pageId, int retailerId,Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method will return the Retailer Location Advertisement List
	 * based on input parameter.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<RetailerLocationAdvertisement> getWelcomePageForDisplay(Long adsIDs) throws ScanSeeServiceException;

	/**
	 * This service method will update Welcome page Advertisement Info.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String updateWelcomePageInfo(RetailerLocationAdvertisement objRetLocAds) throws ScanSeeServiceException;

	/**
	 * This service method will return list of location from Database based on
	 * parameter(adsSplashID).
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<RetailerLocation> getLocationIDForWelcomePage(Long adsIDs) throws ScanSeeServiceException;

	/**
	 * This service method deletes the welcome page from list.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteWelcomePage(RetailerLocationAdvertisement objRetLocAds) throws ScanSeeServiceException;

	/**
	 * This service method will create banner page.
	 * 
	 * @param retLocationAds
	 *            instance of RetailerLocationAdvertisement
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String buildBannerAdInfo(RetailerLocationAdvertisement retLocationAds, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method will display retailer banner page details based on
	 * input parameter (bannerId) by calling DAO method.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<RetailerLocation> getLocationIDForBannerPage(Long adsIDs, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method deletes the banner page from list.
	 * 
	 * @param objBannerAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteBannerPage(RetailerLocationAdvertisement objBannerAds, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method will update existing banner page details based return
	 * based on input parameter (bannerId) by calling DAO method.
	 * 
	 * @param objRetLocAds
	 *            instance of RetailerLocationAdvertisement.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String updateBuildBannerInfo(RetailerLocationAdvertisement objRetLocAds, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method will display list of created Banner by the retailer
	 * or by searching Banner Names.
	 * 
	 * @param lRetailID
	 *            as request parameter.
	 * @param objForm
	 *            as instance of SearchResultInfo.
	 * @param lowerLimit
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	SearchResultInfo getBannerAdsByAdsNames(Long lRetailID, SearchForm objForm, int lowerLimit, int recordCount, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method will return the Banner Advertisement info based on
	 * input parameter.
	 * 
	 * @param adsIDs
	 *            as request parameter.
	 * @return Retailer Location Advertisement List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<RetailerLocationAdvertisement> getBannerPageForDisplay(Long adsIDs, Boolean isBand) throws ScanSeeServiceException;

	public String processRetailerCroppedImage(CommonsMultipartFile retailerUploadLogoInfo, String realPath, Users loginUser, int x, int y, int w,
			int h, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method is used for fetching the image icons to display in
	 * link page.
	 * 
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public ArrayList<RetailerImages> getRetailerImageIconsDisplay(String pageType, Boolean isBand) throws ScanSeeServiceException;

	/**
	 * This service method is used to display the list of special offer pages.
	 * 
	 * @param retailerId
	 * @param RetailerCustomPage objCustomPage
	 * @param currentPage
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public SearchResultInfo getspecialOfferList(int retailerId, RetailerCustomPage objCustomPage, int currentPage, int recordCount) throws ScanSeeServiceException;

	public void addLocationCoordinatesWithRetailID(String locationCoordinates, Integer retailID, String storeId) throws ScanSeeServiceException;

	public RetailerCustomPage getRetailerSplOfrPageInfo(Long retailerId, Long pageId) throws ScanSeeServiceException;

	/**
	 * This service method is used for updating special offer page.
	 * 
	 * @param retailerCustomPage
	 * @return
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String updateRetailerSplOfrPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeServiceException;

	/**
	 * The service method will get Quick response code(QR code)for retailer
	 * Location based on input parameter by calling DAO methods.
	 * 
	 * @param retLocID
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @return instance of RetailerCustomPage.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	RetailerCustomPage getLocQrCodeDetails(Long retLocID, int retailerId, Long userId) throws ScanSeeServiceException;

	public String processRetailerUploadLogoForMinDimension(RetailerUploadLogoInfo retailerUploadLogoInfo, String realPath, Users loginUser,
			BufferedImage img) throws ScanSeeServiceException;

	public Map<String, Map<String, String>> getRetailerDiscountPlans() throws ScanSeeServiceException;

	/**
	 * This service method will return the all RetailerLocation list based on
	 * input parameter.
	 * 
	 * @param searchKey
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<RetailerLocationProduct> getAllRetailerLocationList(Long retailerId, String searchKey) throws ScanSeeServiceException;

	/**
	 * This service method will return the Product details based on input
	 * parameter.
	 * 
	 * @param objRetLocationProduct
	 *            as request parameter.
	 * @return Product Details.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public List<RetailerLocationProduct> getAssocProdToLocationPop(RetailerLocationProduct objRetLocationProduct) throws ScanSeeServiceException;

	/**
	 * This service method will update Location SetUp List Info.
	 * 
	 * @param objRetLocationProduct
	 *            instance of RetailerLocationProduct.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String updateRetailerProductList(RetailerLocationProduct objRetLocationProduct) throws ScanSeeServiceException;

	/**
	 * This service method to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param strSortOrder
	 *            as request parameter.
	 * @param strPageID
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String saveReorderList(String strSortOrder, String strPageID, int retailerId, Boolean isBand) throws ScanSeeServiceException;

	public RetailerRegistration registerFreeAppListRetailer(RetailerRegistration retailerRegistrationInfo, String strLogoImage)
			throws ScanSeeServiceException;

	public List<RetailerLocation> saveAppListLocation(RetailerLocation retailerLocation) throws ScanSeeServiceException;

	public String createAppListingAboutUsPage(RetailerCustomPage retailerCustomPage) throws ScanSeeServiceException;

	public List<RetailerLocation> retrievRetailLocation(int retailID) throws ScanSeeServiceException;

	/**
	 * This service method display the product details based on input
	 * parameter(productId) by calling DAO method.
	 * 
	 * @param objRetLtnProduct
	 *            instance of RetailerLocationProduct.
	 * @return productList, one the products details.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<RetailerLocationProduct> getRetailerProductPreview(RetailerLocationProduct objRetLtnProduct) throws ScanSeeServiceException;

	public RetailerRegistration finalizeFreeAppSiteRetReg(RetailerRegistration retailerRegistrationInfo, String strLogoImage)
			throws ScanSeeServiceException;

	public String validateDuplicateRetailer(Long retailId, int duplicateRetailId, int duplicateRetailLocId) throws ScanSeeServiceException;

	public String markAsDuplicateRetailer(Long retailId, Long retailLocId) throws ScanSeeServiceException;

	public ArrayList<DropDown> displaySupplierRetailerDemographicsDD() throws ScanSeeServiceException;

	/**
	 * This service method save Giveaway page information.
	 * 
	 * @param retailerCustomPage
	 *            as a parameter.
	 * @return QR code image path.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String createGiveAwayPage(RetailerCustomPage retailerCustomPage) throws ScanSeeServiceException;

	/**
	 * This service method to get Giveaway page information.
	 * 
	 * @param retailerId
	 *            as a parameter.
	 * @param pageId
	 *            as a parameter.
	 * @return RetailerCustomPage obj.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public RetailerCustomPage getGiveAwayPageInfo(Long retailerId, Long pageId) throws ScanSeeServiceException;

	/**
	 * This service method to Update Giveaway page information.
	 * 
	 * @param retailerCustomPage
	 *            as a parameter.
	 * @return QR code image path.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String updateGiveawayPageInfo(RetailerCustomPage retailerCustomPage) throws ScanSeeServiceException;

	/**
	 * This service method is used to display the list of give away promotion
	 * pages.
	 * 
	 * @param retailerId
	 * @param searchKey
	 * @param currentPage
	 * @param recordCount
	 * @return SearchResultInfo
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public SearchResultInfo getGiveAwayList(int retailerId, String searchKey, int currentPage, int recordCount) throws ScanSeeServiceException;

	/**
	 * This service method is used for deleting Giveaway page.
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return String
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	public String deleteGiveAwayPage(Long pageId, int retailerId) throws ScanSeeServiceException;

	/**
	 * This method is for displaying the QRCode for give away page.
	 * 
	 * @param pageId
	 * @param retailerId
	 * @return RetailerCustomPage
	 * @throws ScanSeeServiceException
	 */
	public RetailerCustomPage getGAQrCodeDetails(Long pageId, int retailerId) throws ScanSeeServiceException;

	/**
	 * This service method return all the products that are associated with
	 * Retailer hot deals on input parameter(productId) by calling DAO method.
	 * 
	 * @param productId as request parameter.
	 * @return productList, All the products.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<Product> getPdtInfoForDealCoupon(String productId, int retailerId) throws ScanSeeServiceException;

	
	/**
	 * The service method for displaying list of Hot deals by calling DAO.
	 * @param userId as request parameter.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @return Hot deal Names,List of Hot deal Names.
	 */
	ArrayList<HotDealInfo> getAllHotDealsName(Long userId) throws ScanSeeServiceException;
	
	/**
	 * This service method is used to get Hot dealView Report details.
	 * 
	 * @param hotDealId as request parameter.
	 * @param retailerId as request parameter.
	 * @return objHotDeal instance of HotDealInfo.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	HotDealInfo getHotDealViewReport(int hotDealId, Long retailerId, int currentPage, int recordCount) throws ScanSeeServiceException;


	public RetailerRegistration findDuplicateRetailer(RetailerRegistration objRetailerRegistration) throws ScanSeeServiceException;	

	/**
	 * This service method will return the Retailer Location List
	 * based on input parameter.
	 * 
	 * @param pageId
	 *            as request parameter.
	 * @param retailerId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeServiceException
	 *             will be thrown.
	 */
	public List<RetailerLocation> getLocationIDForAnythingPage(Long pageId,int retailerId) throws ScanSeeServiceException;
	
	/**
	 * This service method to drag and drop/reorder/renumber table row
	 * functionality.
	 * 
	 * @param strSortOrder
	 *            as request parameter.
	 * @param strPageID
	 *            as request parameter.
	 * @param iRetailId
	 *            as request parameter.
	 * @return success or failure status.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String saveReorderSpecialOfferList(String strSortOrder, String strPageID, int iRetailId) throws ScanSeeServiceException;
	
	/**
	 * The service method for displaying all the Event categories and it will
	 * calling DAO.
	 * 
	 * @return categories,List of event categories.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	ArrayList<Category> getAllEventCategory(Boolean isBand) throws ScanSeeServiceException;
	
	/**
	 * This service method will return list of Retailer Locations/fetch the
	 * retailer location based on input parameter by calling DAO methods.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @return list of Retailer Locations.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	ArrayList<RetailerLocation> getEventRetailerLocationList(int retailerId) throws ScanSeeServiceException;
	
	/**
	 * This service method will create/Update Event page.
	 * 
	 * @param event
	 *            instance of Event
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String addUpdateEventDetail(Event objEvent, Boolean isBand) throws ScanSeeServiceException;
	
	/**
	 * This service method to get Retailer event page information.
	 * 
	 * @param eventId as a parameter.
	 * @return Event detail.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 * @throws ParseException 
	 */
	Event getEventDetail(Integer eventId, Boolean isBand) throws ScanSeeServiceException, ParseException;

	
	/**
	 * This service method will return list of Retailer Events based on input parameter by calling DAO methods.
	 * 
	 * @param retailerId
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @param lowerLimit
	 *            as request parameter.
	 * @param storeIdentification
	 *            as request parameter.
	 * @return searchResult instance of SearchResultInfo
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	SearchResultInfo getRetailerEventList(int retailerId, int lowerLimit, String eventName, Integer isFundraising, Boolean isBand)
			throws ScanSeeServiceException;
	
	/**
	 * This service method will return list of event patterns.
	 * 
	 * @return List of event patterns. 
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<Event> getEventPatterns() throws ScanSeeServiceException;
	
	/**
	 * This service method deletes the Event page from list.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	String deleteEvent(Integer eventId, Boolean isBand) throws ScanSeeServiceException;
	
	/**
	 * This service method will display all associated  Retailer Location with that Event.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return Retailer Location List.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	List<RetailerLocation> getAllLocationEvent(Integer eventId, int iRetailId) throws ScanSeeServiceException;
	
	SearchResultInfo getFundraiserEventList(int retailerId, int lowerLimit, String eventName, Long userId) throws ScanSeeServiceException;
	
	List<RetailerLocation> getFundraiserEventLocation(Integer eventId, int iRetailId) throws ScanSeeServiceException;
	
	List<Category> getFundraiserDepartmentList(Integer retailId) throws ScanSeeServiceException;
	
	ArrayList<Category> getFundraiserEventCategory(Integer retailId) throws ScanSeeServiceException;
	
	String saveFundraiserDept(Long userId, Integer retailId, String deptNamee) throws ScanSeeServiceException;

	String saveUpdateFundraiser(Long userId, Event objEvent) throws ScanSeeServiceException;
	
	/**
	 * This service method deletes the fundraising Event page from list.
	 * 
	 * @param eventId
	 *            as request parameter.
	 * @return success or failure.
	 * @throws ScanSeeServiceException
	 *             will be thrown as service exception.
	 */
	
	String deleteFundraiser(Integer eventId) throws ScanSeeServiceException;
	
	Event getFundEventDetails(Integer eventID, Integer retailID) throws ScanSeeServiceException;
	
	public List<Category> fetchFilters(String filterIds) throws ScanSeeServiceException;
	
	public List<Category> fetchSubCategories(String catIds) throws ScanSeeServiceException;
	
	public List<Category> fetchAppsiteHome(Integer retailerId) throws ScanSeeServiceException;
	
	public Category fetchTutorials() throws ScanSeeServiceException;
	public List<AppSiteDetails> getLogisticsRetailer(AppSiteDetails appSiteDetails) throws ScanSeeServiceException;
	
	public List<AppSiteDetails> displayLogisticsRetailLocations(AppSiteDetails appSiteDetails) throws ScanSeeServiceException;
	
	public SearchResultInfo getBusinessHours(int retailerId, Long userId, int lowerLimit, String retailerLocationID) throws ScanSeeServiceException;
	
	public String saveBusinessHours(int retailerId, Long userId, RetailerLocation businessHoursList, String retailerLocationID)  throws ScanSeeServiceException;
}

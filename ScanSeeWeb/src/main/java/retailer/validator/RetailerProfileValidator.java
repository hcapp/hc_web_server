package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.RetailerRegistration;

/**
 * validation used in Retailer Registration Screen.
 * 
 * @author Created by SPAN.
 */
public class RetailerProfileValidator implements Validator
{
	/**
	 * Variable contactPhone declared as String.
	 */
	private final String contactPhone = "contactPhone";

	/**
	 * Variable contactPhone declared as String.
	 */
	private final String contactPhoneNo = "contactPhoneNo";

	/**
	 * Variable contactEmail declared as String.
	 */
	private final String contactEmail = "contactEmail";

	/**
	 * Variable city declared as String.
	 */
	private final String city = "city";
	
	/**
	 * Variable city declared as String.
	 */
	private final String state = "state";

	/**
	 * Variable terms declared as String.
	 */
	private final String terms = "terms";

	/**
	 * Getting the RetailerRegistrationInfo Instance.
	 * 
	 * @param target
	 *            as request parameter.
	 * @return retailerRegistrationInfo instance of RetailerRegistration
	 */
	public final boolean supports(Class<?> target)
	{
		return RetailerRegistration.class.isAssignableFrom(target);
	}

	/**
	 * This method validate RetailerRegistration screen.
	 * 
	 * @param arg0
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors.
	 * @param status
	 *            as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DUPLICATE_USER))
		{
			errors.reject("userName.exist");
		}
		if (status.equals(ApplicationConstants.DUPLICATE_RETAILERNAME))
		{
			errors.reject("duplicate.retailer");
		}
		/*
		 * if (status.equals(ApplicationConstants.EMAIL)) {
		 * errors.reject("email.match"); }
		 */
		if (status.equals(ApplicationConstants.INVALIDCORPPHONE))
		{
			errors.rejectValue(contactPhone, "invalid.Phonenumber");
		}
		if (status.equals(ApplicationConstants.INVALIDCONTPHONE))
		{
			errors.rejectValue(contactPhoneNo, "invalid.Phonenumber");
		}
		if (status.equals(ApplicationConstants.INVALIDEMAIL))
		{
			errors.rejectValue(contactEmail, "invalid.email");
		}
		if (status.equals(ApplicationConstants.CITY))
		{
			errors.rejectValue(city, "city.enter");
		}
		if (status.equals(ApplicationConstants.TERM))
		{
			errors.rejectValue(terms, "select.termcondition");
		}
		if (status.equals(ApplicationConstants.BUSINESSCATEGORY))
		{
			errors.rejectValue("bCategory", "select.category");
		}
		if (status.equals(ApplicationConstants.WEBURL))
		{
			errors.rejectValue("webUrl", "invalid.url");
		}

		if (status.equals(ApplicationConstants.DUPLICATE_BUSINESSNAME))
		{
			errors.reject("duplicate.retailer");
		}

		if (status.equals(ApplicationConstants.GEOERROR))
		{
			errors.rejectValue("address1", "invalid.geomessge");
		}
		
		if(status.equals(ApplicationConstants.LATITUDE)){
			errors.rejectValue("retailerLocationLatitude", "latitude.required");
		}
		if(status.equals(ApplicationConstants.LONGITUDE)){
			errors.rejectValue("retailerLocationLongitude", "longitude.required");
		}
		/*
		 * if (status.equals(ApplicationConstants.INVALID_CAPTCHA)) {
		 * errors.rejectValue("captch", "captcha.invalid"); }
		 */
	}

	/**
	 * This method validate RetailerRegistration screen.
	 * 
	 * @param target
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors..
	 */
	public final void validate(Object target, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retailerName", "retailerName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address1", "corpAdderess");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, city, "city.enter");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, state, "state.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "postalCode.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactPhone, "contactPhone.required");
		/*
		 * ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bCategory",
		 * "bCategory"); ValidationUtils.rejectIfEmptyOrWhitespace(errors,
		 * "webUrl", "webUrl.required");
		 * ValidationUtils.rejectIfEmptyOrWhitespace
		 * (errors,"address2","address2.required");
		 * ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retypeEmail",
		 * "retypeEmail.required"); if (null == registVo.getCaptch() ||
		 * registVo.getCaptch().equals("")) {
		 * ValidationUtils.rejectIfEmpty(errors, "captch", "captcha.invalid"); }
		 * ValidationUtils.rejectIfEmptyOrWhitespace(errors,
		 * "legalAuthorityFName", "legalAuthorityFName.required");
		 * ValidationUtils.rejectIfEmptyOrWhitespace(errors,
		 * "legalAuthorityLName", "legalAuthorityLName.required");
		 */
	}

	/**
	 * This method validate Edit RetailerRegistration screen.
	 * 
	 * @param target
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors.
	 */
	public final void validateEditProfile(Object target, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retailerName", "retailerName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address1", "corpAdderess");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, city, "city.enter");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "postalCode.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactPhone, "contactPhone.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contactFName", "contactFName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contactLName", "contactLName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactPhoneNo, "contactPhoneNo.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactEmail, "contactEmail.required");

		/*
		 * ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bCategory",
		 * "bCategory"); ValidationUtils.rejectIfEmptyOrWhitespace(errors,
		 * "webUrl", "webUrl.required");
		 * ValidationUtils.rejectIfEmptyOrWhitespace
		 * (errors,"address2","address2.required");
		 * ValidationUtils.rejectIfEmptyOrWhitespace(errors,
		 * "legalAuthorityFName", "legalAuthorityFName.required");
		 * ValidationUtils.rejectIfEmptyOrWhitespace(errors,
		 * "legalAuthorityLName", "legalAuthorityLName.required");
		 * ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numOfStores",
		 * "numOfStores.required");
		 */
	}

	/**
	 * This method validate RetailerRegistration AppSite screen.
	 * 
	 * @param target
	 *            instance of Object.
	 * @param errors
	 *            instance of Errors.
	 */
	public final void validateAppSite(Object target, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retailerName", "businessName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address1", "businessadderess.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, city, "city.enter");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "postalCode.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactPhone, "phone.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, state, "state.required");

	}

	public final void validateAppSiteFinalize(Object target, Errors errors)
	{
		RetailerRegistration registVo = (RetailerRegistration) target;

		if (registVo.isIslocation())
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "storeNum", "storeNum.required");
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contactFName", "contactFName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contactLName", "contactLName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactPhoneNo, "contactPhoneNo.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactEmail, "contactEmail.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, terms, "terms.required");
	}

	public final void validateRegistration(Object target, Errors errors)
	{
		RetailerRegistration registVo = (RetailerRegistration) target;

		if (registVo.isIslocation())
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "storeNum", "storeNum.required");
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contactFName", "contactFName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contactLName", "contactLName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactPhoneNo, "contactPhoneNo.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, contactEmail, "contactEmail.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, terms, "terms.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "userName.required");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numOfStores", "numOfStores.required");
	}
}
package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.RetailerCustomPage;
import common.util.Utility;

public class GiveawayValidator implements Validator
{

	public boolean supports(Class<?> clazz)
	{
		// TODO Auto-generated method stub
		return RetailerCustomPage.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors)
	{
		final RetailerCustomPage retailerCustomPage = (RetailerCustomPage) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retPageTitle", "pageTile.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "longDescription", "longDescription.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shortDescription", "pageShorDescription.required");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity", "quantity.required");
		
		if (retailerCustomPage.getRetailerImg() == null || "".equals(retailerCustomPage.getRetailerImg()))
		{
			errors.rejectValue("retImage", "imageFile");
		}
		
		if (retailerCustomPage.getRetCreatedPageStartDate() != null && !"".equals(retailerCustomPage.getRetCreatedPageStartDate()))
		{
			if (!Utility.isValidDate(retailerCustomPage.getRetCreatedPageStartDate()))
			{
				errors.rejectValue("retCreatedPageStartDate", "rebStartDate.invalid");
			}
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retCreatedPageStartDate", "startDate.required");
		}

		// TODO Auto-generated method stub

	}
	/**
	 * This method validate Retailer Special Offer page screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT))
		{
			errors.rejectValue("retCreatedPageStartDate", "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT))
		{
			errors.rejectValue("retCreatedPageEndDate", "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER))
		{
			errors.rejectValue("retCreatedPageEndDate", "datebefore");
		}
		
		if (status.equals(ApplicationConstants.QUANTITY))
		{
			errors.rejectValue("quantity", "quantity.valid");
		}
	}

}

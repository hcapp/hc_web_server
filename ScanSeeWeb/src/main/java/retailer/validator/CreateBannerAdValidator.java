/**
 * @ (#) CreateBannerAdValidator.java 28-Dec-2011
 * Project       :ScanSeeWeb
 * File          : CreateBannerAdValidator.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 28-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.RetailerLocationAdvertisement;
import common.util.Utility;
/**
 * validation used in Retailer welcome page screen.
 * 
 * @author Created by SPAN.
 */
public class CreateBannerAdValidator implements Validator
{
	/**
	 * Variable advertisementDate declared as String.
	 */
	private final String advertisementDate = "advertisementDate";
	
	/**
	 * Variable advertisementEndDate declared as String.
	 */
	private final String advertisementEndDate = "advertisementEndDate";
	/**
	 * Getting the RetailerLocationAdvertisement Instance.
	 * 
	 * @param target as request parameter.
	 * @return retailerLocationAdvertisement instance of RetailerLocationAdvertisement.
	 */
	public final boolean supports(Class<?> target)
	{
		return RetailerLocationAdvertisement.class.isAssignableFrom(target);
	}

	/**
	 * This method validate Retailer welcome screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT))
		{
			errors.rejectValue(advertisementDate, "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT))
		{
			errors.rejectValue(advertisementEndDate, "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER))
		{
			errors.rejectValue(advertisementEndDate, "datebefore");
		}
	}
	
	/**
	 * This method validate Retailer welcome screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors)
	{
		final RetailerLocationAdvertisement retLocationAdvertisement = (RetailerLocationAdvertisement) target;
		if (retLocationAdvertisement.getRetailLocationIds() == null)
		{
			errors.rejectValue("retailLocationIds", "location.required");
		}
		if (retLocationAdvertisement.getStrBannerAdImagePath() == null)
		{
			errors.rejectValue("bannerAdImagePath", "imageFile");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "advertisementName", "adName.required");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, advertisementDate, "adStartDate.required");
		/*ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bannerAdImagePath", "bannerAdImagePath.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ribbonAdImagePath", "ribbonAdImagePath.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ribbonXAdURL", "ribbonXAdURL.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, advertisementEndDate, "adEndDate.required");*/
		if (retLocationAdvertisement.getAdvertisementDate() != null && !"".equals(retLocationAdvertisement.getAdvertisementDate()))
		{
			if (!Utility.isValidDate(retLocationAdvertisement.getAdvertisementDate()))
			{
				errors.rejectValue(advertisementDate, "rebStartDate.invalid");
			}
		}

		if (retLocationAdvertisement.getAdvertisementEndDate() != null && !"".equals(retLocationAdvertisement.getAdvertisementEndDate()))
		{
			if (!Utility.isValidDate(retLocationAdvertisement.getAdvertisementEndDate()))
			{
				errors.rejectValue(advertisementEndDate, "rebEndDate.invalid");
			}
		}
	}
	
	/**
	 * This method validate Retailer edit welcome screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void editValidate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATEAFTER))
		{
			errors.rejectValue(advertisementEndDate, "datebefore");
		}
	}
	
	/**
	 * This method validate edit retailer welcome screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void editValidate(Object target, Errors errors)
	{
		final RetailerLocationAdvertisement retLocationAdvertisement = (RetailerLocationAdvertisement) target;
		if (retLocationAdvertisement.getRetailLocationIds() == null)
		{
			errors.rejectValue("retailLocationIds", "location.required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "advertisementName", "adName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, advertisementDate, "adStartDate.required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, advertisementEndDate, "adEndDate.required");
		if (retLocationAdvertisement.getAdvertisementDate() != null && !"".equals(retLocationAdvertisement.getAdvertisementDate()))
		{
			if (!Utility.isValidDate(retLocationAdvertisement.getAdvertisementDate()))
			{
				errors.rejectValue(advertisementDate, "rebStartDate.invalid");
			}
		}
		if (retLocationAdvertisement.getAdvertisementEndDate() != null && !"".equals(retLocationAdvertisement.getAdvertisementEndDate()))
		{
			if (!Utility.isValidDate(retLocationAdvertisement.getAdvertisementEndDate()))
			{
				errors.rejectValue(advertisementEndDate, "rebEndDate.invalid");
			}
		}
	}
}

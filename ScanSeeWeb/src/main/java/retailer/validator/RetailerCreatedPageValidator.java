package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.RetailerCustomPage;
import common.pojo.Users;
import common.util.Utility;

/**
 * validation used in Retailer custom page screen.
 * 
 * @author Created by SPAN.
 */
public class RetailerCreatedPageValidator implements Validator {
	/**
	 * Getting the RetailerCustomPage Instance.
	 * 
	 * @param clazz as request parameter.
	 * @return retailerCustomPage instance of RetailerCustomPage.
	 */
	public final boolean supports(Class<?> clazz) {
		return RetailerCustomPage.class.isAssignableFrom(clazz);
	}

	/**
	 * This method validate Retailer custom page.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			errors.rejectValue("retCreatedPageStartDate", "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			errors.rejectValue("retCreatedPageEndDate", "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue("retCreatedPageEndDate", "datebefore");
		}
		if (status.equals(ApplicationConstants.ENDDATE)) {
			errors.rejectValue("retCreatedPageEndDate", "endDate.required");
		}
		if (status.equals(ApplicationConstants.STARTDATE)) {
			errors.rejectValue("retCreatedPageStartDate", "startDate.required");
		}
	}

	/**
	 * This method validate Retailer Banner Screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors,Boolean isBand) {
		final RetailerCustomPage retailerCustomPage = (RetailerCustomPage) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retCreatedPageAttachLinkTitle", "pageTile.required");
		
		if(!isBand){
			if (retailerCustomPage.getRetCreatedPageAttachLinkLocId() == null || "".equals(retailerCustomPage.getRetCreatedPageAttachLinkLocId())) {
				errors.rejectValue("retCreatedPageAttachLinkLocId", "location.required");
			}
		}
		if (retailerCustomPage.getRetCreatedPageattachLink() != null && !"".equals(retailerCustomPage.getRetCreatedPageattachLink())) {
			if (!Utility.validateURL(retailerCustomPage.getRetCreatedPageattachLink())) {
				errors.rejectValue("retCreatedPageattachLink", "url.valid");
			}
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retCreatedPageattachLink", "link.required");
		}
	}

	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		
		final RetailerCustomPage retailerCustomPage = (RetailerCustomPage) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retCreatedPageAttachLinkTitle", "pageTile.required");
		
		
			if (retailerCustomPage.getRetCreatedPageAttachLinkLocId() == null || "".equals(retailerCustomPage.getRetCreatedPageAttachLinkLocId())) {
				errors.rejectValue("retCreatedPageAttachLinkLocId", "location.required");
			}
		
		if (retailerCustomPage.getRetCreatedPageattachLink() != null && !"".equals(retailerCustomPage.getRetCreatedPageattachLink())) {
			if (!Utility.validateURL(retailerCustomPage.getRetCreatedPageattachLink())) {
				errors.rejectValue("retCreatedPageattachLink", "url.valid");
			}
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "retCreatedPageattachLink", "link.required");
		}
		
	}
	

}

/**
 * @ (#) CreateCouponValidator.java 26-Dec-2011
 * Project       :ScanSeeWeb
 * File          : CreateCouponValidator.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 26-Dec-2011
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */

package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.Coupon;
import common.util.Utility;

/**
 * validation used in coupon page screen.
 * 
 * @author Created by SPAN.
 */
public class CreateCouponValidator implements Validator
{
	/**
	 * Variable couponStartDate declared as String.
	 */
	private final String couponStartDate = "couponStartDate";
	
	/**
	 * Variable couponEndDate declared as String.
	 */
	private final String couponEndDate = "couponEndDate";
	
	/**
	 * Variable couponExpireDate declared as String type.
	 */
	private final String couponExpireDate ="couponExpireDate";
	/**
	 * Getting the Coupon Instance.
	 * 
	 * @param target as request parameter.
	 * @return coupon instance of Coupon.
	 */
	public final boolean supports(Class<?> target)
	{
		return Coupon.class.isAssignableFrom(target);
	}

	/**
	 * This method validate Retailer coupon screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			errors.rejectValue(couponStartDate, "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			errors.rejectValue(couponEndDate, "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue(couponEndDate, "datebefore");
		}
		if (status.equals(ApplicationConstants.EXPIREDDATE)) {
			errors.rejectValue(couponExpireDate, "dateexpcurrent");
		}
		if (status.equals(ApplicationConstants.DATEEXPAFTER)) {
			errors.rejectValue(couponExpireDate, "dateexpbefore");
		}
		
		if (status.equals(ApplicationConstants.DATEEXPENTER)) {
			errors.rejectValue("couponExpireDate", "coupentrexp");
		}

		if (status.equals(ApplicationConstants.DATEENDENTER)) {
			errors.rejectValue("couponEndDate", "coupentrend");
		}

		
	}
	
	/**
	 * This method validate Retailer coupon screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors)
	{
		final Coupon couponinfo = (Coupon) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "couponName", "couponName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bannerTitle", "bannerTitle.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numOfCouponIssue", "numOfCouponIssue.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, couponStartDate, "couponStartDate.required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, couponEndDate, "couponEndDate.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "couponLongDesc", "couponLongDesc.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "couponImgPath", "imageFile.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "detailImgPath", "detailImage.required");
		
		
		if (couponinfo.getCouponStartDate() != null && !"".equals(couponinfo.getCouponStartDate()))
		{
			if (!Utility.isValidDate(couponinfo.getCouponStartDate()))
			{
				errors.rejectValue(couponStartDate, "rebStartDate.invalid");
			}
		}
		
		
		if(null != couponinfo.getNumOfCouponIssue() && !"".equals(couponinfo.getNumOfCouponIssue() ))
				
				{
			if(Utility.validateZero(String.valueOf(couponinfo.getNumOfCouponIssue())))
					{
				errors.rejectValue("numOfCouponIssue", "numOfCouponIssue.notvalid");
				
					}
				}

/*		if (couponinfo.getCouponEndDate() != null && !"".equals(couponinfo.getCouponEndDate()))
		{
			if (!Utility.isValidDate(couponinfo.getCouponEndDate()))
			{
				errors.rejectValue(couponEndDate, "rebEndDate.invalid");
			}
		}*/
		if (couponinfo.getLocationID() == null)
		{
			errors.rejectValue("locationID", "location.required");
		}
	}
}

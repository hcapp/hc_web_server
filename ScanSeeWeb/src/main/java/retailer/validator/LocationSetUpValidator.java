package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.RetailerLocation;

/**
 * validation used in retailer location screen.
 * 
 * @author Created by SPAN.
 */
public class LocationSetUpValidator implements Validator {
	/**
	 * Getting the RetailerLocation Instance.
	 * 
	 * @param target as request parameter.
	 * @return RetailerLocation instance.
	 */
	public final boolean supports(Class<?> target) {
		return RetailerLocation.class.isAssignableFrom(target);
	}
	
	/**
	 * This method validate retailer location screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "storeID", "storeID.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "storeAddress", "address.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "city.enter");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "state", "state.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneNumber", "phone.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "postalCode.required");
	}
	
	/**
	 * This method validate retailer location screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status) {

		if (status.equals(ApplicationConstants.DUPLICATE_STORE)) {
			errors.rejectValue("storeID", "storeID.exist");
		} 
		if (status.equals(ApplicationConstants.INVALIDCONTPHONE)) {
			errors.rejectValue("phoneNumber", "invalid.Phonenumber");
		}
		if (status.equals(ApplicationConstants.STATE)) {
			errors.rejectValue("state", "select.State");
		} 
		if (status.equals(ApplicationConstants.CITY)) {
			errors.rejectValue("city", "city.enter");
		} 
		if (status.equals(ApplicationConstants.INVALIDEMAIL)) {
			errors.rejectValue("contactEmail", "invalid.email");
		}
		if (status.equals(ApplicationConstants.INVALIDURL)) {
			errors.rejectValue("retailUrl", "invalid.retailurl");
		}
		if (status.equals(ApplicationConstants.INVALIDPOSTALCODE)) {
			errors.rejectValue("postalCode", "invalid.postalCode");
		}
		if(status.equals(ApplicationConstants.GEOERROR)){
			errors.rejectValue("storeAddress", "invalid.geomessge");
		}
		if(status.equals(ApplicationConstants.LATITUDE)){
			errors.rejectValue("retailerLocationLatitude", "latitude.required");
		}
		if(status.equals(ApplicationConstants.LONGITUDE)){
			errors.rejectValue("retailerLocationLongitude", "longitude.required");
		}
	}
}

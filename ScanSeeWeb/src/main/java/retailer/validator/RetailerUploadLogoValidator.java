package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import common.pojo.RetailerUploadLogoInfo;

/**
 * validation used in Retailer Upload Logo screen.
 * 
 * @author Created by SPAN.
*/
public class RetailerUploadLogoValidator implements Validator
{
   /*
    * This Validator for Validating the Retailser Logo file Size, file Size should be (greater than 0 and less than 20kb). 
    * 
    */
	/**
	 * Getting the RetailerUploadLogoInfo Instance.
	 * 
	 * @param target as request parameter.
	 * @return retailerUploadLogoInfo instance of RetailerUploadLogoInfo.
	 */
	public final boolean supports(Class<?> target)
       {
	         return RetailerUploadLogoInfo.class.isAssignableFrom(target);
       }

	/**
	 * This method validate RetailerUploadLogoInfo Instance screen.
	 * 
	 * @param target instance of Object.
	 * @param errors instance of Errors.
	 */
       public final void validate(Object target, Errors errors)
       {
    	//RetailerUploadLogoInfo retailerUploadLogoInfo = (RetailerUploadLogoInfo)target;
 		/*if(retailerUploadLogoInfo.getRetailerLogo().getSize() == 0){
 		     errors.rejectValue("retailerLogo", "retailerLogo.required");
 		}
 		else if(retailerUploadLogoInfo.getRetailerLogo().getSize() > 20000){
 			 errors.rejectValue("retailerLogo", "retailerLogo.required");
 		}*/
      }
}

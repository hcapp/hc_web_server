package retailer.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.HotDealInfo;

/**
 * validation used in Retailer Hot Deals page screen.
 * 
 * @author Created by SPAN.
 */
public class HotDealRetailerDetailsValidator implements Validator {

	/**
	 * Variable dealStartDate declared as String.
	 */
	private final String dealStartDate = "dealStartDate";
	
	/**
	 * Variable dealEndDate declared as String.
	 */
	private final String dealEndDate = "dealEndDate";
	/**
	 * Getting the HotDealInfo Instance.
	 * 
	 * @param arg0 as request parameter.
	 * @return hotDealInfo instance of HotDealInfo.
	 */
	public final boolean supports(Class<?> arg0) {
		return HotDealInfo.class.isAssignableFrom(arg0);
	}

	/**
	 * This method validate Retailer Hot Deals page screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 * @param status as request parameter.
	 */
	public final void validate(Object arg0, Errors errors, String status)
	{
		if (status.equals(ApplicationConstants.DATESTARTCURRENT)) {
			errors.rejectValue(dealStartDate, "datestartcurrent");
		}
		if (status.equals(ApplicationConstants.DATEENDCURRENT)) {
			errors.rejectValue(dealEndDate, "dateendcurrent");
		}
		if (status.equals(ApplicationConstants.DATEAFTER)) {
			errors.rejectValue(dealEndDate, "datebefore");
		}
		if (status.equals(ApplicationConstants.BUSINESSCATEGORY)) {
			errors.rejectValue("bCategory", "select.category");
		}
		if (status.equals(ApplicationConstants.POPULATIONCENTER)) {
			errors.rejectValue("city", "select.city");
		}
		if (status.equals(ApplicationConstants.DEALRETAILERLOC)) {
			errors.rejectValue("retailerLocID", "select.retailerloc");
		}
		if (status.equals(ApplicationConstants.EXPIREDDATE)) {
			errors.rejectValue("expireDate", "expireddate");
		}
		if (status.equals(ApplicationConstants.EXPIREDDATEBEFORE)) {
			errors.rejectValue("expireDate", "expiredatebefore");
		}
		if (status.equals(ApplicationConstants.EXPIREDDATE_GRTN_STARTDATE)) {
			errors.rejectValue("expireDate", "expiredateafter");
		}
	}
	/**
	 * This method validate Retailer Hot Deals page screen.
	 * 
	 * @param arg0 instance of Object.
	 * @param errors instance of Errors.
	 */
	public final void validate(Object arg0, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hotDealName", "hotDealName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "price");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hotDealShortDescription", "hotDealShortDescription");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hotDealTermsConditions", "hotDealTermsConditions");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, dealStartDate, dealStartDate);
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, dealEndDate, dealEndDate);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salePrice", "salePrice");
	//	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "expireDate", "expireDate");
	//	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "couponCode", "couponCode");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numOfHotDeals", "numOfHotDeals");
	}
	
}

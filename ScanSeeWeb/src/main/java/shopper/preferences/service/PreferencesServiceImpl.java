package shopper.preferences.service;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.preferences.dao.PreferencesDAO;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.City;
import common.pojo.CountryCode;
import common.pojo.UserPreference;
import common.pojo.shopper.CountryCodes;
import common.pojo.shopper.CountryDetails;
import common.pojo.shopper.CreateOrUpdateUserPreference;
import common.pojo.shopper.EducationalLevelDetails;
import common.pojo.shopper.EducationalLevelInfo;
import common.pojo.shopper.IncomeRange;
import common.pojo.shopper.IncomeRanges;
import common.pojo.shopper.MaritalStatusDetails;
import common.pojo.shopper.MaritalStatusInfo;
import common.pojo.shopper.UpdateUserInfo;
import common.pojo.shopper.UserInfo;
import common.pojo.shopper.UserRegistrationInfo;
import common.util.EncryptDecryptPwd;

public class PreferencesServiceImpl implements PreferencesService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PreferencesServiceImpl.class);
	/**
	 * Instance variable for Wish list DAO instance.
	 */
	/**
	 * Instance variable for Wish List DAO instance.
	 */
	@SuppressWarnings("unused")
	private PreferencesDAO preferencesDao;

	/**
	 * sets the preferencesDAO DAO.
	 * 
	 * @param preferencesDao
	 *            The instance for PreferencesDAO
	 */

	public final void setPreferencesDao(PreferencesDAO preferencesDao)
	{
		this.preferencesDao = preferencesDao;
	}

	/**
	 * The service method for fetching user setting preferences information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public final CreateOrUpdateUserPreference fetchUserPreferences(Long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchUserPreferences";
		LOG.info("Inside fetchUserPreferences method");
		CreateOrUpdateUserPreference userPref;

		userPref = preferencesDao.fetchUserPreference(userId);

		LOG.info("In Service..." + methodName);

		return userPref;
	}

	/**
	 * The Service method for Saving User setting preffereces information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param userPreference
	 *            Contains user preferred categories information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public final String saveUserPreference(UserPreference userPreference) throws ScanSeeWebSqlException
	{
		final String methodName = "saveUserPreference";
		LOG.info(methodName);
		String response = null;

		if (userPreference.getUserID() != null)
		{
			response = preferencesDao.saveUserPreferenceDetails(userPreference);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * The service method for fetching user information. This method validates
	 * the userId, if it is valid it will call the DAO method.
	 * 
	 * @param userID
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public final UserInfo fetchUserInfo(Long userID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchUserInfo";
		LOG.info("In Service " + ApplicationConstants.METHODSTART + methodName);
		UpdateUserInfo updateUserInfo = null;
		new UpdateUserInfo();
		List<IncomeRange> incomeRange = null;
		List<CountryCode> countryCode = null;
		List<EducationalLevelInfo> eduLevelInfo = null;
		List<MaritalStatusInfo> maritalStatusInfo = null;

		UserInfo userInfo = null;
		incomeRange = preferencesDao.getIncomeRanges();
		countryCode = preferencesDao.getCountryCodes();
		eduLevelInfo = preferencesDao.getEducationalLevel();
		maritalStatusInfo = preferencesDao.getMaritalStatusInfo();
		updateUserInfo = preferencesDao.fetchUserInfo(userID);
		userInfo = new UserInfo();
		if (null != updateUserInfo)
		{
			userInfo.setUpdateUserInfo(updateUserInfo);
		}
		if (null != incomeRange)
		{
			final IncomeRanges iranges = new IncomeRanges();
			iranges.setIncomeRange(incomeRange);
			userInfo.setIncomeRanges(iranges);
		}
		if (null != countryCode)
		{
			final CountryCodes countryCodes = new CountryCodes();
			countryCodes.setCountryCode(countryCode);
			userInfo.setCountryCodes(countryCodes);
		}

		if (null != eduLevelInfo)
		{
			final EducationalLevelDetails eduLevelDetails = new EducationalLevelDetails();
			eduLevelDetails.setEducationalInfo(eduLevelInfo);
			userInfo.setEducationalLevelDetail(eduLevelDetails);
		}
		if (null != maritalStatusInfo)
		{
			final MaritalStatusDetails maritalStatusDetails = new MaritalStatusDetails();
			maritalStatusDetails.setMaritalInfo(maritalStatusInfo);
			userInfo.setMaritalStatusDetail(maritalStatusDetails);
		}

		LOG.info("In managesettings Service" + ApplicationConstants.METHODEND + methodName);

		return userInfo;
	}

	/**
	 * The Service method for Saving User Information. This method checks for
	 * mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param userRegistrationInfo
	 *            containing user information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public final String insertUserInfo(UserRegistrationInfo userRegistrationInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "insertUserInfo";
		LOG.info("In Service " + ApplicationConstants.METHODSTART + methodName);
		String response = null;
		response = preferencesDao.insertUserInfo(userRegistrationInfo);
		LOG.info("In Service..." + ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * The service method for all the States.
	 * 
	 * @return the XML containing states information in the response.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	public final CountryDetails getAllStates() throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllStates";
		LOG.info("In manage settings Service...." + ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> stateList = null;
		CountryDetails statesDetails = null;

		stateList = preferencesDao.getAllStates();

		if (null != stateList && !stateList.isEmpty())
		{
			statesDetails = new CountryDetails();
			statesDetails.setStateInfo(stateList);
		}

		return statesDetails;
	}

	/**
	 * The service method for all the cities for the given statename and search
	 * keyword.
	 * 
	 * @param stateName
	 *            -for which cities to be fetched
	 * @return the XML containing cities information in the response.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public final ArrayList<City> fetchAllCities(String stateName) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllCities";
		LOG.info("In Service layer:" + ApplicationConstants.METHODSTART + methodName);
		ArrayList<City> cities = null;
		try
		{
			cities = preferencesDao.getAllCities(stateName);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cities;
	}

	/**
	 * The service Method takes the user new password and encrypt it. Calls the
	 * dao method with encrypted password and userID.
	 * 
	 * @param userId
	 *            in the User Request.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public final String changePassword(Long userId, String password) throws ScanSeeWebSqlException
	{
		final String methodName = "changePassword";
		LOG.info(ApplicationConstants.METHODSTART + methodName + "in Service layer");
		String response = null;

		EncryptDecryptPwd encryptDecryptPwd;
		String encryptedpwd = null;
		try
		{
			encryptDecryptPwd = new EncryptDecryptPwd();
			encryptedpwd = encryptDecryptPwd.encrypt(password);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error("Exception in Encryption of password..", e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error("Exception in Encryption of user password..", e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidKeyException e)
		{
			LOG.error("user password Encryption Exception    ", e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error("Exception while Encryption  ", e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error("Exception during Encryption ", e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error("Exception in user password Encryption ", e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (BadPaddingException e)
		{
			LOG.error("Exception in Encryption ", e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		response = preferencesDao.changePassword(userId, encryptedpwd);

		LOG.info(ApplicationConstants.METHODEND + methodName + "in Service layer");
		return response;

	}

}

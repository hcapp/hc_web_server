package shopper.preferences.controller;

import java.text.ParseException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.preferences.service.PreferencesService;
import shopper.service.CommonService;
import shopper.service.ShopperService;
import shopper.validator.ShopperProfileValidator;
import supplier.service.SupplierService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.State;
import common.pojo.UserPreference;
import common.pojo.Users;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.CreateOrUpdateUserPreference;
import common.pojo.shopper.MainCategoryDetail;
import common.pojo.shopper.SubCategoryDetail;
import common.pojo.shopper.UserInfo;
import common.pojo.shopper.UserRegistrationInfo;
import common.util.Utility;

/**
 * This class is a controller class for user preference settings.
 * 
 * @author manjunatha_gh
 */
@Controller
public class ConsumerUserPreferencesController
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(UserPreferencesController.class);

	ShopperProfileValidator shopperProfileValidator;

	@Autowired
	public void setShopperProfileValidator(ShopperProfileValidator shopperProfileValidator)
	{
		this.shopperProfileValidator = shopperProfileValidator;
	}

	/**
	 * This method is used for displaying user selected preference categories.
	 * 
	 * @param categoryDetail
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/conspreferences.htm", method = RequestMethod.GET)
	public String showPage(@ModelAttribute("preferencesusrcategories") CategoryDetail categoryDetail, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		String methodName = "showPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		final Integer lowerlimit = null;
		Integer mainCtgyCount = 0;
		Integer mainCtgySelCount = 0;
		Integer subCtgyCount = 0;
		Integer subCtgySelCount = 0;
		String catName = null;
		String subCatName = null;
		StringBuffer buffer = null;
		StringBuffer stringBuffer = null;
		StringBuffer finalBuffer = null;
		Integer categoryId;
		Integer favCatId = 0;
		StringBuffer selectedCat = null;
		
		try
		{
			finalBuffer = new StringBuffer();
			buffer = new StringBuffer();
			selectedCat = new StringBuffer();
			final Users user = (Users) session.getAttribute("loginuser");
			final Long shopperId = Long.valueOf(user.getUserID());

			if (null != shopperId)
			{
				categoryDetail = shopperService.getUserFavCategories(shopperId, lowerlimit);
				
				for (MainCategoryDetail mainCategoryDetail : categoryDetail.getMainCategoryDetaillst())
				{
					subCtgySelCount = 0;
					subCtgyCount = 0;
					mainCtgyCount++;
					stringBuffer = new StringBuffer();
					catName = mainCategoryDetail.getMainCategoryName();
					
					for (SubCategoryDetail subCategoryDetail : mainCategoryDetail.getSubCategoryDetailLst())
					{
						subCtgyCount++;
						favCatId++;
						subCatName = subCategoryDetail.getSubCategoryName();
						categoryId = subCategoryDetail.getCategoryId();
						stringBuffer.append("<tr name="
										+ catName
										+ " onclick='toggleConfirm();'><td align='center' width='4%' class='wrpWord'>&nbsp;</td><td width='4%' class='wrpWord'>");
						
						if (subCategoryDetail.getDisplayed() == 0)
						{
							stringBuffer.append("<input type='checkbox' id='favCatId" + favCatId + "' value='" + categoryId + "' name='favCatId'/>");
						}
						else
						{
							stringBuffer.append("<input type='checkbox' id='favCatId" + favCatId + "' checked='checked' value='" + categoryId + "' name='favCatId' />");
							subCtgySelCount++;
						}
						
						stringBuffer.append("</td><td width='92%' class='wrpWord'>" + subCatName + "</td>");
					}
					
					if(subCtgyCount.equals(subCtgySelCount))
					{
						buffer.append("<tr name=" + catName + " class='mainCtgry'>" +
								"<td width='4%' align='center' class='wrpWord'>" +
								"<input type='checkbox' name='favCatId' checked='checked'/></td>"+
								"<td colspan='2' width='96%'  class='wrpWord'>" + catName + "</td></tr>");
						mainCtgySelCount++;
						String[] name = {catName} ;
						name = name[0].split("[&|,| ]");
						selectedCat.append(name[0] + ",");
					}
					else
					{
						buffer.append("<tr name=" + catName + " class='mainCtgry'>" +
								"<td width='4%' align='center' class='wrpWord'>" +
								"<input type='checkbox' name='favCatId'/></td>"+
								"<td colspan='2' width='96%'  class='wrpWord'>" + catName + "</td></tr>");
						if(!subCtgySelCount.equals(0))
						{
							String[] name = {catName} ;
							name = name[0].split("[&|,| ]");
							selectedCat.append(name[0] + ",");
						}
					}
					
					buffer.append(stringBuffer);

				}
				
				finalBuffer.append("<thead><tr class='subHdr'><th align='center'>");
				
				if(mainCtgyCount.equals(mainCtgySelCount))
				{
					finalBuffer.append("<input type='checkbox' id='checkAll' name='checkAll' checked='checked'>");					
				}
				else
				{
					finalBuffer.append("<input type='checkbox' id='checkAll' name='checkAll'>");
				}
				
				finalBuffer.append("<label for='checkbox' name='ctgry'></label></th><th align='left' colspan='2'>" +
									"<img width='14' height='13' alt='Preferences' src='../images/prefIconSml.png'/>" +
									"Select Your Preferences</th></tr></thead><tbody>");
				finalBuffer.append(buffer);
				finalBuffer.append("</tbody>");
				selectedCat.append("end");
				session.setAttribute("expandCategory", selectedCat);
				session.setAttribute("preferenceCategories", finalBuffer);
				session.setAttribute("categoryDetailresponse", categoryDetail);
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName);
			e.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		model.addAttribute("preferencesusrcategories", categoryDetail);
		return "conspreferencessetting";
	}

	/**
	 * This method is used for saving user selected preference categories.
	 * 
	 * @param categoryDetail
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 */
	@RequestMapping(value = "/consuserPreferences.htm", method = RequestMethod.POST)
	public ModelAndView preferencesSubmit(@ModelAttribute("preferencesusrcategories") CategoryDetail categoryDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
	{
		String methodName = "preferencesSubmit";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		String response = null;
		String favId = null;
		CategoryDetail categoryDetailresponse = null;
		final Integer lowerlimit = null;
		Integer mainCtgyCount = 0;
		Integer mainCtgySelCount = 0;
		Integer subCtgyCount = 0;
		Integer subCtgySelCount = 0;
		String catName = null;
		String subCatName = null;
		StringBuffer buffer = null;
		StringBuffer stringBuffer = null;
		StringBuffer finalBuffer = null;
		Integer categoryId;
		Integer favCatId = 0;
		StringBuffer selectedCat = null;
		
		try
		{
			finalBuffer = new StringBuffer();
			buffer = new StringBuffer();
			selectedCat = new StringBuffer();
			final Users loginUser = (Users) session.getAttribute("loginuser");
			final Long userId = loginUser.getUserID();
			favId = categoryDetail.getFavCatId();
			if (null != favId)
			{
				favId = favId.replace("on,", "");
			}
			response = shopperService.setUserFavCategories(favId, userId, categoryDetail.isCellPhone(), categoryDetail.isEmail());
			if (response.equals(ApplicationConstants.SUCCESS))
			{
				request.setAttribute("UpdatePreferedCategories", ApplicationConstants.SETUSERFAVCATEGORIES);
			}
			else
			{
				request.setAttribute("UpdatePreferedCategories", ApplicationConstants.ERROROCCURED);
			}

			if (null != response)
			{

				if (null != userId)
				{
					categoryDetailresponse = shopperService.getUserFavCategories(userId, lowerlimit);
					
					for (MainCategoryDetail mainCategoryDetail : categoryDetailresponse.getMainCategoryDetaillst())
					{
						subCtgySelCount = 0;
						subCtgyCount = 0;
						mainCtgyCount++;
						stringBuffer = new StringBuffer();
						catName = mainCategoryDetail.getMainCategoryName();
						
						for (SubCategoryDetail subCategoryDetail : mainCategoryDetail.getSubCategoryDetailLst())
						{
							subCtgyCount++;
							favCatId++;
							subCatName = subCategoryDetail.getSubCategoryName();
							categoryId = subCategoryDetail.getCategoryId();
							stringBuffer.append("<tr name="
											+ catName
											+ " onclick='toggleConfirm();'><td align='center' width='4%' class='wrpWord'>&nbsp;</td><td width='4%' class='wrpWord'>");
							
							if (subCategoryDetail.getDisplayed() == 0)
							{
								stringBuffer.append("<input type='checkbox' id='favCatId" + favCatId + "' value='" + categoryId + "' name='favCatId'/>");
							}
							else
							{
								stringBuffer.append("<input type='checkbox' id='favCatId" + favCatId + "' checked='checked' value='" + categoryId + "' name='favCatId' />");
								subCtgySelCount++;
							}
							
							stringBuffer.append("</td><td width='92%' class='wrpWord'>" + subCatName + "</td>");
						}
						
						if(subCtgyCount.equals(subCtgySelCount))
						{
							buffer.append("<tr name=" + catName + " class='mainCtgry'>" +
									"<td width='4%' align='center' class='wrpWord'>" +
									"<input type='checkbox' name='favCatId' checked='checked'/></td>"+
									"<td colspan='2' width='96%'  class='wrpWord'>" + catName + "</td></tr>");
							mainCtgySelCount++;
							String[] name = {catName} ;
							name = name[0].split("[&|,| ]");
							selectedCat.append(name[0] + ",");
						}
						else
						{
							buffer.append("<tr name=" + catName + " class='mainCtgry'>" +
									"<td width='4%' align='center' class='wrpWord'>" +
									"<input type='checkbox' name='favCatId'/></td>"+
									"<td colspan='2' width='96%'  class='wrpWord'>" + catName + "</td></tr>");
							if(!subCtgySelCount.equals(0))
							{
								String[] name = {catName} ;
								name = name[0].split("[&|,| ]");
								selectedCat.append(name[0] + ",");
							}
						}
						
						buffer.append(stringBuffer);

					}
					
					finalBuffer.append("<thead><tr class='subHdr'><th align='center'>");
					
					if(mainCtgyCount.equals(mainCtgySelCount))
					{
						finalBuffer.append("<input type='checkbox' id='checkAll' name='checkAll' checked='checked'>");					
					}
					else
					{
						finalBuffer.append("<input type='checkbox' id='checkAll' name='checkAll'>");
					}
					
					finalBuffer.append("<label for='checkbox' name='ctgry'></label></th><th align='left' colspan='2'>" +
										"<img width='14' height='13' alt='Preferences' src='../images/prefIconSml.png'/>" +
										"Select Your Preferences</th></tr></thead><tbody>");
					finalBuffer.append(buffer);
					finalBuffer.append("</tbody>");
					selectedCat.append("end");
					session.setAttribute("expandCategory", selectedCat);
					session.setAttribute("preferenceCategories", finalBuffer);
					session.setAttribute("categoryDetailresponse", categoryDetailresponse);
				}
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName);
			e.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("conspreferencessetting");
	}

	/**
	 * This method is used to display user radius.
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/consfetchusersettings.htm", method = RequestMethod.GET)
	public ModelAndView fetchUserSettings(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "fetchUserSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");

		final String responseXML = null;

		final Users loginUser = (Users) session.getAttribute("loginuser");
		final Long userId = loginUser.getUserID();
		try
		{
			CreateOrUpdateUserPreference createOrUpdateUserPreference = null;
			createOrUpdateUserPreference = preferencesService.fetchUserPreferences(userId);
			final UserPreference userPreference = new UserPreference();
			if (null != createOrUpdateUserPreference)
			{
				final Integer locRadius = createOrUpdateUserPreference.getLocationRadius();
				request.setAttribute("userLocRadius", locRadius);
				if (null != locRadius)
				{
					userPreference.setLocaleRadius(locRadius.longValue());
				}

			}
			model.put("preferencesusersettings", userPreference);
		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView("consselectradius");
	}

	/**
	 * This method is used to save user radius.
	 * 
	 * @param userPreference
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/conssaveusersettings.htm", method = RequestMethod.POST)
	public ModelAndView saveUserSettings(@ModelAttribute("preferencesusersettings") UserPreference userPreference, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "saveUserSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");

		final String responseXML = null;

		final Users loginUser = (Users) session.getAttribute("loginuser");
		final Long userId = loginUser.getUserID();

		try
		{
			userPreference.setUserID(userId);
			final String response = preferencesService.saveUserPreference(userPreference);
			if (response.equals(ApplicationConstants.SUCCESS))
			{
				request.setAttribute("RadiusSaved", ApplicationConstants.UPDATELOCALEUSRRADIUS);
			}
			else
			{
				request.setAttribute("RadiusSaved", ApplicationConstants.ERROROCCURED);
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView("consselectradius");
	}

	/**
	 * This method is used to display user details.
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 * @throws ParseException
	 */
	@RequestMapping(value = "/consfetchuserinfo.htm", method = RequestMethod.GET)
	public ModelAndView fetchUserInfo(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException, ParseException
	{
		final String methodName = "fetchUserInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");
		final SupplierService supplierService = (SupplierService) appContext.getBean(ApplicationConstants.SUPPLIERSERVICE);

		final String responseXML = null;

		final Users loginUser = (Users) session.getAttribute("loginuser");
		final Long userId = loginUser.getUserID();
		ArrayList<State> states = null;
		try
		{
			UserInfo userInfoObj = null;
			final UserRegistrationInfo userRegistrationInfo = new UserRegistrationInfo();
			userInfoObj = preferencesService.fetchUserInfo(userId);

			if (null != userInfoObj)
			{
				userRegistrationInfo.setUserId(userId);
				userRegistrationInfo.setFirstName(userInfoObj.getUpdateUserInfo().getFirstName());
				userRegistrationInfo.setLastName(userInfoObj.getUpdateUserInfo().getLastName());
				userRegistrationInfo.setAddress1(userInfoObj.getUpdateUserInfo().getAddress1());
				userRegistrationInfo.setAddress2(userInfoObj.getUpdateUserInfo().getAddress2());

				if (null != userInfoObj.getUpdateUserInfo().getNumberOfChildren() && !userInfoObj.getUpdateUserInfo().getNumberOfChildren().isEmpty())
				{
					final Integer nofchildren = Integer.parseInt(userInfoObj.getUpdateUserInfo().getNumberOfChildren());
					userRegistrationInfo.setChildren(nofchildren);
				}

				userRegistrationInfo.setPostalCode(userInfoObj.getUpdateUserInfo().getPostalCode());

				if (null != userInfoObj.getUpdateUserInfo().getMobileNumber() && !userInfoObj.getUpdateUserInfo().getMobileNumber().isEmpty())
				{
					userRegistrationInfo.setMobileNumber(Utility.getPhoneFormate(userInfoObj.getUpdateUserInfo().getMobileNumber()));
				}
				if (null != userInfoObj.getUpdateUserInfo().getGender() && !userInfoObj.getUpdateUserInfo().getGender().isEmpty())
				{
					userRegistrationInfo.setGender(Integer.valueOf(userInfoObj.getUpdateUserInfo().getGender()));
				}
				if (null != userInfoObj.getUpdateUserInfo().getDob() && !userInfoObj.getUpdateUserInfo().getDob().isEmpty())
				{
					userRegistrationInfo.setDob(Utility.getCalenderFormattedDate(userInfoObj.getUpdateUserInfo().getDob()));
				}
				if (null != userInfoObj.getUpdateUserInfo().getSelectedCountryId())
				{
					userRegistrationInfo.setCountryID(Integer.valueOf(userInfoObj.getUpdateUserInfo().getSelectedCountryId()));
				}

				userRegistrationInfo.setCity(userInfoObj.getUpdateUserInfo().getCity());
				userRegistrationInfo.setState(userInfoObj.getUpdateUserInfo().getState());
				states = supplierService.getState(userInfoObj.getUpdateUserInfo().getState());

				for (State state : states)
				{
					userRegistrationInfo.setStateHidden(state.getStateName());
				}

				userRegistrationInfo.setStateCodeHidden(userInfoObj.getUpdateUserInfo().getState());

				if (null != userInfoObj.getUpdateUserInfo().getEducationLevelId() && !userInfoObj.getUpdateUserInfo().getEducationLevelId().isEmpty())
				{
					userRegistrationInfo.setEducationLevelId(Integer.valueOf(userInfoObj.getUpdateUserInfo().getEducationLevelId()));
				}
				if (null != userInfoObj.getUpdateUserInfo().getHomeOwner())
				{
					userRegistrationInfo.setHomeOwner(Integer.valueOf(userInfoObj.getUpdateUserInfo().getHomeOwner()));
				}

				userRegistrationInfo.setMaritalStatus(userInfoObj.getUpdateUserInfo().getMaritalStatus());

				if (null != userInfoObj.getUpdateUserInfo().getSelectedIncomeRangeID()
						&& !userInfoObj.getUpdateUserInfo().getSelectedIncomeRangeID().isEmpty())
				{
					userRegistrationInfo.setIncomeRangeID(Integer.valueOf(userInfoObj.getUpdateUserInfo().getSelectedIncomeRangeID()));
				}

				session.setAttribute("matitalstatuslst", userInfoObj.getMaritalStatusDetail());
				session.setAttribute("incomerangelst", userInfoObj.getIncomeRanges());
				session.setAttribute("educationlst", userInfoObj.getEducationalLevelDetail());
				request.setAttribute("userId", userId);
			}
			model.put("preferencesuserinfo", userRegistrationInfo);
		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView("conspreferencesuserinfo");
	}

	/**
	 * This method is used to save user details.
	 * 
	 * @param userRegistrationInfoObj
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/consfetchuserinfo.htm", method = RequestMethod.POST)
	public ModelAndView saveUserInfo(@ModelAttribute("preferencesuserinfo") UserRegistrationInfo userRegistrationInfoObj, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "saveUserInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");
		final CommonService commonService = (CommonService) appContext.getBean("commonService");

		final String responseXML = null;

		final Users loginUser = (Users) session.getAttribute("loginuser");
		final Long userId = loginUser.getUserID();
		Users userInfo = null;
		boolean isValidContactNum = true;
		boolean isValidDob = true;
		try
		{
			userRegistrationInfoObj.setUserId(userId);
			userRegistrationInfoObj.setState(userRegistrationInfoObj.getStateCodeHidden());
			shopperProfileValidator.validateUserPreferenceInfo(userRegistrationInfoObj, result);
			isValidContactNum = Utility.validatePhoneNum(userRegistrationInfoObj.getMobileNumber());
			isValidDob = Utility.ValidateDOB(userRegistrationInfoObj.getDob(), "MM/dd/yyyy");
			if (result.hasErrors())
			{
				return new ModelAndView("conspreferencesuserinfo");
			}
			if (!isValidContactNum)
			{
				shopperProfileValidator.validatePreferences(userRegistrationInfoObj, result, ApplicationConstants.INVALIDCONTPHONE);
			}
			if (result.hasErrors())
			{
				return new ModelAndView("conspreferencesuserinfo");
			}
			if (!isValidDob)
			{
				shopperProfileValidator.validatePreferences(userRegistrationInfoObj, result, ApplicationConstants.INVALIDDOB);
			}
			if (result.hasErrors())
			{
				return new ModelAndView("conspreferencesuserinfo");
			}
			final String response = preferencesService.insertUserInfo(userRegistrationInfoObj);

			if (response.equals(ApplicationConstants.SUCCESS))
			{
				request.setAttribute("SavedUserInfo", ApplicationConstants.SAVEUSERINFORMATION);
				userInfo = commonService.getLoginUserDetails(userId);
				userInfo.setUserID(userId);
				final String uname = loginUser.getUserName();
				final String fname = userInfo.getFirstName();
				final String emailId = userInfo.getEmail();
				session.setAttribute("truncUsername", Utility.truncate(uname, 15));
				session.setAttribute("truncFirstName", Utility.truncate(fname, 15));
				session.setAttribute("emailId", emailId);
				userInfo.setUserName(loginUser.getUserName());
				userInfo.setUserType(1);
				session.removeAttribute("loginuser");
				session.setAttribute("loginuser", userInfo);
			}
			else
			{
				request.setAttribute("SavedUserInfo", ApplicationConstants.ERROROCCURED);
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			e.printStackTrace();
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView("conspreferencesuserinfo");
	}
}

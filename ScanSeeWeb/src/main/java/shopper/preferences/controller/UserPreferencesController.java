package shopper.preferences.controller;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.preferences.service.PreferencesService;
import shopper.service.ShopperService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.City;
import common.pojo.PreferencesInfo;
import common.pojo.UserPreference;
import common.pojo.Users;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.CountryDetails;
import common.pojo.shopper.CreateOrUpdateUserPreference;
import common.pojo.shopper.UserInfo;
import common.pojo.shopper.UserRegistrationInfo;
import common.util.Utility;

@Controller
public class UserPreferencesController
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(UserPreferencesController.class);

	/**
	 * This method is used to get the details of the alerted and other products
	 * of user wish list Calls method in service layer. accepts a GET request in
	 * mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/preferencesmain.htm", method = RequestMethod.GET)
	public ModelAndView getUserPreferences(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getWishListProducts of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");
		String responseXML = null;

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info("Exited in getWishListProducts Get Method....");

		return new ModelAndView("preferencesmain");
	}

	/**
	 * This method is used to get the details of the alerted and other products
	 * of user wish list Calls method in service layer. accepts a GET request in
	 * mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/fetchusersettings.htm", method = RequestMethod.GET)
	public ModelAndView fetchUserSettings(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "fetchUserSettings of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");

		String responseXML = null;

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		try
		{
			CreateOrUpdateUserPreference createOrUpdateUserPreference = null;
			createOrUpdateUserPreference = preferencesService.fetchUserPreferences(userId);
			UserPreference userPreference = new UserPreference();
			if (createOrUpdateUserPreference != null)
			{
				Integer locRadius = createOrUpdateUserPreference.getLocationRadius();
				request.setAttribute("userLocRadius", locRadius);
				userPreference.setLocaleRadius(locRadius.longValue());

			}
			model.put("preferencesusersettings", userPreference);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info("Exited in getWishListProducts Get Method....");

		return new ModelAndView("preferencesusersettings");
	}

	/**
	 * This method is used to get the details of the alerted and other products
	 * of user wish list Calls method in service layer. accepts a GET request in
	 * mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/saveusersettings.htm", method = RequestMethod.POST)
	public ModelAndView saveUserSettings(@ModelAttribute("preferencesusersettings") UserPreference userPreference, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "fetchUserSettings of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");

		String responseXML = null;

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		try
		{
			userPreference.setUserID(userId);
			String response = preferencesService.saveUserPreference(userPreference);
			if (response.equals(ApplicationConstants.SUCCESS))
			{
				request.setAttribute("RadiusSaved", ApplicationConstants.UPDATELOCALEUSRRADIUS);
			}
			else
			{
				request.setAttribute("RadiusSaved", ApplicationConstants.ERROROCCURED);
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info("Exited in getWishListProducts Get Method....");

		return new ModelAndView("preferencesusersettings");
	}

	/**
	 * This method is used to get the details of the alerted and other products
	 * of user wish list Calls method in service layer. accepts a GET request in
	 * mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/fetchuserinfo.htm", method = RequestMethod.GET)
	public ModelAndView fetchUserInfo(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "fetchUserSettings of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");

		String responseXML = null;

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		try
		{
			UserInfo userInfoObj = null;
			UserRegistrationInfo userRegistrationInfo = new UserRegistrationInfo();
			userInfoObj = preferencesService.fetchUserInfo(userId);

			if (userInfoObj != null)
			{
				userRegistrationInfo.setUserId(userId);
				userRegistrationInfo.setFirstName(userInfoObj.getUpdateUserInfo().getFirstName());
				userRegistrationInfo.setLastName(userInfoObj.getUpdateUserInfo().getLastName());
				userRegistrationInfo.setAddress1(userInfoObj.getUpdateUserInfo().getAddress1());
				userRegistrationInfo.setEmail(userInfoObj.getUpdateUserInfo().getEmail());
				userRegistrationInfo.setAddress1(userInfoObj.getUpdateUserInfo().getAddress1());
				userRegistrationInfo.setAddress2(userInfoObj.getUpdateUserInfo().getAddress2());
									
				if(userInfoObj.getUpdateUserInfo().getNumberOfChildren()!=null && userInfoObj.getUpdateUserInfo().getNumberOfChildren() != " " )
				{
					Integer nofchildren=Integer.parseInt(userInfoObj.getUpdateUserInfo().getNumberOfChildren());
							userRegistrationInfo.setChildren(nofchildren);
							
				}
				
				
				userRegistrationInfo.setPostalCode(userInfoObj.getUpdateUserInfo().getPostalCode());
				userRegistrationInfo.setMobileNumber(userInfoObj.getUpdateUserInfo().getMobileNumber());
				userRegistrationInfo.setGender(Integer.valueOf(userInfoObj.getUpdateUserInfo().getGender()));
				userRegistrationInfo.setDob(userInfoObj.getUpdateUserInfo().getDob());
				if(null != userInfoObj.getUpdateUserInfo().getSelectedCountryId()){
				userRegistrationInfo.setCountryID(Integer.valueOf(userInfoObj.getUpdateUserInfo().getSelectedCountryId()));
				}
				userRegistrationInfo.setCity(userInfoObj.getUpdateUserInfo().getCity());
				userRegistrationInfo.setState(userInfoObj.getUpdateUserInfo().getState());
				// for lists

				if (userInfoObj.getUpdateUserInfo().getEducationLevelId() != null && !userInfoObj.getUpdateUserInfo().getEducationLevelId().isEmpty()
						&& !userInfoObj.getUpdateUserInfo().getEducationLevelId().equals(" "))
				{
					userRegistrationInfo.setEducationLevelId(Integer.valueOf(userInfoObj.getUpdateUserInfo().getEducationLevelId()));
				}
				if(null != userInfoObj.getUpdateUserInfo().getHomeOwner() ){
				userRegistrationInfo.setHomeOwner(Integer.valueOf(userInfoObj.getUpdateUserInfo().getHomeOwner()));
				}
				userRegistrationInfo.setMaritalStatus(userInfoObj.getUpdateUserInfo().getMaritalStatus());
				if (userInfoObj.getUpdateUserInfo().getSelectedIncomeRangeID() != null && userInfoObj.getUpdateUserInfo().getSelectedIncomeRangeID() != " ")
				{
					userRegistrationInfo.setIncomeRangeID(Integer.valueOf(userInfoObj.getUpdateUserInfo().getSelectedIncomeRangeID()));
				}
				request.setAttribute("userdetails", userInfoObj);
				session.setAttribute("matitalstatuslst", userInfoObj.getMaritalStatusDetail());
				session.setAttribute("incomerangelst", userInfoObj.getIncomeRanges());
				session.setAttribute("educationlst", userInfoObj.getEducationalLevelDetail());
				session.setAttribute("statlst", userInfoObj);
				session.setAttribute("countrylst", userInfoObj.getCountryCodes());
				request.setAttribute("userId", userId);

				/*
				 * session.setAttribute("countrylst",
				 * userInfoObj.getCountryCodes());
				 * session.setAttribute("statlst", userInfoObj.g());
				 * session.setAttribute("educationlst",
				 * userInfoObj.getEducationalLevelDetail());
				 * session.setAttribute("matitalstatuslst",
				 * userInfoObj.getMaritalStatusDetail());
				 * session.setAttribute("incomerangelst",
				 * userInfoObj.getIncomeRanges());
				 */

				// For fatching States and cities

				CountryDetails statescitisLst = null;

				statescitisLst = preferencesService.getAllStates();

				session.setAttribute("statelst", statescitisLst);

				String cityName = userInfoObj.getUpdateUserInfo().getCity();
				if (cityName != null)
				{
					request.setAttribute("userCity", cityName);
				}

				// statescitisLst = preferencesService.fetchAllCities("AK");
				// session.setAttribute("citieslst", statescitisLst);

			}
			model.put("preferencesuserinfo", userRegistrationInfo);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info("Exited in getWishListProducts Get Method....");

		return new ModelAndView("preferencesuserinfo");
	}

	/**
	 * This method is used to get the details of the alerted and other products
	 * of user wish list Calls method in service layer. accepts a GET request in
	 * mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/fetchuserinfo.htm", method = RequestMethod.POST)
	public ModelAndView saveUserInfo(@ModelAttribute("preferencesuserinfo") UserRegistrationInfo userRegistrationInfoObj, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "fetchUserSettings of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");

		String responseXML = null;

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		try
		{
			userRegistrationInfoObj.setUserId(userId);
			String response = preferencesService.insertUserInfo(userRegistrationInfoObj);

			if (response.equals(ApplicationConstants.SUCCESS))
			{
				request.setAttribute("SavedUserInfo", ApplicationConstants.SAVEUSERINFORMATION);
			}
			else
			{
				request.setAttribute("SavedUserInfo", ApplicationConstants.ERROROCCURED);
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info("Exited in getWishListProducts Get Method....");

		return new ModelAndView("preferencesuserinfo");
	}

	@RequestMapping(value = "/usrGetCity", method = RequestMethod.GET)
	public @ResponseBody
	String getCities(@RequestParam(value = "stateName", required = true) String stateCode,
			@RequestParam(value = "city", required = true) String city, HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		ArrayList<City> cities = null;
		StringBuffer innerHtml = new StringBuffer();
		String finalHtml = "<select name='city' id='City'><option value='0'>--Select--</option>";

		innerHtml.append(finalHtml);

		try
		{

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");
			cities = preferencesService.fetchAllCities(stateCode);
			response.setContentType("text/xml");
			/*
			 * for (int i = 0; i < cities.size(); i++) {
			 * innerHtml.append("<option value=" + "'" +
			 * cities.get(i).getGeoPositionID() + "'>" +
			 * cities.get(i).getCityName() + "</option>"); }
			 * innerHtml.append("</select>");
			 */

			String selCity = (String) request.getAttribute("userCity");
			if (selCity == null || selCity == "")
			{
				selCity = city;
			}
			for (int i = 0; i < cities.size(); i++)
			{
				String cityName = cities.get(i).getCityName();
				if (null != selCity && cityName.equals(selCity))
				{
					innerHtml.append("<option selected=true >" + cities.get(i).getCityName() + "</option>");
				}
				else
				{
					innerHtml.append("<option>" + cities.get(i).getCityName() + "</option>");
				}
			}
			innerHtml.append("</select>");

		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return innerHtml.toString();
	}

	@RequestMapping(value = "/updateUsrPwd.htm", method = RequestMethod.GET)
	public @ResponseBody
	String updateUserPassword(@RequestParam(value = "userId", required = true) Long userId,
			@RequestParam(value = "password", required = true) String password,

			HttpServletRequest request, HttpServletResponse response, HttpSession session)

	{
		final String methodName = "updateUserPassword";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String daoResponse = null;

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			PreferencesService preferencesService = (PreferencesService) appContext.getBean("preferencesService");

			daoResponse = preferencesService.changePassword(userId, password);

			if (daoResponse.equalsIgnoreCase(ApplicationConstants.SUCCESS))
				;
			{
				daoResponse = ApplicationConstants.PASSWORDUPDATED;
			}

		}
		catch (Exception exception)
		{
			LOG.error("Error occureed in updateUsrPwd.htm", exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

	@RequestMapping(value = "/userPreferences.htm", method = RequestMethod.GET)
	public ModelAndView showPage(@ModelAttribute("preferencesusrcategories") CategoryDetail categoryDetail, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{

		LOG.info("Inside PreferencesController : showPage ");

		PreferencesInfo preferencesInfo = new PreferencesInfo();

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");

		Integer lowerlimit = null;
		CategoryDetail categoryDetailresponse = null;
		Boolean objBoolEmail = new Boolean(false);
		Boolean objBoolCell = new Boolean(false);
		boolean bEmail = false;
		boolean bCell = false;
		String strcellStatus = null;
		String strEmailStatus = null;

		strEmailStatus = (String) session.getAttribute("emailStatus");
		if (!Utility.checkNull(strEmailStatus).equals(""))
		{
			objBoolEmail = Boolean.valueOf(strEmailStatus);
			bEmail = objBoolEmail.booleanValue();
			LOG.info("Inside PreferencesController : showPage : objBoolEmail : " + objBoolEmail + " : bEmail : " + bEmail);
		}

		strcellStatus = (String) session.getAttribute("cellPhoneStatus");
		if (!Utility.checkNull(strcellStatus).equals(""))
		{
			objBoolCell = Boolean.valueOf(strcellStatus);
			bCell = objBoolCell.booleanValue();
			LOG.info("Inside PreferencesController : showPage : objBoolCell : " + objBoolCell + " : bCell : " + bCell);
		}

		boolean flag = false;
		try
		{
			Users user = (Users) session.getAttribute("loginuser");
			Long shopperId = Long.valueOf(user.getUserID());

			if (shopperId != null)
			{
				categoryDetailresponse = shopperService.getUserFavCategories(shopperId, lowerlimit);
				if (bEmail)
				{
					flag = true;
					categoryDetail.setEmail(flag);
					LOG.info("Inside PreferencesController : showPage : categoryDetailresponse.setEmail(flag) : " + categoryDetailresponse.isEmail());
				}
				if (bCell)
				{
					flag = true;
					categoryDetail.setCellPhone(flag);
					LOG.info("Inside PreferencesController : showPage : categoryDetailresponse.setCellPhone(flag) : "
							+ categoryDetailresponse.isCellPhone());
				}
				session.setAttribute("categoryDetailresponse", categoryDetailresponse);
			}
		}
		catch (ScanSeeServiceException e)
		{
			e.printStackTrace();
		}
		return new ModelAndView("preferencesusrcategories");

	}

	@RequestMapping(value = "/userPreferences.htm", method = RequestMethod.POST)
	public ModelAndView preferencesSubmit(@ModelAttribute("preferencesusrcategories") CategoryDetail categoryDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
	{
		LOG.info("Inside PreferencesController : preferencesSubmit");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		request.getParameter("mainCategoryId");
		String response = null;

		String favId = null;
		CategoryDetail categoryDetailresponse = null;

		PreferencesInfo alertpreferencesInfo = new PreferencesInfo();
		Integer lowerlimit = null;
		try
		{
			session.removeAttribute("emailStatus");
			session.removeAttribute("cellPhoneStatus");
			Users loginUser = (Users) session.getAttribute("loginuser");
			Long userId = loginUser.getUserID();
			favId = categoryDetail.getFavCatId();

			if (null != categoryDetail)
			{
				if (categoryDetail.isEmail() == true)
				{
					alertpreferencesInfo.setEmailStatus("Yes");
					alertpreferencesInfo.setEmail(true);
					boolean bEmail = categoryDetail.isEmail();
					String strIsEmail = new Boolean(bEmail).toString();
					session.setAttribute("emailStatus", strIsEmail);
				}
				else
				{
					alertpreferencesInfo.setEmailStatus("No");
				}
				if (categoryDetail.isCellPhone() == true)
				{

					alertpreferencesInfo.setCellPhoneStatus("Yes");
					alertpreferencesInfo.setCellPhone(true);
					boolean bCell = categoryDetail.isCellPhone();
					String strIsCell = new Boolean(bCell).toString();
					session.setAttribute("cellPhoneStatus", strIsCell);
				}
				else
				{
					alertpreferencesInfo.setCellPhoneStatus("No");
				}
			}
			response = shopperService.setUserFavCategories(favId, userId, categoryDetail.isCellPhone(),categoryDetail.isEmail());

			if (response.equals(ApplicationConstants.SUCCESS))
			{
				request.setAttribute("UpdatePreferedCategories", ApplicationConstants.SETUSERFAVCATEGORIES);
			}
			else
			{
				request.setAttribute("UpdatePreferedCategories", ApplicationConstants.ERROROCCURED);
			}

			if (response != null)

				if (userId != null)
				{
					categoryDetailresponse = shopperService.getUserFavCategories(userId, lowerlimit);
					session.setAttribute("categoryDetailresponse", categoryDetailresponse);
				}

		}
		catch (ScanSeeServiceException e)
		{
			e.printStackTrace();
		}

		/*
		 * session.setAttribute("alertpreferencesInfo", alertpreferencesInfo);
		 * return new ModelAndView(new RedirectView("selectschool.htm"));
		 */

		return new ModelAndView("preferencesusrcategories");
	}
}

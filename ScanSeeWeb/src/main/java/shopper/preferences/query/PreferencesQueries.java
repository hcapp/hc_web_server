package shopper.preferences.query;
/**
 * This class defined for shopper preferences queries.
 * @author shyamsundara_hm
 *
 */
public class PreferencesQueries
{

	
	public static final String FETCHUSERPREFERENCEQUERY = " select up.LocaleRadius locationRadius,up.ScannerSilent, up.DisplayCoupons,up.DisplayLoyaltyRewards,up.DisplayFieldAgent,up.FieldAgentRadius,up.SavingsActivated "
		+ ", up.SleepStatus  sleepStatus, up.DisplayRebates displayRebates from [Users] u join [UserPreference] up on u.UserID = up.UserID where u.UserID= ?";
	
	
	/**
	 * The query for fetchIncomeRanges.
	 */
	public static final String FETCHINCOMERANGESQUERY = "select IncomeRangeID,IncomeRange from IncomeRange";
	
	/**
	 * The query for fetch Country Codes.
	 */
	public static final String FETCHCOUNTRYCODESQUERY = "select CountryID,CountryName from CountryCode where Active = 1";
	
	/**
	 * The query for fetch EducationalLevel.
	 */
	public static final String FETCHEDUCATIONALLEVELQUERY = "SELECT EducationLevelID as educationalLevelId, EducationLevelDescription as educationalLevelDesc FROM EducationLevel";
	
	/**
	 * The query for fetch MaritalStatus.
	 */
	public static final String FETCHMARITALSTATUSQUERY = "SELECT MaritalStatusID as maritalStatusId  , MaritalStatusName maritalStatusName FROM MaritalStatus";
	
	/**
	 * The query for fetch State.
	 */
	public static final String FETCHSTATEQUERY = "SELECT DISTINCT State stateName FROM GeoPosition ORDER BY state";
	
	/**
	 * The query for fetch City.
	 */
	public static final String FETCHCITYQUERY = "SELECT DISTINCT city cityName FROM GeoPosition WHERE State=? order by city";
	
	/**
	 * The query for Population Center cities.
	 */
	public static final String POPULATIONCENTER = "SELECT DISTINCT City cityName FROM GeoPosition WHERE PopulationCenterFlag = 1 ORDER BY City";
	
	/**
	 * The query for Population research Center cities.
	 */
	public static final String POPULATIONCENTERSEARCHCITY = "SELECT DISTINCT City cityName FROM GeoPosition WHERE PopulationCenterFlag = 1 and City like ? ORDER BY City";
}

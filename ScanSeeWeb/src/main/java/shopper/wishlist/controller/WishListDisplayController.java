package shopper.wishlist.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.service.ShopperService;
import shopper.shoppingList.service.ShoppingListService;
import shopper.wishlist.service.WishListService;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.Offer;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShoppingListRequest;
import common.pojo.shopper.WishListHistoryDetails;
import common.pojo.shopper.WishListProducts;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is a controller class for wish list screens.
 * 
 * @author shyamsundara_hm
 */
@Controller
public class WishListDisplayController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(WishListDisplayController.class);

	/**
	 * This method is used to get the details of the alerted and other products
	 * of user wish list Calls method in service layer. accepts a GET request in
	 * mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/wishlistdisplay.htm", method = RequestMethod.GET)
	public ModelAndView getWishListProducts(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getWishListProducts of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		session.removeAttribute("retailerNameProductSum");
		session.removeAttribute("salepriceProductSum");
		session.removeAttribute("objAreaInfoVO");
		session.removeAttribute("favourite");

		session.setAttribute("WishListMainPage", "WishListMainPage");

		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

		WishListService wishListService = (WishListService) appContext.getBean("wishListService");
		String responseXML = null;

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		WishListProducts wishListProducts = new WishListProducts();
		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		objAreaInfoVO.setUserID(userId);
		// objAreaInfoVO.setUserID((long) 2);

		try
		{
			wishListProducts = wishListService.getWishListItems(objAreaInfoVO);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		if (wishListProducts != null)
		{
			session.setAttribute("wishlistproducts", wishListProducts);

		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info("Exited in getWishListProducts Get Method....");
		session.setAttribute("fromWL", "WLMain");
		session.removeAttribute("FromSLSearch");
		session.removeAttribute("fromSLFavSearch");
		session.removeAttribute("FromWLSearch");
		session.removeAttribute("prodSearch");
		session.removeAttribute("fromFind");
		session.removeAttribute("fromscannow");
		session.removeAttribute("fromSL");
		session.removeAttribute("fromSLHistory");
		session.removeAttribute("fromSLFAV");
		session.removeAttribute("fromWLHistory");
		return new ModelAndView("wishlistdisplay");
	}

	/**
	 * This method is used to get the details of the alerted and other products
	 * of user wish list based on user location attributes Calls method in
	 * service layer. accepts a GET request in mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/wishlistdisplay.htm", method = RequestMethod.POST)
	public ModelAndView getWishListProductsDisplay(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getWishListProductsDisplay of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();

		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

		WishListService wishListService = (WishListService) appContext.getBean("wishListService");
		String responseXML = null;

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		WishListProducts wishListProducts = new WishListProducts();
		session.removeAttribute("wishlistproducts");
		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		String zipcode = request.getParameter("zipcodeWL");
		String cityNameTL = request.getParameter("cityNameTL");
		objAreaInfoVO.setUserID(userId);
		Double lattitude = null;
		Double longitude = null;
		// objAreaInfoVO.setUserID((long) 2);

		if (zipcode != null && !"".equals(zipcode))
		{
			objAreaInfoVO.setZipCode(zipcode);
		}
		else if (cityNameTL != null && !"".equals(cityNameTL))
		{
			if (!"".equals(request.getParameter("latitude")) && null != request.getParameter("latitude"))
			{
				lattitude = Double.parseDouble(request.getParameter("latitude"));
			}
			if (!"".equals(request.getParameter("longitude")) && null != request.getParameter("longitude"))
			{
				longitude = Double.parseDouble(request.getParameter("longitude"));
			}

			objAreaInfoVO.setCityNameTL(cityNameTL);
			objAreaInfoVO.setLattitude(lattitude);
			objAreaInfoVO.setLongitude(longitude);

		}
		else if ((AreaInfoVO) session.getAttribute("objAreaInfoVO") != null)
		{
			AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
			objAreaInfoVO.setZipCode(objAreaInfoVO1.getZipCode());
			objAreaInfoVO.setCityNameTL(objAreaInfoVO1.getCityNameTL());
			objAreaInfoVO.setLattitude(objAreaInfoVO1.getLattitude());
			objAreaInfoVO.setLongitude(objAreaInfoVO1.getLongitude());
		}
		objAreaInfoVO.setUserID(userId);

		try
		{
			wishListProducts = wishListService.getWishListItems(objAreaInfoVO);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		if (wishListProducts != null)
		{
			session.setAttribute("wishlistproducts", wishListProducts);

		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info("Exited in getWishListProductsDisplay Post Method....");
		session.setAttribute("objAreaInfoVO", objAreaInfoVO);
		request.setAttribute("zipcode", zipcode);
		request.setAttribute("cityNameTL", cityNameTL);
		session.setAttribute("fromWL", "WLMain");
		session.removeAttribute("FromSLSearch");
		session.removeAttribute("fromSLFavSearch");
		session.removeAttribute("FromWLSearch");
		session.removeAttribute("prodSearch");
		session.removeAttribute("fromFind");
		session.removeAttribute("fromscannow");
		session.removeAttribute("fromSL");
		session.removeAttribute("fromSLHistory");
		session.removeAttribute("fromSLFAV");
		session.removeAttribute("fromWLHistory");
		return new ModelAndView("wishlistdisplay");
	}

	/**
	 * The method for displaying wish list history products . Calls method in
	 * service layer. accepts a GET request in mime type text/plain.
	 * 
	 * @param userId
	 *            as request parameter
	 * @param lowerlimit
	 *            as request parameter
	 * @return returns response.
	 */
	@RequestMapping(value = "/wishlisthistorydisplay.htm", method = RequestMethod.GET)
	public String fetchWishListHistory(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "fetchWishListHistory";

		LOG.info(ApplicationConstants.METHODSTART + methodName);
		WishListHistoryDetails wishListHistDetails = new WishListHistoryDetails();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		String pageFlag = (String) request.getParameter("pageFlag");
		session.removeAttribute("wishlistproducts");

		AreaInfoVO objAreaInfoVO = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO == null)
		{
			objAreaInfoVO = new AreaInfoVO();
		}

		String zipcode = request.getParameter("zipcodeTL");
		String cityNameTL = request.getParameter("cityNameTL");
		objAreaInfoVO.setUserID(userId);
		Double lattitude=null;
		Double longitude=null;
		if (zipcode != null && !"".equals(zipcode))
		{
			objAreaInfoVO.setZipCode(zipcode);
		}else if (cityNameTL != null && !"".equals(cityNameTL))
		{
			if (!"".equals(request.getParameter("latitude")) && null != request.getParameter("latitude"))
			{
				lattitude = Double.parseDouble(request.getParameter("latitude"));
			}
			if (!"".equals(request.getParameter("longitude")) && null != request.getParameter("longitude"))
			{
				longitude = Double.parseDouble(request.getParameter("longitude"));
			}

			objAreaInfoVO.setCityNameTL(cityNameTL);
			objAreaInfoVO.setLattitude(lattitude);
			objAreaInfoVO.setLongitude(longitude);

		}
		else if ((AreaInfoVO) session.getAttribute("objAreaInfoVO") != null)
		{

			AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
			objAreaInfoVO.setZipCode(objAreaInfoVO1.getZipCode());
			objAreaInfoVO.setCityNameTL(objAreaInfoVO1.getCityNameTL());
			objAreaInfoVO.setLattitude(objAreaInfoVO1.getLattitude());
			objAreaInfoVO.setLongitude(objAreaInfoVO1.getLongitude());
		
		}
		

		String wishListMainPage = (String) session.getAttribute("WishListMainPage");

		if (null != wishListMainPage)
		{
			session.setAttribute("WishListMainPage", "WishListMainPage");
		}

		if (null != pageFlag && pageFlag.equals("true"))
		{
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data : userId:" + userId);
		}
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		WishListService wishListService = (WishListService) appContext.getBean("wishListService");
		try
		{
			wishListHistDetails = wishListService.fetchWishListHistoryDetails(userId, lowerLimit);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		request.setAttribute("wishListHistDetails", wishListHistDetails);
		if (wishListHistDetails != null)
		{
			Pagination objPage = Utility.getPagination(wishListHistDetails.getTotalSize(), currentPage, "wishlisthistorydisplaypag.htm");
			session.setAttribute("pagination", objPage);
		}

		session.setAttribute("objAreaInfoVO", objAreaInfoVO);
		request.setAttribute("zipcode", zipcode);
		request.setAttribute("cityNameTL", cityNameTL);
		session.setAttribute("fromWLHistory", "WLHistory");
		session.removeAttribute("FromSLSearch");
		session.removeAttribute("fromSLFavSearch");
		session.removeAttribute("FromWLSearch");
		session.removeAttribute("prodSearch");
		session.removeAttribute("fromFind");
		session.removeAttribute("fromscannow");
		session.removeAttribute("fromSL");
		session.removeAttribute("fromSLHistory");
		session.removeAttribute("fromSLFAV");
		session.removeAttribute("fromWL");
		return "wishlisthistorydisplay";

	}

	/**
	 * The method for displaying wish list history products . Calls method in
	 * service layer. accepts a GET request in mime type text/plain.
	 * 
	 * @param userId
	 *            as request parameter
	 * @param lowerlimit
	 *            as request parameter
	 * @return returns response.
	 */
	@RequestMapping(value = "/wishlisthistorydisplaypag.htm", method = RequestMethod.POST)
	public String fetchWishListHistoryPagination(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "fetchWishListHistory";

		LOG.info(ApplicationConstants.METHODSTART + methodName);
		WishListHistoryDetails wishListHistDetails = new WishListHistoryDetails();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		String pageFlag = (String) request.getParameter("pageFlag");

		if (null != pageFlag && pageFlag.equals("true"))
		{
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data : userId:" + userId);
		}
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		WishListService wishListService = (WishListService) appContext.getBean("wishListService");
		try
		{
			wishListHistDetails = wishListService.fetchWishListHistoryDetails(userId, lowerLimit);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		request.setAttribute("wishListHistDetails", wishListHistDetails);
		if (wishListHistDetails != null)
		{
			Pagination objPage = Utility.getPagination(wishListHistDetails.getTotalSize(), currentPage, "wishlisthistorydisplaypag.htm");
			session.setAttribute("pagination", objPage);
		}
		return "wishlisthistorydisplay";

	}

	/**
	 * This method is used to get the details of wish list alerted hot deal
	 * details Calls method in service layer. accepts a GET request in mime type
	 * text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/wlhotdealproductinfo.htm", method = RequestMethod.POST)
	public ModelAndView getWLhotDealProdInfo(@ModelAttribute("wishlistdisplay") ProductDetail productDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getWLhotDealProdInfo of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

		WishListService wishListService = (WishListService) appContext.getBean("wishListService");

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		HotDealsDetails hotDealsDetails = new HotDealsDetails();
		AreaInfoVO objAreaInfoVO = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO == null)
		{
			objAreaInfoVO = new AreaInfoVO();
		}
		/*
		 * if(objAreaInfoVO != null) { if( objAreaInfoVO.getZipCode() !=null) {
		 * zipcode = objAreaInfoVO.getZipCode(); }else
		 * if(objAreaInfoVO.getLattitude() != null &&
		 * objAreaInfoVO.getLongitude()!=null) {
		 * objAreaInfoVO.setLattitude(lattitude);
		 * objAreaInfoVO.setLongitude(longitude); } }
		 */

		objAreaInfoVO.setUserID(userId);
		// objAreaInfoVO.setUserID((long) 2);
		objAreaInfoVO.setProductId(productDetail.getProductId());

		try
		{
			hotDealsDetails = wishListService.getHotDealInfoForProduct(objAreaInfoVO);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (hotDealsDetails != null)
		{
			request.setAttribute("hotDealsDetails", hotDealsDetails);

		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:");
		}
		LOG.info("Exited in getWLhotDealProdInfo Get Method....");

		return new ModelAndView("wishlistdisplay");
	}

	/**
	 * This method is used to get the details of wish list alerted product local
	 * store details Calls method in service layer. accepts a GET request in
	 * mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/wllocalstores.htm", method = RequestMethod.POST)
	public ModelAndView getWLLocalStrores(@ModelAttribute("wishlistdisplay") ProductDetail productDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getWLLocalStrores of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		session.removeAttribute("WishListMainPage");
		WishListService wishListService = (WishListService) appContext.getBean("wishListService");
		RetailersDetails retailersDetails = new RetailersDetails();
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		AreaInfoVO objAreaInfoVO = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO == null)
		{
			objAreaInfoVO = new AreaInfoVO();
		}
		objAreaInfoVO.setUserID(userId);
		// objAreaInfoVO.setUserID((long) 2);

		Integer productId = Integer.valueOf(request.getParameter("productId"));

		request.setAttribute("productId", productId);

		if (productId != null)
		{
			objAreaInfoVO.setProductId(productId);
		}

		try
		{
			retailersDetails = wishListService.getAllWishListStoreDetails(objAreaInfoVO);

			if (retailersDetails != null)
			{
				request.setAttribute("retailersDetails", retailersDetails);

			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:");
		}
		LOG.info("Exited in getWLLocalStrores Get Method....");

		return new ModelAndView("wishlistdisplay");
	}

	/**
	 * The method for Deleting wish list item. Calls method in service layer.
	 * accepts a POST request in mime type text/xml.
	 * 
	 * @param xml
	 *            input request
	 * @return returns response XML based on Success or Error.
	 */
	@RequestMapping(value = "/wishlistdelete.htm", method = RequestMethod.GET)
	public @ResponseBody
	String deleteWishListItem(@RequestParam(value = "userProductId", required = true) Integer userProductId, HttpServletRequest request,
			HttpSession session)
	{
		final String methodName = "deleteWishListItem";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		WishListService wishListService = (WishListService) appContext.getBean("wishListService");
		session.setAttribute("WishListMainPage", "WishListMainPage");
		try
		{
			response = wishListService.deleteWishListItem(userId, userProductId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This method is used to get the details of wish list alerted coupon
	 * details Calls method in service layer. accepts a GET request in mime type
	 * text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/wlcouponproductinfo.htm", method = RequestMethod.POST)
	public ModelAndView getWLCouponInfo(@ModelAttribute("wishlistdisplay") ProductDetail productDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getWLCouponInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		session.removeAttribute("wishlistproducts");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

		WishListService wishListService = (WishListService) appContext.getBean("wishListService");

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		ArrayList<CouponDetails> couponArrayList = null;
		AreaInfoVO objAreaInfoVO = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO == null)
		{
			objAreaInfoVO = new AreaInfoVO();
		}
		objAreaInfoVO.setUserID(userId);
		// objAreaInfoVO.setUserID((long) 2);

		objAreaInfoVO.setProductId(productDetail.getProductId());
		try
		{
			couponArrayList = wishListService.fetchCouponDetails(objAreaInfoVO);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (couponArrayList != null)
		{
			request.setAttribute("couponlist", couponArrayList);
			if (productDetail.getProductImagePath() != null)
			{
				request.setAttribute("productImage", productDetail.getProductImagePath());
			}

		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:");
		}
		LOG.info("Exited in getWLCouponInfo Post Method....");

		return new ModelAndView("wishlistdisplay");
	}

	/**
	 * This method is used to add the wish list product coupons Calls method in
	 * service layer. accepts a GET request in mime type text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/wlcouponOperation", method = RequestMethod.GET)
	public @ResponseBody
	String addRemoveCoupon(@RequestParam(value = "couponId", required = true) Integer couponId,
			@RequestParam(value = "usage", required = true) String usage, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{
		final String methodName = "addRemoveCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		StringBuffer innerHtml = new StringBuffer();

		try
		{

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");

			String daoResponse = null;

			Users loginUser = (Users) session.getAttribute("loginuser");

			Long userId = loginUser.getUserID();
			// Long userId = (long) 2;

			if (usage.equalsIgnoreCase("Red"))
			{
				daoResponse = shopperService.couponAdd(userId, couponId);
				innerHtml.append("<img src='/ScanSeeWeb/images/cpnIcon.png' alt='Cpn' width='27' height='25' id = 'cpnTg" + couponId
						+ "' name= 'cpnTg" + couponId + "' onclick='wlcouponOperation(" + couponId + " ,\"Green\" , \"cpnTg" + couponId + "\")'/>");

			}
			else if (usage.equalsIgnoreCase("Green"))
			{
				daoResponse = shopperService.couponRemove(userId, couponId);
				innerHtml.append("<img src='/ScanSeeWeb/images/cpnAddicon.png' alt='Cpn' width='27' height='25'  id = 'cpnTg" + couponId
						+ "' name= 'cpnTg" + couponId + "'  onclick='wlcouponOperation(" + couponId + " , \"Red\" , \"cpnTg" + couponId + "\")'/>");

			}

		}
		catch (ScanSeeServiceException exception)
		{

			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		LOG.info("Controller Layer::wish list addRemoveCoupon Exiting Get Method");

		return innerHtml.toString();
	}

	/**
	 * This method is used to get the details of wish list alerted coupon
	 * details Calls method in service layer. accepts a GET request in mime type
	 * text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/addWLProductBySearch", method = RequestMethod.POST)
	public String addWLProductBySearch(@ModelAttribute("ShoppingList") ShoppingListRequest ShoppingList, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{

		final String methodName = "addWLProductBySearch of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

		WishListService wishListService = (WishListService) appContext.getBean("wishListService");
		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		ProductDetails productDetails = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		String view = null;
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		ArrayList<CouponDetails> couponArrayList = null;

		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		objAreaInfoVO.setUserID(userId);
		String response = null;
		objAreaInfoVO.setProductId(ShoppingList.getSelectedProductId());
		try
		{
			response = wishListService.addWishListProd(objAreaInfoVO);

			if (response.equals(ApplicationConstants.PRODUCTEXISTS))
			{
				if (null != pageFlag && "true".equalsIgnoreCase(pageFlag))
				{
					pageNumber = request.getParameter("pageNumber");
					Pagination pageSess = (Pagination) session.getAttribute("pagination");
					if (Integer.valueOf(pageNumber) != 0)
					{
						currentPage = Integer.valueOf(pageNumber);
						int number = Integer.valueOf(currentPage) - 1;
						int pageSize = pageSess.getPageRange();
						lowerLimit = pageSize * Integer.valueOf(number);
					}
					else
					{
						lowerLimit = 0;
					}

				}
				ServletContext servletContext2 = request.getSession().getServletContext();
				WebApplicationContext appContext2 = WebApplicationContextUtils.getWebApplicationContext(servletContext2);
				ShoppingListService shoppingListService = (ShoppingListService) appContext2.getBean("shoppingList");
				productDetails = shoppingListService.scanSearchProductForName(ShoppingList, lowerLimit);
				if (null != productDetails)
				{
					Pagination objPage = Utility.getPagination(productDetails.getProductDetail().size(), currentPage, "searchShoppingList.htm");
					session.setAttribute("pagination", objPage);

				}
				request.setAttribute("duplicateprodtxt", ApplicationConstants.DUPLICATPRODUCTTEXT);
				request.setAttribute("productDetails", productDetails);
				LOG.info("Product Exists Already.");
				ShoppingList.setIsWishlst(true);

				request.setAttribute("FromWishlist", ShoppingList.isIsWishlst());

				view = "searchShoppingList";
			}
			else
			{

				WishListProducts wishListProducts = new WishListProducts();

				objAreaInfoVO.setUserID(ShoppingList.getUserId());
				wishListProducts = wishListService.getWishListItems(objAreaInfoVO);
				session.setAttribute("wishlistproducts", wishListProducts);
				view = "wishlistdisplay";
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return view;
	}

	@RequestMapping(value = "/wlonlinestores.htm", method = RequestMethod.POST)
	public ModelAndView getWLOnlineStrores(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		final String methodName = "getWLOnlineStrores";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		WishListService wishListService = (WishListService) appContext.getBean("wishListService");
		String view = null;
		session.removeAttribute("WishListMainPage");
		ExternalAPIInformation apiInfo = null;
		List<Offer> onlineStoresList = null;

		ProductDetail productDet = null;
		OnlineStoresRequest onlineStores = null;
		OnlineStoresService onlineStoresService = null;
		OnlineStores onlineStoresinfo = null;

		try
		{

			Users loginUser = (Users) session.getAttribute("loginuser");

			final String userId = loginUser.getUserID().toString();

			String productId = request.getParameter("productId");
			LOG.info("userId*************" + userId);
			LOG.info("productId*************" + productId);
			// online stores
			apiInfo = wishListService.externalApiInfo("FindOnlineStores");

			productDet = wishListService.fetchProductDetails(productId);
			// productDet = wishListService.fetchProductDetails("25");

			onlineStoresService = new OnlineStoresServiceImpl();
			onlineStores = new OnlineStoresRequest();
			onlineStores.setPageStart("0");
			onlineStores.setUpcCode(productDet.getScanCode());
			onlineStores.setUserId(userId);
			onlineStores.setValues();
			onlineStoresinfo = onlineStoresService.onlineStoresInfoEmailTemplate(apiInfo, onlineStores);
			request.setAttribute("productId", productId);
			if (onlineStoresinfo != null)
			{
				if (!onlineStoresinfo.getOffers().isEmpty())
				{
					onlineStoresList = onlineStoresinfo.getOffers();
					request.setAttribute("wishListOnlineStoresList", onlineStoresList);

					view = "wishlistdisplay";
				}

			}

			request.setAttribute("online", "online");

		}
		catch (ScanSeeServiceException exception)
		{

			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info("ShopperProfileController :: Inside Exit Post Method");
		String zipcode = "";
		Integer radius = 0;
		AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO1 != null)
		{
			zipcode = objAreaInfoVO1.getZipCode();
			radius = objAreaInfoVO1.getRadius();
			request.setAttribute("zipcode", zipcode);
			request.setAttribute("radius", radius);
		}
		return new ModelAndView("wishlistdisplay");
	}

	/**
	 * This method is used to get the details of wish list alerted coupon
	 * details Calls method in service layer. accepts a GET request in mime type
	 * text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/addWLProduct", method = RequestMethod.GET)
	public @ResponseBody
	String addWLProduct(@RequestParam(value = "productId", required = true) Integer productId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{

		final String methodName = "addWLProduct of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		WishListService wishListService = (WishListService) appContext.getBean("wishListService");

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		String view = null;
		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		objAreaInfoVO.setUserID(userId);
		String sResponse = null;
		objAreaInfoVO.setProductId(productId);
		try
		{
			sResponse = wishListService.addWishListProd(objAreaInfoVO);

			if (sResponse.equals(ApplicationConstants.PRODUCTEXISTS))
			{

				sResponse = ApplicationConstants.DUPLICATPRODUCTTEXT;

			}
			else
			{

				sResponse = ApplicationConstants.PRODUCTADDEDSUCCESSTEXT;

			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return sResponse;
	}
}

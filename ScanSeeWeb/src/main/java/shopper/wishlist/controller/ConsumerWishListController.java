package shopper.wishlist.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import shopper.find.service.FindService;
import shopper.service.CommonService;

import com.scansee.externalapi.common.exception.ScanSeeExternalAPIException;
import com.scansee.externalapi.common.findnearby.pojos.FindNearByIntactResponse;
import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.Offer;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.Users;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.WishListHistoryDetails;
import common.pojo.shopper.WishListProducts;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is used to handle consumer Wish List module functionalities.
 * 
 * @author shyamsundara_hm
 */

@Controller
public class ConsumerWishListController
{

	/**
	 * LOG declared as logger instance,it will be used for logging.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ConsumerWishListController.class);

	@RequestMapping(value = "/conswishlisthome.htm", method = RequestMethod.GET)
	public ModelAndView getWishListHomeGet(@ModelAttribute("WishListHome") ProductDetail productDetail,HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
	{
		final String strMethodName = "getWishListHome";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strViewName = "conswishlisthome";
		ConsumerRequestDetails objConsumerRequestDetails = null;
		int intRecordCount = 20;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "wishlist";
		int intPageSize = 0;
		String strRedirectUrl = "conswishlisthome";
		String strPageNumber = "0";
		WishListProducts objWishListProducts = null;
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{
			session.removeAttribute("wishlistprods");
			session.removeAttribute("wlsearchprods");
			session.removeAttribute("consumerReqObj");
			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.WISHLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber)
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.WISHLISTMODULE, intRecordCount, strFrmSL, null);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objWishListProducts = objCommonService.getConsWishListProds(objConsumerRequestDetails);

			if (objWishListProducts != null)
			{
				session.setAttribute("wishlistprods", objWishListProducts);
				Pagination objPagination = Utility.getPagination(objWishListProducts.getTotalSize(), intCurrentPage,
						"/ScanSeeWeb/shopper/conswishlisthome.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);

			}
			if(null != productDetail && !"".equals(productDetail))
			{
				strViewName= productDetail.getScreenName();
			}
			
			if(null != strViewName && !"".equals(strViewName))
			{
				if(strViewName.equals("search"))
				{
					strRedirectUrl = "conswlsearchhome.htm";
					return new ModelAndView(new RedirectView("/ScanSeeWeb/shopper/"+strRedirectUrl));
				}
			}

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strRedirectUrl);

	}

	@RequestMapping(value = "/conswishlisthome.htm", method = RequestMethod.POST)
	public ModelAndView getWishListHomePagination(@ModelAttribute("WishListHome") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model)
	{
		final String strMethodName = "getWishListHome";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strViewName = "conswishlisthome";
		ConsumerRequestDetails objConsumerRequestDetails = null;
		
		
		
		int intRecordCount = 20;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "wishlist";
		int intPageSize = 0;
		String strRedirectUrl= "conswlsearchhome.htm";
		String strPageNumber = "0";
		WishListProducts objWishListProducts = null;
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{
			session.removeAttribute("wishlistprods");
			session.removeAttribute("consumerReqObj");
			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;

			if (null != productDetail)
			{
				intRecordCount = productDetail.getRecordCount();
				strViewName = productDetail.getScreenName();
			}
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.WISHLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber && !"0".equals(strPageNumber))
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.WISHLISTMODULE, intRecordCount, strFrmSL, null);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objWishListProducts = objCommonService.getConsWishListProds(objConsumerRequestDetails);

			if (objWishListProducts != null)
			{
				session.setAttribute("wishlistprods", objWishListProducts);

				Pagination objPagination = Utility.getPagination(objWishListProducts.getTotalSize(), intCurrentPage,
						"/ScanSeeWeb/shopper/conswishlisthome.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);

			}
			if(null != strViewName && !"".equals(strViewName))
			{
				if(strViewName.equals("history"))
				{
					strRedirectUrl = "conswlhistory.htm";
				}
				else{
					strRedirectUrl="conswlsearchhome.htm";
				}
			}

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(new RedirectView("/ScanSeeWeb/shopper/"+strRedirectUrl));

	}

	@RequestMapping(value = "/conswlsearchhome.htm", method = RequestMethod.GET)
	public ModelAndView consWishListSearchHome(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
	{
		final String methodName = "consWishListSearchHome";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = "conswishlisthome";
		session.removeAttribute("wlsearchprods");
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView(viewName);
	}

	@RequestMapping(value = "/conswlsearchprods.htm", method = RequestMethod.POST)
	public ModelAndView consWishListSearchProd(@ModelAttribute("WishListHome") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model)
	{
		final String strMethodName = "consWishListSearchProd";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ConsumerRequestDetails objConsumerRequestDetails = null;
		int intRecordCount = 50;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "shoplist";
		int intPageSize = 0;
		String strPageNumber = "0";
		String strSearchKey = null;
		ProductDetails objProductDetails = null;
		String strViewName = "conswishlisthome";
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{
			session.removeAttribute("wlsearchprods");
			session.removeAttribute("consumerReqObj");

			if (null != productDetail)
			{
				strSearchKey = productDetail.getSearchkey();

			}
			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.WISHLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			/*final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber)
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}*/
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.WISHLISTMODULE, intRecordCount, strFrmSL, strSearchKey);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objProductDetails = objCommonService.getConsShopSearchProd(objConsumerRequestDetails);

			if (objProductDetails != null)
			{
				session.setAttribute("wlsearchprods", objProductDetails);
				/*Pagination objPagination = Utility.getPagination(objProductDetails.getTotalSize(), intCurrentPage,
						"/ScanSeeWeb/shopper/conswlsearchprods.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);*/

			}else{
				request.setAttribute("norecord","true");
			}
			request.setAttribute("prodsearchkey", objConsumerRequestDetails.getSearchKey());

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		// model.put("ShoppingListHome",productDetail);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "/conswlhistory.htm", method = RequestMethod.GET)
	public ModelAndView getWLHistory(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
	{
		final String strMethodName = "conswlhistory";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		String strViewName = "conswlhistory";
		ConsumerRequestDetails objConsumerRequestDetails = null;
		int intRecordCount = 60;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "wishlist";
		int intPageSize = 0;
		String strPageNumber = "0";
		WishListHistoryDetails objWishListHistoryDetails = null;
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{
			session.removeAttribute("wlhistoryprods");
			session.removeAttribute("consumerReqObj");
			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.WISHLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber)
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.WISHLISTMODULE, intRecordCount, strFrmSL, null);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objWishListHistoryDetails = objCommonService.getConsWLHistoryProds(objConsumerRequestDetails);

			if (objWishListHistoryDetails != null)
			{
				session.setAttribute("wlhistoryprods", objWishListHistoryDetails);
				Pagination objPagination = Utility.getPagination(objWishListHistoryDetails.getTotalSize(), intCurrentPage,
						"/ScanSeeWeb/shopper/conswlhistory.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);

			}

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);

	}

	@RequestMapping(value = "/conswldeleteprod.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consDeleteWLProd(@RequestParam(value = "userprodid", required = true) String userProductId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "consDeleteWLProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdDelResponse = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					objProductDetails.setUserId(loginUserId);
				}

				if (null != userProductId && !"".equals(userProductId))
				{
					objProductDetails.setUsrProdId(userProductId);

				}
				objProductDetails = commonServiceObj.consDeleteWLProd(objProductDetails);

				if (objProductDetails.getResponse() != null)
				{
					if (objProductDetails.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						strProdDelResponse = ApplicationConstants.DELETEPRODUCTTEXT;
					}
					else
					{
						strProdDelResponse = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consDeleteTodayListProd", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdDelResponse;
	}

	@RequestMapping(value = "/conswlhistorydelprod", method = RequestMethod.GET)
	public @ResponseBody
	String consDeleteWLHistoryProd(@RequestParam(value = "userprodid", required = true) String userProductId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "consDeleteWLHistoryProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdDelResponse = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					objProductDetails.setUserId(loginUserId);
				}

				if (null != userProductId && !"".equals(userProductId))
				{
					objProductDetails.setUsrProdId(userProductId);

				}
				objProductDetails = commonServiceObj.consDeleteWLHistoryProd(objProductDetails);

				if (objProductDetails.getResponse() != null)
				{
					if (objProductDetails.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						strProdDelResponse = ApplicationConstants.DELETEPRODUCTTEXT;
					}
					else
					{
						strProdDelResponse = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consDeleteWLHistoryProd", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdDelResponse;
	}

	@RequestMapping(value = "/consaddsearchprodtowl.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consAddSearchProdToWL(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "consAddSearchProdToWL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdExists = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					objProductDetails.setUserId(loginUserId);
				}

				if (null != productId && !"".equals(productId))
				{
					objProductDetails.setProductIds(productId);

				}
				objProductDetails = commonServiceObj.consAddSearchProdToWL(objProductDetails);

				if (objProductDetails.isProductExists() != null)
				{
					if (objProductDetails.isProductExists() == ApplicationConstants.TRUE)
					{
						strProdExists = ApplicationConstants.WLADDPRODTLEXISTS;
					}
					else
					{
						strProdExists = ApplicationConstants.CONSWLPRODUCTADDED;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consAddSearchProdToWL", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdExists;
	}

	/**
	 * This method is used to get the details of wish list alerted coupon
	 * details Calls method in service layer. accepts a GET request in mime type
	 * text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/conswlcouponinfo.htm", method = RequestMethod.GET)
	public ModelAndView getConsWLCouponInfo(@ModelAttribute("WishListHome") ProductDetail objProductDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getConsWLCouponInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		Long lngUserId = null;
		String strVeiwName = "conswlcouplst";
		ArrayList<CouponDetails> couponArrayList = null;
		String intProductId = null;
		String intAlertedProdId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		objProductDetail = new ProductDetail();
		Integer intMainMenuId = null;
		String intCoupCount = null;
		objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
		if (null != objConsumerRequestDetails)
		{
			intMainMenuId = objConsumerRequestDetails.getMainMenuId();
			objProductDetail.setMainMenuID(intMainMenuId);
		}
		Users loginUser = (Users) session.getAttribute("loginuser");

		if (null != loginUser && !"".equals(loginUser))
		{
			lngUserId = loginUser.getUserID();
		}

		if (null != lngUserId && !"".equals(lngUserId))
		{
			objProductDetail.setUserId(lngUserId);
		}

		intProductId = request.getParameter("key1");
		if (null != intProductId && !"".equals(intProductId))
		{
			objProductDetail.setProductId(Integer.parseInt(intProductId));
		}
		intAlertedProdId = request.getParameter("key2");
		if (null != intAlertedProdId && !"".equals(intAlertedProdId))
		{
			objProductDetail.setAlertProdID(Integer.parseInt(intAlertedProdId));
		}
		intCoupCount = request.getParameter("key3");
		if (null != intCoupCount && !"".equals(intCoupCount))
		{
			request.setAttribute("couponcount", intCoupCount);
		}
		try
		{
			couponArrayList = commonServiceObj.getConsWLCouponDetails(objProductDetail);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (null != couponArrayList && !couponArrayList.isEmpty())
		{
			request.setAttribute("conswlcouponlst", couponArrayList);
			if (objProductDetail.getProductImagePath() != null)
			{
				request.setAttribute("productImage", objProductDetail.getProductImagePath());
			}
			request.setAttribute("alertProdName", couponArrayList.get(0).getProductName());
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:");
		}
		LOG.info("Exited in getConsWLCouponInfo Post Method....");

		return new ModelAndView(strVeiwName);
	}

	/**
	 * This method is used to get the details of wish list alerted hot deal
	 * details Calls method in service layer. accepts a GET request in mime type
	 * text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/conswlhotdealinfo.htm", method = RequestMethod.GET)
	public ModelAndView getConsWLHotDealInfo(@ModelAttribute("WishListHome") ProductDetail objProductDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getConsWLHotDealInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		Long lngUserId = null;
		String intProductId = null;
		String intAlertedProdId = null;
		String strVeiwName = "conswlhotdealslst";
		ConsumerRequestDetails objConsumerRequestDetails = null;
		Integer intMainMenuId = null;
		String intDealCount = null;
		objProductDetail = new ProductDetail();
		objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
		if (null != objConsumerRequestDetails)
		{
			intMainMenuId = objConsumerRequestDetails.getMainMenuId();
			objProductDetail.setMainMenuID(intMainMenuId);
		}
		Users loginUser = (Users) session.getAttribute("loginuser");
		if (null != loginUser && !"".equals(loginUser))
		{
			lngUserId = loginUser.getUserID();
		}

		ArrayList<HotDealsDetails> lstHotDeals = null;

		if (null != lngUserId && !"".equals(lngUserId))
		{
			objProductDetail.setUserId(lngUserId);
		}

		intProductId = request.getParameter("key1");
		if (null != intProductId && !"".equals(intProductId))
		{
			objProductDetail.setProductId(Integer.parseInt(intProductId));
		}
		intAlertedProdId = request.getParameter("key2");
		if (null != intAlertedProdId && !"".equals(intAlertedProdId))
		{
			objProductDetail.setAlertProdID(Integer.parseInt(intAlertedProdId));
		}
		intDealCount = request.getParameter("key3");
		if (null != intDealCount && !"".equals(intDealCount))
		{
			request.setAttribute("dealcount", intDealCount);
		}
		try
		{
			lstHotDeals = commonServiceObj.getConsWLHotDealInfo(objProductDetail);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (lstHotDeals != null && !"".equals(lstHotDeals))
		{
			request.setAttribute("conswlhotdeals", lstHotDeals);
			request.setAttribute("alertProdName", lstHotDeals.get(0).getProductName());

		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:");
		}
		LOG.info("Exited in getConsWLHotDealInfo Get Method....");

		return new ModelAndView(strVeiwName);
	}

	@RequestMapping(value = "/conswllocalstore.htm", method = RequestMethod.GET)
	public ModelAndView getConsWLLocalStores(@ModelAttribute("WishListHome") ProductDetail objProductDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getConsWLLocalStores";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		Long lngUserId = null;
		String intProductId = null;
		String intAlertedProdId = null;
		String strVeiwName = "conswllocalstore";
		ConsumerRequestDetails objConsumerRequestDetails = null;
		Integer intMainMenuId = null;
		FindNearByIntactResponse findNearByIntactRespObj = null;
		Integer defaultRadius = null;
		ProductDetail productDetail = null;
		objProductDetail = new ProductDetail();
		objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
		if (null != objConsumerRequestDetails)
		{
			intMainMenuId = objConsumerRequestDetails.getMainMenuId();
			objProductDetail.setMainMenuID(intMainMenuId);
		}
		Users loginUser = (Users) session.getAttribute("loginuser");
		if (null != loginUser && !"".equals(loginUser))
		{
			lngUserId = loginUser.getUserID();
		}

		if (null != lngUserId && !"".equals(lngUserId))
		{
			objProductDetail.setUserId(lngUserId);
		}

		intProductId = request.getParameter("key1");
		if (null != intProductId && !"".equals(intProductId))
		{
			objProductDetail.setProductId(Integer.parseInt(intProductId));
			request.setAttribute("wlproductId", intProductId);

		}
		intAlertedProdId = request.getParameter("key2");
		if (null != intAlertedProdId && !"".equals(intAlertedProdId))
		{
			objProductDetail.setAlertProdID(Integer.parseInt(intAlertedProdId));
		}

		try
		{
			// Fetching product scancode from database.
			productDetail = commonServiceObj.fetchConsumerProductDetails(objProductDetail);

			if (null != productDetail && !"".equals(productDetail))
			{

				request.setAttribute("wlprodname", productDetail.getProductName());
				request.setAttribute("wlprodimage", productDetail.getProductImagePath());
				request.setAttribute("wlproddesc", productDetail.getProductShortDescription());
			}

			defaultRadius = commonServiceObj.getDefaultRadius();
			if (null != defaultRadius && !"".equals(defaultRadius))
			{
				objProductDetail.setRadius(defaultRadius);
			}

			final FindNearByDetails findNearByDetails = commonServiceObj.getConsFindNearByRetailer(objProductDetail);
			if (findNearByDetails != null)
			{
				session.setAttribute("findNearByDetails", findNearByDetails);

			}
			// If scansee find near by retailer is not there , call retailgence
			// api for find near by retailers.

			else
			{
				if (defaultRadius != null)
				{
					objProductDetail.setRadius(defaultRadius);
				}

				findNearByIntactRespObj = commonServiceObj.getConsFindRetailersExternalAPI(objProductDetail);
				session.setAttribute("ExtApifindNearByDetails", findNearByIntactRespObj);

			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		catch (ScanSeeExternalAPIException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:");
		}
		LOG.info("Exited in getConsWLLocalStores Get Method....");

		return new ModelAndView(strVeiwName);
	}

	@RequestMapping(value = "/conswlonlinestore.htm", method = RequestMethod.GET)
	public ModelAndView getConsWLOnlineStores(@ModelAttribute("WishListHome") ProductDetail objProductDetail, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getConsWLOnlineStores";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		Long lngUserId = null;
		String intProductId = null;
		String intAlertedProdId = null;
		String strVeiwName = "conswlonlinestore";
		ConsumerRequestDetails objConsumerRequestDetails = null;
		Integer intMainMenuId = null;
		ExternalAPIInformation apiInfo = null;
		List<Offer> onlineStoresList = null;
		OnlineStoresRequest onlineStores = null;
		OnlineStoresService onlineStoresService = null;
		OnlineStores onlineStoresinfo = null;
		objProductDetail = new ProductDetail();
		ProductDetail productDetail = null;
		objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
		if (null != objConsumerRequestDetails)
		{
			intMainMenuId = objConsumerRequestDetails.getMainMenuId();
			objProductDetail.setMainMenuID(intMainMenuId);
		}
		Users loginUser = (Users) session.getAttribute("loginuser");
		if (null != loginUser && !"".equals(loginUser))
		{
			lngUserId = loginUser.getUserID();
		}
		if (null != lngUserId && !"".equals(lngUserId))
		{
			objProductDetail.setUserId(lngUserId);

		}

		intProductId = request.getParameter("key1");
		if (null != intProductId && !"".equals(intProductId))
		{
			objProductDetail.setProductId(Integer.parseInt(intProductId));
			request.setAttribute("wlproductId", intProductId);
		}
		intAlertedProdId = request.getParameter("key2");
		if (null != intAlertedProdId && !"".equals(intAlertedProdId))
		{
			objProductDetail.setAlertProdID(Integer.parseInt(intAlertedProdId));
		}

		try
		{
			// for fetching external API shopzilla input parameters from
			// database.

			apiInfo = commonServiceObj.getConsExternalApiInfo(ApplicationConstants.CONSFINDONLINESTORE);
			onlineStoresService = new OnlineStoresServiceImpl();
			onlineStores = new OnlineStoresRequest();
			onlineStores.setPageStart("0");

			// Fetching product scancode from database.
			productDetail = commonServiceObj.fetchConsumerProductDetails(objProductDetail);

			if (null != productDetail && !"".equals(productDetail))
			{

				request.setAttribute("wlprodname", productDetail.getProductName());
				request.setAttribute("wlprodimage", productDetail.getProductImagePath());
				request.setAttribute("wlproddesc", productDetail.getProductShortDescription());
				onlineStores.setUpcCode(productDetail.getScanCode());
				
			}
			onlineStores.setFromWeb(true);
			if (objProductDetail.getUserId() != null)
			{
				onlineStores.setUserId(String.valueOf(objProductDetail.getUserId()));
			}
			else
			{
				onlineStores.setUserId("0");
			}
			onlineStores.setValues();
			onlineStoresinfo = onlineStoresService.onlineStoresInfoEmailTemplate(apiInfo, onlineStores);

			if (onlineStoresinfo != null)
			{
				if (!onlineStoresinfo.getOffers().isEmpty())
				{
					onlineStoresList = onlineStoresinfo.getOffers();
					session.setAttribute("OnlineStoresList", onlineStoresList);

				}

			}

			/**
			 * For commission junction data integration...
			 */

			final CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
			ArrayList<RetailerDetail> commiJuncDatalst = null;

			commiJuncDatalst = shoppingListService.getConsumerCommisJunctionData(objProductDetail);

			if (null != commiJuncDatalst && !commiJuncDatalst.isEmpty())
			{
				session.setAttribute("CJOnlineStores", commiJuncDatalst);

			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:");
		}
		LOG.info("Exited in getConsWLOnlineStores Get Method....");

		return new ModelAndView(strVeiwName);
	}

	@RequestMapping(value = "/conswladdcoupon", method = RequestMethod.GET)
	public @ResponseBody
	String consWLAddCoupon(@RequestParam(value = "couponId", required = true) Integer couponId,
			@RequestParam(value = "couponlstId", required = true) Integer couponLstId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{
		final String methodName = "consWLAddCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Long lngUserId = null;
		String addCouponResponse = null;
		CouponDetail objCouponDetail = new CouponDetail();
		try
		{

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");

			Users loginUser = (Users) session.getAttribute("loginuser");
			if (null != loginUser && !"".equals(loginUser))
			{
				lngUserId = loginUser.getUserID();
			}
			if (null != lngUserId && !"".equals(lngUserId))
			{
				objCouponDetail.setUserId(lngUserId.intValue());

			}
			if (null != couponId && !"".equals(couponId))
			{
				objCouponDetail.setCouponId(couponId);
			}
			if (null != couponLstId && !"".equals(couponLstId))
			{
				objCouponDetail.setCouponListID(couponLstId);
			}

			addCouponResponse = commonServiceObj.consWLAddCoupon(objCouponDetail);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		return addCouponResponse;
	}

	@RequestMapping(value = "/conswlremovecoupon", method = RequestMethod.GET)
	public @ResponseBody
	String consWLRemoveCoupon(@RequestParam(value = "couponId", required = true) Integer couponId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "consWLRemoveCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Long lngUserId = null;
		String removeCouponResponse = null;
		CouponDetail objCouponDetail = new CouponDetail();
		try
		{

			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");

			Users loginUser = (Users) session.getAttribute("loginuser");
			if (null != loginUser && !"".equals(loginUser))
			{
				lngUserId = loginUser.getUserID();
			}
			if (null != lngUserId && !"".equals(lngUserId))
			{
				objCouponDetail.setUserId(lngUserId.intValue());

			}
			if (null != couponId && !"".equals(couponId))
			{
				objCouponDetail.setCouponId(couponId);
			}

			removeCouponResponse = commonServiceObj.consWLRemoveCoupon(objCouponDetail);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		return removeCouponResponse;
	}

	@RequestMapping(value = "/conswlprintprod.htm", method = RequestMethod.GET)
	public final ModelAndView showConsProdInfoPrint(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws ScanSeeServiceException
	{
		final String methodName = "showConsProdInfoPrint";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService objCommonService = (CommonService) appContext.getBean("commonService");
		final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		Long userId = null;
		Integer intRecordCount = null;
		WishListProducts objWishListProducts = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		if (objUsers != null && !"".equals(objUsers))
		{
			userId = objUsers.getUserID();
		}
		try
		{		
			String strCount = (String) request.getAttribute("recordCount");
			if(null !=strCount && !"".equals(strCount))
			{
				intRecordCount = Integer.valueOf(strCount);
			}
						
			objConsumerRequestDetails=	(ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if(null == objConsumerRequestDetails || "".equals(objConsumerRequestDetails))
			{
				objConsumerRequestDetails.setUserId(userId);
				objConsumerRequestDetails.setRecordCount(intRecordCount);
			}
			objWishListProducts = objCommonService.getConsWishListProds(objConsumerRequestDetails);

			if (objWishListProducts != null)
			{
				session.setAttribute("wishlistprods", objWishListProducts);
			}
		

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		return new ModelAndView("conswlprintprod");
	}
}

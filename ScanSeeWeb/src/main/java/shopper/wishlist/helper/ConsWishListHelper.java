package shopper.wishlist.helper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.find.service.FindService;
import shopper.service.CommonService;

import common.constatns.ApplicationConstants;
import common.pojo.ConsumerRequestDetails;
import common.pojo.Users;
import common.pojo.shopper.AlertedProducts;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.WishListHistoryDetails;
import common.pojo.shopper.WishListProducts;
import common.pojo.shopper.WishListResultSet;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class contains methods for grouping wish list history products based on
 * wish list deleted date.
 * 
 * @author shyamsundara_hm
 */

public class ConsWishListHelper
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ConsWishListHelper.class);

	/**
	 * for WishListHelper constructor.
	 */
	public ConsWishListHelper()
	{
		LOG.info("Inside WishListHelper class");
	}

	/**
	 * This method for grouping based on wish list add date and sorting by
	 * current date.
	 * 
	 * @param wishlistHistorylst
	 *            AS parameter
	 * @return list containing wish list history.
	 */

	public static WishListHistoryDetails getWishListHistory(ArrayList<ProductDetail> wishlistHistorylst)
	{

		final WishListHistoryDetails wishListHistoryDetails = new WishListHistoryDetails();

		final HashMap<String, WishListResultSet> wishListResultMap = new HashMap<String, WishListResultSet>();

		ArrayList<ProductDetail> productDetaillst = null;

		WishListResultSet wishListResultSetInfo = new WishListResultSet();

		String key = null;
		for (ProductDetail listResultSet : wishlistHistorylst)
		{
			key = listResultSet.getWishListAddDate();

			if (wishListResultMap.containsKey(key))
			{

				wishListResultSetInfo = wishListResultMap.get(key);
				productDetaillst = (ArrayList<ProductDetail>) wishListResultSetInfo.getProductDetail();
				if (null != productDetaillst)

				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductId());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getProductPrice());
					productDetail.setProductShortDescription(listResultSet.getProductShortDescription());
					productDetail.setSalePrice(listResultSet.getSalePrice());
					productDetail.setRow_Num(listResultSet.getRow_Num());
					productDetaillst.add(productDetail);
					wishListResultSetInfo.setProductDetail(productDetaillst);
				}
			}
			else
			{
				wishListResultSetInfo = new WishListResultSet();

				wishListResultSetInfo.setWishListAddDate(listResultSet.getWishListAddDate());
				productDetaillst = new ArrayList<ProductDetail>();
				final ProductDetail productDetail = new ProductDetail();
				if (null != listResultSet.getProductId())
				{
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductId());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getProductPrice());
					productDetail.setProductShortDescription(listResultSet.getProductShortDescription());
					productDetail.setSalePrice(listResultSet.getSalePrice());
					productDetail.setRow_Num(listResultSet.getRow_Num());
					productDetaillst.add(productDetail);
					wishListResultSetInfo.setProductDetail(productDetaillst);
				}
				wishListResultMap.put(key, wishListResultSetInfo);
			}
		}

		final Set<Map.Entry<String, WishListResultSet>> set = wishListResultMap.entrySet();

		final ArrayList<WishListResultSet> wishListResultSetlst = new ArrayList<WishListResultSet>();

		for (Map.Entry<String, WishListResultSet> entry : set)
		{
			wishListResultSetlst.add(entry.getValue());
		}

		/**
		 * Calling Comparator interface for sorting wishListAddDate. It sorts
		 * and displays according to current date.
		 */
		final SortWishListHistoryByDate sortHistoryByDateObj = new SortWishListHistoryByDate();
		Collections.sort(wishListResultSetlst, sortHistoryByDateObj);
		Collections.reverse(wishListResultSetlst);

		wishListHistoryDetails.setProductHistoryInfo(wishListResultSetlst);
		return wishListHistoryDetails;

	}

	/**
	 * This method for grouping based on wish list add date and sorting by
	 * Current date. It also groups products into alerted products and other
	 * products based on sale price.
	 * 
	 * @param wishlistHistorylst
	 *            As parameter
	 * @return list containing wish list history.
	 */

	public static WishListProducts getWishListDetails(ArrayList<ProductDetail> wishlistHistorylst)
	{

		final WishListProducts wishListProd = new WishListProducts();

		final HashMap<String, WishListResultSet> wishListResultMap = new HashMap<String, WishListResultSet>();

		ArrayList<ProductDetail> productDetaillst = null;

		final ArrayList<ProductDetail> alertProdList = new ArrayList<ProductDetail>();

		final AlertedProducts objAlert = new AlertedProducts();
		WishListResultSet wishListResultSetInfo = null;
		String key = null;
		for (ProductDetail listResultSet : wishlistHistorylst)
		{
			key = listResultSet.getWishListAddDate();

			if (wishListResultMap.containsKey(key))
			{

				wishListResultSetInfo = wishListResultMap.get(key);
				productDetaillst = (ArrayList<ProductDetail>) wishListResultSetInfo.getProductDetail();
				if (null != productDetaillst)
				{
					if (listResultSet.getSalePrice() != null && !ApplicationConstants.NOTAPPLICABLE.equals(listResultSet.getSalePrice())
							|| (listResultSet.getCouponSaleFlag() != null && listResultSet.getCouponSaleFlag() > 0)
							|| (listResultSet.getHotDealFlag() != null && listResultSet.getHotDealFlag() > 0))
					{
						final ProductDetail objProduct = new ProductDetail();
						objProduct.setProductId(listResultSet.getProductId());
						objProduct.setProductName(listResultSet.getProductName());
						objProduct.setSalePrice(listResultSet.getSalePrice());
						objProduct.setUserProductId(listResultSet.getUserProductId());
						objProduct.setProductPrice(listResultSet.getProductPrice());
						objProduct.setProductImagePath(listResultSet.getProductImagePath());
						objProduct.setRetailFlag(listResultSet.getRetailFlag());
						objProduct.setCouponSaleFlag(Integer.valueOf(listResultSet.getCouponSaleFlag()));
						objProduct.setHotDealFlag(Integer.valueOf(listResultSet.getHotDealFlag()));
						objProduct.setPushNotifyFlag(listResultSet.getPushNotifyFlag());
						objProduct.setRow_Num(listResultSet.getRow_Num());
						objProduct.setProductDescription(listResultSet.getProductDescription());
						objProduct.setAlertProdID(listResultSet.getAlertProdID());
						alertProdList.add(objProduct);
					}
					else
					{

						final ProductDetail productDetail = new ProductDetail();
						productDetail.setProductId(listResultSet.getProductId());
						productDetail.setUserProductId(listResultSet.getUserProductId());
						productDetail.setProductName(listResultSet.getProductName());
						productDetail.setProductImagePath(listResultSet.getProductImagePath());
						productDetail.setProductPrice(listResultSet.getProductPrice());
						productDetail.setSalePrice(listResultSet.getSalePrice());
						productDetail.setCouponSaleFlag(Integer.valueOf(listResultSet.getCouponSaleFlag()));
						productDetail.setHotDealFlag(Integer.valueOf(listResultSet.getHotDealFlag()));
						productDetail.setRetailFlag(listResultSet.getRetailFlag());
						productDetail.setPushNotifyFlag(listResultSet.getPushNotifyFlag());
						productDetail.setRow_Num(listResultSet.getRow_Num());
						productDetail.setProductDescription(listResultSet.getProductDescription());
						productDetail.setAlertProdID(listResultSet.getAlertProdID());
						productDetaillst.add(productDetail);
					}
					wishListResultSetInfo.setProductDetail(productDetaillst);
				}
			}
			else
			{
				wishListResultSetInfo = new WishListResultSet();
				wishListResultSetInfo.setWishListAddDate(listResultSet.getWishListAddDate());
				productDetaillst = new ArrayList<ProductDetail>();

				if (listResultSet.getSalePrice() != null && !ApplicationConstants.NOTAPPLICABLE.equals(listResultSet.getSalePrice())
						|| (listResultSet.getCouponSaleFlag() != null && listResultSet.getCouponSaleFlag() > 0)
						|| (listResultSet.getHotDealFlag() != null && listResultSet.getHotDealFlag() > 0))
				{
					final ProductDetail objProduct = new ProductDetail();
					objProduct.setProductId(listResultSet.getProductId());
					objProduct.setUserProductId(listResultSet.getUserProductId());
					objProduct.setProductName(listResultSet.getProductName());
					objProduct.setSalePrice(listResultSet.getSalePrice());
					objProduct.setProductPrice(listResultSet.getProductPrice());
					objProduct.setProductImagePath(listResultSet.getProductImagePath());
					objProduct.setCouponSaleFlag(Integer.valueOf(listResultSet.getCouponSaleFlag()));
					objProduct.setHotDealFlag(Integer.valueOf(listResultSet.getHotDealFlag()));
					objProduct.setRetailFlag(listResultSet.getRetailFlag());
					objProduct.setPushNotifyFlag(listResultSet.getPushNotifyFlag());
					objProduct.setRow_Num(listResultSet.getRow_Num());
					objProduct.setProductDescription(listResultSet.getProductDescription());
					objProduct.setAlertProdID(listResultSet.getAlertProdID());
					alertProdList.add(objProduct);
				}
				else
				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductId());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getProductPrice());
					productDetail.setSalePrice(listResultSet.getSalePrice());
					productDetail.setCouponSaleFlag(Integer.valueOf(listResultSet.getCouponSaleFlag()));
					productDetail.setHotDealFlag(Integer.valueOf(listResultSet.getHotDealFlag()));
					productDetail.setRetailFlag(listResultSet.getRetailFlag());
					productDetail.setPushNotifyFlag(listResultSet.getPushNotifyFlag());
					productDetail.setAlertProdID(listResultSet.getAlertProdID());
					productDetail.setRow_Num(listResultSet.getRow_Num());
					productDetail.setProductDescription(listResultSet.getProductDescription());
					productDetaillst.add(productDetail);
					wishListResultSetInfo.setProductDetail(productDetaillst);
					wishListResultMap.put(key, wishListResultSetInfo);
				}
			}
		}

		final Set<Map.Entry<String, WishListResultSet>> set = wishListResultMap.entrySet();

		final ArrayList<WishListResultSet> wishListResultSetlst = new ArrayList<WishListResultSet>();

		for (Map.Entry<String, WishListResultSet> entry : set)
		{
			wishListResultSetlst.add(entry.getValue());
		}
		//final Comparator<ProductDetail> comp = Collections.reverseOrder(new SortProductByFlag());

		if (alertProdList != null && !alertProdList.isEmpty())
		{
			//Collections.sort(alertProdList, comp);
			objAlert.setAlertProductDetails(alertProdList);
			wishListProd.setAlertProducts(objAlert);
		}

		/**
		 * Calling Comparator interface for sorting wishListAddDate. It sorts
		 * and displays according to current date.
		 */

		if (wishListResultSetlst != null && !wishListResultSetlst.isEmpty())
		{

			final SortWishListHistoryByDate sortHistoryByDateObj = new SortWishListHistoryByDate();
			Collections.sort(wishListResultSetlst, sortHistoryByDateObj);
			Collections.reverse(wishListResultSetlst);
			wishListProd.setProductInfo(wishListResultSetlst);
		}

		return wishListProd;
	}
	
}

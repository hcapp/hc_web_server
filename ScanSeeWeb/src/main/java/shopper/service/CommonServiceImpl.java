package shopper.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.dao.BaseDAO;
import shopper.dao.CommonDAO;
import shopper.dao.ManageSettingsDAO;
import shopper.dao.RateReviewDAO;
import shopper.wishlist.helper.ConsWishListHelper;

import com.scansee.externalapi.common.exception.ScanSeeExternalAPIException;
import com.scansee.externalapi.common.findnearby.pojos.FindNearByIntactResponse;
import com.scansee.externalapi.common.findnearby.pojos.FindNearByRequest;
import com.scansee.externalapi.common.pojos.ExternalAPIErrorLog;
import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.findNearBy.FindNearByService;
import com.scansee.externalapi.findNearBy.FindNearByServiceImpl;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;
import common.constatns.ApplicationConstants;
import common.email.EmailComponent;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.externalapi.ExternalAPIManager;
import common.pojo.AppConfiguration;
import common.pojo.ConsumerRequestDetails;
import common.pojo.Retailer;
import common.pojo.SearchForm;
import common.pojo.Users;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.ProductRatingReview;
import common.pojo.shopper.ProductReview;
import common.pojo.shopper.ProductSummaryVO;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.UpdateUserInfo;
import common.pojo.shopper.UserRatingInfo;
import common.pojo.shopper.UserTrackingData;
import common.pojo.shopper.WishListHistoryDetails;
import common.pojo.shopper.WishListProducts;
import common.util.Utility;

/**
 * This class is a service implementation class for CommonService.
 * 
 * @author manjunath_gh
 */
public class CommonServiceImpl implements CommonService
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(CommonServiceImpl.class);

	/**
	 * The variable of type ThisLocationDAO.
	 */

	private CommonDAO commonDAO;

	/**
	 * Setter method for thisLocationDao.
	 * 
	 * @param thisLocationDao
	 *            the Variable of type ThisLocationDAO
	 */

	public void setcommonDAO(CommonDAO commonDAO)
	{
		this.commonDAO = commonDAO;
	}

	/**
	 * The variable of type ThisLocationDAO.
	 */

	private BaseDAO baseDAO;

	/**
	 * Setter method for thisLocationDao.
	 * 
	 * @param thisLocationDao
	 *            the Variable of type ThisLocationDAO
	 */

	public void setBaseDAO(BaseDAO baseDAO)
	{
		this.baseDAO = baseDAO;
	}

	/**
	 * Instance variable for Manage Settings DAO instance.
	 */
	private ManageSettingsDAO manageSettingsDao;

	/**
	 * for setting tManageSettingsDao.
	 * 
	 * @param manageSettingsDao
	 *            the manageSettingsDao to set
	 */
	public void setManageSettingsDao(ManageSettingsDAO manageSettingsDao)
	{
		this.manageSettingsDao = manageSettingsDao;
	}

	/**
	 * Instance variable for RateReviewDAO DAO instance.
	 */
	private RateReviewDAO rateReviewDao;

	/**
	 * Setter for rate review DAO.
	 * 
	 * @param rateReviewDao
	 *            the rateReviewDao to set
	 */
	public void setRateReviewDao(RateReviewDAO rateReviewDao)
	{
		this.rateReviewDao = rateReviewDao;
	}

	/**
	 * holds the number of online Stores values;
	 */

	private String totalOnlineStores = null;

	/**
	 * holds the number of online Stores values;
	 */

	private String minimumPrice = null;

	/**
	 * This is a service Method for fetching product summary for the given input
	 * as XML. This method validates the input XML, if it is valid it will call
	 * the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the product
	 *            summary
	 * @return ProductSummaryVO object containing product summary containing
	 *         product summary in the response.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ProductSummaryVO getProductSummary(ProductDetailsRequest productDetailsRequest)
	{
		final String methodName = "getProductSummary";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductSummaryVO productSummaryVO = new ProductSummaryVO();
		Long userId = productDetailsRequest.getUserId();
		Integer retailId = productDetailsRequest.getRetailID();
		final Integer productId = productDetailsRequest.getProductId();
		final String postalcode = productDetailsRequest.getPostalcode();
		final int radius = 5;
		final Double latitude = productDetailsRequest.getLatitude();
		final Double longitude = productDetailsRequest.getLongitude();

		// Get the configuration images and headers;
		try
		{
			final ArrayList<AppConfiguration> stockInfoList = commonDAO.getAppConfig(ApplicationConstants.CONFIGURATIONTYPESTOCK);

			HashMap hmap = getConfigData(stockInfoList);

			// Get the product information rating,Coupons etc.
			final ProductDetail productDetail = baseDAO.getProductDetail(userId, productId, retailId);

			if (null != productDetail)
			{
				// First get the LocalStore information from database.
				final FindNearByIntactResponse findNearByDetailsResultSet = findRetailersLocalDataBase(userId, latitude, productId, longitude,
						postalcode, radius);

				// Request for Call to Online Stores External API.
				final OnlineStoresRequest onlineStoresRequest = new OnlineStoresRequest();
				onlineStoresRequest.setProductId(productId);
				onlineStoresRequest.setUserId(String.valueOf(userId));
				onlineStoresRequest.setPageStart(String.valueOf(0));

				// Call External API for Online Stores.
				OnlineStores objOnlineStores = new OnlineStores();
				objOnlineStores = getOnlineStoreData(onlineStoresRequest);

				/**
				 * For commission junction data....
				 */
				ArrayList<RetailerDetail> onlineRetailerlst = null;
				ArrayList<Double> salePricelst = new ArrayList<Double>();
				onlineRetailerlst = commonDAO.getCommisJunctionData(userId, productId);

				if (null != onlineRetailerlst && !onlineRetailerlst.isEmpty())
				{

					for (int i = 0; i < onlineRetailerlst.size(); i++)
					{

						if (onlineRetailerlst.get(i).getSalePrice() != null && !onlineRetailerlst.get(i).getSalePrice().contains("$")
								&& !(onlineRetailerlst.get(i).getSalePrice().equalsIgnoreCase(ApplicationConstants.NOTAPPLICABLE)))
						{

							String commisPrice = onlineRetailerlst.get(i).getSalePrice();
							commisPrice = commisPrice.substring(1, commisPrice.length());
							salePricelst.add(Double.parseDouble(commisPrice));
						}

					}

					Double onlineMinPrice = null;
					if (salePricelst != null && !salePricelst.isEmpty())
					{
						onlineMinPrice = Collections.min(salePricelst);
					}
					Double shopMinPrice = null;
					if (minimumPrice != null)
					{
						int dollerindex = minimumPrice.indexOf('$');
						minimumPrice = minimumPrice.substring(1, minimumPrice.length());
						shopMinPrice = Double.parseDouble(minimumPrice);
					}

					if (shopMinPrice != null && !shopMinPrice.equals(ApplicationConstants.NOTAPPLICABLE) && onlineMinPrice != null
							&& !shopMinPrice.equals(ApplicationConstants.NOTAPPLICABLE))
					{
						if (onlineMinPrice < shopMinPrice)
						{

							minimumPrice = String.valueOf(onlineMinPrice);
							minimumPrice = "$" + Utility.formatDecimalValue(minimumPrice);
						}
						else
						{
							minimumPrice = String.valueOf(shopMinPrice);
							minimumPrice = "$" + Utility.formatDecimalValue(minimumPrice);
						}

					}
					else
					{
						if (onlineMinPrice != null && !onlineMinPrice.equals(ApplicationConstants.NOTAPPLICABLE))
						{

							minimumPrice = String.valueOf(onlineMinPrice);
							minimumPrice = "$" + Utility.formatDecimalValue(minimumPrice);
						}
					}
					if (totalOnlineStores != null)
					{
						int total = Integer.parseInt(totalOnlineStores);
						totalOnlineStores = String.valueOf(total + onlineRetailerlst.size());
					}
					else
					{
						totalOnlineStores = String.valueOf(onlineRetailerlst.size());
						// productSummaryVO.setObjOnlineStores().onlineStoresMetaData.totalStores
					}

				}

				/**
				 * Calling rate review dao method to get review and rating
				 * information. It includes reviews of the product and user
				 * ratings.
				 */
				final ArrayList<ProductReview> productReviewslist = rateReviewDao.getProductReviews(userId, productId);
				UserRatingInfo userRatingInfo = rateReviewDao.fecthUserProductRating(userId, productId);

				// Setting all responses in the POJO object
				// productSummaryVO.setFindNearByDetailsResultSet(findNearByDetailsResultSet);

				productSummaryVO.setObjOnlineStores(objOnlineStores);
				productSummaryVO.setProductDetail(productDetail);
				productSummaryVO.setProductReviewslist(productReviewslist);
				productSummaryVO.setAppConfigData(hmap);
				productSummaryVO.setUserRatingInfo(userRatingInfo);

				productSummaryVO.setTotalStores(totalOnlineStores);
				productSummaryVO.setMinimumPrice(minimumPrice);

			}

		}
		catch (Exception e)
		{
			LOG.info("Exception occured", e);
		}
		return productSummaryVO;
	}

	/**
	 * This is Service method for finding retailers local database.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to get the number of Retailers and lowest price
	 * of the products searching for.
	 * 
	 * @param userID
	 *            requested user.
	 * @param latitude
	 *            current location of user.
	 * @param productId
	 *            products to be searched for.
	 * @param longitude
	 *            current location of user.
	 * @param postalcode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @return String XMl list of retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public FindNearByIntactResponse findRetailersLocalDataBase(Long userID, Double latitude, Integer productId, Double longitude, String postalcode,
			int radius) throws ScanSeeWebSqlException
	{
		final String methodName = " findRetailersLocalDataBase ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.info("Querying Local DataBase for Retailers");
		final FindNearByIntactResponse findNearByDetailsResultSet = null;
		// final FindNearByIntactResponse findNearByDetailsResultSet =
		// commonDAO.fetchNearByLowestPrice(userID, latitude, productId,
		// longitude,postalcode, radius);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return findNearByDetailsResultSet;
	}

	/**
	 * This is Service method for find near by retailers based on lowest
	 * price.This method validates the input parameters and returns validation
	 * error if validation fails. calls DAO methods to get the number of
	 * Retailers and lowest price of the products searching for.
	 * 
	 * @param userID
	 *            requested user.
	 * @param latitude
	 *            current location of user.
	 * @param productId
	 *            products to be searched for.
	 * @param longitude
	 *            current location of user.
	 * @param postalcode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @return String XMl list of retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public String findRetailersExternalAPI(Integer userID, Integer productId, Double latitude, Double longitude, Integer radius, Integer page,
			String postalcode) throws ScanSeeWebSqlException
	{
		final String methodName = " findRetailersExternalAPI ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.info("::::::Invocation of External API::::::::::");
		String response = null;
		final FindNearByRequest findNearByRequest = new FindNearByRequest();
		ProductDetail productInfo = null;
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();
		UpdateUserInfo updateUserInfo = null;
		String externalAPIURL;
		@SuppressWarnings("unused")
		ExternalAPIErrorLog externalAPIErrorLog = null;
		productInfo = commonDAO.getProductInfoforExternalAPI(productId);
		updateUserInfo = manageSettingsDao.fetchUserInfo(userID);

		if (null != productInfo)
		{

			if (null != productInfo.getBarCode() && !productInfo.getBarCode().equals(ApplicationConstants.NOTAPPLICABLE))
			{

				findNearByRequest.setUpcCode(productInfo.getBarCode());
			}
			else if (null != productInfo.getSkuCode() && !productInfo.getSkuCode().equals(ApplicationConstants.NOTAPPLICABLE))
			{
				findNearByRequest.setSkuCode(productInfo.getSkuCode());
			}
			else if (null != productInfo.getProductName() && !productInfo.getProductName().equals(ApplicationConstants.NOTAPPLICABLE))
			{
				findNearByRequest.setKeywords(productInfo.getProductName());
			}
			else
			{
				// No product Information is available to invoke external API
				LOG.info("No product Information is available to invoke external API");
				return null;
			}

			if (null != latitude && null != longitude)
			{
				findNearByRequest.setLatitude(String.valueOf(latitude));
				findNearByRequest.setLongiTude(String.valueOf(longitude));
			}
			else
			{
				if (null == postalcode)
				{
					if (null != updateUserInfo)
					{
						if (null != updateUserInfo.getPostalCode())
						{
							findNearByRequest.setZipCode(updateUserInfo.getPostalCode());
						}

					}
					else
					{
						LOG.info("Either Latitude and Longitude or ZipCode Available to invoke External API");
						return null;
					}
				}
				else
				{
					findNearByRequest.setZipCode(postalcode);
				}

			}

			findNearByRequest.setRadius(String.valueOf(radius));
			findNearByRequest.setFormat(ApplicationConstants.EXTERNALAPIFORMAT);
			findNearByRequest.setPage(String.valueOf(page));
			findNearByRequest.setUserId(userID);
			findNearByRequest.setValues();

			final FindNearByService findNearByService = new FindNearByServiceImpl();

			externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDNEARBYMODULENAME);

			if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
			{
				LOG.info("Invoking External API for Module::::::::" + ApplicationConstants.FINDNEARBYMODULENAME + " With Parameters:Product ID:"
						+ productId + " Product Name:" + productInfo.getProductName() + " Latitude::" + latitude + " Logitude::" + longitude
						+ " Radius::" + radius);

				response = findNearByService.processFindNearByRequest(externalAPIInformation, findNearByRequest);
				externalAPIURL = findNearByService.getApiURL();
				if (null != externalAPIURL)
				{
					commonDAO.insertExternalAPIURL(externalAPIURL, userID);

				}

				if (findNearByService.getResponsecode() != 11000)
				{
					response = null;
				}
				if (ApplicationConstants.EXTERNALAPIFAILURE == findNearByService.getResponsecode())
				{
					externalAPIErrorLog = findNearByService.getExternalAPIErrors();
					// commonDAO.LogExternalApiErrorMessages(externalAPIErrorLog);
				}

				LOG.info("XML Response from External API:::::" + response);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to get external API information.
	 * 
	 * @param moduleName
	 *            -Requested module name.
	 * @return ExternalAPIInformation
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ExternalAPIInformation getExternalAPIInformation(String moduleName) throws ScanSeeWebSqlException
	{
		final String methodName = " getExternalAPIInformation ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ExternalAPIManager externalAPIManager = new ExternalAPIManager(moduleName, commonDAO);
		final ExternalAPIInformation externalAPIInformation = externalAPIManager.getExternalAPIInformation();
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIInformation;
	}

	/**
	 * This service method is used to get online store informations.
	 * 
	 * @param onlineStoresRequest
	 *            -Request object
	 * @return response as success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	private OnlineStores getOnlineStoreData(OnlineStoresRequest onlineStoresRequest) throws ScanSeeWebSqlException
	{

		ProductDetail productInfo = null;
		OnlineStores objOnlineStores = new OnlineStores();
		@SuppressWarnings("unused")
		ExternalAPIErrorLog externalAPIErrorLog = null;
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();
		final OnlineStoresService onlineStoresService = new OnlineStoresServiceImpl();
		if (null == onlineStoresRequest.getApiURL())
		{
			productInfo = commonDAO.getProductInfoforExternalAPI(onlineStoresRequest.getProductId());

			if (null != productInfo)
			{
				if (productInfo.getBarCode() != ApplicationConstants.NOTAPPLICABLE)
				{
					final OnlineStoresRequest onlineStores = new OnlineStoresRequest();
					onlineStores.setPageStart(onlineStoresRequest.getPageStart());
					onlineStores.setUpcCode(productInfo.getBarCode());
					onlineStores.setUserId(onlineStoresRequest.getUserId());
					onlineStores.setValues();

					externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDONLINESTOESMODULENAME);

					if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
					{
						objOnlineStores = onlineStoresService.onlineStoresInfoEmailTemplate(externalAPIInformation, onlineStores);
						totalOnlineStores = onlineStoresService.getNumberOfStores();
						minimumPrice = onlineStoresService.getMinimumPrice();

						if (ApplicationConstants.EXTERNALAPIFAILURE == onlineStoresService.getResponseCode())
						{
							externalAPIErrorLog = onlineStoresService.getExternalAPIErrors();
						}

					}

				}
				else
				{

					LOG.info("Product Barcode is not available to Invoke External External API");
				}
			}
		}

		return objOnlineStores;
	}

	/**
	 * This is Service method for retrieving find near by retailers.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to get the Retailers.
	 * 
	 * @param AreaInfoVO
	 *            area information dtoes in the instance.
	 * @return FindNearByDetails instance containing list of retailers.
	 */

	public FindNearByDetails findNearBy(AreaInfoVO objAreaInfoVO, Integer productId)
	{

		final String methodName = "findNearBy";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		FindNearByDetails findNearByDetails = new FindNearByDetails();
		try
		{
			findNearByDetails = commonDAO.fetchNearByInfo(objAreaInfoVO, productId);
		}
		catch (Exception e)
		{
			LOG.error("", e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return findNearByDetails;
	}

	/**
	 * 
	 * 
	 */
	@SuppressWarnings("unchecked")
	public HashMap getConfigData(ArrayList<AppConfiguration> stockInfoList)
	{
		HashMap hmap = new HashMap();
		for (int i = 0; i <= stockInfoList.size() - 1; i++)
		{
			AppConfiguration appConfiguration = (AppConfiguration) stockInfoList.get(i);
			if ("NearbyImage".equals(appConfiguration.getScreenName()))
			{
				hmap.put(0, appConfiguration.getScreenContent());
			}
			if ("NearbyHeader".equals(appConfiguration.getScreenName()))
			{
				hmap.put(1, appConfiguration.getScreenContent());
			}
			/*
			 * if("ProductImage".equals(appConfiguration.getScreenName())) {
			 * hmap.put(2,appConfiguration.getScreenContent()); }
			 * if("ProductHeader".equals(appConfiguration.getScreenName())) {
			 * hmap.put(3,appConfiguration.getScreenContent()); }
			 */
			if ("ReviewImage".equals(appConfiguration.getScreenName()))
			{
				hmap.put(4, appConfiguration.getScreenContent());
			}
			if ("ReviewHeader".equals(appConfiguration.getScreenName()))
			{
				hmap.put(5, appConfiguration.getScreenContent());
			}
			if ("DiscountImage".equals(appConfiguration.getScreenName()))
			{
				hmap.put(6, appConfiguration.getScreenContent());
			}
			if ("DiscountHeader".equals(appConfiguration.getScreenName()))
			{
				hmap.put(7, appConfiguration.getScreenContent());
			}
			if ("OnlineImage".equals(appConfiguration.getScreenName()))
			{
				hmap.put(8, appConfiguration.getScreenContent());
			}
			if ("OnlineHeader".equals(appConfiguration.getScreenName()))
			{
				hmap.put(9, appConfiguration.getScreenContent());
			}

		}
		return hmap;
	}

	/**
	 * This is a service method used for retrieving the particular retailer
	 * details in the near by section
	 * 
	 * @param retailerId
	 *            retailerId of particular retailer
	 * @return Retailer Retailer instance which gives the details of the
	 *         retailer
	 */
	public Retailer findNearByRetailerDetail(Integer retailerId)
	{
		LOG.info("");
		Retailer retailer = new Retailer();
		try
		{
			retailer = commonDAO.findNearByRetailerDetail(retailerId);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("", e);
		}
		LOG.info("");
		return retailer;
	}

	/**
	 * This is a Service Method for fetching product information for the given
	 * userId,productId and retailId.This method validates the userId, productId
	 * and retailId if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which product information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productId
	 *            for which product information need to be fetched.If productId
	 *            is null then it is invalid request.
	 * @param retailId
	 *            for which product information need to be fetched.If retailId
	 *            is null then it is invalid request.
	 * @return XML containing product information in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ProductDetail getProductsInfo(Long userId, Integer productId, Integer retailId)
	{

		final String methodName = "getProductsInfo of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetail = null;
		try
		{
			productDetail = baseDAO.getProductDetail(userId, productId, retailId);
			if (productDetail == null)
			{
				productDetail = new ProductDetail();
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("", e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetail;
	}

	/**
	 * The Service Method for fetching product reviews and user ratings.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML String Containing Product Reviews and User Ratings.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ProductRatingReview getProductReviews(Long userId, Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "getProductReviews";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ProductRatingReview productRatingReview = new ProductRatingReview();
		final ArrayList<ProductReview> productReviewslist = rateReviewDao.getProductReviews(userId, productId);
		final UserRatingInfo userRatingInfo = rateReviewDao.fecthUserProductRating(userId, productId);

		if (null != productReviewslist && !productReviewslist.isEmpty())
		{
			productRatingReview.setProductReviews(productReviewslist);
			productRatingReview.setTotalReviews(String.valueOf(productReviewslist.size()));

		}
		else
		{
			productRatingReview.setTotalReviews("0");
		}

		if (null != userRatingInfo)
		{

			productRatingReview.setUserRatingInfo(userRatingInfo);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productRatingReview;
	}

	/**
	 * This is a service implementation method for saving user product rating
	 * information. Calls method in dao layer. accepts a POST request, MIME type
	 * is text/xml.
	 * 
	 * @param xml
	 *            input request for which needed to save user product rating. If
	 *            userId or proudctId is null then it is invalid request.
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String saveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "saveUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		response = rateReviewDao.saveUserProductRating(userRatingInfo);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the products media info
	 * based on the media type.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ProductDetails getMediaDetails(Integer productID, String mediaType) throws ScanSeeServiceException
	{
		final String methodName = " getMediaDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ProductDetails productDetails = null;
		try
		{
			productDetails = commonDAO.getProductMediaDetails(productID, mediaType);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	public ArrayList<RetailerDetail> getCommisJunctionData(Long userId, Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = " getMediaDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<RetailerDetail> commissionDatalst = null;
		try
		{
			commissionDatalst = commonDAO.getCommisJunctionData(userId, productId);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return commissionDatalst;
	}

	/**
	 * This is a Service Method for fetching product information for the given
	 * userId,productId and retailId.This method validates the userId, productId
	 * and retailId if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which product information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productId
	 *            for which product information need to be fetched.If productId
	 *            is null then it is invalid request.
	 * @param retailId
	 *            for which product information need to be fetched.If retailId
	 *            is null then it is invalid request.
	 * @return XML containing product information in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ProductDetail fetchProductAttributes(Long userId, Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductAttributes of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetail = null;
		try
		{
			productDetail = baseDAO.fetchProductAttributeDetails(userId, productId);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetail;
	}

	/**
	 * This method is used to display product details like name,image,audio
	 * ,video and attributes.
	 */

	public ProductDetail fetchConsumerProductDetails(ProductDetail productDetail) throws ScanSeeServiceException
	{
		final String methodName = "fetchConsumerProductDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetails = null;
		try
		{
			productDetails = baseDAO.fetchConsumerProductDetails(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * This method is used to display product details like name,image,audio
	 * ,video and attributes.
	 */
	public List<ProductDetail> fetchConsumerProductImagelst(SearchForm searchForm) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchConsumerProductImagelst of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productImagelst = null;
		try
		{
			productImagelst = baseDAO.fetchConsumerProductImagelst(searchForm);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productImagelst;

	}

	/**
	 * This method is used to display product details like name,image,audio
	 * ,video and attributes.
	 */

	public List<ProductDetail> fetchConsumerProductMediaInfo(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductMediaInfo of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> prodMediaInfoList = null;
		try
		{
			prodMediaInfoList = baseDAO.fetchConsumerProductMediaInfo(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return prodMediaInfoList;
	}

	public List<ProductDetail> fetchConsumerProductAttributes(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductAttributes of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productAttributesList = null;
		try
		{
			productAttributesList = baseDAO.fetchConsumerProductAttributeDetails(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productAttributesList;
	}

	public HotDealsDetails getConsumerHdProdInfo(HotDealsListRequest hotDealsListRequestObj) throws ScanSeeServiceException
	{
		final String methodName = "getConsumerHdProdInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		HotDealsDetails hotDealsDetailsObj = null;
		try
		{
			hotDealsDetailsObj = baseDAO.getConsumerHotDealDetail(hotDealsListRequestObj);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsDetailsObj;
		// }
	}

	public FindNearByDetails getConsFindNearByRetailer(ProductDetail productDetailObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsFindNearByRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		FindNearByDetails findNearByDetails = new FindNearByDetails();
		try
		{
			findNearByDetails = baseDAO.getConsFindNearByRetailer(productDetailObj);

		}
		catch (Exception e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return findNearByDetails;
	}

	public FindNearByIntactResponse getConsFindRetailersExternalAPI(ProductDetail productDetailObj) throws ScanSeeWebSqlException,
			ScanSeeExternalAPIException
	{
		final String methodName = " findRetailersExternalAPI ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.info("::::::Invocation of External API::::::::::");
		String response = null;
		final FindNearByRequest findNearByRequest = new FindNearByRequest();
		ProductDetail productInfo = null;
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();
		UpdateUserInfo updateUserInfo = null;
		String externalAPIURL;
		Integer intPageNo = 0;
		@SuppressWarnings("unused")
		ExternalAPIErrorLog externalAPIErrorLog = null;
		productInfo = commonDAO.getProductInfoforExternalAPI(productDetailObj.getProductId());
		// updateUserInfo = manageSettingsDao.fetchUserInfo(userID);
		FindNearByIntactResponse findNearByIntactResponseObj = null;
		Integer intPostalcode = null;
		if (null != productInfo)
		{

			if (null != productInfo.getBarCode() && !productInfo.getBarCode().equals(ApplicationConstants.NOTAPPLICABLE))
			{

				findNearByRequest.setUpcCode(productInfo.getBarCode());
			}
			else if (null != productInfo.getSkuCode() && !productInfo.getSkuCode().equals(ApplicationConstants.NOTAPPLICABLE))
			{
				findNearByRequest.setSkuCode(productInfo.getSkuCode());
			}
			else if (null != productInfo.getProductName() && !productInfo.getProductName().equals(ApplicationConstants.NOTAPPLICABLE))
			{
				String encodedProdName = null;

				try
				{
					encodedProdName = URLEncoder.encode(productInfo.getProductName(), "UTF-8");
				}
				catch (UnsupportedEncodingException e)
				{
					LOG.info("No product Information is available to invoke external API" + e.getMessage());
				}

				findNearByRequest.setKeywords(encodedProdName);
			}
			else
			{
				// No product Information is available to invoke external API
				LOG.info("No product Information is available to invoke external API");
				return null;
			}

				if(null != productDetailObj.getZipcode() && !"".equals(productDetailObj.getZipcode()))
						{
					findNearByRequest.setZipCode(productDetailObj.getZipcode());
						}else
						{
							if (null != productDetailObj.getUserId() && !"".equals(productDetailObj.getUserId()))
							{
								
								intPostalcode = baseDAO.getPostalCode( productDetailObj.getUserId());
								if(null != intPostalcode && !"".equals(intPostalcode) )
								{
									findNearByRequest.setZipCode(String.valueOf(intPostalcode));
								}
							}
						}
				
						findNearByRequest.setRadius(String.valueOf(productDetailObj.getRadius()));
			findNearByRequest.setFormat(ApplicationConstants.EXTERNALAPIFORMAT);
			findNearByRequest.setPage(String.valueOf(intPageNo));
			Integer userId = null;
			if (productDetailObj.getUserId() != null)
			{

				userId = productDetailObj.getUserId().intValue();
			}
			findNearByRequest.setUserId(userId);
			findNearByRequest.setValues();

			final FindNearByService findNearByService = new FindNearByServiceImpl();

			externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDNEARBYMODULENAME);

			if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
			{
				LOG.info("Invoking External API for Module::::::::" + ApplicationConstants.FINDNEARBYMODULENAME + " With Parameters:Product ID:"
						+ productDetailObj.getProductId() + " Product Name:" + productInfo.getProductName() + " Latitude::"
						+ productDetailObj.getLatitude() + " Logitude::" + productDetailObj.getLongitude() + " Radius::"
						+ productDetailObj.getRadius());

				findNearByIntactResponseObj = findNearByService.processFindNearByRequestEmailTemplate(externalAPIInformation, findNearByRequest);
				externalAPIURL = findNearByService.getApiURL();
				if (null != externalAPIURL)
				{
					commonDAO.insertExternalAPIURL(externalAPIURL, userId);

				}

				if (findNearByService.getResponsecode() != 11000)
				{
					response = null;
				}
				if (ApplicationConstants.EXTERNALAPIFAILURE == findNearByService.getResponsecode())
				{
					externalAPIErrorLog = findNearByService.getExternalAPIErrors();
					// commonDAO.LogExternalApiErrorMessages(externalAPIErrorLog);
				}

				LOG.info("XML Response from External API:::::" + response);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return findNearByIntactResponseObj;
	}

	public List<ProductDetail> fetchConsumerProductReviews(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductReviews";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productReviewsList = null;
		try
		{
			productReviewsList = baseDAO.fetchConsumerProductReviews(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productReviewsList;
	}

	public ArrayList<RetailerDetail> getConsumerCommisJunctionData(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = " getMediaDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<RetailerDetail> commissionDatalst = null;
		try
		{
			commissionDatalst = baseDAO.getConsumerCommisJunctionData(productDetail);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return commissionDatalst;
	}

	public List<CouponDetail> fetchConsumerProductCoupons(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchConsumerProductCoupons";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetail> productCouponsList = null;
		try
		{
			productCouponsList = baseDAO.fetchConsumerProductCoupons(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productCouponsList;
	}

	public Integer getDefaultRadius() throws ScanSeeWebSqlException
	{
		final String methodName = "getDefaultRadius";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer defaultRadius = null;
		try
		{
			defaultRadius = baseDAO.getDefaultRadius();

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return defaultRadius;
	}

	public ProductDetail consAddProdToShopp(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddProdToShopp";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consAddProdToShopp(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	public ProductDetail consAddProdToWishList(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddProdToWishList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToWL = null;
		try
		{
			addProdToWL = baseDAO.consAddProdToWishList(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToWL;
	}

	public Long consGetUsrTrakingMainMenuId(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consGetUsrTrakingMainMenuId";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Long mainMenuId = null;
		try
		{
			mainMenuId = baseDAO.consGetUsrTrakingMainMenuId(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return mainMenuId;
	}

	public String consShareProductInfoViaEmail(ProductDetail shareProductInfoObj) throws ScanSeeWebSqlException
	{
		final String methodName = " shareProductInfo of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String productInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = "Product information shared from Scansee";
		String msgBody = null;
		RetailerDetail retailerDetail = null;
		String shareURL = null;
		ArrayList<AppConfiguration> stockInfoList = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;

		try
		{
			if (shareProductInfoObj != null)
			{
				stockInfoList = baseDAO.consGetAppConfig(ApplicationConstants.CONFIGURATIONTYPESHAREURLFORWEB);

				for (int i = 0; i < stockInfoList.size(); i++)
				{
					if (stockInfoList.get(i).getScreenName().equals(ApplicationConstants.CONFIGURATIONTYPESHAREURLFORWEB))
					{
						shareURL = stockInfoList.get(i).getScreenContent();
					}

				}

				productInfo = baseDAO.ShareConsumerProductDetails(shareProductInfoObj, shareURL);

				subject = ApplicationConstants.SHAREPRODUCTINFOBYEMAIL;

				if (shareProductInfoObj.getFromEmailId() == null)
				{
					if (shareProductInfoObj.getUserId() != null)
					{
						// if user is registered ,fetching user email id from
						// database.

						userEmailId = baseDAO.getConsUserInfo(shareProductInfoObj);
						if (userEmailId == null)
						{
							LOG.info("Validation failed::::To Email is not available");

							response = ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE;
							return response;
						}
					}
				}
				else
				{
					userEmailId = shareProductInfoObj.getFromEmailId();
				}
				toAddress = shareProductInfoObj.getToEmailId();
				emailConf = baseDAO.consGetAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				msgBody = productInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				LOG.info("Mail delivered to:" + toAddress);
				// response =
				// Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
				// ApplicationConstants.SHAREPRODUCTINFO);
				response = ApplicationConstants.SHAREPRODUCTINFO;
			}
		}
		catch (MessagingException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = ApplicationConstants.INVALIDEMAILADDRESSTEXT;
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : shareProductInfo : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getCause());
		}

		LOG.info(com.scansee.externalapi.common.constants.ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public ExternalAPIInformation getConsExternalApiInfo(String moduleName) throws ScanSeeServiceException
	{

		ExternalAPIInformation externalAPIInformation = null;

		try
		{

			externalAPIInformation = baseDAO.getConsExternalApiInfo(moduleName);

		}
		catch (ScanSeeWebSqlException exception)
		{

			throw new ScanSeeServiceException(exception.getMessage());
		}

		return externalAPIInformation;
	}

	public UserRatingInfo consGetProductRatings(ProductDetail productRatings) throws ScanSeeWebSqlException
	{
		final String methodName = "consGetUsrTrakingMainMenuId";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		UserRatingInfo userRatingInfoObj = null;
		try
		{
			userRatingInfoObj = baseDAO.consGetProductRatings(productRatings);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userRatingInfoObj;
	}

	public String consSaveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "consSaveUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		response = baseDAO.consSaveUserProductRating(userRatingInfo);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String getConsUserEmailId(Long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "consSaveUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		response = baseDAO.getConsEmailId(userId);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String consShareHotDealInfoViaEmail(ProductDetail shareProductInfoObj) throws ScanSeeWebSqlException
	{
		final String methodName = "consShareHotDealInfoViaEmail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String hotdealInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = "HotDeal information shared from Scansee";
		String msgBody = null;
		RetailerDetail retailerDetail = null;
		String shareURL = null;
		ArrayList<AppConfiguration> stockInfoList = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;

		try
		{
			if (shareProductInfoObj != null)
			{
				stockInfoList = baseDAO.consGetAppConfig(ApplicationConstants.CONFIGURATIONTYPESHAREURL);

				for (int i = 0; i < stockInfoList.size(); i++)
				{
					if (stockInfoList.get(i).getScreenName().equals(ApplicationConstants.EMAILSHAREURL))
					{
						shareURL = stockInfoList.get(i).getScreenContent();
					}

				}

				hotdealInfo = baseDAO.ShareConsumerHodDealDetails(shareProductInfoObj, shareURL);

				subject = ApplicationConstants.SHAREHOTDEALINFO;

				if (shareProductInfoObj.getFromEmailId() == null)
				{
					if (shareProductInfoObj.getUserId() != null)
					{
						// if user is registered ,fetching user email id from
						// database.

						userEmailId = baseDAO.getConsUserInfo(shareProductInfoObj);
						if (userEmailId == null)
						{
							LOG.info("Validation failed::::To Email is not available");

							response = ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE;
							return response;
						}
					}
				}
				else
				{
					userEmailId = shareProductInfoObj.getFromEmailId();
				}
				toAddress = shareProductInfoObj.getToEmailId();
				emailConf = baseDAO.consGetAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				msgBody = hotdealInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				LOG.info("Mail delivered to:" + toAddress);
				// response =
				// Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
				// ApplicationConstants.SHAREPRODUCTINFO);
				response = ApplicationConstants.SHAREPRODUCTINFO;
			}
		}
		catch (MessagingException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = ApplicationConstants.INVALIDEMAILADDRESSTEXT;
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Inside thislocationserviceimpl : shareProductInfo : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getCause());
		}

		LOG.info(com.scansee.externalapi.common.constants.ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public List<HotDealsDetails> consDisplayPopulationCenters(HotDealsListRequest hotDealsListRequestObj) throws ScanSeeWebSqlException
	{
		final String methodName = "consDisplayPopulationCenters";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> populationCenterslst = null;
		try
		{
			populationCenterslst = baseDAO.consDisplayPopulationCenters(hotDealsListRequestObj);

		}
		catch (Exception e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return populationCenterslst;
	}

	public String getConsShareEmailConfiguration() throws ScanSeeServiceException
	{
		String shareURL = null;
		ArrayList<AppConfiguration> appConfiguration = null;

		try
		{
			appConfiguration = baseDAO.consGetAppConfig(ApplicationConstants.CONFIGURATIONTYPESHAREURLFORWEB);
			for (int i = 0; i < appConfiguration.size(); i++)
			{
				if (appConfiguration.get(i).getScreenName().equals(ApplicationConstants.CONFIGURATIONTYPESHAREURLFORWEB))
				{
					shareURL = appConfiguration.get(i).getScreenContent();
				}

			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return shareURL;
	}

	/**
	 * This method returns the shopper details.
	 * 
	 * @param userID
	 * @return Users
	 * @throws ScanSeeServiceException
	 */
	public Users getLoginUserDetails(Long userID) throws ScanSeeServiceException
	{

		final String methodName = "getLoginUserDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users userInfo = null;

		try
		{
			userInfo = commonDAO.getLoginUserDetails(userID);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
			throw new ScanSeeServiceException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userInfo;
	}

	public List<UserTrackingData> getConsumerShareTypes() throws ScanSeeWebSqlException
	{
		final String methodName = "getConsumerShareTypes";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<UserTrackingData> lstUserTrackingDatas = null;
		try
		{
			lstUserTrackingDatas = baseDAO.getConsumerShareTypes();
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return lstUserTrackingDatas;
	}

	public ProductDetail saveConsShareProdDetails(ProductDetail ObjProductDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsShareProdDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail objpProductDetail = null;
		List<UserTrackingData> lstUserTrackingDatas = null;
		try
		{
			objpProductDetail = baseDAO.saveConsShareProdDetails(ObjProductDetail);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objpProductDetail;
	}

	public ProductDetail getConsumerProdMultipleImages(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsumerProdMultipleImages";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetails = null;
		try
		{
			productDetails = baseDAO.getConsumerProdMultipleImages(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * This method is used to fetch user Shopping list products.
	 * 
	 * @param objProductDetail
	 *            contains the userId,lower limit.
	 * @throws ScanSeeWebSqlException
	 *             which will be caught in controller layer.
	 */
	public AddRemoveSBProducts getConsShopLstProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getConsShopLstProds";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		AddRemoveSBProducts objAddRemoveSBProducts = null;

		objAddRemoveSBProducts = baseDAO.getConsShopLstProds(objConsumerRequestDetails);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return objAddRemoveSBProducts;
	}

	
	public AddRemoveSBProducts getConsSLHistoryProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getConsSLHistoryProds";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		AddRemoveSBProducts objAddRemoveSBProducts = null;

		objAddRemoveSBProducts = baseDAO.getConsSLHistoryProds(objConsumerRequestDetails);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return objAddRemoveSBProducts;
	}

	
	public AddRemoveSBProducts getConsSLFavoriteProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getConsSLFavoriteProds";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		AddRemoveSBProducts objAddRemoveSBProducts = null;

		objAddRemoveSBProducts = baseDAO.getConsSLFavoriteProds(objConsumerRequestDetails);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return objAddRemoveSBProducts;
	}

	
	public ProductDetails getSLSearchProducts(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getConsSLFavoriteProds";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ProductDetails objProductDetails = null;

		objProductDetails = baseDAO.getSLSearchProducts(objConsumerRequestDetails);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return objProductDetails;
	}

	
	public ProductDetails getConsShopSearchProd(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getConsShopSearchProd";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ProductDetails objProductDetails = null;

		objProductDetails = baseDAO.getConsShopSearchProd(objConsumerRequestDetails);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return objProductDetails;
	}

	
	public ProductDetail consFavDeleteProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consFavDeleteProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consFavDeleteProd(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	
	public ProductDetail consAddHistoryProdToSL(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddHistoryProdToSL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consAddHistoryProdToSL(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	
	public ProductDetail consAddFavProdToShopList(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddFavProdToShopList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consAddFavProdToShopList(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	
	public ProductDetail consAddSearchProdToSL(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddSearchProdToSL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consAddSearchProdToSL(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	@SuppressWarnings("static-access")
	
	public WishListProducts getConsWishListProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsWishListProds";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		WishListProducts objWishListProducts = null;
		ProductDetails objProductDetail = null;
		Integer intNextPage = null;
		Integer intMaxCount = null;
		ArrayList<ProductDetail> productDetaillst = null;
		try
		{
			objProductDetail = baseDAO.getConsWishListProds(objConsumerRequestDetails);

			if (null != objProductDetail )
			{
				
				intMaxCount = objProductDetail.getTotalSize();
				ConsWishListHelper wishlisthelperObj = new ConsWishListHelper();
				productDetaillst = (ArrayList<ProductDetail>) objProductDetail.getProductDetail();
				objWishListProducts = wishlisthelperObj.getWishListDetails(productDetaillst);
				objWishListProducts.setTotalSize(intMaxCount);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objWishListProducts;
	}

	
	public ProductDetail consDeleteTodayListProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consDeleteTodayListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consDeleteTodayListProd(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	
	public ProductDetail consCheckOutTodayListProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consCheckOutTodayListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consCheckOutTodayListProd(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	
	public ProductDetail consMoveToBasketProds(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consMoveToBasketProds";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consMoveToBasketProds(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	@SuppressWarnings("static-access")
	
	public WishListHistoryDetails getConsWLHistoryProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsWLHistoryProds";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		WishListHistoryDetails objWishListHistoryDetails = null;
		ArrayList<ProductDetail> productDetaillst = null;
		Integer intNextPage = null;
		Integer intMaxCount = null;
		try
		{
			productDetaillst = baseDAO.getConsWLHistoryProds(objConsumerRequestDetails);

			if (null != productDetaillst && !productDetaillst.isEmpty())
			{
				intNextPage = productDetaillst.get(0).getNextPage();
				intMaxCount = productDetaillst.get(0).getTotalSize();
				ConsWishListHelper wishlisthelperObj = new ConsWishListHelper();
				objWishListHistoryDetails = wishlisthelperObj.getWishListHistory(productDetaillst);
				objWishListHistoryDetails.setTotalSize(intMaxCount);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objWishListHistoryDetails;
	}

	
	public ProductDetail consDeleteWLProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consDeleteWLProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consDeleteWLProd(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	
	public ProductDetail consDeleteWLHistoryProd(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consDeleteWLHistoryProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consDeleteWLHistoryProd(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	
	
	public ProductDetail consAddSearchProdToWL(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consAddSearchProdToWL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail addProdToShopp = null;
		try
		{
			addProdToShopp = baseDAO.consAddSearchProdToWL(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addProdToShopp;
	}

	
	public ArrayList<CouponDetails> getConsWLCouponDetails(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		 ArrayList<CouponDetails> lstCouponDetails = null;
		try
		{
			lstCouponDetails = baseDAO.getConsWLCouponDetails(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return lstCouponDetails;
	}

	
	public ArrayList<HotDealsDetails> getConsWLHotDealInfo(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getConsWLHotDealInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<HotDealsDetails> lstHotdeals = null;
		try
		{
			lstHotdeals = baseDAO.getConsWLHotDealInfo(productDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return lstHotdeals;
	}

	
	public ProductDetail getProductInfoforExternalAPI(Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "getProductInfoforExternalAPI";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail objProductDetail = null;
		try
		{
			objProductDetail = commonDAO.getProductInfoforExternalAPI(productId);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objProductDetail;
	}

	
	public String consWLAddCoupon(CouponDetail objCouponDetail) throws ScanSeeWebSqlException
	{

		final String methodName = "consWLAddCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String addCoupon = null;
		try
		{
			addCoupon = baseDAO.consWLAddCoupon(objCouponDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addCoupon;
	}

	
	public String consWLRemoveCoupon(CouponDetail objCouponDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "consWLRemoveCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String removeCoupon = null;
		try
		{
			removeCoupon = baseDAO.consWLRemoveCoupon(objCouponDetail);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("error occured in " + methodName + " method");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return removeCoupon;
	}

	
	public ArrayList<ProductDetail> getPrintProductInfo(ProductDetail productDetail) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getPrintProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ArrayList<ProductDetail> objProductDetails = null;

		objProductDetails = baseDAO.getPrintProductInfo(productDetail);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return objProductDetails;
	}

	
	public ProductDetails getConsCoupAssocitedProds(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException
	{
		final String strMethodName = "getPrintProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ProductDetails objProductDetails = null;

		objProductDetails = baseDAO.getConsCoupAssocitedProds(clrDetailsObj);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);

		return objProductDetails;
	}

}

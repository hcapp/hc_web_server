package shopper.service;

import java.util.ArrayList;

import common.exception.ScanSeeServiceException;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;

public interface ShopperSearchService {
	public ArrayList<ProductVO> getAllProduct(String zipcode, String productName, int lowerLimit, String screenName);

	public ProductVO getAllProductInfo(Integer retailLocationID, Integer productID)throws ScanSeeServiceException;

	public SearchResultInfo searchProducts(Users loginUser, SearchForm objForm,int lowerLimit) throws ScanSeeServiceException;
	public SearchResultInfo searchDeals(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException;
}

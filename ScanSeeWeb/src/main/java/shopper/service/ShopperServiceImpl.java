package shopper.service;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.dao.ShopperDAO;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.DoubleCheckInfo;
import common.pojo.FaceBookConfiguration;
import common.pojo.NonProfitPartner;
import common.pojo.NonProfitPartnerInfo;
import common.pojo.Preferences;
import common.pojo.SchoolInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Universities;
import common.pojo.University;
import common.pojo.UserPreference;
import common.pojo.UserPreferenceDisplay;
import common.pojo.Users;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.DiscountInfo;
import common.pojo.shopper.DiscountUserInfo;
import common.pojo.shopper.DoubleCheck;
import common.pojo.shopper.LoyaltyDetail;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.RebateDetail;
import common.util.EncryptDecryptPwd;
import common.util.Utility;

/**
 * This class is a service implementation class for ShopperService.
 * @author manjunath_gh
 *
 */
public class ShopperServiceImpl implements ShopperService
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ShopperServiceImpl.class);

	/**
	 * Instance variable for FirstUseDAO.
	 */

	private ShopperDAO shopperDAO;
	/**
	 * 
	 * @param shopperDAO
	 */
	public void setShopperDAO(ShopperDAO shopperDAO)
	{
		this.shopperDAO = shopperDAO;
	}
	/**
	 * 
	 */
	public Users processFaceBookUser(String fbUserID) throws ScanSeeServiceException
	{
		final String methodName = "processFaceBookUser";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users userInfo = null;

		try
		{
			userInfo = shopperDAO.registerFaceBookUser(fbUserID);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getCause());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userInfo;
	}

	/**
	 * This method saves shopper registration information.
	 *
	 * @param shopperRegistrationInfo
	 * @param strLogoImage
	 * @return
	 * @throws ScanSeeServiceException
	 */
	public Users processShopperProfileDetails(Users objUsers, String strLogoImage) throws ScanSeeServiceException
	{
		final String methodName = "processShopperProfileDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		Users user = null;
		String smtpHost = null;
		String smtpPort = null;
		String strAdminEmailId = null;
		EncryptDecryptPwd enryptDecryptpwd;
		String encryptedpwd = null;
		
		try
		{
			enryptDecryptpwd = new EncryptDecryptPwd();
			encryptedpwd = enryptDecryptpwd.encrypt(objUsers.getPassword());
			objUsers.setPassword(encryptedpwd);
			
			user = shopperDAO.addShopperRegistration(objUsers);
			
			if (user.getIsProfileCreated() != null && user.getIsProfileCreated().equals(ApplicationConstants.SUCCESS))
			{
				final ArrayList<AppConfiguration> emailConf = shopperDAO.getAppConfig(ApplicationConstants.EMAILCONFIG);
				
				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}
				
				final ArrayList<AppConfiguration> adminEmailList = shopperDAO.getAppConfig(ApplicationConstants.WEBREGISTRATION);
				
				for (int j = 0; j < adminEmailList.size(); j++)
				{
					if (adminEmailList.get(j).getScreenName().equals(ApplicationConstants.ADMINEMAILID))
					{
						strAdminEmailId = adminEmailList.get(j).getScreenContent();
					}
				}
				
				Utility.sendMailShopperLoginSuccess(user, smtpHost, smtpPort, strAdminEmailId, strLogoImage);
				
			}
		}
		catch (GeneralSecurityException e) 
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			e.getStackTrace();
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeServiceException(e.getMessage());
		}		
		
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		return user;
	}
	/**
	 * 
	 */
	public String processPreferenceInfo(CategoryDetail categoryDetail, Long userId) throws ScanSeeServiceException
	{
		String response = null;
		try
		{
			response = shopperDAO.addUserPreferences(categoryDetail, userId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getCause());
		}
		return response;
	}
	/**
	 * 
	 */
	public String processSchoolInfo(SchoolInfo schoolInfo) throws ScanSeeServiceException
	{
		String response = null;
		List<University> arUniversityList = null;

		List<NonProfitPartner> arNonProfitList = null;
		Long preferenceId;
		ArrayList<Long> selectedUniversityIdsList;
		ArrayList<Long> selectedNonProfitIdsList;

		try
		{

			selectedUniversityIdsList = new ArrayList<Long>();
			selectedNonProfitIdsList = new ArrayList<Long>();

			arUniversityList = shopperDAO.getAllUniversity();

			if (schoolInfo.getUniversityList() != null && arUniversityList != null)
			{

				for (int i = 0; i < schoolInfo.getUniversityList().size(); i++)
				{

					for (int j = 0; j < arUniversityList.size(); j++)
					{

						if (schoolInfo.getUniversityList().get(i).equals(arUniversityList.get(j).getUniversityName()))
						{
							preferenceId = arUniversityList.get(j).getUniversityID();
							selectedUniversityIdsList.add(preferenceId);
						}
					}
				}
			}

			arNonProfitList = shopperDAO.getAllNonProfitPartner();

			if (schoolInfo.getNonProfitList() != null && arNonProfitList != null)
			{
				for (int i = 0; i < schoolInfo.getNonProfitList().size(); i++)
				{

					for (int j = 0; j < arNonProfitList.size(); j++)
					{

						if (schoolInfo.getNonProfitList().get(i).equals(arNonProfitList.get(j).getNonProfitPartnerName()))
						{
							preferenceId = arNonProfitList.get(j).getNonProfitPartnerID();
							selectedNonProfitIdsList.add(preferenceId);
						}
					}
				}
			}

			response = shopperDAO.addSchoolSelection(schoolInfo, selectedUniversityIdsList, selectedNonProfitIdsList);
		}
		catch (ScanSeeWebSqlException exception)
		{
			exception.printStackTrace();
		}
		return response;
	}
	/**
	 * 
	 */
	public String processDoubleCheckInfo(DoubleCheckInfo doubleCheckInfo) throws ScanSeeServiceException
	{

		try
		{
			shopperDAO.addDoubleCheckInfo(doubleCheckInfo);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getCause());
		}
		return "SUCCESS";

	}
	/**
	 * This method for getting the All Shopper Preferences Category List.
	 */
	public List<Preferences> getShopperPreferences() throws ScanSeeServiceException
	{
		final String methodName = "getShopperPreferences";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Preferences> preferencesList = null;
		List<Category> arCategoryList = null;

		try
		{
			preferencesList = new ArrayList<Preferences>();
			arCategoryList = shopperDAO.getAllCategory();

			final Iterator<Category> iterator = arCategoryList.iterator();

			while (iterator.hasNext())
			{
				Category objCategory = (Category) iterator.next();
				String catName = objCategory.getParentCategoryName();

				preferencesList.add(new Preferences(catName, catName));

			}

		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return preferencesList;
	}
	/**
	 * This method for getting the All Universities List.
	 */
	public List<Universities> getUniversities() throws ScanSeeServiceException
	{

		final String methodName = "getUniversities";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<Universities> universityList = null;
		List<University> arUniversityList = null;

		try
		{
			universityList = new ArrayList<Universities>();
			arUniversityList = shopperDAO.getAllUniversity();

			final Iterator<University> iterator = arUniversityList.iterator();

			while (iterator.hasNext())
			{
				University objUniversity = (University) iterator.next();
				String universityName = objUniversity.getUniversityName();
				universityList.add(new Universities(universityName, universityName));
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return universityList;

	}

	/**
	 *  This method for getting the All Non Profit Partners List.
	 * @return List<NonProfitPartnerInfo>
	 * @throws ScanSeeServiceException
	 */
	public List<NonProfitPartnerInfo> getNonProfitPartners() throws ScanSeeServiceException
	{
		final String methodName = "getNonProfitPartners";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		List<NonProfitPartnerInfo> nonProfitPartList = null;
		List<NonProfitPartner> arNonProfitPartnerList = null;

		try
		{
			nonProfitPartList = new ArrayList<NonProfitPartnerInfo>();
			arNonProfitPartnerList = shopperDAO.getAllNonProfitPartner();

			final Iterator<NonProfitPartner> iterator = arNonProfitPartnerList.iterator();

			while (iterator.hasNext())
			{
				NonProfitPartner objNonProPartner = (NonProfitPartner) iterator.next();
				String partenerName = objNonProPartner.getNonProfitPartnerName();

				nonProfitPartList.add(new NonProfitPartnerInfo(partenerName, partenerName));
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return nonProfitPartList;

	}
	/**
	 * This method returns shopper registered information.
	 * @param shopperId
	 * @return UserPreferenceDisplay
	 * @throws ScanSeeServiceException
	 */
	public UserPreferenceDisplay getDoubleCheckDetails(Long shopperId) throws ScanSeeServiceException
	{
		final String methodName = "getDoubleCheckDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		UserPreferenceDisplay userPreferenceDisplay;
		try
		{
			userPreferenceDisplay = shopperDAO.displayPreferencesDetails(shopperId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userPreferenceDisplay;

	}
	
	/**
	 * This method for getting the All state List based .
	 * @return ArrayList<SchoolInfo>
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<SchoolInfo> getAllStates() throws ScanSeeServiceException
	{
		final String methodName = "getAllStates";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		ArrayList<SchoolInfo> states = null;
		try
		{
			states = shopperDAO.getAllStates();
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return states;
	}

	/**
	 * This method for getting the All College List based on the state code.
	 * @param stateCode
	 * @return ArrayList<SchoolInfo>
	 * @throws ScanSeeServiceException
	 */
	public ArrayList<SchoolInfo> getAllColleges(String stateCode) throws ScanSeeServiceException
	{
		final String methodName = "getAllColleges";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		ArrayList<SchoolInfo> collegeList = null;
		try
		{
			collegeList = shopperDAO.getCollegeList(stateCode);
		}
		catch (ScanSeeWebSqlException e)
		{

			throw new ScanSeeServiceException(e.getCause());
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return collegeList;
	}

	/**
	 * This method for saving the all the school information.
	 * @param schoolInfo
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String saveSchoolInfo(SchoolInfo schoolInfo) throws ScanSeeServiceException
	{

		final String methodName = "saveSchoolInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{

			response = shopperDAO.addSchoolInfo(schoolInfo);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method for saving the all the school information.
	 * @param userId
	 * @return SchoolInfo
	 * @throws ScanSeeServiceException
	 */
	public SchoolInfo displaySchoolInfo(Long userId) throws ScanSeeServiceException
	{
		final String methodName = "displaySchoolInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		SchoolInfo schoolInfo = null;

		try
		{

			schoolInfo = shopperDAO.displaySchoolInfo(userId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return schoolInfo;

	}
	/**
	 * 
	 */
	public SearchResultInfo searchProducts(Users loginUser, SearchForm objForm, int lowerLimit) throws ScanSeeServiceException
	{

		SearchResultInfo resultInfo = null;
		try
		{
			resultInfo = shopperDAO.fetchAllProduct(objForm, loginUser, lowerLimit);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return resultInfo;

	}
	/**
	 * 
	 */
	public DiscountInfo getDiscountInfo(DiscountUserInfo discountUserInfo) throws ScanSeeServiceException
	{

		DiscountInfo discountInfo = null;
		List<CouponDetail> couponDetail = null;
		List<LoyaltyDetail> loyaltyDetail = null;
		List<RebateDetail> rebateDetail = null;

		try
		{
			if (discountUserInfo != null)
			{
				discountInfo = new DiscountInfo();
				couponDetail = shopperDAO.fetchCouponInfo(discountUserInfo);
				loyaltyDetail = shopperDAO.fetchLoyaltyInfo(discountUserInfo);
				rebateDetail = shopperDAO.fetchRebateInfo(discountUserInfo);
				discountInfo.setCouponDetail(couponDetail);
				discountInfo.setRebateDetail(rebateDetail);
				discountInfo.setLoyaltyDetail(loyaltyDetail);

			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return discountInfo;

	}
	/**
	 * 
	 */
	public String couponAdd(Long userId, Integer couponId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = shopperDAO.addCoupon(userId, couponId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	/**
	 * 
	 */
	public String couponRemove(Long userId, Integer couponId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = shopperDAO.removeCoupon(userId, couponId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	/**
	 * 
	 */
	public String rebateAdd(Long userId, Integer couponId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = shopperDAO.addRebate(userId, couponId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	/**
	 * 
	 */
	public String rebateRemove(Long userId, Integer couponId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = shopperDAO.removeRebate(userId, couponId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	/**
	 * 
	 */
	public String loyaltyAdd(Long userId, Integer couponId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = shopperDAO.addLoyalty(userId, couponId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	/**
	 * 
	 */
	public String loyaltyRemove(Long userId, Integer couponId) throws ScanSeeServiceException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		try
		{
			response = shopperDAO.removeLoyalty(userId, couponId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	/**
	 * 
	 */
	public ExternalAPIInformation externalApiInfo(String moduleName) throws ScanSeeServiceException
	{

		ExternalAPIInformation externalAPIInformation = null;

		try
		{

			externalAPIInformation = shopperDAO.getExternalApiInfo(moduleName);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return externalAPIInformation;

	}
	/**
	 * 
	 */
	public ProductDetail fetchProductDetails(String productId) throws ScanSeeServiceException
	{

		ProductDetail prdDetail = null;

		try
		{

			prdDetail = shopperDAO.getProductDetails(productId);

		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getMessage());
		}

		return prdDetail;

	}
	/**
	 * 
	 */
	public String getUniversityName(Integer universityId) throws ScanSeeServiceException
	{

		String univerSityName = null;

		try
		{

			univerSityName = shopperDAO.fetchUniversityName(universityId);
		}
		catch (ScanSeeWebSqlException exception)
		{
			throw new ScanSeeServiceException(exception.getCause());
		}

		return univerSityName;

	}

	/**
	 * The service method for fetching user favorite categories information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @param lowerlimit
	 *            for pagination
	 * @return XML containing user favourite categories information in the
	 *         response.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	public final CategoryDetail getUserFavCategories(Long userId, Integer lowerlimit) throws ScanSeeServiceException
	{

		final String methodName = "getUserFavCategories";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		CategoryDetail categoryDetailresponse = null;

		if (lowerlimit == null)
		{
			lowerlimit = 0;
		}
		try
		{
			if (userId != null)
			{
				categoryDetailresponse = shopperDAO.fetchUserFavCategories(userId, lowerlimit);

			}

		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e.getCause());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return categoryDetailresponse;

	}

	/**
	 * The Service method for Saving User favourite categories Information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param Categories
	 *            Contains user favourite categories information.
	 * @return returns SUCCESS or ERROR.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */

	public String setUserFavCategories(String categoryId, Long userID, boolean cellPhoneStatus, boolean emailStatus) throws ScanSeeServiceException
	{
		final String methodName = "setUserFavCategories";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		String response = null;
		try
		{
			response = shopperDAO.createOrUpdateFavCategories(categoryId, userID, cellPhoneStatus, emailStatus);
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			throw new ScanSeeServiceException(e.getCause());
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	/**
	 * 
	 */
	public List<DoubleCheck> getAllpreferences(Long UserId) throws ScanSeeServiceException
	{

		List<DoubleCheck> preferencesList = null;
		try
		{
			preferencesList = shopperDAO.fetchAllCategory(UserId);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}
		return preferencesList;
	}
	/**
	 * The Service method for updating the user information
	 * 
	 * @param UserPreference
	 * @return returns SUCCESS or ERROR.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */
	public String updateUserData(UserPreference userPreference) throws ScanSeeServiceException
	{
		final String methodName = "updateUserData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		String result = null;
		
		try
		{
			result = shopperDAO.updateUserData(userPreference);
		}
		catch (ScanSeeWebSqlException e)
		{
			throw new ScanSeeServiceException(e);
		}
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return result;
	}
	/**
	 * 
	 */
	public FaceBookConfiguration getFaceBookConfiguration() throws ScanSeeServiceException
	{
		ArrayList<AppConfiguration> faceBookConf;
		final FaceBookConfiguration faceBookConfiguration = new FaceBookConfiguration();
		try
		{
			faceBookConf = shopperDAO.getAppConfig(ApplicationConstants.FACEBOOKCONFG);

			for (int j = 0; j < faceBookConf.size(); j++)
			{
				if (faceBookConf.get(j).getScreenName().equals(ApplicationConstants.SCANSEEBASEURL))
				{
					faceBookConfiguration.setScanSeeBaseUrl(faceBookConf.get(j).getScreenContent());
				}
				if (faceBookConf.get(j).getScreenName().equals(ApplicationConstants.FBAPPID))
				{
					faceBookConfiguration.setAppId(faceBookConf.get(j).getScreenContent());
				}
				if (faceBookConf.get(j).getScreenName().equals(ApplicationConstants.FBAPPSECRETID))
				{
					faceBookConfiguration.setAppSecretId(faceBookConf.get(j).getScreenContent());
				}
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return faceBookConfiguration;
	}
	/**
	 * 
	 */
	public String getShareEmailConfiguration() throws ScanSeeServiceException
	{
		String shareURL = null;
		ArrayList<AppConfiguration> appConfiguration = null;

		try
		{
			appConfiguration = shopperDAO.getAppConfig(ApplicationConstants.CONFIGURATIONTYPESHAREURL);
			for (int i = 0; i < appConfiguration.size(); i++)
			{
				if (appConfiguration.get(i).getScreenName().equals(ApplicationConstants.EMAILSHAREURL))
				{
					shareURL = appConfiguration.get(i).getScreenContent();
				}

			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		return shareURL;
	}

	/**
	 * This method is used to domain Name for Application configuration.
	 * 
	 * @return String
	 * @throws ScanSeeServiceException
	 */
	public String getDomainName() throws ScanSeeServiceException
	{
		String strDomainName = null;
		try
		{
			final ArrayList<AppConfiguration> domainNameList = shopperDAO.getAppConfigForGeneralImages(ApplicationConstants.SERVER_CONFIGURATION);
			for (int j = 0; j < domainNameList.size(); j++)
			{
				strDomainName = domainNameList.get(j).getScreenContent();
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info("Inside SupplierServiceImpl : getDomainName : " + e);
			throw new ScanSeeServiceException(e);
		}
		return strDomainName;
	}

}

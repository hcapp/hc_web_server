package shopper.service;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.find.controller.ConsumerFindController;

import common.pojo.shopper.RetailerDetail;

/**
 * This class for sorting wish list history based on date.
 * 
 * @author Shyamsundhar_hm
 */
public class MygalleryCouponSort implements Comparator<RetailerDetail>
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(MygalleryCouponSort.class);

	/**
	 * This method is to compare two products.
	 * @param cLRDetailslist1
	 * @param cLRDetailslist2
	 * @return scanDateComp
	 */
	public int compare(RetailerDetail cLRDetailslist1, RetailerDetail cLRDetailslist2)
	{

		LOG.info("inside MygalleryCouponSort class");
		String catName1;
		String catName2;
		int scanDateComp = 0;
		

		try
		{
			catName1 = cLRDetailslist1.getCateName();
			catName2 = cLRDetailslist2.getCateName();
			scanDateComp = catName1.compareTo(catName2);

		}
		catch (Exception exception)
		{
			LOG.info("exception in MygallerySortbyAlpha " + exception);
		}
		LOG.info("exiting MygalleryCouponSort class");
		return scanDateComp;

	}

}
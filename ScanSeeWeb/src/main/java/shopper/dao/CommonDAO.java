package shopper.dao;

import java.util.ArrayList;

import shopper.shoppingList.dao.ShoppingListDAOImpl;

import com.scansee.externalapi.common.pojos.ExternalAPISearchParameters;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.Retailer;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.FindNearByIntactResponse;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;

/**
 * The Interface for ShoppingListDAO. ImplementedBy {@link ShoppingListDAOImpl}
 * 
 * @author malathi_lr
 *
 */
public interface CommonDAO {

	/**
	 * This method Fetching App Configuration details.
	 * 
	 * @return ArrayList.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException;
	
	/**
	 * method to get the Retailers from the database based on query parameters.
	 * 
	 * @param userID
	 *            requested user.
	 * @param productId
	 *            products to be searched for.
	 * @param latitude
	 *            current location of user.
	 * @param longitude
	 *            current location of user.
	 * @param postalcode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @return FindNearByLowestPrice list of retailers.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	FindNearByIntactResponse fetchNearByLowestPrice(Long userID, Double latitude, int productId, Double longitude, String postalcode, double radius)
			throws ScanSeeWebSqlException;
	
	/**
	 * methods to get product info for external API.
	 * 
	 * @param productId
	 *            requested productId.
	 * @return ProductDetail object
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ProductDetail getProductInfoforExternalAPI(Integer productId) throws ScanSeeWebSqlException;
	
	/**
	 * This method is used to insert apiurls into FindNearByLog table.
	 * 
	 * @param apiURL
	 *            -As String parameter
	 * @param userId
	 *            -As int parameter
	 * @return response as failure or success
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	String insertExternalAPIURL(String apiURL, int userId) throws ScanSeeWebSqlException;
	
	/**
	 * methods to get the external API List.
	 * 
	 * @param moduleName
	 *            requested moduleName.
	 * @return externalAPIList
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws ScanSeeWebSqlException;
	
	/**
	 * methods to get the external API Input parameter List.
	 * 
	 * @param apiUsageID
	 *            requested APIUsageID.
	 * @param moduleName
	 *            requested moduleName.
	 * @return externalAPIInputParameters
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ArrayList<ExternalAPISearchParameters> getExternalAPIInputParameters(Integer apiUsageID, String moduleName) throws ScanSeeWebSqlException;

	/**
	 * method to get the Retailers from the database based on query parameters.
	 * 
	 * @param AreaInfoVO
	 *            area information Stored in the instance.
	 * @return FindNearByDetails 
	 *  		  instance containing list of retailers.
	 * @throws ScanSeeWebSqlException
	 *             throws if exception occurs
	 */

	FindNearByDetails fetchNearByInfo(AreaInfoVO objAreaInfoVO, Integer productId) throws ScanSeeWebSqlException;
	
	/**
	 * method to get the particular retailer details in the nearby section
	 * 
	 * @param retailerId
	 * 			retailer Id
	 * 
	 * @return Retailer
	 * 			Retailer instance which stores retailer details
	 * 
	 */
	Retailer findNearByRetailerDetail(Integer retailerId) throws ScanSeeWebSqlException;
	
	/**
	 * methods to get the products media info based on the media type from
	 * database.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return ProductDetails with media details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	ProductDetails getProductMediaDetails(int productID, String mediaType) throws ScanSeeWebSqlException;
	
	/**
	 * methods to get the products media info based on the media type from
	 * database.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return ProductDetails with media details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	ArrayList<RetailerDetail> getCommisJunctionData(Long userId, Integer productId) throws ScanSeeWebSqlException;
	
	/**
	 * This method returns the shopper details.
	 * @param userID
	 * @return Users
	 * @throws ScanSeeWebSqlException
	 */
	public Users getLoginUserDetails(Long userID) throws ScanSeeWebSqlException;
}

package shopper.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.HotDealInfo;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;

public class ShopperSearchDAOImpl implements ShopperSearchDAO
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ShopperSearchDAOImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public ArrayList<ProductVO> fetchAllProduct(String zipcode, String productName, int lowerLimit, String screenName)
	{
		final String methodName = "fetchAllProduct";
		// log.info(ApplicationConstants.METHODSTART + methodName);

		// ProductVO product = null;
		ArrayList<ProductVO> productList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSearchProductName");
			simpleJdbcCall.returningResultSet("productLists", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("ZIPCode", zipcode);
			fetchProductDetailsParameters.addValue("ProductName", productName);
			fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
			fetchProductDetailsParameters.addValue("ScreenName", screenName);
			fetchProductDetailsParameters.addValue("Radius", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			productList = (ArrayList<ProductVO>) resultFromProcedure.get("productLists");

		}
		catch (DataAccessException e)
		{
			// log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName +
			// e);
			// throw new ScanSeeException(e.getMessage());
			e.printStackTrace();
		}
		// log.info(ApplicationConstants.METHODEND + methodName);
		return productList;
	}

	public ProductVO fetchProductInfo(Integer retailLocationID, Integer productID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductInfo";

		LOG.info("In DAO...." + methodName);
		LOG.info("retailLocationID...." + retailLocationID);
		LOG.info("retailLocationID...." + productID);

		ProductVO productInfo = null;
		List<ProductVO> productInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductDetails");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("ProductID", productID);
			fetchProductInfoParameters.addValue("RetailLocationID", retailLocationID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);

			productInfoList = (ArrayList<ProductVO>) resultFromProcedure.get("ProductInfo");
			if (null != productInfoList && !productInfoList.isEmpty())
			{
				productInfo = new ProductVO();
				productInfo = productInfoList.get(0);
			}
		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}
		if (null != productInfo)
		{
			return productInfo;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return productInfo;
	}

	public SearchResultInfo fetchAllProduct(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllProduct";
		// log.info(ApplicationConstants.METHODSTART + methodName);

		// ProductVO product = null;
		SearchResultInfo searchInfo = null;
		int userType = 0;
		MapSqlParameterSource fetchProductDetailsParameters = null;
		try
		{
			searchInfo = new SearchResultInfo();
			if (loginUser != null)
			{
				userType = loginUser.getUserType();
			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			if (userType == 0 || userType == 1)
			{
				simpleJdbcCall.withProcedureName("usp_WebSearchProductName");
				fetchProductDetailsParameters = new MapSqlParameterSource();

				if (objForm.getZipCode().equals(""))
				{
					fetchProductDetailsParameters.addValue("ZIPCode", null);

				}
				else
				{

					fetchProductDetailsParameters.addValue("ZIPCode", objForm.getZipCode());
				}

				fetchProductDetailsParameters.addValue("ProductName", objForm.getSearchKey());
				fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
				fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
				fetchProductDetailsParameters.addValue("Radius", null);
			}
			else
			{
				simpleJdbcCall.withProcedureName("usp_WebProductSearchRetailerOrSupplier");
				fetchProductDetailsParameters = new MapSqlParameterSource();
				fetchProductDetailsParameters.addValue("UserID", loginUser.getUserID());
				fetchProductDetailsParameters.addValue("SearchParameter", objForm.getSearchKey());
				fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
				fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
			}
			simpleJdbcCall.returningResultSet("productLists", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (resultFromProcedure != null)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum)
				{
					List<ProductVO> prodList = (List<ProductVO>) resultFromProcedure.get("productLists");
					if (null != prodList && !prodList.isEmpty())
					{
						int totalSize = (Integer) resultFromProcedure.get("RowCount");
						searchInfo.setTotalSize(totalSize);
					}
					searchInfo.setProductList(prodList);

				}
				else
				{
					LOG.info("Error Occured in registerUser method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}

		return searchInfo;
	}

	public SearchResultInfo searchHotDeals(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		SearchResultInfo searchInfo = null;
		int userType = 0;
		MapSqlParameterSource fetchProductDetailsParameters = null;
		try
		{
			searchInfo = new SearchResultInfo();
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebHotDealSearch");
			fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("ZIPCode", objForm.getZipCode());
			fetchProductDetailsParameters.addValue("HotDealName", objForm.getSearchKey());
			fetchProductDetailsParameters.addValue("LowerLimit", 0);
			fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
			fetchProductDetailsParameters.addValue("Radius", null);
			simpleJdbcCall.returningResultSet("dealList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (resultFromProcedure != null)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum)
				{
					List<HotDealInfo> dealList = (List<HotDealInfo>) resultFromProcedure.get("dealList");
					if (null != dealList && !dealList.isEmpty())
					{
						int totalSize = (Integer) resultFromProcedure.get("RowCount");
						searchInfo.setTotalSize(totalSize);
					}
					searchInfo.setDealList(dealList);

				}
				else
				{
					LOG.info("Error Occured in registerUser method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}

		return searchInfo;
	}

	public SearchResultInfo getProductWithDeal(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException
	{
		final String methodName = "getProductWithDeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo searchInfo = null;
		int userType = 0;
		MapSqlParameterSource fetchProductDetailsParameters = null;
		try
		{
			searchInfo = new SearchResultInfo();
			if (loginUser != null)
			{
				userType = loginUser.getUserType();
			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			if (userType == 0)
			{
				simpleJdbcCall.withProcedureName("usp_WebSearchProductWithDeal");
			
				fetchProductDetailsParameters = new MapSqlParameterSource();
				fetchProductDetailsParameters.addValue("ZIPCode", objForm.getZipCode());
				fetchProductDetailsParameters.addValue("ProductName", objForm.getSearchKey());
				fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
				fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
				fetchProductDetailsParameters.addValue("Radius", null);
			}
			else
			{
				simpleJdbcCall.withProcedureName("usp_WebProductSearchRetailerOrSupplier");
				fetchProductDetailsParameters = new MapSqlParameterSource();
				fetchProductDetailsParameters.addValue("UserID", loginUser.getUserID());
				fetchProductDetailsParameters.addValue("SearchParameter", objForm.getSearchKey());
				fetchProductDetailsParameters.addValue("LowerLimit", 0);
				fetchProductDetailsParameters.addValue("ScreenName", "This Location - Product List");
			}
			simpleJdbcCall.returningResultSet("productLists", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (resultFromProcedure != null)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum)
				{
					List<ProductVO> prodList = (List<ProductVO>) resultFromProcedure.get("productLists");
					if (null != prodList && !prodList.isEmpty())
					{
						int totalSize = (Integer) resultFromProcedure.get("RowCount");
						searchInfo.setTotalSize(totalSize);
					}
					searchInfo.setProductList(prodList);

				}
				else
				{
					LOG.info("Error Occured in registerUser method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}

		return searchInfo;
	}

	public ProductVO fetchProductInfoWithDeals(Integer productID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductInfo";

		LOG.info("In DAO...." + methodName);

		LOG.info("retailLocationID...." + productID);

		ProductVO productInfo = null;
		List<ProductVO> productInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductInfo");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("ProductID", productID);
			// fetchProductInfoParameters.addValue("RetailLocationID",
			// retailLocationID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);

			productInfoList = (ArrayList<ProductVO>) resultFromProcedure.get("ProductInfo");
			if (null != productInfoList && !productInfoList.isEmpty())
			{
				productInfo = new ProductVO();
				productInfo = productInfoList.get(0);
			}
		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}
		if (null != productInfo)
		{
			return productInfo;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return productInfo;
	}

	public HotDealInfo fetchDealsInfo(Integer hotDealID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchProductInfo";

		LOG.info("In DAO...." + methodName);

		LOG.info("hotDealID...." + hotDealID);

		HotDealInfo hotDealInfo = null;
		List<HotDealInfo> productInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayHotDealDetailsDetails");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("HotDealID", hotDealID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);
			productInfoList = (ArrayList<HotDealInfo>) resultFromProcedure.get("ProductInfo");
			if (null != productInfoList && !productInfoList.isEmpty())
			{
				hotDealInfo = new HotDealInfo();
				hotDealInfo = productInfoList.get(0);
			}
		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}
		if (null != hotDealInfo)
		{
			return hotDealInfo;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return hotDealInfo;
	}

	public List<ProductVO> fetchProductAssociatedWithDeals(int productID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllProduct";

		List<ProductVO> prodAssociatedHotDealList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayProductAssociatedHotDeals");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("ProductID", productID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);

			prodAssociatedHotDealList = (ArrayList<ProductVO>) resultFromProcedure.get("ProductInfo");

		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return prodAssociatedHotDealList;

	}

	public List<ProductVO> fetchDealWithProductAssociated(Integer hotDealID) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchDealWithProductAssociated";

		List<ProductVO> prodAssociatedHotDealList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebDisplayHotDealProducts");
			simpleJdbcCall.returningResultSet("ProductInfo", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));

			final MapSqlParameterSource fetchProductInfoParameters = new MapSqlParameterSource();
			fetchProductInfoParameters.addValue("ProductHotDealID", hotDealID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductInfoParameters);

			prodAssociatedHotDealList = (ArrayList<ProductVO>) resultFromProcedure.get("ProductInfo");

		}
		catch (DataAccessException e)
		{

			throw new ScanSeeWebSqlException(e);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return prodAssociatedHotDealList;

	}
}

package shopper.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.query.ShoppingListQueries;

import com.scansee.externalapi.common.pojos.ExternalAPISearchParameters;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.Retailer;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.FindNearByDetail;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.FindNearByIntactResponse;
import common.pojo.shopper.FindNearByPartialResponse;
import common.pojo.shopper.FindNearByResponse;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.util.Utility;

/**
 * This class is a DAO implementation class for CommonDAO.
 * @author manjunath_gh
 *
 */
public class CommonDAOImpl implements CommonDAO
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(CommonDAOImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This method Fetching App Configuration details.
	 * 
	 * @return ArrayList<AppConfiguration> .
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException
	{

		final String methodName = "getStockInfo";

		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_GetScreenContent Store Procedure with error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e);
		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}

	/**
	 * The method for fetching Lowest price for the product and total retailers
	 * where product is available.
	 * 
	 * @param userID
	 *            The userId in the user request.
	 * @param latitude
	 *            The latitude value.
	 * @param longitude
	 *            The longitude value.
	 * @param productId
	 *            The product ID for which lowest price information is to be
	 * @param postalcode
	 *            The postal code for which lowest price information is to be
	 *            displayed.
	 * @param radius
	 *            The radius specified by the user.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 * @return findNearByLowestPrice The xml with NearBy-Lowest Price Info.
	 */

	public FindNearByIntactResponse fetchNearByLowestPrice(Long userID, Double latitude, int productId, Double longitude, String postalcode,
			double radius) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchNearByLowestPrice in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		int findNearByCount;
		String findNearByLowest;

		final FindNearByIntactResponse findNearByDetailsResultSet = new FindNearByIntactResponse();
		FindNearByPartialResponse findNearByPartialResponse = null;
		ArrayList<FindNearByResponse> findNearByDetailList = null;
		Integer dbresult = 0;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_FindNearByRetailer");
			simpleJdbcCall.returningResultSet("FindNearByInfo", new BeanPropertyRowMapper<FindNearByResponse>(FindNearByResponse.class));

			final SqlParameterSource fetchNearByParameters = new MapSqlParameterSource().addValue("UserID", userID).addValue("Latitude", latitude)
					.addValue("Longitude", longitude).addValue("ProductId", productId).addValue("PostalCode", postalcode).addValue("Radius", radius);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchNearByParameters);

			if (null != resultFromProcedure)
			{

				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{

					dbresult = (Integer) resultFromProcedure.get("Result");
					if (dbresult != null)
					{
						if (dbresult == -1)
						{

							findNearByDetailsResultSet.setFindNearBy(true);
							return findNearByDetailsResultSet;
						}
					}

					findNearByDetailList = (ArrayList<FindNearByResponse>) resultFromProcedure.get("FindNearByInfo");
					if (null != findNearByDetailList && !findNearByDetailList.isEmpty())
					{
						findNearByCount = findNearByDetailList.size();
						findNearByLowest = findNearByDetailList.get(0).getProductPrice();
						findNearByPartialResponse = new FindNearByPartialResponse();
						findNearByPartialResponse.setLowestPrice(findNearByLowest);
						findNearByPartialResponse.setTotalLocations(String.valueOf(findNearByCount) + ApplicationConstants.STORES);
						findNearByPartialResponse.setImagePath(findNearByDetailList.get(0).getImagePath());

						findNearByDetailsResultSet.setFindNearByDetails(findNearByDetailList);
						findNearByDetailsResultSet.setFindNearByMetaData(findNearByPartialResponse);

					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					log.error("Error occurred in usp_FindNearByRetailer Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}

		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return findNearByDetailsResultSet;
	}

	/**
	 * methods to get product info for external API.
	 * 
	 * @param productId
	 *            requested productId.
	 * @return ProductDetail object
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	public ProductDetail getProductInfoforExternalAPI(Integer productId) throws ScanSeeWebSqlException
	{

		final String methodName = "getProductInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetail = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			productDetail = simpleJdbcTemplate.queryForObject(ShoppingListQueries.PRODUCTINOQUERY, new BeanPropertyRowMapper<ProductDetail>(
					ProductDetail.class), productId);
			if (productDetail != null)
			{
				return productDetail;
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			return null;

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetail;
	}

	/**
	 * This method is used to insert apiurls into FindNearByLog table.
	 * 
	 * @param apiURL
	 *            -As String parameter
	 * @param userId
	 *            -As int parameter
	 * @return response as failure or success
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	public String insertExternalAPIURL(String apiURL, int userId) throws ScanSeeWebSqlException
	{
		final String methodName = "insertBatchStatus";
		log.info(ApplicationConstants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			result = this.jdbcTemplate.update(ShoppingListQueries.INSERTEXTERNALAPICALURL, apiURL, Utility.getFormattedCurrentDate(), userId);
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
			}
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return response;
	}

	/**
	 * methods to get the external API Input parameter List.
	 * 
	 * @param apiUsageID
	 *            requested APIUsageID.
	 * @param moduleName
	 *            requested moduleName.
	 * @return externalAPIInputParameters
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	@SuppressWarnings("unchecked")
	public ArrayList<ExternalAPISearchParameters> getExternalAPIInputParameters(Integer apiUsageID, String moduleName) throws ScanSeeWebSqlException
	{
		final String methodName = "getExternalAPIList";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ExternalAPISearchParameters> externalAPIInputParameters = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIInputParameters");
			simpleJdbcCall.returningResultSet("externalAPIInputParameters", new BeanPropertyRowMapper<ExternalAPISearchParameters>(
					ExternalAPISearchParameters.class));

			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("prAPIUsageID", apiUsageID);
			externalAPIListParameters.addValue("PrAPISubModuleName", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIInputParameters = (ArrayList<ExternalAPISearchParameters>) resultFromProcedure.get("externalAPIInputParameters");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				log.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message -->: {}");
				throw new ScanSeeWebSqlException(errorMsg);
			}
			else if (null != externalAPIInputParameters && !externalAPIInputParameters.isEmpty())
			{
				return externalAPIInputParameters;
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIInputParameters;

	}

	/**
	 * methods to get the external API List.
	 * 
	 * @param moduleName
	 *            requested moduleName.
	 * @return externalAPIList
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	@SuppressWarnings("unchecked")
	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws ScanSeeWebSqlException
	{

		final String methodName = "getExternalAPIList";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ExternalAPIVendor> externalAPIList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			// TODO:Change this for constant
			if ("FindOnlineStores".equals(moduleName) || "Find".equals(moduleName))
			{
				simpleJdbcCall.withProcedureName("usp_GetAPIListOnline");
			}
			else
			{
				simpleJdbcCall.withProcedureName("usp_GetAPIList");
			}

			simpleJdbcCall.returningResultSet("externalAPIListInfo", new BeanPropertyRowMapper<ExternalAPIVendor>(ExternalAPIVendor.class));

			final SqlParameterSource externalAPIListParameters = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIList = (ArrayList<ExternalAPIVendor>) resultFromProcedure.get("externalAPIListInfo");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				log.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message:{}");
				throw new ScanSeeWebSqlException(errorMsg);
			}
			else if (null != externalAPIList && !externalAPIList.isEmpty())
			{
				return externalAPIList;
			}

		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIList;
	}

	/**
	 * method for fetching FindNearBy Info.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param latitude
	 *            The latitude in request.
	 * @param productId
	 *            The product Id in request.
	 * @param longitude
	 *            The longitude in request.
	 * @param postalCode
	 *            The postal code for which lowest price information is to be
	 * @param radius
	 *            The radius in request
	 * @return List of type FindNearByDetail objects. The method is called from
	 *         the service layer.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 */

	@SuppressWarnings("unchecked")
	public FindNearByDetails fetchNearByInfo(AreaInfoVO objAreaInfoVO, Integer productId) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchNearByInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		// Temperory fix, after integrating we will remove this code
		Integer userID = 2;
		Integer radius = objAreaInfoVO.getRadius();
		Double latitude = objAreaInfoVO.getLattitude();
		Double longitude = objAreaInfoVO.getLongitude();
		String postalcode = objAreaInfoVO.getZipCode();
		FindNearByDetails findNearByDetails = null;
		List<FindNearByDetail> findNearByDetaillst = null;
		Integer dbresult = 0;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_FindNearByRetailer");
			simpleJdbcCall.returningResultSet("SLfetchNearByInfo", new BeanPropertyRowMapper<FindNearByDetail>(FindNearByDetail.class));

			final SqlParameterSource fetchNearByParameters = new MapSqlParameterSource().addValue("UserID", userID).addValue("ProductId", productId)
					.addValue("Latitude", latitude).addValue("Longitude", longitude).addValue("PostalCode", postalcode).addValue("Radius", radius);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchNearByParameters);

			if (null != resultFromProcedure)
			{
				dbresult = (Integer) resultFromProcedure.get("Result");
				if (dbresult != null)
				{
					if (dbresult == -1)
					{
						findNearByDetails = new FindNearByDetails();
						findNearByDetails.setFindNearBy(true);
						return findNearByDetails;
					}
				}
				findNearByDetaillst = (List<FindNearByDetail>) resultFromProcedure.get("SLfetchNearByInfo");
				if (findNearByDetaillst != null && !findNearByDetaillst.isEmpty())
				{
					findNearByDetails = new FindNearByDetails();
					findNearByDetails.setFindNearByDetail(findNearByDetaillst);
				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return findNearByDetails;
	}

	/**
	 * method to get the particular retailer details in the nearby section
	 * 
	 * @param retailerId
	 *            retailer Id
	 * @return RetailerDetail RetailerDetail instance which stores retailer
	 *         details
	 */
	public Retailer findNearByRetailerDetail(Integer retailerId) throws ScanSeeWebSqlException
	{
		final String methodName = "findNearByRetailerDetail";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Retailer retailer = new Retailer();
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			retailer = simpleJdbcTemplate.queryForObject(ShoppingListQueries.RETAILERDETAIL, new BeanPropertyRowMapper<Retailer>(Retailer.class),
					retailerId);
			if (retailer != null)
			{
				return retailer;
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			return null;

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return retailer;
	}

	/**
	 * methods to get the products media info based on the media type from
	 * database.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return ProductDetails with media details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	public ProductDetails getProductMediaDetails(int productID, String mediaType) throws ScanSeeWebSqlException
	{

		final String methodName = "getProductMediaDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productMediaDetaillst = null;
		ProductDetails productdetailsObj = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_ProductDetailsMediaDispaly");
			simpleJdbcCall.returningResultSet("productMediaInfo", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource mediaParameters = new MapSqlParameterSource();
			if ("audio".equalsIgnoreCase(mediaType))
			{
				mediaType = "Audio Files";
			}
			else if ("video".equalsIgnoreCase(mediaType))
			{
				mediaType = "Video Files";
			}
			else
			{
				mediaType = "Other Files";
			}
			mediaParameters.addValue("ProductID", productID);
			mediaParameters.addValue("MediaType", mediaType);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(mediaParameters);

			if (null != resultFromProcedure)
			{
				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_HotDealSearch Store Procedure error number: {}and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
				else
				{
					productMediaDetaillst = (List<ProductDetail>) resultFromProcedure.get("productMediaInfo");
					if (productMediaDetaillst != null && !productMediaDetaillst.isEmpty())
					{
						productdetailsObj = new ProductDetails();
						productdetailsObj.setMediaType(mediaType);
						productdetailsObj.setProductDetail(productMediaDetaillst);
					}
				}

			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productdetailsObj;
	}

	public ArrayList<RetailerDetail> getCommisJunctionData(Long userId, Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchCommissionJunctionData";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<RetailerDetail> onlineRetailerlst = null;
		try
		{
			log.info(" Request received from Userid ", userId);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_OnlineRetailerInformation");
			simpleJdbcCall.returningResultSet("commissionjunction", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue(ApplicationConstants.USERID, userId);
			fetchProductDetailsParameters.addValue("ProductID", productId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{

				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{

					onlineRetailerlst = (ArrayList<RetailerDetail>) resultFromProcedure.get("commissionjunction");

				}
				else
				{

					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in fetchCommissionJunctionData Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return onlineRetailerlst;
	}

	/**
	 * This method returns the shopper details.
	 * @param userID
	 * @return Users
	 * @throws ScanSeeWebSqlException
	 */
	public Users getLoginUserDetails(Long userID) throws ScanSeeWebSqlException
	{
		final String methodName = "getLoginUserDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Users userInfo = null;
		List<Users> userInfoLst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerUserDetails");

			simpleJdbcCall.returningResultSet("userInfo", new BeanPropertyRowMapper<Users>(Users.class));
			final MapSqlParameterSource loginCredParams = new MapSqlParameterSource();
			loginCredParams.addValue("UserID", userID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(loginCredParams);
			if (null != resultFromProcedure)
			{
				userInfo = new Users();
				userInfoLst = (ArrayList<Users>) resultFromProcedure.get("userInfo");
				if (!userInfoLst.isEmpty() || userInfoLst != null)
				{
					userInfo = userInfoLst.get(0);
				}
				
			}
			log.info("usp_WebConsumerUserDetails executed Successfully.");
		}
		catch (DataAccessException exception)
		{
			log.error("", exception);
			throw new ScanSeeWebSqlException(exception.getMessage());
		}
		
		log.info(ApplicationConstants.METHODEND + methodName);
		return userInfo;
	}
}

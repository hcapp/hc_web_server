package shopper.dao;

import java.util.List;

import common.exception.ScanSeeWebSqlException;
import common.pojo.HotDealInfo;
import common.pojo.ProductVO;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;

public interface ShopperSearchDAO 
{
	public SearchResultInfo fetchAllProduct(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException;

	public ProductVO fetchProductInfo(Integer retailLocationID, Integer productID)throws ScanSeeWebSqlException;

	public SearchResultInfo searchHotDeals(SearchForm objForm, Users loginUser,int lowerLimit) throws ScanSeeWebSqlException;
   
	public SearchResultInfo getProductWithDeal(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException;
   
	public ProductVO fetchProductInfoWithDeals(Integer productID)throws ScanSeeWebSqlException;

	public HotDealInfo fetchDealsInfo(Integer hotDealID)throws ScanSeeWebSqlException;
	
	public List<ProductVO> fetchProductAssociatedWithDeals(int productID) throws ScanSeeWebSqlException;
	
	public List<ProductVO> fetchDealWithProductAssociated(Integer hotDealID)throws ScanSeeWebSqlException;
}

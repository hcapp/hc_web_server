package shopper.dao;

import java.util.ArrayList;

import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CategoryInfo;

/**
 * The Interface for MyGalleryDAO. ImplementedBy {@link MyGalleryDAOImpl}
 * 
 * @author sowjanya_d
 */
public interface MyGalleryDAO
{
	/**
	 * This DAO method is used to fetch my gallery c,l,r information based on
	 * userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             service layer
	 */
	CLRDetails getMyGalleryInfo(CLRDetails clrDetails) throws ScanSeeWebSqlException;

	/**
	 * This DAOImpl method is used to fetch my gallery of All type c,l,r
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             service layer
	 */
	CLRDetails getMyGalleryAllTypeInfo(CLRDetails clrDetails) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to fetch my gallery of type Expired c,l,r
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSeeWebSqlException
	 *             Exception defined for the application will be thrown which is
	 *             caught in the Service layer
	 */
	CLRDetails getMyGalleryExpiredTypeInfo(CLRDetails clrDetails) throws ScanSeeWebSqlException;

	/**
	 * This DAOImpl method is used to fetch my gallery of type Used c,l,r
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the Service
	 *             layer
	 */
	CLRDetails getMyGalleryUsedTypeInfo(CLRDetails clrDetails) throws ScanSeeWebSqlException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods based on user id and coupon id.
	 * 
	 * @param couponDetailsObj
	 *            request parameter.
	 * @return String XML with my gallery details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	// String userRedeemCoupon(CouponDetails couponDetailsObj) throws
	// ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the c,l,r based on user id.
	 * 
	 * @param loyaltyDetailObj
	 *            request parameter.
	 * @return String XMLwith success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	// String userRedeemLoyalty(LoyaltyDetail loyaltyDetailObj) throws
	// ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the c,l,r based on user id.
	 * 
	 * @param rebateDetail
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	// String userRedeemRebate(RebateDetail rebateDetail) throws
	// ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to remove the used coupon.
	 * 
	 * @param couponDetailsObj
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	// String deleteUsedCoupon(CouponDetails couponDetailsObj) throws
	// ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to remove the used loyalty.
	 * 
	 * @param loyaltyDetailObj
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	// String deleteUsedLoyalty(LoyaltyDetail loyaltyDetailObj) throws
	// ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to remove the used rebate.
	 * 
	 * @param rebateDetailObj
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	// String deleteUsedRebate(RebateDetail rebateDetailObj) throws
	// ScanSeeException;

	/**
	 * this method is used to fetch the list of categories.
	 * 
	 * @return ArrayList<CategoryInfo> returns list of categories along with the
	 *         Category Information.
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<CategoryInfo> getCategories() throws ScanSeeWebSqlException;
}

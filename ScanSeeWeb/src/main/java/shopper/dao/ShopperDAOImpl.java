package shopper.dao;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.query.ShoppingListQueries;
import shopper.query.ThisLocationQuery;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.common.pojos.ExternalAPISearchParameters;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.DoubleCheckInfo;
import common.pojo.NonProfitPartner;
import common.pojo.PreferencesInfo;
import common.pojo.ProductVO;
import common.pojo.SchoolInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.University;
import common.pojo.UserPreference;
import common.pojo.UserPreferenceDisplay;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.DiscountUserInfo;
import common.pojo.shopper.DoubleCheck;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.LoyaltyDetail;
import common.pojo.shopper.MainCategoryDetail;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.RebateDetail;
import common.pojo.shopper.RetailerCreatedPages;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.SubCategoryDetail;
import common.pojo.shopper.ThisLocationRequest;
import common.pojo.shopper.ThisLocationRetailerInfo;
import common.util.Utility;

/**
 * This class is a DAO implementation class for ShopperDAO.
 * 
 * @author manjunath_gh
 */
public class ShopperDAOImpl implements ShopperDAO
{
	/**
	 * Getting the logger Instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(ShopperDAOImpl.class);

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;

	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public Users registerFaceBookUser(String userId) throws ScanSeeWebSqlException
	{
		final String methodName = "registerFaceBookUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String response = null;
		String faceBook = null;
		Users userInfo = null;
		List<Users> userInfoLst = null;
		try
		{
			faceBook = "1";
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUserFaceBookLogIn");
			simpleJdbcCall.returningResultSet("userInfo", new BeanPropertyRowMapper<Users>(Users.class));
			final MapSqlParameterSource userRegistrationParameter = new MapSqlParameterSource();
			userRegistrationParameter.addValue(ApplicationConstants.USERNAME, userId);
			userRegistrationParameter.addValue(ApplicationConstants.FACEBOOK, faceBook);
			userRegistrationParameter.addValue("DateCreated", Utility.getFormattedDate());
			userRegistrationParameter.addValue("DeviceID", null);
			userRegistrationParameter.addValue("UserLongitude", null);
			userRegistrationParameter.addValue("UserLatitude", null);
			resultFromProcedure = simpleJdbcCall.execute(userRegistrationParameter);
			/**
			 * For getting response from stored procedure ,sp return Result as
			 * response success as 0 failure as 1 it will return list resultset
			 * so casting into map and getting Result param value
			 */
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				// response = ((Integer)
				// resultFromProcedure.get("FirstUserID")).toString();

				userInfo = new Users();
				userInfoLst = (ArrayList<Users>) resultFromProcedure.get("userInfo");
				if (!userInfoLst.isEmpty() || userInfoLst != null)
				{
					userInfo = userInfoLst.get(0);
					userInfo.setLoginSuccess(true);
					int userType = (Integer) resultFromProcedure.get("UserType");
					String pageForwardValue = (String) resultFromProcedure.get("LandingPage");
					userInfo.setUserType(userType);
					userInfo.setPageForwardValue(pageForwardValue);

				}

			}

		}
		catch (DataAccessException exception)
		{
			final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
			final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
			log.info("Error Occured in registerUser method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
			throw new ScanSeeWebSqlException(errorMsg);
		}
		catch (ParseException exception)
		{
			log.error("Exception occurred in registerUser", exception);
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return userInfo;
	}

	/**
	 * This method insert the Shopper registration details.
	 * 
	 * @param objUsers
	 *            instance of Users.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 */
	public Users addShopperRegistration(Users objUsers) throws ScanSeeWebSqlException
	{
		final String methodName = "addShopperRegistration";
		log.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		boolean isDuplicateUser;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebShopperCreation");
			final MapSqlParameterSource objAddRegParameter = new MapSqlParameterSource();

			objAddRegParameter.addValue("FirstName", objUsers.getFirstName());
			objAddRegParameter.addValue("LastName", objUsers.getLastName());
			objAddRegParameter.addValue("Address1", objUsers.getAddress1());
			objAddRegParameter.addValue("Address2", objUsers.getAddress2());
			objAddRegParameter.addValue("State", objUsers.getState());
			objAddRegParameter.addValue("City", objUsers.getCity());
			objAddRegParameter.addValue("PostalCode", objUsers.getPostalCode());
			objAddRegParameter.addValue("PhoneNumber", Utility.removePhoneFormate(objUsers.getMobilePhone()));
			objAddRegParameter.addValue("ContactEmail", objUsers.getEmail());
			objAddRegParameter.addValue("Password", objUsers.getPassword());
			objAddRegParameter.addValue("UserName", objUsers.getUserName());

			resultFromProcedure = simpleJdbcCall.execute(objAddRegParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				isDuplicateUser = (Boolean) resultFromProcedure.get("DuplicateFlag");

				if (isDuplicateUser)
				{
					response = ApplicationConstants.DUPLICATE_USER;
					objUsers.setIsProfileCreated(response);
					log.info("Inside ShopperDAOImpl : addShopperRegistration : Return response message : " + response);
					// throw new ScanSeeWebSqlException(response);
				}
				else
				{
					Integer userId = (Integer) resultFromProcedure.get("UserID");
					objUsers.setUserID(Long.valueOf(userId));
					response = ApplicationConstants.SUCCESS;
					objUsers.setIsProfileCreated(response);
					log.info("Inside ShopperDAOImpl : addShopperRegistration : Return response message : " + response);
				}

				log.info("Inside ShopperDAOImpl : addShopperRegistration : responseFromProc is : " + responseFromProc);
			}
			else
			{
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Inside ShopperDAOImpl : addShopperRegistration   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Inside ShopperDAOImpl : addShopperRegistration : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}

		log.info("User Status:::::" + objUsers.getIsProfileCreated());
		log.info("User ID:::::" + objUsers.getUserID());

		log.info(ApplicationConstants.METHODEND + methodName);
		return objUsers;
	}

	public String addPreferencesInfo(PreferencesInfo preferencesInfo) throws ScanSeeWebSqlException
	{
		try
		{

		}
		catch (Exception e)
		{
			throw new ScanSeeWebSqlException(e.getMessage(), e.getCause());
		}
		return "";
	}

	/**
	 * This method select School details.
	 * 
	 * @param objUserPreference
	 *            instance of UserPreference.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	// usp_WebUserSchoolSelection
	public String addSchoolSelection(SchoolInfo schoolInfo) throws Exception
	{
		log.info("Inside ShopperDAOImpl : addSchoolSelection ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUserSchoolSelection ");
			final MapSqlParameterSource objAddUsrPreferParameter = new MapSqlParameterSource();

			/*
			 * objAddUsrPreferParameter.addValue("UserID", schoolInfo.get);
			 * objAddUsrPreferParameter.addValue("CollegeName",
			 * schoolInfo.getCollegeName());
			 * objAddUsrPreferParameter.addValue("UniversityIDs",
			 * schoolInfo.getUniversityID());
			 * objAddUsrPreferParameter.addValue("NonProfitPartnerIDs",
			 * schoolInfo.getNonProfitPartnerIDs());
			 */

			resultFromProcedure = simpleJdbcCall.execute(objAddUsrPreferParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{

				response = ApplicationConstants.SUCCESS;
				log.info("Inside ShopperDAOImpl: addSchoolSelection : responseFromProc is : " + responseFromProc);
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in addSchoolSelection method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
			}
		}
		catch (Exception exception)
		{
			log.info("Exception in addSchoolSelection : " + exception);
		}
		return response;
	}

	/**
	 * This method Add UserPreference details.
	 * 
	 * @param objUsrPref
	 *            instance of UserPreference
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	public String addUserPreferences(CategoryDetail categoryDetail, Long userId) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : addUserPreferences ");

		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		int email;
		int cellPhone;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUserPreferences");
			final MapSqlParameterSource objAddUsrPreferParameter = new MapSqlParameterSource();

			if (categoryDetail.isEmail())
			{
				email = 1;
			}
			else
			{
				email = 0;
			}

			if (categoryDetail.isCellPhone())
			{
				cellPhone = 1;
			}
			else
			{
				cellPhone = 0;
			}

			objAddUsrPreferParameter.addValue("UserID", userId);
			objAddUsrPreferParameter.addValue("EmailAlert", Integer.valueOf(email));
			objAddUsrPreferParameter.addValue("CellPhoneAlert", Integer.valueOf(cellPhone));
			resultFromProcedure = simpleJdbcCall.execute(objAddUsrPreferParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{

				response = ApplicationConstants.SUCCESS;
				log.info("Inside RetailerDAOImpl : addUserPreferences : responseFromProc is : " + responseFromProc);
			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in addUserPreferences method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
			}

		}
		catch (Exception exception)
		{
			log.info("Exception in addUserPreferences : " + exception);
		}
		return response;
	}

	public String addDoubleCheckInfo(DoubleCheckInfo doubleCheckInfo) throws ScanSeeWebSqlException
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This method will return all University .
	 * 
	 * @return List of UniversityName and University ID .
	 * @throws Exception
	 */
	public List<University> getAllUniversity() throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;
		List<University> arUniversityList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveUniversity");
			simpleJdbcCall.returningResultSet("getUniversityList", new BeanPropertyRowMapper<University>(University.class));
			final MapSqlParameterSource objUniversityParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objUniversityParameter);
			arUniversityList = (ArrayList) resultFromProcedure.get("getUniversityList");
			log.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" + arUniversityList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllUniversity" + exception);
		}
		return arUniversityList;
	}

	/**
	 * This method will return all Non Profit Partner.
	 * 
	 * @return List of NonProfitPartnerID , NonProfitPartnerName .
	 * @throws Exception
	 */
	public List<NonProfitPartner> getAllNonProfitPartner() throws ScanSeeWebSqlException
	{
		final String methodName = "getAllNonProfitPartner";
		log.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		List<NonProfitPartner> arNonProfitPartnerList = null;
		MapSqlParameterSource objNonProfitPartner = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveNonProfitpartner");
			simpleJdbcCall.returningResultSet("getNonProfitpartnerList", new BeanPropertyRowMapper<NonProfitPartner>(NonProfitPartner.class));
			objNonProfitPartner = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objNonProfitPartner);
			arNonProfitPartnerList = (ArrayList) resultFromProcedure.get("getNonProfitpartnerList");
			log.info("Inside ShopperDAOImpl : getAllNonProfitPartner  Size \t:" + arNonProfitPartnerList.size());
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getAllNonProfitPartner" + exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return arNonProfitPartnerList;
	}

	/**
	 * This method will return all Category .
	 * 
	 * @return List of Category ID , Category Name .
	 * @throws Exception
	 */
	public List<Category> getAllCategory() throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllNonProfitPartner");
		Map<String, Object> resultFromProcedure = null;
		List<Category> arCategoryList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveCategory");
			simpleJdbcCall.returningResultSet("getCategoryList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource objCategoryParameter = new MapSqlParameterSource();
			resultFromProcedure = simpleJdbcCall.execute(objCategoryParameter);
			arCategoryList = (ArrayList) resultFromProcedure.get("getCategoryList");
			log.info("Inside ShopperDAOImpl :getAllNonProfitPartner  Size \t:" + arCategoryList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllNonProfitPartner" + exception);
		}
		return arCategoryList;
	}

	/**
	 * This method Add Hot Deal details.
	 * 
	 * @param objProdtHotDeal
	 *            instance of ProductHotDeal.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */

	/*
	 * public String addUserPreferences(UserPreference objUsrPref) throws
	 * ScanSeeWebSqlException {
	 * log.info("Inside ShopperDAOImpl : addUserPreferences :"); String response
	 * = null; Map<String, Object> resultFromProcedure = null; Integer
	 * responseFromProc = null; try { simpleJdbcCall = new
	 * SimpleJdbcCall(jdbcTemplate);
	 * simpleJdbcCall.withProcedureName("usp_WebUserPreferences"); final
	 * MapSqlParameterSource objAddUsrPreferParameter = new
	 * MapSqlParameterSource(); objAddUsrPreferParameter.addValue("UserID",
	 * objUsrPref.getUserID()); objAddUsrPreferParameter.addValue("EmailAlert",
	 * objUsrPref.getEmailAlert());
	 * objAddUsrPreferParameter.addValue("CellPhoneAlert",
	 * objUsrPref.getCellPhoneAlert());
	 * objAddUsrPreferParameter.addValue("Category",
	 * objUsrPref.getCategoryID()); resultFromProcedure =
	 * simpleJdbcCall.execute(objAddUsrPreferParameter); responseFromProc =
	 * (Integer) resultFromProcedure.get(ApplicationConstants.STATUS); if (null
	 * != responseFromProc && responseFromProc.intValue() == 0) { response =
	 * ApplicationConstants.SUCCESS; log.info(
	 * "Inside ShopperDAOImpl : addUserPreferences : responseFromProc is : " +
	 * responseFromProc); } else { final Integer errorNum = (Integer)
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER); final String
	 * errorMsg = (String)
	 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
	 * log.info("Inside ShopperDAOImpl : addUserPreferences method ..errorNum..."
	 * + errorNum + "errorMsg  .." + errorMsg); } } catch (DataAccessException
	 * exception) { log.error("Exception in addUserPreferences : " + exception);
	 * } return response; }
	 */

	/**
	 * This method select School details.
	 * 
	 * @param objUserPreference
	 *            instance of UserPreference.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	public String addSchoolSelection(SchoolInfo schoolInfo, ArrayList<Long> selectedUniversityIdsList, ArrayList<Long> selectedNonProfitIdsList)
			throws ScanSeeWebSqlException
	{
		// UserPreference objUsrPref

		log.info("Inside ShopperDAOImpl : addSchoolSelection ");
		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Iterator<Long> iteratorList;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUserSchoolSelection");
			final MapSqlParameterSource objAddUsrPreferParameter = new MapSqlParameterSource();

			objAddUsrPreferParameter.addValue("UserID", schoolInfo.getUserId());
			objAddUsrPreferParameter.addValue("CollegeName", schoolInfo.getCollege());

			iteratorList = selectedUniversityIdsList.iterator();
			StringBuffer bufferUniversity = new StringBuffer();

			while (iteratorList.hasNext())
			{
				Long selectedPreference = iteratorList.next();
				bufferUniversity.append(selectedPreference);
				bufferUniversity.append(",");

				log.info("Selected preferences :::::::" + bufferUniversity);
			}
			objAddUsrPreferParameter.addValue("UniversityIDs", bufferUniversity.toString());

			iteratorList = selectedNonProfitIdsList.iterator();
			StringBuffer bufferNonProfit = new StringBuffer();

			while (iteratorList.hasNext())
			{
				Long selectedPreference = iteratorList.next();
				bufferNonProfit.append(selectedPreference);
				bufferNonProfit.append(",");
			}

			objAddUsrPreferParameter.addValue("NonProfitPartnerIDs", bufferNonProfit.toString());

			resultFromProcedure = simpleJdbcCall.execute(objAddUsrPreferParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
				log.info("Inside ShopperDAOImpl : addSchoolSelection : responseFromProc is : " + responseFromProc);
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in addSchoolSelection  : ErrorNum : " + errorNum + "ErrorMsg  : " + errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception in addSchoolSelection : " + exception);
		}
		return response;
	}

	/**
	 * This method will display Shopper User's Preferences details.
	 * 
	 * @param userId
	 * @return User's Preferences, Caterory Name, University Name,
	 *         NonProfitPartnerName details.
	 * @throws Exception
	 */
	public UserPreferenceDisplay displayPreferencesDetails(Long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "displayPreferencesDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		List<UserPreference> arShopPrefDetails = null;
		List<CategoryDetail> categoryDetailList = null;
		List<SchoolInfo> arUniversityList = null;
		List<NonProfitPartner> arNonProfitPartnerList = null;
		UserPreferenceDisplay objUserPreDis = null;
		MapSqlParameterSource objusrPrefParameters = null;
		List<SubCategoryDetail> subList = null;
		final List<MainCategoryDetail> mainList = new ArrayList<MainCategoryDetail>();
		MainCategoryDetail mainCategoryDetail = null;
		int preMainCategorId = 0;
		int currentCatId;
		CategoryDetail categoryDetail = null;
		CategoryDetail categoryDetailResp = null;
		Integer email = 0;
		Integer cellPhone = 0;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebUserDisplayPreferences");
			simpleJdbcCall.returningResultSet("arUserPreferenceList", new BeanPropertyRowMapper<UserPreference>(UserPreference.class))
					.returningResultSet("arCategoriesList", new BeanPropertyRowMapper<CategoryDetail>(CategoryDetail.class))
					.returningResultSet("arUniversityList", new BeanPropertyRowMapper<SchoolInfo>(SchoolInfo.class))
					.returningResultSet("arNonPartnersList", new BeanPropertyRowMapper<NonProfitPartner>(NonProfitPartner.class));
			objusrPrefParameters = new MapSqlParameterSource();

			objusrPrefParameters.addValue("UserID", userId);
			resultFromProcedure = simpleJdbcCall.execute(objusrPrefParameters);

			arShopPrefDetails = (ArrayList) resultFromProcedure.get("arUserPreferenceList");
			categoryDetailList = (ArrayList) resultFromProcedure.get("arCategoriesList");
			arUniversityList = (ArrayList) resultFromProcedure.get("arUniversityList");
			arNonProfitPartnerList = (ArrayList) resultFromProcedure.get("arNonPartnersList");

			objUserPreDis = new UserPreferenceDisplay();
			if (null != arShopPrefDetails && !arShopPrefDetails.isEmpty())
			{
				objUserPreDis.setUserPreferenceList(arShopPrefDetails);
				for (UserPreference preference : arShopPrefDetails)
				{
					email = preference.getEmailAlert();
					cellPhone = preference.getCellPhoneAlert();
				}
			}
			/*
			 * if (null != categoryDetailList && !categoryDetailList.isEmpty() )
			 * { objUserPreDis.setCategoryList(arCategoriesList); }
			 */
			if (null != categoryDetailList && !categoryDetailList.isEmpty())
			{
				for (Iterator<CategoryDetail> iterator = categoryDetailList.iterator(); iterator.hasNext();)
				{
					categoryDetail = iterator.next();
					currentCatId = categoryDetail.getParentCategoryID();
					if (preMainCategorId == 0)
					{
						preMainCategorId = categoryDetail.getParentCategoryID();
						mainCategoryDetail = new MainCategoryDetail();
						mainCategoryDetail.setMainCategoryId(preMainCategorId);
						mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
						subList = new ArrayList<SubCategoryDetail>();
					}
					if (currentCatId != preMainCategorId)
					{
						mainCategoryDetail.setSubCategoryDetailLst(subList);
						mainList.add(mainCategoryDetail);
						subList = new ArrayList<SubCategoryDetail>();
						final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
						subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
						subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
						subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
						// subCategoryDetail.setRowNum(categoryDetail.getRowNum());
						subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
						subCategoryDetail.setDisplayed(categoryDetail.getDisplayed());
						subList.add(subCategoryDetail);
						mainCategoryDetail = new MainCategoryDetail();
						mainCategoryDetail.setMainCategoryId(currentCatId);
						mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());

						preMainCategorId = categoryDetail.getParentCategoryID();
					}
					else
					{
						final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
						subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
						subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
						subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
						subCategoryDetail.setDisplayed(categoryDetail.getDisplayed());
						// subCategoryDetail.setRowNum(categoryDetail.getRowNum());
						subList.add(subCategoryDetail);
					}
				}

				if (null != mainList && !mainList.isEmpty())
				{
					categoryDetailResp = new CategoryDetail();
					categoryDetailResp.setMainCategoryDetaillst(mainList);
					Boolean cellPhoneAlert;
					if (cellPhone == 0)
					{
						cellPhoneAlert = false;
					}
					else
					{
						cellPhoneAlert = true;
					}

					Boolean emailAlert;
					if (email == 0)
					{
						emailAlert = false;
					}
					else
					{
						emailAlert = true;
					}
					if (null != cellPhoneAlert)
					{
						if (cellPhoneAlert)
						{
							categoryDetailResp.setCellPhone(true);
						}
					}
					if (null != emailAlert)
					{
						if (emailAlert)
						{
							categoryDetailResp.setEmail(true);
						}
					}
				}
				if (null != categoryDetailResp)
				{
					objUserPreDis.setCategoryDetailresponse(categoryDetailResp);
				}

			}

			if (null != arUniversityList && !arUniversityList.isEmpty())
			{
				objUserPreDis.setSchollList(arUniversityList);
			}
			if (null != arNonProfitPartnerList && !arNonProfitPartnerList.isEmpty())
			{
				objUserPreDis.setNonProfitPartnersList(arNonProfitPartnerList);
			}
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in displayPreferencesDetails : " + exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return objUserPreDis;
	}

	/**
	 * The method fetches the Retailer Details from the database for the given
	 * search parameters(Zipcode or Latitude and longitude, radius and user id).
	 * 
	 * @param objAreaInfoVO
	 *            Instance of AreaInfoVO.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return RetailersDetails returns RetailerDetails object container List of
	 *         retailers. Controller layer.
	 */
	public RetailersDetails fetchRetailerDetails(AreaInfoVO objAreaInfoVO, String screenName) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchRetailerDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> retailerList = new ArrayList<RetailerDetail>();
		RetailersDetails details = null;
		Integer rowcount = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_FetchRetailerListPagination");
			simpleJdbcCall.returningResultSet("retailers", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource fetchRetailerDetailsParameters = new MapSqlParameterSource();

			fetchRetailerDetailsParameters.addValue(ApplicationConstants.USERID, objAreaInfoVO.getUserID());
			fetchRetailerDetailsParameters.addValue("Radius", objAreaInfoVO.getRadius());
			fetchRetailerDetailsParameters.addValue("Longitude", objAreaInfoVO.getLongitude());
			fetchRetailerDetailsParameters.addValue("Latitude", objAreaInfoVO.getLattitude());
			fetchRetailerDetailsParameters.addValue("ZipCode", objAreaInfoVO.getZipCode());
			fetchRetailerDetailsParameters.addValue("LowerLimit", objAreaInfoVO.getLowerLimit());

			fetchRetailerDetailsParameters.addValue("ScreenName", screenName);
			// below param are output params from SP.
			fetchRetailerDetailsParameters.addValue("ErrorNumber", null);
			fetchRetailerDetailsParameters.addValue("ErrorMessage", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRetailerDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					retailerList = (List<RetailerDetail>) resultFromProcedure.get("retailers");
					if (null != retailerList && !retailerList.isEmpty())
					{
						details = new RetailersDetails();
						if (retailerList != null && !retailerList.isEmpty())
						{
							rowcount = (Integer) resultFromProcedure.get("MaxCnt");
							details.setRetailerDetail(retailerList);
							details.setTotalSize(rowcount);
						}
						else
						{
							final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
							final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
							log.info("Inside RetailerDAOImpl : getRetailerByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
							rowcount = (Integer) resultFromProcedure.get("MaxCnt");
							details.setTotalSize(rowcount);
						}
					}
					else
					{
						log.info("No Retailers found ");
						return details;
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchRetailerList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				}

			}
		}
		catch (DataAccessException e)
		{
			log.error("Error occured in fetchRetailerDetails method", e);

		}
		log.info("");
		return details;
	}

	/**
	 * This method for getting the All state List based .
	 * 
	 * @return ArrayList<SchoolInfo>
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<SchoolInfo> getAllStates() throws ScanSeeWebSqlException
	{
		final String methodName = "getAllStates";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<SchoolInfo> countryList = null;
		try
		{
			countryList = this.jdbcTemplate.query("SELECT StateName,Stateabbrevation FROM [State] order by StateName", new RowMapper<SchoolInfo>() {
				public SchoolInfo mapRow(ResultSet rs, int rowNum) throws SQLException
				{
					final SchoolInfo state = new SchoolInfo();
					state.setState(rs.getString("StateName"));
					state.setStateabbr(rs.getString("Stateabbrevation"));

					return state;
				}

			});
		}
		catch (EmptyResultDataAccessException exception)
		{

			throw new ScanSeeWebSqlException(exception);

		}
		catch (DataAccessException e)
		{
			throw new ScanSeeWebSqlException(e);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return (ArrayList<SchoolInfo>) countryList;
	}

	/**
	 * This method for getting the All College List based on the state code.
	 * 
	 * @param stateCode
	 * @return ArrayList<SchoolInfo>
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<SchoolInfo> getCollegeList(String stateCode) throws ScanSeeWebSqlException
	{

		final String methodName = "getCollegeList";
		log.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		ArrayList<SchoolInfo> arUniversityList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveUniversity");
			simpleJdbcCall.returningResultSet("getUniversityList", new BeanPropertyRowMapper<SchoolInfo>(SchoolInfo.class));
			final MapSqlParameterSource objUniversityParameter = new MapSqlParameterSource();

			objUniversityParameter.addValue("State", stateCode);
			resultFromProcedure = simpleJdbcCall.execute(objUniversityParameter);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					arUniversityList = (ArrayList<SchoolInfo>) resultFromProcedure.get("getUniversityList");
					log.info("Inside ShopperDAOImpl : getCollegeList  Size \t:" + arUniversityList.size());
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
					final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
					log.error("Error occurred in getCollegeList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllUniversity" + exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return arUniversityList;

	}

	/**
	 * The method fetches the Product Details from the database for the given
	 * search parameters(Retailer LocationID, lower limit and user id).
	 * 
	 * @param productDetailsRequest
	 *            Instance of ProductDetailsRequest.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return ProductDetails returns ProductDetails object container List of
	 *         products available on the retailer.
	 */
	public ProductDetails fetchProductDetails(ProductDetailsRequest productDetailsRequest, String screenName) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchProductDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetails productDetails = null;
		List<ProductDetail> productDetailLst = null;
		Integer rowcount = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_FetchProductListPagination");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			// below parameters are the input parameters for SP
			fetchProductDetailsParameters.addValue("UserID", productDetailsRequest.getUserId());
			fetchProductDetailsParameters.addValue("RetailLocationID", productDetailsRequest.getRetailLocationID());
			fetchProductDetailsParameters.addValue("ScreenName", screenName);
			fetchProductDetailsParameters.addValue("LowerLimit", productDetailsRequest.getLowerLimit());
			// below param are output params from SP.
			fetchProductDetailsParameters.addValue("ErrorNumber", null);
			fetchProductDetailsParameters.addValue("ErrorMessage", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailLst = (List<ProductDetail>) resultFromProcedure.get("productDetails");
					if (null != productDetailLst && !productDetailLst.isEmpty())
					{
						productDetails = new ProductDetails();

						if (productDetailLst != null && !productDetailLst.isEmpty())
						{
							rowcount = (Integer) resultFromProcedure.get("MaxCnt");
							productDetails.setProductDetail(productDetailLst);
							productDetails.setTotalSize(rowcount);
						}
						else
						{
							final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
							final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
							log.info("Inside RetailerDAOImpl : getRetailerByRebateName  : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
							rowcount = (Integer) resultFromProcedure.get("MaxCnt");
							productDetails.setTotalSize(rowcount);
						}
					}
					else
					{
						log.info("No Products found for the search");
						return productDetails;
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchProductList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in scanBarCodeProduct", exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * This method for saving the all the school information.
	 * 
	 * @param schoolInfo
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	public String addSchoolInfo(SchoolInfo schoolInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "addSchoolInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerPreferredUniversity");
			final MapSqlParameterSource objAddUsrPreferParameter = new MapSqlParameterSource();

			objAddUsrPreferParameter.addValue("UserID", schoolInfo.getUserId());
			objAddUsrPreferParameter.addValue("UniversityID", schoolInfo.getUniversityID());
			objAddUsrPreferParameter.addValue("NonProfitPartnerIDs", null);

			resultFromProcedure = simpleJdbcCall.execute(objAddUsrPreferParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				response = ApplicationConstants.SUCCESS;
				log.info("Inside ShopperDAOImpl : addSchoolSelection : responseFromProc is : " + responseFromProc);
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in addSchoolSelection  : ErrorNum : " + errorNum + "ErrorMsg  : " + errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception in addSchoolSelection : " + exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method for saving the all the school information.
	 * 
	 * @param userId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public SchoolInfo displaySchoolInfo(Long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "displaySchoolInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		ArrayList<SchoolInfo> schoolInfoList = null;
		SchoolInfo schoolInfo = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerPreferredUniversityDisplay");
			final MapSqlParameterSource objAddUsrPreferParameter = new MapSqlParameterSource();
			objAddUsrPreferParameter.addValue("UserID", userId);
			simpleJdbcCall.returningResultSet("schoolInfo", new BeanPropertyRowMapper<SchoolInfo>(SchoolInfo.class));
			resultFromProcedure = simpleJdbcCall.execute(objAddUsrPreferParameter);
			schoolInfoList = (ArrayList<SchoolInfo>) resultFromProcedure.get("schoolInfo");
			for (SchoolInfo info : schoolInfoList)
			{
				schoolInfo = new SchoolInfo();
				schoolInfo.setUniversityID(info.getUniversityID());
				schoolInfo.setUniversityName(info.getUniversityName());
				schoolInfo.setState(info.getState());
				schoolInfo.setUserId(info.getUserId());
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception in addSchoolSelection : " + exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return schoolInfo;
	}

	public SearchResultInfo fetchAllProduct(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchAllProduct";
		log.info(ApplicationConstants.METHODSTART + methodName);

		String zipCode = objForm.getZipCode();
		if (zipCode == null)
			zipCode = loginUser.getPostalCode();
		SearchResultInfo searchInfo = null;

		MapSqlParameterSource fetchProductDetailsParameters = null;
		try
		{
			searchInfo = new SearchResultInfo();

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebSearchProductName");
			fetchProductDetailsParameters = new MapSqlParameterSource();
			// fetchProductDetailsParameters.addValue("ZIPCode", 91303);
			fetchProductDetailsParameters.addValue("ZIPCode", zipCode);
			fetchProductDetailsParameters.addValue("ProductName", objForm.getSearchKey());
			fetchProductDetailsParameters.addValue("LowerLimit", lowerLimit);
			fetchProductDetailsParameters.addValue("Radius", objForm.getRadius());

			simpleJdbcCall.returningResultSet("productLists", new BeanPropertyRowMapper<ProductVO>(ProductVO.class));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (resultFromProcedure != null)
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				if (null == errorNum)
				{
					List<ProductVO> prodList = (List<ProductVO>) resultFromProcedure.get("productLists");
					if (null != prodList && !prodList.isEmpty())
					{
						int totalSize = (Integer) resultFromProcedure.get("RowCount");
						searchInfo.setTotalSize(totalSize);
					}
					searchInfo.setProductList(prodList);

				}
				else
				{
					log.info("Error Occured in registerUser method ..errorNum..." + errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception);
		}

		return searchInfo;
	}

	public List<CouponDetail> fetchCouponInfo(DiscountUserInfo discountUserInfo) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchCouponInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<CouponDetail> couponDetailList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListCoupon");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetail>(CouponDetail.class));

			final SqlParameterSource fetchCouponParameters = new MapSqlParameterSource().addValue("ProductID", discountUserInfo.getProductID())
					.addValue("RetailID", discountUserInfo.getRetailID()).addValue("UserID", discountUserInfo.getUserID()).addValue("LowerLimit", 0)
					.addValue("ScreenName", "clr screen");

			/*
			 * .addValue("ProductID", 8884) .addValue("RetailID", 517)
			 * .addValue("UserID", 1) .addValue("LowerLimit", 0)
			 * .addValue("ScreenName", "clr screen");
			 */
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);
			couponDetailList = (List<CouponDetail>) resultFromProcedure.get("CouponDetails");

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return couponDetailList;

	}

	public List<LoyaltyDetail> fetchLoyaltyInfo(DiscountUserInfo discountUserInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchLoyaltyInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<LoyaltyDetail> loyaltyDetailList;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListLoyalty");
			simpleJdbcCall.returningResultSet("SLLoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final SqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource().addValue("ProductID", discountUserInfo.getProductID())
					.addValue("RetailID", discountUserInfo.getRetailID()).addValue("UserID", discountUserInfo.getUserID()).addValue("LowerLimit", 0)
					.addValue("ScreenName", "clr screen");

			/*
			 * .addValue("ProductID", 1) .addValue("RetailID", 239)
			 * .addValue("UserID", 1) .addValue("LowerLimit", 0)
			 * .addValue("ScreenName", "clr screen");
			 */

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("SLLoyaltyDetails");

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return loyaltyDetailList;

	}

	public List<RebateDetail> fetchRebateInfo(DiscountUserInfo discountUserInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchRebateInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<RebateDetail> rebateDetailList;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListRebate");
			simpleJdbcCall.returningResultSet("SLRebateDetails", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final SqlParameterSource fetchRebateParameters = new MapSqlParameterSource()

			.addValue("ProductID", discountUserInfo.getProductID()).addValue("RetailID", discountUserInfo.getRetailID())
					.addValue("UserID", discountUserInfo.getUserID()).addValue("LowerLimit", 0).addValue("ScreenName", "clr screen");

			/*
			 * .addValue("ProductID", 25) .addValue("RetailID", 31)
			 * .addValue("UserID", 1) .addValue("LowerLimit", 0)
			 * .addValue("ScreenName", "clr screen");
			 */

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);

			rebateDetailList = (List<RebateDetail>) resultFromProcedure.get("SLRebateDetails");

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return rebateDetailList;
	}

	public String addCoupon(Long userId, Integer couponId) throws ScanSeeWebSqlException
	{
		final String methodName = "addCoupon in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{

			log.info("Inside Add Coupon for User {}", userId);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddCoupon");
			final SqlParameterSource addCoupon = new MapSqlParameterSource().addValue("CouponID", couponId).addValue("UserID", userId)
					.addValue("UserCouponClaimTypeID", null).addValue("CouponPayoutMethod", null).addValue("RetailLocationID", null)
					.addValue("CouponClaimDate", Utility.getFormattedDate());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addCoupon);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListAddCoupon Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String removeCoupon(Long userId, Integer couponId) throws ScanSeeWebSqlException

	{

		final String methodName = "removeCoupon";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String responseFromProc = null;
		Integer fromProc = null;

		try
		{
			log.info(" Executing the Remove coupon method User ID {}", userId);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListDeleteCoupon");
			final SqlParameterSource removeCoupon = new MapSqlParameterSource().addValue("CouponID", couponId).addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeCoupon);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_MasterShoppingListDeleteCoupon method ..errorNum.{} errorMsg.. {}", errorNum, errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return responseFromProc;

	}

	public String addRebate(Long userId, Integer couponId) throws ScanSeeWebSqlException
	{

		final String methodName = "addRebate";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddRebate");
			final SqlParameterSource addRebate = new MapSqlParameterSource().addValue("UserID", userId)

			.addValue("RebateID", couponId).addValue("RetailLocationID", null).addValue("RedemptionStatusID", null).addValue("PurchaseAmount", null)
					.addValue("PurchaseDate", Utility.getFormattedDate()).addValue("ProductSerialNumber", null).addValue("ScanImage", null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addRebate);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListAddRebate Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			response = ApplicationConstants.FAILURE;
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String removeRebate(Long userId, Integer couponId) throws ScanSeeWebSqlException
	{
		final String methodName = "removeRebate in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListDeleteRebate");
			final SqlParameterSource removeRebate = new MapSqlParameterSource().addValue("RebateID", couponId).addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeRebate);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListDeleteRebate Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Error ocurred in Remove Rebate", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String addLoyalty(Long userId, Integer couponId) throws ScanSeeWebSqlException
	{
		final String methodName = "addLoyalty in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = 0;
		String response;
		try
		{
			log.info("Inside Add Loyalty");
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddLoyalty");
			final SqlParameterSource addLoyalty = new MapSqlParameterSource().addValue("UserID", userId).addValue("LoyaltyDealID", couponId)
					.addValue("LoyaltyDealSource", null).addValue("LoyaltyDealPayoutMethod", null)
					.addValue("LoyaltyDealRedemptionDate", Utility.getFormattedDate());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addLoyalty);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListAddLoyalty Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);

		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	public String removeLoyalty(Long userId, Integer couponId) throws ScanSeeWebSqlException
	{
		final String methodName = "removeLoyalty in DAO layer";

		String responseFromProc = null;
		Integer fromProc = null;

		log.info(ApplicationConstants.METHODSTART + methodName);

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListDeleteLoyalty");
			final SqlParameterSource removeLoyalty = new MapSqlParameterSource().addValue("LoyaltyDealID", couponId).addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeLoyalty);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListDeleteLoyalty Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	public ExternalAPIInformation getExternalApiInfo(String moduleName) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;

		ExternalAPIInformation exteApi = null;

		List<University> arUniversityList = null;

		List<ExternalAPIVendor> vendorList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIListOnline");
			simpleJdbcCall.returningResultSet("vendorList", new BeanPropertyRowMapper<ExternalAPIVendor>(ExternalAPIVendor.class));

			final MapSqlParameterSource vendorParameter = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			resultFromProcedure = simpleJdbcCall.execute(vendorParameter);
			vendorList = (ArrayList) resultFromProcedure.get("vendorList");

			ArrayList<ExternalAPISearchParameters> externalAPIInputParameters = null;

			final HashMap<Integer, ArrayList<ExternalAPISearchParameters>> apiParamMap = new HashMap<Integer, ArrayList<ExternalAPISearchParameters>>();

			exteApi = new ExternalAPIInformation();
			exteApi.setVendorList((ArrayList<ExternalAPIVendor>) vendorList);
			if ("FindOnlineStores".equals(moduleName))
			{
				for (ExternalAPIVendor obj : vendorList)
				{
					externalAPIInputParameters = getApiparams(Integer.valueOf(obj.getApiUsageID()), "FindOnlineStores");
					apiParamMap.put(Integer.valueOf(obj.getApiUsageID()), externalAPIInputParameters);
				}

			}

			exteApi.setSerchParameters(apiParamMap);

			// log.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" +
			// arUniversityList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllUniversity" + exception);
		}
		return exteApi;
	}

	private ArrayList<ExternalAPISearchParameters> getApiparams(int id, String moduleName) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllUniversity ");
		Map<String, Object> resultFromProcedure = null;

		ExternalAPIInformation exteApi = null;

		List<University> arUniversityList = null;

		ArrayList<ExternalAPISearchParameters> paramList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetAPIInputParameters");
			simpleJdbcCall.returningResultSet("aipInputList", new BeanPropertyRowMapper<ExternalAPISearchParameters>(
					ExternalAPISearchParameters.class));

			final MapSqlParameterSource APISearchParameters = new MapSqlParameterSource().addValue("prAPIUsageID", id).addValue("PrAPISubModuleName",
					moduleName);
			resultFromProcedure = simpleJdbcCall.execute(APISearchParameters);
			paramList = (ArrayList) resultFromProcedure.get("aipInputList");

			log.info("Inside ShopperDAOImpl : getAllUniversity  Size \t:" + paramList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllUniversity" + exception);
		}
		return paramList;
	}

	public ProductDetail getProductDetails(String productId) throws ScanSeeWebSqlException
	{

		ProductDetail prdDetail = null;

		simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
		prdDetail = simpleJdbcTemplate.queryForObject(ShoppingListQueries.ONLINESTOREPRODUCTINFOQUERY, new BeanPropertyRowMapper<ProductDetail>(
				ProductDetail.class), productId);
		if (prdDetail != null)
		{
			return prdDetail;
		}

		return prdDetail;
	}

	public String fetchUniversityName(Integer universityId) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchUniversityName";
		log.info(ApplicationConstants.METHODSTART + methodName);
		University university = null;
		String univerSityName = null;
		try
		{

			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			university = simpleJdbcTemplate.queryForObject(ShoppingListQueries.FETCHUNIVERSITYNAME, new BeanPropertyRowMapper<University>(
					University.class), universityId);

			univerSityName = university.getUniversityName();
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return univerSityName;

	}

	/**
	 * The DAO method for fetching user favourite categories.
	 * 
	 * @param userId
	 *            for which the information to be fetched.
	 * @param lowerLimit
	 *            used for pagination
	 * @return CategoryDetail object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@SuppressWarnings("unchecked")
	public CategoryDetail fetchUserFavCategories(Long userId, Integer lowerLimit) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchUserFavCategories";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<SubCategoryDetail> subList = null;
		final List<MainCategoryDetail> mainList = new ArrayList<MainCategoryDetail>();
		MainCategoryDetail mainCategoryDetail = null;
		CategoryDetail categoryDetailResp = null;

		final Map<String, Object> resultFromProcedure;

		List<CategoryDetail> categoryDetailList = null;

		int preMainCategorId = 0;
		CategoryDetail categoryDetail = null;
		int currentCatId;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerPreferredCategoryDisplay");
			simpleJdbcCall.returningResultSet("fetchPreferredCategories", new BeanPropertyRowMapper<CategoryDetail>(CategoryDetail.class));

			final MapSqlParameterSource fetchPreferredRetailersParams = new MapSqlParameterSource();
			fetchPreferredRetailersParams.addValue("UserID", userId);

			resultFromProcedure = simpleJdbcCall.execute(fetchPreferredRetailersParams);
			categoryDetailList = (List<CategoryDetail>) resultFromProcedure.get("fetchPreferredCategories");

			for (Iterator<CategoryDetail> iterator = categoryDetailList.iterator(); iterator.hasNext();)
			{
				categoryDetail = iterator.next();
				currentCatId = categoryDetail.getParentCategoryID();

				if (preMainCategorId == 0)
				{
					preMainCategorId = categoryDetail.getParentCategoryID();
					mainCategoryDetail = new MainCategoryDetail();
					mainCategoryDetail.setMainCategoryId(preMainCategorId);
					mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
					subList = new ArrayList<SubCategoryDetail>();
				}

				if (currentCatId != preMainCategorId)
				{
					mainCategoryDetail.setSubCategoryDetailLst(subList);
					mainList.add(mainCategoryDetail);
					subList = new ArrayList<SubCategoryDetail>();
					final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
					subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
					subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
					subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
					// subCategoryDetail.setRowNum(categoryDetail.getRowNum());
					subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
					subCategoryDetail.setDisplayed(categoryDetail.getDisplayed());
					subList.add(subCategoryDetail);
					mainCategoryDetail = new MainCategoryDetail();
					mainCategoryDetail.setMainCategoryId(currentCatId);
					mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());

					preMainCategorId = categoryDetail.getParentCategoryID();
				}
				else
				{
					final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
					subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
					subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
					subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
					subCategoryDetail.setDisplayed(categoryDetail.getDisplayed());
					// subCategoryDetail.setRowNum(categoryDetail.getRowNum());
					subList.add(subCategoryDetail);
				}
			}

			if (null != mainList && !mainList.isEmpty())
			{
				categoryDetailResp = new CategoryDetail();
				categoryDetailResp.setMainCategoryDetaillst(mainList);
				Boolean cellPhone = (Boolean) resultFromProcedure.get("CellPhone");
				Boolean email = (Boolean) resultFromProcedure.get("Email");

				if (null != cellPhone)
				{
					if (cellPhone)
					{
						categoryDetailResp.setCellPhone(true);
					}
				}

				if (null != email)
				{
					if (email)
					{
						categoryDetailResp.setEmail(true);
					}
				}
				// categoryDetailResp.setCellPhone(categoryDetailList)
				/*
				 * final Boolean nextpage = (Boolean)
				 * resultFromProcedure.get("NxtPageFlag"); if (nextpage) {
				 * categoryDetailResp.setNextPage(1); } else {
				 * categoryDetailResp.setNextPage(0); }
				 */

			}
		}
		catch (DataAccessException e)
		{
			log.error("fetchUserFavCategories" + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return categoryDetailResp;
	}

	/**
	 * The DAo method for saving user favourite categories in the database.
	 * 
	 * @param categoryDetail
	 *            categories details.
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public String createOrUpdateFavCategories(String categoryId, Long userID, boolean cellPhoneStatus, boolean emailStatus)
			throws ScanSeeWebSqlException
	{
		final String methodName = "createOrUpdateFavCategories";
		log.info(ApplicationConstants.METHODSTART + methodName);

		int fromProc = 0;
		final Date currentDate = new Date();
		Integer cellPhone = 0;
		Integer email = 0;
		if (cellPhoneStatus == true)
		{
			cellPhone = 1;
		}
		if (emailStatus == true)
		{
			email = 1;
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerPreferredCategory");
			SqlParameterSource updateFavCategoriesParams;
			updateFavCategoriesParams = new MapSqlParameterSource()

			.addValue("userID", userID).addValue("Category", categoryId).addValue("Date", new java.sql.Timestamp(currentDate.getTime()))
					.addValue("EmailAlert", email).addValue("CellPhoneAlert", cellPhone);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(updateFavCategoriesParams);
			fromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

			if (fromProc == 1)
			{
				log.info("Inside HotDealsDAOImpl : createOrUpdateFavCategories : Failure :" + ApplicationConstants.FAILURE);
				return ApplicationConstants.FAILURE;
			}
			else
			{
				log.info("Inside HotDealsDAOImpl : createOrUpdateFavCategories : Success : " + ApplicationConstants.SUCCESS);
				return ApplicationConstants.SUCCESS;
			}
		}
		catch (DataAccessException e)
		{
			log.error("Inside HotDealsDAOImpl : createOrUpdateFavCategories " + e.getMessage());
			throw new ScanSeeWebSqlException(e.getMessage());
		}
	}

	public List<DoubleCheck> fetchAllCategory(Long userId) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAllNonProfitPartner");
		Map<String, Object> resultFromProcedure = null;
		List<DoubleCheck> arCategoryList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_PreferredCategoryDisplay");
			simpleJdbcCall.returningResultSet("getCategoryList", new BeanPropertyRowMapper<DoubleCheck>(DoubleCheck.class));

			final MapSqlParameterSource objCategoryParameter = new MapSqlParameterSource().addValue("UserID", userId);

			resultFromProcedure = simpleJdbcCall.execute(objCategoryParameter);
			arCategoryList = (ArrayList) resultFromProcedure.get("getCategoryList");
			log.info("Inside ShopperDAOImpl :getAllNonProfitPartner  Size \t:" + arCategoryList.size());
		}
		catch (DataAccessException exception)
		{
			log.info("Exception occurred in getAllNonProfitPartner" + exception);
		}
		return arCategoryList;
	}

	/**
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAppConfig ");
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Inside ShopperDAOImpl : getAppConfig : Error occurred in  Store Procedure with error number: {} " + errorNum
						+ " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error("Inside ShopperDAOImpl : getAppConfig : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}

	/**
	 * The method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as parameter
	 * @param retailerDetail
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String getProductInfo(common.pojo.shopper.ShareProductInfo shareProductInfo, String shareURL) throws ScanSeeWebSqlException
	{

		final String methodName = "getProductInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetaillst = null;
		ProductDetail productDetail = null;
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			productDetaillst = simpleJdbcTemplate.query(ShoppingListQueries.PRODUCTINOQUERY, new BeanPropertyRowMapper<ProductDetail>(
					ProductDetail.class), shareProductInfo.getProductId());
			if (productDetaillst != null && !productDetaillst.isEmpty())
			{
				try
				{
					productDetail = productDetaillst.get(0);
					response = Utility.formProductInfoHTML(productDetail, shareProductInfo.getUserId(), shareURL);
				}
				catch (InvalidKeyException e)
				{
					log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

				}
				catch (NoSuchAlgorithmException e)
				{
					log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
				}
				catch (NoSuchPaddingException e)
				{
					log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
				}
				catch (InvalidAlgorithmParameterException e)
				{
					log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
				}
				catch (InvalidKeySpecException e)
				{
					log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
				}
				catch (IllegalBlockSizeException e)
				{
					log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
				}
				catch (BadPaddingException e)
				{
					log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
				}
			}
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method for getting user information like email id .
	 * 
	 * @param shareProductInfo
	 *            contains userid throws ScanSeeWebSqlException
	 */

	public String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException
	{

		final String methodName = "getUserInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			response = this.jdbcTemplate.queryForObject(ShoppingListQueries.FETCHUSEREMAILID, new Object[] { shareProductInfo.getUserId() },
					String.class);

		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The Service method for updating the user information
	 * 
	 * @param userPreference
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	public String updateUserData(UserPreference userPreference) throws ScanSeeWebSqlException
	{
		final String methodName = "updateUserData";
		log.info(ApplicationConstants.METHODSTART + methodName);

		String result = null;
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		Integer errorNum = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebShopperProfileUpdation");
			final MapSqlParameterSource objAddRegParameter = new MapSqlParameterSource();

			objAddRegParameter.addValue("UserID", userPreference.getUserID());
			objAddRegParameter.addValue("FirstName", userPreference.getFirstName());
			objAddRegParameter.addValue("LastName", userPreference.getLastName());
			objAddRegParameter.addValue("Address1", userPreference.getAddress1());
			objAddRegParameter.addValue("Address2", userPreference.getAddress2());
			objAddRegParameter.addValue("State", userPreference.getState());
			objAddRegParameter.addValue("City", userPreference.getCity());
			objAddRegParameter.addValue("PostalCode", userPreference.getPostalCode());
			objAddRegParameter.addValue("MobilePhoneNumber", Utility.removePhoneFormate(userPreference.getMobilePhone()));

			resultFromProcedure = simpleJdbcCall.execute(objAddRegParameter);
			responseFromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (null != responseFromProc && responseFromProc.intValue() == 0)
			{
				result = ApplicationConstants.SUCCESS;
			}
			else
			{
				errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Inside ShopperDAOImpl : updateUserData   : errorNumber  : " + errorNum + "errorMessage : " + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (Exception exception)
		{
			log.error("Inside ShopperDAOImpl : updateUserData : " + exception.getMessage());
			throw new ScanSeeWebSqlException(exception.getMessage(), exception.getCause());
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return result;
	}

	public ArrayList<ThisLocationRequest> getRediusDetails() throws ScanSeeWebSqlException
	{
		final String methodName = "getRediusDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ThisLocationRequest> thislocatonRediuslst;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_FetchRadius");
			simpleJdbcCall.returningResultSet("radiuslst", new BeanPropertyRowMapper<ThisLocationRequest>(ThisLocationRequest.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				thislocatonRediuslst = (ArrayList<ThisLocationRequest>) resultFromProcedure.get("radiuslst");

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return thislocatonRediuslst;
	}

	/**
	 * This method is used to fetch the Latitude and Longitude for the given
	 * Zipcode.
	 * 
	 * @param zipcode
	 *            Zipcode for getting Lat and Long.
	 * @return ThisLocationRequest Container lat and long.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ThisLocationRequest fetchLatLong(String zipcode) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchLatLong in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<ThisLocationRequest> thisLocationRequestLst = null;
		ThisLocationRequest thisLocationRequestObj = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			thisLocationRequestLst = simpleJdbcTemplate.query(ThisLocationQuery.FETCHLATLONG, new BeanPropertyRowMapper<ThisLocationRequest>(
					ThisLocationRequest.class), zipcode);

			if (!thisLocationRequestLst.isEmpty() && thisLocationRequestLst != null)
			{
				thisLocationRequestObj = new ThisLocationRequest();
				thisLocationRequestObj = thisLocationRequestLst.get(0);
			}
		}
		catch (DataAccessException exception)
		{

			log.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception.getMessage());

		}
		return thisLocationRequestObj;
	}

	/**
	 * This method is used to domain Name for Application configuration
	 * 
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<AppConfiguration> getAppConfigForGeneralImages(String configType) throws ScanSeeWebSqlException
	{
		log.info("Inside ShopperDAOImpl : getAppConfigForGeneralImages ");
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GeneralImagesGetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Inside ShopperDAOImpl : getAppConfigForGeneralImages : Error occurred in usp_GeneralImagesGetScreenContent Store Procedure with error number: {} "
						+ errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error("Inside SupplierDAOImpl : getAppConfig : " + e);
			throw new ScanSeeWebSqlException(e);
		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}

	/**
	 * This method used for fetching retailer details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	@SuppressWarnings("unchecked")
	public List<RetailerDetail> getRetailerSummary(ThisLocationRetailerInfo retailerDetailObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetailerSummary";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + retailerDetailObj.getUserId());
		}

		String responseFromProc = null;

		List<RetailerDetail> retailSummarylst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_DisplayRetailLocationInfo");
			simpleJdbcCall.returningResultSet("retailerSummaryDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", retailerDetailObj.getUserId());
			scanQueryParams.addValue("RetailID", retailerDetailObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", retailerDetailObj.getRetailLocationID());
			scanQueryParams.addValue("Latitude", retailerDetailObj.getRetLat());
			scanQueryParams.addValue("Longitude", retailerDetailObj.getRetLng());
			scanQueryParams.addValue("PostalCode", retailerDetailObj.getPostalCode());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			retailSummarylst = (List<RetailerDetail>) resultFromProcedure.get("retailerSummaryDetails");

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_DisplayRetailLocationInfo ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getRetailerSummary", exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return retailSummarylst;
	}

	/**
	 * This Rest Easy method for fetching retailer created page details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	public List<RetailerCreatedPages> getRetCreatePages(ThisLocationRetailerInfo retailerDetailObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetCreatePages";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + retailerDetailObj.getUserId());
		}

		String responseFromProc = null;

		List<RetailerCreatedPages> retCreatedPageslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_QRDisplayRetailerCreatedPages");
			simpleJdbcCall.returningResultSet("retCreatedPages", new BeanPropertyRowMapper<RetailerCreatedPages>(RetailerCreatedPages.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", retailerDetailObj.getUserId());
			scanQueryParams.addValue("RetailID", retailerDetailObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", retailerDetailObj.getRetailLocationID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			retCreatedPageslst = (List<RetailerCreatedPages>) resultFromProcedure.get("retCreatedPages");

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_QRRetailerCreatedPagesDetails ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getRetCreatePages", exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return retCreatedPageslst;
	}

	/**
	 * This Rest Easy method for fetching retailer special offer list.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	public List<RetailerDetail> fetchSpecialOffers(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchSpecialOffers";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + thisLocationRetailerInfoObj.getUserId());
		}
		String responseFromProc = null;
		List<RetailerDetail> specialOfferlst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_RetailerLocationSpecialOffersDisplay");
			simpleJdbcCall.returningResultSet("retailerStoreDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", thisLocationRetailerInfoObj.getUserId());
			scanQueryParams.addValue("RetailID", thisLocationRetailerInfoObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", thisLocationRetailerInfoObj.getRetailLocationID());

			if (null == thisLocationRetailerInfoObj.getLastVisitedNo())
			{
				thisLocationRetailerInfoObj.setLastVisitedNo(0);
			}

			scanQueryParams.addValue("LowerLimit", thisLocationRetailerInfoObj.getLastVisitedNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.THISLOCATIONPRODUCTSSCREEN);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			specialOfferlst = (List<RetailerDetail>) resultFromProcedure.get("retailerStoreDetails");
			if (null != resultFromProcedure)
			{

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_RetailerLocationSpecialOffersDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchSpecialOffers", exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return specialOfferlst;
	}

	/**
	 * This method for fetching retailer special offers, coupons, hot deals
	 * details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	public List<RetailerDetail> fetchSpecialOffersHotDealDiscounts(ThisLocationRetailerInfo thisLocationRetailerInfoObj)
			throws ScanSeeWebSqlException
	{
		final String methodName = "fetchSpecialOffersHotDealDiscounts";

		String responseFromProc = null;
		ProductDetails productDetailsObj = null;
		List<RetailerDetail> specialOfferlst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_RetailLocationSpecialDealsDisplay");
			simpleJdbcCall.returningResultSet("specialofferdiscounts", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", thisLocationRetailerInfoObj.getUserId());
			scanQueryParams.addValue("RetailID", thisLocationRetailerInfoObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", thisLocationRetailerInfoObj.getRetailLocationID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			specialOfferlst = (List<RetailerDetail>) resultFromProcedure.get("specialofferdiscounts");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != specialOfferlst && !specialOfferlst.isEmpty())
					{
						productDetailsObj = new ProductDetails();
						productDetailsObj.setSpecialOfferlst(specialOfferlst);
					}

				}

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_RetailLocationSpecialDealsDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return specialOfferlst;
	}

	/**
	 * This method for fetching retailer coupons details.It includes retailer
	 * coupons ,loyalties and rebates.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */

	public CLRDetails getRetailerCLRDetails(ThisLocationRetailerInfo retailerDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetailerCLRDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;
		/**
		 * This variables needed to check nextPage is 1 or 0 for pagination.
		 */
		boolean isCouponNextPage = false;
		boolean isLoyaltyNextPage = false;
		boolean isRebateNextPage = false;

		List<RebateDetail> rebateDetailList = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		List<CouponDetails> couponDetailList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_RetailLocationCouponsDisplay");

			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("UserID", retailerDetail.getUserId());
			fetchCouponParameters.addValue("RetailID", retailerDetail.getRetailerId());
			fetchCouponParameters.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			if (retailerDetail.getLastVisitedNo() == null)
			{
				retailerDetail.setLastVisitedNo(0);
			}
			fetchCouponParameters.addValue("LowerLimit", retailerDetail.getLastVisitedNo());
			fetchCouponParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get("ErrorNumber"))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_RetailLocationCouponsDisplay method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					if (null != couponDetailList && !couponDetailList.isEmpty())
					{
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								isCouponNextPage = true;
							}
						}
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_RetailLocationLoyaltyDealDisplay");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue("UserID", retailerDetail.getUserId());
			fetchLoyaltyParameters.addValue("RetailID", retailerDetail.getRetailerId());
			fetchLoyaltyParameters.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			fetchLoyaltyParameters.addValue("LowerLimit", retailerDetail.getLastVisitedNo());
			fetchLoyaltyParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_RetailLocationLoyaltyDealDisplay method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");
				if (null != loyaltyDetailList && !loyaltyDetailList.isEmpty())
				{
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							isRebateNextPage = true;
						}
					}

				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_RetailLocationRebateDisplay");
			simpleJdbcCall.returningResultSet("RebateDetails", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final MapSqlParameterSource fetchRebateParameters = new MapSqlParameterSource();
			fetchRebateParameters.addValue("UserID", retailerDetail.getUserId());
			fetchRebateParameters.addValue("RetailID", retailerDetail.getRetailerId());
			fetchRebateParameters.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			fetchRebateParameters.addValue("LowerLimit", retailerDetail.getLastVisitedNo());
			fetchRebateParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_RetailLocationRebateDisplay  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
			else
			{
				rebateDetailList = (List<RebateDetail>) resultFromProcedure.get("RebateDetails");
				if (null != rebateDetailList && !rebateDetailList.isEmpty())
				{
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							isLoyaltyNextPage = true;
						}
					}

				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		if (couponDetailList != null || !couponDetailList.isEmpty() || rebateDetailList != null || !rebateDetailList.isEmpty()
				|| loyaltyDetailList != null || !loyaltyDetailList.isEmpty())
		{
			clrDetatilsObj = new CLRDetails();
			if (isCouponNextPage == true || isRebateNextPage == true || isRebateNextPage == true)
			{
				clrDetatilsObj.setNextPageFlag(1);
			}
			if (isCouponNextPage == true)
			{
				clrDetatilsObj.setClrC(1);
			}
			else
			{
				clrDetatilsObj.setClrC(0);
			}
			if (isLoyaltyNextPage == true)
			{
				clrDetatilsObj.setClrL(1);
			}
			else
			{
				clrDetatilsObj.setClrL(0);
			}
			if (isLoyaltyNextPage == true)
			{
				clrDetatilsObj.setClrR(1);
			}
			else
			{
				clrDetatilsObj.setClrR(0);
			}
			if (couponDetailList != null && !couponDetailList.isEmpty())
			{
				clrDetatilsObj.setIsCouponthere(true);
				clrDetatilsObj.setCouponDetails(couponDetailList);
			}
			else
			{
				clrDetatilsObj.setIsCouponthere(false);
			}
			if (rebateDetailList != null && !rebateDetailList.isEmpty())
			{
				clrDetatilsObj.setIsRebatethere(true);
				clrDetatilsObj.setRebateDetails(rebateDetailList);
			}
			else
			{
				clrDetatilsObj.setIsRebatethere(false);
			}
			if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
			{
				clrDetatilsObj.setIsLoyaltythere(true);
				clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
			}
			else
			{
				clrDetatilsObj.setIsLoyaltythere(false);
			}

		}
		return clrDetatilsObj;
	}

	/**
	 * This DAO method for fetching retailer location hot deals.
	 * 
	 * @param retailerDetailObj
	 *            as the request object containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response object or exception.
	 */

	public List<HotDealsDetails> getRetailerHotDeals(ThisLocationRetailerInfo retailerDetailObj) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetailerHotDeals";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + retailerDetailObj.getUserId());
		}

		List<HotDealsDetails> hotDealsDetailslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_RetailLocationHotDealsDisplay");
			simpleJdbcCall.returningResultSet("hotDeals", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", retailerDetailObj.getUserId());
			scanQueryParams.addValue("RetailID", retailerDetailObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", retailerDetailObj.getRetailLocationID());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.RETAILERHOTDEALS);

			if (retailerDetailObj.getLastVisitedNo() == null)
			{
				retailerDetailObj.setLastVisitedNo(0);
			}

			scanQueryParams.addValue("LowerLimit", retailerDetailObj.getLastVisitedNo());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			hotDealsDetailslst = (List<HotDealsDetails>) resultFromProcedure.get("hotDeals");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_RetailLocationHotDealsDisplay ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getRetailerHotDeals", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		return hotDealsDetailslst;
	}

	/**
	 * This DAO method for fetching retailer special deal details.
	 * 
	 * @param retailerDetailObj
	 *            as the request object containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response object or exception.
	 */
	public List<RetailerDetail> fetchSpecialDealsDetails(RetailerDetail specialOfferRequest) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchSpecialDealsDetails";

		String responseFromProc = null;
		List<RetailerDetail> specialOfferlst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_RetailerLocationSpecialOffersDetails");
			simpleJdbcCall.returningResultSet("retailerStoreDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("PageID", specialOfferRequest.getPageID());
			scanQueryParams.addValue("RetailID", specialOfferRequest.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", specialOfferRequest.getRetailLocationID());

			if (null == specialOfferRequest.getLastVisitedNo())
			{
				specialOfferRequest.setLastVisitedNo(0);
			}

			scanQueryParams.addValue("LowerLimit", specialOfferRequest.getLastVisitedNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.THISLOCATIONPRODUCTSSCREEN);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			specialOfferlst = (List<RetailerDetail>) resultFromProcedure.get("retailerStoreDetails");
			if (null != resultFromProcedure)
			{

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_RetailerLocationSpecialOffersDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return specialOfferlst;
	}
}

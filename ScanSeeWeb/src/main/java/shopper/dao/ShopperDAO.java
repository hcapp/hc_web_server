package shopper.dao;

import java.util.ArrayList;
import java.util.List;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.Category;
import common.pojo.DoubleCheckInfo;
import common.pojo.NonProfitPartner;
import common.pojo.PreferencesInfo;
import common.pojo.SchoolInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.University;
import common.pojo.UserPreference;
import common.pojo.UserPreferenceDisplay;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.DiscountUserInfo;
import common.pojo.shopper.DoubleCheck;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.LoyaltyDetail;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.RebateDetail;
import common.pojo.shopper.RetailerCreatedPages;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.ThisLocationRequest;
import common.pojo.shopper.ThisLocationRetailerInfo;

/**
 * This interface consists of DAO layer methods for shopper.
 * @author manjunath_gh
 *
 */
public interface ShopperDAO
{
	/**
	 * This method insert the facebook user registration details.
	 * 
	 * @param userRegistrationInfo
	 *            instance of UserRegistrationInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	Users registerFaceBookUser(String userId) throws ScanSeeWebSqlException;

	/**
	 * This method insert the Shopper registration details.
	 * 
	 * @param objUsers
	 *            instance of Users.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeWebSqlException
	 */
	public Users addShopperRegistration(Users objUsers) throws ScanSeeWebSqlException;

	public String addPreferencesInfo(PreferencesInfo preferencesInfo) throws ScanSeeWebSqlException;

	public String addDoubleCheckInfo(DoubleCheckInfo doubleCheckInfo) throws ScanSeeWebSqlException;

	/**
	 * This method Add UserPreference details.
	 * 
	 * @param objUserPreference
	 *            instance of UserPreference.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	// usp_WebUserPreferences
	public String addUserPreferences(CategoryDetail categoryDetail, Long userId) throws ScanSeeWebSqlException;

	/*
	 * This method Add UserPreference details.
	 * @param objUserPreference instance of UserPreference.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */

	// public String addUserPreferences(PreferencesInfo preferencesInfo) throws
	// ScanSeeWebSqlException;

	/**
	 * This method will return all University .
	 * 
	 * @return List of UniversityName and University ID .
	 * @throws Exception
	 */
	public List<University> getAllUniversity() throws ScanSeeWebSqlException;

	/**
	 * This method will return all Non Profit Partner.
	 * 
	 * @return List of NonProfitPartnerID , NonProfitPartnerName .
	 * @throws Exception
	 */
	public List<NonProfitPartner> getAllNonProfitPartner() throws ScanSeeWebSqlException;

	/**
	 * This method will return all Category .
	 * 
	 * @return List of Category ID , Category Name .
	 * @throws Exception
	 */
	public List<Category> getAllCategory() throws ScanSeeWebSqlException;

	/**
	 * This method Add UserPreference details.
	 * 
	 * @param objUserPreference
	 *            instance of UserPreference.
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */

	// public String addUserPreferences(UserPreference objUserPreference) throws
	// ScanSeeWebSqlException;

	/**
	 * This method select School details.
	 * 
	 * @param objUserPreference
	 *            instance of UserPreference
	 * @return success or failure depending upon the result of operation.
	 * @throws Exception
	 */
	// public String addSchoolSelection(UserPreference objUserPreference) throws
	// ScanSeeWebSqlException;
	public String addSchoolSelection(SchoolInfo schoolInfo, ArrayList<Long> selectedUniversityIdsList, ArrayList<Long> selectedNonProfitIdsList)
			throws ScanSeeWebSqlException;

	/**
	 * This method will display Shopper User's Preferences details
	 * 
	 * @param userId
	 * @return User's Preferences, Caterory Name, University Name,
	 *         NonProfitPartnerName details.
	 * @throws Exception
	 */
	public UserPreferenceDisplay displayPreferencesDetails(Long userId) throws ScanSeeWebSqlException;

	/**
	 * The method fetches the Retailer Details from the database for the given
	 * search parameters(Zipcode or Latitude and longitude, radius and user id).
	 * 
	 * @param objAreaInfoVO
	 *            Instance of AreaInfoVO.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return RetailersDetails returns RetailerDetails object container List of
	 *         retailers. Controller layer.
	 */
	public RetailersDetails fetchRetailerDetails(AreaInfoVO objAreaInfoVO, String screenName) throws ScanSeeWebSqlException;

	/**
	 * This method for getting the All state List based .
	 * @return ArrayList<SchoolInfo>
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<SchoolInfo> getAllStates() throws ScanSeeWebSqlException;

	/**
	 * This method for getting the All College List based on the state code.
	 * @param stateCode
	 * @return ArrayList<SchoolInfo>
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<SchoolInfo> getCollegeList(String stateCode) throws ScanSeeWebSqlException;

	/**
	 * The method fetches the Product Details from the database for the given
	 * search parameters(Retailer LocationID, lower limit and user id).
	 * 
	 * @param productDetailsRequest
	 *            Instance of ProductDetailsRequest.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return ProductDetails returns ProductDetails object container List of
	 *         products available on the retailer.
	 */
	public ProductDetails fetchProductDetails(ProductDetailsRequest productDetailsRequest, String screenName) throws ScanSeeWebSqlException;

	/**
	 * This method for saving the all the school information.
	 * @param schoolInfo
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	public String addSchoolInfo(SchoolInfo schoolInfo) throws ScanSeeWebSqlException;
	
	/**
	 * This method for saving the all the school information.
	 * @param userId
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public SchoolInfo displaySchoolInfo(Long userId) throws ScanSeeWebSqlException;

	/**
	 * This method will return all ProductList .
	 * 
	 * @throws Exception
	 */
	public SearchResultInfo fetchAllProduct(SearchForm objForm, Users loginUser, int lowerLimit) throws ScanSeeWebSqlException;

	public List<CouponDetail> fetchCouponInfo(DiscountUserInfo discountUserInfo) throws ScanSeeWebSqlException;

	public List<RebateDetail> fetchRebateInfo(DiscountUserInfo discountUserInfo) throws ScanSeeWebSqlException;

	public List<LoyaltyDetail> fetchLoyaltyInfo(DiscountUserInfo discountUserInfo) throws ScanSeeWebSqlException;

	public String addCoupon(Long userId, Integer couponId) throws ScanSeeWebSqlException;

	public String removeCoupon(Long userId, Integer couponId) throws ScanSeeWebSqlException;

	public String addRebate(Long userId, Integer couponId) throws ScanSeeWebSqlException;

	public String removeRebate(Long userId, Integer couponId) throws ScanSeeWebSqlException;

	public String addLoyalty(Long userId, Integer couponId) throws ScanSeeWebSqlException;

	public String removeLoyalty(Long userId, Integer couponId) throws ScanSeeWebSqlException;

	public ExternalAPIInformation getExternalApiInfo(String moduleName) throws ScanSeeWebSqlException;

	public ProductDetail getProductDetails(String productId) throws ScanSeeWebSqlException;

	public String fetchUniversityName(Integer universityId) throws ScanSeeWebSqlException;

	/**
	 * The DAO method for fetching user favourite categories.
	 * 
	 * @param userId
	 *            for which the information to be fetched.
	 * @param lowerLimit
	 *            used for pagination
	 * @return CategoryDetail object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@SuppressWarnings("unchecked")
	public CategoryDetail fetchUserFavCategories(Long userId, Integer lowerLimit) throws ScanSeeWebSqlException;

	/**
	 * The DAo method for saving user favourite categories in the database.
	 * 
	 * @param categoryDetail
	 *            categories details.
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeWebSqlException will be
	 *             thrown.
	 */

	public String createOrUpdateFavCategories(String categoryId, Long userID, boolean cellPhoneStatus, boolean emailStatus) throws ScanSeeWebSqlException;

	public List<DoubleCheck> fetchAllCategory(Long userId) throws ScanSeeWebSqlException;

	/**
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException;

	/**
	 * The DAo method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @param retailerDetail
	 *            as request parameter
	 * @param userId
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getProductInfo(ShareProductInfo shareProductInfo, String shareURL) throws ScanSeeWebSqlException;

	/**
	 * This method for getting user information like email id .
	 * 
	 * @param shareProductInfo
	 *            contains userid throws ScanSeeWebSqlException
	 */
	String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException;

	/**
	 * The Service method for updating the user information
	 * @param userPreference
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	public String updateUserData(UserPreference userPreference) throws ScanSeeWebSqlException;

	/**
	 * Method declaration of get radius details.
	 * 
	 * @param userID
	 *            The userID in the request.
	 * @param retailID
	 *            The retailID in the request.
	 * @return favCategoryDetails info.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	ArrayList<ThisLocationRequest> getRediusDetails() throws ScanSeeWebSqlException;

	/**
	 * This method is used to fetch the Latitude and Longitude for the given
	 * Zipcode.
	 * 
	 * @param zipcode
	 *            Zipcode for getting Lat and Long.
	 * @return ThisLocationRequest Container lat and long.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ThisLocationRequest fetchLatLong(String zipcode) throws ScanSeeWebSqlException;

	/**
	 * This method is used to domain Name for Application configuration
	 * 
	 * @param configType
	 * @return
	 * @throws ScanSeeWebSqlException
	 */
	public ArrayList<AppConfiguration> getAppConfigForGeneralImages(String configType) throws ScanSeeWebSqlException;

	/**
	 * This Rest Easy method for fetching retailer summary details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	List<RetailerDetail> getRetailerSummary(ThisLocationRetailerInfo thisLocationRetailerInfo) throws ScanSeeWebSqlException;

	/**
	 * This Rest Easy method for fetching retailer created pages details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	List<RetailerCreatedPages> getRetCreatePages(ThisLocationRetailerInfo thisLocationRetailerInfo) throws ScanSeeWebSqlException;

	/**
	 * This Rest Easy method for fetching special offer details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	List<RetailerDetail> fetchSpecialOffers(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException;

	/**
	 * This Rest Easy method for fetching special offer,hot deals and
	 * coupon,loyalty and rebate discounts based on retailer and location id.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	List<RetailerDetail> fetchSpecialOffersHotDealDiscounts(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException;

	/**
	 * This Rest Easy method for fetching retailer coupons details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	CLRDetails getRetailerCLRDetails(ThisLocationRetailerInfo retailerDetail) throws ScanSeeWebSqlException;

	/**
	 * This DAO method for fetching retailer location hot deals.
	 * 
	 * @param xml
	 *            as the request object containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response object or exception.
	 */
	List<HotDealsDetails> getRetailerHotDeals(ThisLocationRetailerInfo thisLocationRetailerInfoObj) throws ScanSeeWebSqlException;

	/**
	 * This DAO method for fetching retailer special deal details.
	 * 
	 * @param xml
	 *            as the request object containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response object or exception.
	 */
	List<RetailerDetail> fetchSpecialDealsDetails(RetailerDetail specialOfferRequest) throws ScanSeeWebSqlException;

}

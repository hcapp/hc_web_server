package shopper.dao;

import java.util.ArrayList;
import java.util.List;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.ConsumerRequestDetails;
import common.pojo.SearchForm;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.UserRatingInfo;
import common.pojo.shopper.UserTrackingData;
import common.pojo.shopper.WishListProducts;

/**
 * The Interface for BaseDAO. ImplementedBy {@link BaseDAOImpl}
 * 
 * @author malathi_lr
 */
public interface BaseDAO
{

	/**
	 * The method for fetching product details.
	 * 
	 * @param productId
	 *            request parameter
	 * @param userId
	 *            request parameter
	 * @param retailId
	 *            request parameter
	 * @return The ProductDetail Information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetail getProductDetail(Long userId, Integer productId, Integer retailId) throws ScanSeeWebSqlException;

	/**
	 * The method for getting Product Attribute information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	ProductDetail fetchProductAttributeDetails(Long userId, Integer productId) throws ScanSeeWebSqlException;

	// ##########################################NEWLY CREATED METHODS FOR
	// CONSUMER IMPLEMENTATION#############################

	/**
	 * This method is used to display product details like name,image,audio
	 * ,video and attributes.
	 */
	ProductDetail fetchConsumerProductDetails(ProductDetail productDetail) throws ScanSeeWebSqlException;

	List<ProductDetail> fetchConsumerProductImagelst(SearchForm searchForm) throws ScanSeeWebSqlException;

	List<ProductDetail> fetchConsumerProductMediaInfo(ProductDetail productDetail) throws ScanSeeWebSqlException;

	List<ProductDetail> fetchConsumerProductAttributeDetails(ProductDetail productDetail) throws ScanSeeWebSqlException;

	HotDealsDetails getConsumerHotDealDetail(HotDealsListRequest hotDealsListRequestObj) throws ScanSeeWebSqlException;

	FindNearByDetails getConsFindNearByRetailer(ProductDetail productDetailObj) throws ScanSeeWebSqlException;

	List<ProductDetail> fetchConsumerProductReviews(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ArrayList<RetailerDetail> getConsumerCommisJunctionData(ProductDetail productDetail) throws ScanSeeWebSqlException;

	List<CouponDetail> fetchConsumerProductCoupons(ProductDetail productDetail) throws ScanSeeWebSqlException;

	Integer getDefaultRadius() throws ScanSeeWebSqlException;

	ProductDetail consAddProdToShopp(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ProductDetail consAddProdToWishList(ProductDetail productDetail) throws ScanSeeWebSqlException;

	Long consGetUsrTrakingMainMenuId(ProductDetail productDetail) throws ScanSeeWebSqlException;

	ArrayList<AppConfiguration> consGetAppConfig(String configType) throws ScanSeeWebSqlException;

	String getConsUserInfo(ProductDetail shareProductInfo) throws ScanSeeWebSqlException;

	String ShareConsumerProductDetails(ProductDetail productDetail, String shareURL) throws ScanSeeWebSqlException;

	ExternalAPIInformation getConsExternalApiInfo(String moduleName) throws ScanSeeWebSqlException;

	UserRatingInfo consGetProductRatings(ProductDetail productRatings) throws ScanSeeWebSqlException;

	String consSaveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeWebSqlException;

	String getConsEmailId(Long userId) throws ScanSeeWebSqlException;

	String ShareConsumerHodDealDetails(ProductDetail productDetail, String shareURL) throws ScanSeeWebSqlException;

	List<HotDealsDetails> consDisplayPopulationCenters(HotDealsListRequest hotDealsListRequestObj) throws ScanSeeWebSqlException;

	List<UserTrackingData> getConsumerShareTypes() throws ScanSeeWebSqlException;
	
	ProductDetail saveConsShareProdDetails(ProductDetail ObjProductDetail) throws ScanSeeWebSqlException;

	 ProductDetail getConsumerProdMultipleImages(ProductDetail productDetail) throws ScanSeeWebSqlException;
	 
	 
	 //####################################### CONSUMER SHOPPING LIST METHODS #######################################
	 
	 AddRemoveSBProducts getConsShopLstProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	  AddRemoveSBProducts getConsSLHistoryProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	  AddRemoveSBProducts getConsSLFavoriteProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	  ProductDetails getSLSearchProducts(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	  
	  ProductDetails getConsShopSearchProd(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	  ProductDetail consFavDeleteProd(ProductDetail productDetail) throws ScanSeeWebSqlException;
	  ProductDetail consAddHistoryProdToSL(ProductDetail productDetail) throws ScanSeeWebSqlException;
	  ProductDetail consAddFavProdToShopList(ProductDetail productDetail) throws ScanSeeWebSqlException;
	  ProductDetail consAddSearchProdToSL(ProductDetail productDetail) throws ScanSeeWebSqlException;
	   ProductDetail consDeleteTodayListProd(ProductDetail productDetail) throws ScanSeeWebSqlException;
	   ProductDetail consCheckOutTodayListProd(ProductDetail productDetail) throws ScanSeeWebSqlException;
	   ProductDetail consMoveToBasketProds(ProductDetail productDetail) throws ScanSeeWebSqlException;
	 //####################################### CONSUMER WISH LIST METHODS #######################################
	   
	   ProductDetails getConsWishListProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	   ArrayList<ProductDetail> getConsWLHistoryProds(ConsumerRequestDetails objConsumerRequestDetails) throws ScanSeeWebSqlException;
	   ProductDetail consDeleteWLProd(ProductDetail productDetail) throws ScanSeeWebSqlException;
	   ProductDetail consDeleteWLHistoryProd(ProductDetail productDetail) throws ScanSeeWebSqlException;
	 
		
		ProductDetail consAddSearchProdToWL(ProductDetail productDetail) throws ScanSeeWebSqlException;
		 ArrayList<CouponDetails> getConsWLCouponDetails(ProductDetail productDetail) throws ScanSeeWebSqlException;
		 ArrayList<HotDealsDetails> getConsWLHotDealInfo(ProductDetail productDetail) throws ScanSeeWebSqlException;
		  Integer getPostalCode(Long lngUserId) throws ScanSeeWebSqlException;
		  String consWLAddCoupon(CouponDetail objCouponDetail) throws ScanSeeWebSqlException;
		   String consWLRemoveCoupon(CouponDetail objCouponDetail) throws ScanSeeWebSqlException;
		   
		   ArrayList<ProductDetail> getPrintProductInfo(ProductDetail productDetail) throws ScanSeeWebSqlException;
		   //####################################### CONSUMER MY GALLERY METHODS #######################################
		    ProductDetails getConsCoupAssocitedProds(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException;
}

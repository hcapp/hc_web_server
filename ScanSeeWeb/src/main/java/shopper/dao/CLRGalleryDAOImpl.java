package shopper.dao;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.query.ShoppingListQueries;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.CouponsDetails;
import common.pojo.shopper.LoyaltyDetail;
import common.pojo.shopper.LoyaltyDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.RebateDetail;
import common.pojo.shopper.RebateDetails;
import common.pojo.shopper.ShareProductInfo;
import common.util.Utility;

/**
 * This class for fetching CLRGallery details.
 * 
 * @author shyamsundara_hm
 */
public class CLRGalleryDAOImpl implements CLRGalleryDAO
{
	/**
	 * Logger instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CLRGalleryDAOImpl.class.getName());

	/**
	 * for jdbcTemplate connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set ParameterizedBeanPropertyRowMapper to map POJO.
	 */

	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * This method for to get data source Spring DI.
	 * 
	 * @param dataSource
	 *            for DB operations.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * Method for fetching Coupon Info. The method is called from the service
	 * layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @throws ScanSeeWebSqlException
	 *             The exception defined for the application.
	 * @return couponsDetails The xml with Coupon Details.
	 */

	@SuppressWarnings("unchecked")
	public final CouponDetails getCouponInfo(Integer userId, Integer productId, Integer retailId) throws ScanSeeWebSqlException
	{

		final String methodName = "fetchCouponInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		CouponDetails couponsDetails = null;
		List<CouponDetails> couponDetailList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListCoupon");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			Integer retailerId = retailId;
			if (0 == retailId)
			{
				retailerId = null;
			}
			final SqlParameterSource fetchCouponParameters = new MapSqlParameterSource().addValue("ProductID", productId)
					.addValue("RetailID", retailerId).addValue(ApplicationConstants.USERID, userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);
			couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get("ErrorNumber"))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Error Occured in usp_MasterShoppingListCoupon method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);

				}
				else
				{
					if (null == couponDetailList || couponDetailList.isEmpty())
					{
						return couponsDetails;
					}
					else
					{
						couponsDetails = new CouponDetails();
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return couponsDetails;

	}

	/**
	 * Method for fetching Loyalty Info. The method is called from the service
	 * layer.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,loyaltyId needed to fetch loyalty details.
	 * @return loyaltyDetails The xml with loyaltyDetails.
	 * @throws ScanSeeWebSqlException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("unchecked")
	public final LoyaltyDetails getLoyaltyInfo(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchLoyaltyInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<LoyaltyDetail> loyaltyDetailList;
		LoyaltyDetails loyaltyInfo = null;
		ArrayList<ProductDetail> productlst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GalleryLoyaltyDetails");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final SqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource().addValue(ApplicationConstants.USERID,
					clrDetailsObj.getUserId()).addValue("LoyaltyDealID", clrDetailsObj.getLoyaltyDealID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GalleryLoyaltyDetails method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");
				if (!loyaltyDetailList.isEmpty() && loyaltyDetailList != null)
				{
					productlst = new ArrayList<ProductDetail>();
					for (int i = 0; i < loyaltyDetailList.size(); i++)
					{
						
						ProductDetail productDetailObj = new ProductDetail();
						productDetailObj.setProductName(loyaltyDetailList.get(i).getProductName());
						productDetailObj.setProductId(loyaltyDetailList.get(i).getProductId());
						productlst.add(productDetailObj);
					}
					loyaltyInfo = new LoyaltyDetails();
					loyaltyInfo.setProductLst(productlst);
					loyaltyDetailList.get(0).setProductName(null);
					loyaltyDetailList.get(0).setProductId(null);
					loyaltyInfo.setLoyaltyInfo(loyaltyDetailList.get(0));
				}
			}

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return loyaltyInfo;

	}

	/**
	 * Method for fetching Rebate Info. The method is called from the service
	 * layer.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,rebateId in the request.
	 * @return rebateDetails The xml with rebateDetails.
	 * @throws ScanSeeWebSqlException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("unchecked")
	public final RebateDetails getRebateInfo(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchRebateInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RebateDetail> rebateDetailList;

		RebateDetails rebateInfo = null;
		ArrayList<ProductDetail> productlst = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GalleryRebateDetails");
			simpleJdbcCall.returningResultSet("RebateDetails", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final SqlParameterSource fetchRebateParameters = new MapSqlParameterSource().addValue("UserID", clrDetailsObj.getUserId()).addValue(
					"RebateID", clrDetailsObj.getRebateId());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GalleryRebateDetails  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
			else
			{
				rebateDetailList = (List<RebateDetail>) resultFromProcedure.get("RebateDetails");
				if (!rebateDetailList.isEmpty() && rebateDetailList != null)
				{
					productlst = new ArrayList<ProductDetail>();
					for (int i = 0; i < rebateDetailList.size(); i++)
					{
						ProductDetail productDetailObj = new ProductDetail();
						productDetailObj.setProductName(rebateDetailList.get(i).getProductName());
						productDetailObj.setProductId(rebateDetailList.get(i).getProductId());
						productlst.add(productDetailObj);

					}
					rebateInfo = new RebateDetails();
					rebateInfo.setProductLst(productlst);
					rebateDetailList.get(0).setProductName(null);
					rebateDetailList.get(0).setProductId(null);
					rebateInfo.setRebateInfo(rebateDetailList.get(0));

				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return rebateInfo;
	}

	/**
	 * Method for fetching coupon details for particular coupon id. The method
	 * is called from the service layer.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,couponId in the request.
	 * @return couponsDetails returns the coupon details.
	 * @throws ScanSeeWebSqlException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("unchecked")
	public final CouponsDetails fetchCouponDetails(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchCouponDetails in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> couponDetailslst = null;
		CouponsDetails couponInfo = null;
		ArrayList<ProductDetail> productlst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGalleryCouponsDetails");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final SqlParameterSource fetchCouponParameters = new MapSqlParameterSource().addValue("UserID", clrDetailsObj.getUserId())
					.addValue("CouponID", clrDetailsObj.getCouponId()).addValue("CouponListID", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);
			couponDetailslst = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GalleryCouponsDetails  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
			else
			{
				if (!couponDetailslst.isEmpty() && couponDetailslst != null)
				{

					productlst = new ArrayList<ProductDetail>();
					for (int i = 0; i < couponDetailslst.size(); i++)
					{
						ProductDetail productDetailObj = new ProductDetail();
						productDetailObj.setProductName(couponDetailslst.get(i).getProductName());
						productDetailObj.setProductId(couponDetailslst.get(i).getProductId());
						productlst.add(productDetailObj);

					}

					couponInfo = new CouponsDetails();
					couponInfo.setProductLst((ArrayList<ProductDetail>) productlst);
					couponDetailslst.get(0).setProductName(null);
					couponDetailslst.get(0).setProductId(null);
					couponInfo.setCouponInfo(couponDetailslst.get(0));

				}
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in fetchCouponDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		return couponInfo;
	}

	/**
	 * couponAdd is a method for adding the coupon to myCoupon.
	 * 
	 * @param couponId
	 *            contains the id of the coupon to be added.
	 * @param userId
	 *            contains user id.
	 * @return String returns success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public final String addCoupon(Long userId, StringBuffer couponId) throws ScanSeeWebSqlException
	{
		final String methodName = "addCoupon in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{

			LOG.info("Inside Add Coupon for User {}", userId);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerMasterShoppingListAddCoupon");
			final SqlParameterSource addCoupon = new MapSqlParameterSource().addValue("CouponID", couponId).addValue("UserID", userId)
					.addValue("UserCouponClaimTypeID", null).addValue("CouponPayoutMethod", null).addValue("RetailLocationID", null)
					.addValue("CouponClaimDate", Utility.getFormattedDate()).addValue("CouponListID", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addCoupon);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_MasterShoppingListAddCoupon Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String addRebate(Long userId, Integer rebateId) throws ScanSeeWebSqlException
	{

		final String methodName = "addRebate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddRebate");
			final SqlParameterSource addRebate = new MapSqlParameterSource().addValue("UserID", userId)

			.addValue("RebateID", rebateId).addValue("RetailLocationID", null).addValue("RedemptionStatusID", null).addValue("PurchaseAmount", null)
					.addValue("PurchaseDate", Utility.getFormattedDate()).addValue("ProductSerialNumber", null).addValue("ScanImage", null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addRebate);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_MasterShoppingListAddRebate Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			response = ApplicationConstants.FAILURE;
		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public String addLoyalty(Long userId, Integer loyaltyDealId) throws ScanSeeWebSqlException
	{
		final String methodName = "addLoyalty in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = 0;
		String response;
		try
		{
			LOG.info("Inside Add Loyalty");
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddLoyalty");
			final SqlParameterSource addLoyalty = new MapSqlParameterSource().addValue("UserID", userId).addValue("LoyaltyDealID", loyaltyDealId)
					.addValue("LoyaltyDealSource", null).addValue("LoyaltyDealPayoutMethod", null)
					.addValue("LoyaltyDealRedemptionDate", Utility.getFormattedDate());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addLoyalty);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_MasterShoppingListAddLoyalty Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);

		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	public String removeLoyalty(Long userId, Integer loyaltyDealId) throws ScanSeeWebSqlException
	{
		final String methodName = "removeLoyalty in DAO layer";

		String responseFromProc = null;
		Integer fromProc = null;

		LOG.info(ApplicationConstants.METHODSTART + methodName);

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListDeleteLoyalty");
			final SqlParameterSource removeLoyalty = new MapSqlParameterSource().addValue("LoyaltyDealID", loyaltyDealId).addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeLoyalty);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_MasterShoppingListDeleteLoyalty Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeWebSqlException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	public String removeRebate(Long userId, Integer rebateId) throws ScanSeeWebSqlException
	{
		final String methodName = "removeRebate in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListDeleteRebate");
			final SqlParameterSource removeRebate = new MapSqlParameterSource().addValue("RebateID", rebateId).addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeRebate);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_MasterShoppingListDeleteRebate Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Error ocurred in Remove Rebate", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * couponRemove is a method used to remove coupon from myCoupons or
	 * allCoupons.
	 * 
	 * @param userId
	 *            contains user id of the coupon associated with.
	 * @param couponId
	 *            contains coupon's id to be removed.
	 * @return String returns success or failure
	 * @throws ScanSeeWebSqlException
	 */
	public final String removeCoupon(Long userId, StringBuffer couponId) throws ScanSeeWebSqlException

	{

		final String methodName = "removeCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseFromProc = null;
		Integer fromProc = null;

		try
		{
			LOG.info(" Executing the Remove coupon method User ID {}", userId);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.WEBSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerMasterShoppingListDeleteCoupon");
			final SqlParameterSource removeCoupon = new MapSqlParameterSource().addValue("CouponID", couponId).addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeCoupon);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_MasterShoppingListDeleteCoupon method ..errorNum.{} errorMsg.. {}", errorNum, errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseFromProc;

	}

	/**
	 * this method is used to mark the coupon as used coupon.
	 * 
	 * @param userId
	 *            contains user id of the coupon associated with.
	 * @param couponId
	 *            contains coupon's id to be removed.
	 * @return String returns success or failure.
	 * @throws ScanSeeWebSqlException
	 */
	public final String userRedeemCoupon(Long userId, StringBuffer couponId) throws ScanSeeWebSqlException
	{
		final String methodName = "userRedeemCoupon in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerGalleryRedeemCoupon");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, userId);
			fetchCouponParameters.addValue("CouponID", couponId);
			fetchCouponParameters.addValue("UserCouponClaimTypeID", null);
			fetchCouponParameters.addValue("CouponPayoutMethod", null);
			fetchCouponParameters.addValue("RetailLocationID", null);
			fetchCouponParameters.addValue("CouponClaimDate", null);
			fetchCouponParameters.addValue("CouponListID", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;
			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * This DAOImpl method is used to redeem loyalty information based on
	 * userId.
	 * 
	 * @param loyaltyDetailObj
	 *            contains user id,loyalty deal id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */

	public final String userRedeemLoyalty(Long userId, Integer loyaltyDealId) throws ScanSeeWebSqlException
	{
		final String methodName = "userRedeemLoyalty in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		Integer usedFlag = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GalleryRedeemLoyalty");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, userId);
			fetchCouponParameters.addValue("UserClaimTypeID", null);
			fetchCouponParameters.addValue("LoyaltyDealID", loyaltyDealId);
			fetchCouponParameters.addValue("APIPartnerID", null);
			fetchCouponParameters.addValue("LoyaltyDealRedemptionDate", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				fromProc = (Integer) resultFromProcedure.get("Status");
				if (fromProc == 0)
				{
					responseFromProc = ApplicationConstants.SUCCESS;
					usedFlag = (Integer) resultFromProcedure.get("UsedFlag");

				}
				else
				{
					responseFromProc = ApplicationConstants.FAILURE;
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return responseFromProc;
	}

	/**
	 * This DAOImpl method is used to redeem rebate information based on userId.
	 * 
	 * @param rebateDetailObj
	 *            contains user id,rebate id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */

	public String userRedeemRebate(Long userId, Integer rebateId) throws ScanSeeWebSqlException
	{

		final String methodName = "userRedeemRebate in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		Integer usedFlag = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GalleryRedeemRebate");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, userId);
			fetchCouponParameters.addValue("RebateID", rebateId);
			fetchCouponParameters.addValue("RetailLocationID", null);
			fetchCouponParameters.addValue("RedemptionStatusID", null);
			fetchCouponParameters.addValue("PurchaseAmount", null);
			fetchCouponParameters.addValue("PurchaseDate", null);
			fetchCouponParameters.addValue("ProductSerialNumber", null);
			fetchCouponParameters.addValue("ScanImage", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				fromProc = (Integer) resultFromProcedure.get("Status");
				if (fromProc == 0)
				{
					responseFromProc = ApplicationConstants.SUCCESS;
					usedFlag = (Integer) resultFromProcedure.get("UsedFlag");

				}
				else
				{
					responseFromProc = ApplicationConstants.FAILURE;
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}
		return responseFromProc;
	}

	/**
	 * this method is used to share CLR via email.
	 * 
	 * @param shareProductInfoObj
	 * @return String
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public String getCouponShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "getShareCouponInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> couponDetailslst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerCouponShare");
			simpleJdbcCall.returningResultSet("shareCouponInfo", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("CouponID", shareProductInfo.getCouponId());

			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					couponDetailslst = (List<CouponDetails>) resultFromProcedure.get("shareCouponInfo");
					if (!couponDetailslst.isEmpty() && couponDetailslst != null)
					{

						response = Utility.formCouponInfoHTML(couponDetailslst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_HotDealShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches share coupon details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	public String getRebateShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "getShareRebateInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RebateDetail> rebateDetaillst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_RebateShare");
			simpleJdbcCall.returningResultSet("shareRebateInfo", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("rebateId", shareProductInfo.getRebateId());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					rebateDetaillst = (List<RebateDetail>) resultFromProcedure.get("shareRebateInfo");
					if (!rebateDetaillst.isEmpty() && rebateDetaillst != null)
					{
						response = Utility.formRebateInfoHTML(rebateDetaillst);
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_HotDealShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}

		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches share coupon details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	public String getLoyaltyShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "getShareLoyaltyInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<LoyaltyDetail> loyaltyDetaillst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_LoyaltyShare");
			simpleJdbcCall.returningResultSet("shareLoyaltyInfo", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("LoyaltyID", shareProductInfo.getLoyaltyId());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					loyaltyDetaillst = (List<LoyaltyDetail>) resultFromProcedure.get("shareLoyaltyInfo");
					if (!loyaltyDetaillst.isEmpty() && loyaltyDetaillst != null)
					{
						response = Utility.formLoyaltyInfoHTML(loyaltyDetaillst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_LoyaltyShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}

		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for fetching user information for sending mail to share
	 * product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public final String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException
	{
		final String methodName = "getUserInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			response = this.jdbcTemplate.queryForObject(ShoppingListQueries.FETCHUSEREMAILID, new Object[] { shareProductInfo.getUserId() },
					String.class);

		}

		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method Fetching App Configuration details.
	 * 
	 * @return ArrayList<AppConfiguration> .
	 * @throws ScanSeeWebSqlException
	 *             throws if exception occurs.
	 */
	@SuppressWarnings("unchecked")
	public final ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException
	{

		final String methodName = "getStockInfo";

		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_GetScreenContent Store Procedure with error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e);
		}

		return appConfigurationList;
	}

	/**
	 * this method is used to add products to wishList.
	 * 
	 * @param areaInfoVO
	 *            contains userId.
	 * @param productId
	 *            contains product id's to be added to wishList
	 * @return String returns product already exists or added successfully.
	 * @throws ScanSeeWebSqlException
	 */
	public final String addWishListProd(AreaInfoVO areaInfoVO, StringBuffer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "addWishListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		MapSqlParameterSource scanQueryParams = null;

		String responseFromProc = null;
		Integer fromProc = null;
		Integer productExits = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WishListSearchAdd");
			simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
			scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue(ApplicationConstants.USERID, areaInfoVO.getUserID());
			scanQueryParams.addValue("ProductID", productId);
			scanQueryParams.addValue("WishListAddDate", Utility.getFormattedDate());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			fromProc = (Integer) resultFromProcedure.get("Status");

			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;
				productExits = (Integer) resultFromProcedure.get("ProductExists");
				if (productExits != null)
				{
					if (productExits == 1)
					{
						responseFromProc = ApplicationConstants.PRODUCTEXISTS;
					}
					else
					{
						responseFromProc = ApplicationConstants.SUCCESS;
					}
				}
			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in addWishListProd method ..errorNum is..." + errorNum + "errorMsg..is " + errorMsg);
			}

		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	public String discountUserRedeemCLR(Long userId, Integer clrId, String clrFlag) throws ScanSeeWebSqlException
	{
		final String methodName = "userRedeemCoupon in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		Integer usedFlag = null;
		final MapSqlParameterSource fetchCouponParameters;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			if (clrFlag.equalsIgnoreCase("coupon"))
			{
				simpleJdbcCall.withProcedureName("usp_GalleryRedeemCoupon");

				fetchCouponParameters = new MapSqlParameterSource();

				fetchCouponParameters.addValue(ApplicationConstants.USERID, userId);
				fetchCouponParameters.addValue("CouponID", clrId);
				fetchCouponParameters.addValue("UserCouponClaimTypeID", null);
				fetchCouponParameters.addValue("CouponPayoutMethod", null);
				fetchCouponParameters.addValue("RetailLocationID", null);
				fetchCouponParameters.addValue("CouponClaimDate", null);
			}
			else if (clrFlag.equalsIgnoreCase("rebate"))
			{
				simpleJdbcCall.withProcedureName("usp_GalleryRedeemRebate");

				fetchCouponParameters = new MapSqlParameterSource();
				fetchCouponParameters.addValue(ApplicationConstants.USERID, userId);
				fetchCouponParameters.addValue("RebateID", clrId);
				fetchCouponParameters.addValue("RetailLocationID", null);
				fetchCouponParameters.addValue("RedemptionStatusID", null);
				fetchCouponParameters.addValue("PurchaseAmount", null);
				fetchCouponParameters.addValue("PurchaseDate", null);
				fetchCouponParameters.addValue("ProductSerialNumber", null);
				fetchCouponParameters.addValue("ScanImage", null);
			}
			else
			{

				simpleJdbcCall.withProcedureName("usp_GalleryRedeemLoyalty");

				fetchCouponParameters = new MapSqlParameterSource();
				fetchCouponParameters.addValue(ApplicationConstants.USERID, userId);
				fetchCouponParameters.addValue("UserClaimTypeID", null);
				fetchCouponParameters.addValue("LoyaltyDealID", clrId);
				fetchCouponParameters.addValue("APIPartnerID", null);
				fetchCouponParameters.addValue("LoyaltyDealRedemptionDate", null);

			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

				usedFlag = (Integer) resultFromProcedure.get("UsedFlag");

				if (usedFlag != null)
				{
					if (usedFlag == 1)
					{
						responseFromProc = ApplicationConstants.PRODUCTEXISTS;
					}
					else
					{
						responseFromProc = ApplicationConstants.SUCCESS;
					}
				}

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return responseFromProc;
	}

	/**
	 * fetchCouponProductDetails is a method for fetching the products
	 * associated with the coupon.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,couponId,rebateId and loyaltyId.
	 * @return ArrayList<ProductDetail> returns list of product details
	 *         associated with the CLR.
	 * @throws ScanSeeWebSqlException
	 */

	@SuppressWarnings("unchecked")
	public final ArrayList<ProductDetail> fetchCouponProductDetails(CLRDetails clrDetailsObj) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchCouponProductDetails in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> productlst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetCLRProductDetails");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();

			fetchProductDetailsParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchProductDetailsParameters.addValue("CouponID", clrDetailsObj.getCouponId());
			fetchProductDetailsParameters.addValue("LoyaltyID", clrDetailsObj.getLoyaltyDealID());
			fetchProductDetailsParameters.addValue("RebateID", clrDetailsObj.getRebateId());
			fetchProductDetailsParameters.addValue("ErrorNumber", null);
			fetchProductDetailsParameters.addValue("ErrorMessage", null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{
				productlst = (ArrayList<ProductDetail>) resultFromProcedure.get("productDetails");
			}

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GetCLRProductDetails  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);

			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in fetchCouponDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productlst;
	}

	/*
	 * final String methodName = "fetchProductDetails in DAO layer";
	 * LOG.info(ApplicationConstants.METHODSTART + methodName); ProductDetails
	 * productDetails = null; List<ProductDetail> productDetailLst = null; try {
	 * simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
	 * simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
	 * simpleJdbcCall.withProcedureName("usp_GetCLRProductDetails");
	 * simpleJdbcCall.returningResultSet("productDetails", new
	 * BeanPropertyRowMapper<ProductDetail>(ProductDetail.class)); final
	 * MapSqlParameterSource fetchProductDetailsParameters = new
	 * MapSqlParameterSource(); fetchProductDetailsParameters.addValue("UserID",
	 * clrDetailsObj.getUserId());
	 * fetchProductDetailsParameters.addValue("CouponID",
	 * clrDetailsObj.getCouponId());
	 * fetchProductDetailsParameters.addValue("LoyaltyID",
	 * clrDetailsObj.getLoyaltyDealID());
	 * fetchProductDetailsParameters.addValue("RebateID",
	 * clrDetailsObj.getRebateId());
	 * fetchProductDetailsParameters.addValue("ErrorNumber", null);
	 * fetchProductDetailsParameters.addValue("ErrorMessage", null); final
	 * Map<String, Object> resultFromProcedure =
	 * simpleJdbcCall.execute(fetchProductDetailsParameters); if (null !=
	 * resultFromProcedure) { if (null ==
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) {
	 * productDetailLst = (List<ProductDetail>)
	 * resultFromProcedure.get("productDetails"); if (null != productDetailLst
	 * && !productDetailLst.isEmpty()) { productDetails = new ProductDetails();
	 * productDetails.setProductDetail(productDetailLst); final Boolean nextpage
	 * = (Boolean) resultFromProcedure.get("NxtPageFlag"); if (nextpage != null)
	 * { if (nextpage) { productDetails.setNextPage(1); } else {
	 * productDetails.setNextPage(0); } } } else {
	 * LOG.info("No Products found for the search"); return productDetails; } }
	 * else { final String errorMsg = (String)
	 * resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE); final String
	 * errorNum = Integer.toString((Integer)
	 * resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)); LOG.error(
	 * "Error occurred in usp_FetchProductList Store Procedure error number: {} and error message: {}"
	 * , errorNum, errorMsg); throw new ScanSeeException(errorMsg); } } } catch
	 * (DataAccessException exception) {
	 * LOG.error("Exception occurred in scanBarCodeProduct", exception); throw
	 * new ScanSeeException(exception); }
	 * LOG.info(ApplicationConstants.METHODEND + methodName); return
	 * productDetails; }
	 */

	/**
	 * addShoppingListProd is a Method for adding products associated with the
	 * coupon to shopping list.
	 * 
	 * @param areaInfoVO
	 *            contains userId.
	 * @param productId
	 *            contains product id's to be added to wishList
	 * @return String returns product already exists or added successfully.
	 * @throws ScanSeeWebSqlException
	 */

	public final String addShoppingListProd(AreaInfoVO areaInfoVO, StringBuffer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "addShoppingListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		MapSqlParameterSource scanQueryParams = null;

		String responseFromProc = null;
		Integer fromProc = null;
		Integer productExits = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_TodayShoppingListSearchAddProduct");
			simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
			scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue(ApplicationConstants.USERID, areaInfoVO.getUserID());
			scanQueryParams.addValue("ProductID", productId);
			scanQueryParams.addValue("TodayListAddDate", Utility.getFormattedDate());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			fromProc = (Integer) resultFromProcedure.get("Status");

			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;
				productExits = (Integer) resultFromProcedure.get("ProductExists");
				if (productExits != null)
				{
					if (productExits == 1)
					{
						responseFromProc = ApplicationConstants.PRODUCTEXISTS;
					}
					else
					{
						responseFromProc = ApplicationConstants.SUCCESS;
					}
				}
			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_TodayShoppingListSearchAddProduct ..errorNum is..." + errorNum + "errorMsg..is " + errorMsg);
			}

		}
		catch (ParseException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

}

package shopper.dao;

import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.UpdateUserInfo;

public interface ManageSettingsDAO {

	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            - for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */
	UpdateUserInfo fetchUserInfo(int userID) throws ScanSeeWebSqlException;
	
}

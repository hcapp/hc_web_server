package shopper.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.UpdateUserInfo;

public class ManageSettingsDAOImpl implements ManageSettingsDAO {

	/**
	 * To create logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(ManageSettingsDAOImpl.class.getName());

	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedue.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set ParameterizedBeanPropertyRowMapper to map Pojos.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            of DataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */

	@SuppressWarnings("unchecked")
	public UpdateUserInfo fetchUserInfo(int userID) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchUserInfo";
		log.info("In DAO...." + ApplicationConstants.METHODSTART + methodName);
		log.info("userID...." + userID);
		UpdateUserInfo updateUserInfo = null;
		List<UpdateUserInfo> updateUserInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_UserInfoDisplay");
			simpleJdbcCall.returningResultSet("UserInfo", new BeanPropertyRowMapper<UpdateUserInfo>(UpdateUserInfo.class));

			final MapSqlParameterSource fetchUserInfoParameters = new MapSqlParameterSource();
			fetchUserInfoParameters.addValue("UserId", userID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchUserInfoParameters);
			updateUserInfoList = (ArrayList<UpdateUserInfo>) resultFromProcedure.get("UserInfo");
			if (null != updateUserInfoList && !updateUserInfoList.isEmpty())
			{
				updateUserInfo = new UpdateUserInfo();
				updateUserInfo = updateUserInfoList.get(0);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e);
		}
		if (null != updateUserInfo)
		{
			return updateUserInfo;
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return updateUserInfo;
	}

}

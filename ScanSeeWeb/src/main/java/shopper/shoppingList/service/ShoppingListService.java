package shopper.shoppingList.service;

import java.util.ArrayList;

import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.AddSLRequest;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.ShoppingListDetails;
import common.pojo.shopper.ShoppingListRequest;
import common.pojo.shopper.ShoppingListRetailerCategoryList;
import common.pojo.shopper.UserNotesDetails;

/**
 * This interface consists of service layer methods for shopping list.
 * 
 * @author emmanuel_b
 * 
 */
public interface ShoppingListService {

	/**
	 * The service method For fetching product information based on search key
	 * and user Id.
	 * 
	 * @param shoppingListRequest
	 *            as request contains user Id and search key
	 * @return ProductDetails based on success or failure
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */


	ProductDetails scanSearchProductForName(
			ShoppingListRequest shoppingListRequest)
			throws ScanSeeWebSqlException;

	ProductDetails scanSearchProductForName(ShoppingListRequest shoppingListRequest,Integer lowerLimit) throws ScanSeeWebSqlException;


	/**
	 * This is a service Method for adding product to MSL and calls the DAO
	 * methods for adding products from DB.
	 * 
	 * @param
	 * @return String with Success or failure information.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	ProductDetails addProductMainToShopList(
			ShoppingListRequest shoppingListRequest)
			throws ScanSeeWebSqlException;

	/**
	 * This is a Service method for fetching the products from shopping basket
	 * for the given userId.This method validates userId and returns validation
	 * error if validation fails. and calls DAO methods.
	 * 
	 * @param userId
	 *            cart products for the user.
	 * @return String XML with list of Products.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	AddRemoveSBProducts getShopBasketProducts(Long userId)
			throws ScanSeeWebSqlException;

	/**
	 * This service Method for adding today's SL product for the given product
	 * id and user id.
	 * 
	 * @param xml
	 *            -containing the request for External API call .
	 * @return XML containing List of SL products.
	 * @throws ScanSeeException
	 *             -If any exception occurs ScanSeeException will be thrown.
	 */
	ProductDetail addTodaySLProductsBySearch(
			ShoppingListRequest shoppingListRequest)
			throws ScanSeeWebSqlException;

	/**
	 * This is Service method for adds/removes the products to/from Today's
	 * shopping list.This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to add/remove the
	 * products to/from Today Shopping List.
	 * 
	 * @param ShoppingListRequest
	 *            products information.
	 * @return String with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String addRemoveTodaySLProducts(ShoppingListRequest shoppingListRequest)
			throws ScanSeeWebSqlException;

	/**
	 * This is a Service Method for add/remove SB products.This method validates
	 * the input parameters and returns validation error if validation fails.
	 * and calls DAO methods to add/Remove product to/from cart.
	 * 
	 * @param ShoppingListRequest
	 *            products to be added/removed.
	 * @return String with success or failure.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String addRemoveSBProducts(ShoppingListRequest shoppingListRequest)
			throws ScanSeeWebSqlException;

	/**
	 * This is Service method for retrieve User notes for shopping listt.This
	 * method validates the userId and returns validation error if validation
	 * fails.
	 * 
	 * @param userId
	 *            for fetching user notes.
	 * @return String XML contains User notes info.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public UserNotesDetails getUserNotesDetails(Long userId)
			throws ScanSeeWebSqlException;

	/**
	 * This is Service method for add User notes for shopping list.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to add user notes.
	 * 
	 * @param userId
	 *            ,userId .
	 * 
	 * @return String with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String addUserNotes(Long userId, String userNotes)
			throws ScanSeeWebSqlException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. calls DAO methods to removes the products from Cart.
	 * 
	 * @param userID
	 *            requested user.
	 * @param cartLatitude
	 *            location of user.
	 * @param cartLongitude
	 *            location of user.
	 * @return String with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String checkOutFromBasket(Long userID, float cartLatitude,
			float cartLongitude) throws ScanSeeWebSqlException;

	/**
	 * This is a service Method for fetching master shopping list products for
	 * the given input as XML. This method validates the input XML, if it is
	 * valid it will call the DAO method.
	 * 
	 * @param userId
	 *            as request parameter. containing input information need to be
	 *            fetched the MSL products .
	 * @return XML containing master shopping list products in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	ShoppingListDetails getMSLCategoryProducts(Long userId)
			throws ScanSeeWebSqlException;

	
	/**
	 * This is a service Method for fetching shopping list history products for
	 * the given userId. This method validates the input XML, if it is valid it
	 * will call the DAO method.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the Shopping
	 *            list history products .
	 * @return XML containing shopping list history products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ShoppingListRetailerCategoryList fetchSLHistoryItems(Long userId, Integer lowerlimit) throws ScanSeeWebSqlException;

	
	/**
	 * This is a service Method for deleting product from SL. This method
	 * validates the input xml if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be deleted product from
	 *            SL.
	 * @return response: return the whether updated is successful or not.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String deleteProductMainShopListFav(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException;


	
	/**
	 * This is a service Method for adding shopping list history product to List
	 * ,favorites and both for the given userId. This method validates the input
	 * XML, if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing product information needed for adding to
	 *            list,favorites and both.
	 * @return XML containing message saying it added to list or favorites or
	 *         both.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ProductDetails addSLHistoryProdut(AddSLRequest addSLRequest) throws ScanSeeWebSqlException;

	/**
	 * The method used for adding UnassignedProduct.
	 * 
	 * @param ProductDetailsRequest
	 *            The ProductDetailsRequest object.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	public ProductDetails addUnassigedProd(ProductDetailsRequest productDetailsRequest) throws ScanSeeWebSqlException;
	
	/**
	 * The service method for Adding the product to Main Shopping List. This
	 * method validates the userId if it is valid it will call the DAO method
	 * else returns Insufficient Data error message.
	 * 
	 * @param xml
	 *            The XML with Add to Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	public ProductDetails addProductMainToShopList(Long userId,Integer productId) throws ScanSeeWebSqlException;
	/**
	 * 
	 */
	public ArrayList<ProductDetails> categorySmartSearch(String prodSearch) throws ScanSeeWebSqlException;
	/**
	 * 
	 */
	public ArrayList<ProductDetail> productSmartSearch(String prodSearch, String parentCategoryID) throws ScanSeeWebSqlException;
}

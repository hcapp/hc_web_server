/**
 * 
 */
package shopper.shoppingList.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.shoppingList.dao.ShoppingListDAO;
import shopper.shoppingList.helper.ShoppingListHelper;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.AddSLRequest;
import common.pojo.shopper.MainSLRetailerCategories;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.ShoppingListDetails;
import common.pojo.shopper.ShoppingListRequest;
import common.pojo.shopper.ShoppingListRetailerCategoryList;
import common.pojo.shopper.UserNotesDetails;

/**
 * This class is used to display user shopping list items depending on
 * retailers.
 * 
 * @author emmanuel_b
 */
public class ShoppingListServiceImpl implements ShoppingListService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ShoppingListServiceImpl.class);

	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private ShoppingListDAO shoppingListDao;

	/**
	 * sets the ShoppingListDAO DAO.
	 * 
	 * @param shoppingListDao
	 *            The instance for ShoppingListDAO
	 */
	public void setShoppingListDao(ShoppingListDAO shoppingListDao)
	{
		this.shoppingListDao = shoppingListDao;
	}

	/**
	 * The service implementation method for fetching product information by
	 * passing search key. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param shoppingListRequest
	 *            the request shoppingListRequest.
	 * @return response ProductDetails .
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ProductDetails scanSearchProductForName(ShoppingListRequest shoppingListRequest, Integer lowerLimit) throws ScanSeeWebSqlException
	{
		final String methodName = "scanForProdName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ProductDetails productDetails = shoppingListDao.scanForProductName(shoppingListRequest, lowerLimit);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * The service method for Adding the product to Main Shopping List. This
	 * method validates the userId if it is valid it will call the DAO method
	 * else returns Insufficient Data error message.
	 * 
	 * @param xml
	 *            The XML with Add to Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	public ProductDetails addProductMainToShopList(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException
	{
		final String methodName = "addProductMainToShopList of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ProductDetails userProdIdList = shoppingListDao.addProductMainToShopList(shoppingListRequest);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userProdIdList;
	}

	/**
	 * This is a Service method for fetching the products from shopping basket
	 * for the given userId.This method validates userId and returns validation
	 * error if validation fails. and calls DAO methods.
	 * 
	 * @param userId
	 *            cart products for the user.
	 * @return with list of Products.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public AddRemoveSBProducts getShopBasketProducts(Long userId) throws ScanSeeWebSqlException
	{

		final String methodName = "getShopBasketProducts of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		AddRemoveSBProducts addRemoveSBProducts = null;
		if (null == userId)
		{
			LOG.info("Validation failed because User Id is not available");
		}
		else
		{
			addRemoveSBProducts = shoppingListDao.getShopBasketProducts(userId);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return addRemoveSBProducts;
	}

	/**
	 * This service Method for adding today's SL product for the given product
	 * id and user id.
	 * 
	 * @param xml
	 *            -containing the request for External API call .
	 * @return XML containing List of SL products.
	 * @throws ScanSeeException
	 *             -If any exception occurs ScanSeeException will be thrown.
	 */
	public ProductDetail addTodaySLProductsBySearch(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException
	{
		final String methodName = "addTodaySLProductsBySearch of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail prodObj = new ProductDetail();
		if (null == shoppingListRequest.getUserId())
		{
			LOG.info("Validation failed User Id is not available");
		}
		else
		{
			prodObj = shoppingListDao.addTodaySLProductsBySearch(shoppingListRequest);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return prodObj;
	}

	/**
	 * This is Service method for adds/removes the products to/from Today's
	 * shopping list.This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to add/remove the
	 * products to/from Today Shopping List.
	 * 
	 * @param xml
	 *            contains products information.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String addRemoveTodaySLProducts(ShoppingListRequest ShoppingList) throws ScanSeeWebSqlException
	{

		final String methodName = "addRemoveTodaySLProducts of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == ShoppingList.getUserProductID() && null == ShoppingList.getProductDetails())
		{
			LOG.info("Validation failed because UserProductID is not available");
			response = ApplicationConstants.FAILURE;
		}
		else
		{
			response = shoppingListDao.addRemoveTodaySLProducts(ShoppingList);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a Service Method for add/remove SB products.This method validates
	 * the input parameters and returns validation error if validation fails.
	 * and calls DAO methods to add/Remove product to/from cart.
	 * 
	 * @param ShoppingList
	 *            products to be added/removed.
	 * @return String with success or failure.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String addRemoveSBProducts(ShoppingListRequest ShoppingList) throws ScanSeeWebSqlException
	{

		final String methodName = "addRemoveSBProducts of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		response = shoppingListDao.addRemoveSBProducts(ShoppingList);
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is Service method for retrieve User notes for shopping listt.This
	 * method validates the userId and returns validation error if validation
	 * fails.
	 * 
	 * @param userId
	 *            for fetching user notes.
	 * @return String XML contains User notes info.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public UserNotesDetails getUserNotesDetails(Long userId) throws ScanSeeWebSqlException
	{

		final String methodName = " getUserNotesDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final UserNotesDetails userNotesDetails = shoppingListDao.getUserNotesDetails(userId);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userNotesDetails;
	}

	/**
	 * This is Service method for add User notes for shopping list.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to add user notes.
	 * 
	 * @param xml
	 *            contains user notes details.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String addUserNotes(Long userId, String userNotes) throws ScanSeeWebSqlException
	{

		final String methodName = " addUserNotes of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		response = shoppingListDao.addUserNotesDetails(userId, userNotes);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method validates the input parameters and returns validation error
	 * if validation fails. calls DAO methods to removes the products from Cart.
	 * 
	 * @param userID
	 *            The userID entered by User.
	 * @param cartLatitude
	 *            The Latitude value(required for entry in
	 *            UserShoppingCartHistoryProduct table).
	 * @param cartLongitude
	 *            The Longitude value (required for entry in
	 *            UserShoppingCartHistoryProduct table).
	 * @return String with success or error code based on success or failure of
	 *         operation.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 */

	public String checkOutFromBasket(Long userID, float cartLatitude, float cartLongitude) throws ScanSeeWebSqlException
	{
		final String methodName = " checkOutFromBasket of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userID)
		{
			LOG.info("Validation failed because User Id is not available");
		}
		else
		{
			response = shoppingListDao.checkOutFromBasket(userID, cartLatitude, cartLongitude);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a service Method for fetching master shopping list products for
	 * the given input as userId. This method validates the input userId, if it
	 * is valid it will call the DAO method.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the MSL
	 *            products .
	 * @return ShoppingListDetails containing master shopping list products in
	 *         the response.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("static-access")
	public ShoppingListDetails getMSLCategoryProducts(Long userId) throws ScanSeeWebSqlException
	{
		final String methodName = "getMSLProductslst of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ShoppingListHelper shoppingListHelper = new ShoppingListHelper();
		ShoppingListDetails shoppingListDetails = new ShoppingListDetails();

		if (userId == null)
		{
			LOG.info("Request does not contain");

		}
		else
		{
			ArrayList<ProductDetail> ProductDetailList = null;
			ProductDetailList = shoppingListDao.getMSLCategoryProcuctDetails(userId);

			if (null != ProductDetailList && !ProductDetailList.isEmpty())
			{

				shoppingListDetails = shoppingListHelper.getShoppingListCategoryList(ProductDetailList);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return shoppingListDetails;
	}

	/**
	 * This is a service Method for fetching shopping list history products for
	 * the given userId. This method validates the input XML, if it is valid it
	 * will call the DAO method.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the Shopping
	 *            list history products .
	 * @return XML containing shopping list history products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ShoppingListRetailerCategoryList fetchSLHistoryItems(Long userId, Integer lowerlimit) throws ScanSeeWebSqlException
	{
		final String methodName = "fetchSLHistoryItems of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ShoppingListRetailerCategoryList shoppingListCategoryInfo = null;
		if (lowerlimit == null)
		{
			lowerlimit = 0;
		}
		MainSLRetailerCategories mainSLRetailerCategories = null;
		mainSLRetailerCategories = shoppingListDao.fetchSLHistoryProducts(userId, lowerlimit);
		if (null != mainSLRetailerCategories)
		{
			final ShoppingListHelper shoppingListHelper = new ShoppingListHelper();
			shoppingListCategoryInfo = shoppingListHelper.fetchSLHistoryCategoryDisplay(mainSLRetailerCategories.getMainSLRetailerCategory());
			shoppingListCategoryInfo.setTotalSize(mainSLRetailerCategories.getTotalSize());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return shoppingListCategoryInfo;
	}

	/**
	 * The method for removing to Shopping List product. This method validates
	 * the userId if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param xml
	 *            The XML with delete from Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	public String deleteProductMainShopListFav(ShoppingListRequest ShoppingList) throws ScanSeeWebSqlException
	{
		final String methodName = "dleteProductMainShopList of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		if (null != ShoppingList)
		{

			if (null == ShoppingList.getUserId())
			{
				LOG.info("Validation failed because User Id is not available");

			}
			else if (ShoppingList.getUserProductID() != null)
			{
				response = shoppingListDao.deleteProductMainShopListFav(ShoppingList);

			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a service Method for adding shopping list history product to List
	 * ,favorites and both for the given userId. This method validates the input
	 * XML, if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing product information needed for adding to
	 *            list,favorites and both.
	 * @return XML containing message saying it added to list or favorites or
	 *         both.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ProductDetails addSLHistoryProdut(AddSLRequest addSLRequest) throws ScanSeeWebSqlException
	{
		final String methodName = "addSLHistoryProdut of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetails productDetailsObj = new ProductDetails();
		productDetailsObj = shoppingListDao.addSLHistoryProductInfo(addSLRequest);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetailsObj;

	}

	public ProductDetails scanSearchProductForName(ShoppingListRequest shoppingListRequest) throws ScanSeeWebSqlException
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * The method used for adding UnassignedProduct.
	 * 
	 * @param ProductDetailsRequest
	 *            The ProductDetailsRequest object.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeWebSqlException
	 *             The exception defined for the application.
	 */

	public ProductDetails addUnassigedProd(ProductDetailsRequest productDetailsRequest) throws ScanSeeWebSqlException
	{

		final String methodName = "addUnassigedProd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ProductDetails userProdIdList = shoppingListDao.addUnassignedPro(productDetailsRequest);
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return userProdIdList;

	}

	/**
	 * The service method for Adding the product to Main Shopping List. This
	 * method validates the userId if it is valid it will call the DAO method
	 * else returns Insufficient Data error message.
	 * 
	 * @param xml
	 *            The XML with Add to Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	public ProductDetails addProductMainToShopList(Long userId, Integer productId) throws ScanSeeWebSqlException
	{
		final String methodName = "addProductMainToShopList of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetails userProdIdList = new ProductDetails();
		try
		{
			userProdIdList = shoppingListDao.addProductMainToShopList(userId, productId);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userProdIdList;
	}

	public ArrayList<ProductDetails> categorySmartSearch(String prodSearch) throws ScanSeeWebSqlException
	{
		ArrayList<ProductDetails> productList = null;
		try
		{
			productList = shoppingListDao.categorySmartSearch(prodSearch);

		}
		catch (ScanSeeWebSqlException e)
		{

		}
		return productList;
	}

	public ArrayList<ProductDetail> productSmartSearch(String prodSearch, String parentCategoryID) throws ScanSeeWebSqlException
	{
		ArrayList<ProductDetail> productList = null;
		try
		{
			productList = shoppingListDao.productSmartSearch(prodSearch, parentCategoryID);

		}
		catch (ScanSeeWebSqlException e)
		{

		}
		return productList;
	}
}

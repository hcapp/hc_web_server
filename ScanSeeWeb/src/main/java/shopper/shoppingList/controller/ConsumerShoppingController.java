package shopper.shoppingList.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import shopper.find.service.FindService;
import shopper.service.CommonService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.Users;
import common.pojo.shopper.AddRemoveSBProducts;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is used to handle consumer shopping list module functionalities.
 * 
 * @author shyamsundara_hm
 */
@Controller
public class ConsumerShoppingController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ConsumerShoppingController.class);

	/**
	 * This method is used to display user added products.
	 * 
	 * @param request
	 *            contains the user id.
	 * @param session
	 *            session.
	 * @param model
	 *            model.
	 * @return list of products.
	 */

	@RequestMapping(value = "/consshoplisthome.htm", method = RequestMethod.GET)
	public String consShoppinglistHome(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "consShoppinglistHome";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return "consshopphome";

	}

	/**
	 * 
	 */
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "/consdisplayslprod.htm", method = RequestMethod.GET)
	public ModelAndView consDisplaySLProductsGet(@ModelAttribute("ShoppingListHome") ProductDetail productDetail, HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		final String strMethodName = "consDisplaySLProducts";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ConsumerRequestDetails objConsumerRequestDetails = null;
		int intRecordCount = 20;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "shoplist";
		int intPageSize = 0;
		String strPageNumber = "0";
		String strViewName = null;
		String screenName = null;
		String strRedirectUrl = "consdisplayfavprods.htm";
		AddRemoveSBProducts objAddRemoveSBProducts = null;
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{
			session.removeAttribute("shopplistprods");
			session.removeAttribute("consumerReqObj");
			session.removeAttribute("shopcartcnt");
			
			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber)
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, strFrmSL, null);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objAddRemoveSBProducts = objCommonService.getConsShopLstProds(objConsumerRequestDetails);

			if (objAddRemoveSBProducts.getCartProducts() != null)
			{
				session.setAttribute("shopplistprods", objAddRemoveSBProducts);
				Pagination objPagination = Utility.getPagination(objAddRemoveSBProducts.getMaxCount(), intCurrentPage,
						"/ScanSeeWeb/shopper/consdisplayslprod.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);
				//session.setAttribute("shopcartcnt", objAddRemoveSBProducts.getMaxShopCartCnt());
				

			}
			session.setAttribute("shopcartcnt", objAddRemoveSBProducts.getMaxShopCartCnt());
		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		if(null != productDetail && !"".equals(productDetail))
		{
			strViewName= productDetail.getScreenName();
		}
		
		if(null != strViewName && !"".equals(strViewName))
		{
			if(strViewName.equals("search"))
			{
				strRedirectUrl = "conssearchhome.htm";
			}else if(strViewName.equals("history"))
			{
				strRedirectUrl="consslhistory.htm";
			}else{
				strRedirectUrl="consdisplayfavprods.htm";
			}
		}
		
		return new ModelAndView(new RedirectView("/ScanSeeWeb/shopper/"+strRedirectUrl));

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/consdisplayslprod.htm", method = RequestMethod.POST)
	public ModelAndView consDisplaySLProducts(@ModelAttribute("ShoppingListHome") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model)
	{
		final String strMethodName = "consDisplaySLProducts";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ConsumerRequestDetails objConsumerRequestDetails = null;
		int intRecordCount = 20;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "shoplist";
		int intPageSize = 0;
		String strPageNumber = "0";
		String strViewName = null;
		String strRedirectUrl = "consdisplayfavprods.htm";
		
		AddRemoveSBProducts objAddRemoveSBProducts = null;
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{

			if(null != productDetail && !"".equals(productDetail))
			{
				intRecordCount = productDetail.getRecordCount();
				strViewName = productDetail.getScreenName();
			}
			
			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber && !"0".equals(strPageNumber))
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, strFrmSL, null);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objAddRemoveSBProducts = objCommonService.getConsShopLstProds(objConsumerRequestDetails);

			if (objAddRemoveSBProducts.getCartProducts() != null)
			{
				session.setAttribute("shopplistprods", objAddRemoveSBProducts);
				Pagination objPagination = Utility.getPagination(objAddRemoveSBProducts.getMaxCount(), intCurrentPage,
						"/ScanSeeWeb/shopper/consdisplayslprod.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);

			}
			
			if(null != strViewName && !"".equals(strViewName))
			{
				if(strViewName.equals("search"))
				{
					strRedirectUrl = "conssearchhome.htm";
				}else if(strViewName.equals("history"))
				{
					strRedirectUrl="consslhistory.htm";
				}else{
					strRedirectUrl="consdisplayfavprods.htm";
				}
			}
			
			
		
		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(new RedirectView("/ScanSeeWeb/shopper/"+strRedirectUrl));
	}

	@RequestMapping(value = "/consslhistory.htm", method = RequestMethod.GET)
	public ModelAndView consDisplayHistoryProdsGet(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		final String strMethodName = "consDisplayHistoryProdsGet";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ConsumerRequestDetails objConsumerRequestDetails = null;
		int intRecordCount = 60;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "shoplist";
		int intPageSize = 0;
		String strPageNumber = "0";
		String strViewName = "consshopphistory";
		AddRemoveSBProducts objAddRemoveSBProducts = null;
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{
			session.removeAttribute("slhistoryprods");
			session.removeAttribute("consumerReqObj");

			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			/*final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber)
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}*/
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, strFrmSL, null);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objAddRemoveSBProducts = objCommonService.getConsSLHistoryProds(objConsumerRequestDetails);

			if (objAddRemoveSBProducts != null)
			{
				session.setAttribute("slhistoryprods", objAddRemoveSBProducts);
				/*Pagination objPagination = Utility.getPagination(objAddRemoveSBProducts.getMaxCount(), intCurrentPage,
						"/ScanSeeWeb/shopper/consdisplayslprod.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);*/

			}

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "/consdisplayfavprods.htm", method = RequestMethod.GET)
	public String consDisplayFavoriteProds(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		final String strMethodName = "consDisplayFavoriteProds";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ConsumerRequestDetails objConsumerRequestDetails = null;
		int intRecordCount = 60;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "shoplist";
		int intPageSize = 0;

		String strPageNumber = "0";
		String strViewName = "consshopphome";
		AddRemoveSBProducts objAddRemoveSBProducts = null;
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{
			session.removeAttribute("slfavoriteprods");
			session.removeAttribute("consumerReqObj");

			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			/*final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber)
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}*/
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, strFrmSL, null);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objAddRemoveSBProducts = objCommonService.getConsSLFavoriteProds(objConsumerRequestDetails);

			if (objAddRemoveSBProducts != null)
			{
				session.setAttribute("slfavoriteprods", objAddRemoveSBProducts);
				/*Pagination objPagination = Utility.getPagination(objAddRemoveSBProducts.getMaxCount(), intCurrentPage,
						"/ScanSeeWeb/shopper/consdisplayslprod.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);
*/
			}

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return strViewName;
	}

	@RequestMapping(value = "/consslsearchprods.htm", method = RequestMethod.POST)
	public ModelAndView consSLSearchProd(@ModelAttribute("ShoppingListHome") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model)
	{
		final String strMethodName = "consDisplayFavoriteProds";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		ConsumerRequestDetails objConsumerRequestDetails = null;
		int intRecordCount = 60;
		int intMainMenuId = 0;
		int intLowerLimit = 0;
		int intCurrentPage = 1;
		int intNumber = 0;
		String strFrmSL = "shoplist";

		int intPageSize = 0;
		String strPageNumber = "0";
		String strSearchKey = null;
		ProductDetails objProductDetails = null;
		String strViewName = "consshoppsearch";
		final ServletContext objServletContext = request.getSession().getServletContext();
		final WebApplicationContext objWebApplicationContext = WebApplicationContextUtils.getWebApplicationContext(objServletContext);
		final CommonService objCommonService = (CommonService) objWebApplicationContext.getBean(ApplicationConstants.COMMONSERVICE);
		final FindService objFindService = (FindService) objWebApplicationContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		try
		{
			session.removeAttribute("slsearchprods");
			session.removeAttribute("consumerReqObj");

			if (null != productDetail)
			{
				strSearchKey = productDetail.getSearchkey();

			}
			final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			Long userId = null;
			String consumerType = null;
			if (objUsers != null)
			{
				userId = objUsers.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;
			}

			// For fetching main menu id.
			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, null);
			intMainMenuId = objFindService.getMainMenuID(objConsumerRequestDetails);

			// For pagination
			/*final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				strPageNumber = request.getParameter("pageNumber");
				Pagination objPagination = (Pagination) session.getAttribute("pagination");

				if (null != strPageNumber)
				{
					intCurrentPage = Integer.valueOf(strPageNumber);
					intNumber = intCurrentPage - 1;
					intPageSize = objPagination.getPageRange();
					intLowerLimit = intPageSize * intNumber;
				}

			}*/
			// For setting request parameters.

			objConsumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, intMainMenuId, null, intLowerLimit,
					ApplicationConstants.SHOPPINGLISTMODULE, intRecordCount, strFrmSL, strSearchKey);
			session.setAttribute("consumerReqObj", objConsumerRequestDetails);

			objProductDetails = objCommonService.getConsShopSearchProd(objConsumerRequestDetails);

			if (objProductDetails != null)
			{
				session.setAttribute("slsearchprods", objProductDetails);
				/*Pagination objPagination = Utility.getPagination(objProductDetails.getTotalSize(), intCurrentPage,
						"/ScanSeeWeb/shopper/consdisplayslprod.htm", intRecordCount);
				session.setAttribute("pagination", objPagination);*/
				
			}else{
				request.setAttribute("norecord","true");
			}
			request.setAttribute("prodsearchkey", objConsumerRequestDetails.getSearchKey());

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURED + strMethodName + exception.getMessage());
		}

		model.put("ShoppingListHome", productDetail);
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return new ModelAndView(strViewName);
	}

	@RequestMapping(value = "/conssladdprod.htm", method = RequestMethod.POST)
	public ModelAndView consAddProdToShopList(@ModelAttribute("ShoppingListHome") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model)
	{
		final String methodName = "consAddProdToShopList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail productDetailsObj = null;
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strViewName = "consshopphome";
		productDetailsObj = (ProductDetail) session.getAttribute(ApplicationConstants.CONSPRODDETALS);
		try
		{

			final Users loginUser = (Users) session.getAttribute("loginuser");

			if (loginUser != null)

			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					productDetailsObj.setUserId(loginUserId);
				}

				productDetailsObj = commonServiceObj.consAddProdToShopp(productDetailsObj);

				if (productDetailsObj.isProductExists() != null)
				{
					if (productDetailsObj.isProductExists() == ApplicationConstants.TRUE)
					{
						request.setAttribute("addedtoshop", ApplicationConstants.CONSPRODUCTEXISTS);
					}
					else
					{
						request.setAttribute("addedtoshop", ApplicationConstants.CONSPRODUCTADDED);
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consAddProdToShopList", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView(viewName);
	}

	@RequestMapping(value = "/conssearchhome.htm", method = RequestMethod.GET)
	public ModelAndView consShopListSearchHome(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
	{
		final String methodName = "consShopListSearchHome";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = "consshoppsearch";
		session.removeAttribute("slsearchprods");
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView(viewName);
	}

	@RequestMapping(value = "/consfavaddprod.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consAddFavProdToShopList(@RequestParam(value = "userproductId", required = true) String userProductId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "consAddProdToShopList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdExists = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					objProductDetails.setUserId(loginUserId);
				}

				if (null != userProductId)
				{
					objProductDetails.setUsrProdId(userProductId);

				}
				objProductDetails = commonServiceObj.consAddFavProdToShopList(objProductDetails);

				if (objProductDetails.isProductExists() != null)
				{
					if (objProductDetails.isProductExists() == ApplicationConstants.TRUE)
					{
						strProdExists = ApplicationConstants.SLADDPRODTLEXISTS;
					}
					else
					{
						strProdExists = ApplicationConstants.CONSPRODUCTADDED;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consAddProdToShopList", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdExists;
	}

	@RequestMapping(value = "/consfavdeleteprod.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consDeleteFavProd(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "consDeleteFavProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdDelResponse = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					objProductDetails.setUserId(loginUserId);
				}

				if (null != productId)
				{
					objProductDetails.setProductIds(productId);

				}
				objProductDetails = commonServiceObj.consFavDeleteProd(objProductDetails);

				if (objProductDetails.getResponse() != null)
				{
					if (objProductDetails.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						strProdDelResponse = ApplicationConstants.DELETEPRODUCTTEXT;
					}
					else
					{
						strProdDelResponse = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consAddProdToShopList", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdDelResponse;
	}

	@RequestMapping(value = "/conshistoryaddprod.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consAddHistoryProdToSL(@RequestParam(value = "usrProdId", required = true) String usrProdId,
			@RequestParam(value = "addTo", required = true) String addTo, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{
		final String methodName = "consAddHistoryProdToSL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdExists = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					objProductDetails.setUserId(loginUserId);
				}

				if (null != usrProdId)
				{
					objProductDetails.setUsrProdId(usrProdId);

				}
				if (null != addTo)
				{
					objProductDetails.setAddTo(addTo);
				}
				objProductDetails = commonServiceObj.consAddHistoryProdToSL(objProductDetails);

				if (objProductDetails.getResponseFlag() != null)
				{
					strProdExists = objProductDetails.getResponseFlag();

				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consAddHistoryProdToSL", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdExists;
	}

	@RequestMapping(value = "/conssearchaddprod.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consAddSearchProdToListandFav(@RequestParam(value = "productId", required = true) String productId,
			@RequestParam(value = "addTo", required = true) String addTo, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{
		final String methodName = "consAddProdToShopList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdExists = null;
		Integer intMainMenuId = null;
		String strAdded = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					objProductDetails.setUserId(loginUserId);
				}

				if (null != productId)
				{
					objProductDetails.setProductIds(productId);

				}
				if (null != addTo && !"".equals(addTo))
				{
					objProductDetails.setAddTo(addTo);
					strAdded = addTo;
					
				}
				objProductDetails = commonServiceObj.consAddSearchProdToSL(objProductDetails);

				if (objProductDetails.isProductExists() != null)
				{
					if (objProductDetails.isProductExists() == ApplicationConstants.TRUE)
					{
						if(strAdded.equals("List"))
						{
							strProdExists = ApplicationConstants.SLADDPRODTLEXISTS;
						}else if(strAdded.equals("Favorites")){
							strProdExists = ApplicationConstants.FAVADDPRODTLEXISTS;
						}else
						{
							strProdExists = ApplicationConstants.SLHISTORYPRODUCTEXISTINLISTANDFAVORITES;
						}
										
					}else 
						{
						if(strAdded.equals("List"))
						{
							strProdExists = ApplicationConstants.CONSADDPRODTOSHOPPLIST;
						}else if(strAdded.equals("Favorites")){
							strProdExists = ApplicationConstants.CONSADDPRODTOFAVORITES;
						}else
						{
							strProdExists = ApplicationConstants.ADDEDTOLISTFAVORITES;
						}
					
							
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consAddProdToShopList", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdExists;
	}

	/*@RequestMapping(value = "/constodaylstdeleteprod.htm", method = RequestMethod.POST)
	public ModelAndView consDeleteTodayListProd(@ModelAttribute("ShoppingListHome") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model)*/
	
	@RequestMapping(value = "/constodaylstdeleteprod", method = RequestMethod.GET)
	public @ResponseBody
	String consDeleteTodayListProd(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "consDeleteFavProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdDelResponse = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					objProductDetails.setUserId(loginUserId);
				}

				if (null != productId && !"".equals(productId))
				{
					objProductDetails.setProductIds(productId);
					
				}
				objProductDetails = commonServiceObj.consDeleteTodayListProd(objProductDetails);

				if (objProductDetails.getResponse() != null)
				{
					if (objProductDetails.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						strProdDelResponse = ApplicationConstants.DELETEPRODUCTTEXT;
					}
					else
					{
						strProdDelResponse = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consDeleteTodayListProd", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		
		return strProdDelResponse;
	}

	@RequestMapping(value = "/conscheckoutshoplst.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consCheckOutTodayListProd(@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "conscheckouttodaylst";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdDelResponse = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					objProductDetails.setUserId(loginUserId);
				}

				objProductDetails = commonServiceObj.consCheckOutTodayListProd(objProductDetails);

				if (objProductDetails.getResponse() != null)
				{
					if (objProductDetails.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						strProdDelResponse = ApplicationConstants.SHOPPINGLISTCHECKOUTTEXT;
					}
					else
					{
						strProdDelResponse = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consDeleteTodayListProd", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdDelResponse;
	}

	@RequestMapping(value = "/consmovetobasket.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consMoveToBasketProds(@RequestParam(value = "usrproductid", required = true) String usrproductid, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "consMoveToBasketProds";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail objProductDetails = new ProductDetail();
		Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		String strProdDelResponse = null;
		Integer intMainMenuId = null;
		ConsumerRequestDetails objConsumerRequestDetails = null;
		try
		{

			objConsumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if (null != objConsumerRequestDetails)
			{
				intMainMenuId = objConsumerRequestDetails.getMainMenuId();
				objProductDetails.setMainMenuID(intMainMenuId);
			}
			final Users loginUser = (Users) session.getAttribute("loginuser");
			if (loginUser != null)
			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					objProductDetails.setUserId(loginUserId);
				}

				if (null != usrproductid && !"".equals(usrproductid))
				{
					objProductDetails.setUsrProdId(usrproductid);

				}

				objProductDetails = commonServiceObj.consMoveToBasketProds(objProductDetails);

				if (objProductDetails.getResponse() != null)
				{
					if (objProductDetails.getResponse().equals(ApplicationConstants.SUCCESS))
					{
						strProdDelResponse = ApplicationConstants.DELETEPRODUCTTEXT;
					}
					else
					{
						strProdDelResponse = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in consMoveToBasketProds", exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strProdDelResponse;
	}
	
	
	@RequestMapping(value = "/consslprintprod.htm", method = RequestMethod.GET)
	public final ModelAndView showConsShopPrintInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws ScanSeeServiceException
	{
		final String methodName = "showConsProdInfoPrint";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService objCommonService = (CommonService) appContext.getBean("commonService");
		final Users objUsers = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		Long userId = null;	
		Integer intRecordCount = null;
		AddRemoveSBProducts objAddRemoveSBProducts = null;
			ConsumerRequestDetails objConsumerRequestDetails = null;
		if (objUsers != null && !"".equals(objUsers))
		{
			userId = objUsers.getUserID();
		}
		try
		{
			
			String strCount = (String) request.getAttribute("recordCount");
			if(null !=strCount && !"".equals(strCount))
			{
				intRecordCount = Integer.valueOf(strCount);
			}
			
			objConsumerRequestDetails=	(ConsumerRequestDetails) session.getAttribute("consumerReqObj");
			if(null == objConsumerRequestDetails || "".equals(objConsumerRequestDetails))
			{
				objConsumerRequestDetails.setUserId(userId);
				objConsumerRequestDetails.setRecordCount(intRecordCount);
			}
			objAddRemoveSBProducts = objCommonService.getConsShopLstProds(objConsumerRequestDetails);

			if (objAddRemoveSBProducts.getCartProducts() != null)
			{
				session.setAttribute("shopplistprods", objAddRemoveSBProducts);
											
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		return new ModelAndView("consslprintprod");
	}

}

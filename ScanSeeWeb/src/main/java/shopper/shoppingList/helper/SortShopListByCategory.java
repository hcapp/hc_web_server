package shopper.shoppingList.helper;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.pojo.shopper.Category;

public class SortShopListByCategory implements Comparator<Category>
{
	/**
	 * Logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(SortShopListByCategory.class.getName());
	
	/**
	 * This method is to compare two products.
	 * @param objHDCategoryInfo1
	 *          -As parameter
	 * @param objHDCategoryInfo2
	 *          -As parameter 
	 * @return iCategory                 
	 */
	public int compare(Category objHDCategoryInfo1, Category objHDCategoryInfo2)
	{
		String strCategory1 = null;
		String strCategory2 = null;
		int iCategory = 0;
		try {
			strCategory1 = objHDCategoryInfo1.getParentCategoryName();
			strCategory2 = objHDCategoryInfo2.getParentCategoryName();
			iCategory = strCategory1.compareTo(strCategory2);
		} catch (Exception exception) {
			log.info("exception in SortShopListByCategory " + exception);
		}
		return iCategory;
	}
	



}

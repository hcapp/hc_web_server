package shopper.find.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.find.FindHelper;
import shopper.find.service.FindService;
import shopper.service.CommonService;

import com.scansee.externalapi.common.exception.ScanSeeExternalAPIException;
import com.scansee.externalapi.common.findnearby.pojos.FindNearByIntactResponse;
import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.Offer;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.SearchForm;
import common.pojo.Users;
import common.pojo.shopper.CouponDetail;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.GoogleCategory;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.IpCityResponse;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.UserRatingInfo;
import common.pojo.shopper.UserTrackingData;
import common.tags.Pagination;
import common.util.IpInfoDbClient;
import common.util.Utility;


/**
 * This class is used for find module functionalities like product search,deals
 * search and location search implementation.
 * 
 * @author shyamsundara_hm
 */

@Controller
public class ConsumerFindController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ConsumerFindController.class);

	/**
	 * This class is used for find module home display, it contains the
	 * functionalities like displaying the business categories.
	 * 
	 * @param request
	 *            containing module identifier.
	 * @param session
	 *            containing module identifier.
	 * @param model
	 *            containing module identifier.
	 * @return list of find categories.
	 */
	@RequestMapping(value = "/consfindhome.htm", method = RequestMethod.GET)
	public String consFindHome(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "ConsFindHome";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Long userId = null;
		List<GoogleCategory> categoryInfo = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FindService objFindService = (FindService) appContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
		final SearchForm searchForm = new SearchForm();

		final String searchType = (String) request.getParameter(ApplicationConstants.SEARCHTYPETEXT);
		session.removeAttribute(ApplicationConstants.PAGINATION);
		session.removeAttribute(ApplicationConstants.PAGINATIONLOC);
		String requestIpAddr = null;
		IpCityResponse ipCityResponse = null;
		ConsumerRequestDetails consumerRequestDetails = null;
		// Below code is used to select search option based on the type
		final String showHeaderText = ApplicationConstants.SHOWHEADERSEARCH;

		request.setAttribute(showHeaderText, ApplicationConstants.SHOWTEXT);
		try
		{

			categoryInfo = objFindService.getCategoryInfo(userId);
			if (searchType != null)
			{

				if ("Products".equals(searchType))
				{
					request.setAttribute(ApplicationConstants.SEARCHTYPETEXT, "Products");
				}
				else if ("Locations".equals(searchType))
				{
					request.setAttribute("searchType", "Locations");

				}
				else if ("Deals".equals(searchType))
				{
					request.setAttribute("searchType", "Deals");
				}

				consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute(ApplicationConstants.CONSUMERREQOBJ);

				if (null != consumerRequestDetails)
				{
					searchForm.setZipCode(consumerRequestDetails.getPostalCode());
					searchForm.setSearchKey(consumerRequestDetails.getSearchKey());
					searchForm.setCategoryName(consumerRequestDetails.getCategoryName());
					request.setAttribute("searchCategory", consumerRequestDetails.getCategoryName());
				}
			}

			else
			{
				if (loginUser != null)
				{
					userId = loginUser.getUserID();
					searchForm.setZipCode(loginUser.getPostalCode());
					consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute(ApplicationConstants.CONSUMERREQOBJ);
					consumerRequestDetails.setModuleName(ApplicationConstants.FINDMODULE);
					consumerRequestDetails.setConsumerType(ApplicationConstants.LOGINUSER);
					session.setAttribute(ApplicationConstants.CONSUMERREQOBJ, consumerRequestDetails);
				}
				else
				{

					consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerIpDetail");

					if (null == consumerRequestDetails)
					{

						requestIpAddr = request.getHeader("X-FORWARDED-FOR");
						if (requestIpAddr == null)
						{
							consumerRequestDetails = new ConsumerRequestDetails();
							consumerRequestDetails.setModuleName(ApplicationConstants.FINDMODULE);
							consumerRequestDetails.setConsumerType(ApplicationConstants.GUESTUSER);

							requestIpAddr = request.getRemoteAddr();
							// requestIpAddr = "199.36.142.82";
							ipCityResponse = IpInfoDbClient.getIpInfo(requestIpAddr);
							if (null != ipCityResponse)
							{
								if (ipCityResponse.getZipCode() != null && !"-".equals(ipCityResponse.getZipCode()))
								{

									searchForm.setZipCode(ipCityResponse.getZipCode());
									consumerRequestDetails.setPostalCode(ipCityResponse.getZipCode());
								}
								else
								{
									consumerRequestDetails.setPostalCode(null);

								}
								if (null != ipCityResponse.getLatitude() && !"0".equals(ipCityResponse.getLatitude()))
								{
									consumerRequestDetails.setLatitute(ipCityResponse.getLatitude());
								}
								else
								{
									consumerRequestDetails.setLatitute(null);
								}
								if (null != ipCityResponse.getLongitude() && !"0".equals(ipCityResponse.getLongitude()))
								{
									consumerRequestDetails.setLongitude(ipCityResponse.getLongitude());
								}
								else
								{
									consumerRequestDetails.setLongitude(null);
								}
								session.setAttribute("consumerIpDetail", consumerRequestDetails);
							}

							session.setAttribute(ApplicationConstants.CONSUMERREQOBJ, consumerRequestDetails);
						}

					}
					else
					{
						consumerRequestDetails.setModuleName(ApplicationConstants.FINDMODULE);
						consumerRequestDetails.setConsumerType(ApplicationConstants.GUESTUSER);
						session.setAttribute(ApplicationConstants.CONSUMERREQOBJ, consumerRequestDetails);
						if (consumerRequestDetails.getPostalCode() != null)
						{

							searchForm.setZipCode(consumerRequestDetails.getPostalCode());
						}

					}
				}
			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Error occureed in ConsFindHome", e.getMessage());

		}
		catch (IOException e)
		{
			LOG.error("Error occureed in ConsFindHome", e.getMessage());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		request.setAttribute("categoryInfo", categoryInfo);
		model.addAttribute("SearchForm", searchForm);
		return "consfindhome";

	}

	/**
	 * This method is used to search product based on user entered search key.
	 * It returns the product details like name,image,it based on search key.
	 * 
	 * @param searchForm
	 *            contains the search string and zip code.
	 * @param request
	 *            contains the search key.
	 * @param session
	 *            for storing the product search results.
	 * @param model
	 *            model
	 * @return list of products
	 * @throws ScanSeeServiceException
	 * @throws ScanSeeWebSqlException
	 */

	@RequestMapping(value = "/consfindprodsearch.htm", method = RequestMethod.GET)
	public ModelAndView consProductSearchGet(@ModelAttribute("SearchForm") SearchForm searchForm, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{
		final String methodName = "ConsProductSearchGet";

		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final String viewName = "consfindprodlst";
		ProductDetails productDetailsObj = null;

		productDetailsObj = (ProductDetails) session.getAttribute(ApplicationConstants.SEARCHRESULTS);

		session.setAttribute("searchresults", productDetailsObj);
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used to display product details with respect to matching
	 * search string.
	 * 
	 * @param searchForm
	 *            contains the search string and zip code.
	 * @param request
	 *            contains the search string.
	 * @param session
	 *            to store search results.
	 * @param model
	 *            model.
	 * @return list of products based on search string.
	 */
	@RequestMapping(value = "/consfindprodsearch.htm", method = RequestMethod.POST)
	public ModelAndView consProductSearch(@ModelAttribute("SearchForm") SearchForm searchForm, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{
		final String methodName = "ConsProductSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		int mainMenuId = 0;
		ConsumerRequestDetails consumerRequestDetails = null;
		int lowerLimit = 0;
		int recordCount = 20;
		String searchKey = null;
		Long userId = null;
		String consumerType = null;
		int currentPage = 1;
		String pageNumber = "0";
		Pagination objPage = null;
		final List<ProductDetail> prodslst = new ArrayList<ProductDetail>();
		final String viewName = "consfindprodlst";
		Integer intProdCount = 0;
		SearchForm objForm = null;
		String userEmailId = null;
		request.setAttribute(ApplicationConstants.SHOWHEADERSEARCH, ApplicationConstants.SHOWTEXT);
		if (searchForm.getSearchKey() == null)
		{
			searchKey = (String) session.getAttribute(ApplicationConstants.SEARCHKEY);
			searchForm.setSearchKey(searchKey);

		}

		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);

		if (null != searchForm.getRecordCount() && !"".equals(searchForm.getRecordCount()) && !"undefined".equals(searchForm.getRecordCount()))
		{
			recordCount = searchForm.getRecordCount();
		}
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final FindService findService = (FindService) appContext.getBean(ApplicationConstants.FINDSERVICEBEAN);
		final CommonService commonServiceObj = (CommonService) appContext.getBean(ApplicationConstants.COMMONSERVICE);
		// for fetching main menu id from database.
		try
		{
			if (null != loginUser)
			{
				userId = loginUser.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
				// if user id is not null , then fetching email id from
				// database.
				userEmailId = commonServiceObj.getConsUserEmailId(userId);
				session.setAttribute(ApplicationConstants.USEREMAIID, userEmailId);
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}

			consumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, mainMenuId, searchForm.getZipCode(), searchForm.getSearchKey(),
					lowerLimit, ApplicationConstants.FINDMODULE, recordCount, null);
			mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			consumerRequestDetails.setMainMenuId(mainMenuId);
			session.setAttribute(ApplicationConstants.CONSUMERREQOBJ, consumerRequestDetails);
			searchForm.setMainMenuID(mainMenuId);

			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && ApplicationConstants.TRUETEXT.equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute(ApplicationConstants.SEARCHFORM);
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);

				if (pageNumber != null)
				{
					if (Integer.valueOf(pageNumber) != 0)
					{
						currentPage = Integer.valueOf(pageNumber);

					}
					lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));
					searchForm.setLastVisitedNo(lowerLimit);
				}
			}
			else
			{
				searchKey = searchForm.getSearchKey();
			}

			if (objForm == null)
			{
				objForm = new SearchForm();
				objForm.setSearchKey(searchForm.getSearchKey());
				objForm.setZipCode(searchForm.getZipCode());
				session.setAttribute(ApplicationConstants.SEARCHFORM, objForm);
			}
			searchForm.setRecordCount(recordCount);
			consumerRequestDetails.setSearchKey(searchKey);

			// for fetching the product details like product name,image etc.
			final ProductDetails productDetailsObj = findService.searchProducts(searchForm);

			if (productDetailsObj != null)
			{
				// if product have multiple images , then we are splitting
				// images of product and adding into list.
				final List<ProductDetail> prodList = productDetailsObj.getProductDetail();
				ProductDetail prodD = null;
				List<String> imagelst = null;
				String[] items = null;
				String image = null;
				for (int j = 0; j < prodList.size(); j++)
				{
					prodD = prodList.get(j);
					items = prodD.getProductImagePath().split(ApplicationConstants.COMMA);
					imagelst = new ArrayList<String>();

					for (int i = 0; i < items.length; i++)
					{
						image = items[i];
						imagelst.add(image);
					}
					prodD.setImagelst(imagelst);
					prodslst.add(prodD);
				}
				productDetailsObj.setProductDetail(prodslst);
				intProdCount = productDetailsObj.getTotalSize();
				session.setAttribute(ApplicationConstants.TOTALRESULTS, intProdCount);

				request.setAttribute("breadCrumbTxt", String.valueOf(objForm.getSearchKey() == null ? "" : objForm.getSearchKey()) + " (Showing "
						+ intProdCount + " results)");

				objPage = Utility.getPagination(intProdCount, currentPage, "/ScanSeeWeb/shopper/consfindprodsearch.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}

			session.setAttribute(ApplicationConstants.SEARCHFORM, searchForm);
			session.setAttribute(ApplicationConstants.SEARCHKEY, searchKey);
			session.setAttribute("searchresults", productDetailsObj);
			session.setAttribute(ApplicationConstants.ZIPCODE, searchForm.getZipCode());
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Error occureed in ConsProductSearch", e.getMessage());

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Error occureed in ConsProductSearch", e.getMessage());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView(viewName);

	}

	/**
	 * This method is used to display product details like name,image,audio
	 * ,video and attributes.
	 * 
	 * @param productDetail
	 *            contains the product details.
	 * @param request
	 *            contains the product Id.
	 * @param response
	 *            contains the product details.
	 * @param session
	 *            to store product details.
	 * @param model
	 *            model.
	 * @return product details.
	 * @throws ScanSeeWebSqlException
	 * @throws ScanSeeServiceException
	 */
	@SuppressWarnings("null")
	@RequestMapping(value = "/consfindprodinfo.htm", method = RequestMethod.GET)
	public ModelAndView consProdSummary(ProductDetail productDetail, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model)
	{
		final String methodName = "ConsProdSummary";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		request.setAttribute("displaySaleProdSummy", "ProductSummary");
		int mainMenuId = 0;
		ConsumerRequestDetails consumerRequestDetails = null;
		final String landingPage = request.getParameter("page");
		String viewName = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean(ApplicationConstants.COMMONSERVICE);
		ProductDetail productDetailsObj = null;
		final ProductDetails prodMediaAttriInfoObj = new ProductDetails();
		List<ProductDetail> productAttributesList = null;
		List<ProductDetail> productMediaInfoList = null;
		String zipCode = null;
		String productId = null;
		String productlstId = null;
		String userEmailId = null;
		String consumerType = null;
		String emailFlag = null;
		Long userId = null;
		request.setAttribute(ApplicationConstants.SHOWHEADERSEARCH, ApplicationConstants.SHOWTEXT);
		zipCode = (String) session.getAttribute(ApplicationConstants.ZIPCODE);
		String breadCrumLink = null;
		if (zipCode != null)
		{
			productDetail.setZipcode(zipCode);
		}

		emailFlag = request.getParameter("share");

		if (emailFlag == null)
		{

			emailFlag = "No";
		}
		request.setAttribute("shareEmailFlag", emailFlag);
		// key1 is ProductId
		productId = request.getParameter("key1");
		if (productId != null && !"".equals(productId))
		{
			productDetail.setProductId(Integer.parseInt(productId));
		}
		// key2 is ProductListId
		productlstId = request.getParameter("key2");
		if (productlstId != null && !"".equals(productlstId))
		{
			productDetail.setProductListID(Integer.valueOf(productlstId));
		}
		
		breadCrumLink = request.getParameter("page");
		if(breadCrumLink !=null)
		{
		request.setAttribute("breadCrumLink", breadCrumLink);
		}
		try
		{
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			if (null != loginUser)
			{
				userId = loginUser.getUserID();
				productDetail.setUserId(userId);
				consumerType = ApplicationConstants.LOGINUSER;
				userEmailId = commonServiceObj.getConsUserEmailId(loginUser.getUserID());
				session.setAttribute(ApplicationConstants.USEREMAIID, userEmailId);
			}
			else
			{
				productDetail.setUserId(null);
				consumerType = ApplicationConstants.GUESTUSER;
			}

			FindService findService = (FindService) appContext.getBean(ApplicationConstants.FINDSERVICEBEAN);

			consumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, mainMenuId, zipCode, null, null,
					ApplicationConstants.FINDMODULE, null, null);

			mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			consumerRequestDetails.setMainMenuId(mainMenuId);
		//	session.setAttribute(ApplicationConstants.CONSUMERREQOBJ, consumerRequestDetails);
			productDetail.setMainMenuID(mainMenuId);
			// for fetching the product details.
			productDetailsObj = commonServiceObj.getConsumerProdMultipleImages(productDetail);

			if (productDetailsObj != null)
			{

				productDetailsObj.setMainMenuID(mainMenuId);
				productDetailsObj.setProductListID(productDetail.getProductListID());
				// for displaying the product multiple images.
				final ArrayList<String> imagelst = new ArrayList<String>();
				final String[] items = productDetailsObj.getProductImagePath().split(",");
				String image = null;
				for (int i = 0; i < items.length; i++)
				{

					image = items[i];

					imagelst.add(image);

				}
				productDetailsObj.setImagelst(imagelst);

				// for fetching the product media information.
				productMediaInfoList = commonServiceObj.fetchConsumerProductMediaInfo(productDetail);

				if (productMediaInfoList != null && !productMediaInfoList.isEmpty())
				{
					prodMediaAttriInfoObj.setProdMediaList(productMediaInfoList);

				}

				// for fetching the product attributes list.
				productAttributesList = commonServiceObj.fetchConsumerProductAttributes(productDetail);

				if (productAttributesList != null && !productAttributesList.isEmpty())
				{
					prodMediaAttriInfoObj.setProdAttributeList(productAttributesList);
				}

				if (productDetailsObj.getScanCode() != null)
				{
					productDetail.setScanCode(productDetailsObj.getScanCode());
				}

				session.setAttribute(ApplicationConstants.CONSPRODDETALS, productDetailsObj);
				session.setAttribute("prodmediaattriinfo", prodMediaAttriInfoObj);
			}

			// for calling product online store details .

			// productDetail
			consProdOnlineStoreInfo(productDetail, request, response, model, session);
			consProdNearByRetailer(productDetail, request, response, model, session);

			// for fetching user ratings on product.
			consGetProductRatings(productDetail, request, session);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Error occureed in ConsProdSummary", e.getMessage());

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Error occureed in ConsProdSummary", e.getMessage());

		}

		if (landingPage != null && "location".equals(landingPage))
		{

			final SearchForm searchFormModelObj = (SearchForm) session.getAttribute("findLocSearchResultProdSummry");
			model.addAttribute("findLocSearchResultForm", searchFormModelObj);
			viewName = "consfindresultloclistsales";
		}
		else
		{

			viewName = "consfindprodinfo";
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used to display the hot deals based on user entered search
	 * key and zip code.
	 * 
	 * @param searchForm
	 *            contains deals search key.
	 * @param request
	 *            contains the search key to search.
	 * @param session
	 *            session.
	 * @param model
	 *            model.
	 * @return list of deals based on search key.
	 * @throws ScanSeeServiceException
	 *             ScanSeeServiceException.
	 * @throws ScanSeeWebSqlException
	 *             ScanSeeWebSqlException.
	 */

	@RequestMapping(value = "/consdealssearch.htm", method = RequestMethod.POST)
	public ModelAndView consFindDealsSearch(@ModelAttribute("SearchForm") SearchForm searchForm, HttpServletRequest request, HttpSession session,
			ModelMap model) throws ScanSeeServiceException, ScanSeeWebSqlException
	{

		final String methodName = "inside ConsFindDealsSearch ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		Integer mainMenuId = 0;
		ConsumerRequestDetails consumerRequestDetails = null;
		Integer lowerLimit = 0;
		int recordCount = 20;
		Long userId = null;
		String consumerType = null;
		int currentPage = 1;
		String pageNumber = "0";

		Pagination objPage = null;
		String userEmailId = null;
		String viewName = "consdealssearch";
		Integer intProdCount = 0;
		Integer totalDealsSize = 0;
		HotDealsListResultSet hotDealsListResultSetObj = null;
		SearchForm objForm = null;
		String searchKey = null;
		String zipcode = null;
		request.setAttribute("showHeaderSearch", "show");
		session.removeAttribute("dealsearhresults");
		session.removeAttribute("userEmailId");
		session.removeAttribute("totalresults");
		// session.removeAttribute("searchKey");

		if (searchForm.getSearchKey() == null)
		{
			searchKey = (String) session.getAttribute("searchKey");
			searchForm.setSearchKey(searchKey);

		}
		else
		{
			session.removeAttribute("searchKey");
		}
		if (searchForm.getZipCode() == null)
		{
			zipcode = (String) session.getAttribute("zipcode");
			searchForm.setZipCode(zipcode);

		}
		else
		{
			session.removeAttribute("zipcode");
		}

		final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);

		if (null != searchForm.getRecordCount() && !"".equals(searchForm.getRecordCount()) && !"undefined".equals(searchForm.getRecordCount()))
		{
			recordCount = searchForm.getRecordCount();
		}
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		FindService findService = (FindService) appContext.getBean("findService");
		CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		// for fetching main menu id from database.
		try
		{
			if (null != loginUser)
			{
				userId = loginUser.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
				// if user id is not null , then fetching email id from
				// database.
				userEmailId = commonServiceObj.getConsUserEmailId(userId);
				session.setAttribute("userEmailId", userEmailId);
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}

			consumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, mainMenuId, searchForm.getZipCode(), searchKey, lowerLimit,
					ApplicationConstants.FINDMODULE, recordCount, null);
			mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			searchForm.setMainMenuID(mainMenuId);
			consumerRequestDetails.setMainMenuId(mainMenuId);
			session.setAttribute("consumerReqObj", consumerRequestDetails);
			searchForm.setMainMenuID(mainMenuId);
			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);

				if (pageNumber != null)
				{
					if (Integer.valueOf(pageNumber) != 0)
					{
						currentPage = Integer.valueOf(pageNumber);

					}
					lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));
					searchForm.setLastVisitedNo(lowerLimit);
				}
			}

			else
			{
				searchKey = searchForm.getSearchKey();
				zipcode = searchForm.getZipCode();
			}

			if (objForm == null)
			{
				objForm = new SearchForm();
				objForm.setSearchKey(searchForm.getSearchKey());
				objForm.setZipCode(searchForm.getZipCode());
				session.setAttribute("searchForm", objForm);
			}
			searchForm.setRecordCount(recordCount);
			// for fetching the deal details like deal name,image ,short and
			// long description.
			hotDealsListResultSetObj = findService.findDealsSearch(searchForm);

			// Grouping the deals based on category.
			if (hotDealsListResultSetObj != null)
			{
				totalDealsSize = hotDealsListResultSetObj.getTotalSize();
				hotDealsListResultSetObj = FindHelper.getHotDealsList(hotDealsListResultSetObj.getHotDealsListResponselst());

			}

			if (null != hotDealsListResultSetObj)
			{
				objPage = Utility.getPagination(totalDealsSize, currentPage, "/ScanSeeWeb/shopper/consdealssearch.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
			}
			request.setAttribute("breadCrumbTxt", String.valueOf(objForm.getSearchKey() == null ? "" : objForm.getSearchKey()) + " (Showing "
					+ intProdCount + " results)");
			if (hotDealsListResultSetObj != null)
			{
				// for()

				session.setAttribute("dealsearhresults", hotDealsListResultSetObj);
				intProdCount = hotDealsListResultSetObj.getTotalSize();
				session.setAttribute("totalresults", totalDealsSize);
			}
			else
			{
				request.setAttribute("message", "No deals found.");
				request.setAttribute("noproductfound", "true");
			}

			session.setAttribute("searchForm", searchForm);
			session.setAttribute("searchKey", searchKey);
			session.setAttribute("zipcode", zipcode);

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Error occureed in ConsFindDealsSearchch", e.getMessage());

		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated cat block
			LOG.error("Error occureed in ConsFindDealsSearchch", e.getMessage());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return new ModelAndView(viewName);

	}

	/**
	 * This method is used to display the hot deals based on user entered search
	 * key and zipcode.
	 * 
	 * @param searchForm
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/consdealssearch.htm", method = RequestMethod.GET)
	public ModelAndView ConsFindDealsSearchGet(@ModelAttribute("SearchForm") SearchForm searchForm, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{
		final String methodName = "ConsFindDealsSearchGet";

		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = "consdealssearch";
		HotDealsListResultSet hotDealsListResultSetObj = null;

		hotDealsListResultSetObj = (HotDealsListResultSet) session.getAttribute("dealsearhresults");

		session.setAttribute("dealsearhresults", hotDealsListResultSetObj);
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used to display hot deal details like deal name,sale price
	 * ,short and long description grouped by category name.
	 * 
	 * @param hotDealsListRequestObj
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 * @throws ScanSeeServiceException
	 * @throws ScanSeeWebSqlException
	 */

	@RequestMapping(value = "/conshddetails", method = RequestMethod.GET)
	public ModelAndView ConsHotDealDetails(HotDealsListRequest hotDealsListRequestObj, HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "ConsHotDealDetails";
		HotDealsDetails hotDealsDetailsObj = null;
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long loginUserId = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		request.setAttribute("showHeaderSearch", "show");
		request.setAttribute("displaySaleProdSummy", "HotDeal");
		String breadCrumLink = null;

		try
		{
			String hotdealId = request.getParameter("hotdealId");
			String hotdealLstId = request.getParameter("hdlstid");

			breadCrumLink = request.getParameter("page");
			request.setAttribute("breadCrumLink", breadCrumLink);
			if (hotdealId != null)
			{
				hotDealsListRequestObj.setHotDealId(Integer.valueOf(hotdealId));
			}

			if (hotdealLstId != null && !"".equals(hotdealLstId))
			{
				hotDealsListRequestObj.setHotDealListID(hotdealLstId);
			}

			if (loginUser != null)

			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					hotDealsListRequestObj.setUserId(loginUserId);

				}
			}

			/*
			 * FindService findService = (FindService)
			 * appContext.getBean("findService"); consumerRequestDetails = new
			 * ConsumerRequestDetails(loginUserId, consumerType, mainMenuId,
			 * null, null, null, ApplicationConstants.FINDMODULE, null, null);
			 * mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			 * consumerRequestDetails.setMainMenuId(mainMenuId);
			 * session.setAttribute("consumerReqObj", consumerRequestDetails);
			 */

			hotDealsDetailsObj = commonServiceObj.getConsumerHdProdInfo(hotDealsListRequestObj);
			if (hotDealsDetailsObj != null)
			{
				session.setAttribute("hotDealsDetailsObj", hotDealsDetailsObj);
			}

		}
		catch (ScanSeeServiceException e)
		{

			LOG.error("Error occureed in ConsHotDealDetails", e.getMessage());

		}
		/*
		 * catch (ScanSeeServiceException e) {
		 * LOG.error("Error occureed in ConsHotDealDetails", e.getMessage()); }
		 */

		if (breadCrumLink != null && "SpclHotDeal".equals(breadCrumLink))
		{

			final SearchForm searchFormModelObj = (SearchForm) session.getAttribute("findLocSearchResultProdSummry");
			model.addAttribute("findLocSearchResultForm", searchFormModelObj);
			viewName = "consfindresultloclistsales";
		}
		else
		{

			viewName = "conshotdealinfo";
		}
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used to display product near by retailers.
	 * 
	 * @param productDetailObj
	 *            contains the product id.
	 * @param request
	 *            contains the productId.
	 * @param response
	 *            response.
	 * @param model
	 *            model.
	 * @param session
	 *            session.
	 */

	public static void consProdNearByRetailer(ProductDetail productDetailObj, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session)
	{

		final String methodName = "ConsProdNearByRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		FindNearByIntactResponse findNearByIntactRespObj = null;
		Integer defaultRadius = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		session.removeAttribute("findNearByDetails");
		try
		{
			defaultRadius = commonServiceObj.getDefaultRadius();
			if (defaultRadius != null)
			{
				productDetailObj.setRadius(defaultRadius);
			}

			final FindNearByDetails findNearByDetails = commonServiceObj.getConsFindNearByRetailer(productDetailObj);
			if (findNearByDetails != null)
			{
				session.setAttribute("findNearByDetails", findNearByDetails);

			}
			// If scansee find near by retailer is not there , call retailgence
			// api for find near by retailers.

			else
			{
				if (defaultRadius != null)
				{
					productDetailObj.setRadius(defaultRadius);
				}

				findNearByIntactRespObj = commonServiceObj.getConsFindRetailersExternalAPI(productDetailObj);
				session.setAttribute("ExtApifindNearByDetails", findNearByIntactRespObj);

			}

			if (findNearByDetails == null && findNearByIntactRespObj == null)
			{
				session.setAttribute("message", "Near by details are not available for the product.");
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.error("Error occureed in ConsProdNearByRetailer", e.getMessage());

		}
		catch (ScanSeeExternalAPIException e)
		{
			LOG.error("Error occureed in ConsProdNearByRetailer", e.getMessage());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

	}

	/**
	 * This method is used to display product online stores.
	 * 
	 * @param productDetail
	 *            contains the productId.
	 * @param request
	 *            request.
	 * @param response
	 *            response.
	 * @param model
	 *            model.
	 * @param session
	 *            session.
	 */
	public static void consProdOnlineStoreInfo(ProductDetail productDetail, HttpServletRequest request, HttpServletResponse response, ModelMap model,
			HttpSession session)
	{

		LOG.info("Entering to the  ConsProdOnlineStoreInfo......");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		session.removeAttribute("OnlineStoresList");
		session.removeAttribute("CJOnlineStores");
		ExternalAPIInformation apiInfo = null;
		List<Offer> onlineStoresList = null;
		OnlineStoresRequest onlineStores = null;
		OnlineStoresService onlineStoresService = null;
		OnlineStores onlineStoresinfo = null;

		try
		{
			// for fetching external API shopzilla input parameters from
			// database.

			apiInfo = commonServiceObj.getConsExternalApiInfo(ApplicationConstants.CONSFINDONLINESTORE);
			onlineStoresService = new OnlineStoresServiceImpl();
			onlineStores = new OnlineStoresRequest();
			onlineStores.setPageStart("0");
			onlineStores.setUpcCode(productDetail.getScanCode());
			onlineStores.setFromWeb(true);
			if (productDetail.getUserId() != null)
			{
				onlineStores.setUserId(String.valueOf(productDetail.getUserId()));
			}
			else
			{
				onlineStores.setUserId("0");
			}
			onlineStores.setValues();
			onlineStoresinfo = onlineStoresService.onlineStoresInfoEmailTemplate(apiInfo, onlineStores);

			if (onlineStoresinfo != null)
			{
				if (!onlineStoresinfo.getOffers().isEmpty())
				{
					onlineStoresList = onlineStoresinfo.getOffers();
					session.setAttribute("OnlineStoresList", onlineStoresList);

				}

			}

			/**
			 * For commission junction data integration...
			 */

			final CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
			ArrayList<RetailerDetail> commiJuncDatalst = null;

			commiJuncDatalst = shoppingListService.getConsumerCommisJunctionData(productDetail);

			if (null != commiJuncDatalst && !commiJuncDatalst.isEmpty())
			{
				session.setAttribute("CJOnlineStores", commiJuncDatalst);

			}

		}
		catch (ScanSeeServiceException exception)
		{

			LOG.error("Error occureed in ConsProdOnlineStoreInfo ", exception.getMessage());

		}
		catch (NumberFormatException exception)
		{
			LOG.error("Error occureed in ConsProdOnlineStoreInfo : ", exception.getMessage());

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in ConsProdOnlineStoreInfo", exception.getMessage());
		}
		LOG.info("ShopperProfileController :: Inside Exit Post Method");

	}

	/**
	 * This method is used to fetch product reviews.
	 * 
	 * @param productDetail
	 *            contains the product details.
	 * @param request
	 *            contains the product id.
	 * @param session
	 *            session.
	 * @param model
	 *            model.
	 * @return product reviews of product id.
	 */
	@SuppressWarnings("null")
	@RequestMapping(value = "/consprodreview.htm", method = RequestMethod.GET)
	public ModelAndView consProdReviews(@ModelAttribute("prodsummaryform") ProductDetail productDetail, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		final String methodName = "ConsProdReviews inside method";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail productDetailsObj = null;
		request.setAttribute("showHeaderSearch", "show");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		List<ProductDetail> productReviewLst = null;
		Long loginUserId = null;
		String consumerType = null;
		Integer mainMenuId = 0;
		ConsumerRequestDetails consumerRequestDetails = null;

		productDetailsObj = (ProductDetail) session.getAttribute(ApplicationConstants.CONSPRODDETALS);
		try
		{
			session.removeAttribute("prodreviews");
			// for fetching the product details.
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			if (loginUser != null)

			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					consumerType = ApplicationConstants.LOGINUSER;
				}
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}

			final FindService findService = (FindService) appContext.getBean("findService");

			consumerRequestDetails = new ConsumerRequestDetails(loginUserId, consumerType, mainMenuId, null, null, null,
					ApplicationConstants.FINDMODULE, null, null);
			mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			consumerRequestDetails.setMainMenuId(mainMenuId);
			session.setAttribute("consumerReqObj", consumerRequestDetails);

			productReviewLst = commonServiceObj.fetchConsumerProductReviews(productDetailsObj);

			if (productReviewLst != null && !productReviewLst.isEmpty())
			{
				session.setAttribute("prodreviews", productReviewLst);
			}
			else
			{
				request.setAttribute("message", "No Reviews found.");
				request.setAttribute("noreviewfound", "true");
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in ConsProdReviews", exception.getMessage());
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error("Error occureed in ConsProdReviews", exception.getMessage());
		}

		viewName = "consprodreview";
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used to fetch product associated coupons.
	 * 
	 * @param productDetail
	 *            contains the product information.
	 * @param request
	 *            contains the product id.
	 * @param session
	 *            session.
	 * @param model
	 *            model.
	 * @return product associated coupons.
	 */

	@RequestMapping(value = "/consprodcoupon.htm", method = RequestMethod.GET)
	public ModelAndView consProdCoupons(@ModelAttribute("prodsummaryform") ProductDetail productDetail, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		final String methodName = "ConsProdCoupons inside method";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail productDetailsObj = null;
		request.setAttribute("showHeaderSearch", "show");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		List<CouponDetail> productCouponList = null;
		final Long loginUserId = null;
		final String consumerType = null;
		Integer mainMenuId = 0;
		ConsumerRequestDetails consumerRequestDetails = null;

		productDetailsObj = (ProductDetail) session.getAttribute(ApplicationConstants.CONSPRODDETALS);
		try
		{
			session.removeAttribute("prodcoupons");

			final FindService findService = (FindService) appContext.getBean("findService");

			consumerRequestDetails = new ConsumerRequestDetails(loginUserId, consumerType, mainMenuId, null, null, null,
					ApplicationConstants.FINDMODULE, null, null);
			mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			consumerRequestDetails.setMainMenuId(mainMenuId);
			session.setAttribute("consumerReqObj", consumerRequestDetails);

			productCouponList = commonServiceObj.fetchConsumerProductCoupons(productDetailsObj);

			if (productCouponList != null && !productCouponList.isEmpty())
			{
				session.setAttribute("prodcoupons", productCouponList);
			}
			else
			{
				request.setAttribute("message", "No Coupons found.");
				request.setAttribute("noreviewfound", "true");
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in ConsProdCoupons", exception.getMessage());
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error("Error occureed in ConsProdCoupons", exception.getMessage());
		}

		viewName = "consprodcoupon";
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(viewName);
	}

	/**
	 * This method is used to adding product to shopping list.
	 * 
	 * @param productDetail
	 *            contains the product details.
	 * @param request
	 *            request.
	 * @param response
	 *            containing the success or failure string.
	 * @param session
	 *            session.
	 * @param model
	 *            model.
	 * @return success or failure depending upon database flag.
	 */

	/*@RequestMapping(value = "/consaddprodtoshopp.htm", method = RequestMethod.GET)
	public ModelAndView consAddProdToSL(@ModelAttribute("prodsummaryform") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model)*/
	@RequestMapping(value = "/consaddprodtoshopp.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consAddProdToSL(HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "ConsAddProdToSL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = "consdisplayslprod";
		ProductDetail productDetailsObj = null;
			Long loginUserId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		final String feedURL = null;
		Integer productId = null;
		String strResponse = null;
		productDetailsObj = (ProductDetail) session.getAttribute(ApplicationConstants.CONSPRODDETALS);
		try
		{

			final Users loginUser = (Users) session.getAttribute("loginuser");
			ProductDetail	productDetail = new ProductDetail();
			if (loginUser != null)

			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					productDetail.setUserId(loginUserId);
				}

				if(null != productDetailsObj && !"".equals(productDetailsObj))
				{
					
					productId= productDetailsObj.getProductId();
					if(null != productId && !"".equals(productId))
					{
						productDetail.setProductIds(String.valueOf(productId));
						
					}
				}
				
				productDetailsObj = commonServiceObj.consAddProdToShopp(productDetail);

				if (productDetailsObj.isProductExists() != null)
				{
					if (productDetailsObj.isProductExists() == ApplicationConstants.TRUE)
					{
						strResponse=ApplicationConstants.DUPLICATESLPRODUCTTEXT;
					}
					else
					{
						strResponse=ApplicationConstants.CONSADDPRODTOSHOPPLIST;
					}
				}
			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in ConsAddProdToSL", exception.getMessage());
		}

		
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return strResponse;
	}

	/**
	 * This method is used to adding product to wish list.
	 * 
	 * @param productDetail
	 *            contains the product details.
	 * @param request
	 *            contains the product id.
	 * @param session
	 *            session.
	 * @param model
	 *            model.
	 * @return success or failure.
	 */
/*	@RequestMapping(value = "/consaddprodtowishlist.htm", method = RequestMethod.GET)
	public ModelAndView consAddProdToWL(@ModelAttribute("prodsummaryform") ProductDetail productDetail, HttpServletRequest request,
			HttpSession session, ModelMap model)*/
	@RequestMapping(value = "/consaddprodtowishlist.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consAddProdToWL(HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		final String methodName = "ConsAddProdToWL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ProductDetail productDetailsObj = null;
		Long loginUserId = null;
		String strResponse = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		
		productDetailsObj = (ProductDetail) session.getAttribute("consproddetails");
		try
		{
			final Users loginUser = (Users) session.getAttribute("loginuser");
		
			if (loginUser != null)

			{
				if (loginUser.getUserID() != null)
				{
					loginUserId = loginUser.getUserID();
					// Integer.
					productDetailsObj.setUserId(loginUserId);
				}
							
				productDetailsObj = commonServiceObj.consAddProdToWishList(productDetailsObj);

				if (productDetailsObj.isProductExists() != null)
				{
					if (productDetailsObj.isProductExists() == ApplicationConstants.TRUE)
					{
					strResponse =	 ApplicationConstants.DUPLICATPRODUCTTEXT;
						
					}
					else
					{
						strResponse= ApplicationConstants.CONSWLPRODUCTADDED;
						
					}
				}

			}

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error("Error occureed in ConsAddProdToWL", exception.getMessage());
		}

		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This method is used to share product information via Email.
	 * 
	 * @param toEmailId
	 *            to send product information.
	 * @param fromEmailId
	 *            to share product information.
	 * @param productId
	 *            used to fetch product details.
	 * @param productListId
	 *            is used for user tracking.
	 * @param request
	 *            contains the product id.
	 * @param response
	 *            response.
	 * @param session
	 *            session.
	 * @return string contain the success of failure.
	 */

	@RequestMapping(value = "/shareprodinfoviaemail.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consSendShareProdInfoMail(@RequestParam(value = "toEmailId", required = true) String toEmailId,
			@RequestParam(value = "fromEmailId", required = true) String fromEmailId,
			@RequestParam(value = "productId", required = true) Integer productId,
			@RequestParam(value = "productListId", required = true) Integer productListId,

			HttpServletRequest request, HttpServletResponse response, HttpSession session)

	{
		final String methodName = "sendShareProdInfoMail inside method.";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String daoResponse = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
			Long userId = null;
			final Users loginUser = (Users) session.getAttribute("loginuser");
			List<UserTrackingData> lstUserTrackingDatas = null;
			if (loginUser != null)
			{
				userId = loginUser.getUserID();
			}

			final ProductDetail shareProductInfoObj = new ProductDetail();
			shareProductInfoObj.setUserId(userId);
			shareProductInfoObj.setProductId(productId);
			shareProductInfoObj.setToEmailId(toEmailId);
			shareProductInfoObj.setFromEmailId(fromEmailId);
			String strShareTypName = null;
			lstUserTrackingDatas = commonServiceObj.getConsumerShareTypes();

			for (UserTrackingData userTrackingData : lstUserTrackingDatas)
			{

				strShareTypName = userTrackingData.getShrTypNam();
				if (strShareTypName.equals(ApplicationConstants.PRODEMAILSHARETYPETEXT))
				{
					shareProductInfoObj.setShareTypeID(userTrackingData.getShrTypID());
				}

			}

			shareProductInfoObj.setProductListID(productListId);

			commonServiceObj.saveConsShareProdDetails(shareProductInfoObj);

			daoResponse = commonServiceObj.consShareProductInfoViaEmail(shareProductInfoObj);

		}
		catch (Exception exception)
		{
			LOG.error("Error occureed in galleryhome.htm", exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

	/**
	 * This method is used to update user rating on product.
	 * 
	 * @param userRating
	 *            used to update user rating on product.
	 * @param productId
	 *            used to update the ratings.
	 * @param request
	 *            request.
	 * @param session
	 *            session.
	 * @return
	 */

	@RequestMapping(value = "/saveuserprodrating.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consSaveUserProductRating(@RequestParam(value = "userRating", required = true) int userRating,
			@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request, HttpSession session)
	{
		final String methodName = "inside the consSaveUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = "";
		Long userId = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		final UserRatingInfo userRatingInfo = new UserRatingInfo();
		final Users loginUser = (Users) session.getAttribute("loginuser");
		if (loginUser != null)
		{
			userId = loginUser.getUserID();
		}

		userRatingInfo.setUserId(userId);
		userRatingInfo.setCurrentRating(userRating);
		userRatingInfo.setProductId(productId);
		ProductDetail prodUserRatings = null;
		try
		{
			response = commonServiceObj.consSaveUserProductRating(userRatingInfo);

			prodUserRatings = (ProductDetail) session.getAttribute("consproddetails");

			session.removeAttribute(ApplicationConstants.PRODUCTRATINGREVIEW);

			// consGetProductRatings(prodUserRatings, request, session);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to fetch user ratings on product.
	 * 
	 * @param productDetail
	 *            contains the product id.
	 * @param request
	 *            request.
	 * @param session
	 *            session.
	 */

	public void consGetProductRatings(ProductDetail productDetail, HttpServletRequest request, HttpSession session)

	{
		final String methodName = "consGetProductRatings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		UserRatingInfo userRatingInfoObj = null;
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");

		try
		{
			session.removeAttribute("productRatingReview");
			userRatingInfoObj = commonServiceObj.consGetProductRatings(productDetail);

			session.setAttribute("productRatingReview", userRatingInfoObj);

		}
		catch (ScanSeeWebSqlException exception)
		{
			// TODO Auto-generated catch block
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}

	}

	/**
	 * This method is used to share product information via Email.
	 * 
	 * @param toEmailId
	 *            is used to share hotdeal information to that email id.
	 * @param fromEmailId
	 *            is used to share hotdeal details.
	 * @param hotdealId
	 *            is used to fetch hot deal details.
	 * @param request
	 *            request.
	 * @param response
	 *            response.
	 * @param session
	 *            session.
	 * @return string containing success or failure.
	 */

	@RequestMapping(value = "/sharehotdealinfoviaemail.htm", method = RequestMethod.GET)
	public @ResponseBody
	String consShareHotDealInfoMail(@RequestParam(value = "toEmailId", required = true) String toEmailId,
			@RequestParam(value = "fromEmailId", required = true) String fromEmailId,
			@RequestParam(value = "hotdealId", required = true) Integer hotdealId,

			HttpServletRequest request, HttpServletResponse response, HttpSession session)

	{
		final String methodName = "consShareHotDealInfoMail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String daoResponse = null;

		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
			Long userId = null;
			final Users loginUser = (Users) session.getAttribute("loginuser");

			if (loginUser != null)
			{
				userId = loginUser.getUserID();
			}
			final ProductDetail shareHotdealInfoObj = new ProductDetail();
			shareHotdealInfoObj.setUserId(userId);
			shareHotdealInfoObj.setHotDealId(hotdealId);
			shareHotdealInfoObj.setToEmailId(toEmailId);
			shareHotdealInfoObj.setFromEmailId(fromEmailId);
			daoResponse = commonServiceObj.consShareHotDealInfoViaEmail(shareHotdealInfoObj);

		}
		catch (Exception exception)
		{
			LOG.error("Error occureed in galleryhome.htm", exception.getMessage());

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

	/*
	 * public static void main(String a[]) { Integer productid = 2332; String
	 * pname = "coolpix"; String strImage =
	 * "http://localhost:8080/Images/manufacturer/245/images33.jpg, " +
	 * "http://localhost:8080/Images/manufacturer/245/sofaB1.jpg," +
	 * " http://localhost:8080/Images/manufacturer/245/tab_bg.png, " +
	 * "http://localhost:8080/Images/manufacturer/245/artist2.png";
	 * ProductDetails multipleProdImage = new ProductDetails(); ProductDetail
	 * productDetailO = new ProductDetail();
	 * productDetailO.setProductName(pname);
	 * productDetailO.setProductImagePath(strImage); ArrayList<String> imagelst
	 * = new ArrayList<String>(); String[] items =
	 * productDetailO.getProductImagePath().split(","); for (int i = 0; i <
	 * items.length; i++) { String image = items[i]; imagelst.add(image); }
	 * productDetailO.setImagelst(imagelst); LOG.info("product details :" +
	 * productDetailO); }
	 */

}

package shopper.find.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import shopper.find.service.FindService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.HotDealInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailerPage;
import common.pojo.shopper.RetailersDetails;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is used for This Location functionalities in Find module such as
 * Retail Location search based on the search criteria name,category and zip
 * code, listing anything pages created by retailer, displaying anything page
 * details and displaying special offers.
 * 
 * @author dileepa_cc
 */
@Controller
public class ConsumerFindThisLocationController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ConsumerFindThisLocationController.class);

	/**
	 * This method is used to display retail location list based on the search
	 * criteria name,category and Zipcode.
	 * 
	 * @param searchForm
	 *            model attribute as a parameter
	 * @param result
	 *            as a parameter
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return ModelandView
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/findresultloctnlist.htm", method = RequestMethod.POST)
	public String findResultLoctnList(@ModelAttribute("SearchForm") SearchForm searchForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		final String methodName = "ConsFindHome";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int mainMenuId = 0;
		ConsumerRequestDetails consumerRequestDetails = null;
		ConsumerRequestDetails consumerReqDetSession = null;
		List<RetailerDetail> retailLocList = null;
		int recordCount = 20;
		int currentPage = 1;
		String pageNumber = "0";
		int lowerLimit = 0;
		Long userId = null;
		String consumerType = null;
		SearchForm objForm = null;
		String latitute = null;
		String longitude = null;
		session.removeAttribute("retailLocList");
		session.removeAttribute("consumerReqObj");
		request.setAttribute("showHeaderSearch", "show");
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			FindService findService = (FindService) appContext.getBean("findService");
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);

			if (!"".equals(Utility.checkNull(searchForm.getRecordCount())))
			{
				recordCount = searchForm.getRecordCount();
			}
			if (null != loginUser)
			{
				userId = loginUser.getUserID();
				consumerType = ApplicationConstants.LOGINUSER;
			}
			else
			{
				consumerType = ApplicationConstants.GUESTUSER;

			}
			searchForm = getSearchParameters(searchForm, loginUser);

			consumerReqDetSession = (ConsumerRequestDetails) session.getAttribute("consumerIpDetail");
			if (null != consumerReqDetSession)
			{
				if (null != consumerReqDetSession.getPostalCode() && consumerReqDetSession.getPostalCode().equals(searchForm.getZipCode()))
				{
					latitute = consumerReqDetSession.getLatitute();
					longitude = consumerReqDetSession.getLongitude();
				}

			}
			consumerRequestDetails = new ConsumerRequestDetails(userId, latitute, longitude, searchForm.getZipCode(), ApplicationConstants.FINDMODULE);
			mainMenuId = findService.getMainMenuID(consumerRequestDetails);
			consumerRequestDetails.setMainMenuId(mainMenuId);

			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchFormLoc");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATIONLOC);

				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);

				}
				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));

				searchForm.setCategoryName(objForm.getCategoryName());
				searchForm.setZipCode(objForm.getZipCode());
				searchForm.setSearchKey(objForm.getSearchKey());
				searchForm.setCurrentPageNumber(currentPage);
			}

			if (objForm == null)
			{
				objForm = new SearchForm();
				objForm.setSearchKey(searchForm.getSearchKey());
				objForm.setZipCode(searchForm.getZipCode());
				objForm.setCategoryName(searchForm.getCategoryName());
				searchForm.setCurrentPageNumber(currentPage);
				objForm.setCurrentPageNumber(currentPage);
				session.setAttribute("searchFormLoc", objForm);
				session.setAttribute("zipcode", searchForm.getZipCode());
			}

			consumerRequestDetails = new ConsumerRequestDetails(userId, consumerType, mainMenuId, objForm.getZipCode(), objForm.getSearchKey(),
					lowerLimit, ApplicationConstants.FINDMODULE, recordCount, objForm.getCategoryName());
			session.setAttribute("consumerReqObj", consumerRequestDetails);
			RetailersDetails retailersDetails = findService.searchRetailLocation(consumerRequestDetails);
			if (null != retailersDetails)
			{
				retailLocList = retailersDetails.getRetailerDetail();

				if (null != retailLocList && !retailLocList.isEmpty())
				{
					session.setAttribute("retailLocList", retailLocList);
				}
				else
				{
					session.setAttribute("retailLocList", null);
				}
				request.setAttribute("breadCrumbTxt", String.valueOf(objForm.getSearchKey() == null ? "" : objForm.getSearchKey()) + " (Showing "
						+ retailersDetails.getTotalSize() + " results)");

				final Pagination objPage = Utility.getPagination(retailersDetails.getTotalSize(), currentPage,
						"/ScanSeeWeb/shopper/findresultloctnlist.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATIONLOC, objPage);
			}
			else
			{

				request.setAttribute("showHeaderSearch", null);
				session.removeAttribute(ApplicationConstants.PAGINATIONLOC);
			}
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in findResultLoctnList method:" + e);
			throw e;
		}
		model.addAttribute("findLocSearchResultForm", searchForm);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "consfindresultloclist";

	}

	/**
	 * This method is used to get search parameters.
	 * 
	 * @param searchForm
	 *            as a parameter
	 * @param loginUser
	 *            as a parameter
	 * @returns searchForm
	 */
	public static SearchForm getSearchParameters(SearchForm searchForm, Users loginUser)
	{
		if ("".equals(searchForm.getSearchKey()))
		{
			searchForm.setSearchKey(null);
		}

		if ("".equals(Utility.checkNull(searchForm.getCategoryName())))
		{
			searchForm.setCategoryName(null);
		}

		if ("".equals(searchForm.getZipCode()) || "Zipcode".equals(searchForm.getZipCode()))
		{
			if (null != loginUser)
			{
				searchForm.setZipCode(loginUser.getPostalCode());
			}
			else
			{
				searchForm.setZipCode(null);
			}
		}

		return searchForm;

	}

	/**
	 * This method is used fetch retailer created anything pages.
	 * 
	 * @param retailId
	 *            as a parameter
	 * @param retailLocId
	 *            as a parameter
	 * @param retListID
	 *            as a parameter
	 * @param request
	 *            as a parameter
	 * @param response
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @return Anything page list in HTML format as a string
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/getanythingpagelist", method = RequestMethod.GET)
	@ResponseBody
	public final String getanythingpagelist(@RequestParam(value = "retailId", required = true) Integer retailId,
			@RequestParam(value = "retailLocId", required = true) Integer retailLocId,
			@RequestParam(value = "retListId", required = true) Integer retListID, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException
	{

		final String methodName = "getanythingpagelist";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		FindService findService = (FindService) appContext.getBean("findService");
		RetailerDetail retailerDetail = new RetailerDetail();
		String anyThingPageListHtml = null;
		response.setContentType("text/html");
		ConsumerRequestDetails consumerRequestDetails = null;
		request.setAttribute("showHeaderSearch", "show");
		try
		{
			final Users loginUser = (Users) request.getSession().getAttribute(ApplicationConstants.LOGINUSER);
			if (null != loginUser)
			{
				retailerDetail.setUserId(loginUser.getUserID());
			}
			else
			{
				retailerDetail.setUserId(null);

			}

			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			retailerDetail.setRetailerId(retailId);
			retailerDetail.setRetailLocationID(retailLocId);
			retailerDetail.setRetListID(retListID);
			retailerDetail.setMainMenuId(consumerRequestDetails.getMainMenuId());
			anyThingPageListHtml = findService.getRetailerAnythigPageList(retailerDetail);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in getanythingpagelist method:" + e);
			throw e;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return anyThingPageListHtml;
	}

	/**
	 * This method is used to display special offers and also decides the page
	 * to be rendered among special offer, sale product and hot deal.
	 * 
	 * @param searchForm
	 *            model attribute as a parameter
	 * @param result
	 *            as a parameter
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return ModelandView
	 * @throws ScanSeeServiceException
	 *             throws ScanSeeServiceException exception
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/displayspecials.htm", method = RequestMethod.POST)
	public String displaySpecial(@ModelAttribute("findLocSearchResultForm") SearchForm searchForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		final String methodName = "ConsFindHome";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ConsumerRequestDetails consumerRequestDetails = null;
		List<RetailerPage> specialOfferPageList = null;
		int recordCount = 20;
		int currentPage = 1;
		String pageNumber = "0";
		int lowerLimit = 0;
		SearchForm objForm = null;
		final RetailerDetail retailerDetail = new RetailerDetail();
		RetailersDetails retailersDetailsObj = null;
		List<RetailerDetail> retailLocList = null;
		RetailerDetail retailerInfo = null;
		SearchForm searchFormModelObj = null;
		HashMap<String, Boolean> mapDiaplayPage = null;
		List<ProductDetail> saleProductList = null;
		String showPage = null;
		ProductDetails productDetailsObj = null;
		SearchResultInfo searchResultInfo = null;
		List<HotDealInfo> hotDealList = null;
		request.setAttribute("showHeaderSearch", "show");
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			FindService findService = (FindService) appContext.getBean("findService");
			if (!"".equals(Utility.checkNull(searchForm.getRecordCount())))
			{
				recordCount = searchForm.getRecordCount();
			}
			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			mapDiaplayPage = findService.checkDiscountPageToDisplay(consumerRequestDetails.getUserId(), searchForm.getRetailerId(),
					searchForm.getRetailLocationID(), searchForm.getRetListID());

			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);

				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);

				}
				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));

			}

			if (objForm == null)
			{
				objForm = new SearchForm();
				objForm.setRetailerId(searchForm.getRetailerId());
				objForm.setRetailLocationID(searchForm.getRetailLocationID());
				objForm.setRetListID(searchForm.getRetListID());
				objForm.setSearchKey(searchForm.getSearchKey());
				objForm.setZipCode(searchForm.getZipCode());
				objForm.setCategoryName(searchForm.getCategoryName());
				objForm.setCurrentPageNumber(searchForm.getCurrentPageNumber());

				if (0 != searchForm.getLocSearchRecCount())
				{
					objForm.setLocSearchRecCount(searchForm.getLocSearchRecCount());
				}
				else
				{
					objForm.setLocSearchRecCount(recordCount);
				}

				session.setAttribute("searchForm", objForm);

			}

			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			searchFormModelObj = new SearchForm();
			searchFormModelObj.setRetailerId(objForm.getRetailerId());
			searchFormModelObj.setRetailLocationID(objForm.getRetailLocationID());
			searchFormModelObj.setRetListID(objForm.getRetListID());
			searchFormModelObj.setSearchKey(objForm.getSearchKey());
			searchFormModelObj.setZipCode(objForm.getZipCode());
			searchFormModelObj.setCategoryName(objForm.getCategoryName());
			searchFormModelObj.setCurrentPageNumber(objForm.getCurrentPageNumber());
			searchFormModelObj.setLocSearchRecCount(objForm.getLocSearchRecCount());

			model.addAttribute("findLocSearchResultForm", searchFormModelObj);
			session.setAttribute("findLocSearchResultProdSummry", searchFormModelObj);
			retailerDetail.setUserId(consumerRequestDetails.getUserId());
			retailerDetail.setRetailerId(objForm.getRetailerId());
			retailerDetail.setRetailLocationID(objForm.getRetailLocationID());
			retailerDetail.setRetListID(objForm.getRetListID());
			retailerDetail.setLowerLimit(lowerLimit);
			retailerDetail.setRecordCount(recordCount);
			retailerDetail.setMainMenuId(consumerRequestDetails.getMainMenuId());

			if (mapDiaplayPage.get(ApplicationConstants.SALEFLAG))
			{
				productDetailsObj = findService.getSaleProductList(retailerDetail);
				request.setAttribute("displaySaleProdSummy", "Sale");
				if (null != productDetailsObj)
				{
					saleProductList = productDetailsObj.getProductDetail();

					if (null != saleProductList && !saleProductList.isEmpty())
					{

						request.setAttribute("saleProductList", saleProductList);
					}
					else
					{

						request.setAttribute("saleProductList", null);
					}

					final Pagination objPage = Utility.getPagination(productDetailsObj.getTotalSize(), currentPage,
							"/ScanSeeWeb/shopper/findresultloctnlistsales.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);

				}
				showPage = "consfindresultloclistsales";
			}
			else if (mapDiaplayPage.get(ApplicationConstants.SPECIALFLAG))
			{

				retailersDetailsObj = findService.getRetailerSpecialOfferPageList(retailerDetail);

				if (null != retailersDetailsObj)
				{
					specialOfferPageList = retailersDetailsObj.getRetailerPageList();

					if (null != specialOfferPageList && !specialOfferPageList.isEmpty())
					{
						request.setAttribute("retailLocListSpecials", specialOfferPageList);

					}
					else
					{
						request.setAttribute("retailLocListSpecials", null);

					}

					final Pagination objPage = Utility.getPagination(retailersDetailsObj.getTotalSize(), currentPage,
							"/ScanSeeWeb/shopper/findresultloctnlistspecials.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);

				}
				showPage = "consfindresultloclistspecials";
			}
			else if (mapDiaplayPage.get(ApplicationConstants.HOTDEALFLAG))
			{
				searchResultInfo = findService.getHotDealList(retailerDetail);

				if (null != searchResultInfo)
				{
					hotDealList = searchResultInfo.getDealList();
					if (null != hotDealList && !hotDealList.isEmpty())
					{
						request.setAttribute("hotDealList", hotDealList);
					}
					else
					{

						request.setAttribute("hotDealList", null);
					}

					final Pagination objPage = Utility.getPagination(searchResultInfo.getTotalSize(), currentPage,
							"/ScanSeeWeb/shopper/findresultloctnlistdeals.htm", recordCount);
					session.setAttribute(ApplicationConstants.PAGINATION, objPage);

				}
				showPage = "consfindresultlochotdeals";
			}

			retailLocList = (List<RetailerDetail>) session.getAttribute("retailLocList");

			if (null != retailLocList)
			{
				for (RetailerDetail retailerDetails : retailLocList)
				{

					if (retailerDetails.getRetailLocationID().intValue() == objForm.getRetailLocationID().intValue())
					{
						retailerInfo = retailerDetails;
						break;
					}

				}

			}

			session.setAttribute("retailerInfo", retailerInfo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in displaySpecial method:" + e);
			throw e;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return showPage;

	}

	/**
	 * This method is used to display sale product page.
	 * 
	 * @param searchForm
	 *            model attribute as a parameter
	 * @param result
	 *            as a parameter
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return ModelandView
	 * @throws ScanSeeServiceException
	 *             throws ScanSeeServiceException exception
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/findresultloctnlistsales.htm", method = RequestMethod.POST)
	public String getSaleProductList(@ModelAttribute("findLocSearchResultForm") SearchForm searchForm, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		final String methodName = "getSaleProductList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ConsumerRequestDetails consumerRequestDetails = null;
		List<ProductDetail> saleProductList = null;
		int recordCount = 20;
		int currentPage = 1;
		String pageNumber = "0";
		int lowerLimit = 0;
		SearchForm objForm = null;
		final RetailerDetail retailerDetail = new RetailerDetail();
		ProductDetails productDetailsObj = null;
		List<RetailerDetail> retailLocList = null;
		RetailerDetail retailerInfo = null;
		SearchForm searchFormModelObj = null;
		request.setAttribute("showHeaderSearch", "show");
		request.setAttribute("displaySaleProdSummy", "Sale");
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			FindService findService = (FindService) appContext.getBean("findService");
			if (!"".equals(Utility.checkNull(searchForm.getRecordCount())))
			{
				recordCount = searchForm.getRecordCount();
			}
			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);

				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);

				}
				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));

			}

			if (objForm == null)
			{
				objForm = new SearchForm();
				objForm.setRetailerId(searchForm.getRetailerId());
				objForm.setRetailLocationID(searchForm.getRetailLocationID());
				objForm.setRetListID(searchForm.getRetListID());
				objForm.setSearchKey(searchForm.getSearchKey());
				objForm.setZipCode(searchForm.getZipCode());
				objForm.setCategoryName(searchForm.getCategoryName());
				objForm.setCurrentPageNumber(searchForm.getCurrentPageNumber());
				if (0 != searchForm.getLocSearchRecCount())
				{
					objForm.setLocSearchRecCount(searchForm.getLocSearchRecCount());
				}
				else
				{
					objForm.setLocSearchRecCount(recordCount);
				}

				session.setAttribute("searchForm", objForm);
			}
			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			searchFormModelObj = new SearchForm();
			searchFormModelObj.setRetailerId(objForm.getRetailerId());
			searchFormModelObj.setRetailLocationID(objForm.getRetailLocationID());
			searchFormModelObj.setRetListID(objForm.getRetListID());
			searchFormModelObj.setSearchKey(objForm.getSearchKey());
			searchFormModelObj.setZipCode(objForm.getZipCode());
			searchFormModelObj.setCategoryName(objForm.getCategoryName());
			searchFormModelObj.setCurrentPageNumber(objForm.getCurrentPageNumber());
			searchFormModelObj.setLocSearchRecCount(objForm.getLocSearchRecCount());

			model.addAttribute("findLocSearchResultForm", searchFormModelObj);
			session.setAttribute("findLocSearchResultProdSummry", searchFormModelObj);
			retailerDetail.setUserId(consumerRequestDetails.getUserId());
			retailerDetail.setRetailerId(objForm.getRetailerId());
			retailerDetail.setRetailLocationID(objForm.getRetailLocationID());
			retailerDetail.setRetListID(objForm.getRetListID());
			retailerDetail.setLowerLimit(lowerLimit);
			retailerDetail.setRecordCount(recordCount);
			retailerDetail.setMainMenuId(consumerRequestDetails.getMainMenuId());

			productDetailsObj = findService.getSaleProductList(retailerDetail);

			if (null != productDetailsObj)
			{
				saleProductList = productDetailsObj.getProductDetail();

				final Pagination objPage = Utility.getPagination(productDetailsObj.getTotalSize(), currentPage,
						"/ScanSeeWeb/shopper/findresultloctnlistsales.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				if (null != saleProductList && !saleProductList.isEmpty())
				{

					request.setAttribute("saleProductList", saleProductList);
				}
				else
				{
					session.removeAttribute(ApplicationConstants.PAGINATION);
					request.setAttribute("saleProductList", null);
				}

			}
			else
			{

				session.removeAttribute(ApplicationConstants.PAGINATION);
			}
			retailLocList = (List<RetailerDetail>) session.getAttribute("retailLocList");

			for (RetailerDetail retailerDetails : retailLocList)
			{

				if (retailerDetails.getRetailLocationID().intValue() == objForm.getRetailLocationID().intValue())
				{
					retailerInfo = retailerDetails;
					break;
				}

			}

			session.setAttribute("retailerInfo", retailerInfo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in getSaleProductList method:" + e);
			throw e;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return "consfindresultloclistsales";

	}

	/**
	 * This method is used to display Hot deals page.
	 * 
	 * @param searchForm
	 *            model attribute as a parameter
	 * @param result
	 *            as a parameter
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return ModelandView
	 * @throws ScanSeeServiceException
	 *             throws ScanSeeServiceException exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/findresultloctnlistdeals.htm", method = RequestMethod.POST)
	public String getHotDealList(@ModelAttribute("findLocSearchResultForm") SearchForm searchForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		final String methodName = "getHotDealList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ConsumerRequestDetails consumerRequestDetails = null;
		int recordCount = 20;
		int currentPage = 1;
		String pageNumber = "0";
		int lowerLimit = 0;
		SearchForm objForm = null;
		final RetailerDetail retailerDetail = new RetailerDetail();
		SearchResultInfo searchResultInfo = null;
		List<RetailerDetail> retailLocList = null;
		RetailerDetail retailerInfo = null;
		SearchForm searchFormModelObj = null;
		List<HotDealInfo> hotDealList = null;
		request.setAttribute("showHeaderSearch", "show");
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			FindService findService = (FindService) appContext.getBean("findService");
			if (!"".equals(Utility.checkNull(searchForm.getRecordCount())))
			{
				recordCount = searchForm.getRecordCount();
			}
			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);

				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);

				}
				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));

			}

			if (objForm == null)
			{
				objForm = new SearchForm();
				objForm.setRetailerId(searchForm.getRetailerId());
				objForm.setRetailLocationID(searchForm.getRetailLocationID());
				objForm.setRetListID(searchForm.getRetListID());
				objForm.setSearchKey(searchForm.getSearchKey());
				objForm.setZipCode(searchForm.getZipCode());
				objForm.setCategoryName(searchForm.getCategoryName());
				objForm.setCurrentPageNumber(searchForm.getCurrentPageNumber());
				if (0 != searchForm.getLocSearchRecCount())
				{
					objForm.setLocSearchRecCount(searchForm.getLocSearchRecCount());
				}
				else
				{
					objForm.setLocSearchRecCount(recordCount);
				}
				session.setAttribute("searchForm", objForm);
			}

			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			searchFormModelObj = new SearchForm();
			searchFormModelObj.setRetailerId(objForm.getRetailerId());
			searchFormModelObj.setRetailLocationID(objForm.getRetailLocationID());
			searchFormModelObj.setRetListID(objForm.getRetListID());
			searchFormModelObj.setSearchKey(objForm.getSearchKey());
			searchFormModelObj.setZipCode(objForm.getZipCode());
			searchFormModelObj.setCategoryName(objForm.getCategoryName());
			searchFormModelObj.setCurrentPageNumber(objForm.getCurrentPageNumber());
			searchFormModelObj.setLocSearchRecCount(objForm.getLocSearchRecCount());

			model.addAttribute("findLocSearchResultForm", searchFormModelObj);

			retailerDetail.setUserId(consumerRequestDetails.getUserId());
			retailerDetail.setRetailerId(objForm.getRetailerId());
			retailerDetail.setRetailLocationID(objForm.getRetailLocationID());
			retailerDetail.setRetListID(objForm.getRetListID());
			retailerDetail.setLowerLimit(lowerLimit);
			retailerDetail.setRecordCount(recordCount);
			retailerDetail.setMainMenuId(consumerRequestDetails.getMainMenuId());

			searchResultInfo = findService.getHotDealList(retailerDetail);

			if (null != searchResultInfo)
			{
				hotDealList = searchResultInfo.getDealList();
				final Pagination objPage = Utility.getPagination(searchResultInfo.getTotalSize(), currentPage,
						"/ScanSeeWeb/shopper/findresultloctnlistdeals.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);
				if (null != hotDealList && !hotDealList.isEmpty())
				{
					request.setAttribute("hotDealList", hotDealList);
				}
				else
				{
					session.removeAttribute(ApplicationConstants.PAGINATION);
					request.setAttribute("hotDealList", null);
				}

			}
			else
			{

				session.removeAttribute(ApplicationConstants.PAGINATION);
			}

			retailLocList = (List<RetailerDetail>) session.getAttribute("retailLocList");

			for (RetailerDetail retailerDetails : retailLocList)
			{

				if (retailerDetails.getRetailLocationID().intValue() == objForm.getRetailLocationID().intValue())
				{
					retailerInfo = retailerDetails;
					break;
				}

			}

			session.setAttribute("retailerInfo", retailerInfo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in getHotDealList method:" + e);
			throw e;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return "consfindresultlochotdeals";

	}

	/**
	 * This method is used to display special offers page.
	 * 
	 * @param searchForm
	 *            model attribute as a parameter
	 * @param result
	 *            as a parameter
	 * @param request
	 *            as a parameter
	 * @param session
	 *            as a parameter
	 * @param model
	 *            as a parameter
	 * @return ModelandView
	 * @throws ScanSeeServiceException
	 *             throws ScanSeeServiceException exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/findresultloctnlistspecials.htm", method = RequestMethod.POST)
	public String findResultLoctnListSpecial(@ModelAttribute("findLocSearchResultForm") SearchForm searchForm, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{

		final String methodName = "ConsFindHome";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ConsumerRequestDetails consumerRequestDetails = null;
		List<RetailerPage> specialOfferPageList = null;
		int recordCount = 20;
		int currentPage = 1;
		String pageNumber = "0";
		int lowerLimit = 0;
		SearchForm objForm = null;
		final RetailerDetail retailerDetail = new RetailerDetail();
		RetailersDetails retailersDetailsObj = null;
		List<RetailerDetail> retailLocList = null;
		RetailerDetail retailerInfo = null;
		SearchForm searchFormModelObj = null;
		request.setAttribute("showHeaderSearch", "show");
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			FindService findService = (FindService) appContext.getBean("findService");
			if (!"".equals(Utility.checkNull(searchForm.getRecordCount())))
			{
				recordCount = searchForm.getRecordCount();
			}
			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			final String pageFlag = (String) request.getParameter("pageFlag");

			if (null != pageFlag && "true".equals(pageFlag))
			{
				objForm = (SearchForm) session.getAttribute("searchForm");
				pageNumber = request.getParameter("pageNumber");
				final Pagination pageSess = (Pagination) session.getAttribute(ApplicationConstants.PAGINATION);

				if (Integer.valueOf(pageNumber) != 0)
				{
					currentPage = Integer.valueOf(pageNumber);

				}
				lowerLimit = Utility.getLowerLimit(pageSess, Integer.valueOf(pageNumber));

			}

			if (objForm == null)
			{
				objForm = new SearchForm();
				objForm.setRetailerId(searchForm.getRetailerId());
				objForm.setRetailLocationID(searchForm.getRetailLocationID());
				objForm.setRetListID(searchForm.getRetListID());
				objForm.setSearchKey(searchForm.getSearchKey());
				objForm.setZipCode(searchForm.getZipCode());
				objForm.setCategoryName(searchForm.getCategoryName());
				objForm.setCurrentPageNumber(searchForm.getCurrentPageNumber());
				if (0 != searchForm.getLocSearchRecCount())
				{
					objForm.setLocSearchRecCount(searchForm.getLocSearchRecCount());
				}
				else
				{
					objForm.setLocSearchRecCount(recordCount);
				}

				session.setAttribute("searchForm", objForm);

			}
			consumerRequestDetails = (ConsumerRequestDetails) session.getAttribute("consumerReqObj");

			searchFormModelObj = new SearchForm();
			searchFormModelObj.setRetailerId(objForm.getRetailerId());
			searchFormModelObj.setRetailLocationID(objForm.getRetailLocationID());
			searchFormModelObj.setRetListID(objForm.getRetListID());
			searchFormModelObj.setSearchKey(objForm.getSearchKey());
			searchFormModelObj.setZipCode(objForm.getZipCode());
			searchFormModelObj.setCategoryName(objForm.getCategoryName());
			searchFormModelObj.setCurrentPageNumber(objForm.getCurrentPageNumber());
			searchFormModelObj.setLocSearchRecCount(objForm.getLocSearchRecCount());

			model.addAttribute("findLocSearchResultForm", searchFormModelObj);

			retailerDetail.setUserId(consumerRequestDetails.getUserId());
			retailerDetail.setRetailerId(objForm.getRetailerId());
			retailerDetail.setRetailLocationID(objForm.getRetailLocationID());
			retailerDetail.setRetListID(objForm.getRetListID());
			retailerDetail.setLowerLimit(lowerLimit);
			retailerDetail.setRecordCount(recordCount);
			retailerDetail.setMainMenuId(consumerRequestDetails.getMainMenuId());

			retailersDetailsObj = findService.getRetailerSpecialOfferPageList(retailerDetail);

			if (null != retailersDetailsObj)
			{
				specialOfferPageList = retailersDetailsObj.getRetailerPageList();
				final Pagination objPage = Utility.getPagination(retailersDetailsObj.getTotalSize(), currentPage,
						"/ScanSeeWeb/shopper/findresultloctnlistspecials.htm", recordCount);
				session.setAttribute(ApplicationConstants.PAGINATION, objPage);

				if (null != specialOfferPageList && !specialOfferPageList.isEmpty())
				{
					request.setAttribute("retailLocListSpecials", specialOfferPageList);

				}
				else
				{
					session.removeAttribute(ApplicationConstants.PAGINATION);
					request.setAttribute("retailLocListSpecials", null);

				}

			}
			else
			{

				session.removeAttribute(ApplicationConstants.PAGINATION);
			}

			retailLocList = (List<RetailerDetail>) session.getAttribute("retailLocList");

			if (null != retailLocList)
			{
				for (RetailerDetail retailerDetails : retailLocList)
				{

					if (retailerDetails.getRetailLocationID().intValue() == objForm.getRetailLocationID().intValue())
					{
						retailerInfo = retailerDetails;
						break;
					}

				}

			}

			session.setAttribute("retailerInfo", retailerInfo);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in findResultLoctnListSpecial method:" + e);
			throw e;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "consfindresultloclistspecials";

	}

}

package shopper.find.service;

import java.util.HashMap;
import java.util.List;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.shopper.GoogleCategory;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailerPage;
import common.pojo.shopper.RetailersDetails;

/**
 * @author pradip_k
 */
public interface FindService
{

	/**
	 * The service method to find retailers given the coordinates.
	 * 
	 * @param xml
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	// String findService(String xml) throws ScanSeeServiceException;

	/**
	 * The service method For displaying Categories. No query parameter
	 * 
	 * @param userId
	 *            as request parameter
	 * @return String based success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */

	// ################################################### New Consumer
	// Implementation Methods #############################################.
	public List<GoogleCategory> getCategoryInfo(Long userId) throws ScanSeeServiceException;

	/**
	 * This method is used to search product based on user entered search key.
	 * It returns the product details like name,image,it based on search key.
	 */
	public ProductDetails searchProducts(SearchForm searchFormObj) throws ScanSeeWebSqlException;

	/**
	 * This service method is used to fetch retail locations.
	 * 
	 * @param consumerRequestDetails
	 *            as a parameter which holds the data required to fetch retail
	 *            locations
	 * @return retail location list
	 * @throws ScanSeeServiceException
	 */
	public RetailersDetails searchRetailLocation(ConsumerRequestDetails consumerRequestDetails) throws ScanSeeServiceException;

	public HotDealsListResultSet findDealsSearch(SearchForm searchFormObj) throws ScanSeeWebSqlException;

	/**
	 * This service method is used to get Main Menu id which is used for user
	 * tracking.
	 * 
	 * @param consumerRequestDetails
	 *            as a parameter which holds the details required to get main
	 *            menu id from database
	 * @return main menu id
	 * @throws ScanSeeServiceException
	 */
	public int getMainMenuID(ConsumerRequestDetails consumerRequestDetails) throws ScanSeeServiceException;

	/**
	 * This service method is used to get retailer created anything pages.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get
	 *            anything pages
	 * @return anything page list in HTML format as a string
	 * @throws ScanSeeServiceException
	 */
	public String getRetailerAnythigPageList(RetailerDetail retailerDetail) throws ScanSeeServiceException;

	/**
	 * This service method is used to get special offers page list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get special
	 *            offers page list.
	 * @return retailersDetails object having special offers list
	 * @throws ScanSeeServiceException
	 */
	public RetailersDetails getRetailerSpecialOfferPageList(RetailerDetail retailerDetail) throws ScanSeeServiceException;

	/**
	 * This service method is used to get sale products list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get sale
	 *            products list.
	 * @return productDetailsObj object having sale product list
	 * @throws ScanSeeServiceException
	 */
	public ProductDetails getSaleProductList(RetailerDetail retailerDetail) throws ScanSeeServiceException;

	/**
	 * This service method is used to get hot deals list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get hot
	 *            deals list.
	 * @return searchResultInfo object having hot deals list
	 * @throws ScanSeeServiceException
	 */
	public SearchResultInfo getHotDealList(RetailerDetail retailerDetail) throws ScanSeeServiceException;

	/**
	 * This service method is used to get special offer page to be displayed.
	 * 
	 * @param userId
	 *            as a parameter
	 * @param retailId
	 *            as a parameter
	 * @param retailLocId
	 *            as a parameter
	 * @param retailLocListId
	 *            as a parameter
	 * @return hashMap containing the flag for the page to be displayed
	 * @throws ScanSeeServiceException
	 */
	public HashMap<String, Boolean> checkDiscountPageToDisplay(Long userId, Integer retailId, Integer retailLocId, Integer retailLocListId)
			throws ScanSeeServiceException;
}

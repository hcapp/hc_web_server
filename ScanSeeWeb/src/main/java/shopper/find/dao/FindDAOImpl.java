package shopper.find.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.HotDealInfo;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.shopper.GoogleCategory;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.HotDealsResultSet;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailerPage;
import common.pojo.shopper.RetailersDetails;

/**
 * @author pradip_k
 */
public class FindDAOImpl implements FindDAO
{

	private static final Logger LOG = LoggerFactory.getLogger(FindDAOImpl.class.getName());

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            for setDataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This DAO Implementation method for getting hot deals category list . No
	 * query parameter.
	 * 
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	public List<GoogleCategory> getCategoryDetail() throws ScanSeeWebSqlException
	{
		final String methodName = "getCategoryDetail in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<GoogleCategory> categorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_FindCategoryDisplay");
			simpleJdbcCall.returningResultSet("Categorylist", new BeanPropertyRowMapper<GoogleCategory>(GoogleCategory.class));

			final SqlParameterSource fetchCategoryParameters = new MapSqlParameterSource().addValue("ApiPartnerName", "Google");

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCategoryParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					categorylst = (List<GoogleCategory>) resultFromProcedure.get("Categorylist");
				}

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in getCategoryDetail", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return categorylst;
	}

	/**
	 * This method is used to search product based on user entered search key.
	 * It returns the product details like name,image,it based on search key.
	 */
	@SuppressWarnings("unused")
	public ProductDetails fetchAllProduct(SearchForm searchInfoObj) throws ScanSeeWebSqlException
	{
		final String methodName = "  fetchAllProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;
		Integer rowcount = null;

		try
		{
			if (searchInfoObj.getSearchKey() != null)
			{
				if (searchInfoObj.getSearchKey().equals(""))
				{
					searchInfoObj.setSearchKey(null);
				}
			}

			if (null == searchInfoObj.getLastVisitedNo())
			{
				searchInfoObj.setLastVisitedNo(0);

			}

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerProductSmartSearchResults");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", searchInfoObj.getUserId());
			scanQueryParams.addValue("ProdSearch", searchInfoObj.getSearchKey());
			scanQueryParams.addValue("LowerLimit", searchInfoObj.getLastVisitedNo());
			scanQueryParams.addValue("RecordCount", searchInfoObj.getRecordCount());
			scanQueryParams.addValue("MainmenuID", searchInfoObj.getMainMenuID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{

						LOG.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);
						rowcount = (Integer) resultFromProcedure.get("MaxCnt");
						productDetails.setTotalSize(rowcount);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_WebConsumerProductSmartSearchResults Store Procedure error number: {} and error message: {}",
						errorNum, errorMsg);
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");
				productDetails.setTotalSize(rowcount);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in fetchAllProduct", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	public HotDealsListResultSet findDealsSearch(SearchForm searchFormObj) throws ScanSeeWebSqlException
	{
		final String methodName = "findDealsSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<HotDealsResultSet> hotDealsResultSetList = null;
		HotDealsListResultSet hotDealsListResultSetObj = null;
		Integer rowcount = null;

		try
		{

			if (searchFormObj.getSearchKey() != null)
			{
				if (searchFormObj.getSearchKey().equals(""))
				{
					searchFormObj.setSearchKey(null);
				}
			}

			if (searchFormObj.getZipCode() != null)
			{
				if (searchFormObj.getZipCode().equals("") || searchFormObj.getZipCode().equalsIgnoreCase("Zipcode"))
				{
					searchFormObj.setZipCode(null);
				}
			}

			if (null == searchFormObj.getLastVisitedNo())
			{
				searchFormObj.setLastVisitedNo(0);

			}

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerFindDealSearch");
			simpleJdbcCall.returningResultSet("hotdeals", new BeanPropertyRowMapper<HotDealsResultSet>(HotDealsResultSet.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", searchFormObj.getUserId());
			scanQueryParams.addValue("SearchString", searchFormObj.getSearchKey());
			scanQueryParams.addValue("PostalCode", searchFormObj.getZipCode());
			scanQueryParams.addValue("LowerLimit", searchFormObj.getLastVisitedNo());
			scanQueryParams.addValue("MainMenuID", searchFormObj.getMainMenuID());
			scanQueryParams.addValue("RecordCount", searchFormObj.getRecordCount());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			hotDealsResultSetList = (ArrayList<HotDealsResultSet>) resultFromProcedure.get("hotdeals");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != hotDealsResultSetList && !hotDealsResultSetList.isEmpty())
					{

						LOG.info("Products found for the search");
						hotDealsListResultSetObj = new HotDealsListResultSet();
						hotDealsListResultSetObj.setHotDealsListResponselst(hotDealsResultSetList);
						rowcount = (Integer) resultFromProcedure.get("RowCount");
						hotDealsListResultSetObj.setTotalSize(rowcount);
					}

				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				rowcount = (Integer) resultFromProcedure.get("MaxCnt");

				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in findDealsSearch", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsListResultSetObj;
	}

	/**
	 * This DAO method is used to fetch retail locations.
	 * 
	 * @param consumerRequestDetails
	 *            as a parameter which holds the data required to fetch retail
	 *            locations
	 * @return retailersDetails
	 * @throws ScanSeeWebSqlException
	 */
	public RetailersDetails searchRetailLocation(ConsumerRequestDetails consumerRequestDetails) throws ScanSeeWebSqlException
	{
		final String methodName = "findRetailerLocationSearch";
		if (LOG.isDebugEnabled())
		{
			LOG.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + consumerRequestDetails.getUserId());
		}

		RetailersDetails retailersDetailsObj = null;
		List<RetailerDetail> retailStoreslst = null;
		Integer findRetSeaID = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerFindRetailerSearchPagination");

			simpleJdbcCall.returningResultSet("searchRetailerDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue(ApplicationConstants.USERID, consumerRequestDetails.getUserId());
			scanQueryParams.addValue("SearchKey", consumerRequestDetails.getSearchKey());
			scanQueryParams.addValue("Postalcode", consumerRequestDetails.getPostalCode());
			scanQueryParams.addValue("MainMenuId", consumerRequestDetails.getMainMenuId());
			scanQueryParams.addValue("LowerLimit", consumerRequestDetails.getLowerLimit());
			scanQueryParams.addValue("RecordCount", consumerRequestDetails.getRecordCount());
			scanQueryParams.addValue("CategoryName", consumerRequestDetails.getCategoryName());

			scanQueryParams.addValue("ScreenName", ApplicationConstants.CONSFNDLOCSEARCHPAGINATION);
			// for user tracking

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			retailStoreslst = (List<RetailerDetail>) resultFromProcedure.get("searchRetailerDetails");
			findRetSeaID = (Integer) resultFromProcedure.get("FindRetailerSearchID");

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

				if (null != retailStoreslst && !retailStoreslst.isEmpty())
				{
					retailersDetailsObj = new RetailersDetails();
					retailersDetailsObj.setRetailerDetail(retailStoreslst);
					LOG.info("Retailer found for the search");
					final int maxCount = (Integer) resultFromProcedure.get("MaxCnt");
					retailersDetailsObj.setTotalSize(maxCount);
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							retailersDetailsObj.setNextPage(1);
						}
						else
						{
							retailersDetailsObj.setNextPage(0);
						}
					}

				}

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.ERROROCCURRED + "usp_WebConsumerFindRetailerSearchPagination ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in fetchSearchRetailerDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}

		return retailersDetailsObj;
	}

	/**
	 * This DAO method is used to get Main Menu id which is used for user
	 * tracking.
	 * 
	 * @param consumerRequestDetails
	 *            as a parameter which holds the details required to get main
	 *            menu id from database
	 * @return main menu id
	 * @throws ScanSeeWebSqlException
	 */
	public int getMainMenuID(ConsumerRequestDetails consumerRequestDetails) throws ScanSeeWebSqlException
	{
		Map<String, Object> resultFromProcedure = null;
		int moduleId = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_UserTrackingWebConsumerMainMenuCreation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.APPSCHEMA);
			final MapSqlParameterSource objRetailerLocation = new MapSqlParameterSource();
			objRetailerLocation.addValue("UserID", consumerRequestDetails.getUserId());
			objRetailerLocation.addValue("ModuleName", consumerRequestDetails.getModuleName());
			objRetailerLocation.addValue("Latitude", consumerRequestDetails.getLatitute());
			objRetailerLocation.addValue("Longitude", consumerRequestDetails.getLongitude());
			objRetailerLocation.addValue("Postalcode", consumerRequestDetails.getPostalCode());

			resultFromProcedure = simpleJdbcCall.execute(objRetailerLocation);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{

				moduleId = (Integer) resultFromProcedure.get("MainMenuID");
			}

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTION_OCCURED + "in getMainMenuID method", e);
			throw new ScanSeeWebSqlException(e);
		}
		return moduleId;
	}

	/**
	 * This DAO method is used to get retailer created anything pages.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get
	 *            anything pages
	 * @return anything page list
	 * @throws ScanSeeWebSqlException
	 */
	public List<RetailerPage> getRetailerAnythigPageList(RetailerDetail retailerDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetailerAnythigPageList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseFromProc = null;
		Integer fromProc = null;
		RetailersDetails retailersDetailsObj = null;
		List<RetailerPage> anythingPageList = null;
		Integer findRetSeaID = null;
		String ribbonAdImagePath = null;
		String ribbonAdURL = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerRetailerAnythingPageDisplay");

			simpleJdbcCall.returningResultSet("anythingPageList", new BeanPropertyRowMapper<RetailerPage>(RetailerPage.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue(ApplicationConstants.USERID, retailerDetail.getUserId());
			queryParams.addValue("RetailId", retailerDetail.getRetailerId());
			queryParams.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			queryParams.addValue("MainMenuId", retailerDetail.getMainMenuId());
			queryParams.addValue("RetailerListID", retailerDetail.getRetListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

				anythingPageList = (List<RetailerPage>) resultFromProcedure.get("anythingPageList");
				ribbonAdImagePath = (String) resultFromProcedure.get("ribbonAdImagePath");
				ribbonAdURL = (String) resultFromProcedure.get("ribbonAdURL");

				if (null != anythingPageList && !anythingPageList.isEmpty())
				{

					RetailerPage retailerPage = anythingPageList.remove(0);
					retailerPage.setRibbonAdImagePath(ribbonAdImagePath);
					retailerPage.setRibbonAdURL(ribbonAdURL);
					anythingPageList.add(0, retailerPage);
				}
			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.ERROROCCURRED + "usp_WebConsumerRetailerAnythingPageDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in getRetailerAnythigPageList", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return anythingPageList;
	}

	/**
	 * This DAO method is used to get special offers page list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get special
	 *            offers page list.
	 * @return retailersDetailsObj object having special offers list
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public RetailersDetails getRetailerSpecialOfferPageList(RetailerDetail retailerDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getRetailerSpecialOfferPageList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerPage> spacialOfferPageList = null;
		RetailersDetails retailersDetailsObj = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerRetailerLocationSpecialOffersDisplay");

			simpleJdbcCall.returningResultSet("spacialOfferPageList", new BeanPropertyRowMapper<RetailerPage>(RetailerPage.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue(ApplicationConstants.USERID, retailerDetail.getUserId());
			queryParams.addValue("RetailId", retailerDetail.getRetailerId());
			queryParams.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			queryParams.addValue("RetailerListID", retailerDetail.getRetListID());
			queryParams.addValue("LowerLimit", retailerDetail.getLowerLimit());
			queryParams.addValue("RecordCount", retailerDetail.getRecordCount());
			queryParams.addValue("MainMenuID", retailerDetail.getMainMenuId());
			queryParams.addValue("ScreenName", ApplicationConstants.CONSFNDDEALSSEARCHPAGINATION);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

				spacialOfferPageList = (List<RetailerPage>) resultFromProcedure.get("spacialOfferPageList");

				if (null != spacialOfferPageList && !spacialOfferPageList.isEmpty())
				{
					retailersDetailsObj = new RetailersDetails();
					retailersDetailsObj.setRetailerPageList(spacialOfferPageList);
					LOG.info("Retailer found for the search");
					final int maxCount = (Integer) resultFromProcedure.get("MaxCnt");
					retailersDetailsObj.setTotalSize(maxCount);
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							retailersDetailsObj.setNextPage(1);
						}
						else
						{
							retailersDetailsObj.setNextPage(0);
						}
					}

				}

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.ERROROCCURRED + "usp_WebConsumerRetailerLocationSpecialOffersDisplay ..errorNum..." + errorNum
						+ "errorMsg.." + errorMsg);

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in getRetailerSpecialOfferPageList", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailersDetailsObj;
	}

	/**
	 * This DAO method is used to get sale products list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get sale
	 *            products list.
	 * @return productDetailsObj object having sale product list
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public ProductDetails getSaleProductList(RetailerDetail retailerDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getSaleProductList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> saleProductList = null;
		ProductDetails productDetailsObj = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerRetailLocationDealsDisplay");

			simpleJdbcCall.returningResultSet("saleProductList", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue(ApplicationConstants.USERID, retailerDetail.getUserId());
			queryParams.addValue("RetailId", retailerDetail.getRetailerId());
			queryParams.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			queryParams.addValue("RetailerListID", retailerDetail.getRetListID());
			queryParams.addValue("LowerLimit", retailerDetail.getLowerLimit());
			queryParams.addValue("RecordCount", retailerDetail.getRecordCount());
			queryParams.addValue("MainMenuID", retailerDetail.getMainMenuId());
			queryParams.addValue("ScreenName", ApplicationConstants.CONSFNDDEALSSEARCHPAGINATION);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

				saleProductList = (List<ProductDetail>) resultFromProcedure.get("saleProductList");

				if (null != saleProductList && !saleProductList.isEmpty())
				{
					productDetailsObj = new ProductDetails();
					productDetailsObj.setProductDetail(saleProductList);
					LOG.info("Retailer found for the search");
					final int maxCount = (Integer) resultFromProcedure.get("MaxCnt");
					productDetailsObj.setTotalSize(maxCount);
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							productDetailsObj.setNextPage(1);
						}
						else
						{
							productDetailsObj.setNextPage(0);
						}
					}

				}

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.ERROROCCURRED + "usp_WebConsumerRetailLocationDealsDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in getSaleProductList", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetailsObj;
	}

	/**
	 * This service method is used to get hot deals list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get hot
	 *            deals list.
	 * @return searchResultInfo object having hot deals list
	 * @throws ScanSeeWebSqlException
	 */
	@SuppressWarnings("unchecked")
	public SearchResultInfo getHotDealList(RetailerDetail retailerDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "getHotDealList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SearchResultInfo searchResultInfo = null;
		List<HotDealInfo> hotDealList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerRetailLocationHotDealsDisplay");

			simpleJdbcCall.returningResultSet("hotDealList", new BeanPropertyRowMapper<HotDealInfo>(HotDealInfo.class));
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue(ApplicationConstants.USERID, retailerDetail.getUserId());
			queryParams.addValue("RetailId", retailerDetail.getRetailerId());
			queryParams.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			queryParams.addValue("RetailerListID", retailerDetail.getRetListID());
			queryParams.addValue("LowerLimit", retailerDetail.getLowerLimit());
			queryParams.addValue("RecordCount", retailerDetail.getRecordCount());
			queryParams.addValue("MainMenuID", retailerDetail.getMainMenuId());
			queryParams.addValue("ScreenName", ApplicationConstants.CONSFNDDEALSSEARCHPAGINATION);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

				hotDealList = (List<HotDealInfo>) resultFromProcedure.get("hotDealList");

				if (null != hotDealList && !hotDealList.isEmpty())
				{
					searchResultInfo = new SearchResultInfo();
					searchResultInfo.setDealList(hotDealList);
					LOG.info("Retailer found for the search");
					final int maxCount = (Integer) resultFromProcedure.get("MaxCnt");
					searchResultInfo.setTotalSize(maxCount);
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							searchResultInfo.setNextPage(1);
						}
						else
						{
							searchResultInfo.setNextPage(0);
						}
					}

				}

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.ERROROCCURRED + "usp_WebConsumerRetailLocationHotDealsDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in getHotDealList", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return searchResultInfo;
	}

	/**
	 * This service method is used to get special offer page to be displayed.
	 * 
	 * @param userId
	 *            as a parameter
	 * @param retailId
	 *            as a parameter
	 * @param retailLocId
	 *            as a parameter
	 * @param retailLocListId
	 *            as a parameter
	 * @return hashMap containing the flag for the page to be displayed
	 * @throws ScanSeeWebSqlException
	 */
	public HashMap<String, Boolean> checkDiscountPageToDisplay(Long userId, Integer retailId, Integer retailLocId, Integer retailLocListId)
			throws ScanSeeWebSqlException
	{
		final String methodName = "checkDiscountPageToDisplay";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		HashMap<String, Boolean> mapDiaplayPage = new HashMap<String, Boolean>();
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_WebConsumerRetailLocationSpecialDealsDisplay");
			final MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue(ApplicationConstants.USERID, userId);
			queryParams.addValue("RetailId", retailId);
			queryParams.addValue("RetailLocationID", retailLocId);
			queryParams.addValue("RetailerListID", retailLocListId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(queryParams);

			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				mapDiaplayPage.put(ApplicationConstants.SALEFLAG, (Boolean) resultFromProcedure.get(ApplicationConstants.SALEFLAG));
				mapDiaplayPage.put(ApplicationConstants.SPECIALFLAG, (Boolean) resultFromProcedure.get(ApplicationConstants.SPECIALFLAG));
				mapDiaplayPage.put(ApplicationConstants.HOTDEALFLAG, (Boolean) resultFromProcedure.get(ApplicationConstants.HOTDEALFLAG));
				mapDiaplayPage.put(ApplicationConstants.COUPONFLAG, (Boolean) resultFromProcedure.get(ApplicationConstants.COUPONFLAG));
			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.ERROROCCURRED + "usp_WebConsumerRetailLocationHotDealsDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in checkDiscountPageToDisplay method", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return mapDiaplayPage;
	}
}

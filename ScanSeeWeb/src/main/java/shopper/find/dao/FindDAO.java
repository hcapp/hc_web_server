package shopper.find.dao;

import java.util.HashMap;
import java.util.List;

import common.exception.ScanSeeWebSqlException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.shopper.GoogleCategory;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailerPage;
import common.pojo.shopper.RetailersDetails;

/**
 * @author pradip_k
 */
public interface FindDAO
{

	/**
	 * The DAO method fetches Category Details. no Query parameter
	 * 
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public List<GoogleCategory> getCategoryDetail() throws ScanSeeWebSqlException;

	/**
	 * This method is used to search product based on user entered search key.
	 * It returns the product details like name,image,it based on search key.
	 */
	public ProductDetails fetchAllProduct(SearchForm objForm) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to fetch retail locations.
	 * 
	 * @param consumerRequestDetails
	 *            as a parameter which holds the data required to fetch retail
	 *            locations
	 * @return retailersDetails
	 * @throws ScanSeeWebSqlException
	 */
	public RetailersDetails searchRetailLocation(ConsumerRequestDetails consumerRequestDetails) throws ScanSeeWebSqlException;

	public HotDealsListResultSet findDealsSearch(SearchForm searchFormObj) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to get Main Menu id which is used for user
	 * tracking.
	 * 
	 * @param consumerRequestDetails
	 *            as a parameter which holds the details required to get main
	 *            menu id from database
	 * @return main menu id
	 * @throws ScanSeeWebSqlException
	 */
	public int getMainMenuID(ConsumerRequestDetails consumerRequestDetails) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to get retailer created anything pages.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get
	 *            anything pages
	 * @return anything page list
	 * @throws ScanSeeWebSqlException
	 */
	public List<RetailerPage> getRetailerAnythigPageList(RetailerDetail retailerDetail) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to get special offers page list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get special
	 *            offers page list.
	 * @return retailersDetailsObj object having special offers list
	 * @throws ScanSeeWebSqlException
	 */
	public RetailersDetails getRetailerSpecialOfferPageList(RetailerDetail retailerDetail) throws ScanSeeWebSqlException;

	/**
	 * This DAO method is used to get sale products list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get sale
	 *            products list.
	 * @return productDetailsObj object having sale product list
	 * @throws ScanSeeWebSqlException
	 */
	public ProductDetails getSaleProductList(RetailerDetail retailerDetail) throws ScanSeeWebSqlException;

	/**
	 * This service method is used to get hot deals list.
	 * 
	 * @param retailerDetail
	 *            as a parameter which holds the details required to get hot
	 *            deals list.
	 * @return searchResultInfo object having hot deals list
	 * @throws ScanSeeWebSqlException
	 */
	public SearchResultInfo getHotDealList(RetailerDetail retailerDetail) throws ScanSeeWebSqlException;

	/**
	 * This service method is used to get special offer page to be displayed.
	 * 
	 * @param userId
	 *            as a parameter
	 * @param retailId
	 *            as a parameter
	 * @param retailLocId
	 *            as a parameter
	 * @param retailLocListId
	 *            as a parameter
	 * @return hashMap containing the flag for the page to be displayed
	 * @throws ScanSeeWebSqlException
	 */
	public HashMap<String, Boolean> checkDiscountPageToDisplay(Long userId, Integer retailId, Integer retailLocId, Integer retailLocListId)
			throws ScanSeeWebSqlException;

}

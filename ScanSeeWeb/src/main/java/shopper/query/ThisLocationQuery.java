package shopper.query;

/**
 * The class has queries to be used in the ThisLocation module.
 * 
 * @author malathi_lr
 *
 */
public class ThisLocationQuery {

	/**
	 * For fetching Lat and Long for the given zipcode.
	 */
	public static final  String FETCHLATLONG="SELECT Latitude, Longitude FROM GeoPosition WHERE PostalCode = ?";
}

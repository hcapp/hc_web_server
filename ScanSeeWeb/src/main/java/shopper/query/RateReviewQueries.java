package shopper.query;

/**
 * This class for handling RateReview queries.
 * @author malathi_lr
 *
 */
public class RateReviewQueries {

	/**
	 * For fetching product review details.
	 */
	public static final String  GETPRODUCTREVIEWS = "SELECT ProductReviewsID productReviewsID,ProductID productID,ReviewURL reviewURL,ReviewComments reviewComments FROM ProductReviews WHERE  ProductID=?";

	public static final String  PRODUCTREVIEWS = "SELECT ProductReviewsID productReviewsID,ProductID productID,ReviewURL reviewURL,ReviewComments comments FROM ProductReviews WHERE  ProductID=?";
	/**
	 * constructor for RateReviewQueries.
	 */
     protected RateReviewQueries()
     {
	 
     }
}

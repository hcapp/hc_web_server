package shopper.hotdeals.dao;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import shopper.query.ShoppingListQueries;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.HotDealInfo;
import common.pojo.shopper.Categories;
import common.pojo.shopper.Category;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.HotDealsResultSet;
import common.pojo.shopper.MainCategoryDetail;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.SubCategoryDetail;
import common.util.Utility;

/**
 * This is implementation class for HotDealsDAO. This class has methods for Hot
 * Deals Module. The methods of this class are called from the HotDeals Service
 * layer.
 * 
 * @author shyamsundara_hm
 */
public class HotDealsDAOImpl implements HotDealsDAO

{

	/**
	 * for getting logger...
	 */

	private static final Logger log = Logger.getLogger(HotDealsDAOImpl.class.getName());

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedue.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set ParameterizedBeanPropertyRowMapper to map POJO.
	 */

	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            for setDataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This DAO Implementation fir method fetches hotDeals list.
	 * 
	 * @param hotDealsListRequest
	 *            of HotDealsListRequest
	 * @param screenName
	 *            as query parameter.
	 * @return HotDealsListResultSet
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	public HotDealsListResultSet fetchHotDealsDetails(HotDealsListRequest hotDealsListRequest, String screenName) throws ScanSeeWebSqlException
	{

		List<HotDealsResultSet> hotDealsListResponselst = null;
		HotDealsListResultSet hotDealsListResultSet = null;
		final String methodName = "fetchHotDealsDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName + "requested userid is -->" + hotDealsListRequest.getUserId());

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_HotDealSearchPagination");
			simpleJdbcCall.returningResultSet("hotDealslst", new BeanPropertyRowMapper<HotDealsResultSet>(HotDealsResultSet.class));
			final MapSqlParameterSource fetchHotDealsParameters = new MapSqlParameterSource();
			fetchHotDealsParameters.addValue(ApplicationConstants.USERID, hotDealsListRequest.getUserId());
			fetchHotDealsParameters.addValue("Search", hotDealsListRequest.getSearchItem());
			fetchHotDealsParameters.addValue("ScreenName", screenName);
			fetchHotDealsParameters.addValue("Category", hotDealsListRequest.getCategory());
			fetchHotDealsParameters.addValue("LowerLimit", hotDealsListRequest.getLastVisitedProductNo());
			fetchHotDealsParameters.addValue("PopulationCentreID", hotDealsListRequest.getPopulationCentreID());
			fetchHotDealsParameters.addValue("Latitude", hotDealsListRequest.getLatitude());
			fetchHotDealsParameters.addValue("Longitude", hotDealsListRequest.getLongitude());
			fetchHotDealsParameters.addValue("City", hotDealsListRequest.getCity());
			fetchHotDealsParameters.addValue("Radius", hotDealsListRequest.getRadius());
			fetchHotDealsParameters.addValue("PostalCode", null);
			fetchHotDealsParameters.addValue("MainMenuID", null);
			fetchHotDealsParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			fetchHotDealsParameters.addValue(ApplicationConstants.ERRORMESSAGE, null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHotDealsParameters);
			if (null != resultFromProcedure)
			{
				hotDealsListResultSet = new HotDealsListResultSet();
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsListResponselst = (List<HotDealsResultSet>) resultFromProcedure.get("hotDealslst");
					final Boolean favCat = (Boolean) resultFromProcedure.get("FavCatFlag");
					final Boolean categoryFlag = (Boolean) resultFromProcedure.get("ByCategoryFlag");
					if (null != hotDealsListResponselst && !hotDealsListResponselst.isEmpty())
					{
						hotDealsListResultSet.setHotDealsListResponselst((ArrayList<HotDealsResultSet>) hotDealsListResponselst);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						final Integer maxCount = (Integer) resultFromProcedure.get("MaxCnt");
						hotDealsListResultSet.setRowCount(maxCount);
						if (nextpage != null)
						{
							if (nextpage)
							{
								hotDealsListResultSet.setNextPage(1);
							}
							else
							{
								hotDealsListResultSet.setNextPage(0);
							}
						}
						if (favCat != null)
						{
							if (favCat)
							{
								hotDealsListResultSet.setFavCat(1);

							}
							else
							{
								hotDealsListResultSet.setFavCat(0);

							}
						}
					}
					if (favCat != null)
					{
						if (favCat)
						{
							hotDealsListResultSet.setFavCat(1);

						}
						else
						{
							hotDealsListResultSet.setFavCat(0);

						}
					}
					if (categoryFlag != null)
					{
						if (categoryFlag)
						{
							hotDealsListResultSet.setCategoryFlag(1);

						}
						else
						{
							hotDealsListResultSet.setCategoryFlag(0);

						}
					}
				}

			}

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_HotDealSearchPagination Store Procedure error number: {}" + errorNum + "  and error message: {}"
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchHotDealsDetails", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info("Response from server is " + hotDealsListResponselst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsListResultSet;
	}

	/**
	 * This DAO Implementation method fetches each hot deals information.
	 * 
	 * @param userId
	 *            as query parameter
	 * @param hotDealId
	 *            as query parameter
	 * @return HotDealsListResponse
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	public HotDealsCategoryInfo getHotDealProductDetail(Integer userId, Integer hotDealId) throws ScanSeeWebSqlException
	{
		final String methodName = "getHotDealProductDetail in DAO layer";
		final List<HotDealsDetails> hotDealsListResponselst;
		log.info(ApplicationConstants.METHODSTART + methodName + "Requested user id is -->" + userId);
		HotDealsCategoryInfo hotDealsCategoryInfolst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_HotDealDetails");
			simpleJdbcCall.returningResultSet("HotDealDetails", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final SqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource().addValue("UserID", userId).addValue("HotDealID",
					hotDealId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					hotDealsListResponselst = (List<HotDealsDetails>) resultFromProcedure.get("HotDealDetails");
					if (null != hotDealsListResponselst && !hotDealsListResponselst.isEmpty())
					{
						hotDealsCategoryInfolst = new HotDealsCategoryInfo();
						hotDealsCategoryInfolst.setHotDealsDetailsArrayLst((ArrayList<HotDealsDetails>) hotDealsListResponselst);
					}
					else
					{
						log.info("No Products found for the search");
						return hotDealsCategoryInfolst;
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred  in usp_HotDealDetails Store Procedure error number: {} " + errorNum + " and  error message: {}"
							+ errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getHotDealProductDetail ", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info("Response frm server in getHotDealProductDetail " + hotDealsCategoryInfolst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsCategoryInfolst;
	}

	/**
	 * This DAO Implementation method removes hot deals .
	 * 
	 * @param hotDealsListRequest
	 *            as query parameter
	 * @return response based on success or failure.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String removeHDProduct(HotDealsListRequest hotDealsListRequest) throws ScanSeeWebSqlException
	{
		final String methodName = "removeHDProduct in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + hotDealsListRequest.getUserId());
		String responseFromProc = null;
		Integer fromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_HotDealInterest");
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", hotDealsListRequest.getUserId());
			scanQueryParams.addValue("HotDealID", hotDealsListRequest.getHotDealId());
			scanQueryParams.addValue("Interested", hotDealsListRequest.gethDInterested());
			scanQueryParams.addValue("InterestDate", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;
			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in removeHDProduct method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in removeHDProduct", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		catch (ParseException exception)
		{
			log.error("Exception occurred in removeHDProduct", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info("Response frm removeHDProduct " + responseFromProc);
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * This DAO Implementation method for getting hot deals category list . No
	 * query parameter.
	 * 
	 * @param userId
	 *            as request parameter
	 * @return response based on success or failure.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	public List<HotDealsCategoryInfo> getHdCategoryDetail(Long userId) throws ScanSeeWebSqlException
	{
		log.info("Inside HotDealsDAOImpl : getHdCategoryDetail ");
		List<HotDealsCategoryInfo> hotDealsCategorylst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_HotDealsCategoryList");
			simpleJdbcCall.returningResultSet("HotDealCategorylist", new BeanPropertyRowMapper<HotDealsCategoryInfo>(HotDealsCategoryInfo.class));

			final MapSqlParameterSource hotDealCategoryQueryParams = new MapSqlParameterSource();
			hotDealCategoryQueryParams.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(hotDealCategoryQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsCategorylst = (List<HotDealsCategoryInfo>) resultFromProcedure.get("HotDealCategorylist");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_HotDealDetails  Store Procedure error number: {} " + errorNum + " and error  message: {}"
							+ errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Inside HotDealsDAOImpl : getHdCategoryDetail", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info("Inside HotDealsDAOImpl : getHdCategoryDetail : Response " + hotDealsCategorylst);
		return hotDealsCategorylst;
	}

	/**
	 * This DAO Implementation method for getting hot deals population centres .
	 * No query parameter.
	 * 
	 * @return response based on success or failure.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public List<HotDealsCategoryInfo> getHdPopulationCenters() throws ScanSeeWebSqlException
	{
		final String methodName = "getHdPopulationCenters in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsCategoryInfo> hotDealsCategorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_HotDealPopulationCentres");
			simpleJdbcCall.returningResultSet("HotDealCategorylist", new BeanPropertyRowMapper<HotDealsCategoryInfo>(HotDealsCategoryInfo.class));

			final SqlParameterSource fetchHotDealCategoryParameters = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHotDealCategoryParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsCategorylst = (List<HotDealsCategoryInfo>) resultFromProcedure.get("HotDealCategorylist");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_HotDealDetails Store Procedure error number: {} " + errorNum + " and error message: {}"
							+ errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getHotDealProductDetail", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info("Response frm server in getHotDealProductDetail" + hotDealsCategorylst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsCategorylst;
	}

	/**
	 * This DAO Implementation method for getting hot deals population centres .
	 * No query parameter.
	 * 
	 * @param dmaName
	 *            as request parameter
	 * @return response based on success or failure.
	 * @throws ScanSeeWebSqlException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	public List<HotDealsCategoryInfo> searchHdPopulationCenters(String dmaName) throws ScanSeeWebSqlException
	{
		final String methodName = "getHdPopulationCenters in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsCategoryInfo> hotDealsCategorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_HotDealSearchPopulationCenter");
			simpleJdbcCall.returningResultSet("HotDealCategorylist", new BeanPropertyRowMapper<HotDealsCategoryInfo>(HotDealsCategoryInfo.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("PopulationCenter", dmaName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsCategorylst = (List<HotDealsCategoryInfo>) resultFromProcedure.get("HotDealCategorylist");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_HotDealDetails Store Procedure error number: {} " + errorNum + " and error message: {}"
							+ errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getHotDealProductDetail", exception);
			throw new ScanSeeWebSqlException(exception);
		}
		log.info("Response frm server in getHotDealProductDetail" + hotDealsCategorylst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsCategorylst;
	}

	/**
	 * The DAO method for fetching user favourite categories.
	 * 
	 * @param userId
	 *            for which the information to be fetched.
	 * @param lowerLimit
	 *            used for pagination
	 * @return CategoryDetail object.
	 * @throws ScanSeeWebSqlException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public CategoryDetail fetchUserFavCategories(Long userId, Integer lowerLimit) throws ScanSeeWebSqlException
	{
		log.info("Inside HotDealsDAOImpl : fetchUserFavCategories ");
		List<SubCategoryDetail> subList = null;
		final List<MainCategoryDetail> mainList = new ArrayList<MainCategoryDetail>();
		MainCategoryDetail mainCategoryDetail = null;
		CategoryDetail categoryDetailResp = null;

		final Map<String, Object> resultFromProcedure;

		List<CategoryDetail> categoryDetailList = null;

		int preMainCategorId = 0;
		CategoryDetail categoryDetail = null;
		int currentCatId;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_PreferredCategoryDisplay");
			simpleJdbcCall.returningResultSet("fetchPreferredCategories", new BeanPropertyRowMapper<CategoryDetail>(CategoryDetail.class));

			final MapSqlParameterSource fetchPreferredRetailersParams = new MapSqlParameterSource();
			fetchPreferredRetailersParams.addValue("UserID", userId);
			// fetchPreferredRetailersParams.addValue("LowerLimit", lowerLimit);
			// fetchPreferredRetailersParams.addValue("ScreenName",
			// ApplicationConstants.USERFAVCATEGORIES);
			resultFromProcedure = simpleJdbcCall.execute(fetchPreferredRetailersParams);
			categoryDetailList = (List<CategoryDetail>) resultFromProcedure.get("fetchPreferredCategories");

		}
		catch (DataAccessException e)
		{
			log.error("Inside HotDealsDAOImpl : fetchUserFavCategories " + e.getMessage());
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		for (Iterator<CategoryDetail> iterator = categoryDetailList.iterator(); iterator.hasNext();)
		{
			categoryDetail = iterator.next();
			currentCatId = categoryDetail.getParentCategoryID();
			if (preMainCategorId == 0)
			{
				preMainCategorId = categoryDetail.getParentCategoryID();
				mainCategoryDetail = new MainCategoryDetail();
				mainCategoryDetail.setMainCategoryId(preMainCategorId);
				mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
				subList = new ArrayList<SubCategoryDetail>();
			}
			if (currentCatId != preMainCategorId)
			{
				mainCategoryDetail.setSubCategoryDetailLst(subList);
				mainList.add(mainCategoryDetail);
				subList = new ArrayList<SubCategoryDetail>();
				final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
				subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
				subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				// subCategoryDetail.setRowNum(categoryDetail.getRowNum());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				subCategoryDetail.setDisplayed(categoryDetail.getDisplayed());
				subList.add(subCategoryDetail);
				mainCategoryDetail = new MainCategoryDetail();
				mainCategoryDetail.setMainCategoryId(currentCatId);
				mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());

				preMainCategorId = categoryDetail.getParentCategoryID();
			}
			else
			{
				final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
				subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
				subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				subCategoryDetail.setDisplayed(categoryDetail.getDisplayed());
				// subCategoryDetail.setRowNum(categoryDetail.getRowNum());
				subList.add(subCategoryDetail);
			}
		}

		if (null != mainList && !mainList.isEmpty())
		{
			categoryDetailResp = new CategoryDetail();
			categoryDetailResp.setMainCategoryDetaillst(mainList);
			/*
			 * final Boolean nextpage = (Boolean)
			 * resultFromProcedure.get("NxtPageFlag"); if (nextpage) {
			 * categoryDetailResp.setNextPage(1); } else {
			 * categoryDetailResp.setNextPage(0); }
			 */

		}
		return categoryDetailResp;
	}

	/**
	 * The DAo method for saving user favourite categories in the database.
	 * 
	 * @param categoryDetail
	 *            categories details.
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public String createOrUpdateFavCategories(String categoryId, Long userID) throws ScanSeeWebSqlException

	{
		log.info("Inside HotDealsDAOImpl : createOrUpdateFavCategories ");
		int fromProc = 0;
		final Date currentDate = new Date();

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_PreferredCategory");
			SqlParameterSource updateFavCategoriesParams;
			updateFavCategoriesParams = new MapSqlParameterSource()

			.addValue("userID", userID).addValue("Category", categoryId).addValue("Date", new java.sql.Timestamp(currentDate.getTime()));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(updateFavCategoriesParams);
			fromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

		}
		catch (DataAccessException e)
		{
			log.error("Inside HotDealsDAOImpl : createOrUpdateFavCategories " + e.getMessage());
			throw new ScanSeeWebSqlException(e.getMessage());
		}
		if (fromProc == 1)
		{
			log.info("Inside HotDealsDAOImpl : createOrUpdateFavCategories : Failure :" + ApplicationConstants.FAILURE);
			return ApplicationConstants.FAILURE;
		}
		else
		{
			log.info("Inside HotDealsDAOImpl : createOrUpdateFavCategories : Success : " + ApplicationConstants.SUCCESS);
			return ApplicationConstants.SUCCESS;
		}
	}


	public String getHotDealShareThruEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeWebSqlException
	{

		final String methodName = "getShareHotDealInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> hotDealListlst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_HotDealDetails");
			simpleJdbcCall.returningResultSet("shareCouponInfo", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

					final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("HotdealID", shareProductInfoObj.getHotdealId()).addValue("UserID", shareProductInfoObj.getUserId());

			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					hotDealListlst = (List<HotDealsDetails>) resultFromProcedure.get("shareCouponInfo");
					if (!hotDealListlst.isEmpty() && hotDealListlst != null)
					{

						response = Utility.formHotDealInfoHTML(hotDealListlst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_HotDealShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeWebSqlException(errorMsg);
				}
			}
		}
		catch (InvalidKeyException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeWebSqlException
	{

		final String methodName = "getStockInfo";

		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.APPSCHEMA);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_GetScreenContent Store Procedure with error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeWebSqlException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeWebSqlException(e);
		}

		return appConfigurationList;
	}


	public String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeWebSqlException
	{

		final String methodName = "getUserInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			response = this.jdbcTemplate.queryForObject(ShoppingListQueries.FETCHUSEREMAILID, new Object[] { shareProductInfo.getUserId() },
					String.class);

		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeWebSqlException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}

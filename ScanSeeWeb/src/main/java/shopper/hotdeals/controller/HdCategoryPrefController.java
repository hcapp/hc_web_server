/**
 * @ (#) ProductUPCListHotDeal.java 15-Feb-2012
 * Project       :ScanSeeWeb
 * File          : HdCategoryPrefController.java
 * Author        : Kumar D
 * Company       : Span Systems Corporation
 * Date Created  : 15-Feb-2012
 */
package shopper.hotdeals.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import shopper.hotdeals.service.HotDealsService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Users;
import common.pojo.shopper.*;

/**
 * @author kumar_dodda
 */

@Controller
public class HdCategoryPrefController
{
	/**
	 * Getting the logger Instance.
	 */
	final public String RETURN_VIEW = "consdisplayhotdeals";

	private static final Logger log = LoggerFactory.getLogger(HdCategoryPrefController.class);

	@RequestMapping(value = "/hdcategoryPref.htm", method = RequestMethod.GET)
	public ModelAndView showHdCategoryPage(@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
	{
		log.info("Inside HdCategoryPrefController : showHdCategoryPage");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");
		CategoryDetail objCategoryDetail = null;
		Long userID = null;
		Users objLogUser = (Users) request.getSession().getAttribute("loginuser");
		session.setAttribute("hotDealsList", null);
		session.setAttribute("populationcenterList", null);
		session.setAttribute("hotDealDetails", null);
		session.setAttribute("displaymap", false);

		if(objLogUser !=null)
		{
		userID = objLogUser.getUserID();
		}
		try
		{
			objCategoryDetail = hotDealsService.getUserFavCategories(userID, 0);
			session.setAttribute("dealsCategoryList", objCategoryDetail);
			session.setAttribute("displayNarrowList", false);
		}
		catch (ScanSeeServiceException e)
		{
			log.info("Inside HdCategoryPrefController : showHdCategoryPage : " + e.getMessage());
			e.printStackTrace();
		}

		return new ModelAndView(RETURN_VIEW);
	}

	@RequestMapping(value = "/hdcategoryPref.htm", method = RequestMethod.POST)
	public 
	
	@ResponseBody
	String saveHdCategoryPage(@RequestParam(value = "category", required = true) String strCategory,
			@ModelAttribute("hotdealmainpage") HotDealsCategoryInfo hotDealInfo, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpSession session1)
	{
		log.info("Inside HdCategoryPrefController : showHdCategoryPage");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		HotDealsService hotDealsService = (HotDealsService) appContext.getBean("hotDealsService");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String response = null;
		Long userId=null;
		if(loginUser!=null){
		 userId = loginUser.getUserID();
		}
		session.setAttribute("displayNarrowList",false);
		session.setAttribute("displaymap", false);
		try
		{
			response = hotDealsService.setUserFavCategories(strCategory, userId);
		}
		catch (ScanSeeServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(response.equals(ApplicationConstants.SUCCESS)){
			response = ApplicationConstants.SUCCESS;
		}else{
			response = ApplicationConstants.FAILURE;
		}

		return response;

	}
}

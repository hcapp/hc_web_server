package shopper.hotdeals.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.find.service.FindServiceImpl;
import shopper.hotdeals.dao.ConsumerHotDealDao;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.shopper.HotDealAPIResultSet;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.HotDealsResultSet;

public class ConsumerHotDealServiceImpl implements ConsumerHotDealService
{
	
	private ConsumerHotDealDao consumerHotDealDao;
	/**
	 * Getting the logger instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(FindServiceImpl.class.getName());

	/**
	 * This methods to get find DAO.
	 * 
	 * @return the findDAO
	 */
	public ConsumerHotDealDao getConsumerHotDealDao()
	{
		return consumerHotDealDao;
	}

	/**
	 * This method to set find DAO.
	 * 
	 * @param findDAO
	 *            the findDAO to set
	 */
	public void setConsumerHotDealDao(ConsumerHotDealDao consumerHotDealDao)
	{
		this.consumerHotDealDao = consumerHotDealDao;
	}

	/**
	 * The service implementation method for fetching hot deals list. Calls the
	 * XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeServiceException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public HotDealsListResultSet getHotDealslst(HotDealsListRequest hotDealsListRequest) throws ScanSeeServiceException
	{
		final String methodName = "getHotDealslst method";
		log.info(ApplicationConstants.METHODSTART + methodName);
		HotDealsListResultSet hotDealsListResultSet = null;
		List<HotDealsCategoryInfo> hotDealsCategoryInfo = null;
		ArrayList<HotDealsDetails> hotDealsDetailsArrayLst = null;
		ArrayList<HotDealAPIResultSet> hotDealsCategoryInfolst = null;
		final List<HotDealsCategoryInfo> hotDealsCategoryInfoList = new ArrayList<HotDealsCategoryInfo>();
		String response = null;

		ArrayList<HotDealsResultSet> hotDealsDetailsResultSet;
		HotDealsListResultSet dbDealsResult;
		try
		{
			dbDealsResult = consumerHotDealDao.fetchHotDealsDetails(hotDealsListRequest, ApplicationConstants.HOTDEALSSEARCHSCREEN);
			hotDealsDetailsResultSet = (ArrayList<HotDealsResultSet>) dbDealsResult.getHotDealsListResponselst();
			if (null != hotDealsDetailsResultSet && !hotDealsDetailsResultSet.isEmpty())
			{
				hotDealsListResultSet = HotDealsHelper.getHotDealsList(hotDealsDetailsResultSet);
				if (null != hotDealsListResultSet)
				{
					hotDealsCategoryInfo = hotDealsListResultSet.getHotDealsCategoryInfo();
					if (null != hotDealsCategoryInfo && !hotDealsCategoryInfo.isEmpty())
					{
						for (HotDealsCategoryInfo hotDealsCategory : hotDealsCategoryInfo)
						{
							final HotDealsCategoryInfo hotDealsCategoryAPIInfo = new HotDealsCategoryInfo();
							hotDealsCategoryAPIInfo.setCategoryName(hotDealsCategory.getCategoryName());
							hotDealsCategoryAPIInfo.setCategoryId(hotDealsCategory.getCategoryId());
							hotDealsDetailsArrayLst = hotDealsCategory.getHotDealsDetailsArrayLst();
							hotDealsCategoryInfolst = HotDealsHelper.getHotDealsAPIList(hotDealsDetailsArrayLst);
							hotDealsCategoryAPIInfo.setHotDealAPIResultSetLst(hotDealsCategoryInfolst);
							hotDealsCategoryInfoList.add(hotDealsCategoryAPIInfo);
						}
					}
				}
				hotDealsListResultSet.setHotDealsCategoryInfo(hotDealsCategoryInfoList);
				hotDealsListResultSet.setNextPage(dbDealsResult.getNextPage());
				hotDealsListResultSet.setFavCat(dbDealsResult.getFavCat());
				hotDealsListResultSet.setCategoryFlag(dbDealsResult.getCategoryFlag());
				hotDealsListResultSet.setRowCount(dbDealsResult.getRowCount());
				hotDealsListResultSet.setDealCategories(dbDealsResult.getDealCategories());
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return hotDealsListResultSet;
	}


}

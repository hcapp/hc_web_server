package shopper.hotdeals.service;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.hotdeals.dao.HotDealsDAO;

import com.scansee.externalapi.common.helper.XstreamParserHelper;
import common.constatns.ApplicationConstants;
import common.email.EmailComponent;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.AppConfiguration;
import common.pojo.shopper.Categories;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.HotDealAPIResultSet;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.HotDealsResultSet;
import common.pojo.shopper.ShareProductInfo;

/**
 * This class is implementation of HotDealsService methods.
 * 
 * @author shyamsundara_hm
 */
public class HotDealsServiceImpl implements HotDealsService
{

	/**
	 * Getting the logger instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(HotDealsServiceImpl.class.getName());
	/**
	 * The variable of type HotDealsDAO.
	 */

	private HotDealsDAO hotDealsDao;

	/**
	 * Setter method for HotDealsDAO.
	 * 
	 * @param hotDealsDao
	 *            the Variable of type HotDealsDAO
	 */

	public void setHotDealsDao(HotDealsDAO hotDealsDao)
	{
		this.hotDealsDao = hotDealsDao;
	}

	
	/**
	 * The service implementation method for fetching hot deals list. Calls the
	 * XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeServiceException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public HotDealsListResultSet getHotDealslst(HotDealsListRequest hotDealsListRequest) throws ScanSeeServiceException
	{
		final String methodName = "getHotDealslst";
		log.info(ApplicationConstants.METHODSTART + methodName);
		HotDealsListResultSet hotDealsListResultSet = null;
		List<HotDealsCategoryInfo> hotDealsCategoryInfo = null;
		ArrayList<HotDealsDetails> hotDealsDetailsArrayLst = null;
		ArrayList<HotDealAPIResultSet> hotDealsCategoryInfolst = null;
		final List<HotDealsCategoryInfo> hotDealsCategoryInfoList = new ArrayList<HotDealsCategoryInfo>();
		String response = null;

		ArrayList<HotDealsResultSet> hotDealsDetailsResultSet;
		HotDealsListResultSet dbDealsResult;
		try
		{
			dbDealsResult = hotDealsDao.fetchHotDealsDetails(hotDealsListRequest, ApplicationConstants.HOTDEALSSEARCHSCREEN);
			hotDealsDetailsResultSet = (ArrayList<HotDealsResultSet>) dbDealsResult.getHotDealsListResponselst();
			if (null != hotDealsDetailsResultSet && !hotDealsDetailsResultSet.isEmpty())
			{
				hotDealsListResultSet = HotDealsHelper.getHotDealsList(hotDealsDetailsResultSet);
				if (null != hotDealsListResultSet)
				{
					hotDealsCategoryInfo = hotDealsListResultSet.getHotDealsCategoryInfo();
					if (null != hotDealsCategoryInfo && !hotDealsCategoryInfo.isEmpty())
					{
						for (HotDealsCategoryInfo hotDealsCategory : hotDealsCategoryInfo)
						{
							final HotDealsCategoryInfo hotDealsCategoryAPIInfo = new HotDealsCategoryInfo();
							hotDealsCategoryAPIInfo.setCategoryName(hotDealsCategory.getCategoryName());
							hotDealsCategoryAPIInfo.setCategoryId(hotDealsCategory.getCategoryId());
							hotDealsDetailsArrayLst = hotDealsCategory.getHotDealsDetailsArrayLst();
							hotDealsCategoryInfolst = HotDealsHelper.getHotDealsAPIList(hotDealsDetailsArrayLst);
							hotDealsCategoryAPIInfo.setHotDealAPIResultSetLst(hotDealsCategoryInfolst);
							hotDealsCategoryInfoList.add(hotDealsCategoryAPIInfo);
						}
					}
				}
				hotDealsListResultSet.setHotDealsCategoryInfo(hotDealsCategoryInfoList);
				hotDealsListResultSet.setNextPage(dbDealsResult.getNextPage());
				hotDealsListResultSet.setFavCat(dbDealsResult.getFavCat());
				hotDealsListResultSet.setCategoryFlag(dbDealsResult.getCategoryFlag());
				hotDealsListResultSet.setRowCount(dbDealsResult.getRowCount());
			}

		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return hotDealsListResultSet;
	}

	/**
	 * The service implementation method for getHotDeal information. Calls the
	 * XStreams helper class methods for parsing.
	 * 
	 * @param userId
	 *            the request .
	 * @param hotDealId
	 *            the request parameter.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeServiceException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public HotDealsDetails getHdProdInfo(Integer userId, Integer hotDealId) throws ScanSeeServiceException
	{
		final String methodName = "getHdProdInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		HotDealsCategoryInfo hotDealsCategoryInfo = null;
		HotDealsDetails hotDealInfo = null;
		try
		{
			hotDealsCategoryInfo = hotDealsDao.getHotDealProductDetail(userId, hotDealId);
			ArrayList<HotDealsDetails> hotDealsDetailsArrayLst = hotDealsCategoryInfo.getHotDealsDetailsArrayLst();

			if (!hotDealsDetailsArrayLst.isEmpty())
			{
				hotDealInfo = hotDealsDetailsArrayLst.get(0);

			}

		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealInfo;
	}

	/**
	 * The service implementation method for remove Hot deals. Calls the XStream
	 * helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request .
	 * @return response string with success code or error code.
	 * @throws ScanSeeServiceException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String removeHDProd(HotDealsListRequest hotDealsListRequest) throws ScanSeeServiceException
	{
		final String methodName = "removeHDProd";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();

		try
		{
			response = hotDealsDao.removeHDProduct(hotDealsListRequest);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (null != response)
		{
			// / response =
			// Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
			// ApplicationConstants.HOTDEALSUPDATETEXT);
		}
		else
		{
			// response =
			// Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE,
			// ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This service implementation method for getting hot deals Category. Calls
	 * the XStreams helper class methods for parsing.
	 * 
	 * @param userId
	 *            as request parameter
	 * @return response string with success code or error code.
	 * @throws ScanSeeServiceException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public List<HotDealsCategoryInfo> getHdCategoryInfo(Long userId) throws ScanSeeServiceException
	{
		log.info("Inside HotDealsServiceImpl : getHdCategoryInfo ");
		HotDealsCategoryInfo objHDCategoryInfo = null;
		List<HotDealsCategoryInfo> hotDealCategorylst = null;
		try
		{
			hotDealCategorylst = hotDealsDao.getHdCategoryDetail(userId);
			if (!hotDealCategorylst.isEmpty() && hotDealCategorylst != null)
			{
				objHDCategoryInfo = hotDealCategorylst.get(0);
			}
		}
		catch (ScanSeeWebSqlException e)
		{
			log.error("Inside HotDealsServiceImpl : getHdCategoryInfo : " + e.getMessage());
			e.printStackTrace();
		}

		log.info("Inside HotDealsServiceImpl : getHdCategoryInfo : Response ");
		return hotDealCategorylst;
	}

	/**
	 * This service implementation method for getting hot deals population
	 * center. Calls the XStreams helper class methods for parsing.
	 * 
	 * @return response string with population centers
	 * @throws ScanSeeServiceException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public List<HotDealsCategoryInfo> getHdPopulationCenters() throws ScanSeeServiceException
	{
		final String methodName = "getHdPopulationCenters";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<HotDealsCategoryInfo> hotDealCategorylst = null;
		try
		{
			hotDealCategorylst = hotDealsDao.getHdPopulationCenters();
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealCategorylst;
	}

	/**
	 * This service implementation method for searching hot deals population
	 * center. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param dmaName
	 *            as request parameter
	 * @return response string with population centers
	 * @throws ScanSeeServiceException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public List<HotDealsCategoryInfo> searchHdPopulationCenters(String dmaName) throws ScanSeeServiceException
	{
		final String methodName = "getHdPopulationCenters";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsCategoryInfo> hotDealCategorylst = null;

		try
		{
			hotDealCategorylst = hotDealsDao.searchHdPopulationCenters(dmaName);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealCategorylst;
	}

	/**
	 * The service method for fetching user favorite categories information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @param lowerlimit
	 *            for pagination
	 * @return user favourite categories information in the response.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public CategoryDetail getUserFavCategories(Long userId, Integer lowerlimit) throws ScanSeeServiceException
	{
		log.info("Inside HotDealServiceImpl : getUserFavCategories ");
		CategoryDetail objcategoryDetail = null;
		try
		{
			objcategoryDetail = hotDealsDao.fetchUserFavCategories(userId, lowerlimit);
		}
		catch (ScanSeeWebSqlException exception)
		{
			log.error("Inside HotDealServiceImpl : getUserFavCategories : " + exception.getMessage());
			throw new ScanSeeServiceException(exception.getMessage(), exception);
		}
		return objcategoryDetail;
	}

	/**
	 * The Service method for Saving User favourite categories Information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param Categories
	 *            Contains user favourite categories information.
	 * @return returns SUCCESS or ERROR.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be
	 *             thrown.
	 */

	public String setUserFavCategories(String categoryId, Long userID) throws ScanSeeServiceException
	{
		log.info("Inside HotDealServiceImpl : setUserFavCategories ");
		String response = null;
		try
		{
			response = hotDealsDao.createOrUpdateFavCategories(categoryId, userID);
		}
		catch (ScanSeeWebSqlException e)
		{
			log.error("Inside HotDealServiceImpl : setUserFavCategories : " + e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		}
		return response;
	}

	public String sharHotDealbyEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeServiceException
	{
		final String methodName = " shareCLRbyEmail of Service layer ";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String dealInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = null;
		String msgBody = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;

		try
		{

			if (null != shareProductInfoObj)
			{

				dealInfo = hotDealsDao.getHotDealShareThruEmail(shareProductInfoObj);

				subject = ApplicationConstants.SHAREHOTDEALINFO;

				userEmailId = hotDealsDao.getUserInfo(shareProductInfoObj);
				if (userEmailId == null)
				{
					log.info("Validation failed::::To Email is not available");

					response = ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE;
					return response;
				}
				toAddress = shareProductInfoObj.getToEmail();
				emailConf = hotDealsDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				msgBody = dealInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				log.info("Mail delivered to:" + toAddress);
				// response =
				// Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
				// ApplicationConstants.SHAREPRODUCTINFO);
				response = ApplicationConstants.SHAREPRODUCTINFO;
			}
		}
		catch (MessagingException exception)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = ApplicationConstants.INVALIDEMAILADDRESSTEXT;
		}
		catch (ScanSeeWebSqlException e)
		{
			log.error("Inside HotDealServiceImpl : setUserFavCategories : " + e.getMessage());
			throw new ScanSeeServiceException(e.getCause());
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
}

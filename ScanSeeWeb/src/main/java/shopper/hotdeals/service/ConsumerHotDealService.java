package shopper.hotdeals.service;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;

public interface ConsumerHotDealService
{
	
	public HotDealsListResultSet getHotDealslst(HotDealsListRequest hotDealsListRequest) throws ScanSeeServiceException;

}

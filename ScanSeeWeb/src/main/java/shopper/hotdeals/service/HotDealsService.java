package shopper.hotdeals.service;

import java.util.List;

import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.shopper.Categories;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.HotDealsCategoryInfo;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.HotDealsListResultSet;
import common.pojo.shopper.ShareProductInfo;


/**
 * This interface for hot deals methods, which are implemented by hotdealservice
 * implementation class.
 * 
 * @author shyamsundara_hm
 */
public interface HotDealsService
{

	/**
	 * The service method For displaying hot deals list based on category wise.
	 * 
	 * @param xml
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeServiceException
	 *             for exception
	 */

	HotDealsListResultSet getHotDealslst(HotDealsListRequest hotDealsListRequest) throws ScanSeeServiceException;

	/**
	 * The service method For retrieving each hot deals details.
	 * 
	 * @param userId
	 *            as request
	 * @param hotDealId
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeServiceException
	 *             for exception
	 */

	HotDealsDetails getHdProdInfo(Integer userId, Integer hotDealId) throws ScanSeeServiceException;

	/**
	 * The service method For remove hot deal details.
	 * 
	 * @param xml
	 *            as request
	 * @return String based success or failure
	 * @throws ScanSeeServiceException
	 *             for exception
	 */

	String removeHDProd(HotDealsListRequest hotDealsListRequest) throws ScanSeeServiceException;

	/**
	 * The service method For displaying hot deal Categories. No query parameter
	 * @param userId as request parameter
	 * @return String based success or failure
	 * @throws ScanSeeServiceException
	 *             for exception
	 */

	List<HotDealsCategoryInfo> getHdCategoryInfo(Long userId) throws ScanSeeServiceException;

	/**
	 * The service method For displaying hot deal Categories. No query parameter
	 * 
	 * @return String based success or failure
	 * @throws ScanSeeServiceException
	 *             for exception
	 */

	List<HotDealsCategoryInfo> getHdPopulationCenters() throws ScanSeeServiceException;

	/**
	 * The service method For displaying hot deal Categories. No query parameter
	 * @param dmaName as request parameter
	 * @return String based success or failure
	 * @throws ScanSeeServiceException
	 *             for exception
	 */

	List<HotDealsCategoryInfo> searchHdPopulationCenters(String dmaName) throws ScanSeeServiceException;
	
		/**
	 * The service method for fetching user favorite categories information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @param lowerlimit
	 *            for pagination
	 * @return user favourite categories information in the
	 *         response.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	CategoryDetail getUserFavCategories(Long userId, Integer lowerlimit) throws ScanSeeServiceException;
	
	/**
	 * The Service method for Saving User favourite categories Information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param Categories
	 *            Contains user favourite categories information.
	 * @return returns  SUCCESS or ERROR.
	 * @throws ScanSeeServiceException
	 *             If any exception occurs ScanSeeServiceException will be thrown.
	 */
	String setUserFavCategories(String categoryId, Long userID) throws  ScanSeeServiceException;
	
	String sharHotDealbyEmail(ShareProductInfo shareProductInfoObj) throws ScanSeeServiceException;
}

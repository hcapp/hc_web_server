package shopper.scannow.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.find.service.FindService;
import shopper.scannow.service.ScanNowService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.Users;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is a controller class for scan now screens.
 * 
 * @author shyamsundara_hm
 */
@Controller
public class ScanNowController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ScanNowController.class);

	/**
	 * This method is used to provides scan now search functionality
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */

	@RequestMapping(value = "/scannow.htm", method = RequestMethod.GET)
	public ModelAndView getScanNowSearch(HttpServletRequest request, HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getWishListProducts of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

		ScanNowService scanNowService = (ScanNowService) appContext.getBean("scanNowService");

		String responseXML = null;
		session.removeAttribute("pagination");
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info("Exited in getScanNowSearch Get Method....");

		return new ModelAndView("scannow");
	}

	@RequestMapping(value = "/scannowprodsearch.htm", method = RequestMethod.POST)
	public ModelAndView productSearch(@ModelAttribute("scannowform") SearchForm searchForm, HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1)
	{
		final String methodName = "getWishListProducts of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		int currentPage = 1;
		String pageNumber = "0";
		String searhKey = null;
		if (searchForm.getSearchKey() == null)
		{
			searchForm = (SearchForm) session.getAttribute("scannowform");
		}

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ScanNowService scanNowService = (ScanNowService) appContext.getBean("scanNowService");
		FindService findService = (FindService) appContext.getBean("findService");

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		Pagination objPage = null;
		SearchForm objForm = null;
		/*
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 */
		if (null != pageFlag && pageFlag.equals("true"))
		{
			searchForm = (SearchForm) session.getAttribute("scannowform");
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}
		else
		{

			searhKey = searchForm.getSearchKey();

		}
		try
		{
			searchForm.setUserId(loginUser.getUserID());
			ProductDetails productDetailsObj = findService.searchProducts(searchForm);

			if (null != productDetailsObj)
			{
				objPage = Utility.getPagination(productDetailsObj.getTotalSize(), currentPage, "scannowprodsearch.htm");

			}

			if (productDetailsObj != null)
			{
				request.setAttribute("searchresults", productDetailsObj);

			}
			else
			{
				request.setAttribute("message", "No product found.");
			}

			session.setAttribute("prodSearch", "true");
			session.setAttribute("pagination", objPage);
			session.setAttribute("scannowform", searchForm);
			session.setAttribute("fromscannow", "fromscannow");
			session.removeAttribute("FromSLSearch");
			session.removeAttribute("fromSLFavSearch");
			session.removeAttribute("FromWLSearch");
			session.removeAttribute("prodSearch");
			session.removeAttribute("fromWLHistory");
			session.removeAttribute("fromFind");
			session.removeAttribute("fromSL");
			session.removeAttribute("fromSLHistory");
			session.removeAttribute("fromSLFAV");
			session.removeAttribute("fromWL");

		}
		catch (ScanSeeWebSqlException e)
		{

			e.printStackTrace();
		}

		return new ModelAndView("scannow");

	}

	/**
	 * This method is used to get the details of wish list alerted coupon
	 * details Calls method in service layer. accepts a GET request in mime type
	 * text/plain
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/scannowsmartsearch", method = RequestMethod.GET)
	public @ResponseBody
	String getSmartSearchProd(@RequestParam(value = "searchKey", required = true) String searchKey, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
	{
		response.setContentType("json");
		final String methodName = "scannowsmartsearch of controller";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ScanNowService scanNowService = (ScanNowService) appContext.getBean("scanNowService");

		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		String searchResponse = null;
		StringBuffer innerHtml = new StringBuffer();
		SearchForm searchFormObj = new SearchForm();
		StringBuilder sb = new StringBuilder();

		ProductDetails productDetailsObj = null;
		try
		{
			if (searchKey != null && !"".equals(searchKey))
			{
				searchFormObj.setSearchKey(searchKey);
				searchFormObj = new SearchForm();
				searchFormObj.setUserId(userId);
				searchFormObj.setSearchKey(searchKey);
				productDetailsObj = scanNowService.smartSearchProducts(searchFormObj);
				List<ProductDetail> productNamelst = null;

				ArrayList<String> productNameslst = new ArrayList<String>();
				productNamelst = productDetailsObj.getProductDetail();
				sb.append("{countries:[");
				for (int i = 0; i < productNamelst.size(); i++)
				{
					String prodName = productNamelst.get(i).getProductName();
					Integer productId = productNamelst.get(i).getProductId();
					productNameslst.add(prodName);

					sb.append("{label:" + prodName);
					sb.append(",value:" + productId + "}" + "\n");
					if (i != productNamelst.size() - 1)
					{
						sb.append(",");
					}
					else
					{
						sb.append("]}");
					}

				}

				innerHtml.append(sb);

			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

}

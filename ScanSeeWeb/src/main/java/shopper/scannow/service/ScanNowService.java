package shopper.scannow.service;

import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.shopper.ProductDetails;

public interface ScanNowService
{

	/**
	 * The service method For fetching product information based on search key
	 * and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	ProductDetails smartSearchProducts(SearchForm seacrhDetail) throws ScanSeeWebSqlException;

}

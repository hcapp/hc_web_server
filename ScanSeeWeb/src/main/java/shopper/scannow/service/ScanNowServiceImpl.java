package shopper.scannow.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import shopper.scannow.dao.ScanNowDAO;

import common.exception.ScanSeeWebSqlException;
import common.pojo.SearchForm;
import common.pojo.shopper.ProductDetails;

public class ScanNowServiceImpl implements ScanNowService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanNowServiceImpl.class);
	/**
	 * Instance variable for Wish list DAO instance.
	 */
	/**
	 * Instance variable for Scan now DAO instance.
	 */
	private ScanNowDAO scanNowDAO;

	/**
	 * To set scan now dao
	 * 
	 * @param scanNowDAO
	 */
	public void setScanNowDAO(ScanNowDAO scanNowDAO)
	{
		this.scanNowDAO = scanNowDAO;
	}

	/**
	 * The service implementation method for fetching product information by
	 * passing search key. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request Xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public ProductDetails smartSearchProducts(SearchForm seacrhDetail) throws ScanSeeWebSqlException
	{
		final String methodName = "smartSearchProducts";

		String response = null;

		final ProductDetails productDetails = scanNowDAO.smartSearchProducts(seacrhDetail);

		return productDetails;
	}
}

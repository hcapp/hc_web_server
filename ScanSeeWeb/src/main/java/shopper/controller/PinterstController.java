package shopper.controller;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import shopper.service.CommonService;
import shopper.service.MyGalleryService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.FaceBookConfiguration;
import common.pojo.Users;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.UserTrackingData;

/**
 * This class is a controller class for pinterest sharing.
 * 
 * @author manjunatha_gh
 */
@Controller
public class PinterstController
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(PinterstController.class);

	/**
	 * This method is used to share product via pinterest.
	 * 
	 * @param productId
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws InvalidKeySpecException
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 * @throws Exception
	 */
	@RequestMapping(value = "/pinterestShareProductInfo.htm", method = RequestMethod.GET)
	@ResponseBody
	public String pinterestShareProductInfo(@RequestParam("productId") Integer productId, @RequestParam("productListId") Integer productListId,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException
	{

		String methodName = "pinterestShareProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetail = new ProductDetail();
		productDetail.setProductId(productId);
		ProductDetail productDetailResult = null;
		String shareURL = null;
		/*
		 * int userId = 0; String decryptedUserId = null; String key = null;
		 * String productKey = null; EncryptDecryptPwd enryptDecryptpwd = null;
		 */
		JSONObject productjson = null;

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
			// Users loginUser = (Users) session.getAttribute("loginuser");

			List<UserTrackingData> lstUserTrackingDatas = null;
			lstUserTrackingDatas = commonServiceObj.getConsumerShareTypes();

			for (UserTrackingData userTrackingData : lstUserTrackingDatas)
			{
				if (userTrackingData.getShrTypNam().equals(ApplicationConstants.PRODTWITTERSHARETYPETEXT))
				{
					productDetail.setShareTypeID(userTrackingData.getShrTypID());
				}

			}
			commonServiceObj.saveConsShareProdDetails(productDetail);
			productDetailResult = commonServiceObj.fetchConsumerProductDetails(productDetail);
		
			shareURL = commonServiceObj.getConsShareEmailConfiguration();

			/*
			 * if (null != loginUser) { if (null != loginUser.getUserID()) {
			 * userId = loginUser.getUserID().intValue(); decryptedUserId =
			 * Long.toString(userId); } } else { decryptedUserId =
			 * Long.toString(userId); } key = decryptedUserId + "/" +
			 * productDetail.getProductId(); enryptDecryptpwd = new
			 * EncryptDecryptPwd(); productKey = enryptDecryptpwd.encrypt(key);
			 */
			shareURL = shareURL + "key1=" + productId + "&key2=" + productListId + "&share=yes";

			productjson = new JSONObject();

			productjson.put("productURL", shareURL);
			productjson.put("productName", productDetailResult.getProductName());
			productjson.put("productImagePath", productDetailResult.getProductImagePath());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName + e.getMessage());
		}
		catch (ScanSeeWebSqlException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName + e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productjson.toString();

	}

	/**
	 * This method is used to share Deals via pinterest.
	 * 
	 * @param dealId
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pinterestShareDealInfo.htm", method = RequestMethod.GET)
	@ResponseBody
	public String pinterestShareDealInfo(@RequestParam("dealId") Integer dealId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException
	{

		String methodName = "pinterestShareDealInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		HotDealsListRequest hotDealsListRequestObj = new HotDealsListRequest();
		HotDealsDetails hotDealsDetailsObj = null;
		hotDealsListRequestObj.setHotDealId(dealId);
		String shareURL = null;
		JSONObject dealjson = null;

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
			Users loginUser = (Users) session.getAttribute("loginuser");

			if (null != loginUser)

			{
				if (null != loginUser.getUserID())
				{
					hotDealsListRequestObj.setUserId(loginUser.getUserID());
				}
			}
			hotDealsDetailsObj = commonServiceObj.getConsumerHdProdInfo(hotDealsListRequestObj);
			dealjson = new JSONObject();

			final FaceBookController faceBookController = new FaceBookController();
			final FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);
			shareURL = faceBookConfiguration.getScanSeeBaseUrl() + ApplicationConstants.CONSDEALPINSHARE;

			dealjson.put("dealURL", shareURL);
			dealjson.put("dealName", hotDealsDetailsObj.getHotDealName());
			dealjson.put("dealImagePath", hotDealsDetailsObj.getHotDealImagePath());

		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName + e.getMessage());
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return dealjson.toString();

	}

	/**
	 * This method is used to share Coupons via pinterest.
	 * 
	 * @param couponId
	 * @param request
	 * @param response
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pinterestShareCouponInfo.htm", method = RequestMethod.GET)
	@ResponseBody
	public String pinterestShareCouponInfo(@RequestParam("couponId") Integer couponId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException
	{

		String methodName = "pinterestShareCouponInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetail = new CLRDetails();
		clrDetail.setCouponId(couponId);
		CLRDetails clrDetails = null;
		String shareURL = null;
		JSONObject couponjson = null;

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			clrDetails = galleryService.getCLRInfoInfo(clrDetail, "C");
			couponjson = new JSONObject();

			final FaceBookController faceBookController = new FaceBookController();
			final FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);
			shareURL = faceBookConfiguration.getScanSeeBaseUrl() + ApplicationConstants.CONSCOUPONPINSHARE;

			couponjson.put("couponURL", shareURL);
			couponjson.put("couponName", clrDetails.getCoupDetails().getCouponInfo().getCouponName());
			couponjson.put("couponImagePath", clrDetails.getCoupDetails().getCouponInfo().getCouponImagePath());

		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName + e.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return couponjson.toString();

	}

}

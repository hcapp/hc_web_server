package shopper.controller;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.service.CommonService;
import shopper.service.ShopperService;
import shopper.service.ThisLocationService;
import shopper.validator.ShopperProfileValidator;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.ConsumerRequestDetails;
import common.pojo.SchoolInfo;
import common.pojo.UserPreference;
import common.pojo.Users;
import common.pojo.shopper.CategoryDetail;
import common.pojo.shopper.DoubleCheckData;
import common.util.Utility;

/**
 * This class is a controller class for shopper home.
 * 
 * @author manjunatha_gh
 */
@Controller
public class ShopperDashboardController
{

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ShopperDashboardController.class);

	/**
	 * Reference variable for ShopperProfileValidator class.
	 */
	ShopperProfileValidator shopperProfileValidator;

	@Autowired
	public void setShopperProfileValidator(ShopperProfileValidator shopperProfileValidator)
	{
		this.shopperProfileValidator = shopperProfileValidator;
	}

	/**
	 * This method is used to save shopper double checked information.
	 * 
	 * @param doubleCheckData
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/shopperdashboard.htm", method = RequestMethod.POST)
	public ModelAndView showPage(@ModelAttribute("doublecheckinfoform") DoubleCheckData doubleCheckData, BindingResult result,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "showPage";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		final ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		final CommonService commonService = (CommonService) appContext.getBean("commonService");

		Users userInfo = null;
		ConsumerRequestDetails consumerRequestDetails = null;
		final Users user = (Users) session.getAttribute("loginuser");
		final UserPreference userPreference = doubleCheckData.getUserPreferenceInfoList();
		userPreference.setUserID(user.getUserID());
		final CategoryDetail categoryDetailresponse = doubleCheckData.getCategoryDetailresponse();
		String favId = categoryDetailresponse.getFavCatId();
		categoryDetailresponse.setCellPhone(doubleCheckData.getAlertpreferencesInfo().isCellPhone());
		categoryDetailresponse.setEmail(doubleCheckData.getAlertpreferencesInfo().isEmail());
		final SchoolInfo schoolInfo = doubleCheckData.getStates();
		final String universityIdHidden = schoolInfo.getUniversityHidden();

		if (null != universityIdHidden && !universityIdHidden.isEmpty())
		{
			final Integer universityId = Integer.parseInt(universityIdHidden);
			schoolInfo.setUniversityID(universityId);
		}

		try
		{
			consumerRequestDetails = new ConsumerRequestDetails(ApplicationConstants.HOMEMODULE);
			session.setAttribute("consumerReqObj", consumerRequestDetails);
			boolean isValidContactNum = true;
			shopperProfileValidator.validateEditProfile(userPreference, result);

			if (result.hasErrors())
			{
				return new ModelAndView("consdoublecheck");
			}

			isValidContactNum = Utility.validatePhoneNum(userPreference.getMobilePhone());

			if (!isValidContactNum)
			{
				shopperProfileValidator.validateMobileNumber(userPreference, result, ApplicationConstants.INVALIDCONTPHONE);
			}

			if (result.hasErrors())
			{
				return new ModelAndView("consdoublecheck");
			}

			shopperService.updateUserData(userPreference);
			
			if (null != favId)
			{
				favId = favId.replace("on,", "");
			}
			
			shopperService.setUserFavCategories(favId, user.getUserID(), doubleCheckData.getAlertpreferencesInfo().isCellPhone(), doubleCheckData
					.getAlertpreferencesInfo().isEmail());

			schoolInfo.setUserId(user.getUserID());
			schoolInfo.setStateHidden(schoolInfo.getState());
			shopperService.saveSchoolInfo(schoolInfo);

			userInfo = commonService.getLoginUserDetails(user.getUserID());
			userInfo.setUserID(user.getUserID());

			final String uname = user.getUserName();
			final String fname = userInfo.getFirstName();
			final String emailId = userInfo.getEmail();
			session.setAttribute("truncUsername", Utility.truncate(uname, 15));
			session.setAttribute("truncFirstName", Utility.truncate(fname, 15));
			session.setAttribute("emailId", emailId);

			userInfo.setUserName(user.getUserName());
			userInfo.setUserType(1);

			session.removeAttribute("loginuser");
			session.setAttribute("loginuser", userInfo);

		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + e);
			e.getStackTrace();
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView("shopperdashboard");
	}

	/**
	 * This method returns shopper home page.
	 * @param request
	 * @param session
	 * @param model
	 * @return String
	 */
	@RequestMapping(value = "/shopperdashboard.htm", method = RequestMethod.GET)
	public String showPageGet(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "showPageGet";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		
		final ConsumerRequestDetails consumerRequestDetails = new ConsumerRequestDetails();
		consumerRequestDetails.setModuleName(ApplicationConstants.HOMEMODULE);
		session.setAttribute("consumerReqObj", consumerRequestDetails);
		
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "shopperdashboard";
	}

	@RequestMapping(value = "/thislocation.htm", method = RequestMethod.GET)
	public ModelAndView showThisLocation(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws IOException
	{
		LOG.info("Entered in This Location GET Method....");
		String view = null;
		request.setAttribute("message", "Please Enter zip code or city name  to view retailers.");
		final ServletContext servletContext = request.getSession().getServletContext();
		final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
		// ArrayList<ThisLocationRequest> radiuslst = null;
		session.removeAttribute("currentspecial");

		session.removeAttribute("zipcode");

		final Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();

		/*
		 * radiuslst = thisLocationService.getRediusInfo(); if(radiuslst !=null
		 * &&! radiuslst.isEmpty() ) { session.setAttribute("radiuslst",
		 * radiuslst); }
		 */
		view = "thislocation";
		return new ModelAndView(view);
	}

	@RequestMapping(value = "/onlinestoredetails.htm", method = RequestMethod.POST)
	public ModelAndView showOnlineStores(HttpServletRequest request, HttpServletResponse response, ModelMap model, HttpSession session)
			throws IOException
	{
		LOG.info("Entered in Online Stores POST Method....");
		String view = null;
		view = "onlinestoredetails";
		return new ModelAndView(view);
	}
}

package shopper.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import shopper.service.CommonService;
import shopper.service.MyGalleryService;
import shopper.service.ShopperService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.FaceBookConfiguration;
import common.pojo.Users;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CouponsDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.HotDealsListRequest;
import common.pojo.shopper.LoyaltyDetails;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.RebateDetails;
import common.pojo.shopper.UserTrackingData;
import common.util.EncryptDecryptPwd;
import common.util.Utility;

/**
 * This class is a controller class for facebook sharing.
 * 
 * @author manjunatha_gh
 */
@Controller
public class FaceBookController
{

	/**
	 * FaceBook Oauth Dialogue URL.
	 */
	private static final String DIALOG_OAUTH = "https://www.facebook.com/dialog/oauth";
	/**
	 * FaceBook Logout URL.
	 */
	private static final String FB_LOGOUT_URL = "https://www.facebook.com/logout.php?";
	/**
	 * FaceBook Access token URL which will be invoked once the user is
	 * authenticated.
	 */
	private static final String ACCESS_TOKEN = "https://graph.facebook.com/oauth/access_token";

	/**
	 * FaceBook Feed API URL used for Post to wall feature.
	 */
	private static final String FEED_URL = "http://www.facebook.com/dialog/feed";

	private static final String SCOPE = "email,user_about_me";

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(FaceBookController.class);

	/**
	 * The Method used for facebook login.
	 * 
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/facebookLogin.htm", method = RequestMethod.GET)
	private void fbSignin(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		String methodName = "fbSignin";
		LOG.info("Inside the method" + methodName);
		session.removeAttribute("ACCESS_TOKEN");
		session.removeAttribute("FaceBookLogin");
		try
		{
			FaceBookController faceBookController = new FaceBookController();
			// Get FaceBook Application Configuration
			FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);
			response.sendRedirect(DIALOG_OAUTH + "? client_id=" + faceBookConfiguration.getAppId() + "&redirect_uri="
					+ faceBookConfiguration.getScanSeeBaseUrl() + "shopper/fbLoginCallback" + "&scope=" + SCOPE);

		}
		catch (IOException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Exiting method" + methodName);
	}

	/**
	 * The Method used for facebook logout.
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/facebookSignout.htm", method = RequestMethod.GET)
	private void fbSignOut(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		String methodName = "fbSignOut";
		LOG.info("Inside the method" + methodName);
		try
		{

			String ACCESS_TOKEN = (String) session.getAttribute("ACCESS_TOKEN");
			FaceBookController faceBookController = new FaceBookController();
			FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);
			LOG.info("FaceBook Logout url:::::" + FB_LOGOUT_URL + ACCESS_TOKEN + " &confirm=1&next=" + faceBookConfiguration.getScanSeeBaseUrl()
					+ "logout.htm");
			response.sendRedirect(FB_LOGOUT_URL + ACCESS_TOKEN + " &confirm=1&next=" + faceBookConfiguration.getScanSeeBaseUrl() + "logout.htm");

		}
		catch (IOException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Exiting method" + methodName);
	}

	/**
	 * The CallBack method for facebook login.
	 * 
	 * @throws MalformedURLException
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/fbLoginCallback", params = "code", method = RequestMethod.GET)
	private ModelAndView fbAccessCode(@RequestParam("code") String code, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws ScanSeeServiceException
	{
		String methodName = "fbAccessCode";
		LOG.info("Inside the method" + methodName);
		String view = null;
		try
		{
			response.setContentType("text/html");
			Users fbUserInfo = null;
			Users userInfo = null;
			FaceBookController faceBookController = new FaceBookController();
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
			FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);

			String strUrl = ACCESS_TOKEN + "?client_id=" + faceBookConfiguration.getAppId() + "&redirect_uri="
					+ faceBookConfiguration.getScanSeeBaseUrl() + "shopper/fbLoginCallback" + "&client_secret="
					+ faceBookConfiguration.getAppSecretId() + "&code=" + code;
			URL url = new URL(strUrl);
			String res = readURL(url);
			int index = res.indexOf("&expires");
			fbUserInfo = authFacebookLogin(res.substring(0, index));
			userInfo = shopperService.processFaceBookUser(fbUserInfo.getEmail());
			String uname = fbUserInfo.getFirstName();
			String fname = fbUserInfo.getLastName();
			session.setAttribute("truncUsername", Utility.truncate(uname, 15));
			session.setAttribute("truncFirstName", Utility.truncate(fname, 15));
			session.setAttribute("emailId", fbUserInfo.getEmail());
			// Login is success
			String pageForwardValue = userInfo.getPageForwardValue();
			session.setAttribute("loginuser", userInfo);
			session.setAttribute("FaceBookLogin", "FaceBookLogin");
			session.setAttribute("ACCESS_TOKEN", res.substring(0, index));
			if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOSHOPPERPREFERENCES))
			{

				view = "/ScanSeeWeb/shopper/preferences.htm";
			}
			else if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOSHOPPERSELECTSCHOOL))
			{
				view = "/ScanSeeWeb/shopper/selectschool.htm";
			}
			else if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.NOSHOPPERDOUBLECHECK))
			{
				view = "/ScanSeeWeb/shopper/doublecheck.htm";
			}
			else if (pageForwardValue.equalsIgnoreCase(ApplicationConstants.SHOPPERDASHBOARD))
			{
				view = "/ScanSeeWeb/shopper/shopperdashboard.htm";
			}
		}
		catch (MalformedURLException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
			throw new ScanSeeServiceException(e);
		}
		LOG.info("Exiting method" + methodName);
		return new ModelAndView(new RedirectView(view));
	}

	/**
	 * The method used to read the response from URL.
	 * 
	 * @throws ScanSeeServiceException
	 * @throws IOException
	 */
	private static String readURL(URL url)
	{
		String methodName = "readURL";
		LOG.info("Inside the method" + methodName);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try
		{

			InputStream is = url.openStream();
			int r;
			while ((r = is.read()) != -1)
			{
				baos.write(r);
			}
		}
		catch (IOException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
		}

		LOG.info("Exiting method" + methodName);
		return new String(baos.toByteArray());
	}

	/**
	 * The CallBack method for facebook login.
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/fbLoginCallback", params = "error_reason", method = RequestMethod.GET)
	private void fberror(@RequestParam("error_reason") String errorReason, @RequestParam("error") String error,
			@RequestParam("error_description") String description, HttpServletRequest request, HttpServletResponse response)
	{
		String methodName = "fberror";
		LOG.info("Inside the method" + methodName);
		try
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, description);
			LOG.info(errorReason);
			LOG.info(error);
			LOG.info(description);
		}
		catch (IOException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
		}
		LOG.info("Exiting method" + methodName);
	}

	/**
	 * @throws MalformedURLException
	 *             The method used to fetch user information once the user is
	 *             authorised.
	 * @throws ScanSeeServiceException
	 * @throws
	 */
	public static Users authFacebookLogin(String accessToken) throws MalformedURLException
	{
		String methodName = "authFacebookLogin";
		LOG.info("Inside the method" + methodName);
		String email = null;
		String first_name = null;
		String last_name = null;
		Users fbUserInfo = new Users();
		try
		{

			JSONObject resp = new JSONObject(readURL(new URL("https://graph.facebook.com/me?" + accessToken)));
			email = resp.getString("email");
			first_name = resp.getString("first_name");
			last_name = resp.getString("last_name");
			fbUserInfo.setFirstName(first_name);
			fbUserInfo.setLastName(last_name);
			fbUserInfo.setEmail(email);
		}
		catch (ParseException e)
		{
			LOG.error("Exception occurred in login:::::" + e.getMessage());
		}
		LOG.info("Exiting method" + methodName);
		return fbUserInfo;
	}

	/**
	 * The method used share hot deal information via facebook.
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/fbShareHotDealInfo.htm", method = RequestMethod.POST)
	public void shareHotDealInfo(@ModelAttribute("dealsearchform") HotDealsListRequest hotDealsListRequestObj, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, IOException
	{

		String methodName = "shareHotDealInfo";
		LOG.info("Inside method " + methodName);
		HotDealsDetails hotDealDetails = null;
		String feedMsg = "Great Find At ScanSee";
		String hdURL = null;
		FaceBookController faceBookController = new FaceBookController();
		FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");

		hotDealDetails = commonServiceObj.getConsumerHdProdInfo(hotDealsListRequestObj);

		if (hotDealDetails != null)
		{
			String redirectUrl = null;
			String newRedirectUrl = null;

			redirectUrl = hotDealsListRequestObj.getRedirectUrl();

			if (redirectUrl != null)
			{
				if (redirectUrl.equals("dealsearch"))
				{
					newRedirectUrl = "consdealssearch.htm";
				}
				if (redirectUrl.equals("dealdetails"))
				{
					newRedirectUrl = "conshddetails.htm";
				}

			}

			String feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + hotDealDetails.getHotDealName() + "&caption="
			// + feedMsg + "Deal Price:$" + hotDealDetails.gethDSalePrice() +
			// "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl()
					+ feedMsg + "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl() + "shopper/" + newRedirectUrl;

			if (null != hotDealDetails.gethDSalePrice() && !hotDealDetails.gethDSalePrice().equals(""))
			{
				feedURL = feedURL + "&Deal Price:$" + hotDealDetails.gethDSalePrice();

			}
			if (null != hotDealDetails.getHdURL() && !hotDealDetails.getHdURL().equals(""))
			{
				feedURL = feedURL + "&link=" + hotDealDetails.getHdURL();

			}
			if (null != hotDealDetails.getHotDealImagePath() && !hotDealDetails.getHotDealImagePath().equals(""))
			{

				feedURL = feedURL + "&picture=" + hotDealDetails.getHotDealImagePath();
			}
			if (null != hotDealDetails.gethDLognDescription() && !hotDealDetails.gethDLognDescription().equals(""))
			{

				feedURL = feedURL + "&description=" + hotDealDetails.gethDLognDescription();
			}
			LOG.info("Exiting method" + methodName);
			response.sendRedirect(feedURL);

		}
		else
		{
			LOG.info("hotDeal details not found ");
		}
	}

	/**
	 * The method used share hot deal information via facebook.
	 * 
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/HotDealfbShare.htm", method = RequestMethod.POST)
	public void shareHotDeal(@ModelAttribute("hotdealmainpage") HotDealsListRequest hotDealsListRequestObj, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, IOException
	{

		String methodName = "shareHotDeal";
		LOG.info("Inside method " + methodName);
		HotDealsDetails hotDealDetails = null;
		String feedMsg = "Great Find At ScanSee";
		String hdURL = null;
		FaceBookController faceBookController = new FaceBookController();
		FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		hotDealsListRequestObj.setHotDealId(hotDealsListRequestObj.getHotDealID());
		hotDealDetails = commonServiceObj.getConsumerHdProdInfo(hotDealsListRequestObj);

		if (hotDealDetails != null)
		{
			String newRedirectUrl = hotDealsListRequestObj.getRedirectUrl();

			if (newRedirectUrl != null)
			{
				if (newRedirectUrl.equals("menudealdetails"))
				{
					newRedirectUrl = "consdisplayhotdeals.htm";
				}
			}

			String feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + hotDealDetails.getHotDealName() + "&caption="
			// + feedMsg + "Deal Price:$" + hotDealDetails.gethDSalePrice() +
			// "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl()
					+ feedMsg + "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl() + "shopper/" + newRedirectUrl;

			if (null != hotDealDetails.gethDSalePrice() && !hotDealDetails.gethDSalePrice().equals(""))
			{
				feedURL = feedURL + "&Deal Price:$" + hotDealDetails.gethDSalePrice();

			}
			if (null != hotDealDetails.getHdURL() && !hotDealDetails.getHdURL().equals(""))
			{
				feedURL = feedURL + "&link=" + hotDealDetails.getHdURL();

			}
			if (null != hotDealDetails.getHotDealImagePath() && !hotDealDetails.getHotDealImagePath().equals(""))
			{

				feedURL = feedURL + "&picture=" + hotDealDetails.getHotDealImagePath();
			}
			if (null != hotDealDetails.gethDLognDescription() && !hotDealDetails.gethDLognDescription().equals(""))
			{

				feedURL = feedURL + "&description=" + hotDealDetails.gethDLognDescription();
			}

			response.sendRedirect(feedURL);
			LOG.info("Exiting method" + methodName);
		}
		else
		{
			LOG.info("hotDeal details not found ");
		}
	}

	/**
	 * The method used share product information via facebook.
	 * 
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws ScanSeeWebSqlException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws InvalidKeySpecException
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 * @throws IOException
	 */
	@RequestMapping(value = "/fbShareProductInfo.htm", method = RequestMethod.POST)
	public void fbShareProductInfo(@ModelAttribute("productsummary") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException, IOException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException
	{
		String methodName = "    fbShareProductInfo";
		LOG.info("Inside method" + methodName);

		String feedMsg = " Great Find At ScanSee!!!  ";

		FaceBookController faceBookController = new FaceBookController();
		FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);

		session.setAttribute("fbShareProductId", productDetail.getProductId());
		String shareURL = null;
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		CommonService commonServiceObj = (CommonService) appContext.getBean("commonService");
		shareURL = commonServiceObj.getConsShareEmailConfiguration();
		String productKey = null;
		Users loginUser = (Users) session.getAttribute("loginuser");
		String decryptedUserId = null;
		List<UserTrackingData> lstUserTrackingDatas=null;
		String feedURL = null;
		/*
		 * int userId = 0; if (loginUser != null) { if(loginUser.getUserID() !=
		 * null) { userId = loginUser.getUserID().intValue(); decryptedUserId =
		 * Long.toString(userId); } }else{ decryptedUserId =
		 * Long.toString(userId); } String key = decryptedUserId + "/" +
		 * productDetail.getProductId(); EncryptDecryptPwd enryptDecryptpwd =
		 * null; enryptDecryptpwd = new EncryptDecryptPwd();
		 */
		try{

		lstUserTrackingDatas =	commonServiceObj.getConsumerShareTypes();
		
		for (UserTrackingData userTrackingData : lstUserTrackingDatas)
		{
			if(userTrackingData.getShrTypNam().equals(ApplicationConstants.PRODFACEBOOKSHARETYPETEXT))
			{
				productDetail.setShareTypeID(userTrackingData.getShrTypID());
			}
					
		}	
		commonServiceObj.saveConsShareProdDetails(productDetail);
		
		ProductDetail prdDetail = commonServiceObj.fetchConsumerProductDetails(productDetail);

		// productKey = enryptDecryptpwd.encrypt(key);
		shareURL = shareURL + "key1=" + productDetail.getProductId() + "%26key2=" + productDetail.getProductListID() + "%26share=yes";

		String redirectUrl = null;
		redirectUrl = productDetail.getRedirectUrl();
		String newRedirectUrl = null;
		if (redirectUrl != null)
		{
			if (redirectUrl.equals("findprodlist"))
			{
				newRedirectUrl = "consfindprodsearch.htm";
			}
			if (redirectUrl.equals("findprodsummary"))
			{
				newRedirectUrl = "consfindprodinfo.htm";
			}

		}
		if(null != prdDetail.getProductImagePath() && !"".equals(productDetail.getProductImagePath()))
				{
			feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + "View Product Details" + "&caption=" + feedMsg
			+ prdDetail.getProductName() +"&picture=" +prdDetail.getProductImagePath()+ "&link=" + shareURL + "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl() + "shopper/"
			+ newRedirectUrl;
				}else{

		feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + "View Product Details" + "&caption=" + feedMsg
				+ prdDetail.getProductName() + "&link=" + shareURL + "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl() + "shopper/"
				+ newRedirectUrl;
				}
		}catch(ScanSeeWebSqlException e)
		{
			LOG.error("Errot occured in"+ methodName +e.getMessage());
			
		}
		LOG.info("Exiting method" + methodName);
		response.sendRedirect(feedURL);

	}

	/**
	 * The method used share product information via facebook.
	 */
	@RequestMapping(value = "/fbShareclrInfo.htm", method = RequestMethod.POST)
	public void fbShareCLRInfo(@ModelAttribute("clrForm") CLRDetails clrForm, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws ScanSeeServiceException
	{

		final String methodName = "fbShareCLRInfo";
		LOG.info("Inside the method" + methodName);
		final String feedMsg = "Great Find At ScanSee!!!  ";
		final String couponName = "View Coupon Details";
		final String rebateName = "View Rebate Details";
		final String loyaltyName = "View Loyalty Details";
		final String startDate = "Start Date:";
		final String expiryDate = "Expiry Date:";
		final Users loginUser = (Users) request.getSession().getAttribute("loginuser");
		session.setAttribute("limit", clrForm.getLowerLimit());
		if (null != loginUser)
		{
			clrForm.setUserId(Utility.longToInteger(loginUser.getUserID()));
		}
		final FaceBookController faceBookController = new FaceBookController();
		final FaceBookConfiguration faceBookConfiguration = faceBookController.getFaceBookConfiguration(request);

		final String clrType = clrForm.getClrType();
		clrForm.setAdded(clrForm.getAdded());
		session.setAttribute("fbRedirectclrForm", clrForm);
		session.removeAttribute("CouponDetails");
		String feedURL = null;
		try
		{
			final ServletContext servletContext = request.getSession().getServletContext();
			final WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			final MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");

			if ("C".equals(clrType))
			{
				final CLRDetails clrDetails = galleryService.getCLRInfoInfo(clrForm, "C");
				clrDetails.setClrType(clrForm.getRequestType());
				clrDetails.setClrImagePath(clrForm.getClrImagePath());
				session.setAttribute("CouponDetails", clrDetails);
				final CouponsDetails couponsDetails = clrDetails.getCoupDetails();
				couponsDetails.getCouponInfo().setCouponImagePath(clrForm.getClrImagePath());
				if (null != couponsDetails.getCouponInfo().getCouponURL() && !couponsDetails.getCouponInfo().getCouponURL().equals("NotApplicable"))
				{
					feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + couponName + "&caption=" + feedMsg
							+ couponsDetails.getCouponInfo().getCouponName() + " " + startDate + couponsDetails.getCouponInfo().getCouponStartDate()
							+ "  " + expiryDate + couponsDetails.getCouponInfo().getCouponExpireDate() + "&link="
							+ couponsDetails.getCouponInfo().getCouponURL() + "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl()
							+ "shopper/" + clrForm.getReturnURL();

				}
				else
				{

					feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + couponsDetails.getCouponInfo().getCouponName()
							+ "&caption=" + feedMsg + couponsDetails.getCouponInfo().getCouponName() + " " + startDate
							+ couponsDetails.getCouponInfo().getCouponStartDate() + "  " + expiryDate
							+ couponsDetails.getCouponInfo().getCouponExpireDate() + "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl()
							+ "shopper/" + clrForm.getReturnURL();
				}
				if (null != couponsDetails.getCouponInfo().getCouponImagePath()
						&& !couponsDetails.getCouponInfo().getCouponImagePath().equals("NotApplicable")
						&& !couponsDetails.getCouponInfo().getCouponImagePath().equals("/ScanSeeWeb/images/blankImage.gif"))
				{

					feedURL = feedURL + "&picture=" + couponsDetails.getCouponInfo().getCouponImagePath();
				}
				else
				{

					feedURL = feedURL + "&picture=" + faceBookConfiguration.getScanSeeBaseUrl() + "images/blankImage.gif";
				}
				response.sendRedirect(feedURL);
			}
			else if ("L".equals(clrType))
			{
				final LoyaltyDetails loyaltyDetails = (LoyaltyDetails) session.getAttribute("fbloyaltydetails");

				if (null != loyaltyDetails.getLoyaltyInfo().getLoyaltyURL()
						&& loyaltyDetails.getLoyaltyInfo().getLoyaltyURL().equals("NotApplicable"))
				{
					feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + loyaltyName + "&caption=" + feedMsg
							+ loyaltyDetails.getLoyaltyInfo().getLoyaltyDealName() + " " + startDate
							+ loyaltyDetails.getLoyaltyInfo().getLoyaltyDealStartDate() + "  " + expiryDate
							+ loyaltyDetails.getLoyaltyInfo().getLoyaltyDealExpireDate() + "&link=" + loyaltyDetails.getLoyaltyInfo().getLoyaltyURL()
							+ "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl() + "shopper/fetchclrdetails.htm";
				}
				else
				{
					feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name="
							+ loyaltyDetails.getLoyaltyInfo().getLoyaltyDealName() + "&caption=" + feedMsg
							+ loyaltyDetails.getLoyaltyInfo().getLoyaltyDealName() + " " + startDate
							+ loyaltyDetails.getLoyaltyInfo().getLoyaltyDealStartDate() + "  " + expiryDate
							+ loyaltyDetails.getLoyaltyInfo().getLoyaltyDealExpireDate() + "&redirect_uri="
							+ faceBookConfiguration.getScanSeeBaseUrl() + "shopper/fetchclrdetails.htm";
				}

				if (null != loyaltyDetails.getLoyaltyInfo().getImagePath() && !loyaltyDetails.getLoyaltyInfo().getImagePath().equals("NotApplicable")
						&& !loyaltyDetails.getLoyaltyInfo().getImagePath().equals("/ScanSeeWeb/images/blankImage.gif"))
				{

					feedURL = feedURL + "&picture=" + loyaltyDetails.getLoyaltyInfo().getImagePath();
				}
				else
				{

					feedURL = feedURL + "&picture=" + faceBookConfiguration.getScanSeeBaseUrl() + "images/blankImage.gif";
				}
				response.sendRedirect(feedURL);
			}
			else if ("R".equals(clrType))
			{
				final RebateDetails rebateDetails = (RebateDetails) session.getAttribute("fbrebatedetails");
				if (null != rebateDetails.getRebateInfo().getRebateURL() && rebateDetails.getRebateInfo().getRebateURL().equals("NotApplicable"))
				{
					feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + rebateName + "&caption=" + feedMsg
							+ rebateDetails.getRebateInfo().getRebateName() + " " + startDate + rebateDetails.getRebateInfo().getRebateStartDate()
							+ "  " + expiryDate + rebateDetails.getRebateInfo().getRebateEndDate() + "&link="
							+ rebateDetails.getRebateInfo().getRebateURL() + "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl()
							+ "shopper/fetchclrdetails.htm";
				}
				else
				{
					feedURL = FEED_URL + "?app_id=" + faceBookConfiguration.getAppId() + "&name=" + rebateDetails.getRebateInfo().getRebateName()
							+ "&caption=" + feedMsg + rebateDetails.getRebateInfo().getRebateName() + " " + startDate
							+ rebateDetails.getRebateInfo().getRebateStartDate() + "  " + expiryDate
							+ rebateDetails.getRebateInfo().getRebateEndDate() + "&redirect_uri=" + faceBookConfiguration.getScanSeeBaseUrl()
							+ "shopper/fetchclrdetails.htm";
				}

				if (null != rebateDetails.getRebateInfo().getImagePath() && !rebateDetails.getRebateInfo().getImagePath().equals("NotApplicable")
						&& !rebateDetails.getRebateInfo().getImagePath().equals("/ScanSeeWeb/images/blankImage.gif"))
				{

					feedURL = feedURL + "&picture=" + rebateDetails.getRebateInfo().getImagePath();
				}
				else
				{

					feedURL = feedURL + "&picture=" + faceBookConfiguration.getScanSeeBaseUrl() + "images/blankImage.gif";
				}
				response.sendRedirect(feedURL);

			}
		}
		catch (IOException e)
		{
			LOG.info(ApplicationConstants.EXCEPTION_OCCURED + methodName + e.getMessage());
		}

	}

	public FaceBookConfiguration getFaceBookConfiguration(HttpServletRequest request) throws ScanSeeServiceException
	{
		String methodName = "getFaceBookConfiguration";
		LOG.info("Inside the method" + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		FaceBookConfiguration faceBookConfiguration = shopperService.getFaceBookConfiguration();
		LOG.info("Exiting method" + methodName);
		return faceBookConfiguration;
	}

}

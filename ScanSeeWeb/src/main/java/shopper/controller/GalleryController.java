/**
 * 
 */
package shopper.controller;

import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import shopper.service.MyGalleryService;
import com.scansee.externalapi.common.constants.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CategoryInfo;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.LoyaltyDetail;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.RebateDetail;
import common.pojo.shopper.ShareProductInfo;
import common.util.Utility;

/**
 */
@Controller
public class GalleryController
{
	/**
	 * getting instance logger
	 */
	public static final Logger LOG = LoggerFactory.getLogger(GalleryController.class);

	/**
	 * getGalleryAll is a method for displaying All coupons based on the
	 * category.
	 * 
	 * @param clrForm
	 *            contains retailerId,categoryID and searchKey.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String returns viewName to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/galleryhome.htm", method = RequestMethod.GET)
	public String getGalleryAll(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request, HttpSession session,
			ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getGalleryAll";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			CLRDetails clrDetails = new CLRDetails();
			clrDetails.setRetailerId(clrForm.getRetailerId());
			clrDetails.setCategoryID(clrForm.getCategoryID());
			clrDetails.setSearchKey(clrForm.getSearchKey());
			clrDetails.setLowerLimit(0);
			clrDetails.setUserId(Utility.longToInteger(loginUser.getUserID()));
			CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "ALL");
			clrResult.setLowerLimit(0);
			request.setAttribute("allGalleryResult", clrResult);
			model.put("clrForm", clrForm);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "galleryall";

	}

	/**
	 * getGalleryExpired is a method for displaying Expired coupons based on the
	 * category.
	 * 
	 * @param clrForm
	 *            contains retailerId,categoryID and searchKey.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String returns viewName to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/galleryexp.htm", method = RequestMethod.GET)
	public String getGalleryExpired(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getGalleryExpired";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			CLRDetails clrDetails = new CLRDetails();
			clrDetails.setRetailerId(clrForm.getRetailerId());
			clrDetails.setCategoryID(clrForm.getCategoryID());
			clrDetails.setSearchKey(clrForm.getSearchKey());
			clrDetails.setLowerLimit(0);
			clrDetails.setUserId(Utility.longToInteger(loginUser.getUserID()));
			CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "EXPIRED");
			clrResult.setLowerLimit(0);
			request.setAttribute("expGalleryResult", clrResult);
			model.put("clrForm", clrForm);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "galleryexp";

	}

	/**
	 * getGalleryUsed is a method for displaying Used coupons based on the
	 * category.
	 * 
	 * @param clrForm
	 *            contains retailerId,categoryID and searchKey.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String returns viewName to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/galleryused.htm", method = RequestMethod.GET)
	public String getGalleryUsed(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getGalleryUsed";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			CLRDetails clrDetails = new CLRDetails();
			clrDetails.setRetailerId(clrForm.getRetailerId());
			clrDetails.setCategoryID(clrForm.getCategoryID());
			clrDetails.setSearchKey(clrForm.getSearchKey());
			clrDetails.setLowerLimit(0);
			clrDetails.setUserId(Utility.longToInteger(loginUser.getUserID()));
			CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "USED");
			clrResult.setLowerLimit(0);
			request.setAttribute("usedGalleryResult", clrResult);
			model.put("clrForm", clrForm);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "galleryused";

	}

	/**
	 * getCouponGallery is a method for displaying myCoupons based on the
	 * category .
	 * 
	 * @param clrForm
	 *            contains retailerId,categoryID and searchKey.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String returns viewName to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/coupongallery.htm", method = RequestMethod.GET)
	public String getCouponGallery(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpSession session, ModelMap model) throws ScanSeeServiceException
	{
		final String methodName = "getCouponGallery";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<CategoryInfo> categories = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			CLRDetails clrDetails = new CLRDetails();
			clrDetails.setRetailerId(clrForm.getRetailerId());
			clrDetails.setCategoryID(clrForm.getCategoryID());
			clrDetails.setSearchKey(clrForm.getSearchKey());
			clrDetails.setLowerLimit(0);
			clrDetails.setUserId(Utility.longToInteger(loginUser.getUserID()));
			CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrDetails, "MYGALLERY");
			categories = galleryService.getCategories();
			clrResult.setLowerLimit(0);
			request.setAttribute("galleryResult", clrResult);
			session.setAttribute("categories", categories);
			model.put("clrForm", clrForm);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "mygallery";

	}

	/**
	 * getCouponDetails is a method for displaying coupon details.
	 * 
	 * @param clrForm
	 *            contains clrType,requestType.
	 * @param result
	 * @param request
	 * @param session
	 * @param model
	 * @return String returns viewName to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/fetchclrdetails.htm", method = RequestMethod.POST)
	public String getCouponDetails(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "getCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ArrayList<ProductDetail> productlist = null;
		String productIdWithComma = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			clrForm.setUserId(Utility.longToInteger(loginUser.getUserID()));
			String clrType = clrForm.getClrType();
			String requestType = clrForm.getRequestType();
			String addedFlag = request.getParameter("added");
			String imagePath = clrForm.getClrImagePath();
			boolean viewOnWeb = clrForm.isViewableOnWeb();
			if (null != clrType)
			{
				CLRDetails clrDetails = galleryService.getCLRInfoInfo(clrForm, clrType);
				if ("C".equals(clrType))
				{

					LOG.info("details of the coupon");
					CouponDetails coupDetails = clrDetails.getCoupDetails().getCouponInfo();
					coupDetails.setViewableOnWeb(viewOnWeb);
					coupDetails.setCouponImagePath(imagePath);
					productlist = clrDetails.getCoupDetails().getProductLst();
					request.setAttribute("coupondetails", clrDetails.getCoupDetails());
					session.setAttribute("fbcoupondetails", clrDetails.getCoupDetails());
					viewName = "redeem";
				}
				else if ("L".equals(clrType))
				{
					LOG.info("details of the loyalty");
					LoyaltyDetail loyDetails = clrDetails.getLoyDetails().getLoyaltyInfo();
					loyDetails.setImagePath(imagePath);
					productlist = clrDetails.getLoyDetails().getProductLst();
					request.setAttribute("loyaltydetails", clrDetails.getLoyDetails());
					session.setAttribute("fbloyaltydetails", clrDetails.getLoyDetails());
					viewName = "loyaltydetails";
				}
				else
				{
					LOG.info("details of the rebate");
					RebateDetail rebateDetails = clrDetails.getRebDetails().getRebateInfo();
					rebateDetails.setImagePath(imagePath);
					productlist = clrDetails.getRebDetails().getProductLst();
					request.setAttribute("rebatedetails", clrDetails.getRebDetails());
					session.setAttribute("fbrebatedetails", clrDetails.getRebDetails());
					viewName = "rebatedetails";
				}
				request.setAttribute("requestType", requestType);
				request.setAttribute("added", addedFlag);
			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}

	/**
	 * addCoupon is a method for adding the coupon from allCoupons to myCoupons.
	 * It adds the coupon only if it is not used.
	 * 
	 * @param couponId
	 *            contains the id of the coupon to be added.
	 * @param usage
	 *            contains the information whether the coupon is used or not.
	 * @param from
	 * @param request
	 * @param response
	 * @param session
	 * @return String returns viewName to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/addcoupon", method = RequestMethod.GET)
	public @ResponseBody
	String addCoupon(@RequestParam(value = "couponId", required = true) Integer couponId,
			@RequestParam(value = "usage", required = true) String usage, @RequestParam(value = "from", required = true) String from,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		String methodName = "addCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		StringBuffer innerHtml = new StringBuffer();
		String daoResponse = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");

			Users loginUser = (Users) session.getAttribute("loginuser");

			Long userId = loginUser.getUserID();

			if (usage.equalsIgnoreCase("false"))
			{
				//daoResponse = galleryService.couponAdd(userId, couponId);
				if (from.equalsIgnoreCase("all"))
				{
					innerHtml.append("<img src='/ScanSeeWeb/images/cpnIcon.png' alt='Cpn' width='27' height='25' id = 'cpnTg" + couponId
							+ "' name= 'cpnTg" + couponId + "'/>");
				}
				else
				{
					innerHtml
							.append("<div id='cpnTg'><a href='#'><img src='/ScanSeeWeb/images/markAsUsed.png' alt='mark as used' width='140' height='30' onclick='userRedeemCLR(\'coupon\')' /></a><span class='wrpWord'><a href='#'><img class='fade' src='/ScanSeeWeb/images/clipCoupon.png' alt='clip coupon' width='140' height='30' /></a></span></div>");

				}

			}

		}catch (Exception e) {
			// TODO: handle exception
		}/*
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.ERROROCCURRED + methodName);
			throw e;
		}*/
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return innerHtml.toString();
	}

	@RequestMapping(value = "/addloyality", method = RequestMethod.GET)
	public @ResponseBody
	String addLoyality(@RequestParam(value = "loyaltyDealId", required = true) Integer loyaltyDealId,
			@RequestParam(value = "usage", required = true) String usage, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{

		StringBuffer innerHtml = new StringBuffer();

		try
		{

			LOG.info("Entering in to GalleryController......");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");

			String daoResponse = null;

			Users loginUser = (Users) session.getAttribute("loginuser");

			Long userId = loginUser.getUserID();

			if (usage.equalsIgnoreCase("0"))
			{
				daoResponse = galleryService.loyaltyAdd(userId, loyaltyDealId);

				innerHtml.append("<img src='/ScanSeeWeb/images/cpnIcon.png' alt='Cpn' width='27' height='25' id = 'lytTg" + loyaltyDealId
						+ "' name= 'lytTg" + loyaltyDealId + "'/>");

			}

		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}
		LOG.info("ShopperProfileController :: Inside Exit Post Method");

		return innerHtml.toString();
	}

	@RequestMapping(value = "/addrebate", method = RequestMethod.GET)
	public @ResponseBody
	String addRebate(@RequestParam(value = "rebateId", required = true) Integer rebateId,
			@RequestParam(value = "usage", required = true) String usage, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{

		StringBuffer innerHtml = new StringBuffer();

		try
		{

			LOG.info("Entering in to GalleryController......");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");

			String daoResponse = null;

			Users loginUser = (Users) session.getAttribute("loginuser");

			Long userId = loginUser.getUserID();

			if (usage.equalsIgnoreCase("0"))
			{
				daoResponse = galleryService.rebateAdd(userId, rebateId);

				innerHtml.append("<img src='/ScanSeeWeb/images/cpnIcon.png' alt='Cpn' width='27' height='25' id = 'rbtTg" + rebateId
						+ "' name= 'rbtTg" + rebateId + "'/>");

			}

		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}
		LOG.info("GalleryController :: Inside Exit Post Method");

		return innerHtml.toString();
	}

	/**
	 * deleteCLR is a method for deleting CLR from myCoupon and allCoupons.
	 * 
	 * @param id
	 *            contains coupon id to be deleted.
	 * @param clrName
	 *            contains CLR type.
	 * @param request
	 * @param response
	 * @param session
	 * @return String returns viewName to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/clrdelete", method = RequestMethod.GET)
	public @ResponseBody
	String deleteCLR(@RequestParam(value = "couponId", required = true) Integer id, @RequestParam(value = "clrName", required = true) String clrName,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{
		String methodName = "deleteCLR";
		String daoResponse = null;
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");

			Users loginUser = (Users) session.getAttribute("loginuser");
			Long userId = loginUser.getUserID();

			if (userId != null && id != null)
			{

				if ("coupon".equalsIgnoreCase(clrName))
				{
					LOG.info("coupon deleted");
					//daoResponse = galleryService.couponRemove(userId, id);
				}
				else if ("loyality".equalsIgnoreCase(clrName))
				{
					LOG.info("loyalty deleted");
					daoResponse = galleryService.loyaltyRemove(userId, id);
				}
				else if ("rebate".equalsIgnoreCase(clrName))
				{
					LOG.info("rebate deleted");
					daoResponse = galleryService.rebateRemove(userId, id);
				}

			}

		}
		catch (ScanSeeServiceException e)
		{
			LOG.info(ApplicationConstants.ERROROCCURRED + methodName);
			throw e;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

	/*
	 * @RequestMapping(value = "/addusedclr.htm", method = RequestMethod.POST)
	 * public String addUsedCLR(@ModelAttribute("clrForm") CLRDetails clrForm,
	 * BindingResult result, HttpServletRequest request, HttpServletResponse
	 * response, ModelMap model, HttpSession session) { final String methodName
	 * = "getCouponDetails"; LOG.info(ApplicationConstants.METHODSTART +
	 * methodName); String viewName = null; String daoResponse = null; try {
	 * ServletContext servletContext = request.getSession().getServletContext();
	 * WebApplicationContext appContext =
	 * WebApplicationContextUtils.getWebApplicationContext(servletContext);
	 * MyGalleryService galleryService = (MyGalleryService)
	 * appContext.getBean("myGalleryService"); Users loginUser = (Users)
	 * request.getSession().getAttribute("loginuser"); Long userId =
	 * loginUser.getUserID(); String clrType = clrForm.getClrType(); if ((userId
	 * != null && clrForm.getCouponId()!=null) &&
	 * clrType.equalsIgnoreCase("coupon")) { daoResponse =
	 * galleryService.couponAdd(userId, clrForm.getCouponId()); } else if
	 * ((userId != null && clrForm.getLoyaltyDealID() !=null) &&
	 * clrType.equalsIgnoreCase("loyality")) { daoResponse =
	 * galleryService.loyaltyAdd(userId, clrForm.getLoyaltyDealID()); } else if
	 * ((userId != null && clrForm.getRebateId() !=null) &&
	 * clrType.equalsIgnoreCase("rebate")) { daoResponse =
	 * galleryService.rebateAdd(userId, clrForm.getRebateId()); } if
	 * (daoResponse.equalsIgnoreCase("SUCCESS")) { viewName = "addusedclr.htm";
	 * } } catch (Exception exception) {
	 * LOG.error("Error occureed in galleryhome.htm", exception); }
	 * LOG.info(ApplicationConstants.METHODEND + methodName); //return new
	 * ModelAndView(new RedirectView(viewName)); return viewName; }
	 */

	/**
	 * userRedeemCLR is a method for marking CLR as used(redeeming CLR). It is
	 * done after clipping the coupon.
	 * 
	 * @param clrForm
	 *            contains clrType,couponId,rebateId and layaltyId.
	 * @param result
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return ModelAndView returns viewName to be displayed along with the
	 *         model values.
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/userredeemclr.htm", method = RequestMethod.POST)
	public ModelAndView userRedeemCLR(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "userRedeemCLR";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		String daoResponse = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");

			Long userId = loginUser.getUserID();

			String clrType = clrForm.getClrType();

			if ((userId != null && clrForm.getCouponId() != null) && clrType.equalsIgnoreCase("coupon"))
			{
				LOG.info("coupon redeemed");
				//daoResponse = galleryService.userRedeemCoupon(userId, clrForm.getCouponId());

			}
			else if ((userId != null && clrForm.getLoyaltyDealID() != null) && clrType.equalsIgnoreCase("loyality"))
			{
				LOG.info("loyalty redeemed");
				daoResponse = galleryService.userRedeemLoyalty(userId, clrForm.getLoyaltyDealID());

			}
			else if ((userId != null && clrForm.getRebateId() != null) && clrType.equalsIgnoreCase("rebate"))
			{
				LOG.info("rebate redeemed");
				daoResponse = galleryService.userRedeemRebate(userId, clrForm.getRebateId());
			}

			if (daoResponse.equalsIgnoreCase("SUCCESS"))
			{
				viewName = "galleryused.htm";

			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new ModelAndView(new RedirectView(viewName));
	}

	/**
	 * paginatedGalleryRecords is a method to get next or previous set of
	 * records.
	 * 
	 * @param clrForm
	 *            contains pageFlowType,lowerLimit and requestType
	 * @param result
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return String returns view name to be displayed.
	 * @throws ScanSeeServiceException
	 */

	@RequestMapping(value = "/getpagedclr.htm", method = RequestMethod.POST)
	public String paginatedGalleryRecords(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "paginatedGalleryRecords";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			String pageFlowType = clrForm.getPageFlowType();
			int lowerLimit = clrForm.getLowerLimit();
			if (null != pageFlowType && !"".equals(pageFlowType))
			{
				if ("Next".equals(pageFlowType))
				{
					lowerLimit = lowerLimit + 10;
					clrForm.setLowerLimit(lowerLimit);
				}
				else
				{
					if (lowerLimit > 0)
					{
						lowerLimit = lowerLimit - 10;
						clrForm.setLowerLimit(lowerLimit);
					}
					else
					{
						clrForm.setLowerLimit(0);
					}
				}
			}
			String requestType = clrForm.getRequestType();
			clrForm.setUserId(Utility.longToInteger(loginUser.getUserID()));
			CLRDetails clrResult = galleryService.getMyGalleryAllTypeInfo(clrForm, requestType);
			clrResult.setLowerLimit(lowerLimit);
			if (requestType.equalsIgnoreCase("ALL"))
			{
				LOG.info("allGalleryResult");
				request.setAttribute("allGalleryResult", clrResult);
				viewName = "galleryall";
			}
			else if (requestType.equalsIgnoreCase("EXPIRED"))
			{
				LOG.info("expiredGalleryResult");
				request.setAttribute("expGalleryResult", clrResult);
				viewName = "galleryexp";
			}
			else if (requestType.equalsIgnoreCase("USED"))
			{
				LOG.info("usedGalleryResult");
				request.setAttribute("usedGalleryResult", clrResult);
				viewName = "galleryused";
			}
			else
			{
				LOG.info("myGalleryResult");
				request.setAttribute("galleryResult", clrResult);
				viewName = "mygallery";
			}
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}

	@RequestMapping(value = "/addusedclr", method = RequestMethod.GET)
	public @ResponseBody
	String addCLR(@RequestParam(value = "clrId", required = true) Integer id, @RequestParam(value = "clrType", required = true) String clrType,
			@RequestParam(value = "addFlag", required = true) String addedFlag,

			HttpServletRequest request, HttpServletResponse response, HttpSession session)

	{
		final String methodName = "addCLR";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String daoResponse = null;
		String html = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");

			Long userId = loginUser.getUserID();

			if (userId != null && id != null)
			{

				if (clrType.equalsIgnoreCase("coupon"))
				{
					//daoResponse = galleryService.couponAdd(userId, id);

					// html =
					// "<li><a href='#'><img src='../images/tab_up_add_cpns.png' alt='Expired' width='80' height='50' onclick ='addCLR("+id+",\"coupon\",\"1\")'/>";

				}
				else if (clrType.equalsIgnoreCase("loyality"))
				{

					daoResponse = galleryService.loyaltyAdd(userId, id);
					// html =
					// "<li><a href='#'><img src='../images/tab_up_add_cpns.png' alt='Expired' width='80' height='50' onclick ='addCLR("+id+",\"loyality\",\"1\")'/>";

				}
				else if (clrType.equalsIgnoreCase("rebate"))
				{

					daoResponse = galleryService.rebateAdd(userId, id);
					// html =
					// "<li><a href='#'><img src='../images/tab_up_add_cpns.png' alt='Expired' width='80' height='50' onclick ='addCLR("+id+",\"rebate\",\"1\")'/>";

				}
				html = "<li><img src='../images/tab_up_add_cpns.png' alt='Expired' width='80' height='50'/>";
			}

		}
		catch (Exception exception)
		{
			LOG.error("Error occureed in addCLR", exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return html;
	}

	/**
	 * sendMail is a method for sending CLR via email.
	 * 
	 * @param id
	 *            contains couponId or layaltyId or rebateId.
	 * @param clrType
	 *            contain the CLR type
	 * @param toEmailId
	 *            contains receipt email id.
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws
	 */

	@RequestMapping(value = "/sendmail", method = RequestMethod.GET)
	public @ResponseBody
	String sendMail(@RequestParam(value = "clrId", required = true) Integer id, @RequestParam(value = "clrType", required = true) String clrType,
			@RequestParam(value = "toEmailId", required = true) String toEmailId, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)

	{
		final String methodName = "sendMail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String daoResponse = null;

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");

			Integer userId = (int) (loginUser.getUserID().longValue());

			ShareProductInfo shareProductInfoObj = new ShareProductInfo();
			shareProductInfoObj.setUserId(userId);
			shareProductInfoObj.setToEmail(toEmailId);

			if (userId != null && id != null)
			{

				if (clrType.equalsIgnoreCase("coupon"))

				{

					shareProductInfoObj.setCouponId(id);

					daoResponse = galleryService.shareCLRbyEmail(shareProductInfoObj);

				}
				else if (clrType.equalsIgnoreCase("loyality"))
				{
					shareProductInfoObj.setLoyaltyId(id);
					daoResponse = galleryService.shareCLRbyEmail(shareProductInfoObj);

				}
				else if (clrType.equalsIgnoreCase("rebate"))
				{

					shareProductInfoObj.setRebateId(id);
					daoResponse = galleryService.shareCLRbyEmail(shareProductInfoObj);

				}

			}

		}
		catch (Exception exception)
		{
			LOG.error("Error occureed in sendMail", exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

	/**
	 * This method is used to add products to wish list. accepts a GET request
	 * in mime type text/plain
	 * 
	 * @param productIds
	 *            contains product id's to be added to wishList.
	 * @param request
	 * @param session
	 * @param response
	 * @return String returns whether the product is successfully added or
	 *         exists already.
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/addWishListwithproduct", method = RequestMethod.GET)
	public @ResponseBody
	String addWLProduct(@RequestParam(value = "productIds", required = true) StringBuffer productIds, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{

		final String methodName = "addWLProduct";
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");

		Long userId = loginUser.getUserID();

		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		objAreaInfoVO.setUserID(userId);
		String sResponse = null;
		try
		{
			sResponse = galleryService.addWishListProd(objAreaInfoVO, productIds);

			if ("Exists".equals(sResponse))
			{
				LOG.info("Product already added to Wish List.");
				sResponse = "Product already added to Wish List.";

			}
			else
			{
				LOG.info("Product Successfully added");
				sResponse = "Product Successfully added";

			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return sResponse;
	}

	/**
	 * fbRedirectCouponDetails is a method for sharing coupon in facebook.
	 * 
	 * @param clrFormobj
	 * @param result
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return String returns view name to be displayed.
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/fetchclrdetails.htm", method = RequestMethod.GET)
	public String fbRedirectCouponDetails(@ModelAttribute("clrForm") CLRDetails clrFormobj, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "fbRedirectCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ArrayList<ProductDetail> productlist = null;
		String productIdWithComma = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			CLRDetails clrForm = (CLRDetails) session.getAttribute("fbRedirectclrForm");
			clrForm.setUserId(Utility.longToInteger(loginUser.getUserID()));
			String clrType = clrForm.getClrType();
			String requestType = clrForm.getRequestType();
			// String addedFlag = request.getParameter("added");
			String imagePath = clrForm.getClrImagePath();
			if (null != clrType)
			{
				CLRDetails clrDetails = galleryService.getCLRInfoInfo(clrForm, clrType);
				if ("C".equals(clrType))
				{
					LOG.info("coupon shared");
					CouponDetails coupDetails = clrDetails.getCoupDetails().getCouponInfo();
					coupDetails.setCouponImagePath(imagePath);
					productlist = clrDetails.getCoupDetails().getProductLst();
					request.setAttribute("coupondetails", clrDetails.getCoupDetails());
					session.setAttribute("fbcoupondetails", clrDetails.getCoupDetails());
					viewName = "coupondetails";
				}
				else if ("L".equals(clrType))
				{
					LOG.info("loyalty shared");
					LoyaltyDetail loyDetails = clrDetails.getLoyDetails().getLoyaltyInfo();
					loyDetails.setImagePath(imagePath);
					productlist = clrDetails.getLoyDetails().getProductLst();
					request.setAttribute("loyaltydetails", clrDetails.getLoyDetails());
					session.setAttribute("fbloyaltydetails", clrDetails.getLoyDetails());
					viewName = "loyaltydetails";
				}
				else
				{
					LOG.info("rebate shared");
					RebateDetail rebateDetails = clrDetails.getRebDetails().getRebateInfo();
					rebateDetails.setImagePath(imagePath);
					productlist = clrDetails.getRebDetails().getProductLst();
					request.setAttribute("rebatedetails", clrDetails.getRebDetails());
					session.setAttribute("fbrebatedetails", clrDetails.getRebDetails());
					viewName = "rebatedetails";
				}

				productIdWithComma = Utility.getCommaSepartedValues(productlist);
				request.setAttribute("productIdWithComma", productIdWithComma);
				request.setAttribute("addedFlag", clrForm.getAdded());
				request.setAttribute("requestType", requestType);
				request.setAttribute("expired", requestType);
				request.setAttribute("used", requestType);
			}
			model.put("clrForm", clrForm);
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}

	/*
	 * @RequestMapping(value = "/clrredeem", method = RequestMethod.GET) public
	 * @ResponseBody String discountUserRedeemCLR(@RequestParam(value =
	 * "productId", required = true) Integer clrId,
	 * @RequestParam(value = "clrFlag", required = true) String clrFlag,
	 * HttpServletRequest request, HttpServletResponse response, HttpSession
	 * session) { { final String methodName = "getCouponDetails";
	 * LOG.info(ApplicationConstants.METHODSTART + methodName); String
	 * daoResponse = null; try { ServletContext servletContext =
	 * request.getSession().getServletContext(); WebApplicationContext
	 * appContext =
	 * WebApplicationContextUtils.getWebApplicationContext(servletContext);
	 * MyGalleryService galleryService = (MyGalleryService)
	 * appContext.getBean("myGalleryService"); Users loginUser = (Users)
	 * request.getSession().getAttribute("loginuser"); Long userId =
	 * loginUser.getUserID(); if (userId != null && clrId != null) { if
	 * (clrFlag.equalsIgnoreCase("coupon")) { daoResponse =
	 * galleryService.userRedeemCLRDiscount(userId, clrId, clrFlag); } else if
	 * (clrFlag.equalsIgnoreCase("loyality")) { daoResponse =
	 * galleryService.userRedeemCLRDiscount(userId, clrId, clrFlag); } else {
	 * daoResponse = galleryService.userRedeemCLRDiscount(userId, clrId,
	 * clrFlag); } if (daoResponse.equals("Exists")) { daoResponse =
	 * "Already redeemed."; } else { daoResponse = "Redeemed"; } } } catch
	 * (Exception exception) { LOG.error("Error occureed in userredeemclr.htm",
	 * exception); } LOG.info(ApplicationConstants.METHODEND + methodName);
	 * return daoResponse; } }
	 */

	/**
	 * This method is used to fetch the products details associated with the
	 * coupon . accepts a GET request in mime type text/plain
	 * 
	 * @param clrForm
	 *            contains clrType.
	 * @param result
	 * @param request
	 * @param response
	 * @param model
	 * @param session
	 * @return String returns view name to be displayed.
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/fetchclrproductdetails.htm", method = RequestMethod.POST)
	public String getCouponProductDetails(@ModelAttribute("clrForm") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws ScanSeeServiceException
	{
		final String methodName = "getCouponProductDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		ArrayList<ProductDetail> productlist = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			clrForm.setUserId(Utility.longToInteger(loginUser.getUserID()));
			String clrType = clrForm.getClrType();
			if (null != clrType)
			{
				productlist = galleryService.getCLRProductInfo(clrForm, clrType);
				if ("C".equals(clrType))
				{
					request.setAttribute("productdetails", productlist);
					viewName = "couponproductlist";
				}
				else if ("L".equals(clrType))
				{
					request.setAttribute("productdetails", productlist);
					viewName = "loyaltydetails";
				}
				else
				{
					request.setAttribute("productdetails", productlist);
					viewName = "rebatedetails";
				}

			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error("Error occureed in getCouponProductDetails", exception);
			throw exception;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return viewName;
	}

	/**
	 * This method is used to add the products associated with the coupon to the
	 * shopping list. accepts a GET request in mime type text/plain
	 * 
	 * @param productIds
	 *            contains product id's to be added to wishList.
	 * @param request
	 * @param session
	 * @param response
	 * @return String returns whether the product is successfully added or
	 *         exists already.
	 * @throws ScanSeeServiceException
	 */
	@RequestMapping(value = "/addShoppingListwithproduct", method = RequestMethod.GET)
	public @ResponseBody
	String addSLProduct(@RequestParam(value = "productIds", required = true) StringBuffer productIds, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws ScanSeeServiceException
	{

		final String methodName = "addSLProduct";
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
		Users loginUser = (Users) request.getSession().getAttribute("loginuser");

		Long userId = loginUser.getUserID();

		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		objAreaInfoVO.setUserID(userId);
		String sResponse = null;
		try
		{
			sResponse = galleryService.addShoppingListProd(objAreaInfoVO, productIds);

			if ("Exists".equals(sResponse))
			{
				LOG.info("Product already added to Shopping List.");
				sResponse = "Product already added to Shopping List.";

			}
			else
			{
				LOG.info("Product Successfully added");
				sResponse = "Product Successfully added";

			}

		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error("Error occureed in getCouponProductDetails", exception);
			throw exception;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return sResponse;
	}

}

package shopper.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.service.CommonService;
import shopper.service.MyGalleryService;
import shopper.service.ShopperService;
import shopper.service.ThisLocationService;

import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.Offer;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;
import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.exception.ScanSeeWebSqlException;
import common.pojo.Retailer;
import common.pojo.SearchForm;
import common.pojo.SearchResultInfo;
import common.pojo.Users;
import common.pojo.shopper.AreaInfoVO;
import common.pojo.shopper.CLRDetails;
import common.pojo.shopper.CouponDetails;
import common.pojo.shopper.DiscountInfo;
import common.pojo.shopper.DiscountUserInfo;
import common.pojo.shopper.FindNearByDetail;
import common.pojo.shopper.FindNearByDetails;
import common.pojo.shopper.HotDealsDetails;
import common.pojo.shopper.LoyaltyDetail;
import common.pojo.shopper.MediaVO;
import common.pojo.shopper.ProductDetail;
import common.pojo.shopper.ProductDetails;
import common.pojo.shopper.ProductDetailsRequest;
import common.pojo.shopper.ProductRatingReview;
import common.pojo.shopper.ProductSummaryParametersVO;
import common.pojo.shopper.ProductSummaryVO;
import common.pojo.shopper.RebateDetail;
import common.pojo.shopper.RetailerCreatedPages;
import common.pojo.shopper.RetailerDetail;
import common.pojo.shopper.RetailersDetails;
import common.pojo.shopper.ShareProductInfo;
import common.pojo.shopper.ThisLocationRequest;
import common.pojo.shopper.ThisLocationRetailerInfo;
import common.pojo.shopper.UserRatingInfo;
import common.tags.Pagination;
import common.util.Utility;

/**
 * This class is a controller class for this location page
 * 
 * @author malathi_lr
 */
@Controller
public class ThisLocationController
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ThisLocationController.class);

	/**
	 * This method is used to get the details of the online retailers for the
	 * particular position
	 * 
	 * @param request
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/retailerinfo.htm", method = RequestMethod.POST)
	public String getRetailersInfoForLocation(HttpServletRequest request, HttpSession session, ModelMap model)
	{
		LOG.info("Entered in getRetailersInfoForLocation Get Method....");
		session.removeAttribute("currentspecial");
		String responseXML = null;
		String radius = null;
		String distance = null;
		int lowerLimit = 0;
		Pagination objPage = null;
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		RetailersDetails retailsDetails = new RetailersDetails();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
		ThisLocationRequest thisLocationRequestObj = new ThisLocationRequest();
		int currentPage = 1;
		String pageNumber = "0";
		String zipcode=null;
		String retZipcode=null;
		String pageFlag = (String) request.getParameter("pageFlag");
		String flag = (String) session.getAttribute("page");
		if ("true".equals(flag))
		{
			pageFlag = flag;
			pageNumber = (String) session.getAttribute("pageNumber");
			session.setAttribute("page", "");
			session.setAttribute("pageNumber", "");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			pageFlag = "false";
		}
		if (null != pageFlag && pageFlag.equals("true"))
		{
			pageNumber = request.getParameter("pageNumber");
			session.setAttribute("page", pageFlag);
			session.setAttribute("pageNumber", pageNumber);
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}
			
		}

		AreaInfoVO objAreaInfoVO = new AreaInfoVO();
		 zipcode = request.getParameter("zipcodeTL");
		String cityNameTL = request.getParameter("cityNameTL");
		Double lattitude = null;
		Double longitude = null;
		
		if (null != pageFlag && pageFlag.equals("true"))
		{
			retZipcode=(String)session.getAttribute("retzipcode");	
		}else
		{
			retZipcode=zipcode;
		}
		
				
		if (retZipcode != null && !"".equals(retZipcode))
		{
			objAreaInfoVO.setZipCode(retZipcode);
			thisLocationRequestObj = thisLocationService.getLatLong(zipcode);
			if (thisLocationRequestObj != null)
			{
				session.setAttribute("zipcodeLat", thisLocationRequestObj.getLatitude());
				session.setAttribute("zipcodeLong", thisLocationRequestObj.getLongitude());
			}
		}
		else if (cityNameTL != null && !"".equals(cityNameTL))
		{
			if (!"".equals(request.getParameter("latitude")) && null != request.getParameter("latitude"))
			{
				lattitude = Double.parseDouble(request.getParameter("latitude"));
			}
			if (!"".equals(request.getParameter("longitude")) && null != request.getParameter("longitude"))
			{
				longitude = Double.parseDouble(request.getParameter("longitude"));
			}

			objAreaInfoVO.setCityNameTL(cityNameTL);
			objAreaInfoVO.setLattitude(lattitude);
			objAreaInfoVO.setLongitude(longitude);

		}
		else if ((AreaInfoVO) session.getAttribute("objAreaInfoVO") != null)
		{
			AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
			objAreaInfoVO.setZipCode(objAreaInfoVO1.getZipCode());
			objAreaInfoVO.setCityNameTL(objAreaInfoVO1.getCityNameTL());
			objAreaInfoVO.setLattitude(objAreaInfoVO1.getLattitude());
			objAreaInfoVO.setLongitude(objAreaInfoVO1.getLongitude());
		}
		objAreaInfoVO.setUserID(userId);
		objAreaInfoVO.setLowerLimit(lowerLimit);
		if (request.getParameter("raiudTL") != null && !"".equals(request.getParameter("raiudTL")))
		{
			radius = (request.getParameter("raiudTL"));
			Double selRadius = Double.parseDouble(radius);
			objAreaInfoVO.setPrefdRadius(selRadius);

		}
		else if (session.getAttribute("objAreaInfoVO") != null)
		{
			AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
			objAreaInfoVO.setRadius(objAreaInfoVO1.getRadius());
		}

		retailsDetails = thisLocationService.getRetailersInfoForLocation(objAreaInfoVO);
		if (retailsDetails != null)
		{
			request.setAttribute("retailsDetails", retailsDetails);
			objPage = Utility.getPagination(retailsDetails.getTotalSize(), currentPage, "retailerinfo.htm", 30);

		}
		else
		{
			request.setAttribute("message", "Retailers not found for the given zipcode.");
			if (cityNameTL != null && !"".equals(cityNameTL))
				request.setAttribute("message", "Retailers not found for the entered city.");
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}

		LOG.info("Exited in getRetailersInfoForLocation Get Method....");
		session.setAttribute("objAreaInfoVO", objAreaInfoVO);
		session.setAttribute("zipcode", retZipcode);
		session.setAttribute("radius", radius);
		request.setAttribute("cityNameTL", cityNameTL);
		session.setAttribute("pagination", objPage);
		session.setAttribute("retzipcode", retZipcode);
		session.setAttribute("retlatitude", lattitude);
		session.setAttribute("retlongitude", longitude);

		// setting the distance in session scope and it will be used in retailer
		// summary screen.

		return "thislocation";

	}

	/**
	 * This method for fetching Products information for the specified Category
	 * and Retailer location ID. Method: POST
	 */

	@RequestMapping(value = "/productinfo.htm", method = RequestMethod.POST)
	public String getProductsInfo(@ModelAttribute("thislocation") ThisLocationRetailerInfo thisLocationRetailerInfo, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		int lowerLimit = 0;
		int currentPage = 1;
		String pageNumber = "0";
		Pagination objPage = null;
		String cityNameTL = null;
		String pageFlag = (String) request.getParameter("pageFlag");
		request.setAttribute("selbutn", "sales");
		String zipcode = "";
		Integer radius = 0;
		AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		zipcode = objAreaInfoVO1.getZipCode();
		radius = objAreaInfoVO1.getRadius();
		cityNameTL = objAreaInfoVO1.getCityNameTL();
		String flag = (String) session.getAttribute("prodPage");

		if ("true".equals(flag))
		{
			pageFlag = flag;
			pageNumber = (String) session.getAttribute("prodPageNumber");
			session.setAttribute("prodPage", "");
			session.setAttribute("prodPageNumber", "");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			pageFlag = "false";
		}

		if (null != pageFlag && pageFlag.equals("true"))
		{
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
				session.setAttribute("prodPage", pageFlag);
				session.setAttribute("prodPageNumber", pageNumber);
			}
			else
			{
				lowerLimit = 0;
			}

		}
		ProductDetailsRequest productDetailsRequest = new ProductDetailsRequest();
		productDetailsRequest.setLowerLimit(lowerLimit);
		productDetailsRequest.setUserId(userId);
		Integer retailLocationID = thisLocationRetailerInfo.getRetailLocationID();
		Integer retailerId = thisLocationRetailerInfo.getRetailerId();
		String retailerName = thisLocationRetailerInfo.getRetailerName();
		productDetailsRequest.setRetailLocationID(retailLocationID);
		productDetailsRequest.setRetailID(retailerId);
		LOG.info("Entered in getProductsInfo Get Method....");
		ProductDetails prodcutDetails = new ProductDetails();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
		prodcutDetails = thisLocationService.getProductsInfo(productDetailsRequest);
		if (prodcutDetails != null)
		{
			List<ProductDetail> productDetail = prodcutDetails.getProductDetail();
			for (int i = 0; i < productDetail.size(); i++)
			{
				ProductDetail objProductDetail = productDetail.get(i);
				if (objProductDetail.getSalePrice().contains("."))
				{

				}
				else
				{
					String salePrice = objProductDetail.getSalePrice();
					salePrice = salePrice + ".00";
					objProductDetail.setSalePrice(salePrice);
				}

			}
			request.setAttribute("prodcutDetails", prodcutDetails);
			objPage = Utility.getPagination(prodcutDetails.getTotalSize(), currentPage, "productinfo.htm", 30);
			session.setAttribute("pagination", objPage);
		}
		else
		{
			request.setAttribute("message", "None Available ,Select another category below.");
		}
		request.setAttribute("retailerId", retailerId);
		request.setAttribute("retailLocationID", retailLocationID);
		request.setAttribute("retailerName", retailerName);
		request.setAttribute("zipcode", zipcode);
		request.setAttribute("radius", radius);
		request.setAttribute("cityNameTL", cityNameTL);
		session.setAttribute("pagination", objPage);
		session.setAttribute("moduleName", "thislocation");
		LOG.info("Exited in getProductsInfo Get Method....");
		session.removeAttribute("FromSLSearch");
		session.removeAttribute("fromSLFavSearch");
		session.removeAttribute("fromSL");
		session.removeAttribute("fromSLFAV");
		session.removeAttribute("fromSLHistory");
		return "thislocation";

	}

	/**
	 * This method for fetching Products information for the specified Category
	 * and Retailer location ID. Method: POST
	 */

	@RequestMapping(value = "/productSummary.htm", method = RequestMethod.POST)
	public String getProductsSummary(@ModelAttribute("productsummary") ProductDetail productDetail, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{
		LOG.info("Entered in getProductsInfo Get Method....");
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		Integer productId = productDetail.getProductId();
		Integer radius = 0;
		Double latitude = 0.0;
		Double longitude = 0.0;
		Integer lowerLimit = 0;
		String postalcode = "";
		String cityNameTL = null;
		String retailLocationID = productDetail.getRetailLocationID();
		ProductSummaryParametersVO productSummaryParametersVO = null;
		AreaInfoVO objAreaInfoVO = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO != null)
		{
			radius = objAreaInfoVO.getRadius();
			latitude = objAreaInfoVO.getLattitude();
			longitude = objAreaInfoVO.getLongitude();
			lowerLimit = objAreaInfoVO.getLowerLimit();
			postalcode = objAreaInfoVO.getZipCode();
			cityNameTL = objAreaInfoVO.getCityNameTL();
		}

		Integer retailerID = productDetail.getRetailerId();
		String regularPrice = productDetail.getRegularPrice();
		ProductDetailsRequest productDetailsRequest = new ProductDetailsRequest();
		productDetailsRequest.setRetailID(retailerID);
		productDetailsRequest.setLatitude(latitude);
		productDetailsRequest.setLongitude(longitude);
		productDetailsRequest.setLowerLimit(lowerLimit);
		productDetailsRequest.setRadius(radius);
		productDetailsRequest.setProductId(productId);
		productDetailsRequest.setUserId(userId);
		productDetailsRequest.setPostalcode(postalcode);
		String retailerName = productDetail.getRetailerName();
		if (retailerName == null || retailerName.equals(""))
		{
			String retailerNameProductSum = (String) session.getAttribute("retailerNameProductSum");
			if (null != retailerNameProductSum)
			{
				retailerName = retailerNameProductSum;
			}

		}
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
		ProductSummaryVO productSummaryVO = shoppingListService.getProductSummary(productDetailsRequest);
		LOG.info("Exited in getProductsInfo Get Method....");
		request.setAttribute("productSummaryVO", productSummaryVO);
		request.setAttribute("productId", productId);
		session.setAttribute("regularPrice", regularPrice);
		request.setAttribute("retailID", retailerID);
		request.setAttribute("retailerName", retailerName);
		session.setAttribute("retailerNameProductSum", retailerName);
		request.setAttribute("retailLocationID", retailLocationID);
		if (productSummaryVO.getFindNearByDetailsResultSet().getFindNearByMetaData() != null)
		{
			String price = productSummaryVO.getFindNearByDetailsResultSet().getFindNearByMetaData().getLowestPrice();
			if (price != null)
			{
				request.setAttribute("price", productSummaryVO.getFindNearByDetailsResultSet().getFindNearByMetaData().getLowestPrice());
			}
		}
		session.setAttribute("productNameReview", productSummaryVO.getProductDetail().getProductName());
		String zipcode = "";
		AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO1 != null)
		{
			zipcode = objAreaInfoVO1.getZipCode();
			request.setAttribute("zipcode", zipcode);
			request.setAttribute("radius", radius);
			request.setAttribute("cityNameTL", cityNameTL);
		}
		ProductSummaryParametersVO productSummaryParametersVO1 = (ProductSummaryParametersVO) session.getAttribute("productSummaryParametersVO");
		if (productSummaryParametersVO1 != null)
		{
			if (productSummaryParametersVO1.getRetailerId() != null)
			{
				if (productSummaryParametersVO1.getRetailerId() == retailerID || retailerID == null)
				{
					productSummaryParametersVO = (ProductSummaryParametersVO) session.getAttribute("productSummaryParametersVO");
					session.setAttribute("productSummaryParametersVO", productSummaryParametersVO);
				}
				else if (productSummaryParametersVO1.getRetailerId() != retailerID && retailerID != null)
				{
					productSummaryParametersVO = new ProductSummaryParametersVO();
					productSummaryParametersVO.setRetailerId(retailerID);
					productSummaryParametersVO.setRetailerName(retailerName);
					productSummaryParametersVO.setRetailLocationID(retailLocationID);
					session.setAttribute("productSummaryParametersVO", productSummaryParametersVO);
				}

			}
			else
			{
				productSummaryParametersVO = (ProductSummaryParametersVO) session.getAttribute("productSummaryParametersVO");
				session.setAttribute("productSummaryParametersVO", productSummaryParametersVO);
			}
		}
		else
		{
			productSummaryParametersVO = new ProductSummaryParametersVO();
			productSummaryParametersVO.setRetailerId(retailerID);
			productSummaryParametersVO.setRetailerName(retailerName);
			productSummaryParametersVO.setRetailLocationID(retailLocationID);
			session.setAttribute("productSummaryParametersVO", productSummaryParametersVO);
		}
		String frmWLSearch = (String) session.getAttribute("FromWLSearch");
		if ("true".equals(frmWLSearch))
		{
			session.setAttribute("moduleName", "WLSearch");
		}
		String frmSLFavSearch = (String) session.getAttribute("fromSLFavSearch");
		if ("true".equals(frmSLFavSearch))
		{
			session.setAttribute("moduleName", "SLFavSearch");
		}
		String frmSLSearch = (String) session.getAttribute("FromSLSearch");
		if ("true".equals(frmSLSearch))
		{
			session.setAttribute("moduleName", "SLSearch");
		}
		String strProdSearch = (String) session.getAttribute("prodSearch");
		if ("true".equals(strProdSearch))
		{
			session.setAttribute("prodDet", "true");
		}
		String fromFind = (String) session.getAttribute("fromFind");

		if ("fromFind".equals(fromFind))
		{
			session.setAttribute("moduleName", fromFind);
		}
		String fromScannow = (String) session.getAttribute("fromscannow");

		if ("fromscannow".equals(fromScannow))
		{
			session.setAttribute("moduleName", fromScannow);
		}
		String fromSL = (String) session.getAttribute("fromSL");
		if ("SLMain".equals(fromSL))
		{
			session.setAttribute("moduleName", fromSL);
		}
		String fromSLFAV = (String) session.getAttribute("fromSLFAV");
		if ("SLFAV".equals(fromSLFAV))
		{
			session.setAttribute("moduleName", fromSLFAV);
		}
		String fromSLHistory = (String) session.getAttribute("fromSLHistory");
		if ("SLHistory".equals(fromSLHistory))
		{
			session.setAttribute("moduleName", fromSLHistory);
		}
		String fromWL = (String) session.getAttribute("fromWL");
		if ("WLMain".equals(fromWL))
		{
			session.setAttribute("moduleName", fromWL);
		}
		String fromWLHistory = (String) session.getAttribute("fromWLHistory");
		if ("WLHistory".equals(fromWLHistory))
		{
			session.setAttribute("moduleName", fromWLHistory);
		}
		if (productDetail.isSearchProd() == true)
		{
			request.setAttribute("fromSearchprod", productDetail.isSearchProd());
		}

		String fromThisLocation = (String) request.getParameter("fromThisLocation");
		if ("true".equals(fromThisLocation))
		{
			if (null == productDetail.getSalePrice() || productDetail.getSalePrice().equals("") || productDetail.getSalePrice().equals("$"))
			{

				String salepriceProductSum = (String) session.getAttribute("salepriceProductSum");
				if (null != salepriceProductSum)
				{
					productDetail.setSalePrice(salepriceProductSum);
				}

			}
			request.setAttribute("saleprice", productDetail.getSalePrice());
			session.setAttribute("salepriceProductSum", productDetail.getSalePrice());
		}

		ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
		ArrayList<ThisLocationRequest> radiuslst = null;

		radiuslst = thisLocationService.getRediusInfo();

		if (radiuslst != null && !radiuslst.isEmpty())
		{
			session.setAttribute("radiuslst", radiuslst);

		}

		return "productsummary";

	}

	/**
	 * This is a RestEasy WebService Method for finding near by retailers.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be find near by
	 *            retailers.
	 * @return XML containing near by retailers as response.
	 */
	@RequestMapping(value = "/nearBy.htm", method = RequestMethod.POST)
	public String findNearByRetailers(@ModelAttribute("productsummary") ProductDetail productDetail, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{
		final String methodName = "findNearByRetailers of ShoppingListController";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		AreaInfoVO objAreaInfoVO = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		Integer productId = productDetail.getProductId();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
		FindNearByDetails findNearByDetails = shoppingListService.findNearBy(objAreaInfoVO, productId);
		if (findNearByDetails != null)
		{
			request.setAttribute("findNearByDetails", findNearByDetails);
			session.setAttribute("retailerDetails", findNearByDetails);
		}
		else
		{
			request.setAttribute("message", "Near by details are not available for the product.");
		}

		String zipcode = "";
		if (objAreaInfoVO != null)
		{
			zipcode = objAreaInfoVO.getZipCode();
			request.setAttribute("zipcode", zipcode);
			request.setAttribute("radius", objAreaInfoVO.getRadius());
			request.setAttribute("cityNameTL", objAreaInfoVO.getCityNameTL());
		}
		request.setAttribute("productId", productId);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "productsummary";
	}

	/**
	 * This is a RestEasy WebService Method for finding retailer details in the
	 * nearby section. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be find near by
	 *            retailers.
	 * @return XML containing near by retailers as response.
	 */
	@RequestMapping(value = "/nearByReatailer.htm", method = RequestMethod.POST)
	public String findNearByRetailerDeatils(@ModelAttribute("productsummary") FindNearByDetail findNearByDetail, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		final String methodName = "findNearByRetailers of ShoppingListController";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer retailerId = findNearByDetail.getRetailerId();
		String distance = findNearByDetail.getDistance();
		String productPrice = findNearByDetail.getProductPrice();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
		Retailer retailer = shoppingListService.findNearByRetailerDetail(retailerId);
		request.setAttribute("retailer", retailer);
		request.setAttribute("distance", distance);
		request.setAttribute("productPrice", productPrice);
		String zipcode = "";
		AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO1 != null)
		{
			zipcode = objAreaInfoVO1.getZipCode();
			request.setAttribute("zipcode", zipcode);
			request.setAttribute("radius", objAreaInfoVO1.getRadius());
			request.setAttribute("cityNameTL", objAreaInfoVO1.getCityNameTL());
		}
		String lattitude = null;
		String longitude = null;
		FindNearByDetails findNearByDetails = (FindNearByDetails) session.getAttribute("retailerDetails");
		for (int i = 0; i < findNearByDetails.getFindNearByDetail().size(); i++)
		{
			if (findNearByDetails.getFindNearByDetail().get(i).getRetailerId().equals(retailerId))
			{
				lattitude = findNearByDetails.getFindNearByDetail().get(i).getLatitude();
				longitude = findNearByDetails.getFindNearByDetail().get(i).getLongitude();
			}
			
			
			
			

		}
		request.setAttribute("productId", findNearByDetail.getProductId());
		request.setAttribute("latitude", lattitude);
		request.setAttribute("longitude", longitude);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "productsummary";
	}

	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given userId,productId and retailId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which product information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productId
	 *            for which product information need to be fetched.If productId
	 *            is null then it is invalid request.
	 * @param retailId
	 *            for which product information need to be fetched.If retailId
	 *            is null then it is invalid request.
	 * @return XML containing product information in the response.
	 */
	@RequestMapping(value = "/productdesc.htm", method = RequestMethod.POST)
	public String getProductInfo(@ModelAttribute("productsummary") ProductDetail productDetail, HttpServletRequest request, HttpSession session,
			ModelMap model)
	{

		final String methodName = "getProductInfo of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		Integer productId = productDetail.getProductId();
		Integer retailId = productDetail.getRetailerId();
		ProductDetail productinfo = new ProductDetail();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService shoppingListService = (CommonService) appContext.getBean("commonService");

		try
		{
			productinfo = shoppingListService.fetchProductAttributes(userId, productId);

			request.setAttribute("productinfo", productinfo);
			String zipcode = "";
			AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
			if (objAreaInfoVO1 != null)
			{
				zipcode = objAreaInfoVO1.getZipCode();
				request.setAttribute("zipcode", zipcode);
				request.setAttribute("radius", objAreaInfoVO1.getRadius());
				request.setAttribute("cityNameTL", objAreaInfoVO1.getCityNameTL());
			}
			LOG.info(ApplicationConstants.METHODEND + methodName);
			request.setAttribute("productId", productId);
			session.setAttribute("retailId", retailId);
		}
		catch (ScanSeeWebSqlException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "productsummary";
	}

	/**
	 * this method is used for normal product search.
	 * 
	 * @param searchForm
	 * @param request
	 * @param session
	 * @param model
	 * @param session1
	 * @return
	 */
	@RequestMapping(value = "/productsearchresults.htm", method = RequestMethod.POST)
	public ModelAndView productSearch(@ModelAttribute("SearchForm") SearchForm searchForm, HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1)
	{

		int lowerLimit = 0;
		String pageFlag = (String) request.getParameter("pageFlag");
		Users loginUser = (Users) session.getAttribute("loginuser");
		String zipCode = null;
		Integer objRadius = null;
		String searhKey = null;
		String searchType = null;
		int currentPage = 1;
		String pageNumber = "0";
		String view = null;
		Pagination objPage = null;
		SearchForm objForm = null;
		if (searchForm.getSearchKey() == null)
		{
			searchForm = (SearchForm) session.getAttribute("formDetails");
		}
		Integer retailerId = searchForm.getRetailerId();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");

		/*
		 * 1) Store search parameters in Session object 2) check and get the
		 * Parameters in session object 3) Check login state and deligate based
		 * on state 4) Pagination
		 */
		if (null != pageFlag && pageFlag.equals("true"))
		{
			objForm = (SearchForm) session.getAttribute("searchForm");
			searchType = objForm.getSearchType();
			pageNumber = request.getParameter("pageNumber");
			Pagination pageSess = (Pagination) session.getAttribute("pagination");
			if (Integer.valueOf(pageNumber) != 0)
			{
				currentPage = Integer.valueOf(pageNumber);
				int number = Integer.valueOf(currentPage) - 1;
				int pageSize = pageSess.getPageRange();
				lowerLimit = pageSize * Integer.valueOf(number);
			}
			else
			{
				lowerLimit = 0;
			}

		}
		else
		{
			AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
			if (objAreaInfoVO1 != null)
			{
				zipCode = objAreaInfoVO1.getZipCode();
				objRadius = objAreaInfoVO1.getRadius();
			}
			if (zipCode == null)
				zipCode = loginUser.getPostalCode();

			searhKey = searchForm.getSearchKey();
			searchType = searchForm.getSearchType();

			objForm = new SearchForm(zipCode, searhKey, searchType);

			if (objRadius != null)
			{
				objForm.setRadius(objRadius);
			}

			session.setAttribute("searchForm", objForm);

		}

		try
		{
			SearchResultInfo resultInfo = shopperService.searchProducts(loginUser, objForm, lowerLimit);

			view = "productList";
			request.setAttribute("seacrhList", resultInfo);
			if (resultInfo != null && resultInfo.getTotalSize() > 0)
			{
				objPage = Utility.getPagination(resultInfo.getTotalSize(), currentPage, "productsearchresults.htm");
				session.setAttribute("pagination", objPage);
			}
			else
			{
				request.setAttribute("message", "No product found.");
			}
			String tlFind = searchForm.getTlFind();
			request.setAttribute("retailerId", retailerId);
			if ("tlFind".equals(tlFind))
			{
				view = "thislocation";
				session.setAttribute("prodSearch", "true");
			}

			session.setAttribute("formDetails", searchForm);
		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}

		return new ModelAndView(view);

	}

	@RequestMapping(value = "/getdiscount.htm", method = RequestMethod.POST)
	public ModelAndView getDiscount(@ModelAttribute("productsummary") DiscountInfo DiscountInfo, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException
	{

		LOG.info("Entering in to DiscountController......");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");

		DiscountInfo discountInfo = null;
		Integer productId = 0;
		String view = null;

		try
		{

			Users loginUser = (Users) session.getAttribute("loginuser");

			Long userId = loginUser.getUserID();

			productId = Integer.parseInt(request.getParameter("productId"));

			Integer retailID = Integer.parseInt(request.getParameter("retailerId"));

			DiscountUserInfo discountUserInfo = new DiscountUserInfo();
			discountUserInfo.setProductID(productId);
			discountUserInfo.setRetailID(retailID);
			discountUserInfo.setUserID(userId);

			discountInfo = shopperService.getDiscountInfo(discountUserInfo);

			view = "productsummary";

			request.setAttribute("discountinfo", discountInfo);
			String zipcode = "";
			AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
			if (objAreaInfoVO1 != null)
			{
				zipcode = objAreaInfoVO1.getZipCode();
				request.setAttribute("zipcode", zipcode);
				request.setAttribute("radius", objAreaInfoVO1.getRadius());
				request.setAttribute("cityNameTL", objAreaInfoVO1.getCityNameTL());
			}
			session.setAttribute("productId", productId);
			session.setAttribute("retailID", retailID);

		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}
		LOG.info("ShopperProfileController :: Inside Exit Post Method");
		request.setAttribute("productId", productId);
		return new ModelAndView(view);
	}

	/**
	 * This is a RestEasy WebService Method for fetching product reviews and
	 * user ratings.Method Type:GET.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML Containing Product Reviews and User Ratings.
	 */
	@RequestMapping(value = "/reviews.htm", method = RequestMethod.POST)
	public String getProductReviews(@ModelAttribute("productsummary") ProductDetail productDetail, HttpServletRequest request, HttpSession session,
			ModelMap model, HttpSession session1)
	{
		final String methodName = "getProductReviews";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		Integer productId = productDetail.getProductId();

		ProductRatingReview productRatingReview = new ProductRatingReview();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
		try
		{
			productRatingReview = shoppingListService.getProductReviews(userId, productId);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		request.setAttribute("productRatingReview", productRatingReview);
		String zipcode = "";
		AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO1 != null)
		{
			zipcode = objAreaInfoVO1.getZipCode();
			request.setAttribute("zipcode", zipcode);
			request.setAttribute("radius", objAreaInfoVO1.getRadius());
			request.setAttribute("cityNameTL", objAreaInfoVO1.getCityNameTL());
		}
		if (productDetail.isWlratereview() == true)
		{
			request.setAttribute("fromwlratereview", productDetail.isWlratereview());

		}
		if (productDetail.isWlonlineratereview() == true)
		{
			request.setAttribute("fromwlonlineratereview", productDetail.isWlonlineratereview());

		}
		request.setAttribute("productId", productId);
		request.setAttribute("ratingproductid", productId);
		return "productsummary";
	}

	@RequestMapping(value = "/couponoperation", method = RequestMethod.GET)
	public @ResponseBody
	String couponOperation(@RequestParam(value = "couponId", required = true) Integer couponId,
			@RequestParam(value = "usage", required = true) String usage, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{

		StringBuffer innerHtml = new StringBuffer();

		try
		{
			LOG.info("Entering in to DiscountController......");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
			String daoResponse = null;
			Users loginUser = (Users) session.getAttribute("loginuser");
			Long userId = loginUser.getUserID();
			if (usage.equalsIgnoreCase("Red"))
			{
				daoResponse = shopperService.couponAdd(userId, couponId);
				innerHtml.append("<img src='/ScanSeeWeb/images/cpnIcon.png' alt='Cpn' width='27' height='25' id = 'cpnTg" + couponId
						+ "' name= 'cpnTg" + couponId + "' onclick='couponOperation(" + couponId + " ,\"Green\" , \"cpnTg" + couponId + "\")'/>");
			}
			else if (usage.equalsIgnoreCase("Green"))
			{
				daoResponse = shopperService.couponRemove(userId, couponId);
				innerHtml.append("<img src='/ScanSeeWeb/images/cpnAddicon.png' alt='Cpn' width='27' height='25'  id = 'cpnTg" + couponId
						+ "' name= 'cpnTg" + couponId + "'  onclick='couponOperation(" + couponId + " , \"Red\" , \"cpnTg" + couponId + "\")'/>");
			}
		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}
		LOG.info("ShopperProfileController :: Inside Exit Post Method");

		return innerHtml.toString();
	}

	@RequestMapping(value = "/rebateoperation", method = RequestMethod.GET)
	public @ResponseBody
	String rebateOperation(@RequestParam(value = "rebateId", required = true) Integer rebateId,
			@RequestParam(value = "usage", required = true) String usage, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{

		StringBuffer innerHtml = new StringBuffer();

		try
		{

			LOG.info("Entering in to DiscountController......");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
			String daoResponse = null;
			Users loginUser = (Users) session.getAttribute("loginuser");
			Long userId = loginUser.getUserID();
			if (usage.equalsIgnoreCase("Red"))
			{
				daoResponse = shopperService.rebateAdd(userId, rebateId);
				request.setAttribute("rebatenormalImage", "rebatenormalImage");
				innerHtml.append("<img src='/ScanSeeWeb/images/cpnIcon.png' alt='Cpn' width='27' height='25' id = 'cpnTg" + rebateId
						+ "' name= 'cpnTg" + rebateId + "' onclick='rebateOperation(" + rebateId + " ,\"Green\" , \"cpnTg" + rebateId + "\")'/>");
			}
			else if (usage.equalsIgnoreCase("Green"))
			{
				daoResponse = shopperService.rebateRemove(userId, rebateId);
				request.setAttribute("rebateplusImage", "rebateplusImage");
				innerHtml.append("<img src='/ScanSeeWeb/images/cpnAddicon.png' alt='Cpn' width='27' height='25'  id = 'cpnTg" + rebateId
						+ "' name= 'cpnTg" + rebateId + "'  onclick='rebateOperation(" + rebateId + " , \"Red\" , \"cpnTg" + rebateId + "\")'/>");
			}
		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}
		LOG.info("ShopperProfileController :: Inside Exit Post Method");

		return innerHtml.toString();
	}

	@RequestMapping(value = "/loyalityoperation", method = RequestMethod.GET)
	public @ResponseBody
	String loyalityOperation(@RequestParam(value = "loyaltyDealId", required = true) Integer loyaltyDealId,
			@RequestParam(value = "usage", required = true) String usage, HttpServletRequest request, HttpServletResponse response,
			HttpSession session)
	{

		StringBuffer innerHtml = new StringBuffer();

		try
		{

			LOG.info("Entering in to DiscountController......");
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
			String daoResponse = null;
			Users loginUser = (Users) session.getAttribute("loginuser");
			Long userId = loginUser.getUserID();
			if (usage.equalsIgnoreCase("Red"))
			{
				daoResponse = shopperService.loyaltyAdd(userId, loyaltyDealId);
				request.setAttribute("loyalitynormalImage", "loyalitynormalImage");
				innerHtml.append("<img src='/ScanSeeWeb/images/cpnIcon.png' alt='Cpn' width='27' height='25' id = 'cpnTg" + loyaltyDealId
						+ "' name= 'cpnTg" + loyaltyDealId + "' onclick='loyalityOperation(" + loyaltyDealId + " ,\"Green\" , \"cpnTg"
						+ loyaltyDealId + "\")'/>");
			}
			else if (usage.equalsIgnoreCase("Green"))
			{
				daoResponse = shopperService.loyaltyRemove(userId, loyaltyDealId);
				request.setAttribute("loyalityplusImage", "loyalityplusImage");
				innerHtml.append("<img src='/ScanSeeWeb/images/cpnAddicon.png' alt='Cpn' width='27' height='25'  id = 'cpnTg" + loyaltyDealId
						+ "' name= 'cpnTg" + loyaltyDealId + "'  onclick='loyalityOperation(" + loyaltyDealId + " , \"Red\" , \"cpnTg"
						+ loyaltyDealId + "\")'/>");
			}
		}
		catch (ScanSeeServiceException e)
		{

			e.printStackTrace();
		}
		LOG.info("ShopperProfileController :: Inside Exit Post Method");
		return innerHtml.toString();
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getonlinestoreinfo.htm", method = RequestMethod.POST)
	public ModelAndView getOnlineStoreInfo(@ModelAttribute("productsummary") ProductDetail productDetail, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session) throws IOException
	{

		LOG.info("Entering in to DiscountController......");
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ShopperService shopperService = (ShopperService) appContext.getBean("shopperService");
		String view = null;

		ExternalAPIInformation apiInfo = null;
		List<Offer> onlineStoresList = null;

		ProductDetail productDet = null;
		OnlineStoresRequest onlineStores = null;
		OnlineStoresService onlineStoresService = null;
		OnlineStores onlineStoresinfo = null;

		try
		{

			Users loginUser = (Users) session.getAttribute("loginuser");

			// Integer productId =
			// Integer.parseInt(request.getParameter("productId"));

			// Integer retailID
			// =Integer.parseInt(request.getParameter("retailerId"));

			final String userId = loginUser.getUserID().toString();

			final String productId = productDetail.getProductId().toString();

			LOG.info("userId*************" + userId);
			LOG.info("productId*************" + productId);
			// online stores
			apiInfo = shopperService.externalApiInfo("FindOnlineStores");

			productDet = shopperService.fetchProductDetails(productId);

			onlineStoresService = new OnlineStoresServiceImpl();
			onlineStores = new OnlineStoresRequest();
			onlineStores.setPageStart("0");
			onlineStores.setUpcCode(productDet.getScanCode());
			onlineStores.setUserId(userId);
			onlineStores.setValues();
			onlineStoresinfo = onlineStoresService.onlineStoresInfoEmailTemplate(apiInfo, onlineStores);

			if (onlineStoresinfo != null)
			{
				if (!onlineStoresinfo.getOffers().isEmpty())
				{
					onlineStoresList = onlineStoresinfo.getOffers();
					request.setAttribute("OnlineStoresList", onlineStoresList);
					view = "productsummary";
				}

			}

			/**
			 * For commission junction data integration...
			 */

			CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
			/**
			 * For commission junction data....
			 */
			ArrayList<RetailerDetail> commiJuncDatalst = null;

			commiJuncDatalst = shoppingListService.getCommisJunctionData(Long.valueOf(userId), Integer.parseInt(productId));

			if (!commiJuncDatalst.isEmpty() && commiJuncDatalst != null)
			{
				request.setAttribute("CommiJunctionData", commiJuncDatalst);
				view = "productsummary";
			}

			/*
			 * if (onlineStoresList != null) {
			 * request.setAttribute("OnlineStoresList", onlineStoresList); }
			 * view = "thislocation";
			 */

		}
		catch (ScanSeeServiceException exception)
		{

			exception.printStackTrace();
		}
		catch (NumberFormatException exception)
		{
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		catch (ScanSeeWebSqlException exception)
		{
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		LOG.info("ShopperProfileController :: Inside Exit Post Method");
		String zipcode = "";
		Integer radius = 0;
		AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO1 != null)
		{
			zipcode = objAreaInfoVO1.getZipCode();
			radius = objAreaInfoVO1.getRadius();
			request.setAttribute("zipcode", zipcode);
			request.setAttribute("radius", radius);
			request.setAttribute("cityNameTL", objAreaInfoVO1.getCityNameTL());
		}
		return new ModelAndView(view);
	}

	/**
	 * This is a method for saving user product rating Calls method in service
	 * layer. accepts a POST request, MIME type is text/xml as request
	 * parameter. If userId or productId is null then it is invalid request.
	 * 
	 * @return response containing success or failure response
	 */

	@RequestMapping(value = "/saveuserrating.htm", method = RequestMethod.GET)
	public @ResponseBody
	String saveUserProductRating(@RequestParam(value = "userRating", required = true) int userRating,
			@RequestParam(value = "productId", required = true) String productId, HttpServletRequest request, HttpSession session)
	{
		final String methodName = "fecthUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = "";
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
		UserRatingInfo userRatingInfo = new UserRatingInfo();
		userRatingInfo.setUserId(userId);
		userRatingInfo.setCurrentRating(userRating);
		userRatingInfo.setProductId(productId);
		try
		{
			response = shoppingListService.saveUserProductRating(userRatingInfo);
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for displaying audio,video and other
	 * information.Method Type:GET.
	 * 
	 * @param userId
	 *            used for fetching media information.
	 * @param productID
	 *            used for fetching media information.
	 * @param mediaType
	 *            fetching product media information.
	 * @return XML containing media information as in the response.
	 */
	@RequestMapping(value = "/getMediaInfo.htm", method = RequestMethod.POST)
	public String getMediaInfo(@ModelAttribute("productsummary") MediaVO mediaVO, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session)
	{

		final String methodName = "getMediaInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
		ProductDetails productDetails = new ProductDetails();
		Integer productId = mediaVO.getProductId();
		String mediaType = mediaVO.getMediaType();
		try
		{
			productDetails = shoppingListService.getMediaDetails(productId, mediaType);
		}
		catch (ScanSeeServiceException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
		}
		request.setAttribute("mediaDetails", productDetails);
		if (productDetails == null)
		{
			request.setAttribute("message", "Media Details are not available.");
		}
		request.setAttribute("productId", productId);
		request.setAttribute("mediaType", mediaType);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "productsummary";
	}

	@RequestMapping(value = "/fetchclrinfo.htm", method = RequestMethod.POST)
	public String getCouponDetails(@ModelAttribute("productsummary") CLRDetails clrForm, BindingResult result, HttpServletRequest request,
			HttpServletResponse response, ModelMap model, HttpSession session)
	{
		final String methodName = "getCouponDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String viewName = null;
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			MyGalleryService galleryService = (MyGalleryService) appContext.getBean("myGalleryService");
			Users loginUser = (Users) request.getSession().getAttribute("loginuser");
			String userId = Long.toString(loginUser.getUserID());
			clrForm.setUserId(new Integer(userId));
			String clrType = clrForm.getClrType();
			String requestType = clrForm.getRequestType();
			String addedFlag = request.getParameter("added");
			String imagePath = clrForm.getClrImagePath();
			clrForm.setCouponId(clrForm.getCouponID());// Variable confilict in
														// thisLocation.jsp
			if (null != clrType)
			{
				CLRDetails clrDetails = galleryService.getCLRInfoInfo(clrForm, clrType);
				if (clrType.equals("C"))
				{
					CouponDetails coupDetails = clrDetails.getCoupDetails().getCouponInfo();
					coupDetails.setCouponImagePath(imagePath);
					request.setAttribute("coupondetails", clrDetails.getCoupDetails());
				}
				else if (clrType.equals("L"))
				{
					LoyaltyDetail loyDetails = clrDetails.getLoyDetails().getLoyaltyInfo();
					loyDetails.setImagePath(imagePath);
					request.setAttribute("loyaltydetails", clrDetails.getLoyDetails());
				}
				else
				{
					RebateDetail rebateDetails = clrDetails.getRebDetails().getRebateInfo();
					rebateDetails.setImagePath(imagePath);
					request.setAttribute("rebatedetails", clrDetails.getRebDetails());
				}
				request.setAttribute("addedFlag", addedFlag);
				request.setAttribute("requestType", requestType);
				request.setAttribute("expired", requestType);
				request.setAttribute("used", requestType);
			}

		}
		catch (Exception exception)
		{
			LOG.error("Error occureed in galleryhome.htm", exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return "productsummary";
	}

	@RequestMapping(value = "/shareProdInfobyEmail.htm", method = RequestMethod.GET)
	public @ResponseBody
	String sendShareProdInfoMail(@RequestParam(value = "toEmailId", required = true) String toEmailId,
			@RequestParam(value = "productId", required = true) Integer productId,

			HttpServletRequest request, HttpServletResponse response, HttpSession session)

	{
		final String methodName = "sendShareProdInfoMail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String daoResponse = null;

		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
			ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
			Users loginUser = (Users) session.getAttribute("loginuser");

			Integer userId = (int) (loginUser.getUserID().longValue());

			ShareProductInfo shareProductInfoObj = new ShareProductInfo();
			shareProductInfoObj.setUserId(userId);
			shareProductInfoObj.setToEmail(toEmailId);
			shareProductInfoObj.setProductId(productId);
			daoResponse = thisLocationService.shareProductInfo(shareProductInfoObj);

		}
		catch (Exception exception)
		{
			LOG.error("Error occureed in galleryhome.htm", exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return daoResponse;
	}

	/**
	 * This is a RestEasy WebService Method for fetching product reviews and
	 * user ratings.Method Type:GET.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML Containing Product Reviews and User Ratings.
	 */
	@RequestMapping(value = "/reviews.htm", method = RequestMethod.GET)
	public String fbRedirectProductReviews(@ModelAttribute("productsummary") ProductDetail productDetail, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpSession session1)
	{
		final String methodName = "getProductReviews";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		Integer productId = (Integer) session.getAttribute("fbShareProductId");

		ProductRatingReview productRatingReview = new ProductRatingReview();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonService shoppingListService = (CommonService) appContext.getBean("commonService");
		try
		{
			productRatingReview = shoppingListService.getProductReviews(userId, productId);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		request.setAttribute("productRatingReview", productRatingReview);
		String zipcode = "";
		AreaInfoVO objAreaInfoVO1 = (AreaInfoVO) session.getAttribute("objAreaInfoVO");
		if (objAreaInfoVO1 != null)
		{
			zipcode = objAreaInfoVO1.getZipCode();
			request.setAttribute("zipcode", zipcode);
			request.setAttribute("radius", objAreaInfoVO1.getRadius());
			request.setAttribute("cityNameTL", objAreaInfoVO1.getCityNameTL());
		}
		if (productDetail.isWlratereview() == true)
		{
			request.setAttribute("fromwlratereview", productDetail.isWlratereview());

		}
		if (productDetail.isWlonlineratereview() == true)
		{
			request.setAttribute("fromwlonlineratereview", productDetail.isWlonlineratereview());

		}
		request.setAttribute("productId", productId);
		request.setAttribute("ratingproductid", productId);
		return "productsummary";
	}

	/**
	 * This method for fetching retailer details and retailer created pages
	 * Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */

	@RequestMapping(value = "/retsummary.htm", method = RequestMethod.POST)
	public ModelAndView getRetailerSummay(@ModelAttribute("thislocation") ThisLocationRetailerInfo thisLocationRetailerInfo,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getRetailerSummay";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
		List<RetailerDetail> retailerPageInfoList = null;
		List<RetailerCreatedPages> retailCreatedPageslst = null;
		RetailersDetails retailersDetailsObj = null;
		String viewName = null;
		String retZipcode = null;
		Double retailerLat = null;
		Double retailerLng = null;
		
		
		retZipcode = (String) session.getAttribute("zipcode");
		retailerLat = (Double) session.getAttribute("retlatitude");
		retailerLng = (Double) session.getAttribute("retlongitude");


		try
		{

			if (thisLocationRetailerInfo != null)
			{				

				if (retailerLat != null && retailerLng != null)
				{
					thisLocationRetailerInfo.setRetLat(String.valueOf(retailerLat));
					thisLocationRetailerInfo.setRetLng(String.valueOf(retailerLng));
				}
				else
				{
					thisLocationRetailerInfo.setPostalCode(retZipcode);
					if(retZipcode!=null)
					{
						retailerLat=	(Double) session.getAttribute("zipcodeLat");
						retailerLng=	(Double) session.getAttribute("zipcodeLong");
					//	thisLocationService.getLatLong(retZipcode);
						//retailerLat
					}
								
				}
				retailerPageInfoList = thisLocationService.getRetailerSummary(thisLocationRetailerInfo);
				if (retailerPageInfoList != null && !retailerPageInfoList.isEmpty())
				{

					request.setAttribute("retailerPageInfoList", retailerPageInfoList.get(0));

					session.setAttribute("retRibbenAd", retailerPageInfoList.get(0).getRibbonAdImagePath());
					session.setAttribute("retName", retailerPageInfoList.get(0).getRetailerName());
					//session.setAttribute("saleFlag", retailerPageInfoList.get(0).getSaleFlag());

					viewName = "thislocation";
				}
				else
				{
				}

				retailCreatedPageslst = thisLocationService.getRetailerCreatedPages(thisLocationRetailerInfo);
				if (retailCreatedPageslst != null && !retailCreatedPageslst.isEmpty())
				{

					request.setAttribute("retailCreatedPageslst", retailCreatedPageslst);
					viewName = "thislocation";
				}
				else
				{

				}

			}
			session.setAttribute("getdirectionlat", retailerLat);
			session.setAttribute("getdirectionlng", retailerLng);
			
		}
		catch (ScanSeeServiceException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		return new ModelAndView("thislocation");

	}

	/**
	 * This method for fetching retailer current specials ,product with sale
	 * price,hotdeals and coupon flags Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */
	@RequestMapping(value = "/getcurrentspecial.htm", method = RequestMethod.POST)
	public ModelAndView fetchCLRSpecialHotdeals(@ModelAttribute("thislocation") ThisLocationRetailerInfo thisLocationRetailerInfo,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "fetchCLRSpecialHotdeal";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> specialOfferlst = null;

		int currentPage = 1;

		Pagination objPage = null;

		List<HotDealsDetails> retHotdealslst = null;

		List<RetailerDetail> retailerSpecialsList = null;
		ProductDetails prodcutDetails = new ProductDetails();

		session.removeAttribute("currentspecial");
		session.setAttribute("retailerId", thisLocationRetailerInfo.getRetailerId());
		session.setAttribute("retailerName", thisLocationRetailerInfo.getRetailerName());
		session.setAttribute("retLocationId", thisLocationRetailerInfo.getRetailLocationID());
		try
		{
			ServletContext servletContext = request.getSession().getServletContext();
			WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);

			ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
			if (thisLocationRetailerInfo != null)
			{

				// check retailer products are there or not.
				prodcutDetails = getRetailerSaleProducts(thisLocationRetailerInfo, request, session, model);

				if (prodcutDetails != null)
				{

					List<ProductDetail> productDetail = prodcutDetails.getProductDetail();
					for (int i = 0; i < productDetail.size(); i++)
					{
						ProductDetail objProductDetail = productDetail.get(i);
						if (objProductDetail.getSalePrice().contains("."))
						{

						}
						else
						{
							String salePrice = objProductDetail.getSalePrice();
							salePrice = salePrice + ".00";
							objProductDetail.setSalePrice(salePrice);
						}

					}
					request.setAttribute("prodcutDetails", prodcutDetails);
					objPage = Utility.getPagination(prodcutDetails.getTotalSize(), currentPage, "productinfo.htm", 30);
					session.setAttribute("pagination", objPage);

				}
				else
				{
					request.setAttribute("message", "Product list is not available for the partuicular retailer.");
				}

				// to check the retailer specials, hot deals and clr are there
				// or not .
				specialOfferlst = thisLocationService.fetchSpecialOffersHotDealDiscounts(thisLocationRetailerInfo);

				if (specialOfferlst != null && !specialOfferlst.isEmpty())
				{

					// fetching retailer special offer list based on retailer
					// location id.
					if (specialOfferlst.get(0).getSpecialOffFlag() == true)
					{

						retailerSpecialsList = thisLocationService.fetchRetSpecialOfferlst(thisLocationRetailerInfo);

						if (retailerSpecialsList != null && !retailerSpecialsList.isEmpty())
						{

							request.setAttribute("retspecials", retailerSpecialsList);

						}
					}
					// fetching retailer hot deals list based on retailer
					// location id.
					else if (specialOfferlst.get(0).getHotDealFlag() == true)
					{

						retHotdealslst = thisLocationService.getRetailerHotDeals(thisLocationRetailerInfo);

						if (retHotdealslst != null && !retHotdealslst.isEmpty())
						{

							request.setAttribute("rethotdeals", retHotdealslst);

						}
					}

					// for enabling the bottom bar button in see current
					// specials screen.

					if (prodcutDetails != null)
					{
						request.setAttribute("selbutn", "sales");
					}
					else if (!retailerSpecialsList.isEmpty() && retailerSpecialsList != null)
					{

						request.setAttribute("selbutn", "specials");
					}
					else if (!retHotdealslst.isEmpty() && retHotdealslst != null)
					{
						request.setAttribute("selbutn", "hotdeals");
					}
					else
					{
						request.setAttribute("selbutn", "coupons");
					}
					// fetching retailer coupons list based on retailer location
					// id.
					/*
					 * else if (specialOfferlst.get(0).getClrFlag() == true) {
					 * cLRDetailsObj = thisLocationService
					 * .getRetailerCLRDetails(thisLocationRetailerInfo); if
					 * (cLRDetailsObj != null) {
					 * request.setAttribute("retcoupons", cLRDetailsObj);
					 * viewName = "thislocation"; } }
					 */
				}

			}
			session.setAttribute("currentspecial", specialOfferlst);

		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}

		return new ModelAndView("thislocation");

	}

	/**
	 * This method for fetching Retailer special offer list information for the
	 * specified retailer id and location ID. Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */

	@RequestMapping(value = "/retspecials.htm", method = RequestMethod.POST)
	public ModelAndView getRetailerSpecials(@ModelAttribute("thislocation") ThisLocationRetailerInfo thisLocationRetailerInfo,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getRetailerSpecial";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
		List<RetailerDetail> retailerSpecialsList = null;
		request.setAttribute("selbutn", "specials");

		String viewName = null;

		try
		{

			if (thisLocationRetailerInfo != null)
			{

				retailerSpecialsList = thisLocationService.fetchRetSpecialOfferlst(thisLocationRetailerInfo);

				if (retailerSpecialsList != null && !retailerSpecialsList.isEmpty())
				{

					request.setAttribute("retspecials", retailerSpecialsList);
					viewName = "thislocation";
				}
				else
				{
					request.setAttribute("retspecials", retailerSpecialsList);
					request.setAttribute("message", "None Available ,Select another category below.");
				}

			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		return new ModelAndView("thislocation");

	}

	/**
	 * This method for fetching Retailer special offer list information for the
	 * specified retailer id and location ID. Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */

	@RequestMapping(value = "/rethotdeals.htm", method = RequestMethod.POST)
	public ModelAndView getRetailerHotdeals(@ModelAttribute("thislocation") ThisLocationRetailerInfo thisLocationRetailerInfo,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getRetailerSpecial ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
		List<HotDealsDetails> retHotdealslst = null;
		request.setAttribute("selbutn", "hotdeals");
		String viewName = null;

		try
		{

			if (thisLocationRetailerInfo != null)
			{

				retHotdealslst = thisLocationService.getRetailerHotDeals(thisLocationRetailerInfo);

				if (retHotdealslst != null && !retHotdealslst.isEmpty())
				{

					request.setAttribute("rethotdeals", retHotdealslst);
					viewName = "thislocation";
				}
				else
				{
					request.setAttribute("rethotdeals", retHotdealslst);

					request.setAttribute("message", "None Available ,Select another category below.");
				}

			}
		}
		catch (ScanSeeWebSqlException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}

		return new ModelAndView("thislocation");

	}

	/**
	 * This method for fetching Retailer coupons list information for the
	 * specified retailer id and location ID. Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */
	@RequestMapping(value = "/retcoupons.htm", method = RequestMethod.POST)
	public ModelAndView getRetailerCoupons(@ModelAttribute("thislocation") ThisLocationRetailerInfo thisLocationRetailerInfo,
			HttpServletRequest request, HttpSession session, ModelMap model)
	{
		final String methodName = "getRetailerCoupons";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
	//	ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
	
		try
		{

			// The below code is commented because coupon implementation is on
			// hold.
			/*
			 * if (thisLocationRetailerInfo != null) { cLRDetailsObj =
			 * thisLocationService
			 * .getRetailerCLRDetails(thisLocationRetailerInfo); if
			 * (cLRDetailsObj != null) { request.setAttribute("retcoupons",
			 * cLRDetailsObj); viewName = "thislocation"; } else { } }
			 * 
			 */
			
			request.setAttribute("retcoupons", "retcoupons");
			request.setAttribute("selbutn", "coupons");
			request.setAttribute("message", "None Available ,Select another category below.");
		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
		}
		return new ModelAndView("thislocation");

	}

	/**
	 * This method for fetching Retailer product with sale information for the
	 * specified retailer id and location ID. Method Type: POST
	 * 
	 * @param retailer
	 *            id and retailer location id are the input parameter.
	 * @throws ScanSeeWebSqlException
	 *             for any kind of exception.
	 */
	public static ProductDetails getRetailerSaleProducts(ThisLocationRetailerInfo thisLocationRetailerInfo, HttpServletRequest request,
			HttpSession session, ModelMap model)
	{
		Users loginUser = (Users) session.getAttribute("loginuser");
		Long userId = loginUser.getUserID();
		int lowerLimit = 0;

		ProductDetailsRequest productDetailsRequest = new ProductDetailsRequest();
		productDetailsRequest.setLowerLimit(lowerLimit);
		productDetailsRequest.setUserId(userId);
		Integer retailLocationID = thisLocationRetailerInfo.getRetailLocationID();
		Integer retailerId = thisLocationRetailerInfo.getRetailerId();
		productDetailsRequest.setRetailLocationID(retailLocationID);
		productDetailsRequest.setRetailID(retailerId);
		LOG.info("Entered in getRetailerSaleProducts Get Method....");
		ProductDetails prodcutDetails = new ProductDetails();
		ServletContext servletContext = request.getSession().getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		ThisLocationService thisLocationService = (ThisLocationService) appContext.getBean("thisLocationService");
		prodcutDetails = thisLocationService.getProductsInfo(productDetailsRequest);

		LOG.info("Exited in getProductsInfo Get Method....");

		return prodcutDetails;

	}

}

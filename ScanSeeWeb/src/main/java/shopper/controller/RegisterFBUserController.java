package shopper.controller;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import shopper.service.ShopperService;

import common.constatns.ApplicationConstants;
import common.exception.ScanSeeServiceException;
import common.pojo.LoginVO;

@Controller
@RequestMapping("/registerfbuser.htm")
public class RegisterFBUserController {

	@RequestMapping(value="/registerfbuser.htm", method = RequestMethod.POST)
	public ModelAndView searchProduct(
			@ModelAttribute("loginform") LoginVO loginVO,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) throws IOException {
		String serResponse = null;
		String view = "login";
		ServletContext servletContext = request.getSession()
				.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		ShopperService shopperService = (ShopperService) appContext
				.getBean("shopperService");

		/*try {

			//serResponse = shopperService.processFaceBookUser(loginVO.getFbUserId());
			if (null != serResponse
					&& !serResponse.equals(ApplicationConstants.FAILURE)) {
				view = "shopperPreferences";
			}

		} catch (ScanSeeServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		return new ModelAndView(view);
	}
}

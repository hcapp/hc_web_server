package shopper.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import common.constatns.ApplicationConstants;
import common.pojo.ShopperRegistrationInfo;
import common.pojo.UserPreference;
import common.pojo.Users;
import common.pojo.shopper.UserRegistrationInfo;

public class ShopperProfileValidator implements Validator
{
	
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return ShopperRegistrationInfo.class.isAssignableFrom(arg0);
	}

	public void validate(Object arg0, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "firstName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "lastName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address1", "address1.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "postalCode.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "email.required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mobilePhone", "mobilePhone.required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "userName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rePassword", "rePassword.required");
		
		
		Users shopperInfo=(Users)arg0;
		
		if(null == shopperInfo.getState() || shopperInfo.getState().isEmpty() || shopperInfo.getState() == "0"){
		  	errors.rejectValue("state", "state.required");
		}
     	if(null == shopperInfo.getCity() || shopperInfo.getCity().isEmpty()){
		  	errors.rejectValue("city", "city.enter");
		}
       		
		if((shopperInfo.isTerms() == false)){
			errors.rejectValue("terms", "terms.required");
		}
		/*if (null == shopperInfo.getCaptch() || shopperInfo.getCaptch().equals(""))
		{

			ValidationUtils.rejectIfEmpty(errors, "captch", "captcha.invalid");
		}*/
		
	}
	
	public void validate(Object arg0, Errors errors, String status) {
		
		if (status.equals(ApplicationConstants.INVALIDEMAIL)) {
			
		    errors.rejectValue("email", "invalid.email");
		} 
		if (status.equals(ApplicationConstants.PASSWORD)) {
			errors.reject("password.match");
		}
		if (status.equals(ApplicationConstants.PASSWORDLength)) {
			errors.rejectValue("password","password.length");
		}
		else if(status.equals(ApplicationConstants.PASSWORDMATCH))
		{
			errors.rejectValue("password","password.digit");
		}
		if (status.equals(ApplicationConstants.DUPLICATE_USER)) {
			
			errors.reject("user.exist");
		}
		if (status.equals(ApplicationConstants.INVALIDCONTPHONE)) {
			errors.rejectValue("mobilePhone", "invalid.Phonenumber");
		}
	/*	if (status.equals(ApplicationConstants.INVALID_CAPTCHA))
		{

			errors.rejectValue("captch", "captcha.invalid");
		}*/
	}
	public final void validateEditProfile(Object arg0, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPreferenceInfoList.firstName", "firstName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPreferenceInfoList.lastName", "lastName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPreferenceInfoList.address1", "address1.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userPreferenceInfoList.postalCode", "postalCode.required");		
		
		UserPreference shopperInfo=(UserPreference)arg0;
		
     	if(null == shopperInfo.getCity() || shopperInfo.getCity().isEmpty()){
		  	errors.rejectValue("userPreferenceInfoList.city", "city.enter");
		}
	}
	public void validateMobileNumber(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.INVALIDCONTPHONE)) {
			errors.rejectValue("userPreferenceInfoList.mobilePhone", "invalid.Phonenumber");
		}
	}
	public void validateUserPreferenceInfo(Object arg0, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "firstName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "lastName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address1", "address1.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "postalCode.required");
		
		UserRegistrationInfo shopperInfo=(UserRegistrationInfo)arg0;
		
     	if(null == shopperInfo.getCity() || shopperInfo.getCity().isEmpty()){
		  	errors.rejectValue("city", "city.enter");
		}
	}
	public void validatePreferences(Object arg0, Errors errors, String status) {
		if (status.equals(ApplicationConstants.INVALIDCONTPHONE)) {
			errors.rejectValue("mobileNumber", "invalid.Phonenumber");
		}
		if (status.equals(ApplicationConstants.INVALIDDOB)) {
			errors.rejectValue("dob", "invalid.dob");
		}
		
	}
}
	

package common.pojo.shopper;

import common.constatns.ApplicationConstants;

/**
 * This pojo contains MasterShoppinglist Retailer and Category details.
 * @author malathi_lr
 *
 */
public class MainSLRetailerCategory {
	
	/** nextPage declared as Integer.
	 * for next page
	 */
	private Integer nextPage;
	/**
	 * for pagination row num
	 */
	private Integer rowNum;
	/**
	 * The starFlag declared as integer.
	 */
	
	private Integer starFlag;

	/**
	 * For product price.
	 */
	private String price;

	/**
	 * for product productImagePath.
	 */
	private String productImagePath;

	/**
	 * for product CLRFlag.
	 */
	private Integer CLRFlag;
	/**
	 * for product ButtonFlag.
	 */
	private Integer buttonFlag;
	/**
	 * for rebate_Status.
	 */
	private String rebate_Status;

	/**
	 * for loyalty_Status.
	 */
	private String loyalty_Status;

	/**
	 * for coupon_Status.
	 */
	private String coupon_Status;

	/**
	 * for SubCategoryName.
	 */
	private String SubCategoryName;

	/**
	 * for product subCategoryID.
	 */
	private Integer subCategoryID;

	/**
	 * for product name.
	 */
	private String productName;

	/**
	 * for productId.
	 */
	private Integer productId;
	/**
	 * for userProductID.
	 */
	private Integer userProductID;

	/**
	 * for userId.
	 */
	private Integer userId;
	/**
	 * for userRetailPreferenceID.
	 */
	private Integer userRetailPreferenceID;
	/**
	 * for retailID.
	 */
	private Integer retailID;
	/**
	 * for retailName.
	 */
	private String retailName;
	/**
	 * for categoryID.
	 */
	private Integer categoryID;
	/**
	 * for parentCategoryName.
	 */
	private String parentCategoryName;
	/**
	 * for parentCategoryID.
	 */
	private Integer parentCategoryID;

	/**
	 * for gettingretailID.
	 * 
	 * @return retailID
	 */
	public Integer getRetailID()
	{
		return retailID;
	}

	/**
	 * for setting retailID.
	 * 
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Integer retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * for getting retailName.
	 * 
	 * @return the retailName
	 */
	public String getRetailName()
	{
		return retailName;
	}

	/**
	 * for setting retailerName.
	 * 
	 * @param retailName
	 *            the retailName to set
	 */
	public void setRetailName(String retailName)
	{
		if (retailName == null)
		{
			this.retailName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.retailName = retailName;
		}
	}

	/**
	 * for getting userID.
	 * 
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * for setting userId.
	 * 
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * for getting userRetailPreferenceID.
	 * 
	 * @return the userRetailPreferenceID
	 */
	public Integer getUserRetailPreferenceID()
	{
		return userRetailPreferenceID;
	}

	/**
	 * for setting userRetailPreferenceID.
	 * 
	 * @param userRetailPreferenceID
	 *            the userRetailPreferenceID to set
	 */
	public void setUserRetailPreferenceID(Integer userRetailPreferenceID)
	{
		this.userRetailPreferenceID = userRetailPreferenceID;
	}

	/**
	 * for getting categoryID.
	 * 
	 * @return the categoryID
	 */
	public Integer getCategoryID()
	{
		return categoryID;
	}

	/**
	 * for setting categoryID.
	 * 
	 * @param categoryID
	 *            the categoryID to set
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * for getting parentCategoryName.
	 * 
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}

	/**
	 * for setting parentCategoryName.
	 * 
	 * @param parentCategoryName
	 *            the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		if (parentCategoryName == null)
		{
			this.parentCategoryName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.parentCategoryName = parentCategoryName;
		}

	}

	/**
	 * for getting parentCategoryID.
	 * 
	 * @return the parentCategoryID
	 */
	public Integer getParentCategoryID()
	{
		return parentCategoryID;
	}

	/**
	 * for setting parentCategoryID.
	 * 
	 * @param parentCategoryID
	 *            the parentCategoryID to set
	 */
	public void setParentCategoryID(Integer parentCategoryID)
	{
		if(parentCategoryID==null)
		{
			this.parentCategoryID=0;
		}else
		{
		this.parentCategoryID = parentCategoryID;
		}
	}

	/**
	 * for getting cLRFlag.
	 * 
	 * @return the cLRFlag
	 */
	public Integer getCLRFlag()
	{
		return CLRFlag;
	}

	/**
	 * for setting cLRFlag.
	 * 
	 * @param cLRFlag
	 *            the cLRFlag to set
	 */
	public void setCLRFlag(Integer cLRFlag)
	{
		CLRFlag = cLRFlag;
	}

	/**
	 * for getting buttonFlag.
	 * 
	 * @return the buttonFlag
	 */
	public Integer getButtonFlag()
	{
		return buttonFlag;
	}

	/**
	 * for setting buttonFlag.
	 * 
	 * @param buttonFlag
	 *            the buttonFlag to set
	 */
	public void setButtonFlag(Integer buttonFlag)
	{
		this.buttonFlag = buttonFlag;
	}

	/**
	 * for getting rebate_Status.
	 * 
	 * @return the rebate_Status
	 */
	public String getRebate_Status()
	{
		return rebate_Status;
	}

	/**
	 * for setting rebate_Status.
	 * 
	 * @param rebateStatus
	 *            the rebate_Status to set
	 */
	public void setRebate_Status(String rebateStatus)
	{
		if (null == rebateStatus)
		{
			this.rebate_Status = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.rebate_Status = rebateStatus;
		}
	}

	/**
	 * for getting loyalty_Status.
	 * 
	 * @return the loyalty_Status
	 */
	public String getLoyalty_Status()
	{
		return loyalty_Status;
	}

	/**
	 * for setting loyalty_Status.
	 * 
	 * @param loyaltyStatus
	 *            the loyalty_Status to set
	 */
	public void setLoyalty_Status(String loyaltyStatus)
	{
		loyalty_Status = loyaltyStatus;
	}

	/**
	 * for getting coupon_Status.
	 * 
	 * @return the coupon_Status
	 */
	public String getCoupon_Status()
	{
		return coupon_Status;
	}

	/**
	 * for setting coupon_Status.
	 * 
	 * @param couponStatus
	 *            the coupon_Status to set
	 */
	public void setCoupon_Status(String couponStatus)
	{
		if (null == couponStatus)
		{
			this.coupon_Status = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.coupon_Status = couponStatus;
		}
	}

	/**
	 * for getting subCategoryName.
	 * 
	 * @return the subCategoryName
	 */
	public String getSubCategoryName()
	{
		return SubCategoryName;
	}

	/**
	 * for setting subCategoryName.
	 * 
	 * @param subCategoryName
	 *            the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName)
	{
		if (null == subCategoryName)
		{
			this.SubCategoryName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.SubCategoryName = subCategoryName;
		}
	}

	/**
	 * for getting subCategoryID.
	 * 
	 * @return the subCategoryID
	 */
	public Integer getSubCategoryID()
	{
		return subCategoryID;
	}

	/**
	 * for setting subCategoryID.
	 * 
	 * @param subCategoryID
	 *            the subCategoryID to set
	 */
	public void setSubCategoryID(Integer subCategoryID)
	{
		this.subCategoryID = subCategoryID;
	}

	/**
	 * for getting productName.
	 * 
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * for setting productName.
	 * 
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * for getting productId.
	 * 
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * for setting productId.
	 * 
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * for getting userProductID.
	 * 
	 * @return the userProductID
	 */
	public Integer getUserProductID()
	{
		return userProductID;
	}

	/**
	 * for setting userProductID.
	 * 
	 * @param userProductID
	 *            the userProductID to set
	 */
	public void setUserProductID(Integer userProductID)
	{
		this.userProductID = userProductID;
	}

	/**
	 * for getting productImagePath.
	 * 
	 * @return the productImagePath
	 */
	public String getProductImagePath()
	{
		return productImagePath;
	}

	/**
	 * for setting productImagePath.
	 * 
	 * @param productImagePath
	 *            the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath)
	{
		if (null == productImagePath || productImagePath.equals(""))
		{
			//this.productImagePath = ApplicationConstants.NOTAPPLICABLE;
			  //Changed below code as IE browser showing broken image if it did not find image
            this.productImagePath = ApplicationConstants.BLANKIMAGE;
		}
		else
		{
			this.productImagePath = productImagePath;
		}
	}

	/**
	 * for getting price.
	 * 
	 * @return the price
	 */
	public String getPrice()
	{
		return price;
	}

	/**
	 * for setting price.
	 * 
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price)
	{
		if (null == price)
		{
			this.price = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.price = price;
		}
	}

	/**
	 * To get starFlag value.
	 * @return the starFlag
	 */
	public Integer getStarFlag()
	{
		return starFlag;
	}

	/**
	 * To set starFlag value.
	 * @param starFlag the starFlag to set
	 */
	public void setStarFlag(Integer starFlag)
	{
		this.starFlag = starFlag;
	}

	public Integer getRowNum()
	{
		return rowNum;
	}

	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}

	public Integer getNextPage()
	{
		return nextPage;
	}

	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}

}

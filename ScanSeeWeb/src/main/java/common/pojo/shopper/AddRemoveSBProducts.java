package common.pojo.shopper;


/**
 * This pojo for add ,remove shopping basket products.
 * 
 * @author shyamsundara_hm
 */
public class AddRemoveSBProducts 
{
	/**
	 * for userId.
	 */
	protected String userId;
	/**
	 * for ShoppingCartProducts reference cartProducts.
	 */

	protected ShoppingCartProducts cartProducts;
	/**
	 * for ShoppingBasketProducts reference basketProducts.
	 */

	protected ShoppingBasketProducts basketProducts;
	
	private Integer maxCount;
	
	private Integer maxShopCartCnt;

	/**
	 * to get userId.
	 * 
	 * @return userId
	 */

	public String getUserId()
	{
		return userId;
	}

	/**
	 * to set userId.
	 * 
	 * @param userId
	 *            to be set.
	 */
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	/**
	 * to get shopping cart products.
	 * 
	 * @return cartProducts
	 */
	public ShoppingCartProducts getCartProducts()
	{
		return cartProducts;
	}

	/**
	 * for setting cartProducts.
	 * 
	 * @param cartProducts
	 *            to be set.
	 */
	public void setCartProducts(ShoppingCartProducts cartProducts)
	{
		this.cartProducts = cartProducts;
	}

	/**
	 * for getting ShoppingBasketProducts.
	 * 
	 * @return basketProducts
	 */
	public ShoppingBasketProducts getBasketProducts()
	{
		return basketProducts;
	}

	/**
	 * for setting basket products.
	 * 
	 * @param basketProducts
	 *            to be set.
	 */

	public void setBasketProducts(ShoppingBasketProducts basketProducts)
	{
		this.basketProducts = basketProducts;
	}

	public Integer getMaxCount()
	{
		return maxCount;
	}

	public void setMaxCount(Integer maxCount)
	{
		this.maxCount = maxCount;
	}

	public Integer getMaxShopCartCnt()
	{
		return maxShopCartCnt;
	}

	public void setMaxShopCartCnt(Integer maxShopCartCnt)
	{
		this.maxShopCartCnt = maxShopCartCnt;
	}
}

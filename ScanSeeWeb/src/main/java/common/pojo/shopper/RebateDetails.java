package common.pojo.shopper;

import java.util.ArrayList;
import java.util.List;

/**
 * The POJO class for RebateDetails.
 * @author shyamsundara_hm.
 *
 */
public class RebateDetails extends BaseObject
{

	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected ArrayList<ProductDetail> productLst;
	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected ArrayList<String> productNamesLst;
	
	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected RebateDetail rebateInfo;
	
	/**
	 * The rebateDetail property.
	 */

	protected List<RebateDetail> rebateDetail;

	/**
	 * The getter for rebateDetail property.
	 * @return rebateDetail
	 */

	public List<RebateDetail> getRebateDetail()
	{
		return rebateDetail;
	}

	/**
	 * The setter for rebateDetail property.
	 * @param rebateDetail
	 *          the rebateDetail to be set.  
	 *            
	 */

	public void setRebateDetail(List<RebateDetail> rebateDetail)
	{
		this.rebateDetail = rebateDetail;
	}

	/**
	 * @return the rebateInfo
	 */
	public RebateDetail getRebateInfo()
	{
		return rebateInfo;
	}

	/**
	 * @param rebateInfo the rebateInfo to set
	 */
	public void setRebateInfo(RebateDetail rebateInfo)
	{
		this.rebateInfo = rebateInfo;
	}

	/**
	 * @return the productNamesLst
	 */
	public ArrayList<String> getProductNamesLst()
	{
		return productNamesLst;
	}

	/**
	 * @param productNamesLst the productNamesLst to set
	 */
	public void setProductNamesLst(ArrayList<String> productNamesLst)
	{
		this.productNamesLst = productNamesLst;
	}

	public ArrayList<ProductDetail> getProductLst()
	{
		return productLst;
	}

	public void setProductLst(ArrayList<ProductDetail> productLst)
	{
		this.productLst = productLst;
	}

	

}

package common.pojo.shopper;

import java.util.ArrayList;
import java.util.HashMap;

import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import common.pojo.AppConfiguration;

public class ProductSummaryVO {

	/**
	 * For commission junction stores integration
	 * 
	 */
	public String totalStores;
	/**
	 * For commission junction stores integration
	 * 
	 */
	public String minimumPrice;
	
	
	/**
	 * 
	 */
	public HashMap appConfigData = new HashMap();
	
	/**
	 * ProductDetail instance
	 */
	public ProductDetail productDetail = new ProductDetail();
	
	/**
	 * FindNearByIntactResponse instance
	 */
	public FindNearByIntactResponse findNearByDetailsResultSet = new FindNearByIntactResponse();
	
	/**
	 * OnlineStores instance
	 */
	public OnlineStores objOnlineStores = new OnlineStores();
	
	/**
	 * ProductReview list instance
	 */
	public ArrayList<ProductReview> productReviewslist =  new ArrayList<ProductReview>();
	
	/**
	 * UserRatingInfo instance
	 */
	public UserRatingInfo userRatingInfo = new UserRatingInfo();

	/**
	 * @return the productDetail
	 */
	public ProductDetail getProductDetail() {
		return productDetail;
	}

	/**
	 * @param productDetail the productDetail to set
	 */
	public void setProductDetail(ProductDetail productDetail) {
		this.productDetail = productDetail;
	}

	/**
	 * @return the findNearByDetailsResultSet
	 */
	public FindNearByIntactResponse getFindNearByDetailsResultSet() {
		return findNearByDetailsResultSet;
	}

	/**
	 * @param findNearByDetailsResultSet the findNearByDetailsResultSet to set
	 */
	public void setFindNearByDetailsResultSet(
			FindNearByIntactResponse findNearByDetailsResultSet) {
		this.findNearByDetailsResultSet = findNearByDetailsResultSet;
	}

	/**
	 * @return the objOnlineStores
	 */
	public OnlineStores getObjOnlineStores() {
		return objOnlineStores;
	}

	/**
	 * @param objOnlineStores the objOnlineStores to set
	 */
	public void setObjOnlineStores(OnlineStores objOnlineStores) {
		this.objOnlineStores = objOnlineStores;
	}

	/**
	 * @return the productReviewslist
	 */
	public ArrayList<ProductReview> getProductReviewslist() {
		return productReviewslist;
	}

	/**
	 * @param productReviewslist the productReviewslist to set
	 */
	public void setProductReviewslist(ArrayList<ProductReview> productReviewslist) {
		this.productReviewslist = productReviewslist;
	}

	/**
	 * @return the userRatingInfo
	 */
	public UserRatingInfo getUserRatingInfo() {
		return userRatingInfo;
	}

	/**
	 * @param userRatingInfo the userRatingInfo to set
	 */
	public void setUserRatingInfo(UserRatingInfo userRatingInfo) {
		this.userRatingInfo = userRatingInfo;
	}

	/**
	 * @return the appConfigData
	 */
	public HashMap getAppConfigData() {
		return appConfigData;
	}

	/**
	 * @param appConfigData the appConfigData to set
	 */
	public void setAppConfigData(HashMap appConfigData) {
		this.appConfigData = appConfigData;
	}

	public String getTotalStores()
	{
		return totalStores;
	}

	public void setTotalStores(String totalStores)
	{
		if(totalStores !=null)
		{
			this.totalStores = totalStores  +" Stores";
		}
		
	}

	public String getMinimumPrice()
	{
		return minimumPrice;
	}

	public void setMinimumPrice(String minimumPrice)
	{
		if(minimumPrice !=null)
		{
		this.minimumPrice = "$" +minimumPrice;
		}
	}

	
}

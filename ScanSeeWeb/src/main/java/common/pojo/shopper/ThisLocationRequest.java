package common.pojo.shopper;

/**
 * The POJO class for ThisLocationRequest.
 * @author malathi_lr
 *
 */
public class ThisLocationRequest {

	/**
	 * For preferred Radius.
	 */
	private Double preferredRadius;
	/**
	 * for zipcode.
	 */
	private String zipcode;
	/**
	 * for userid.
	 */
	private Integer userId;
	/**
	 * for gpsEnabled.
	 */
	private boolean gpsEnabled;
	/**
	 * for longitude.
	 */
	private Double longitude;
	/**
	 * for latitude.
	 */
	private Double latitude;
	/**
	 * for lastVisitedRecord.
	 */
	private Integer lastVisitedRecord;
	
	/**
	 * productid for Shoppinglist findNearBy method,in othercases it should be
	 * null.
	 */
	
	private Integer productId;
	
	/**
	 * Variable for Screen name For pagination.
	 */
	private String screenName;
	/**
	 *  variable for Postal code.
	 */
	private Integer postalCode;
	
	/**
	 * for redius
	 */
	private String radius;
	/**
	 * for distance in mails
	 */
	private String distInMiles;

	/**
	 * This method for getting productid.
	 * 
	 * @return the productId.
	 */
	public Integer getProductId()
	{
		return productId;
	}

	
	
	/** this method return Screen Name.
	 * @return the screenName.
	 */
	public String getScreenName()
	{
		return screenName;
	}

	/** this method set the Screen name.
	 * @param screenName the screenName to set.
	 */
	public void setScreenName(String screenName)
	{
		this.screenName = screenName;
	}

	/**
	 * This method for setting product id.
	 * 
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * This method for checking gpsenabled or not.
	 * 
	 * @return the gpsEnabled
	 */
	public boolean isGpsEnabled()
	{
		return gpsEnabled;
	}

	/**
	 * This method for setting gpsenabled value.
	 * 
	 * @param gpsEnabled
	 *            the gpsEnabled to set
	 */
	public void setGpsEnabled(boolean gpsEnabled)
	{
		this.gpsEnabled = gpsEnabled;
	}

	/**
	 * This method for getting userId.
	 * 
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * this method for setting userid.
	 * 
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	

	/**
	 * this method for getting longitude value.
	 * 
	 * @return longitude
	 */
	public Double getLongitude()
	{
		return longitude;
	}

	/**
	 * This method for setting logitude value.
	 * 
	 * @param longitude
	 *            as request parameter.
	 */
	public void setLongitude(Double longitude)
	{
		this.longitude = longitude;
	}

	/**
	 * This method for getting latitude.
	 * 
	 * @return latitude
	 */
	public Double getLatitude()
	{
		return latitude;
	}

	/**
	 * This method for setting latitude.
	 * 
	 * @param latitude
	 *            as request parameter.
	 */
	public void setLatitude(Double latitude)
	{
		this.latitude = latitude;
	}

	
	/**
	 * This method for getting lastvisistedRecord.
	 * 
	 * @return lastVisitedRecord
	 */
	public Integer getLastVisitedRecord()
	{
		return lastVisitedRecord;
	}

	/**
	 * This method for setting lastvisitedRecord.
	 * 
	 * @param lastVisitedCounter
	 *            as request parameter.
	 */
	public void setLastVisitedRecord(Integer lastVisitedCounter)
	{
		this.lastVisitedRecord = lastVisitedCounter;
	}

	/**
	 * Method returns Postal Code.
	 * @return the postalCode.
	 */
	public Integer getPostalCode()
	{
		return postalCode;
	}

	/**
	 * Method to set Postal Code.
	 * @param postalCode the postalCode to set.
	 */
	public void setPostalCode(Integer postalCode)
	{
		this.postalCode = postalCode;
	}



	/** for getting zipcode. 
	 * @return the zipcode
	 */
	public String getZipcode()
	{
		return zipcode;
	}



	/** for setting zipcode.
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}



	public Double getPreferredRadius()
	{
		return preferredRadius;
	}



	public void setPreferredRadius(Double preferredRadius)
	{
		this.preferredRadius = preferredRadius;
	}



	public String getRadius()
	{
		return radius;
	}



	public void setRadius(String radius)
	{
		this.radius = radius;
	}



	public String getDistInMiles()
	{
		return distInMiles;
	}



	public void setDistInMiles(String distInMiles)
	{
		this.distInMiles = distInMiles;
	}
}

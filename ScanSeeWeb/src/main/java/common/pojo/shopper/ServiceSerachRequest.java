package common.pojo.shopper;

import java.util.HashMap;

/**
 * @author dileepa_cc
 */

public class ServiceSerachRequest
{
	private String userId;
	private String location;
	private String radius;
	private String sensor;
	private String key;
	private String type;
	private String lat;
	private String lng;
	private String reference;
	private String language;
	private Integer lastVisitedNo;
	
	private String keyword;
	private String pagetoken;

	/**
	 * @return the userId
	 */
	public String getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the lat
	 */
	public String getLat()
	{
		return lat;
	}

	/**
	 * @param lat
	 *            the lat to set
	 */
	public void setLat(String lat)
	{
		this.lat = lat;
	}

	/**
	 * @return the lng
	 */
	public String getLng()
	{
		return lng;
	}

	/**
	 * @param lng
	 *            the lng to set
	 */
	public void setLng(String lng)
	{
		this.lng = lng;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @param values
	 *            the values to set
	 */
	public void setValues(HashMap<String, String> values)
	{
		this.values = values;
	}

	/**
	 * HashMap of Java Parameter(Key) and Parameter Value(Value).
	 */
	private HashMap<String, String> values = new HashMap<String, String>();

	/**
	 * HashMap of Java Parameter(Key) and Parameter Value(Value).
	 */
	private HashMap<String, String> locationValues = new HashMap<String, String>();

	/**
	 * @return the location
	 */
	public String getLocation()
	{
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location)
	{
		this.location = location;
	}

	/**
	 * @return the radius
	 */
	public String getRadius()
	{
		return radius;
	}

	/**
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(String radius)
	{
		this.radius = radius;
	}

	/**
	 * @return the sensor
	 */
	public String getSensor()
	{
		return sensor;
	}

	/**
	 * @param sensor
	 *            the sensor to set
	 */
	public void setSensor(String sensor)
	{
		this.sensor = sensor;
	}

	/**
	 * @return the key
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key)
	{
		this.key = key;
	}

	/**
	 * @return the values
	 */
	public HashMap<String, String> getValues()
	{
		return values;
	}

	/**
	 * @param values
	 *            the values to set
	 */
	public void setValues()
	{
		values.put("jLocation", this.location);
		values.put("jSensor", this.sensor);
		values.put("jPagetoken", this.pagetoken);
		values.put("jRadius", this.radius);
		values.put("jTypes", this.type);
		values.put("jKeyword", this.keyword);
		
	}

	public String getReference()
	{
		return reference;
	}

	public void setReference(String reference)
	{
		this.reference = reference;
	}

	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}

	public void setLocationValues()
	{
		locationValues.put("jReference", this.reference);
		locationValues.put("jSensor", this.sensor);
		locationValues.put("jLanguage", this.language);
	}

	public HashMap<String, String> getLocationValues()
	{
		return locationValues;
	}

	public void setLocationValues(HashMap<String, String> locationValues)
	{
		this.locationValues = locationValues;
	}

	public Integer getLastVisitedNo()
	{
		return lastVisitedNo;
	}

	public void setLastVisitedNo(Integer lastVisitedNo)
	{
		this.lastVisitedNo = lastVisitedNo;
	}

	public String getKeyword()
	{
		return keyword;
	}

	public void setKeyword(String keyword)
	{
		this.keyword = keyword;
	}

	public String getPagetoken()
	{
		return pagetoken;
	}

	public void setPagetoken(String pagetoken)
	{
		this.pagetoken = pagetoken;
	}

	
}

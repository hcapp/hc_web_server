package common.pojo.shopper;

import java.util.ArrayList;
import java.util.List;

/**
 * Pojo for LoyaltyDetails.
 * @author pradip_k
 *
 */
public class LoyaltyDetails extends BaseObject
{

	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected ArrayList<ProductDetail> productLst;
	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected ArrayList<String> productNamesLst;
;
	
	private LoyaltyDetail loyaltyInfo=null;
	
	/**
	 * 
	 * variable loyaltyDetail declare as List.
	 */
	private List<LoyaltyDetail> loyaltyDetail;


	/**
	 * for getting loyaltyDetail.
	 * @return loyaltyDetail
	 */
	public List<LoyaltyDetail> getLoyaltyDetail()
	{
		return loyaltyDetail;
	}

	/**
	 * for setting the loyaltyDetail. 
	 * @param loyaltyDetail
	 *         loyaltyDetail to be set.    
	 */
	public void setLoyaltyDetail(List<LoyaltyDetail> loyaltyDetail)
	{
		this.loyaltyDetail = loyaltyDetail;
	}

	

	/**
	 * @return the loyaltyInfo
	 */
	public LoyaltyDetail getLoyaltyInfo()
	{
		return loyaltyInfo;
	}

	/**
	 * @param loyaltyInfo the loyaltyInfo to set
	 */
	public void setLoyaltyInfo(LoyaltyDetail loyaltyInfo)
	{
		this.loyaltyInfo = loyaltyInfo;
	}

	/**
	 * @return the productNamesLst
	 */
	public ArrayList<String> getProductNamesLst()
	{
		return productNamesLst;
	}

	/**
	 * @param productNamesLst the productNamesLst to set
	 */
	public void setProductNamesLst(ArrayList<String> productNamesLst)
	{
		this.productNamesLst = productNamesLst;
	}

	public ArrayList<ProductDetail> getProductLst()
	{
		return productLst;
	}

	public void setProductLst(ArrayList<ProductDetail> productLst)
	{
		this.productLst = productLst;
	}



}

package common.pojo.shopper;

import java.util.List;

/**
 * The AlertedProducts pojo class contains setter and getter methods.
 * @author shyamsundhar_hm
 *
 */
public class AlertedProducts
{

	/**
	 * The alertProductDetails variable declared as List.
	 */
	private List<ProductDetail> alertProductDetails;

	/**
	 * To get getAlertProductDetails.
	 * @return the alertProductDetails
	 */
	public List<ProductDetail> getAlertProductDetails()
	{
		return alertProductDetails;
	}

	/**
	 * To set alertProductDetails.
	 * @param alertProductDetails the alertProductDetails to set
	 */
	public void setAlertProductDetails(List<ProductDetail> alertProductDetails)
	{
		this.alertProductDetails = alertProductDetails;
	}
	
	
}

package common.pojo.shopper;

/**
 * pojo for ShoppingListResultSet.
 * @author malathi_lr
 *
 */
public class ShoppingListResultSet {


	/**
	 * for button flag.
	 */
	private Integer buttonFlag;
	/**
	 * for CLRFlag.
	 */
	private Integer CLRFlag;
	/**
	 * for category.
	 */
	private Integer  categoryID;
	/**
	 * for parent category name.
	 */
	private String parentCategoryName;
	/**
	 * The retailerId property.
	 */

	private Integer retailerId;

	/**
	 * The retailerName property.
	 */

	private String retailerName;

	/**
	 * The productId property.
	 */

	private Integer productId;

	/**
	 * The productName property.
	 */

	private String productName;

	/**
	 * The coupon property.
	 */

	private Integer coupon;

	/**
	 * The rebate property.
	 */

	private Integer rebate;

	/**
	 * The loyalty property.
	 */

	private Integer loyalty;

	/**
	 * The coupon_Status property.
	 */

	private String coupon_Status;

	/**
	 * The rebate_Status property.
	 */

	private String rebate_Status;

	/**
	 * The loyalty_Status property.
	 */

	private String loyalty_Status;

	/**
	 * The distance property.
	 */

	private Integer distance;

	/**
	 * The ShopListItem property.
	 */

	private Integer ShopListItem;

	/**
	 * The ShopCartItem property.
	 */

	private Integer ShopCartItem;

	/**
	 * The userId property.
	 */

	private Integer userId;

	/**
	 * The usage property.
	 */

	private String usage;

	/**
	 * The userProductID property.
	 */

	private Integer userProductID;

	/**
	 * product price is used for findnearbyRetailer method.
	 */

	private String productPrice;

	/** for getting userId.
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/** for setting userId.
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	
	/**
	 * for getting UserProductID.
	 * @return the userProductID
	 */
	public Integer getUserProductID()
	{
		return userProductID;
	}

	/**
	 * for setting userProductID.
	 * @param userProductID
	 *            the userProductID to set
	 */
	public void setUserProductID(Integer userProductID)
	{
		this.userProductID = userProductID;
	}

	/** for getting shoppingListItem.
	 * @return the shopListItem
	 */
	public Integer getShopListItem()
	{
		return ShopListItem;
	}

	/**for setting ShopListItem.
	 * @param shopListItem
	 *            the shopListItem to set
	 */
	public void setShopListItem(Integer shopListItem)
	{
		ShopListItem = shopListItem;
	}

	/** for getting ShopCartItem.
	 * @return the shopCartItem
	 */
	public Integer getShopCartItem()
	{
		return ShopCartItem;
	}

	/**
	 * for setting shopCartItem.
	 * @param shopCartItem
	 *            the shopCartItem to set
	 */
	public void setShopCartItem(Integer shopCartItem)
	{
		ShopCartItem = shopCartItem;
	}

	/**
	 * for getting Distance.
	 * @return the distance
	 */
	public Integer getDistance()
	{
		return distance;
	}

	/**
	 * setting distance.
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(Integer distance)
	{
		this.distance = distance;
	}

	/**
	 * for getting ProductPrice.
	 * @return the productPrice
	 */
	public String getProductPrice()
	{
		return productPrice;
	}

	/**
	 * for setting productPrice.
	 * @param productPrice
	 *            the productPrice to set
	 */
	public void setProductPrice(String productPrice)
	{
		this.productPrice = productPrice;
	}

	/**
	 * for getting Coupon_Status.
	 * @return The coupon_Status property
	 */

	public String getCoupon_Status()
	{
		return coupon_Status;
	}

	/**
	 * for setting Coupon_Status.
	 * @param couponStatus
	 *            The coupon_Status property
	 */

	public void setCoupon_Status(String couponStatus)
	{
		coupon_Status = couponStatus;
	}

	/**
	 * for getting Rebate_Status.
	 * @return The rebate_Status property
	 */

	public String getRebate_Status()
	{
		return rebate_Status;
	}

	/**
	 * for setting Rebate_Status.
	 * @param rebateStatus
	 *            The rebate_Status property
	 */

	public void setRebate_Status(String rebateStatus)
	{
		rebate_Status = rebateStatus;
	}

	/**
	 * for getting loyalty_Status.
	 * @return The loyalty_Status property
	 */

	public String getLoyalty_Status()
	{
		return loyalty_Status;
	}

	/**
	 * for setting loyaltyStatus.
	 * @param loyaltyStatus
	 *            The loyalty_Status property
	 */

	public void setLoyalty_Status(String loyaltyStatus)
	{
		loyalty_Status = loyaltyStatus;
	}

	/**
	 * for getting productId.
	 * @return The productId property
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * for setting productId.
	 * @param productId
	 *            The productId property
	 */

	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * for getting productName.
	 * @return The productName property
	 */

	public String getProductName()
	{
		return productName;
	}

	/**
	 * for setting productName.
	 * @param productName
	 *            The productName property
	 */

	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * for getting coupon.
	 * @return The coupon property
	 */

	public Integer getCoupon()
	{
		return coupon;
	}

	/**
	 * for setting coupon.
	 * @param coupon
	 *            The coupon property
	 */

	public void setCoupon(Integer coupon)
	{
		this.coupon = coupon;
	}

	/**
	 * for getting rebate.
	 * @return The rebate property
	 */

	public Integer getRebate()
	{
		return rebate;
	}

	/**
	 * for setting rebate.
	 * @param rebate
	 *            The rebate property
	 */

	public void setRebate(Integer rebate)
	{
		this.rebate = rebate;
	}

	/**
	 * for getting loyalty.
	 * @return rebate The loyalty property
	 */

	public Integer getLoyalty()
	{
		return loyalty;
	}

	/**
	 * for setting loyalty.
	 * @param loyalty
	 *            The loyalty property
	 */

	public void setLoyalty(Integer loyalty)
	{
		this.loyalty = loyalty;
	}

	/**
	 * Gets the value of the retailerId property.
	 * 
	 * @return possible object is {@link String }
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * Sets the value of the retailerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setRetailerId(Integer value)
	{
		this.retailerId = value;
	}

	/**
	 * Gets the value of the retailerName property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * Sets the value of the retailerName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setRetailerName(String value)
	{
		this.retailerName = value;
	}

	/**
	 * for getting usage. 
	 * @return the usage
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * for setting usage.
	 * @param usage
	 *            the usage to set
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}

	/**
	 * for setting categoryID.
	 * @return the categoryID
	 */
	public Integer getCategoryID()
	{
		return categoryID;
	}

	/**
	 * for setting categoryID.
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * for getting parentCategoryName.
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}

	/**
	 * for setting parentCategoryName.
	 * @param parentCategoryName the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}

	/**
	 * for getting CLRFlag.
	 * @return the cLRFlag
	 */
	public Integer getCLRFlag()
	{
		return CLRFlag;
	}

	/**
	 * for setting CLRFlag.
	 * @param cLRFlag the cLRFlag to set
	 */
	public void setCLRFlag(Integer cLRFlag)
	{
		CLRFlag = cLRFlag;
	}

	/**
	 * To get buttonFlag.
	 * @return the buttonFlag
	 */
	public Integer getButtonFlag()
	{
		return buttonFlag;
	}

	/**
	 * To set buttonFlag.
	 * @param buttonFlag the buttonFlag to set
	 */
	public void setButtonFlag(Integer buttonFlag)
	{
		this.buttonFlag = buttonFlag;
	}

}

package common.pojo.shopper;

/**
 * The class has getter and setter methods for UserInfo.
 * 
 * @author sowjanya_d
 */
public class UserInfo extends BaseObject
{
	/**
	 * Variable updateUserInfo declared as of type UpdateUserInfo.
	 */
	private UpdateUserInfo updateUserInfo;
	/**
	 * Variable incomeRanges declared as of type IncomeRanges.
	 */
	private IncomeRanges incomeRanges;
	/**
	 * Variable countryCodes declared as of type CountryCodes.
	 */
	private CountryCodes countryCodes;
	
	/**
	 * Variable educationalLevelDetail declared as of type EducationalLevelDetails.
	 */
	private EducationalLevelDetails educationalLevelDetail;
	
	/**
	 * Variable maritalStatusDetail declared as of type maritalStatusDetail.
	 */
	private MaritalStatusDetails maritalStatusDetail;

	/**
	 * Gets the value of the countryCodes property.
	 * 
	 * @return possible object is {@link CountryCodes }
	 */
	public CountryCodes getCountryCodes()
	{
		return countryCodes;
	}

	/**
	 * Sets the value of the countryCodes property.
	 * 
	 * @param countryCodes
	 *            as of type CountryCodes.
	 */
	public void setCountryCodes(CountryCodes countryCodes)
	{
		this.countryCodes = countryCodes;
	}

	/**
	 * Gets the value of the updateUserInfo property.
	 * 
	 * @return possible object is {@link UpdateUserInfo }
	 */
	public UpdateUserInfo getUpdateUserInfo()
	{
		return updateUserInfo;
	}

	/**
	 * Sets the value of the updateUserInfo property.
	 * 
	 * @param updateUserInfo
	 *            as of type UpdateUserInfo.
	 */
	public void setUpdateUserInfo(UpdateUserInfo updateUserInfo)
	{
		this.updateUserInfo = updateUserInfo;
	}

	/**
	 * Gets the value of the incomeRanges property.
	 * 
	 * @return possible object is {@link IncomeRanges }
	 */
	public IncomeRanges getIncomeRanges()
	{
		return incomeRanges;
	}

	/**
	 * Sets the value of the incomeRanges property.
	 * 
	 * @param incomeRanges
	 *            as of type IncomeRanges.
	 */
	public void setIncomeRanges(IncomeRanges incomeRanges)
	{
		this.incomeRanges = incomeRanges;
	}


    /**
     * gets the value of educationalLevelDetail property.
     * 
     * @return educationalLevelDetail To get
     */
	public EducationalLevelDetails getEducationalLevelDetail()
	{
		return educationalLevelDetail;
	}

	/**
	 * Sets the value of educationalLevelDetail property.
	 * @param educationalLevelDetail
	 *           To set educationalLevelDetail.
	 */
	public void setEducationalLevelDetail(EducationalLevelDetails educationalLevelDetail)
	{
		this.educationalLevelDetail = educationalLevelDetail;
	}

	/**
	 * gets the value of maritalStatusDetail property.
	 * @return maritalStatusDetail To get
	 */
	public MaritalStatusDetails getMaritalStatusDetail()
	{
		return maritalStatusDetail;
	}

	/**
	 * Sets the value of maritalStatusDetail property.
	 * @param maritalStatusDetail
	 *         To set maritalStatusDetail.
	 */
	public void setMaritalStatusDetail(MaritalStatusDetails maritalStatusDetail)
	{
		this.maritalStatusDetail = maritalStatusDetail;
	}

	
}

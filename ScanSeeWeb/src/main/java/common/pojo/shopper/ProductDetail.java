package common.pojo.shopper;

import java.util.HashMap;
import java.util.List;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * The POJO class for ProductDetail.
 * 
 * @author malathi
 */

public class ProductDetail

{
	
	private String responseFlag;
	private String addTo;
	private String response;
	private String productIds;
	private Integer hotDealId;

	private Integer hotDealListID;

	private String reviewURL;

	private String reviewComments;

	/**
	 * for wish list main
	 */
	private boolean wishlistmain;

	private boolean wlratereview;
	/**
	 * The IsWishlst property.
	 */
	private boolean IsWishlst = false;
	/**
	 * nextPage declared as Integer. for next page
	 */
	private Integer nextPage;
	/**
	 * to check product exists or there not
	 */
	private Integer productIsThere;
	/**
	 * for product exits or not
	 */
	private Boolean productExists;
	/**
	 * for Regular Price
	 */
	private String regularPrice;
	/**
	 * for product discount.
	 */
	private String discount;
	/**
	 * for gpsEnabled.
	 */
	private Boolean gpsEnabled;
	/**
	 * for radius.
	 */
	private Integer radius;
	/**
	 * for zipcode.
	 */
	private String zipcode;

	/**
	 * Variable scanCode declared as of type String.
	 */
	private String scanCode;

	/**
	 * For product attributes list.
	 */
	private List<ProductAttributes> productAttributeslst;

	/**
	 * for wish list history date.
	 */
	private String wishListAddDate;

	/**
	 * for wishlist deleted product date.
	 */

	private String productDeletedDate;

	/**
	 * for ProductImagePath.
	 */
	private String productImagePath;

	/**
	 * for setting subCategoryName.
	 */
	private String subCategoryName;
	/**
	 * for setting subcategoryid.
	 */
	private Integer subCategoryID;
	/**
	 * for button flag.
	 */
	private Integer buttonFlag;
	/**
	 * for lastVisitedProductNo.
	 */
	private Integer lastVistedProductNo;

	/**
	 * for product Sale price..
	 */

	private String salePrice;
	/**
	 * for clr flag.
	 */
	private Integer CLRFlag;
	/**
	 * for category id.
	 */
	private Integer categoryID;
	/**
	 * for parent category name.
	 */
	private String parentCategoryName;
	/**
	 * For to identify product clr and preference .
	 */
	private Integer clr;

	/**
	 * For product row number.
	 */
	private Integer row_Num;

	/**
	 * for userProductId.
	 */
	private Integer userProductId;
	/**
	 * for productId.
	 */
	private Integer productId;
	/**
	 * for productName.
	 */
	private String productName;
	/**
	 * for productDescription.
	 */
	private String productDescription;
	/**
	 * for productShortDescription.
	 */
	private String productShortDescription;
	/**
	 * for productLongDescription.
	 */
	private String productLongDescription;

	/**
	 * for product Attribute name.
	 */
	private HashMap<String, String> productAttributesMap;

	/**
	 * for manufacturersName.
	 */
	private String manufacturersName;
	/**
	 * for imagePath.
	 */
	private String imagePath;
	/**
	 * for modelNumber.
	 */
	private String modelNumber;
	/**
	 * for suggestedRetailPrice.
	 */
	private String suggestedRetailPrice;
	/**
	 * for productWeight.
	 */
	private String productWeight;
	/**
	 * for productExpDate.
	 */
	private String productExpDate;
	/**
	 * for weightUnits.
	 */
	private String weightUnits;
	/**
	 * for rowNumber.
	 */
	private Integer rowNumber;
	/**
	 * for rebateId.
	 */
	private Integer rebateId;
	/**
	 * for deviceId.
	 */
	private String deviceId;
	/**
	 * for scanLongitude.
	 */
	private Double scanLongitude;
	/**
	 * for scanLatitude.
	 */
	private Double scanLatitude;
	/**
	 * for fieldAgentRequest.
	 */
	private Integer fieldAgentRequest;
	/**
	 * for scanTypeID.
	 */
	private Integer scanTypeID;
	/**
	 * for coupons.
	 */
	private Integer coupons;
	/**
	 * for rebates.
	 */
	private Integer rebates;
	/**
	 * for loyalties.
	 */
	private Integer loyalties;
	/**
	 * for coupon_Status.
	 */
	private String coupon_Status;
	/**
	 * for rebate_Status.
	 */
	private String rebate_Status;
	/**
	 * for loyalty_Status.
	 */
	private String loyalty_Status;
	/**
	 * for ShopCartItem.
	 */
	private Integer shopCartItem;
	/**
	 * for ShopListItem.
	 */
	private Integer shopListItem;

	/**
	 * for usage.
	 */
	private String usage;
	/**
	 * for productVideoUrl.
	 */
	private String productVideoUrl;
	/**
	 * for userId.
	 */
	private Long userId;
	/**
	 * for barCode.
	 */
	private String barCode;
	/**
	 * for searchkey.
	 */
	private String searchkey;
	/**
	 * for videoFlag.
	 */
	private String videoFlag;
	/**
	 * for audioFlag.
	 */
	private String audioFlag;
	/**
	 * for fileFlag.
	 */
	private String fileFlag;
	//
	/**
	 * for productMediaID.
	 */
	private Integer productMediaID;
	/**
	 * for productMediaPath.
	 */
	private String productMediaPath;

	/**
	 * for scanDate.
	 */
	private String scanDate;
	/**
	 * for scanStatus.
	 */
	private String scanStatus;
	/**
	 * for scanType.
	 */

	private String scanType;
	/**
	 * Distance is used for findnearbyRetailer method.
	 */
	private Integer distance;
	/**
	 * for productPrice.
	 */
	private String productPrice;

	/**
	 * for couponFlag.
	 */

	private String couponFlag;

	/**
	 * for loyaltyFlag.
	 */

	private String loyaltyFlag;

	/**
	 * for rebateFlag.
	 */

	private String rebateFlag;

	/**
	 * for productMediaName.
	 */

	private String productMediaName;

	/**
	 * for retailerName.
	 */

	private String retailerName;

	/**
	 * for retailerId.
	 */

	private Integer retailerId;
	/**
	 * for skuCode.
	 */
	private String skuCode;

	/**
	 * This variable used for Wish List module for indicating cause for Sales
	 * price changed for retailer.
	 */

	private Integer retailFlag;

	/**
	 * This variable used for Wish List module for indicating cause for Sales
	 * price changed for Coupon.
	 */

	private Integer couponSaleFlag;

	/**
	 * This variable used for Wish List module for indicating cause for Sales
	 * price changed for hotdeal.
	 */

	private Integer hotDealFlag;

	/**
	 * for PushNotifyFlag.
	 */
	private Integer pushNotifyFlag;

	/**
	 * the pushNotifyFlag declared as string.
	 */

	private String emailTemplateURL;

	/**
	 * starFlag declared as integer.
	 */
	private Integer starFlag;

	/**
	 * for product attributeName.
	 */

	private String attributeName;

	/**
	 * For product display value.
	 */
	private String displayValue;

	/**
	 * To get retailLocationID value.
	 */
	private String retailLocationID;

	/**
	 * For totalsize
	 */
	private Integer totalSize;

	/**
	 * For totalsize
	 */
	private boolean fromFind;
	/**
	 * For totalsize
	 */
	private boolean fromScannow;

	private Integer productListID;

	private String screenName;
	
	private Integer alertProdID;
	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize()
	{
		return totalSize;
	}

	/**
	 * for userProductID.
	 */
	private Integer userProductID;

	/**
	 * For product price.
	 */
	private String price;

	/**
	 * for search value
	 */
	private String searchValue;

	/**
	 * for online reviews
	 */
	private boolean wlonlineratereview;
	/**
	 * for product search screen
	 */
	private boolean searchProd;

	/**
	 * product warranty service information
	 */
	private String warrantyServiceInfo;

	/**
	 * For product attributes list.
	 */
	private List<ProductDetails> productImageslst;

	/**
	 * for product ratings
	 */
	private Integer prodRatingCount;

	/**
	 * for product reviews
	 */
	private Integer prodReviewCount;

	/**
	 * from url
	 */
	private String redirectUrl;

	/**
	 * for user tracking main menu id.
	 */
	private Integer mainMenuID;

	/**
	 * for product audio name.
	 */
	private String audioFilePath;
	/**
	 * for product video name.
	 */
	private String videoFilePath;

	private String latitude;

	private String longitude;

	private List<String> imagelst;

	private Integer moduleID;

	private String toEmailId;

	private String fromEmailId;

	private Integer saleListID;
	
	private Integer shareTypeID;
	
	private String targetAddress;
	
	private Integer recordCount;
	
	private Integer favProductFlag;
	
	private String usrProdId;

	public Integer getProdReviewCount()
	{
		return prodReviewCount;
	}

	public void setProdReviewCount(Integer prodReviewCount)
	{
		this.prodReviewCount = prodReviewCount;
	}

	/**
	 * @param totalSize
	 *            the totalSize to set
	 */
	public void setTotalSize(Integer totalSize)
	{
		this.totalSize = totalSize;
	}

	/**
	 * To get productAttributeslst values.
	 * 
	 * @return the productAttributeslst
	 */
	public List<ProductAttributes> getProductAttributeslst()
	{
		return productAttributeslst;
	}

	/**
	 * To set productAttributeslst values.
	 * 
	 * @param productAttributeslst
	 *            the productAttributeslst to set
	 */
	public void setProductAttributeslst(List<ProductAttributes> productAttributeslst)
	{
		this.productAttributeslst = productAttributeslst;
	}

	/**
	 * To get productAttributesMap values.
	 * 
	 * @return the productAttributesMap
	 */
	public HashMap<String, String> getProductAttributesMap()
	{
		return productAttributesMap;
	}

	/**
	 * To set productAttributesMap values.
	 * 
	 * @param productAttributesMap
	 *            the productAttributesMap to set
	 */
	public void setProductAttributesMap(HashMap<String, String> productAttributesMap)
	{
		this.productAttributesMap = productAttributesMap;
	}

	/**
	 * To get retailLocationID value.
	 * 
	 * @return the retailLocationID
	 */
	public String getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * To set retailLocationID value.
	 * 
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(String retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	/**
	 * To get emailTemplateURL value.
	 * 
	 * @return emailTemplateURL To get
	 */
	public String getEmailTemplateURL()
	{
		return emailTemplateURL;
	}

	/**
	 * To set emailTemplateURL value.
	 * 
	 * @param productId
	 *            the productId to set
	 */
	public void setEmailTemplateURL(String productId)
	{
		this.emailTemplateURL = "http://122.181.128.148:8080/ScanSee-Email/product/showContent.do?productKey=" + productId;
	}

	/**
	 * To get pushNotifyFlag value.
	 * 
	 * @return pushNotifyFlag To get
	 */
	public Integer getPushNotifyFlag()
	{
		return pushNotifyFlag;
	}

	/**
	 * To setpushNotifyFlag value.
	 * 
	 * @param pushNotifyFlag
	 *            the pushNotifyFlag to set
	 */
	public void setPushNotifyFlag(Integer pushNotifyFlag)
	{
		this.pushNotifyFlag = pushNotifyFlag;
	}

	/**
	 * To get skuCode value.
	 * 
	 * @return the skuCode
	 */
	public String getSkuCode()
	{
		return skuCode;
	}

	/**
	 * To set skuCode value.
	 * 
	 * @param skuCode
	 *            the skuCode to set
	 */
	public void setSkuCode(String skuCode)
	{

		if (null == skuCode)
		{
			this.skuCode = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.skuCode = skuCode;
		}

	}

	/**
	 * To get row_Num value.
	 * 
	 * @return the row_Num
	 */

	public Integer getRow_Num()
	{
		return row_Num;
	}

	/**
	 * for setting rowNum.
	 * 
	 * @param rowNum
	 *            the row_Num to set
	 */
	public void setRow_Num(Integer rowNum)
	{
		row_Num = rowNum;
	}

	/**
	 * for getting productMediaName.
	 * 
	 * @return the productMediaName
	 */

	public String getProductMediaName()
	{
		return productMediaName;
	}

	/**
	 * to set productMediaName.
	 * 
	 * @param productMediaName
	 *            . the productMediaName to set
	 */
	public void setProductMediaName(String productMediaName)
	{
		if (null == productMediaName)
		{
			this.productMediaName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.productMediaName = productMediaName;
		}

	}

	/**
	 * this method for getting productMediaID.
	 * 
	 * @return the productMediaID
	 */
	public Integer getProductMediaID()
	{
		return productMediaID;
	}

	/**
	 * this method for setting productMediaID.
	 * 
	 * @param productMediaID
	 *            the productMediaID to set
	 */
	public void setProductMediaID(Integer productMediaID)
	{
		this.productMediaID = productMediaID;
	}

	/**
	 * this method of r getting productMediaPath.
	 * 
	 * @return the productMediaPath
	 */
	public String getProductMediaPath()
	{
		return productMediaPath;
	}

	/**
	 * this method for setting productMediaPath.
	 * 
	 * @param productMediaPath
	 *            the productMediaPath to set
	 */
	public void setProductMediaPath(String productMediaPath)
	{
		if (null == productMediaPath)
		{
			this.productMediaPath = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.productMediaPath = productMediaPath;
		}

	}

	/**
	 * this method for getting videoFlag.
	 * 
	 * @return the videoFlag
	 */
	public String getVideoFlag()
	{
		return videoFlag;
	}

	/**
	 * this method for setting videoFlag.
	 * 
	 * @param videoFlag
	 *            the videoFlag to set
	 */
	public void setVideoFlag(String videoFlag)
	{
		if (null == videoFlag)
		{
			this.videoFlag = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.videoFlag = videoFlag;
		}

	}

	/**
	 * this method for getting audioFlag.
	 * 
	 * @return the audioFlag
	 */
	public String getAudioFlag()
	{
		return audioFlag;
	}

	/**
	 * this method for setting audioFlag.
	 * 
	 * @param audioFlag
	 *            the audioFlag to set
	 */
	public void setAudioFlag(String audioFlag)
	{
		if (null == audioFlag)
		{

			this.audioFlag = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.audioFlag = audioFlag;
		}

	}

	/**
	 * this method for getting fileFlag.
	 * 
	 * @return the fileFlag
	 */
	public String getFileFlag()
	{
		return fileFlag;
	}

	/**
	 * this method for setting fileFlag.
	 * 
	 * @param fileFlag
	 *            the fileFlag to set
	 */
	public void setFileFlag(String fileFlag)
	{
		if (null == fileFlag)
		{

			this.fileFlag = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.fileFlag = fileFlag;
		}

	}

	/**
	 * for getting userId.
	 * 
	 * @return userId.
	 */

	/**
	 * this method for getting userProductId.
	 * 
	 * @return the userProductId
	 */
	public Integer getUserProductId()
	{
		return userProductId;
	}

	/**
	 * this method for setting userProductId.
	 * 
	 * @param userProductId
	 *            the userProductId to set
	 */
	public void setUserProductId(Integer userProductId)
	{
		this.userProductId = userProductId;
	}

	/**
	 * this method for getting shopCartItem.
	 * 
	 * @return the shopCartItem
	 */
	public Integer getShopCartItem()
	{
		return shopCartItem;
	}

	/**
	 * this method for setting shopCartItem.
	 * 
	 * @param shopCartItem
	 *            the shopCartItem to set
	 */
	public void setShopCartItem(Integer shopCartItem)
	{
		this.shopCartItem = shopCartItem;
	}

	/**
	 * this method for getting shopListItem.
	 * 
	 * @return the shopListItem
	 */
	public Integer getShopListItem()
	{
		return shopListItem;
	}

	/**
	 * this method for setting shopListItem.
	 * 
	 * @param shopListItem
	 *            the shopListItem to set
	 */
	public void setShopListItem(Integer shopListItem)
	{
		this.shopListItem = shopListItem;
	}

	/**
	 * this method for getting distance.
	 * 
	 * @return the distance
	 */
	public Integer getDistance()
	{
		return distance;
	}

	/**
	 * this method for setting distance.
	 * 
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(Integer distance)
	{
		this.distance = distance;
	}

	/**
	 * this method for getting productPrice.
	 * 
	 * @return the productPrice
	 */
	public String getProductPrice()
	{
		return productPrice;
	}

	/**
	 * this method for setting productPrice.
	 * 
	 * @param productPrice
	 *            the productPrice to set
	 */
	public void setProductPrice(String productPrice)
	{
		if (null != productPrice && !"".equals(productPrice) )
		{
			this.productPrice = Utility.formatDecimalValue(productPrice);
			this.productPrice = "$" + productPrice;
		}
		else
		{

			this.productPrice = productPrice;

		}

	}

	/**
	 * for getting scanTypeID.
	 * 
	 * @return scanTypeID
	 */
	public Integer getScanTypeID()
	{
		return scanTypeID;
	}

	/**
	 * for setting scanTypeID.
	 * 
	 * @param scanTypeID
	 *            as request parameter.
	 */
	public void setScanTypeID(Integer scanTypeID)
	{
		this.scanTypeID = scanTypeID;
	}

	/**
	 * for getting productWeight.
	 * 
	 * @return productWeight
	 */
	public String getProductWeight()
	{
		return productWeight;
	}

	/**
	 * for setting productWeight.
	 * 
	 * @param productWeight
	 *            as request parameter.
	 */
	public void setProductWeight(String productWeight)
	{
		if (null == productWeight)
		{
			this.productWeight = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.productWeight = productWeight;
		}

	}

	/**
	 * for getting rebateId.
	 * 
	 * @return rebateId
	 */

	public Integer getRebateId()
	{
		return rebateId;
	}

	/**
	 * for setting rebateId.
	 * 
	 * @param rebateId
	 *            as request parameter.
	 */
	public void setRebateId(Integer rebateId)
	{
		this.rebateId = rebateId;
	}

	/**
	 * for getting deviceId.
	 * 
	 * @return deviceId
	 */
	public String getDeviceId()
	{
		return deviceId;
	}

	/**
	 * for setting deviceId.
	 * 
	 * @param deviceId
	 *            as request parameter.
	 */
	public void setDeviceId(String deviceId)
	{
		if (null == deviceId)
		{
			this.deviceId = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.deviceId = deviceId;
		}

	}

	/**
	 * for getting scanLongitude.
	 * 
	 * @return scanLongitude
	 */
	public Double getScanLongitude()
	{
		return scanLongitude;
	}

	/**
	 * for setting scanLongitude.
	 * 
	 * @param scanLongitude
	 *            as request parameter.
	 */
	public void setScanLongitude(Double scanLongitude)
	{
		this.scanLongitude = scanLongitude;
	}

	/**
	 * for getting scanLatitude.
	 * 
	 * @return scanLatitude
	 */
	public Double getScanLatitude()
	{
		return scanLatitude;
	}

	/**
	 * for setting scanLatitude.
	 * 
	 * @param scanLatitude
	 *            as parameter.
	 */
	public void setScanLatitude(Double scanLatitude)
	{
		this.scanLatitude = scanLatitude;
	}

	/**
	 * for getting fieldAgentRequest.
	 * 
	 * @return fieldAgentRequest
	 */
	public Integer getFieldAgentRequest()
	{
		return fieldAgentRequest;
	}

	/**
	 * for setting fieldAgentRequest.
	 * 
	 * @param fieldAgentRequest
	 *            as request parameter.
	 */
	public void setFieldAgentRequest(Integer fieldAgentRequest)
	{
		this.fieldAgentRequest = fieldAgentRequest;
	}

	/**
	 * this method for getting rowNumber.
	 * 
	 * @return the rowNumber
	 */
	public Integer getRowNumber()
	{
		return rowNumber;
	}

	/**
	 * this method for setting rowNumber.
	 * 
	 * @param rowNumber
	 *            the rowNumber to set
	 */
	public void setRowNumber(Integer rowNumber)
	{
		this.rowNumber = rowNumber;
	}

	/**
	 * this method for getting productDescription.
	 * 
	 * @return the productDescription
	 */
	public String getProductDescription()
	{
		return productDescription;
	}

	/**
	 * this method for setting productDescription.
	 * 
	 * @param productDescription
	 *            the productDescription to set
	 */
	public void setProductDescription(String productDescription)
	{
	
			this.productDescription = productDescription;

	

	}

	/**
	 * this method for getting productId.
	 * 
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * this method for setting productId.
	 * 
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * this method for getting productName.
	 * 
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * this method for setting productName.
	 * 
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		if (null != productName && !"".equals(productName))
		{
			this.productName = Utility.removeHTMLTags(productName);
		}
		else
		{
			this.productName = productName;
		}	
		
		
		}

	/**
	 * this method for getting manufacturersName.
	 * 
	 * @return the manufacturersName
	 */
	public String getManufacturersName()
	{
		return manufacturersName;
	}

	/**
	 * this method for setting manufacturersName.
	 * 
	 * @param manufacturersName
	 *            the manufacturersName to set
	 */
	public void setManufacturersName(String manufacturersName)
	{

		this.manufacturersName = manufacturersName;

	}

	/**
	 * this method for getting imagePath.
	 * 
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * this method for setting imagePath.
	 * 
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		if (null == imagePath || imagePath.equals(""))
		{
			this.imagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{
			this.imagePath = imagePath;
		}

	}

	/**
	 * this method for getting modelNumber.
	 * 
	 * @return the modelNumber
	 */
	public String getModelNumber()
	{
		return modelNumber;
	}

	/**
	 * this method for setting modelNumber.
	 * 
	 * @param modelNumber
	 *            the modelNumber to set
	 */
	public void setModelNumber(String modelNumber)
	{
		
			this.modelNumber = modelNumber;
		

	}

	/**
	 * this method for getting suggestedRetailPrice.
	 * 
	 * @return the suggestedRetailPrice
	 */
	public String getSuggestedRetailPrice()
	{
		return suggestedRetailPrice;
	}

	/**
	 * this method for setting suggestedRetailPrice.
	 * 
	 * @param suggestedRetailPrice
	 *            the suggestedRetailPrice to set
	 */
	public void setSuggestedRetailPrice(String suggestedRetailPrice)
	{
		if (null != suggestedRetailPrice && !"".equals(suggestedRetailPrice))
		{		
			this.suggestedRetailPrice = Utility.formatDecimalValue(suggestedRetailPrice);
		}

	}

	/**
	 * this method for getting weight.
	 * 
	 * @return the weight
	 */
	public String getWeight()
	{
		return productWeight;
	}

	/**
	 * this method for setting weight.
	 * 
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(String weight)
	{

		this.productWeight = weight;

	}

	/**
	 * this method for getting productExpDate.
	 * 
	 * @return the productExpDate
	 */
	public String getProductExpDate()
	{
		return productExpDate;
	}

	/**
	 * this method for setting productExpDate.
	 * 
	 * @param productExpDate
	 *            the productExpDate to set
	 */
	public void setProductExpDate(String productExpDate)
	{
		if (productExpDate != null)
		{
			this.productExpDate = productExpDate;
			this.productExpDate = Utility.convertDBdate(productExpDate);
		}

	}

	/**
	 * this method for getting weightUnits.
	 * 
	 * @return the weightUnits
	 */
	public String getWeightUnits()
	{
		return weightUnits;
	}

	/**
	 * this method for setting weightUnits.
	 * 
	 * @param weightUnits
	 *            the weightUnits to set
	 */
	public void setWeightUnits(String weightUnits)
	{

		this.weightUnits = weightUnits;

	}

	/**
	 * thid method for getting coupons.
	 * 
	 * @return the coupons
	 */
	public Integer getCoupons()
	{
		return coupons;
	}

	/**
	 * this method for setting coupons.
	 * 
	 * @param coupons
	 *            the coupons to set
	 */
	public void setCoupons(Integer coupons)
	{
		this.coupons = coupons;
	}

	/**
	 * this method for getting rebates.
	 * 
	 * @return the rebates
	 */
	public Integer getRebates()
	{
		return rebates;
	}

	/**
	 * this method for setting rebates.
	 * 
	 * @param rebates
	 *            the rebates to set
	 */
	public void setRebates(Integer rebates)
	{
		this.rebates = rebates;
	}

	/**
	 * this method for getting loyalties.
	 * 
	 * @return the loyalties
	 */
	public Integer getLoyalties()
	{
		return loyalties;
	}

	/**
	 * this method for setting loyalties.
	 * 
	 * @param loyalties
	 *            the loyalties to set
	 */
	public void setLoyalties(Integer loyalties)
	{
		this.loyalties = loyalties;
	}

	/**
	 * this method for getting coupon_Status.
	 * 
	 * @return the coupon_Status
	 */
	public String getCoupon_Status()
	{
		return coupon_Status;
	}

	/**
	 * this method for setting coupon_Status.
	 * 
	 * @param couponStatus
	 *            the coupon_Status to set
	 */
	public void setCoupon_Status(String couponStatus)
	{
		if (null == couponStatus)
		{
			this.coupon_Status = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.coupon_Status = couponStatus;
		}

	}

	/**
	 * this method for getting rebate_Status.
	 * 
	 * @return the rebate_Status
	 */
	public String getRebate_Status()
	{
		return rebate_Status;
	}

	/**
	 * this method for setting rebate_Status.
	 * 
	 * @param rebateStatus
	 *            the rebate_Status to set
	 */
	public void setRebate_Status(String rebateStatus)
	{
		if (null == rebateStatus)
		{

			this.rebate_Status = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.rebate_Status = rebateStatus;
		}

	}

	/**
	 * this method for getting loyalty_Status.
	 * 
	 * @return the loyalty_Status
	 */
	public String getLoyalty_Status()
	{
		return loyalty_Status;
	}

	/**
	 * this method for setting loyaltyStatus.
	 * 
	 * @param loyaltyStatus
	 *            the loyalty_Status to set
	 */
	public void setLoyalty_Status(String loyaltyStatus)
	{
		if (null == loyaltyStatus)
		{
			this.loyalty_Status = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.loyalty_Status = loyaltyStatus;
		}

	}

	/**
	 * this method for setting productShortDescription.
	 * 
	 * @return productShortDescription
	 */
	public String getProductShortDescription()
	{
		return productShortDescription;
	}

	/**
	 * this method for setting productShortDescription. the loyalty_Status to
	 * set
	 * 
	 * @param productShortDescription
	 *            the productShortDescription to set
	 */

	public void setProductShortDescription(String productShortDescription)
	{

		if (null != productShortDescription && !"".equals(productShortDescription))
		{
			this.productShortDescription = Utility.removeHTMLTags(productShortDescription);
		}
		else
		{
			this.productShortDescription = productShortDescription;
		}
	}

	/**
	 * this method for getting productLongDescription.
	 * 
	 * @return productLongDescription
	 */
	public String getProductLongDescription()
	{
		return productLongDescription;
	}

	/**
	 * this method for setting productLongDescription.
	 * 
	 * @param productLongDescription
	 *            as request parameter.
	 */
	public void setProductLongDescription(String productLongDescription)
	{
		
		if (null != productLongDescription && !"".equals(productLongDescription))
		{
			this.productLongDescription = Utility.removeHTMLTags(productLongDescription);
		}
		else
		{
			this.productLongDescription = productLongDescription;
		}
		

	}

	/**
	 * this method for getting usage.
	 * 
	 * @return the usage
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * for setting usage.
	 * 
	 * @param usage
	 *            the usage to set
	 */
	public void setUsage(String usage)
	{
		if (null == usage)
		{
			this.usage = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.usage = usage;
		}

	}

	/**
	 * for getting productVideoUrl.
	 * 
	 * @return productVideoUrl
	 */
	public String getProductVideoUrl()
	{
		return productVideoUrl;
	}

	/**
	 * for setting productVideoUrl.
	 * 
	 * @param productVideoUrl
	 *            the productVideoUrl to set
	 */
	public void setProductVideoUrl(String productVideoUrl)
	{
		if (null == productVideoUrl)
		{
			this.productVideoUrl = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{

			this.productVideoUrl = productVideoUrl;
		}

	}

	/**
	 * for getting barCode.
	 * 
	 * @return barCode
	 */
	public String getBarCode()
	{
		return barCode;
	}

	/**
	 * for setting barCode.
	 * 
	 * @param barCode
	 *            as parameter
	 */
	public void setBarCode(String barCode)
	{
		if (barCode == null)
		{
			this.barCode = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.barCode = barCode;
		}

	}

	/**
	 * this method for getting searchkey.
	 * 
	 * @return searchkey
	 */
	public String getSearchkey()
	{
		return searchkey;
	}

	/**
	 * for setting searchkey.
	 * 
	 * @param searchkey
	 *            to be set.
	 */
	public void setSearchkey(String searchkey)
	{
		
			this.searchkey = searchkey;
		
	}

	/**
	 * for getting scanDate.
	 * 
	 * @return scanDate
	 */
	public String getScanDate()
	{
		return scanDate;
	}

	/**
	 * for setting scanDate.
	 * 
	 * @param scanDate
	 *            to be set.
	 */

	public void setScanDate(String scanDate)
	{
		if (scanDate != null)
		{

			this.scanDate = scanDate;
			this.scanDate = Utility.convertDBdate(scanDate);
		}
		else
		{
			this.scanDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * for getting scanStatus.
	 * 
	 * @return scanStatus
	 */

	public String getScanStatus()
	{
		return scanStatus;
	}

	/**
	 * for setting scanStatus.
	 * 
	 * @param scanStatus
	 *            to be set.
	 */

	public void setScanStatus(String scanStatus)
	{
		if (null == scanStatus)
		{

			this.scanStatus = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.scanStatus = scanStatus;
		}

	}

	/**
	 * for getting couponFlag.
	 * 
	 * @return couponFlag
	 */

	public String getCouponFlag()
	{
		return couponFlag;
	}

	/**
	 * for setting couponFlag.
	 * 
	 * @param couponFlag
	 *            to be set.
	 */

	public void setCouponFlag(String couponFlag)
	{
		if (null == couponFlag)
		{
			this.couponFlag = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponFlag = couponFlag;
		}

	}

	/**
	 * for getting loyaltyFlag.
	 * 
	 * @return loyaltyFlag
	 */

	public String getLoyaltyFlag()
	{
		return loyaltyFlag;
	}

	/**
	 * for setting loyaltyFlag.
	 * 
	 * @param loyaltyFlag
	 *            to be set.
	 */

	public void setLoyaltyFlag(String loyaltyFlag)
	{
		if (null == loyaltyFlag)
		{
			this.loyaltyFlag = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.loyaltyFlag = loyaltyFlag;
		}

	}

	/**
	 * for getting rebateFlag.
	 * 
	 * @return rebateFlag
	 */

	public String getRebateFlag()
	{
		return rebateFlag;
	}

	/**
	 * for setting rebateFlag.
	 * 
	 * @param rebateFlag
	 *            to be set.
	 */

	public void setRebateFlag(String rebateFlag)
	{
		if (null == rebateFlag)
		{
			this.rebateFlag = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.rebateFlag = rebateFlag;
		}

	}

	/**
	 * for getting scanType.
	 * 
	 * @return scanType
	 */
	public String getScanType()
	{
		return scanType;
	}

	/**
	 * for setting scanType.
	 * 
	 * @param scanType
	 *            to be set.
	 */
	public void setScanType(String scanType)
	{
		if (null == scanType)
		{
			this.scanType = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.scanType = scanType;

		}
	}

	/**
	 * for getting clr.
	 * 
	 * @return clr
	 */
	public Integer getClr()
	{
		return clr;
	}

	/**
	 * for setting clr.
	 * 
	 * @param clr
	 *            the clr to set
	 */
	public void setClr(Integer clr)
	{
		this.clr = clr;
	}

	/**
	 * for getting categoryID.
	 * 
	 * @return categoryID
	 */
	public Integer getCategoryID()
	{
		return categoryID;
	}

	/**
	 * for setting categoryID.
	 * 
	 * @param categoryID
	 *            to be set.
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * for getting parentCategoryName.
	 * 
	 * @return parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}

	/**
	 * for parentCategoryID.
	 */
	private Integer parentCategoryID;

	/**
	 * for setting parentCategoryName.
	 * 
	 * @param parentCategoryName
	 *            to be set.
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		if (null == parentCategoryName)
		{
			this.parentCategoryName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.parentCategoryName = parentCategoryName;
		}

	}

	/**
	 * for getting CLRFlag.
	 * 
	 * @return CLRFlag
	 */
	public Integer getCLRFlag()
	{
		return CLRFlag;
	}

	/**
	 * for setting cLRFlag.
	 * 
	 * @param cLRFlag
	 *            to be set.
	 */
	public void setCLRFlag(Integer cLRFlag)
	{
		CLRFlag = cLRFlag;
	}

	/**
	 * for getting retailerName.
	 * 
	 * @return retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * for setting retailerName.
	 * 
	 * @param retailerName
	 *            to be set.
	 */
	public void setRetailerName(String retailerName)
	{
		if (null == retailerName)
		{
			this.retailerName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.retailerName = retailerName;
		}

	}

	/**
	 * for getting retailerId.
	 * 
	 * @return retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * for setting retailerId.
	 * 
	 * @param retailerId
	 *            to be set.
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * for getting salePrice.
	 * 
	 * @return salePrice
	 */
	public String getSalePrice()
	{
		return salePrice;
	}

	/**
	 * for setting salePrice.
	 * 
	 * @param salePrice
	 *            to be set.
	 */
	public void setSalePrice(String salePrice)
	{
		if (salePrice == null)
		{
			this.salePrice = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			if (!salePrice.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(salePrice))
			{

				// this.salePrice = Utility.formatDecimalValue(salePrice);
				this.salePrice = "$" + Utility.formatDecimalValue(salePrice);

			}
			else
			{
				this.salePrice = salePrice;
			}
		}

	}

	/**
	 * to get lastVistedProductNo.
	 * 
	 * @return the lastVistedProductNo
	 */
	public Integer getLastVistedProductNo()
	{
		return lastVistedProductNo;
	}

	/**
	 * to set lastVistedProductNo.
	 * 
	 * @param lastVistedProductNo
	 *            the lastVistedProductNo to set
	 */
	public void setLastVistedProductNo(Integer lastVistedProductNo)
	{
		this.lastVistedProductNo = lastVistedProductNo;
	}

	/**
	 * to get buttonFlag.
	 * 
	 * @return the buttonFlag
	 */
	public Integer getButtonFlag()
	{
		return buttonFlag;
	}

	/**
	 * to set buttonFlag.
	 * 
	 * @param buttonFlag
	 *            the buttonFlag to set
	 */
	public void setButtonFlag(Integer buttonFlag)
	{
		this.buttonFlag = buttonFlag;
	}

	/**
	 * to get subCategoryName.
	 * 
	 * @return the subCategoryName
	 */
	public String getSubCategoryName()
	{
		return subCategoryName;
	}

	/**
	 * to set subCategoryName.
	 * 
	 * @param subCategoryName
	 *            the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName)
	{
		if (null == subCategoryName)
		{
			this.subCategoryName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.subCategoryName = subCategoryName;
		}

	}

	/**
	 * to get subCategoryID.
	 * 
	 * @return the subCategoryID
	 */
	public Integer getSubCategoryID()
	{
		return subCategoryID;
	}

	/**
	 * to set subCategoryID.
	 * 
	 * @param subCategoryID
	 *            the subCategoryID to set
	 */
	public void setSubCategoryID(Integer subCategoryID)
	{
		this.subCategoryID = subCategoryID;
	}

	/**
	 * to get productImagePath.
	 * 
	 * @return the productImagePath
	 */
	public String getProductImagePath()
	{
		return productImagePath;
	}

	/**
	 * to set productImagePath.
	 * 
	 * @param productImagePath
	 *            the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath)
	{
		if (null == productImagePath || productImagePath.equals(""))
		{
			// this.productImagePath = ApplicationConstants.NOTAPPLICABLE;
			// Changed below code as IE browser showing broken image if it did
			// not find image
			// this.productImagePath = ApplicationConstants.BLANKIMAGE;
			this.productImagePath = ApplicationConstants.IMAGENOTFOUND;

		}
		else
		{
			this.productImagePath = productImagePath;
		}
	}

	/**
	 * To get productDeletedDate value.
	 * 
	 * @return the productDeletedDate
	 */
	public String getProductDeletedDate()
	{
		return productDeletedDate;
	}

	/**
	 * To set productDeletedDate value.
	 * 
	 * @param productDeletedDate
	 *            the productDeletedDate to set
	 */
	public void setProductDeletedDate(String productDeletedDate)
	{
		if (null == productDeletedDate)
		{
			this.productDeletedDate = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.productDeletedDate = productDeletedDate;
		}
	}

	/**
	 * To get wishListAddDate value.
	 * 
	 * @return the wishListAddDate
	 */
	public String getWishListAddDate()
	{
		return wishListAddDate;
	}

	/**
	 * To set wishListAddDate value.
	 * 
	 * @param wishListAddDate
	 *            the wishListAddDate to set
	 */
	public void setWishListAddDate(String wishListAddDate)
	{
		if (null != wishListAddDate && !"".equals(wishListAddDate))
		{
			this.wishListAddDate = wishListAddDate;
			this.wishListAddDate = Utility.convertDBdate(wishListAddDate);
		}
		
	}

	/**
	 * To get retailFlag value.
	 * 
	 * @return the retailFlag
	 */
	public Integer getRetailFlag()
	{
		return retailFlag;
	}

	/**
	 * To set retailFlag value.
	 * 
	 * @param retailFlag
	 *            the retailFlag to set
	 */
	public void setRetailFlag(Integer retailFlag)
	{
		this.retailFlag = retailFlag;
	}

	/**
	 * To get couponSaleFlag value.
	 * 
	 * @return the couponSaleFlag
	 */
	public Integer getCouponSaleFlag()
	{
		return couponSaleFlag;
	}

	/**
	 * To set couponSaleFlag value.
	 * 
	 * @param couponSaleFlag
	 *            the couponSaleFlag to set
	 */
	public void setCouponSaleFlag(Integer couponSaleFlag)
	{
		this.couponSaleFlag = couponSaleFlag;
	}

	/**
	 * To get hotDealFlag avlue.
	 * 
	 * @return the hotDealFlag
	 */
	public Integer getHotDealFlag()
	{
		return hotDealFlag;
	}

	/**
	 * To set hotDealFlag value.
	 * 
	 * @param hotDealFlag
	 *            the hotDealFlag to set
	 */
	public void setHotDealFlag(Integer hotDealFlag)
	{
		this.hotDealFlag = hotDealFlag;
	}

	/**
	 * To get attributeName.
	 * 
	 * @return the attributeName
	 */
	public String getAttributeName()
	{
		return attributeName;
	}

	/**
	 * To set attributeName.
	 * 
	 * @param attributeName
	 *            the attributeName to set
	 */
	public void setAttributeName(String attributeName)
	{
		this.attributeName = attributeName;
	}

	/**
	 * To get displayValue.
	 * 
	 * @return the displayValue
	 */
	public String getDisplayValue()
	{
		return displayValue;
	}

	/**
	 * To set displayValue.
	 * 
	 * @param displayValue
	 *            the displayValue to set
	 */
	public void setDisplayValue(String displayValue)
	{
		this.displayValue = Utility.removeHTMLTags(displayValue);
	}

	/**
	 * To get starFlag.
	 * 
	 * @return the starFlag
	 */
	public Integer getStarFlag()
	{
		return starFlag;
	}

	/**
	 * To set starFlag.
	 * 
	 * @param starFlag
	 *            the starFlag to set
	 */
	public void setStarFlag(Integer starFlag)
	{
		this.starFlag = starFlag;
	}

	/**
	 * @return the zipcode
	 */
	public String getZipcode()
	{
		return zipcode;
	}

	/**
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}

	/**
	 * @return the radius
	 */
	public Integer getRadius()
	{
		return radius;
	}

	/**
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(Integer radius)
	{
		this.radius = radius;
	}

	/**
	 * @return the gpsEnabled
	 */
	public Boolean getGpsEnabled()
	{
		return gpsEnabled;
	}

	/**
	 * @param gpsEnabled
	 *            the gpsEnabled to set
	 */
	public void setGpsEnabled(Boolean gpsEnabled)
	{
		this.gpsEnabled = gpsEnabled;
	}

	public String getDiscount()
	{
		return discount;
	}

	public void setDiscount(String discount)
	{
		if (null == discount)
		{
			this.discount = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{

			this.discount = discount + "%";
		}
	}

	public String getRegularPrice()
	{
		return regularPrice;
	}

	public void setRegularPrice(String regularPrice)
	{
		if (null == regularPrice)
		{
			this.regularPrice = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{

			if (!regularPrice.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(regularPrice))
			{
				// this.regularPrice = Utility.formatDecimalValue(regularPrice);
				this.regularPrice = "$" + Utility.formatDecimalValue(regularPrice);
			}
			else
			{
				this.regularPrice = regularPrice;
			}
		}
	}

	public Boolean isProductExists()
	{
		return productExists;
	}

	public void setProductExists(Boolean productExists)
	{
		this.productExists = productExists;
	}

	public Integer getProductIsThere()
	{
		return productIsThere;
	}

	public void setProductIsThere(Integer productIsThere)
	{
		this.productIsThere = productIsThere;
	}

	public Integer getNextPage()
	{
		return nextPage;
	}

	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}

	/**
	 * @return the scanCode
	 */
	public String getScanCode()
	{
		return scanCode;
	}

	/**
	 * @param scanCode
	 *            the scanCode to set
	 */
	public void setScanCode(String scanCode)
	{
		this.scanCode = scanCode;
	}

	/**
	 * @return the parentCategoryID
	 */
	public Integer getParentCategoryID()
	{
		return parentCategoryID;
	}

	/**
	 * @param parentCategoryID
	 *            the parentCategoryID to set
	 */
	public void setParentCategoryID(Integer parentCategoryID)
	{
		this.parentCategoryID = parentCategoryID;
	}

	/**
	 * @return the price
	 */
	public String getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price)
	{

		if (price == null)
		{
			this.price = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			if (!price.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(price))
			{

				// this.salePrice = Utility.formatDecimalValue(salePrice);
				this.price = "$" + Utility.formatDecimalValue(price);

			}
			else
			{
				this.price = price;
			}
		}

	}

	/**
	 * @return the productExists
	 */
	public Boolean getProductExists()
	{
		return productExists;
	}

	public String getSearchValue()
	{
		return searchValue;
	}

	public void setSearchValue(String searchValue)
	{
		this.searchValue = searchValue;
	}

	public boolean isIsWishlst()
	{
		return IsWishlst;
	}

	public void setIsWishlst(boolean isWishlst)
	{
		IsWishlst = isWishlst;
	}

	public boolean isWlratereview()
	{
		return wlratereview;
	}

	public void setWlratereview(boolean wlratereview)
	{
		this.wlratereview = wlratereview;
	}

	public boolean isWlonlineratereview()
	{
		return wlonlineratereview;
	}

	public void setWlonlineratereview(boolean wlonlineratereview)
	{
		this.wlonlineratereview = wlonlineratereview;
	}

	public boolean isSearchProd()
	{
		return searchProd;
	}

	public void setSearchProd(boolean searchProd)
	{
		this.searchProd = searchProd;
	}

	public boolean isWishlistmain()
	{
		return wishlistmain;
	}

	public void setWishlistmain(boolean wishlistmain)
	{
		this.wishlistmain = wishlistmain;
	}

	public boolean isFromFind()
	{
		return fromFind;
	}

	public void setFromFind(boolean fromFind)
	{
		this.fromFind = fromFind;
	}

	public boolean isFromScannow()
	{
		return fromScannow;
	}

	public void setFromScannow(boolean fromScannow)
	{
		this.fromScannow = fromScannow;
	}

	public String getWarrantyServiceInfo()
	{
		return warrantyServiceInfo;
	}

	public void setWarrantyServiceInfo(String warrantyServiceInfo)
	{
		this.warrantyServiceInfo = warrantyServiceInfo;
	}

	public List<ProductDetails> getProductImageslst()
	{
		return productImageslst;
	}

	public void setProductImageslst(List<ProductDetails> productImageslst)
	{
		this.productImageslst = productImageslst;
	}

	public Integer getProdRatingCount()
	{
		return prodRatingCount;
	}

	public void setProdRatingCount(Integer prodRatingCount)
	{
		this.prodRatingCount = prodRatingCount;
	}

	public String getAudioFilePath()
	{
		return audioFilePath;
	}

	public void setAudioFilePath(String audioFilePath)
	{
		this.audioFilePath = audioFilePath;
	}

	public String getVideoFilePath()
	{
		return videoFilePath;
	}

	public void setVideoFilePath(String videoFilePath)
	{
		this.videoFilePath = videoFilePath;
	}

	public String getRedirectUrl()
	{
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}

	public List<String> getImagelst()
	{
		return imagelst;
	}

	public void setImagelst(List<String> imagelst)
	{
		this.imagelst = imagelst;
	}

	public String getLatitude()
	{
		return latitude;
	}

	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getReviewURL()
	{
		return reviewURL;
	}

	public void setReviewURL(String reviewURL)
	{
		this.reviewURL = reviewURL;
	}

	public String getReviewComments()
	{
		return reviewComments;
	}

	public void setReviewComments(String reviewComments)
	{
		this.reviewComments = reviewComments;
	}

	public Integer getProductListID()
	{
		return productListID;
	}

	public void setProductListID(Integer productListID)
	{
		this.productListID = productListID;
	}

	public Integer getModuleID()
	{
		return moduleID;
	}

	public void setModuleID(Integer moduleID)
	{
		this.moduleID = moduleID;
	}

	public String getToEmailId()
	{
		return toEmailId;
	}

	public void setToEmailId(String toEmailId)
	{
		this.toEmailId = toEmailId;
	}

	public String getFromEmailId()
	{
		return fromEmailId;
	}

	public void setFromEmailId(String fromEmailId)
	{
		this.fromEmailId = fromEmailId;
	}

	public Integer getMainMenuID()
	{
		return mainMenuID;
	}

	public void setMainMenuID(Integer mainMenuID)
	{
		this.mainMenuID = mainMenuID;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public Integer getHotDealId()
	{
		return hotDealId;
	}

	public void setHotDealId(Integer hotDealId)
	{
		this.hotDealId = hotDealId;
	}

	public Integer getHotDealListID()
	{
		return hotDealListID;
	}

	public void setHotDealListID(Integer hotDealListID)
	{
		this.hotDealListID = hotDealListID;
	}

	public Integer getSaleListID()
	{
		return saleListID;
	}

	public void setSaleListID(Integer saleListID)
	{
		this.saleListID = saleListID;
	}

	public Integer getShareTypeID()
	{
		return shareTypeID;
	}

	public void setShareTypeID(Integer shareTypeID)
	{
		this.shareTypeID = shareTypeID;
	}

	public String getTargetAddress()
	{
		return targetAddress;
	}

	public void setTargetAddress(String targetAddress)
	{
		this.targetAddress = targetAddress;
	}

	public Integer getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(Integer recordCount)
	{
		this.recordCount = recordCount;
	}

	public Integer getFavProductFlag()
	{
		return favProductFlag;
	}

	public void setFavProductFlag(Integer favProductFlag)
	{
		this.favProductFlag = favProductFlag;
	}

	public String getScreenName()
	{
		return screenName;
	}

	public void setScreenName(String screenName)
	{
		this.screenName = screenName;
	}

	public String getProductIds()
	{
		return productIds;
	}

	public void setProductIds(String productIds)
	{
		this.productIds = productIds;
	}

	public String getResponse()
	{
		return response;
	}

	public void setResponse(String response)
	{
		this.response = response;
	}

	public String getAddTo()
	{
		return addTo;
	}

	public void setAddTo(String addTo)
	{
		this.addTo = addTo;
	}

	public String getUsrProdId()
	{
		return usrProdId;
	}

	public void setUsrProdId(String usrProdId)
	{
		this.usrProdId = usrProdId;
	}

	public String getResponseFlag()
	{
		return responseFlag;
	}

	public void setResponseFlag(String responseFlag)
	{
		this.responseFlag = responseFlag;
	}

	public Integer getAlertProdID()
	{
		return alertProdID;
	}

	public void setAlertProdID(Integer alertProdID)
	{
		this.alertProdID = alertProdID;
	}

}

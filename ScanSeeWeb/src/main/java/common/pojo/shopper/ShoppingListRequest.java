package common.pojo.shopper;

import java.util.ArrayList;



/**
 * The POJO for ShoppingListRequest. 
 *@author emmanuel_b
 */

public class ShoppingListRequest {
	
	/**
	 * The userId property.
	 */
	private Long userId;
	
	/**
	 * The IsWishlst property.
	 */
	private boolean IsWishlst = false;
	
	/**
	 * The searchKey property.
	 */
	private String searchValue;
	
	/**
	 * for selectedProductId.
	 */
	private Integer selectedProductId;

	/**
	 * for isaddToTSL.
	 */
	private boolean isaddToTSL = false;
	
	/**
	 * for userProductID.
	 */
	private Integer userProductID;

	/**
	 * for cartUserProductID.
	 */
	private Integer cartUserProductID;

	/**
	 * for productDetails.
	 */
	private ArrayList<ProductDetail> productDetails;
	
	/**
	 * for selectedProductId.
	 */
	private Integer productId;

	private String fromSL;
	
	private String fromSLFAV;
	/**
	 * The IsWishlst property.
	 */
	private boolean back = false;
	/**
	 * for productName.
	 */
	private String productName;
	/**
	 * for productDescription.
	 */
	private String productDescription;
	
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return productDescription;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @param productDescription the productDescription to set
	 */
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	/**
	 * @return the back
	 */
	public boolean isBack() {
		return back;
	}
	/**
	 * @param back the back to set
	 */
	public void setBack(boolean back) {
		this.back = back;
	}
	/**
	 * @return the fromSL
	 */
	public String getFromSL() {
		return fromSL;
	}
	/**
	 * @param fromSL the fromSL to set
	 */
	public void setFromSL(String fromSL) {
		this.fromSL = fromSL;
	}
	/**
	 * @return the fromSLFAV
	 */
	public String getFromSLFAV() {
		return fromSLFAV;
	}
	/**
	 * @param fromSLFAV the fromSLFAV to set
	 */
	public void setFromSLFAV(String fromSLFAV) {
		this.fromSLFAV = fromSLFAV;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 
	 * To get searchKey.
	 * @return The getter for searchKey property.
	 */
	public String getSearchValue() {
		return searchValue;
	}
	/**
	 * 
	 * To set searchKey.
	 * @param searchKey The setter for searchKey property.
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	/**
	 * @return the isWishlst
	 */
	public boolean isIsWishlst() {
		return IsWishlst;
	}

	/**
	 * @param isWishlst the isWishlst to set
	 */
	public void setIsWishlst(boolean isWishlst) {
		IsWishlst = isWishlst;
	}
	
	/**
	 * @return the selectedProductId
	 */
	public Integer getSelectedProductId() {
		return selectedProductId;
	}

	/**
	 * @param selectedProductId the selectedProductId to set
	 */
	public void setSelectedProductId(Integer selectedProductId) {
		this.selectedProductId = selectedProductId;
	}

	/**
	 * @return the userProductID
	 */
	public Integer getUserProductID() {
		return userProductID;
	}

	/**
	 * @param userProductID the userProductID to set
	 */
	public void setUserProductID(Integer userProductID) {
		this.userProductID = userProductID;
	}

	/**
	 * @return the isaddToTSL
	 */
	public boolean isIsaddToTSL() {
		return isaddToTSL;
	}

	/**
	 * @param isaddToTSL the isaddToTSL to set
	 */
	public void setIsaddToTSL(boolean isaddToTSL) {
		this.isaddToTSL = isaddToTSL;
	}
	/**
	 * @return the cartUserProductID
	 */
	public Integer getCartUserProductID() {
		return cartUserProductID;
	}
	/**
	 * @param cartUserProductID the cartUserProductID to set
	 */
	public void setCartUserProductID(Integer cartUserProductID) {
		this.cartUserProductID = cartUserProductID;
	}
	/**
	 * @return the productDetails
	 */
	public ArrayList<ProductDetail> getProductDetails() {
		return productDetails;
	}
	/**
	 * @param productDetails the productDetails to set
	 */
	public void setProductDetails(ArrayList<ProductDetail> productDetails) {
		this.productDetails = productDetails;
	}
	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	

}

package common.pojo.shopper;

/**
 * This class contains setter and getter methods.
 * @author dileepa_cc
 *
 */
public class EducationalLevelInfo
{
	
	/**
	 * property for educational LevelID.
	 */
	private String educationalLevelId;

	/**
	 * property for educational LevelDesc.
	 */

	private String educationalLevelDesc;

	/**
	 * getter method for educational LevelDesc.
	 * 
	 * @return educationalLevelId
	 */

	public String getEducationalLevelId()
	{
		return educationalLevelId;
	}

	/**
	 * setter method for educational LevelID.
	 * 
	 * @param educationalLevelId parameter
	 */

	public void setEducationalLevelId(String educationalLevelId)
	{

		if (null == educationalLevelId)
		{

			this.educationalLevelId = "N/A";
		}
		else
		{

			this.educationalLevelId = educationalLevelId;
		}

	}

	/**
	 * getter method for educational LevelDesc.
	 * 
	 * @return educationalLevelDesc
	 */

	public String getEducationalLevelDesc()
	{
		return educationalLevelDesc;
	}

	/**
	 * 
	 * setter method for educational LevelDesc.
	 * 
	 * @param educationalLevelDesc parameter
	 */

	public void setEducationalLevelDesc(String educationalLevelDesc)
	{

		if (null == educationalLevelDesc)
		{

			this.educationalLevelDesc = "N/A";
		}
		else
		{

			this.educationalLevelDesc = educationalLevelDesc;
		}

	}


}

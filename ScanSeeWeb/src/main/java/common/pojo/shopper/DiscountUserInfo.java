package common.pojo.shopper;

public class DiscountUserInfo
{
	private Long userID;
	
	private Integer productID;
	
	private Integer retailID;

	

	/**
	 * @return the userID
	 */
	public Long getUserID()
	{
		return userID;
	}

	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Long userID)
	{
		this.userID = userID;
	}

	/**
	 * @return the productID
	 */
	public Integer getProductID()
	{
		return productID;
	}

	/**
	 * @param productID the productID to set
	 */
	public void setProductID(Integer productID)
	{
		this.productID = productID;
	}

	/**
	 * @return the retailID
	 */
	public Integer getRetailID()
	{
		return retailID;
	}

	/**
	 * @param retailID the retailID to set
	 */
	public void setRetailID(Integer retailID)
	{
		this.retailID = retailID;
	}
	
	

	
	
}

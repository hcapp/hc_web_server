package common.pojo.shopper;

import java.util.List;



public class DiscountInfo
{

	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected RebateDetail rebateInfo;
	
	/**
	 * The rebateDetail property.
	 */

	protected List<RebateDetail> rebateDetail;
	
	
    private LoyaltyDetail loyaltyInfo;
	
    
    private Integer couponId;
    
    private Integer couponID;
    
    /**
	 * for rebateId.
	 */
	private Integer rebateId;
	/**
	 * for loyaltyId.
	 */
	private Integer loyaltyDealID;
	
	private String clrType;
	
	
	private String added;
	
	private String clrImagePath;
	
	/**
	 * 
	 * variable loyaltyDetail declare as List.
	 */
	private List<LoyaltyDetail> loyaltyDetail;
	
	
	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected List<CouponDetail> couponDetail;


	/**
	 * @return the rebateInfo
	 */
	public RebateDetail getRebateInfo()
	{
		return rebateInfo;
	}


	/**
	 * @param rebateInfo the rebateInfo to set
	 */
	public void setRebateInfo(RebateDetail rebateInfo)
	{
		this.rebateInfo = rebateInfo;
	}


	/**
	 * @return the rebateDetail
	 */
	public List<RebateDetail> getRebateDetail()
	{
		return rebateDetail;
	}


	/**
	 * @param rebateDetail the rebateDetail to set
	 */
	public void setRebateDetail(List<RebateDetail> rebateDetail)
	{
		this.rebateDetail = rebateDetail;
	}


	/**
	 * @return the loyaltyInfo
	 */
	public LoyaltyDetail getLoyaltyInfo()
	{
		return loyaltyInfo;
	}


	/**
	 * @param loyaltyInfo the loyaltyInfo to set
	 */
	public void setLoyaltyInfo(LoyaltyDetail loyaltyInfo)
	{
		this.loyaltyInfo = loyaltyInfo;
	}


	/**
	 * @return the loyaltyDetail
	 */
	public List<LoyaltyDetail> getLoyaltyDetail()
	{
		return loyaltyDetail;
	}


	/**
	 * @param loyaltyDetail the loyaltyDetail to set
	 */
	public void setLoyaltyDetail(List<LoyaltyDetail> loyaltyDetail)
	{
		this.loyaltyDetail = loyaltyDetail;
	}


	/**
	 * @return the couponDetail
	 */
	public List<CouponDetail> getCouponDetail()
	{
		return couponDetail;
	}


	/**
	 * @param couponDetail the couponDetail to set
	 */
	public void setCouponDetail(List<CouponDetail> couponDetail)
	{
		this.couponDetail = couponDetail;
	}


	/**
	 * @return the couponId
	 */
	public Integer getCouponId()
	{
		return couponId;
	}


	/**
	 * @param couponId the couponId to set
	 */
	public void setCouponId(Integer couponId)
	{
		this.couponId = couponId;
	}


	/**
	 * @return the rebateId
	 */
	public Integer getRebateId()
	{
		return rebateId;
	}


	/**
	 * @param rebateId the rebateId to set
	 */
	public void setRebateId(Integer rebateId)
	{
		this.rebateId = rebateId;
	}


	/**
	 * @return the loyaltyDealID
	 */
	public Integer getLoyaltyDealID()
	{
		return loyaltyDealID;
	}


	/**
	 * @param loyaltyDealID the loyaltyDealID to set
	 */
	public void setLoyaltyDealID(Integer loyaltyDealID)
	{
		this.loyaltyDealID = loyaltyDealID;
	}


	/**
	 * @return the clrType
	 */
	public String getClrType()
	{
		return clrType;
	}


	/**
	 * @param clrType the clrType to set
	 */
	public void setClrType(String clrType)
	{
		this.clrType = clrType;
	}


	/**
	 * @return the added
	 */
	public String getAdded()
	{
		return added;
	}


	/**
	 * @param added the added to set
	 */
	public void setAdded(String added)
	{
		this.added = added;
	}


	/**
	 * @return the clrImagePath
	 */
	public String getClrImagePath()
	{
		return clrImagePath;
	}


	/**
	 * @param clrImagePath the clrImagePath to set
	 */
	public void setClrImagePath(String clrImagePath)
	{
		this.clrImagePath = clrImagePath;
	}


	/**
	 * @return the couponID
	 */
	public Integer getCouponID()
	{
		return couponID;
	}


	/**
	 * @param couponID the couponID to set
	 */
	public void setCouponID(Integer couponID)
	{
		this.couponID = couponID;
	}
	
	
	
	
}

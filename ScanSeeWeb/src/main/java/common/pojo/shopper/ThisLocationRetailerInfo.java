package common.pojo.shopper;


/**
 * The ThisLocationRetailerInfo contains setter and getter methods for ThisLocationRetailerInfo fields.
 * @author Malathi
 *
 */
public class ThisLocationRetailerInfo
{
	/**
	 * The rowNumber property.
	 */

	private Integer rowNumber;

	/**
	 * The retailerId property.
	 */

	private Integer retailerId;

	/**
	 * The retailerName property.
	 */

	private String retailerName;

	/**
	 * The retailLocationID property.
	 */
	private Integer retailLocationID;
	/**
	 * Variable for distance.
	 */
	private String distance;
	
	/**
	 * Variable for retailAddress.
	 */
	private String retailAddress;
	/**
	 * The imagePath property.
	 */

	private String logoImagePath;

	/**
	 * The bannerAdImagePath property.
	 */

	private String bannerAdImagePath;
	/**
	 * The ribbonAdImagePath property.
	 */

	private String ribbonAdImagePath;
	/**
	 * @return the rowNumber
	 */
	
	/**
	 * The ribbonAdURL property.
	 */

	private String ribbonAdURL;
	
	/**
	 * for user id
	 */
	private Long userId;
	/**
	 * for retailer latitude
	 */
	private String retLat;
	/**
	 * for retailer longitude
	 */
	private String retLng;
	/**
	 * for retailer postal code
	 */
	private String postalCode;
	
	/**
	 * for retailer postal code
	 */
	private Integer lastVisitedNo;
	
	private Long saleFlag;
	/**
	 * To get ribbonAdURL.
	 * @return the ribbonAdURL
	 */
	public String getRibbonAdURL()
	{
		return ribbonAdURL;
	}
	/**
	 * To set ribbonAdURL.
	 * @param ribbonAdURL the ribbonAdURL to set
	 */
	public void setRibbonAdURL(String ribbonAdURL)
	{
		this.ribbonAdURL = ribbonAdURL;
	}
	
	/**
	 * To get rowNumber.
	 * @return rowNumber To get
	 */
	public Integer getRowNumber()
	{
		return rowNumber;
	}
	/**
	 * To set rowNumber.
	 * @param rowNumber the rowNumber to set
	 */
	public void setRowNumber(Integer rowNumber)
	{
		this.rowNumber = rowNumber;
	}
	/**
	 *  To get retailerId.
	 * @return the retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}
	/**
	 * To set retailerId.
	 * @param retailerId the retailerId to set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}
	/**
	 *  To get retailerName.
	 * @return the retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}
	/**
	 * To set retailerName.
	 * @param retailerName the retailerName to set
	 */
	public void setRetailerName(String retailerName)
	{
		this.retailerName = retailerName;
	}
	/**
	 *  To get retailLocationID.
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}
	/**
	 * To set retailLocationID.
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}
	/**
	 *  To get distance.
	 * @return the distance
	 */
	public String getDistance()
	{
		return distance;
	}
	/**
	 * To set distance.
	 * @param distance the distance to set
	 */
	public void setDistance(String distance)
	{
		this.distance = distance;
	}
	/**
	 *  To get retailAddress.
	 * @return the retailAddress
	 */
	public String getRetailAddress()
	{
		return retailAddress;
	}
	/**
	 * To set retailAddress.
	 * @param retailAddress the retailAddress to set
	 */
	public void setRetailAddress(String retailAddress)
	{
		this.retailAddress = retailAddress;
	}
	/**
	 *  To get logoImagePath.
	 * @return the logoImagePath
	 */
	public String getLogoImagePath()
	{
		return logoImagePath;
	}
	/**
	 * To set logoImagePath.
	 * @param logoImagePath the logoImagePath to set
	 */
	public void setLogoImagePath(String logoImagePath)
	{
		this.logoImagePath = logoImagePath;
	}
	/**
	 *  To get bannerAdImagePath.
	 * @return the bannerAdImagePath
	 */
	public String getBannerAdImagePath()
	{
		return bannerAdImagePath;
	}
	/**
	 * To set bannerAdImagePath.
	 * @param bannerAdImagePath the bannerAdImagePath to set
	 */
	public void setBannerAdImagePath(String bannerAdImagePath)
	{
		this.bannerAdImagePath = bannerAdImagePath;
	}
	/**
	 *  To get ribbonAdImagePath.
	 * @return the ribbonAdImagePath
	 */
	public String getRibbonAdImagePath()
	{
		return ribbonAdImagePath;
	}
	/**
	 * To set ribbonAdImagePath.
	 * @param ribbonAdImagePath the ribbonAdImagePath to set
	 */
	public void setRibbonAdImagePath(String ribbonAdImagePath)
	{
		this.ribbonAdImagePath = ribbonAdImagePath;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getRetLat() {
		return retLat;
	}
	public void setRetLat(String retLat) {
		this.retLat = retLat;
	}
	public String getRetLng() {
		return retLng;
	}
	public void setRetLng(String retLng) {
		this.retLng = retLng;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public Integer getLastVisitedNo() {
		return lastVisitedNo;
	}
	public void setLastVisitedNo(Integer lastVisitedNo) {
		this.lastVisitedNo = lastVisitedNo;
	}
	public Long getSaleFlag() {
		return saleFlag;
	}
	public void setSaleFlag(Long saleFlag) {
		this.saleFlag = saleFlag;
	}
	

}


package common.pojo.shopper;

/**
 * @author pradip_k
 *
 */
public class GoogleCategory {
	
	private String catName;
	private String catDisName;
	private String catImgPth;
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public String getCatDisName() {
		return catDisName;
	}
	public void setCatDisName(String catDisName) {
		this.catDisName = catDisName;
	}
	public String getCatImgPth() {
		return catImgPth;
	}
	public void setCatImgPth(String catImgPth) {
		this.catImgPth = catImgPth;
	}
	
	
	
	

}

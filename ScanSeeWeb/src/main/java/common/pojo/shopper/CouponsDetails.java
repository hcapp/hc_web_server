package common.pojo.shopper;

import java.util.ArrayList;
import java.util.List;

/**
 * The class has getter and setter methods for CouponsDetails.
 * 
 * @author sowjanya_d
 */
public class CouponsDetails extends BaseObject
{
	
	
	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected ArrayList<String> productNamesLst;
	
	
	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected ArrayList<ProductDetail> productLst;
	
	
	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected CouponDetails couponInfo;

	/**
	 * Variable couponDetail declared as of type List.
	 */
	protected List<CouponDetails> couponDetail;

	/**
	 * Gets the value of the couponDetail property.
	 * 
	 * @return possible object is {@link List }
	 */
	public List<CouponDetails> getCouponDetail()
	{
		return couponDetail;
	}

	/**
	 * Sets the value of the couponDetail property.
	 * 
	 * @param couponDetail
	 *            as of type List.
	 */
	public void setCouponDetail(List<CouponDetails> couponDetail)
	{
		this.couponDetail = couponDetail;
	}

	

	/**
	 * @return the couponInfo
	 */
	public CouponDetails getCouponInfo()
	{
		return couponInfo;
	}

	/**
	 * @param couponInfo the couponInfo to set
	 */
	public void setCouponInfo(CouponDetails couponInfo)
	{
		this.couponInfo = couponInfo;
	}

	public ArrayList<ProductDetail> getProductLst()
	{
		return productLst;
	}

	public void setProductLst(ArrayList<ProductDetail> productLst)
	{
		this.productLst = productLst;
	}

	/**
	 * @return the productNamesLst
	 *//*
	public ArrayList<String> getProductNamesLst()
	{
		return productNamesLst;
	}

	*//**
	 * @param productNamesLst the productNamesLst to set
	 *//*
	public void setProductNamesLst(ArrayList<String> productNamesLst)
	{
		this.productNamesLst = productNamesLst;
	}*/

}

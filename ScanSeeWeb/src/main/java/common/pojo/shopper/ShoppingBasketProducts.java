package common.pojo.shopper;


import java.util.ArrayList;

/**
 * POJO for ShoppingBasketProducts class.
 * @author shyamsundara_hm
 *
 */
public class ShoppingBasketProducts 
{
	/**
	 * The list for storing the type productDetails. 
	 * This variable used for getting request data from client.
	 */

	protected ProductDetail ProductDetail;
	
	/**
	 * Used to Display Products based on Retailers and Category.
	 */
	private ArrayList<RetailerDetail> retailerDetails;
	
	/**
	 * Used to Display Products based on Retailers and Category.
	 */
	private ArrayList<Category> categoryDetails;
	
	/** for getting productDetail.
	 * @return the productDetail
	 */
	public ProductDetail getProductDetail()
	{
		return ProductDetail;
	}

	/**
	 * for setting productDetail.
	 * @param productDetail the productDetail to set
	 */
	public void setProductDetail(ProductDetail productDetail)
	{
		ProductDetail = productDetail;
	}

	/**
	 * for getting retailerDetails.
	 * @return the retilerDetiles
	 */
	public ArrayList<RetailerDetail> getRetailerDetails()
	{
		return retailerDetails;
	}

	/**
	 * for getting retilerDetiles. 
	 * @param retilerDetiles
	 *            the retilerDetiles to set
	 */
	public void setRetailerDetails(ArrayList<RetailerDetail> retilerDetiles)
	{
		this.retailerDetails = retilerDetiles;
	}

	/**
	 * to get categoryDetails.
	 * @return the categoryDetails
	 */
	public ArrayList<Category> getCategoryDetails()
	{
		return categoryDetails;
	}

	/**
	 * to set categoryDetails.
	 * @param categoryDetails the categoryDetails to set
	 */
	public void setCategoryDetails(ArrayList<Category> categoryDetails)
	{
		this.categoryDetails = categoryDetails;
	}
}

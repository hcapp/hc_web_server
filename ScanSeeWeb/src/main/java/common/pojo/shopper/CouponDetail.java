package common.pojo.shopper;

import common.constatns.ApplicationConstants;
import common.util.Utility;

public class CouponDetail
{

	
	private Integer userCouponGalleryID;
	/**
	 * for coupon share info through twitter
	 */
	private String titleText;

	private Integer couponListID;
	
	/**
	 * for coupon share info
	 */
	private String titleText2;
	
	/**
	 * for userId
	 */
	private Integer userId;
	/**
	 * for user coupon claim type id
	 */
	private Integer userCouponClaimTypeID;
	/**
	 * for CouponPayoutMethod
	 */
	private String couponPayoutMethod;
	/**
	 * for RetailLocationID
	 */
	private Integer retailLocationID;
	/**
	 * for CouponClaimDate
	 */
	private String couponClaimDate;
	/**
	 * for coupon url
	 */
	
	private String couponURL;
	/**
	 * for coupon expired or not
	 */
	private Integer couponExpired;
	/**
	 * for product Id.
	 */
	private Integer productId;
	/**
	 * for coupon image path.
	 */
	private String imagePath;
	/**
	 * for product name.
	 */
	private String productName;
	/**
	 * for rowNum.
	 * 
	 * @return the rowNum
	 */
	private Integer rowNum;

	/**
	 * Variable couponId declared as of type Integer.
	 */
	private Integer couponId;
	/**
	 * Variable couponName declared as of type String.
	 */
	private String couponName;
	/**
	 * Variable couponDiscountType declared as of type String.
	 */
	private String couponDiscountType;
	/**
	 * Variable couponDiscountAmount declared as of type String.
	 */
	private String couponDiscountAmount;
	/**
	 * Variable couponDiscountPct declared as of type String.
	 */
	private String couponDiscountPct;
	/**
	 * Variable couponDescription declared as of type String.
	 */
	private String couponShortDescription;
	/**
	 * Variable couponDescription declared as of type String.
	 */
	private String couponLongDescription;
	/**
	 * Variable couponDateAdded declared as of type String.
	 */
	private String couponDateAdded;
	/**
	 * Variable couponStartDate declared as of type String.
	 */
	private String couponStartDate;
	/**
	 * Variable couponExpireDate declared as of type String.
	 */
	private String couponExpireDate;
	/**
	 * Variable usage declared as of type String.
	 */
	private String usage;

	/**
	 * Variable usage declared as of type String.
	 */
	private String couponImagePath;

	/**
	 * Variable added declared as of type String.
	 */
	private String added;
	
	/**
	 * for couponDescription
	 */
	private String couponDescription;

	/**
	 * @return the userCouponGalleryID
	 */
	public Integer getUserCouponGalleryID()
	{
		return userCouponGalleryID;
	}

	/**
	 * @param userCouponGalleryID the userCouponGalleryID to set
	 */
	public void setUserCouponGalleryID(Integer userCouponGalleryID)
	{
		this.userCouponGalleryID = userCouponGalleryID;
	}

	/**
	 * @return the titleText
	 */
	public String getTitleText()
	{
		return titleText;
	}

	/**
	 * @param titleText the titleText to set
	 */
	public void setTitleText(String titleText)
	{
		this.titleText = titleText;
	}

	/**
	 * @return the titleText2
	 */
	public String getTitleText2()
	{
		return titleText2;
	}

	/**
	 * @param titleText2 the titleText2 to set
	 */
	public void setTitleText2(String titleText2)
	{
		this.titleText2 = titleText2;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the userCouponClaimTypeID
	 */
	public Integer getUserCouponClaimTypeID()
	{
		return userCouponClaimTypeID;
	}

	/**
	 * @param userCouponClaimTypeID the userCouponClaimTypeID to set
	 */
	public void setUserCouponClaimTypeID(Integer userCouponClaimTypeID)
	{
		this.userCouponClaimTypeID = userCouponClaimTypeID;
	}

	/**
	 * @return the couponPayoutMethod
	 */
	public String getCouponPayoutMethod()
	{
		return couponPayoutMethod;
	}

	/**
	 * @param couponPayoutMethod the couponPayoutMethod to set
	 */
	public void setCouponPayoutMethod(String couponPayoutMethod)
	{
		this.couponPayoutMethod = couponPayoutMethod;
	}

	/**
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the couponClaimDate
	 */
	public String getCouponClaimDate()
	{
		return couponClaimDate;
	}

	/**
	 * @param couponClaimDate the couponClaimDate to set
	 */
	public void setCouponClaimDate(String couponClaimDate)
	{
		this.couponClaimDate = couponClaimDate;
	}

	/**
	 * @return the couponURL
	 */
	public String getCouponURL()
	{
		return couponURL;
	}

	/**
	 * @param couponURL the couponURL to set
	 */
	public void setCouponURL(String couponURL)
	{
		this.couponURL = couponURL;
	}

	/**
	 * @return the couponExpired
	 */
	public Integer getCouponExpired()
	{
		return couponExpired;
	}

	/**
	 * @param couponExpired the couponExpired to set
	 */
	public void setCouponExpired(Integer couponExpired)
	{
		this.couponExpired = couponExpired;
	}

	/**
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		this.imagePath = imagePath;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the rowNum
	 */
	public Integer getRowNum()
	{
		return rowNum;
	}

	/**
	 * @param rowNum the rowNum to set
	 */
	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}

	/**
	 * @return the couponId
	 */
	public Integer getCouponId()
	{
		return couponId;
	}

	/**
	 * @param couponId the couponId to set
	 */
	public void setCouponId(Integer couponId)
	{
		this.couponId = couponId;
	}

	/**
	 * @return the couponName
	 */
	public String getCouponName()
	{
		return couponName;
	}

	/**
	 * @param couponName the couponName to set
	 */
	public void setCouponName(String couponName)
	{
		this.couponName = couponName;
	}

	/**
	 * @return the couponDiscountType
	 */
	public String getCouponDiscountType()
	{
		return couponDiscountType;
	}

	/**
	 * @param couponDiscountType the couponDiscountType to set
	 */
	public void setCouponDiscountType(String couponDiscountType)
	{
		this.couponDiscountType = couponDiscountType;
	}

	/**
	 * @return the couponDiscountAmount
	 */
	public String getCouponDiscountAmount()
	{
		return couponDiscountAmount;
	}

	/**
	 * @param couponDiscountAmount the couponDiscountAmount to set
	 */
	public void setCouponDiscountAmount(String couponDiscountAmount)
	{
		if (!couponDiscountAmount.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(couponDiscountAmount))
		{
			this.couponDiscountAmount = "$" + Utility.formatDecimalValue(couponDiscountAmount);
			
		}
		else
		{
			this.couponDiscountAmount = couponDiscountAmount;
		}
	}

	/**
	 * @return the couponDiscountPct
	 */
	public String getCouponDiscountPct()
	{
		return couponDiscountPct;
	}

	/**
	 * @param couponDiscountPct the couponDiscountPct to set
	 */
	public void setCouponDiscountPct(String couponDiscountPct)
	{
		this.couponDiscountPct = couponDiscountPct;
	}

	/**
	 * @return the couponShortDescription
	 */
	public String getCouponShortDescription()
	{
		return couponShortDescription;
	}

	/**
	 * @param couponShortDescription the couponShortDescription to set
	 */
	public void setCouponShortDescription(String couponShortDescription)
	{
		this.couponShortDescription = couponShortDescription;
	}

	/**
	 * @return the couponLongDescription
	 */
	public String getCouponLongDescription()
	{
		return couponLongDescription;
	}

	/**
	 * @param couponLongDescription the couponLongDescription to set
	 */
	public void setCouponLongDescription(String couponLongDescription)
	{
		this.couponLongDescription = couponLongDescription;
	}

	/**
	 * @return the couponDateAdded
	 */
	public String getCouponDateAdded()
	{
		return couponDateAdded;
	}

	/**
	 * @param couponDateAdded the couponDateAdded to set
	 */
	public void setCouponDateAdded(String couponDateAdded)
	{
		this.couponDateAdded = couponDateAdded;
	}

	/**
	 * @return the couponStartDate
	 */
	public String getCouponStartDate()
	{
		return couponStartDate;
	}

	/**
	 * @param couponStartDate the couponStartDate to set
	 */
	public void setCouponStartDate(String couponStartDate)
	{
		if (couponStartDate != null)
		{

			this.couponStartDate = Utility.convertDBdate(couponStartDate);

		}
		else
		{
			this.couponStartDate = couponStartDate;
		}
		
	}

	/**
	 * @return the couponExpireDate
	 */
	public String getCouponExpireDate()
	{
		return couponExpireDate;
	}

	/**
	 * @param couponExpireDate the couponExpireDate to set
	 */
	public void setCouponExpireDate(String couponExpireDate)
	{
		if (couponExpireDate != null)
		{

			this.couponExpireDate = couponExpireDate;
			this.couponExpireDate = Utility.convertDBdate(couponExpireDate);

		}
		else
		{
			this.couponExpireDate = couponExpireDate;
		}
		
	}

	/**
	 * @return the usage
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * @param usage the usage to set
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}

	/**
	 * @return the couponImagePath
	 */
	public String getCouponImagePath()
	{
		return couponImagePath;
	}

	/**
	 * @param couponImagePath the couponImagePath to set
	 */
	public void setCouponImagePath(String couponImagePath)
	{
		this.couponImagePath = couponImagePath;
	}

	/**
	 * @return the added
	 */
	public String getAdded()
	{
		return added;
	}

	/**
	 * @param added the added to set
	 */
	public void setAdded(String added)
	{
		this.added = added;
	}

	public String getCouponDescription()
	{
		return couponDescription;
	}

	public void setCouponDescription(String couponDescription)
	{
		this.couponDescription = couponDescription;
	}

	public Integer getCouponListID()
	{
		return couponListID;
	}

	public void setCouponListID(Integer couponListID)
	{
		this.couponListID = couponListID;
	}

	
	
}

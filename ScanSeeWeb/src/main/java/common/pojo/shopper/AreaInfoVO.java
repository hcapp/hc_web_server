package common.pojo.shopper;
/**
 * This class is used as a pojo class for send the required data to get online retailer stores details
 * @author malathi_lr
 *
 */
public class AreaInfoVO {

	private Long userID;
	private String zipCode;
	private Integer radius;
	private Double lattitude;
	private Double longitude;
	private String screenName;
	private Integer lowerLimit;
	private Integer productId;
	private Double prefdRadius;
	
	private String cityNameTL;
	
	public Integer getLowerLimit() {
		return lowerLimit;
	}
	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	public Long getUserID() {
		return userID;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Integer getRadius() {
		return radius;
	}
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
	public Double getLattitude() {
		return lattitude;
	}
	public void setLattitude(Double lattitude) {
		this.lattitude = lattitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public Integer getProductId()
	{
		return productId;
	}
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}
	/**
	 * @return the cityNameTL
	 */
	public String getCityNameTL() {
		return cityNameTL;
	}
	/**
	 * @param cityNameTL the cityNameTL to set
	 */
	public void setCityNameTL(String cityNameTL) {
		this.cityNameTL = cityNameTL;
	}
	public Double getPrefdRadius()
	{
		return prefdRadius;
	}
	public void setPrefdRadius(Double prefdRadius)
	{
		this.prefdRadius = prefdRadius;
	}
	
}

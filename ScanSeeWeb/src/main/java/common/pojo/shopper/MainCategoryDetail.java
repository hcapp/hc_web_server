/**
 * Project     : ScanSeeWeb
 * File        : MainCategoryDetail.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 15th Feburary 2012
 */

package common.pojo.shopper;
import java.util.List;


/**
 * @author kumar_dodda
 *
 */
public class MainCategoryDetail
{
	

	
	/**
	 * The mainCategoryId declared as integer.
	 */
	private Integer  mainCategoryId;
	
	/**
	 * The mainCategoryName declared as string.
	 */
	private String mainCategoryName;
	
	/**
	 * The displayed declared as integer.
	 */
	private Integer displayed;

	/**
	 * The subCategoryDetailLst declared as List.
	 */
	private List<SubCategoryDetail> subCategoryDetailLst;
	
	/**
	 * To get mainCategoryId value.
	 * @return the mainCategoryId
	 */
	public Integer getMainCategoryId() {
		return mainCategoryId;
	}
	/**
	 * To set mainCategoryId value.
	 * @param mainCategoryId the mainCategoryId to set
	 */
	public void setMainCategoryId(Integer mainCategoryId) {
		this.mainCategoryId = mainCategoryId;
	}
	/**
	 * To get mainCategoryName.
	 * @return the mainCategoryName
	 */
	public String getMainCategoryName() {
		return mainCategoryName;
	}
	/**
	 * To set mainCategoryName.
	 * @param mainCategoryName the mainCategoryName to set
	 */
	public void setMainCategoryName(String mainCategoryName) {
		this.mainCategoryName = mainCategoryName;
	}
	/**
	 * To get subCategoryDetailLst.
	 * @return the subCategoryDetailLst
	 */
	public List<SubCategoryDetail> getSubCategoryDetailLst() {
		return this.subCategoryDetailLst;
	}
	/**
	 * To set subCategoryDetailLst.
	 * @param subCategoryDetailLst the subCategoryDetailLst to set
	 */
	public void setSubCategoryDetailLst(List<SubCategoryDetail> subCategoryDetailLst) {
		this.subCategoryDetailLst = subCategoryDetailLst;
	}
	
    /**
     * To get displayed value.
     * @return displayed -The displayed to get
     */
	public Integer getDisplayed()
	{
		return displayed;
	}
	
	/**
	 * To set displayed value.
	 * @param displayed
	 *         The displayed to set
	 */
	public void setDisplayed(Integer displayed)
	{
		this.displayed = displayed;
	}
	
}
package common.pojo.shopper;

import common.constatns.ApplicationConstants;
import common.util.Utility;

public class RebateDetail
{

	
	private Integer usedFlag;
	/**
	 * for userRebateGalleryID
	 */
	private Integer userRebateGalleryID;
	/**
	 * for userId
	 */
	private Integer userId;
	/**
	 * for RetailLocationID
	 */
	private Integer retailLocationID;
	
	/**
	 * for PurchaseAmount
	 */
	private String PurchaseAmount;
	/**
	 * for PurchaseDate
	 */
	private String purchaseDate;
	/**
	 * for ProductSerialNumber
	 */
	private String productSerialNumber;
	
	
	
	/**
	 * variable usage declared as String.
	 */
	private String usage;
	
	/**
	 * for productId.
	 */
	private Integer productId;
	/***
	 * for rebate image path
	 */
	private String imagePath;
	/**
	 * for product name
	 */
	private String productName;
	/**
	 * for rowNum.
	 * 
	 * @return the rowNum
	 */
	private Integer rowNum;

	/**
	 * The rebateName property.
	 */

	private String rebateName;
	/**
	 * The rebateId property.
	 */

	private Integer rebateId;

	
	/**
	 * The retailId; property.
	 */

	private Integer retailId;

	

	/**
	 * The rebateAmount property.
	 */

	private String rebateAmount;

	/**
	 * The rebateStartDate property.
	 */

	private String rebateStartDate;

	/**
	 * The rebateEndDate property.
	 */

	private String rebateEndDate;

	/**
	 * The usage;property.
	 */

	private String productImagePath;
	/**
	 * Variable added declared as of type String.
	 */
	private String added;

	/**
	 * The rebateShortDescription property.
	 */

	private String rebateShortDescription;


	/**
	 * for RebateURL
	 */
	
	private String rebateURL;
	
	
	/**
	 * @return the usedFlag
	 */
	public Integer getUsedFlag()
	{
		return usedFlag;
	}

	/**
	 * @param usedFlag the usedFlag to set
	 */
	public void setUsedFlag(Integer usedFlag)
	{
		this.usedFlag = usedFlag;
	}

	/**
	 * @return the userRebateGalleryID
	 */
	public Integer getUserRebateGalleryID()
	{
		return userRebateGalleryID;
	}

	/**
	 * @param userRebateGalleryID the userRebateGalleryID to set
	 */
	public void setUserRebateGalleryID(Integer userRebateGalleryID)
	{
		this.userRebateGalleryID = userRebateGalleryID;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the purchaseAmount
	 */
	public String getPurchaseAmount()
	{
		return PurchaseAmount;
	}

	/**
	 * @param purchaseAmount the purchaseAmount to set
	 */
	public void setPurchaseAmount(String purchaseAmount)
	{
		PurchaseAmount = purchaseAmount;
	}

	/**
	 * @return the purchaseDate
	 */
	public String getPurchaseDate()
	{
		return purchaseDate;
	}

	/**
	 * @param purchaseDate the purchaseDate to set
	 */
	public void setPurchaseDate(String purchaseDate)
	{
		this.purchaseDate = purchaseDate;
	}

	/**
	 * @return the productSerialNumber
	 */
	public String getProductSerialNumber()
	{
		return productSerialNumber;
	}

	/**
	 * @param productSerialNumber the productSerialNumber to set
	 */
	public void setProductSerialNumber(String productSerialNumber)
	{
		this.productSerialNumber = productSerialNumber;
	}

	/**
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		if (imagePath == null || imagePath == "" )
		{
			//this.imagePath = imagePath;
			//Changed below code as IE browser showing broken image if it did not find image
			this.imagePath = ApplicationConstants.BLANKIMAGE;
		}
		else
		{
			this.imagePath = imagePath;
		}
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the rowNum
	 */
	public Integer getRowNum()
	{
		return rowNum;
	}

	/**
	 * @param rowNum the rowNum to set
	 */
	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}

	/**
	 * @return the rebateName
	 */
	public String getRebateName()
	{
		return rebateName;
	}

	/**
	 * @param rebateName the rebateName to set
	 */
	public void setRebateName(String rebateName)
	{
		this.rebateName = rebateName;
	}

	/**
	 * @return the rebateId
	 */
	public Integer getRebateId()
	{
		return rebateId;
	}

	/**
	 * @param rebateId the rebateId to set
	 */
	public void setRebateId(Integer rebateId)
	{
		this.rebateId = rebateId;
	}

	/**
	 * @return the retailId
	 */
	public Integer getRetailId()
	{
		return retailId;
	}

	/**
	 * @param retailId the retailId to set
	 */
	public void setRetailId(Integer retailId)
	{
		this.retailId = retailId;
	}

	/**
	 * @return the rebateAmount
	 */
	public String getRebateAmount()
	{
		return rebateAmount;
	}

	/**
	 * @param rebateAmount the rebateAmount to set
	 */
	public void setRebateAmount(String rebateAmount)
	{
		if (rebateAmount == null)
		{
			this.rebateAmount = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{

			if (!rebateAmount.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(rebateAmount))
			{
				this.rebateAmount = "$" +  Utility.formatDecimalValue(rebateAmount);
			}
			else
			{
				this.rebateAmount = rebateAmount;
			}
		}
	}

	/**
	 * @return the rebateStartDate
	 */
	public String getRebateStartDate()
	{
		return rebateStartDate;
	}

	/**
	 * @param rebateStartDate the rebateStartDate to set
	 */
	public void setRebateStartDate(String rebateStartDate)
	{
		if (rebateStartDate != null)
		{
			this.rebateStartDate = Utility.convertDBdate(rebateStartDate);
		}
		else
		{
			this.rebateStartDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * @return the rebateEndDate
	 */
	public String getRebateEndDate()
	{
		return rebateEndDate;
	}

	/**
	 * @param rebateEndDate the rebateEndDate to set
	 */
	public void setRebateEndDate(String rebateEndDate)
	{
		if (rebateEndDate != null)
		{
			this.rebateEndDate = Utility.convertDBdate(rebateEndDate);
		}
		else
		{
			this.rebateEndDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath()
	{
		return productImagePath;
	}

	/**
	 * @param productImagePath the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath)
	{
			if (!Utility.checkNull(productImagePath).equals(""))
			{
				this.productImagePath = productImagePath;
			}
			else
			{
				this.productImagePath = ApplicationConstants.BLANKIMAGE;
			}
	}

	/**
	 * @return the added
	 */
	public String getAdded()
	{
		return added;
	}

	/**
	 * @param added the added to set
	 */
	public void setAdded(String added)
	{
		this.added = added;
	}

	/**
	 * @return the rebateShortDescription
	 */
	public String getRebateShortDescription()
	{
		return rebateShortDescription;
	}

	/**
	 * @param rebateShortDescription the rebateShortDescription to set
	 */
	public void setRebateShortDescription(String rebateShortDescription)
	{
		this.rebateShortDescription = rebateShortDescription;
	}

	/**
	 * @return the usage
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * @param usage the usage to set
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}

	/**
	 * @return the rebateURL
	 */
	public String getRebateURL()
	{
		return rebateURL;
	}

	/**
	 * @param rebateURL the rebateURL to set
	 */
	public void setRebateURL(String rebateURL)
	{
		this.rebateURL = rebateURL;
	}

	
	
	
}

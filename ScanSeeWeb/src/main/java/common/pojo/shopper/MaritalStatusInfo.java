package common.pojo.shopper;

/**
 * This pojo class for Marital Status Information.
 * @author sourab_r
 *
 */
public class MaritalStatusInfo
{
	/**
	 * The maritalStatusId declared as string.
	 */
	private String maritalStatusId;
	
	/**
	 * The maritalStatusName declared as string.
	 */
	private String maritalStatusName;

	/**
	 * To get maritalStatusId value.
	 * @return maritalStatusId To get
	 */
	public String getMaritalStatusId()
	{
		return maritalStatusId;
	}

	/**
	 * To set maritalStatusId value.
	 * @return maritalStatusName To get
	 */
	public String getMaritalStatusName()
	{
		return maritalStatusName;
	}

	/**
	 * To get maritalStatusId value.
	 * @param maritalStatusId To set
	 */
	public void setMaritalStatusId(String maritalStatusId)
	{

		if (null == maritalStatusId)
		{

			this.maritalStatusId = "N/A";
		}
		else
		{

			this.maritalStatusId = maritalStatusId;
		}

	}

	/**
	 * To set maritalStatusId value.
	 * @param maritalStatusName To set
	 */
	public void setMaritalStatusName(String maritalStatusName)
	{

		if (null == maritalStatusName)
		{

			this.maritalStatusName = "N/A";
		}
		else
		{

			this.maritalStatusName = maritalStatusName;
		}

	}

}

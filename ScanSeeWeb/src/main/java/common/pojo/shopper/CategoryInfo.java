package common.pojo.shopper;

/**
 * For hotdeals category information.
 * @author malathi_lr
 *
 */
public class CategoryInfo {

	/**
	 * for categoryID.
	 */
		private Integer categoryID;
		/**
		 * for parentCategoryName.
		 */
		private String categoryName;
		
		private String parentCategoryName;
		/**
		 * for parentCategoryID.
		 */
		private Integer parentCategoryID;
		
		/**
		 * for categoryID.
		 * @return the categoryID
		 */
		public Integer getCategoryID()
		{
			return categoryID;
		}
		/**
		 * To set categoryID.
		 * @param categoryID the categoryID to set
		 */
		public void setCategoryID(Integer categoryID)
		{
			this.categoryID = categoryID;
		}
		/**
		 * To get parentCategoryName.
		 * @return the parentCategoryName
		 */
		public String getParentCategoryName()
		{
			return parentCategoryName;
		}
		/**
		 * To set parentCategoryName.
		 * @param parentCategoryName the parentCategoryName to set
		 */
		public void setParentCategoryName(String parentCategoryName)
		{
			this.parentCategoryName = parentCategoryName;
		}
		/**
		 * To get parentCategoryID.
		 * @return the parentCategoryID
		 */
		public Integer getParentCategoryID()
		{
			return parentCategoryID;
		}
		/**
		 * To set parentCategoryID.
		 * @param parentCategoryID the parentCategoryID to set
		 */
		public void setParentCategoryID(Integer parentCategoryID)
		{
			this.parentCategoryID = parentCategoryID;
		}
		
		public String getCategoryName() {
			
			return categoryName;
			
		}
		
		public void setCategoryName(String categoryName) {
			
			this.categoryName = categoryName;
			
		}
		
}

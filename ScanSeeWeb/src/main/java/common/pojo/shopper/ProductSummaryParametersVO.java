package common.pojo.shopper;

public class ProductSummaryParametersVO {

	/**
	 * variable declared for productId
	 */
	private Integer retailerId;
	
	/**
	 * variable declared for retailerName
	 */
	private String retailerName;
	
	/**
	 * variable declared for retailLocationID
	 */
	private String retailLocationID;

	/**
	 * @return the retailerName
	 */
	public String getRetailerName() {
		return retailerName;
	}

	/**
	 * @return the retailLocationID
	 */
	public String getRetailLocationID() {
		return retailLocationID;
	}

	/**
	 * @return the retailerId
	 */
	public Integer getRetailerId() {
		return retailerId;
	}

	/**
	 * @param retailerId the retailerId to set
	 */
	public void setRetailerId(Integer retailerId) {
		this.retailerId = retailerId;
	}

	/**
	 * @param retailerName the retailerName to set
	 */
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	/**
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(String retailLocationID) {
		this.retailLocationID = retailLocationID;
	}
}

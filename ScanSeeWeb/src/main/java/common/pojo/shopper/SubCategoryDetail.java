/**
 * Project     : ScanSeeWeb
 * File        : SubCategoryDetail .java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 15th Feburary 2012
 */

package common.pojo.shopper;

/**
 * @author kumar_dodda
 *
 */
public class SubCategoryDetail 
{
	/**
	 * for row num
	 */
	private Integer rowNum;
	/**
	 * The subCategoryId property.
	 */

	private Integer subCategoryId;

	/**
	 * The subCategoryName property.
	 */

	private String subCategoryName;

	/**
	 * The displayed property.
	 */

	private Integer displayed;

	/**
	 * The categoryId property.
	 */

	private Integer categoryId;

	/**
	 * To get displayed.
	 * @return displayed the displayed property.
	 */

	public Integer getDisplayed()
	{
		return displayed;
	}

	/**
	 * To set displayed.
	 * @param displayed
	 *            the displayed property.
	 */
	public void setDisplayed(Integer displayed)
	{
		this.displayed = displayed;
	}

	/**
	 * To get subCategoryId.
	 * @return the subCategoryId property.
	 */
	public Integer getSubCategoryId()
	{
		return subCategoryId;
	}

	/**
	 * To set subCategoryId.
	 * @param subCategoryId
	 *            the subCategoryId to set
	 */
	public void setSubCategoryId(Integer subCategoryId)
	{
		this.subCategoryId = subCategoryId;
	}

	/**
	 * To get subCategoryName.
	 * @return the subCategoryName
	 */
	public String getSubCategoryName()
	{
		return subCategoryName;
	}

	/**
	 * To set subCategoryName.
	 * @param subCategoryName
	 *            the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName)
	{
		this.subCategoryName = subCategoryName;
	}

	/**
	 * To get categoryId.
	 * @return getCategoryId the categoryId property.
	 */
	public Integer getCategoryId()
	{
		return categoryId;
	}

	/**
	 * To set categoryId.
	 * @param categoryId
	 *            the categoryId property.
	 */

	public void setCategoryId(Integer categoryId)
	{
		this.categoryId = categoryId;

	}

	public Integer getRowNum()
	{
		return rowNum;
	}

	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}
}
/**
 * Project     : ScanSeeWeb
 * File        : HotDealsCategoryInfo.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 15th Feburary 2012
 */

package common.pojo.shopper;

import java.util.ArrayList;

import common.constatns.ApplicationConstants;

/**
 * @author kumar_dodda
 */
public class HotDealsCategoryInfo
{

	private String searchKey;

	private String popCntCity;
	/**
	 * for PopulationCenterID
	 */
	private Integer hotDealID;

	/**
	 * for PopulationCenterID
	 */
	private Integer populationCenterID;
	/**
	 * for DMAName.
	 */
	private String dMAName;

	/**
	 * Variable categoryId declared as Integer.
	 */
	private Integer categoryId;
	/**
	 * Variable categoryName declared as String.
	 */
	private String categoryName;
	/**
	 * Variable hotDealsDetailsArrayLst declared as ArrayList.
	 */

	private String favCatId;
	/**
	 * 
	 */
	private Integer recordCount;
	/**
	 * 
	 */
	private String redirectUrl;

	private Integer populationCentId;

	private String category;

	private String catID;

	public String getSearchKey()
	{
		return searchKey;
	}

	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	public String getFavCatId()
	{
		return favCatId;
	}

	public void setFavCatId(String favCatId)
	{
		this.favCatId = favCatId;
	}

	private ArrayList<HotDealsDetails> hotDealsDetailsArrayLst;

	/**
	 * Variable hotDealAPIResultSetLst declared as ArrayList.
	 */
	private ArrayList<HotDealAPIResultSet> hotDealAPIResultSetLst;

	/**
	 * To get hotDealAPIResultSetLst.
	 * 
	 * @return the hotDealAPIResultSetLst
	 */
	public ArrayList<HotDealAPIResultSet> getHotDealAPIResultSetLst()
	{
		return hotDealAPIResultSetLst;
	}

	/**
	 * To set hotDealAPIResultSetLst.
	 * 
	 * @param hotDealAPIResultSetLst
	 *            the hotDealAPIResultSetLst to set
	 */
	public void setHotDealAPIResultSetLst(ArrayList<HotDealAPIResultSet> hotDealAPIResultSetLst)
	{
		this.hotDealAPIResultSetLst = hotDealAPIResultSetLst;
	}

	// HotDealAPIResultSet hotDealAPIResultSet = null;

	/**
	 * @return the hotDealAPIResultSet
	 */
	/*
	 * public HotDealAPIResultSet getHotDealAPIResultSet() { return
	 * hotDealAPIResultSet; }
	 *//**
	 * @param hotDealAPIResultSet
	 *            the hotDealAPIResultSet to set
	 */
	/*
	 * public void setHotDealAPIResultSet(HotDealAPIResultSet
	 * hotDealAPIResultSet) { this.hotDealAPIResultSet = hotDealAPIResultSet; }
	 */

	/**
	 * Gets the value of the categoryId property.
	 * 
	 * @return the categoryId
	 */
	public Integer getCategoryId()
	{
		return categoryId;
	}

	public String getPopCntCity()
	{
		return popCntCity;
	}

	public void setPopCntCity(String popCntCity)
	{
		this.popCntCity = popCntCity;
	}

	/**
	 * Sets the value of the categoryId property.
	 * 
	 * @param categoryId
	 *            as of type Integer.
	 */
	public void setCategoryId(Integer categoryId)
	{
		this.categoryId = categoryId;
	}

	/**
	 * Gets the value of the categoryName property.
	 * 
	 * @return the categoryName
	 */
	public String getCategoryName()
	{
		return categoryName;
	}

	/**
	 * Sets the value of the categoryName property.
	 * 
	 * @param categoryName
	 *            as of type String.
	 */
	public void setCategoryName(String categoryName)
	{
		if (null == categoryName)
		{
			this.categoryName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.categoryName = categoryName;
		}
	}

	/**
	 * Gets the value of the hotDealsDetailsArrayLst property.
	 * 
	 * @return the hotDealsDetailsArrayLst
	 */
	public ArrayList<HotDealsDetails> getHotDealsDetailsArrayLst()
	{
		return hotDealsDetailsArrayLst;
	}

	/**
	 * Sets the value of the hotDealsDetailsArrayLst property.
	 * 
	 * @param hotDealsDetailsArrayLst
	 *            as of type ArrayList.
	 */
	public void setHotDealsDetailsArrayLst(ArrayList<HotDealsDetails> hotDealsDetailsArrayLst)
	{
		this.hotDealsDetailsArrayLst = hotDealsDetailsArrayLst;
	}

	/**
	 * for retrieving populationCenterID
	 * 
	 * @return populationCenterID
	 */

	public Integer getPopulationCenterID()
	{
		return populationCenterID;
	}

	/**
	 * for setting populationCenterID
	 * 
	 * @return populationCenterID
	 */
	public void setPopulationCenterID(Integer populationCenterID)
	{
		this.populationCenterID = populationCenterID;
	}

	/**
	 * for getting dMAName
	 * 
	 * @return populationCenterID
	 */
	public String getdMAName()
	{
		return dMAName;
	}

	/**
	 * for setting dMAName
	 * 
	 * @return populationCenterID
	 */
	public void setdMAName(String dMAName)
	{
		this.dMAName = dMAName;
	}

	public Integer getHotDealID()
	{
		return hotDealID;
	}

	public void setHotDealID(Integer hotDealID)
	{
		this.hotDealID = hotDealID;
	}

	/**
	 * @param recordCount
	 *            the recordCount to set
	 */
	public void setRecordCount(Integer recordCount)
	{
		this.recordCount = recordCount;
	}

	/**
	 * @return the recordCount
	 */
	public Integer getRecordCount()
	{
		return recordCount;
	}

	/**
	 * @param redirectUrl
	 *            the redirectUrl to set
	 */
	public void setRedirectUrl(String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}

	/**
	 * @return the redirectUrl
	 */
	public String getRedirectUrl()
	{
		return redirectUrl;
	}

	public Integer getPopulationCentId()
	{
		return populationCentId;
	}

	public void setPopulationCentId(Integer populationCentId)
	{
		this.populationCentId = populationCentId;
	}

	public String getCategory()
	{
		return category;
	}

	public void setCategory(String category)
	{
		this.category = category;
	}

	public String getCatID()
	{
		return catID;
	}

	public void setCatID(String catID)
	{
		this.catID = catID;
	}

}

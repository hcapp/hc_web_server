package common.pojo.shopper;

import java.util.ArrayList;
import java.util.List;
/**
 * The POJO class for HotDealsCategoryInfo.
 * 
 * @author shyamsundara_hm.
 */
public class HotDealsListResultSet
{
	
	/**
	 * for pagination
	 */
	private Integer lowerLimit;
	/**
	 * for category flag
	 */
	private Integer rowCount;
	/**
	 * for category flag
	 */
	private Integer categoryFlag;
	/**
	 * Flag for Pagination which tell whether next set of records available or not.
	 */
	private Integer nextPage;
	
	/**
	 * Variable FavCat declared as integer.
	 */
	private Integer FavCat;
	
	private Integer  totalSize;
	/**
	 * Variable hotDealsCategoryInfo declared as List.
	 */
	private List<HotDealsCategoryInfo> hotDealsCategoryInfo;
	
	
	private ArrayList<HotDealsResultSet> hotDealsListResponselst;
	/**
	 * 
	 */
	private String 	redirectUrl;
	
	/**
	 * for category list.
	 */
	private List<Category> dealCategories;
	/**
	 * Gets the value of favCat.
	 * @return the favCat
	 */
	public Integer getFavCat()
	{
		return FavCat;
	}

	/**
	 * Sets the value of favCat.
	 * @param favCat the favCat to set
	 */
	public void setFavCat(Integer favCat)
	{
		FavCat = favCat;
	}

	/**
	 * Gets the value of nextPage.
	 * @return the nextPage
	 */
	public Integer getNextPage()
	{
		return nextPage;
	}

	/**
	 * Sets the value of nextPage.
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}


	/**
	 * Gets the value of the hotDealsCategoryInfo property.
	 * 
	 * @return the hotDealsCategoryInfo
	 */
	public List<HotDealsCategoryInfo> getHotDealsCategoryInfo()
	{
		return hotDealsCategoryInfo;
	}

	/**
	 * Sets the value of the hotDealsCategoryInfo property.
	 * @param hotDealsCategoryInfo as of type List.
	 * 
	 */
	public void setHotDealsCategoryInfo(List<HotDealsCategoryInfo> hotDealsCategoryInfo)
	{
		this.hotDealsCategoryInfo = hotDealsCategoryInfo;
	}

	

	public Integer getCategoryFlag()
	{
		return categoryFlag;
	}

	public void setCategoryFlag(Integer categoryFlag)
	{
		this.categoryFlag = categoryFlag;
	}

	public Integer getRowCount()
	{
		return rowCount;
	}

	public void setRowCount(Integer rowCount)
	{
		this.rowCount = rowCount;
	}

	public Integer getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

	public ArrayList<HotDealsResultSet> getHotDealsListResponselst() {
		return hotDealsListResponselst;
	}

	public void setHotDealsListResponselst(
			ArrayList<HotDealsResultSet> hotDealsListResponselst) {
		this.hotDealsListResponselst = hotDealsListResponselst;
	}

	public Integer getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @param redirectUrl the redirectUrl to set
	 */
	public void setRedirectUrl(String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}

	/**
	 * @return the redirectUrl
	 */
	public String getRedirectUrl()
	{
		return redirectUrl;
	}

	public final List<Category> getDealCategories()
	{
		return dealCategories;
	}

	public final void setDealCategories(List<Category> dealCategories)
	{
		this.dealCategories = dealCategories;
	}

	/*
	 * private String categoryName; private Integer categoryId; private Integer
	 * lastVistedProductNo; private List<HotDealsDetails> hotDealsDetails;
	 * public String getCategoryName() { return categoryName; } public void
	 * setCategoryName(String categoryName) { this.categoryName = categoryName;
	 * } public Integer getCategoryId() { return categoryId; } public void
	 * setCategoryId(Integer categoryId) { this.categoryId = categoryId; }
	 *//**
	 * @return the hotDealsDetails
	 */
	/*
	 * public List<HotDealsDetails> getHotDealsDetails() { return
	 * hotDealsDetails; }
	 *//**
	 * @param hotDealsDetails
	 *            the hotDealsDetails to set
	 */
	/*
	 * public void setHotDealsDetails(List<HotDealsDetails> hotDealsDetails) {
	 * this.hotDealsDetails = hotDealsDetails; }
	 *//**
	 * @return the lastVistedProductNo
	 */
	/*
	 * public Integer getLastVistedProductNo() { return lastVistedProductNo; }
	 *//**
	 * @param lastVistedProductNo
	 *            the lastVistedProductNo to set
	 */
	/*
	 * public void setLastVistedProductNo(Integer lastVistedProductNo) {
	 * this.lastVistedProductNo = lastVistedProductNo; }
	 */

}

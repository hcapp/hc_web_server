package common.pojo.shopper;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * The POJO class for HotDealsDetails.
 * 
 * @author shyamsundara_hm.
 */
public class HotDealsDetails
{
	private String productName;
	/**
	 * for hot deal id
	 */
	private Integer productHotDealID;
	/**
	 * for coupon share info through twitter
	 */
	private String titleText;

	/**
	 * for coupon share info
	 */
	private String titleText2;
	/**
	 * for city
	 */
	private String city;
	/**
	 * for hot deal expired
	 */
	private Integer hotDealExpired;
	/**
	 * for api image path.
	 */
	private String apiPartnerImagePath;
	/**
	 * for rowNumber.
	 * 
	 * @return the rowNumber
	 */
	private Integer rowNumber;

	/**
	 * Variable apiPartnerId declared as Integer.
	 */
	private Integer apiPartnerId;
	/**
	 * Variable apiPartnerName declared as String.
	 */
	private String apiPartnerName;
	/**
	 * Variable hotDealName declared as String.
	 */
	private String hotDealName;
	/**
	 * Variable hotDealId declared as Integer.
	 */
	private Integer hotDealId;
	/**
	 * Variable hotDealImagePath declared as String.
	 */
	private String hotDealImagePath;
	/**
	 * Variable hDshortDescription declared as String.
	 */
	private String hDshortDescription;
	/**
	 * Variable hDLognDescription declared as String.
	 */
	private String hDLognDescription;
	/**
	 * Variable hDPrice declared as String.
	 */
	private String hDPrice;
	/**
	 * Variable hDSalePrice declared as String.
	 */
	private String hDSalePrice;
	/**
	 * Variable hDTermsConditions declared as String.
	 */
	private String hDTermsConditions;
	/**
	 * Variable hdURL declared as String.
	 */
	private String hdURL;
	/**
	 * Variable hDStartDate declared as String.
	 */
	private String hDStartDate;
	/**
	 * Variable hDEndDate declared as String.
	 */
	private String hDEndDate;
	/**
	 * Variable hDDiscountType declared as String.
	 */
	private String hDDiscountType;
	/**
	 * Variable hDDiscountAmount declared as double.
	 */
	private String hDDiscountAmount;
	/**
	 * Variable hDDiscountPct declared as double.
	 */
	private String hDDiscountPct;
	/**
	 * Variable productID declared as Integer.
	 */
	private Integer productID;

	/**
	 * The rowNumber property.
	 */

	private String distance;

	private String hdCatgoryId;

	private String hdCategoryName;

	private Integer hotdealLstId;

	private String poweredBy;

	private String retName;

	private String retAddr;

	private String state;

	private String zipcode;

	private String dmaName;

	private Integer populationCentId;

	private String hotdealCnt;

	/**
	 * To get distance.
	 * 
	 * @return the distance
	 */
	public String getDistance()
	{
		return distance;
	}

	/**
	 * To set distance.
	 * 
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(String distance)
	{
		this.distance = distance;
	}

	/**
	 * To get rowNumber.
	 * 
	 * @return rowNumber To get
	 */
	public Integer getRowNumber()
	{
		return rowNumber;
	}

	/**
	 * To set rowNumber.
	 * 
	 * @param rowNumber
	 *            the rowNumber to set
	 */
	public void setRowNumber(Integer rowNumber)
	{
		this.rowNumber = rowNumber;
	}

	/**
	 * Gets the value of the hDPrice property.
	 * 
	 * @return the hDPrice
	 */
	public String gethDPrice()
	{
		return hDPrice;
	}

	/**
	 * Sets the value of the hDPrice property.
	 * 
	 * @param hDPrice
	 *            as of type String.
	 */

	public void sethDPrice(String hDPrice)
	{

		if (null == hDPrice)
		{
			this.hDPrice = ApplicationConstants.NOTAPPLICABLE;
		}
		else if (null != hDPrice && !ApplicationConstants.NOTAPPLICABLE.equals(hDPrice) && !hDPrice.contains("$"))
		{
			this.hDPrice = "$" + Utility.formatDecimalValue(hDPrice);
		}
		else
		{
			this.hDPrice = hDPrice;
		}

	}

	/**
	 * Gets the value of the hDSalePrice property.
	 * 
	 * @return the hDSalePrice
	 */
	public String gethDSalePrice()
	{
		return hDSalePrice;
	}

	/**
	 * Sets the value of the hDSalePrice property.
	 * 
	 * @param hDSalePrice
	 *            as of type String.
	 */
	public void sethDSalePrice(String hDSalePrice)
	{

		if (null == hDSalePrice)
		{
			this.hDSalePrice = ApplicationConstants.NOTAPPLICABLE;
		}
		else if (null != hDSalePrice && !ApplicationConstants.NOTAPPLICABLE.equals(hDSalePrice) && !hDSalePrice.contains("$"))
		{
			this.hDSalePrice = "$" + Utility.formatDecimalValue(hDSalePrice);
		}
		else
		{
			this.hDSalePrice = hDSalePrice;
		}

	}

	/**
	 * Gets the value of the productID property.
	 * 
	 * @return the productID
	 */
	public Integer getProductID()
	{
		return productID;
	}

	/**
	 * Sets the value of the productID property.
	 * 
	 * @param productID
	 *            as of type String.
	 */
	public void setProductID(Integer productID)
	{
		this.productID = productID;
	}

	/**
	 * Gets the value of the hDDiscountType property.
	 * 
	 * @return the hDDiscountType
	 */
	public String gethDDiscountType()
	{
		return hDDiscountType;
	}

	/**
	 * Sets the value of the hDDiscountType property.
	 * 
	 * @param hDDiscountType
	 *            as of type String.
	 */
	public void sethDDiscountType(String hDDiscountType)
	{

		this.hDDiscountType = hDDiscountType;

	}

	/**
	 * Gets the value of the hotDealImagePath property.
	 * 
	 * @return the hotDealImagePath
	 */
	public String getHotDealImagePath()
	{
		return hotDealImagePath;
	}

	/**
	 * Sets the value of the hotDealImagePath property.
	 * 
	 * @param hotDealImagePath
	 *            as of type String.
	 */
	public void setHotDealImagePath(String hotDealImagePath)
	{
		if (hotDealImagePath == null || "".equals(hotDealImagePath))
		{
			this.hotDealImagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{

			this.hotDealImagePath = hotDealImagePath;
		}

	}

	/**
	 * Gets the value of the apiPartnerId property.
	 * 
	 * @return the apiPartnerId
	 */
	public Integer getApiPartnerId()
	{
		return apiPartnerId;
	}

	/**
	 * Sets the value of the apiPartnerId property.
	 * 
	 * @param apiPartnerId
	 *            as of type String.
	 */
	public void setApiPartnerId(Integer apiPartnerId)
	{
		this.apiPartnerId = apiPartnerId;
	}

	/**
	 * Gets the value of the apiPartnerName property.
	 * 
	 * @return the apiPartnerName
	 */
	public String getApiPartnerName()
	{
		return apiPartnerName;
	}

	/**
	 * Sets the value of the apiPartnerName property.
	 * 
	 * @param apiPartnerName
	 *            as of type String.
	 */
	public void setApiPartnerName(String apiPartnerName)
	{

		this.apiPartnerName = apiPartnerName;

	}

	/**
	 * Gets the value of the hotDealName property.
	 * 
	 * @return the hotDealName
	 */
	public String getHotDealName()
	{
		return hotDealName;
	}

	/**
	 * Sets the value of the hotDealName property.
	 * 
	 * @param hotDealName
	 *            as of type String.
	 */
	public void setHotDealName(String hotDealName)
	{

		this.hotDealName = hotDealName;

	}

	/**
	 * Gets the value of the hotDealId property.
	 * 
	 * @return the hotDealId
	 */
	public Integer getHotDealId()
	{
		return hotDealId;
	}

	/**
	 * Sets the value of the hotDealId property.
	 * 
	 * @param hotDealId
	 *            as of type String.
	 */
	public void setHotDealId(Integer hotDealId)
	{
		this.hotDealId = hotDealId;
	}

	/**
	 * Gets the value of the hDshortDescription property.
	 * 
	 * @return the hDshortDescription
	 */
	public String gethDshortDescription()
	{
		return hDshortDescription;
	}

	/**
	 * Sets the value of the hDshortDescription property.
	 * 
	 * @param hDshortDescription
	 *            as of type String.
	 */
	public void sethDshortDescription(String hDshortDescription)
	{

		if (null != hDshortDescription && !"".equals(hDshortDescription))
		{
			this.hDshortDescription = Utility.removeHTMLTags(hDshortDescription);
		}
		else
		{
			this.hDshortDescription = hDshortDescription;
		}

	}

	/**
	 * Gets the value of the hDLognDescription property.
	 * 
	 * @return the hDLognDescription
	 */
	public String gethDLognDescription()
	{
		return hDLognDescription;
	}

	/**
	 * Sets the value of the hDLognDescription property.
	 * 
	 * @param hDLognDescription
	 *            as of type String.
	 */
	public void sethDLognDescription(String hDLognDescription)
	{
		if (null != hDLognDescription && !"".equals(hDLognDescription))
		{
			this.hDLognDescription = Utility.removeHTMLTags(hDLognDescription);
		}
		else
		{
			this.hDLognDescription = hDLognDescription;
		}

	}

	/**
	 * Gets the value of the hDTermsConditions property.
	 * 
	 * @return the hDTermsConditions
	 */
	public String gethDTermsConditions()
	{
		return hDTermsConditions;
	}

	/**
	 * Sets the value of the hDTermsConditions property.
	 * 
	 * @param hDTermsConditions
	 *            as of type String.
	 */
	public void sethDTermsConditions(String hDTermsConditions)
	{

		this.hDTermsConditions = hDTermsConditions;

	}

	/**
	 * Gets the value of the hdURL property.
	 * 
	 * @return the hdURL
	 */
	public String getHdURL()
	{
		return hdURL;
	}

	/**
	 * Sets the value of the hdURL property.
	 * 
	 * @param hdURL
	 *            as of type String.
	 */
	public void setHdURL(String hdURL)
	{

		this.hdURL = hdURL;

	}

	/**
	 * Gets the value of the hDStartDate property.
	 * 
	 * @return the hDStartDate
	 */
	public String gethDStartDate()
	{
		return hDStartDate;
	}

	/**
	 * Sets the value of the hDStartDate property.
	 * 
	 * @param hDStartDate
	 *            as of type String.
	 */
	public void sethDStartDate(String hDStartDate)
	{

		this.hDStartDate = hDStartDate;

	}

	/**
	 * Gets the value of the hDEndDate property.
	 * 
	 * @return the hDEndDate
	 */
	public String gethDEndDate()
	{
		return hDEndDate;
	}

	/**
	 * Sets the value of the hDEndDate property.
	 * 
	 * @param hDEndDate
	 *            as of type String.
	 */
	public void sethDEndDate(String hDEndDate)
	{

		this.hDEndDate = hDEndDate;

	}

	/**
	 * gets the value of hDDiscountAmount.
	 * 
	 * @return the hDDiscountAmount
	 */
	public String gethDDiscountAmount()
	{
		return hDDiscountAmount;
	}

	/**
	 * sets the value of hDDiscountAmount.
	 * 
	 * @param hDDiscountAmount
	 *            the hDDiscountAmount to set
	 */
	public void sethDDiscountAmount(String hDDiscountAmount)
	{

		this.hDDiscountAmount = hDDiscountAmount;
	}

	/**
	 * gets the value of hDDiscountPct.
	 * 
	 * @return the hDDiscountPct
	 */
	public String gethDDiscountPct()
	{
		return hDDiscountPct;
	}

	/**
	 * sets the value of hDDiscountPct.
	 * 
	 * @param hDDiscountPct
	 *            the hDDiscountPct to set
	 */
	public void sethDDiscountPct(String hDDiscountPct)
	{

		this.hDDiscountPct = hDDiscountPct;

	}

	/**
	 * @return the apiPartnerImagePath
	 */
	public String getApiPartnerImagePath()
	{
		return apiPartnerImagePath;
	}

	/**
	 * @param apiPartnerImagePath
	 *            the apiPartnerImagePath to set
	 */
	public void setApiPartnerImagePath(String apiPartnerImagePath)
	{

		this.apiPartnerImagePath = apiPartnerImagePath;

	}

	public Integer getHotDealExpired()
	{
		return hotDealExpired;
	}

	public void setHotDealExpired(Integer hotDealExpired)
	{
		this.hotDealExpired = hotDealExpired;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getTitleText()
	{
		return titleText;
	}

	public void setTitleText(String titleText)
	{
		this.titleText = titleText;
	}

	public String getTitleText2()
	{
		return titleText2;
	}

	public void setTitleText2(String titleText2)
	{
		this.titleText2 = titleText2;
	}

	public Integer getProductHotDealID()
	{
		return productHotDealID;
	}

	public void setProductHotDealID(Integer productHotDealID)
	{
		this.productHotDealID = productHotDealID;
	}

	public String getHdCatgoryId()
	{
		return hdCatgoryId;
	}

	public void setHdCatgoryId(String hdCatgoryId)
	{
		this.hdCatgoryId = hdCatgoryId;
	}

	public String getHdCategoryName()
	{
		return hdCategoryName;
	}

	public void setHdCategoryName(String hdCategoryName)
	{
		this.hdCategoryName = hdCategoryName;
	}

	public Integer getHotdealLstId()
	{
		return hotdealLstId;
	}

	public void setHotdealLstId(Integer hotdealLstId)
	{
		this.hotdealLstId = hotdealLstId;
	}

	public String getPoweredBy()
	{
		return poweredBy;
	}

	public void setPoweredBy(String poweredBy)
	{
		this.poweredBy = poweredBy;
	}

	public String getRetName()
	{
		return retName;
	}

	public void setRetName(String retName)
	{
		this.retName = retName;
	}

	public String getRetAddr()
	{
		return retAddr;
	}

	public void setRetAddr(String retAddr)
	{
		this.retAddr = retAddr;
	}

	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getZipcode()
	{
		return zipcode;
	}

	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}

	public final String getDmaName()
	{
		return dmaName;
	}

	public final void setDmaName(String dmaName)
	{
		this.dmaName = dmaName;
	}

	public Integer getPopulationCentId()
	{
		return populationCentId;
	}

	public void setPopulationCentId(Integer populationCentId)
	{
		this.populationCentId = populationCentId;
	}

	public void setHotdealCnt(String hotdealCnt)
	{
		this.hotdealCnt = hotdealCnt;
	}

	public String getHotdealCnt()
	{
		return hotdealCnt;
	}

	public String getProductName()
	{
		return productName;
	}

	public void setProductName(String productName)
	{
		this.productName = productName;
	}

}

package common.pojo.shopper;

import java.util.List;

import common.pojo.RetailerInfo;

/**
 *  This class for handling Master Shopping list retailer and category list.
 * @author malathi_lr
 *
 */
public class ShoppingListRetailerCategoryList {

	/**
	 * for pagination next page
	 */
	private Integer nextPage;
	/**
	 * for list containing category list.
	 */
	private List<SLCategory> categoryInfolst;

	/**
	 * for list containing retailers and category list.
	 */
	private List<RetailerInfo> retailerInfolst;
	
	/**
	 * 
	 */
	private Integer totalSize;
	
	/**
	 * @return the totalSize
	 */
	public Integer getTotalSize() {
		return totalSize;
	}

	/**
	 * @param totalSize the totalSize to set
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}

	/**
	 * to get retailerInfolst.
	 * 
	 * @return the retailerInfolst
	 */
	public List<RetailerInfo> getRetailerInfolst()
	{
		return retailerInfolst;
	}

	/**
	 * to set retailerInfolst.
	 * 
	 * @param retailerInfolst
	 *            the retailerInfolst to set
	 */
	public void setRetailerInfolst(List<RetailerInfo> retailerInfolst)
	{
		this.retailerInfolst = retailerInfolst;
	}

	/**
	 * to get categoryInfolst.
	 * 
	 * @return the categoryInfolst
	 */
	public List<SLCategory> getCategoryInfolst()
	{
		return categoryInfolst;
	}

	/**
	 * to set categoryInfolst.
	 * 
	 * @param categoryInfolst
	 *            the categoryInfolst to set
	 */
	public void setCategoryInfolst(List<SLCategory> categoryInfolst)
	{
		this.categoryInfolst = categoryInfolst;
	}

	public Integer getNextPage()
	{
		return nextPage;
	}

	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}

}

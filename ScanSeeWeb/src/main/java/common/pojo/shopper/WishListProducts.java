/**
 * 
 */
package common.pojo.shopper;

import java.util.ArrayList;

/**
 * This pojo class contains setter and getter methods for WishListProducts.
 * @author shyamsundara_hm
 *
 */
public class WishListProducts
{	
	
	
	
	/**
	 * The alertProducts declared as AlertedProducts object.
	 */
	private AlertedProducts alertProducts;
	
	/**
	 * for wish list history details.
	 */
	private ArrayList<WishListResultSet> productInfo;
	
	/**
	 * To get alertProducts.
	 * @return the alertProducts
	 */
	public AlertedProducts getAlertProducts()
	{
		return alertProducts;
	}

	/**
	 * Gets the value of alertProducts property.
	 * @param alertProducts the alertProducts to set
	 */
	public void setAlertProducts(AlertedProducts alertProducts)
	{
		this.alertProducts = alertProducts;
	}

	/**
	 * Sets the value of alertProducts property.
	 * @return the productInfo
	 */
	public ArrayList<WishListResultSet> getProductInfo()
	{
		return productInfo;
	}

	/**
	 * Gets the value of productInfo property.
	 * @param productInfo the productInfo to set
	 */
	public void setProductInfo(ArrayList<WishListResultSet> productInfo)
	{
		this.productInfo = productInfo;
	}
	/**
	 * for total number of records.
	 */
	private Integer totalSize;

	public Integer getTotalSize()
	{
		return totalSize;
	}

	public void setTotalSize(Integer totalSize)
	{
		this.totalSize = totalSize;
		
	}
	 
	
}

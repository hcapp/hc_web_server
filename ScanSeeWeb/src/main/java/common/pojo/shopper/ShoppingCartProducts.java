package common.pojo.shopper;


import java.util.ArrayList;

/**
 * The POJO class for ShoppingCartProducts.
 * @author shyamsundara_hm
 */

public class ShoppingCartProducts 
{
	
	
	/**
	 * The list for storing the type productDetails. 
	 * This variable used for getting request data from client.
	 */

	protected ProductDetail ProductDetail;
	
	/**
	 * For product Category list.
	 */

	private ArrayList<Category> categoryDetails;
	

	/**
	 * Used to Display Products based on Retailers and Category.
	 */
	
	private ArrayList<RetailerDetail> retailerDetails;

	/**
	 * to get retilerDetiles.
	 * @return the retilerDetiles
	 */
	public ArrayList<RetailerDetail> getRetailerDetails()
	{
		return retailerDetails;
	}

	/**
	 * to set retilerDetiales.
	 * @param retilerDetiles
	 *            the retilerDetiles to set
	 */
	public void setRetailerDetails(ArrayList<RetailerDetail> retilerDetiles)
	{
		this.retailerDetails = retilerDetiles;
	}

	/**
	 * to set productDetail.
	 * @return the productDetail
	 */
	public ProductDetail getProductDetail()
	{
		return ProductDetail;
	}

	/**
	 * to set productDetail.
	 * @param productDetail the productDetail to set
	 */
	public void setProductDetail(ProductDetail productDetail)
	{
		ProductDetail = productDetail;
	}

	/**
	 * to get categoryDetails.
	 * @return the categoryDetails
	 */
	public ArrayList<Category> getCategoryDetails()
	{
		return categoryDetails;
	}

	/**
	 * to set categoryDetails.
	 * @param categoryDetails the categoryDetails to set
	 */
	public void setCategoryDetails(ArrayList<Category> categoryDetails)
	{
		this.categoryDetails = categoryDetails;
	}

}

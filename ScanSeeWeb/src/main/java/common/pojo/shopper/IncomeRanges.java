package common.pojo.shopper;

import java.util.List;

/**
 * The class has getter and setter methods for IncomeRanges.
 * 
 * @author sowjanya_d
 */
public class IncomeRanges extends BaseObject
{
	/**
	 * Variable incomeRange declared as of type List.
	 */
	private List<IncomeRange> incomeRange;

	/**
	 * Gets the value of the incomeRange property.
	 * 
	 * @return incomeRangeId object is {@link List }
	 */
	public List<IncomeRange> getIncomeRange()
	{
		return incomeRange;
	}

	/**
	 * Sets the value of the incomeRange property.
	 * 
	 * @param incomeRange
	 *            allowed object is {@link List }
	 */
	public void setIncomeRange(List<IncomeRange> incomeRange)
	{
		this.incomeRange = incomeRange;
	}
}

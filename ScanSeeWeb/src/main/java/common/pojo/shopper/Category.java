/**
 * Project     : ScanSeeWeb
 * File        : Category.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 16th Feburary 2012
 */

package common.pojo.shopper;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kumar_dodda
 *
 */
public class Category
{
	/**
	 * for product list.
	 */
	private ArrayList<ProductDetail> productDetails;
	/**
	 * Variable parentCategoryName declared as String.
	 */
	private String parentCategoryName;
	
	/**
	 * Variable categoryId declared as Integer.
	 */
	private Integer categoryId;
	/**
	 * Variable parentCategoryId declared as Integer.
	 */
	private Integer parentCategoryId;
	/**
	 * Variable subcategory declared as List.
	 */
	private List<SubCategoryDetail> subcategory;
	
	private String category;
	
	private String catID;
	/**
	 * Gets the value of the categoryId property.
	 * 
	 * @return the categoryId
	 */
	public Integer getCategoryId()
	{
		return categoryId;
	}
	/**
	 * Sets the value of the categoryId property.
	 * 
	 * @param categoryId
	 *            as of type Integer.
	 */
	public void setCategoryId(Integer categoryId)
	{
		this.categoryId = categoryId;
	}
	/**
	 * Gets the value of the subcategory property.
	 * 
	 * @return the subcategory
	 */
	public List<SubCategoryDetail> getSubcategory()
	{
		return subcategory;
	}
	/**
	 * Sets the value of the subcategory property.
	 * 
	 * @param subcategory
	 *            as of type List.
	 */
	public void setSubcategory(List<SubCategoryDetail> subcategory)
	{
		this.subcategory = subcategory;
	}
	/**
	 * Gets the value of the parentCategoryId property.
	 * 
	 * @return the parentCategoryId
	 */
	public Integer getParentCategoryId()
	{
		return parentCategoryId;
	}
	/**
	 * Sets the value of the parentCategoryId property.
	 * 
	 * @param parentCategoryId
	 *            as of type Integer.
	 */
	public void setParentCategoryId(Integer parentCategoryId)
	{
		this.parentCategoryId = parentCategoryId;
	}
	/**
	 * to get parentCategoryName.
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}
	/**
	 * to set parentCategoryName.
	 * @param parentCategoryName the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}
	/**
	 * to get productDetails.
	 * @return the productDetails
	 */
	public ArrayList<ProductDetail> getProductDetails()
	{
		return productDetails;
	}
	/**
	 * to set productDetails.
	 * @param productDetails the productDetails to set
	 */
	public void setProductDetails(ArrayList<ProductDetail> productDetails)
	{
		this.productDetails = productDetails;
	}
	public final String getCategory()
	{
		return category;
	}
	public final void setCategory(String category)
	{
		this.category = category;
	}
	public final String getCatID()
	{
		return catID;
	}
	public final void setCatID(String catID)
	{
		this.catID = catID;
	}


}
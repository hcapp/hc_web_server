package common.pojo;

import java.util.List;

public class Response 
{
    /**
     * Variable errorMessage declared as string.
     */
	private String errorMessage;
	
	/**
	 * Variable errorCode declared as Integer.
	 */
	private Integer errorCode;
	
	/**
	 * Variable responseMessage declared as List.
	 */
	private List<String> responseMessage;
	
	/**
	 * To set errorMessage.
	 * @param errorMessage
	 *        The errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	/**
	 * To get errorMessage.
	 * @return errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**
	 * To set errorCode.
	 * @param errorCode
	 *          The errorCode to set
	 */
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	/**
	 * To get errorCode.
	 * @return errorCode
	 */
	public Integer getErrorCode() {
		return errorCode;
	}
	
	/**
	 * To set responseMessage.
	 * @param responseMessage
	 *         The responseMessage to set
	 */
	public void setResponseMessage(List<String>  responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	/**
	 * To get responseMessage.
	 * @return responseMessage
	 */
	public List<String>  getResponseMessage() {
		return responseMessage;
	}
	
}

package common.pojo;

public class SearchForm
{

	private Integer lastVisitedNo;

	private Long userId;

	private String searchType;

	private String searchKey;

	private String zipCode;

	private String tlFind;

	private Integer retailerId;

	private Integer radius;

	private String url;

	private Integer productId;

	private Integer mainMenuID;

	private Integer recordCount;

	private String categoryName;

	private Integer retListID;
	/**
	 * The retailLocationID property.
	 */
	private Integer retailLocationID;
	
	private int currentPageNumber;
	
	private int locSearchRecCount;
	public Integer getProductId()
	{
		return productId;
	}

	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * @return the radius
	 */
	public Integer getRadius()
	{
		return radius;
	}

	/**
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(Integer radius)
	{
		this.radius = radius;
	}

	public SearchForm()
	{

	}

	/**
	 * @return the searchType
	 */

	public SearchForm(String zipCode, String searchKey, String searchType)
	{
		this.searchType = searchType;
		this.searchKey = searchKey;
		this.zipCode = zipCode;
	}

	public String getSearchType()
	{
		return searchType;
	}

	/**
	 * @param searchType
	 *            the searchType to set
	 */
	public void setSearchType(String searchType)
	{
		this.searchType = searchType;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode()
	{
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}

	/**
	 * @return the tlFind
	 */
	public String getTlFind()
	{
		return tlFind;
	}

	/**
	 * @param tlFind
	 *            the tlFind to set
	 */
	public void setTlFind(String tlFind)
	{
		this.tlFind = tlFind;
	}

	/**
	 * @return the retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * @param retailerId
	 *            the retailerId to set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public Integer getLastVisitedNo()
	{
		return lastVisitedNo;
	}

	public void setLastVisitedNo(Integer lastVisitedNo)
	{
		this.lastVisitedNo = lastVisitedNo;
	}

	public Integer getMainMenuID()
	{
		return mainMenuID;
	}

	public void setMainMenuID(Integer mainMenuID)
	{
		this.mainMenuID = mainMenuID;
	}

	public Integer getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(Integer recordCount)
	{
		this.recordCount = recordCount;
	}

	public String getCategoryName()
	{
		return categoryName;
	}

	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}

	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}

	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	public Integer getRetListID()
	{
		return retListID;
	}

	public void setRetListID(Integer retListID)
	{
		this.retListID = retListID;
	}

	public int getCurrentPageNumber()
	{
		return currentPageNumber;
	}

	public void setCurrentPageNumber(int currentPageNumber)
	{
		this.currentPageNumber = currentPageNumber;
	}

	public int getLocSearchRecCount()
	{
		return locSearchRecCount;
	}

	public void setLocSearchRecCount(int locSearchRecCount)
	{
		this.locSearchRecCount = locSearchRecCount;
	}


	
}

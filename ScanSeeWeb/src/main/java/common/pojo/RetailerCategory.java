/**
 * Project     : Scan See
 * File        : RetailerCategory.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;

/**
 * Bean used in RetailerCategory.
 * 
 * @author kumar_dodda
 */
public class RetailerCategory
{
	/**
	 * 
	 */
	private Long retailerCategoryID;
	/**
	 * 
	 */
	private Long retailID;
	/**
	 * 
	 */
	private Long categoryID;
	/**
	 * 
	 */
	private Long createDate;
	/**
	 * 
	 */
	private Retailer retailer;
	/**
	 * 
	 */
	private Category category;

	/**
	 * 
	 */
	public RetailerCategory()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param retailerLocationID
	 * @param retailID
	 * @param categoryID
	 * @param createDate
	 * @param retailer
	 * @param category
	 */
	public RetailerCategory(Long retailerCategoryID, Long retailID, Long categoryID, Long createDate, Retailer retailer, Category category)
	{
		super();
		this.retailerCategoryID = retailerCategoryID;
		this.retailID = retailID;
		this.categoryID = categoryID;
		this.createDate = createDate;
		this.retailer = retailer;
		this.category = category;
	}

	/**
	 * @return the retailerLocationID
	 */
	public Long getRetailerCategoryID()
	{
		return retailerCategoryID;
	}

	/**
	 * @param retailerLocationID
	 *            the retailerLocationID to set
	 */
	public void setRetailerCategoryID(Long retailerCategoryID)
	{
		this.retailerCategoryID = retailerCategoryID;
	}

	/**
	 * @return the retailID
	 */
	public Long getRetailID()
	{
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Long retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * @return the categoryID
	 */
	public Long getCategoryID()
	{
		return categoryID;
	}

	/**
	 * @param categoryID
	 *            the categoryID to set
	 */
	public void setCategoryID(Long categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * @return the createDate
	 */
	public Long getCreateDate()
	{
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(Long createDate)
	{
		this.createDate = createDate;
	}

	/**
	 * @return the retailer
	 */
	public Retailer getRetailer()
	{
		return retailer;
	}

	/**
	 * @param retailer
	 *            the retailer to set
	 */
	public void setRetailer(Retailer retailer)
	{
		this.retailer = retailer;
	}

	/**
	 * @return the category
	 */
	public Category getCategory()
	{
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(Category category)
	{
		this.category = category;
	}
}

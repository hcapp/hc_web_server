package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class RetailerUploadLogoInfo
{ 
	/*
	 * This setter method implemented for getting the Retailer Uploaded Logo. 
    */
	private CommonsMultipartFile retailerLogo;
	private Long retailerID;
	private String navigation;
	
	/**
	 * @return the retailerID
	 */
	public Long getRetailerID() {
		return retailerID;
	}

	/**
	 * @param retailerID the retailerID to set
	 */
	public void setRetailerID(Long retailerID) {
		this.retailerID = retailerID;
	}

	public CommonsMultipartFile getRetailerLogo()
	{
		return retailerLogo;
	}

	public void setRetailerLogo(CommonsMultipartFile retailerLogo)
	{
		this.retailerLogo = retailerLogo;
	}

	/**
	 * @return the navigation
	 */
	public String getNavigation()
	{
		return navigation;
	}

	/**
	 * @param navigation the navigation to set
	 */
	public void setNavigation(String navigation)
	{
		this.navigation = navigation;
	}

	
}

/**
 * Project     : Scan See
 * File        : Retailer.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;

import common.pojo.Contact;
import common.pojo.RetailerContact;

/**
 * Bean used in Retailer.
 * 
 * @author kumar_dodda
 */
public class Retailer
{
	private int retailLocationID;
	private int retailID;

	private Long retailerLocationId;
	

	private String retailName;

	public int getRetailID()
	{
		return retailID;
	}

	public void setRetailID(int retailID)
	{
		this.retailID = retailID;
	}

	public String getRetailName()
	{
		return retailName;
	}

	public void setRetailName(String retailName)
	{
		this.retailName = retailName;
	}

	public int getRetailLocationID()
	{
		return retailLocationID;
	}

	public void setRetailLocationID(int retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	

	/**
	 * @return the retailerLocationId
	 */
	public Long getRetailerLocationId()
	{
		return retailerLocationId;
	}

	/**
	 * @param retailerLocationId
	 *            the retailerLocationId to set
	 */
	public void setRetailerLocationId(Long retailerLocationId)
	{
		this.retailerLocationId = retailerLocationId;
	}

	/**
	 * 
	 */
	private Long retailerID;
	/**
	 * 
	 */
	private String retailerName;
	/**
	 * 
	 */
	private String address1;
	/**
	 * 
	 */
	private String address2;
	/**
	 * 
	 */
	private String address3;
	/**
	 * 
	 */
	private String address4;
	/**
	 * 
	 */
	private String city;
	/**
	 * 
	 */
	private String state;
	/**
	 * 
	 */
	private String postalCode;
	/**
	 * 
	 */
	private Long countryID;
	/**
	 * 
	 */
	private String retailerImagePath;
	/**
	 * 
	 */
	private Long createdUserID;
	/**
	 * 
	 */
	private String createDate;
	/**
	 * 
	 */
	private Long modifiedUserID;
	/**
	 * 
	 */
	private String modifyDate;
	/**
	 * 
	 */
	private int programParticipant;
	/**
	 * 
	 */
	//private int wishPondRetailID;
	/**
	 * 
	 */
	private int posIntegrated;
	/**
	 * 
	 */
	private String retailURL;
	/**
	 * 
	 */
	private String legalAuthorityFName;
	/**
	 * 
	 */
	private String legalAuthorityLName;
	/**
	 * 
	 */
	private String corporatePhoneNo;
	/**
     * 
     */
	private CountryCode countryCode;
	/**
     * 
     */
	private Users user;

	private String retailerLocation;

	private int retailerLocID;
	/**
	 * @return the posIntegrated
	 */
	public int getPosIntegrated()
	{
		return posIntegrated;
	}

	/**
	 * @param posIntegrated
	 *            the posIntegrated to set
	 */
	public void setPosIntegrated(int posIntegrated)
	{
		this.posIntegrated = posIntegrated;
	}

	/**
	 * @return the retailerLocation
	 */
	public String getRetailerLocation()
	{
		return retailerLocation;
	}

	/**
	 * @param retailerLocation
	 *            the retailerLocation to set
	 */
	public void setRetailerLocation(String retailerLocation)
	{
		this.retailerLocation = retailerLocation;
	}

	/**
     * 
     */
	private RetailerContact retailerContact;

	/**
     * 
     */
	private String retContactFName;
	/**
     * 
     */
	private String retContactLName;
	/**
     * 
     */
	private String retContactPhone;
	/**
     * 
     */
	private int adminFlag;
	/**
     * 
     */
	private Contact contact;
	/**
     * 
     */
	private String contactEmail;

	/**
	 * 
	 */

	public Retailer()
	{
		
	}

	/**
	 * @param retailerID
	 * @param retailerName
	 * @param address1
	 * @param address2
	 * @param address3
	 * @param address4
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param countryID
	 * @param retailerImagePath
	 * @param createdUserID
	 * @param createDate
	 * @param modifiedUserID
	 * @param modifyDate
	 * @param programParticipant
	 * @param wishPondRetailID
	 * @param posIntegrated
	 * @param retailURL
	 * @param legalAuthorityFName
	 * @param legalAuthorityLName
	 * @param corporatePhoneNo
	 * @param countryCode
	 * @param user
	 * @param retailerContact
	 * @param retContactFName
	 * @param retContactLName
	 * @param retContactPhone
	 * @param adminFlag
	 * @param contact
	 * @param contactEmail
	 */
	public Retailer(Long retailerID, String retailerName, String address1, String address2, String address3, String address4, String city,
			String state, String postalCode, Long countryID, String retailerImagePath, Long createdUserID, String createDate, Long modifiedUserID,
			String modifyDate, int programParticipant,  int posIntegrated, String retailURL, String legalAuthorityFName,
			String legalAuthorityLName, String corporatePhoneNo, CountryCode countryCode, Users user, RetailerContact retailerContact,
			String retContactFName, String retContactLName, String retContactPhone, int adminFlag, Contact contact, String contactEmail)
	{
		super();
		this.retailerID = retailerID;
		this.retailerName = retailerName;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.address4 = address4;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.countryID = countryID;
		this.retailerImagePath = retailerImagePath;
		this.createdUserID = createdUserID;
		this.createDate = createDate;
		this.modifiedUserID = modifiedUserID;
		this.modifyDate = modifyDate;
		this.programParticipant = programParticipant;
		//this.wishPondRetailID = wishPondRetailID;
		this.posIntegrated = posIntegrated;
		this.retailURL = retailURL;
		this.legalAuthorityFName = legalAuthorityFName;
		this.legalAuthorityLName = legalAuthorityLName;
		this.corporatePhoneNo = corporatePhoneNo;
		this.countryCode = countryCode;
		this.user = user;
		this.retailerContact = retailerContact;
		this.retContactFName = retContactFName;
		this.retContactLName = retContactLName;
		this.retContactPhone = retContactPhone;
		this.adminFlag = adminFlag;
		this.contact = contact;
		this.contactEmail = contactEmail;
	}

	/**
	 * @return the retailerID
	 */
	public Long getRetailerID()
	{
		return retailerID;
	}

	/**
	 * @param retailerID
	 *            the retailerID to set
	 */
	public void setRetailerID(Long retailerID)
	{
		this.retailerID = retailerID;
	}

	/**
	 * @return the retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * @param retailerName
	 *            the retailerName to set
	 */
	public void setRetailerName(String retailerName)
	{
		this.retailerName = retailerName;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1()
	{
		return address1;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2()
	{
		return address2;
	}

	/**
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}

	/**
	 * @return the address3
	 */
	public String getAddress3()
	{
		return address3;
	}

	/**
	 * @param address3
	 *            the address3 to set
	 */
	public void setAddress3(String address3)
	{
		this.address3 = address3;
	}

	/**
	 * @return the address4
	 */
	public String getAddress4()
	{
		return address4;
	}

	/**
	 * @param address4
	 *            the address4 to set
	 */
	public void setAddress4(String address4)
	{
		this.address4 = address4;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the countryID
	 */
	public Long getCountryID()
	{
		return countryID;
	}

	/**
	 * @param countryID
	 *            the countryID to set
	 */
	public void setCountryID(Long countryID)
	{
		this.countryID = countryID;
	}

	/**
	 * @return the retailerImagePath
	 */
	public String getRetailerImagePath()
	{
		return retailerImagePath;
	}

	/**
	 * @param retailerImagePath
	 *            the retailerImagePath to set
	 */
	public void setRetailerImagePath(String retailerImagePath)
	{
		this.retailerImagePath = retailerImagePath;
	}

	/**
	 * @return the createdUserID
	 */
	public Long getCreatedUserID()
	{
		return createdUserID;
	}

	/**
	 * @param createdUserID
	 *            the createdUserID to set
	 */
	public void setCreatedUserID(Long createdUserID)
	{
		this.createdUserID = createdUserID;
	}

	/**
	 * @return the createDate
	 */
	public String getCreateDate()
	{
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(String createDate)
	{
		this.createDate = createDate;
	}

	/**
	 * @return the modifiedUserID
	 */
	public Long getModifiedUserID()
	{
		return modifiedUserID;
	}

	/**
	 * @param modifiedUserID
	 *            the modifiedUserID to set
	 */
	public void setModifiedUserID(Long modifiedUserID)
	{
		this.modifiedUserID = modifiedUserID;
	}

	/**
	 * @return the modifyDate
	 */
	public String getModifyDate()
	{
		return modifyDate;
	}

	/**
	 * @param modifyDate
	 *            the modifyDate to set
	 */
	public void setModifyDate(String modifyDate)
	{
		this.modifyDate = modifyDate;
	}

	/**
	 * @return the programParticipant
	 */
	public int getProgramParticipant()
	{
		return programParticipant;
	}

	/**
	 * @param programParticipant
	 *            the programParticipant to set
	 */
	public void setProgramParticipant(int programParticipant)
	{
		this.programParticipant = programParticipant;
	}

	/**
	 * @return the wishPondRetailID
	 */
	/*public int getWishPondRetailID()
	{
		return wishPondRetailID;
	}

	*//**
	 * @param wishPondRetailID
	 *            the wishPondRetailID to set
	 *//*
	public void setWishPondRetailID(int wishPondRetailID)
	{
		this.wishPondRetailID = wishPondRetailID;
	}*/

	/**
	 * @return the pOSIntegrated
	 */
	public int getPOSIntegrated()
	{
		return posIntegrated;
	}

	/**
	 * @param pOSIntegrated
	 *            the pOSIntegrated to set
	 */
	public void setPOSIntegrated(int posIntegrated)
	{
		this.posIntegrated = posIntegrated;
	}

	/**
	 * @return the retailURL
	 */
	public String getRetailURL()
	{
		return retailURL;
	}

	/**
	 * @param retailURL
	 *            the retailURL to set
	 */
	public void setRetailURL(String retailURL)
	{
		this.retailURL = retailURL;
	}

	/**
	 * @return the legalAuthorityFName
	 */
	public String getLegalAuthorityFName()
	{
		return legalAuthorityFName;
	}

	/**
	 * @param legalAuthorityFName
	 *            the legalAuthorityFName to set
	 */
	public void setLegalAuthorityFName(String legalAuthorityFName)
	{
		this.legalAuthorityFName = legalAuthorityFName;
	}

	/**
	 * @return the legalAuthorityLName
	 */
	public String getLegalAuthorityLName()
	{
		return legalAuthorityLName;
	}

	/**
	 * @param legalAuthorityLName
	 *            the legalAuthorityLName to set
	 */
	public void setLegalAuthorityLName(String legalAuthorityLName)
	{
		this.legalAuthorityLName = legalAuthorityLName;
	}

	/**
	 * @return the corporatePhoneNo
	 */
	public String getCorporatePhoneNo()
	{
		return corporatePhoneNo;
	}

	/**
	 * @param corporatePhoneNo
	 *            the corporatePhoneNo to set
	 */
	public void setCorporatePhoneNo(String corporatePhoneNo)
	{
		this.corporatePhoneNo = corporatePhoneNo;
	}

	/**
	 * @return the countryCode
	 */
	public CountryCode getCountryCode()
	{
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(CountryCode countryCode)
	{
		this.countryCode = countryCode;
	}

	/**
	 * @return the user
	 */
	public Users getUser()
	{
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(Users user)
	{
		this.user = user;
	}

	/**
	 * @return the retailerContact
	 */
	public RetailerContact getRetailerContact()
	{
		return retailerContact;
	}

	/**
	 * @return the retContactFName
	 */
	public String getRetContactFName()
	{
		return retContactFName;
	}

	/**
	 * @param retContactFName
	 *            the retContactFName to set
	 */
	public void setRetContactFName(String retContactFName)
	{
		this.retContactFName = retContactFName;
	}

	/**
	 * @return the retContactLName
	 */
	public String getRetContactLName()
	{
		return retContactLName;
	}

	/**
	 * @param retContactLName
	 *            the retContactLName to set
	 */
	public void setRetContactLName(String retContactLName)
	{
		this.retContactLName = retContactLName;
	}

	/**
	 * @return the retContactPhone
	 */
	public String getRetContactPhone()
	{
		return retContactPhone;
	}

	/**
	 * @param retContactPhone
	 *            the retContactPhone to set
	 */
	public void setRetContactPhone(String retContactPhone)
	{
		this.retContactPhone = retContactPhone;
	}

	/**
	 * @return the adminFlag
	 */
	public int getAdminFlag()
	{
		return adminFlag;
	}

	/**
	 * @param adminFlag
	 *            the adminFlag to set
	 */
	public void setAdminFlag(int adminFlag)
	{
		this.adminFlag = adminFlag;
	}

	/**
	 * @param retailerContact
	 *            the retailerContact to set
	 */
	public void setRetailerContact(RetailerContact retailerContact)
	{
		this.retailerContact = retailerContact;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact()
	{
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	/**
	 * @return the contactPhone
	 */
	public String getContactEmail()
	{
		return contactEmail;
	}

	/**
	 * @param contactEmail
	 *            the contactEmail to set
	 */
	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = contactEmail;
	}

	public int getRetailerLocID()
	{
		return retailerLocID;
	}

	public void setRetailerLocID(int retailerLocID)
	{
		this.retailerLocID = retailerLocID;
	}
	
	
}
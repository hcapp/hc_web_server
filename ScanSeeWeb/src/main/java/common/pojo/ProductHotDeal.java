/**
 * Project     : Scan See
 * File        : ProductHotDeal.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 29th December 2011
 */
package common.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean is used in Add Hot deals Info.
 * 
 * @author kumar_dodda
 */
public class ProductHotDeal
{
	/**
	 * 
	 */
	private Long productHotDealID;
	/**
	 * 
	 */
	private Long productID;
	/**
	 * 
	 */
	private Long retailID;
	/**
	 * 
	 */
	private Long apiPartnerID;
	/**
	 * 
	 */
	private String hotDealProvider;
	/**
	 * 
	 */
	private double price;
	/**
	 * 
	 */
	private String hotDealDiscountType;
	/**
	 * 
	 */
	private double hotDealDiscountAmt;
	/**
	 * 
	 */
	private Double hotDealDiscountPct;
	/**
	 * 
	 */
	private double salePrice;
	/**
	 * 
	 */
	private String hotDealName;
	/**
	 * 
	 */
	private String hotDealShortDesc;
	/**
	 * 
	 */
	private String hotDealLongDesc;
	/**
	 * 
	 */
	private String hotDealImagePath;
	/**
	 * 
	 */
	private String hotDealTermsCondt;
	/**
	 * 
	 */
	private String hotDealURL;
	/**
	 * 
	 */
	private int expired;
	/**
	 * 
	 */
	private String hotDealStartDate;
	/**
	 * 
	 */
	private String hotDealEndDate;
	/**
	 * 
	 */
	private Long sourceID;
	/**
	 * 
	 */
	private String categoryName;
	/**
	 * 
	 */
	private String createdDate;
	/**
	 * 
	 */
	private String dateModified;
	/**
	 * 
	 */
	private Long categoryID;
	/**
	 * 
	 */
	private Category category;
	/**
	 * 
	 */
	private Product product;
	/**
	 * 
	 */
	private Retailer retailer;
	/**
	 * 
	 */
	private State state;
	/**
	 * 
	 */
	private String city;

	/**
	 * 
	 */
	private String cityState;
	/**
	 * 
	 */
	private RetailerLocation retailLocation;
	/**
	 * 
	 */
	private Long retailLocID;
	/**
	 * 
	 */
	private List arCityState = new ArrayList();
	/**
	 * 
	 */
	private String dealStartTime;
	/**
	 * 
	 */
	private String dealEndTime;
	/**
	 * 
	 */
	private String dealStartTimeHrs;
	/**
	 * 
	 */
	private String dealEndTimeHrs;
	/**
	 * 
	 */
	private String dealStartTimeMins;
	/**
	 * 
	 */
	private String dealEndTimeMins;
	/**
	 * 
	 */
	private String states;

	/**
	 * 
	 */
	public ProductHotDeal()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param productHotDealID
	 * @param productID
	 * @param retailID
	 * @param apiPartnerID
	 * @param hotDealProvider
	 * @param price
	 * @param hotDealDiscountType
	 * @param hotDealDiscountAmt
	 * @param hotDealDiscountPct
	 * @param salePrice
	 * @param hotDealName
	 * @param hotDealShortDesc
	 * @param hotDealLongDesc
	 * @param hotDealImagePath
	 * @param hotDealTermsCondt
	 * @param hotDealURL
	 * @param expired
	 * @param hotDealStartDate
	 * @param hotDealEndDate
	 * @param sourceID
	 * @param categoryName
	 * @param createdDate
	 * @param dateModified
	 * @param categoryID
	 * @param category
	 * @param product
	 * @param retailer
	 * @param state
	 * @param city
	 * @param cityState
	 * @param retailLocation
	 * @param retailLocID
	 * @param arCityState
	 * @param dealStartTime
	 * @param dealEndTime
	 * @param dealStartTimeHrs
	 * @param dealEndTimeHrs
	 * @param dealStartTimeMins
	 * @param dealEndTimeMins
	 */
	public ProductHotDeal(Long productHotDealID, Long productID, Long retailID, Long apiPartnerID, String hotDealProvider, double price,
			String hotDealDiscountType, double hotDealDiscountAmt, Double hotDealDiscountPct, double salePrice, String hotDealName,
			String hotDealShortDesc, String hotDealLongDesc, String hotDealImagePath, String hotDealTermsCondt, String hotDealURL, int expired,
			String hotDealStartDate, String hotDealEndDate, Long sourceID, String categoryName, String createdDate, String dateModified,
			Long categoryID, Category category, Product product, Retailer retailer, State state, String city, String cityState,
			RetailerLocation retailLocation, Long retailLocID, List arCityState, String dealStartTime, String dealEndTime, String dealStartTimeHrs,
			String dealEndTimeHrs, String dealStartTimeMins, String dealEndTimeMins)
	{
		super();
		this.productHotDealID = productHotDealID;
		this.productID = productID;
		this.retailID = retailID;
		this.apiPartnerID = apiPartnerID;
		this.hotDealProvider = hotDealProvider;
		this.price = price;
		this.hotDealDiscountType = hotDealDiscountType;
		this.hotDealDiscountAmt = hotDealDiscountAmt;
		this.hotDealDiscountPct = hotDealDiscountPct;
		this.salePrice = salePrice;
		this.hotDealName = hotDealName;
		this.hotDealShortDesc = hotDealShortDesc;
		this.hotDealLongDesc = hotDealLongDesc;
		this.hotDealImagePath = hotDealImagePath;
		this.hotDealTermsCondt = hotDealTermsCondt;
		this.hotDealURL = hotDealURL;
		this.expired = expired;
		this.hotDealStartDate = hotDealStartDate;
		this.hotDealEndDate = hotDealEndDate;
		this.sourceID = sourceID;
		this.categoryName = categoryName;
		this.createdDate = createdDate;
		this.dateModified = dateModified;
		this.categoryID = categoryID;
		this.category = category;
		this.product = product;
		this.retailer = retailer;
		this.state = state;
		this.city = city;
		this.cityState = cityState;
		this.retailLocation = retailLocation;
		this.retailLocID = retailLocID;
		this.arCityState = arCityState;
		this.dealStartTime = dealStartTime;
		this.dealEndTime = dealEndTime;
		this.dealStartTimeHrs = dealStartTimeHrs;
		this.dealEndTimeHrs = dealEndTimeHrs;
		this.dealStartTimeMins = dealStartTimeMins;
		this.dealEndTimeMins = dealEndTimeMins;
	}

	/**
	 * @return the productHotDealID
	 */
	public final Long getProductHotDealID()
	{
		return productHotDealID;
	}

	/**
	 * @param productHotDealID
	 *            the productHotDealID to set
	 */
	public final void setProductHotDealID(Long productHotDealID)
	{
		this.productHotDealID = productHotDealID;
	}

	/**
	 * @return the productID
	 */
	public final Long getProductID()
	{
		return productID;
	}

	/**
	 * @param productID
	 *            the productID to set
	 */
	public final void setProductID(Long productID)
	{
		this.productID = productID;
	}

	/**
	 * @return the retailID
	 */
	public final Long getRetailID()
	{
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public final void setRetailID(Long retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * @return the apiPartnerID
	 */
	public final Long getApiPartnerID()
	{
		return apiPartnerID;
	}

	/**
	 * @param apiPartnerID
	 *            the apiPartnerID to set
	 */
	public final void setApiPartnerID(Long apiPartnerID)
	{
		this.apiPartnerID = apiPartnerID;
	}

	/**
	 * @return the hotDealProvider
	 */
	public final String getHotDealProvider()
	{
		return hotDealProvider;
	}

	/**
	 * @param hotDealProvider
	 *            the hotDealProvider to set
	 */
	public final void setHotDealProvider(String hotDealProvider)
	{
		this.hotDealProvider = hotDealProvider;
	}

	/**
	 * @return the price
	 */
	public final double getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public final void setPrice(double price)
	{
		this.price = price;
	}

	/**
	 * @return the hotDealDiscountType
	 */
	public final String getHotDealDiscountType()
	{
		return hotDealDiscountType;
	}

	/**
	 * @param hotDealDiscountType
	 *            the hotDealDiscountType to set
	 */
	public final void setHotDealDiscountType(String hotDealDiscountType)
	{
		this.hotDealDiscountType = hotDealDiscountType;
	}

	/**
	 * @return the hotDealDiscountAmt
	 */
	public final double getHotDealDiscountAmt()
	{
		return hotDealDiscountAmt;
	}

	/**
	 * @param hotDealDiscountAmt
	 *            the hotDealDiscountAmt to set
	 */
	public final void setHotDealDiscountAmt(double hotDealDiscountAmt)
	{
		this.hotDealDiscountAmt = hotDealDiscountAmt;
	}

	/**
	 * @return the hotDealDiscountPct
	 */
	public final Double getHotDealDiscountPct()
	{
		return hotDealDiscountPct;
	}

	/**
	 * @param hotDealDiscountPct
	 *            the hotDealDiscountPct to set
	 */
	public final void setHotDealDiscountPct(Double hotDealDiscountPct)
	{
		this.hotDealDiscountPct = hotDealDiscountPct;
	}

	/**
	 * @return the salePrice
	 */
	public final double getSalePrice()
	{
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public final void setSalePrice(double salePrice)
	{
		this.salePrice = salePrice;
	}

	/**
	 * @return the hotDealName
	 */
	public final String getHotDealName()
	{
		return hotDealName;
	}

	/**
	 * @param hotDealName
	 *            the hotDealName to set
	 */
	public final void setHotDealName(String hotDealName)
	{
		this.hotDealName = hotDealName;
	}

	/**
	 * @return the hotDealShortDesc
	 */
	public final String getHotDealShortDesc()
	{
		return hotDealShortDesc;
	}

	/**
	 * @param hotDealShortDesc
	 *            the hotDealShortDesc to set
	 */
	public final void setHotDealShortDesc(String hotDealShortDesc)
	{
		this.hotDealShortDesc = hotDealShortDesc;
	}

	/**
	 * @return the hotDealLongDesc
	 */
	public final String getHotDealLongDesc()
	{
		return hotDealLongDesc;
	}

	/**
	 * @param hotDealLongDesc
	 *            the hotDealLongDesc to set
	 */
	public final void setHotDealLongDesc(String hotDealLongDesc)
	{
		this.hotDealLongDesc = hotDealLongDesc;
	}

	/**
	 * @return the hotDealImagePath
	 */
	public final String getHotDealImagePath()
	{
		return hotDealImagePath;
	}

	/**
	 * @param hotDealImagePath
	 *            the hotDealImagePath to set
	 */
	public final void setHotDealImagePath(String hotDealImagePath)
	{
		this.hotDealImagePath = hotDealImagePath;
	}

	/**
	 * @return the hotDealTermsCondt
	 */
	public final String getHotDealTermsCondt()
	{
		return hotDealTermsCondt;
	}

	/**
	 * @param hotDealTermsCondt
	 *            the hotDealTermsCondt to set
	 */
	public final void setHotDealTermsCondt(String hotDealTermsCondt)
	{
		this.hotDealTermsCondt = hotDealTermsCondt;
	}

	/**
	 * @return the hotDealURL
	 */
	public final String getHotDealURL()
	{
		return hotDealURL;
	}

	/**
	 * @param hotDealURL
	 *            the hotDealURL to set
	 */
	public final void setHotDealURL(String hotDealURL)
	{
		this.hotDealURL = hotDealURL;
	}

	/**
	 * @return the expired
	 */
	public final int getExpired()
	{
		return expired;
	}

	/**
	 * @param expired
	 *            the expired to set
	 */
	public final void setExpired(int expired)
	{
		this.expired = expired;
	}

	/**
	 * @return the hotDealStartDate
	 */
	public final String getHotDealStartDate()
	{
		return hotDealStartDate;
	}

	/**
	 * @param hotDealStartDate
	 *            the hotDealStartDate to set
	 */
	public final void setHotDealStartDate(String hotDealStartDate)
	{
		this.hotDealStartDate = hotDealStartDate;
	}

	/**
	 * @return the hotDealEndDate
	 */
	public final String getHotDealEndDate()
	{
		return hotDealEndDate;
	}

	/**
	 * @param hotDealEndDate
	 *            the hotDealEndDate to set
	 */
	public final void setHotDealEndDate(String hotDealEndDate)
	{
		this.hotDealEndDate = hotDealEndDate;
	}

	/**
	 * @return the sourceID
	 */
	public final Long getSourceID()
	{
		return sourceID;
	}

	/**
	 * @param sourceID
	 *            the sourceID to set
	 */
	public final void setSourceID(Long sourceID)
	{
		this.sourceID = sourceID;
	}

	/**
	 * @return the categoryName
	 */
	public final String getCategoryName()
	{
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public final void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	/**
	 * @return the createdDate
	 */
	public final String getCreatedDate()
	{
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public final void setCreatedDate(String createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * @return the dateModified
	 */
	public final String getDateModified()
	{
		return dateModified;
	}

	/**
	 * @param dateModified
	 *            the dateModified to set
	 */
	public final void setDateModified(String dateModified)
	{
		this.dateModified = dateModified;
	}

	/**
	 * @return the categoryID
	 */
	public final Long getCategoryID()
	{
		return categoryID;
	}

	/**
	 * @param categoryID
	 *            the categoryID to set
	 */
	public final void setCategoryID(Long categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * @return the category
	 */
	public final Category getCategory()
	{
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public final void setCategory(Category category)
	{
		this.category = category;
	}

	/**
	 * @return the product
	 */
	public final Product getProduct()
	{
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public final void setProduct(Product product)
	{
		this.product = product;
	}

	/**
	 * @return the retailer
	 */
	public final Retailer getRetailer()
	{
		return retailer;
	}

	/**
	 * @param retailer
	 *            the retailer to set
	 */
	public final void setRetailer(Retailer retailer)
	{
		this.retailer = retailer;
	}

	/**
	 * @return the state
	 */
	public final State getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public final void setState(State state)
	{
		this.state = state;
	}

	/**
	 * @return the city
	 */
	public final String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public final void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the cityState
	 */
	public final String getCityState()
	{
		return cityState;
	}

	/**
	 * @param cityState
	 *            the cityState to set
	 */
	public final void setCityState(String cityState)
	{
		this.cityState = cityState;
	}

	/**
	 * @return the retailLocation
	 */
	public final RetailerLocation getRetailLocation()
	{
		return retailLocation;
	}

	/**
	 * @param retailLocation
	 *            the retailLocation to set
	 */
	public final void setRetailLocation(RetailerLocation retailLocation)
	{
		this.retailLocation = retailLocation;
	}

	/**
	 * @return the retailLocID
	 */
	public final Long getRetailLocID()
	{
		return retailLocID;
	}

	/**
	 * @param retailLocID
	 *            the retailLocID to set
	 */
	public final void setRetailLocID(Long retailLocID)
	{
		this.retailLocID = retailLocID;
	}

	/**
	 * @return the arCityState
	 */
	public final List getArCityState()
	{
		return arCityState;
	}

	/**
	 * @param arCityState
	 *            the arCityState to set
	 */
	public final void setArCityState(List arCityState)
	{
		this.arCityState = arCityState;
	}

	/**
	 * @return the dealStartTime
	 */
	public final String getDealStartTime()
	{
		return dealStartTime;
	}

	/**
	 * @param dealStartTime
	 *            the dealStartTime to set
	 */
	public final void setDealStartTime(String dealStartTime)
	{
		this.dealStartTime = dealStartTime;
	}

	/**
	 * @return the dealEndTime
	 */
	public final String getDealEndTime()
	{
		return dealEndTime;
	}

	/**
	 * @param dealEndTime
	 *            the dealEndTime to set
	 */
	public final void setDealEndTime(String dealEndTime)
	{
		this.dealEndTime = dealEndTime;
	}

	/**
	 * @return the dealStartTimeHrs
	 */
	public final String getDealStartTimeHrs()
	{
		return dealStartTimeHrs;
	}

	/**
	 * @param dealStartTimeHrs
	 *            the dealStartTimeHrs to set
	 */
	public final void setDealStartTimeHrs(String dealStartTimeHrs)
	{
		this.dealStartTimeHrs = dealStartTimeHrs;
	}

	/**
	 * @return the dealEndTimeHrs
	 */
	public final String getDealEndTimeHrs()
	{
		return dealEndTimeHrs;
	}

	/**
	 * @param dealEndTimeHrs
	 *            the dealEndTimeHrs to set
	 */
	public final void setDealEndTimeHrs(String dealEndTimeHrs)
	{
		this.dealEndTimeHrs = dealEndTimeHrs;
	}

	/**
	 * @return the dealStartTimeMins
	 */
	public final String getDealStartTimeMins()
	{
		return dealStartTimeMins;
	}

	/**
	 * @param dealStartTimeMins
	 *            the dealStartTimeMins to set
	 */
	public final void setDealStartTimeMins(String dealStartTimeMins)
	{
		this.dealStartTimeMins = dealStartTimeMins;
	}

	/**
	 * @return the dealEndTimeMins
	 */
	public final String getDealEndTimeMins()
	{
		return dealEndTimeMins;
	}

	/**
	 * @param dealEndTimeMins
	 *            the dealEndTimeMins to set
	 */
	public final void setDealEndTimeMins(String dealEndTimeMins)
	{
		this.dealEndTimeMins = dealEndTimeMins;
	}
	/**
	 * @return the states
	 */
	public final String getStates()
	{
		return states;
	}

	/**
	 * @param states the states to set
	 */
	public final void setStates(String states)
	{
		this.states = states;
	}
}

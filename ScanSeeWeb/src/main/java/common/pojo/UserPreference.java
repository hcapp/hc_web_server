/**
 * Project     : Scan See
 * File        : UserPreference.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 30th December 2011
 */
package common.pojo;

import common.pojo.University;

/**
 * Bean is used in Shopper UserPreference details.
 * @author kumar_dodda
 */
public class UserPreference
{
	/**
	 * 
	 */
	private Long userPreferenceID;
	/**
	 * 
	 */
	private Long localeID;
	/**
	 * 
	 */
	private Long userID;
	/**
	 * 
	 */
	private Long localeRadius;
	/**
	 * 
	 */
	private int scannerSilent;
	/**
	 * 
	 */
	private int displayCoupons;
	/**
	 * 
	 */
	private int displayRebates;
	/**
	 * 
	 */
	private int displayLoyaltyRewards;
	/**
	 * 
	 */
	private int displayFieldAgent;
	/**
	 * 
	 */
	private int fieldAgentRadius;
	/**
	 * 
	 */
	private int savingsActivated;
	/**
	 * 
	 */
	private int sleepStatus;
	/**
	 * 
	 */
	private String sleepActivationDate;
	/**
	 * 
	 */
	private String dateModified;
	/**
	 * 
	 */
	private int updateFBWishList;
	/**
	 * 
	 */
	private int addPurchaseFBTimeline;
	/**
	 * 
	 */
	private int monthlySummyFBDonatnPower;
	/**
	 * 
	 */
	private int updateTwitterWishList;
	/**
	 * 
	 */
	private int receiveShopListSummy;
	/**
	 * 
	 */
	private int receiveWishListSummy;
	/**
	 * 
	 */
	private int receiveFavoritesSummy;
	/**
	 * 
	 */
	private int receiveDonationPowerSummy;
	/**
	 * 
	 */
	private String collegeName;
	/**
	 * 
	 */
	private int emailAlert;
	/**
	 * emailStatus for storing the String value yes or No based on emailAlert  one or Zero respectively
	 */
	private String emailStatus;
	
	
	private int cellPhoneAlert;
	/**
	 * cellPhoneStatus for storing the String value yes or No based on cellPhoneAlert  one or Zero respectively
	 */
	
	private String cellPhoneStatus;
	
	
	
	private Users user;
	/**
	 * 
	 */
	private Category category;
	/**
	 * 
	 */
	private Long categoryID;
	/**
	 * 
	 */
	private University university;
	/**
	 * 
	 */
	private NonProfitPartner nonProfitPartner;
	/**
	 * 
	 */
	private Long universityID;
	/**
	 * 
	 */
	private Long NonProfitPartnerIDs;
	/**
	 * 
	 */
	private String firstName;
	/**
	 * 
	 */
	private String lastName;
	/**
	 * 
	 */
	private String address1;
	/**
	 * 
	 */
	private String address2;
	/**
	 * 
	 */
	private String state;
	/**
	 * 
	 */
	private String stateHidden;
	/**
	 * 
	 */
	private String stateCodeHidden;
	/**
	 * 
	 */
	private String city;
	/**
	 * 
	 */
	private String postalCode;
	/**
	 * 
	 */
	private String mobilePhone;
	/**
	 * 
	 */
	private String parentCategoryName;
	/**
	 * 
	 */
	private String universityName;
	/**
	 * 
	 */
	private String nonProfitPartnerName; 
	/**
	 * 
	 */
	public UserPreference()
	{
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param userPreferenceID
	 * @param localeID
	 * @param userID
	 * @param localeRadius
	 * @param scannerSilent
	 * @param displayCoupons
	 * @param displayRebates
	 * @param displayLoyaltyRewards
	 * @param displayFieldAgent
	 * @param fieldAgentRadius
	 * @param savingsActivated
	 * @param sleepStatus
	 * @param sleepActivationDate
	 * @param dateModified
	 * @param updateFBWishList
	 * @param addPurchaseFBTimeline
	 * @param monthlySummyFBDonatnPower
	 * @param updateTwitterWishList
	 * @param receiveShopListSummy
	 * @param receiveWishListSummy
	 * @param receiveFavoritesSummy
	 * @param receiveDonationPowerSummy
	 * @param collegeName
	 * @param emailAlert
	 * @param cellPhoneAlert
	 * @param user
	 * @param category
	 * @param categoryID
	 * @param university
	 * @param nonProfitPartner
	 * @param universityID
	 * @param nonProfitPartnerIDs
	 * @param firstName
	 * @param lastName
	 * @param address1
	 * @param address2
	 * @param state
	 * @param city
	 * @param postalCode
	 * @param mobilePhone
	 * @param parentCategoryName
	 * @param universityName
	 * @param nonProfitPartnerName
	 */
	public UserPreference(Long userPreferenceID, Long localeID, Long userID, Long localeRadius, int scannerSilent, int displayCoupons,
			int displayRebates, int displayLoyaltyRewards, int displayFieldAgent, int fieldAgentRadius, int savingsActivated, int sleepStatus,
			String sleepActivationDate, String dateModified, int updateFBWishList, int addPurchaseFBTimeline, int monthlySummyFBDonatnPower,
			int updateTwitterWishList, int receiveShopListSummy, int receiveWishListSummy, int receiveFavoritesSummy, int receiveDonationPowerSummy,
			String collegeName, int emailAlert, int cellPhoneAlert, Users user, Category category, Long categoryID, University university,
			NonProfitPartner nonProfitPartner, Long universityID, Long nonProfitPartnerIDs, String firstName, String lastName, String address1,
			String address2, String state, String city, String postalCode, String mobilePhone, String parentCategoryName, String universityName,
			String nonProfitPartnerName)
	{
		super();
		this.userPreferenceID = userPreferenceID;
		this.localeID = localeID;
		this.userID = userID;
		this.localeRadius = localeRadius;
		this.scannerSilent = scannerSilent;
		this.displayCoupons = displayCoupons;
		this.displayRebates = displayRebates;
		this.displayLoyaltyRewards = displayLoyaltyRewards;
		this.displayFieldAgent = displayFieldAgent;
		this.fieldAgentRadius = fieldAgentRadius;
		this.savingsActivated = savingsActivated;
		this.sleepStatus = sleepStatus;
		this.sleepActivationDate = sleepActivationDate;
		this.dateModified = dateModified;
		this.updateFBWishList = updateFBWishList;
		this.addPurchaseFBTimeline = addPurchaseFBTimeline;
		this.monthlySummyFBDonatnPower = monthlySummyFBDonatnPower;
		this.updateTwitterWishList = updateTwitterWishList;
		this.receiveShopListSummy = receiveShopListSummy;
		this.receiveWishListSummy = receiveWishListSummy;
		this.receiveFavoritesSummy = receiveFavoritesSummy;
		this.receiveDonationPowerSummy = receiveDonationPowerSummy;
		this.collegeName = collegeName;
		this.emailAlert = emailAlert;
		this.cellPhoneAlert = cellPhoneAlert;
		this.user = user;
		this.category = category;
		this.categoryID = categoryID;
		this.university = university;
		this.nonProfitPartner = nonProfitPartner;
		this.universityID = universityID;
		NonProfitPartnerIDs = nonProfitPartnerIDs;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address1 = address1;
		this.address2 = address2;
		this.state = state;
		this.city = city;
		this.postalCode = postalCode;
		this.mobilePhone = mobilePhone;
		this.parentCategoryName = parentCategoryName;
		this.universityName = universityName;
		this.nonProfitPartnerName = nonProfitPartnerName;
	}
	/**
	 * @return the userPreferenceID
	 */
	public Long getUserPreferenceID()
	{
		return userPreferenceID;
	}
	/**
	 * @param userPreferenceID the userPreferenceID to set
	 */
	public void setUserPreferenceID(Long userPreferenceID)
	{
		this.userPreferenceID = userPreferenceID;
	}
	/**
	 * @return the localeID
	 */
	public Long getLocaleID()
	{
		return localeID;
	}
	/**
	 * @param localeID the localeID to set
	 */
	public void setLocaleID(Long localeID)
	{
		this.localeID = localeID;
	}
	/**
	 * @return the userID
	 */
	public Long getUserID()
	{
		return userID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Long userID)
	{
		this.userID = userID;
	}
	/**
	 * @return the localeRadius
	 */
	public Long getLocaleRadius()
	{
		return localeRadius;
	}
	/**
	 * @param localeRadius the localeRadius to set
	 */
	public void setLocaleRadius(Long localeRadius)
	{
		this.localeRadius = localeRadius;
	}
	/**
	 * @return the scannerSilent
	 */
	public int getScannerSilent()
	{
		return scannerSilent;
	}
	/**
	 * @param scannerSilent the scannerSilent to set
	 */
	public void setScannerSilent(int scannerSilent)
	{
		this.scannerSilent = scannerSilent;
	}
	/**
	 * @return the displayCoupons
	 */
	public int getDisplayCoupons()
	{
		return displayCoupons;
	}
	/**
	 * @param displayCoupons the displayCoupons to set
	 */
	public void setDisplayCoupons(int displayCoupons)
	{
		this.displayCoupons = displayCoupons;
	}
	/**
	 * @return the displayRebates
	 */
	public int getDisplayRebates()
	{
		return displayRebates;
	}
	/**
	 * @param displayRebates the displayRebates to set
	 */
	public void setDisplayRebates(int displayRebates)
	{
		this.displayRebates = displayRebates;
	}
	/**
	 * @return the displayLoyaltyRewards
	 */
	public int getDisplayLoyaltyRewards()
	{
		return displayLoyaltyRewards;
	}
	/**
	 * @param displayLoyaltyRewards the displayLoyaltyRewards to set
	 */
	public void setDisplayLoyaltyRewards(int displayLoyaltyRewards)
	{
		this.displayLoyaltyRewards = displayLoyaltyRewards;
	}
	/**
	 * @return the displayFieldAgent
	 */
	public int getDisplayFieldAgent()
	{
		return displayFieldAgent;
	}
	/**
	 * @param displayFieldAgent the displayFieldAgent to set
	 */
	public void setDisplayFieldAgent(int displayFieldAgent)
	{
		this.displayFieldAgent = displayFieldAgent;
	}
	/**
	 * @return the fieldAgentRadius
	 */
	public int getFieldAgentRadius()
	{
		return fieldAgentRadius;
	}
	/**
	 * @param fieldAgentRadius the fieldAgentRadius to set
	 */
	public void setFieldAgentRadius(int fieldAgentRadius)
	{
		this.fieldAgentRadius = fieldAgentRadius;
	}
	/**
	 * @return the savingsActivated
	 */
	public int getSavingsActivated()
	{
		return savingsActivated;
	}
	/**
	 * @param savingsActivated the savingsActivated to set
	 */
	public void setSavingsActivated(int savingsActivated)
	{
		this.savingsActivated = savingsActivated;
	}
	/**
	 * @return the sleepStatus
	 */
	public int getSleepStatus()
	{
		return sleepStatus;
	}
	/**
	 * @param sleepStatus the sleepStatus to set
	 */
	public void setSleepStatus(int sleepStatus)
	{
		this.sleepStatus = sleepStatus;
	}
	/**
	 * @return the sleepActivationDate
	 */
	public String getSleepActivationDate()
	{
		return sleepActivationDate;
	}
	/**
	 * @param sleepActivationDate the sleepActivationDate to set
	 */
	public void setSleepActivationDate(String sleepActivationDate)
	{
		this.sleepActivationDate = sleepActivationDate;
	}
	/**
	 * @return the dateModified
	 */
	public String getDateModified()
	{
		return dateModified;
	}
	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(String dateModified)
	{
		this.dateModified = dateModified;
	}
	/**
	 * @return the updateFBWishList
	 */
	public int getUpdateFBWishList()
	{
		return updateFBWishList;
	}
	/**
	 * @param updateFBWishList the updateFBWishList to set
	 */
	public void setUpdateFBWishList(int updateFBWishList)
	{
		this.updateFBWishList = updateFBWishList;
	}
	/**
	 * @return the addPurchaseFBTimeline
	 */
	public int getAddPurchaseFBTimeline()
	{
		return addPurchaseFBTimeline;
	}
	/**
	 * @param addPurchaseFBTimeline the addPurchaseFBTimeline to set
	 */
	public void setAddPurchaseFBTimeline(int addPurchaseFBTimeline)
	{
		this.addPurchaseFBTimeline = addPurchaseFBTimeline;
	}
	/**
	 * @return the monthlySummyFBDonatnPower
	 */
	public int getMonthlySummyFBDonatnPower()
	{
		return monthlySummyFBDonatnPower;
	}
	/**
	 * @param monthlySummyFBDonatnPower the monthlySummyFBDonatnPower to set
	 */
	public void setMonthlySummyFBDonatnPower(int monthlySummyFBDonatnPower)
	{
		this.monthlySummyFBDonatnPower = monthlySummyFBDonatnPower;
	}
	/**
	 * @return the updateTwitterWishList
	 */
	public int getUpdateTwitterWishList()
	{
		return updateTwitterWishList;
	}
	/**
	 * @param updateTwitterWishList the updateTwitterWishList to set
	 */
	public void setUpdateTwitterWishList(int updateTwitterWishList)
	{
		this.updateTwitterWishList = updateTwitterWishList;
	}
	/**
	 * @return the receiveShopListSummy
	 */
	public int getReceiveShopListSummy()
	{
		return receiveShopListSummy;
	}
	/**
	 * @param receiveShopListSummy the receiveShopListSummy to set
	 */
	public void setReceiveShopListSummy(int receiveShopListSummy)
	{
		this.receiveShopListSummy = receiveShopListSummy;
	}
	/**
	 * @return the receiveWishListSummy
	 */
	public int getReceiveWishListSummy()
	{
		return receiveWishListSummy;
	}
	/**
	 * @param receiveWishListSummy the receiveWishListSummy to set
	 */
	public void setReceiveWishListSummy(int receiveWishListSummy)
	{
		this.receiveWishListSummy = receiveWishListSummy;
	}
	/**
	 * @return the receiveFavoritesSummy
	 */
	public int getReceiveFavoritesSummy()
	{
		return receiveFavoritesSummy;
	}
	/**
	 * @param receiveFavoritesSummy the receiveFavoritesSummy to set
	 */
	public void setReceiveFavoritesSummy(int receiveFavoritesSummy)
	{
		this.receiveFavoritesSummy = receiveFavoritesSummy;
	}
	/**
	 * @return the receiveDonationPowerSummy
	 */
	public int getReceiveDonationPowerSummy()
	{
		return receiveDonationPowerSummy;
	}
	/**
	 * @param receiveDonationPowerSummy the receiveDonationPowerSummy to set
	 */
	public void setReceiveDonationPowerSummy(int receiveDonationPowerSummy)
	{
		this.receiveDonationPowerSummy = receiveDonationPowerSummy;
	}
	/**
	 * @return the collegeName
	 */
	public String getCollegeName()
	{
		return collegeName;
	}
	/**
	 * @param collegeName the collegeName to set
	 */
	public void setCollegeName(String collegeName)
	{
		this.collegeName = collegeName;
	}
	/**
	 * @return the emailAlert
	 */
	public int getEmailAlert()
	{
		return emailAlert;
	}
	/**
	 * @param emailAlert the emailAlert to set
	 */
	public void setEmailAlert(int emailAlert)
	{
		this.emailAlert = emailAlert;
	}
	/**
	 * @return the cellPhoneAlert
	 */
	public int getCellPhoneAlert()
	{
		return cellPhoneAlert;
	}
	/**
	 * @param cellPhoneAlert the cellPhoneAlert to set
	 */
	public void setCellPhoneAlert(int cellPhoneAlert)
	{
		this.cellPhoneAlert = cellPhoneAlert;
	}
	/**
	 * @return the user
	 */
	public Users getUser()
	{
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(Users user)
	{
		this.user = user;
	}
	/**
	 * @return the category
	 */
	public Category getCategory()
	{
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category)
	{
		this.category = category;
	}
	/**
	 * @return the categoryID
	 */
	public Long getCategoryID()
	{
		return categoryID;
	}
	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(Long categoryID)
	{
		this.categoryID = categoryID;
	}
	/**
	 * @return the university
	 */
	public University getUniversity()
	{
		return university;
	}
	/**
	 * @param university the university to set
	 */
	public void setUniversity(University university)
	{
		this.university = university;
	}
	/**
	 * @return the nonProfitPartner
	 */
	public NonProfitPartner getNonProfitPartner()
	{
		return nonProfitPartner;
	}
	/**
	 * @param nonProfitPartner the nonProfitPartner to set
	 */
	public void setNonProfitPartner(NonProfitPartner nonProfitPartner)
	{
		this.nonProfitPartner = nonProfitPartner;
	}
	/**
	 * @return the universityID
	 */
	public Long getUniversityID()
	{
		return universityID;
	}
	/**
	 * @param universityID the universityID to set
	 */
	public void setUniversityID(Long universityID)
	{
		this.universityID = universityID;
	}
	/**
	 * @return the nonProfitPartnerIDs
	 */
	public Long getNonProfitPartnerIDs()
	{
		return NonProfitPartnerIDs;
	}
	/**
	 * @param nonProfitPartnerIDs the nonProfitPartnerIDs to set
	 */
	public void setNonProfitPartnerIDs(Long nonProfitPartnerIDs)
	{
		NonProfitPartnerIDs = nonProfitPartnerIDs;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	/**
	 * @return the address1
	 */
	public String getAddress1()
	{
		return address1;
	}
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}
	/**
	 * @return the address2
	 */
	public String getAddress2()
	{
		return address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}
	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}
	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}
	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone()
	{
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}
	/**
	 * @param parentCategoryName the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}
	/**
	 * @return the universityName
	 */
	public String getUniversityName()
	{
		return universityName;
	}
	/**
	 * @param universityName the universityName to set
	 */
	public void setUniversityName(String universityName)
	{
		this.universityName = universityName;
	}
	/**
	 * @return the nonProfitPartnerName
	 */
	public String getNonProfitPartnerName()
	{
		return nonProfitPartnerName;
	}
	/**
	 * @param nonProfitPartnerName the nonProfitPartnerName to set
	 */
	public void setNonProfitPartnerName(String nonProfitPartnerName)
	{
		this.nonProfitPartnerName = nonProfitPartnerName;
	}
	public String getEmailStatus()
	{
		return emailStatus;
	}
	public void setEmailStatus(String emailStatus)
	{
		this.emailStatus = emailStatus;
	}
	public String getCellPhoneStatus()
	{
		return cellPhoneStatus;
	}
	public void setCellPhoneStatus(String cellPhoneStatus)
	{
		this.cellPhoneStatus = cellPhoneStatus;
	}
	/**
	 * @param stateHidden the stateHidden to set
	 */
	public void setStateHidden(String stateHidden) {
		this.stateHidden = stateHidden;
	}
	/**
	 * @return the stateHidden
	 */
	public String getStateHidden() {
		return stateHidden;
	}
	/**
	 * @param stateCodeHidden the stateCodeHidden to set
	 */
	public void setStateCodeHidden(String stateCodeHidden) {
		this.stateCodeHidden = stateCodeHidden;
	}
	/**
	 * @return the stateCodeHidden
	 */
	public String getStateCodeHidden() {
		return stateCodeHidden;
	}
	
	
	
	
}

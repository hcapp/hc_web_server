package common.pojo;

import java.util.ArrayList;

import common.constatns.ApplicationConstants;
import common.pojo.shopper.CategoryInfo;
import common.pojo.shopper.ProductDetail;

public class RetailerInfo
{

	/**
	 * for userRetailPreferenceID.
	 */
	private Integer userRetailPreferenceID;
	/**
	 * for category id.
	 */
	private Integer categoryID;
	/**
	 * for parent category name.
	 */
	private String parentCategoryName;

	/**
	 * The retailerId property.
	 */
	private Integer retailerId;

	/**
	 * The retailerName property.
	 */

	private String retailerName;

	/**
	 * The list for storing type productDetails.
	 */

	private ArrayList<ProductDetail> productDetails;

	/**
	 * The list for storing type categoryDetails.
	 */
	private ArrayList<CategoryInfo> categoryDetails;

	private ArrayList<RetailProduct> inValidProdlist;

	private ArrayList<RetailProduct> ValidProdlist;

	/**
	 * Gets the value of the retailerId property.
	 * 
	 * @return possible object is {@link String }
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * Sets the value of the retailerId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setRetailerId(Integer value)
	{
		this.retailerId = value;
	}

	/**
	 * Gets the value of the retailerName property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * Sets the value of the retailerName property.
	 * 
	 * @param retailerName
	 *            allowed object is {@link String }
	 */
	public void setRetailerName(String retailerName)
	{
		if (retailerName == null)
		{
			retailerName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.retailerName = retailerName;
		}
	}

	/**
	 * for getting productDetails.
	 * 
	 * @return The list of type productDetails.
	 */

	public ArrayList<ProductDetail> getProductDetails()
	{
		return productDetails;
	}

	/**
	 * for setting productDetails.
	 * 
	 * @param productDetails
	 *            The list of type productDetails.
	 */

	public void setProductDetails(ArrayList<ProductDetail> productDetails)
	{
		this.productDetails = productDetails;
	}

	/**
	 * for getting categoryID.
	 * 
	 * @return the categoryID
	 */
	public Integer getCategoryID()
	{
		return categoryID;
	}

	/**
	 * for setting categoryID.
	 * 
	 * @param categoryID
	 *            the categoryID to set
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * for getting parentCategoryName.
	 * 
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}

	/**
	 * for setting parentCategoryName.
	 * 
	 * @param parentCategoryName
	 *            the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}

	/**
	 * for setting categoryDetails.
	 * 
	 * @return the categoryDetails
	 */
	public ArrayList<CategoryInfo> getCategoryDetails()
	{
		return categoryDetails;
	}

	/**
	 * for setting categoryDetails.
	 * 
	 * @param categoryDetails
	 *            the categoryDetails to set
	 */
	public void setCategoryDetails(ArrayList<CategoryInfo> categoryDetails)
	{
		this.categoryDetails = categoryDetails;
	}

	/**
	 * for getting userRetailPreferenceID.
	 * 
	 * @return the userRetailPreferenceID
	 */
	public Integer getUserRetailPreferenceID()
	{
		return userRetailPreferenceID;
	}

	/**
	 * for setting userRetailPreferenceID.
	 * 
	 * @param userRetailPreferenceID
	 *            the userRetailPreferenceID to set
	 */
	public void setUserRetailPreferenceID(Integer userRetailPreferenceID)
	{
		this.userRetailPreferenceID = userRetailPreferenceID;
	}

	public ArrayList<RetailProduct> getInValidProdlist()
	{
		return inValidProdlist;
	}

	public void setInValidProdlist(ArrayList<RetailProduct> inValidProdlist)
	{
		this.inValidProdlist = inValidProdlist;
	}

	public ArrayList<RetailProduct> getValidProdlist()
	{
		return ValidProdlist;
	}

	public void setValidProdlist(ArrayList<RetailProduct> validProdlist)
	{
		ValidProdlist = validProdlist;
	}


}

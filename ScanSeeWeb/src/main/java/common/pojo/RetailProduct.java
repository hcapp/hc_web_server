/**
 * @ (#) RetailProduct.java 05-Jan-2012
 * Project       :ScanSeeWeb
 * File          : RetailProduct.java
 * Author        : Dileep
 * Company       : Span Systems Corporation
 * Date Created  : 05-Jan-2012
 *
 * @author       :  Dileep
 * Modified by   :  
 * Modified date :  
 * Reason        :  
 */


package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class RetailProduct {

	  /**
     * 
     */
    private CommonsMultipartFile productuploadFilePath;
    
	private String scanCode;
	/**
	 * 
	 */
	private String storeIdentificationID;
	
	/**
	 * 
	 */
	private String longDescription;
	
	private Double price;
	private Double salePrice;
	
	private String productPrice;
	private String productSalePrice;
	
	/**
	 * 
	 */
	private String key;
	/**
	 * 
	 */
	public Double getSalePrice()
	{
		return salePrice;
	}

	public void setSalePrice(Double salePrice)
	{
		this.salePrice = salePrice;
	}

	/**
	 * 
	 */
	private String saleStartDate;
	/**
	 * 
	 */
	private String saleEndDate;
	
	private String errorMessage;
	private String dicardedProductsCSVFilePath;
	

	public String getSaleStartDate()
	{
		return saleStartDate;
	}

	public void setSaleStartDate(String saleStartDate)
	{
		this.saleStartDate = saleStartDate;
	}

	public String getSaleEndDate()
	{
		return saleEndDate;
	}

	public void setSaleEndDate(String saleEndDate)
	{
		this.saleEndDate = saleEndDate;
	}

	public String getScanCode() {
		return scanCode;
	}

	public void setScanCode(String scanCode) {
		this.scanCode = scanCode;
	}

	

	public String getStoreIdentificationID()
	{
		return storeIdentificationID;
	}

	public void setStoreIdentificationID(String storeIdentificationID)
	{
		this.storeIdentificationID = storeIdentificationID;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public CommonsMultipartFile getProductuploadFilePath() {
		return productuploadFilePath;
	}

	public void setProductuploadFilePath(CommonsMultipartFile productuploadFilePath) {
		this.productuploadFilePath = productuploadFilePath;
	}

	/**
	 * @return the productPrice
	 */
	public String getProductPrice() {
		return productPrice;
	}

	/**
	 * @return the productSalePrice
	 */
	public String getProductSalePrice() {
		return productSalePrice;
	}

	/**
	 * @param productPrice the productPrice to set
	 */
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	/**
	 * @param productSalePrice the productSalePrice to set
	 */
	public void setProductSalePrice(String productSalePrice) {
		this.productSalePrice = productSalePrice;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public String getDicardedProductsCSVFilePath()
	{
		return dicardedProductsCSVFilePath;
	}

	public void setDicardedProductsCSVFilePath(String dicardedProductsCSVFilePath)
	{
		this.dicardedProductsCSVFilePath = dicardedProductsCSVFilePath;
	}

	/**
	 * @return the key
	 */
	public String getKey()
	{
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key)
	{
		this.key = key;
	}
}

package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.util.Utility;

/**
 * @author dileepa_cc
 */
public class RetailerCustomPage
{
	private int retailerId;
	private String mediaPath;
	private String externalFlag;
	private Long pageId;
	private String qrType;
	private String startDate;
	private String endDate = null;
	private String pageTitle;
	private String qrCode;
	private String retailerImg;
	private String attachLink;
	private String qrImagePath;
	private String qrUrl;
	private String url;
	private String hiddenLocId;
	private String shortDescription;
	private String longDescription;
	private String pageDescription;
	private String retailLocationID;
	private String retailLocations;
	private String pageIds;
	private String sortIds;
	

	private int imageIconID;
	private boolean indefiniteAdDurationFlag = false;
	private String response;
	private String keyword;
	/**
	 * @return the retailLocations
	 */
	public String getRetailLocations()
	{
		return retailLocations;
	}

	/**
	 * @param retailLocations
	 *            the retailLocations to set
	 */
	public void setRetailLocations(String retailLocations)
	{
		this.retailLocations = retailLocations;
	}

	private String imagePath;
	private String pageType;
	private String imageName;
	private String startTime;
	private String endTime;
	private String fileName;
	private String pageStartDate;
	private String pageEndDate = null;
	private long existingPageId;
	/**
	 * Retailer Page Properties
	 */
	private String retPageTitle;
	private int retLocId;
	private String retPageDescription;
	private String retPageShortDescription;
	private String retPageLongDescription;
	private CommonsMultipartFile retImage;

	/**
	 * Retailer Created Page Properties
	 */

	private String retCreatedPageTitle;
	private CommonsMultipartFile retCreatedPageImage;
	private String retCreatedPageDescription;
	private String retCreatedPageShortDescription;
	private String retCreatedPageLongDescription;
	private String retCreatedPageLocId;
	private String retCreatedPageStartDate;
	private String retCreatedPageEndDate = null;
	private String retCreatedPageattachLink;
	private CommonsMultipartFile[] retCreatedPageFile;
	private String splOfferType;
	private String retCreatedPageAttachLinkTitle;
	private String retCreatedPageAttachLinkLocId;
	private String landigPageType;
	private String landingPageStartTimeHrs;
	private String landingPageStartTimeMin;
	private String landingPageEndTimeHrs;
	private String landingPageEndTimeMin;
	/**
	 * Special Offer Page Properties
	 */

	private String splOfferTitle;
	private CommonsMultipartFile splOfferImage;
	private String splOfferDescription;
	private String splOfferShortDescription;
	private String splOfferLongDescription;
	private String splOfferLocId;
	private String splOfferStartDate;
	private String splOfferEndDate = null;
	private String splOfferattachLink;
	private CommonsMultipartFile[] splOfferFile;
	private String splOfferAttatchLinkTitle;
	private String splOfferAttatchLinkLocId;
	private String splofferStartTimeHrs;
	private String splofferStartTimeMin;
	private String splOfferEndTimeHrs;
	private String splOfferEndTimeMin;
	private String searchKey;
	private long QRRetailerCustomPageID;

	private String viewName;
	private String imageUplType;

	private String defaultImageIcon;

	private String storeID;

	private CommonsMultipartFile retFile;
	private String pdfFileName;
	private String oldPdfFileName;
	private Integer iRowId;
	private Integer iSortOrderID;
	private String sortOrderID;
	private String primaryKeys;
	private String pageNumber;
	private String pageFlag;
	// /Setter and Getter Methods
	private String recordCount;
	private String aboutUs;
	private String rule;
	private String termsandConditions;
	private Integer quantity;
	
	private String bandLocationHidden;

	
	/**
	 * @return the defaultImageIcon
	 */
	public String getDefaultImageIcon()
	{
		return defaultImageIcon;
	}

	/**
	 * @param defaultImageIcon
	 *            the defaultImageIcon to set
	 */
	public void setDefaultImageIcon(String defaultImageIcon)
	{
		this.defaultImageIcon = defaultImageIcon;
	}

	/**
	 * @return the qRRetailerCustomPageID
	 */
	public long getQRRetailerCustomPageID()
	{
		return QRRetailerCustomPageID;
	}

	/**
	 * @param qRRetailerCustomPageID
	 *            the qRRetailerCustomPageID to set
	 */
	public void setQRRetailerCustomPageID(long qRRetailerCustomPageID)
	{
		QRRetailerCustomPageID = qRRetailerCustomPageID;
	}

	/**
	 * @return the searchKey
	 */
	public String getSearchKey()
	{
		return searchKey;
	}

	/**
	 * @param searchKey
	 *            the searchKey to set
	 */
	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	public String getRetPageTitle()
	{
		return retPageTitle;
	}

	public int getRetailerId()
	{
		return retailerId;
	}

	/**
	 * @return the imageIconID
	 */
	public int getImageIconID()
	{
		return imageIconID;
	}

	/**
	 * @param imageIconID
	 *            the imageIconID to set
	 */
	public void setImageIconID(int imageIconID)
	{
		this.imageIconID = imageIconID;
	}

	public void setRetailerId(int retailerId)
	{
		this.retailerId = retailerId;
	}

	public void setRetPageTitle(String retPageTitle)
	{
		this.retPageTitle = Utility.removeWhiteSpaces(retPageTitle);
	}

	public int getRetLocId()
	{
		return retLocId;
	}

	public void setRetLocId(int retLocId)
	{
		this.retLocId = retLocId;
	}

	public String getRetPageDescription()
	{
		return retPageDescription;
	}

	public void setRetPageDescription(String retPageDescription)
	{
		this.retPageDescription = retPageDescription;
	}

	public String getRetPageShortDescription()
	{
		return retPageShortDescription;
	}

	public void setRetPageShortDescription(String retPageShortDescription)
	{
		this.retPageShortDescription = Utility.removeWhiteSpaces(retPageShortDescription);
	}

	public String getRetPageLongDescription()
	{
		return retPageLongDescription;
	}

	public void setRetPageLongDescription(String retPageLongDescription)
	{
		this.retPageLongDescription = Utility.removeWhiteSpaces(retPageLongDescription);
	}

	public CommonsMultipartFile getRetImage()
	{
		return retImage;
	}

	public void setRetImage(CommonsMultipartFile retImage)
	{
		this.retImage = retImage;
	}

	public String getRetCreatedPageTitle()
	{
		return retCreatedPageTitle;
	}

	public void setRetCreatedPageTitle(String retCreatedPageTitle)
	{
		this.retCreatedPageTitle = retCreatedPageTitle;
	}

	public CommonsMultipartFile getRetCreatedPageImage()
	{
		return retCreatedPageImage;
	}

	public void setRetCreatedPageImage(CommonsMultipartFile retCreatedPageImage)
	{
		this.retCreatedPageImage = retCreatedPageImage;
	}

	public String getRetCreatedPageDescription()
	{
		return retCreatedPageDescription;
	}

	public void setRetCreatedPageDescription(String retCreatedPageDescription)
	{
		this.retCreatedPageDescription = retCreatedPageDescription;
	}

	public String getRetCreatedPageShortDescription()
	{
		return retCreatedPageShortDescription;
	}

	public void setRetCreatedPageShortDescription(String retCreatedPageShortDescription)
	{
		this.retCreatedPageShortDescription = retCreatedPageShortDescription;
	}

	public String getRetCreatedPageLongDescription()
	{
		return retCreatedPageLongDescription;
	}

	public void setRetCreatedPageLongDescription(String retCreatedPageLongDescription)
	{
		this.retCreatedPageLongDescription = retCreatedPageLongDescription;
	}

	public String getRetCreatedPageLocId()
	{
		return retCreatedPageLocId;
	}

	public void setRetCreatedPageLocId(String retCreatedPageLocId)
	{
		this.retCreatedPageLocId = retCreatedPageLocId;
	}

	public String getRetCreatedPageStartDate()
	{
		return retCreatedPageStartDate;
	}

	public void setRetCreatedPageStartDate(String retCreatedPageStartDate)
	{
		this.retCreatedPageStartDate = retCreatedPageStartDate;
	}

	public String getRetCreatedPageEndDate()
	{
		return retCreatedPageEndDate;
	}

	public void setRetCreatedPageEndDate(String retCreatedPageEndDate)
	{
		this.retCreatedPageEndDate = retCreatedPageEndDate;
	}

	public String getRetCreatedPageattachLink()
	{
		return retCreatedPageattachLink;
	}

	public void setRetCreatedPageattachLink(String retCreatedPageattachLink)
	{
		this.retCreatedPageattachLink = retCreatedPageattachLink;
	}

	public String getSplOfferTitle()
	{
		return splOfferTitle;
	}

	public void setSplOfferTitle(String splOfferTitle)
	{
		this.splOfferTitle = Utility.removeWhiteSpaces(splOfferTitle);
	}

	public CommonsMultipartFile getSplOfferImage()
	{
		return splOfferImage;
	}

	public void setSplOfferImage(CommonsMultipartFile splOfferImage)
	{
		this.splOfferImage = splOfferImage;
	}

	public String getSplOfferDescription()
	{
		return splOfferDescription;
	}

	public void setSplOfferDescription(String splOfferDescription)
	{
		this.splOfferDescription = splOfferDescription;
	}

	public String getSplOfferShortDescription()
	{
		return splOfferShortDescription;
	}

	public void setSplOfferShortDescription(String splOfferShortDescription)
	{
		this.splOfferShortDescription = Utility.removeWhiteSpaces(splOfferShortDescription);
	}

	public String getSplOfferLongDescription()
	{
		return splOfferLongDescription;
	}

	public void setSplOfferLongDescription(String splOfferLongDescription)
	{
		this.splOfferLongDescription = Utility.removeWhiteSpaces(splOfferLongDescription);
	}

	public String getSplOfferLocId()
	{
		return splOfferLocId;
	}

	public void setSplOfferLocId(String splOfferLocId)
	{
		this.splOfferLocId = splOfferLocId;
	}

	public String getSplOfferStartDate()
	{
		return splOfferStartDate;
	}

	public void setSplOfferStartDate(String splOfferStartDate)
	{
		this.splOfferStartDate = splOfferStartDate;
	}

	public String getSplOfferEndDate()
	{
		return splOfferEndDate;
	}

	public void setSplOfferEndDate(String splOfferEndDate)
	{
		this.splOfferEndDate = splOfferEndDate;
	}

	public String getSplOfferattachLink()
	{
		return splOfferattachLink;
	}

	public void setSplOfferattachLink(String splOfferattachLink)
	{
		this.splOfferattachLink = splOfferattachLink;
	}

	public CommonsMultipartFile[] getRetCreatedPageFile()
	{
		return retCreatedPageFile;
	}

	public void setRetCreatedPageFile(CommonsMultipartFile[] retCreatedPageFile)
	{
		this.retCreatedPageFile = retCreatedPageFile;
	}

	public CommonsMultipartFile[] getSplOfferFile()
	{
		return splOfferFile;
	}

	public void setSplOfferFile(CommonsMultipartFile[] splOfferFile)
	{
		this.splOfferFile = splOfferFile;
	}



	/**
	 * @return the qrType
	 */
	public String getQrType()
	{
		return qrType;
	}

	/**
	 * @param qrType
	 *            the qrType to set
	 */
	public void setQrType(String qrType)
	{
		this.qrType = qrType;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(String startDate)
	{
		if (null == startDate || "".equals(startDate))
		{
			this.startDate = "";
		}
		else
		{
			this.startDate = Utility.convertDBdate(startDate);
		}
		// this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String endDate)
	{
		if (null == endDate || "".equals(endDate))
		{
			this.endDate = "";
		}
		else
		{
			this.endDate = Utility.convertDBdate(endDate);
		}
		// this.endDate = endDate;
	}

	public String getMediaPath()
	{
		return mediaPath;
	}

	public void setMediaPath(String mediaPath)
	{
		this.mediaPath = mediaPath;
	}

	public String getExternalFlag()
	{
		return externalFlag;
	}

	public void setExternalFlag(String externalFlag)
	{
		this.externalFlag = externalFlag;
	}

	public String getRetailerImg()
	{
		return retailerImg;
	}

	public void setRetailerImg(String retailerImg)
	{
		this.retailerImg = retailerImg;
	}

	/**
	 * @return the pageTitle
	 */
	public String getPageTitle()
	{
		return pageTitle;
	}

	/**
	 * @param pageTitle
	 *            the pageTitle to set
	 */
	public void setPageTitle(String pageTitle)
	{
		this.pageTitle = pageTitle;
	}

	/**
	 * @return the qrCode
	 */
	public String getQrCode()
	{
		return qrCode;
	}

	/**
	 * @param qrCode
	 *            the qrCode to set
	 */
	public void setQrCode(String qrCode)
	{
		this.qrCode = qrCode;
	}

	public String getSplOfferType()
	{
		return splOfferType;
	}

	/*
	 * public String getAttachLinkTitle() { return attachLinkTitle; }
	 */

	/**
	 * @return the qrImagePath
	 */
	public String getQrImagePath()
	{
		return qrImagePath;
	}

	/**
	 * @param qrImagePath
	 *            the qrImagePath to set
	 */
	public void setQrImagePath(String qrImagePath)
	{
		this.qrImagePath = qrImagePath;
	}

	/**
	 * @return the qrUrl
	 */
	public String getQrUrl()
	{
		return qrUrl;
	}

	/**
	 * @param qrUrl
	 *            the qrUrl to set
	 */
	public void setQrUrl(String qrUrl)
	{
		this.qrUrl = qrUrl;
	}

	public void setSplOfferType(String splOfferType)
	{
		this.splOfferType = splOfferType;
	}

	public String getSplOfferAttatchLinkTitle()
	{
		return splOfferAttatchLinkTitle;
	}

	public void setSplOfferAttatchLinkTitle(String splOfferAttatchLinkTitle)
	{
		this.splOfferAttatchLinkTitle = splOfferAttatchLinkTitle;
	}

	public String getSplOfferAttatchLinkLocId()
	{
		return splOfferAttatchLinkLocId;
	}

	public void setSplOfferAttatchLinkLocId(String splOfferAttatchLinkLocId)
	{
		this.splOfferAttatchLinkLocId = splOfferAttatchLinkLocId;
	}

	public String getHiddenLocId()
	{
		return hiddenLocId;
	}

	public void setHiddenLocId(String hiddenLocId)
	{
		if (hiddenLocId == null || "".equals(hiddenLocId) || "null".equals(hiddenLocId))
		{
			hiddenLocId = null;
		}
		else
		{
			this.hiddenLocId = hiddenLocId;
		}
	}

	public String getShortDescription()
	{
		return shortDescription;
	}

	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}

	public String getLongDescription()
	{
		return longDescription;
	}

	public void setLongDescription(String longDescription)
	{
		this.longDescription = longDescription;
	}

	public String getPageDescription()
	{
		return pageDescription;
	}

	public void setPageDescription(String pageDescription)
	{
		this.pageDescription = pageDescription;
	}

	public String getRetailLocationID()
	{
		return retailLocationID;
	}

	public void setRetailLocationID(String retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	public String getImagePath()
	{
		return imagePath;
	}

	public void setImagePath(String imagePath)
	{
		this.imagePath = imagePath;
	}

	public String getAttachLink()
	{
		return attachLink;
	}

	public void setAttachLink(String attachLink)
	{
		this.attachLink = attachLink;
	}

	public String getPageType()
	{
		return pageType;
	}

	public void setPageType(String pageType)
	{
		this.pageType = pageType;
	}

	public String getImageName()
	{
		return imageName;
	}

	public void setImageName(String imageName)
	{
		this.imageName = imageName;
	}

	public String getRetCreatedPageAttachLinkTitle()
	{
		return retCreatedPageAttachLinkTitle;
	}

	public void setRetCreatedPageAttachLinkTitle(String retCreatedPageAttachLinkTitle)
	{
		this.retCreatedPageAttachLinkTitle = retCreatedPageAttachLinkTitle;
	}

	public String getRetCreatedPageAttachLinkLocId()
	{
		return retCreatedPageAttachLinkLocId;
	}

	public void setRetCreatedPageAttachLinkLocId(String retCreatedPageAttachLinkLocId)
	{
		this.retCreatedPageAttachLinkLocId = retCreatedPageAttachLinkLocId;
	}

	public String getLandigPageType()
	{
		return landigPageType;
	}

	public void setLandigPageType(String landigPageType)
	{
		this.landigPageType = landigPageType;
	}

	public String getLandingPageStartTimeHrs()
	{
		return landingPageStartTimeHrs;
	}

	public void setLandingPageStartTimeHrs(String landingPageStartTimeHrs)
	{
		this.landingPageStartTimeHrs = landingPageStartTimeHrs;
	}

	public String getLandingPageStartTimeMin()
	{
		return landingPageStartTimeMin;
	}

	public void setLandingPageStartTimeMin(String landingPageStartTimeMin)
	{
		this.landingPageStartTimeMin = landingPageStartTimeMin;
	}

	public String getLandingPageEndTimeHrs()
	{
		return landingPageEndTimeHrs;
	}

	public void setLandingPageEndTimeHrs(String landingPageEndTimeHrs)
	{
		this.landingPageEndTimeHrs = landingPageEndTimeHrs;
	}

	public String getLandingPageEndTimeMin()
	{
		return landingPageEndTimeMin;
	}

	public void setLandingPageEndTimeMin(String landingPageEndTimeMin)
	{
		this.landingPageEndTimeMin = landingPageEndTimeMin;
	}

	public String getSplofferStartTimeHrs()
	{
		return splofferStartTimeHrs;
	}

	public void setSplofferStartTimeHrs(String splofferStartTimeHrs)
	{
		this.splofferStartTimeHrs = splofferStartTimeHrs;
	}

	public String getSplofferStartTimeMin()
	{
		return splofferStartTimeMin;
	}

	public void setSplofferStartTimeMin(String splofferStartTimeMin)
	{
		this.splofferStartTimeMin = splofferStartTimeMin;
	}

	public String getSplOfferEndTimeHrs()
	{
		return splOfferEndTimeHrs;
	}

	public void setSplOfferEndTimeHrs(String splOfferEndTimeHrs)
	{
		this.splOfferEndTimeHrs = splOfferEndTimeHrs;
	}

	public String getSplOfferEndTimeMin()
	{
		return splOfferEndTimeMin;
	}

	public void setSplOfferEndTimeMin(String splOfferEndTimeMin)
	{
		this.splOfferEndTimeMin = splOfferEndTimeMin;
	}

	public String getStartTime()
	{
		return startTime;
	}

	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}

	public String getEndTime()
	{
		return endTime;
	}

	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	/**
	 * @return the pageStartDate
	 */
	public String getPageStartDate()
	{
		return pageStartDate;
	}

	/**
	 * @param pageStartDate
	 *            the pageStartDate to set
	 */
	public void setPageStartDate(String pageStartDate)
	{
		if (null == pageStartDate || "".equals(pageStartDate))
		{
			this.pageStartDate = "";
		}
		else
		{
			this.pageStartDate = Utility.convertDBdateTime(pageStartDate);
		}

	}

	/**
	 * @return the pageEndDate
	 */
	public String getPageEndDate()
	{
		return pageEndDate;
	}

	/**
	 * @param pageEndDate
	 *            the pageEndDate to set
	 */
	public void setPageEndDate(String pageEndDate)
	{
		if (null == pageEndDate || "".equals(pageEndDate))
		{
			this.pageEndDate = "";
		}
		else
		{
			this.pageEndDate = Utility.convertDBdateTime(pageEndDate);
		}

	}

	public long getExistingPageId()
	{
		return existingPageId;
	}

	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	public void setExistingPageId(long existingPageId)
	{
		this.existingPageId = existingPageId;
	}

	public String getViewName()
	{
		return viewName;
	}

	public void setViewName(String viewName)
	{
		this.viewName = viewName;
	}

	public String getImageUplType()
	{
		return imageUplType;
	}

	public void setImageUplType(String imageUplType)
	{
		this.imageUplType = imageUplType;
	}

	public String getStoreID()
	{
		return storeID;
	}

	public void setStoreID(String storeID)
	{
		this.storeID = storeID;
	}

	public boolean isIndefiniteAdDurationFlag()
	{
		return indefiniteAdDurationFlag;
	}

	public void setIndefiniteAdDurationFlag(boolean indefiniteAdDurationFlag)
	{
		this.indefiniteAdDurationFlag = indefiniteAdDurationFlag;
	}

	public CommonsMultipartFile getRetFile()
	{
		return retFile;
	}

	public void setRetFile(CommonsMultipartFile retFile)
	{
		this.retFile = retFile;
	}

	public String getPdfFileName()
	{
		return pdfFileName;
	}

	public void setPdfFileName(String pdfFileName)
	{
		this.pdfFileName = pdfFileName;
	}

	/**
	 * @return the iRowId
	 */
	public Integer getiRowId()
	{
		return iRowId;
	}

	/**
	 * @return the iSortOrderID
	 */
	public Integer getiSortOrderID()
	{
		return iSortOrderID;
	}

	/**
	 * @param iSortOrderID
	 *            the iSortOrderID to set
	 */
	public void setiSortOrderID(Integer iSortOrderID)
	{
		this.iSortOrderID = iSortOrderID;
	}

	/**
	 * @param iRowId
	 *            the iRowId to set
	 */
	public void setiRowId(Integer iRowId)
	{
		this.iRowId = iRowId;
	}

	/**
	 * @return the sortOrderID
	 */
	public String getSortOrderID()
	{
		return sortOrderID;
	}

	/**
	 * @param sortOrderID
	 *            the sortOrderID to set
	 */
	public void setSortOrderID(String sortOrderID)
	{
		this.sortOrderID = sortOrderID;
	}

	/**
	 * @return the primaryKeys
	 */
	public String getPrimaryKeys()
	{
		return primaryKeys;
	}

	/**
	 * @param primaryKeys
	 *            the primaryKeys to set
	 */
	public void setPrimaryKeys(String primaryKeys)
	{
		this.primaryKeys = primaryKeys;
	}

	/**
	 * @return the pageNumber
	 */
	public String getPageNumber()
	{
		return pageNumber;
	}

	/**
	 * @param pageNumber
	 *            the pageNumber to set
	 */
	public void setPageNumber(String pageNumber)
	{
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the pageFlag
	 */
	public String getPageFlag()
	{
		return pageFlag;
	}

	/**
	 * @param pageFlag
	 *            the pageFlag to set
	 */
	public void setPageFlag(String pageFlag)
	{
		this.pageFlag = pageFlag;
	}

	public String getAboutUs()
	{
		return aboutUs;
	}

	public void setAboutUs(String aboutUs)
	{
		this.aboutUs = aboutUs;
	}

	public String getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(String recordCount)
	{
		this.recordCount = recordCount;
	}

	public String getRule()
	{
		return rule;
	}

	public void setRule(String rule)
	{
		this.rule = rule;
	}

	public String getTermsandConditions()
	{
		return termsandConditions;
	}

	public void setTermsandConditions(String termsandConditions)
	{
		this.termsandConditions = termsandConditions;
	}

	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @param oldPdfFileName the oldPdfFileName to set
	 */
	public void setOldPdfFileName(String oldPdfFileName)
	{
		this.oldPdfFileName = oldPdfFileName;
	}

	/**
	 * @return the oldPdfFileName
	 */
	public String getOldPdfFileName()
	{
		return oldPdfFileName;
	}

	/**
	 * @return the pageIds
	 */
	public String getPageIds() {
		return pageIds;
	}

	/**
	 * @param pageIds the pageIds to set
	 */
	public void setPageIds(String pageIds) {
		this.pageIds = pageIds;
	}

	/**
	 * @return the sortIds
	 */
	public String getSortIds() {
		return sortIds;
	}

	/**
	 * @param sortIds the sortIds to set
	 */
	public void setSortIds(String sortIds) {
		this.sortIds = sortIds;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	
	public String getBandLocationHidden() {
		return bandLocationHidden;
	}

	public void setBandLocationHidden(String bandLocationHidden) {
		this.bandLocationHidden = bandLocationHidden;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}




	
	
	
}

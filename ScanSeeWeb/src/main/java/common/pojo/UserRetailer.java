/**
 * 
 */
package common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class UserRetailer
{
	/**
	 * 
	 */
	private Long userRetailerID;
	/**
	 * 
	 */
	private Long RetailID;
	/**
	 * 
	 */
	private Long UserID;
	/**
	 * 
	 */
	private Long AdminFlag;
	/**
	 * 
	 */
	private Long RetailLocationID;
	/**
	 * 
	 */
	private Retailer retailer;
	/**
	 * 
	 */
	private Users user;
	/**
	 * 
	 */
	private RetailerLocation retailerLocation;
	/**
	 * 
	 */
	public UserRetailer()
	{
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param userRetailerID
	 * @param retailID
	 * @param userID
	 * @param adminFlag
	 * @param retailLocationID
	 * @param retailer
	 * @param user
	 * @param retailerLocation
	 */
	public UserRetailer(Long userRetailerID, Long retailID, Long userID, Long adminFlag, Long retailLocationID, Retailer retailer, Users user,
			RetailerLocation retailerLocation)
	{
		super();
		this.userRetailerID = userRetailerID;
		RetailID = retailID;
		UserID = userID;
		AdminFlag = adminFlag;
		RetailLocationID = retailLocationID;
		this.retailer = retailer;
		this.user = user;
		this.retailerLocation = retailerLocation;
	}
	/**
	 * @return the userRetailerID
	 */
	public Long getUserRetailerID()
	{
		return userRetailerID;
	}
	/**
	 * @param userRetailerID the userRetailerID to set
	 */
	public void setUserRetailerID(Long userRetailerID)
	{
		this.userRetailerID = userRetailerID;
	}
	/**
	 * @return the retailID
	 */
	public Long getRetailID()
	{
		return RetailID;
	}
	/**
	 * @param retailID the retailID to set
	 */
	public void setRetailID(Long retailID)
	{
		RetailID = retailID;
	}
	/**
	 * @return the userID
	 */
	public Long getUserID()
	{
		return UserID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Long userID)
	{
		UserID = userID;
	}
	/**
	 * @return the adminFlag
	 */
	public Long getAdminFlag()
	{
		return AdminFlag;
	}
	/**
	 * @param adminFlag the adminFlag to set
	 */
	public void setAdminFlag(Long adminFlag)
	{
		AdminFlag = adminFlag;
	}
	/**
	 * @return the retailLocationID
	 */
	public Long getRetailLocationID()
	{
		return RetailLocationID;
	}
	/**
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(Long retailLocationID)
	{
		RetailLocationID = retailLocationID;
	}
	/**
	 * @return the retailer
	 */
	public Retailer getRetailer()
	{
		return retailer;
	}
	/**
	 * @param retailer the retailer to set
	 */
	public void setRetailer(Retailer retailer)
	{
		this.retailer = retailer;
	}
	/**
	 * @return the user
	 */
	public Users getUser()
	{
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(Users user)
	{
		this.user = user;
	}
	/**
	 * @return the retailerLocation
	 */
	public RetailerLocation getRetailerLocation()
	{
		return retailerLocation;
	}
	/**
	 * @param retailerLocation the retailerLocation to set
	 */
	public void setRetailerLocation(RetailerLocation retailerLocation)
	{
		this.retailerLocation = retailerLocation;
	}
}

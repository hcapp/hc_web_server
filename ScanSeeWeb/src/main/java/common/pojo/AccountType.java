/**
 * Project     : Scan See
 * File        : AccountType.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 30th January 2012
 */
package common.pojo;
/**
 * Bean used in Bank Account Type.
 * @author Created by SPAN.
 */
public class AccountType
{
	/**
	 * 
	 */
	private Long accountTypeID;
	/**
	 * 
	 */
	private String accountType;
	/**
	 * 
	 */
	public AccountType()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param accountTypeID
	 * @param accountType
	 */
	public AccountType(Long accountTypeID, String accountType)
	{
		super();
		this.accountTypeID = accountTypeID;
		this.accountType = accountType;
	}
	/**
	 * @return the accountTypeID
	 */
	public Long getAccountTypeID()
	{
		return accountTypeID;
	}
	/**
	 * @param accountTypeID the accountTypeID to set
	 */
	public void setAccountTypeID(Long accountTypeID)
	{
		this.accountTypeID = accountTypeID;
	}
	/**
	 * @return the accountType
	 */
	public String getAccountType()
	{
		return accountType;
	}
	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType)
	{
		this.accountType = accountType;
	}
}

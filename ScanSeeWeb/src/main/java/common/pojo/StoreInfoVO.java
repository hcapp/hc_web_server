package common.pojo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.constatns.ApplicationConstants;
import common.util.Utility;

/**
 * This is a pojo class declared to store the Location Data
 * 
 * @author malathi_lr
 * 
 */
public class StoreInfoVO {

	/**
	 * 
	 */
	public Integer retailID = null;

	/**
	 * variable declared for store Identification ID
	 */
	public String storeID = null;

	/**
	 * variable declared for store Address
	 */
	public String storeAddress = null;

	/**
	 * variable declared for state
	 */
	public String state = null;

	/**
	 * variable declared for city
	 */
	public String city = null;

	/**
	 * variable declared for postal code
	 */
	public String postalCode = null;

	/**
	 * variable declared for phone number
	 */
	public String phoneNumber = null;

	/**
	 * variable declared for cityHidden
	 */
	public String cityHidden = null;
	/**
	 * 
	 */
	private String keyword = null;

	/**
	 * variable declared for location setup file
	 */
	public String locationSetUpFile = null;

	/**
	 * 
	 */
	public String retailUrl = null;

	/**
	 * Variable storeStartTime declared as String.
	 */

	
	private Double retailerLocationLatitude;
	/**
	 * 
	 */
	private Double retailerLocationLongitude;
	/**
	 * 
	 */
	private String geoError;
	/**
     * 
     */
	private String stateHidden;
	/**
     * 
     */
	private String stateCodeHidden;
	/**
	 * 
	 */
	private String locationImgPath;
	/**
	 * 
	 */
	private CommonsMultipartFile imageFile;
	/**
	 * 
	 */
	private String viewName;
	/**
	 * 
	 */
	private CommonsMultipartFile[] imageFilePath;
	

	/**
	 * 
	 */
	private String storeStartHrs;
	/**
	 * 
	 */
	private String storeStartMins;

	/**
	 * 
	 */
	private String storeEndHrs;
	/**
	 * 
	 */
	private String storeEndMins;
	/**
	 * 
	 */
	private Integer storeTimeZoneId;
	/**
	 * 
	 */
	private String startTimeUTC;

	/**
	 * 
	 */
	private String endTimeUTC;
	/**
	 * 
	 */
	private String timeZoneHidden;
	
	private String startTime;
	
	private String endTime;
	
	private String weekDayName;
	
	private List <StoreInfoVO>  businessHoursList = new ArrayList<StoreInfoVO>();

	
	/**
	 * @return the retailUrl
	 */
	public String getRetailUrl() {
		return retailUrl;
	}

	/**
	 * @param retailUrl
	 *            the retailUrl to set
	 */
	public void setRetailUrl(String retailUrl) {
		this.retailUrl = Utility.removeWhiteSpaces(retailUrl);
	}

	
	/**
	 * @return the locationSetUpFile
	 */
	public String getLocationSetUpFile() {
		return locationSetUpFile;
	}

	/**
	 * @param locationSetUpFile
	 *            the locationSetUpFile to set
	 */
	public void setLocationSetUpFile(String locationSetUpFile) {
		this.locationSetUpFile = locationSetUpFile;
	}

	/**
	 * @return the cityHidden
	 */
	public String getCityHidden() {
		return cityHidden;
	}

	/**
	 * @param cityHidden
	 *            the cityHidden to set
	 */
	public void setCityHidden(String cityHidden) {
		this.cityHidden = cityHidden;
	}

	/**
	 * @return the retailID
	 */
	public Integer getRetailID() {
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Integer retailID) {
		this.retailID = retailID;
	}

	/**
	 * @return the storeID
	 */
	public String getStoreID() {
		return storeID;
	}

	/**
	 * @return the storeAddress
	 */
	public String getStoreAddress() {
		return storeAddress;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @return the phoneNUmber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param storeID
	 *            the storeID to set
	 */
	public void setStoreID(String storeID) {
		this.storeID = Utility.removeWhiteSpaces(storeID);
	}

	/**
	 * @param storeAddress
	 *            the storeAddress to set
	 */
	public void setStoreAddress(String storeAddress) {
		this.storeAddress = Utility.removeWhiteSpaces(storeAddress);
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @param phoneNUmber
	 *            the phoneNUmber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * @param keyword
	 *            the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = Utility.removeWhiteSpaces(keyword);
	}

	/**
	 * @return the retailerLocationLatitude
	 */
	public Double getRetailerLocationLatitude() {
		return retailerLocationLatitude;
	}

	/**
	 * @param retailerLocationLatitude
	 *            the retailerLocationLatitude to set
	 */
	public void setRetailerLocationLatitude(Double retailerLocationLatitude) {
		this.retailerLocationLatitude = retailerLocationLatitude;
	}

	/**
	 * @return the retailerLocationLongitude
	 */
	public Double getRetailerLocationLongitude() {
		return retailerLocationLongitude;
	}

	/**
	 * @param retailerLocationLongitude
	 *            the retailerLocationLongitude to set
	 */
	public void setRetailerLocationLongitude(Double retailerLocationLongitude) {
		this.retailerLocationLongitude = retailerLocationLongitude;
	}

	/**
	 * @return the geoError
	 */
	public String getGeoError() {
		return geoError;
	}

	/**
	 * @param geoError
	 *            the geoError to set
	 */
	public void setGeoError(String geoError) {
		this.geoError = geoError;
	}

	/**
	 * @return the stateHidden
	 */
	public String getStateHidden() {
		return stateHidden;
	}

	/**
	 * @param stateHidden
	 *            the stateHidden to set
	 */
	public void setStateHidden(String stateHidden) {
		this.stateHidden = stateHidden;
	}

	/**
	 * @return the stateCodeHidden
	 */
	public String getStateCodeHidden() {
		return stateCodeHidden;
	}

	/**
	 * @param stateCodeHidden
	 *            the stateCodeHidden to set
	 */
	public void setStateCodeHidden(String stateCodeHidden) {
		this.stateCodeHidden = stateCodeHidden;
	}

	/**
	 * @return the locationImgPath
	 */
	public String getLocationImgPath() {
		return locationImgPath;
	}

	/**
	 * @param locationImgPath
	 *            the locationImgPath to set
	 */
	public void setLocationImgPath(String locationImgPath) {
		if ("".equals(Utility.checkNull(locationImgPath))) {
			// Changed below code as IE browser showing broken image if it did
			// not find image.
			this.locationImgPath = ApplicationConstants.BLANKIMAGE;
		} else {
			this.locationImgPath = locationImgPath;
		}
	}

	/**
	 * @return the imageFile
	 */
	public CommonsMultipartFile getImageFile() {
		return imageFile;
	}

	/**
	 * @param imageFile
	 *            the imageFile to set
	 */
	public void setImageFile(CommonsMultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	/**
	 * @return the viewName
	 */
	public String getViewName() {
		return viewName;
	}

	/**
	 * @param viewName
	 *            the viewName to set
	 */
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	/**
	 * @return the imageFilePath
	 */
	public CommonsMultipartFile[] getImageFilePath() {
		return imageFilePath;
	}

	/**
	 * @param imageFilePath
	 *            the imageFilePath to set
	 */
	public void setImageFilePath(CommonsMultipartFile[] imageFilePath) {
		this.imageFilePath = imageFilePath;
	}
	
	/**
	 * @return the storeTimeZone
	 */

	public Integer getStoreTimeZoneId() {
		return storeTimeZoneId;
	}

	/**
	 * @param storeTimeZone
	 *            the storeTimeZone to set
	 */

	public void setStoreTimeZoneId(Integer storeTimeZoneId) {
		this.storeTimeZoneId = storeTimeZoneId;
	}

	/**
	 * @return the storeStartHrs
	 */
	public String getStoreStartHrs() {
		return storeStartHrs;
	}

	/**
	 * @param storeStartHrs
	 *            the storeStartHrs to set
	 */
	public void setStoreStartHrs(String storeStartHrs) {
		this.storeStartHrs = storeStartHrs;
	}

	/**
	 * @return the storeStartMins
	 */
	public String getStoreStartMins() {
		return storeStartMins;
	}

	/**
	 * @param storeStartMins
	 *            the storeStartMins to set
	 */
	public void setStoreStartMins(String storeStartMins) {
		this.storeStartMins = storeStartMins;
	}

	/**
	 * @return the storeEndHrs
	 */
	public String getStoreEndHrs() {
		return storeEndHrs;
	}

	/**
	 * @param storeEndHrs
	 *            the storeEndHrs to set
	 */
	public void setStoreEndHrs(String storeEndHrs) {
		this.storeEndHrs = storeEndHrs;
	}

	/**
	 * @return the storeEndMins
	 */

	public String getStoreEndMins() {
		return storeEndMins;
	}

	/**
	 * @param storeEndMins
	 *            the storeEndMins to set
	 */
	public void setStoreEndMins(String storeEndMins) {
		this.storeEndMins = storeEndMins;
	}

	/**
	 * @return the startTimeUTC
	 */

	public String getStartTimeUTC() {
		return startTimeUTC;
	}

	/**
	 * @param startTimeUTC
	 *            the startTimeUTC to set
	 */
	public void setStartTimeUTC(String startTimeUTC) {
		this.startTimeUTC = startTimeUTC;
	}

	/**
	 * @return the endTimeUTC
	 */

	public String getEndTimeUTC() {
		return endTimeUTC;
	}

	/**
	 * @param endTimeUTC
	 *            the endTimeUTC to set
	 */
	public void setEndTimeUTC(String endTimeUTC) {
		this.endTimeUTC = endTimeUTC;
	}
	
	public String getTimeZoneHidden() {
		return timeZoneHidden;
	}

	public void setTimeZoneHidden(String timeZoneHidden) {
		this.timeZoneHidden = timeZoneHidden;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public List <StoreInfoVO> getBusinessHoursList() {
		return businessHoursList;
	}

	public void setBusinessHoursList(List <StoreInfoVO> businessHoursList) {
		this.businessHoursList = businessHoursList;
	}

	public String getWeekDayName() {
		return weekDayName;
	}

	public void setWeekDayName(String weekDayName) {
		this.weekDayName = weekDayName;
	}


}

package common.pojo;

public class RetailerLocationProductJson
{

	String productID;
	String price;
	String salePrice;
	String location;
	String description;
	/**
	 * 
	 */
	private String saleStartDate;
	/**
	 * 
	 */
	private String saleEndDate;
		/**
	 * @return the productID
	 */
	public String getProductID()
	{
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(String productID)
	{
		this.productID = productID;
	}
	/**
	 * @return the price
	 */
	public String getPrice()
	{
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price)
	{
		this.price = price;
	}
	/**
	 * @return the salePrice
	 */
	public String getSalePrice()
	{
		return salePrice;
	}
	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(String salePrice)
	{
		this.salePrice = salePrice;
	}
	/**
	 * @return the location
	 */
	public String getLocation()
	{
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location)
	{
		this.location = location;
	}
	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getSaleStartDate()
	{
		return saleStartDate;
	}
	public void setSaleStartDate(String saleStartDate)
	{
		this.saleStartDate = saleStartDate;
	}
	public String getSaleEndDate()
	{
		return saleEndDate;
	}
	public void setSaleEndDate(String saleEndDate)
	{
		this.saleEndDate = saleEndDate;
	}
	
	
	
	
}

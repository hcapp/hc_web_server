package common.pojo;

import common.constatns.ApplicationConstants;

public class ProductVO
{

	private String zipcode;
	private String productName;
	private String productLongDescription;
	private String productImagePath;
	private int lowerLimit;
	private String screenName;
	private Integer retailLocationID;
	private Integer productID;
	private Double price;
	private String productMediaPath;
	private String productShortDescription;
	private String attributeName;
	private String displayValue;
	private Integer radius;
	private String warrantyServiceInformation;
	private Integer hotDealID;
	private Double salePrice;
    private String scanCode;
	private String hotDealName;
	private int rowNumber;
	/**
	 * 
	 */
	private String saleStartDate;
	/**
	 * 
	 */
	private String saleEndDate;
	
	/*private String hotDealShortDesc;
	
	private String hotDealLongDesc;
	
	private String hotDealURL;
	
	private double hotDealPrice;*/
	
	
	
	
	/**
	 * @return the hotDealShortDesc
	 *//*
	public String getHotDealShortDesc()
	{
		return hotDealShortDesc;
	}

	*//**
	 * @param hotDealShortDesc the hotDealShortDesc to set
	 *//*
	public void setHotDealShortDesc(String hotDealShortDesc)
	{
		this.hotDealShortDesc = hotDealShortDesc;
	}

	*//**
	 * @return the hotDealLongDesc
	 *//*
	public String getHotDealLongDesc()
	{
		return hotDealLongDesc;
	}

	*//**
	 * @param hotDealLongDesc the hotDealLongDesc to set
	 *//*
	public void setHotDealLongDesc(String hotDealLongDesc)
	{
		this.hotDealLongDesc = hotDealLongDesc;
	}

	*//**
	 * @return the hotDealURL
	 *//*
	public String getHotDealURL()
	{
		return hotDealURL;
	}

	*//**
	 * @param hotDealURL the hotDealURL to set
	 *//*
	public void setHotDealURL(String hotDealURL)
	{
		this.hotDealURL = hotDealURL;
	}

	*//**
	 * @return the hotDealPrice
	 *//*
	public double getHotDealPrice()
	{
		return hotDealPrice;
	}

	*//**
	 * @param hotDealPrice the hotDealPrice to set
	 *//*
	public void setHotDealPrice(double hotDealPrice)
	{
		this.hotDealPrice = hotDealPrice;
	}
*/
	/**
	 * @return the hotDealName
	 */
	public String getHotDealName()
	{
		return hotDealName;
	}

	/**
	 * @param hotDealName the hotDealName to set
	 */
	public void setHotDealName(String hotDealName)
	{
		this.hotDealName = hotDealName;
	}

	/**
	 * @return the hotDealID
	 */
	public Integer getHotDealID()
	{
		return hotDealID;
	}

	/**
	 * @param hotDealID
	 *            the hotDealID to set
	 */
	public void setHotDealID(Integer hotDealID)
	{
		this.hotDealID = hotDealID;
	}

	/**
	 * @return the price
	 */
	public Double getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Double price)
	{
		this.price = price;
	}

	/**
	 * @return the productMediaPath
	 */
	public String getProductMediaPath()
	{
		return productMediaPath;
	}

	/**
	 * @param productMediaPath
	 *            the productMediaPath to set
	 */
	public void setProductMediaPath(String productMediaPath)
	{
		this.productMediaPath = productMediaPath;
	}

	/**
	 * @return the productShortDescription
	 */
	public String getProductShortDescription()
	{
		return productShortDescription;
	}

	/**
	 * @param productShortDescription
	 *            the productShortDescription to set
	 */
	public void setProductShortDescription(String productShortDescription)
	{
		this.productShortDescription = productShortDescription;
	}

	/**
	 * @return the attributeName
	 */
	public String getAttributeName()
	{
		return attributeName;
	}

	/**
	 * @param attributeName
	 *            the attributeName to set
	 */
	public void setAttributeName(String attributeName)
	{
		this.attributeName = attributeName;
	}

	/**
	 * @return the displayValue
	 */
	public String getDisplayValue()
	{
		return displayValue;
	}

	/**
	 * @param displayValue
	 *            the displayValue to set
	 */
	public void setDisplayValue(String displayValue)
	{
		this.displayValue = displayValue;
	}

	/**
	 * @return the warrantyServiceInformation
	 */
	public String getWarrantyServiceInformation()
	{
		return warrantyServiceInformation;
	}

	/**
	 * @param warrantyServiceInformation
	 *            the warrantyServiceInformation to set
	 */
	public void setWarrantyServiceInformation(String warrantyServiceInformation)
	{
		this.warrantyServiceInformation = warrantyServiceInformation;
	}

	/**
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the productID
	 */
	public Integer getProductID()
	{
		return productID;
	}

	/**
	 * @param productID
	 *            the productID to set
	 */
	public void setProductID(Integer productID)
	{
		this.productID = productID;
	}

	/**
	 * @return the productLongDescription
	 */
	public String getProductLongDescription()
	{
		return productLongDescription;
	}

	/**
	 * @param productLongDescription
	 *            the productLongDescription to set
	 */
	public void setProductLongDescription(String productLongDescription)
	{
		this.productLongDescription = productLongDescription;
	}

	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath()
	{
		return productImagePath;
	}

	/**
	 * @param productImagePath
	 *            the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath)
	{
		if (null == productImagePath || productImagePath.equals("") )
		{
			//this.productImagePath = ApplicationConstants.NOTAPPLICABLE;
            //Changed below code as IE browser showing broken image if it did not find image
            this.productImagePath = ApplicationConstants.BLANKIMAGE;

		}
		else
		{
			this.productImagePath = productImagePath;
		}
	}

	/**
	 * @return the lowerLimit
	 */
	public int getLowerLimit()
	{
		return lowerLimit;
	}

	/**
	 * @param lowerLimit
	 *            the lowerLimit to set
	 */
	public void setLowerLimit(int lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	/**
	 * @return the screenName
	 */
	public String getScreenName()
	{
		return screenName;
	}

	/**
	 * @param screenName
	 *            the screenName to set
	 */
	public void setScreenName(String screenName)
	{
		this.screenName = screenName;
	}

	/**
	 * @return the zipcode
	 */
	public String getZipcode()
	{
		return zipcode;
	}

	/**
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the radius
	 */
	public Integer getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(Integer radius) {
		this.radius = radius;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice()
	{
		return salePrice;
	}

	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(Double salePrice)
	{
		this.salePrice = salePrice;
	}

	public String getScanCode()
	{
		return scanCode;
	}

	public void setScanCode(String scanCode)
	{
		this.scanCode = scanCode;
	}

	public String getSaleStartDate()
	{
		return saleStartDate;
	}

	public void setSaleStartDate(String saleStartDate)
	{
		this.saleStartDate = saleStartDate;
	}

	public String getSaleEndDate()
	{
		return saleEndDate;
	}

	public void setSaleEndDate(String saleEndDate)
	{
		this.saleEndDate = saleEndDate;
	}

	public int getRowNumber()
	{
		return rowNumber;
	}

	public void setRowNumber(int rowNumber)
	{
		this.rowNumber = rowNumber;
	}
	
}

package common.pojo;

/**
 * 
 * @author malathi_lr
 *
 */
public class LocationResult {
	/**
	 * 
	 */
	public String status = null;
	
	/**
	 * 
	 */
	public Integer failureCount = null;

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the failureCount
	 */
	public Integer getFailureCount() {
		return failureCount;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param failureCount the failureCount to set
	 */
	public void setFailureCount(Integer failureCount) {
		this.failureCount = failureCount;
	}
}

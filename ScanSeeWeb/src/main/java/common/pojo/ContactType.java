package common.pojo;

public class ContactType {

	/**
	 * variable declared for contact type id
	 */
	public Integer contactTitleID = 0;
	
	/**
	 * variable declared for contact type
	 */
	public String contactTitle = null;

	/**
	 * @return the contactTitleID
	 */
	public Integer getContactTitleID() {
		return contactTitleID;
	}

	/**
	 * @return the contactTitle
	 */
	public String getContactTitle() {
		return contactTitle;
	}

	/**
	 * @param contactTitleID the contactTitleID to set
	 */
	public void setContactTitleID(Integer contactTitleID) {
		this.contactTitleID = contactTitleID;
	}

	/**
	 * @param contactTitle the contactTitle to set
	 */
	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	
}

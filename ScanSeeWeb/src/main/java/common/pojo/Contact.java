/**
 * 
 */
package common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class Contact {

	/**
	 * identifier field.
	 */
	private Long contactId;
	/**
	 * 
	 */
	private String contactFirstName;  
	/**
	 * 
	 */
	private String contactLastName;
	/**
	 * 
	 */
	private String contactTitle;
	/**
	 * 
	 */
	private String contactType;
	/**
	 * 
	 */
	private String contactPhone;
	/**
	 * 
	 */
	private String contactMobileNo;
	/**
	 * 
	 */
	private String contactEmail;
	/**
	 * 
	 */
	private String createdDate;
	/**
	 * 
	 */
	private Long createdUserID;
	/**
	 * 
	 */
	private String modifiedDate;
	/**
	 * 
	 */
	private Long modifiedUserID;
	/**
	 * 
	 */
	private Users user;
	/**
	 * 
	 */
	private String claimed;
	/**
	 * 
	 */
	private String redeemed;
	
	/**
	 * 
	 */
	public Contact() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param contactId
	 * @param contactFirstName
	 * @param contactLastName
	 * @param contactTitle
	 * @param contactType
	 * @param contactPhone
	 * @param contactMobileNo
	 * @param contactEmail
	 * @param createdDate
	 * @param createdUserID
	 * @param modifiedDate
	 * @param modifiedUserID
	 * @param user
	 */
	public Contact(Long contactId, String contactFirstName,
			String contactLastName, String contactTitle, String contactType,
			String contactPhone, String contactMobileNo, String contactEmail,
			String createdDate, Long createdUserID, String modifiedDate,
			Long modifiedUserID, Users user) {
		super();
		this.contactId = contactId;
		this.contactFirstName = contactFirstName;
		this.contactLastName = contactLastName;
		this.contactTitle = contactTitle;
		this.contactType = contactType;
		this.contactPhone = contactPhone;
		this.contactMobileNo = contactMobileNo;
		this.contactEmail = contactEmail;
		this.createdDate = createdDate;
		this.createdUserID = createdUserID;
		this.modifiedDate = modifiedDate;
		this.modifiedUserID = modifiedUserID;
		this.user = user;
	}
	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}
	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
	/**
	 * @return the contactFirstName
	 */
	public String getContactFirstName() {
		return contactFirstName;
	}
	/**
	 * @param contactFirstName the contactFirstName to set
	 */
	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}
	/**
	 * @return the contactLastName
	 */
	public String getContactLastName() {
		return contactLastName;
	}
	/**
	 * @param contactLastName the contactLastName to set
	 */
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}
	/**
	 * @return the contactTitle
	 */
	public String getContactTitle() {
		return contactTitle;
	}
	/**
	 * @param contactTitle the contactTitle to set
	 */
	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}
	/**
	 * @return the contactType
	 */
	public String getContactType() {
		return contactType;
	}
	/**
	 * @param contactType the contactType to set
	 */
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}
	/**
	 * @return the contactPhone
	 */
	public String getContactPhone() {
		return contactPhone;
	}
	/**
	 * @param contactPhone the contactPhone to set
	 */
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	/**
	 * @return the contactMobileNo
	 */
	public String getContactMobileNo() {
		return contactMobileNo;
	}
	/**
	 * @param contactMobileNo the contactMobileNo to set
	 */
	public void setContactMobileNo(String contactMobileNo) {
		this.contactMobileNo = contactMobileNo;
	}
	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}
	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the createdUserID
	 */
	public Long getCreatedUserID() {
		if (user != null) {
			this.createdUserID = user.getUserID();
		}
		return createdUserID;
	}
	/**
	 * @param createdUserID the createdUserID to set
	 */
	public void setCreatedUserID(Long createdUserID) {
		this.createdUserID = createdUserID;
	}
	/**
	 * @return the modifiedDate
	 */
	public String getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedUserID
	 */
	public Long getModifiedUserID() {
		if (user != null) {
			this.modifiedUserID = user.getUserID();
		}
		return modifiedUserID;
	}
	/**
	 * @param modifiedUserID the modifiedUserID to set
	 */
	public void setModifiedUserID(Long modifiedUserID) {
		this.modifiedUserID = modifiedUserID;
	}
	/**
	 * @return the user
	 */
	public Users getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(Users user) {
		this.user = user;
	}
	/**
	 * @return the claimed
	 */
	public String getClaimed()
	{
		return claimed;
	}
	/**
	 * @param claimed the claimed to set
	 */
	public void setClaimed(String claimed)
	{
		this.claimed = claimed;
	}
	/**
	 * @return the redeemed
	 */
	public String getRedeemed()
	{
		return redeemed;
	}
	/**
	 * @param redeemed the redeemed to set
	 */
	public void setRedeemed(String redeemed)
	{
		this.redeemed = redeemed;
	}
}

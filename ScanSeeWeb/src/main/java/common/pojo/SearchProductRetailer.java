package common.pojo;
/**
 * Bean used for SearchProductRetailer 
 * 
 * @author deepthi_j
 *
 */
public class SearchProductRetailer {

	private String productName;
	private String suggestedRetailerPrice;
	private String productImagePath;
	private String productMediaPath;
	private String productShortDescription;
	private String productLongDescription;
	private String attributeName;
	private String displayValue;
	private String warrantyServiceInformation;
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSuggestedRetailerPrice() {
		return suggestedRetailerPrice;
	}
	public void setSuggestedRetailerPrice(String suggestedRetailerPrice) {
		this.suggestedRetailerPrice = suggestedRetailerPrice;
	}
	public String getProductImagePath() {
		return productImagePath;
	}
	public void setProductImagePath(String productImagePath) {
		this.productImagePath = productImagePath;
	}
	public String getProductMediaPath() {
		return productMediaPath;
	}
	public void setProductMediaPath(String productMediaPath) {
		this.productMediaPath = productMediaPath;
	}
	public String getProductShortDescription() {
		return productShortDescription;
	}
	public void setProductShortDescription(String productShortDescription) {
		this.productShortDescription = productShortDescription;
	}
	public String getProductLongDescription() {
		return productLongDescription;
	}
	public void setProductLongDescription(String productLongDescription) {
		this.productLongDescription = productLongDescription;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getDisplayValue() {
		return displayValue;
	}
	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}
	public String getWarrantyServiceInformation() {
		return warrantyServiceInformation;
	}
	public void setWarrantyServiceInformation(String warrantyServiceInformation) {
		this.warrantyServiceInformation = warrantyServiceInformation;
	}
	
	
	
	
}

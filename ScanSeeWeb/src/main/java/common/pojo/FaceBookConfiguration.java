package common.pojo;

public class FaceBookConfiguration
{

	String scanSeeBaseUrl;
	String appId;
	String appSecretId;

	public String getScanSeeBaseUrl()
	{
		return scanSeeBaseUrl;
	}

	public void setScanSeeBaseUrl(String scanSeeBaseUrl)
	{
		this.scanSeeBaseUrl = scanSeeBaseUrl;
	}

	public String getAppId()
	{
		return appId;
	}

	public void setAppId(String appId)
	{
		this.appId = appId;
	}

	public String getAppSecretId()
	{
		return appSecretId;
	}

	public void setAppSecretId(String appSecretId)
	{
		this.appSecretId = appSecretId;
	}

}

/**
 * Project     : Scan See
 * File        : RetailerLocation.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.constatns.ApplicationConstants;
import common.pojo.Rebates;
import common.util.Utility;

/**
 * Bean used in RetailerLocation.
 * 
 * @author kumar_dodda
 */
public class RetailerLocation {

	private int rowIndex;
	private String checkboxDel;
	private Long uploadRetaillocationsID;

	private String searchKey;

	private String LocID;

	private String retailLocationID;
	/**
	 * 
	 */
	private Long productID;
	/**
	 * 
	 */
	private int retailerLocationID;
	/**
	 * 
	 */
	private Long retailerID;
	/**
	 * 
	 */
	private int headquarters;
	/**
	 * 
	 */
	private String address1;
	/**
	 * 
	 */
	private String address2;
	/**
	 * 
	 */
	private String address3;
	/**
	 * 
	 */
	private String address4;
	/**
	 * 
	 */
	private String city;
	/**
	 * 
	 */
	private String state;
	/**
	 * 
	 */
	private String postalCode;
	/**
	 * 
	 */
	private Long countryID;
	/**
	 * 
	 */
	private Double retailerLocationLatitude;
	/**
	 * 
	 */
	private Double retailerLocationLongitude;
	/**
	 * 
	 */
	private String retailerLocationTimeZone;
	/**
	 * 
	 */
	private String createDate;
	/**
	 * 
	 */
	private String modifyDate;

	/**
     * 
     */
	private CountryCode countryCode;
	/**
     * 
     */
	private Retailer retailer;
	/**
	 * 
	 */
	private Rebates rebates;

	private String rebTermCondtn;
	/**
	 * 
	 */
	private String rebateName;
	/**
     * 
     */
	private String rebateAmt;
	/**
     * 
     */
	private String rebStartDate;
	/**
     * 
     */
	private String rebEndDate;
	/**
     * 
     */
	private String rebStartTime;
	/**
     * 
     */
	private String rebEndTime;
	/**
     * 
     */
	private String rebLongDescription;
	/**
     * 
     */
	private CommonsMultipartFile locationSetUpFile;
	/**
	 * 
	 */
	private String productName;
	/**
	 * 
	 */
	private String storeIdentification;

	/**
	 * 
	 */
	private String storeAddress;

	/**
	 * 
	 */
	private String phonenumber;
	/**
	 * 
	 */
	private String retailLocationStoreHours;

	private String prodJson;
	private String prodJson1;
	private Long retailID;

	private String contactEmail;
	private String contactLastName;
	private String contactTitle;

	private String pageNumber;
	private String pageFlag;

	private String contactFirstName;
	private String contactMobilePhone;
	private String contactType;

	public Integer contactTitleID;

	private String retailerLocID;
	/**
	 * 
	 */
	public String reasonForDiscarding;

	/**
	 * 
	 */
	public String retailLocationUrl;
	/**
	 * 
	 */
	private String retailLocationIDHidden;
	/**
	 * 
	 */
	private String retailLocationIds;
	/**
	 * 
	 */
	private String keyword;
	/**
	 * 
	 */
	private String saleStartDate;
	/**
	 * 
	 */
	private String saleEndDate;
	
	/**
	 * 
	 */
	private Double salePrice;
	/**
	 * 
	 */
	private Double price;
	/**
	 * 
	 */
	private String key;

	private String qrImagePath;

	private String imagName;

	private String recordCount;

	private long totalSize;
	private int currentPage;

	private boolean geoError;
	private String highliteRow;
	
	

	/**
	 * 
	 */
	private String locationImgPath;
	/**
	 * 
	 */
	private String uploadImgName;
	/**
	 * 
	 */
	private CommonsMultipartFile[] imageFile;
	/**
	 * 
	 */
	private String viewName;
	/**
	 * 
	 */
	private String imgLocationPath;
	/**
	 * 
	 */
	private String gridImgLocationPath;
	/**
	 * 
	 */
	private CommonsMultipartFile[] imageFilePath;
	/**
	 * 
	 */
	private boolean uploadImage;

	private Integer TimeZoneID;
	
	/**
	 * 
	 */
	private String storeStartHrs;
	/**
	 * 
	 */
	private String storeStartMins;
	/**
	 * 
	 */
	private String storeEndHrs;
	/**
	 * 
	 */
	private String storeEndMins;
	/**
	 * 
	 */
	private Integer storeTimeZoneId;
	/**
	 * 
	 */
	private String startTimeUTC;
	/**
	 * 
	 */
	private String endTimeUTC;
	/**
	 * 
	 */
	private String startTime;
	/**
	 * 
	 */
	private String endTime;
	/**
	 * 
	 */
	private String timeZoneHidden;
	
	private String weekDayName;

	

	/**
	 * @return the retailLocationUrl
	 */
	public String getRetailLocationUrl() {
		return retailLocationUrl;
	}

	/**
	 * @param retailLocationUrl
	 *            the retailLocationUrl to set
	 */
	public void setRetailLocationUrl(String retailLocationUrl) {
		this.retailLocationUrl = Utility.removeWhiteSpaces(retailLocationUrl);
	}

	/**
	 * @return the reasonForDiscarding
	 */
	public String getReasonForDiscarding() {
		return reasonForDiscarding;
	}

	/**
	 * @param reasonForDiscarding
	 *            the reasonForDiscarding to set
	 */
	public void setReasonForDiscarding(String reasonForDiscarding) {
		this.reasonForDiscarding = reasonForDiscarding;
	}

	/**
	 * @return the uploadRetaillocationsID
	 */
	public Long getUploadRetaillocationsID() {
		return uploadRetaillocationsID;
	}

	private int rebateID;

	/**
	 * @param uploadRetaillocationsID
	 *            the uploadRetaillocationsID to set
	 */
	public void setUploadRetaillocationsID(Long uploadRetaillocationsID) {
		this.uploadRetaillocationsID = uploadRetaillocationsID;
	}

	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * @param contactEmail
	 *            the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * @return the contactLastName
	 */
	public String getContactLastName() {
		return contactLastName;
	}

	/**
	 * @param contactLastName
	 *            the contactLastName to set
	 */
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	/**
	 * @return the contactTitle
	 */
	public String getContactTitle() {
		return contactTitle;
	}

	/**
	 * @param contactTitle
	 *            the contactTitle to set
	 */
	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	/**
	 * @return the prodJson
	 */
	public String getProdJson() {
		return prodJson;
	}

	/**
	 * @param prodJson
	 *            the prodJson to set
	 */
	public void setProdJson(String prodJson) {
		this.prodJson = prodJson;
	}

	/**
	 * @return the retailLocationStoreHours
	 */
	public String getRetailLocationStoreHours() {
		return retailLocationStoreHours;
	}

	/**
	 * @param retailLocationStoreHours
	 *            the retailLocationStoreHours to set
	 */
	public void setRetailLocationStoreHours(String retailLocationStoreHours) {
		this.retailLocationStoreHours = retailLocationStoreHours;
	}

	/**
	 * @return the phonenumber
	 */
	public String getPhonenumber() {
		return phonenumber;
	}

	/**
	 * @param phonenumber
	 *            the phonenumber to set
	 */
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	/**
	 * @return the retailID
	 */
	public Long getRetailID() {
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Long retailID) {
		this.retailID = retailID;
	}

	/**
	 * @return the storeIdentification
	 */
	public String getStoreIdentification() {
		return storeIdentification;
	}

	/**
	 * @param storeIdentification
	 *            the storeIdentification to set
	 */
	public void setStoreIdentification(String storeIdentification) {
		this.storeIdentification = Utility.removeWhiteSpaces(storeIdentification);
	}

	/**
	 * @return the storeAddress
	 */
	public String getStoreAddress() {
		return storeAddress;
	}

	/**
	 * @param storeAddress
	 *            the storeAddress to set
	 */
	public void setStoreAddress(String storeAddress) {
		this.storeAddress = Utility.removeWhiteSpaces(storeAddress);
	}

	/**
	 * @return the locationSetUpFile
	 */
	public CommonsMultipartFile getLocationSetUpFile() {
		return locationSetUpFile;
	}

	/**
	 * @param locationSetUpFile
	 *            the locationSetUpFile to set
	 */
	public void setLocationSetUpFile(CommonsMultipartFile locationSetUpFile) {
		this.locationSetUpFile = locationSetUpFile;
	}

	/**
	 * 
	 */
	public RetailerLocation() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param retailerLocationID
	 * @param retailerID
	 * @param headquarters
	 * @param address1
	 * @param address2
	 * @param address3
	 * @param address4
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param countryID
	 * @param retailerLocationLatitude
	 * @param retailerLocationLongitude
	 * @param retailerLocationTimeZone
	 * @param createDate
	 * @param modifyDate
	 * @param countryCode
	 * @param retailer
	 * @param rebates
	 * @param rebateName
	 * @param rebateAmt
	 * @param rebStartDate
	 * @param rebEndDate
	 * @param rebStartTime
	 * @param rebEndTime
	 * @param rebLongDescription
	 */
	public RetailerLocation(int retailerLocationID, Long retailerID, int headquarters, String address1, String address2, String address3, String address4,
			String city, String state, String postalCode, Long countryID, Double retailerLocationLatitude, Double retailerLocationLongitude,
			String storeStartHrs, String storeStartMins, String storeEndHrs, String storeEndMins, Integer storeTimeZoneId, String startTimeUTC,
			String endTimeUTC, String startTime, String endTime, String weekDayName, String retailerLocationTimeZone, String createDate, String modifyDate,
			CountryCode countryCode, Retailer retailer, Rebates rebates, String rebateName, String rebateAmt, String rebStartDate, String rebEndDate,
			String rebStartTime, String rebEndTime, String rebLongDescription) {
		super();
		this.retailerLocationID = retailerLocationID;
		this.retailerID = retailerID;
		this.headquarters = headquarters;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.address4 = address4;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.countryID = countryID;
		this.retailerLocationLatitude = retailerLocationLatitude;
		this.retailerLocationLongitude = retailerLocationLongitude;
		this.storeStartHrs = storeStartHrs;
		this.storeStartMins = storeStartMins;
		this.storeEndHrs = storeEndHrs;
		this.storeEndMins = storeEndMins;
		this.storeTimeZoneId = storeTimeZoneId;
		this.startTimeUTC = startTimeUTC;
		this.endTimeUTC = endTimeUTC;
		this.startTime = startTime;
		this.endTime = endTime;
		this.weekDayName = weekDayName;
		this.retailerLocationTimeZone = retailerLocationTimeZone;
		this.createDate = createDate;
		this.modifyDate = modifyDate;
		this.countryCode = countryCode;
		this.retailer = retailer;
		this.rebates = rebates;
		this.rebateName = rebateName;
		this.rebateAmt = rebateAmt;
		this.rebStartDate = rebStartDate;
		this.rebEndDate = rebEndDate;
		this.rebStartTime = rebStartTime;
		this.rebEndTime = rebEndTime;
		this.rebLongDescription = rebLongDescription;
	}

	/**
	 * @return the retailerLocationID
	 */
	public int getRetailerLocationID() {
		return retailerLocationID;
	}

	/**
	 * @param retailerLocationID
	 *            the retailerLocationID to set
	 */
	public void setRetailerLocationID(int retailerLocationID) {
		this.retailerLocationID = retailerLocationID;
	}

	/**
	 * @return the retailerID
	 */
	public Long getRetailerID() {
		if (retailer != null) {
			this.retailerID = retailer.getRetailerID();
		}
		return retailerID;
	}

	/**
	 * @param retailerID
	 *            the retailerID to set
	 */
	public void setRetailerID(Long retailerID) {
		this.retailerID = retailerID;
	}

	/**
	 * @return the headquarters
	 */
	public int getHeadquarters() {
		return headquarters;
	}

	/**
	 * @param headquarters
	 *            the headquarters to set
	 */
	public void setHeadquarters(int headquarters) {
		this.headquarters = headquarters;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = Utility.removeWhiteSpaces(address1);
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the address3
	 */
	public String getAddress3() {
		return address3;
	}

	/**
	 * @param address3
	 *            the address3 to set
	 */
	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	/**
	 * @return the address4
	 */
	public String getAddress4() {
		return address4;
	}

	/**
	 * @param address4
	 *            the address4 to set
	 */
	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the countryID
	 */
	public Long getCountryID() {
		return countryID;
	}

	/**
	 * @param countryID
	 *            the countryID to set
	 */
	public void setCountryID(Long countryID) {
		this.countryID = countryID;
	}

	/**
	 * @return the retailerLocationLatitude
	 */
	public Double getRetailerLocationLatitude() {
		return retailerLocationLatitude;
	}

	/**
	 * @param retailerLocationLatitude
	 *            the retailerLocationLatitude to set
	 */
	public void setRetailerLocationLatitude(Double retailerLocationLatitude) {
		this.retailerLocationLatitude = retailerLocationLatitude;
	}

	/**
	 * @return the retailerLocationLongitude
	 */
	public Double getRetailerLocationLongitude() {
		return retailerLocationLongitude;
	}

	/**
	 * @param retailerLocationLongitude
	 *            the retailerLocationLongitude to set
	 */
	public void setRetailerLocationLongitude(Double retailerLocationLongitude) {
		this.retailerLocationLongitude = retailerLocationLongitude;
	}

	/**
	 * @return the retailerLocationTimeZone
	 */
	public String getRetailerLocationTimeZone() {
		return retailerLocationTimeZone;
	}

	/**
	 * @param retailerLocationTimeZone
	 *            the retailerLocationTimeZone to set
	 */
	public void setRetailerLocationTimeZone(String retailerLocationTimeZone) {
		this.retailerLocationTimeZone = retailerLocationTimeZone;
	}

	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the modifyDate
	 */
	public String getModifyDate() {
		return modifyDate;
	}

	/**
	 * @param modifyDate
	 *            the modifyDate to set
	 */
	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

	/**
	 * @return the countryCode
	 */
	public CountryCode getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(CountryCode countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the retailer
	 */
	public Retailer getRetailer() {
		return retailer;
	}

	/**
	 * @param retailer
	 *            the retailer to set
	 */
	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}

	/**
	 * @return the rebates
	 */
	public Rebates getRebates() {
		return rebates;
	}

	/**
	 * @param rebates
	 *            the rebates to set
	 */
	public void setRebates(Rebates rebates) {
		this.rebates = rebates;
	}

	/**
	 * @return the rebateName
	 */
	public String getRebateName() {
		if (rebates != null) {
			this.rebateName = rebates.getRebName();
		}
		return rebateName;
	}

	/**
	 * @param rebateName
	 *            the rebateName to set
	 */
	public void setRebateName(String rebateName) {
		this.rebateName = rebateName;
	}

	/**
	 * @return the rebateAmt
	 */
	public String getRebateAmt() {
		/*
		 * if (rebates != null) { this.rebateAmt =
		 * String.valueOf(rebates.getRebAmount()); }
		 */
		return rebateAmt;
	}

	/**
	 * @param rebateAmt
	 *            the rebateAmt to set
	 */
	public void setRebateAmt(String rebateAmt) {
		this.rebateAmt = rebateAmt;
	}

	/**
	 * @return the rebStartDate
	 */
	public String getRebStartDate() {
		return rebStartDate;
	}

	/**
	 * @param rebStartDate
	 *            the rebStartDate to set
	 */
	public void setRebStartDate(String rebStartDate) {
		this.rebStartDate = rebStartDate;
	}

	/**
	 * @return the rebEndDate
	 */
	public String getRebEndDate() {
		return rebEndDate;
	}

	/**
	 * @param rebEndDate
	 *            the rebEndDate to set
	 */
	public void setRebEndDate(String rebEndDate) {
		this.rebEndDate = rebEndDate;
	}

	/**
	 * @return the rebStartTime
	 */
	public String getRebStartTime() {
		return rebStartTime;
	}

	/**
	 * @param rebStartTime
	 *            the rebStartTime to set
	 */
	public void setRebStartTime(String rebStartTime) {
		this.rebStartTime = rebStartTime;
	}

	/**
	 * @return the rebEndTime
	 */
	public String getRebEndTime() {
		return rebEndTime;
	}

	/**
	 * @param rebEndTime
	 *            the rebEndTime to set
	 */
	public void setRebEndTime(String rebEndTime) {
		this.rebEndTime = rebEndTime;
	}

	/**
	 * @return the rebLongDescription
	 */
	public String getRebLongDescription() {
		if (rebates != null) {
			this.rebLongDescription = rebates.getRebLongDescription();
		}
		return rebLongDescription;
	}

	/**
	 * @param rebLongDescription
	 *            the rebLongDescription to set
	 */
	public void setRebLongDescription(String rebLongDescription) {
		this.rebLongDescription = rebLongDescription;
	}

	public Long getProductID() {
		return productID;
	}

	public void setProductID(Long productID) {
		this.productID = productID;
	}

	public String getLocID() {
		return LocID;
	}

	public void setLocID(String locID) {
		LocID = locID;
	}

	public void setRebateID(int rebateID) {
		this.rebateID = rebateID;
	}

	public int getRebateID() {
		return rebateID;
	}

	public void setRebTermCondtn(String rebTermCondtn) {
		this.rebTermCondtn = rebTermCondtn;
	}

	public String getRebTermCondtn() {
		return rebTermCondtn;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}

	/**
	 * @return the pageNumber
	 */
	public String getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber
	 *            the pageNumber to set
	 */
	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the pageFlag
	 */
	public String getPageFlag() {
		return pageFlag;
	}

	/**
	 * @param pageFlag
	 *            the pageFlag to set
	 */
	public void setPageFlag(String pageFlag) {
		this.pageFlag = pageFlag;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getRetailLocationID() {
		return retailLocationID;
	}

	public void setRetailLocationID(String retailLocationID) {
		this.retailLocationID = retailLocationID;
	}

	public String getCheckboxDel() {
		return checkboxDel;
	}

	public void setCheckboxDel(String checkboxDel) {
		this.checkboxDel = checkboxDel;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactMobilePhone() {
		return contactMobilePhone;
	}

	public void setContactMobilePhone(String contactMobilePhone) {
		this.contactMobilePhone = contactMobilePhone;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public Integer getContactTitleID() {
		return contactTitleID;
	}

	public void setContactTitleID(Integer contactTitleID) {
		this.contactTitleID = contactTitleID;
	}

	/**
	 * @return the retailLocationIDHidden
	 */
	public String getRetailLocationIDHidden() {
		return retailLocationIDHidden;
	}

	/**
	 * @param retailLocationIDHidden
	 *            the retailLocationIDHidden to set
	 */
	public void setRetailLocationIDHidden(String retailLocationIDHidden) {
		this.retailLocationIDHidden = retailLocationIDHidden;
	}

	/**
	 * @return the retailLocationIds
	 */
	public String getRetailLocationIds() {
		return retailLocationIds;
	}

	/**
	 * @param retailLocationIds
	 *            the retailLocationIds to set
	 */
	public void setRetailLocationIds(String retailLocationIds) {
		this.retailLocationIds = retailLocationIds;
	}

	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * @param keyword
	 *            the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = Utility.removeWhiteSpaces(keyword);
	}

	/**
	 * @return the saleStartDate
	 */
	public String getSaleStartDate() {
		return saleStartDate;
	}

	/**
	 * @param saleStartDate
	 *            the saleStartDate to set
	 */
	public void setSaleStartDate(String saleStartDate) {
		this.saleStartDate = saleStartDate;
	}

	/**
	 * @return the saleEndDate
	 */
	public String getSaleEndDate() {
		return saleEndDate;
	}

	/**
	 * @param saleEndDate
	 *            the saleEndDate to set
	 */
	public void setSaleEndDate(String saleEndDate) {
		this.saleEndDate = saleEndDate;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice() {
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;

	}

	public String getRetailerLocID() {
		return retailerLocID;
	}

	public void setRetailerLocID(String retailerLocID) {
		this.retailerLocID = retailerLocID;
	}

	public String getQrImagePath() {
		return qrImagePath;
	}

	public void setQrImagePath(String qrImagePath) {
		this.qrImagePath = qrImagePath;
	}

	public String getImagName() {
		return imagName;
	}

	public void setImagName(String imagName) {
		this.imagName = imagName;
	}

	public String getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}

	/**
	 * @return the totalSize
	 */
	public long getTotalSize() {
		return totalSize;
	}

	/**
	 * @return the currentPage
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param totalSize
	 *            the totalSize to set
	 */
	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}

	/**
	 * @param currentPage
	 *            the currentPage to set
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public boolean isGeoError() {
		return geoError;
	}

	public void setGeoError(boolean geoError) {
		this.geoError = geoError;
	}

	public String getHighliteRow() {
		return highliteRow;
	}

	public void setHighliteRow(String highliteRow) {
		this.highliteRow = highliteRow;
	}

	/**
	 * @return the locationImgPath
	 */
	public String getLocationImgPath() {
		return locationImgPath;
	}

	/**
	 * @param locationImgPath
	 *            the locationImgPath to set
	 */
	public void setLocationImgPath(String locationImgPath) {
		if ("".equals(Utility.checkNull(locationImgPath))) {
			// Changed below code as IE browser showing broken image if it did
			// not find image.
			this.locationImgPath = ApplicationConstants.UPLOADGRIDIMAGE;
		} else {
			this.locationImgPath = locationImgPath;
		}
	}

	/*	*//**
	 * @return the imageFile
	 */
	/*
	 * public CommonsMultipartFile getImageFile() { return imageFile; }
	 *//**
	 * @param imageFile
	 *            the imageFile to set
	 */
	/*
	 * public void setImageFile(CommonsMultipartFile imageFile) { this.imageFile
	 * = imageFile; }
	 */

	/**
	 * @return the viewName
	 */
	public String getViewName() {
		return viewName;
	}

	/**
	 * @return the imageFile
	 */
	public CommonsMultipartFile[] getImageFile() {
		return imageFile;
	}

	/**
	 * @param imageFile
	 *            the imageFile to set
	 */
	public void setImageFile(CommonsMultipartFile[] imageFile) {
		this.imageFile = imageFile;
	}

	/**
	 * @param viewName
	 *            the viewName to set
	 */
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	/**
	 * @return the imageFilePath
	 */
	public CommonsMultipartFile[] getImageFilePath() {
		return imageFilePath;
	}

	/**
	 * @param imageFilePath
	 *            the imageFilePath to set
	 */
	public void setImageFilePath(CommonsMultipartFile[] imageFilePath) {
		this.imageFilePath = imageFilePath;
	}

	/**
	 * @return the uploadImgName
	 */
	public String getUploadImgName() {
		return uploadImgName;
	}

	/**
	 * @param uploadImgName
	 *            the uploadImgName to set
	 */
	public void setUploadImgName(String uploadImgName) {
		if ("".equals(Utility.checkNull(uploadImgName))) {
			this.uploadImgName = uploadImgName;
		} else {
			String extension = null;
			extension = FilenameUtils.getExtension(uploadImgName.trim());
			if (!"".equals(Utility.checkNull(extension))) {
				if (!extension.equals("png")) {
					uploadImgName = FilenameUtils.removeExtension(uploadImgName);
					if (!"".equals(Utility.checkNull(uploadImgName))) {
						this.uploadImgName = uploadImgName + ApplicationConstants.PNGIMAGEFORMAT;
					}
				} else {
					this.uploadImgName = uploadImgName;
				}
			}
		}
	}

	/**
	 * @return the prodJson1
	 */
	public String getProdJson1() {
		return prodJson1;
	}

	/**
	 * @param prodJson1
	 *            the prodJson1 to set
	 */
	public void setProdJson1(String prodJson1) {
		this.prodJson1 = prodJson1;
	}

	/**
	 * @return the imgLocationPath
	 */
	public String getImgLocationPath() {
		return imgLocationPath;
	}

	/**
	 * @param imgLocationPath
	 *            the imgLocationPath to set
	 */
	public void setImgLocationPath(String imgLocationPath) {
		this.imgLocationPath = imgLocationPath;
	}

	/**
	 * @return the uploadImage
	 */
	public boolean isUploadImage() {
		return uploadImage;
	}

	/**
	 * @param uploadImage
	 *            the uploadImage to set
	 */
	public void setUploadImage(boolean uploadImage) {
		this.uploadImage = uploadImage;
	}

	/**
	 * @return the gridImgLocationPath
	 */
	public String getGridImgLocationPath() {
		return gridImgLocationPath;
	}

	/**
	 * @param gridImgLocationPath
	 *            the gridImgLocationPath to set
	 */
	public void setGridImgLocationPath(String gridImgLocationPath) {
		this.gridImgLocationPath = gridImgLocationPath;
	}

	/**
	 * @return the rowIndex
	 */
	public int getRowIndex() {
		return rowIndex;
	}

	/**
	 * @param rowIndex
	 *            the rowIndex to set
	 */
	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}
	
	public Integer getTimeZoneID() {
		return TimeZoneID;
	}

	public void setTimeZoneID(Integer timeZoneID) {
		TimeZoneID = timeZoneID;
		storeTimeZoneId = timeZoneID;
	}
	
	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}
	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}
	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	

	/**
	 * @return the storeStartHrs
	 */
	public String getStoreStartHrs() {
		return storeStartHrs;
	}

	/**
	 * @param storeStartHrs
	 *            the storeStartHrs to set
	 */
	public void setStoreStartHrs(String storeStartHrs) {
		this.storeStartHrs = storeStartHrs;
	}

	/**
	 * @return the storeStartMins
	 */
	public String getStoreStartMins() {
		return storeStartMins;
	}

	/**
	 * @param storeStartMins
	 *            the storeStartMins to set
	 */
	public void setStoreStartMins(String storeStartMins) {
		this.storeStartMins = storeStartMins;
	}

	/**
	 * @return the storeEndHrs
	 */
	public String getStoreEndHrs() {
		return storeEndHrs;
	}

	/**
	 * @param storeEndHrs
	 *            the storeEndHrs to set
	 */
	public void setStoreEndHrs(String storeEndHrs) {
		this.storeEndHrs = storeEndHrs;
	}

	/**
	 * @return the storeEndMins
	 */
	public String getStoreEndMins() {
		return storeEndMins;
	}

	/**
	 * @param storeEndMins
	 *            the storeEndMins to set
	 */
	public void setStoreEndMins(String storeEndMins) {
		this.storeEndMins = storeEndMins;
	}

	/**
	 * @return the storeTimeZoneId
	 */
	public Integer getStoreTimeZoneId() {
		return storeTimeZoneId;
	}

	/**
	 * @param storeTimeZoneId
	 *            the storeTimeZoneId to set
	 */
	public void setStoreTimeZoneId(Integer storeTimeZoneId) {
		this.storeTimeZoneId = storeTimeZoneId;
	}

	/**
	 * @return the startTimeUTC
	 */
	public String getStartTimeUTC() {
		return startTimeUTC;
	}

	/**
	 * @param startTimeUTC
	 *            the startTimeUTC to set
	 */
	public void setStartTimeUTC(String startTimeUTC) {
		this.startTimeUTC = startTimeUTC;
	}

	/**
	 * @return the endTimeUTC
	 */
	public String getEndTimeUTC() {
		return endTimeUTC;
	}

	/**
	 * @param endTimeUTC
	 *            the endTimeUTC to set
	 */
	public void setEndTimeUTC(String endTimeUTC) {
		this.endTimeUTC = endTimeUTC;
	}
	/**
	 * @return the timeZoneHidden
	 */
	public String getTimeZoneHidden() {
		return timeZoneHidden;
	}
	/**
	 * @param timeZoneHidden
	 *            the timeZoneHidden to set
	 */
	public void setTimeZoneHidden(String timeZoneHidden) {
		this.timeZoneHidden = timeZoneHidden;
	}

	public String getWeekDayName() {
		return weekDayName;
	}

	public void setWeekDayName(String weekDayName) {
		this.weekDayName = weekDayName;
	}

}

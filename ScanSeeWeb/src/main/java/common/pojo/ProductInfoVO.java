package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ProductInfoVO
{
		
	private CommonsMultipartFile [] imageFile;
	private CommonsMultipartFile [] audioFile;
	private CommonsMultipartFile [] videoFile;
	
	private String productUPC;
	private String productName;
	private String modelNumber;
	private long productID;
	

	private String suggestedRetailPrice;
	private String longDescription;
	private String warrantyORService;
	private String shortDescription;
	
	private String productCategory;
	
	private String attributes;
	
	private String imagePaths;
	
	private String audioFilePath;
	
	private String videoFilePath;
	
	private String prdAttributes;
	
	private String values;
	
	private String btnType;
	/*
	 * This variable Stores the Dynamic Html generated for Attributes.
	 */
	private String attributeHtml;
	
	private String defaultImagePath;
	/**
	 * @return the modelNumber
	 */
	public String getModelNumber() {
		return modelNumber;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	/**
	 * @param modelNumber the modelNumber to set
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	
	
	/**
	 * @return the imageFile
	 */
	public CommonsMultipartFile[] getImageFile()
	{
		return imageFile;
	}
	/**
	 * @param imageFile the imageFile to set
	 */
	public void setImageFile(CommonsMultipartFile[] imageFile)
	{
		this.imageFile = imageFile;
	}
	
	
	/**
	 * @return the audioFile
	 */
	public CommonsMultipartFile[] getAudioFile()
	{
		return audioFile;
	}
	/**
	 * @param audioFile the audioFile to set
	 */
	public void setAudioFile(CommonsMultipartFile[] audioFile)
	{
		this.audioFile = audioFile;
	}
	/**
	 * @return the videoFile
	 */
	public CommonsMultipartFile[] getVideoFile()
	{
		return videoFile;
	}
	/**
	 * @param videoFile the videoFile to set
	 */
	public void setVideoFile(CommonsMultipartFile[] videoFile)
	{
		this.videoFile = videoFile;
	}
	public String getProductUPC()
	{
		return productUPC;
	}
	public void setProductUPC(String productUPC)
	{
		this.productUPC = productUPC;
	}
	public String getProductName()
	{
		return productName;
	}
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	
	
	
	public String getSuggestedRetailPrice()
	{
		return suggestedRetailPrice;
	}
	public void setSuggestedRetailPrice(String suggestedRetailPrice)
	{
		this.suggestedRetailPrice = suggestedRetailPrice;
	}
	public String getLongDescription()
	{
		return longDescription;
	}
	public void setLongDescription(String longDescription)
	{
		this.longDescription = longDescription;
	}
	public String getWarrantyORService()
	{
		return warrantyORService;
	}
	public void setWarrantyORService(String warrantyORService)
	{
		this.warrantyORService = warrantyORService;
	}
	public String getShortDescription()
	{
		return shortDescription;
	}
	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}
	/**
	 * @return the attributes
	 */
	public String getAttributes()
	{
		return attributes;
	}
	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(String attributes)
	{
		this.attributes = attributes;
	}
	/**
	 * @return the imagePaths
	 */
	public String getImagePaths()
	{
		return imagePaths;
	}
	/**
	 * @param imagePaths the imagePaths to set
	 */
	public void setImagePaths(String imagePaths)
	{
		this.imagePaths = imagePaths;
	}
	/**
	 * @return the audioFilePath
	 */
	public String getAudioFilePath()
	{
		return audioFilePath;
	}
	/**
	 * @param audioFilePath the audioFilePath to set
	 */
	public void setAudioFilePath(String audioFilePath)
	{
		this.audioFilePath = audioFilePath;
	}
	/**
	 * @return the videoFilePath
	 */
	public String getVideoFilePath()
	{
		return videoFilePath;
	}
	/**
	 * @param videoFilePath the videoFilePath to set
	 */
	public void setVideoFilePath(String videoFilePath)
	{
		this.videoFilePath = videoFilePath;
	}
	/**
	 * @return the prdAttributes
	 */
	public String getPrdAttributes()
	{
		return prdAttributes;
	}
	/**
	 * @param prdAttributes the prdAttributes to set
	 */
	public void setPrdAttributes(String prdAttributes)
	{
		this.prdAttributes = prdAttributes;
	}
	/**
	 * @return the values
	 */
	public String getValues()
	{
		return values;
	}
	/**
	 * @param values the values to set
	 */
	public void setValues(String values)
	{
		this.values = values;
	}
	/**
	 * @return the attributeHtml
	 */
	public String getAttributeHtml()
	{
		return attributeHtml;
	}
	/**
	 * @param attributeHtml the attributeHtml to set
	 */
	public void setAttributeHtml(String attributeHtml)
	{
		this.attributeHtml = attributeHtml;
	}
	/**
	 * @return the defaultImagePath
	 */
	public String getDefaultImagePath()
	{
		return defaultImagePath;
	}
	/**
	 * @param defaultImagePath the defaultImagePath to set
	 */
	public void setDefaultImagePath(String defaultImagePath)
	{
		this.defaultImagePath = defaultImagePath;
	}
	public void setProductID(long productID) {
		this.productID = productID;
	}
	public long getProductID() {
		return productID;
	}
	public void setBtnType(String btnType) {
		this.btnType = btnType;
	}
	public String getBtnType() {
		return btnType;
	}
	

	
}

/**
 * Project     : Scan See
 * File        : RetailerContact.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;

/**
 * Bean used in RetailerContact.
 * 
 * @author kumar_dodda
 */
public class RetailerContact
{
	/**
	 * 
	 */
	private Long retailContactID;
	/**
	 * 
	 */
	private Long contactID;
	/**
	 * 
	 */
	private Long retailLocationID;
	/**
	 * 
	 */
	private String contactLName;
	/**
	 * 
	 */
	private String title;
	/**
	 * 
	 */
	private String email;
	/**
	 * 
	 */
	private String phone;
	/**
	 * 
	 */
	private String mobilePhone;
	/**
	 * 
	 */
	private Contact contact;
	/**
	 * 
	 */
	private RetailerLocation retailerLocation;

	/**
	 * 
	 */
	public RetailerContact()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param retailContactID
	 * @param contactID
	 * @param retailLocationID
	 * @param contactLName
	 * @param title
	 * @param email
	 * @param phone
	 * @param mobilePhone
	 * @param contact
	 * @param retailerLocation
	 */
	public RetailerContact(Long retailContactID, Long contactID, Long retailLocationID, String contactLName, String title, String email,
			String phone, String mobilePhone, Contact contact, RetailerLocation retailerLocation)
	{
		super();
		this.retailContactID = retailContactID;
		this.contactID = contactID;
		this.retailLocationID = retailLocationID;
		this.contactLName = contactLName;
		this.title = title;
		this.email = email;
		this.phone = phone;
		this.mobilePhone = mobilePhone;
		this.contact = contact;
		this.retailerLocation = retailerLocation;
	}

	/**
	 * @return the retailContactID
	 */
	public Long getRetailContactID()
	{
		return retailContactID;
	}

	/**
	 * @param retailContactID
	 *            the retailContactID to set
	 */
	public void setRetailContactID(Long retailContactID)
	{
		this.retailContactID = retailContactID;
	}

	/**
	 * @return the contactID
	 */
	public Long getContactID()
	{
		return contactID;
	}

	/**
	 * @param contactID
	 *            the contactID to set
	 */
	public void setContactID(Long contactID)
	{
		this.contactID = contactID;
	}

	/**
	 * @return the retailLocationID
	 */
	public Long getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(Long retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the contactLName
	 */
	public String getContactLName()
	{
		return contactLName;
	}

	/**
	 * @param contactLName
	 *            the contactLName to set
	 */
	public void setContactLName(String contactLName)
	{
		this.contactLName = contactLName;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone()
	{
		return mobilePhone;
	}

	/**
	 * @param mobilePhone
	 *            the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact()
	{
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	/**
	 * @return the retailerLocation
	 */
	public RetailerLocation getRetailerLocation()
	{
		return retailerLocation;
	}

	/**
	 * @param retailerLocation
	 *            the retailerLocation to set
	 */
	public void setRetailerLocation(RetailerLocation retailerLocation)
	{
		this.retailerLocation = retailerLocation;
	}

}

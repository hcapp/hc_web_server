/**
 * 
 */
package common.pojo;

import java.util.List;

import common.util.*;

/**
 * @author kumar_dodda
 */
public class RetailerRegistration
{

	private Long retailID;

	private int userId;

	private char CorporatePhoneNo;

	private String storeNum;
	/**
	 * 
	 */
	private String retailerName;
	/**
	 * 
	 */
	private String address1;
	/**
	 * 
	 */
	private String address2;
	/**
	 * 
	 */
	private String city;
	/**
	 * 
	 */
	private String state;
	/**
	 * 
	 */
	private String stateHidden;
	/**
	 * 
	 */
	private String stateCodeHidden;
	/**
	 * 
	 */
	private String postalCode;
	/**
	 * 
	 */
	private String contactPhone;
	/**
	 * 
	 */
	private String legalAuthorityFName;
	/**
	 * 
	 */
	private String legalAuthorityLName;
	/**
	 * for contact first name
	 */
	private String contactFName;
	/**
	 * for contact last name
	 */
	private String contactLName;
	/**
	 * for contact phone number
	 */
	private String contactPhoneNo;
	/**
	 * for contact email
	 */
	private String contactEmail;
	/**
	 * for password
	 */
	private String password;
	/**
	 * 
	 */
	private Long adminFlag;
	/**
	 * 
	 */
	private Retailer retailer;
	/**
	 * 
	 */
	private Users user;
	/**
	 * 
	 */
	private RetailerContact retailerContact;
	/**
	 * 
	 */

	private UserRetailer userRetailer;

	/**
	 * 
	 */
	private Contact contact;
	/**
	 * 
	 */
	private String retypeEmail;
	/**
	 * 
	 */
	private String retypePassword;
	/**
	 * 
	 */
	private boolean terms;
	/**
	 * 
	 */
	public String clear;
	/**
	 * 
	 */
	public String submit;
	/**
	 * 
	 */
	private String bCategory;
	/**
	 * 
	 */
	private boolean nonProfit;
	/**
	 * 
	 */
	private boolean islocation;

	private String tabIndex;
	/**
	 * 
	 */
	private String bCategoryHidden;
	/**
	 * 
	 */
	private String cityHidden;

	/**
	 * 
	 */
	private String webUrl = null;
	/**
	 * 
	 */
	private Long numOfStores;
	/**
	 * @return the retailID
	 */
	private String scanSeeUrl;

	private String captch;
	/**
	 * 
	 */
	private String keyword;
	/**
	 * 
	 */
	private Double retailerLocationLatitude;
	/**
	 * 
	 */
	private Double retailerLocationLongitude;

	private String referralName;

	private String response;

	private String associateOrg;

	private Long retailLocId;

	List<Retailer> duplicateRetList;

	private String citySelectedFlag;

	private Integer duplicateRetId;
	private Integer duplicateRetLocId;
	private String flagDuplicateRet;
	/**
	 * 
	 */
	private String geoError;
	
	private boolean geoErr;
	
	/**
	 * 
	 */
	private String filters;
	
	/**
	 * 
	 */
	private String filterValues;
	/**
	 * 
	 */
	private String filterCategory;
	/**
	 * 
	 */
	private String hiddenFilterCategory;
	/**
	 * 
	 */
	private String subCategories;	
	/**
	 * 
	 */
	private String subCategory;
	/**
	 * 
	 */
	private String hiddensubCategory;
	
	private String hiddensubCategies;
	
	public Long getRetailID()
	{
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Long retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * @return the corporatePhoneNo
	 */
	public char getCorporatePhoneNo()
	{
		return CorporatePhoneNo;
	}

	/**
	 * @param corporatePhoneNo
	 *            the corporatePhoneNo to set
	 */
	public void setCorporatePhoneNo(char corporatePhoneNo)
	{
		CorporatePhoneNo = corporatePhoneNo;
	}

	/**
	 * @return the retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * @param retailerName
	 *            the retailerName to set
	 */
	public void setRetailerName(String retailerName)
	{
		this.retailerName = Utility.removeWhiteSpaces(retailerName);
	}

	/**
	 * @return the address1
	 */
	public String getAddress1()
	{
		return address1;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1)
	{
		this.address1 = Utility.removeWhiteSpaces(address1);
	}

	/**
	 * @return the address2
	 */
	public String getAddress2()
	{
		return address2;
	}

	/**
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2)
	{
		this.address2 = Utility.removeWhiteSpaces(address2);
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the contactPhone
	 */
	public String getContactPhone()
	{
		return contactPhone;
	}

	/**
	 * @param contactPhone
	 *            the contactPhone to set
	 */
	public void setContactPhone(String contactPhone)
	{
		this.contactPhone = contactPhone;
	}

	/**
	 * @return the legalAuthorityFName
	 */
	public String getLegalAuthorityFName()
	{
		return legalAuthorityFName;
	}

	/**
	 * @param legalAuthorityFName
	 *            the legalAuthorityFName to set
	 */
	public void setLegalAuthorityFName(String legalAuthorityFName)
	{
		this.legalAuthorityFName = legalAuthorityFName;
	}

	/**
	 * @return the legalAuthorityLName
	 */
	public String getLegalAuthorityLName()
	{
		return legalAuthorityLName;
	}

	/**
	 * @param legalAuthorityLName
	 *            the legalAuthorityLName to set
	 */
	public void setLegalAuthorityLName(String legalAuthorityLName)
	{
		this.legalAuthorityLName = legalAuthorityLName;
	}

	/**
	 * @return the contactFName
	 */
	public String getContactFName()
	{
		return contactFName;
	}

	/**
	 * @param contactFName
	 *            the contactFName to set
	 */
	public void setContactFName(String contactFName)
	{
		this.contactFName = Utility.removeWhiteSpaces(contactFName);
	}

	/**
	 * @return the contactLName
	 */
	public String getContactLName()
	{
		return contactLName;
	}

	/**
	 * @param contactLName
	 *            the contactLName to set
	 */
	public void setContactLName(String contactLName)
	{
		this.contactLName = Utility.removeWhiteSpaces(contactLName);
	}

	/**
	 * @return the contactPhoneNo
	 */
	public String getContactPhoneNo()
	{
		return contactPhoneNo;
	}

	/**
	 * @param contactPhoneNo
	 *            the contactPhoneNo to set
	 */
	public void setContactPhoneNo(String contactPhoneNo)
	{
		this.contactPhoneNo = contactPhoneNo;
	}

	/**
	 * @return the contactEmail
	 */
	public String getContactEmail()
	{
		return contactEmail;
	}

	/**
	 * @param contactEmail
	 *            the contactEmail to set
	 */
	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = Utility.removeWhiteSpaces(contactEmail);

	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the adminFlag
	 */
	public Long getAdminFlag()
	{
		return adminFlag;
	}

	/**
	 * @param adminFlag
	 *            the adminFlag to set
	 */
	public void setAdminFlag(Long adminFlag)
	{
		this.adminFlag = adminFlag;
	}

	/**
	 * @return the retailer
	 */
	public Retailer getRetailer()
	{
		return retailer;
	}

	/**
	 * @param retailer
	 *            the retailer to set
	 */
	public void setRetailer(Retailer retailer)
	{
		this.retailer = retailer;
	}

	/**
	 * @return the user
	 */
	public Users getUser()
	{
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(Users user)
	{
		this.user = user;
	}

	/**
	 * @return the retailerContact
	 */
	public RetailerContact getRetailerContact()
	{
		return retailerContact;
	}

	/**
	 * @param retailerContact
	 *            the retailerContact to set
	 */
	public void setRetailerContact(RetailerContact retailerContact)
	{
		this.retailerContact = retailerContact;
	}

	/**
	 * @return the userRetailer
	 */
	public UserRetailer getUserRetailer()
	{
		return userRetailer;
	}

	/**
	 * @param userRetailer
	 *            the userRetailer to set
	 */
	public void setUserRetailer(UserRetailer userRetailer)
	{
		this.userRetailer = userRetailer;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact()
	{
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	/**
	 * @return the retypeEmail
	 */
	public String getRetypeEmail()
	{
		return retypeEmail;
	}

	/**
	 * @param retypeEmail
	 *            the retypeEmail to set
	 */
	public void setRetypeEmail(String retypeEmail)
	{
		this.retypeEmail = retypeEmail;
	}

	/**
	 * @return the retypePassword
	 */
	public String getRetypePassword()
	{
		return retypePassword;
	}

	/**
	 * @param retypePassword
	 *            the retypePassword to set
	 */
	public void setRetypePassword(String retypePassword)
	{
		this.retypePassword = retypePassword;
	}

	/**
	 * @return the terms
	 */
	public boolean isTerms()
	{
		return terms;
	}

	/**
	 * @param terms
	 *            the terms to set
	 */
	public void setTerms(boolean terms)
	{
		this.terms = terms;
	}

	/**
	 * @return the clear
	 */
	public String getClear()
	{
		return clear;
	}

	/**
	 * @param clear
	 *            the clear to set
	 */
	public void setClear(String clear)
	{
		this.clear = clear;
	}

	/**
	 * @return the submit
	 */
	public String getSubmit()
	{
		return submit;
	}

	/**
	 * @param submit
	 *            the submit to set
	 */
	public void setSubmit(String submit)
	{
		this.submit = submit;
	}

	/**
	 * @return the bCategory
	 */
	public String getbCategory()
	{
		return bCategory;
	}

	/**
	 * @param bCategory
	 *            the bCategory to set
	 */
	public void setbCategory(String bCategory)
	{
		this.bCategory = bCategory;
	}

	/**
	 * @return the nonProfit
	 */
	public boolean isNonProfit()
	{
		return nonProfit;
	}

	/**
	 * @param nonProfit
	 *            the nonProfit to set
	 */
	public void setNonProfit(boolean nonProfit)
	{
		this.nonProfit = nonProfit;
	}

	/**
	 * @return the tabIndex
	 */
	public String getTabIndex()
	{
		return tabIndex;
	}

	/**
	 * @param tabIndex
	 *            the tabIndex to set
	 */
	public void setTabIndex(String tabIndex)
	{
		this.tabIndex = tabIndex;
	}

	/**
	 * @return the bCategoryHidden
	 */
	public String getbCategoryHidden()
	{
		return bCategoryHidden;
	}

	/**
	 * @param bCategoryHidden
	 *            the bCategoryHidden to set
	 */
	public void setbCategoryHidden(String bCategoryHidden)
	{
		this.bCategoryHidden = bCategoryHidden;
	}

	/**
	 * @return the cityHidden
	 */
	public String getCityHidden()
	{
		return cityHidden;
	}

	/**
	 * @param cityHidden
	 *            the cityHidden to set
	 */
	public void setCityHidden(String cityHidden)
	{
		this.cityHidden = cityHidden;
	}

	/**
	 * 
	 */
	private String userName;

	/**
	 * 
	 */
	public RetailerRegistration()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param retailID
	 * @param corporatePhoneNo
	 * @param retailerName
	 * @param address1
	 * @param address2
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param contactPhone
	 * @param legalAuthorityFName
	 * @param legalAuthorityLName
	 * @param contactFName
	 * @param contactLName
	 * @param contactPhoneNo
	 * @param contactEmail
	 * @param password
	 * @param adminFlag
	 * @param retailer
	 * @param user
	 * @param retailerContact
	 * @param userRetailer
	 * @param contact
	 * @param retypeEmail
	 * @param retypePassword
	 * @param terms
	 * @param clear
	 * @param submit
	 * @param bCategory
	 * @param nonProfit
	 * @param tabIndex
	 * @param bCategoryHidden
	 * @param cityHidden
	 * @param colrporateAndSore
	 */
	public RetailerRegistration(Long retailID, char corporatePhoneNo, String retailerName, String address1, String address2, String city,
			String state, String postalCode, String contactPhone, String legalAuthorityFName, String legalAuthorityLName, String contactFName,
			String contactLName, String contactPhoneNo, String contactEmail, String password, Long adminFlag, Retailer retailer, Users user,
			RetailerContact retailerContact, UserRetailer userRetailer, Contact contact, String retypeEmail, String retypePassword, boolean terms,
			String clear, String submit, String bCategory, boolean nonProfit, String tabIndex, String bCategoryHidden, String cityHidden)
	{
		super();
		this.retailID = retailID;
		CorporatePhoneNo = corporatePhoneNo;
		this.retailerName = retailerName;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.contactPhone = contactPhone;
		this.legalAuthorityFName = legalAuthorityFName;
		this.legalAuthorityLName = legalAuthorityLName;
		this.contactFName = contactFName;
		this.contactLName = contactLName;
		this.contactPhoneNo = contactPhoneNo;
		this.contactEmail = contactEmail;
		this.password = password;
		this.adminFlag = adminFlag;
		this.retailer = retailer;
		this.user = user;
		this.retailerContact = retailerContact;
		this.userRetailer = userRetailer;
		this.contact = contact;
		this.retypeEmail = retypeEmail;
		this.retypePassword = retypePassword;
		this.terms = terms;
		this.clear = clear;
		this.submit = submit;
		this.bCategory = bCategory;
		this.nonProfit = nonProfit;
		this.tabIndex = tabIndex;
		this.bCategoryHidden = bCategoryHidden;
		this.cityHidden = cityHidden;

	}

	public boolean isIslocation()
	{
		return islocation;
	}

	public void setIslocation(boolean islocation)
	{
		this.islocation = islocation;
	}

	/**
	 * @return the scanSeeUrl
	 */
	public String getScanSeeUrl()
	{
		return scanSeeUrl;
	}

	/**
	 * @param scanSeeUrl
	 *            the scanSeeUrl to set
	 */
	public void setScanSeeUrl(String scanSeeUrl)
	{
		this.scanSeeUrl = scanSeeUrl;
	}

	/**
	 * @return the storeNum
	 */
	public String getStoreNum()
	{
		return storeNum;
	}

	/**
	 * @param storeNum
	 *            the storeNum to set
	 */
	public void setStoreNum(String storeNum)
	{
		this.storeNum = storeNum;
	}

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = Utility.removeWhiteSpaces(userName);
	}

	/**
	 * @return the webUrl
	 */
	public String getWebUrl()
	{
		return webUrl;
	}

	/**
	 * @param webUrl
	 *            the webUrl to set
	 */
	public void setWebUrl(String webUrl)
	{
		this.webUrl = Utility.removeWhiteSpaces(webUrl);
	}

	/**
	 * @return the numOfStores
	 */
	public Long getNumOfStores()
	{
		return numOfStores;
	}

	/**
	 * @param numOfStores
	 *            the numOfStores to set
	 */
	public void setNumOfStores(Long numOfStores)
	{
		this.numOfStores = numOfStores;
	}

	public String getCaptch()
	{
		return captch;
	}

	public void setCaptch(String captch)
	{
		this.captch = captch;
	}

	/**
	 * @return the keyword
	 */
	public String getKeyword()
	{
		return keyword;
	}

	/**
	 * @param keyword
	 *            the keyword to set
	 */
	public void setKeyword(String keyword)
	{
		this.keyword = Utility.removeWhiteSpaces(keyword);
	}

	/**
	 * @return the retailerLocationLatitude
	 */
	public Double getRetailerLocationLatitude()
	{
		return retailerLocationLatitude;
	}

	/**
	 * @param retailerLocationLatitude
	 *            the retailerLocationLatitude to set
	 */
	public void setRetailerLocationLatitude(Double retailerLocationLatitude)
	{
		this.retailerLocationLatitude = retailerLocationLatitude;
	}

	/**
	 * @return the retailerLocationLongitude
	 */
	public Double getRetailerLocationLongitude()
	{
		return retailerLocationLongitude;
	}

	/**
	 * @param retailerLocationLongitude
	 *            the retailerLocationLongitude to set
	 */
	public void setRetailerLocationLongitude(Double retailerLocationLongitude)
	{
		this.retailerLocationLongitude = retailerLocationLongitude;
	}

	public String getReferralName()
	{
		return referralName;
	}

	public void setReferralName(String referralName)
	{
		this.referralName = referralName;
	}

	public String getResponse()
	{
		return response;
	}

	public void setResponse(String response)
	{
		this.response = response;
	}

	public int getUserId()
	{
		return userId;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	public String getAssociateOrg()
	{
		return associateOrg;
	}

	public void setAssociateOrg(String associateOrg)
	{
		this.associateOrg = associateOrg;
	}

	public List<Retailer> getDuplicateRetList()
	{
		return duplicateRetList;
	}

	public void setDuplicateRetList(List<Retailer> duplicateRetList)
	{
		this.duplicateRetList = duplicateRetList;
	}

	public Long getRetailLocId()
	{
		return retailLocId;
	}

	public void setRetailLocId(Long retailLocId)
	{
		this.retailLocId = retailLocId;
	}

	public String getStateHidden()
	{
		return stateHidden;
	}

	public void setStateHidden(String stateHidden)
	{
		this.stateHidden = stateHidden;
	}

	public String getStateCodeHidden()
	{
		return stateCodeHidden;
	}

	public void setStateCodeHidden(String stateCodeHidden)
	{
		this.stateCodeHidden = stateCodeHidden;
	}

	/**
	 * @param citySelectedFlag
	 *            the citySelectedFlag to set
	 */
	public void setCitySelectedFlag(String citySelectedFlag)
	{
		this.citySelectedFlag = citySelectedFlag;
	}

	/**
	 * @return the citySelectedFlag
	 */
	public String getCitySelectedFlag()
	{
		return citySelectedFlag;
	}

	public Integer getDuplicateRetId()
	{
		return duplicateRetId;
	}

	public void setDuplicateRetId(Integer duplicateRetId)
	{
		this.duplicateRetId = duplicateRetId;
	}

	public Integer getDuplicateRetLocId()
	{
		return duplicateRetLocId;
	}

	public void setDuplicateRetLocId(Integer duplicateRetLocId)
	{
		this.duplicateRetLocId = duplicateRetLocId;
	}

	public String getFlagDuplicateRet()
	{
		return flagDuplicateRet;
	}

	public void setFlagDuplicateRet(String flagDuplicateRet)
	{
		this.flagDuplicateRet = flagDuplicateRet;
	}

	/**
	 * @return the geoError
	 */
	public String getGeoError()
	{
		return geoError;
	}

	/**
	 * @param geoError the geoError to set
	 */
	public void setGeoError(String geoError)
	{
		this.geoError = geoError;
	}

	public boolean isGeoErr()
	{
		return geoErr;
	}

	public void setGeoErr(boolean geoErr)
	{
		this.geoErr = geoErr;
	}

	/**
	 * @return the filters
	 */
	public String getFilters() {
		return filters;
	}
	/**
	 * @param filters the filters to set
	 */
	public void setFilters(String filters) {
		this.filters = filters;
	}
	/**
	 * @return the filterValues
	 */
	public String getFilterValues() {
		return filterValues;
	}
	/**
	 * @param filterValues the filterValues to set
	 */
	public void setFilterValues(String filterValues) {
		this.filterValues = filterValues;
	}

	/**
	 * @return the filterCategory
	 */
	public String getFilterCategory() {
		return filterCategory;
	}

	/**
	 * @param filterCategory the filterCategory to set
	 */
	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}

	/**
	 * @return the hiddenFilterCategory
	 */
	public String getHiddenFilterCategory() {
		return hiddenFilterCategory;
	}

	/**
	 * @param hiddenFilterCategory the hiddenFilterCategory to set
	 */
	public void setHiddenFilterCategory(String hiddenFilterCategory) {
		this.hiddenFilterCategory = hiddenFilterCategory;
	}
	
	/**
	 * @return the subCategories
	 */
	public String getSubCategories() {
		return subCategories;
	}
	/**
	 * @param subCategories the subCategories to set
	 */
	public void setSubCategories(String subCategories) {
		this.subCategories = subCategories;
	}
	/**
	 * @return the subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}
	/**
	 * @param subCategory the subCategory to set
	 */
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	/**
	 * @return the hiddensubCategory
	 */
	public String getHiddensubCategory() {
		return hiddensubCategory;
	}
	/**
	 * @param hiddensubCategory the hiddensubCategory to set
	 */
	public void setHiddensubCategory(String hiddensubCategory) {
		this.hiddensubCategory = hiddensubCategory;
	}

	/**
	 * @return the hiddensubCategies
	 */
	public String getHiddensubCategies() {
		return hiddensubCategies;
	}

	/**
	 * @param hiddensubCategies the hiddensubCategies to set
	 */
	public void setHiddensubCategies(String hiddensubCategies) {
		this.hiddensubCategies = hiddensubCategies;
	}
}
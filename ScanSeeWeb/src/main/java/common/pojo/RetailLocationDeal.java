/**
 * Project     : Scan See
 * File        : RetailLocationDeal.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;

/**
 * Bean used in RetailLocationDeal.
 * 
 * @author kumar_dodda
 */
public class RetailLocationDeal
{
	/**
	 * 
	 */
	private Long retailLocationDealID;
	/**
	 * 
	 */
	private Long retailLocationID;
	/**
	 * 
	 */
	private Long productID;
	/**
	 * 
	 */
	private String retailLocationProductDesc;
	/**
	 * 
	 */
	private int quantity;
	/**
	 * 
	 */
	private Double price;
	/**
	 * 
	 */
	private Double salePrice;
	/**
	 * 
	 */
	private String saleStartDate;
	/**
	 * 
	 */
	private String saleEndDate;
	/**
	 * 
	 */
	private String createdDate;
	/**
	 * 
	 */
	private String modifiedDate;
	/**
	 * 
	 */
	private String thumbNailProductImagePath;
	/**
	 * 
	 */
	private String largeProductImagePath;
	/**
	 * 
	 */
	private Product product;
	/**
	 * 
	 */
	private RetailerLocation retailerLocation;

	/**
	 * 
	 */
	public RetailLocationDeal()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param retailLocationDealID
	 * @param retailLocationID
	 * @param productID
	 * @param retailLocationProductDesc
	 * @param quantity
	 * @param price
	 * @param salePrice
	 * @param saleStartDate
	 * @param saleEndDate
	 * @param createdDate
	 * @param modifiedDate
	 * @param thumbNailProductImagePath
	 * @param largeProductImagePath
	 * @param product
	 * @param retailerLocation
	 */
	public RetailLocationDeal(Long retailLocationDealID, Long retailLocationID, Long productID, String retailLocationProductDesc, int quantity,
			Double price, Double salePrice, String saleStartDate, String saleEndDate, String createdDate, String modifiedDate,
			String thumbNailProductImagePath, String largeProductImagePath, Product product, RetailerLocation retailerLocation)
	{
		super();
		this.retailLocationDealID = retailLocationDealID;
		this.retailLocationID = retailLocationID;
		this.productID = productID;
		this.retailLocationProductDesc = retailLocationProductDesc;
		this.quantity = quantity;
		this.price = price;
		this.salePrice = salePrice;
		this.saleStartDate = saleStartDate;
		this.saleEndDate = saleEndDate;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.thumbNailProductImagePath = thumbNailProductImagePath;
		this.largeProductImagePath = largeProductImagePath;
		this.product = product;
		this.retailerLocation = retailerLocation;
	}

	/**
	 * @return the retailLocationDealID
	 */
	public Long getRetailLocationDealID()
	{
		return retailLocationDealID;
	}

	/**
	 * @param retailLocationDealID
	 *            the retailLocationDealID to set
	 */
	public void setRetailLocationDealID(Long retailLocationDealID)
	{
		this.retailLocationDealID = retailLocationDealID;
	}

	/**
	 * @return the retailLocationID
	 */
	public Long getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(Long retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the productID
	 */
	public Long getProductID()
	{
		return productID;
	}

	/**
	 * @param productID
	 *            the productID to set
	 */
	public void setProductID(Long productID)
	{
		this.productID = productID;
	}

	/**
	 * @return the retailLocationProductDesc
	 */
	public String getRetailLocationProductDesc()
	{
		return retailLocationProductDesc;
	}

	/**
	 * @param retailLocationProductDesc
	 *            the retailLocationProductDesc to set
	 */
	public void setRetailLocationProductDesc(String retailLocationProductDesc)
	{
		this.retailLocationProductDesc = retailLocationProductDesc;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}

	/**
	 * @return the price
	 */
	public Double getPrice()
	{
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(Double price)
	{
		this.price = price;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice()
	{
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(Double salePrice)
	{
		this.salePrice = salePrice;
	}

	/**
	 * @return the saleStartDate
	 */
	public String getSaleStartDate()
	{
		return saleStartDate;
	}

	/**
	 * @param saleStartDate
	 *            the saleStartDate to set
	 */
	public void setSaleStartDate(String saleStartDate)
	{
		this.saleStartDate = saleStartDate;
	}

	/**
	 * @return the saleEndDate
	 */
	public String getSaleEndDate()
	{
		return saleEndDate;
	}

	/**
	 * @param saleEndDate
	 *            the saleEndDate to set
	 */
	public void setSaleEndDate(String saleEndDate)
	{
		this.saleEndDate = saleEndDate;
	}

	/**
	 * @return the createdDate
	 */
	public String getCreatedDate()
	{
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(String createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * @return the modifiedDate
	 */
	public String getModifiedDate()
	{
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            the modifiedDate to set
	 */
	public void setModifiedDate(String modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the thumbNailProductImagePath
	 */
	public String getThumbNailProductImagePath()
	{
		return thumbNailProductImagePath;
	}

	/**
	 * @param thumbNailProductImagePath
	 *            the thumbNailProductImagePath to set
	 */
	public void setThumbNailProductImagePath(String thumbNailProductImagePath)
	{
		this.thumbNailProductImagePath = thumbNailProductImagePath;
	}

	/**
	 * @return the largeProductImagePath
	 */
	public String getLargeProductImagePath()
	{
		return largeProductImagePath;
	}

	/**
	 * @param largeProductImagePath
	 *            the largeProductImagePath to set
	 */
	public void setLargeProductImagePath(String largeProductImagePath)
	{
		this.largeProductImagePath = largeProductImagePath;
	}

	/**
	 * @return the product
	 */
	public Product getProduct()
	{
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(Product product)
	{
		this.product = product;
	}

	/**
	 * @return the retailerLocation
	 */
	public RetailerLocation getRetailerLocation()
	{
		return retailerLocation;
	}

	/**
	 * @param retailerLocation
	 *            the retailerLocation to set
	 */
	public void setRetailerLocation(RetailerLocation retailerLocation)
	{
		this.retailerLocation = retailerLocation;
	}
}

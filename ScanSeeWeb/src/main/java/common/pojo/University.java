/**
 * Project     : Scan See
 * File        : University.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 30th December 2011
 */
package common.pojo;

/**
 * Bean is used in University details.
 * @author kumar_dodda
 *
 */
public class University
{
	/**
	 * 
	 */
	private Long universityID;
	/**
	 * 
	 */
	private String universityName;
	/**
	 * 
	 */
	private String dateCreated;
	/**
	 * 
	 */
	private String dateModified;
	/**
	 * 
	 */
	private int activeFlag;
	/**
	 * 
	 */
	public University()
	{
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param universityID
	 * @param universityName
	 * @param dateCreated
	 * @param dateModified
	 * @param activeFlag
	 */
	public University(Long universityID, String universityName, String dateCreated, String dateModified, int activeFlag)
	{
		super();
		this.universityID = universityID;
		this.universityName = universityName;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.activeFlag = activeFlag;
	}
	/**
	 * @return the universityID
	 */
	public Long getUniversityID()
	{
		return universityID;
	}
	/**
	 * @param universityID the universityID to set
	 */
	public void setUniversityID(Long universityID)
	{
		this.universityID = universityID;
	}
	/**
	 * @return the universityName
	 */
	public String getUniversityName()
	{
		return universityName;
	}
	/**
	 * @param universityName the universityName to set
	 */
	public void setUniversityName(String universityName)
	{
		this.universityName = universityName;
	}
	/**
	 * @return the dateCreated
	 */
	public String getDateCreated()
	{
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(String dateCreated)
	{
		this.dateCreated = dateCreated;
	}
	/**
	 * @return the dateModified
	 */
	public String getDateModified()
	{
		return dateModified;
	}
	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(String dateModified)
	{
		this.dateModified = dateModified;
	}
	/**
	 * @return the activeFlag
	 */
	public int getActiveFlag()
	{
		return activeFlag;
	}
	/**
	 * @param activeFlag the activeFlag to set
	 */
	public void setActiveFlag(int activeFlag)
	{
		this.activeFlag = activeFlag;
	}

}

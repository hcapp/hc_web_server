package common.pojo;

/**
 * This class is used to store the QR details of supplier
 * @author malathi_lr
 *
 */
public class QRDetails {

	private int productId;
	
	private String productImagePath;
	
	private String audio;
	
	private String audioImagePath;
	
	private String video;
	
	private String videoImagePath;
	
	private String fileLink;
	
	private String fileLinkImagePath;
	
	private String message=null;
	

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath() {
		return productImagePath;
	}

	/**
	 * @return the audio
	 */
	public String getAudio() {
		return audio;
	}

	/**
	 * @return the audioImagePath
	 */
	public String getAudioImagePath() {
		return audioImagePath;
	}

	/**
	 * @return the video
	 */
	public String getVideo() {
		return video;
	}

	/**
	 * @return the videoImagePath
	 */
	public String getVideoImagePath() {
		return videoImagePath;
	}

	/**
	 * @return the fileLink
	 */
	public String getFileLink() {
		return fileLink;
	}

	/**
	 * @return the fileLinkImagePath
	 */
	public String getFileLinkImagePath() {
		return fileLinkImagePath;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}

	/**
	 * @param productImagePath the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath) {
		this.productImagePath = productImagePath;
	}

	/**
	 * @param audio the audio to set
	 */
	public void setAudio(String audio) {
		this.audio = audio;
	}

	/**
	 * @param audioImagePath the audioImagePath to set
	 */
	public void setAudioImagePath(String audioImagePath) {
		this.audioImagePath = audioImagePath;
	}

	/**
	 * @param video the video to set
	 */
	public void setVideo(String video) {
		this.video = video;
	}

	/**
	 * @param videoImagePath the videoImagePath to set
	 */
	public void setVideoImagePath(String videoImagePath) {
		this.videoImagePath = videoImagePath;
	}

	/**
	 * @param fileLink the fileLink to set
	 */
	public void setFileLink(String fileLink) {
		this.fileLink = fileLink;
	}

	/**
	 * @param fileLinkImagePath the fileLinkImagePath to set
	 */
	public void setFileLinkImagePath(String fileLinkImagePath) {
		this.fileLinkImagePath = fileLinkImagePath;
	}
	
	
	
}

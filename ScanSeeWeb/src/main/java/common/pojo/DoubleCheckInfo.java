package common.pojo;

public class DoubleCheckInfo
{
	
	private String aboutMe;
	private String preferences;
	private String givingBack;
	
	public String getAboutMe()
	{
		return aboutMe;
	}
	public void setAboutMe(String aboutMe)
	{
		this.aboutMe = aboutMe;
	}
	public String getPreferences()
	{
		return preferences;
	}
	public void setPreferences(String preferences)
	{
		this.preferences = preferences;
	}
	public String getGivingBack()
	{
		return givingBack;
	}
	public void setGivingBack(String givingBack)
	{
		this.givingBack = givingBack;
	}
	
}

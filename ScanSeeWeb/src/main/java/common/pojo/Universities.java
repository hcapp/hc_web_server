package common.pojo;

public class Universities
{

	private String unversityKey;
	private String universityValue;
	
	public Universities(String unversityKey, String universityValue)
	{
	this.unversityKey = unversityKey;
	this.universityValue = universityValue;
	}
	
	public String getUnversityKey()
	{
		return unversityKey;
	}
	public void setUnversityKey(String unversityKey)
	{
		this.unversityKey = unversityKey;
	}

	public String getUniversityValue()
	{
		return universityValue;
	}

	public void setUniversityValue(String universityValue)
	{
		this.universityValue = universityValue;
	}
	
	
}

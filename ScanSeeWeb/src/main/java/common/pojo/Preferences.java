package common.pojo;

public class Preferences
{
	
	private String preferenceKey;
	//private Boolean preferenceValue;
	//private boolean preferenceValue = false;
	
	private String preferenceValue ;
	
	
	
	/*public boolean isPreferenceValue()
	{
		return preferenceValue;
	}

	public void setPreferenceValue(boolean preferenceValue)
	{
		this.preferenceValue = preferenceValue;
	}
	*/

	public String getPreferenceValue()
	{
		return preferenceValue;
	}

	public void setPreferenceValue(String preferenceValue)
	{
		this.preferenceValue = preferenceValue;
	}

	public Preferences(String preferenceKey, String preferenceValue)
	{
	this.preferenceKey = preferenceKey;
	this.preferenceValue = preferenceValue;
	}

	public String getPreferenceKey()
	{
		return preferenceKey;
	}

	public void setPreferenceKey(String preferenceKey)
	{
		this.preferenceKey = preferenceKey;
	}
	
	

/*
 * 	public Boolean getPreferenceValue()
 
	{
		return preferenceValue;
	}

	public void setPreferenceValue(Boolean preferenceValue)
	{
		this.preferenceValue = preferenceValue;
	}
		
*/	
}

package common.pojo;

public class RetailerImages {

	public String imagePath;
	
	public int qRRetailerCustomPageIconID;

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @return the qRRetailerCustomPageIconID
	 */
	public int getqRRetailerCustomPageIconID() {
		return qRRetailerCustomPageIconID;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @param qRRetailerCustomPageIconID the qRRetailerCustomPageIconID to set
	 */
	public void setqRRetailerCustomPageIconID(int qRRetailerCustomPageIconID) {
		this.qRRetailerCustomPageIconID = qRRetailerCustomPageIconID;
	}
}

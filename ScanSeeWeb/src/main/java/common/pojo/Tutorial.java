package common.pojo;

/**
 * This POJO is used for tutorial videos.
 * @author shyamsundara_hm
 *
 */
public class Tutorial {
	
	private Integer tutorialId;
	private String tutorialType;
	private String videoPath;
	private String poster;
	public String getTutorialType() {
		return tutorialType;
	}
	public void setTutorialType(String tutorialType) {
		this.tutorialType = tutorialType;
	}
	public String getVideoPath() {
		return videoPath;
	}
	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}
	public String getPoster() {
		return poster;
	}
	public void setPoster(String poster) {
		this.poster = poster;
	}
	public Integer getTutorialId() {
		return tutorialId;
	}
	public void setTutorialId(Integer tutorialId) {
		this.tutorialId = tutorialId;
	}

}

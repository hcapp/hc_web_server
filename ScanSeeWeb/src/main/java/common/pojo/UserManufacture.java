/**
 * 
 */
package common.pojo;

/**
 * @author kumar_dodda
 *
 */
public class UserManufacture {
	/**
	 * 
	 */
	private Long userManufactureID;
	/**
	 * 
	 */
	private int adminFlag;
	/**
	 * 
	 */
	private Long manufactureID;
	/**
	 * 
	 */
	private Long userID;
	/**
	 * 
	 */
	private Manufacture manufacture;
	/**
	 * 
	 */
	private Users user;
	
	/**
	 * 
	 */
	public UserManufacture() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param userManufactureID
	 * @param adminFlag
	 * @param manufactureID
	 * @param userID
	 * @param manufacture
	 * @param user
	 */
	public UserManufacture(Long userManufactureID, int adminFlag,
			Long manufactureID, Long userID, Manufacture manufacture, Users user) {
		super();
		this.userManufactureID = userManufactureID;
		this.adminFlag = adminFlag;
		this.manufactureID = manufactureID;
		this.userID = userID;
		this.manufacture = manufacture;
		this.user = user;
	}

	/**
	 * @return the userManufactureID
	 */
	public Long getUserManufactureID() {
		return userManufactureID;
	}

	/**
	 * @param userManufactureID the userManufactureID to set
	 */
	public void setUserManufactureID(Long userManufactureID) {
		this.userManufactureID = userManufactureID;
	}

	/**
	 * @return the adminFlag
	 */
	public int getAdminFlag() {
		return adminFlag;
	}

	/**
	 * @param adminFlag the adminFlag to set
	 */
	public void setAdminFlag(int adminFlag) {
		this.adminFlag = adminFlag;
	}

	/**
	 * @return the manufactureID
	 */
	public Long getManufactureID() {
		if(manufacture!=null){
			this.manufactureID=manufacture.getManufactureID();
		}
		return manufactureID;
	}

	/**
	 * @param manufactureID the manufactureID to set
	 */
	public void setManufactureID(Long manufactureID) {
		this.manufactureID = manufactureID;
	}

	/**
	 * @return the userID
	 */
	public Long getUserID() {
		if(user!=null){
			this.userID=user.getUserID();
		}
		return userID;
	}

	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Long userID) {
		this.userID = userID;
	}

	/**
	 * @return the manufacture
	 */
	public Manufacture getManufacture() {
		return manufacture;
	}

	/**
	 * @param manufacture the manufacture to set
	 */
	public void setManufacture(Manufacture manufacture) {
		this.manufacture = manufacture;
	}

	/**
	 * @return the user
	 */
	public Users getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(Users user) {
		this.user = user;
	}
}

package common.pojo;

/**
 * This is a pojo class use for storing retailer contact email id
 * @author malathi_lr
 *
 */
public class ContactDetail {

	/**
	 * identifier field.
	 */
	private Long contactId;
	
	/**
	 * 
	 */
	private String contactEmail;

	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}

	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
}

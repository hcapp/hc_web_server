/**
 * Project     : Scan See
 * File        : Category.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;

import java.util.ArrayList;

/**
 * Bean is used in Category details.
 * @author kumar_dodda
 */
public class Category
{
	/**
	 * 
	 */
	private Long categoryID;
	/**
	 * 
	 */
	private String categoryName;
	/**
	 * 
	 */
	private int parentCategoryID;
	/**
	 * 
	 */
	private String parentCategoryName;
	/**
	 * 
	 */
	private String parentCategoryDesc;
	/**
	 * 
	 */
	private Long subCategoryID;
	/**
	 * 
	 */
	private String subCategoryName;
	/**
	 * 
	 */
	private String subCategoryDesc;
	/**
	 * 
	 */
	private String categoryAddDate;
	/**
	 * 
	 */
	private String categoryModifyDate;
	/**
	 * 
	 */
	private String parentSubCategory;
	/**
	 * 
	 */
    private Long businessCategoryID;
	
	private String businessCategoryName;
	
	private String dateCreated;
	
	private String dateModified;
	
	private String businessCategoryDisplayValue;

	private Integer deptID;
	
	private String deptName;

	private Boolean isAssociated;
	
    private Integer filterID;
	
	private String filterName;
	
	private Integer filterValueID;
	
	private String filterValueName;
	/**
	 * 
	 * 
	 */
	private Boolean isSubCat;
	
	private Boolean isSplash;
	private Boolean isLogo;
	private Boolean isBanner;
	private Boolean isAnyThingPage;
	
	private ArrayList<Tutorial> createTutorialLst = null;
	private ArrayList<Tutorial> getStrtTutorialLst = null;
	private ArrayList<Tutorial> newFeatTutorialLst = null;
	private ArrayList<Tutorial> tipsTechTutorialLst = null;
	
	public Category()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param categoryID
	 * @param parentCategoryID
	 * @param parentCategoryName
	 * @param parentCategoryDesc
	 * @param subCategoryID
	 * @param subCategoryName
	 * @param subCategoryDesc
	 * @param categoryAddDate
	 * @param categoryModifyDate
	 */
	public Category(Long categoryID, int parentCategoryID, String parentCategoryName, String parentCategoryDesc, Long subCategoryID,
			String subCategoryName, String subCategoryDesc, String categoryAddDate, String categoryModifyDate)
	{
		super();
		this.categoryID = categoryID;
		this.parentCategoryID = parentCategoryID;
		this.parentCategoryName = parentCategoryName;
		this.parentCategoryDesc = parentCategoryDesc;
		this.subCategoryID = subCategoryID;
		this.subCategoryName = subCategoryName;
		this.subCategoryDesc = subCategoryDesc;
		this.categoryAddDate = categoryAddDate;
		this.categoryModifyDate = categoryModifyDate;
	}

	/**
	 * @return the categoryID
	 */
	public Long getCategoryID()
	{
		return categoryID;
	}

	/**
	 * @param categoryID
	 *            the categoryID to set
	 */
	public void setCategoryID(Long categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the parentCategoryID
	 */
	public int getParentCategoryID()
	{
		return parentCategoryID;
	}

	/**
	 * @param parentCategoryID
	 *            the parentCategoryID to set
	 */
	public void setParentCategoryID(int parentCategoryID)
	{
		this.parentCategoryID = parentCategoryID;
	}

	/**
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}

	/**
	 * @param parentCategoryName
	 *            the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}

	/**
	 * @return the parentCategoryDesc
	 */
	public String getParentCategoryDesc()
	{
		return parentCategoryDesc;
	}

	/**
	 * @param parentCategoryDesc
	 *            the parentCategoryDesc to set
	 */
	public void setParentCategoryDesc(String parentCategoryDesc)
	{
		this.parentCategoryDesc = parentCategoryDesc;
	}

	/**
	 * @return the subCategoryID
	 */
	public Long getSubCategoryID()
	{
		return subCategoryID;
	}

	/**
	 * @param subCategoryID
	 *            the subCategoryID to set
	 */
	public void setSubCategoryID(Long subCategoryID)
	{
		this.subCategoryID = subCategoryID;
	}

	/**
	 * @return the subCategoryName
	 */
	public String getSubCategoryName()
	{
		return subCategoryName;
	}

	/**
	 * @param subCategoryName
	 *            the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName)
	{
		this.subCategoryName = subCategoryName;
	}

	/**
	 * @return the subCategoryDesc
	 */
	public String getSubCategoryDesc()
	{
		return subCategoryDesc;
	}

	/**
	 * @param subCategoryDesc
	 *            the subCategoryDesc to set
	 */
	public void setSubCategoryDesc(String subCategoryDesc)
	{
		this.subCategoryDesc = subCategoryDesc;
	}

	/**
	 * @return the categoryAddDate
	 */
	public String getCategoryAddDate()
	{
		return categoryAddDate;
	}

	/**
	 * @param categoryAddDate
	 *            the categoryAddDate to set
	 */
	public void setCategoryAddDate(String categoryAddDate)
	{
		this.categoryAddDate = categoryAddDate;
	}

	/**
	 * @return the categoryModifyDate
	 */
	public String getCategoryModifyDate()
	{
		return categoryModifyDate;
	}

	/**
	 * @param categoryModifyDate
	 *            the categoryModifyDate to set
	 */
	public void setCategoryModifyDate(String categoryModifyDate)
	{
		this.categoryModifyDate = categoryModifyDate;
	}

	/**
	 * @return the parentSubCategory
	 */
	public String getParentSubCategory()
	{
		return parentSubCategory;
	}

	/**
	 * @param parentSubCategory the parentSubCategory to set
	 */
	public void setParentSubCategory(String parentSubCategory)
	{
		this.parentSubCategory = parentSubCategory;
	}

	/**
	 * @return the businessCategoryID
	 */
	public Long getBusinessCategoryID() {
		return businessCategoryID;
	}

	/**
	 * @param businessCategoryID the businessCategoryID to set
	 */
	public void setBusinessCategoryID(Long businessCategoryID) {
		this.businessCategoryID = businessCategoryID;
	}

	/**
	 * @return the businessCategoryName
	 */
	public String getBusinessCategoryName() {
		return businessCategoryName;
	}

	/**
	 * @param businessCategoryName the businessCategoryName to set
	 */
	public void setBusinessCategoryName(String businessCategoryName) {
		this.businessCategoryName = businessCategoryName;
	}

	/**
	 * @return the dateCreated
	 */
	public String getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the dateModified
	 */
	public String getDateModified() {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * @return the businessCategoryDisplayValue
	 */
	public String getBusinessCategoryDisplayValue() {
		return businessCategoryDisplayValue;
	}

	/**
	 * @param businessCategoryDisplayValue the businessCategoryDisplayValue to set
	 */
	public void setBusinessCategoryDisplayValue(String businessCategoryDisplayValue) {
		this.businessCategoryDisplayValue = businessCategoryDisplayValue;
	}

	public Integer getDeptID() {
		return deptID;
	}

	public void setDeptID(Integer deptID) {
		this.deptID = deptID;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
	 * @return the isAssociated
	 */
	public Boolean getIsAssociated() {
		return isAssociated;
	}

	/**
	 * @param isAssociated the isAssociated to set
	 */
	public void setIsAssociated(Boolean isAssociated) {
		this.isAssociated = isAssociated;
	}

	/**
	 * @return the filterID
	 */
	public Integer getFilterID() {
		return filterID;
	}

	/**
	 * @param filterID the filterID to set
	 */
	public void setFilterID(Integer filterID) {
		this.filterID = filterID;
	}

	/**
	 * @return the filterName
	 */
	public String getFilterName() {
		return filterName;
	}

	/**
	 * @param filterName the filterName to set
	 */
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	/**
	 * @return the filterValueID
	 */
	public Integer getFilterValueID() {
		return filterValueID;
	}

	/**
	 * @param filterValueID the filterValueID to set
	 */
	public void setFilterValueID(Integer filterValueID) {
		this.filterValueID = filterValueID;
	}

	/**
	 * @return the filterValueName
	 */
	public String getFilterValueName() {
		return filterValueName;
	}

	/**
	 * @param filterValueName the filterValueName to set
	 */
	public void setFilterValueName(String filterValueName) {
		this.filterValueName = filterValueName;
	}

	/**
	 * @return the isSubCatAssociated
	 */
	public Boolean getIsSubCat() {
		return isSubCat;
	}

	/**
	 * @param isSubCatAssociated the isSubCatAssociated to set
	 */
	public void setIsSubCat(Boolean isSubCat) {
		this.isSubCat = isSubCat;
	}

	public Boolean getIsSplash() {
		return isSplash;
	}

	public void setIsSplash(Boolean isSplash) {
		this.isSplash = isSplash;
	}

	public Boolean getIsLogo() {
		return isLogo;
	}

	public void setIsLogo(Boolean isLogo) {
		this.isLogo = isLogo;
	}

	public Boolean getIsBanner() {
		return isBanner;
	}

	public void setIsBanner(Boolean isBanner) {
		this.isBanner = isBanner;
	}

	public Boolean getIsAnyThingPage() {
		return isAnyThingPage;
	}

	public void setIsAnyThingPage(Boolean isAnyThingPage) {
		this.isAnyThingPage = isAnyThingPage;
	}

	public ArrayList<Tutorial> getCreateTutorialLst() {
		return createTutorialLst;
	}

	public void setCreateTutorialLst(ArrayList<Tutorial> createTutorialLst) {
		this.createTutorialLst = createTutorialLst;
	}

	public ArrayList<Tutorial> getGetStrtTutorialLst() {
		return getStrtTutorialLst;
	}

	public void setGetStrtTutorialLst(ArrayList<Tutorial> getStrtTutorialLst) {
		this.getStrtTutorialLst = getStrtTutorialLst;
	}

	public ArrayList<Tutorial> getNewFeatTutorialLst() {
		return newFeatTutorialLst;
	}

	public void setNewFeatTutorialLst(ArrayList<Tutorial> newFeatTutorialLst) {
		this.newFeatTutorialLst = newFeatTutorialLst;
	}

	public ArrayList<Tutorial> getTipsTechTutorialLst() {
		return tipsTechTutorialLst;
	}

	public void setTipsTechTutorialLst(ArrayList<Tutorial> tipsTechTutorialLst) {
		this.tipsTechTutorialLst = tipsTechTutorialLst;
	}
	
}

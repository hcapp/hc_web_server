/**
 * Project     : Scan See
 * File        : CountryCode.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;

/**
 * Bean used in CountryCode.
 * 
 * @author kumar_dodda
 */
public class CountryCode
{
	
	/**
	 * for state name.
	 * 
	 */
	private String stateName;
	
	
	/**
	 * for country id
	 * 
	 */
	private Long countryID;
	/**
	 * 
	 */
	private String countryName;
	/**
	 * 
	 */
	private Integer active;

	/**
	 * 
	 */
	public CountryCode()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * Variable cityName declared as of type String.
	 */
	protected String cityName;

	/**
	 * @param countryID
	 * @param countryName
	 * @param active
	 */
	public CountryCode(Long countryID, String countryName, Integer active)
	{
		super();
		this.countryID = countryID;
		this.countryName = countryName;
		this.active = active;
	}

	/**
	 * @return the countryID
	 */
	public Long getCountryID()
	{
		return countryID;
	}

	/**
	 * @param countryID
	 *            the countryID to set
	 */
	public void setCountryID(Long countryID)
	{
		this.countryID = countryID;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName()
	{
		return countryName;
	}

	/**
	 * @param countryName
	 *            the countryName to set
	 */
	public void setCountryName(String countryName)
	{
		this.countryName = countryName;
	}

	/**
	 * @return the active
	 */
	public Integer getActive()
	{
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Integer active)
	{
		this.active = active;
	}

	public String getCityName()
	{
		return cityName;
	}

	public void setCityName(String cityName)
	{
		this.cityName = cityName;
	}

	public String getStateName()
	{
		return stateName;
	}

	public void setStateName(String stateName)
	{
		this.stateName = stateName;
	}

}

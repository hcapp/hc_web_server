package common.pojo;

/**
 * @author dileepa_cc
 */
public class ConsumerRequestDetails
{

	/**
	 * User id of logged in user.
	 */
	private Long userId;

	/**
	 * Consumer type- LoginUser or GuestUser
	 */
	private String consumerType;

	private Integer mainMenuId;
	private String latitute;
	private String longitude;
	private String postalCode;
	private String searchKey;
	private Integer lowerLimit;
	private String moduleName;
	private Integer recordCount;
	private String categoryName;
	private String fromModule;

	public ConsumerRequestDetails()
	{
		super();
	}
	
	public ConsumerRequestDetails(String moduleName)
	{
		super();
		this.moduleName = moduleName;
	}
	
	public ConsumerRequestDetails(String moduleName, String consumerType)
	{
		super();
		this.moduleName = moduleName;
		this.consumerType = consumerType;
	}

	
	public ConsumerRequestDetails(Long userId, String latitute, String longitude, String postalCode, String moduleName)
	{
		super();
		this.userId = userId;
		this.latitute = latitute;
		this.longitude = longitude;
		this.postalCode = postalCode;
		this.moduleName = moduleName;
	}

	public ConsumerRequestDetails(Long userId, String consumerType, Integer mainMenuId, String postalCode, String searchKey, Integer lowerLimit,
			String moduleName, Integer recordCount, String categoryName)
	{
		super();
		this.userId = userId;
		this.consumerType = consumerType;
		this.mainMenuId = mainMenuId;
		this.postalCode = postalCode;
		this.searchKey = searchKey;
		this.lowerLimit = lowerLimit;
		this.moduleName = moduleName;
		this.recordCount = recordCount;
		this.categoryName = categoryName;
	}

	public ConsumerRequestDetails(Long userId, String consumerType, Integer mainMenuId, String postalCode,  Integer lowerLimit,
			String moduleName, Integer recordCount, String fromModule,String searchKey)
	{
		super();
		this.userId = userId;
		this.consumerType = consumerType;
		this.mainMenuId = mainMenuId;
		this.postalCode = postalCode;
		this.lowerLimit = lowerLimit;
		this.moduleName = moduleName;
		this.recordCount = recordCount;
		this.fromModule = fromModule;
		this.searchKey= searchKey;
	
	}

	
	
	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public String getConsumerType()
	{
		return consumerType;
	}

	public void setConsumerType(String consumerType)
	{
		this.consumerType = consumerType;
	}

	public Integer getMainMenuId()
	{
		return mainMenuId;
	}

	public void setMainMenuId(Integer mainMenuId)
	{
		this.mainMenuId = mainMenuId;
	}

	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}

	public String getSearchKey()
	{
		return searchKey;
	}

	public void setSearchKey(String searchKey)
	{
		this.searchKey = searchKey;
	}

	public Integer getLowerLimit()
	{
		return lowerLimit;
	}

	public void setLowerLimit(Integer lowerLimit)
	{
		this.lowerLimit = lowerLimit;
	}

	public String getModuleName()
	{
		return moduleName;
	}

	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	public Integer getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(Integer recordCount)
	{
		this.recordCount = recordCount;
	}

	public String getCategoryName()
	{
		return categoryName;
	}

	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	public String getLatitute()
	{
		return latitute;
	}

	public void setLatitute(String latitute)
	{
		this.latitute = latitute;
	}

	public String getLongitude()
	{
		return longitude;
	}

	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public String getFromModule()
	{
		return fromModule;
	}

	public void setFromModule(String fromModule)
	{
		this.fromModule = fromModule;
	}

}

/**
 * 
 */
package common.pojo;

import java.util.ArrayList;

/**
 * @author kumar_dodda
 *
 */
public class Product {
	
	private String price;
	
	
	/**
	 * 
	 */
	private long productID;
	/**
	 * 
	 */
	private String scanCode;
	/**
	 * 
	 */
	private String productName;
	/**
	 * 
	 */
	private String modelNumber;
	/**
	 * 
	 */
	private double suggestedRetailPrice;
	/**
	 * 
	 */
	private String imagePath; 
	/**
	 * 
	 */
	private String longDescription;
	/**
	 * 
	 */
	private String shortDescription;
	/**
	 * 
	 */
	private String warranty;
	/**
	 * 
	 */
	private Long manufacturerID;
	/**
	 * 
	 */
	private int scanTypeID;
	/**
	 * 
	 */
	private int service;
	/**
	 * 
	 */
	private String skuNumber;
	/**
	 * 
	 */
	private String familyCode1;
	/**
	 * 
	 */
	private String familyCode2;
	/**
	 * 
	 */
	private double weight;
	/**
	 * 
	 */
	private String weightUnits;
	/**
	 * 
	 */
	private String prodExpirationDate;
	/**
	 * 
	 */
	private String productAddDate;
	/**
	 * 
	 */
	private String productModifyDate;
	/**
	 * 
	 */
	private int wishPondDealID;
	/**
	 * 
	 */
	private String manufProuductURL;
	/**
	 * 
	 */
	private int etilizeProductID ;
    /**
     * 
     */
    private String productImagePath;
	/**
	 * 
	 */
    private String audioPath;
    
    private String[] prodAudioPath;
    
    private ArrayList prodVideoPath;
    
private String mediaPath;
	
	private String productMediaType;
    
    private String videoPath;
    
    private Long retLocID;
    
    private String fullMediaPath;
    
    private Integer productMediaID;
    
	/**
	 * @return the productMediaID
	 */
	public Integer getProductMediaID() {
		return productMediaID;
	}
	/**
	 * @param productMediaID the productMediaID to set
	 */
	public void setProductMediaID(Integer productMediaID) {
		this.productMediaID = productMediaID;
	}
	/**
	 * @return the fullMediaPath
	 */
	public String getFullMediaPath() {
		return fullMediaPath;
	}
	/**
	 * @param fullMediaPath the fullMediaPath to set
	 */
	public void setFullMediaPath(String fullMediaPath) {
		this.fullMediaPath = fullMediaPath;
	}
	public Long getRetLocID() {
		return retLocID;
	}
	public void setRetLocID(Long retLocID) {
		this.retLocID = retLocID;
	}
	private Manufacture manufacture;
	public Product() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param productID
	 * @param productName
	 * @param modelNumber
	 * @param suggestedRetailPrice
	 * @param imagePath
	 * @param longDescription
	 * @param shortDescription
	 * @param warranty
	 * @param manufacturerID
	 * @param scanTypeID
	 * @param service
	 * @param skuNumber
	 * @param familyCode1
	 * @param familyCode2
	 * @param weight
	 * @param weightUnits
	 * @param prodExpirationDate
	 * @param productAddDate
	 * @param productModifyDate
	 * @param wishPondDealID
	 * @param manufProuductURL
	 * @param etilizeProductID
	 * @param manufacture
	 * @param productImagePath;
	 */
	public Product(long productID,String scanCode, String productName, String modelNumber,
			double suggestedRetailPrice, String imagePath,
			String longDescription, String shortDescription, String warranty) {
		super();
		this.productID = productID;
		this.scanCode = scanCode;
		this.productName = productName;
		this.modelNumber = modelNumber;
		this.suggestedRetailPrice = suggestedRetailPrice;
		this.imagePath = imagePath;
		this.longDescription = longDescription;
		this.shortDescription = shortDescription;
		this.warranty = warranty;
		this.manufacturerID = manufacturerID;
		this.scanTypeID = scanTypeID;
		this.service = service;
		this.skuNumber = skuNumber;
		this.familyCode1 = familyCode1;
		this.familyCode2 = familyCode2;
		this.weight = weight;
		this.weightUnits = weightUnits;
		this.prodExpirationDate = prodExpirationDate;
		this.productAddDate = productAddDate;
		this.productModifyDate = productModifyDate;
		this.wishPondDealID = wishPondDealID;
		this.manufProuductURL = manufProuductURL;
		this.etilizeProductID = etilizeProductID;
		this.manufacture = manufacture;
		this.productImagePath = productImagePath;
	}
	/**
	 * @return the productID
	 */
	public long getProductID() {
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(long productID) {
		this.productID = productID;
	}
	/**
	 * @return the scanCode
	 */
	public String getScanCode() {
		return scanCode;
	}
	/**
	 * @param scanCode the scanCode to set
	 */
	public void setScanCode(String scanCode) {
		this.scanCode = scanCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the modelNumber
	 */
	public String getModelNumber() {
		return modelNumber;
	}
	/**
	 * @param modelNumber the modelNumber to set
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	/**
	 * @return the suggestedRetailPrice
	 */
	public double getSuggestedRetailPrice() {
		return suggestedRetailPrice;
	}
	/**
	 * @param suggestedRetailPrice the suggestedRetailPrice to set
	 */
	public void setSuggestedRetailPrice(double suggestedRetailPrice) {
		this.suggestedRetailPrice = suggestedRetailPrice;
	}
	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}
	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	/**
	 * @return the longDescription
	 */
	public String getLongDescription() {
		return longDescription;
	}
	/**
	 * @param longDescription the longDescription to set
	 */
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return shortDescription;
	}
	/**
	 * @param shortDescription the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	/**
	 * @return the warranty
	 */
	public String getWarranty() {
		return warranty;
	}
	/**
	 * @param warranty the warranty to set
	 */
	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}
	/**
	 * @return the manufacturerID
	 */
	public Long getManufacturerID()
	{
		return manufacturerID;
	}
	/**
	 * @param manufacturerID the manufacturerID to set
	 */
	public void setManufacturerID(Long manufacturerID)
	{
		this.manufacturerID = manufacturerID;
	}
	/**
	 * @return the scanTypeID
	 */
	public int getScanTypeID()
	{
		return scanTypeID;
	}
	/**
	 * @param scanTypeID the scanTypeID to set
	 */
	public void setScanTypeID(int scanTypeID)
	{
		this.scanTypeID = scanTypeID;
	}
	/**
	 * @return the service
	 */
	public int getService()
	{
		return service;
	}
	/**
	 * @param service the service to set
	 */
	public void setService(int service)
	{
		this.service = service;
	}
	/**
	 * @return the skuNumber
	 */
	public String getSkuNumber()
	{
		return skuNumber;
	}
	/**
	 * @param skuNumber the skuNumber to set
	 */
	public void setSkuNumber(String skuNumber)
	{
		this.skuNumber = skuNumber;
	}
	/**
	 * @return the familyCode1
	 */
	public String getFamilyCode1()
	{
		return familyCode1;
	}
	/**
	 * @param familyCode1 the familyCode1 to set
	 */
	public void setFamilyCode1(String familyCode1)
	{
		this.familyCode1 = familyCode1;
	}
	/**
	 * @return the familyCode2
	 */
	public String getFamilyCode2()
	{
		return familyCode2;
	}
	/**
	 * @param familyCode2 the familyCode2 to set
	 */
	public void setFamilyCode2(String familyCode2)
	{
		this.familyCode2 = familyCode2;
	}
	/**
	 * @return the weight
	 */
	public double getWeight()
	{
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(double weight)
	{
		this.weight = weight;
	}
	/**
	 * @return the weightUnits
	 */
	public String getWeightUnits()
	{
		return weightUnits;
	}
	/**
	 * @param weightUnits the weightUnits to set
	 */
	public void setWeightUnits(String weightUnits)
	{
		this.weightUnits = weightUnits;
	}
	/**
	 * @return the prodExpirationDate
	 */
	public String getProdExpirationDate()
	{
		return prodExpirationDate;
	}
	/**
	 * @param prodExpirationDate the prodExpirationDate to set
	 */
	public void setProdExpirationDate(String prodExpirationDate)
	{
		this.prodExpirationDate = prodExpirationDate;
	}
	/**
	 * @return the productAddDate
	 */
	public String getProductAddDate()
	{
		return productAddDate;
	}
	/**
	 * @param productAddDate the productAddDate to set
	 */
	public void setProductAddDate(String productAddDate)
	{
		this.productAddDate = productAddDate;
	}
	/**
	 * @return the productModifyDate
	 */
	public String getProductModifyDate()
	{
		return productModifyDate;
	}
	/**
	 * @param productModifyDate the productModifyDate to set
	 */
	public void setProductModifyDate(String productModifyDate)
	{
		this.productModifyDate = productModifyDate;
	}
	/**
	 * @return the wishPondDealID
	 */
	public int getWishPondDealID()
	{
		return wishPondDealID;
	}
	/**
	 * @param wishPondDealID the wishPondDealID to set
	 */
	public void setWishPondDealID(int wishPondDealID)
	{
		this.wishPondDealID = wishPondDealID;
	}
	/**
	 * @return the manufProuductURL
	 */
	public String getManufProuductURL()
	{
		return manufProuductURL;
	}
	/**
	 * @param manufProuductURL the manufProuductURL to set
	 */
	public void setManufProuductURL(String manufProuductURL)
	{
		this.manufProuductURL = manufProuductURL;
	}
	/**
	 * @return the etilizeProductID
	 */
	public int getEtilizeProductID()
	{
		return etilizeProductID;
	}
	/**
	 * @param etilizeProductID the etilizeProductID to set
	 */
	public void setEtilizeProductID(int etilizeProductID)
	{
		this.etilizeProductID = etilizeProductID;
	}
	/**
	 * @return the manufacture
	 */
	public Manufacture getManufacture()
	{
		return manufacture;
	}
	/**
	 * @param manufacture the manufacture to set
	 */
	public void setManufacture(Manufacture manufacture)
	{
		this.manufacture = manufacture;
	}
	/**
	 * @return the productImagePath
	 */
	public String getProductImagePath()
	{
		return productImagePath;
	}
	/**
	 * @param productImagePath the productImagePath to set
	 */
	public void setProductImagePath(String productImagePath)
	{
		this.productImagePath = productImagePath;
	}
	public String getPrice()
	{
		return price;
	}
	public void setPrice(String price)
	{
		this.price = price;
	}
	public void setAudioPath(String audioPath) {
		this.audioPath = audioPath;
	}
	public String getAudioPath() {
		return audioPath;
	}
	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}
	public String getVideoPath() {
		return videoPath;
	}
	public void setProdAudioPath(String[] prodAudioPath) {
		this.prodAudioPath = prodAudioPath;
	}
	public String[] getProdAudioPath() {
		return prodAudioPath;
	}
	public void setProdVideoPath(ArrayList prodVideoPath) {
		this.prodVideoPath = prodVideoPath;
	}
	public ArrayList getProdVideoPath() {
		return prodVideoPath;
	}
	
	public void setProductMediaType(String productMediaType) {
		this.productMediaType = productMediaType;
	}
	public String getProductMediaType() {
		return productMediaType;
	}
	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}
	public String getMediaPath() {
		return mediaPath;
	}
	
	
}

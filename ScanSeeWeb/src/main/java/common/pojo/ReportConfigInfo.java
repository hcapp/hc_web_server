package common.pojo;

public class ReportConfigInfo
{
	private String reportPath;
	private String reportURL;
	private String reportServerUserName;
	private String reortServerPassword;
	/**
	 * @param reportPath the reportPath to set
	 */
	public void setReportPath(String reportPath)
	{
		this.reportPath = reportPath;
	}
	/**
	 * @return the reportPath
	 */
	public String getReportPath()
	{
		return reportPath;
	}
	/**
	 * @param reportURL the reportURL to set
	 */
	public void setReportURL(String reportURL)
	{
		this.reportURL = reportURL;
	}
	/**
	 * @return the reportURL
	 */
	public String getReportURL()
	{
		return reportURL;
	}
	/**
	 * @return the reportServerUserName
	 */
	public String getReportServerUserName()
	{
		return reportServerUserName;
	}
	/**
	 * @param reportServerUserName the reportServerUserName to set
	 */
	public void setReportServerUserName(String reportServerUserName)
	{
		this.reportServerUserName = reportServerUserName;
	}
	/**
	 * @return the reortServerPassword
	 */
	public String getReortServerPassword()
	{
		return reortServerPassword;
	}
	/**
	 * @param reortServerPassword the reortServerPassword to set
	 */
	public void setReortServerPassword(String reortServerPassword)
	{
		this.reortServerPassword = reortServerPassword;
	}
	
	

}

package common.pojo;

import java.util.List;


import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class Event {

	private int rowNum;
	private Integer eventID;
	private String eventName;
	private String eventCategoryName;

	private String eventDate;
	private String eventTime;
	private String eventTimeHrs;
	private String eventTimeMins;

	
	private String address;
	private String city;
	private String state;
	private String postalCode;
	private String eventSearchKey;
	private Integer lowerLimit;

	private CommonsMultipartFile eventImageFile;
	private String eventImageName;
	private String strEventImageName;
	private String longDescription;
	private String shortDescription;
	private String bsnsLoc;
	private String[] appsiteID;
	private String[] retailLocationID;

	private String eventCategory;


	private String viewName;
	
	private String hiddenState;
	private String hiddenCategory;
	private String hiddenLocationIDs;
	private String hiddenDays;
	private String hiddenDay;
	private Integer hiddenDate;
	private Integer hiddenWeek;
	

	private Double latitude;
	private Double longitude;
	private boolean geoError;

	private boolean showEventPkgTab;
	private List<RetailerLocation> retLocationList;
	private String eventImagePath;
	private String appsiteIDs;
	private String retailLocationIDs;
	private String searchType;
	private String moreInfoURL;

	private String isOngoing;
	private String isOngoingDaily;
	private String isOngoingMonthly;
	
	private String eventStartDate;
	private String eventStartTime;
	private String eventStartTimeHrs;
	private String eventStartTimeMins;

	private String eventEndDate;
	private String eventEndTime;
	private String eventEndTimeHrs;
	private String eventEndTimeMins;

	private Integer recurrencePatternID;
	private String recurrencePatternName;

	private Integer everyWeekDay;
	private Integer everyWeek;
	private Integer dateOfMonth;
	private Integer everyMonth;
	private Integer everyDayMonth;
	private String[] everyWeekDayMonth;
	private String dayOfMonth;
	private String[] days;
	private Integer endAfter;
	private Integer dayNumber;
	private String occurenceType;
	private Integer recurrenceInterval;
	private Boolean isWeekDay;
	private Boolean byDayNumber;

	private String hiddenWeekDay;
	private String eventEDate;
	private String eventETime;
	private String eventETimeHrs;
	private String eventETimeMins;
	private Integer retailID;
	private String backButton;
	
	private String department;
	private String isEventTied;
	private String moreInfo;
	private String fundraisingGoal;
	private String purchaseProduct;
	private String currentLevel;
	private String organization;
	private Integer retLocId;
	private String isRetLoc;
	private String eventAssociatedIds;
	private String hidEventAssociatedIds;
	private String hidLongDescription;
	private String fundSearchKey;
	
	private CommonsMultipartFile evtThumbImage;
	private String evtImageName;
	private String strEvtImageName;

	private boolean uploadedThumbImg;
	private boolean uploadedImage;
	private String  imageType;
	private String evtImagePath;
	private String EventLocationKeyword;
	private boolean isBandRetailer;
	private String retailerId;
	private String hiddenLocationId;
	private String locationId;
	private String retailerName;
	private String LocationAddress;
	
	/**
	 * @return the rowNum
	 */
	public int getRowNum() {
		return rowNum;
	}
	/**
	 * @param rowNum the rowNum to set
	 */
	public void setRowNum(int rowNum) {
		this.rowNum = rowNum;
	}
	/**
	 * @return the eventID
	 */
	public Integer getEventID() {
		return eventID;
	}
	/**
	 * @param eventID the eventID to set
	 */
	public void setEventID(Integer eventID) {
		this.eventID = eventID;
	}
	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * @return the eventCategoryName
	 */
	public String getEventCategoryName() {
		return eventCategoryName;
	}
	/**
	 * @param eventCategoryName the eventCategoryName to set
	 */
	public void setEventCategoryName(String eventCategoryName) {
		this.eventCategoryName = eventCategoryName;
	}
	/**
	 * @return the eventDate
	 */
	public String getEventDate() {
		return eventDate;
	}
	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	/**
	 * @return the eventTime
	 */
	public String getEventTime() {
		return eventTime;
	}
	/**
	 * @param eventTime the eventTime to set
	 */
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	/**
	 * @return the eventTimeHrs
	 */
	public String getEventTimeHrs() {
		return eventTimeHrs;
	}
	/**
	 * @param eventTimeHrs the eventTimeHrs to set
	 */
	public void setEventTimeHrs(String eventTimeHrs) {
		this.eventTimeHrs = eventTimeHrs;
	}
	/**
	 * @return the eventTimeMins
	 */
	public String getEventTimeMins() {
		return eventTimeMins;
	}
	/**
	 * @param eventTimeMins the eventTimeMins to set
	 */
	public void setEventTimeMins(String eventTimeMins) {
		this.eventTimeMins = eventTimeMins;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	/**
	 * @return the eventSearchKey
	 */
	public String getEventSearchKey() {
		return eventSearchKey;
	}
	/**
	 * @param eventSearchKey the eventSearchKey to set
	 */
	public void setEventSearchKey(String eventSearchKey) {
		this.eventSearchKey = eventSearchKey;
	}
	/**
	 * @return the lowerLimit
	 */
	public Integer getLowerLimit() {
		return lowerLimit;
	}
	/**
	 * @param lowerLimit the lowerLimit to set
	 */
	public void setLowerLimit(Integer lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	/**
	 * @return the eventImageFile
	 */
	public CommonsMultipartFile getEventImageFile() {
		return eventImageFile;
	}
	/**
	 * @param eventImageFile the eventImageFile to set
	 */
	public void setEventImageFile(CommonsMultipartFile eventImageFile) {
		this.eventImageFile = eventImageFile;
	}
	/**
	 * @return the eventImageName
	 */
	public String getEventImageName() {
		return eventImageName;
	}
	/**
	 * @param eventImageName the eventImageName to set
	 */
	public void setEventImageName(String eventImageName) {
		this.eventImageName = eventImageName;
	}
	/**
	 * @return the longDescription
	 */
	public String getLongDescription() {
		return longDescription;
	}
	/**
	 * @param longDescription the longDescription to set
	 */
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	/**
	 * @return the shortDescription
	 */
	public String getShortDescription() {
		return shortDescription;
	}
	/**
	 * @param shortDescription the shortDescription to set
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	/**
	 * @return the bsnsLoc
	 */
	public String getBsnsLoc() {
		return bsnsLoc;
	}
	/**
	 * @param bsnsLoc the bsnsLoc to set
	 */
	public void setBsnsLoc(String bsnsLoc) {
		this.bsnsLoc = bsnsLoc;
	}
	/**
	 * @return the appsiteID
	 */
	public String[] getAppsiteID() {
		return appsiteID;
	}
	/**
	 * @param appsiteID the appsiteID to set
	 */
	public void setAppsiteID(String[] appsiteID) {
		this.appsiteID = appsiteID;
	}
	/**
	 * @return the retailLocationID
	 */
	public String[] getRetailLocationID() {
		return retailLocationID;
	}
	/**
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(String[] retailLocationID) {
		this.retailLocationID = retailLocationID;
	}
	/**
	 * @return the eventCategory
	 */
	public String getEventCategory() {
		return eventCategory;
	}
	/**
	 * @param eventCategory the eventCategory to set
	 */
	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}

	/**
	 * @return the viewName
	 */
	public String getViewName() {
		return viewName;
	}
	/**
	 * @param viewName the viewName to set
	 */
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
	/**
	 * @return the hiddenState
	 */
	public String getHiddenState() {
		return hiddenState;
	}
	/**
	 * @param hiddenState the hiddenState to set
	 */
	public void setHiddenState(String hiddenState) {
		this.hiddenState = hiddenState;
	}
	/**
	 * @return the hiddenCategory
	 */
	public String getHiddenCategory() {
		return hiddenCategory;
	}
	/**
	 * @param hiddenCategory the hiddenCategory to set
	 */
	public void setHiddenCategory(String hiddenCategory) {
		this.hiddenCategory = hiddenCategory;
	}
	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the geoError
	 */
	public boolean isGeoError() {
		return geoError;
	}
	/**
	 * @param geoError the geoError to set
	 */
	public void setGeoError(boolean geoError) {
		this.geoError = geoError;
	}
	/**
	 * @return the showEventPkgTab
	 */
	public boolean isShowEventPkgTab() {
		return showEventPkgTab;
	}
	/**
	 * @param showEventPkgTab the showEventPkgTab to set
	 */
	public void setShowEventPkgTab(boolean showEventPkgTab) {
		this.showEventPkgTab = showEventPkgTab;
	}
	/**
	 * @return the retLocationList
	 */
	public List<RetailerLocation> getRetLocationList() {
		return retLocationList;
	}
	/**
	 * @param retLocationList the retLocationList to set
	 */
	public void setRetLocationList(List<RetailerLocation> retLocationList) {
		this.retLocationList = retLocationList;
	}
	/**
	 * @return the eventImagePath
	 */
	public String getEventImagePath() {
		return eventImagePath;
	}
	/**
	 * @param eventImagePath the eventImagePath to set
	 */
	public void setEventImagePath(String eventImagePath) {
		this.eventImagePath = eventImagePath;
	}
	/**
	 * @return the strEventImageName
	 */
	public String getStrEventImageName() {
		return strEventImageName;
	}
	/**
	 * @param strEventImageName the strEventImageName to set
	 */
	public void setStrEventImageName(String strEventImageName) {
		this.strEventImageName = strEventImageName;
	}
	/**
	 * @return the appsiteIDs
	 */
	public String getAppsiteIDs() {
		return appsiteIDs;
	}
	/**
	 * @param appsiteIDs the appsiteIDs to set
	 */
	public void setAppsiteIDs(String appsiteIDs) {
		this.appsiteIDs = appsiteIDs;
	}
	/**
	 * @return the retailLocationIDs
	 */
	public String getRetailLocationIDs() {
		return retailLocationIDs;
	}
	/**
	 * @param retailLocationIDs the retailLocationIDs to set
	 */
	public void setRetailLocationIDs(String retailLocationIDs) {
		this.retailLocationIDs = retailLocationIDs;
	}
	/**
	 * @return the searchType
	 */
	public String getSearchType() {
		return searchType;
	}
	/**
	 * @param searchType the searchType to set
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	/**
	 * @return the moreInfoURL
	 */
	public String getMoreInfoURL() {
		return moreInfoURL;
	}
	/**
	 * @param moreInfoURL the moreInfoURL to set
	 */
	public void setMoreInfoURL(String moreInfoURL) {
		this.moreInfoURL = moreInfoURL;
	}
	/**
	 * @return the isOngoing
	 */
	public String getIsOngoing() {
		return isOngoing;
	}
	/**
	 * @param isOngoing the isOngoing to set
	 */
	public void setIsOngoing(String isOngoing) {
		this.isOngoing = isOngoing;
	}
	/**
	 * @return the isOngoingDaily
	 */
	public String getIsOngoingDaily() {
		return isOngoingDaily;
	}
	/**
	 * @param isOngoingDaily the isOngoingDaily to set
	 */
	public void setIsOngoingDaily(String isOngoingDaily) {
		this.isOngoingDaily = isOngoingDaily;
	}
	/**
	 * @return the isOngoingMonthly
	 */
	public String getIsOngoingMonthly() {
		return isOngoingMonthly;
	}
	/**
	 * @param isOngoingMonthly the isOngoingMonthly to set
	 */
	public void setIsOngoingMonthly(String isOngoingMonthly) {
		this.isOngoingMonthly = isOngoingMonthly;
	}
	/**
	 * @return the eventStartDate
	 */
	public String getEventStartDate() {
		return eventStartDate;
	}
	/**
	 * @param eventStartDate the eventStartDate to set
	 */
	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	/**
	 * @return the eventStartTime
	 */
	public String getEventStartTime() {
		return eventStartTime;
	}
	/**
	 * @param eventStartTime the eventStartTime to set
	 */
	public void setEventStartTime(String eventStartTime) {
		this.eventStartTime = eventStartTime;
	}
	/**
	 * @return the eventStartTimeHrs
	 */
	public String getEventStartTimeHrs() {
		return eventStartTimeHrs;
	}
	/**
	 * @param eventStartTimeHrs the eventStartTimeHrs to set
	 */
	public void setEventStartTimeHrs(String eventStartTimeHrs) {
		this.eventStartTimeHrs = eventStartTimeHrs;
	}
	/**
	 * @return the eventStartTimeMins
	 */
	public String getEventStartTimeMins() {
		return eventStartTimeMins;
	}
	/**
	 * @param eventStartTimeMins the eventStartTimeMins to set
	 */
	public void setEventStartTimeMins(String eventStartTimeMins) {
		this.eventStartTimeMins = eventStartTimeMins;
	}
	/**
	 * @return the eventEndDate
	 */
	public String getEventEndDate() {
		return eventEndDate;
	}
	/**
	 * @param eventEndDate the eventEndDate to set
	 */
	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}
	/**
	 * @return the eventEndTime
	 */
	public String getEventEndTime() {
		return eventEndTime;
	}
	/**
	 * @param eventEndTime the eventEndTime to set
	 */
	public void setEventEndTime(String eventEndTime) {
		this.eventEndTime = eventEndTime;
	}
	/**
	 * @return the eventEndTimeHrs
	 */
	public String getEventEndTimeHrs() {
		return eventEndTimeHrs;
	}
	/**
	 * @param eventEndTimeHrs the eventEndTimeHrs to set
	 */
	public void setEventEndTimeHrs(String eventEndTimeHrs) {
		this.eventEndTimeHrs = eventEndTimeHrs;
	}
	/**
	 * @return the eventEndTimeMins
	 */
	public String getEventEndTimeMins() {
		return eventEndTimeMins;
	}
	/**
	 * @param eventEndTimeMins the eventEndTimeMins to set
	 */
	public void setEventEndTimeMins(String eventEndTimeMins) {
		this.eventEndTimeMins = eventEndTimeMins;
	}
	/**
	 * @return the recurrencePatternID
	 */
	public Integer getRecurrencePatternID() {
		return recurrencePatternID;
	}
	/**
	 * @param recurrencePatternID the recurrencePatternID to set
	 */
	public void setRecurrencePatternID(Integer recurrencePatternID) {
		this.recurrencePatternID = recurrencePatternID;
	}
	/**
	 * @return the recurrencePatternName
	 */
	public String getRecurrencePatternName() {
		return recurrencePatternName;
	}
	/**
	 * @param recurrencePatternName the recurrencePatternName to set
	 */
	public void setRecurrencePatternName(String recurrencePatternName) {
		this.recurrencePatternName = recurrencePatternName;
	}
	/**
	 * @return the everyWeekDay
	 */
	public Integer getEveryWeekDay() {
		return everyWeekDay;
	}
	/**
	 * @param everyWeekDay the everyWeekDay to set
	 */
	public void setEveryWeekDay(Integer everyWeekDay) {
		this.everyWeekDay = everyWeekDay;
	}
	/**
	 * @return the everyWeek
	 */
	public Integer getEveryWeek() {
		return everyWeek;
	}
	/**
	 * @param everyWeek the everyWeek to set
	 */
	public void setEveryWeek(Integer everyWeek) {
		this.everyWeek = everyWeek;
	}
	/**
	 * @return the dateOfMonth
	 */
	public Integer getDateOfMonth() {
		return dateOfMonth;
	}
	/**
	 * @param dateOfMonth the dateOfMonth to set
	 */
	public void setDateOfMonth(Integer dateOfMonth) {
		this.dateOfMonth = dateOfMonth;
	}
	/**
	 * @return the everyMonth
	 */
	public Integer getEveryMonth() {
		return everyMonth;
	}
	/**
	 * @param everyMonth the everyMonth to set
	 */
	public void setEveryMonth(Integer everyMonth) {
		this.everyMonth = everyMonth;
	}
	/**
	 * @return the everyDayMonth
	 */
	public Integer getEveryDayMonth() {
		return everyDayMonth;
	}
	/**
	 * @param everyDayMonth the everyDayMonth to set
	 */
	public void setEveryDayMonth(Integer everyDayMonth) {
		this.everyDayMonth = everyDayMonth;
	}
	/**
	 * @return the everyWeekDayMonth
	 */
	public String[] getEveryWeekDayMonth() {
		return everyWeekDayMonth;
	}
	/**
	 * @param everyWeekDayMonth the everyWeekDayMonth to set
	 */
	public void setEveryWeekDayMonth(String[] everyWeekDayMonth) {
		this.everyWeekDayMonth = everyWeekDayMonth;
	}
	/**
	 * @return the dayOfMonth
	 */
	public String getDayOfMonth() {
		return dayOfMonth;
	}
	/**
	 * @param dayOfMonth the dayOfMonth to set
	 */
	public void setDayOfMonth(String dayOfMonth) {
		this.dayOfMonth = dayOfMonth;
	}
	/**
	 * @return the days
	 */
	public String[] getDays() {
		return days;
	}
	/**
	 * @param days the days to set
	 */
	public void setDays(String[] days) {
		this.days = days;
	}
	/**
	 * @return the endAfter
	 */
	public Integer getEndAfter() {
		return endAfter;
	}
	/**
	 * @param endAfter the endAfter to set
	 */
	public void setEndAfter(Integer endAfter) {
		this.endAfter = endAfter;
	}
	/**
	 * @return the dayNumber
	 */
	public Integer getDayNumber() {
		return dayNumber;
	}
	/**
	 * @param dayNumber the dayNumber to set
	 */
	public void setDayNumber(Integer dayNumber) {
		this.dayNumber = dayNumber;
	}
	/**
	 * @return the occurenceType
	 */
	public String getOccurenceType() {
		return occurenceType;
	}
	/**
	 * @param occurenceType the occurenceType to set
	 */
	public void setOccurenceType(String occurenceType) {
		this.occurenceType = occurenceType;
	}
	/**
	 * @return the recurrenceInterval
	 */
	public Integer getRecurrenceInterval() {
		return recurrenceInterval;
	}
	/**
	 * @param recurrenceInterval the recurrenceInterval to set
	 */
	public void setRecurrenceInterval(Integer recurrenceInterval) {
		this.recurrenceInterval = recurrenceInterval;
	}
	/**
	 * @return the isWeekDay
	 */
	public Boolean getIsWeekDay() {
		return isWeekDay;
	}
	/**
	 * @param isWeekDay the isWeekDay to set
	 */
	public void setIsWeekDay(Boolean isWeekDay) {
		this.isWeekDay = isWeekDay;
	}
	/**
	 * @return the byDayNumber
	 */
	public Boolean getByDayNumber() {
		return byDayNumber;
	}
	/**
	 * @param byDayNumber the byDayNumber to set
	 */
	public void setByDayNumber(Boolean byDayNumber) {
		this.byDayNumber = byDayNumber;
	}
	/**
	 * @return the hiddenDays
	 */
	public String getHiddenDays() {
		return hiddenDays;
	}
	/**
	 * @param hiddenDays the hiddenDays to set
	 */
	public void setHiddenDays(String hiddenDays) {
		this.hiddenDays = hiddenDays;
	}
	/**
	 * @return the hiddenDay
	 */
	public String getHiddenDay() {
		return hiddenDay;
	}
	/**
	 * @param hiddenDay the hiddenDay to set
	 */
	public void setHiddenDay(String hiddenDay) {
		this.hiddenDay = hiddenDay;
	}
	/**
	 * @return the hiddenDate
	 */
	public Integer getHiddenDate() {
		return hiddenDate;
	}
	/**
	 * @param hiddenDate the hiddenDate to set
	 */
	public void setHiddenDate(Integer hiddenDate) {
		this.hiddenDate = hiddenDate;
	}
	/**
	 * @return the hiddenWeek
	 */
	public Integer getHiddenWeek() {
		return hiddenWeek;
	}
	/**
	 * @param hiddenWeek the hiddenWeek to set
	 */
	public void setHiddenWeek(Integer hiddenWeek) {
		this.hiddenWeek = hiddenWeek;
	}
	/**
	 * @return the hiddenWeekDay
	 */
	public String getHiddenWeekDay() {
		return hiddenWeekDay;
	}
	/**
	 * @param hiddenWeekDay the hiddenWeekDay to set
	 */
	public void setHiddenWeekDay(String hiddenWeekDay) {
		this.hiddenWeekDay = hiddenWeekDay;
	}
	/**
	 * @return the eventEDate
	 */
	public String getEventEDate() {
		return eventEDate;
	}
	/**
	 * @param eventEDate the eventEDate to set
	 */
	public void setEventEDate(String eventEDate) {
		this.eventEDate = eventEDate;
	}
	/**
	 * @return the eventETime
	 */
	public String getEventETime() {
		return eventETime;
	}
	/**
	 * @param eventETime the eventETime to set
	 */
	public void setEventETime(String eventETime) {
		this.eventETime = eventETime;
	}
	/**
	 * @return the eventETimeHrs
	 */
	public String getEventETimeHrs() {
		return eventETimeHrs;
	}
	/**
	 * @param eventETimeHrs the eventETimeHrs to set
	 */
	public void setEventETimeHrs(String eventETimeHrs) {
		this.eventETimeHrs = eventETimeHrs;
	}
	/**
	 * @return the eventETimeMins
	 */
	public String getEventETimeMins() {
		return eventETimeMins;
	}
	/**
	 * @param eventETimeMins the eventETimeMins to set
	 */
	public void setEventETimeMins(String eventETimeMins) {
		this.eventETimeMins = eventETimeMins;
	}
	/**
	 * @return the retailID
	 */
	public Integer getRetailID() {
		return retailID;
	}
	/**
	 * @param retailID the retailID to set
	 */
	public void setRetailID(Integer retailID) {
		this.retailID = retailID;
	}
	/**
	 * @return the hiddenLocationIDs
	 */
	public String getHiddenLocationIDs() {
		return hiddenLocationIDs;
	}
	/**
	 * @return the backButton
	 */
	public String getBackButton() {
		return backButton;
	}
	/**
	 * @param hiddenLocationIDs the hiddenLocationIDs to set
	 */
	public void setHiddenLocationIDs(String hiddenLocationIDs) {
		this.hiddenLocationIDs = hiddenLocationIDs;
	}
	/**
	 * @param backButton the backButton to set
	 */
	public void setBackButton(String backButton) {
		this.backButton = backButton;
	}
	/**
	 * @return the uploadedImage
	 */
	public boolean isUploadedImage() {
		return uploadedImage;
	}
	/**
	 * @param uploadedImage the uploadedImage to set
	 */
	public void setUploadedImage(boolean uploadedImage) {
		this.uploadedImage = uploadedImage;
	}
	
	public String getFundSearchKey() {
		return fundSearchKey;
	}
	public void setFundSearchKey(String fundSearchKey) {
		this.fundSearchKey = fundSearchKey;
	}
	public String getHidLongDescription() {
		return hidLongDescription;
	}
	public void setHidLongDescription(String hidLongDescription) {
		this.hidLongDescription = hidLongDescription;
	}
	public String getHidEventAssociatedIds() {
		return hidEventAssociatedIds;
	}
	public void setHidEventAssociatedIds(String hidEventAssociatedIds) {
		this.hidEventAssociatedIds = hidEventAssociatedIds;
	}
	public String getIsRetLoc() {
		return isRetLoc;
	}
	public void setIsRetLoc(String isRetLoc) {
		this.isRetLoc = isRetLoc;
	}
	public Integer getRetLocId() {
		return retLocId;
	}
	public void setRetLocId(Integer retLocId) {
		this.retLocId = retLocId;
	}
	public String getEventAssociatedIds() {
		return eventAssociatedIds;
	}
	public void setEventAssociatedIds(String eventAssociatedIds) {
		this.eventAssociatedIds = eventAssociatedIds;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getMoreInfo() {
		return moreInfo;
	}
	public void setMoreInfo(String moreInfo) {
		this.moreInfo = moreInfo;
	}
	public String getFundraisingGoal() {
		return fundraisingGoal;
	}
	public void setFundraisingGoal(String fundraisingGoal) {
		this.fundraisingGoal = fundraisingGoal;
	}
	public String getPurchaseProduct() {
		return purchaseProduct;
	}
	public void setPurchaseProduct(String purchaseProduct) {
		this.purchaseProduct = purchaseProduct;
	}
	public String getCurrentLevel() {
		return currentLevel;
	}
	public void setCurrentLevel(String currentLevel) {
		this.currentLevel = currentLevel;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getIsEventTied() {
		return isEventTied;
	}
	public void setIsEventTied(String isEventTied) {
		this.isEventTied = isEventTied;
	}
	/**
	 * @return the evtThumbImage
	 */
	public CommonsMultipartFile getEvtThumbImage() {
		return evtThumbImage;
	}
	/**
	 * @return the evtImageName
	 */
	public String getEvtImageName() {
		return evtImageName;
	}
	/**
	 * @return the strEvtImageName
	 */
	public String getStrEvtImageName() {
		return strEvtImageName;
	}
	/**
	 * @param evtThumbImage the evtThumbImage to set
	 */
	public void setEvtThumbImage(CommonsMultipartFile evtThumbImage) {
		this.evtThumbImage = evtThumbImage;
	}
	/**
	 * @param evtImageName the evtImageName to set
	 */
	public void setEvtImageName(String evtImageName) {
		this.evtImageName = evtImageName;
	}
	/**
	 * @param strEvtImageName the strEvtImageName to set
	 */
	public void setStrEvtImageName(String strEvtImageName) {
		this.strEvtImageName = strEvtImageName;
	}
	/**
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}
	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	/**
	 * @return the uploadedThumbImg
	 */
	public boolean isUploadedThumbImg() {
		return uploadedThumbImg;
	}
	/**
	 * @param uploadedThumbImg the uploadedThumbImg to set
	 */
	public void setUploadedThumbImg(boolean uploadedThumbImg) {
		this.uploadedThumbImg = uploadedThumbImg;
	}
	/**
	 * @return the evtImagePath
	 */
	public String getEvtImagePath() {
		return evtImagePath;
	}
	/**
	 * @param evtImagePath the evtImagePath to set
	 */
	public void setEvtImagePath(String evtImagePath) {
		this.evtImagePath = evtImagePath;
	}
	
	
		
	
	/**
	 * @return the isBandRetailer
	 */
	public boolean isBandRetailer() {
		return isBandRetailer;
	}
	/**
	 * @param isBandRetailer the isBandRetailer to set
	 */	
	public void setBandRetailer(boolean isBandRetailer) {
		this.isBandRetailer = isBandRetailer;
	}
	
	/**
	 * @return the eventLocationTitle
	 */
	public String getEventLocationKeyword() {
		return EventLocationKeyword;
	}
	/**
	 * @param eventLocationTitle the eventLocationTitle to set
	 */
	public void setEventLocationKeyword(String eventLocationKeyword) {
		EventLocationKeyword = eventLocationKeyword;
	}
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public String getHiddenLocationId() {
		return hiddenLocationId;
	}
	public void setHiddenLocationId(String hiddenLocationId) {
		this.hiddenLocationId = hiddenLocationId;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getRetailerName() {
		return retailerName;
	}
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}
	public String getLocationAddress() {
		return LocationAddress;
	}
	public void setLocationAddress(String locationAddress) {
		LocationAddress = locationAddress;
	}
	
	
}
/**
 * Project     : Scan See
 * File        : RetailerLocationAdvertisement.java
 * Author      : Kumar D
 * Company     : Span Systems   
 * Date Created: 17th December 2011
 */
package common.pojo;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import common.util.Utility;

/**
 * Bean used in RetailerLocationAdvertisement.
 * 
 * @author kumar_dodda
 */
public class RetailerLocationAdvertisement
{
	/**
	 * 
	 */
	private Long retailLocationAdvertisementID;
	/**
	 * 
	 */
	private Long retailLocationID;
	/**
	 * 
	 */
	private String advertisementName;
	/**
	 * 
	 */
	private String advertisementDescription;
	/**
	 * 
	 */
	private CommonsMultipartFile bannerAdImagePath;
	/**
	 * 
	 */
	private CommonsMultipartFile ribbonAdImagePath;
	/**
	 * 
	 */
	private String strBannerAdImagePath;
	/**
	 * 
	 */
	private String strRibbonAdImagePath;
	/**
	 * 
	 */
	private String ribbonXAdURL;
	/**
	 * 
	 */
	private String advertisementDate;
	/**
	 * 
	 */
	private String advertisementEndDate = null;
	/**
	 * 
	 */
	private String createdDate;
	/**
	 * 
	 */
	private String modifiedDate;
	/**
	 * 
	 */
	private RetailerLocation retailerLocation;
	/**
	 * 
	 */
	private Retailer retailer;
	/**
	 * 
	 */
	private Long retailID;
	/**
	 * 
	 */
	private String comments;
	/**
	 * 
	 */
	private String retailLocationIDHidden;
	/**
	 * 
	 */
	private String retailLocationIds;
	/**
	 * 
	 */
	private String invalidLocationIds;
	/**
	 * 
	 */
	private int expireFlag;
	/**
	 * 
	 */
	private int deleteFlag;
	/**
	 * 
	 */
	private String viewName;
	/**
	 * 
	 */
	private String tempImageName;
	/**
	 * 
	 */
	private String dbAdvertisementStartDate;
	/**
	 * 
	 */
	private String dbAdvertisementEndDate = null;
	
	private boolean indefiniteAdDurationFlag = false;
	
	private String recordCount ;
	
	private boolean isBand;
	
	

	public RetailerLocationAdvertisement()
	{
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param retailLocationAdvertisementID
	 * @param retailLocationID
	 * @param advertisementName
	 * @param advertisementDescription
	 * @param bannerAdImagePath
	 * @param ribbonAdImagePath
	 * @param ribbonXAdURL
	 * @param advertisementDate
	 * @param advertisementEndDate
	 * @param createdDate
	 * @param modifiedDate
	 * @param retailer
	 * @param retailID
	 */
	public RetailerLocationAdvertisement(Long retailLocationAdvertisementID, Long retailLocationID, String advertisementName,
			String advertisementDescription, CommonsMultipartFile bannerAdImagePath, CommonsMultipartFile ribbonAdImagePath, String ribbonXAdURL, String advertisementDate,
			String advertisementEndDate, String createdDate, String modifiedDate, RetailerLocation retailerLocation, Retailer retailer, Long retailID)
	{
		super();
		this.retailLocationAdvertisementID = retailLocationAdvertisementID;
		this.retailLocationID = retailLocationID;
		this.advertisementName = advertisementName;
		this.advertisementDescription = advertisementDescription;
		this.bannerAdImagePath = bannerAdImagePath;
		this.ribbonAdImagePath = ribbonAdImagePath;
		this.ribbonXAdURL = ribbonXAdURL;
		this.advertisementDate = advertisementDate;
		this.advertisementEndDate = advertisementEndDate;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.retailerLocation = retailerLocation;
		this.retailer = retailer;
		this.retailID = retailID;
	}

	/**
	 * @return the retailLocationAdvertisementID
	 */
	public Long getRetailLocationAdvertisementID()
	{
		return retailLocationAdvertisementID;
	}

	/**
	 * @param retailLocationAdvertisementID
	 *            the retailLocationAdvertisementID to set
	 */
	public void setRetailLocationAdvertisementID(Long retailLocationAdvertisementID)
	{
		this.retailLocationAdvertisementID = retailLocationAdvertisementID;
	}

	/**
	 * @return the retailLocationID
	 */
	public Long getRetailLocationID()
	{
		return retailLocationID;
	}

	/**
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(Long retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	/**
	 * @return the advertisementName
	 */
	public String getAdvertisementName()
	{
		return advertisementName;
	}

	/**
	 * @param advertisementName
	 *            the advertisementName to set
	 */
	public void setAdvertisementName(String advertisementName)
	{
		this.advertisementName = Utility.removeWhiteSpaces(advertisementName);
	}

	/**
	 * @return the advertisementDescription
	 */
	public String getAdvertisementDescription()
	{
		return advertisementDescription;
	}

	/**
	 * @param advertisementDescription
	 *            the advertisementDescription to set
	 */
	public void setAdvertisementDescription(String advertisementDescription)
	{
		this.advertisementDescription = advertisementDescription;
	}

	

	/**
	 * @return the ribbonXAdURL
	 */
	public String getRibbonXAdURL()
	{
		return ribbonXAdURL;
	}

	/**
	 * @param ribbonXAdURL
	 *            the ribbonXAdURL to set
	 */
	public void setRibbonXAdURL(String ribbonXAdURL)
	{
		this.ribbonXAdURL = ribbonXAdURL;
	}

	/**
	 * @return the advertisementDate
	 */
	public String getAdvertisementDate()
	{
		return advertisementDate;
	}

	/**
	 * @param advertisementDate
	 *            the advertisementDate to set
	 */
	public void setAdvertisementDate(String advertisementDate)
	{
		this.advertisementDate = advertisementDate;
	}

	/**
	 * @return the advertisementEndDate
	 */
	public String getAdvertisementEndDate()
	{
		return advertisementEndDate;
	}

	/**
	 * @param advertisementEndDate
	 *            the advertisementEndDate to set
	 */
	public void setAdvertisementEndDate(String advertisementEndDate)
	{
		this.advertisementEndDate = advertisementEndDate;
	}

	/**
	 * @return the createdDate
	 */
	public String getCreatedDate()
	{
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(String createdDate)
	{
		this.createdDate = createdDate;
	}

	/**
	 * @return the modifiedDate
	 */
	public String getModifiedDate()
	{
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            the modifiedDate to set
	 */
	public void setModifiedDate(String modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the retailerLocation
	 */
	public RetailerLocation getRetailerLocation()
	{
		return retailerLocation;
	}

	/**
	 * @param retailerLocation
	 *            the retailerLocation to set
	 */
	public void setRetailerLocation(RetailerLocation retailerLocation)
	{
		this.retailerLocation = retailerLocation;
	}


	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}


	/**
	 * @return the retailer
	 */
	public Retailer getRetailer()
	{
		return retailer;
	}

	/**
	 * @param retailer
	 *            the retailer to set
	 */
	public void setRetailer(Retailer retailer)
	{
		this.retailer = retailer;
	}

	/**
	 * @return the retailID
	 */
	public Long getRetailID()
	{
		return retailID;
	}

	/**
	 * @param retailID
	 *            the retailID to set
	 */
	public void setRetailID(Long retailID)
	{
		this.retailID = retailID;
	}

	/**
	 * @return the bannerAdImagePath
	 */
	public CommonsMultipartFile getBannerAdImagePath()
	{
		return bannerAdImagePath;
	}

	/**
	 * @param bannerAdImagePath the bannerAdImagePath to set
	 */
	public void setBannerAdImagePath(CommonsMultipartFile bannerAdImagePath)
	{
		this.bannerAdImagePath = bannerAdImagePath;
	}

	/**
	 * @return the ribbonAdImagePath
	 */
	public CommonsMultipartFile getRibbonAdImagePath()
	{
		return ribbonAdImagePath;
	}

	/**
	 * @param ribbonAdImagePath the ribbonAdImagePath to set
	 */
	public void setRibbonAdImagePath(CommonsMultipartFile ribbonAdImagePath)
	{
		this.ribbonAdImagePath = ribbonAdImagePath;
	}

	public String getStrBannerAdImagePath()
	{
		return strBannerAdImagePath;
	}

	public void setStrBannerAdImagePath(String strBannerAdImagePath)
	{
		this.strBannerAdImagePath = strBannerAdImagePath;
	}

	public String getStrRibbonAdImagePath()
	{
		return strRibbonAdImagePath;
	}

	public void setStrRibbonAdImagePath(String strRibbonAdImagePath)
	{
		this.strRibbonAdImagePath = strRibbonAdImagePath;
	}

	/**
	 * @return the retailLocationIDHidden
	 */
	public String getRetailLocationIDHidden()
	{
		return retailLocationIDHidden;
	}

	/**
	 * @param retailLocationIDHidden the retailLocationIDHidden to set
	 */
	public void setRetailLocationIDHidden(String retailLocationIDHidden)
	{
		this.retailLocationIDHidden = retailLocationIDHidden;
	}

	/**
	 * @return the retailLocationIds
	 */
	public String getRetailLocationIds()
	{
		return retailLocationIds;
	}

	/**
	 * @param retailLocationIds the retailLocationIds to set
	 */
	public void setRetailLocationIds(String retailLocationIds)
	{
		this.retailLocationIds = retailLocationIds;
	}

	/**
	 * @return the invalidLocationIds
	 */
	public String getInvalidLocationIds()
	{
		return invalidLocationIds;
	}

	/**
	 * @param invalidLocationIds the invalidLocationIds to set
	 */
	public void setInvalidLocationIds(String invalidLocationIds)
	{
		this.invalidLocationIds = invalidLocationIds;
	}

	/**
	 * @return the expireFlag
	 */
	public int getExpireFlag()
	{
		return expireFlag;
	}

	/**
	 * @param expireFlag the expireFlag to set
	 */
	public void setExpireFlag(int expireFlag)
	{
		this.expireFlag = expireFlag;
	}

	/**
	 * @return the deleteFlag
	 */
	public int getDeleteFlag()
	{
		return deleteFlag;
	}

	/**
	 * @param deleteFlag the deleteFlag to set
	 */
	public void setDeleteFlag(int deleteFlag)
	{
		this.deleteFlag = deleteFlag;
	}

	public String getViewName()
	{
		return viewName;
	}

	public void setViewName(String viewName)
	{
		this.viewName = viewName;
	}

	public String getTempImageName()
	{
		return tempImageName;
	}

	public void setTempImageName(String tempImageName)
	{
		this.tempImageName = tempImageName;
	}

	

	/**
	 * @return the dbAdvertisementStartDate
	 */
	public String getDbAdvertisementStartDate()
	{
		return dbAdvertisementStartDate;
	}

	/**
	 * @param dbAdvertisementStartDate the dbAdvertisementStartDate to set
	 */
	public void setDbAdvertisementStartDate(String dbAdvertisementStartDate)
	{
		if (!"".equals(Utility.checkNull(dbAdvertisementStartDate))) {
			this.dbAdvertisementStartDate = Utility.convertDBdate(dbAdvertisementStartDate);
		} else if ("".equals(Utility.checkNull(dbAdvertisementStartDate))){
			this.dbAdvertisementStartDate = dbAdvertisementStartDate;
		}
	}

	/**
	 * @return the dbAdvertisementEndDate
	 */
	public String getDbAdvertisementEndDate()
	{
		return dbAdvertisementEndDate;
	}

	/**
	 * @param dbAdvertisementEndDate the dbAdvertisementEndDate to set
	 */
	public void setDbAdvertisementEndDate(String dbAdvertisementEndDate)
	{
		if (!"".equals(Utility.checkNull(dbAdvertisementEndDate))) {
			this.dbAdvertisementEndDate = Utility.convertDBdate(dbAdvertisementEndDate);
		} else if ("".equals(Utility.checkNull(dbAdvertisementEndDate))){
			this.dbAdvertisementEndDate = dbAdvertisementEndDate;
		}
	}

	public boolean isIndefiniteAdDurationFlag()
	{
		return indefiniteAdDurationFlag;
	}

	public void setIndefiniteAdDurationFlag(boolean indefiniteAdDurationFlag)
	{
		this.indefiniteAdDurationFlag = indefiniteAdDurationFlag;
	}

	public String getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(String recordCount)
	{
		this.recordCount = recordCount;
	}
	
	public boolean isBand() {
		return isBand;
	}

	public void setBand(boolean isBand) {
		this.isBand = isBand;
	}
	
}

package common.pojo;

public class PlanInfo {
	/**
	 * 
	 */
	private String productId;
	/**
	 * 
	 */
	private String productName;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String quantity;
	/**
	 * 
	 */
	private String price;
	/**
	 * 
	 */
	private Double subTotal;
	/**
	 * 
	 */
	private String total;
	/**
	 * 
	 */
	private Double discountSubTotal;
	/**
	 * 
	 */
	private Double discount;
	/**
	 * 
	 */
	private String discountCode;
	/**
	 * 
	 */
	private String planJson;
	/**
	 * 
	 */
	private String productID;
	/**
	 * 
	 */
	private String grandTotal;
	/**
	 * 
	 */
	private Integer discountPlanManufacturerID;
	/**
	 * 
	 */
	private Integer discountPlanRetailerID;
	/**
	 * 
	 */
	private Integer manufacturerDiscountPlanID;
	/**
	 * 
	 */
	private String feedescription;
	/**
	 * 
	 */
	private Long manufBankInfoID;
	/**
	 * 
	 */
	private Long userID;
	/**
	 * 
	 */
	private Long manufacturerID;
	/**
	 * 
	 */
	private boolean achOrBankInfoFlag;
	/**
	 * 
	 */
	private Long accountTypeID;
	/**
	 * 
	 */
	private String bankName;
	/**
	 * 
	 */
	private String routingNumber;
	/**
	 * 
	 */
	private Long accountNumber;
	/**
	 * 
	 */
	private String accountHolderName;
	/**
	 * 
	 */
	private String billingAddress;
	/**
	 * 
	 */
	private String city;
	/**
	 * 
	 */
	private String state;
	/**
	 * 
	 */
	private String zip;
	/**
	 * 
	 */
	private String phoneNumber;
	/**
	 * 
	 */
	private boolean creditCardFlag;
	/**
	 * 
	 */
	private String creditCardNumber;
	/**
	 * 
	 */
	private String expirationDate;
	/**
	 * 
	 */
	private Long cVV;
	/**
	 * 
	 */
	private String cardholderName;
	/**
	 * 
	 */
	private String creditCardBillingAddress;
	/**
	 * 
	 */
	private String accountType;
	/**
	 * 
	 */
	private String day;
	/**
	 * 
	 */
	private String month;
	/**
	 * 
	 */
	private String year;
	/**
	 * 
	 */
	private String accessCode ;
	/**
	 * 
	 */
	private boolean alternatePaymentFlag ;
	
	private String selectedTab;
	
	private String retailerBillingYearly;
	
	private String retailerBillingMonthly;
	
	private String firstName;
	
	private String middleName;
	
	private String lastName;
	
	/**
	 * 
	 */
	public PlanInfo()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param productId
	 * @param productName
	 * @param description
	 * @param quantity
	 * @param price
	 * @param subTotal
	 * @param total
	 * @param discountSubTotal
	 * @param discount
	 * @param discountCode
	 * @param planJson
	 * @param productID2
	 * @param grandTotal
	 * @param discountPlanManufacturerID
	 * @param discountPlanRetailerID
	 * @param manufacturerDiscountPlanID
	 * @param feedescription
	 * @param manufBankInfoID
	 * @param userID
	 * @param manufacturerID
	 * @param achOrBankInfoFlag
	 * @param accountTypeID
	 * @param bankName
	 * @param routingNumber
	 * @param accountNumber
	 * @param accountHolderName
	 * @param billingAddress
	 * @param city
	 * @param state
	 * @param zip
	 * @param phoneNumber
	 * @param creditCardFlag
	 * @param creditCardNumber
	 * @param expirationDate
	 * @param cVV
	 * @param cardholderName
	 * @param creditCardBillingAddress
	 * @param accountType
	 * @param day
	 * @param month
	 * @param year
	 * @param accessCode
	 * @param alternatePaymentFlag
	 */
	public PlanInfo(String productId, String productName, String description, String quantity, String price, Double subTotal, String total,
			Double discountSubTotal, Double discount, String discountCode, String planJson, String productID2, String grandTotal,
			Integer discountPlanManufacturerID, Integer discountPlanRetailerID, Integer manufacturerDiscountPlanID, String feedescription,
			Long manufBankInfoID, Long userID, Long manufacturerID, boolean achOrBankInfoFlag, Long accountTypeID, String bankName,
			String routingNumber, Long accountNumber, String accountHolderName, String billingAddress, String city, String state, String zip,
			String phoneNumber, boolean creditCardFlag, String creditCardNumber, String expirationDate, Long cVV, String cardholderName,
			String creditCardBillingAddress, String accountType, String day, String month, String year, String accessCode,
			boolean alternatePaymentFlag)
	{
		super();
		this.productId = productId;
		this.productName = productName;
		this.description = description;
		this.quantity = quantity;
		this.price = price;
		this.subTotal = subTotal;
		this.total = total;
		this.discountSubTotal = discountSubTotal;
		this.discount = discount;
		this.discountCode = discountCode;
		this.planJson = planJson;
		productID = productID2;
		this.grandTotal = grandTotal;
		this.discountPlanManufacturerID = discountPlanManufacturerID;
		this.discountPlanRetailerID = discountPlanRetailerID;
		this.manufacturerDiscountPlanID = manufacturerDiscountPlanID;
		this.feedescription = feedescription;
		this.manufBankInfoID = manufBankInfoID;
		this.userID = userID;
		this.manufacturerID = manufacturerID;
		this.achOrBankInfoFlag = achOrBankInfoFlag;
		this.accountTypeID = accountTypeID;
		this.bankName = bankName;
		this.routingNumber = routingNumber;
		this.accountNumber = accountNumber;
		this.accountHolderName = accountHolderName;
		this.billingAddress = billingAddress;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.phoneNumber = phoneNumber;
		this.creditCardFlag = creditCardFlag;
		this.creditCardNumber = creditCardNumber;
		this.expirationDate = expirationDate;
		this.cVV = cVV;
		this.cardholderName = cardholderName;
		this.creditCardBillingAddress = creditCardBillingAddress;
		this.accountType = accountType;
		this.day = day;
		this.month = month;
		this.year = year;
		this.accessCode = accessCode;
		this.alternatePaymentFlag = alternatePaymentFlag;
	}
	/**
	 * @return the productId
	 */
	public String getProductId()
	{
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId)
	{
		this.productId = productId;
	}
	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}
	/**
	 * @return the quantity
	 */
	public String getQuantity()
	{
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}
	/**
	 * @return the price
	 */
	public String getPrice()
	{
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price)
	{
		this.price = price;
	}
	/**
	 * @return the subTotal
	 */
	public Double getSubTotal()
	{
		return subTotal;
	}
	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(Double subTotal)
	{
		this.subTotal = subTotal;
	}
	/**
	 * @return the total
	 */
	public String getTotal()
	{
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(String total)
	{
		this.total = total;
	}
	/**
	 * @return the discountSubTotal
	 */
	public Double getDiscountSubTotal()
	{
		return discountSubTotal;
	}
	/**
	 * @param discountSubTotal the discountSubTotal to set
	 */
	public void setDiscountSubTotal(Double discountSubTotal)
	{
		this.discountSubTotal = discountSubTotal;
	}
	/**
	 * @return the discount
	 */
	public Double getDiscount()
	{
		return discount;
	}
	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(Double discount)
	{
		this.discount = discount;
	}
	/**
	 * @return the discountCode
	 */
	public String getDiscountCode()
	{
		return discountCode;
	}
	/**
	 * @param discountCode the discountCode to set
	 */
	public void setDiscountCode(String discountCode)
	{
		this.discountCode = discountCode;
	}
	/**
	 * @return the planJson
	 */
	public String getPlanJson()
	{
		return planJson;
	}
	/**
	 * @param planJson the planJson to set
	 */
	public void setPlanJson(String planJson)
	{
		this.planJson = planJson;
	}
	/**
	 * @return the productID
	 */
	public String getProductID()
	{
		return productID;
	}
	/**
	 * @param productID the productID to set
	 */
	public void setProductID(String productID)
	{
		this.productID = productID;
	}
	/**
	 * @return the grandTotal
	 */
	public String getGrandTotal()
	{
		return grandTotal;
	}
	/**
	 * @param grandTotal the grandTotal to set
	 */
	public void setGrandTotal(String grandTotal)
	{
		this.grandTotal = grandTotal;
	}
	/**
	 * @return the discountPlanManufacturerID
	 */
	public Integer getDiscountPlanManufacturerID()
	{
		return discountPlanManufacturerID;
	}
	/**
	 * @param discountPlanManufacturerID the discountPlanManufacturerID to set
	 */
	public void setDiscountPlanManufacturerID(Integer discountPlanManufacturerID)
	{
		this.discountPlanManufacturerID = discountPlanManufacturerID;
	}
	/**
	 * @return the discountPlanRetailerID
	 */
	public Integer getDiscountPlanRetailerID()
	{
		return discountPlanRetailerID;
	}
	/**
	 * @param discountPlanRetailerID the discountPlanRetailerID to set
	 */
	public void setDiscountPlanRetailerID(Integer discountPlanRetailerID)
	{
		this.discountPlanRetailerID = discountPlanRetailerID;
	}
	/**
	 * @return the manufacturerDiscountPlanID
	 */
	public Integer getManufacturerDiscountPlanID()
	{
		return manufacturerDiscountPlanID;
	}
	/**
	 * @param manufacturerDiscountPlanID the manufacturerDiscountPlanID to set
	 */
	public void setManufacturerDiscountPlanID(Integer manufacturerDiscountPlanID)
	{
		this.manufacturerDiscountPlanID = manufacturerDiscountPlanID;
	}
	/**
	 * @return the feedescription
	 */
	public String getFeedescription()
	{
		return feedescription;
	}
	/**
	 * @param feedescription the feedescription to set
	 */
	public void setFeedescription(String feedescription)
	{
		this.feedescription = feedescription;
	}
	/**
	 * @return the manufBankInfoID
	 */
	public Long getManufBankInfoID()
	{
		return manufBankInfoID;
	}
	/**
	 * @param manufBankInfoID the manufBankInfoID to set
	 */
	public void setManufBankInfoID(Long manufBankInfoID)
	{
		this.manufBankInfoID = manufBankInfoID;
	}
	/**
	 * @return the userID
	 */
	public Long getUserID()
	{
		return userID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Long userID)
	{
		this.userID = userID;
	}
	/**
	 * @return the manufacturerID
	 */
	public Long getManufacturerID()
	{
		return manufacturerID;
	}
	/**
	 * @param manufacturerID the manufacturerID to set
	 */
	public void setManufacturerID(Long manufacturerID)
	{
		this.manufacturerID = manufacturerID;
	}
	/**
	 * @return the achOrBankInfoFlag
	 */
	public boolean isAchOrBankInfoFlag()
	{
		return achOrBankInfoFlag;
	}
	/**
	 * @param achOrBankInfoFlag the achOrBankInfoFlag to set
	 */
	public void setAchOrBankInfoFlag(boolean achOrBankInfoFlag)
	{
		this.achOrBankInfoFlag = achOrBankInfoFlag;
	}
	/**
	 * @return the accountTypeID
	 */
	public Long getAccountTypeID()
	{
		return accountTypeID;
	}
	/**
	 * @param accountTypeID the accountTypeID to set
	 */
	public void setAccountTypeID(Long accountTypeID)
	{
		this.accountTypeID = accountTypeID;
	}
	/**
	 * @return the bankName
	 */
	public String getBankName()
	{
		return bankName;
	}
	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}
	/**
	 * @return the routingNumber
	 */
	public String getRoutingNumber()
	{
		return routingNumber;
	}
	/**
	 * @param routingNumber the routingNumber to set
	 */
	public void setRoutingNumber(String routingNumber)
	{
		this.routingNumber = routingNumber;
	}
	/**
	 * @return the accountNumber
	 */
	public Long getAccountNumber()
	{
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Long accountNumber)
	{
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the accountHolderName
	 */
	public String getAccountHolderName()
	{
		return accountHolderName;
	}
	/**
	 * @param accountHolderName the accountHolderName to set
	 */
	public void setAccountHolderName(String accountHolderName)
	{
		this.accountHolderName = accountHolderName;
	}
	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress()
	{
		return billingAddress;
	}
	/**
	 * @param billingAddress the billingAddress to set
	 */
	public void setBillingAddress(String billingAddress)
	{
		this.billingAddress = billingAddress;
	}
	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}
	/**
	 * @return the zip
	 */
	public String getZip()
	{
		return zip;
	}
	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip)
	{
		this.zip = zip;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the creditCardFlag
	 */
	public boolean isCreditCardFlag()
	{
		return creditCardFlag;
	}
	/**
	 * @param creditCardFlag the creditCardFlag to set
	 */
	public void setCreditCardFlag(boolean creditCardFlag)
	{
		this.creditCardFlag = creditCardFlag;
	}
	/**
	 * @return the creditCardNumber
	 */
	public String getCreditCardNumber()
	{
		return creditCardNumber;
	}
	/**
	 * @param creditCardNumber the creditCardNumber to set
	 */
	public void setCreditCardNumber(String creditCardNumber)
	{
		this.creditCardNumber = creditCardNumber;
	}
	/**
	 * @return the expirationDate
	 */
	public String getExpirationDate()
	{
		return expirationDate;
	}
	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(String expirationDate)
	{
		this.expirationDate = expirationDate;
	}
	/**
	 * @return the cVV
	 */
	public Long getcVV()
	{
		return cVV;
	}
	/**
	 * @param cVV the cVV to set
	 */
	public void setcVV(Long cVV)
	{
		this.cVV = cVV;
	}
	/**
	 * @return the cardholderName
	 */
	public String getCardholderName()
	{
		return cardholderName;
	}
	/**
	 * @param cardholderName the cardholderName to set
	 */
	public void setCardholderName(String cardholderName)
	{
		this.cardholderName = cardholderName;
	}
	/**
	 * @return the creditCardBillingAddress
	 */
	public String getCreditCardBillingAddress()
	{
		return creditCardBillingAddress;
	}
	/**
	 * @param creditCardBillingAddress the creditCardBillingAddress to set
	 */
	public void setCreditCardBillingAddress(String creditCardBillingAddress)
	{
		this.creditCardBillingAddress = creditCardBillingAddress;
	}
	/**
	 * @return the accountType
	 */
	public String getAccountType()
	{
		return accountType;
	}
	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType)
	{
		this.accountType = accountType;
	}
	/**
	 * @return the day
	 */
	public String getDay()
	{
		return day;
	}
	/**
	 * @param day the day to set
	 */
	public void setDay(String day)
	{
		this.day = day;
	}
	/**
	 * @return the month
	 */
	public String getMonth()
	{
		return month;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(String month)
	{
		this.month = month;
	}
	/**
	 * @return the year
	 */
	public String getYear()
	{
		return year;
	}
	/**
	 * @param year the year to set
	 */
	public void setYear(String year)
	{
		this.year = year;
	}
	/**
	 * @return the accessCode
	 */
	public String getAccessCode()
	{
		return accessCode;
	}
	/**
	 * @param accessCode the accessCode to set
	 */
	public void setAccessCode(String accessCode)
	{
		this.accessCode = accessCode;
	}
	/**
	 * @return the alternatePaymentFlag
	 */
	public boolean isAlternatePaymentFlag()
	{
		return alternatePaymentFlag;
	}
	/**
	 * @param alternatePaymentFlag the alternatePaymentFlag to set
	 */
	public void setAlternatePaymentFlag(boolean alternatePaymentFlag)
	{
		this.alternatePaymentFlag = alternatePaymentFlag;
	}
	/**
	 * @return the selectedTab
	 */
	public String getSelectedTab()
	{
		return selectedTab;
	}
	/**
	 * @param selectedTab the selectedTab to set
	 */
	public void setSelectedTab(String selectedTab)
	{
		this.selectedTab = selectedTab;
	}
	public String getRetailerBillingYearly()
	{
		return retailerBillingYearly;
	}
	public void setRetailerBillingYearly(String retailerBillingYearly)
	{
		this.retailerBillingYearly = retailerBillingYearly;
	}
	public String getRetailerBillingMonthly()
	{
		return retailerBillingMonthly;
	}
	public void setRetailerBillingMonthly(String retailerBillingMonthly)
	{
		this.retailerBillingMonthly = retailerBillingMonthly;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public String getMiddleName()
	{
		return middleName;
	}
	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
}

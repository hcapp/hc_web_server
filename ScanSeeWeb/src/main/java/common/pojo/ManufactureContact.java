/**
 * 
 */
package common.pojo;

/**
 * @author kumar_dodda
 *
 */
/**
 * @author kumar_dodda
 *
 */
public class ManufactureContact {
	/**
	 * 
	 */
	private Long ManufactureContactID;
	/**
	 * 
	 */
	private Long contactID;
	/**
	 * 
	 */
	private Long manufactureID;
	/**
	 * 
	 */
	private String createdDate;
	/**
	 * 
	 */
	private Contact contact;
	/**
	 * 
	 */
	private Manufacture manufacture;
	/**
	 * 
	 */
	public ManufactureContact() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param manufactureContactID
	 * @param contactID
	 * @param manufacture
	 * @param createdDate
	 * @param contact
	 * @param manufacture
	 */
	public ManufactureContact(Long manufactureContactID, Long contactID,
			Long manufactureID, String createdDate, Contact contact,
			Manufacture manufacture) {
		super();
		ManufactureContactID = manufactureContactID;
		this.contactID = contactID;
		this.manufactureID = manufactureID;
		this.createdDate = createdDate;
		this.contact = contact;
		this.manufacture = manufacture;
	}
	/**
	 * @return the manufactureContactID
	 */
	public Long getManufactureContactID() {
		return ManufactureContactID;
	}
	/**
	 * @param manufactureContactID the manufactureContactID to set
	 */
	public void setManufactureContactID(Long manufactureContactID) {
		ManufactureContactID = manufactureContactID;
	}
	/**
	 * @return the contactID
	 */
	public Long getContactID() {
		if(contact!=null){
			this.contactID=contact.getContactId();
		}
		return contactID;
	}
	/**
	 * @param contactID the contactID to set
	 */
	public void setContactID(Long contactID) {
		this.contactID = contactID;
	}
	/**
	 * @return the manufacture
	 */
	public Long getManufactureID() {
		if(manufacture!=null){
			this.manufactureID=manufacture.getManufactureID();
		}
		return manufactureID;
	}
	/**
	 * @param manufacture the manufacture to set
	 */
	public void setManufacture(Long manufactureID) {
		this.manufactureID = manufactureID;
	}
	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the contact
	 */
	public Contact getContact() {
		return contact;
	}
	/**
	 * @param contact the contact to set
	 */
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	/**
	 * @return the manufacture
	 */
	public Manufacture getManufacture() {
		return manufacture;
	}
	/**
	 * @param manufacture the manufacture to set
	 */
	public void setManufacture(Manufacture manufacture) {
		this.manufacture = manufacture;
	}
}

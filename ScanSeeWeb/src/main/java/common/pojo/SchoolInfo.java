package common.pojo;

import java.util.List;

public class SchoolInfo
{
	
	private String college;
	
	private List universityList;
	private List nonProfitList;
	
	private String state;
	
	private String stateabbr;
	
	private Integer universityID;
	private String stateHidden;
	private String universityHidden;
	private String universityName;
	
	
	/**
	 * @return the universityID
	 */
	public Integer getUniversityID()
	{
		return universityID;
	}
	/**
	 * @param universityID the universityID to set
	 */
	public void setUniversityID(Integer universityID)
	{
		this.universityID = universityID;
	}
	/**
	 * @return the stateabbr
	 */
	public String getStateabbr()
	{
		return stateabbr;
	}
	/**
	 * @param stateabbr the stateabbr to set
	 */
	public void setStateabbr(String stateabbr)
	{
		this.stateabbr = stateabbr;
	}
	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}
	private Long userId;
	
	public Long getUserId()
	{
		return userId;
	}
	public void setUserId(Long userId)
	{
		this.userId = userId;
	}
	
	public List getUniversityList()
	{
		return universityList;
	}
	public void setUniversityList(List universityList)
	{
		this.universityList = universityList;
	}
	public List getNonProfitList()
	{
		return nonProfitList;
	}
	public void setNonProfitList(List nonProfitList)
	{
		this.nonProfitList = nonProfitList;
	}
	public String getCollege()
	{
		return college;
	}
	public void setCollege(String college)
	{
		this.college = college;
	}
	/**
	 * @return the stateHidden
	 */
	public String getStateHidden()
	{
		return stateHidden;
	}
	/**
	 * @param stateHidden the stateHidden to set
	 */
	public void setStateHidden(String stateHidden)
	{
		this.stateHidden = stateHidden;
	}
	/**
	 * @return the universityHidden
	 */
	public String getUniversityHidden()
	{
		return universityHidden;
	}
	/**
	 * @param universityHidden the universityHidden to set
	 */
	public void setUniversityHidden(String universityHidden)
	{
		this.universityHidden = universityHidden;
	}
	/**
	 * @param universityName the universityName to set
	 */
	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
	/**
	 * @return the universityName
	 */
	public String getUniversityName() {
		return universityName;
	}
	
	
}

package common.util;

import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import common.pojo.GAddress;

/**
 * this extension of Utility class for utlit methods.
 * 
 * @author manjunatha_gh
 */
public class UtilCode
{
	String[] mainMdoules = { "Home", "MyAppsite", "Promotions", "Analytics", "ManageLocations", "ManageProducts", "UpdateAccounts", "Event", "FundraiserEvent" };

	String[] subModules = { "Logo", "WelcomePage", "Banner", "AnythingPage", "SplOfferPage", "Coupons", "Rebates", "Hotdeals", "AddLocation",
			"UploadLocations", "ManageLocations","Deals" };

	public static final String activeMainModStyle = "mainTab active";

	public static final String inactiveMainModStyel = "mainTab";

	public static final String activeSubModStyle = "subTab active";

	public static final String inactiveSubModStyle = "subTab";

	public static RetailerLeftNavigation setRetailerModulesStyle(String mainmodule, String subModule, RetailerLeftNavigation leftnavigation)
	{

		if (mainmodule.equalsIgnoreCase("Home"))
		{
			leftnavigation.setHomeTabStyle(activeMainModStyle);
			leftnavigation.setAppSiteStyle(inactiveMainModStyel);
			leftnavigation.setPromotionStyle(inactiveMainModStyel);
			leftnavigation.setAnalyticsStyle(inactiveMainModStyel);
			leftnavigation.setManageLocStyle(inactiveMainModStyel);
			leftnavigation.setManageProStyle(inactiveMainModStyel);
			leftnavigation.setAccountStyle(inactiveMainModStyel);
			leftnavigation.setEventStyle(inactiveMainModStyel);
			leftnavigation.setFundraiserStyle(inactiveMainModStyel);
		}
		else if (mainmodule.equalsIgnoreCase("MyAppsite"))
		{
			leftnavigation.setAppSiteStyle(activeMainModStyle);
			leftnavigation.setHomeTabStyle(inactiveMainModStyel);
			leftnavigation.setPromotionStyle(inactiveMainModStyel);
			leftnavigation.setAnalyticsStyle(inactiveMainModStyel);
			leftnavigation.setManageLocStyle(inactiveMainModStyel);
			leftnavigation.setManageProStyle(inactiveMainModStyel);
			leftnavigation.setAccountStyle(inactiveMainModStyel);
			leftnavigation.setEventStyle(inactiveMainModStyel);
			leftnavigation.setFundraiserStyle(inactiveMainModStyel);
		}
		else if (mainmodule.equalsIgnoreCase("Promotions"))
		{
			leftnavigation.setPromotionStyle(activeMainModStyle);
			leftnavigation.setHomeTabStyle(inactiveMainModStyel);
			leftnavigation.setAppSiteStyle(inactiveMainModStyel);
			leftnavigation.setAnalyticsStyle(inactiveMainModStyel);
			leftnavigation.setManageLocStyle(inactiveMainModStyel);
			leftnavigation.setManageProStyle(inactiveMainModStyel);
			leftnavigation.setAccountStyle(inactiveMainModStyel);
			leftnavigation.setEventStyle(inactiveMainModStyel);
			leftnavigation.setFundraiserStyle(inactiveMainModStyel);
		}
		else if (mainmodule.equalsIgnoreCase("Analytics"))
		{
			leftnavigation.setAnalyticsStyle(activeMainModStyle);
			leftnavigation.setHomeTabStyle(inactiveMainModStyel);
			leftnavigation.setAppSiteStyle(inactiveMainModStyel);
			leftnavigation.setPromotionStyle(inactiveMainModStyel);
			leftnavigation.setManageLocStyle(inactiveMainModStyel);
			leftnavigation.setManageProStyle(inactiveMainModStyel);
			leftnavigation.setAccountStyle(inactiveMainModStyel);
			leftnavigation.setEventStyle(inactiveMainModStyel);
			leftnavigation.setFundraiserStyle(inactiveMainModStyel);
		}
		else if (mainmodule.equalsIgnoreCase("ManageLocations"))
		{
			leftnavigation.setManageLocStyle(activeMainModStyle);
			leftnavigation.setHomeTabStyle(inactiveMainModStyel);
			leftnavigation.setAppSiteStyle(inactiveMainModStyel);
			leftnavigation.setPromotionStyle(inactiveMainModStyel);
			leftnavigation.setAnalyticsStyle(inactiveMainModStyel);
			leftnavigation.setManageProStyle(inactiveMainModStyel);
			leftnavigation.setAccountStyle(inactiveMainModStyel);
			leftnavigation.setEventStyle(inactiveMainModStyel);
			leftnavigation.setFundraiserStyle(inactiveMainModStyel);
		}
		else if (mainmodule.equalsIgnoreCase("ManageProducts"))
		{
			leftnavigation.setManageProStyle(activeMainModStyle);
			leftnavigation.setManageLocStyle(inactiveMainModStyel);
			leftnavigation.setHomeTabStyle(inactiveMainModStyel);
			leftnavigation.setAppSiteStyle(inactiveMainModStyel);
			leftnavigation.setPromotionStyle(inactiveMainModStyel);
			leftnavigation.setAnalyticsStyle(inactiveMainModStyel);
			leftnavigation.setAccountStyle(inactiveMainModStyel);
			leftnavigation.setEventStyle(inactiveMainModStyel);
			leftnavigation.setFundraiserStyle(inactiveMainModStyel);
		}
		else if (mainmodule.equalsIgnoreCase("UpdateAccounts"))
		{
			leftnavigation.setAccountStyle(activeMainModStyle);
			leftnavigation.setManageProStyle(inactiveMainModStyel);
			leftnavigation.setManageLocStyle(inactiveMainModStyel);
			leftnavigation.setHomeTabStyle(inactiveMainModStyel);
			leftnavigation.setAppSiteStyle(inactiveMainModStyel);
			leftnavigation.setPromotionStyle(inactiveMainModStyel);
			leftnavigation.setAnalyticsStyle(inactiveMainModStyel);
			leftnavigation.setEventStyle(inactiveMainModStyel);
			leftnavigation.setFundraiserStyle(inactiveMainModStyel);
		}
		else if (mainmodule.equalsIgnoreCase("Event"))
		{
			leftnavigation.setEventStyle(activeMainModStyle);
			leftnavigation.setAccountStyle(inactiveMainModStyel);
			leftnavigation.setManageProStyle(inactiveMainModStyel);
			leftnavigation.setManageLocStyle(inactiveMainModStyel);
			leftnavigation.setHomeTabStyle(inactiveMainModStyel);
			leftnavigation.setAppSiteStyle(inactiveMainModStyel);
			leftnavigation.setPromotionStyle(inactiveMainModStyel);
			leftnavigation.setAnalyticsStyle(inactiveMainModStyel);
			leftnavigation.setFundraiserStyle(inactiveMainModStyel);
		}
		else if (mainmodule.equalsIgnoreCase("FundraiserEvent"))
		{
			leftnavigation.setFundraiserStyle(activeMainModStyle);
			leftnavigation.setAccountStyle(inactiveMainModStyel);
			leftnavigation.setManageProStyle(inactiveMainModStyel);
			leftnavigation.setManageLocStyle(inactiveMainModStyel);
			leftnavigation.setHomeTabStyle(inactiveMainModStyel);
			leftnavigation.setAppSiteStyle(inactiveMainModStyel);
			leftnavigation.setPromotionStyle(inactiveMainModStyel);
			leftnavigation.setAnalyticsStyle(inactiveMainModStyel);
			leftnavigation.setEventStyle(inactiveMainModStyel);
		}

		if (null != subModule) {
			leftnavigation = setRetSubModStyle(subModule, leftnavigation);
		} else {
			leftnavigation = resetSubModStyle(leftnavigation);
		}
		return leftnavigation;
	}

	public static RetailerLeftNavigation setRetSubModStyle(String subModule, RetailerLeftNavigation leftnavigation)
	{

		if (subModule.equalsIgnoreCase("Logo"))
		{
			leftnavigation.setLogoStyle(activeSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);

		}
		else if (subModule.equalsIgnoreCase("WelcomePage"))
		{
			leftnavigation.setWelocomePageStyle(activeSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);
		}
		else if (subModule.equalsIgnoreCase("Banner"))
		{
			leftnavigation.setBannerStyle(activeSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);
		}
		else if (subModule.equalsIgnoreCase("AnythingPage"))
		{
			leftnavigation.setAnythingPageStyle(activeSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);
		}
		else if (subModule.equalsIgnoreCase("SplOfferPage"))
		{
			leftnavigation.setSplPageStyle(activeSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);
		}
		else if (subModule.equalsIgnoreCase("Deals"))
		{
			leftnavigation.setCouponStyle(activeSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);
		}
		else if (subModule.equalsIgnoreCase("Rebates"))
		{
			leftnavigation.setRebatesStyle(activeSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);
		}
		else if (subModule.equalsIgnoreCase("Hotdeals"))
		{
			leftnavigation.setDealsStyle(activeSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);

		}
		else if (subModule.equalsIgnoreCase("AddLocation"))
		{
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(activeSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);

		}
		else if (subModule.equalsIgnoreCase("UploadLocations"))
		{
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(activeSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);

		}
		else if (subModule.equalsIgnoreCase("ManageLocations"))
		{
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(activeSubModStyle);
			leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);

		}
		else if (subModule.equalsIgnoreCase("Giveaway"))
		{
			leftnavigation.setDealsStyle(inactiveSubModStyle);
			leftnavigation.setLogoStyle(inactiveSubModStyle);
			leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
			leftnavigation.setBannerStyle(inactiveSubModStyle);
			leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
			leftnavigation.setSplPageStyle(inactiveSubModStyle);
			leftnavigation.setCouponStyle(inactiveSubModStyle);
			leftnavigation.setRebatesStyle(inactiveSubModStyle);
			leftnavigation.setAddLocationStyle(inactiveSubModStyle);
			leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
			leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
			leftnavigation.setGiveAwaySubStyle(activeSubModStyle);

		}
		return leftnavigation;

	}

	public static RetailerLeftNavigation resetSubModStyle(RetailerLeftNavigation leftnavigation)
	{
		leftnavigation.setLogoStyle(inactiveSubModStyle);
		leftnavigation.setWelocomePageStyle(inactiveSubModStyle);
		leftnavigation.setBannerStyle(inactiveSubModStyle);
		leftnavigation.setAnythingPageStyle(inactiveSubModStyle);
		leftnavigation.setSplPageStyle(inactiveSubModStyle);
		leftnavigation.setCouponStyle(inactiveSubModStyle);
		leftnavigation.setRebatesStyle(inactiveSubModStyle);
		leftnavigation.setDealsStyle(inactiveSubModStyle);
		leftnavigation.setAddLocationStyle(inactiveSubModStyle);
		leftnavigation.setUploadLocationStyle(inactiveSubModStyle);
		leftnavigation.setManageLocationSubStyle(inactiveSubModStyle);
		leftnavigation.setGiveAwaySubStyle(inactiveSubModStyle);
		return leftnavigation;
	}
	
	public static GAddress getGeoDetails(String address){
		//String ADDRESS = "10616 Mellow Meadows,Austin,TX";
		String DEFAULT_KEY = "";
		String URL = "http://maps.googleapis.com/maps/api/geocode/json?&sensor=false&components=country:US";
		URL url;
		GAddress geoDetails = null;
		try
		{
			url = new URL(URL + "&address=" + URLEncoder.encode(address));
			URLConnection conn = url.openConnection();
			StringWriter sw = new StringWriter();
			IOUtils.copy(conn.getInputStream(), sw);
			System.out.println("sw " +sw.toString());
			geoDetails = processResult(sw.toString());
		}
		catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return geoDetails;
		
	}
	
	
	public static GAddress processResult(String response){
		JSONObject json;
		GAddress gaddr = new GAddress();
		try
		{
			json = new JSONObject(response.toString());
			JSONArray resultArray = json.getJSONArray("results");
			if (resultArray != null && resultArray.length() > 0)
			{

				JSONObject resultObj = (JSONObject) resultArray.getJSONObject(0);
				JSONObject geometryObj = resultObj.getJSONObject("geometry");
				if (geometryObj != null)
				{
					String locationType = geometryObj.getString("location_type");
					String latStr = geometryObj.getJSONObject("location").getString("lat");
					String lngStr = geometryObj.getJSONObject("location").getString("lng");
					if (latStr != null && lngStr != null)
					{

						gaddr.setLat(Double.parseDouble(latStr));
						gaddr.setLng(Double.parseDouble(lngStr));
					}
					gaddr.setAddrReturned(resultObj.getString("formatted_address"));
					gaddr.setLocationType(locationType);
					
					
					System.out.println("lat and lng "+latStr +"  "+lngStr);
					System.out.println("location_type "+locationType);
					System.out.println("location type "+resultObj.getString("formatted_address"));
					System.out.println("status " +json.getString("status"));
				}
			}
			gaddr.setStatus(json.getString("status"));
			
		}
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return gaddr;
		
	}
}

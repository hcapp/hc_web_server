package common.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.util.Utility;

public class ConsumerRegistrationTab extends SimpleTagSupport
{
	// this tag will take module name which user has clicked and based on value
	// it will form the html and return.
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(PaginationTagHandler.class);

	public static final String PREFERENCES = "Preferences";
	public static final String SELECTSCHOOL = "Select School";
	public static final String DOUDLECHECK = "Double Check";
	public static final String SHOP = "Shop";
	String[][] modulesDetails = new String[][] { { PREFERENCES, "preferences.htm"}, { SELECTSCHOOL, "selectschool.htm"},
			{ DOUDLECHECK, "doublecheck.htm"}, { SHOP, "shopperdashboard.htm"} };

	/**
	 * This variable holds selects menu name.
	 */

	public String menuTitle = null;

	/**
	 * @return the menuTitle
	 */
	public String getMenuTitle()
	{
		return menuTitle;
	}

	/**
	 * @param menuTitle
	 *            the menuTitle to set
	 */
	public void setMenuTitle(String menuTitle)
	{
		this.menuTitle = menuTitle;
	}

	@Override
	public void doTag() throws JspException, IOException
	{
		LOG.info("Inside ConsumerRegistrationTab tag handler : doTag ");
		PageContext pageContext = (PageContext) getJspContext();
		JspWriter out = pageContext.getOut();
		StringBuffer menuStrBuffer = new StringBuffer("<ul class=\"tabs\">");
		if (Utility.checkNull(this.menuTitle) != "")
		{
			/*if (HOME.equals(this.menuTitle))
			{*/
				menuStrBuffer.append(getMenuItem());
				menuStrBuffer.append("</ul>");
				LOG.info("Pagination tag body :: " + menuStrBuffer.toString());
				out.write(menuStrBuffer.toString());

			//}
		}
		else
		{
			LOG.error("No Menu items Provided");

		}

	}

	public String getMenuItem()
	{
		StringBuffer itemStrBuffer = new StringBuffer();
		for (int i = 0; i < modulesDetails.length; i++)
		{
			if (menuTitle.equals(modulesDetails[i][0]))
			{
				itemStrBuffer.append("<li class=\"active\">");
			}
			else
			{
				itemStrBuffer.append("<li>");
			}
			itemStrBuffer.append("<a href=\"" + modulesDetails[i][1] + "\">");
			//itemStrBuffer.append("<img alt=\"find\" src=\"/ScanSeeWeb/images/consumer/" + modulesDetails[i][2] + "\"/>");
			itemStrBuffer.append(modulesDetails[i][0]);
			itemStrBuffer.append("</a></li>");
		}
		return itemStrBuffer.toString();
	}
}

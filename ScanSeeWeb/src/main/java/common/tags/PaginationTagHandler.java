package common.tags;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is tag handler class for Pagination JSTL tag.
 * 
 * @author manjunatha_gh
 */
public class PaginationTagHandler extends SimpleTagSupport
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(PaginationTagHandler.class);
	/**
	 * This variable holds the current page value.
	 */
	private String currentPage;

	/**
	 * This variable holds the nextPage value.
	 */
	private String nextPage;

	/**
	 * This variable holds the totalSize of list value.
	 */

	private String totalSize;

	/**
	 * This variable holds the pageRange or number or records per page value.
	 */

	private String pageRange;

	/**
	 * This variable holds the pageRange or number or records per page value.
	 */
	private String url;

	/**
	 * This method returns current Page.
	 * 
	 * @return the currentPage.
	 */

	private boolean enablePerPage;

	public String getCurrentPage()
	{
		return currentPage;
	}

	/**
	 * This method sets current Page.
	 * 
	 * @param currentPage
	 *            the currentPage to set.
	 */
	public void setCurrentPage(String currentPage)
	{
		this.currentPage = currentPage;
	}

	/**
	 * This method returns next Page.
	 * 
	 * @return the nextPage.
	 */
	public String getNextPage()
	{
		return nextPage;
	}

	/**
	 * his method sets nextPage.
	 * 
	 * @param nextPage
	 *            the nextPage to set.
	 */
	public void setNextPage(String nextPage)
	{
		this.nextPage = nextPage;
	}

	/**
	 * @return the totalSize.
	 */
	public String getTotalSize()
	{
		return totalSize;
	}

	/**
	 * @param totalSize
	 *            the totalSize to set
	 */
	public void setTotalSize(String totalSize)
	{
		this.totalSize = totalSize;
	}

	/**
	 * @return the url.
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * @param url
	 *            the url to set.
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	/**
	 * @return the pageRange.
	 */
	public String getPageRange()
	{
		return pageRange;
	}

	/**
	 * @param pageRange
	 *            the pageRange to set.
	 */
	public void setPageRange(String pageRange)
	{
		this.pageRange = pageRange;
	}

	public boolean isEnablePerPage()
	{
		return enablePerPage;
	}

	public void setEnablePerPage(boolean enablePerPage)
	{
		this.enablePerPage = enablePerPage;
	}

	/**
	 * this Method process the constructs the body for pagination tag.
	 * 
	 * @throws JspException
	 *             if any excpetion during processing the tag.
	 */
	public void doTag() throws JspException
	{
		LOG.info("Inside PaginationTagHandler : doTag ");
		PageContext pageContext = (PageContext) getJspContext();
		JspWriter out = pageContext.getOut();
		int previousPage = 0;
		int nextpage = 0;
		List<Integer> pageNumberToDisplay = new ArrayList<Integer>();
		try
		{
			LOG.info("Current Page " + this.currentPage);
			StringBuffer sb = new StringBuffer();
			sb.append("<b>Simple tag Example </b>");
			sb.append("Current Page " + this.currentPage + "\n");
			sb.append("nextPage " + this.nextPage + "\n");
			sb.append("totalSize" + this.totalSize + "\n");
			int iTotalSize = Integer.valueOf(this.totalSize);
			int numberOfPages = Integer.valueOf(this.totalSize) / Integer.valueOf(this.pageRange);
			int remainder = Integer.valueOf(this.totalSize) % Integer.valueOf(this.pageRange);
			int numberOfPagesPerPage = Integer.valueOf(this.totalSize) / 20;
			StringBuffer pageHtml = new StringBuffer();

			pageHtml.append("<td align=\"left\" width='67%'><p>");
			if (numberOfPages == 0)
			{
				if (remainder > 0)
				{
					numberOfPagesPerPage = numberOfPagesPerPage + 1;
				}
			}

			if (numberOfPages >= 1 && iTotalSize != Integer.valueOf(this.pageRange))
			{
				if (remainder > 0)
				{
					numberOfPages = numberOfPages + 1;
					numberOfPagesPerPage = numberOfPagesPerPage + 1;
				}

				if (numberOfPages >= 10)
				{

					if (Integer.valueOf(this.currentPage) > 6 && Integer.valueOf(this.currentPage) <= numberOfPages - 4)
					{
						for (int j = Integer.valueOf(this.currentPage) - 5; j <= Integer.valueOf(this.currentPage) + 4; j++)
						{

							pageNumberToDisplay.add(j);
						}

					}
					else if (Integer.valueOf(this.currentPage) >= numberOfPages - 4)
					{
						int pageDiff = 9 - (numberOfPages - (Integer.valueOf(this.currentPage)));

						for (int j = Integer.valueOf(this.currentPage) - pageDiff; j <= numberOfPages; j++)
						{

							pageNumberToDisplay.add(j);
						}
					}
					else
					{
						for (int j = 1; j <= 10; j++)
						{

							pageNumberToDisplay.add(j);
						}
					}
				}
				else
				{
					for (int j = 1; j <= numberOfPages; j++)
					{

						pageNumberToDisplay.add(j);
					}
				}
				// pageHtml.append(startDiv);
				previousPage = Integer.valueOf(this.currentPage) - 1;
				nextpage = Integer.valueOf(this.currentPage) + 1;
				/*
				 * pageHtml.append(getPreviusPageHtml(previousPage));
				 * pageHtml.append(getNextPageHtml(nextpage,numberOfPages));
				 */
				// pageHtml.append(endDiv);
				if (Integer.valueOf(this.currentPage) == 1)
				{
					// To disable the Last records button
					pageHtml.append(getPreviusPageDisableHtml(previousPage));

				}
				else
				{
					pageHtml.append(getPreviusPageHtml(previousPage));
				}
				pageHtml.append(getPaginationText(numberOfPages, pageNumberToDisplay, Integer.valueOf(this.currentPage)));
				if (Integer.valueOf(this.currentPage) == numberOfPages)
				{
					// to disable the Next button
					pageHtml.append(getNextPageDisableHtml(nextpage, numberOfPages));
				}
				else
				{
					pageHtml.append(getNextPageHtml(nextpage, numberOfPages));
				}
				pageHtml.append("</p></td>");

				pageHtml.append("<td align=\"center\">");

				pageHtml.append("Page " + this.currentPage + " of " + numberOfPages);

				pageHtml.append("</td>");

			}
			if (iTotalSize > 20)
			{
				if (this.enablePerPage)
				{
					pageHtml.append("<td align=\"right\">Per Page: <select id='selPerPage' onchange=\"getPerPgaVal();\" class=\"txtBoxPagn\">");

					pageHtml.append(getPerPageHtml(numberOfPagesPerPage));

					pageHtml.append("</select></td>");
				}
			}

			/*
			 * else{ pageHtml.append(getPaginationText(numberOfPages)); }
			 */

			LOG.info("Pagination tag body :: " + pageHtml.toString());

			out.println(pageHtml.toString());
		}
		catch (Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * returns pagination with links.
	 * 
	 * @param previousPage
	 *            previous page number.
	 * @return pagination String.
	 */
	public String getPreviusPageHtml(int previousPage)
	{
		StringBuffer prevContent = new StringBuffer();
		prevContent.append("<a  title=\"BackWard\" href=\"#\" onclick = \"callNextPage(" + 0 + ",'" + this.url + "')\">");
		prevContent.append("<span id=\"backWard\"></span></a>");
		prevContent.append("<a href=\"#\" title=\"Previous\" onclick = \"callNextPage(" + previousPage + ",'" + this.url + "')\">");
		prevContent.append("<span id=\"previous\" ></span></a>");
		return prevContent.toString();
	}

	/**
	 * returns pagination with links.
	 * 
	 * @param previousPage
	 *            previous page number.
	 * @return pagination String.
	 */

	public String getPreviusPageDisableHtml(int previousPage)
	{
		StringBuffer prevContent = new StringBuffer();
		prevContent.append("<a  title=\"disImg\" href=\"#\" onclick = \"callNextPage(" + 0 + ",'" + this.url + "')\">");
		prevContent.append("<span id=\"disImg\"></span></a>");
		prevContent.append("<a href=\"#\" title=\"disImg\" onclick = \"callNextPage(" + previousPage + ",'" + this.url + "')\">");
		prevContent.append("<span id=\"disImg\" ></span></a>");
		return prevContent.toString();
	}

	/**
	 * returns pagination with links.
	 * 
	 * @param nextpage
	 *            next page number.
	 * @param numberOfPages
	 *            number of pages.
	 * @return pagination String.
	 */

	public String getNextPageHtml(int nextpage, int numberOfPages)
	{
		StringBuffer nextContent = new StringBuffer();
		nextContent.append("<a href=\"#\" title=\"Next\" onclick = \"callNextPage(" + nextpage + ",'" + this.url + "')\">");
		nextContent.append("<span id=\"nxt\"></span></a>");
		nextContent.append("<a href=\"#\" title=\"ForWard\" onclick = \"callNextPage(" + numberOfPages + ",'" + this.url + "')\">");
		nextContent.append("<span id=\"forWard\"></span></a>");
		return nextContent.toString();
	}

	/**
	 * returns pagination with disable links.
	 * 
	 * @param nextpage
	 *            next page number.
	 * @param numberOfPages
	 *            number of pages.
	 * @return pagination String.
	 */
	public String getNextPageDisableHtml(int nextpage, int numberOfPages)
	{
		StringBuffer nextContent = new StringBuffer();
		nextContent.append("<a href=\"#\" title=\"disImg\" onclick = \"callNextPage(" + nextpage + ",'" + this.url + "')\">");
		nextContent.append("<span id=\"disImg\"></span></a>");
		nextContent.append("<a href=\"#\" title=\"disImg\" onclick = \"callNextPage(" + numberOfPages + ",'" + this.url + "')\">");
		nextContent.append("<span id=\"disImg\"></span></a>");
		return nextContent.toString();
	}

	/**
	 * Returns the pagination text.
	 * 
	 * @param noOfpages
	 *            takes no of pages input.
	 * @return sb pagination text.
	 */
	public String getPaginationText(int noOfpages, List<Integer> pageNumberToDisplay, int currentPage)
	{
		StringBuffer sb = new StringBuffer();
		sb.append("<label class=\"paginationText\" >");
		for (int i = 0; i < pageNumberToDisplay.size(); i++)
		{
			if (pageNumberToDisplay.get(i) == currentPage)
			{
				sb.append("<a href=\"#\" style=\"color: #CC0000\" onclick = \"callNextPage(" + pageNumberToDisplay.get(i) + ",'" + this.url + "')\">"
						+ pageNumberToDisplay.get(i) + " " + "</a>");
			}
			else
			{
				sb.append("<a href=\"#\" onclick = \"callNextPage(" + pageNumberToDisplay.get(i) + ",'" + this.url + "')\">"
						+ pageNumberToDisplay.get(i) + "</a>");
			}

			// sb.append(i + " ");
		}

		sb.append("</label>");
		return sb.toString();
	}

	public String getPerPageHtml(int size)
	{

		StringBuffer sb = new StringBuffer();

		int perPageSize = 0;
		for (int i = 1; i <= size; i++)
		{
			perPageSize = 20 * i;
			if (perPageSize == Integer.valueOf(this.pageRange))
			{
				sb.append("<option selected='true'>");
				sb.append(String.valueOf(perPageSize));
				sb.append("</option>");
				if (i == 4)
					break;
			}
			else
			{
				sb.append("<option>");
				sb.append(String.valueOf(perPageSize));
				sb.append("</option>");
				if (i == 4)
					break;
			}

		}

		return sb.toString();
	}
}

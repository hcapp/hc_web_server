<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<%@page contentType="text/html;charset=UTF-8"%>
<title>frontpage</title>


<script type="text/javascript">
	//This method is used to call feed list page.
	
	function reply_clickEmp(id){
	document.getElementById("loading").style.display = 'inline' ;
		document.frontpageform.feedType.value = id;
		document.frontpageform.method = "GET";
		document.frontpageform.action = "employment.htm";
		document.frontpageform.submit();
	}
	function reply_click(id) {
	document.getElementById("loading").style.display = 'inline' ;
		document.frontpageform.feedType.value = id;
		document.frontpageform.method = "GET";
		document.frontpageform.action = "home.htm";
		document.frontpageform.submit();
	}
	
	function reply_clickTop(id) {
	document.getElementById("loading").style.display = 'inline' ;
		document.frontpageform.feedType.value = id;
		document.frontpageform.method = "GET";
		document.frontpageform.action = "combineFeeds.htm";
		document.frontpageform.submit();
	}

	function reply_videos(id) {
	document.getElementById("loading").style.display = 'inline' ;
		document.frontpageform.feedType.value = id;
		document.frontpageform.method = "GET";
		document.frontpageform.action = "videos.htm";
		document.frontpageform.submit();
	}

	function reply_real(id) {
	document.getElementById("loading").style.display = 'inline' ;
		document.frontpageform.feedType.value = id;
		document.frontpageform.method = "GET";
		document.frontpageform.action = "realestate.htm";
		document.frontpageform.submit();
	}
	
	
	
	function hideLoading(){
 document.getElementById("loading").style.display = 'none' ;
}
</script>

<style type="text/css">

#loading {
  width: 500px;
  height: 250px;
  top: 40%;
  left: 40%;
 border:2px solid white;
  position: fixed;
  display: block;
  border-radius:20px;
  background-color: black;
  
  
}

#loading-image {
  position: absolute;
  top: 20%;
  left: 15%;
  width:100px;
  height:100px;
  z-index: 90;
}

body {
	margin: 0px;
	padding: 0px;
	overflow: auto;
}

table {
	width: 100%;
	height: 100%;
	min-height: 567px;
	padding: 0px;
	border-collapse: collapse;
	background-color: white;
}

table tr td {
	padding-top: 0;
	padding-bottom: 0;
	padding-left: 0;
	padding-right: 0;
	width: 50%;
	
}

table tr td div.tiles{
width: 675px;
}




img {
	display: block;
	height:600px; 
	width:675px;
}
</style>
</head>
<body onload="hideLoading();">


	<center>
		<form name="frontpageform" commandName="frontpageform">

<div id="loading" style="background: 1px solid black;">
<img id="loading-image" src="images/ajax-loaders.gif" alt="Loading..." /><span style="color:white;font-size: 3em; position: absolute;font-weight:bolder;top: 30%;letter-spacing:0.2em;
  left: 50%;">Please Wait...</span> 
</div>  

			<table>
				<input type="hidden" name="feedType" />
				<tr>

				
							<td align="center" onclick="reply_videos(this.id)" id="videos"><img src="images/videos.jpg" /><div class="tiles" style="z-index:2;position:absolute;top:500px;left:0;border-right: 1px solid white;background-color: black;color:white;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Videos</div></td>
					
				
					<c:choose>
						<c:when
							test="${sessionScope.sports ne null && !empty sessionScope.sports && sessionScope.sports ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="sports">
								<img src="${sessionScope.sports}" />
								<div class="tiles" style="z-index:2;position:absolute;top:500px;left:675px;color: white;border-left: 1px solid white;background-color:black;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Sports</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_click(this.id)" id="sports"><img src="images/sports.jpg" /><div class="tiles" style="z-index:2;position:absolute;top:500px;left:675px;color: white;border-left: 1px solid white;background-color:black;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Sports</div></td>
						</c:otherwise>
					</c:choose>
				</tr>

				<tr>
				
						<td align="center" onclick='location.href="http://www.tylerpaper.com/weather"' ><img src="images/weather.png" /><div class="tiles" style="z-index:2;position:absolute;top:1100px;left:0px;background-color: black;border-right: 1px solid white;color:white;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Weather</div></td>
					
				
					<c:choose>
						<c:when
							test="${sessionScope.health  ne null && !empty sessionScope.health && sessionScope.health ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="health">
								<img src="${sessionScope.health}" />
								<div class="tiles" style="z-index:2;position:absolute;top:1100px;left:675px;background-color: black;color:white;border-left: 1px solid white;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Health</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_click(this.id)" id="health"><img src="images/health.jpg" /><div class="tiles" style="z-index:2;position:absolute;top:1100px;left:675px;background-color: black;color:white;border-left: 1px solid white;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Health</div></td>
						</c:otherwise>
					</c:choose>
				</tr>

				<tr>
					<c:choose>
						<c:when
							test="${sessionScope.business ne null && !empty sessionScope.business && sessionScope.business ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="business">
								<img src="${sessionScope.business}" />
								<div class="tiles" style="z-index:2;position:absolute;top:1700px;left:0;background-color: black;color:white;border-right: 1px solid white;height: 100px;font-weight: bold;font-size:4em;font-family: Arial;letter-spacing: 0.1em;">Business</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_click(this.id)" id="business"><img src="images/business.jpg" /><div class="tiles" style="z-index:2;position:absolute;top:1700px;left:0;background-color: black;color:white;border-right: 1px solid white;height: 100px;font-weight: bold;font-size:4em;font-family: Arial;letter-spacing: 0.1em;">Business</div></td>
						</c:otherwise>
					</c:choose>
				

					
						
							
							<td align="center" onclick="reply_real(this.id)" id="real"><img src="images/real.jpg" />
						<div class="tiles" style="z-index:2;position:absolute;top:1700px;left:675px;background-color: black;color:white;height: 100px;font-weight: bold;border-left: 1px solid white;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Real Estate</div>
							</td>
						
				

				</tr>
				<tr>

					<c:choose>
						<c:when
							test="${sessionScope.top  ne null && !empty sessionScope.top && sessionScope.top ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="top">
								<img src="${sessionScope.top}" />
								<div class="tiles" style="z-index:2;position:absolute;top:2300px;left:0px;background-color: black;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Top News</div>
							</td>
						</c:when>
						<c:otherwise>
							<td align="center" onclick="reply_clickTop(this.id)" id="top"><img src="images/top.jpg" /><div class="tiles" style="z-index:2;position:absolute;top:2300px;left:0px;background-color: black;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Top News</div></td>
						</c:otherwise>
					</c:choose>
					
					<c:choose>
						<c:when
							test="${sessionScope.life  ne null && !empty sessionScope.life && sessionScope.life ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="life">
								<img src="${sessionScope.life}" />
							<div class="tiles" style="z-index:2;position:absolute;top:2300px;left:675px;background-color: black;color:white;height: 100px;font-weight: bold;border-left: 1px solid white;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Life</div>
							</td>
						</c:when>
						<c:otherwise>
							<div class="tiles" style="z-index:2;position:absolute;top:2300px;left:675px;background-color: black;color:white;height: 100px;font-weight: bold;border-left: 1px solid white;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Life</div>
						</c:otherwise>
					</c:choose>
					</tr>
				<tr>
					<c:choose>
						<c:when
							test="${sessionScope.opinion  ne null && !empty sessionScope.opinion && sessionScope.opinion ne ''}">
							<td align="center" onclick="reply_click(this.id)" id="opinion">
								<img src="${sessionScope.opinion}" />
								<div class="tiles" style="z-index:2;position:absolute;top:2900px;background-color:100%;left:0px;background-color: black;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Opinion</div>
							</td>
						</c:when>
						<c:otherwise>
						<div class="tiles" style="z-index:2;position:absolute;top:2900px;background-color:100%;left:0px;background-color: black;color:white;height: 100px;font-weight: bold;border-right: 1px solid white;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Opinion</div>
						</c:otherwise>
					</c:choose>
				
					
							<td align="center" onclick="reply_clickEmp(this.id)" id="employment"><img src="images/employment.png" /><div class="tiles" style="z-index:2;border-left: 1px solid white;position:absolute;top:2900px;left:675px;;background-color: black;color:white;height: 100px;font-weight: bold;font-size: 4em;font-family: Arial;letter-spacing: 0.1em;">Employment</div></td>
						
				</tr>
				
				<tr>
					<td colspan="2"><div id="bottombar">
							<img
								style="float: right; width: 180px; height: 70px; margin-right: 20px; padding: 10px;"
								align="right" src="images/poweredby_1.jpg"
								alt="Powered By HubCiti">
						</div></td>
				</tr>
				
			</table>
		</form>
	</center>
</body>
</html>